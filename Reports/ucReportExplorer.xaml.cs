﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using Ifx.SL;
using EntityBll.SL;
using EntityWireTypeSL;
using System.ComponentModel;
using vFilterControls;
using vUICommon;
using TypeServices;

namespace Reports
{
    public partial class ucReportExplorer : UserControl
    {

        #region Initialize Variables

        private static string _as = "Reports";
        private static string _cn = "ucReportExplorer";

        ObservableCollection<v_ReportGroup_lstRptExplorer_Restricted_Binding> _groups = null;
        ObservableCollection<v_Report_lstRptExplorer_Restricted_Binding> _reports = null;

        ObservableCollection<ReportExplorerNode> _reportNodes = new ObservableCollection<ReportExplorerNode>();

        vQueryTargetType _reportType = vQueryTargetType.None;

        ReportProvider _reportProvider = null;
        ucFilterManager _filterManager = null;

        #endregion Initialize Variables
               

        public ucReportExplorer()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucReportExplorer", IfxTraceCategory.Enter);
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this)) { return; }
            LoadReportNodes();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucReportExplorer", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucReportExplorer", IfxTraceCategory.Leave);
            }
        }

        public void SetFilterManager(ucFilterManager filterManager)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucReportExplorer", IfxTraceCategory.Enter);
                _filterManager = filterManager;
                _reportProvider = new ReportProvider(_filterManager);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucReportExplorer", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucReportExplorer", IfxTraceCategory.Leave);
            }
        }


        #region Report Explorer
        
        public void LoadReportNodes()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadReportNodes", IfxTraceCategory.Enter);

                _groups = CommonClientData_Bll_staticLists.v_ReportGroup_lstRptExplorer_Restricted_BindingListProperty;
                 _reports = CommonClientData_Bll_staticLists.v_Report_lstRptExplorer_Restricted_BindingListProperty;
                 _reportNodes.Clear();
                 foreach (v_ReportGroup_lstRptExplorer_Restricted_Binding item in _groups)
                 {
                     if (item.ParentGroup_Id == null)
                     {
                         _reportNodes.Add(new ReportExplorerNode(ReportsExplorerNodeType.Group, item.v_RptGrp_Id, null, null, item.v_RptGrp_Name, null, false, false));
                     }
                 }

                 xdtRptExp.ItemsSource = _reportNodes;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadReportNodes", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadReportNodes", IfxTraceCategory.Leave);
            }
        }
        
        public ObservableCollection<ReportExplorerNode> ReportNodes
        {
            get { return _reportNodes; }
            set { _reportNodes = value; }
        }
        
        #endregion Report Explorer


        #region Events

        private void btnRunSSRSReport_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRunSSRSReport_Click", IfxTraceCategory.Enter);
                ReportExplorerNode rptNode = ((ReportExplorerNode)((Infragistics.Controls.Menus.XamDataTreeNodeControl)((Grid)((Button)(sender)).Parent).Parent).Node.Data);
                _reportProvider.StartReport((Guid)rptNode.FilterSession_Id, vQueryTargetType.SSRS);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRunSSRSReport_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRunSSRSReport_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnRunExcelReport_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRunExcelReport_Click", IfxTraceCategory.Enter);
                ReportExplorerNode rptNode = ((ReportExplorerNode)((Infragistics.Controls.Menus.XamDataTreeNodeControl)((Grid)((Button)(sender)).Parent).Parent).Node.Data);
                _reportProvider.StartReport((Guid)rptNode.FilterSession_Id, vQueryTargetType.Excel);


                //// Create an instance of the open file dialog box.
                //OpenFileDialog openFileDialog1 = new OpenFileDialog();

                //// Set filter options and filter index.
                //openFileDialog1.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*";
                //openFileDialog1.FilterIndex = 1;

                //openFileDialog1.Multiselect = true;

                //// Call the ShowDialog method to show the dialog box.
                //bool? userClickedOK = openFileDialog1.ShowDialog();

                //// Process input if the user clicked OK.
                //if (userClickedOK == true)
                //{
                //    // Open the selected file to read.
                //    System.IO.Stream fileStream = openFileDialog1.File.OpenRead();

                //    using (System.IO.StreamReader reader = new System.IO.StreamReader(fileStream))
                //    {
                //        // Read the first line from the file and write it the textbox.
                //        MessageBox.Show(reader.ReadLine());
                //    }
                //    fileStream.Close();
                //}





            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRunExcelReport_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRunExcelReport_Click", IfxTraceCategory.Leave);
            }
        }
        
        #endregion Events




    }
}
