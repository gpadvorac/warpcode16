﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using ProxyWrapper;
using Ifx.SL;
using System.ComponentModel;
using Infragistics.Controls.Grids;
using Velocity.SL;
using vReportParameter;
using EntityWireTypeSL;
using System.Windows.Browser;
using vFilterControls;
using vUICommon;
using TypeServices;

namespace Reports
{
    public partial class ucReportManager : UserControl
    {


        #region Initialize Variables

        private static string _as = "Reports";
        private static string _cn = "ucReportManager";

        vReportService_ProxyWrapper _proxy = null;

        //List<vReport_cmb_Binding> _list = new List<vReport_cmb_Binding>();

        Guid? _reportId = null;
        string _reportName = null;
        string _reportSysName = null;
        static string _reportServer = null;


        #endregion Initialize Variables



        #region Constructors


        public ucReportManager()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucReportManager", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();
                Initialize_navList();
                this.Loaded += new RoutedEventHandler(ucReportClient_Loaded);
                FilterManager.ReturnFilterData += new ReturnFilterDataEventHandler(FilterManager_ReturnFilterData);
                _proxy = new vReportService_ProxyWrapper();
                _proxy.Getv_Report_cmbByUserIdCompleted += new EventHandler<Getv_Report_cmbByUserIdCompletedEventArgs>(Getv_Report_cmbByUserIdCompleted);
                _proxy.RunReportCompleted += new EventHandler<RunReportCompletedEventArgs>(RunReportCompleted);

        
                

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucReportManager", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucReportManager", IfxTraceCategory.Leave);
            }
        }


        #endregion Constructors


        #region Load

        void ucReportClient_Loaded(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucReportClient_Loaded", IfxTraceCategory.Enter);

                //LoadReports();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucReportClient_Loaded", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucReportClient_Loaded", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// An Infragistics XamGrid event that fires when the grid is first initialized
        /// as this control loads. Various custom settings can be set here such as binding editor
        /// controls to grid cells, allowing new rows to be added, etc.
        /// </summary>
        void Initialize_navList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Initialize_navList", IfxTraceCategory.Enter);


                xgdReports.EditingSettings.IsOnCellActiveEditingEnabled = false;
                xgdReports.EditingSettings.IsEnterKeyEditingEnabled = true;
                xgdReports.EditingSettings.IsMouseActionEditingEnabled = MouseEditingAction.SingleClick;

                xgdReports.RowSelectorSettings.Visibility = Visibility.Collapsed;
                //xgdReports.RowSelectorSettings.EnableRowNumbering = true;
                xgdReports.IsAlternateRowsEnabled = true;


                // SET EVENTS
                //xgdReports.CellClicked += new EventHandler<CellClickedEventArgs>(xgdReports_CellClicked);

                // Grid Cell Events
                //xgdReports.RowAdding += new EventHandler<CancellableRowAddingEventArgs>(xgdReports_RowAdding);


                // SAVE This one
                xgdReports.SelectedRowsCollectionChanged += new EventHandler<SelectionCollectionChangedEventArgs<SelectedRowsCollection>>(xgdReports_SelectedRowsCollectionChanged);

                // Clipboard
                xgdReports.SelectionSettings.CellSelection = SelectionType.None;
                xgdReports.SelectionSettings.RowSelection = SelectionType.Single;
                xgdReports.SelectionSettings.CellClickAction = CellSelectionAction.SelectRow;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Initialize_navList", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Initialize_navList", IfxTraceCategory.Leave);
            }
        }

        public void LoadReports()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadReports", IfxTraceCategory.Enter);
                if (NavList_ItemSource != null)
                {
                    NavList_ItemSource.Clear();
                }
                _proxy.Begin_Getv_Report_cmbByUserId(Credentials.UserId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadReports", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadReports", IfxTraceCategory.Leave);
            }
        }


        //////public void SetProjectFilterListData(List<IvFilterListBinding> data)
        //////{
        //////    Guid? traceId = Guid.NewGuid();
        //////    try
        //////    {
        //////        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetProjectFilterListData", IfxTraceCategory.Enter);

        //////        ReportMetaData.ProjectList = data;

        //////    }
        //////    catch (Exception ex)
        //////    {
        //////        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetProjectFilterListData", ex);
        //////        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //////    }
        //////    finally
        //////    {
        //////        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetProjectFilterListData", IfxTraceCategory.Leave);
        //////    }
        //////}



        #endregion Load


        #region Properties


        public List<vReport_cmb_Binding> NavList_ItemSource
        {
            get { return (List<vReport_cmb_Binding>)xgdReports.ItemsSource; }
            set { xgdReports.ItemsSource = value; }
        }

        public static string ReportServer
        {
            get { return _reportServer; }
            //  set { ucReportClient._reportServer = value; }
        }




        #endregion Properties


        #region Button Event


        //private void btnRunReport_Click(object sender, RoutedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRunReport_Click", IfxTraceCategory.Enter);

        //        if (_reportId == null)
        //        {
        //            MessageBox.Show("Please select a report first.", "Report selection", MessageBoxButton.OK);
        //            return;
        //        }
        //        //RunReport();
        //        //MessageBox.Show("Running report:  " + _reportId.ToString(), "Report selection", MessageBoxButton.OK);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRunReport_Click", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRunReport_Click", IfxTraceCategory.Leave);
        //    }
        //}


        void FilterManager_ReturnFilterData(object sender, ReturnFilterDataArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FilterManager_ReturnFilterData", IfxTraceCategory.Enter);

                if (_reportId == null)
                {
                    MessageBox.Show("Please select a report first.", "Report selection", MessageBoxButton.OK);
                    return;
                }

                if (FilterManager.FilterTargetType == vQueryTargetType.SSRS || FilterManager.FilterTargetType == vQueryTargetType.Excel)
                {
                    vReport rpt = new vReport((Guid)FilterManager.ReportId, FilterManager.ReportSysName, FilterManager.ReportName, Convert_FilterReturnParameter_To_ReportParameters(e.ReturnData));
                    RunReport(rpt);
                }
                else
                {
                    // don't know what to do yet as we dont know what the 'other' options are.
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FilterManager_ReturnFilterData", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FilterManager_ReturnFilterData", IfxTraceCategory.Leave);
            }
        }


        List<vParameter> Convert_FilterReturnParameter_To_ReportParameters(List<vFilterReturnParameter> returnData)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Convert_FilterReturnParameter_To_ReportParameters", IfxTraceCategory.Enter);
                if (returnData == null)
                {
                    return null;
                }
                else if (returnData.Count == 0)
                {
                    return null;
                }
                else
                {
                    List<vParameter> parms = new List<vParameter>();
                    foreach (var item in returnData)
                    {
                        string value = null;
                        if (item.ParameterValue != null)
                        {
                            value = item.ParameterValue.ToString();
                        }
                        parms.Add(new vParameter(item.ParameterName, value));
                    }
                    return parms;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Convert_FilterReturnParameter_To_ReportParameters", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Convert_FilterReturnParameter_To_ReportParameters", IfxTraceCategory.Leave);
            }
        }



        #endregion Button Event



        #region Service Calls


        #region Getv_Report_cmbByUserId

        void Getv_Report_cmbByUserIdCompleted(object sender, Getv_Report_cmbByUserIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_cmbByUserIdCompleted", IfxTraceCategory.Enter);

                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                object[] obj = (object[])array[0];
                _reportServer = ((object[])array[0])[0] as string;
                object[] reports = array[1] as object[];
                if (NavList_ItemSource == null)
                {
                    //NavList_ItemSource = new List<vReport_cmb_Binding>();
                    xgdReports.ItemsSource = new List<vReport_cmb_Binding>();
                }
                if (reports != null)
                {
                    List<vReport_cmb_Binding> list = new List<vReport_cmb_Binding>();
                    for (int i = 0; i < reports.GetUpperBound(0) + 1; i++)
                    {
                        //NavList_ItemSource.Add(new vReport_cmb_Binding((object[])data[i]));
                        list.Add(new vReport_cmb_Binding((object[])reports[i]));
                    }
                    xgdReports.ItemsSource = list;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_cmbByUserIdCompleted", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_cmbByUserIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Getv_Report_cmbByUserId


        #region Run Report

        void RunReport(vReport rpt)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", IfxTraceCategory.Enter);


                if (rpt == null)
                {
                    MessageBox.Show("The report metadata was not found.  Please contac support.", "Missing Information", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    //***  might need to use   GetValuesAsObjectJaggedArray
                    //_proxy.Begin_RunReport(rpt.GetValuesAsObjectArray());
                }

                //// WYCOFF; JOSEPH W
                //// Create a test parameter

                //if (_reportSysName == "SalesReport_v01")
                //{
                //    List<vParameter> parms = new List<vParameter>();
                //    parms.Add(new vParameter("WorkOrderSalesman", "WYCOFF; JOSEPH W"));
                //    vReport rpt = new vReport((Guid)_reportId, _reportSysName, parms);
                //    _proxy.Begin_RunReport(rpt.GetValuesAsObjectArray());
                //}
                //else
                //{
                //    vReport rpt = new vReport((Guid)_reportId, _reportSysName);
                //    _proxy.Begin_RunReport(rpt.GetValuesAsObjectArray());
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", IfxTraceCategory.Leave);
            }
        }

        void RunReportCompleted(object sender, RunReportCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReportCompleted", IfxTraceCategory.Enter);
                //Guid? key = e.Result;
                //if (key == null)
                //{
                //    MessageBox.Show("The report could not be returned from the server.  If this problem continues, please contact support.", "Report Not Returned", MessageBoxButton.OK);
                //    return;
                //}
                //CallReport((Guid)key);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReportCompleted", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReportCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Run Report


        #endregion Service Calls


        void CallReport(Guid key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReportCompleted", IfxTraceCategory.Enter);

                //http://localhost/VelocityServer_v01/vReports/VelocityReportService.aspx
                //HtmlPage.Window.Navigate(new Uri("myPage.aspx", UriKind.Relative), "_blank");
                // http://localhost/VelocityServer_v01/vReports/VelocityReportService.aspx?Param=1ad671f8-2a05-4730-b611-bad1576b9788

                // http://localhost/Firesafe.Web/vReports/VelocityReportService.aspx


                HtmlPage.Window.Navigate(new Uri(_reportServer + "/vReports/VelocityReportService.aspx?Param=" + key.ToString(), UriKind.Absolute), "_blank");

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReportCompleted", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReportCompleted", IfxTraceCategory.Leave);
            }
        }

        void xgdReports_SelectedRowsCollectionChanged(object sender, Infragistics.Controls.Grids.SelectionCollectionChangedEventArgs<Infragistics.Controls.Grids.SelectedRowsCollection> e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "xgdReports_SelectedRowsCollectionChanged", IfxTraceCategory.Enter);
                if (e.NewSelectedItems.Count == 1)
                {
                    vReport_cmb_Binding obj = e.NewSelectedItems[0].Data as vReport_cmb_Binding;
                    if (obj == null) { return; }
                    _reportId = obj.v_Rpt_Id;
                    _reportName = obj.v_Rpt_Name;
                    _reportSysName = obj.v_Rpt_SysName;
                    LoadFilters(_reportSysName);
                }
                else
                {
                    _reportId = null;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "xgdReports_SelectedRowsCollectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "xgdReports_SelectedRowsCollectionChanged", IfxTraceCategory.Leave);
            }
        }

        void LoadFilters(string reportName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadFilters", IfxTraceCategory.Enter);
                FilterManager.ClearFilters();
                vReport rpt = null;
                switch (reportName)
                {
                    case "IdeasPastDue":
                        if (_reportId == null) { return; }
                        rpt = new vReport((Guid)_reportId, _reportSysName, _reportName);
                        RunReport(rpt);
                        break;
                    case "IdeaStatusByProject":
                        if (_reportId == null) { return; }
                        //FilterManager.SetFilters(ReportMetaData.GetFilterSessionDataFor_IdeaStatusByProject(_reportId, _reportName, _reportSysName));
                        break;
                    //case "ChristmasList":
                    //    //spChristmasList
                    //    FilterManager.SetFilters(ReportMetaData.GetFilterSessionDataFor_ChristmasList(_reportId, _reportName, _reportSysName));
                    //    break;
                    //case "EfficiencyForServiceTechs":
                    //    //spEfficiencyForServiceTechs
                    //    FilterManager.SetFilters(ReportMetaData.GetFilterSessionDataFor_EfficiencyForServiceTechs(_reportId, _reportName, _reportSysName));
                    //    break;
                    //case "SalesReport_v01":
                    //    //spSalesReport_v01
                    //    // get employee list from sm_EMPLOYEE
                    //    FilterManager.SetFilters(ReportMetaData.GetFilterSessionDataFor_SalesReport(_reportId, _reportName, _reportSysName));
                    //    break;
                    //case "WorkOrdersBySalespersons":
                    //    //spWorkOrdersBySalespersons
                    //    FilterManager.SetFilters(ReportMetaData.GetFilterSessionDataFor_WorkOrdersBySalespersons(_reportId, _reportName, _reportSysName));
                    //    break;
                    //case "WorkOrderMarginBySalespersons":
                    //    FilterManager.SetFilters(ReportMetaData.GetFilterSessionDataFor_WorkOrderMarginBySalespersons(_reportId, _reportName, _reportSysName));
                    //    break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadFilters", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadFilters", IfxTraceCategory.Leave);
            }
        }

        //void GetFilterSessionDataFor_ChristmasList()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterSessionDataFor_ChristmasList", IfxTraceCategory.Enter);
        //        // Build Filter Parameter List
        //        List<vFilterControlInputFieldMetadata> inputFields = new List<vFilterControlInputFieldMetadata>();
        //        // 1st InputField

        //        // Setup the Start Date field
        //        List<vFilterControlInputFieldRuleMetadata> startDateRules = new List<vFilterControlInputFieldRuleMetadata>();
        //        vFilterControlInputFieldRuleMetadata startDateRule = new vFilterControlInputFieldRuleMetadata(vDateRangeFilter.RuleKeyType.StartDateRequiredKey, "Start date is required", false);
        //        startDateRules.Add(startDateRule);


        //        //vFilterControlInputFieldMetadata inputFieldStartMD = new vFilterControlInputFieldMetadata("Start", "Start date", vFilterInputFieldDataType.Date, true, null, vFilterInputFieldDataType.Date, startDateRules);
        //        vFilterControlInputFieldMetadata inputFieldStartMD = new vFilterControlInputFieldMetadata("Start", "Start date", vFilterInputFieldDataType.Date, true, null, null, startDateRules);
        //        inputFields.Add(inputFieldStartMD);

        //        //// Setup the end date field
        //        //List<vFilterControlInputFieldRuleMetadata> endDateRules = new List<vFilterControlInputFieldRuleMetadata>();
        //        //vFilterControlInputFieldRuleMetadata endDateRule = new vFilterControlInputFieldRuleMetadata(vDateRangeFilter.RuleKeyType.EndDateRequiredKey, "End date is required", false);
        //        //endDateRules.Add(endDateRule);


        //        //vFilterControlInputFieldMetadata inputFieldEndMD = new vFilterControlInputFieldMetadata("End", "End date", vFilterInputFieldDataType.Date, true, null, vFilterInputFieldDataType.Date, endDateRules);
        //        vFilterControlInputFieldMetadata inputFieldEndMD = new vFilterControlInputFieldMetadata("End", "End date", vFilterInputFieldDataType.Date, true, null, null, null);
        //        inputFields.Add(inputFieldEndMD);

        //        // Build Filter Item List
        //        List<vFilterControlMetadata> filterControls = new List<vFilterControlMetadata>();
        //        // 1st Filer Item
        //        vFilterControlMetadata item = new vFilterControlMetadata(vFilterType.DateRange, "Work order date range", "Select a date range for the work orders.", true, inputFields);
        //        filterControls.Add(item);

        //        // Create the metadata object
        //        vFilterSessionMetadata fsm = new vFilterSessionMetadata(vFilterTarget.Report, _reportId, _reportName, _reportSysName, filterControls);
        //        FilterManager.SetFilters(fsm);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterSessionDataFor_ChristmasList", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterSessionDataFor_ChristmasList", IfxTraceCategory.Leave);
        //    }
        //}

        //void GetFilterSessionDataFor_WorkOrdersBySalespersons()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterSessionDataFor_WorkOrdersBySalespersons", IfxTraceCategory.Enter);

        //        // Build Filter Parameter List
        //        List<vFilterControlInputFieldMetadata> inputFields = new List<vFilterControlInputFieldMetadata>();
        //        // 1st InputField

        //        // Setup the Start Date field
        //        List<vFilterControlInputFieldRuleMetadata> startDateRules = new List<vFilterControlInputFieldRuleMetadata>();
        //        vFilterControlInputFieldRuleMetadata startDateRule = new vFilterControlInputFieldRuleMetadata(vDateRangeFilter.RuleKeyType.StartDateRequiredKey, "Start date is required", false);
        //        startDateRules.Add(startDateRule);


        //        //vFilterControlInputFieldMetadata inputFieldStartMD = new vFilterControlInputFieldMetadata("Start", "Start date", vFilterInputFieldDataType.Date, true, null, vFilterInputFieldDataType.Date, startDateRules);
        //        vFilterControlInputFieldMetadata inputFieldStartMD = new vFilterControlInputFieldMetadata("Start", "Start date", vFilterInputFieldDataType.Date, true, null, null, startDateRules);
        //        inputFields.Add(inputFieldStartMD);

        //        //// Setup the end date field
        //        //List<vFilterControlInputFieldRuleMetadata> endDateRules = new List<vFilterControlInputFieldRuleMetadata>();
        //        //vFilterControlInputFieldRuleMetadata endDateRule = new vFilterControlInputFieldRuleMetadata(vDateRangeFilter.RuleKeyType.EndDateRequiredKey, "End date is required", false);
        //        //endDateRules.Add(endDateRule);


        //        //vFilterControlInputFieldMetadata inputFieldEndMD = new vFilterControlInputFieldMetadata("End", "End date", vFilterInputFieldDataType.Date, true, null, vFilterInputFieldDataType.Date, endDateRules);
        //        vFilterControlInputFieldMetadata inputFieldEndMD = new vFilterControlInputFieldMetadata("End", "End date", vFilterInputFieldDataType.Date, true, null, null, null);
        //        inputFields.Add(inputFieldEndMD);

        //        // Build Filter Item List
        //        List<vFilterControlMetadata> filterControls = new List<vFilterControlMetadata>();
        //        // 1st Filer Item
        //        vFilterControlMetadata item = new vFilterControlMetadata(vFilterType.DateRange, "Work order date range", "Select a date range for the work orders.", true, inputFields);
        //        filterControls.Add(item);

        //        // Create the metadata object
        //        vFilterSessionMetadata fsm = new vFilterSessionMetadata(vFilterTarget.Report, _reportId, _reportName, _reportSysName, filterControls);
        //        FilterManager.SetFilters(fsm);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterSessionDataFor_WorkOrdersBySalespersons", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterSessionDataFor_WorkOrdersBySalespersons", IfxTraceCategory.Leave);
        //    }
        //}

        //void GetFilterSessionDataFor_EfficiencyForServiceTechs()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterSessionDataFor_EfficiencyForServiceTechs", IfxTraceCategory.Enter);
        //        // Build Filter Parameter List
        //        List<vFilterControlInputFieldMetadata> inputFields = new List<vFilterControlInputFieldMetadata>();
        //        // 1st InputField

        //        // Setup the Start Date field
        //        List<vFilterControlInputFieldRuleMetadata> startDateRules = new List<vFilterControlInputFieldRuleMetadata>();
        //        vFilterControlInputFieldRuleMetadata startDateRule = new vFilterControlInputFieldRuleMetadata(vDateRangeFilter.RuleKeyType.StartDateRequiredKey, "Start date is required", false);
        //        startDateRules.Add(startDateRule);


        //        //vFilterControlInputFieldMetadata inputFieldStartMD = new vFilterControlInputFieldMetadata("Start", "Start date", vFilterInputFieldDataType.Date, true, null, vFilterInputFieldDataType.Date, startDateRules);
        //        vFilterControlInputFieldMetadata inputFieldStartMD = new vFilterControlInputFieldMetadata("Start", "Start date", vFilterInputFieldDataType.Date, true, null, null, startDateRules);
        //        inputFields.Add(inputFieldStartMD);

        //        //// Setup the end date field
        //        //List<vFilterControlInputFieldRuleMetadata> endDateRules = new List<vFilterControlInputFieldRuleMetadata>();
        //        //vFilterControlInputFieldRuleMetadata endDateRule = new vFilterControlInputFieldRuleMetadata(vDateRangeFilter.RuleKeyType.EndDateRequiredKey, "End date is required", false);
        //        //endDateRules.Add(endDateRule);


        //        //vFilterControlInputFieldMetadata inputFieldEndMD = new vFilterControlInputFieldMetadata("End", "End date", vFilterInputFieldDataType.Date, true, null, vFilterInputFieldDataType.Date, endDateRules);
        //        vFilterControlInputFieldMetadata inputFieldEndMD = new vFilterControlInputFieldMetadata("End", "End date", vFilterInputFieldDataType.Date, true, null, null, null);
        //        inputFields.Add(inputFieldEndMD);

        //        // Build Filter Item List
        //        List<vFilterControlMetadata> filterControls = new List<vFilterControlMetadata>();
        //        // 1st Filer Item
        //        vFilterControlMetadata item = new vFilterControlMetadata(vFilterType.DateRange, "Work order date range", "Select a date range for the work orders.", true, inputFields);
        //        filterControls.Add(item);

        //        // Create the metadata object
        //        vFilterSessionMetadata fsm = new vFilterSessionMetadata(vFilterTarget.Report, _reportId, _reportName, _reportSysName, filterControls);
        //        FilterManager.SetFilters(fsm);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterSessionDataFor_EfficiencyForServiceTechs", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterSessionDataFor_EfficiencyForServiceTechs", IfxTraceCategory.Leave);
        //    }
        //}

        //void GetFilterSessionDataFor_SalesReport()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterSessionDataFor_SalesReport", IfxTraceCategory.Enter);

        //        //TestGetlist();
        //        // Build Filter control List
        //        List<vFilterControlMetadata> filterControls = new List<vFilterControlMetadata>();


        //        //******************  1st Filter Control

        //        // Build Filter Parameter List
        //        List<vFilterControlInputFieldMetadata> inputFieldsForDateRange = new List<vFilterControlInputFieldMetadata>();
        //        // 1st InputField

        //        // Setup the Start Date field
        //        List<vFilterControlInputFieldRuleMetadata> startDateRules = new List<vFilterControlInputFieldRuleMetadata>();
        //        vFilterControlInputFieldRuleMetadata startDateRule = new vFilterControlInputFieldRuleMetadata(vDateRangeFilter.RuleKeyType.StartDateRequiredKey, "Start date is required", false);
        //        startDateRules.Add(startDateRule);


        //        //vFilterControlInputFieldMetadata inputFieldStartMD = new vFilterControlInputFieldMetadata("Start", "Start date", vFilterInputFieldDataType.Date, true, null, vFilterInputFieldDataType.Date, startDateRules);
        //        vFilterControlInputFieldMetadata inputFieldStartMD = new vFilterControlInputFieldMetadata("Start", "Start date", vFilterInputFieldDataType.Date, true, null, null, startDateRules);
        //        inputFieldsForDateRange.Add(inputFieldStartMD);


        //        //vFilterControlInputFieldMetadata inputFieldEndMD = new vFilterControlInputFieldMetadata("End", "End date", vFilterInputFieldDataType.Date, true, null, vFilterInputFieldDataType.Date, endDateRules);
        //        vFilterControlInputFieldMetadata inputFieldEndMD = new vFilterControlInputFieldMetadata("End", "End date", vFilterInputFieldDataType.Date, true, null, null, null);
        //        inputFieldsForDateRange.Add(inputFieldEndMD);


        //        // 1st Filer Item
        //        vFilterControlMetadata daterangeControl = new vFilterControlMetadata(vFilterType.DateRange, "Work order date range", "Select a date range for the work orders.", true, inputFieldsForDateRange);
        //        filterControls.Add(daterangeControl);



        //        //*************** 2nd Filter Control

        //        // Empoyee List Metadata

        //        // Create rules for this field
        //        List<vFilterControlInputFieldRuleMetadata> employeeListRules = new List<vFilterControlInputFieldRuleMetadata>();
        //        vFilterControlInputFieldRuleMetadata rule = new vFilterControlInputFieldRuleMetadata(vListFilter.RuleKeyType.RequiredKey, "Employee is required", false);
        //        employeeListRules.Add(rule);

        //        //List<IvFilterListBinding> list = new List<IvFilterListBinding>();
        //        //list.Add(new SampleListData(1, "xxxxxxxx"));
        //        //list.Add(new SampleListData(2, "ccccccccccc"));
        //        //list.Add(new SampleListData(3, "vvvvvvvvvvv"));
        //        //list.Add(new SampleListData(4, "bbbbbbbbbbbb"));
        //        //list.Add(new SampleListData(5, "qqqqqqqqq"));

        //        SalesEmployee_BindingList list = vFilterDataSupport.SalesEmployee_List;


        //        List<vFilterControlInputFieldMetadataAttribute> attributes = new List<vFilterControlInputFieldMetadataAttribute>();
        //        attributes.Add(new vFilterControlInputFieldMetadataAttribute("OutputDataType", vFilterIdDataType.Guid));
        //        attributes.Add(new vFilterControlInputFieldMetadataAttribute("IdName", "EmpoyeeId"));
        //        attributes.Add(new vFilterControlInputFieldMetadataAttribute("IsMultiSelect", false));
        //        attributes.Add(new vFilterControlInputFieldMetadataAttribute("List1", list));
        //        attributes.Add(new vFilterControlInputFieldMetadataAttribute("ListNameColumnHeader", "Employees"));
        //        attributes.Add(new vFilterControlInputFieldMetadataAttribute("UseListLabel", false));
        //        attributes.Add(new vFilterControlInputFieldMetadataAttribute("FilterMaxWidth", 300));
        //        attributes.Add(new vFilterControlInputFieldMetadataAttribute("FilterMaxHeight", 300));

        //        vFilterControlInputFieldMetadata inputField_Employee_MD = new vFilterControlInputFieldMetadata("PM", "Employees", vFilterInputFieldDataType.Guid, true, null, attributes, employeeListRules);
        //        List<vFilterControlInputFieldMetadata> inputFields_Employee = new List<vFilterControlInputFieldMetadata>();
        //        inputFields_Employee.Add(inputField_Employee_MD);

        //        vFilterControlMetadata filterControlMD_EmployeeList = new vFilterControlMetadata(vFilterType.List, "Sales Employee", "Select an employee from the list.", true, inputFields_Employee);
        //        filterControls.Add(filterControlMD_EmployeeList);



        //        // Create the metadata object
        //        vFilterSessionMetadata fsm = new vFilterSessionMetadata(vFilterTarget.Report, _reportId, _reportName, _reportSysName, filterControls);
        //        FilterManager.SetFilters(fsm);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterSessionDataFor_SalesReport", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterSessionDataFor_SalesReport", IfxTraceCategory.Leave);
        //    }
        //}

        public void ClearReportList()
        {
            if (NavList_ItemSource != null)
            {
                NavList_ItemSource.Clear();
            }
        }

        void TestGetlist()
        {
            //FiresafeReport_ProxyWrapper proxy = new FiresafeReport_ProxyWrapper();
            //proxy.SalesEmployeeListCompleted += new EventHandler<SalesEmployeeListCompletedEventArgs>(proxy_SalesEmployeeListCompleted);
            //proxy.Begin_SalesEmployeeList();

        }

        //void proxy_SalesEmployeeListCompleted(object sender, SalesEmployeeListCompletedEventArgs e)
        //{
        //    object[] data = e.Result;
        //    string test = data.ToString();
        //}

    }
}
