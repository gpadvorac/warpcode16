﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Infragistics.Controls.Menus;
using Ifx.SL;
using EntityWireTypeSL;
using EntityBll.SL;

namespace Reports
{

    public enum ReportsExplorerNodeType
    {
        None,
        Group,
        Report,
        LoadingStub
    }


    public class ReportExplorerNode
    {


        #region Initialize Variables

        /*
         
v_Rpt_Id, 
v_RptGrpMM_RptGrp_Id AS RptGrp_Id, 
v_Rpt_Rpt_Id, 
v_Rpt_RptTp_Id, 
v_Rpt_SortIndex, 
v_Rpt_Name, 
v_Rpt_SysName, 
v_Rpt_SpName, 
v_Rpt_Desc         
         
         */


        //http://blogs.infragistics.com/blogs/curtis_taylor/archive/2010/06/23/control-monster-explores-multi-level-xamtrees-part-i.aspx

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "Reports";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "ReportExplorerNode";

        Guid? _report_Id;
        Guid? _group_Id;
        Guid? _parentReport_Id;
        Guid? _filterSession_Id;
        int _sortOrder = 0;
        string _nodeName;
        string _sysName;
        string _sprocName;
        string _description;
        bool _isSSRS = false;
        bool _isExcel = false;

        ReportsExplorerNodeType _nodeType = ReportsExplorerNodeType.Group;
        ////NodeType _treeNodeType;
        ////int _itemCount;
        ////string _objectName;
        ////string _objectNumber;
        //Guid? _app_Id;
        //Guid? _parent_Id;
        ////EntityType _parentType;
        //int _childArtifactCount = 0;
        //int _childRoleCount = 0;
        ////int _childUserCount = 0;
        string _imageURI;
        ObservableCollection<ReportExplorerNode> _children = new ObservableCollection<ReportExplorerNode>();
        XamDataTreeNode _parentTreeNode = null;

        static string _loadingStubText = "Loading Data";
        static string _rootHeaderText = "Application";
        static string _noChildrenText = "No Items";

        //ProxyWrapper.v_UiArtifactEntityService_ProxyWrapper _uiArtifactProxy = null;
        //ProxyWrapper.aspnet_SecurityGeneralEntityService_ProxyWrapper _securityProxy = null;
        //ProxyWrapper.aspnet_RolesEntityService_ProxyWrapper _aspnet_RolesProxy = null;
        //ProxyWrapper.aspnet_UsersEntityService_ProxyWrapper _aspnet_UsersProxy = null;


        #endregion Initialize Variables


        public ReportExplorerNode(ReportsExplorerNodeType type, Guid? grp_Id, Guid? rpt_Id, Guid? filterSession_Id, string name, string description, bool isSSRS, bool isExcel)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReportExplorerNode", IfxTraceCategory.Enter);
                _nodeType = type;
                _group_Id = grp_Id;
                _report_Id = rpt_Id;
                _filterSession_Id = filterSession_Id;
                _nodeName = name;
                _description = description;
                _isSSRS = isSSRS;
                _isExcel = isExcel;
                if (_nodeType == ReportsExplorerNodeType.Group)
                {
                    LoadChildGroups();
                    LoadChildReports();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReportExplorerNode", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReportExplorerNode", IfxTraceCategory.Leave);
            }
        }

        void LoadChildGroups()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildGroups", IfxTraceCategory.Enter);

                ObservableCollection<v_ReportGroup_lstRptExplorer_Restricted_Binding> groups = CommonClientData_Bll_staticLists.v_ReportGroup_lstRptExplorer_Restricted_BindingListProperty;
                if (groups == null) { return; }
                foreach (v_ReportGroup_lstRptExplorer_Restricted_Binding item in groups)
                {
                    if (item.ParentGroup_Id == _group_Id)
                    {
                        _children.Add( new ReportExplorerNode(ReportsExplorerNodeType.Group, item.v_RptGrp_Id, null, null, item.v_RptGrp_Name, null, false, false));

                    }
                }


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildGroups", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildGroups", IfxTraceCategory.Leave);
            }
        }
        
        void LoadChildReports()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildReports", IfxTraceCategory.Enter);

                ObservableCollection<v_Report_lstRptExplorer_Restricted_Binding> reports = CommonClientData_Bll_staticLists.v_Report_lstRptExplorer_Restricted_BindingListProperty;
                if (reports == null) { return; }
                foreach (v_Report_lstRptExplorer_Restricted_Binding item in reports)
                {
                    if (item.RptGrp_Id == _group_Id)
                    {
                        _children.Add(new ReportExplorerNode(ReportsExplorerNodeType.Report, item.RptGrp_Id, item.v_Rpt_Id, item.v_FSM_Id, item.v_Rpt_Name, item.v_Rpt_Desc, item.v_Rpt_TargetSSRS, item.v_Rpt_TargetExcel));

                    }
                }


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildReports", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildReports", IfxTraceCategory.Leave);
            }
        }
        

        #region  Properties

        public Guid? FilterSession_Id
        {
            get { return _filterSession_Id; }
            set { _filterSession_Id = value; }
        }

        public Guid? Report_Id
        {
            get { return _report_Id; }
            set { _report_Id = value; }
        }

        public Guid? Group_Id
        {
            get { return _group_Id; }
            set { _group_Id = value; }
        }

        public Guid? ParentReport_Id
        {
            get { return _parentReport_Id; }
            set { _parentReport_Id = value; }
        }

        public int SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }
        }

        public string NodeName
        {
            get { return _nodeName; }
            set { _nodeName = value; }
        }

        public string SysName
        {
            get { return _sysName; }
            set { _sysName = value; }
        }

        public string SprocName
        {
            get { return _sprocName; }
            set { _sprocName = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public Visibility SSRS_ButtonVisibility
        {
            get
            {
                if (_isSSRS)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }

        public Visibility Excel_ButtonVisibility
        {
            get
            {
                if (_isExcel)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }

        public Visibility NodeImageVisibility
        {
            get
            {
                if (_nodeType == ReportsExplorerNodeType.Group)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }


        public ObservableCollection<ReportExplorerNode> Children
        {
            get { return _children; }
            set { _children = value; }
        }

        public string ImageUrl
        {
            get
            {
                return ReporImageHelper.GetImagePath(_nodeType);
            }
        }

        #endregion  Properties






    }
}
