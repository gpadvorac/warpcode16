﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Ifx.SL;
using System.Windows.Browser;
using ProxyWrapper;
using vReportParameter;
using ApplicationTypeServices;
using vFilterMetaData;
using System.Collections.Generic;
using System.Diagnostics;
using vFilterDataManager;
using Velocity.SL;
using vFilterControls;
using vUICommon;
using TypeServices;
using vXamGridExcel;
using SLcompression;

namespace Reports
{
    public class ReportProvider
    {

        // https://www.diffchecker.com/diff



        #region Initialize Variables

        private static string _as = "Reports";
        private static string _cn = "ReportProvider";

        vReportService_ProxyWrapper _proxy = null;
        ucFilterManager _filterManager = null;
        public event System.EventHandler<Getv_Report_ReportDataForGridOrExcelCompletedEventArgs> Getv_Report_ReportDataForGridOrExcelCompleted;

        public event ReturnParamsFromFilterSessionEventHandler ReturnParameters;


        #endregion Initialize Variables


        #region Constructors

        /// <summary>
        /// Use this constructor when the filter manager is not available and will not be used.
        /// Just a 
        /// </summary>
        public ReportProvider()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReportProvider", IfxTraceCategory.Enter);
                _proxy = new vReportService_ProxyWrapper();
                _proxy.RunReportCompleted += new EventHandler<RunReportCompletedEventArgs>(RunReportCompleted);
                _proxy.Getv_Report_ReportDataForGridOrExcelCompleted += new EventHandler<Getv_Report_ReportDataForGridOrExcelCompletedEventArgs>(proxy_Getv_Report_ReportDataForGridOrExcelCompleted);
                _proxy.Begin_Getv_Report_Server();
                //_filterManager = filterManager;
                //_filterManager.ReturnFilterData += new ReturnFilterDataEventHandler(FilterManager_ReturnFilterData);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReportProvider", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReportProvider", IfxTraceCategory.Leave);
            }
        }


        public ReportProvider(ucFilterManager filterManager)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReportProvider", IfxTraceCategory.Enter);
                _proxy = new vReportService_ProxyWrapper();
                _proxy.RunReportCompleted += new EventHandler<RunReportCompletedEventArgs>(RunReportCompleted);
                _proxy.Getv_Report_ReportDataForGridOrExcelCompleted += new EventHandler<Getv_Report_ReportDataForGridOrExcelCompletedEventArgs>(proxy_Getv_Report_ReportDataForGridOrExcelCompleted);
                _proxy.Begin_Getv_Report_Server();

                _filterManager = filterManager;
                _filterManager.ReturnFilterData += new ReturnFilterDataEventHandler(FilterManager_ReturnFilterData);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReportProvider", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReportProvider", IfxTraceCategory.Leave);
            }
        }


        #endregion Constructors


        #region Begin Report

        /// <summary>
        /// Initialize the filter screen for a specific report or query.
        /// </summary>
        /// <param name="filterSession_Id">Filter session Id which all the filters are configured for.</param>
        /// <param name="reportType">SSRS Report, Excel Report or for a Grid.</param>
        public void StartReport(Guid filterSession_Id, vQueryTargetType reportType)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "StartReport", IfxTraceCategory.Enter);
                /*
                 *      STEPS TO RUN A REPORT
                 *      1.  Get the filter session data
                 *      
                 *      3.  See if filters are needed
                 *          3.A.  If no filters are needed
                 *              Collect context parameters if there are any
                 *              Call Report (Excel or SSRS
                 *          3.B If filters are needed
                 *              Load Filters
                 *              Filter manager raises an event when filters are done and passes thier values back here
                 *              catch the event and combine context parameters with filter parameters
                 *              Call Report (Excel or SSRS
                 */


                List<vParameter> contextParms = null;
                if (reportType == vQueryTargetType.Excel)
                {
                    contextParms = CollectContextParameters(filterSession_Id);
                }

                vFilterSessionMD session = FilterMetadataCache.GetSession(filterSession_Id);
                //
                session.TargetReportType = reportType;
                if (session.SessionFilterControls.Count > 0)
                {
                    //  This report uses filters, to open the Filter Manager

                    _filterManager.SetFilters(session, contextParms);


                    //  **  This code is duplicated in vFilterAdmin.ucv_FilterSessionMD.LoadFilters
                    //         !!!!!!!!!!     it needs to be replaced with 1 method that can be shared by both and is not part of the main framework.
                    // * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * ! * !

                    //ucv_FilterSessionMD  might be able to use this class.

                    object[] parms = null;
                    // Check to see if we need to load up some parameters to pass into the control which will be used to fetch a list of data
                    foreach (var filter in _filterManager.Filters)
                    {
                        foreach (var field in filter.InputFields)
                        {
                            if (filter.HasFetchParams)
                            {
                                // if this filter has fetch parameters, loop through them and get thier values
                                if (field.Value.FetchParameters.Count > 0)
                                {
                                    parms = new object[field.Value.FetchParameters.Count];

                                    // Now loop thru the list and load up the param values.
                                    for (int i = 0; i < field.Value.FetchParameters.Count; i++)
                                    {
                                        switch ((vFilterFetchParameterSource)field.Value.FetchParameters[i].v_FCFPm_Source)
                                        {
                                            case vFilterFetchParameterSource.Project:
                                                // call a method to get the project Id
                                                if (ContextValues.CurrentProjectId == null)
                                                {
                                                    MessageBox.Show("Please select a project first.");
                                                    return;
                                                }
                                                parms[i] = ContextValues.CurrentProjectId;
                                                break;
                                            case vFilterFetchParameterSource.User:
                                                parms[i] = Credentials.UserId;
                                                break;
                                            case vFilterFetchParameterSource.SomethingElse:
                                                // call a method to get the project Id
                                                //parms[i] = "Some Value";
                                                break;
                                        }
                                    }
                                    // Pass the params back to the filter and it will fetch it's list from a web service
                                    filter.RecieveFieldFetchParameters((int)field.Value.v_FCIFM_Sort, parms);
                                }
                            }
                            else
                            {
                                //  No fetch params.  Just fetch the list.
                                filter.RecieveFieldFetchParameters((int)field.Value.v_FCIFM_Sort, null);
                            }
                        }
                    }
                }
                else
                {
                    //  This report doesnt use params from filters, so run the report now
                    RunReport(filterSession_Id, null);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "StartReport", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "StartReport", IfxTraceCategory.Leave);
            }
        }

        void FilterManager_ReturnFilterData(object sender, ReturnFilterDataArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FilterManager_ReturnFilterData", IfxTraceCategory.Enter);
                RunReport(e.FilterSessionId, e.ReturnData);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FilterManager_ReturnFilterData", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FilterManager_ReturnFilterData", IfxTraceCategory.Leave);
            }
        }

        public void RunReport(Guid filterSession_Id, List<vFilterReturnParameter> filterParms)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", IfxTraceCategory.Enter);

                vFilterSessionMD session = FilterMetadataCache.GetSession(filterSession_Id);
                vReport rpt = null;
                // Param collection to pass into report.
                List<vParameter> parms = new List<vParameter>();
                // See if we need to collect context parameters
                if (session.SessionContextParameters.Count > 0)
                {
                    // Collect the context parameters
                    parms = CollectContextParameters(filterSession_Id);
                    if (parms == null)
                    {
                        //Debugger.Break();
                        throw new Exception("Reports.ucReportExplorer.RunReport: parms was null after alling CollectContextParameters(" + filterSession_Id.ToString() + "); ");
                    }

                }
                // add the fiter params to the report's param collection
                if (filterParms != null)
                {
                    foreach (var item in filterParms)
                    {
                        if (item.ParameterValue == null)
                        {
                            parms.Add(new vParameter(item.ParameterName, null, item.SimpleType));
                        }
                        else
                        {
                            parms.Add(new vParameter(item.ParameterName, item.ParameterValue.ToString(), item.SimpleType));
                        }
                    }
                }
                //  This is where the thread channels toward the apropreate tartet report type.
                rpt = new vReport((Guid)session.v_FSM_TargetId, session.v_FSM_SysName, session.v_FSM_ReportName, parms);
                switch (session.TargetReportType)
                {
                    case vQueryTargetType.SSRS:
                        _proxy.Begin_RunReport(rpt.GetValuesAsObjectArray());
                        break;
                    case vQueryTargetType.Excel:
                        // This should NEVER be hit becuase running an excel report gets initiated from a button click event in the filter manager.
                        RunExcelReport(rpt);
                        break;
                    case vQueryTargetType.Grid:  //***
                        Raise_ReturnFilterData(rpt);
                        break;
                    case vQueryTargetType.None:
                        //_proxy.Begin_Getv_Report_ReportDataForGridOrExcel(rpt.GetValuesAsObjectArray());
                        break;
                    default:
                        throw new Exception("Reports.ReportProvider.RunReport:  Missing Report Target Type.");
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", IfxTraceCategory.Leave);
            }
        }


        public void RunReport_SingleParameter(Guid filterSession_Id, string paramName, string paramValue, SimpleDataType simpleType, Guid targetId, string sysName, string reportName, vQueryTargetType targeType)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", IfxTraceCategory.Enter);

                vReport rpt = null;
                // Param collection to pass into report.
                List<vParameter> parms = new List<vParameter>();
                parms.Add(new vParameter(paramName, paramValue, simpleType));
                rpt = new vReport(targetId, sysName, reportName, parms);
                switch (targeType)
                {
                    case vQueryTargetType.SSRS:
                        _proxy.Begin_RunReport(rpt.GetValuesAsObjectArray());
                        break;
                    case vQueryTargetType.Excel:
                        // This should NEVER be hit becuase running an excel report gets initiated from a button click event in the filter manager.
                        RunExcelReport(rpt);
                        break;
                    case vQueryTargetType.None:  //  How about GRID?
                        //_proxy.Begin_Getv_Report_ReportDataForGridOrExcel(rpt.GetValuesAsObjectArray());
                        break;
                    default:
                        throw new Exception("Reports.ReportProvider.RunReport:  Missing Report Target Type.");
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", IfxTraceCategory.Leave);
            }
        }



        #endregion Begin Report


        #region Excel Report



        SaveFileDialog _saveDialog = null;
        OpenFileDialog _openDialog = null;

        void RunExcelReport(vReport rpt)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunExcelReport", IfxTraceCategory.Enter);

                _saveDialog = new SaveFileDialog { Filter = "Excel files|*.xlsx", DefaultExt = "xlsx" };

                _saveDialog.DefaultFileName = rpt.ReportCaptionName;
                _openDialog = new OpenFileDialog();

                bool? showDialog = _saveDialog.ShowDialog();
                if (showDialog == true)
                {
                    _proxy.Begin_Getv_Report_ReportDataForGridOrExcel(rpt.GetValuesAsObjectArray());
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunExcelReport", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunExcelReport", IfxTraceCategory.Leave);
            }
        }



        void proxy_Getv_Report_ReportDataForGridOrExcelCompleted(object sender, Getv_Report_ReportDataForGridOrExcelCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_ReportDataForGridOrExcelCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Getv_Report_ReportDataForGridOrExcelCompletedEventArgs> handler = Getv_Report_ReportDataForGridOrExcelCompleted;
                byte[] array = e.Result;
                //object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];

                object[] data = PayloadHelper.CompressedBytesToObject(array) as object[];

                ExcelExportHelper.ExportArrayToExcel(_saveDialog, _openDialog, data);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_ReportDataForGridOrExcelCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_ReportDataForGridOrExcelCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Excel Report


        #region SSRS Report

        /// <summary>
        /// This is the return call from the server after setting up the SSRS report.  
        /// Now we need to open a browser passing this key back to display the report.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void RunReportCompleted(object sender, RunReportCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReportCompleted", IfxTraceCategory.Enter);
                Guid? key = e.Result;
                if (key == null)
                {
                    MessageBox.Show("The report could not be returned from the server.  If this problem continues, please contact support.", "Report Not Returned", MessageBoxButton.OK);
                    return;
                }
                CallSsrsReport((Guid)key);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReportCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReportCompleted", IfxTraceCategory.Leave);
            }
        }

        void CallSsrsReport(Guid key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CallSsrsReport", IfxTraceCategory.Enter);

                //http://localhost/VelocityServer_v01/vReports/VelocityReportService.aspx
                //HtmlPage.Window.Navigate(new Uri("myPage.aspx", UriKind.Relative), "_blank");
                // http://localhost/VelocityServer_v01/vReports/VelocityReportService.aspx?Param=1ad671f8-2a05-4730-b611-bad1576b9788

                if (ApplicationLevelVariables.ReportServerURL == null || ApplicationLevelVariables.ReportServerURL.Length == 0)
                {
                    throw new Exception("Reports.ReportProvider.CallSsrsReport:  ApplicationLevelVariables.ReportServerURL = null.");
                }
                HtmlPage.Window.Navigate(new Uri(ApplicationLevelVariables.ReportServerURL + "/vReports/VelocityReportService.aspx?Param=" + key.ToString(), UriKind.Absolute), "_blank");

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CallSsrsReport", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CallSsrsReport", IfxTraceCategory.Leave);
            }
        }

        #endregion SSRS Report

        #region Filter For Grid

        void Raise_ReturnFilterData(vReport rpt)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReturnParameters", IfxTraceCategory.Enter);

                ReturnParamsFromFilterSessionEventHandler handler = ReturnParameters;

                ReturnParamsFromFilterSessionArgs args = new ReturnParamsFromFilterSessionArgs(rpt);
                if (handler != null)
                {
                    handler(this, args);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReturnParameters", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReturnParameters", IfxTraceCategory.Leave);
            }
        }



        #endregion Filter For Grid


        private List<vParameter> CollectContextParameters(Guid filterSession_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CollectContextParameters", IfxTraceCategory.Enter);

                vFilterSessionMD session = FilterMetadataCache.GetSession(filterSession_Id);
                List<vParameter> parms = new List<vParameter>();
                foreach (var item in session.SessionContextParameters)
                {
                    switch ((vFilterFetchParameterSource)item.Value.v_FSCP_SourceId)
                    {
                        case vFilterFetchParameterSource.Project:
                            parms.Add(new vParameter(item.Value.v_FSCP_Name, ContextValues.CurrentProjectId.ToString(), SimpleDataType.Guid));
                            break;
                        case vFilterFetchParameterSource.User:
                            parms.Add(new vParameter(item.Value.v_FSCP_Name, Credentials.UserId.ToString(), SimpleDataType.Guid));
                            break;
                        default:

                            break;
                    }
                }
                return parms;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CollectContextParameters", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CollectContextParameters", IfxTraceCategory.Leave);
            }
        }

    }


    /// <summary>
    /// Used to pass back to a grid which will then call a WS
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    #region ReturnParamsFromFilterSession

    public delegate void ReturnParamsFromFilterSessionEventHandler(object sender, ReturnParamsFromFilterSessionArgs e);

    public class ReturnParamsFromFilterSessionArgs : EventArgs
    {
        private readonly vReport _rpt;

        public ReturnParamsFromFilterSessionArgs(vReport rpt)
        {
            _rpt = rpt;
        }

        public vReport Rpt
        {
            get { return _rpt; }
        }

    }

    #endregion ReturnParamsFromFilterSession

}
