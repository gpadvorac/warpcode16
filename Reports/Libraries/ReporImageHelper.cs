﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Reports
{
    public class ReporImageHelper
    {
        static string _group = "ReportGroup16.png";
        static string _report = "Report16.png";
        static string _imageURI = "/Reports;component/Images/";



        //  "/Reports;component/images/report2-24.png" /

        public static string GetImagePath(ReportsExplorerNodeType type)
        {
            string img = "";
            switch (type)
            {
                case ReportsExplorerNodeType.Group:
                    img = _imageURI + _group;
                    break;
                case ReportsExplorerNodeType.Report:
                    img = _imageURI + _report;
                    break;
            }
            return img;
        }



    }
}
