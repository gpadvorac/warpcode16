﻿using ApplicationTypeServices;
using EntityWireTypeSL;
using Ifx.SL;
using Infragistics.Controls.Editors;
using Infragistics.Controls.Grids;
using ProxyWrapper;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using TypeServices;
using vUICommon;

namespace vGridColumnGroupManager
{



    #region ColumnGroupSettingsChanged


    public delegate void ColumnGroupSettingsChangedEventHandler(object sender, ColumnGroupSettingsChangedArgs e);

    public class ColumnGroupSettingsChangedArgs : EventArgs
    {
       
        public ColumnGroupSettingsChangedArgs()
        {
           
        }


    }


    #endregion ColumnGroupSettingsChanged




    public class ColumnGroupManager
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "vGridColumnGroupManager";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "GroupManager";

        public event ColumnGroupSettingsChangedEventHandler ColumnGroupSettingsChanged;
        vGridColumnGroupService_ProxyWrapper _proxy = null;

        public enum CurrentListType
        {
            System = 1,
            UserDefined = 2,
            Both = 3
        }

        CurrentListType _listType = CurrentListType.System;

        List<v_GridColumnGroup_cmbByType_Binding> _cmbList = new List<v_GridColumnGroup_cmbByType_Binding>();

        XamComboEditor _cmbColumnGroups = null;

        XamGrid _currentXamGrid = null;

        Guid? _ownerId = null;

        Guid? _gridId = null;

        bool _setDefaultColumnGroup = true;

        public event ColumnGroupsLoadedEventHandler ColumnGroupsLoaded;



        #endregion Initialize Variables




        public ColumnGroupManager()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GroupManager Constructor", IfxTraceCategory.Enter);

                _proxy = new vGridColumnGroupService_ProxyWrapper();
                _proxy.Getv_GridColumnGroup_cmbByTypeCompleted += Getv_GridColumnGroup_cmbByTypeCompleted;
                _proxy.GetGridColumnGroupsAndColumnsCompleted += GetGridColumnGroupsAndColumnsCompleted;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GroupManager Constructor", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GroupManager Constructor", IfxTraceCategory.Leave);
            }
        }


        
        public void Initialize_cmbColumnGroups(XamGrid grid, XamComboEditor cbo, Guid gridId, Guid? ownerId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Initialize_cmbColumnGroups", IfxTraceCategory.Enter);

                _currentXamGrid = grid;
                _cmbColumnGroups = cbo;
                _gridId = gridId;
                _ownerId = ownerId;
                _cmbColumnGroups.DisplayMemberPath = "v_GdColGrp_Name";
                _cmbColumnGroups.SelectedIndex = 0;



                _cmbColumnGroups.SelectionChanged += cmbColumnGroups_SelectionChanged;

                if (GridColumnGroupHelper.GetGridColumnGroupTypePreference((Guid)_gridId) == 1)
                {
                    LoadComboGroupsAndColumnLists(CurrentListType.System);
                }
                else if (GridColumnGroupHelper.GetGridColumnGroupTypePreference((Guid)_gridId) == 2)
                {
                    LoadComboGroupsAndColumnLists(CurrentListType.UserDefined);
                }
                else
                {
                    LoadComboGroupsAndColumnLists(CurrentListType.Both);
                }


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Initialize_cmbColumnGroups", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Initialize_cmbColumnGroups", IfxTraceCategory.Leave);
            }
        }
        
        public void LoadComboGroupsAndColumnLists(CurrentListType type)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadComboGroupsAndColumnLists", IfxTraceCategory.Enter);

                if (type == CurrentListType.System)
                {
                    //_proxy.Begin_Getv_GridColumnGroup_cmbByType((Guid)_gridId, null);
                    _proxy.Begin_GetGridColumnGroupsAndColumns((Guid)_gridId, null, 1);
                }
                else if (type == CurrentListType.UserDefined)
                {
                    //_proxy.Begin_Getv_GridColumnGroup_cmbByType((Guid)_gridId, _ownerId);
                    _proxy.Begin_GetGridColumnGroupsAndColumns((Guid)_gridId, _ownerId, 2);
                }
                else 
                {
                    //_proxy.Begin_Getv_GridColumnGroup_cmbByType((Guid)_gridId, _ownerId);
                    _proxy.Begin_GetGridColumnGroupsAndColumns((Guid)_gridId, _ownerId, 3);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadComboGroupsAndColumnLists", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadComboGroupsAndColumnLists", IfxTraceCategory.Leave);
            }
        }

        //***  Add to WC
        public void SelectColumnGroup(string groupName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SelectColumnGroup", IfxTraceCategory.Enter);

                foreach (var item in _cmbColumnGroups.Items)
                {
                    if (((v_GridColumnGroup_cmbByType_Binding)item.Data).v_GdColGrp_SysName == groupName)
                    {
                        // This item was just checked becuase it's checked in the list, but its visibility isnt set to true yet
                        item.IsSelected = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SelectColumnGroup", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SelectColumnGroup", IfxTraceCategory.Leave);
            }
        }

        public void SelectColumnGroupById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SelectColumnGroupById", IfxTraceCategory.Enter);

                foreach (var item in _cmbColumnGroups.Items)
                {
                    if (((v_GridColumnGroup_cmbByType_Binding)item.Data).v_GdColGrp_Id == id)
                    {
                        // This item was just checked becuase it's checked in the list, but its visibility isnt set to true yet
                        item.IsSelected = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SelectColumnGroupById", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SelectColumnGroupById", IfxTraceCategory.Leave);
            }
        }

        public void UnSelectColumnGroupById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnSelectColumnGroupById", IfxTraceCategory.Enter);

                foreach (var item in _cmbColumnGroups.Items)
                {
                    if (((v_GridColumnGroup_cmbByType_Binding)item.Data).v_GdColGrp_Id == id)
                    {
                        // This item was just checked becuase it's checked in the list, but its visibility isnt set to true yet
                        item.IsSelected = false;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnSelectColumnGroupById", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnSelectColumnGroupById", IfxTraceCategory.Leave);
            }
        }


        void cmbColumnGroups_SelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "cbo_SelectionChanged", IfxTraceCategory.Enter);

                ShowHideColumns();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "cbo_SelectionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "cbo_SelectionChanged", IfxTraceCategory.Leave);
            }
        }



        public void SetDefaultVisibility()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultVisibility", IfxTraceCategory.Enter);


                foreach (var item in _currentXamGrid.Columns)
                {
                    if (item is Column)
                    {
                        item.Visibility = Visibility.Collapsed;
                    }
                }


                foreach (var item in _cmbColumnGroups.Items)
                {
                    if (((v_GridColumnGroup_cmbByType_Binding)item.Data).v_GdColGrp_IsDefault == true)
                    {
                        item.IsSelected = true;
                        ((v_GridColumnGroup_cmbByType_Binding)item.Data).SetColumnVisibility(true);
                    }
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultVisibility", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultVisibility", IfxTraceCategory.Leave);
            }
        }




        public void ShowHideColumns()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ShowHideColumns", IfxTraceCategory.Enter);
                
                foreach (var item in _cmbColumnGroups.Items)
                {
                    if (item.IsSelected == false)
                    {
                        ((v_GridColumnGroup_cmbByType_Binding)item.Data).SetColumnVisibility(false);
                    }
                }

                foreach (var item in _cmbColumnGroups.Items)
                {
                    if (item.IsSelected == true)
                    {
                        ((v_GridColumnGroup_cmbByType_Binding)item.Data).SetColumnVisibility(true);
                    }
                }


                ColumnGroupSettingsChangedEventHandler handler = ColumnGroupSettingsChanged;
                if (handler != null)
                {
                    handler(this, new ColumnGroupSettingsChangedArgs());
                }

                //xxx

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ShowHideColumns", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ShowHideColumns", IfxTraceCategory.Leave);
            }
        }


        void OnColumnGroupsLoaded()
        {
            ColumnGroupsLoadedEventHandler handler = ColumnGroupsLoaded;
            if (handler != null)
            {
                handler(this, new ColumnGroupsLoadedArgs());
            }
        }


        #region Properties



        public CurrentListType ListType
        {
            get { return _listType; }
            set { _listType = value; }
        }


        public List<v_GridColumnGroup_cmbByType_Binding> CmbList
        {
            get { return _cmbList; }
            set { _cmbList = value; }
        }


        public Guid? OwnerId
        {
            get { return _ownerId; }
            set { _ownerId = value; }
        }


        public XamComboEditor CmbColumnGroups
        {
            get { return _cmbColumnGroups; }
            set { _cmbColumnGroups = value; }
        }


        public XamGrid CurrentXamGrid
        {
            get { return _currentXamGrid; }
            set { _currentXamGrid = value; }
        }


        public Guid? GridId
        {
            get { return _gridId; }
            set { _gridId = value; }
        }


        #endregion Properties
        

        #region Fetch



        void GetGridColumnGroupsAndColumnsCompleted(object sender, GetGridColumnGroupsAndColumnsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetGridColumnGroupsAndColumnsCompleted", IfxTraceCategory.Enter);

                object[] arGroups = null;
                object[] arColumns = null;

                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                _cmbList.Clear();
                _cmbColumnGroups.SelectedItems.Clear();

                if (array != null && array.Length > 0)
                {
                    arGroups = array[0] as object[];
                    arColumns = array[1] as object[];
                }



                if (arGroups != null && arGroups.Length > 0)
                {
                    for (int i = 0; i < arGroups.Length; i++)
                    {
                        _cmbList.Add(new v_GridColumnGroup_cmbByType_Binding((object[])arGroups[i], arColumns, _currentXamGrid));

                    }
                }



                _cmbColumnGroups.ItemsSource = null;
                _cmbColumnGroups.ItemsSource = _cmbList;


                //if (_setDefaultColumnGroup == true)
                //{
                SetDefaultVisibility();
                //}

                // Notify the calling UI that the column groups have loaded.
                OnColumnGroupsLoaded();
                //xxx
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetGridColumnGroupsAndColumnsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetGridColumnGroupsAndColumnsCompleted", IfxTraceCategory.Leave);
                _setDefaultColumnGroup = false;
            }
        }

        void Getv_GridColumnGroup_cmbByTypeCompleted(object sender, Getv_GridColumnGroup_cmbByTypeCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroup_cmbByTypeCompleted", IfxTraceCategory.Enter);

                //byte[] data = e.Result;
                //object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                //_cmbList.Clear();

                //if (array != null && array.Length > 0)
                //{
                //    for (int i = 0; i < array.Length; i++)
                //    {
                //        _cmbList.Add(new v_GridColumnGroup_cmbByType_Binding((object[])array[i]));
                //    }
                //}
                //_cmbColumnGroups.ItemsSource = null;
                //_cmbColumnGroups.ItemsSource = _cmbList;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroup_cmbByTypeCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroup_cmbByTypeCompleted", IfxTraceCategory.Leave);
            }
        }




        #endregion Fetch




    }
}
