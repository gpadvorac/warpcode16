﻿using ApplicationTypeServices;
using Ifx.SL;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace vGridColumnGroupManager
{
    public class GridColumnGroupHelper
    {


        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "vGridColumnGroupManager";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "GridColumnGroupHelper";

        public static int GetGridColumnGroupTypePreference(Guid gridId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadComboGroupsAndColumnLists", IfxTraceCategory.Enter);

                foreach (var item in ApplicationLevelVariables.GridColumnGroupTypePreferenceList)
                {
                    if (item.GridId == gridId)
                    {
                        return item.GroupType;
                    }
                }
                return 1;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadComboGroupsAndColumnLists", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadComboGroupsAndColumnLists", IfxTraceCategory.Leave);
            }
        }






    }
}
