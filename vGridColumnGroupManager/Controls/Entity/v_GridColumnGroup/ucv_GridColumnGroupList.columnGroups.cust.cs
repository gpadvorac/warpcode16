using System;
using System.Windows;
using ApplicationTypeServices;
using Ifx.SL;

// Gen Timestamp:  11/22/2014 2:04:07 PM

namespace vGridColumnGroupManager
{
    /// <summary>
    ///  Column Groups Custom Code.  Add additional Groups and Columns here.
    /// </summary>
    public partial class ucv_GridColumnGroupList
    {


        public partial class ColumnGroupNames
        {
            //public const string ColumnGroupVariableName = "ColumnGroupVariableName";
            //public const string ColumnGroupVariable_Header = "Column Group Label";

        }

        void Configure_cmbGrouping_Cust()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Configure_cmbGrouping_Cust", IfxTraceCategory.Enter);
                ColumnGroup grp;

                //// ValueAttribute
                //grp = new ColumnGroup(ColumnGroupNames.ColumnGroupVariableName, ColumnGroupNames.ColumnGroupVariable_Header, true);
                //grp.IsVisible = false;
                //grp.IsDefault = false;
                //_columnGroups.Add(grp);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Configure_cmbGrouping_Cust", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Configure_cmbGrouping_Cust", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// For this to work, there is a global static list of column names for each group i.e.  'ContextValues.ValueAttributeNames'.
        /// When this list is modified (see IdeaBase: ucIdea.LoadAttributeMatrixColumns for an example), this method is called
        /// to refresh the new column references in the group.
        /// </summary>
        public void RefreshMatrixGroups()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshMatrixGroups", IfxTraceCategory.Enter);
                foreach (var grp in _columnGroups)
                {
                    //if (grp.GroupName == ColumnGroupNames.ValueAttribute)
                    //{
                    //    grp.Fields.Clear();
                    //    foreach (var item in ContextValues.ValueAttributeNames)
                    //    {
                    //        if (grp.IsVisible == true)
                    //        {
                    //            navList.Columns.DataColumns[item].Visibility = Visibility.Visible;
                    //        }
                    //        else
                    //        {
                    //            navList.Columns.DataColumns[item].Visibility = Visibility.Collapsed;
                    //        }
                    //        grp.Fields.Add(navList.Columns.DataColumns[item]);

                    //    }
                    //    break;
                    //}
                }

                foreach (var grp in _columnGroups)
                {
                    //if (grp.GroupName == ColumnGroupNames.Scenario)
                    //{
                    //    grp.Fields.Clear();
                    //    foreach (var item in ContextValues.ScenarioNames)
                    //    {
                    //        if (grp.IsVisible == true)
                    //        {
                    //            navList.Columns.DataColumns[item].Visibility = Visibility.Visible;
                    //        }
                    //        else
                    //        {
                    //            navList.Columns.DataColumns[item].Visibility = Visibility.Collapsed;
                    //        }
                    //        grp.Fields.Add(navList.Columns.DataColumns[item]);
                    //    }
                    //    break;
                    //}
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshMatrixGroups", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshMatrixGroups", IfxTraceCategory.Leave);
            }
        }





    }
}
