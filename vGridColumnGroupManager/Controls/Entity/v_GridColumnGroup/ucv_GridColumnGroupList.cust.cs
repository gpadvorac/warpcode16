using System;
using Ifx.SL;
using EntityBll.SL;
using TypeServices;
using Infragistics.Controls.Grids;
using vUICommon;
using Velocity.SL;
using vControls;
using Infragistics.Controls.Editors;
using System.Windows.Controls;

// Gen Timestamp:  11/22/2014 2:04:07 PM

namespace vGridColumnGroupManager
{
    public partial class ucv_GridColumnGroupList
    {

        #region Initialize Variables


        bool _mnuClearFilters_AllowVisibility = false;
        bool _mnuGridTools_AllowVisibility = false;
        bool _mnuRichGrid_AllowVisibility = false;
        bool _mnuSplitScreen_AllowVisibility = false;
        bool _isAllowNewRow = true;
        bool _mnuExcelExport_AllowVisibility = false;
        bool _gridDataSourceCombo_AllowVisibility = false;
        bool _menuGridRow_AllowVisibility = false;  // the area where the grid menu is located


        ProxyWrapper.vGridColumnGroupService_ProxyWrapper _Proxy = null;

        int _groupTypeId = 1;
        Guid? _gridGroupOwnerId;
        Guid _gridId;



        #endregion Initialize Variables


        #region Constructor

        void CustomConstructionCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", IfxTraceCategory.Enter);

                _Proxy = new ProxyWrapper.vGridColumnGroupService_ProxyWrapper();
                _Proxy.v_GridColumnGroup_GetListByFKCompleted += new EventHandler<v_GridColumnGroup_GetListByFKCompletedEventArgs>(v_GridColumnGroup_GetListByFKCompleted);
                _Proxy.v_GridColumnGroup_GetAllCompleted += new EventHandler<v_GridColumnGroup_GetAllCompletedEventArgs>(v_GridColumnGroup_GetAllCompleted);

                navList.CellEnteringEditMode += navList_CellEnteringEditMode;


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", IfxTraceCategory.Leave);
            }
        }

  

        #endregion Constructor


        #region Security

        UserSecurityContext _userContext = new UserSecurityContext();
        //***  Add the ACTUAL Guid below, and then delete this comment
        private Guid _ancestorSecurityId = Guid.NewGuid();
        ControlCache cCache;
        EntityCache eCache = null;
        bool defaultEntityOperationPermission = true;
        bool _secuitySettingIsReadOnly = false;
        //***  Add the ACTUAL Guid below, and then delete this comment
        Guid _controlId = Guid.NewGuid();
        bool _isDeleteActionAllowed = false;
        Guid _deleteActionId = Guid.NewGuid();  // Hard code the actual guid from the security tool


        public void SetSecurityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Enter);

                _userContext.LoadArtifactPermissions(_ancestorSecurityId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Leave);
            }
        }

        void UserSecurityContext_SecurityArtifactsRetrieved(object sender, SecurityArtifactsRetrievedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Enter);
                SecurityCache.AddControlCacheForAncestor(e.ArtifactAncestorData);
                cCache = SecurityCache.GetControlGroupById(e.ArtifactAncestorData.Id);

                UiControlStateSetting setting = cCache.GetControlById(_controlId).Setting;
                if (setting == UiControlStateSetting.Enable)
                {
                    _secuitySettingIsReadOnly = false;
                }
                else
                {
                    _secuitySettingIsReadOnly = true;
                    navList_SplitScreenMode();
                }

                //_isDeleteActionAllowed = SecurityCache.IsEnabled(_deleteActionId);
                //if (_isDeleteActionAllowed)
                //{
                //    navList.DeleteKeyAction = DeleteKeyAction.DeleteSelectedRows;
                //}

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Leave);
            }
        }

        #endregion Security

        #region Method Extentions For Custom Code


        public void Set_vXamComboColumn_ItemSourcesWithParams()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", IfxTraceCategory.Enter);
                
                //_leaseProxy.Begin_GetLeaseAltNumberSource_ComboItemList((Guid)_prj_Id);

//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", IfxTraceCategory.Leave);
//            }
        }

        #region Combo ItemsSource for Non-Static Lists




        #endregion region Combo ItemsSource for Non-Static Lists



        void vXamComboColumn_SelectionChanged_CustomCode(object sender, vXamComboColumnSelectionChangedEventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void vXamMultiColumnComboColumn_SelectionChanged_CustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void vDatePickerColumn_TextChanged_CustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void XamColorPicker_SelectedColorChanged_CustomCode(XamColorPicker ctl, SelectedColorChangedEventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void vCheckColumn_CheckedChanged_CustomCode(vCheckBox ctl, IBusinessObject obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void vTextColumn_TextChanged_CustomCode(TextBox ctl, v_GridColumnGroup_Bll obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void vDecimalColumn_TextChanged_CustomCode(TextBox ctl, v_GridColumnGroup_Bll obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void navList_RowEnteredEditMode_Custom(object sender, EditingRowEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", IfxTraceCategory.Enter);
                v_GridColumnGroup_Bll obj = e.Row.Data as v_GridColumnGroup_Bll;
                if (obj == null) { return; }

                        // For multi parets
////                if (_guidParentId == null)
////                {
////                    throw new Exception(_as + "." + _cn + ".navList_RowEnteredEditMode:  _guidParentId  was null.  _guidParentId must be used for the FK.");
////                }
//                //obj.StandingFK = (Guid)_prj_Id;    // added this line
//
//                //switch (_parentType)
//                //{
//                //    case "ucWell":
//                //        // this might not ever get his.
//                //        break;
//                //    case "ucWellDt":
//                //        obj.Current.WlD_Id_noevents = _guidParentId;
//                //        break;
//                //    case "ucContractDt":
//                //        obj.Current.CtD_Id_noevents = _guidParentId;
//                //        break;
//                //}

                //  Or For a Fixed Single Parent
                _isRowInEditMode = true;
                if (obj.State.IsNew() == true)
                {
                    //***  this was already called in the main method.  i dont think we need it again.  see if we can take this out of WC
                    //SetNewRowValidation();
                    obj.v_GdColGrp_GdColGrpTp_Id = _groupTypeId;
                    obj.Current.v_GdColGrp_GdColGrpTp_Id_noevents = _groupTypeId;
                    obj.Current.v_GdColGrp_Gd_Id_noevents = _gridId;
                    if (_groupTypeId == 2)
                    {
                        obj.Current.v_GdColGrp_OwnerId_noevents = _gridGroupOwnerId;
                    }
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", IfxTraceCategory.Leave);
            }
        }


        void navList_SelectedRowsCollectionChanged_Custom(object sender, SelectionCollectionChangedEventArgs<SelectedRowsCollection> e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", IfxTraceCategory.Leave);
//            }
        }

        #endregion Method Extentions For Custom Code


        #region CodeGen Methods for modification



        /// <overloads>
        ///     This is a standard method in most entity related controls where state and related
        ///     Id values are passed in from the parent control. Often an <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/The_Entity_Manager.html">
        ///     Entity Manager</a> control is a child to another entity and needs the parent Id to
        ///     filter its navigation list. Sometimes the current Id value is also known and used
        ///     to select the corresponding row in ucEntityList. Selecting this row starts a series
        ///     of events which end up populating <see cref="ucv_GridColumnGroupProps">ucProps</see> with the
        ///     correct data. Since the parent control sometime has information and logic available
        ///     which it can use to create a filtered list, it has the option to preload a list of
        ///     <see cref="EntityBll.v_GridColumnGroup_Bll">
        /// 		<see cref="EntityBll.v_GridColumnGroup_Bll">v_GridColumnGroup_Bll</see>
        /// 	</see> objects and pass into this method which will then be passed into this class
        ///     via this method.
        /// </overloads>
        /// <param name="parentType">
        ///     A string value naming the current parent's type. It’s possible for an Entity
        ///     Manager to have many different parent types while others have only one or none at
        ///     all. For example, an <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/The_Entity_Manager.html">Entity Manager</a> for an Address entity could have a parent type
        ///     of Customer, Company, Person, and so on. There may be situations where different
        ///     logic or filters need to be applied depending on the type of parent. This helps
        ///     make the <see cref="TypeServices.IEntityControl">IEntityControl</see> interface
        ///     more extendable and reusable.
        /// </param>
        /// <param name="intParentId">
        ///     int Id value of the object currently acting as the parent. Used to filter the
        ///     navigation list (<see cref="ucv_GridColumnGroupList">ucv_GridColumnGroupList</see>) and also to set the
        ///     StandingFK value. Both int and Guid data types are used as input parameters so the
        ///     <see cref="TypeServices.IEntityControl">IEntityControl</see> interface can
        ///     accommodate both data types making this more extendable and reusable.
        /// </param>
        /// <param name="guidParentId">
        ///     Guid Id value of the object currently acting as the parent. Used to filter the
        ///     navigation list (<see cref="ucv_GridColumnGroupList">ucv_GridColumnGroupList</see>) and also to set the
        ///     StandingFK value. Both int and Guid data types are used as input parameters so the
        ///     <see cref="TypeServices.IEntityControl">IEntityControl</see> interface can
        ///     accommodate both data types making this more extendable and reusable.
        /// </param>
        /// <param name="intId">
        ///     int Id value for the current v_GridColumnGroup. This can be used in <see cref="ucv_GridColumnGroupList.FindRowById">ucv_GridColumnGroupList.FindRowById</see> to select this instance of
        ///     <see cref="EntityBll.v_GridColumnGroup_Bll">v_GridColumnGroup_ Bll</see> in the navigation list and/or can
        ///     also be used to populate <see cref="ucv_GridColumnGroupProps">ucProps</see>. ucProps will
        ///     either use the instance of <see cref="EntityBll.v_GridColumnGroup_Bll">v_GridColumnGroup_Bll</see> selected
        ///     in the navigation list (<see cref="ucv_GridColumnGroupList">ucv_GridColumnGroupList</see>), or can be used
        ///     to fetch the data from the database. Both int and Guid data types are used as input
        ///     parameters so the <see cref="TypeServices.IEntityControl">IEntityControl</see>
        ///     interface can accommodate both types making this more extendable and reusable.
        /// </param>
        /// <param name="guidId">
        ///     Guid Id value for the current v_GridColumnGroup. This can be used in <see cref="ucv_GridColumnGroupList.FindRowById">ucv_GridColumnGroupList.FindRowById</see> to select this instance of
        ///     <see cref="EntityBll.v_GridColumnGroup_Bll">v_GridColumnGroup_ Bll</see> in the navigation list and/or can
        ///     also be used to populate <see cref="ucv_GridColumnGroupProps">ucProps</see>. ucProps will
        ///     either use the instance of <see cref="EntityBll.v_GridColumnGroup_Bll">v_GridColumnGroup_Bll</see> selected
        ///     in the navigation list (<see cref="ucv_GridColumnGroupList">ucv_GridColumnGroupList</see>), or can be used
        ///     to fetch the data from the database. Both int and Guid data types are used as input
        ///     parameters so the <see cref="TypeServices.IEntityControl">IEntityControl</see>
        ///     interface can accommodate both types making this more extendable and reusable.
        /// </param>
        /// <param name="currentBusinessObject">
        ///     In instance of v_GridColumnGroup_Bll can be passed in and bound to ucProps and/or added to the
        ///     list in <see cref="ucv_GridColumnGroupList">ucv_GridColumnGroupList</see>.
        /// </param>
        /// <param name="list">
        ///     A list of <see cref="EntityWireType.v_GridColumnGroup_ValuesMngr">v_GridColumnGroup_ValuesMngr</see>
        ///     objects using the <see cref="TypeServices.IEntity_ValuesMngr">IEntity_ValuesMngr</see> Interface which can be
        ///     passed in and used to populate <see cref="ucv_GridColumnGroupList">ucv_GridColumnGroupList</see> using the
        ///     <see cref="EntityBll.v_GridColumnGroup_List.ReplaceList">EntityBll.v_GridColumnGroup_List.ReplaceList</see>
        ///     method. This allows logic known only to the parent to create a filtered list.
        /// </param>
        /// <param name="newText">
        /// In cases when an entity is used in a dropdown list and the user wants to edit an
        /// item in the list, or add a new item; the text the user entered could be passed into
        /// this parameter and preloaded into the data entry part of the screen.
        /// </param>
        public void SetStateFromParent(string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, IBusinessObject currentBusinessObject, IEntity_ValuesMngr[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 7
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Enter);
                _intCurrentId = intId;
                _intParentId = intParentId;
                _guidParentId = guidParentId;
                _newText = newText;
                if (null != list)
                {
                    NavList_ItemSource.ReplaceList(list);
                }    
                else
                {
                    if (guidParentId == null)
                    {
                        NavListRefreshFromObjectArray(null);
                        navList.IsEnabled = false;
                    }
                    else
                    {
                        //  Add code here for multiple parent types
                        navList.IsEnabled = true;
                        _Proxy.Begin_v_GridColumnGroup_GetListByFK((Guid)_guidParentId);
                    }
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Leave);
            }
        }





        public bool IsGridReadOnly
        {
            get
            {
                //***  wc  need to change this in wc
                //***  we probably need to add a prop in wc for allow write in split screen
                //if (_secuitySettingIsReadOnly || _isSplitSreenMode || _isGridReadOnly)
                if (_secuitySettingIsReadOnly || _isGridReadOnly)



                //if (_secuitySettingIsReadOnly || _isGridReadOnly)   // put this back into use 2012-10-08.  this allows us to be in split screen mode and still be able to add new rows - for  cases when split screen mode is used to use a child grid in the right side such as category / sub category
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }






        #endregion CodeGen Methods for modification

        
        #region Custom Code


        public int GroupTypeId
        {
            get { return _groupTypeId; }
            set { _groupTypeId = value; }
        }


        public Guid? GridColumnGroupOwnerId
        {
            get { return _gridGroupOwnerId; }
            set { _gridGroupOwnerId = value; }
        }



        void navList_CellEnteringEditMode(object sender, BeginEditingCellEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellEnteredEditMode", IfxTraceCategory.Enter);
                v_GridColumnGroup_Bll obj = e.Cell.Row.Data as v_GridColumnGroup_Bll;
                if (obj == null) { return; }

                //  IF The Screen Is In 'User Defined Groups' and the selected item's group type ID = 3, then dont allow edits.
                //     this is a special system group which is availavble in both scnearios, System Groups and User Defined Groups, but we cant allow users to modify it.
                if (_groupTypeId == 2 && obj.v_GdColGrp_GdColGrpTp_Id == 3)
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellEnteredEditMode", ex);
                // throw IfxWrapperException.GetError(ex, (Guid)traceId);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellEnteredEditMode", IfxTraceCategory.Leave);
            }
        }


        public Guid GridId
        {
            get { return _gridId; }
            set { _gridId = value; }
        }

        #endregion Custom Code


        #region Fetch Data





        #endregion Fetch Data



    }
}
