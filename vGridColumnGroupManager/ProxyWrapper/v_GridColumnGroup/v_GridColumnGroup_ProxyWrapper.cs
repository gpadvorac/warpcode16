using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  5/17/2017 3:17:45 PM

namespace ProxyWrapper
{
    public partial class vGridColumnGroupService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<v_GridColumnGroup_GetByIdCompletedEventArgs> v_GridColumnGroup_GetByIdCompleted;
        public event System.EventHandler<v_GridColumnGroup_GetAllCompletedEventArgs> v_GridColumnGroup_GetAllCompleted;
        public event System.EventHandler<v_GridColumnGroup_GetListByFKCompletedEventArgs> v_GridColumnGroup_GetListByFKCompleted;
        public event System.EventHandler<v_GridColumnGroup_SaveCompletedEventArgs> v_GridColumnGroup_SaveCompleted;
        public event System.EventHandler<v_GridColumnGroup_DeleteCompletedEventArgs> v_GridColumnGroup_DeleteCompleted;
        public event System.EventHandler<v_GridColumnGroup_SetIsDeletedCompletedEventArgs> v_GridColumnGroup_SetIsDeletedCompleted;
        public event System.EventHandler<v_GridColumnGroup_DeactivateCompletedEventArgs> v_GridColumnGroup_DeactivateCompleted;
        public event System.EventHandler<v_GridColumnGroup_RemoveCompletedEventArgs> v_GridColumnGroup_RemoveCompleted;

        public event System.EventHandler<Getv_GridColumnGroup_ReadOnlyStaticListsCompletedEventArgs> Getv_GridColumnGroup_ReadOnlyStaticListsCompleted;
		public event System.EventHandler<Getv_GridColumnGroupType_ComboItemListCompletedEventArgs> Getv_GridColumnGroupType_ComboItemListCompleted;
		public event System.EventHandler<Getv_GridColumnGroup_lstByTypeCompletedEventArgs> Getv_GridColumnGroup_lstByTypeCompleted;
		public event System.EventHandler<Getv_GridColumnGroupColumn_AssignedCompletedEventArgs> Getv_GridColumnGroupColumn_AssignedCompleted;
		public event System.EventHandler<Getv_GridColumnGroup_cmbByTypeCompletedEventArgs> Getv_GridColumnGroup_cmbByTypeCompleted;
		public event System.EventHandler<Getv_GridColumnGroupColumn_lstByGroupTypeCompletedEventArgs> Getv_GridColumnGroupColumn_lstByGroupTypeCompleted;
		public event System.EventHandler<Executev_GridColumnGroupColumn_AddCompletedEventArgs> Executev_GridColumnGroupColumn_AddCompleted;
		public event System.EventHandler<Executev_GridColumnGroupColumn_RemoveCompletedEventArgs> Executev_GridColumnGroupColumn_RemoveCompleted;
		public event System.EventHandler<Getv_GridColumnGroupTypeUserPreference_GetTypeCompletedEventArgs> Getv_GridColumnGroupTypeUserPreference_GetTypeCompleted;
		public event System.EventHandler<Executev_GridColumnGroupTypeUserPreference_AddUpdateCompletedEventArgs> Executev_GridColumnGroupTypeUserPreference_AddUpdateCompleted;
		public event System.EventHandler<Getv_GridColumnGroupTypeUserPreference_lstByUserIdCompletedEventArgs> Getv_GridColumnGroupTypeUserPreference_lstByUserIdCompleted;
		public event System.EventHandler<Getv_GridMetaData_lstCompletedEventArgs> Getv_GridMetaData_lstCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region v_GridColumnGroup_GetById

        public void Begin_v_GridColumnGroup_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_GetById", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.v_GridColumnGroup_GetByIdCompleted += new EventHandler<v_GridColumnGroup_GetByIdCompletedEventArgs>(proxy_v_GridColumnGroup_GetByIdCompleted);
                proxy.v_GridColumnGroup_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_v_GridColumnGroup_GetByIdCompleted(object sender, v_GridColumnGroup_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<v_GridColumnGroup_GetByIdCompletedEventArgs> handler = v_GridColumnGroup_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion v_GridColumnGroup_GetById

        #region v_GridColumnGroup_GetAll

        public void Begin_v_GridColumnGroup_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_GetAll", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.v_GridColumnGroup_GetAllCompleted += new EventHandler<v_GridColumnGroup_GetAllCompletedEventArgs>(proxy_v_GridColumnGroup_GetAllCompleted);
                proxy.v_GridColumnGroup_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_v_GridColumnGroup_GetAllCompleted(object sender, v_GridColumnGroup_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<v_GridColumnGroup_GetAllCompletedEventArgs> handler = v_GridColumnGroup_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion v_GridColumnGroup_GetAll

        #region v_GridColumnGroup_GetListByFK

        public void Begin_v_GridColumnGroup_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_GetListByFK", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.v_GridColumnGroup_GetListByFKCompleted += new EventHandler<v_GridColumnGroup_GetListByFKCompletedEventArgs>(proxy_v_GridColumnGroup_GetListByFKCompleted);
                proxy.v_GridColumnGroup_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_v_GridColumnGroup_GetListByFKCompleted(object sender, v_GridColumnGroup_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<v_GridColumnGroup_GetListByFKCompletedEventArgs> handler = v_GridColumnGroup_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion v_GridColumnGroup_GetListByFK

        #region v_GridColumnGroup_Save

        public void Begin_v_GridColumnGroup_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_Save", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                //proxy.v_GridColumnGroup_SaveCompleted += new EventHandler<v_GridColumnGroup_SaveCompletedEventArgs>(proxy_v_GridColumnGroup_SaveCompleted);
                proxy.v_GridColumnGroup_SaveCompleted += new EventHandler<v_GridColumnGroup_SaveCompletedEventArgs>(proxy_v_GridColumnGroup_SaveCompleted);
                proxy.v_GridColumnGroup_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_v_GridColumnGroup_SaveCompleted(object sender, v_GridColumnGroup_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<v_GridColumnGroup_SaveCompletedEventArgs> handler = v_GridColumnGroup_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion v_GridColumnGroup_Save

        #region v_GridColumnGroup_Delete

        public void Begin_v_GridColumnGroup_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_DeleteCompleted", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.v_GridColumnGroup_DeleteCompleted += new EventHandler<v_GridColumnGroup_DeleteCompletedEventArgs>(proxy_v_GridColumnGroup_DeleteCompleted);
                proxy.v_GridColumnGroup_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_v_GridColumnGroup_DeleteCompleted(object sender, v_GridColumnGroup_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<v_GridColumnGroup_DeleteCompletedEventArgs> handler = v_GridColumnGroup_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion v_GridColumnGroup_Delete

        #region v_GridColumnGroup_SetIsDeleted

        public void Begin_v_GridColumnGroup_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.v_GridColumnGroup_SetIsDeletedCompleted += new EventHandler<v_GridColumnGroup_SetIsDeletedCompletedEventArgs>(proxy_v_GridColumnGroup_SetIsDeletedCompleted);
                proxy.v_GridColumnGroup_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_v_GridColumnGroup_SetIsDeletedCompleted(object sender, v_GridColumnGroup_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<v_GridColumnGroup_SetIsDeletedCompletedEventArgs> handler = v_GridColumnGroup_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion v_GridColumnGroup_SetIsDeleted

        #region v_GridColumnGroup_Deactivate

        public void Begin_v_GridColumnGroup_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_Deactivate", IfxTraceCategory.Enter);
            IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
            AssignCredentials(proxy);
            proxy.v_GridColumnGroup_DeactivateCompleted += new EventHandler<v_GridColumnGroup_DeactivateCompletedEventArgs>(proxy_v_GridColumnGroup_DeactivateCompleted);
            proxy.v_GridColumnGroup_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_v_GridColumnGroup_DeactivateCompleted(object sender, v_GridColumnGroup_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<v_GridColumnGroup_DeactivateCompletedEventArgs> handler = v_GridColumnGroup_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion v_GridColumnGroup_Deactivate

        #region v_GridColumnGroup_Remove

        public void Begin_v_GridColumnGroup_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_Remove", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.v_GridColumnGroup_RemoveCompleted += new EventHandler<v_GridColumnGroup_RemoveCompletedEventArgs>(proxy_v_GridColumnGroup_RemoveCompleted);
                proxy.v_GridColumnGroup_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_v_GridColumnGroup_RemoveCompleted(object sender, v_GridColumnGroup_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<v_GridColumnGroup_RemoveCompletedEventArgs> handler = v_GridColumnGroup_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion v_GridColumnGroup_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists

        public void Begin_Getv_GridColumnGroup_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroup_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.Getv_GridColumnGroup_ReadOnlyStaticListsCompleted += new EventHandler<Getv_GridColumnGroup_ReadOnlyStaticListsCompletedEventArgs>(proxy_Getv_GridColumnGroup_ReadOnlyStaticListsCompleted);
                proxy.Getv_GridColumnGroup_ReadOnlyStaticListsAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroup_ReadOnlyStaticLists", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroup_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }

        void proxy_Getv_GridColumnGroup_ReadOnlyStaticListsCompleted(object sender, Getv_GridColumnGroup_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_Getv_GridColumnGroup_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Getv_GridColumnGroup_ReadOnlyStaticListsCompletedEventArgs> handler = Getv_GridColumnGroup_ReadOnlyStaticListsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_Getv_GridColumnGroup_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_Getv_GridColumnGroup_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion ReadOnlyStaticLists

        #region Getv_GridColumnGroupType_ComboItemList

        public void Begin_Getv_GridColumnGroupType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupType_ComboItemList", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.Getv_GridColumnGroupType_ComboItemListCompleted += new EventHandler<Getv_GridColumnGroupType_ComboItemListCompletedEventArgs>(proxy_Getv_GridColumnGroupType_ComboItemListCompleted);
                proxy.Getv_GridColumnGroupType_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupType_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_Getv_GridColumnGroupType_ComboItemListCompleted(object sender, Getv_GridColumnGroupType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupType_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<Getv_GridColumnGroupType_ComboItemListCompletedEventArgs> handler = Getv_GridColumnGroupType_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupType_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion Getv_GridColumnGroupType_ComboItemList

        #region Getv_GridColumnGroup_lstByType

        public void Begin_Getv_GridColumnGroup_lstByType(Guid v_GdColGrp_Gd_Id, Guid? OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroup_lstByType", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.Getv_GridColumnGroup_lstByTypeCompleted += new EventHandler<Getv_GridColumnGroup_lstByTypeCompletedEventArgs>(proxy_Getv_GridColumnGroup_lstByTypeCompleted);
                proxy.Getv_GridColumnGroup_lstByTypeAsync(v_GdColGrp_Gd_Id, OwnerId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroup_lstByType", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroup_lstByType", IfxTraceCategory.Leave);
            }
        }

        void proxy_Getv_GridColumnGroup_lstByTypeCompleted(object sender, Getv_GridColumnGroup_lstByTypeCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroup_lstByType", IfxTraceCategory.Enter);
                System.EventHandler<Getv_GridColumnGroup_lstByTypeCompletedEventArgs> handler = Getv_GridColumnGroup_lstByTypeCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroup_lstByType", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroup_lstByType", IfxTraceCategory.Leave);
            }
        }

        #endregion Getv_GridColumnGroup_lstByType

        #region Getv_GridColumnGroupColumn_Assigned

        public void Begin_Getv_GridColumnGroupColumn_Assigned(Guid v_GdColGrp_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupColumn_Assigned", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.Getv_GridColumnGroupColumn_AssignedCompleted += new EventHandler<Getv_GridColumnGroupColumn_AssignedCompletedEventArgs>(proxy_Getv_GridColumnGroupColumn_AssignedCompleted);
                proxy.Getv_GridColumnGroupColumn_AssignedAsync(v_GdColGrp_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupColumn_Assigned", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupColumn_Assigned", IfxTraceCategory.Leave);
            }
        }

        void proxy_Getv_GridColumnGroupColumn_AssignedCompleted(object sender, Getv_GridColumnGroupColumn_AssignedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupColumn_Assigned", IfxTraceCategory.Enter);
                System.EventHandler<Getv_GridColumnGroupColumn_AssignedCompletedEventArgs> handler = Getv_GridColumnGroupColumn_AssignedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupColumn_Assigned", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupColumn_Assigned", IfxTraceCategory.Leave);
            }
        }

        #endregion Getv_GridColumnGroupColumn_Assigned

        #region Getv_GridColumnGroup_cmbByType

        public void Begin_Getv_GridColumnGroup_cmbByType(Guid Grid_Id, Guid? OwnerId, Int32 ListType )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroup_cmbByType", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.Getv_GridColumnGroup_cmbByTypeCompleted += new EventHandler<Getv_GridColumnGroup_cmbByTypeCompletedEventArgs>(proxy_Getv_GridColumnGroup_cmbByTypeCompleted);
                proxy.Getv_GridColumnGroup_cmbByTypeAsync(Grid_Id, OwnerId, ListType );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroup_cmbByType", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroup_cmbByType", IfxTraceCategory.Leave);
            }
        }

        void proxy_Getv_GridColumnGroup_cmbByTypeCompleted(object sender, Getv_GridColumnGroup_cmbByTypeCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroup_cmbByType", IfxTraceCategory.Enter);
                System.EventHandler<Getv_GridColumnGroup_cmbByTypeCompletedEventArgs> handler = Getv_GridColumnGroup_cmbByTypeCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroup_cmbByType", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroup_cmbByType", IfxTraceCategory.Leave);
            }
        }

        #endregion Getv_GridColumnGroup_cmbByType

        #region Getv_GridColumnGroupColumn_lstByGroupType

        public void Begin_Getv_GridColumnGroupColumn_lstByGroupType(Guid Grid_Id, Guid? OwnerId, Int32 ListType )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupColumn_lstByGroupType", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.Getv_GridColumnGroupColumn_lstByGroupTypeCompleted += new EventHandler<Getv_GridColumnGroupColumn_lstByGroupTypeCompletedEventArgs>(proxy_Getv_GridColumnGroupColumn_lstByGroupTypeCompleted);
                proxy.Getv_GridColumnGroupColumn_lstByGroupTypeAsync(Grid_Id, OwnerId, ListType );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupColumn_lstByGroupType", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupColumn_lstByGroupType", IfxTraceCategory.Leave);
            }
        }

        void proxy_Getv_GridColumnGroupColumn_lstByGroupTypeCompleted(object sender, Getv_GridColumnGroupColumn_lstByGroupTypeCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupColumn_lstByGroupType", IfxTraceCategory.Enter);
                System.EventHandler<Getv_GridColumnGroupColumn_lstByGroupTypeCompletedEventArgs> handler = Getv_GridColumnGroupColumn_lstByGroupTypeCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupColumn_lstByGroupType", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupColumn_lstByGroupType", IfxTraceCategory.Leave);
            }
        }

        #endregion Getv_GridColumnGroupColumn_lstByGroupType

        #region Executev_GridColumnGroupColumn_Add

        public void Begin_Executev_GridColumnGroupColumn_Add(Guid v_GdColGrpCol_Id, Guid v_GdColGrpCol_GdGrp_Id, String v_GdColGrpCol_KeyName, String v_GdColGrpCol_HeaderText, Int32 v_GdColGrpCol_Index, Guid v_GdColGrpCol_CreatedUserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Executev_GridColumnGroupColumn_Add", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.Executev_GridColumnGroupColumn_AddCompleted += new EventHandler<Executev_GridColumnGroupColumn_AddCompletedEventArgs>(proxy_Executev_GridColumnGroupColumn_AddCompleted);
                proxy.Executev_GridColumnGroupColumn_AddAsync(v_GdColGrpCol_Id, v_GdColGrpCol_GdGrp_Id, v_GdColGrpCol_KeyName, v_GdColGrpCol_HeaderText, v_GdColGrpCol_Index, v_GdColGrpCol_CreatedUserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Executev_GridColumnGroupColumn_Add", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Executev_GridColumnGroupColumn_Add", IfxTraceCategory.Leave);
            }
        }

        void proxy_Executev_GridColumnGroupColumn_AddCompleted(object sender, Executev_GridColumnGroupColumn_AddCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupColumn_Add", IfxTraceCategory.Enter);
                System.EventHandler<Executev_GridColumnGroupColumn_AddCompletedEventArgs> handler = Executev_GridColumnGroupColumn_AddCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupColumn_Add", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupColumn_Add", IfxTraceCategory.Leave);
            }
        }

        #endregion Executev_GridColumnGroupColumn_Add

        #region Executev_GridColumnGroupColumn_Remove

        public void Begin_Executev_GridColumnGroupColumn_Remove(String crit )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Executev_GridColumnGroupColumn_Remove", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.Executev_GridColumnGroupColumn_RemoveCompleted += new EventHandler<Executev_GridColumnGroupColumn_RemoveCompletedEventArgs>(proxy_Executev_GridColumnGroupColumn_RemoveCompleted);
                proxy.Executev_GridColumnGroupColumn_RemoveAsync(crit );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Executev_GridColumnGroupColumn_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Executev_GridColumnGroupColumn_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_Executev_GridColumnGroupColumn_RemoveCompleted(object sender, Executev_GridColumnGroupColumn_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupColumn_Remove", IfxTraceCategory.Enter);
                System.EventHandler<Executev_GridColumnGroupColumn_RemoveCompletedEventArgs> handler = Executev_GridColumnGroupColumn_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupColumn_Remove", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupColumn_Remove", IfxTraceCategory.Leave);
            }
        }

        #endregion Executev_GridColumnGroupColumn_Remove

        #region Getv_GridColumnGroupTypeUserPreference_GetType

        public void Begin_Getv_GridColumnGroupTypeUserPreference_GetType(Guid v_GdColGrpTpUP_UserId, Guid v_GdColGrpTpUP_Gd_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupTypeUserPreference_GetType", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.Getv_GridColumnGroupTypeUserPreference_GetTypeCompleted += new EventHandler<Getv_GridColumnGroupTypeUserPreference_GetTypeCompletedEventArgs>(proxy_Getv_GridColumnGroupTypeUserPreference_GetTypeCompleted);
                proxy.Getv_GridColumnGroupTypeUserPreference_GetTypeAsync(v_GdColGrpTpUP_UserId, v_GdColGrpTpUP_Gd_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupTypeUserPreference_GetType", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupTypeUserPreference_GetType", IfxTraceCategory.Leave);
            }
        }

        void proxy_Getv_GridColumnGroupTypeUserPreference_GetTypeCompleted(object sender, Getv_GridColumnGroupTypeUserPreference_GetTypeCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupTypeUserPreference_GetType", IfxTraceCategory.Enter);
                System.EventHandler<Getv_GridColumnGroupTypeUserPreference_GetTypeCompletedEventArgs> handler = Getv_GridColumnGroupTypeUserPreference_GetTypeCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupTypeUserPreference_GetType", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupTypeUserPreference_GetType", IfxTraceCategory.Leave);
            }
        }

        #endregion Getv_GridColumnGroupTypeUserPreference_GetType

        #region Executev_GridColumnGroupTypeUserPreference_AddUpdate

        public void Begin_Executev_GridColumnGroupTypeUserPreference_AddUpdate(Guid v_GdColGrpTpUP_UserId, Guid v_GdColGrpTpUP_Gd_Id, Int32 v_GdColGrpTpUP_GdGrpTp_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Executev_GridColumnGroupTypeUserPreference_AddUpdate", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.Executev_GridColumnGroupTypeUserPreference_AddUpdateCompleted += new EventHandler<Executev_GridColumnGroupTypeUserPreference_AddUpdateCompletedEventArgs>(proxy_Executev_GridColumnGroupTypeUserPreference_AddUpdateCompleted);
                proxy.Executev_GridColumnGroupTypeUserPreference_AddUpdateAsync(v_GdColGrpTpUP_UserId, v_GdColGrpTpUP_Gd_Id, v_GdColGrpTpUP_GdGrpTp_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Executev_GridColumnGroupTypeUserPreference_AddUpdate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Executev_GridColumnGroupTypeUserPreference_AddUpdate", IfxTraceCategory.Leave);
            }
        }

        void proxy_Executev_GridColumnGroupTypeUserPreference_AddUpdateCompleted(object sender, Executev_GridColumnGroupTypeUserPreference_AddUpdateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupTypeUserPreference_AddUpdate", IfxTraceCategory.Enter);
                System.EventHandler<Executev_GridColumnGroupTypeUserPreference_AddUpdateCompletedEventArgs> handler = Executev_GridColumnGroupTypeUserPreference_AddUpdateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupTypeUserPreference_AddUpdate", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupTypeUserPreference_AddUpdate", IfxTraceCategory.Leave);
            }
        }

        #endregion Executev_GridColumnGroupTypeUserPreference_AddUpdate

        #region Getv_GridColumnGroupTypeUserPreference_lstByUserId

        public void Begin_Getv_GridColumnGroupTypeUserPreference_lstByUserId(Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupTypeUserPreference_lstByUserId", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.Getv_GridColumnGroupTypeUserPreference_lstByUserIdCompleted += new EventHandler<Getv_GridColumnGroupTypeUserPreference_lstByUserIdCompletedEventArgs>(proxy_Getv_GridColumnGroupTypeUserPreference_lstByUserIdCompleted);
                proxy.Getv_GridColumnGroupTypeUserPreference_lstByUserIdAsync(UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupTypeUserPreference_lstByUserId", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroupTypeUserPreference_lstByUserId", IfxTraceCategory.Leave);
            }
        }

        void proxy_Getv_GridColumnGroupTypeUserPreference_lstByUserIdCompleted(object sender, Getv_GridColumnGroupTypeUserPreference_lstByUserIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupTypeUserPreference_lstByUserId", IfxTraceCategory.Enter);
                System.EventHandler<Getv_GridColumnGroupTypeUserPreference_lstByUserIdCompletedEventArgs> handler = Getv_GridColumnGroupTypeUserPreference_lstByUserIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupTypeUserPreference_lstByUserId", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupTypeUserPreference_lstByUserId", IfxTraceCategory.Leave);
            }
        }

        #endregion Getv_GridColumnGroupTypeUserPreference_lstByUserId

        #region Getv_GridMetaData_lst

        public void Begin_Getv_GridMetaData_lst()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridMetaData_lst", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.Getv_GridMetaData_lstCompleted += new EventHandler<Getv_GridMetaData_lstCompletedEventArgs>(proxy_Getv_GridMetaData_lstCompleted);
                proxy.Getv_GridMetaData_lstAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridMetaData_lst", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridMetaData_lst", IfxTraceCategory.Leave);
            }
        }

        void proxy_Getv_GridMetaData_lstCompleted(object sender, Getv_GridMetaData_lstCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridMetaData_lst", IfxTraceCategory.Enter);
                System.EventHandler<Getv_GridMetaData_lstCompletedEventArgs> handler = Getv_GridMetaData_lstCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridMetaData_lst", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridMetaData_lst", IfxTraceCategory.Leave);
            }
        }

        #endregion Getv_GridMetaData_lst

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


