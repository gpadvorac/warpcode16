using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  11/22/2014 2:03:22 PM

namespace ProxyWrapper
{
    public partial class vGridColumnGroupService_ProxyWrapper
    {

        #region Initialize Variables

        public event System.EventHandler<v_GridColumnGroup_AssignColumnsCompletedEventArgs> v_GridColumnGroup_AssignColumnsCompleted;
        //public event System.EventHandler<v_GridColumnGroup_RemoveColumnsCompletedEventArgs> v_GridColumnGroup_RemoveColumnsCompleted;
        public event System.EventHandler<GetGridColumnGroupsAndColumnsCompletedEventArgs> GetGridColumnGroupsAndColumnsCompleted;
        //public event System.EventHandler<Get_GridColumnGroupTypeUserPreference_GroupTypeCompletedEventArgs> Get_GridColumnGroupTypeUserPreference_GroupTypeCompleted;
        //public event System.EventHandler<v_GridColumnGroupTypeUserPreference_AddUpdateCompletedEventArgs> v_GridColumnGroupTypeUserPreference_AddUpdateCompleted;
        //public event System.EventHandler<v_GridColumnGroupTypeUserPreference_lstByUserIdCompletedEventArgs> v_GridColumnGroupTypeUserPreference_lstByUserIdCompleted;


        #endregion Initialize Variables




        //#region ReadOnlyStaticLists

        //public void Begin_Getv_GridColumnGroup_ReadOnlyStaticLists(Guid v_GdColGrp_Gd_Id,  Guid v_GdColGrp_OwnerId)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroup_ReadOnlyStaticLists", IfxTraceCategory.Enter);
        //        vGridColumnGroupServiceClient proxy = new vGridColumnGroupServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.Getv_GridColumnGroup_ReadOnlyStaticListsCompleted += new EventHandler<Getv_GridColumnGroup_ReadOnlyStaticListsCompletedEventArgs>(proxy_Getv_GridColumnGroup_ReadOnlyStaticListsCompleted);
        //        proxy.Getv_GridColumnGroup_ReadOnlyStaticListsAsync(v_GdColGrp_Gd_Id, v_GdColGrp_Gd_Id, v_GdColGrp_OwnerId);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroup_ReadOnlyStaticLists", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_GridColumnGroup_ReadOnlyStaticLists", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_Getv_GridColumnGroup_ReadOnlyStaticListsCompleted(object sender, Getv_GridColumnGroup_ReadOnlyStaticListsCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_Getv_GridColumnGroup_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
        //        System.EventHandler<Getv_GridColumnGroup_ReadOnlyStaticListsCompletedEventArgs> handler = Getv_GridColumnGroup_ReadOnlyStaticListsCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_Getv_GridColumnGroup_ReadOnlyStaticListsCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_Getv_GridColumnGroup_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion ReadOnlyStaticLists



        #region v_GridColumnGroup_AssignColumns

        public void Begin_v_GridColumnGroup_AssignColumns(object[] data, Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_AssignColumns", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.v_GridColumnGroup_AssignColumnsCompleted += new EventHandler<v_GridColumnGroup_AssignColumnsCompletedEventArgs>(proxy_v_GridColumnGroup_AssignColumnsCompleted);
                proxy.v_GridColumnGroup_AssignColumnsAsync(data, userId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_AssignColumns", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_AssignColumns", IfxTraceCategory.Leave);
            }
        }

        void proxy_v_GridColumnGroup_AssignColumnsCompleted(object sender, v_GridColumnGroup_AssignColumnsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_AssignColumns", IfxTraceCategory.Enter);
                System.EventHandler<v_GridColumnGroup_AssignColumnsCompletedEventArgs> handler = v_GridColumnGroup_AssignColumnsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_AssignColumns", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_AssignColumns", IfxTraceCategory.Leave);
            }
        }

        #endregion v_GridColumnGroup_AssignColumns




        //#region v_GridColumnGroup_RemoveColumns

        //public void Begin_v_GridColumnGroup_RemoveColumns(string crit)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_RemoveColumns", IfxTraceCategory.Enter);
        //        vGridColumnGroupServiceClient proxy = new vGridColumnGroupServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.v_GridColumnGroup_RemoveColumnsCompleted += new EventHandler<v_GridColumnGroup_RemoveColumnsCompletedEventArgs>(proxy_v_GridColumnGroup_RemoveColumnsCompleted);
        //        proxy.v_GridColumnGroup_RemoveColumnsAsync( crit);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_RemoveColumns", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroup_RemoveColumns", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_v_GridColumnGroup_RemoveColumnsCompleted(object sender, v_GridColumnGroup_RemoveColumnsCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_RemoveColumns", IfxTraceCategory.Enter);
        //        System.EventHandler<v_GridColumnGroup_RemoveColumnsCompletedEventArgs> handler = v_GridColumnGroup_RemoveColumnsCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_RemoveColumns", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_RemoveColumns", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion v_GridColumnGroup_RemoveColumns




        #region GetGridColumnGroupsAndColumns

        public void Begin_GetGridColumnGroupsAndColumns(Guid v_GdColGrp_Gd_Id, Guid? OwnerId, Int32 ListType)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetGridColumnGroupsAndColumns", IfxTraceCategory.Enter);
                IvGridColumnGroupServiceClient proxy = new IvGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.GetGridColumnGroupsAndColumnsCompleted += new EventHandler<GetGridColumnGroupsAndColumnsCompletedEventArgs>(proxy_GetGridColumnGroupsAndColumnsCompleted);
                proxy.GetGridColumnGroupsAndColumnsAsync(v_GdColGrp_Gd_Id, OwnerId, ListType);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetGridColumnGroupsAndColumns", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetGridColumnGroupsAndColumns", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetGridColumnGroupsAndColumnsCompleted(object sender, GetGridColumnGroupsAndColumnsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetGridColumnGroupsAndColumns", IfxTraceCategory.Enter);
                System.EventHandler<GetGridColumnGroupsAndColumnsCompletedEventArgs> handler = GetGridColumnGroupsAndColumnsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetGridColumnGroupsAndColumns", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetGridColumnGroupsAndColumns", IfxTraceCategory.Leave);
            }
        }

        #endregion GetGridColumnGroupsAndColumns





        //#region Get_GridColumnGroupTypeUserPreference_GroupType

        //public void Begin_Get_GridColumnGroupTypeUserPreference_GroupType(Guid userId, Guid gridId)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Get_GridColumnGroupTypeUserPreference_GroupType", IfxTraceCategory.Enter);
        //        vGridColumnGroupServiceClient proxy = new vGridColumnGroupServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.Get_GridColumnGroupTypeUserPreference_GroupTypeCompleted += new EventHandler<Get_GridColumnGroupTypeUserPreference_GroupTypeCompletedEventArgs>(proxy_Get_GridColumnGroupTypeUserPreference_GroupTypeCompleted);
        //        proxy.Get_GridColumnGroupTypeUserPreference_GroupTypeAsync(userId, gridId);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Get_GridColumnGroupTypeUserPreference_GroupType", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Get_GridColumnGroupTypeUserPreference_GroupType", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_Get_GridColumnGroupTypeUserPreference_GroupTypeCompleted(object sender, Get_GridColumnGroupTypeUserPreference_GroupTypeCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_GridColumnGroupTypeUserPreference_GroupType", IfxTraceCategory.Enter);
        //        System.EventHandler<Get_GridColumnGroupTypeUserPreference_GroupTypeCompletedEventArgs> handler = Get_GridColumnGroupTypeUserPreference_GroupTypeCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_GridColumnGroupTypeUserPreference_GroupType", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_GridColumnGroupTypeUserPreference_GroupType", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion Get_GridColumnGroupTypeUserPreference_GroupType




        //#region v_GridColumnGroupTypeUserPreference_AddUpdate

        //public void Begin_v_GridColumnGroupTypeUserPreference_AddUpdate(Guid userId, Guid gridId, int groupType)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroupTypeUserPreference_AddUpdate", IfxTraceCategory.Enter);
        //        vGridColumnGroupServiceClient proxy = new vGridColumnGroupServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.v_GridColumnGroupTypeUserPreference_AddUpdateCompleted += new EventHandler<v_GridColumnGroupTypeUserPreference_AddUpdateCompletedEventArgs>(proxy_v_GridColumnGroupTypeUserPreference_AddUpdateCompleted);
        //        proxy.v_GridColumnGroupTypeUserPreference_AddUpdateAsync(userId, gridId, groupType);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroupTypeUserPreference_AddUpdate", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroupTypeUserPreference_AddUpdate", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_v_GridColumnGroupTypeUserPreference_AddUpdateCompleted(object sender, v_GridColumnGroupTypeUserPreference_AddUpdateCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_AddUpdate", IfxTraceCategory.Enter);
        //        System.EventHandler<v_GridColumnGroupTypeUserPreference_AddUpdateCompletedEventArgs> handler = v_GridColumnGroupTypeUserPreference_AddUpdateCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_AddUpdate", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_AddUpdate", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion v_GridColumnGroupTypeUserPreference_AddUpdate






        //#region v_GridColumnGroupTypeUserPreference_lstByUserId

        //public void Begin_v_GridColumnGroupTypeUserPreference_lstByUserId(Guid userId)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroupTypeUserPreference_lstByUserId", IfxTraceCategory.Enter);
        //        vGridColumnGroupServiceClient proxy = new vGridColumnGroupServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.v_GridColumnGroupTypeUserPreference_lstByUserIdCompleted += new EventHandler<v_GridColumnGroupTypeUserPreference_lstByUserIdCompletedEventArgs>(proxy_v_GridColumnGroupTypeUserPreference_lstByUserIdCompleted);
        //        proxy.v_GridColumnGroupTypeUserPreference_lstByUserIdAsync(userId);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroupTypeUserPreference_lstByUserId", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_GridColumnGroupTypeUserPreference_lstByUserId", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_v_GridColumnGroupTypeUserPreference_lstByUserIdCompleted(object sender, v_GridColumnGroupTypeUserPreference_lstByUserIdCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_lstByUserId", IfxTraceCategory.Enter);
        //        System.EventHandler<v_GridColumnGroupTypeUserPreference_lstByUserIdCompletedEventArgs> handler = v_GridColumnGroupTypeUserPreference_lstByUserIdCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_lstByUserId", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_lstByUserId", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion v_GridColumnGroupTypeUserPreference_lstByUserId




    }
}


