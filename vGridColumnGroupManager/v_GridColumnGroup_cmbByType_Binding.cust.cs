﻿using System;
using System.Windows;
using System.Diagnostics;
using System.Collections.Generic;
using Infragistics.Controls.Grids;
using Ifx.SL;
using vUICommon;
using TypeServices;

namespace EntityWireTypeSL
{
    public partial class v_GridColumnGroup_cmbByType_Binding
    {

        XamGrid _currentXamGrid = null;

        List<Column> _fields = new List<Column>();


        #region Custom Constructor

        public v_GridColumnGroup_cmbByType_Binding(object[] data, object[] columns, XamGrid grid)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_cmbByType_Binding", IfxTraceCategory.Enter);

                _currentXamGrid = grid;

                _a = (Guid)data[0];                //  v_GdColGrp_Id
                _b = ObjectHelper.GetNullableIntFromObjectValue(data[1]);                 //  v_GdColGrp_SortOrder
                _c = (String)data[2];                //  _v_GdColGrp_SysName
                _d = (String)data[3];                //  v_GdColGrp_Name
                _e = (Boolean)data[4];                //  v_GdColGrp_IsDefault

                AddFields(columns);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_cmbByType_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_cmbByType_Binding", IfxTraceCategory.Leave);
            }
        }

        #endregion Custom Constructor




        #region Methods



        void AddFields(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AddFields", IfxTraceCategory.Enter);

                if (data != null && data.Length > 0)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        object[] item = (object[])data[i];
                        Guid? grp = item[1] as Guid?;
                        if (grp == v_GdColGrp_Id)
                        {
                            string key = item[2] as string;
                            try
                            {

                                if (_currentXamGrid.Columns.DataColumns[key] == null)
                                {
                                    string msg = "Attempting to reference column '" + key + "', however, this column doesn't exist in the grid.  Please refresh the columns in the column group '" + v_GdColGrp_Name + "'.";
                                    msg = msg + System.Environment.NewLine + "You can view this message from the exception logs.";
                                    msg = msg + System.Environment.NewLine + System.Environment.NewLine + "Group_Id: " + v_GdColGrp_Id.ToString();
                                    msg = msg + System.Environment.NewLine + "v_GdColGrp_Name: " + v_GdColGrp_Name;
                                    msg = msg + System.Environment.NewLine + "v_GdColGrp_SysName: " + v_GdColGrp_SysName;
                                    msg = msg + System.Environment.NewLine + "Key: " + key;

                                    IfxEvent.PublishTrace(traceId, _as, _cn, "AddFields", new Exception(msg));
                                    // Dont alert the user.  this might be by design becuase a column was programattically removed.
                                    //MessageBox.Show(msg, "Outdated Column Reference", MessageBoxButton.OK);
                                }
                                else
                                {
                                    _fields.Add(_currentXamGrid.Columns.DataColumns[key]);
                                }
                            }
                            catch (Exception exx)
                            {
                                IfxEvent.PublishTrace(traceId, _as, _cn, "AddFields", exx);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AddFields", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AddFields", IfxTraceCategory.Leave);
            }
        }

        public void SetColumnVisibility(bool isVisible)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetColumnVisibility", IfxTraceCategory.Enter);

                IsGroupVisible = isVisible;
                Visibility visible = Visibility.Collapsed;
                if (isVisible == true)
                {
                    visible = Visibility.Visible;
                }
                foreach (Column col in _fields)
                {
                    if (col != null)
                    {
                        col.Visibility = visible;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetColumnVisibility", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetColumnVisibility", IfxTraceCategory.Leave);
            }
        }



        #endregion Methods



        #region Properties



        bool _isGroupVisible = false;


        public bool IsGroupVisible
        {
            get { return _isGroupVisible; }
            set { _isGroupVisible = value; }
        }

        public List<Column> Fields
        {
            get { return _fields; }
            set { _fields = value; }
        }



        public XamGrid CurrentXamGrid
        {
            get { return _currentXamGrid; }
            set { _currentXamGrid = value; }
        }





        #endregion Properties



    }
}
