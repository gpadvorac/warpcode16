using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

namespace EntityWireTypeSL
{


    [DataContract]
    public partial class v_GridColumnGroup_cmbByType_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "vGridColumnGroupManager";
        private static string _cn = "v_GridColumnGroup_cmbByType_Binding";

        #endregion Initialize Variables


        #region Constructors

        public v_GridColumnGroup_cmbByType_Binding() { }


        public v_GridColumnGroup_cmbByType_Binding(Guid _v_GdColGrp_Id, Int32? _v_GdColGrp_SortOrder, String _v_GdColGrp_SysName, String _v_GdColGrp_Name, Boolean _v_GdColGrp_IsDefault)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_cmbByType_Binding", IfxTraceCategory.Enter);
                _a = _v_GdColGrp_Id;
                _b = _v_GdColGrp_SortOrder;
                _c = _v_GdColGrp_SysName;
                _d = _v_GdColGrp_Name;
                _e = _v_GdColGrp_IsDefault;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_cmbByType_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_cmbByType_Binding", IfxTraceCategory.Leave);
            }
        }

        public v_GridColumnGroup_cmbByType_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_cmbByType_Binding", IfxTraceCategory.Enter);
                _a = (Guid)data[0];                //  v_GdColGrp_Id
                _b = ObjectHelper.GetNullableIntFromObjectValue(data[1]);                 //  v_GdColGrp_SortOrder
                _c = ObjectHelper.GetStringFromObjectValue(data[2]);                 //  v_GdColGrp_SysName
                _d = (String)data[3];                //  v_GdColGrp_Name
                _e = (Boolean)data[4];                //  v_GdColGrp_IsDefault
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_cmbByType_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_cmbByType_Binding", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region v_GdColGrp_Id

        private Guid _a;
        //        [DataMember]
        //        public Guid A
        //        {
        //            get { return _a; }
        //            set { _a = value; }
        //        }

        //[NonSerialized]
        public Guid v_GdColGrp_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion v_GdColGrp_Id


        #region v_GdColGrp_SortOrder

        private Int32? _b;
        //        [DataMember]
        //        public Int32? B
        //        {
        //            get { return _b; }
        //            set { _b = value; }
        //        }

        //[NonSerialized]
        public Int32? v_GdColGrp_SortOrder
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion v_GdColGrp_SortOrder


        #region v_GdColGrp_SysName

        private String _c;
        //        [DataMember]
        //        public String C
        //        {
        //            get { return _c; }
        //            set { _c = value; }
        //        }

        //[NonSerialized]
        public String v_GdColGrp_SysName
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion v_GdColGrp_SysName


        #region v_GdColGrp_Name

        private String _d;
        //        [DataMember]
        //        public String D
        //        {
        //            get { return _d; }
        //            set { _d = value; }
        //        }

        //[NonSerialized]
        public String v_GdColGrp_Name
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion v_GdColGrp_Name


        #region v_GdColGrp_IsDefault

        private Boolean _e;
        //        [DataMember]
        //        public Boolean E
        //        {
        //            get { return _e; }
        //            set { _e = value; }
        //        }

        //[NonSerialized]
        public Boolean v_GdColGrp_IsDefault
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion v_GdColGrp_IsDefault


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4} ", v_GdColGrp_Id, v_GdColGrp_SortOrder, v_GdColGrp_SysName, v_GdColGrp_Name, v_GdColGrp_IsDefault);
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4} ", v_GdColGrp_Id, v_GdColGrp_SortOrder, v_GdColGrp_SysName, v_GdColGrp_Name, v_GdColGrp_IsDefault);
        }

    }

}
