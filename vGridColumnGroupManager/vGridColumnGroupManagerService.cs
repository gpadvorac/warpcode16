﻿using ApplicationTypeServices;
using Ifx.SL;
using Infragistics.Controls.Grids;
using Infragistics.Controls.Interactions;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using vDialogControl;
using vUICommon;

namespace vGridColumnGroupManager
{
    public class vGridColumnGroupManagerService
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "vGridColumnGroupManager";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "vGridColumnGroupManagerService";

        public event vGridColumnGroupManagerServiceClosingEventHandler vGridColumnGroupManagerServiceClosing;

        int _groupType;
        Guid? _gridGroupOwnerId;
        XamDialogWindow _xdw = null;
        ucv_GridColumnGroup _ucGp = null;
        XamGrid _grd = null;
        Guid _gridId;

        //GroupManager _mngr = null;

        #endregion Initialize Variables



        public vGridColumnGroupManagerService(XamGrid grd, Guid gridId, int groupType, Guid? gridGroupOwnerId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - vGridColumnGroupManagerService", IfxTraceCategory.Enter);
                _groupType = groupType;
                _gridGroupOwnerId = gridGroupOwnerId;
                _grd = grd;
                _gridId = gridId;
                LoadControl();
                

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - vGridColumnGroupManagerService", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - vGridColumnGroupManagerService", IfxTraceCategory.Leave);
            }
        }


        private void LoadControl()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadControl", IfxTraceCategory.Enter);

                //_xdw = new XamDialogWindow
                //{
                //    Header = "Manage Column Groups",
                //    Content = _ucGp,
                //    HeaderIconVisibility = Visibility.Collapsed,
                //    Height = 450,
                //    Width = 800
                //};
                //_xdw.WindowStateChanged += WindowStateChanged;
                
                _ucGp = new ucv_GridColumnGroup
                {
                    Grd = _grd,
                    GridId = _gridId,
                    GroupType = _groupType,
                    GridColumnGroupOwnerId = _gridGroupOwnerId
                };
                
                var attachmentDialog = new vDialog
                {
                    IsShowSizeInHeader = ApplicationLevelVariables.IsShowSizeInHeader,
                    MinHeight = 250,
                    Height = 450,
                    Width = 800,
                    Header = "Manage Column Groups",
                    Padding = new Thickness(5),
                    //HeaderHeight = _headerHeight,
                    //Padding = _thickness,
                    //HeaderForeground = _headerForeground,
                    Content = _ucGp
                };




                _ucGp.SetStateFromParent(null, _gridId, null, null, "");
                //_xdw.Content = _ucGp;
                //ApplicationLevelVariables.MainGrid.Children.Add(_xdw);
                //Grid.SetColumnSpan(_xdw, 3);
                //Grid.SetRowSpan(_xdw, 2);

                attachmentDialog.WindowStateChanged += dialog_WindowStateChanged;
                attachmentDialog.Show();
                

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadControl", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadControl", IfxTraceCategory.Leave);
            }
        }



        static void dialog_WindowStateChanged(object sender, WindowStateChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "dialog_WindowStateChanged", IfxTraceCategory.Enter);

                if (e.NewWindowState == Infragistics.Controls.Interactions.WindowState.Hidden)
                {
                    ((vDialog)sender).Content = null;
                    sender = null;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "dialog_WindowStateChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "dialog_WindowStateChanged", IfxTraceCategory.Leave);
            }
        }


        //void WindowStateChanged(object sender, WindowStateChangedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WindowStateChanged", IfxTraceCategory.Enter);

        //        if (e.NewWindowState == Infragistics.Controls.Interactions.WindowState.Hidden)
        //        {
        //            ApplicationLevelVariables.MainGrid.Children.Remove(_xdw);
        //            _xdw = null;

        //            vGridColumnGroupManagerServiceClosingArgs args = new vGridColumnGroupManagerServiceClosingArgs();
        //            vGridColumnGroupManagerServiceClosingEventHandler handler = vGridColumnGroupManagerServiceClosing;
        //            if (handler != null)
        //            {
        //                handler(this, args);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WindowStateChanged", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WindowStateChanged", IfxTraceCategory.Leave);
        //    }
        //}

        
        public XamDialogWindow Xdw
        {
            get { return _xdw; }
            set { _xdw = value; }
        }

        public int GroupType
        {
            get { return _groupType; }
            set { _groupType = value; }
        }

        public Guid? GridColumnGroupOwnerId
        {
            get { return _gridGroupOwnerId; }
            set { _gridGroupOwnerId = value; }
        }

        public XamGrid Grd
        {
            get { return _grd; }
            set { _grd = value; }
        }

        public Guid GridId
        {
            get { return _gridId; }
            set { _gridId = value; }
        }









    }




    #region vGridColumnGroupManagerServiceClosing


    public delegate void vGridColumnGroupManagerServiceClosingEventHandler(object sender, vGridColumnGroupManagerServiceClosingArgs e);

    public class vGridColumnGroupManagerServiceClosingArgs : EventArgs
    {
        
        public vGridColumnGroupManagerServiceClosingArgs()
        {

        }

    }
    
    #endregion vGridColumnGroupManagerServiceClosing




}
