﻿using EntityWireTypeSL;
using Ifx.SL;
using Infragistics.Controls.Grids;
using ProxyWrapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using TypeServices;
using Velocity.SL;
using vUICommon;

namespace vGridColumnGroupManager
{
    public partial class ucColumnAssingments : UserControl
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "vGridColumnGroupManager";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "ucColumnAssingments";


        vGridColumnGroupService_ProxyWrapper _proxy = null;

        XamGrid _xGrd = null;
        Guid? _groupId = null;

        #endregion Initialize Variables




        public ucColumnAssingments()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucColumnAssingments", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();
                _proxy = new vGridColumnGroupService_ProxyWrapper();
                _proxy.Getv_GridColumnGroupColumn_AssignedCompleted += Getv_GridColumnGroupColumn_AssignedCompleted;
                _proxy.Executev_GridColumnGroupColumn_AddCompleted += Executev_GridColumnGroupColumn_AddCompleted;
                _proxy.Executev_GridColumnGroupColumn_RemoveCompleted += Executev_GridColumnGroupColumn_RemoveCompleted;
                _proxy.v_GridColumnGroup_AssignColumnsCompleted += v_GridColumnGroup_AssignColumnsCompleted;
            

                btnRemove.Content = "<<Remove";
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucColumnAssingments", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucColumnAssingments", IfxTraceCategory.Leave);
            }
        }





        public void  LoadGrids(XamGrid grd, Guid? groupId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGrids", IfxTraceCategory.Enter);

                _xGrd = grd;
                _groupId = groupId;


                if (_groupId == null)
                {

                }
                else
                {
                    _proxy.Begin_Getv_GridColumnGroupColumn_Assigned((Guid)_groupId);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGrids", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGrids", IfxTraceCategory.Leave);
            }
        }
        

        private void btnAssign_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAssign_Click", IfxTraceCategory.Enter);
                //StringBuilder sb = new StringBuilder();
                object[] items = new object[gdAval.SelectionSettings.SelectedRows.Count];
                int idx = 0;
                foreach (Row rw in gdAval.SelectionSettings.SelectedRows)
                {
                    v_GridColumnGroupColumnList_Binding data = rw.Data as v_GridColumnGroupColumnList_Binding;
                    if (data != null)
                    {
                        //_proxy.Begin_Executev_GridColumnGroupColumn_Add(obj.v_GdColGrpCol_Id, obj.v_GdColGrpCol_GdGrp_Id, obj.v_GdColGrpCol_KeyName, obj.v_GdColGrpCol_HeaderText, (int)obj.v_GdColGrpCol_Index, (Guid)Credentials.UserId);
                        object[] obj = new object[5];
                        obj[0] = data.v_GdColGrpCol_Id;
                        obj[1] = data.v_GdColGrpCol_GdGrp_Id;
                        obj[2] = data.v_GdColGrpCol_KeyName;
                        obj[3] = data.v_GdColGrpCol_HeaderText;
                        obj[4] = data.v_GdColGrpCol_Index;
                        items[idx] = obj;
                        idx += 1;
                    }


                    //if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    //{
                    //    object[] obj = new object[5];
                    //    obj[0] = ((v_GridColumnGroupColumnList_Binding)rw.Data).v_GdColGrpCol_Id;
                    //    obj[1] = ((v_GridColumnGroupColumnList_Binding)rw.Data).v_GdColGrpCol_GdGrp_Id;
                    //    obj[2] = ((v_GridColumnGroupColumnList_Binding)rw.Data).v_GdColGrpCol_KeyName;
                    //    obj[3] = ((v_GridColumnGroupColumnList_Binding)rw.Data).v_GdColGrpCol_HeaderText;
                    //    obj[4] = ((v_GridColumnGroupColumnList_Binding)rw.Data).v_GdColGrpCol_Index;
                    //    items[idx] = obj;
                    //    idx += 1;
                    //}
                }

                if(items.Length>0)
                {
                    _proxy.Begin_v_GridColumnGroup_AssignColumns(items, (Guid)Credentials.UserId);
                    //_proxy.Begin_Executev_GridColumnGroupColumn_Add(items, (Guid)Credentials.UserId);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAssign_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAssign_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAssign_Click", IfxTraceCategory.Enter);
                StringBuilder sb = new StringBuilder();
                //object[] items = new object[gdAval.SelectionSettings.SelectedRows.Count];
                //int idx = 0;



                foreach (Row rw in gdSel.SelectionSettings.SelectedRows)
                {
                    if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        if (sb.Length == 0)
                        {
                            sb.Append("'" + ((v_GridColumnGroupColumnList_Binding)rw.Data).v_GdColGrpCol_Id.ToString() + "'");
                        }
                        else
                        {
                            sb.Append(", '" + ((v_GridColumnGroupColumnList_Binding)rw.Data).v_GdColGrpCol_Id.ToString() + "'");
                        }
                    }
                }

                if (sb.Length > 0)
                {
                    _proxy.Begin_Executev_GridColumnGroupColumn_Remove(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAssign_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAssign_Click", IfxTraceCategory.Leave);
            }
        }


        public void ClearGrids()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearGrids", IfxTraceCategory.Enter);

                gdAval.ItemsSource = null;
                _avail.Clear();
                gdAval.ItemsSource = _avail;

                gdSel.ItemsSource = null;
                _assigned.Clear();
                gdSel.ItemsSource = _assigned;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearGrids", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearGrids", IfxTraceCategory.Leave);
            }
        }


        #region Fetch

        List<v_GridColumnGroupColumnList_Binding> _assigned = new List<v_GridColumnGroupColumnList_Binding>();
        List<v_GridColumnGroupColumnList_Binding> _avail = new List<v_GridColumnGroupColumnList_Binding>();




        void v_GridColumnGroup_AssignColumnsCompleted(object sender, v_GridColumnGroup_AssignColumnsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_AssignColumnsCompleted", IfxTraceCategory.Enter);
                if (e.Result == 0)
                {
                    MessageBox.Show("There was a problem assigning these columns.  If this continues, please contect support.", "Error", MessageBoxButton.OK);
                }
                else
                {
                    _proxy.Begin_Getv_GridColumnGroupColumn_Assigned((Guid)_groupId);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_AssignColumnsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_AssignColumnsCompleted", IfxTraceCategory.Leave);
            }
        }


        void Getv_GridColumnGroupColumn_AssignedCompleted(object sender, Getv_GridColumnGroupColumn_AssignedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupColumn_AssignedCompleted", IfxTraceCategory.Enter);

                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                if (array != null)
                {

                    gdSel.ItemsSource = null;
                    _assigned.Clear();

                    for (int i = 0; i < array.GetUpperBound(0) + 1; i++)
                    {
                        object[] obj = (object[])array[i];
                        _assigned.Add(new v_GridColumnGroupColumnList_Binding(obj));
                    }

                    List<v_GridColumnGroupColumnList_Binding> asgnd = new List<v_GridColumnGroupColumnList_Binding>();
                    foreach (var col in _xGrd.Columns)
                    {
                        foreach (var item in _assigned)
                        {
                            if (col.Key == item.v_GdColGrpCol_KeyName)
                            {
                                asgnd.Add(item);
                                break;
                            }
                        }
                    }

                    gdSel.ItemsSource = asgnd;
                }
                else
                {
                    gdSel.ItemsSource = null;
                    _assigned.Clear();
                    gdSel.ItemsSource = _assigned;
                }

                _avail.Clear();

                if (_groupId != null)
                {
                    foreach (var col in _xGrd.Columns)
                    {
                        // If this is a special column such as a dynamic matrix column, then dont add it to the list 
                        //   as users should not add these to user defined groups.
                        IGridDynamicColumn test = col.Tag as IGridDynamicColumn;
                        if (test == null)
                        {

                            if (col is Column)
                            {
                                _avail.Add(new v_GridColumnGroupColumnList_Binding(Guid.NewGuid(), (Guid) _groupId,
                                    col.Key, col.HeaderText, gdAval.Columns.IndexOf(col)));
                            }
                        }



                    }
                }

                foreach (v_GridColumnGroupColumnList_Binding asgn in _assigned)
                {
                    for (int i = _avail.Count-1; i > -1; i--)
                    {
                        if (_avail[i].v_GdColGrpCol_KeyName == asgn.v_GdColGrpCol_KeyName)
                        {
                            _avail.Remove(_avail[i]);
                        }
                    }
                }
                gdAval.ItemsSource = null;
                gdAval.ItemsSource = _avail;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupColumn_AssignedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupColumn_AssignedCompleted", IfxTraceCategory.Leave);
            }
        }


        void Executev_GridColumnGroupColumn_AddCompleted(object sender, Executev_GridColumnGroupColumn_AddCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupColumn_AddCompleted", IfxTraceCategory.Enter);
                object[] result = e.Result;
                int? success = result[1] as int?;

                if (success == null || success == 0)
                {
                    MessageBox.Show("There was a problem assigning these columns.  If this continues, please contect support.", "Error", MessageBoxButton.OK);
                }
                else
                {
                    _proxy.Begin_Getv_GridColumnGroupColumn_Assigned((Guid)_groupId);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupColumn_AddCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupColumn_AddCompleted", IfxTraceCategory.Leave);
            }
        }





        void Executev_GridColumnGroupColumn_RemoveCompleted(object sender, Executev_GridColumnGroupColumn_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupColumn_RemoveCompleted", IfxTraceCategory.Enter);
                object[] result = e.Result;
                int? success = result[0] as int?;

                if (success == null || success == 0)
                {
                    MessageBox.Show("There was a problem removing these columns.  If this continues, please contect support.", "Error", MessageBoxButton.OK);
                }
                else
                {
                    _proxy.Begin_Getv_GridColumnGroupColumn_Assigned((Guid)_groupId);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupColumn_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_GridColumnGroupColumn_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }


        #endregion Fetch





    }
}
