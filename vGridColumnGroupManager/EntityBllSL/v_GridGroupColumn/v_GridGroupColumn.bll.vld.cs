using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  11/22/2014 2:03:13 PM

namespace EntityBll.SL
{
    public partial class v_GridGroupColumn_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_v_GdGrpCol_KeyName = 100;
		public const int STRINGSIZE_v_GdGrpCol_HeaderText = 200;
		private const string BROKENRULE_v_GdGrpCol_Id_Required = "GdGrpCol_Id is a required field.";
		private const string BROKENRULE_v_GdGrpCol_GdGrp_Id_Required = "GdGrpCol_GdGrp_Id is a required field.";
		private string BROKENRULE_v_GdGrpCol_KeyName_TextLength = "Column key name has a maximum text length of  '" + STRINGSIZE_v_GdGrpCol_KeyName + "'.";
		private const string BROKENRULE_v_GdGrpCol_KeyName_Required = "Column key name is a required field.";
		private string BROKENRULE_v_GdGrpCol_HeaderText_TextLength = "Column name has a maximum text length of  '" + STRINGSIZE_v_GdGrpCol_HeaderText + "'.";
		private const string BROKENRULE_v_GdGrpCol_HeaderText_Required = "Column name is a required field.";
		private const string BROKENRULE_v_GdGrpCol_Stamp_Required = "GdGrpCol_Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_Id", BROKENRULE_v_GdGrpCol_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_GdGrp_Id", BROKENRULE_v_GdGrpCol_GdGrp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_KeyName", BROKENRULE_v_GdGrpCol_KeyName_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_KeyName", BROKENRULE_v_GdGrpCol_KeyName_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_HeaderText", BROKENRULE_v_GdGrpCol_HeaderText_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_HeaderText", BROKENRULE_v_GdGrpCol_HeaderText_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_Stamp", BROKENRULE_v_GdGrpCol_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void v_GdGrpCol_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdGrpCol_Id");
                string newBrokenRules = "";
                
                if (v_GdGrpCol_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_Id", BROKENRULE_v_GdGrpCol_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdGrpCol_Id", BROKENRULE_v_GdGrpCol_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdGrpCol_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdGrpCol_Id", _brokenRuleManager.IsPropertyValid("v_GdGrpCol_Id"), IsPropertyDirty("v_GdGrpCol_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void v_GdGrpCol_GdGrp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_GdGrp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdGrpCol_GdGrp_Id");
                string newBrokenRules = "";
                
                if (v_GdGrpCol_GdGrp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_GdGrp_Id", BROKENRULE_v_GdGrpCol_GdGrp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdGrpCol_GdGrp_Id", BROKENRULE_v_GdGrpCol_GdGrp_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdGrpCol_GdGrp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdGrpCol_GdGrp_Id", _brokenRuleManager.IsPropertyValid("v_GdGrpCol_GdGrp_Id"), IsPropertyDirty("v_GdGrpCol_GdGrp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_GdGrp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_GdGrp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void v_GdGrpCol_KeyName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_KeyName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdGrpCol_KeyName");
                string newBrokenRules = "";
                				int len = 0;
                if (v_GdGrpCol_KeyName != null)
                {
                    len = v_GdGrpCol_KeyName.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_KeyName", BROKENRULE_v_GdGrpCol_KeyName_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdGrpCol_KeyName", BROKENRULE_v_GdGrpCol_KeyName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdGrpCol_KeyName", BROKENRULE_v_GdGrpCol_KeyName_Required);

                    if (len > STRINGSIZE_v_GdGrpCol_KeyName)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_KeyName", BROKENRULE_v_GdGrpCol_KeyName_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("v_GdGrpCol_KeyName", BROKENRULE_v_GdGrpCol_KeyName_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdGrpCol_KeyName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdGrpCol_KeyName", _brokenRuleManager.IsPropertyValid("v_GdGrpCol_KeyName"), IsPropertyDirty("v_GdGrpCol_KeyName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_KeyName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_KeyName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void v_GdGrpCol_HeaderText_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_HeaderText_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdGrpCol_HeaderText");
                string newBrokenRules = "";
                				int len = 0;
                if (v_GdGrpCol_HeaderText != null)
                {
                    len = v_GdGrpCol_HeaderText.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_HeaderText", BROKENRULE_v_GdGrpCol_HeaderText_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdGrpCol_HeaderText", BROKENRULE_v_GdGrpCol_HeaderText_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdGrpCol_HeaderText", BROKENRULE_v_GdGrpCol_HeaderText_Required);

                    if (len > STRINGSIZE_v_GdGrpCol_HeaderText)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_HeaderText", BROKENRULE_v_GdGrpCol_HeaderText_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("v_GdGrpCol_HeaderText", BROKENRULE_v_GdGrpCol_HeaderText_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdGrpCol_HeaderText");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdGrpCol_HeaderText", _brokenRuleManager.IsPropertyValid("v_GdGrpCol_HeaderText"), IsPropertyDirty("v_GdGrpCol_HeaderText"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_HeaderText_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_HeaderText_Validate", IfxTraceCategory.Leave);
            }
        }

        private void v_GdGrpCol_CreatedUserId_Validate()
        {
        }

        private void v_GdGrpCol_CreatedDate_Validate()
        {
        }

        private void v_GdGrpCol_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdGrpCol_Stamp");
                string newBrokenRules = "";
                
                if (v_GdGrpCol_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdGrpCol_Stamp", BROKENRULE_v_GdGrpCol_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdGrpCol_Stamp", BROKENRULE_v_GdGrpCol_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdGrpCol_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdGrpCol_Stamp", _brokenRuleManager.IsPropertyValid("v_GdGrpCol_Stamp"), IsPropertyDirty("v_GdGrpCol_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdGrpCol_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


