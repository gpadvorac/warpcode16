using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;

// Gen Timestamp:  12/11/2014 12:51:09 AM

namespace EntityBll.SL
{

    public partial class v_GridColumnGroup_Bll_staticLists
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "vGridColumnGroupManager";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "v_GridColumnGroup_Bll_staticLists";



        private static ComboItemList _v_GridColumnGroupType_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _v_GridColumnGroupType_ComboItemList_BindingList_HasItems = true;

        static bool isDataLoaded = false;
        static ProxyWrapper.vGridColumnGroupService_ProxyWrapper _staticv_GridColumnGroupProxy = null;

        #endregion Initialize Variables


        #region Load Data

        public static void InitializeProxyWrapper()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Enter);

                if (_staticv_GridColumnGroupProxy == null)
                {
                    _staticv_GridColumnGroupProxy = new ProxyWrapper.vGridColumnGroupService_ProxyWrapper();
                    _staticv_GridColumnGroupProxy.Getv_GridColumnGroup_ReadOnlyStaticListsCompleted += new EventHandler<Getv_GridColumnGroup_ReadOnlyStaticListsCompletedEventArgs>(Getv_GridColumnGroup_ReadOnlyStaticListsCompleted);
                    _staticv_GridColumnGroupProxy.Getv_GridColumnGroupType_ComboItemListCompleted += new EventHandler<Getv_GridColumnGroupType_ComboItemListCompletedEventArgs>(Getv_GridColumnGroupType_ComboItemListCompleted);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Leave);
            }
        }

        public static void LoadStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Enter);

                if (_staticv_GridColumnGroupProxy == null)
                {
                    InitializeProxyWrapper();
                }

                _staticv_GridColumnGroupProxy.Begin_Getv_GridColumnGroup_ReadOnlyStaticLists();

                LoadStaticLists_Custom();
                isDataLoaded = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Leave);
            }
        }

        static void Getv_GridColumnGroup_ReadOnlyStaticListsCompleted(object sender, Getv_GridColumnGroup_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroup_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                if (data != null)
                {


                    // v_GridColumnGroupType_ComboItemList
                    _v_GridColumnGroupType_ComboItemList_BindingList.IsRefreshingData = true;
                    _v_GridColumnGroupType_ComboItemList_BindingList.CachedList.Clear();
                    _v_GridColumnGroupType_ComboItemList_BindingList.Clear();
                    _v_GridColumnGroupType_ComboItemList_BindingList.ReplaceList((object[])data[0]);

                    _v_GridColumnGroupType_ComboItemList_BindingList.IsRefreshingData = false;
                    isDataLoaded = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroup_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroup_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Load Data


        #region Properties

        public static bool IsDataLoaded
        {
            get { return isDataLoaded; }
        }

                    // v_GridColumnGroupType_ComboItemList
        public static ComboItemList v_GridColumnGroupType_ComboItemList_BindingListProperty
        {
            get
            {
                return _v_GridColumnGroupType_ComboItemList_BindingList;
            }
            set
            {
                _v_GridColumnGroupType_ComboItemList_BindingList = value;
            }
        }

        #endregion Properties


        #region RefreshLists



        #region v_GridColumnGroupType_ComboItemList

        public static void Refresh_v_GridColumnGroupType_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_v_GridColumnGroupType_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticv_GridColumnGroupProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticv_GridColumnGroupProxy.Begin_Getv_GridColumnGroupType_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_v_GridColumnGroupType_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_v_GridColumnGroupType_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void Getv_GridColumnGroupType_ComboItemListCompleted(object sender, Getv_GridColumnGroupType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupType_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _v_GridColumnGroupType_ComboItemList_BindingList.IsRefreshingData = true;
                _v_GridColumnGroupType_ComboItemList_BindingList.ReplaceList(data);
                _v_GridColumnGroupType_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupType_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_GridColumnGroupType_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion v_GridColumnGroupType_ComboItemList



        #endregion RefreshLists


    }

    #region Classes for Binding

#endregion Classes for Binding

}

