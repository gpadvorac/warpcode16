using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/19/2015 11:49:05 PM

namespace EntityBll.SL
{
    public partial class v_GridColumnGroup_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_v_GdColGrp_SysName = 50;
		public const int STRINGSIZE_v_GdColGrp_Name = 50;
		public const int STRINGSIZE_v_GdColGrp_Desc = 300;
		private const string BROKENRULE_v_GdColGrp_Id_Required = "GdGrp_Id is a required field.";
		private const string BROKENRULE_v_GdColGrp_Gd_Id_Required = "GdGrp_Gd_Id is a required field.";
		private const string BROKENRULE_v_GdColGrp_GdColGrpTp_Id_Required = "Group type is a required field.";
		private const string BROKENRULE_v_GdColGrp_GdColGrpTp_Id_ZeroNotAllowed = "Group type:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_v_GdColGrp_SortOrder_ZeroNotAllowed = "Sort:  '0'  (zero) is not allowed.";
		private string BROKENRULE_v_GdColGrp_SysName_TextLength = "System Name has a maximum text length of  '" + STRINGSIZE_v_GdColGrp_SysName + "'.";
		private const string BROKENRULE_v_GdColGrp_SysName_Required = "System Name is a required field.";
		private string BROKENRULE_v_GdColGrp_Name_TextLength = "Group Name has a maximum text length of  '" + STRINGSIZE_v_GdColGrp_Name + "'.";
		private const string BROKENRULE_v_GdColGrp_Name_Required = "Group Name is a required field.";
		private string BROKENRULE_v_GdColGrp_Desc_TextLength = "Group Description has a maximum text length of  '" + STRINGSIZE_v_GdColGrp_Desc + "'.";
		private const string BROKENRULE_v_GdColGrp_IsDefault_Required = "Default is a required field.";
		private const string BROKENRULE_v_GdColGrp_Stamp_Required = "GdGrp_Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_Id", BROKENRULE_v_GdColGrp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_Gd_Id", BROKENRULE_v_GdColGrp_Gd_Id_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_GdColGrpTp_Id", BROKENRULE_v_GdColGrp_GdColGrpTp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_GdColGrpTp_Id", BROKENRULE_v_GdColGrp_GdColGrpTp_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_SortOrder", BROKENRULE_v_GdColGrp_SortOrder_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_SysName", BROKENRULE_v_GdColGrp_SysName_TextLength);
                //_brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_SysName", BROKENRULE_v_GdColGrp_SysName_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_Name", BROKENRULE_v_GdColGrp_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_Name", BROKENRULE_v_GdColGrp_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_Desc", BROKENRULE_v_GdColGrp_Desc_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_IsDefault", BROKENRULE_v_GdColGrp_IsDefault_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_Stamp", BROKENRULE_v_GdColGrp_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void v_GdColGrp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_Id");
                string newBrokenRules = "";
                
                if (v_GdColGrp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_Id", BROKENRULE_v_GdColGrp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_Id", BROKENRULE_v_GdColGrp_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdColGrp_Id", _brokenRuleManager.IsPropertyValid("v_GdColGrp_Id"), IsPropertyDirty("v_GdColGrp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void v_GdColGrp_Gd_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Gd_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_Gd_Id");
                string newBrokenRules = "";
                
                if (v_GdColGrp_Gd_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_Gd_Id", BROKENRULE_v_GdColGrp_Gd_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_Gd_Id", BROKENRULE_v_GdColGrp_Gd_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_Gd_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdColGrp_Gd_Id", _brokenRuleManager.IsPropertyValid("v_GdColGrp_Gd_Id"), IsPropertyDirty("v_GdColGrp_Gd_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Gd_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Gd_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void v_GdColGrp_GdColGrpTp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_GdColGrpTp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_GdColGrpTp_Id");
                string newBrokenRules = "";
                
                if (v_GdColGrp_GdColGrpTp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_GdColGrpTp_Id", BROKENRULE_v_GdColGrp_GdColGrpTp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_GdColGrpTp_Id", BROKENRULE_v_GdColGrp_GdColGrpTp_Id_Required);
                }

                if (v_GdColGrp_GdColGrpTp_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_GdColGrpTp_Id", BROKENRULE_v_GdColGrp_GdColGrpTp_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_GdColGrpTp_Id", BROKENRULE_v_GdColGrp_GdColGrpTp_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_GdColGrpTp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdColGrp_GdColGrpTp_Id", _brokenRuleManager.IsPropertyValid("v_GdColGrp_GdColGrpTp_Id"), IsPropertyDirty("v_GdColGrp_GdColGrpTp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_GdColGrpTp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_GdColGrpTp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void v_GdColGrp_SortOrder_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_SortOrder_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_SortOrder");
                string newBrokenRules = "";
                
                if (v_GdColGrp_SortOrder == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_SortOrder", BROKENRULE_v_GdColGrp_SortOrder_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_SortOrder", BROKENRULE_v_GdColGrp_SortOrder_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_SortOrder");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdColGrp_SortOrder", _brokenRuleManager.IsPropertyValid("v_GdColGrp_SortOrder"), IsPropertyDirty("v_GdColGrp_SortOrder"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_SortOrder_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_SortOrder_Validate", IfxTraceCategory.Leave);
            }
        }

        private void v_GdColGrp_SysName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_SysName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_SysName");
                string newBrokenRules = "";
                				int len = 0;
                if (v_GdColGrp_SysName != null)
                {
                    len = v_GdColGrp_SysName.Length;
                }

                if (len == 0)
                {
                    //_brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_SysName", BROKENRULE_v_GdColGrp_SysName_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_SysName", BROKENRULE_v_GdColGrp_SysName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_SysName", BROKENRULE_v_GdColGrp_SysName_Required);

                    if (len > STRINGSIZE_v_GdColGrp_SysName)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_SysName", BROKENRULE_v_GdColGrp_SysName_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_SysName", BROKENRULE_v_GdColGrp_SysName_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_SysName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdColGrp_SysName", _brokenRuleManager.IsPropertyValid("v_GdColGrp_SysName"), IsPropertyDirty("v_GdColGrp_SysName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_SysName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_SysName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void v_GdColGrp_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (v_GdColGrp_Name != null)
                {
                    len = v_GdColGrp_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_Name", BROKENRULE_v_GdColGrp_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_Name", BROKENRULE_v_GdColGrp_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_Name", BROKENRULE_v_GdColGrp_Name_Required);

                    if (len > STRINGSIZE_v_GdColGrp_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_Name", BROKENRULE_v_GdColGrp_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_Name", BROKENRULE_v_GdColGrp_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdColGrp_Name", _brokenRuleManager.IsPropertyValid("v_GdColGrp_Name"), IsPropertyDirty("v_GdColGrp_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void v_GdColGrp_Desc_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Desc_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_Desc");
                string newBrokenRules = "";
                				int len = 0;
                if (v_GdColGrp_Desc != null)
                {
                    len = v_GdColGrp_Desc.Length;
                }

                if (len > STRINGSIZE_v_GdColGrp_Desc)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_Desc", BROKENRULE_v_GdColGrp_Desc_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_Desc", BROKENRULE_v_GdColGrp_Desc_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_Desc");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdColGrp_Desc", _brokenRuleManager.IsPropertyValid("v_GdColGrp_Desc"), IsPropertyDirty("v_GdColGrp_Desc"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Desc_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Desc_Validate", IfxTraceCategory.Leave);
            }
        }

        private void v_GdColGrp_IsDefault_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_IsDefault_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_IsDefault");
                string newBrokenRules = "";
                
                if (v_GdColGrp_IsDefault == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_IsDefault", BROKENRULE_v_GdColGrp_IsDefault_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_IsDefault", BROKENRULE_v_GdColGrp_IsDefault_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_IsDefault");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdColGrp_IsDefault", _brokenRuleManager.IsPropertyValid("v_GdColGrp_IsDefault"), IsPropertyDirty("v_GdColGrp_IsDefault"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_IsDefault_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_IsDefault_Validate", IfxTraceCategory.Leave);
            }
        }

        private void v_GdColGrp_OwnerId_Validate()
        {
        }

        private void v_GdColGrp_CreatedUserId_Validate()
        {
        }

        private void v_GdColGrp_CreatedDate_Validate()
        {
        }

        private void v_GdColGrp_UserId_Validate()
        {
        }

        private void v_GdColGrp_LastModifiedDate_Validate()
        {
        }

        private void v_GdColGrp_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_Stamp");
                string newBrokenRules = "";
                
                if (v_GdColGrp_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("v_GdColGrp_Stamp", BROKENRULE_v_GdColGrp_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("v_GdColGrp_Stamp", BROKENRULE_v_GdColGrp_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("v_GdColGrp_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("v_GdColGrp_Stamp", _brokenRuleManager.IsPropertyValid("v_GdColGrp_Stamp"), IsPropertyDirty("v_GdColGrp_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GdColGrp_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


