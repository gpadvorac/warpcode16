﻿using System;
using System.Windows;
using System.Windows.Controls;
using Infragistics.Controls.Grids;
using System.Windows.Input;
using Infragistics.Controls.Editors;
namespace vControls
{


    #region vTextColumnTextChangedEventArgs Text Changed Event

    public class vTextColumnTextChangedEventArgs : EventArgs
    {
        Cell _cell;
        int _index;
        string _key;
        object _data;
        TextChangedEventArgs _textChangedArgs;

        internal vTextColumnTextChangedEventArgs(Cell cell, int index, string key, object data, TextChangedEventArgs e)
        {
            _cell = cell;
            _index = index;
            _key = key;
            _data = data;
            _textChangedArgs = e;
        }

        public Cell ContentCell { get { return _cell; } }
        public int Index { get { return _index; } }
        public string Key { get { return _key; } }
        public object Data { get { return _data; } }
        public TextChangedEventArgs TextChangedArgs { get { return _textChangedArgs; } }

    }

    #endregion vTextColumnTextChangedEventArgs Text Changed Event



    #region vDecimalColumnTextChangedEventArgs Text Changed Event

    public class vDecimalColumnTextChangedEventArgs : EventArgs
    {
        Cell _cell;
        int _index;
        string _key;
        object _data;
        TextChangedEventArgs _textChangedArgs;

        internal vDecimalColumnTextChangedEventArgs(Cell cell, int index, string key, object data, TextChangedEventArgs e)
        {
            _cell = cell;
            _index = index;
            _key = key;
            _data = data;
            _textChangedArgs = e;
        }

        public Cell ContentCell { get { return _cell; } }
        public int Index { get { return _index; } }
        public string Key { get { return _key; } }
        public object Data { get { return _data; } }
        public TextChangedEventArgs TextChangedArgs { get { return _textChangedArgs; } }

    }

    #endregion vDecimalColumnTextChangedEventArgs Text Changed Event




    #region vIntegerColumnTextChangedEventArgs Text Changed Event

    public class vIntegerColumnTextChangedEventArgs : EventArgs
    {
        Cell _cell;
        int _index;
        string _key;
        object _data;
        TextChangedEventArgs _textChangedArgs;

        internal vIntegerColumnTextChangedEventArgs(Cell cell, int index, string key, object data, TextChangedEventArgs e)
        {
            _cell = cell;
            _index = index;
            _key = key;
            _data = data;
            _textChangedArgs = e;
        }

        public Cell ContentCell { get { return _cell; } }
        public int Index { get { return _index; } }
        public string Key { get { return _key; } }
        public object Data { get { return _data; } }
        public TextChangedEventArgs TextChangedArgs { get { return _textChangedArgs; } }

    }

    #endregion vIntegerColumnTextChangedEventArgs Text Changed Event





    #region vCheckColumnCheckedChangedEventArgs Checked Changed Event

    public class vCheckColumnCheckedChangedEventArgs : EventArgs
    {
        Cell _cell;
        int _index;
        string _key;
        object _data;
        RoutedEventArgs _checkedChangedArgs;

        internal vCheckColumnCheckedChangedEventArgs(Cell cell, int index, string key, object data, RoutedEventArgs e)
        {
            _cell = cell;
            _index = index;
            _key = key;
            _data = data;
            _checkedChangedArgs = e;
        }

        public Cell ContentCell { get { return _cell; } }
        public int Index { get { return _index; } }
        public string Key { get { return _key; } }
        public object Data { get { return _data; } }
        public RoutedEventArgs CheckedChangedArgs { get { return _checkedChangedArgs; } }

    }

    #endregion vCheckColumnCheckedChangedEventArgs Checked Changed Event


    #region vDatePickerColumnTextChangedEventArgs Text Changed Event

    public class vDatePickerColumnTextChangedEventArgs : EventArgs
    {
        Cell _cell;
        int _index;
        string _key;
        object _data;
        TextChangedEventArgs _textChangedArgs;

        internal vDatePickerColumnTextChangedEventArgs(Cell cell, int index, string key, object data, TextChangedEventArgs e)
        {
            _cell = cell;
            _index = index;
            _key = key;
            _data = data;
            _textChangedArgs = e;
        }

        public Cell ContentCell { get { return _cell; } }
        public int Index { get { return _index; } }
        public string Key { get { return _key; } }
        public object Data { get { return _data; } }
        public TextChangedEventArgs TextChangedArgs { get { return _textChangedArgs; } }

    }

    #endregion vDatePickerColumnTextChangedEventArgs Text Changed Event


    #region vXamComboColumnSelectionChangedEventArgs Text Changed Event

    public class vXamComboColumnSelectionChangedEventArgs : EventArgs
    {
        Cell _cell;
        int _index;
        string _key;
        object _data;
        EventArgs _selectionChangedArgs;

        internal vXamComboColumnSelectionChangedEventArgs(Cell cell, int index, string key, object data, EventArgs e)
        {
            _cell = cell;
            _index = index;
            _key = key;
            _data = data;
            _selectionChangedArgs = e;
        }

        public Cell ContentCell { get { return _cell; } }
        public int Index { get { return _index; } }
        public string Key { get { return _key; } }
        public object Data { get { return _data; } }
        public EventArgs SelectionChangedArgs { get { return _selectionChangedArgs; } }

    }

    #endregion vXamComboColumnSelectionChangedEventArgs Text Changed Event



    #region vXamMultiColumnComboColumnSelectionChangedEventArgs Text Changed Event

    public class vXamMultiColumnComboColumnSelectionChangedEventArgs : EventArgs
    {
        Cell _cell;
        int _index;
        string _key;
        object _data;
        EventArgs _selectionChangedArgs;

        internal vXamMultiColumnComboColumnSelectionChangedEventArgs(Cell cell, int index, string key, object data, EventArgs e)
        {
            _cell = cell;
            _index = index;
            _key = key;
            _data = data;
            _selectionChangedArgs = e;
        }

        public Cell ContentCell { get { return _cell; } }
        public int Index { get { return _index; } }
        public string Key { get { return _key; } }
        public object Data { get { return _data; } }
        public EventArgs SelectionChangedArgs { get { return _selectionChangedArgs; } }

    }

    #endregion vXamMultiColumnComboColumnSelectionChangedEventArgs Text Changed Event



    #region vXamComboColumnKeyEventArgs Key Event


    public class vXamComboColumnKeyEventArgs : EventArgs
    {
        Cell _cell;
        int _index;
        string _columnKey;
        object _data;
        KeyEventArgs _keyEventArgs;

        internal vXamComboColumnKeyEventArgs(Cell cell, int index, string key, object data, KeyEventArgs e)
        {
            _cell = cell;
            _index = index;
            _columnKey = key;
            _data = data;
            _keyEventArgs = e;
        }

        public Cell ContentCell { get { return _cell; } }
        public int Index { get { return _index; } }
        public string ColumnKey { get { return _columnKey; } }
        public object Data { get { return _data; } }
        public KeyEventArgs XamComboColumnKeyEventArgs { get { return _keyEventArgs; } }

    }


    #endregion vXamComboColumnKeyEventArgs Key Event



    #region vXamComboColumnKeyEventArgs Key Event


    public class vXamComboColumnItemAddingEventArgs : EventArgs
    {
        vXamComboEditor _comboEditor;
        Cell _cell;
        int _index;
        string _columnKey;
        object _data;
        ComboItemAddingEventArgs<ComboEditorItem> _args;

        internal vXamComboColumnItemAddingEventArgs(vXamComboEditor comboEditor, Cell cell, int index, string key, object data, ComboItemAddingEventArgs<ComboEditorItem> e)
        {
            _comboEditor = comboEditor;
            _cell = cell;
            _index = index;
            _columnKey = key;
            _data = data;
            _args = e;
        }

        public vXamComboEditor ComboEditor { get { return _comboEditor; } }
        public Cell ContentCell { get { return _cell; } }
        public int Index { get { return _index; } }
        public string ColumnKey { get { return _columnKey; } }
        public object Data { get { return _data; } }
        public ComboItemAddingEventArgs<ComboEditorItem> XamComboColumnItemAddingEventArgs { get { return _args; } }

    }


    #endregion vXamComboColumnItemAddingEventArgs Key Event



}
