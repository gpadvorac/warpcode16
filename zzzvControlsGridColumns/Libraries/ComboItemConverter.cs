﻿using System;
using System.Windows.Data;
using vComboDataTypes;
using System.Linq;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace vControls
{
    public class ComboItemConverter : IValueConverter
    {


        #region IValueConverter Members
      
        ObservableCollection<ComboItem> itemList;

        public ComboItemConverter(ObservableCollection<ComboItem> source)
        {
            try
            {
                itemList = source;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
            }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
            if (value == null)
                return null;
            var s = (from i in itemList
                     where i.Id.ToString() == value.ToString()
                     select i).FirstOrDefault();

            return s;
            //hrow new NotImplementedException();
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value == null)
                    return null;
                
                ComboItem ci = value as ComboItem;
                if (ci == null) { return null; }

            var s = (from i in itemList
                     where i.Id == ci.Id
                     select i.Id).FirstOrDefault();

            return s;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                return null;
            }
        }

        #endregion



    }
}
