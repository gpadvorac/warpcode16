﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Infragistics.Controls.Grids.Primitives;
using vTooltipProvider;
using TypeServices;

namespace vControls
{
    /// <summary>
    /// Since we can't derive directly from TextBlock we have to use Grid.  This custom grid is used to create
    /// the tooltip on the header's TextBlock control.
    /// </summary>
    public class vColumnHeaderGrid : Grid
    {
        public vColumnHeaderGrid()
        {
            try
            {
                // handle the loaded event.
                Loaded += new RoutedEventHandler(vColumnHeaderGrid_Loaded);
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// Here is where we will attach the tooltip to the textblock.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void vColumnHeaderGrid_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                // Travel up the visual tree starting at ourselves.
                HeaderCellControl header = FindHeader(this);
                if (header != null)
                {
                    this.DataContext = header.DataContext;

                    // cast the header's column to the custom text column.
                    IvColumn customColumn = header.Column as IvColumn;
                    
                    if (customColumn != null && customColumn.HeaderTooltipContent != null)
                    {
                        //if (customColumn.HeaderTooltipContent.ItemsSource != null)
                        //{
                        //    customColumn.HeaderTooltipContent.ToolTipCaption = customColumn.HeaderToolTipCaption;
                        //    customColumn.HeaderTooltipContent.ItemsSource = customColumn.HeaderToolTipItemsSource;
                        //    vToolTipHelper.BindBusinessRuleToolTipToElement(customColumn.HeaderTooltipContent, ((Infragistics.Controls.Grids.Primitives.HeaderCellControl)this.Parent));

                        //}
                        //else if (customColumn.HeaderToolTipStringArray != null)
                        if (customColumn.HeaderToolTipStringArray!=null)
                        {
                            List<vRuleItem> toolTipLines = new List<vRuleItem>();
                            for (int i = 0; i < customColumn.HeaderToolTipStringArray.Length; i++)
                            {
                                toolTipLines.Add(new vRuleItem(customColumn.HeaderToolTipStringArray[i]));
                            }
                            customColumn.HeaderTooltipContent.ToolTipCaption = customColumn.HeaderToolTipCaption;
                            customColumn.HeaderTooltipContent.ItemsSource = toolTipLines;
                            vToolTipHelper.BindBusinessRuleToolTipToElement(customColumn.HeaderTooltipContent, ((Infragistics.Controls.Grids.Primitives.HeaderCellControl)this.Parent));


                        }
                        else if (customColumn.HeaderToolTipCaption != null)
                        {
                            customColumn.HeaderTooltipContent.ToolTipCaption = customColumn.HeaderToolTipCaption;
                            vToolTipHelper.BindBusinessRuleToolTipToElement(customColumn.HeaderTooltipContent, ((Infragistics.Controls.Grids.Primitives.HeaderCellControl)this.Parent));
                        }
                    }

                }
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// Travels up the visual tree looking for the column header.
        /// </summary>
        /// <param name="visualTreeNode"></param>
        /// <returns></returns>
        HeaderCellControl FindHeader(DependencyObject visualTreeNode)
        {
            try
            {
                // Travel up the parent until we find the header.
                while (visualTreeNode != null)
                {
                    visualTreeNode = VisualTreeHelper.GetParent(visualTreeNode);

                    if (visualTreeNode is HeaderCellControl)
                        return visualTreeNode as HeaderCellControl;
                }

                return null;
            }
            catch (Exception ex)
            { return null; }
        }


    }
}