﻿using vTooltipProvider;

namespace vControls
{

    public interface IvColumn
    {


        string HeaderToolTipCaption { get; set; }
        string[] HeaderToolTipStringArray { get; set; }
        System.Collections.IEnumerable HeaderToolTipItemsSource { get; set; }
        IvTooltipContent HeaderTooltipContent { get; set; }


    }

}
