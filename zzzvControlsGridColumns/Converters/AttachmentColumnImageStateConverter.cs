﻿using System;
using System.Windows;
using System.Windows.Data;
namespace vControls
{

    public class AttachmentColumnImageStateConverter : IValueConverter
    {

        /// <summary>
        /// This is the oposite of 'AttachmentColumnTextBlockStateConverter'
        /// When the mouse moves over the row, the cell's Image will be Visible, otherwise, it will be Collapsed
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                if (value.ToString().Contains("VISIBLE"))
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }

}
