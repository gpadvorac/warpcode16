﻿
using System;
using System.Windows;
using System.Windows.Data;
namespace vControls
{

    public class AttachmentColumnImageTypeConverter : IValueConverter
    {



        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                ISpecialtyFields obj = value as ISpecialtyFields;
                if (obj != null)
                {
                    if (obj.AttachmentCount > 0)
                    {
                        return "/vControlsGridColumns;component/Images/AttachmentEdit.png";
                    }
                    else
                    {
                        return "/vControlsGridColumns;component/Images/AttachmentAdd.png";
                    }
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }

}
