﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace vControls
{
    public class vComboColumnBaseNameToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((value is vXamComboColumn || value is vXamMultiColumnComboColumn) && parameter.Equals("show"))
            {
                return Visibility.Visible;
            }

            if (!(value is vXamComboColumn) && !(value is vXamMultiColumnComboColumn) && parameter.Equals("other"))
            {
                return Visibility.Visible;
            } 

            return Visibility.Collapsed;

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
