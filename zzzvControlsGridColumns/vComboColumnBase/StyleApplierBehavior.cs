﻿using Infragistics.Controls.Grids.Primitives;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace vControls
{
    public class StyleApplierBehavior : Behavior<ContentPresenter>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Loaded += AssociatedObject_Opened;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.Loaded -= AssociatedObject_Opened;
        }

        void AssociatedObject_Opened(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {

                FilterSelectorControl selectionControl = AssociatedObject.Content as FilterSelectorControl;
                if (selectionControl != null && !selectionControl.Resources.Contains(typeof(ListBoxItem)))
                    selectionControl.Resources.Add(typeof(ListBoxItem), StyleToApply);

            }));
        }

        public Style StyleToApply { get; set; }
    }
}
