﻿using Infragistics.Controls.Grids;
using Infragistics.Controls.Grids.Primitives;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace vControls
{
    public abstract class vComboColumnBase :  EditableColumn, IvColumn
    {
        #region EditableColumn Members

        protected override Infragistics.Controls.Grids.Primitives.ColumnContentProviderBase GenerateContentProvider()
        {
            throw new NotImplementedException("Should be implemented in the derived classes");
        }

        #endregion

        #region IvColumn Memebers

        public virtual string HeaderToolTipCaption
        {
            get
            {
                throw new NotImplementedException("Should be implemented in the derived classes");
            }
            set
            {
                throw new NotImplementedException("Should be implemented in the derived classes");
            }
        }

        public virtual string[] HeaderToolTipStringArray
        {
            get
            {
                throw new NotImplementedException("Should be implemented in the derived classes");
            }
            set
            {
                throw new NotImplementedException("Should be implemented in the derived classes");
            }
        }

        public virtual System.Collections.IEnumerable HeaderToolTipItemsSource
        {
            get
            {
                throw new NotImplementedException("Should be implemented in the derived classes");
            }
            set
            {
                throw new NotImplementedException("Should be implemented in the derived classes");
            }
        }

        public virtual vTooltipProvider.IvTooltipContent HeaderTooltipContent
        {
            get
            {
                throw new NotImplementedException("Should be implemented in the derived classes");
            }
            set
            {
                throw new NotImplementedException("Should be implemented in the derived classes");
            }
        }

        #endregion 

        #region Overrides
         
        protected override void OnColumnLayoutChanged()
        {
            try
            {
                base.OnColumnLayoutChanged();

                if ( (this.ColumnLayout.Grid!=null) && !this.ColumnLayout.Grid.Resources.Contains(typeof(ListBoxItem)))
                { 
                    ResourceDictionary dictionary = new ResourceDictionary();
                    dictionary.Source = new Uri("/vControlsGridColumns;component/vComboColumnBase/FilterMenuStyles.xaml", UriKind.Relative);

                    //Application.Current.Resources.Add(typeof(HeaderDropDownControl), dictionary[typeof(HeaderDropDownControl)] as Style);

                    if (!Application.Current.Resources.Contains(typeof(HeaderDropDownControl)))
                    {
                        Application.Current.Resources.Add(typeof(HeaderDropDownControl), dictionary[typeof(HeaderDropDownControl)] as Style);
                    }
                }

            }
            catch (Exception ex)
            {
            }
        }

        #endregion
    }
}
