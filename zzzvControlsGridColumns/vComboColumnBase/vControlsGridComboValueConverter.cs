﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using Infragistics.Controls.Grids.Primitives;
using System.Linq;
using vComboDataTypes;

namespace vControls
{

    /// <summary>
    ///  Filter Menu Code - This was added by IG to make the filter menu work
    /// </summary>
    public class vControlsGridComboValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            FilterValueProxy proxy = value as FilterValueProxy;

            // 2015-03-19:  This method gets called when a combo column is hidden and the user clicks on the Column Chooser element located in the grid column headers.
            // In this case the user was not opening the filter menu and therefore proxy will be null.
            // Exit as there nothing to do.
            if (proxy == null) { return null; }
            string filterValue = proxy.ContentString;

            if (proxy.Column is vXamComboColumn
                || proxy.Column is vXamMultiColumnComboColumn)
            { 
                var source = proxy.Column.GetType().GetProperty("ItemsSource").GetValue(proxy.Column, null) as ComboItemList;

                var item = source.Where(o => o.Id.ToString().Equals(proxy.ContentString)).FirstOrDefault();
                if (item != null)
                {
                    filterValue = item.ItemName;
                } 
            }

            return filterValue;

            //ComboValueContainer data = parameter as ComboValueContainer;
            //int id = -1;
            //int.TryParse(value.ToString(), out id);
            //if (id != -1 && data.Data.ContainsKey(id))
            //    return data.Data[id];
            //else
            //    return "";

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
