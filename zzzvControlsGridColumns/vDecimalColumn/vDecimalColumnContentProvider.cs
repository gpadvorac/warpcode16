﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Controls.Grids.Primitives;
using Infragistics.Controls.Grids;
using System.Diagnostics;
using TypeServices;

namespace vControls
{
    public class vDecimalColumnContentProvider : ColumnContentProviderBase
    {

        #region Initialize Variables

        TextBlock _textDisplay;
        vTextBox _vTextBox;
        vDecimalColumn _column = null;
        Cell _cell;
        bool _resetBindingSet;


        #endregion Initialize Variables

        #region Constructor

        public vDecimalColumnContentProvider()
        {
            try
            {
                _textDisplay = new TextBlock();
                _textDisplay.Margin = new Thickness(0);
                _vTextBox = new vTextBox();
                _vTextBox.Margin = new Thickness(-5);
                _vTextBox.TextChanged += new TextChangedEventHandler(vTextBox_TextChanged);
                _vTextBox.GotFocus += new RoutedEventHandler(vTextBox_GotFocus);
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        #endregion Constructor

        #region Overrides

        #region ResolveDisplayElement

        public override FrameworkElement ResolveDisplayElement(Cell cell, System.Windows.Data.Binding cellBinding)
        {
            try
            {
                _cell = cell;
                _column = (vDecimalColumn)cell.Column;

                //_textDisplay.TextWrapping = _column.TextWrapping;
                _resetBindingSet = cell.Row.RowType == RowType.FilterRow;
                _textDisplay.Foreground = _column.Foreground;

                if (cellBinding != null)
                {

                    cellBinding.ConverterCulture = System.Globalization.CultureInfo.CurrentCulture;
                    cellBinding.StringFormat = _column.DecimalFormat;
                    _textDisplay.SetBinding(TextBlock.TextProperty, cellBinding);

                }
                else
                {
                    Binding textBinding = new Binding();
                    textBinding.Path = new PropertyPath(_column.Key);
                    textBinding.Mode = BindingMode.TwoWay;

                    textBinding.StringFormat = _column.DecimalFormat;

                    //if (_column.DecimalFormat != null)
                    //{
                    //    textBinding.StringFormat = _column.DecimalFormat;
                    //}
                    _textDisplay.SetBinding(TextBlock.TextProperty, textBinding);


                }
                return _textDisplay;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveDisplayElement


        #region ResolveEditorControl

        protected override FrameworkElement ResolveEditorControl(Cell cell, object editorValue, double availableWidth, double availableHeight, System.Windows.Data.Binding editorBinding)
        {
            try
            {
                Binding textBinding = new Binding();
                textBinding.Path = new PropertyPath(_column.Key);
                textBinding.Mode = BindingMode.OneWay;
                _vTextBox.Foreground = _column.Foreground;

                // Added this 2014-05-07 becuase when using German culture and the decimal cell enters edit mode, the value will change from "5,00" (german) to "5.00" (english).
                //    However, "5.00" in german means 5 thousand, so the business object takes the text  five point zero and converts it to five thousand.
                //   Setting the culture here corrects the problem.
                textBinding.ConverterCulture = System.Globalization.CultureInfo.CurrentCulture;
                //textBinding.StringFormat = "#.####################";

                if (_column.HorizontalContentAlignment == HorizontalAlignment.Right)
                {
                    _vTextBox.TextAlignment = TextAlignment.Right;
                }
                else if (_column.HorizontalContentAlignment == HorizontalAlignment.Center)
                {
                    _vTextBox.TextAlignment = TextAlignment.Center;
                }

                _vTextBox.SetBinding(vTextBox.TextProperty, textBinding);


                _vTextBox.Style = cell.EditorStyleResolved;


                //_vTextBox.TextWrapping = _column.TextWrapping;
                if (_resetBindingSet == false)
                {
                    IBusinessObject obj = cell.Row.Data as IBusinessObject;
                    if (obj == null) { return null; }
                    SetXamGridEditControlValidStateAppearance(_column.Key, _vTextBox, obj);
                }
                else
                {
                    // Set the text from what it was the last time we entered a value.
                    _vTextBox.Text = _textDisplay.Text;
                }
                return _vTextBox;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveEditorControl


        #region ResolveValueFromEditor

        public override object ResolveValueFromEditor(Infragistics.Controls.Grids.Cell cell)
        {
            try
            {
                return this._vTextBox.Text;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveValueFromEditor


        #endregion Overrides


        #region Event Handlers

        void vTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {

                if (_vTextBox.Parent == null) { return; }

                if (_resetBindingSet == false)
                {
                    IBusinessObject data = _cell.Row.Data as IBusinessObject;
                    string key = _cell.Column.Key;
                    int index = _cell.Row.Index;
                    vDecimalColumnTextChangedEventArgs args = new vDecimalColumnTextChangedEventArgs(_cell, index, key, data, e);
                    _column.OnTextChanged(_vTextBox, args);
                }
                else
                {
                    // No need to raise the text changed event here as we dont want to set any values to the business object.
                }
                this.NotifyEditorValueChanged(this._vTextBox.Text);
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        void vTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                _vTextBox.SelectAll();
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }
        #endregion Event Handlers


        #region Methods

        public static void SetXamGridEditControlValidStateAppearance(string key, vTextBox ctl, IBusinessObject obj)
        {
            try
            {

                if (obj.IsPropertyValid(key))
                {
                    ctl.ValidStateAppearance = ValidationState.Valid;
                }
                else
                {
                    if (obj.IsPropertyDirty(key))
                    {
                        ctl.ValidStateAppearance = ValidationState.NotValidIsDirty;
                    }
                    else
                    {
                        ctl.ValidStateAppearance = ValidationState.NotValidNotDirty;
                    }
                }
            }
            catch (Exception ex)
            {
            
            }
        }

        #endregion Methods



    }
}
