﻿using System;
using System.Windows;
using System.Windows.Controls;
using Infragistics.Controls.Grids.Primitives;
using System.Windows.Data;
using Infragistics.Controls.Grids;
using System.Diagnostics;
//using vControls;
using Infragistics.Controls.Editors;
using vComboDataTypes;
using TypeServices;
using System.Windows.Media;
using System.Windows.Controls.Primitives;
using Ifx.SL;
using System.Windows.Markup;

//http://blogsprajeesh.blogspot.com/2010/03/infragistics-working-with-2.html

//http://news.infragistics.com/blogs/nikolay_zhekov/archive/2011/06/02/xamgrid-implementing-custom-column-filtering-ui.aspx

namespace vControls
{
    public class vXamMultiColumnComboColumnContentProvider : ColumnContentProviderBase
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "vControlsGridColumns";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "vXamMultiColumnComboColumnContentProvider";

        TextBlock _textDisplay;
        vControls.vXamMultiColumnComboEditor _comboEditor;
        vXamMultiColumnComboColumn _column = null;
        ComboItemList _comboItemSource;
        //bool _bindingSet = false;
        bool _resetBindingSet;
        Cell _cell;

        //  Added "_cellContent" 2012-10-19 by Krasimir to allow for conditional formating by Krasimir:
        // have bind the Content of the ContentControl, using the cellBinding parameter, 
        // in order have the content of the cell the same type as the DataType of the column and I have also created a DataTempalte, 
        // in which I am adding a TextBlock, bound to the Content of the ContentControl (the int value) and using a Converter (which I have defined in the vComboDataTypes in IdToItemNameConverter.cs file), 
        // I am converting the int to its corresponding string value and since this does not change the Content of the ContentControl and the cell, 
        // the conditional formatting is applied and the value of the cell appear as expected.
        ContentControl _cellContent;

        #endregion Initialize Variables


        #region Constructor

        public vXamMultiColumnComboColumnContentProvider()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumnContentProvider", IfxTraceCategory.Enter);
                _textDisplay = new TextBlock();
                _comboEditor = new vControls.vXamMultiColumnComboEditor();
                _comboEditor.Margin = new Thickness(-5);
                _comboEditor.Padding = new Thickness(3, 3, 0, 0);
                _comboEditor.vXamMultiColumnComboEditorSelectionChanged += new vXamMultiColumnComboEditorSelectionChangedEventHandler(vXamMultiColumnComboEditorSelectionChanged);
                _comboEditor.KeyDown += new System.Windows.Input.KeyEventHandler(ComboEditor_KeyDown);
                _comboEditor.KeyUp += new System.Windows.Input.KeyEventHandler(ComboEditor_KeyUp);
                //_comboEditor.DropDownOpened += new EventHandler(ComboEditor_DropDownOpened);

                ////_comboEditor.AutoGenerateColumns = false;

                //var colItemName = new TextComboColumn();
                //var colItemDesc = new TextComboColumn();
                //colItemName.Key = "ItemName";
                //colItemDesc.Key = "ItemDesc";
                //_comboEditor.Columns.Add(colItemName);
                ////_comboEditor.Columns.Add(colItemDesc);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumnContentProvider", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumnContentProvider", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructor


        #region Overrides

        #region ResolveDisplayElement

        public override FrameworkElement ResolveDisplayElement(Cell cell, System.Windows.Data.Binding cellBinding)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveDisplayElement", IfxTraceCategory.Enter);
                _cell = cell;
                _column = (vXamMultiColumnComboColumn)cell.Column;
                _cellContent = new ContentControl();    // new code used for conditional formating
                _textDisplay.Foreground = _column.Foreground;

                _resetBindingSet = cell.Row.RowType == Infragistics.Controls.Grids.RowType.FilterRow;
                if (_resetBindingSet == true)
                {

                    if (_comboEditor.SelectedItem == null)
                    {

                        // New Code Added for filtering 12/12/2011

                        if (cellBinding != null)
                        {
                            FilterRowCell frc = (FilterRowCell)_cell;
                            frc.FilterCellValue = null;
                            //_textDisplay.SetBinding(TextBlock.TextProperty, cellBinding);
                        }
                        // Please Note - this logic will be hit when the clear filters button is clicked
                        // You may need to add logic here to reset the child combo itemsource back 
                        // to the entire list here
                        _textDisplay.Text = "";
                    }
                    else
                    {
                        // New Code Added for filtering 12/12/2011
                        if (cellBinding != null)
                        {
                            FilterRowCell frc = (FilterRowCell)_cell;
                            frc.FilterCellValue = ((ComboItem)_comboEditor.SelectedItem).Id;
                            //_textDisplay.SetBinding(TextBlock.TextProperty, cellBinding);
                        }
                        _textDisplay.Text = ((ComboItem)_comboEditor.SelectedItem).ItemName;
                    }
                }
                else
                {


                    if (_column.AllowConditionalFormating == false)
                    {
                        // 2012-10-29:  New logic.  We added the ability to apply conditional formating using a DataTemplate for the 
                        // cell's ContentControl.  However, if Non-valid XML syntax is used in the text, the app will crash, therefore,
                        // Use this logic.  If we dont use conditional formating, then use the original code in the FALSE part of this if statement,
                        // Otherwise, use the data template in the TRUE part of the statment, but make sure all text used will pase using the XML parser.
                        // regarding    IsChildCombo = true....
                        Binding textBinding = new Binding();
                        if (_column.DisplayTextBinding == null)
                        {
                            //  We are not using the DisplayTextBinding so use the standard converter to get the display text
                            textBinding.Path = new PropertyPath(cell.Column.Key);
                            ComboItemList data = _column.ItemsSource as ComboItemList;
                            if (data == null) { return _textDisplay; }

                            if (_column.IsChildCombo == true)
                            {
                                //  Becuase this combo is filtered by an FK, we must seek the item name 
                                //  from the cached (non-filtered) list which is the full list.  Otherwise there are instances such as when using 
                                //  GroupBy that the converter will not find the name.  
                                //  Using the converter that uses the cached list assures we will find it.
                                textBinding.Converter = data.GetNameFromCachedListConverterProperty;
                            }
                            else
                            {
                                //  To save resources; since this combo is not filtered by an FK, there are no items in the ComboItemList's cached list.
                                //  Therefore, use the converter that uses items from the ComboItemList base list.
                                textBinding.Converter = data.GetNameFromListConverterProperty;
                            }
                            textBinding.ConverterParameter = _column.ItemsSource;
                        }
                        else
                        {
                            // We have a DisplayTextBinding value so lets use it instead of using a converter.  This will improve performance.
                            textBinding.Path = new PropertyPath(_column.DisplayTextBinding);
                        }

                        textBinding.Mode = BindingMode.TwoWay;
                        _textDisplay.SetBinding(TextBlock.TextProperty, textBinding);
                    }
                    else
                    {
                        //***************************************
                        /// New code for Conditional Formating   *************************************
                        // in order to have the conditional formattign to work correclty, 
                        //it seems that the cell's content type shold be the same as the DataType of the Colunm
                        //So, i am setting adding a ContentControl as Display Element, bound to the cell's value
                        //and i am using a ContentTempalte for the ContentControl, to change the Ids to Display Values

                        //converting the ItemsSource of the ComboColumn to xaml definitions,
                        //to be able to pass the source as converter parameter
                        string items = "";
                        foreach (ComboItem item in _column.ItemsSource)
                        {
                            items +=
                                @"<data:ComboItem 
                                    Id=""" + item.Id + @""" " +
                                      @"ItemName=""" + item.ItemName + @""" " +
                                      @"FK=""" + item.FK + @""" " +
                                      @"Desc=""" + item.Desc + @""" />";
                        }
                        //Creating DataTempalte
                        string template =
                                @"
                            <DataTemplate
                                xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""
                                xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml""
                                xmlns:ig=""http://schemas.infragistics.com/xaml""
                                xmlns:data=""clr-namespace:vComboDataTypes;assembly=vComboDataTypes"">
                                <Grid>
                                    <Grid.Resources>
                                        <data:IdToItemNameConverter x:Key=""IdToItemNameConverter""/>
                                        <data:ComboItemList x:Key=""ComboItemList"">" +
                            items +
                        @"</data:ComboItemList>
                                    </Grid.Resources>
                                    <TextBlock Text=""{Binding Converter={StaticResource IdToItemNameConverter}, ConverterParameter={StaticResource ComboItemList}}""
                                                Foreground=""{Binding RelativeSource={RelativeSource AncestorType=ig:ConditionalFormattingCellControl}, Path=Foreground}""/>
                                </Grid>
                            </DataTemplate>
                        ";

                        //Setting the DataTempalte as ContentTempate of the ContentControl
                        _cellContent.ContentTemplate = (DataTemplate)XamlReader.Load(template);
                        //Setting the Binding of the ContentControl's content
                        if (cellBinding != null)
                        {
                            _cellContent.SetBinding(ContentControl.ContentProperty, cellBinding);
                        }
 
                    }
                }

                if (_column.AllowConditionalFormating == false)
                {
                    return _textDisplay;
                }
                else
                {
                    return _cellContent;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveDisplayElement", ex);
                return _textDisplay;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveDisplayElement", IfxTraceCategory.Leave);
            }
        }

        #endregion ResolveDisplayElement


        #region ResolveEditorControl

        protected override FrameworkElement ResolveEditorControl(Cell cell, object editorValue, double availableWidth, double availableHeight, System.Windows.Data.Binding editorBinding)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveEditorControl", IfxTraceCategory.Enter);

                _comboEditor.Foreground = _column.Foreground;

                SetupComboBox(cell, editorBinding);
                // 2011-11-22 added by george to try to make the filter take effect
                if (editorBinding != null)
                {
                    if (cell.Row is FilterRow)
                        // for filter.  not working.
                        editorBinding.UpdateSourceTrigger = UpdateSourceTrigger.Default;
                }

                
                IBusinessObject obj = cell.Row.Data as IBusinessObject;
                if (obj == null) { return null; }

                if (_column.IsChildCombo == true)
                {

                    if (cell.Row.RowType == Infragistics.Controls.Grids.RowType.FilterRow)
                    {
                        object fk = null;
                        if (cell.Row.Cells[_column.ParentComboKey].Column.GetType().ToString() == "vControls.vXamComboColumn")
                        {
                            fk = ((vXamComboColumn)cell.Row.Cells[_column.ParentComboKey].Column).SelectedValue;
                        }
                        else
                        {
                            fk = ((vXamMultiColumnComboColumn)cell.Row.Cells[_column.ParentComboKey].Column).SelectedValue;
                        }

                        ((ComboItemList)_comboEditor.ItemsSource).FilterByFK(fk);
                    }
                    else
                    {
                        //object val = ((vXamMultiColumnComboColumn)cell.Row.Cells[_column.ParentComboKey].Column).SelectedValue;
                        object val = obj.GetPropertyValueByKey(_column.ParentComboKey);
                        ((ComboItemList)_comboEditor.ItemsSource).FilterByFK(val);
                    }
                }

                if (_resetBindingSet == true)
                {
                    Set_ComboEditorItem_SelectedItem(_column.SelectedValue);
                }
                else
                {
                    Set_ComboEditorItem_SelectedItem(cell.Value);
                    SetXamGridEditControlValidStateAppearance(_column.Key, _comboEditor, obj);
                }
                return this._comboEditor;



                //IBusinessObject obj = cell.Row.Data as IBusinessObject;
                //if (obj == null) { return null; }

                //if (_column.IsChildCombo == true)
                //{
                //    //object val = ((vXamMultiColumnComboColumn)cell.Row.Cells[_column.ParentComboKey].Column).SelectedValue;
                //    object val = obj.GetPropertyValueByKey(_column.ParentComboKey);
                //    ((ComboItemList)_comboEditor.ItemsSource).FilterByFK(val);
                //}

                //if (_resetBindingSet == true)
                //{
                //    Set_ComboEditorItem_SelectedItem(_column.SelectedValue);

                //}
                //else
                //{

                //    Set_ComboEditorItem_SelectedItem(cell.Value);
                //    SetXamGridEditControlValidStateAppearance(_column.Key, _comboEditor, obj);
                //}

                //return this._comboEditor;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveEditorControl", ex);
                return this._comboEditor;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveEditorControl", IfxTraceCategory.Leave);
            }
        }

        #endregion ResolveEditorControl


        #region ResolveValueFromEditor

        public override object ResolveValueFromEditor(Infragistics.Controls.Grids.Cell cell)
        {
            return this._comboEditor.SelectedItem;
        }

        #endregion ResolveValueFromEditor


        #region GenerateGroupByCellContent

        #endregion GenerateGroupByCellContent


        #endregion Overrides


        #region Event Handlers



        void vXamMultiColumnComboEditorSelectionChanged(object sender, vXamMultiColumnComboEditorSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboEditorSelectionChanged", IfxTraceCategory.Enter);
                //_comboEditor.SelectionChanged -= new EventHandler(ComboEditor_SelectionChanged);
                if (_comboEditor.Parent == null) { return; }

                if (_comboEditor.SelectedItem == null)
                {
                    _column.SelectedValue = null;
                }
                else
                {
                    _column.SelectedValue = ((ComboItem)_comboEditor.SelectedItem).Id;
                }

                if (_resetBindingSet == false)
                {
                    IBusinessObject data = _cell.Row.Data as IBusinessObject;
                    string key = _cell.Column.Key;
                    int index = _cell.Row.Index;
                    vXamMultiColumnComboColumnSelectionChangedEventArgs args = new vXamMultiColumnComboColumnSelectionChangedEventArgs(_cell, index, key, data, e);
                    _column.OnSelectionChanged(_comboEditor, args);

                    if (_column.MustUpdateChildCombo == true)
                    {
                        //// Test to make sure its a vXamMultiColumnComboEditor becuase sometimes it a TextBlock instead.
                        //if (((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content.GetType().ToString() == "vControls.vXamMultiColumnComboEditor")
                        //{
                        //    // Use this cast instead of the one above becuase sometimes it tries to cast a textblock as a combo.
                        //    if ((ComboItemList)((vControls.vXamMultiColumnComboEditor)((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content).ItemsSource != null)
                        //    {
                        //        ((ComboItemList)((vControls.vXamMultiColumnComboEditor)((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content).ItemsSource).FilterByFK(_cell.Value);
                        //    }
                        //}

                        // Test to make sure its a vXamComboEditor becuase sometimes it a TextBlock instead.
                        if (((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content.GetType().ToString() == "vControls.vXamComboEditor")
                        {
                            // Use this cast instead of the one above becuase sometimes it tries to cast a textblock as a combo.
                            vControls.vXamComboEditor cbo = ((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content as vControls.vXamComboEditor;
                            if (cbo != null && cbo.ItemsSource != null)
                            {
                                ((ComboItemList)cbo.ItemsSource).FilterByFK(_cell.Value);
                            }
                        }
                        else if (((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content.GetType().ToString() == "vControls.vXamMultiColumnComboEditor")
                        {
                            // Use this cast instead of the one above becuase sometimes it tries to cast a textblock as a combo.
                            vControls.vXamMultiColumnComboEditor cmbo = ((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content as vControls.vXamMultiColumnComboEditor;
                            if (cmbo != null && cmbo.ItemsSource != null)
                            {
                                ((ComboItemList)cmbo.ItemsSource).FilterByFK(_cell.Value);
                            }
                        }
                    }
                }
                else
                {
                    // No need to raise the text changed event here as we dont want to set any values to the business object.
                }

                // If this is a Filter Row
                if (_cell.Row.RowType == Infragistics.Controls.Grids.RowType.FilterRow)
                {
                    // If this is a parent combo, our other code will clear the child combo for normal rows (data rows)
                    // but this is a filter row and that code doesnt have any effect, so we need to run code here to clear the child columns filter's value.
                    string childKey = ((vXamMultiColumnComboColumn)_cell.Column).ChildComboKey;
                    // ToDo:
                    //  clear the value in the child columns display textblox
                    //  clear the child column's filter.
                }
                // for filter.  not working.
                this.NotifyEditorValueChanged(_column.SelectedValue);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboEditorSelectionChanged", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboEditorSelectionChanged", IfxTraceCategory.Leave);
            }
        }


        //void ComboEditor_SelectionChanged(object sender, EventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_SelectionChanged", IfxTraceCategory.Enter);
        //        //_comboEditor.SelectionChanged -= new EventHandler(ComboEditor_SelectionChanged);
        //        if (_comboEditor.Parent == null) { return; }

        //        if (_comboEditor.SelectedItem == null)
        //        {
        //            _column.SelectedValue = null;
        //        }
        //        else
        //        {
        //            _column.SelectedValue = ((ComboItem)_comboEditor.SelectedItem).Id;
        //        }

        //        if (_resetBindingSet == false)
        //        {
        //            IBusinessObject data = _cell.Row.Data as IBusinessObject;
        //            string key = _cell.Column.Key;
        //            int index = _cell.Row.Index;
        //            vXamMultiColumnComboColumnSelectionChangedEventArgs args = new vXamMultiColumnComboColumnSelectionChangedEventArgs(_cell, index, key, data, e);
        //            _column.OnSelectionChanged(_comboEditor, args);

        //            if (_column.MustUpdateChildCombo == true)
        //            {
        //                //// Test to make sure its a vXamMultiColumnComboEditor becuase sometimes it a TextBlock instead.
        //                //if (((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content.GetType().ToString() == "vControls.vXamMultiColumnComboEditor")
        //                //{
        //                //    // Use this cast instead of the one above becuase sometimes it tries to cast a textblock as a combo.
        //                //    if ((ComboItemList)((vControls.vXamMultiColumnComboEditor)((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content).ItemsSource != null)
        //                //    {
        //                //        ((ComboItemList)((vControls.vXamMultiColumnComboEditor)((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content).ItemsSource).FilterByFK(_cell.Value);
        //                //    }
        //                //}

        //                // Test to make sure its a vXamComboEditor becuase sometimes it a TextBlock instead.
        //                if (((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content.GetType().ToString() == "vControls.vXamComboEditor")
        //                {
        //                    // Use this cast instead of the one above becuase sometimes it tries to cast a textblock as a combo.
        //                    vControls.vXamComboEditor cbo = ((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content as vControls.vXamComboEditor;
        //                    if (cbo != null && cbo.ItemsSource != null)
        //                    {
        //                        ((ComboItemList)cbo.ItemsSource).FilterByFK(_cell.Value);
        //                    }
        //                }
        //                else if (((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content.GetType().ToString() == "vControls.vXamMultiColumnComboEditor")
        //                {
        //                    // Use this cast instead of the one above becuase sometimes it tries to cast a textblock as a combo.
        //                    vControls.vXamMultiColumnComboEditor cmbo = ((CellControl)((vControls.vXamMultiColumnComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content as vControls.vXamMultiColumnComboEditor;
        //                    if (cmbo != null && cmbo.ItemsSource != null)
        //                    {
        //                        ((ComboItemList)cmbo.ItemsSource).FilterByFK(_cell.Value);
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            // No need to raise the text changed event here as we dont want to set any values to the business object.
        //        }

        //        // If this is a Filter Row
        //        if (_cell.Row.RowType == Infragistics.Controls.Grids.RowType.FilterRow)
        //        {
        //            // If this is a parent combo, our other code will clear the child combo for normal rows (data rows)
        //            // but this is a filter row and that code doesnt have any effect, so we need to run code here to clear the child columns filter's value.
        //            string childKey = ((vXamMultiColumnComboColumn)_cell.Column).ChildComboKey;
        //            // ToDo:
        //            //  clear the value in the child columns display textblox
        //            //  clear the child column's filter.
        //        }
        //        // for filter.  not working.
        //        this.NotifyEditorValueChanged(_column.SelectedValue);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_SelectionChanged", ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_SelectionChanged", IfxTraceCategory.Leave);
        //    }
        //}

        void ComboEditor_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyUp", IfxTraceCategory.Enter);

                if (_resetBindingSet == false)
                {
                    IBusinessObject data = _cell.Row.Data as IBusinessObject;
                    string key = _cell.Column.Key;
                    int index = _cell.Row.Index;

                    vXamComboColumnKeyEventArgs args = new vXamComboColumnKeyEventArgs(_cell, index, key, data, e);
                    _column.OnKeyUp(_comboEditor, args);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyUp", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyUp", IfxTraceCategory.Leave);
            }
        }

        void ComboEditor_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyDown", IfxTraceCategory.Enter);

                if (_resetBindingSet == false)
                {
                    IBusinessObject data = _cell.Row.Data as IBusinessObject;
                    string key = _cell.Column.Key;
                    int index = _cell.Row.Index;

                    vXamComboColumnKeyEventArgs args = new vXamComboColumnKeyEventArgs(_cell, index, key, data, e);
                    _column.OnKeyDown(_comboEditor, args);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyDown", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyDown", IfxTraceCategory.Leave);
            }
        }


        #endregion Event Handlers


        #region Methods

        private void SetupComboBox(Cell cell, Binding cellBinding)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetupComboBox", IfxTraceCategory.Enter);
                if (_comboEditor.Columns.Count == 2) { return; }  // already been setup.

                if (_comboEditor.DisplayMemberPath != _column.DisplayMemberPath)
                {
                    if (_column.DisplayMemberPath == null)
                    {
                        _comboEditor.ClearValue(vControls.vXamMultiColumnComboEditor.DisplayMemberPathProperty);
                    }
                    else
                    {
                        _comboEditor.DisplayMemberPath = _column.DisplayMemberPath;
                    }
                }

                _comboEditor.ItemsSource = _column.ItemsSource;

                if (_comboEditor.ItemsSource != null)
                {
                    _comboItemSource = (ComboItemList)_comboEditor.ItemsSource;
                }
                else
                {
                    //Debugger.Break();
                }



                // Initialize combo columns
                _comboEditor.AutoGenerateColumns = false;

                var colItemName = new TextComboColumn();
                var colItemDesc = new TextComboColumn();

                if (_comboEditor.Columns.Count == 0)
                {
                    colItemName.Key = "ItemName";
                    colItemDesc.Key = "Desc";
                    _comboEditor.Columns.Add(colItemName);
                    _comboEditor.Columns.Add(colItemDesc);
                }
                else
                {
                    colItemName = _comboEditor.Columns["ItemName"] as TextComboColumn;
                    colItemDesc = _comboEditor.Columns["Desc"] as TextComboColumn;
                }






                // Min Width
                if (_column.ItemNameMinimumWidth > 0)
                {
                    colItemName.MinimumWidth = _column.ItemNameMinimumWidth;
                }

                if (_column.ItemDescriptionMinimumWidth > 0)
                {
                    colItemDesc.MinimumWidth = _column.ItemDescriptionMinimumWidth;
                }

                // Max Width
                if (_column.ItemNameMaximumWidth > 0)
                {
                    colItemName.MaximumWidth = _column.ItemNameMaximumWidth;
                }

                if (_column.ItemDescriptionMaximumWidth > 0)
                {
                    colItemDesc.MaximumWidth = _column.ItemDescriptionMaximumWidth;
                }

                // Header Text
                colItemName.HeaderText = _column.ItemNameHeaderText;
                colItemDesc.HeaderText = _column.ItemDescriptionHeaderText;

                // Initialize the style for the ItemName column's TextBlock
                Style nameStyle = new Style(typeof(TextBlock));
                // Initialize the style for the ItemDesc column's TextBlock
                Style descStyle = new Style(typeof(TextBlock));


                // TextWrapping
                // item name
                if (_column.ItemNameMaximumWidth > 0)
                {
                    // if we haven't set the max width in xaml, then we can't do text wrapping
                    colItemName.TextWrapping = _column.ItemNameTextWrapping;
                    nameStyle.Setters.Add(new Setter(TextBlock.TextWrappingProperty, _column.ItemNameTextWrapping));
                    nameStyle.Setters.Add(new Setter(TextBlock.MaxWidthProperty, _column.ItemNameMaximumWidth));
                }



                // TextWrapping
                // item description
                if (_column.ItemDescriptionMaximumWidth == -1)
                {
                    colItemDesc.MaximumWidth = 0;
                }
                else if (_column.ItemDescriptionMaximumWidth > 0)
                {
                    // if we haven't set the max width in xaml, then we can't do text wrapping
                    colItemDesc.TextWrapping = _column.ItemDescriptionTextWrapping;
                    descStyle.Setters.Add(new Setter(TextBlock.TextWrappingProperty, _column.ItemDescriptionTextWrapping));
                    descStyle.Setters.Add(new Setter(TextBlock.MaxWidthProperty, _column.ItemDescriptionMaximumWidth));
                }


                // Set the style for the ItemName column's TextBlock
                colItemDesc.TextBlockStyle = nameStyle;
                // Set the style for the ItemDesc column's TextBlock
                colItemDesc.TextBlockStyle = descStyle;


                // SelectedItemsResetButtonVisibility
                _comboEditor.SelectedItemsResetButtonVisibility = _column.SelectedItemsResetButtonVisibility;

                // MaxDropDownHeight
                if (_column.MaxDropDownHeight > 0)
                {
                    _comboEditor.MaxDropDownHeight = _column.MaxDropDownHeight;
                }

                // AllowDropDownResizing
                _comboEditor.AllowDropDownResizing = _column.AllowDropDownResizing;

                // MaxDropDownHeight
                if (_column.DropdownWidth > 0)
                {
                    _comboEditor.DropdownWidth = _column.DropdownWidth;
                }

                // AllowMultipleSelection
                _comboEditor.AllowMultipleSelection = _column.AllowMultipleSelection;

                // CheckBoxVisibility
                _comboEditor.CheckBoxVisibility = _column.CheckBoxVisibility;

                Style comboStyle = cell.EditorStyleResolved;
                if (comboStyle != _comboEditor.Style)
                {
                    if (comboStyle == null)
                    {
                        _comboEditor.ClearValue(Control.StyleProperty);
                    }
                    else
                    {
                        _comboEditor.Style = comboStyle;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetupComboBox", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetupComboBox", IfxTraceCategory.Leave);
            }
        }

        public static void SetXamGridEditControlValidStateAppearance(string key, vControls.vXamMultiColumnComboEditor ctl, IBusinessObject obj)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetXamGridEditControlValidStateAppearance", IfxTraceCategory.Enter);

                if (obj.IsPropertyValid(key))
                {
                    ctl.ValidStateAppearance = ValidationState.Valid;
                }
                else
                {
                    if (obj.IsPropertyDirty(key))
                    {
                        ctl.ValidStateAppearance = ValidationState.NotValidIsDirty;
                    }
                    else
                    {
                        ctl.ValidStateAppearance = ValidationState.NotValidNotDirty;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetXamGridEditControlValidStateAppearance", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetXamGridEditControlValidStateAppearance", IfxTraceCategory.Leave);
            }
        }

        void Set_ComboEditorItem_SelectedItem(object value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem", IfxTraceCategory.Enter);
                if (_comboItemSource == null) { return; }
                if (value == null)
                {
                    _comboEditor.SelectedIndex = -1;
                    return;
                }
                if (_comboItemSource.Id_DataType == ComboItemList.DataType.IntegerType)
                {
                    if (value.GetType().ToString() != "System.Int32") { return; }

                    foreach (ComboRow obj in _comboEditor.Items)
                    {
                        if ((int)((ComboItem)obj.Data).Id == (int)value)
                        {
                            obj.IsSelected = true;
                            return;
                        }
                    }
                }
                else if (_comboItemSource.Id_DataType == ComboItemList.DataType.GuidType)
                {
                    if (value.GetType().ToString() != "System.Guid") { return; }

                    foreach (ComboRow obj in _comboEditor.Items)
                    {
                        if ((Guid)((ComboItem)obj.Data).Id == (Guid)value)
                        {
                            obj.IsSelected = true;
                            return;
                        }
                    }
                }
                else if (_comboItemSource.Id_DataType == ComboItemList.DataType.StringType)
                {
                    if (value.GetType().ToString() != "System.String") { return; }

                    foreach (ComboRow obj in _comboEditor.Items)
                    {
                        if ((string)((ComboItem)obj.Data).Id == (string)value)
                        {
                            obj.IsSelected = true;
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem", IfxTraceCategory.Leave);
            }
        }

        #endregion Methods


    }
}
