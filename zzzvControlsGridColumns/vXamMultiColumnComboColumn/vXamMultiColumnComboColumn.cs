﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Infragistics.Controls.Grids;
using System.Collections;
using vComboDataTypes;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Markup;
using Infragistics.Controls.Editors;
using System.Diagnostics;
using vTooltipProvider;
using System.Text;
//using vControls;

namespace vControls
{
    public class vXamMultiColumnComboColumn : vComboColumnBase
    {

        #region Initialize Variables

        public event EventHandler<vXamMultiColumnComboColumnSelectionChangedEventArgs> SelectionChanged;
        public event EventHandler<vXamComboColumnKeyEventArgs> KeyUp;
        public event EventHandler<vXamComboColumnKeyEventArgs> KeyDown;

        #endregion Initialize Variables


        #region Constructor

        public vXamMultiColumnComboColumn()
        {
            //this.SortComparer = new vXamMultiColumnComboColumn_SortComparer();
        }

        #endregion Constructor


        #region Properties

        #region ItemsSource

        /// <summary>
        /// Identifies the <see cref="ItemsSource"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource",
            typeof(IEnumerable),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(new PropertyChangedCallback(ItemsSourceChanged)));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)this.GetValue(ItemsSourceProperty); }
            set
            {
                try
                {
                    this.SetValue(ItemsSourceProperty, value);

                    // Because this column is designed to work with 3 different data types (int, guid and string), we need to have a SortComparer for each type
                    //if (DataType == typeof(Guid))
                    if (value == null)
                    {
                        return;
                    }

                    // Configure the GroupByItemTemplate
                    string key = (string)this.GetValue(DisplayTextBindingProperty);
                    if (!string.IsNullOrEmpty(key))
                    {
                        //  DisplayTextBindingProperty has a value so we can use it for the TextBlock binding in the data template
                        GroupByItemTemplate = (DataTemplate)XamlReader.Load(
                                @"<DataTemplate 
                        xmlns=""http://schemas.microsoft.com/client/2007"">                            
                            <StackPanel " + @"Orientation=""Horizontal"">"
                                        + "<TextBlock" + @" Text=""{Binding Records[0]." + key + @"}""/>"
                                        + "<TextBlock" + @" Text=""{Binding Count" + @", StringFormat=': (0)'}""/>"
                                    + "</StackPanel>"
                                + "</DataTemplate>"
                        );
                    }
                    else
                    {
                        // DisplayTextBindingProperty is null so we have to use a converter in ComboItemList
                        // But dont think we can apply it here.
                        // is there a way to use a static reference the combo's ItemsSource in a binding below?
                        // if so, we could bind to a converter and pass the combo's ItemsSource in as a parameter.
                        // The Items source would have enough metadata to figure out what to do in the conerter.
                        ComboItemList list = value as ComboItemList;
                        if (list == null) { return; }

                        string items = "";

                        foreach (ComboItem item in list)
                        {
                            items += @"<data:ComboItem Id=""" + item.Id + @""""
                                                       + @" ItemName=""" + item.ItemName + @""""
                                                       + @" FK=""" + item.FK + @""""
                                                       + @" Desc=""" + item.Desc + @"""" + "/> ";
                        }

                        GroupByItemTemplate = (DataTemplate)XamlReader.Load(
                                @"<DataTemplate 
                        xmlns=""http://schemas.microsoft.com/client/2007"""
                                + " xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml' "
                                + " xmlns:data='clr-namespace:vComboDataTypes;assembly=vComboDataTypes'>"
                                + "<StackPanel " + @"Orientation=""Horizontal"">"
                                        + "<StackPanel.Resources>"
                                                + @"<data:ComboItemList x:Key=""comboItemListKey"">"
                                                    + items
                                                + @"</data:ComboItemList >"
                                                + @"<data:ComboItemForGroupByConverter x:Key=""myConverter""/>"
                                        + "</StackPanel.Resources>"
                                        + "<TextBlock" + @" Text=""{Binding Converter={StaticResource myConverter}, ConverterParameter={StaticResource comboItemListKey}}""/>"
                                        + "<TextBlock" + @" Text=""{Binding Count" + @", StringFormat=': (0)'}""/>"
                                    + "</StackPanel>"
                                + "</DataTemplate>"
                        );
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        private static void ItemsSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
            col.OnPropertyChanged("ItemsSource");
        }

        #endregion ItemsSource


        #region DisplayMemberPath

        /// <summary>
        /// Identifies the <see cref="DisplayMemberPath"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty DisplayMemberPathProperty = DependencyProperty.Register("DisplayMemberPath",
            typeof(string),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(new PropertyChangedCallback(DisplayMemberPathChanged)));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public string DisplayMemberPath
        {
            get { return (string)this.GetValue(DisplayMemberPathProperty); }
            set { this.SetValue(DisplayMemberPathProperty, value); }
        }

        private static void DisplayMemberPathChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
            col.OnPropertyChanged("DisplayMemberPath");
        }

        #endregion  DisplayMemberPath


        #region ItemTemplate

        /// <summary>
        /// Identifies the <see cref="ItemTemplate"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty ItemTemplateProperty = DependencyProperty.Register("ItemTemplate",
            typeof(DataTemplate),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(new PropertyChangedCallback(ItemTemplateChanged)));

        /// <summary>
        /// Gets or sets the <see cref="DataTemplate"/> used to display each item.
        /// </summary>
        public string ItemTemplate
        {
            get { return (string)this.GetValue(ItemTemplateProperty); }
            set { this.SetValue(ItemTemplateProperty, value); }
        }

        private static void ItemTemplateChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
            col.OnPropertyChanged("ItemTemplate");
        }

        #endregion // ItemTemplate


        #region ComboBoxStyle

        /// <summary>
        /// Identifies the <see cref="ComboBoxStyle"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty ComboBoxStyleProperty = DependencyProperty.Register("ComboBoxStyle",
            typeof(Style),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(new PropertyChangedCallback(ComboBoxStyleChanged)));

        /// <summary>
        /// Gets or sets the style used by ComboBox element when it is rendered
        /// </summary>
        public string ComboBoxStyle
        {
            get { return (string)this.GetValue(ComboBoxStyleProperty); }
            set { this.SetValue(ComboBoxStyleProperty, value); }
        }

        private static void ComboBoxStyleChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
            col.OnPropertyChanged("ComboBoxStyle");
        }

        #endregion // ComboBoxStyle


        #region DisplayTextBinding

        /// <summary>
        /// Identifies the <see cref="DisplayTextBinding"/> dependency property. 
        /// DisplayTextBinding is used to improve performance for large grids where many instances of combo values must
        /// be converted to text.
        /// This property will  bind to the display text when not in edit mode.  Using this property replaces the need
        /// to use a value converter to get the display text from the Combo's ItemSouce item.  Using this
        /// will require an additional text field for the non-text field which the combo is bound to.  The use of this property 
        /// requires a strict naming convention.  For example:  If the combo is bound to an integer field named "CustomerID", 
        /// the text field bound to DisplayTextBinding must be - "CustomerID_TextField"  (field name + "_TextField")'
        /// </summary>
        public static readonly DependencyProperty DisplayTextBindingProperty = DependencyProperty.Register("DisplayTextBinding",
            typeof(string),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public string DisplayTextBinding
        {
            get { return (string)this.GetValue(DisplayTextBindingProperty); }
            set
            {
                this.SetValue(DisplayTextBindingProperty, value);
            }
        }

        //private static void DisplayTextBindingChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
        //    col.OnPropertyChanged("DisplayTextBinding");
        //}

        #endregion  DisplayTextBinding


        #region IsChildCombo

        /// <summary>
        /// Identifies the <see cref="IsChildCombo"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty IsChildComboProperty = DependencyProperty.Register("IsChildCombo",
            typeof(bool),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(false));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public bool IsChildCombo
        {
            get { return (bool)this.GetValue(IsChildComboProperty); }
            set
            {
                this.SetValue(IsChildComboProperty, value);
            }
        }

        //private static void IsChildComboChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
        //    col.OnPropertyChanged("IsChildCombo");
        //}

        #endregion  IsChildCombo


        #region MustUpdateChildCombo

        /// <summary>
        /// Identifies the <see cref="MustUpdateChildCombo"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty MustUpdateChildComboProperty = DependencyProperty.Register("MustUpdateChildCombo",
            typeof(bool),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(false));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public bool MustUpdateChildCombo
        {
            get { return (bool)this.GetValue(MustUpdateChildComboProperty); }
            set
            {
                this.SetValue(MustUpdateChildComboProperty, value);
            }
        }

        //private static void MustUpdateChildComboChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
        //    col.OnPropertyChanged("MustUpdateChildCombo");
        //}

        #endregion  MustUpdateChildCombo


        #region ParentComboKey

        /// <summary>
        /// Identifies the <see cref="ParentComboKey"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ParentComboKeyProperty = DependencyProperty.Register("ParentComboKey",
            typeof(string),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public string ParentComboKey
        {
            get { return (string)this.GetValue(ParentComboKeyProperty); }
            set
            {
                this.SetValue(ParentComboKeyProperty, value);
            }
        }

        //private static void ParentComboKeyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
        //    col.OnPropertyChanged("ParentComboKey");
        //}

        #endregion  ParentComboKey


        #region ChildComboKey

        /// <summary>
        /// Identifies the <see cref="ChildComboKey"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ChildComboKeyProperty = DependencyProperty.Register("ChildComboKey",
            typeof(string),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public string ChildComboKey
        {
            get { return (string)this.GetValue(ChildComboKeyProperty); }
            set
            {
                this.SetValue(ChildComboKeyProperty, value);
            }
        }

        //private static void ChildComboKeyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
        //    col.OnPropertyChanged("ChildComboKey");
        //}

        #endregion  ChildComboKey


        #region SelectedValue

        /// <summary>
        /// Identifies the <see cref="SelectedValue"/> dependency property. 
        /// If True, then this property will use the value from SelectedValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty SelectedValueProperty = DependencyProperty.Register("SelectedValue",
            typeof(object),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(new PropertyChangedCallback(SelectedValueChanged)));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public object SelectedValue
        {
            get { return (object)this.GetValue(SelectedValueProperty); }
            set
            {
                this.SetValue(SelectedValueProperty, value);
            }
        }

        private static void SelectedValueChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            //vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
            //col.OnPropertyChanged("SelectedValue");
        }

        #endregion  SelectedValue


        #region ItemNameHeaderText

        /// <summary>
        /// Identifies the <see cref="ItemNameHeaderText"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ItemNameHeaderTextProperty = DependencyProperty.Register("ItemNameHeaderText",
            typeof(string),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public string ItemNameHeaderText
        {
            get { return (string)this.GetValue(ItemNameHeaderTextProperty); }
            set
            {
                this.SetValue(ItemNameHeaderTextProperty, value);
            }
        }

        #endregion  ItemNameHeaderText


        #region ItemDescriptionHeaderText

        /// <summary>
        /// Identifies the <see cref="ItemDescriptionHeaderText"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ItemDescriptionHeaderTextProperty = DependencyProperty.Register("ItemDescriptionHeaderText",
            typeof(string),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public string ItemDescriptionHeaderText
        {
            get { return (string)this.GetValue(ItemDescriptionHeaderTextProperty); }
            set
            {
                this.SetValue(ItemDescriptionHeaderTextProperty, value);
            }
        }

        #endregion  ItemDescriptionHeaderText


        #region ItemNameMinimumWidth

        /// <summary>
        /// Identifies the <see cref="ItemNameMinimumWidth"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ItemNameMinimumWidthProperty = DependencyProperty.Register("ItemNameMinimumWidth",
            typeof(double),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public double ItemNameMinimumWidth
        {
            get { return (double)this.GetValue(ItemNameMinimumWidthProperty); }
            set
            {
                this.SetValue(ItemNameMinimumWidthProperty, value);
            }
        }

        #endregion  ItemNameMinimumWidth


        #region ItemDescriptionMinimumWidth

        /// <summary>
        /// Identifies the <see cref="ItemDescriptionMinimumWidth"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ItemDescriptionMinimumWidthProperty = DependencyProperty.Register("ItemDescriptionMinimumWidth",
            typeof(double),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public double ItemDescriptionMinimumWidth
        {
            get { return (double)this.GetValue(ItemDescriptionMinimumWidthProperty); }
            set
            {
                this.SetValue(ItemDescriptionMinimumWidthProperty, value);
            }
        }

        #endregion  ItemDescriptionMinimumWidth


        #region ItemNameMaximumWidth

        /// <summary>
        /// Identifies the <see cref="ItemNameMaximumWidth"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ItemNameMaximumWidthProperty = DependencyProperty.Register("ItemNameMaximumWidth",
            typeof(double),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public double ItemNameMaximumWidth
        {
            get { return (double)this.GetValue(ItemNameMaximumWidthProperty); }
            set
            {
                this.SetValue(ItemNameMaximumWidthProperty, value);
            }
        }

        #endregion  ItemNameMaximumWidth


        #region ItemDescriptionMaximumWidth

        /// <summary>
        /// Identifies the <see cref="ItemDescriptionMaximumWidth"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ItemDescriptionMaximumWidthProperty = DependencyProperty.Register("ItemDescriptionMaximumWidth",
            typeof(double),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public double ItemDescriptionMaximumWidth
        {
            get { return (double)this.GetValue(ItemDescriptionMaximumWidthProperty); }
            set
            {
                this.SetValue(ItemDescriptionMaximumWidthProperty, value);
            }
        }

        #endregion  ItemDescriptionMaximumWidth


        #region ItemNameTextWrapping

        /// <summary>
        /// Identifies the <see cref="ItemNameTextWrapping"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ItemNameTextWrappingProperty = DependencyProperty.Register("ItemNameTextWrapping",
            typeof(TextWrapping),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public TextWrapping ItemNameTextWrapping
        {
            get { return (TextWrapping)this.GetValue(ItemNameTextWrappingProperty); }
            set
            {
                this.SetValue(ItemNameTextWrappingProperty, value);
            }
        }

        #endregion  ItemNameTextWrapping


        #region ItemDescriptionTextWrapping

        /// <summary>
        /// Identifies the <see cref="ItemDescriptionTextWrapping"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ItemDescriptionTextWrappingProperty = DependencyProperty.Register("ItemDescriptionTextWrapping",
            typeof(TextWrapping),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public TextWrapping ItemDescriptionTextWrapping
        {
            get { return (TextWrapping)this.GetValue(ItemDescriptionTextWrappingProperty); }
            set
            {
                this.SetValue(ItemDescriptionTextWrappingProperty, value);
            }
        }

        #endregion  ItemDescriptionTextWrapping


        #region DropdownWidth

        /// <summary>
        /// Identifies the <see cref="DropdownWidth"/> dependency property. 
        /// Sets the width of the dropdown list. Default value is "0" which means this property will not be used.
        /// </summary>
        public static readonly DependencyProperty DropdownWidthProperty = DependencyProperty.Register("DropdownWidth",
            typeof(double),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(0.0));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public double DropdownWidth
        {
            get { return (double)this.GetValue(DropdownWidthProperty); }
            set
            {
                this.SetValue(DropdownWidthProperty, value);
            }
        }

        #endregion  DropdownWidth


        #region MaxDropDownHeight

        /// <summary>
        /// Identifies the <see cref="MaxDropDownHeight"/> dependency property. 
        /// Sets the width of the dropdown list. Default value is "0" which means this property will not be used.
        /// </summary>
        public static readonly DependencyProperty MaxDropDownHeightProperty = DependencyProperty.Register("MaxDropDownHeight",
            typeof(double),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(0.0));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public double MaxDropDownHeight
        {
            get { return (double)this.GetValue(MaxDropDownHeightProperty); }
            set
            {
                this.SetValue(MaxDropDownHeightProperty, value);
            }
        }

        #endregion  MaxDropDownHeight


        #region AllowDropDownResizing

        /// <summary>
        /// Identifies the <see cref="AllowDropDownResizing"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty AllowDropDownResizingProperty = DependencyProperty.Register("AllowDropDownResizing",
            typeof(bool),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(false));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public bool AllowDropDownResizing
        {
            get { return (bool)this.GetValue(AllowDropDownResizingProperty); }
            set
            {
                this.SetValue(AllowDropDownResizingProperty, value);
            }
        }

        #endregion  AllowDropDownResizing


        #region AllowMultipleSelection

        /// <summary>
        /// Identifies the <see cref="AllowMultipleSelection"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty AllowMultipleSelectionProperty = DependencyProperty.Register("AllowMultipleSelection",
            typeof(bool),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(false));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public bool AllowMultipleSelection
        {
            get { return (bool)this.GetValue(AllowMultipleSelectionProperty); }
            set
            {
                this.SetValue(AllowMultipleSelectionProperty, value);
            }
        }

        #endregion  AllowMultipleSelection


        #region CheckBoxVisibility

        /// <summary>
        /// Identifies the <see cref="CheckBoxVisibility"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty CheckBoxVisibilityProperty = DependencyProperty.Register("CheckBoxVisibility",
            typeof(Visibility),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(Visibility.Collapsed));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public Visibility CheckBoxVisibility
        {
            get { return (Visibility)this.GetValue(CheckBoxVisibilityProperty); }
            set
            {
                this.SetValue(CheckBoxVisibilityProperty, value);
            }
        }

        private static void CheckBoxVisibilityChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
            col.OnPropertyChanged("CheckBoxVisibility");
        }


        #endregion  CheckBoxVisibility


        #region SelectedItemsResetButtonVisibility

        /// <summary>
        /// Identifies the <see cref="SelectedItemsResetButtonVisibility"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty SelectedItemsResetButtonVisibilityProperty = DependencyProperty.Register("SelectedItemsResetButtonVisibility",
            typeof(Visibility),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(Visibility.Collapsed));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public Visibility SelectedItemsResetButtonVisibility
        {
            get { return (Visibility)this.GetValue(SelectedItemsResetButtonVisibilityProperty); }
            set
            {
                this.SetValue(SelectedItemsResetButtonVisibilityProperty, value);
            }
        }

        private static void SelectedItemsResetButtonVisibilityChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
            col.OnPropertyChanged("SelectedItemsResetButtonVisibility");
        }


        #endregion  SelectedItemsResetButtonVisibility








        #region AllowConditionalFormating

        /// <summary>
        /// Identifies the <see cref="AllowConditionalFormating"/> dependency property. 
        /// If True, then Conditional Formating is alloed,  - HOWEVER:  Text used in the DisplayName or Description properties must be valid for the XML parser.
        /// This text will be used in a XML data template and illegal XML charecters will make the application crash.
        /// </summary>
        public static readonly DependencyProperty AllowConditionalFormatingProperty = DependencyProperty.Register("AllowConditionalFormating",
            typeof(bool),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(false));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public bool AllowConditionalFormating
        {
            get { return (bool)this.GetValue(AllowConditionalFormatingProperty); }
            set
            {
                this.SetValue(AllowConditionalFormatingProperty, value);
            }
        }

        //private static void AllowConditionalFormatingChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
        //    col.OnPropertyChanged("AllowConditionalFormating");
        //}

        #endregion  AllowConditionalFormating









        #region ToolTip Properties

        #region HeaderToolTipCaption

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipCaptionProperty = DependencyProperty.Register("HeaderToolTipCaption",
            typeof(String),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(""));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public override String HeaderToolTipCaption
        {
            get { return (String)this.GetValue(HeaderToolTipCaptionProperty); }
            set
            {
                try
                {

                    this.SetValue(HeaderToolTipCaptionProperty, value);

                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipCaptionChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipCaption");
        //}

        #endregion HeaderToolTipCaption


        #region HeaderToolTipStringArray

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipStringArrayProperty = DependencyProperty.Register("HeaderToolTipStringArray",
            typeof(String[]),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a string array used to generate a collectin for the tooltip content
        /// </summary>
        public override String[] HeaderToolTipStringArray
        {
            get { return (String[])this.GetValue(HeaderToolTipStringArrayProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderToolTipStringArrayProperty, value);

                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipStringArrayChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipStringArray");
        //}

        #endregion HeaderToolTipStringArray


        #region HeaderToolTipItemsSource

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipItemsSourceProperty = DependencyProperty.Register("HeaderToolTipItemsSource",
            typeof(IEnumerable),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public override IEnumerable HeaderToolTipItemsSource
        {
            get { return (IEnumerable)this.GetValue(HeaderToolTipItemsSourceProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderToolTipItemsSourceProperty, value);

                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipItemsSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipItemsSource");
        //}

        #endregion HeaderToolTipItemsSource


        #region HeaderTooltipContent

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderTooltipContentProperty = DependencyProperty.Register("HeaderTooltipContent",
            typeof(IvTooltipContent),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public override IvTooltipContent HeaderTooltipContent
        {
            get { return (IvTooltipContent)this.GetValue(HeaderTooltipContentProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderTooltipContentProperty, value);

                    StringBuilder xmlCode = new StringBuilder();
                    xmlCode.AppendLine("<DataTemplate xmlns='http://schemas.microsoft.com/client/2007' xmlns:vGridCol='clr-namespace:vControls;assembly=vControlsGridColumns'>");
                    xmlCode.AppendLine("    <vGridCol:vColumnHeaderGrid  >");
                    //xmlCode.AppendLine("        <TextBlock Text=\"{Binding}\"/>");
                    xmlCode.AppendLine("        <TextBlock Text=\"" + this.HeaderText + "\"/>");
                    xmlCode.AppendLine("    </vGridCol:vColumnHeaderGrid>");
                    xmlCode.AppendLine("</DataTemplate>");
                    this.HeaderTemplate = (DataTemplate)XamlReader.Load(xmlCode.ToString());


                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderTooltipContentChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
        //    col.OnPropertyChanged("HeaderTooltipContent");
        //}

        #endregion HeaderTooltipContent

        #endregion ToolTip Properties


        #region Foreground

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty ForegroundProperty = DependencyProperty.Register("Foreground",
            typeof(Brush),
            typeof(vXamMultiColumnComboColumn),
            new PropertyMetadata(new SolidColorBrush(SystemColors.ControlTextColor)));
        //new PropertyMetadata(new PropertyChangedCallback(ForegroundChanged)));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public Brush Foreground
        {
            get { return (Brush)this.GetValue(ForegroundProperty); }
            set { this.SetValue(ForegroundProperty, value); }
        }

        //private static void ForegroundChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
        //    col.OnPropertyChanged("Foreground");

        //}

        #endregion Foreground


        #endregion Properties


        #region Events

        protected internal void OnSelectionChanged(vControls.vXamMultiColumnComboEditor sender, vXamMultiColumnComboColumnSelectionChangedEventArgs e)
        {
            try
            {
                if (SelectionChanged != null)
                {
                    this.SelectionChanged(sender, e);
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected internal void OnKeyUp(vControls.vXamMultiColumnComboEditor sender, vXamComboColumnKeyEventArgs e)
        {
            try
            {
                if (KeyUp != null)
                {
                    this.KeyUp(sender, e);
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected internal void OnKeyDown(vControls.vXamMultiColumnComboEditor sender, vXamComboColumnKeyEventArgs e)
        {
            try
            {
                if (KeyDown != null)
                {
                    this.KeyDown(sender, e);
                }
            }
            catch (Exception ex)
            {

            }
        }


        #endregion Events


        #region Overrides

        /// <summary>
        /// Generates a new <see cref="vXamMultiColumnComboColumnContentProvider"/> that will be used to generate content for <see cref="Cell"/> objects for this <see cref="Column"/>.
        /// </summary>
        /// <returns></returns>
        protected override Infragistics.Controls.Grids.Primitives.ColumnContentProviderBase GenerateContentProvider()
        {
            return new vXamMultiColumnComboColumnContentProvider();
        }
 
        #endregion Overrides


    }



    #region SortComparers

    public static class vXamMultiColumnComboColumn_SortComparer
    {
        public static int Compare(object x, object y, ComboItemList translateList, vXamMultiColumnComboColumn column)
        {
            try
            {
                string s1;
                string s2;

                //Null checks here    
                if (x == null && y == null)
                {
                    return 0;
                }

                if (x == null)
                {
                    return -1;
                }

                if (y == null)
                {
                    return 1;
                }
                //End of the null checks

                // CachedList will not be null ONLY if this is not a child combo

                switch (translateList.Id_DataType)
                {
                    case ComboItemList.DataType.IntegerType:
                        if (translateList.CachedList.Count == 0)
                        {
                            s1 = (from ci in translateList
                                  where (int)ci.Id == (int)x
                                  select ci.ItemName).FirstOrDefault().ToString();

                            s2 = (from ci in translateList
                                  where (int)ci.Id == (int)y
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        else
                        {
                            s1 = (from ci in translateList.CachedList
                                  where (int)ci.Id == (int)x
                                  select ci.ItemName).FirstOrDefault().ToString();

                            s2 = (from ci in translateList.CachedList
                                  where (int)ci.Id == (int)y
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        return String.Compare(s1, s2);


                    case ComboItemList.DataType.GuidType:
                        if (translateList.CachedList.Count == 0)
                        {
                            s1 = (from ci in translateList
                                  where (Guid)ci.Id == (Guid)x
                                  select ci.ItemName).FirstOrDefault().ToString();

                            s2 = (from ci in translateList
                                  where (Guid)ci.Id == (Guid)y
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        else
                        {
                            s1 = (from ci in translateList.CachedList
                                  where (Guid)ci.Id == (Guid)x
                                  select ci.ItemName).FirstOrDefault().ToString();

                            s2 = (from ci in translateList.CachedList
                                  where (Guid)ci.Id == (Guid)y
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        return String.Compare(s1, s2);
                    case ComboItemList.DataType.StringType:


                        if (translateList.CachedList.Count == 0)
                        {
                            s1 = (from ci in translateList
                                  where (string)ci.Id == (string)x
                                  select ci.ItemName).FirstOrDefault().ToString();

                            s2 = (from ci in translateList
                                  where (string)ci.Id == (string)y
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        else
                        {
                            s1 = (from ci in translateList.CachedList
                                  where (string)ci.Id == (string)x
                                  select ci.ItemName).FirstOrDefault().ToString();

                            s2 = (from ci in translateList.CachedList
                                  where (string)ci.Id == (string)y
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        return String.Compare(s1, s2);
                    default:
                        return 0;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class vXamMultiColumnComboColumn_IntSortComparer : IComparer<int?>
    {
        public ComboItemList TranslateList;
        private vXamMultiColumnComboColumn _col;

        public vXamMultiColumnComboColumn_IntSortComparer(ComboItemList list, vXamMultiColumnComboColumn col)
        {
            TranslateList = list;
            _col = col;

        }
        public int Compare(int? x, int? y)
        {
            try
            {
                return vXamMultiColumnComboColumn_SortComparer.Compare(x, y, TranslateList, _col);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class vXamMultiColumnComboColumn_GuidSortComparer : IComparer<Guid>
    {
        public ComboItemList TranslateList;
        private vXamMultiColumnComboColumn _col;

        public vXamMultiColumnComboColumn_GuidSortComparer(ComboItemList list, vXamMultiColumnComboColumn col)
        {
            TranslateList = list;
            _col = col;

        }
        public int Compare(Guid x, Guid y)
        {
            try
            {
                return vXamMultiColumnComboColumn_SortComparer.Compare(x, y, TranslateList, _col);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class vXamMultiColumnComboColumn_StringSortComparer : IComparer<string>
    {
        public ComboItemList TranslateList;
        private vXamMultiColumnComboColumn _col;

        public vXamMultiColumnComboColumn_StringSortComparer(ComboItemList list, vXamMultiColumnComboColumn col)
        {
            TranslateList = list;
            _col = col;

        }
        public int Compare(string x, string y)
        {
            try
            {
                return vXamMultiColumnComboColumn_SortComparer.Compare(x, y, TranslateList, _col);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }


    #endregion SortComparers




}