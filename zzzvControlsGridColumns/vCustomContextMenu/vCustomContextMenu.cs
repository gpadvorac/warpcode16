﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Controls.Menus;

namespace vControls.vCustomContextMenu
{
    public class vCustomContextMenu : XamContextMenu
    {
        protected override DependencyObject GetContainerForItemOverride()
        {
            var item = new vCustomMenuItem();

            var commandBinding = new Binding("Command");
            item.SetBinding(XamMenuItem.CommandProperty, commandBinding);

            var commandParameter = new Binding("CommandParameter");
            item.SetBinding(XamMenuItem.CommandParameterProperty, commandParameter);

            var image = new Image();
            image.SetBinding(Image.SourceProperty, new Binding("ImageSource"));
            item.Icon = image;

            //var items = new Binding("Items");
            //item.SetBinding(XamMenuItem.Item, commandParameter);

            return item;
        }
    }
}