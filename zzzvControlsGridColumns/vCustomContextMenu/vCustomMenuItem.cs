﻿using System.Windows;
using System.Windows.Data;
using Infragistics.Controls.Menus;

namespace vControls.vCustomContextMenu
{
    public class vCustomMenuItem : XamMenuItem
    {
        protected override DependencyObject GetContainerForItemOverride()
        {
            var item = new vCustomMenuItem();

            var commandBinding = new Binding("Command");
            item.SetBinding(CommandProperty, commandBinding);

            return item;
        }
    }
}