using System.Windows.Input;

namespace vControls.vCustomContextMenu
{
    public class MenuItemModel
    {
        public string Name { get; set; }
        public string HeaderText { get; set; }
        public ICommand Command { get; set; }
        public string ImageSource { get; set; }
        public object CommandParameter { get; set; }
    }
}