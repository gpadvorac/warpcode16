﻿using System;
using System.Windows;
using System.Windows.Controls;
using Infragistics.Controls.Grids;
using vControls;
using Infragistics;
using System.Windows.Markup;
using System.Collections;
using vTooltipProvider;
using System.Text;
using System.Windows.Input;
using System.Windows.Media;

namespace vControls
{
    public class vStarRatingColumn : EditableColumn, IvColumn
    {


        #region Initialize Variables

        public event EventHandler<vStarRatingColumnTextChangedEventArgs> StarRatingChanged;

        #endregion Initialize Variables


        #region Constructor

        public vStarRatingColumn()
        {
            //FilterColumnSettings fcs = this.FilterColumnSettings;
            //RowsFilter rf = new RowsFilter(typeof(String), this);
            //ComparisonCondition compCond = new ComparisonCondition();
            //compCond.Operator = ComparisonOperator.Contains;
            ////compCond.FilterValue = 15;
            ////Add condition to the RowFilter
            //rf.Conditions.Add(compCond);
            ////this.dataGrid.FilteringSettings.RowFiltersCollection.Add(rf);


            //this.h

            //HeaderTemplate = "xxx";

        }

        #endregion Constructor


        #region Properties


        #region NumberOfStarsProperty

        public static readonly DependencyProperty NumberOfStarsProperty =
            DependencyProperty.Register(
                "NumberOfStars", typeof (int),
                typeof (vStarRatingColumn), new PropertyMetadata(5, new PropertyChangedCallback(OnNumberOfStarsChanged)));

        public int NumberOfStars
        {
            get { return (int) GetValue(NumberOfStarsProperty); }
            set { SetValue(NumberOfStarsProperty, value); }
        }

        private static void OnNumberOfStarsChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            vStarRatingColumn col = (vStarRatingColumn) sender;
            col.OnPropertyChanged("NumberOfStarsProperty");
        }

        #endregion NumberOfStarsProperty




        #region StarControlWidthProperty

        public static readonly DependencyProperty StarControlWidthProperty =
            DependencyProperty.Register(
                "StarControlWidth", typeof(int),
                typeof(vStarRatingColumn), new PropertyMetadata(60, new PropertyChangedCallback(OnStarControlWidthChanged)));

        public int StarControlWidth
        {
            get { return (int)GetValue(StarControlWidthProperty); }
            set { SetValue(StarControlWidthProperty, value); }
        }

        private static void OnStarControlWidthChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            vStarRatingColumn col = (vStarRatingColumn)sender;
            col.OnPropertyChanged("StarControlWidthProperty");
        }

        #endregion StarControlWidthProperty



        #region StarControlHeightProperty

        public static readonly DependencyProperty StarControlHeightProperty =
            DependencyProperty.Register(
                "StarControlHeight", typeof(int),
                typeof(vStarRatingColumn), new PropertyMetadata(16, new PropertyChangedCallback(OnStarControlHeightChanged)));

        public int StarControlHeight
        {
            get { return (int)GetValue(StarControlHeightProperty); }
            set { SetValue(StarControlHeightProperty, value); }
        }

        private static void OnStarControlHeightChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            vStarRatingColumn col = (vStarRatingColumn)sender;
            col.OnPropertyChanged("StarControlHeightProperty");
        }

        #endregion StarControlHeightProperty




        #region MaxRatingValueProperty

        /// <summary>
        /// Max Rating is 5 or 10
        /// Note:  When Rating is 5, you must divide the Star Rating output by 2 as the internal star values always range from 1 to 10
        /// </summary>
        public static readonly DependencyProperty MaxRatingValueProperty =
            DependencyProperty.Register(
                "MaxRating", typeof (int),
                typeof (vStarRatingColumn), new PropertyMetadata(5, new PropertyChangedCallback(OnMaxRatingChanged)));

        public int MaxRating
        {
            get { return (int) GetValue(MaxRatingValueProperty); }
            set
            {
                SetValue(MaxRatingValueProperty, value);
                //if (value == 10)
                //{
                //    SetValue(MaxRatingValueProperty, 10);
                //}
                //else
                //{
                //    SetValue(MaxRatingValueProperty, 5);
                //}
            }
        }

        private static void OnMaxRatingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            vStarRatingColumn col = (vStarRatingColumn) sender;
            col.OnPropertyChanged("MaxRatingValueProperty");
            //vStarRating maxRating = (vStarRating)sender;
            //maxRating.CreateStars();
            //maxRating.RefreshStarRating();
        }

        #endregion MaxRatingValueProperty

        #region RatingProperty

        public static readonly DependencyProperty RatingProperty = DependencyProperty.Register(
            "Rating", typeof(int),
            typeof(vStarRating), new PropertyMetadata(0, new PropertyChangedCallback(OnRatingChanged)));

        public int Rating
        {
            get { return (int)GetValue(RatingProperty); }
            set
            {
                SetValue(RatingProperty, value);
            }
        }

        private static void OnRatingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            vStarRatingColumn col = (vStarRatingColumn)sender;
            col.OnPropertyChanged("RatingProperty");
            //var src = (vStarRatingColumn)sender;
            //src.DrawUnhovered();
        }

        #endregion

        #region HoverRatingProperty

        public static readonly DependencyProperty HoverRatingProperty = DependencyProperty.Register(
            "HoverRating", typeof(int),
            typeof(vStarRatingColumn), new PropertyMetadata(0, new PropertyChangedCallback(OnHoverRatingChanged)));

        public int HoverRating
        {
            get { return (int)GetValue(HoverRatingProperty); }
            set { SetValue(HoverRatingProperty, value); }
        }

        private static void OnHoverRatingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            vStarRatingColumn col = (vStarRatingColumn)sender;
            col.OnPropertyChanged("HoverRatingProperty");
            //var src = (vStarRatingColumn)sender;
            //src.RefreshStarRating();
        }

        #endregion

        #region StarFillBrushProperty

        public static readonly DependencyProperty StarFillBrushProperty = DependencyProperty.Register(
            "StarFillBrush", typeof(Brush),
            typeof(vStarRatingColumn), new PropertyMetadata(new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0xFF, 0)),
                new PropertyChangedCallback(StarFillBrushChanged)));

        public Brush StarFillBrush
        {
            get { return (Brush)GetValue(StarFillBrushProperty); }
            set { SetValue(StarFillBrushProperty, value); }
        }

        private static void StarFillBrushChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            vStarRatingColumn col = (vStarRatingColumn)sender;
            col.OnPropertyChanged("StarFillBrushProperty");
            //var starRating = (vStarRatingColumn)sender;
            //starRating.RefreshStarRating();
        }

        #endregion

        #region UnselectedStarFillBrushProperty

        public static readonly DependencyProperty UnselectedStarFillBrushProperty = DependencyProperty.Register(
            "UnselectedStarFillBrush", typeof(Brush),
            typeof(vStarRatingColumn), new PropertyMetadata(null,
                new PropertyChangedCallback(UnselectedStarFillBrushChanged)));

        public Brush UnselectedStarFillBrush
        {
            get { return (Brush)GetValue(UnselectedStarFillBrushProperty); }
            set { SetValue(UnselectedStarFillBrushProperty, value); }
        }

        private static void UnselectedStarFillBrushChanged(DependencyObject sender,
            DependencyPropertyChangedEventArgs args)
        {
            vStarRatingColumn col = (vStarRatingColumn)sender;
            col.OnPropertyChanged("UnselectedStarFillBrushProperty");
            //var starRating = (vStarRatingColumn)sender;
            //starRating.RefreshStarRating();
        }

        #endregion

        #region StarOutlineBrushProperty

        public static readonly DependencyProperty StarOutlineBrushProperty = DependencyProperty.Register(
            "StarOutlineBrush", typeof(Brush),
            typeof(vStarRatingColumn), new PropertyMetadata(new SolidColorBrush(Color.FromArgb(0xFF, 0xC0, 0xC0, 0)),
                new PropertyChangedCallback(StarFillBrushChanged)));

        public Brush StarOutlineBrush
        {
            get { return (Brush)GetValue(StarOutlineBrushProperty); }
            set { SetValue(StarOutlineBrushProperty, value); }
        }

        private static void StarOutlineBrushChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            vStarRatingColumn col = (vStarRatingColumn)sender;
            col.OnPropertyChanged("StarOutlineBrushProperty");
            //var starRating = (vStarRatingColumn)sender;
            //starRating.RefreshStarRating();
        }

        #endregion

        #region HoverFillBrushProperty

        public static readonly DependencyProperty HoverFillBrushProperty = DependencyProperty.Register(
            "HoverFillBrush", typeof(Brush),
            typeof(vStarRatingColumn),
            new PropertyMetadata(new SolidColorBrush(Color.FromArgb(0xFF, 0xF0, 0xF0, 0x80)),
                new PropertyChangedCallback(HoverFillBrushChanged)));

        public Brush HoverFillBrush
        {
            get { return (Brush)GetValue(HoverFillBrushProperty); }
            set { SetValue(HoverFillBrushProperty, value); }
        }

        private static void HoverFillBrushChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            vStarRatingColumn col = (vStarRatingColumn)sender;
            col.OnPropertyChanged("HoverFillBrushProperty");
            //var starRating = (vStarRatingColumn)sender;
            //starRating.RefreshStarRating();
        }

        #endregion

        #region UnselectedHoverFillBrushProperty

        public static readonly DependencyProperty UnselectedHoverFillBrushProperty = DependencyProperty.Register(
            "UnselectedHoverFillBrush", typeof(Brush),
            typeof(vStarRatingColumn), new PropertyMetadata(null,
                new PropertyChangedCallback(UnselectedHoverFillBrushChanged)));

        public Brush UnselectedHoverFillBrush
        {
            get { return (Brush)GetValue(UnselectedHoverFillBrushProperty); }
            set { SetValue(UnselectedHoverFillBrushProperty, value); }
        }

        private static void UnselectedHoverFillBrushChanged(DependencyObject sender,
            DependencyPropertyChangedEventArgs args)
        {
            vStarRatingColumn col = (vStarRatingColumn)sender;
            col.OnPropertyChanged("UnselectedHoverFillBrushProperty");
            //var starRating = (vStarRatingColumn)sender;
            //starRating.RefreshStarRating();
        }

        #endregion

        #region HoverOutlineBrushProperty

        public static readonly DependencyProperty HoverOutlineBrushProperty = DependencyProperty.Register(
            "HoverOutlineBrush", typeof(Brush),
            typeof(vStarRatingColumn),
            new PropertyMetadata(new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0x00)),
                new PropertyChangedCallback(HoverOutlineBrushChanged)));

        public Brush HoverOutlineBrush
        {
            get { return (Brush)GetValue(HoverOutlineBrushProperty); }
            set { SetValue(HoverOutlineBrushProperty, value); }
        }

        private static void HoverOutlineBrushChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            vStarRatingColumn col = (vStarRatingColumn)sender;
            col.OnPropertyChanged("HoverOutlineBrushProperty");
            //var starRating = (vStarRatingColumn)sender;
            //starRating.RefreshStarRating();
        }

        #endregion

        #region StrokeThicknessProperty

        public static readonly DependencyProperty StrokeThicknessProperty = DependencyProperty.Register(
            "StrokeThickness", typeof(double),
            typeof(vStarRatingColumn), new PropertyMetadata(2.0));

        public double StrokeThickness
        {
            get { return (double)GetValue(StrokeThicknessProperty); }
            set { SetValue(StrokeThicknessProperty, value); }
        }

        #endregion

        #region StrokeLineJoinProperty

        public static readonly DependencyProperty StrokeLineJoinProperty = DependencyProperty.Register(
            "StrokeLineJoin", typeof(PenLineJoin),
            typeof(vStarRatingColumn), new PropertyMetadata(PenLineJoin.Round));

        public PenLineJoin StrokeLineJoin
        {
            get { return (PenLineJoin)GetValue(StrokeLineJoinProperty); }
            set { SetValue(StrokeLineJoinProperty, value); }
        }

        #endregion



        #region ValidationState

        public static readonly DependencyProperty ValidStateAppearanceProperty = DependencyProperty.Register(
            "ValidStateAppearance",
            typeof (string),
            typeof (vStarRatingColumn),
            new PropertyMetadata(new PropertyChangedCallback(ValidStateAppearanceChanged)));


        private static void ValidStateAppearanceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vStarRatingColumn col = (vStarRatingColumn) obj;
            col.OnPropertyChanged("ValidStateAppearance");
        }


        public string ValidStateAppearance
        {
            get { return (string) GetValue(ValidStateAppearanceProperty); }
            set { SetValue(ValidStateAppearanceProperty, value); }
        }

        #endregion ValidationState

        #region ToolTip Properties

        #region HeaderToolTipCaption

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipCaptionProperty =
            DependencyProperty.Register("HeaderToolTipCaption",
                typeof (String),
                typeof (vStarRatingColumn),
                new PropertyMetadata(""));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public String HeaderToolTipCaption
        {
            get { return (String) this.GetValue(HeaderToolTipCaptionProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderToolTipCaptionProperty, value);
                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipCaptionChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vStarRatingColumn col = (vStarRatingColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipCaption");
        //}

        #endregion HeaderToolTipCaption

        #region HeaderToolTipStringArray

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipStringArrayProperty =
            DependencyProperty.Register("HeaderToolTipStringArray",
                typeof (String[]),
                typeof (vStarRatingColumn),
                new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a string array used to generate a collectin for the tooltip content
        /// </summary>
        public String[] HeaderToolTipStringArray
        {
            get { return (String[]) this.GetValue(HeaderToolTipStringArrayProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderToolTipStringArrayProperty, value);
                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipStringArrayChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vStarRatingColumn col = (vStarRatingColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipStringArray");
        //}

        #endregion HeaderToolTipStringArray

        #region HeaderToolTipItemsSource

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipItemsSourceProperty =
            DependencyProperty.Register("HeaderToolTipItemsSource",
                typeof (IEnumerable),
                typeof (vStarRatingColumn),
                new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public IEnumerable HeaderToolTipItemsSource
        {
            get { return (IEnumerable) this.GetValue(HeaderToolTipItemsSourceProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderToolTipItemsSourceProperty, value);
                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipItemsSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vStarRatingColumn col = (vStarRatingColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipItemsSource");
        //}

        #endregion HeaderToolTipItemsSource

        #region HeaderTooltipContent

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderTooltipContentProperty =
            DependencyProperty.Register("HeaderTooltipContent",
                typeof (IvTooltipContent),
                typeof (vStarRatingColumn),
                new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public IvTooltipContent HeaderTooltipContent
        {
            get { return (IvTooltipContent) this.GetValue(HeaderTooltipContentProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderTooltipContentProperty, value);

                    StringBuilder xmlCode = new StringBuilder();
                    xmlCode.AppendLine(
                        "<DataTemplate xmlns='http://schemas.microsoft.com/client/2007' xmlns:vGridCol='clr-namespace:vControls;assembly=vControlsGridColumns'>");
                    xmlCode.AppendLine("    <vGridCol:vColumnHeaderGrid  >");
                    //xmlCode.AppendLine("        <TextBlock Text=\"{Binding}\"/>");
                    xmlCode.AppendLine("        <TextBlock Text=\"" + this.HeaderText + "\"/>");
                    xmlCode.AppendLine("    </vGridCol:vColumnHeaderGrid>");
                    xmlCode.AppendLine("</DataTemplate>");
                    this.HeaderTemplate = (DataTemplate) XamlReader.Load(xmlCode.ToString());
                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderTooltipContentChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vStarRatingColumn col = (vStarRatingColumn)obj;
        //    col.OnPropertyChanged("HeaderTooltipContent");
        //}

        #endregion HeaderTooltipContent

        #endregion ToolTip Properties

        #endregion Properties



        #region Events


        protected internal void OnStarRatingChanged(vStarRating sender, vStarRatingColumnTextChangedEventArgs e)
        {
            if (StarRatingChanged != null)
            {
                this.StarRatingChanged(sender, e);
            }
        }


        #endregion Events




        #region Overrides

        /// <summary>
        /// Generates a new <see cref="DecimalColumnContentProvider"/> that will be used to generate content for <see cref="Cell"/> objects for this <see cref="Column"/>.
        /// </summary>
        /// <returns></returns>
        protected override Infragistics.Controls.Grids.Primitives.ColumnContentProviderBase GenerateContentProvider()
        {
            return new vStarRatingColumnContentProvider();
        }

        protected override void FillAvailableFilters(FilterOperandCollection availableFilters)
        {
            try
            {
                base.FillAvailableFilters(availableFilters);
                this.FilterColumnSettings.FilterMenuCustomFilteringButtonVisibility = Visibility.Collapsed;

                //availableFilters.Add(new EqualsOperand());
                //availableFilters.Add(new NotEqualsOperand());
                //availableFilters.Add(new GreaterThanOperand());
                //availableFilters.Add(new GreaterThanOrEqualOperand());
                //availableFilters.Add(new LessThanOperand());
                //availableFilters.Add(new LessThanOrEqualOperand());


                ////if (this.DataType != null)
                ////{
                ////    if (this.DataType == typeof(decimal))
                ////    {
                ////        availableFilters.Add(new EqualsOperand());
                ////        availableFilters.Add(new NotEqualsOperand());
                ////        availableFilters.Add(new GreaterThanOperand());
                ////        availableFilters.Add(new GreaterThanOrEqualOperand());
                ////        availableFilters.Add(new LessThanOperand());
                ////        availableFilters.Add(new LessThanOrEqualOperand());

                ////    }
                ////    else if (this.DataType.IsValueType)
                ////    {
                ////        /* Hack solution until we have columntypes to handle numbers or other datatypes.*/


                ////        availableFilters.Add(new EqualsOperand());
                ////        availableFilters.Add(new NotEqualsOperand());
                ////        availableFilters.Add(new GreaterThanOperand());
                ////        availableFilters.Add(new LessThanOperand());
                ////        availableFilters.Add(new StartsWithOperand());
                ////        availableFilters.Add(new ContainsOperand());
                ////        availableFilters.Add(new EndsWithOperand());
                ////        availableFilters.Add(new DoesNotContainOperand());
                ////        availableFilters.Add(new DoesNotStartWithOperand());
                ////        availableFilters.Add(new DoesNotEndWithOperand());
                ////    }
                ////}
            }
            catch (Exception ex)
            {

            }
        }


        FilterOperand _defaultFilterOperand;
        protected override FilterOperand DefaultFilterOperand
        {
            get
            {
                if (this.DataType == typeof(string))
                {
                    if (this._defaultFilterOperand == null)
                        this._defaultFilterOperand = new ContainsOperand();
                    return this._defaultFilterOperand;
                }
                return base.DefaultFilterOperand;
            }
        }


        #endregion Overrides




    }
}
