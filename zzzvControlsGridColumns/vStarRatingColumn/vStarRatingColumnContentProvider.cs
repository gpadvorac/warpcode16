﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Controls.Grids.Primitives;
using Infragistics.Controls.Grids;
using System.Diagnostics;
using TypeServices;


namespace vControls
{
    public class vStarRatingColumnContentProvider : ColumnContentProviderBase
    {

        #region Initialize Variables

        vStarRating _vStarRatingDisplay;
        vStarRating _vStarRatingEdit;
        vStarRatingColumn _column = null;
        Cell _cell;
        bool _resetBindingSet;


        #endregion Initialize Variables

        #region Constructor

        public vStarRatingColumnContentProvider()
        {
            try
            {
                _vStarRatingDisplay = new vStarRating();
                _vStarRatingDisplay.IsEnabled = false;
                //_vStarRatingDisplay.Margin = new Thickness(0);
                _vStarRatingEdit = new vStarRating();
                //_vStarRatingEdit.Margin = new Thickness(-5);
                _vStarRatingEdit.StarRatingChanged += vStarRating_StarRatingChanged;
                //_vStarRatingEdit.GotFocus += new RoutedEventHandler(vTextBox_GotFocus);
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        #endregion Constructor

        #region Overrides

        #region ResolveDisplayElement

        public override FrameworkElement ResolveDisplayElement(Cell cell, System.Windows.Data.Binding cellBinding)
        {
            try
            {
                _cell = cell;
                _column = (vStarRatingColumn)cell.Column;

                //_vStarRatingDisplay.TextWrapping = _column.TextWrapping;
                _resetBindingSet = cell.Row.RowType == RowType.FilterRow;
                //_vStarRatingDisplay.Foreground = _column.Foreground;
                _vStarRatingDisplay.NumberOfStars = _column.NumberOfStars;
                _vStarRatingDisplay.MaxRating = _column.MaxRating;
                _vStarRatingDisplay.Rating = _column.Rating;
                _vStarRatingDisplay.StrokeThickness = _column.StrokeThickness;
                _vStarRatingDisplay.StrokeLineJoin = _column.StrokeLineJoin;
                _vStarRatingDisplay.StarOutlineBrush = _column.StarOutlineBrush;
                _vStarRatingDisplay.StarFillBrush = _column.StarFillBrush;
                _vStarRatingDisplay.HoverOutlineBrush = _column.HoverOutlineBrush;
                _vStarRatingDisplay.HoverFillBrush = _column.HoverFillBrush;
                _vStarRatingDisplay.Width = _column.StarControlWidth;
                _vStarRatingDisplay.Height = _column.StarControlHeight;



                if (cellBinding != null)
                {

                    cellBinding.ConverterCulture = System.Globalization.CultureInfo.CurrentCulture;
                    //cellBinding.StringFormat = _column.DecimalFormat;
                    _vStarRatingDisplay.SetBinding(vStarRating.RatingProperty, cellBinding);

                }
                else
                {
                    Binding textBinding = new Binding();
                    textBinding.Path = new PropertyPath(_column.Key);
                    textBinding.Mode = BindingMode.TwoWay;

                    //textBinding.StringFormat = _column.DecimalFormat;

                    //I dont think we need this as there is no text to bind and display
                    //_vStarRatingDisplay.SetBinding(vStarRating.RatingProperty, textBinding);


                }
                return _vStarRatingDisplay;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveDisplayElement


        #region ResolveEditorControl

        protected override FrameworkElement ResolveEditorControl(Cell cell, object editorValue, double availableWidth, double availableHeight, System.Windows.Data.Binding editorBinding)
        {
            try
            {
                Binding textBinding = new Binding();
                textBinding.Path = new PropertyPath(_column.Key);
                textBinding.Mode = BindingMode.OneWay;
                _vStarRatingEdit.NumberOfStars = _column.NumberOfStars;
                _vStarRatingEdit.MaxRating = _column.MaxRating;
                _vStarRatingEdit.Rating = _column.Rating;
                _vStarRatingEdit.StrokeThickness = _column.StrokeThickness;
                _vStarRatingEdit.StrokeLineJoin = _column.StrokeLineJoin;
                _vStarRatingEdit.StarOutlineBrush = _column.StarOutlineBrush;
                _vStarRatingEdit.StarFillBrush = _column.StarFillBrush;
                _vStarRatingEdit.HoverOutlineBrush = _column.HoverOutlineBrush;
                _vStarRatingEdit.HoverFillBrush = _column.HoverFillBrush;
                _vStarRatingEdit.Width = _column.StarControlWidth;
                _vStarRatingEdit.Height = _column.StarControlHeight;



                //_vStarRatingDisplay.MaxRating = _column.MaxRating;

                //_vStarRatingEdit.Foreground = _column.Foreground;

                // Added this 2014-05-07 becuase when using German culture and the decimal cell enters edit mode, the value will change from "5,00" (german) to "5.00" (english).
                //    However, "5.00" in german means 5 thousand, so the business object takes the text  five point zero and converts it to five thousand.
                //   Setting the culture here corrects the problem.

                ////I dont think we need the next 3 lines as there is no text to bind and display
                //textBinding.ConverterCulture = System.Globalization.CultureInfo.CurrentCulture;

                //_vStarRatingEdit.SetBinding(vStarRating.RatingProperty, textBinding);
                //_vStarRatingEdit.Style = cell.EditorStyleResolved;


                if (_resetBindingSet == false)
                {
                    //***************  UN REM THESE Lines after this test
                    IBusinessObject obj = cell.Row.Data as IBusinessObject;
                    if (obj == null) { return null; }
                    SetXamGridEditControlValidStateAppearance(_column.Key, _vStarRatingEdit, obj);
                }
                else
                {
                    // Set the text from what it was the last time we entered a value.
                    // so we really need this for the star rating?
                    //_vStarRatingEdit.Rating = _vStarRatingDisplay.Rating;
                    //_vStarRatingEdit.MaxRating = _vStarRatingDisplay.MaxRating;
                }
                return _vStarRatingEdit;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveEditorControl


        #region ResolveValueFromEditor

        public override object ResolveValueFromEditor(Infragistics.Controls.Grids.Cell cell)
        {
            try
            {
                return this._vStarRatingEdit.Rating;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveValueFromEditor


        #endregion Overrides


        #region Event Handlers





        void vStarRating_StarRatingChanged(object sender, StarRatingChangedArgs e)
        {
            try
            {

                if (_vStarRatingEdit.Parent == null) { return; }

                if (_resetBindingSet == false)
                {
                    IBusinessObject data = _cell.Row.Data as IBusinessObject;
                    string key = _cell.Column.Key;
                    int index = _cell.Row.Index;
                    vStarRatingColumnTextChangedEventArgs args = new vStarRatingColumnTextChangedEventArgs(_cell, index, key, data, e);
                    _column.OnStarRatingChanged(_vStarRatingEdit, args);
                }
                else
                {
                    // No need to raise the text changed event here as we dont want to set any values to the business object.
                }
                this.NotifyEditorValueChanged(this._vStarRatingEdit.Rating);
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }




        //void vTextBox_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    try
        //    {

        //        if (_vStarRatingEdit.Parent == null) { return; }

        //        if (_resetBindingSet == false)
        //        {
        //            IBusinessObject data = _cell.Row.Data as IBusinessObject;
        //            string key = _cell.Column.Key;
        //            int index = _cell.Row.Index;
        //            vStarRatingColumnTextChangedEventArgs args = new vStarRatingColumnTextChangedEventArgs(_cell, index, key, data, e);
        //            _column.OnTextChanged(_vStarRatingEdit, args);
        //        }
        //        else
        //        {
        //            // No need to raise the text changed event here as we dont want to set any values to the business object.
        //        }
        //        this.NotifyEditorValueChanged(this._vStarRatingEdit.Text);
        //    }
        //    catch (Exception ex)
        //    {
        //        //Debugger.Break();
        //        //System.Diagnostics.Debug.WriteLine(ex.ToString());
        //    }
        //}

        //void vTextBox_GotFocus(object sender, RoutedEventArgs e)
        //{
        //    //try
        //    //{
        //    //    _vStarRatingEdit.SelectAll();
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    //Debugger.Break();
        //    //    //System.Diagnostics.Debug.WriteLine(ex.ToString());
        //    //}
        //}

        #endregion Event Handlers


        #region Methods

        public static void SetXamGridEditControlValidStateAppearance(string key, vStarRating ctl, IBusinessObject obj)
        {
            try
            {

                //if (obj.IsPropertyValid(key))
                //{
                //    ctl.ValidStateAppearance = ValidationState.Valid;
                //}
                //else
                //{
                //    if (obj.IsPropertyDirty(key))
                //    {
                //        ctl.ValidStateAppearance = ValidationState.NotValidIsDirty;
                //    }
                //    else
                //    {
                //        ctl.ValidStateAppearance = ValidationState.NotValidNotDirty;
                //    }
                //}
            }
            catch (Exception ex)
            {
            
            }
        }

        #endregion Methods



    }




    #region vStarRatingColumnTextChangedEventArgs Text Changed Event

    public class vStarRatingColumnTextChangedEventArgs : EventArgs
    {
        Cell _cell;
        int _index;
        string _key;
        object _data;
        StarRatingChangedArgs _starRatingArgs;

        internal vStarRatingColumnTextChangedEventArgs(Cell cell, int index, string key, object data, StarRatingChangedArgs e)
        {
            _cell = cell;
            _index = index;
            _key = key;
            _data = data;
            _starRatingArgs = e;
        }

        public Cell ContentCell { get { return _cell; } }
        public int Index { get { return _index; } }
        public string Key { get { return _key; } }
        public object Data { get { return _data; } }
        public StarRatingChangedArgs StarRatingArgs { get { return _starRatingArgs; } }

    }

    #endregion vStarRatingColumnTextChangedEventArgs Text Changed Event


}
