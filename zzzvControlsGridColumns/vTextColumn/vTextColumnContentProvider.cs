﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Controls.Grids.Primitives;
using Infragistics.Controls.Grids;
using System.Diagnostics;
using TypeServices;
using System.Windows.Media;

namespace vControls
{
    public class vTextColumnContentProvider : ColumnContentProviderBase
    {

        #region Initialize Variables

        TextBlock _textDisplay;
        vTextBox _vTextBox;
        vTextColumn _column = null;
        Cell _cell;
        bool _resetBindingSet;


        #endregion Initialize Variables


        #region Constructor

        public vTextColumnContentProvider()
        {
            try
            {
                _textDisplay = new TextBlock();
                //_textDisplay.VerticalAlignment = VerticalAlignment.Stretch;
                _vTextBox = new vTextBox();
                //_vTextBox.VerticalAlignment = VerticalAlignment.Stretch;
                _textDisplay.Margin = new Thickness(-1);
                _vTextBox.Margin = new Thickness(-5);
                _vTextBox.TextChanged += new TextChangedEventHandler(vTextBox_TextChanged);
                _vTextBox.GotFocus += new RoutedEventHandler(vTextBox_GotFocus);
                _textDisplay.VerticalAlignment = VerticalAlignment.Top;
                _vTextBox.VerticalContentAlignment = VerticalAlignment.Top;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }


        #endregion Constructor

        #region Overrides

        #region ResolveDisplayElement

        public override FrameworkElement ResolveDisplayElement(Cell cell, System.Windows.Data.Binding cellBinding)
        {
            try
            {
                _cell = cell;
                _column = (vTextColumn)cell.Column;
                _textDisplay.TextWrapping = _column.TextWrapping;
                _textDisplay.Foreground = _column.Foreground;

                _column = (vTextColumn)cell.Column;
                _resetBindingSet = cell.Row.RowType == RowType.FilterRow;

                if (cellBinding != null)
                {
                    cellBinding.StringFormat = _column.TextFormat;
                    _textDisplay.SetBinding(TextBlock.TextProperty, cellBinding);
                }
                else
                {
                    Binding textBinding = new Binding();
                    textBinding.Path = new PropertyPath(_column.Key);
                    textBinding.Mode = BindingMode.TwoWay;
                    textBinding.StringFormat = _column.TextFormat;
                    _textDisplay.SetBinding(TextBlock.TextProperty, textBinding);
                }
                return _textDisplay;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveDisplayElement


        #region ResolveEditorControl

        protected override FrameworkElement ResolveEditorControl(Cell cell, object editorValue, double availableWidth, double availableHeight, System.Windows.Data.Binding editorBinding)
        {
            try
            {
                Binding textBinding = new Binding();
                textBinding.Path = new PropertyPath(_column.Key);
                textBinding.Mode = BindingMode.OneWay;
                textBinding.StringFormat = _column.TextEditorFormat;
                _vTextBox.Foreground = _column.Foreground;
                //textBinding.StringFormat = "#.#";

                //  I think we shoud be using a dedicated decimal column for decimal data types.

                if (_column.HorizontalContentAlignment == HorizontalAlignment.Right)
                {
                    _vTextBox.TextAlignment = TextAlignment.Right;
                }
                else if (_column.HorizontalContentAlignment == HorizontalAlignment.Center)
                {
                    _vTextBox.TextAlignment = TextAlignment.Center;
                }


                _vTextBox.SetBinding(vTextBox.TextProperty, textBinding);
                _vTextBox.Style = cell.EditorStyleResolved;
                _vTextBox.TextWrapping = _column.TextWrapping;
                if (_vTextBox.TextWrapping == TextWrapping.Wrap)
                {
                    _vTextBox.AcceptsReturn = true;
                }
                if (_resetBindingSet == false)
                {
                    IBusinessObject obj = cell.Row.Data as IBusinessObject;
                    if (obj == null) { return null; }
                    SetXamGridEditControlValidStateAppearance(_column.Key, _vTextBox, obj);
                }
                else
                {
                    // Set the text from what it was the last time we entered a value.
                    _vTextBox.Text = _textDisplay.Text;
                }

                if (_column.MaxTextLength > 0)
                {
                    _vTextBox.SetTextLengthControlForXamGrid_vTextColumn((int)_column.MaxTextLength);

                }


                return _vTextBox;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveEditorControl


        #region ResolveValueFromEditor

        public override object ResolveValueFromEditor(Infragistics.Controls.Grids.Cell cell)
        {
            try
            {
                return this._vTextBox.Text;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveValueFromEditor


        #endregion Overrides


        #region Event Handlers

        void vTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {

                if (_vTextBox.Parent == null) { return; }

                if (_resetBindingSet == false)
                {
                    IBusinessObject data = _cell.Row.Data as IBusinessObject;
                    string key = _cell.Column.Key;
                    int index = _cell.Row.Index;
                    vTextColumnTextChangedEventArgs args = new vTextColumnTextChangedEventArgs(_cell, index, key, data, e);
                    _column.OnTextChanged(_vTextBox, args);
                }
                else
                {
                    // No need to raise the text changed event here as we dont want to set any values to the business object.
                }
                this.NotifyEditorValueChanged(this._vTextBox.Text);
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        void vTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                _vTextBox.SelectAll();
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        #endregion Event Handlers


        #region Methods

        public static void SetXamGridEditControlValidStateAppearance(string key, vTextBox ctl, IBusinessObject obj)
        {
            try
            {

                if (obj.IsPropertyValid(key))
                {
                    ctl.ValidStateAppearance = ValidationState.Valid;
                }
                else
                {
                    if (obj.IsPropertyDirty(key))
                    {
                        ctl.ValidStateAppearance = ValidationState.NotValidIsDirty;
                    }
                    else
                    {
                        ctl.ValidStateAppearance = ValidationState.NotValidNotDirty;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        #endregion Methods



    }
}
