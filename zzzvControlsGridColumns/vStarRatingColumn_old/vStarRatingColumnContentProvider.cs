﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Controls.Grids.Primitives;
using Infragistics.Controls.Grids;
using System.Diagnostics;
using TypeServices;


namespace vControls
{
    public class vStarRatingColumnContentProvider : ColumnContentProviderBase
    {

        #region Initialize Variables

        vStarRating _textDisplay;
        vStarRating _vTextBox;
        vStarRatingColumn _column = null;
        Cell _cell;
        bool _resetBindingSet;


        #endregion Initialize Variables

        #region Constructor

        public vStarRatingColumnContentProvider()
        {
            try
            {
                _textDisplay = new vStarRating();
                _textDisplay.Margin = new Thickness(0);
                _vTextBox = new vStarRating();
                _vTextBox.Margin = new Thickness(-5);
                _vTextBox.StarRatingChanged += vStarRating_StarRatingChanged;
                //_vTextBox.GotFocus += new RoutedEventHandler(vTextBox_GotFocus);
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        #endregion Constructor

        #region Overrides

        #region ResolveDisplayElement

        public override FrameworkElement ResolveDisplayElement(Cell cell, System.Windows.Data.Binding cellBinding)
        {
            try
            {
                _cell = cell;
                _column = (vStarRatingColumn)cell.Column;

                //_textDisplay.TextWrapping = _column.TextWrapping;
                _resetBindingSet = cell.Row.RowType == RowType.FilterRow;
                //_textDisplay.Foreground = _column.Foreground;

                if (cellBinding != null)
                {

                    cellBinding.ConverterCulture = System.Globalization.CultureInfo.CurrentCulture;
                    //cellBinding.StringFormat = _column.DecimalFormat;
                    _textDisplay.SetBinding(vStarRating.RatingProperty, cellBinding);

                }
                else
                {
                    Binding textBinding = new Binding();
                    textBinding.Path = new PropertyPath(_column.Key);
                    textBinding.Mode = BindingMode.TwoWay;

                    //textBinding.StringFormat = _column.DecimalFormat;


                    _textDisplay.SetBinding(vStarRating.RatingProperty, textBinding);


                }
                return _textDisplay;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveDisplayElement


        #region ResolveEditorControl

        protected override FrameworkElement ResolveEditorControl(Cell cell, object editorValue, double availableWidth, double availableHeight, System.Windows.Data.Binding editorBinding)
        {
            try
            {
                Binding textBinding = new Binding();
                textBinding.Path = new PropertyPath(_column.Key);
                textBinding.Mode = BindingMode.OneWay;
                //_vTextBox.Foreground = _column.Foreground;

                // Added this 2014-05-07 becuase when using German culture and the decimal cell enters edit mode, the value will change from "5,00" (german) to "5.00" (english).
                //    However, "5.00" in german means 5 thousand, so the business object takes the text  five point zero and converts it to five thousand.
                //   Setting the culture here corrects the problem.
                textBinding.ConverterCulture = System.Globalization.CultureInfo.CurrentCulture;
                //textBinding.StringFormat = "#.####################";

                //if (_column.HorizontalContentAlignment == HorizontalAlignment.Right)
                //{
                //    _vTextBox.TextAlignment = TextAlignment.Right;
                //}
                //else if (_column.HorizontalContentAlignment == HorizontalAlignment.Center)
                //{
                //    _vTextBox.TextAlignment = TextAlignment.Center;
                //}

                _vTextBox.SetBinding(vStarRating.RatingProperty, textBinding);


                _vTextBox.Style = cell.EditorStyleResolved;


                //_vTextBox.TextWrapping = _column.TextWrapping;
                if (_resetBindingSet == false)
                {
                    IBusinessObject obj = cell.Row.Data as IBusinessObject;
                    if (obj == null) { return null; }
                    SetXamGridEditControlValidStateAppearance(_column.Key, _vTextBox, obj);
                }
                else
                {
                    // Set the text from what it was the last time we entered a value.
                    _vTextBox.Rating = _textDisplay.Rating;
                }
                return _vTextBox;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveEditorControl


        #region ResolveValueFromEditor

        public override object ResolveValueFromEditor(Infragistics.Controls.Grids.Cell cell)
        {
            try
            {
                return this._vTextBox.Rating;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveValueFromEditor


        #endregion Overrides


        #region Event Handlers





        void vStarRating_StarRatingChanged(object sender, StarRatingChangedArgs e)
        {
            try
            {

                if (_vTextBox.Parent == null) { return; }

                if (_resetBindingSet == false)
                {
                    IBusinessObject data = _cell.Row.Data as IBusinessObject;
                    string key = _cell.Column.Key;
                    int index = _cell.Row.Index;
                    vStarRatingColumnTextChangedEventArgs args = new vStarRatingColumnTextChangedEventArgs(_cell, index, key, data, e);
                    _column.OnStarRatingChanged(_vTextBox, args);
                }
                else
                {
                    // No need to raise the text changed event here as we dont want to set any values to the business object.
                }
                this.NotifyEditorValueChanged(this._vTextBox.Rating);
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }




        //void vTextBox_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    try
        //    {

        //        if (_vTextBox.Parent == null) { return; }

        //        if (_resetBindingSet == false)
        //        {
        //            IBusinessObject data = _cell.Row.Data as IBusinessObject;
        //            string key = _cell.Column.Key;
        //            int index = _cell.Row.Index;
        //            vStarRatingColumnTextChangedEventArgs args = new vStarRatingColumnTextChangedEventArgs(_cell, index, key, data, e);
        //            _column.OnTextChanged(_vTextBox, args);
        //        }
        //        else
        //        {
        //            // No need to raise the text changed event here as we dont want to set any values to the business object.
        //        }
        //        this.NotifyEditorValueChanged(this._vTextBox.Text);
        //    }
        //    catch (Exception ex)
        //    {
        //        //Debugger.Break();
        //        //System.Diagnostics.Debug.WriteLine(ex.ToString());
        //    }
        //}

        //void vTextBox_GotFocus(object sender, RoutedEventArgs e)
        //{
        //    //try
        //    //{
        //    //    _vTextBox.SelectAll();
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    //Debugger.Break();
        //    //    //System.Diagnostics.Debug.WriteLine(ex.ToString());
        //    //}
        //}

        #endregion Event Handlers


        #region Methods

        public static void SetXamGridEditControlValidStateAppearance(string key, vStarRating ctl, IBusinessObject obj)
        {
            try
            {

                //if (obj.IsPropertyValid(key))
                //{
                //    ctl.ValidStateAppearance = ValidationState.Valid;
                //}
                //else
                //{
                //    if (obj.IsPropertyDirty(key))
                //    {
                //        ctl.ValidStateAppearance = ValidationState.NotValidIsDirty;
                //    }
                //    else
                //    {
                //        ctl.ValidStateAppearance = ValidationState.NotValidNotDirty;
                //    }
                //}
            }
            catch (Exception ex)
            {
            
            }
        }

        #endregion Methods



    }




    #region vStarRatingColumnTextChangedEventArgs Text Changed Event

    public class vStarRatingColumnTextChangedEventArgs : EventArgs
    {
        Cell _cell;
        int _index;
        string _key;
        object _data;
        StarRatingChangedArgs _starRatingArgs;

        internal vStarRatingColumnTextChangedEventArgs(Cell cell, int index, string key, object data, StarRatingChangedArgs e)
        {
            _cell = cell;
            _index = index;
            _key = key;
            _data = data;
            _starRatingArgs = e;
        }

        public Cell ContentCell { get { return _cell; } }
        public int Index { get { return _index; } }
        public string Key { get { return _key; } }
        public object Data { get { return _data; } }
        public StarRatingChangedArgs StarRatingArgs { get { return _starRatingArgs; } }

    }

    #endregion vStarRatingColumnTextChangedEventArgs Text Changed Event


}
