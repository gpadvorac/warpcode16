﻿using System;
using System.Windows;
using System.Windows.Controls;
using Infragistics.Controls.Grids;
using vControls;
using Infragistics;
using System.Windows.Markup;
using System.Collections;
using vTooltipProvider;
using System.Text;
using System.Windows.Input;
using System.Windows.Media;

namespace vControls
{
    public class vStarRatingColumn : EditableColumn, IvColumn
    {


        #region Initialize Variables

        public event EventHandler<vStarRatingColumnTextChangedEventArgs> StarRatingChanged;

        #endregion Initialize Variables

        
        #region Constructor

        public vStarRatingColumn()
        {
            //FilterColumnSettings fcs = this.FilterColumnSettings;
            //RowsFilter rf = new RowsFilter(typeof(String), this);
            //ComparisonCondition compCond = new ComparisonCondition();
            //compCond.Operator = ComparisonOperator.Contains;
            ////compCond.FilterValue = 15;
            ////Add condition to the RowFilter
            //rf.Conditions.Add(compCond);
            ////this.dataGrid.FilteringSettings.RowFiltersCollection.Add(rf);


            //this.h

            //HeaderTemplate = "xxx";

        }

        #endregion Constructor


        #region Properties



        #region NumberOfStarsProperty
        public static readonly DependencyProperty NumberOfStarsProperty =
            DependencyProperty.Register(
                "NumberOfStars", typeof(int),
                typeof(vStarRating), new PropertyMetadata(5, new PropertyChangedCallback(OnNumberOfStarsChanged)));

        public int NumberOfStars
        {
            get { return (int)GetValue(NumberOfStarsProperty); }
            set { SetValue(NumberOfStarsProperty, value); }
        }

        private static void OnNumberOfStarsChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            vStarRatingColumn col = (vStarRatingColumn)sender;
            col.OnPropertyChanged("NumberOfStarsProperty");
        }

        #endregion NumberOfStarsProperty







        #region ValidationState


        public static readonly DependencyProperty ValidStateAppearanceProperty = DependencyProperty.Register(
            "ValidStateAppearance",
            typeof(string),
            typeof(vStarRatingColumn),
            new PropertyMetadata(new PropertyChangedCallback(ValidStateAppearanceChanged)));


        private static void ValidStateAppearanceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vStarRatingColumn col = (vStarRatingColumn)obj;
            col.OnPropertyChanged("ValidStateAppearance");
        }


        public string ValidStateAppearance
        {
            get { return (string)GetValue(ValidStateAppearanceProperty); }
            set
            {
                SetValue(ValidStateAppearanceProperty, value);
            }
        }




        #endregion ValidationState


        #region ToolTip Properties

        #region HeaderToolTipCaption

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipCaptionProperty = DependencyProperty.Register("HeaderToolTipCaption",
            typeof(String),
            typeof(vStarRatingColumn),
            new PropertyMetadata(""));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public String HeaderToolTipCaption
        {
            get { return (String)this.GetValue(HeaderToolTipCaptionProperty); }
            set
            {
                try
                {

                    this.SetValue(HeaderToolTipCaptionProperty, value);

                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipCaptionChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vStarRatingColumn col = (vStarRatingColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipCaption");
        //}

        #endregion HeaderToolTipCaption


        #region HeaderToolTipStringArray

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipStringArrayProperty = DependencyProperty.Register("HeaderToolTipStringArray",
            typeof(String[]),
            typeof(vStarRatingColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a string array used to generate a collectin for the tooltip content
        /// </summary>
        public String[] HeaderToolTipStringArray
        {
            get { return (String[])this.GetValue(HeaderToolTipStringArrayProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderToolTipStringArrayProperty, value);

                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipStringArrayChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vStarRatingColumn col = (vStarRatingColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipStringArray");
        //}

        #endregion HeaderToolTipStringArray


        #region HeaderToolTipItemsSource

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipItemsSourceProperty = DependencyProperty.Register("HeaderToolTipItemsSource",
            typeof(IEnumerable),
            typeof(vStarRatingColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public IEnumerable HeaderToolTipItemsSource
        {
            get { return (IEnumerable)this.GetValue(HeaderToolTipItemsSourceProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderToolTipItemsSourceProperty, value);

                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipItemsSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vStarRatingColumn col = (vStarRatingColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipItemsSource");
        //}

        #endregion HeaderToolTipItemsSource


        #region HeaderTooltipContent

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderTooltipContentProperty = DependencyProperty.Register("HeaderTooltipContent",
            typeof(IvTooltipContent),
            typeof(vStarRatingColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public IvTooltipContent HeaderTooltipContent
        {
            get { return (IvTooltipContent)this.GetValue(HeaderTooltipContentProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderTooltipContentProperty, value);

                    StringBuilder xmlCode = new StringBuilder();
                    xmlCode.AppendLine("<DataTemplate xmlns='http://schemas.microsoft.com/client/2007' xmlns:vGridCol='clr-namespace:vControls;assembly=vControlsGridColumns'>");
                    xmlCode.AppendLine("    <vGridCol:vColumnHeaderGrid  >");
                    //xmlCode.AppendLine("        <TextBlock Text=\"{Binding}\"/>");
                    xmlCode.AppendLine("        <TextBlock Text=\"" + this.HeaderText + "\"/>");
                    xmlCode.AppendLine("    </vGridCol:vColumnHeaderGrid>");
                    xmlCode.AppendLine("</DataTemplate>");
                    this.HeaderTemplate = (DataTemplate)XamlReader.Load(xmlCode.ToString());


                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderTooltipContentChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vStarRatingColumn col = (vStarRatingColumn)obj;
        //    col.OnPropertyChanged("HeaderTooltipContent");
        //}

        #endregion HeaderTooltipContent

        #endregion ToolTip Properties





        #endregion Properties



        #region Events


        protected internal void OnStarRatingChanged(vStarRating sender, vStarRatingColumnTextChangedEventArgs e)
        {
            if (StarRatingChanged != null)
            {
                this.OnStarRatingChanged(sender, e);
            }
        }
        

        #endregion Events




        #region Overrides

        /// <summary>
        /// Generates a new <see cref="DecimalColumnContentProvider"/> that will be used to generate content for <see cref="Cell"/> objects for this <see cref="Column"/>.
        /// </summary>
        /// <returns></returns>
        protected override Infragistics.Controls.Grids.Primitives.ColumnContentProviderBase GenerateContentProvider()
        {
            return new vStarRatingColumnContentProvider();
        }

        protected override void FillAvailableFilters(FilterOperandCollection availableFilters)
        {
            try
            {
                base.FillAvailableFilters(availableFilters);
                this.FilterColumnSettings.FilterMenuCustomFilteringButtonVisibility = Visibility.Collapsed;

                availableFilters.Add(new EqualsOperand());
                availableFilters.Add(new NotEqualsOperand());
                availableFilters.Add(new GreaterThanOperand());
                availableFilters.Add(new GreaterThanOrEqualOperand());
                availableFilters.Add(new LessThanOperand());
                availableFilters.Add(new LessThanOrEqualOperand());


                //if (this.DataType != null)
                //{
                //    if (this.DataType == typeof(decimal))
                //    {
                //        availableFilters.Add(new EqualsOperand());
                //        availableFilters.Add(new NotEqualsOperand());
                //        availableFilters.Add(new GreaterThanOperand());
                //        availableFilters.Add(new GreaterThanOrEqualOperand());
                //        availableFilters.Add(new LessThanOperand());
                //        availableFilters.Add(new LessThanOrEqualOperand());

                //    }
                //    else if (this.DataType.IsValueType)
                //    {
                //        /* Hack solution until we have columntypes to handle numbers or other datatypes.*/


                //        availableFilters.Add(new EqualsOperand());
                //        availableFilters.Add(new NotEqualsOperand());
                //        availableFilters.Add(new GreaterThanOperand());
                //        availableFilters.Add(new LessThanOperand());
                //        availableFilters.Add(new StartsWithOperand());
                //        availableFilters.Add(new ContainsOperand());
                //        availableFilters.Add(new EndsWithOperand());
                //        availableFilters.Add(new DoesNotContainOperand());
                //        availableFilters.Add(new DoesNotStartWithOperand());
                //        availableFilters.Add(new DoesNotEndWithOperand());
                //    }
                //}
            }
            catch (Exception ex)
            {

            }
        }


        FilterOperand _defaultFilterOperand;
        protected override FilterOperand DefaultFilterOperand
        {
            get
            {
                if (this.DataType == typeof(string))
                {
                    if (this._defaultFilterOperand == null)
                        this._defaultFilterOperand = new ContainsOperand();
                    return this._defaultFilterOperand;
                }
                return base.DefaultFilterOperand;
            }
        }


        #endregion Overrides




    }
}
