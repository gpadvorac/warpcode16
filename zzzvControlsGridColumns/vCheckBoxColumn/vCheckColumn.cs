﻿using System;
using System.Windows;
using System.Windows.Controls;
using Infragistics.Controls.Grids;
using System.Collections;
using Infragistics.Controls.Grids.Primitives;
using vTooltipProvider;
using System.Text;
using System.Windows.Markup;
using System.Windows.Media;

namespace vControls
{
    public class vCheckColumn : EditableColumn, IvColumn
    {
        /// <summary>
        /// Defines the IsChecked DependencyProperty
        /// </summary>
        public static readonly DependencyProperty IsCheckedProperty = DependencyProperty.Register("IsChecked", typeof(bool), typeof(vCheckColumn), new PropertyMetadata(IsCheckedPropertyChanged));

        /// <summary>
        /// Defines the ValidStateAppearance DependencyProperty
        /// </summary>
        public static readonly DependencyProperty ValidStateAppearanceProperty = DependencyProperty.Register("ValidStateAppearance", typeof(string), typeof(vCheckColumn), new PropertyMetadata(ValidStateAppearancePropertyChanged));

        /// <summary>
        /// Defines the BorderBrush DependencyProperty
        /// </summary>
        public static readonly DependencyProperty BorderBrushProperty = DependencyProperty.Register("BorderBrush", typeof(Brush), typeof(vCheckColumn), new PropertyMetadata(new SolidColorBrush(SystemColors.ActiveBorderColor)));

        public vCheckColumn()
        {
            HorizontalContentAlignment = HorizontalAlignment.Center;
            VerticalContentAlignment = VerticalAlignment.Top;
        }

        /// <summary>
        /// Gets or sets the IsChecked property
        /// </summary>
        public bool IsChecked
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        /// <summary>
        /// Gets or sets the ValidStateAppearance property
        /// </summary>
        public string ValidStateAppearance
        {
            get { return (string) GetValue(ValidStateAppearanceProperty); }
            set { SetValue(ValidStateAppearanceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the BorderBrush property
        /// </summary>
        public Brush BorderBrush
        {
            get { return (Brush)GetValue(BorderBrushProperty); }
            set { SetValue(BorderBrushProperty, value); }
        }

        public event EventHandler<vCheckColumnCheckedChangedEventArgs> CheckedChanged;

        protected internal void OnCheckedChanged(vCheckBox sender, vCheckColumnCheckedChangedEventArgs e)
        {
            if (CheckedChanged != null)
            {
                CheckedChanged(sender, e);
            }
        }

        private static void IsCheckedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            var column = (vCheckColumn) d;
            if (column != null)
            {
                column.OnPropertyChanged("IsChecked");
            }
        }

        private static void ValidStateAppearancePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            var column = (vCheckColumn) d;
            if (column != null)
            {
                column.OnPropertyChanged("ValidStateAppearance");
            }
        }
    
        /// <summary>
        /// Generates a new <see cref="TextColumnContentProvider"/> that will be used to generate content for <see cref="Cell"/> objects for this <see cref="Column"/>.
        /// </summary>
        /// <returns></returns>
        protected override ColumnContentProviderBase GenerateContentProvider()
        {
            return new vCheckColumnContentProvider();
        }

        #region ToolTip Properties

        /// <summary>
        /// Defines the HeaderToolTipCaption DependencyProperty
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipCaptionProperty = DependencyProperty.Register("HeaderToolTipCaption", typeof(String), typeof(vCheckColumn), new PropertyMetadata(""));

        /// <summary>
        /// Defines the HeaderToolTipStringArray DependencyProperty
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipStringArrayProperty = DependencyProperty.Register("HeaderToolTipStringArray", typeof(String[]), typeof(vCheckColumn), new PropertyMetadata(null));

        /// <summary>
        /// Defines the HeaderToolTipItemsSource DependencyProperty
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipItemsSourceProperty = DependencyProperty.Register("HeaderToolTipItemsSource", typeof(IEnumerable), typeof(vCheckColumn), new PropertyMetadata(null));

        /// <summary>
        /// Defines the HeaderTooltipContent DependencyProperty
        /// </summary>
        public static readonly DependencyProperty HeaderTooltipContentProperty = DependencyProperty.Register("HeaderTooltipContent", typeof(IvTooltipContent), typeof(vCheckColumn), new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public String HeaderToolTipCaption
        {
            get { return (String) GetValue(HeaderToolTipCaptionProperty); }
            set { SetValue(HeaderToolTipCaptionProperty, value); }
        }

        /// <summary>
        /// Gets or sets a string array used to generate a collectin for the tooltip content
        /// </summary>
        public String[] HeaderToolTipStringArray
        {
            get { return (String[]) GetValue(HeaderToolTipStringArrayProperty); }
            set { SetValue(HeaderToolTipStringArrayProperty, value); }
        }

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        /// <remarks>
        /// I think this is obsolete becuase other code such as vColumnHeaderGrid.vColumnHeaderGrid_Loaded
        /// will loop thru HeaderToolTipStringArray to build the items source
        /// </remarks>
        public IEnumerable HeaderToolTipItemsSource
        {
            get { return (IEnumerable) GetValue(HeaderToolTipItemsSourceProperty); }
            set { SetValue(HeaderToolTipItemsSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public IvTooltipContent HeaderTooltipContent
        {
            get { return (IvTooltipContent) GetValue(HeaderTooltipContentProperty); }
            set
            {
                SetValue(HeaderTooltipContentProperty, value);

                //var dictionary = ResourceDictionaryHelper.GetResourceDictionary("/vControlsGridColumns;component/Themes/generic.xaml");
                //HeaderTemplate = (DataTemplate) dictionary["CheckBoxColumnHeaderTemplate"];

                //var xmlCode = new StringBuilder();
                //xmlCode.AppendLine("<DataTemplate xmlns='http://schemas.microsoft.com/client/2007' xmlns:vGridCol='clr-namespace:vControls;assembly=vControlsGridColumns' xmlns:vGdCol='clr-namespace:vControls;assembly=vControlsGridColumns'>");
                //xmlCode.AppendLine("    <vGridCol:vColumnHeaderGrid  >");
                //xmlCode.AppendLine("        <Grid.ColumnDefinitions>");
                //xmlCode.AppendLine("            <ColumnDefinition Width=\"*\"/>");
                //xmlCode.AppendLine("            <ColumnDefinition Width=\"Auto\"/>");
                //xmlCode.AppendLine("        </Grid.ColumnDefinitions>");
                //xmlCode.AppendLine("        <TextBlock Grid.Column=\"0\" Text=\"" + HeaderText + "\"/>");
                //xmlCode.AppendLine("        <CheckBox x:Name=\"MainCheck\" Grid.Column=\"1\" vGdCol:ChechBoxColumnHelper.ColumnHeaderGroupName=\"" + HeaderText + "\"/>");
                //xmlCode.AppendLine("    </vGridCol:vColumnHeaderGrid>");
                //xmlCode.AppendLine("</DataTemplate>");
                //HeaderTemplate = (DataTemplate)XamlReader.Load(xmlCode.ToString());
            }
        }

        #endregion ToolTip Properties
    }
}