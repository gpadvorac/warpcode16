﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Controls.Grids.Primitives;
using Infragistics.Controls.Grids;
using System.Diagnostics;
using TypeServices;

using EntityWireTypeSL;

namespace vControls
{
    public class vCheckColumnContentProvider : ColumnContentProviderBase
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "vControlsGridColumns";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "vCheckColumnContentProvider";

        CheckBox _checkBoxRead;
        vCheckBox _vCheckBoxEditor;
        vCheckColumn _column = null;
        Cell _cell;
        bool _dataWasNull;

        #endregion Initialize Variables


        #region Constructor

        public vCheckColumnContentProvider()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyDown", IfxTraceCategory.Enter);
                _checkBoxRead = new CheckBox();
                // Make the chk box behave like its read only.
                _checkBoxRead.IsHitTestVisible = false;
                _checkBoxRead.IsTabStop = false;

                _vCheckBoxEditor = new vCheckBox();
                // Center it in the cell
                _vCheckBoxEditor.HorizontalAlignment = HorizontalAlignment.Center;
                _vCheckBoxEditor.VerticalAlignment = VerticalAlignment.Top;

                _vCheckBoxEditor.Checked += new RoutedEventHandler(vCheckBox_Checked);
                _vCheckBoxEditor.Unchecked += new RoutedEventHandler(vCheckBox_Checked);

            }
            catch (Exception ex)
            {
                ////if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyDown", ex);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyDown", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructor


        #region Overrides

        #region ResolveDisplayElement

        public override FrameworkElement ResolveDisplayElement(Cell cell, System.Windows.Data.Binding cellBinding)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveDisplayElement", IfxTraceCategory.Enter);
                // Added 11-15 - Valerie
                this._checkBoxRead.IsThreeState = (cell.Column.DataType == typeof(bool?));
                _column = (vCheckColumn)cell.Column;
                this._checkBoxRead.BorderBrush = _column.BorderBrush;


                if (cellBinding != null)
                {
                    cellBinding.ValidatesOnNotifyDataErrors = cellBinding.ValidatesOnDataErrors = cellBinding.ValidatesOnExceptions = cellBinding.NotifyOnValidationError = ((vCheckColumn)cell.Column).AllowEditingValidation;
                    this._checkBoxRead.SetBinding(CheckBox.IsCheckedProperty, cellBinding);
                }
                this._checkBoxRead.IsHitTestVisible = false;

                return _checkBoxRead;


                //_cell = cell;
                //_column = (vCheckColumn)cell.Column;
                //Binding checkedBinding = new Binding();
                //checkedBinding.Path = new PropertyPath(_column.Key);
                //checkedBinding.Mode = BindingMode.TwoWay;
                //this._checkBoxRead.SetBinding(CheckBox.IsCheckedProperty, checkedBinding);
                //return _checkBoxRead;
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveDisplayElement", ex);
                return _checkBoxRead;
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveDisplayElement", IfxTraceCategory.Leave);
            }
        }

        #endregion ResolveDisplayElement



        // Added 11-15 - Valerie
        #region AdjustDisplayElement

        /// <summary>
        /// Called during EnsureContent to allow the provider a chance to modify it's display based on the current conditions.
        /// </summary>
        /// <param name="cell"></param>
        public override void AdjustDisplayElement(Cell cell)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AdjustDisplayElement", IfxTraceCategory.Enter);
                // Ok, so with the perf improvements, add new row doesn't exactly play nicely, when it creates cells before the data is loaded. 
                // So what happens is we need to track when the data changes, and if it does, then make sure its binding is applied
                // and that the IsEnabled property is correct. 
                bool dataIsNull = (cell.Row.Data == null);
                if (this._dataWasNull && !dataIsNull)
                {
                    Binding binding = this.ResolveBinding(cell);

                    if (binding != null)
                    {
                        binding.ValidatesOnNotifyDataErrors = binding.ValidatesOnDataErrors = binding.ValidatesOnExceptions = binding.NotifyOnValidationError = ((vCheckColumn)cell.Column).AllowEditingValidation;
                        this._checkBoxRead.SetBinding(CheckBox.IsCheckedProperty, binding);
                    }

                    this._checkBoxRead.IsEnabled = cell.IsEditable;
                }

                this._dataWasNull = dataIsNull;

                // double remmed the next block of 10 lines on 2013-04-15 to get rid of a bug Idea KB Review grid where menu filters were automaticaly being applied when tabing to another tab and back again.
                //     ... a support case on 2012-04-09 with Duan mentioned somthing about setting thise FilterCellCalue was the cause, so evidently I changed this using the if statement, but this allowed the bug 
                //     ... to come back.  I completely remmed this code and the checkbox column still seems to function OK.
                ////////  added by geo on 12-01-03 in search of bug fix where obligation grid always has the ckbx filter preset
                ////if (dataIsNull == true)
                ////{  //  remmed by goe on 2012-04-09 becuase when we click on a different tab so this screen/grid becomes hiden, then back to this tab, this code runs again and sets the filter on even tho we did not apply it yet.  this created a new bug.
                ////    //    try this again with out this code and see if the obligation grid still causes problems
                ////    cell.Column.FilterColumnSettings.FilterCellValue = null;
                ////}
                ////else
                ////{
                ////    cell.Column.FilterColumnSettings.FilterCellValue = _vCheckBoxEditor.IsChecked;
                ////}

                // remmed on 2012-01-03
                //cell.Column.FilterColumnSettings.FilterCellValue = _vCheckBoxEditor.IsChecked;

                //cell.Column.FilterColumnSettings.FilteringOperand.ComparisonOperatorValue

                //  2012-04-09.  the filter row should be 3state so we can toggle the filter off. (i think...)
                if (cell.Row.RowType == Infragistics.Controls.Grids.RowType.FilterRow)
                {
                    _vCheckBoxEditor.IsThreeState = true;
                }


                base.AdjustDisplayElement(cell);
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AdjustDisplayElement", ex);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AdjustDisplayElement", IfxTraceCategory.Leave);
            }
        }

        #endregion AdjustDisplayElement






        #region ResolveEditorControl

        protected override FrameworkElement ResolveEditorControl(Cell cell, object editorValue, double availableWidth, double availableHeight, System.Windows.Data.Binding editorBinding)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveEditorControl", IfxTraceCategory.Enter);
                //Binding checkedBinding = new Binding();
                //checkedBinding.Path = new PropertyPath(_column.Key);
                //checkedBinding.Mode = BindingMode.TwoWay;
                //this._vCheckBoxEditor.SetBinding(vCheckBox.IsCheckedProperty, checkedBinding);
                //this._vCheckBoxEditor.Style = cell.EditorStyleResolved;
                //IBusinessObject obj = cell.Row.Data as IBusinessObject;
                //if (obj == null) { return null; }
                //SetXamGridEditControlValidStateAppearance(_column.Key, _vCheckBoxEditor, obj);
                //return this._vCheckBoxEditor;

                this._vCheckBoxEditor.BorderBrush = _column.BorderBrush;

                Style checkBoxStyle = cell.EditorStyleResolved;
                if (checkBoxStyle != null)
                    this._vCheckBoxEditor.Style = checkBoxStyle;
                else
                    this._vCheckBoxEditor.ClearValue(vCheckBox.StyleProperty);


                this._vCheckBoxEditor.IsHitTestVisible = true;
                this._vCheckBoxEditor.IsThreeState = (cell.Column.DataType == typeof(bool?));

                if (this._vCheckBoxEditor.GetBindingExpression(vCheckBox.IsCheckedProperty) == null)
                {
                    if (editorBinding != null)
                    {
                        // This next line activates the filter
                        if (cell.Row is FilterRow)
                        {
                            editorBinding.UpdateSourceTrigger = UpdateSourceTrigger.Default;
                        }
                        this._vCheckBoxEditor.SetBinding(vCheckBox.IsCheckedProperty, editorBinding);
                    }
                }


                // Create this instance now becuase we may need it later such as in the check changed event.
                _column = (vCheckColumn)cell.Column;

                // Moved code 11-15 - Valerie
                IBusinessObject obj = cell.Row.Data as IBusinessObject;
                if (obj != null)
                {
                    if (cell.Row.RowType == RowType.DataRow)
                    {
                        SetXamGridEditControlValidStateAppearance(_column.Key, _vCheckBoxEditor, obj);
                    }
                }

                //  Added by Maria 2013-03-19 to elimiate issue from virtulization where the chk box would toggle in a different row
                this._vCheckBoxEditor.SetBinding(vCheckBox.IsCheckedProperty, editorBinding);

                if (cell.IsEditing)
                    this._vCheckBoxEditor.Focus();

                return _vCheckBoxEditor;



            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveEditorControl", ex);
                return _vCheckBoxEditor;
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveEditorControl", IfxTraceCategory.Leave);
            }
        }

        #endregion ResolveEditorControl


        #region ResolveValueFromEditor

        public override object ResolveValueFromEditor(Infragistics.Controls.Grids.Cell cell)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveValueFromEditor", IfxTraceCategory.Enter);
                return this._vCheckBoxEditor.IsChecked;
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveValueFromEditor", ex);
                return null;
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveValueFromEditor", IfxTraceCategory.Leave);
            }
        }

        #endregion ResolveValueFromEditor

        #endregion Overrides


        #region Event Handlers

        void vCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckBox_Checked", IfxTraceCategory.Enter);
                //if (_vCheckBoxEditor.Parent == null) { return; }
                //IBusinessObject data = _cell.Row.Data as IBusinessObject;
                //string key = _cell.Column.Key;
                //int index = _cell.Row.Index;
                //vCheckColumnCheckedChangedEventArgs args = new vCheckColumnCheckedChangedEventArgs(_cell, index, key, data, e);
                //_column.OnCheckedChanged(_vCheckBoxEditor, args);


                if (_vCheckBoxEditor.Parent == null) { return; }
                // Add / Modified 11-15 - Valerie
                if (_vCheckBoxEditor.Parent is FilterRowCellControl)
                {
                }
                else
                {
                    CellControl cc = (CellControl)_vCheckBoxEditor.Parent;
                    _cell = (Cell)cc.Cell;
                    IBusinessObject data = _cell.Row.Data as IBusinessObject;
                    IWireTypeBinding wire = null;
                    if (data == null)
                    {
                        wire = _cell.Row.Data as IWireTypeBinding;
                    }


                    string key = _cell.Column.Key;
                    int index = _cell.Row.Index;
                    vCheckColumnCheckedChangedEventArgs args = new vCheckColumnCheckedChangedEventArgs(_cell, index, key, wire, e);
                    _column.OnCheckedChanged(_vCheckBoxEditor, args);
                }


            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckBox_Checked", ex);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckBox_Checked", IfxTraceCategory.Leave);
            }
        }

        #endregion Event Handlers


        #region Methods


        public static void SetXamGridEditControlValidStateAppearance(string key, vCheckBox ctl, IBusinessObject obj)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetXamGridEditControlValidStateAppearance", IfxTraceCategory.Enter);

                if (obj.IsPropertyValid(key))
                {
                    ctl.ValidStateAppearance = ValidationState.Valid;
                }
                else
                {
                    if (obj.IsPropertyDirty(key))
                    {
                        ctl.ValidStateAppearance = ValidationState.NotValidIsDirty;
                    }
                    else
                    {
                        ctl.ValidStateAppearance = ValidationState.NotValidNotDirty;
                    }
                }
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetXamGridEditControlValidStateAppearance", ex);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetXamGridEditControlValidStateAppearance", IfxTraceCategory.Leave);
            }
        }

        #endregion Methods


    }
}
