﻿using System;
using System.Windows;
using System.Windows.Controls;
using Infragistics.Controls.Grids.Primitives;
using System.Windows.Data;
using Infragistics.Controls.Grids;
using System.Diagnostics;
using vControls;
using Infragistics.Controls.Editors;
using vComboDataTypes;
using TypeServices;
using System.Windows.Media;
using System.Windows.Controls.Primitives;
using Ifx.SL;
using System.Windows.Markup;

//http://blogsprajeesh.blogspot.com/2010/03/infragistics-working-with-2.html

//http://news.infragistics.com/blogs/nikolay_zhekov/archive/2011/06/02/xamgrid-implementing-custom-column-filtering-ui.aspx

namespace vControls
{
    public class vXamComboColumnContentProvider : ColumnContentProviderBase
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "vControlsGridColumns";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "vXamComboColumnContentProvider";

        TextBlock _textDisplay;
        vControls.vXamComboEditor _comboEditor;
        vXamComboColumn _column = null;
        ComboItemList _comboItemSource;
        //bool _bindingSet = false;
        bool _resetBindingSet;
        Cell _cell;

        //  Added "_cellContent" 2012-10-19 by Krasimir to allow for conditional formating by Krasimir:
        // have bind the Content of the ContentControl, using the cellBinding parameter, 
        // in order have the content of the cell the same type as the DataType of the column and I have also created a DataTempalte, 
        // in which I am adding a TextBlock, bound to the Content of the ContentControl (the int value) and using a Converter (which I have defined in the vComboDataTypes in IdToItemNameConverter.cs file), 
        // I am converting the int to its corresponding string value and since this does not change the Content of the ContentControl and the cell, 
        // the conditional formatting is applied and the value of the cell appear as expected.
        ContentControl _cellContent;

        #endregion Initialize Variables


        #region Constructor

        public vXamComboColumnContentProvider()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumnContentProvider", IfxTraceCategory.Enter);
                _textDisplay = new TextBlock();
                _comboEditor = new vControls.vXamComboEditor();
                _comboEditor.Margin = new Thickness(-5);
                _comboEditor.Padding = new Thickness(3, 3, 0, 0);
                //***
                _comboEditor.vXamComboEditorSelectionChanged += new vXamComboEditorSelectionChangedEventHandler(vXamComboEditorSelectionChanged);
                _comboEditor.KeyDown += new System.Windows.Input.KeyEventHandler(ComboEditor_KeyDown);
                _comboEditor.KeyUp += new System.Windows.Input.KeyEventHandler(ComboEditor_KeyUp);
                //_comboEditor.DropDownOpened += new EventHandler(ComboEditor_DropDownOpened);
                _comboEditor.ItemAdding += ComboEditor_ItemAdding;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumnContentProvider", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumnContentProvider", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructor


        #region Overrides

        #region ResolveDisplayElement

        public override FrameworkElement ResolveDisplayElement(Cell cell, System.Windows.Data.Binding cellBinding)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveDisplayElement", IfxTraceCategory.Enter);
                _cell = cell;
                _column = (vXamComboColumn)cell.Column;
                _cellContent = new ContentControl();
                _textDisplay.Foreground = _column.Foreground;

                _resetBindingSet = cell.Row.RowType == Infragistics.Controls.Grids.RowType.FilterRow;
                if (_resetBindingSet == true)
                {
                    if (_comboEditor.SelectedItem == null)
                    {

                        // New Code Added for filtering 12/12/2011

                        if (cellBinding != null)
                        {
                            FilterRowCell frc = (FilterRowCell)_cell;
                            frc.FilterCellValue = null;
                            _textDisplay.SetBinding(TextBlock.TextProperty, cellBinding);
                        }
                        // Please Note - this logic will be hit when the clear filters button is clicked
                        // You may need to add logic here to reset the child combo itemsource back 
                        // to the entire list here
                        _textDisplay.Text = "";
                    }
                    else
                    {
                        // New Code Added for filtering 12/12/2011
                        if (cellBinding != null)
                        {
                            FilterRowCell frc = (FilterRowCell)_cell;
                            frc.FilterCellValue = ((ComboItem)_comboEditor.SelectedItem).Id;
                            //_textDisplay.SetBinding(TextBlock.TextProperty, cellBinding);
                        }
                        _textDisplay.Text = ((ComboItem)_comboEditor.SelectedItem).ItemName;
                    }
                }
                else
                {

                    if (_column.AllowConditionalFormating == false)
                    {
                        // 2012-10-29:  New logic.  We added the ability to apply conditional formating using a DataTemplate for the 
                        // cell's ContentControl.  However, if Non-valid XML syntax is used in the text, the app will crash, therefore,
                        // Use this logic.  If we dont use conditional formating, then use the original code in the FALSE part of this if statement,
                        // Otherwise, use the data template in the TRUE part of the statment, but make sure all text used will pase using the XML parser.
                        // regarding    IsChildCombo = true....
                        Binding textBinding = new Binding();
                        if (_column.DisplayTextBinding == null)
                        {
                            //  We are not using the DisplayTextBinding so use the standard converter to get the display text
                            textBinding.Path = new PropertyPath(cell.Column.Key);
                            ComboItemList data = _column.ItemsSource as ComboItemList;
                            if (data == null) { return _textDisplay; }

                            if (_column.IsChildCombo == true)
                            {
                                //  Becuase this combo is filtered by an FK, we must seek the item name 
                                //  from the cached (non-filtered) list which is the full list.  Otherwise there are instances such as when using 
                                //  GroupBy that the converter will not find the name.  
                                //  Using the converter that uses the cached list assures we will find it.
                                textBinding.Converter = data.GetNameFromCachedListConverterProperty;
                            }
                            else
                            {
                                //  To save resources; since this combo is not filtered by an FK, there are no items in the ComboItemList's cached list.
                                //  Therefore, use the converter that uses items from the ComboItemList base list.
                                textBinding.Converter = data.GetNameFromListConverterProperty;
                            }
                            textBinding.ConverterParameter = _column.ItemsSource;
                        }
                        else
                        {
                            // We have a DisplayTextBinding value so lets use it instead of using a converter.  This will improve performance.
                            textBinding.Path = new PropertyPath(_column.DisplayTextBinding);
                        }

                        textBinding.Mode = BindingMode.TwoWay;
                        _textDisplay.SetBinding(TextBlock.TextProperty, textBinding);
                    }
                    else
                    {
                        //***************************************
                        /// New code for Conditional Formating   *************************************
                        // in order to have the conditional formattign to work correclty, 
                        //it seems that the cell's content type shold be the same as the DataType of the Colunm
                        //So, i am setting adding a ContentControl as Display Element, bound to the cell's value
                        //and i am using a ContentTempalte for the ContentControl, to change the Ids to Display Values

                        //converting the ItemsSource of the ComboColumn to xaml definitions,
                        //to be able to pass the source as converter parameter
                        string items = "";
                        foreach (ComboItem item in _column.ItemsSource)
                        {
                            items +=
                                @"<data:ComboItem 
                                                        Id=""" + item.Id + @""" " +
                                      @"ItemName=""" + item.ItemName + @""" " +
                                      @"FK=""" + item.FK + @""" " +
                                      @"Desc=""" + item.Desc + @""" />";
                        }
                        //Creating DataTempalte
                        string template =
                                @"
                                                <DataTemplate
                                                    xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""
                                                    xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml""
                                                    xmlns:ig=""http://schemas.infragistics.com/xaml""
                                                    xmlns:data=""clr-namespace:vComboDataTypes;assembly=vComboDataTypes"">
                                                    <Grid>
                                                        <Grid.Resources>
                                                            <data:IdToItemNameConverter x:Key=""IdToItemNameConverter""/>
                                                            <data:ComboItemList x:Key=""ComboItemList"">" +
                            items +
                        @"</data:ComboItemList>
                                                        </Grid.Resources>
                                                        <TextBlock Text=""{Binding Converter={StaticResource IdToItemNameConverter}, ConverterParameter={StaticResource ComboItemList}}""
                                                                    Foreground=""{Binding RelativeSource={RelativeSource AncestorType=ig:ConditionalFormattingCellControl}, Path=Foreground}""/>
                                                    </Grid>
                                                </DataTemplate>
                                            ";

                        //Setting the DataTempalte as ContentTempate of the ContentControl
                        _cellContent.ContentTemplate = (DataTemplate)XamlReader.Load(template);
                        //Setting the Binding of the ContentControl's content
                        if (cellBinding != null)
                        {
                            _cellContent.SetBinding(ContentControl.ContentProperty, cellBinding);
                        }
                    }                    
                }

                if (_column.AllowConditionalFormating == false)
                {
                    return _textDisplay;
                }
                else
                {
                    return _cellContent;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveDisplayElement", ex);
                return _textDisplay;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveDisplayElement", IfxTraceCategory.Leave);
            }
        }

        #endregion ResolveDisplayElement


        #region ResolveEditorControl

        protected override FrameworkElement ResolveEditorControl(Cell cell, object editorValue, double availableWidth, double availableHeight, System.Windows.Data.Binding editorBinding)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveEditorControl", IfxTraceCategory.Enter);

                _comboEditor.Foreground = _column.Foreground;

                SetupComboBox(cell, editorBinding);
                // 2011-11-22 added by george to try to make the filter take effect
                if (editorBinding != null)
                {
                    if (cell.Row is FilterRow)
                        // for filter.  not working.
                        editorBinding.UpdateSourceTrigger = UpdateSourceTrigger.Default;
                }


                IBusinessObject obj = cell.Row.Data as IBusinessObject;
                if (obj == null) { return null; }

                if (_column.IsChildCombo == true && _comboEditor.ItemsSource !=null)
                {

                    if (cell.Row.RowType == Infragistics.Controls.Grids.RowType.FilterRow)
                    {
                        object fk = null;
                        if (cell.Row.Cells[_column.ParentComboKey].Column.GetType().ToString() == "vControls.vXamComboColumn")
                        {
                            fk = ((vXamComboColumn)cell.Row.Cells[_column.ParentComboKey].Column).SelectedValue;
                        }
                        else
                        {
                            fk = ((vXamMultiColumnComboColumn)cell.Row.Cells[_column.ParentComboKey].Column).SelectedValue;
                        }

                        ((ComboItemList)_comboEditor.ItemsSource).FilterByFK(fk);
                    }
                    else
                    {
                        //object val = ((vXamComboColumn)cell.Row.Cells[_column.ParentComboKey].Column).SelectedValue;
                        object val = obj.GetPropertyValueByKey(_column.ParentComboKey);
                        ((ComboItemList)_comboEditor.ItemsSource).FilterByFK(val);
                    }
                }

                if (_resetBindingSet == true)
                {
                    Set_ComboEditorItem_SelectedItem(_column.SelectedValue);
                }
                else
                {
                    Set_ComboEditorItem_SelectedItem(cell.Value);

                    if (editorBinding != null)
                    {
                        ComboItemList data = _column.ItemsSource as ComboItemList;

                        editorBinding.Converter = new ComboItemConverter(data);

                        this._comboEditor.SetBinding(XamComboEditor.SelectedItemProperty, editorBinding);
                    }


                    SetXamGridEditControlValidStateAppearance(_column.Key, _comboEditor, obj);
                }
                return this._comboEditor;



                //IBusinessObject obj = cell.Row.Data as IBusinessObject;
                //if (obj == null) { return null; }

                //if (_column.IsChildCombo == true)
                //{
                //    //object val = ((vXamComboColumn)cell.Row.Cells[_column.ParentComboKey].Column).SelectedValue;
                //    object val = obj.GetPropertyValueByKey(_column.ParentComboKey);
                //    ((ComboItemList)_comboEditor.ItemsSource).FilterByFK(val);
                //}

                //if (_resetBindingSet == true)
                //{
                //    Set_ComboEditorItem_SelectedItem(_column.SelectedValue);

                //}
                //else
                //{

                //    Set_ComboEditorItem_SelectedItem(cell.Value);
                //    SetXamGridEditControlValidStateAppearance(_column.Key, _comboEditor, obj);
                //}

                //return this._comboEditor;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveEditorControl", ex);
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return this._comboEditor;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ResolveEditorControl", IfxTraceCategory.Leave);
            }
        }

        #endregion ResolveEditorControl

        #region ResolveValueFromEditor

        public override object ResolveValueFromEditor(Infragistics.Controls.Grids.Cell cell)
        {
            return this._comboEditor.SelectedItem;
        }

        #endregion ResolveValueFromEditor


        #endregion Overrides


        #region Event Handlers



        void vXamComboEditorSelectionChanged(object sender, vXamComboEditorSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboEditorSelectionChanged", IfxTraceCategory.Enter);

                //  2012-03-08   remmed and moved below.
                //if (_comboEditor.Parent == null) { return; }



                if (_comboEditor.SelectedItem == null)
                {
                    _column.SelectedValue = null;
                }
                else
                {
                    _column.SelectedValue = ((ComboItem)_comboEditor.SelectedItem).Id;
                }
                //  2012-03-08 added this to try to force township to work.
                this.NotifyEditorValueChanged(_column.SelectedValue);
                //  2012-03-08 Moved this line here because when we were binding to Township which is text, we were getting a value of "vComboDataTypes.ComboItemList" rather than "S" or "N".
                if (_comboEditor.Parent == null) { return; }


                if (_resetBindingSet == false)
                {
                    IBusinessObject data = _cell.Row.Data as IBusinessObject;
                    string key = _cell.Column.Key;
                    int index = _cell.Row.Index;
                    vXamComboColumnSelectionChangedEventArgs args = new vXamComboColumnSelectionChangedEventArgs(_cell, index, key, data, e);
                    _column.OnSelectionChanged(_comboEditor, args);



                    if (_column.MustUpdateChildCombo == true)
                    {

                        // ***Revised 2014-01-27:  Adding logic for multiple child combos
                        vControls.vXamComboEditor cbo = null;

                        // 1st child combo
                        // Test to make sure its a vXamComboEditor becuase sometimes it a TextBlock instead.
                        if (((CellControl)((vControls.vXamComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content.GetType().ToString() == "vControls.vXamComboEditor")
                        {
                            // Use this cast instead of the one above becuase sometimes it tries to cast a textblock as a combo.
                            cbo = ((CellControl)((vControls.vXamComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content as vControls.vXamComboEditor;
                            if (cbo != null && cbo.ItemsSource != null)
                            {
                                ((ComboItemList)cbo.ItemsSource).FilterByFK(_cell.Value);
                            }
                        }

                        // 2nd child combo
                        if (_column.ChildComboKey2 != null)
                        {
                            // Test to make sure its a vXamComboEditor becuase sometimes it a TextBlock instead.
                            if (((CellControl)((vControls.vXamComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey2].Control.Content.GetType().ToString() == "vControls.vXamComboEditor")
                            {
                                // Use this cast instead of the one above becuase sometimes it tries to cast a textblock as a combo.
                                cbo = ((CellControl)((vControls.vXamComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey2].Control.Content as vControls.vXamComboEditor;
                                if (cbo != null && cbo.ItemsSource != null)
                                {
                                    ((ComboItemList)cbo.ItemsSource).FilterByFK(_cell.Value);
                                }
                            }
                        }

                        // 3rd child combo
                        if (_column.ChildComboKey3 != null)
                        {
                            // Test to make sure its a vXamComboEditor becuase sometimes it a TextBlock instead.
                            if (((CellControl)((vControls.vXamComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey3].Control.Content.GetType().ToString() == "vControls.vXamComboEditor")
                            {
                                // Use this cast instead of the one above becuase sometimes it tries to cast a textblock as a combo.
                                cbo = ((CellControl)((vControls.vXamComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey3].Control.Content as vControls.vXamComboEditor;
                                if (cbo != null && cbo.ItemsSource != null)
                                {
                                    ((ComboItemList)cbo.ItemsSource).FilterByFK(_cell.Value);
                                }
                            }
                        }

                        // 4th child combo
                        if (_column.ChildComboKey4 != null)
                        {
                            // Test to make sure its a vXamComboEditor becuase sometimes it a TextBlock instead.
                            if (((CellControl)((vControls.vXamComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey4].Control.Content.GetType().ToString() == "vControls.vXamComboEditor")
                            {
                                // Use this cast instead of the one above becuase sometimes it tries to cast a textblock as a combo.
                                cbo = ((CellControl)((vControls.vXamComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey4].Control.Content as vControls.vXamComboEditor;
                                if (cbo != null && cbo.ItemsSource != null)
                                {
                                    ((ComboItemList)cbo.ItemsSource).FilterByFK(_cell.Value);
                                }
                            }
                        }

                        // Test to make sure its a vXamMultiColumnComboEditor becuase sometimes it a TextBlock instead.
                        if (((CellControl)((vControls.vXamComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content.GetType().ToString() == "vControls.vXamMultiColumnComboEditor")
                        {
                            // Use this cast instead of the one above becuase sometimes it tries to cast a textblock as a combo.
                            vControls.vXamMultiColumnComboEditor cmbo = ((CellControl)((vControls.vXamComboEditor)sender).Parent).Cell.Row.Cells[_column.ChildComboKey].Control.Content as vControls.vXamMultiColumnComboEditor;
                            if (cmbo != null && cmbo.ItemsSource != null)
                            {
                                ((ComboItemList)cmbo.ItemsSource).FilterByFK(_cell.Value);
                            }
                        }
                    }
                }
                else
                {
                    // No need to raise the text changed event here as we dont want to set any values to the business object.
                }

                // If this is a Filter Row
                if (_cell.Row.RowType == Infragistics.Controls.Grids.RowType.FilterRow)
                {
                    // If this is a parent combo, our other code will clear the child combo for normal rows (data rows)
                    // but this is a filter row and that code doesnt have any effect, so we need to run code here to clear the child columns filter's value.
                    string childKey = ((vXamComboColumn)_cell.Column).ChildComboKey;
                    // ToDo:
                    //  clear the value in the child columns display textblox
                    //  clear the child column's filter.
                }
                // for filter.  not working.
                this.NotifyEditorValueChanged(_column.SelectedValue);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboEditorSelectionChanged", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboEditorSelectionChanged", IfxTraceCategory.Leave);
            }
        }


        void ComboEditor_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyUp", IfxTraceCategory.Enter);

                if (_resetBindingSet == false)
                {
                    IBusinessObject data = _cell.Row.Data as IBusinessObject;
                    string key = _cell.Column.Key;
                    int index = _cell.Row.Index;

                    vXamComboColumnKeyEventArgs args = new vXamComboColumnKeyEventArgs(_cell, index, key, data, e);
                    _column.OnKeyUp(_comboEditor, args);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyUp", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyUp", IfxTraceCategory.Leave);
            }
        }

        void ComboEditor_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyDown", IfxTraceCategory.Enter);

                if (_resetBindingSet == false)
                {
                    IBusinessObject data = _cell.Row.Data as IBusinessObject;
                    string key = _cell.Column.Key;
                    int index = _cell.Row.Index;

                    vXamComboColumnKeyEventArgs args = new vXamComboColumnKeyEventArgs(_cell, index, key, data, e);
                    _column.OnKeyDown(_comboEditor, args);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyDown", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_KeyDown", IfxTraceCategory.Leave);
            }
        }



        void ComboEditor_ItemAdding(object sender, ComboItemAddingEventArgs<ComboEditorItem> e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_ItemAdding", IfxTraceCategory.Enter);

                if (_resetBindingSet == false)
                {
                    
                    IBusinessObject data = _cell.Row.Data as IBusinessObject;
                    string key = _cell.Column.Key;
                    int index = _cell.Row.Index;
                    vXamComboColumnItemAddingEventArgs args = new vXamComboColumnItemAddingEventArgs(_comboEditor, _cell, index, key, data, e);
                    _column.OnItemAdding(_comboEditor, args);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_ItemAdding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ComboEditor_ItemAdding", IfxTraceCategory.Leave);
            }
        }




        #endregion Event Handlers


        #region Methods

        private void SetupComboBox(Cell cell, Binding cellBinding)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetupComboBox", IfxTraceCategory.Enter);
                if (_comboEditor.DisplayMemberPath != _column.DisplayMemberPath)
                {
                    if (_column.DisplayMemberPath == null)
                    {
                        _comboEditor.ClearValue(vControls.vXamComboEditor.DisplayMemberPathProperty);
                    }
                    else
                    {
                        _comboEditor.DisplayMemberPath = _column.DisplayMemberPath;
                    }
                }
                
                _comboEditor.ItemsSource = _column.ItemsSource;

                if (_comboEditor.ItemsSource != null)
                {
                    _comboItemSource = (ComboItemList)_comboEditor.ItemsSource;
                }
                else
                {
                    //Debugger.Break();
                }


                // MaxDropDownHeight
                if (_column.MaxDropDownHeight > 0)
                {
                    _comboEditor.MaxDropDownHeight = _column.MaxDropDownHeight;
                }

                // MaxDropDownHeight
                if (_column.DropdownWidth > 0)
                {
                    _comboEditor.DropdownWidth = _column.DropdownWidth;
                }


                Style comboStyle = cell.EditorStyleResolved;
                if (comboStyle != _comboEditor.Style)
                {
                    if (comboStyle == null)
                    {
                        _comboEditor.ClearValue(Control.StyleProperty);
                    }
                    else
                    {
                        _comboEditor.Style = comboStyle;
                    }
                }

                _comboEditor.CustomValueEnteredAction = _column.CustomValueEnteredAction;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetupComboBox", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetupComboBox", IfxTraceCategory.Leave);
            }
        }

        public static void SetXamGridEditControlValidStateAppearance(string key, vControls.vXamComboEditor ctl, IBusinessObject obj)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetXamGridEditControlValidStateAppearance", IfxTraceCategory.Enter);

                if (obj.IsPropertyValid(key))
                {
                    ctl.ValidStateAppearance = ValidationState.Valid;
                }
                else
                {
                    if (obj.IsPropertyDirty(key))
                    {
                        ctl.ValidStateAppearance = ValidationState.NotValidIsDirty;
                    }
                    else
                    {
                        ctl.ValidStateAppearance = ValidationState.NotValidNotDirty;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetXamGridEditControlValidStateAppearance", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetXamGridEditControlValidStateAppearance", IfxTraceCategory.Leave);
            }
        }

        void Set_ComboEditorItem_SelectedItem(object value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem", IfxTraceCategory.Enter);
                if (_comboItemSource == null) { return; }
                if (value == null)
                {
                    _comboEditor.SelectedIndex = -1;
                    return;
                }
                if (_comboItemSource.Id_DataType == ComboItemList.DataType.IntegerType)
                {
                    if (value.GetType().ToString() != "System.Int32") { return; }

                    foreach (ComboEditorItem obj in _comboEditor.Items)
                    {
                        if ((int)((ComboItem)obj.Data).Id == (int)value)
                        {
                            obj.IsSelected = true;
                            return;
                        }
                    }
                }
                else if (_comboItemSource.Id_DataType == ComboItemList.DataType.GuidType)
                {
                    if (value.GetType().ToString() != "System.Guid") { return; }

                    foreach (ComboEditorItem obj in _comboEditor.Items)
                    {
                        if ((Guid)((ComboItem)obj.Data).Id == (Guid)value)
                        {
                            obj.IsSelected = true;
                            return;
                        }
                    }
                }
                else if (_comboItemSource.Id_DataType == ComboItemList.DataType.StringType)
                {
                    if (value.GetType().ToString() != "System.String") { return; }

                    foreach (ComboEditorItem obj in _comboEditor.Items)
                    {
                        if ((string)((ComboItem)obj.Data).Id == (string)value)
                        {
                            obj.IsSelected = true;
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem", IfxTraceCategory.Leave);
            }
        }

        #endregion Methods


    }
}
