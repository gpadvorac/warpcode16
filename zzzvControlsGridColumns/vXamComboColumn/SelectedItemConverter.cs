﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Collections.Generic;
using System.Linq;
using vComboDataTypes;

namespace vControls
{

    public class SelectedItemConverter : IValueConverter
    {

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (parameter == null || value == null)
            {
                return null;
            }

            ComboItemList data = parameter as ComboItemList;
            if (data == null) { return null; }
            string val = null;
            if (data.Id_DataType == ComboItemList.DataType.IntegerType)
            {
                val = (from cd in data
                       where (int)cd.Id == ((int)value)
                       select cd.ItemName).FirstOrDefault();
            }
            else if (data.Id_DataType == ComboItemList.DataType.GuidType)
            {
                val = (from cd in data
                       where (Guid)cd.Id == ((Guid)value)
                       select cd.ItemName).FirstOrDefault();
            }
            else if (data.Id_DataType == ComboItemList.DataType.StringType)
            {
                val = (from cd in data
                       where (string)cd.Id == ((string)value)
                       select cd.ItemName).FirstOrDefault();
            }

            return val;

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException("CustomColumns.SelectedItemConverter.Convert()  is not implemented.");
        }

        #endregion
    }
}
