﻿namespace vControls
{
    public interface IRowHoverBehavior
    {
        bool IsMouseOver { get; }

        void OnRowEnter();
        
        void OnRowLeave();
    }
}