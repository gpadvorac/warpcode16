﻿using System.Windows;
using System.Windows.Data;
using Infragistics.Controls.Grids;

namespace vControls
{
    public class vUnboundButtonColumnContentProvider : vCustomImageUnboundButtonColumnContentProviderBase
    {
        private vUnboundButtonColumn _column;
        private Cell _cell;

        public vUnboundButtonColumnContentProvider()
        {
            Button = new vUnboundButton();
            Button.Click += OnButtonClick;
        }

        protected override void SetBindings(Column baseColumn)
        {
            base.SetBindings(baseColumn);

            _column = (vUnboundButtonColumn)baseColumn;

            if (_column != null)
            {
                var imageSourceBinding = new Binding("MouseOverImageSource") { Source = _column, Mode = BindingMode.TwoWay };
                Button.SetBinding(vUnboundButton.MouseOverImageSourceProperty, imageSourceBinding);
            }
        }

        public override void AdjustDisplayElement(Cell cell)
        {
            _cell = cell;

            base.AdjustDisplayElement(cell);
        }

        private void OnButtonClick(object sender, RoutedEventArgs routedEventArgs)
        {
            var clickArgs = new ButtonColumnClickEventArgs(_cell.Row.Index, _cell.Row.Data);
            _column.OnClick(clickArgs);
            ResetContent();
        }
    }
}