﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace vControls
{
    public class vUnboundButton : vCustomImageButtonBase
    {
        public static readonly DependencyProperty MouseOverImageSourceProperty = DependencyProperty.Register("MouseOverImageSource", typeof (Uri), typeof (vUnboundButton), new PropertyMetadata(default(Uri)));

        public Uri MouseOverImageSource
        {
            get { return (Uri) GetValue(MouseOverImageSourceProperty); }
            set { SetValue(MouseOverImageSourceProperty, value); }
        }

        public vUnboundButton()
        {
            DefaultStyleKey = typeof (vUnboundButton);
        }

        public override void OnRowEnter()
        {
            SetState(IsEnabled ? vCustomImageButtonStates.Default : vCustomImageButtonStates.Disabled);
        }

        public override void OnRowLeave()
        {
            SetState(vCustomImageButtonStates.None);
        }

        protected override void OnMouseEnter(object sender, MouseEventArgs args)
        {
            SetState(IsEnabled ? vCustomImageButtonStates.Visible : vCustomImageButtonStates.Disabled);
        }

        protected override void OnMouseLeave(object sender, MouseEventArgs args)
        {
            SetState(vCustomImageButtonStates.None);
        }

        private void SetState(vCustomImageButtonStates state)
        {
            switch (state)
            {
                case vCustomImageButtonStates.Visible:
                    SetImageSource(new BitmapImage {UriSource = MouseOverImageSource});
                    break;
                case vCustomImageButtonStates.Default:
                    SetImageSource(new BitmapImage {UriSource = DefaultImageSource});
                    break;
                case vCustomImageButtonStates.Disabled:
                    SetImageSource(new BitmapImage {UriSource = DisabledImageSource});
                    break;
                case vCustomImageButtonStates.None:
                    SetImageSource(null);
                    break;
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            SetState(vCustomImageButtonStates.None);
        }
    }
}