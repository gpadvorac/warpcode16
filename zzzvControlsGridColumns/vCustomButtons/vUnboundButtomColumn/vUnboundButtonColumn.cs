﻿using System;
using System.Windows;
using Infragistics.Controls.Grids.Primitives;

namespace vControls
{
    public class vUnboundButtonColumn : vCustomImageUnboundButtonColumnBase
    {
        public static readonly DependencyProperty MouseOverImageSourceProperty = DependencyProperty.Register("MouseOverImageSource", typeof(Uri), typeof(vUnboundButtonColumn), new PropertyMetadata(default(Uri)));

        public Uri MouseOverImageSource
        {
            get { return (Uri)GetValue(MouseOverImageSourceProperty); }
            set { SetValue(MouseOverImageSourceProperty, value); }
        }

        protected override ColumnContentProviderBase GenerateContentProvider()
        {
            return new vUnboundButtonColumnContentProvider();
        }
    }
}