using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Controls.Grids;
using Infragistics.Controls.Grids.Primitives;

namespace vControls
{
    public abstract class vCustomImageButtonColumnContentProviderBase : ColumnContentProviderBase
    {
        protected vCustomImageButtonBase Button;

        public override FrameworkElement ResolveDisplayElement(Cell cell, Binding cellBinding)
        {
            FrameworkElement displayElement = null;

            if (!(cell.Row is FilterRow) && !(cell.Row is AddNewRow))
            {
                SetBindings(cell.Column);

                displayElement = Button;
            }

            return displayElement;
        }

        protected virtual void SetBindings(Column baseColumn)
        {
            var column = (vCustomImageButtonColumnBase)baseColumn;

            if (column != null)
            {
                if (!string.IsNullOrEmpty(column.IsEnabledPath))
                {
                    var isEnabledBinding = new Binding(column.IsEnabledPath) { Mode = BindingMode.OneWay };

                    //Binding to show proper image
                    Button.SetBinding(vCustomImageButtonBase.IsEnabledProperty, isEnabledBinding);
                    //Binding to enable/disable button itself
                    Button.SetBinding(Control.IsEnabledProperty, isEnabledBinding);
                }

                var defaultImageSourceBinding = new Binding("DefaultImageSource") { Source = column, Mode = BindingMode.TwoWay };
                Button.SetBinding(vCustomImageButtonBase.DefaultImageSourceProperty, defaultImageSourceBinding);

                var disabledImageSourceBinding = new Binding("DisabledImageSource") { Source = column, Mode = BindingMode.TwoWay };
                Button.SetBinding(vCustomImageButtonBase.DisabledImageSourceProperty, disabledImageSourceBinding);
            }
        }

        protected override FrameworkElement ResolveEditorControl(Cell cell, object editorValue, double availableWidth, double availableHeight, Binding editorBinding)
        {
            return null;
        }

        public override object ResolveValueFromEditor(Cell cell)
        {
            return null;
        }
    }
}