using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using Infragistics.Controls.Grids;
using vTooltipProvider;

namespace vControls
{
    public abstract class vCustomImageUnboundButtonColumnBase : UnboundColumn, IvColumn
    {
        public static readonly DependencyProperty IsEnabledPathProperty = DependencyProperty.Register("IsEnabledPath", typeof(string), typeof(vCustomImageUnboundButtonColumnBase), new PropertyMetadata(string.Empty));

        public static readonly DependencyProperty DefaultImageSourceProperty = DependencyProperty.Register("DefaultImageSource", typeof(Uri), typeof(vCustomImageUnboundButtonColumnBase), new PropertyMetadata(default(Uri)));

        public static readonly DependencyProperty DisabledImageSourceProperty = DependencyProperty.Register("DisabledImageSource", typeof(Uri), typeof(vCustomImageUnboundButtonColumnBase), new PropertyMetadata(default(Uri)));

        public string IsEnabledPath
        {
            get { return (string)GetValue(IsEnabledPathProperty); }
            set { SetValue(IsEnabledPathProperty, value); }
        }

        public Uri DefaultImageSource
        {
            get { return (Uri)GetValue(DefaultImageSourceProperty); }
            set { SetValue(DefaultImageSourceProperty, value); }
        }

        public Uri DisabledImageSource
        {
            get { return (Uri)GetValue(DisabledImageSourceProperty); }
            set { SetValue(DisabledImageSourceProperty, value); }
        }

        public event EventHandler<ButtonColumnClickEventArgs> Click;
        protected internal void OnClick(ButtonColumnClickEventArgs e)
        {
            if (Click != null)
            {
                Click(this, e);
            }
        }

        #region IvColumn

        /// <summary>
        /// Identifies the <see cref="HeaderToolTipCaption"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipCaptionProperty = DependencyProperty.Register("HeaderToolTipCaption", typeof(String), typeof(vCustomImageUnboundButtonColumnBase), new PropertyMetadata(""));

        /// <summary>
        /// Identifies the <see cref="HeaderToolTipStringArray"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipStringArrayProperty = DependencyProperty.Register("HeaderToolTipStringArray", typeof(String[]), typeof(vCustomImageUnboundButtonColumnBase), new PropertyMetadata(null));

        /// <summary>
        /// Identifies the <see cref="HeaderToolTipItemsSource"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipItemsSourceProperty = DependencyProperty.Register("HeaderToolTipItemsSource", typeof(IEnumerable), typeof(vCustomImageUnboundButtonColumnBase), new PropertyMetadata(null));

        /// <summary>
        /// Identifies the <see cref="HeaderTooltipContent"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderTooltipContentProperty = DependencyProperty.Register("HeaderTooltipContent", typeof(IvTooltipContent), typeof(vCustomImageUnboundButtonColumnBase), new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a HeaderToolTipCaption
        /// </summary>
        public String HeaderToolTipCaption
        {
            get { return (String)GetValue(HeaderToolTipCaptionProperty); }
            set { SetValue(HeaderToolTipCaptionProperty, value); }
        }

        /// <summary>
        /// Gets or sets a string array used to generate a collectin for the tooltip content
        /// </summary>
        public String[] HeaderToolTipStringArray
        {
            get { return (String[])GetValue(HeaderToolTipStringArrayProperty); }
            set { SetValue(HeaderToolTipStringArrayProperty, value); }
        }

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public IEnumerable HeaderToolTipItemsSource
        {
            get { return (IEnumerable)GetValue(HeaderToolTipItemsSourceProperty); }
            set { SetValue(HeaderToolTipItemsSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public IvTooltipContent HeaderTooltipContent
        {
            get { return (IvTooltipContent)GetValue(HeaderTooltipContentProperty); }
            set { SetValue(HeaderTooltipContentProperty, value); }
        }

        #endregion IvColumn
    }
}