﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace vControls
{
    public abstract class vCustomImageButtonBase : Button, IRowHoverBehavior
    {
        public new static readonly DependencyProperty IsEnabledProperty = DependencyProperty.Register("IsEnabled", typeof(bool), typeof(vCustomImageButtonBase), new PropertyMetadata(true));

        public static readonly DependencyProperty DefaultImageSourceProperty = DependencyProperty.Register("DefaultImageSource", typeof(Uri), typeof(vCustomImageButtonBase), new PropertyMetadata(default(Uri)));

        public static readonly DependencyProperty DisabledImageSourceProperty = DependencyProperty.Register("DisabledImageSource", typeof(Uri), typeof(vCustomImageButtonBase), new PropertyMetadata(default(Uri)));

        public new bool IsEnabled
        {
            get { return (bool) GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }

        public Uri DefaultImageSource
        {
            get { return (Uri) GetValue(DefaultImageSourceProperty); }
            set { SetValue(DefaultImageSourceProperty, value); }
        }

        public Uri DisabledImageSource
        {
            get { return (Uri) GetValue(DisabledImageSourceProperty); }
            set { SetValue(DisabledImageSourceProperty, value); }
        }

        protected Image Image;

        protected vCustomImageButtonBase()
        {
            DefaultStyleKey = typeof (vItemCountButton);

            MouseEnter += OnMouseEnter;
            MouseLeave += OnMouseLeave;
        }

        public abstract void OnRowEnter();
        public abstract void OnRowLeave();
        protected abstract void OnMouseEnter(object sender, MouseEventArgs mouseEventArgs);
        protected abstract void OnMouseLeave(object sender, MouseEventArgs mouseEventArgs);

        protected void SetImageSource(ImageSource source)
        {
            Image.Source = source;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Image = GetTemplateChild("PART_Image") as Image;
        }
    }
}