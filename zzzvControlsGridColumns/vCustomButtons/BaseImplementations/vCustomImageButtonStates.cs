namespace vControls
{
    public enum vCustomImageButtonStates
    {
        Text,
        Visible,
        Add,
        Edit,
        Disabled,
        Default,
        None,
    }
}