﻿using System.Collections;

namespace vControls
{
    public interface IContextMenuItemsSource
    {
        IEnumerable GetContextMenuItemsSource();
    }
}