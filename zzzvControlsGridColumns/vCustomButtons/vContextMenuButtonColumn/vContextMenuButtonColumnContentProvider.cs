﻿using System.Windows.Data;
using Infragistics.Controls.Grids;

namespace vControls
{
    public class vContextMenuButtonColumnContentProvider : vCustomImageUnboundButtonColumnContentProviderBase
    {
        public vContextMenuButtonColumnContentProvider()
        {
            Button = new vContextMenuButton();
        }
        
        protected override void SetBindings(Column baseColumn)
        {
            base.SetBindings(baseColumn);

            var column = (vContextMenuButtonColumn)baseColumn;

            if (column != null)
            {
                //var itemsSourceBinding = new Binding(column.ContextMenuItemsSourcePath) { Mode = BindingMode.OneWay };
                //Button.SetBinding(vContextMenuButton.ContextMenuItemsSourceProperty, itemsSourceBinding);

                var imageSourceBinding = new Binding("MouseOverImageSource") { Source = column, Mode = BindingMode.TwoWay };
                Button.SetBinding(vUnboundButton.MouseOverImageSourceProperty, imageSourceBinding);
            }
        }
    }
}