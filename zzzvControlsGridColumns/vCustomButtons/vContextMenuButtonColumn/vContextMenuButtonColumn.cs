﻿using System.Windows;
using Infragistics.Controls.Grids.Primitives;

namespace vControls
{
    public class vContextMenuButtonColumn : vUnboundButtonColumn
    {
        public static readonly DependencyProperty ContextMenuItemsSourcePathProperty = DependencyProperty.Register("ContextMenuItemsSourcePathPath", typeof (string), typeof (vContextMenuButtonColumn), new PropertyMetadata(string.Empty));

        public string ContextMenuItemsSourcePath
        {
            get { return (string) GetValue(ContextMenuItemsSourcePathProperty); }
            set { SetValue(ContextMenuItemsSourcePathProperty, value); }
        }

        protected override ColumnContentProviderBase GenerateContentProvider()
        {
            return new vContextMenuButtonColumnContentProvider();
        }
    }
}