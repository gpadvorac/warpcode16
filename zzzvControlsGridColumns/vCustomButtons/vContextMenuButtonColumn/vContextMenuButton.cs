﻿using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Controls.Menus;
using vControls.vCustomContextMenu;
using ContextMenuService = Infragistics.Controls.Menus.ContextMenuService;

namespace vControls
{
    public class vContextMenuButton : vUnboundButton
    {
        public static readonly DependencyProperty ContextMenuItemsSourceProperty = DependencyProperty.Register("ContextMenuItemsSource", typeof (IEnumerable), typeof (vContextMenuButton), new PropertyMetadata(default(IEnumerable), OnContextMenuItemsSourcePropertyChanged));

        public IEnumerable<MenuItemModel> ContextMenuItemsSource
        {
            get { return (IEnumerable<MenuItemModel>)GetValue(ContextMenuItemsSourceProperty); }
            set { SetValue(ContextMenuItemsSourceProperty, value); }
        }

        private XamContextMenu _contextMenu;

        public vContextMenuButton()
        {
            DefaultStyleKey = typeof (vContextMenuButton);

            Click += OnButtonClick;
        }

        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            var dataContext = (IContextMenuItemsSource)DataContext;
            if (dataContext != null)
            {
                _contextMenu.SetValue(XamContextMenu.ItemsSourceProperty, dataContext.GetContextMenuItemsSource());
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _contextMenu = ContextMenuService.GetManager(this).ContextMenu;
            _contextMenu.SetBinding(ItemsControl.ItemsSourceProperty, new Binding("ContextMenuItemsSource") { Source = this });
        }

        private static void OnContextMenuItemsSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            var button = d as vContextMenuButton;
            if (button != null && button._contextMenu != null && args.NewValue != null)
            {
                button._contextMenu.SetBinding(XamContextMenu.ItemsSourceProperty, new Binding("ContextMenuItemsSource") { Source = button });
            }
        }
    }
}