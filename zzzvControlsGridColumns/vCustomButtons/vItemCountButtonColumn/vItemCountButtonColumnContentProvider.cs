﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Data;
using Infragistics.Controls.Grids;

namespace vControls
{
    public class vItemCountButtonColumnContentProvider : vCustomImageButtonColumnContentProviderBase
    {
        private vItemCountButtonColumn _column;
        private Cell _cell;

        public vItemCountButtonColumnContentProvider()
        {
            try
            {
                Button = new vItemCountButton();
                Button.Click += OnButtonClick;
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            } 
        }

        private void OnButtonClick(object sender, RoutedEventArgs routedEventArgs)
        {
            try
            {
                var clickArgs = new ButtonColumnClickEventArgs(_cell.Row.Index, _cell.Row.Data);
                _column.OnClick(clickArgs);
                ResetContent();
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }

        protected override void SetBindings(Column baseColumn)
        {
            try
            {
                base.SetBindings(baseColumn);

                _column = (vItemCountButtonColumn)baseColumn;

                if (_column != null)
                {
                    var binding = new Binding(_column.Key) { Mode = BindingMode.OneWay, Converter = _column.ItemCountConverter};
                    Button.SetBinding(vItemCountButton.ItemsCountProperty, binding);

                    var addImageSourceBinding = new Binding("AddImageSource") { Source = _column, Mode = BindingMode.TwoWay };
                    Button.SetBinding(vItemCountButton.AddImageSourceProperty, addImageSourceBinding);

                    var editImageSourceBinding = new Binding("EditImageSource") { Source = _column, Mode = BindingMode.TwoWay };
                    Button.SetBinding(vItemCountButton.EditImageSourceProperty, editImageSourceBinding);
                }
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }

        public override void AdjustDisplayElement(Cell cell)
        {
            try
            {
                if (_cell != null)
                {
                    _cell.PropertyChanged -= CellOnPropertyChanged;
                }
                _cell = cell;
                _cell.PropertyChanged += CellOnPropertyChanged;

                base.AdjustDisplayElement(cell);
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }

        private void CellOnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            try
            {
                if (args.PropertyName == "Value")
                {
                    var cell = (Cell)sender;
                    AdjustDisplayElement(cell);
                    ResetContent();
                }
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }
    }
}