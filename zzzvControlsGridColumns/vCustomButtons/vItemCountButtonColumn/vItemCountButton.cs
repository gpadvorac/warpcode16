﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace vControls
{
    public class vItemCountButton  : vCustomImageButtonBase
    {
        public static readonly DependencyProperty ItemsCountProperty = DependencyProperty.Register("ItemsCount", typeof(int), typeof(vItemCountButton), new PropertyMetadata(default(int), ItemsCountPropertyChanged));

        public static readonly DependencyProperty AddImageSourceProperty = DependencyProperty.Register("AddImageSource", typeof(Uri), typeof(vItemCountButton), new PropertyMetadata(default(Uri)));

        public static readonly DependencyProperty EditImageSourceProperty = DependencyProperty.Register("EditImageSource", typeof(Uri), typeof(vItemCountButton), new PropertyMetadata(default(Uri)));

        public int ItemsCount
        {
            get { return (int)GetValue(ItemsCountProperty); }
            set { SetValue(ItemsCountProperty, value); }
        }

        public Uri AddImageSource
        {
            get { return (Uri)GetValue(AddImageSourceProperty); }
            set { SetValue(AddImageSourceProperty, value); }
        }

        public Uri EditImageSource
        {
            get { return (Uri)GetValue(EditImageSourceProperty); }
            set { SetValue(EditImageSourceProperty, value); }
        }

        private TextBlock _textBlock;
        private bool _isLoaded;

        public vItemCountButton()
        {
            try
            {
                DefaultStyleKey = typeof(vItemCountButton);
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }

        public override void OnRowEnter()
        {
            try
            {
                var count = (int)GetValue(ItemsCountProperty);
                SetState(IsEnabled
                    ? count > 0 ? vCustomImageButtonStates.Text : vCustomImageButtonStates.Default
                    : vCustomImageButtonStates.Disabled);
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }

        public override void OnRowLeave()
        {
            try
            {
                var count = (int)GetValue(ItemsCountProperty);
                SetNormalState(count);
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }

        protected override void OnMouseEnter(object sender, MouseEventArgs args)
        {
            try
            {
                var count = (int)GetValue(ItemsCountProperty);
                SetState(IsEnabled
                    ? count > 0 ? vCustomImageButtonStates.Edit : vCustomImageButtonStates.Add
                    : vCustomImageButtonStates.Disabled);
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }

        protected override void OnMouseLeave(object sender, MouseEventArgs args)
        {
            try
            {
                var count = (int)GetValue(ItemsCountProperty);
                SetNormalState(count);
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }

        private void SetState(vCustomImageButtonStates vItemCountButtonState)
        {
            try
            {
                _textBlock.Visibility = Visibility.Collapsed;

                switch (vItemCountButtonState)
                {
                    case vCustomImageButtonStates.Text:
                        _textBlock.Visibility = Visibility.Visible;
                        SetImageSource(null);
                        break;
                    case vCustomImageButtonStates.Add:
                        SetImageSource(new BitmapImage { UriSource = AddImageSource });
                        break;
                    case vCustomImageButtonStates.Edit:
                        SetImageSource(new BitmapImage { UriSource = EditImageSource });
                        break;
                    case vCustomImageButtonStates.Default:
                        SetImageSource(new BitmapImage { UriSource = DefaultImageSource });
                        break;
                    case vCustomImageButtonStates.Disabled:
                        SetImageSource(new BitmapImage { UriSource = DisabledImageSource });
                        break;
                    case vCustomImageButtonStates.None:
                        SetImageSource(null);
                        break;
                }
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }

        public override void OnApplyTemplate()
        {
            try
            {
                base.OnApplyTemplate();

                _textBlock = GetTemplateChild("PART_Text") as TextBlock;
                _isLoaded = true;

                UpdateItemsState();
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }

        public void UpdateItemsState()
        {
            try
            {
                var count = (int)GetValue(ItemsCountProperty);
                SetNormalState(count);
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }

        private void SetNormalState(int count)
        {
            try
            {
                SetState(count > 0 ? vCustomImageButtonStates.Text : vCustomImageButtonStates.None);
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }

        private static void ItemsCountPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            try
            {
                var button = d as vItemCountButton;
                if (button != null && button._isLoaded)
                {
                    button.UpdateItemsState();
                }
            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }
    }
}