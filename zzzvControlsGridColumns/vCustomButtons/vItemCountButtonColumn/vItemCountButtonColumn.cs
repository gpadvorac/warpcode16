﻿using System;
using System.Windows;
using System.Windows.Data;
using Infragistics.Controls.Grids.Primitives;

namespace vControls
{
    public class vItemCountButtonColumn : vCustomImageButtonColumnBase
    {
        public static readonly DependencyProperty AddImageSourceProperty = DependencyProperty.Register("AddImageSource", typeof(Uri), typeof(vItemCountButtonColumn), new PropertyMetadata(default(Uri)));

        public static readonly DependencyProperty EditImageSourceProperty = DependencyProperty.Register("EditImageSource", typeof (Uri), typeof (vItemCountButtonColumn), new PropertyMetadata(default(Uri)));

        public static readonly DependencyProperty ItemCountConverterProperty = DependencyProperty.Register("ItemCountConverter", typeof (IValueConverter), typeof (vItemCountButtonColumn), new PropertyMetadata(default(IValueConverter)));

        public Uri AddImageSource
        {
            get { return (Uri)GetValue(AddImageSourceProperty); }
            set { SetValue(AddImageSourceProperty, value); }
        }

        public Uri EditImageSource
        {
            get { return (Uri)GetValue(EditImageSourceProperty); }
            set { SetValue(EditImageSourceProperty, value); }
        }

        public IValueConverter ItemCountConverter
        {
            get { return (IValueConverter)GetValue(ItemCountConverterProperty); }
            set { SetValue(ItemCountConverterProperty, value); }
        }

        protected override ColumnContentProviderBase GenerateContentProvider()
        {
            return new vItemCountButtonColumnContentProvider();
        }
    }
}