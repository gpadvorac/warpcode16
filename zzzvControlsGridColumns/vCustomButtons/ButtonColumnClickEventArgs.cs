﻿using System;
using Infragistics.Controls.Grids;

namespace vControls
{
    public class ButtonColumnClickEventArgs : EventArgs
    {
        int _index;
        object _data;
        Cell _cell;

        internal ButtonColumnClickEventArgs(int index, object data)
        {
            _index = index;
            _data = data;
        }

        internal ButtonColumnClickEventArgs(Cell cell, int index, object data)
        {
            _cell = cell;
            _index = index;
            _data = data;
        }

        public Cell GridCell { get { return _cell; } }
        public int Index { get { return _index; } }
        public object Data { get { return _data; } }
    }
}