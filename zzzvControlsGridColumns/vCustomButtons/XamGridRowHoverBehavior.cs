﻿using System.Linq;
using System.Windows.Input;
using System.Windows.Interactivity;
using Infragistics.Controls.Grids;

namespace vControls
{
    public class XamGridRowHoverBehavior : Behavior<XamGrid>
    {
        private Row _lastHoverRow;

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.MouseMove += OnMouseMove;
        }
    
        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.MouseMove -= OnMouseMove;
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var row = AssociatedObject.Rows.FirstOrDefault(r => r.IsMouseOver.Equals(true));

            if (_lastHoverRow != null)
            {
                var lastRowCells = _lastHoverRow.Cells.Where(x => x.Column is vItemCountButtonColumn || x.Column is vUnboundButtonColumn);
                foreach (var cell in lastRowCells)
                {
                    if (cell.Control != null && cell.Control.Content != null)
                    {
                        var customButton = cell.Control.Content as IRowHoverBehavior;
                        if (customButton != null)
                        {
                            if (!customButton.IsMouseOver)
                            {
                                customButton.OnRowLeave();
                            }
                        }
                    }
                }
            }

            if (row != null)
            {
                var newRowCells = row.Cells.Where(x => x.Column is vItemCountButtonColumn || x.Column is vUnboundButtonColumn);
                foreach (var cell in newRowCells)
                {
                    if (cell.Control !=null && cell.Control.Content != null)
                    {
                        var customButton = cell.Control.Content as IRowHoverBehavior;
                        if (customButton != null)
                        {
                            if (!customButton.IsMouseOver)
                            {
                                customButton.OnRowEnter();
                            }
                        }
                    }
                }

                _lastHoverRow = row;
            }

        }
    }
}