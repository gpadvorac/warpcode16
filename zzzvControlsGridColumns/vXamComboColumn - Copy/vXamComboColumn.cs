﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Infragistics.Controls.Grids;
using System.Collections;
using vComboDataTypes;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Markup;
//using System.Diagnostics;
using vTooltipProvider;
using System.Text;
using Infragistics.Controls.Editors;

namespace vControls
{
    public class vXamComboColumn : vComboColumnBase
    {

        #region Initialize Variables

        public event EventHandler<vXamComboColumnSelectionChangedEventArgs> SelectionChanged;
        public event EventHandler<vXamComboColumnKeyEventArgs> KeyUp;
        public event EventHandler<vXamComboColumnKeyEventArgs> KeyDown;
        public event EventHandler<vXamComboColumnItemAddingEventArgs> ItemAdding;


        #endregion Initialize Variables


        #region Constructor

        public vXamComboColumn()
        {
            //this.SortComparer = new MySortComparer();
        }

        #endregion Constructor


        #region Properties

        #region ItemSource

        /// <summary>
        /// Identifies the <see cref="ItemSource"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource",
            typeof(IEnumerable),
            typeof(vXamComboColumn),
            new PropertyMetadata(new PropertyChangedCallback(ItemsSourceChanged)));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)this.GetValue(ItemsSourceProperty); }
            set
            {
                try
                {
                    this.SetValue(ItemsSourceProperty, value);

                    // Because this column is designed to work with 3 different data types (int, guid and string), we need to have a SortComparer for each type
                    //if (DataType == typeof(Guid))
                    if (((ComboItemList)value).Id_DataType == ComboItemList.DataType.IntegerType)
                    {
                        this.SortComparer = new MyIntSortComparer((ComboItemList)value, this);
                    }
                    else if (((ComboItemList)value).Id_DataType == ComboItemList.DataType.GuidType)
                    {
                        this.SortComparer = new MyGuidSortComparer((ComboItemList)value, this);
                    }
                    else if (((ComboItemList)value).Id_DataType == ComboItemList.DataType.StringType)
                    {
                        this.SortComparer = new MyStringSortComparer((ComboItemList)value, this);
                    }
               

                    // Configure the GroupByItemTemplate
                    string key = (string)this.GetValue(DisplayTextBindingProperty);
                    if (!string.IsNullOrEmpty(key))
                    {
                        //  DisplayTextBindingProperty has a value so we can use it for the TextBlock binding in the data template
                        GroupByItemTemplate = (DataTemplate)XamlReader.Load(
                                @"<DataTemplate 
                        xmlns=""http://schemas.microsoft.com/client/2007"">                            
                            <StackPanel " + @"Orientation=""Horizontal"">"
                                        + "<TextBlock" + @" Text=""{Binding Records[0]." + key + @"}""/>"
                                        + "<TextBlock" + @" Text=""{Binding Count" + @", StringFormat=': (0)'}""/>"
                                    + "</StackPanel>"
                                + "</DataTemplate>"
                        );
                    }
                    else
                    {
                        // DisplayTextBindingProperty is null so we have to use a converter in ComboItemList
                        // But dont think we can apply it here.
                        // is there a way to use a static reference the combo's ItemsSource in a binding below?
                        // if so, we could bind to a converter and pass the combo's ItemsSource in as a parameter.
                        // The Items source would have enough metadata to figure out what to do in the conerter.
                        ComboItemList list = value as ComboItemList;
                        if (list == null) { return; }

                        string items = "";

                        foreach (ComboItem item in list)
                        {
                            items += @"<data:ComboItem Id=""" + item.Id + @""""
                                                       + @" ItemName=""" + item.ItemName + @""""
                                                       + @" FK=""" + item.FK + @""""
                                                       + @" Desc=""" + item.Desc + @"""" + "/> ";
                        }

                        GroupByItemTemplate = (DataTemplate)XamlReader.Load(
                                @"<DataTemplate 
                        xmlns=""http://schemas.microsoft.com/client/2007"""
                                + " xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml' "
                                + " xmlns:data='clr-namespace:vComboDataTypes;assembly=vComboDataTypes'>"
                                + "<StackPanel " + @"Orientation=""Horizontal"">"
                                        + "<StackPanel.Resources>"
                                                + @"<data:ComboItemList x:Key=""comboItemListKey"">"
                                                    + items
                                                + @"</data:ComboItemList >"
                                                + @"<data:ComboItemForGroupByConverter x:Key=""myConverter""/>"
                                        + "</StackPanel.Resources>"
                                        + "<TextBlock" + @" Text=""{Binding Converter={StaticResource myConverter}, ConverterParameter={StaticResource comboItemListKey}}""/>"
                                        + "<TextBlock" + @" Text=""{Binding Count" + @", StringFormat=': (0)'}""/>"
                                    + "</StackPanel>"
                                + "</DataTemplate>"
                        );
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        private static void ItemsSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vXamComboColumn col = (vXamComboColumn)obj;
            col.OnPropertyChanged("ItemsSource");
        }

        #endregion // ItemSource


        #region DisplayMemberPath

        /// <summary>
        /// Identifies the <see cref="DisplayMemberPath"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty DisplayMemberPathProperty = DependencyProperty.Register("DisplayMemberPath",
            typeof(string),
            typeof(vXamComboColumn),
            new PropertyMetadata(new PropertyChangedCallback(DisplayMemberPathChanged)));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public string DisplayMemberPath
        {
            get { return (string)this.GetValue(DisplayMemberPathProperty); }
            set { this.SetValue(DisplayMemberPathProperty, value); }
        }

        private static void DisplayMemberPathChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vXamComboColumn col = (vXamComboColumn)obj;
            col.OnPropertyChanged("DisplayMemberPath");
        }

        #endregion  DisplayMemberPath


        #region ItemTemplate

        /// <summary>
        /// Identifies the <see cref="ItemTemplate"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty ItemTemplateProperty = DependencyProperty.Register("ItemTemplate",
            typeof(DataTemplate),
            typeof(vXamComboColumn),
            new PropertyMetadata(new PropertyChangedCallback(ItemTemplateChanged)));

        /// <summary>
        /// Gets or sets the <see cref="DataTemplate"/> used to display each item.
        /// </summary>
        public string ItemTemplate
        {
            get { return (string)this.GetValue(ItemTemplateProperty); }
            set { this.SetValue(ItemTemplateProperty, value); }
        }

        private static void ItemTemplateChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vXamComboColumn col = (vXamComboColumn)obj;
            col.OnPropertyChanged("ItemTemplate");
        }

        #endregion // ItemTemplate


        #region ComboBoxStyle

        /// <summary>
        /// Identifies the <see cref="ComboBoxStyle"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty ComboBoxStyleProperty = DependencyProperty.Register("ComboBoxStyle",
            typeof(Style),
            typeof(vXamComboColumn),
            new PropertyMetadata(new PropertyChangedCallback(ComboBoxStyleChanged)));

        /// <summary>
        /// Gets or sets the style used by ComboBox element when it is rendered
        /// </summary>
        public string ComboBoxStyle
        {
            get { return (string)this.GetValue(ComboBoxStyleProperty); }
            set { this.SetValue(ComboBoxStyleProperty, value); }
        }

        private static void ComboBoxStyleChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vXamComboColumn col = (vXamComboColumn)obj;
            col.OnPropertyChanged("ComboBoxStyle");
        }

        #endregion // ComboBoxStyle


        #region DisplayTextBinding

        /// <summary>
        /// Identifies the <see cref="DisplayTextBinding"/> dependency property. 
        /// DisplayTextBinding is used to improve performance for large grids where many instances of combo values must
        /// be converted to text.
        /// This property will  bind to the display text when not in edit mode.  Using this property replaces the need
        /// to use a value converter to get the display text from the Combo's ItemSouce item.  Using this
        /// will require an additional text field for the non-text field which the combo is bound to.  The use of this property 
        /// requires a strict naming convention.  For example:  If the combo is bound to an integer field named "CustomerID", 
        /// the text field bound to DisplayTextBinding must be - "CustomerID_TextField"  (field name + "_TextField")'
        /// </summary>
        public static readonly DependencyProperty DisplayTextBindingProperty = DependencyProperty.Register("DisplayTextBinding",
            typeof(string),
            typeof(vXamComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public string DisplayTextBinding
        {
            get { return (string)this.GetValue(DisplayTextBindingProperty); }
            set { this.SetValue(DisplayTextBindingProperty, value); }
        }

        //private static void DisplayTextBindingChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamComboColumn col = (vXamComboColumn)obj;
        //    col.OnPropertyChanged("DisplayTextBinding");
        //}

        #endregion  DisplayTextBinding



        #region AllowConditionalFormating

        /// <summary>
        /// Identifies the <see cref="AllowConditionalFormating"/> dependency property. 
        /// If True, then Conditional Formating is alloed,  - HOWEVER:  Text used in the DisplayName or Description properties must be valid for the XML parser.
        /// This text will be used in a XML data template and illegal XML charecters will make the application crash.
        /// </summary>
        public static readonly DependencyProperty AllowConditionalFormatingProperty = DependencyProperty.Register("AllowConditionalFormating",
            typeof(bool),
            typeof(vXamComboColumn),
            new PropertyMetadata(false));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public bool AllowConditionalFormating
        {
            get { return (bool)this.GetValue(AllowConditionalFormatingProperty); }
            set
            {
                this.SetValue(AllowConditionalFormatingProperty, value);
            }
        }

        //private static void AllowConditionalFormatingChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamMultiColumnComboColumn col = (vXamMultiColumnComboColumn)obj;
        //    col.OnPropertyChanged("AllowConditionalFormating");
        //}

        #endregion  AllowConditionalFormating

        #region IsChildCombo

        /// <summary>
        /// Identifies the <see cref="IsChildCombo"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty IsChildComboProperty = DependencyProperty.Register("IsChildCombo",
            typeof(bool),
            typeof(vXamComboColumn),
            new PropertyMetadata(false));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public bool IsChildCombo
        {
            get { return (bool)this.GetValue(IsChildComboProperty); }
            set { this.SetValue(IsChildComboProperty, value); }
        }

        //private static void IsChildComboChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamComboColumn col = (vXamComboColumn)obj;
        //    col.OnPropertyChanged("IsChildCombo");
        //}

        #endregion  IsChildCombo


        #region MustUpdateChildCombo

        /// <summary>
        /// Identifies the <see cref="MustUpdateChildCombo"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty MustUpdateChildComboProperty = DependencyProperty.Register("MustUpdateChildCombo",
            typeof(bool),
            typeof(vXamComboColumn),
            new PropertyMetadata(false));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public bool MustUpdateChildCombo
        {
            get { return (bool)this.GetValue(MustUpdateChildComboProperty); }
            set { this.SetValue(MustUpdateChildComboProperty, value); }
        }

        //private static void MustUpdateChildComboChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamComboColumn col = (vXamComboColumn)obj;
        //    col.OnPropertyChanged("MustUpdateChildCombo");
        //}

        #endregion  MustUpdateChildCombo


        #region ParentComboKey

        /// <summary>
        /// Identifies the <see cref="ParentComboKey"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ParentComboKeyProperty = DependencyProperty.Register("ParentComboKey",
            typeof(string),
            typeof(vXamComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public string ParentComboKey
        {
            get { return (string)this.GetValue(ParentComboKeyProperty); }
            set { this.SetValue(ParentComboKeyProperty, value); }
        }

        //private static void ParentComboKeyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamComboColumn col = (vXamComboColumn)obj;
        //    col.OnPropertyChanged("ParentComboKey");
        //}

        #endregion  ParentComboKey


        #region ChildComboKey

        /// <summary>
        /// Identifies the <see cref="ChildComboKey"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ChildComboKeyProperty = DependencyProperty.Register("ChildComboKey",
            typeof(string),
            typeof(vXamComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public string ChildComboKey
        {
            get { return (string)this.GetValue(ChildComboKeyProperty); }
            set { this.SetValue(ChildComboKeyProperty, value); }
        }

 
        #endregion  ChildComboKey

        
        #region ChildComboKey2

        /// <summary>
        /// Identifies the <see cref="ChildComboKey2"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ChildComboKey2Property = DependencyProperty.Register("ChildComboKey2",
            typeof(string),
            typeof(vXamComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public string ChildComboKey2
        {
            get { return (string)this.GetValue(ChildComboKey2Property); }
            set { this.SetValue(ChildComboKey2Property, value); }
        }


        #endregion  ChildComboKey2

        
        #region ChildComboKey3

        /// <summary>
        /// Identifies the <see cref="ChildComboKey3"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ChildComboKey3Property = DependencyProperty.Register("ChildComboKey3",
            typeof(string),
            typeof(vXamComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public string ChildComboKey3
        {
            get { return (string)this.GetValue(ChildComboKey3Property); }
            set { this.SetValue(ChildComboKey3Property, value); }
        }


        #endregion  ChildComboKey3

        
        #region ChildComboKey4

        /// <summary>
        /// Identifies the <see cref="ChildComboKey4"/> dependency property. 
        /// If True, then this property will use the value from ParentIdValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty ChildComboKey4Property = DependencyProperty.Register("ChildComboKey4",
            typeof(string),
            typeof(vXamComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public string ChildComboKey4
        {
            get { return (string)this.GetValue(ChildComboKey4Property); }
            set { this.SetValue(ChildComboKey4Property, value); }
        }


        #endregion  ChildComboKey4

        

        #region SelectedValue

        /// <summary>
        /// Identifies the <see cref="SelectedValue"/> dependency property. 
        /// If True, then this property will use the value from SelectedValue to filter it's list.
        /// </summary>
        public static readonly DependencyProperty SelectedValueProperty = DependencyProperty.Register("SelectedValue",
            typeof(object),
            typeof(vXamComboColumn),
            new PropertyMetadata(new PropertyChangedCallback(SelectedValueChanged)));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public object SelectedValue
        {
            get { return (object)this.GetValue(SelectedValueProperty); }
            set { this.SetValue(SelectedValueProperty, value); }
        }

        private static void SelectedValueChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            //vXamComboColumn col = (vXamComboColumn)obj;
            //col.OnPropertyChanged("SelectedValue");
        }

        #endregion  SelectedValue


        #region DropdownWidth

        /// <summary>
        /// Identifies the <see cref="DropdownWidth"/> dependency property. 
        /// Sets the width of the dropdown list. Default value is "0" which means this property will not be used.
        /// </summary>
        public static readonly DependencyProperty DropdownWidthProperty = DependencyProperty.Register("DropdownWidth",
            typeof(double),
            typeof(vXamComboColumn),
            new PropertyMetadata(0.0));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public double DropdownWidth
        {
            get { return (double)this.GetValue(DropdownWidthProperty); }
            set { this.SetValue(DropdownWidthProperty, value); }
        }

        #endregion  DropdownWidth


        #region MaxDropDownHeight

        /// <summary>
        /// Identifies the <see cref="MaxDropDownHeight"/> dependency property. 
        /// Sets the width of the dropdown list. Default value is "0" which means this property will not be used.
        /// </summary>
        public static readonly DependencyProperty MaxDropDownHeightProperty = DependencyProperty.Register("MaxDropDownHeight",
            typeof(double),
            typeof(vXamComboColumn),
            new PropertyMetadata(0.0));

        /// <summary>
        /// Gets or sets a path to a value on the source object to serve as the visual representation of the object.
        /// </summary>
        public double MaxDropDownHeight
        {
            get { return (double)this.GetValue(MaxDropDownHeightProperty); }
            set { this.SetValue(MaxDropDownHeightProperty, value); }
        }

        #endregion  MaxDropDownHeight




        #region CustomValueEnteredAction

        /// <summary>
        /// Identifies the <see cref="CustomValueEnteredAction"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty CustomValueEnteredActionProperty = DependencyProperty.Register("CustomValueEnteredAction",
            typeof(Infragistics.Controls.Editors.CustomValueEnteredActions),
            typeof(vXamComboColumn),
            new PropertyMetadata(new PropertyChangedCallback(CustomValueEnteredActionChanged)));

        /// <summary>
        /// 
        /// </summary>
        public Infragistics.Controls.Editors.CustomValueEnteredActions CustomValueEnteredAction
        {
            get { return (Infragistics.Controls.Editors.CustomValueEnteredActions)this.GetValue(CustomValueEnteredActionProperty); }
            set { this.SetValue(CustomValueEnteredActionProperty, value); }
        }

        private static void CustomValueEnteredActionChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            //vXamComboColumn col = (vXamComboColumn)obj;
            //col.OnPropertyChanged("CustomValueEnteredAction");
            
        }

        #endregion  CustomValueEnteredAction
         

        #region ToolTip Properties

        #region HeaderToolTipCaption

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipCaptionProperty = DependencyProperty.Register("HeaderToolTipCaption",
            typeof(String),
            typeof(vXamComboColumn),
            new PropertyMetadata(""));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public override String HeaderToolTipCaption
        {
            get { return (String)this.GetValue(HeaderToolTipCaptionProperty); }
            set
            {
                try
                {

                    this.SetValue(HeaderToolTipCaptionProperty, value);

                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipCaptionChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamComboColumn col = (vXamComboColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipCaption");
        //}

        #endregion HeaderToolTipCaption


        #region HeaderToolTipStringArray

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipStringArrayProperty = DependencyProperty.Register("HeaderToolTipStringArray",
            typeof(String[]),
            typeof(vXamComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a string array used to generate a collectin for the tooltip content
        /// </summary>
        public override String[] HeaderToolTipStringArray
        {
            get { return (String[])this.GetValue(HeaderToolTipStringArrayProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderToolTipStringArrayProperty, value);

                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipStringArrayChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamComboColumn col = (vXamComboColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipStringArray");
        //}

        #endregion HeaderToolTipStringArray


        #region HeaderToolTipItemsSource

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipItemsSourceProperty = DependencyProperty.Register("HeaderToolTipItemsSource",
            typeof(IEnumerable),
            typeof(vXamComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public override IEnumerable HeaderToolTipItemsSource
        {
            get { return (IEnumerable)this.GetValue(HeaderToolTipItemsSourceProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderToolTipItemsSourceProperty, value);

                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipItemsSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamComboColumn col = (vXamComboColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipItemsSource");
        //}

        #endregion HeaderToolTipItemsSource


        #region HeaderTooltipContent

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderTooltipContentProperty = DependencyProperty.Register("HeaderTooltipContent",
            typeof(IvTooltipContent),
            typeof(vXamComboColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public override IvTooltipContent HeaderTooltipContent
        {
            get { return (IvTooltipContent)this.GetValue(HeaderTooltipContentProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderTooltipContentProperty, value);

                    StringBuilder xmlCode = new StringBuilder();
                    xmlCode.AppendLine("<DataTemplate xmlns='http://schemas.microsoft.com/client/2007' xmlns:vGridCol='clr-namespace:vControls;assembly=vControlsGridColumns'>");
                    xmlCode.AppendLine("    <vGridCol:vColumnHeaderGrid  >");
                    //xmlCode.AppendLine("        <TextBlock Text=\"{Binding}\"/>");
                    xmlCode.AppendLine("        <TextBlock Text=\"" + this.HeaderText + "\"/>");
                    xmlCode.AppendLine("    </vGridCol:vColumnHeaderGrid>");
                    xmlCode.AppendLine("</DataTemplate>");
                    this.HeaderTemplate = (DataTemplate)XamlReader.Load(xmlCode.ToString());


                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderTooltipContentChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamComboColumn col = (vXamComboColumn)obj;
        //    col.OnPropertyChanged("HeaderTooltipContent");
        //}

        #endregion HeaderTooltipContent

        #endregion ToolTip Properties



        #region Foreground

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty ForegroundProperty = DependencyProperty.Register("Foreground",
            typeof(Brush),
            typeof(vXamComboColumn),
            new PropertyMetadata(new SolidColorBrush(SystemColors.ControlTextColor)));
        //new PropertyMetadata(new PropertyChangedCallback(ForegroundChanged)));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public Brush Foreground
        {
            get { return (Brush)this.GetValue(ForegroundProperty); }
            set { this.SetValue(ForegroundProperty, value); }
        }

        //private static void ForegroundChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vXamComboColumn col = (vXamComboColumn)obj;
        //    col.OnPropertyChanged("Foreground");

        //}

        #endregion Foreground



        #endregion Properties


        #region Events

        protected internal void OnSelectionChanged(vControls.vXamComboEditor sender, vXamComboColumnSelectionChangedEventArgs e)
        {
            try
            {
                if (SelectionChanged != null)
                {
                    this.SelectionChanged(sender, e);
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected internal void OnKeyUp(vControls.vXamComboEditor sender, vXamComboColumnKeyEventArgs e)
        {
            try
            {
                if (KeyUp != null)
                {
                    this.KeyUp(sender, e);
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected internal void OnKeyDown(vControls.vXamComboEditor sender, vXamComboColumnKeyEventArgs e)
        {
            try
            {
                if (KeyDown != null)
                {
                    this.KeyDown(sender, e);
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected internal void OnItemAdding(vControls.vXamComboEditor sender, vXamComboColumnItemAddingEventArgs e)
        {
            try
            {
                if (ItemAdding != null)
                {
                    this.ItemAdding(this, e);
                }
            }
            catch (Exception ex)
            {
            }
        }

        #endregion Events


        #region Overrides

        /// <summary>
        /// Generates a new <see cref="vXamComboColumnContentProvider"/> that will be used to generate content for <see cref="Cell"/> objects for this <see cref="Column"/>.
        /// </summary>
        /// <returns></returns>
        protected override Infragistics.Controls.Grids.Primitives.ColumnContentProviderBase GenerateContentProvider()
        {
            return new vXamComboColumnContentProvider();
        }

         

        #endregion Overrides








    }


    #region SortComparers



    public static class MySortComparer
    {
        public static int Compare(object x, object y, ComboItemList translateList, vXamComboColumn column)
        {
            string s1 = "";
            string s2 = "";

            //Null checks here    
            if (x == null && y == null)
            {
                return 0;
            }

            if (x == null)
            {
                return -1;
            }

            if (y == null)
            {
                return 1;
            }
            //End of the null checks

            // CachedList will not be null ONLY if this is not a child combo

            // Wrap each Linq query below in a try/catch becuase if nothing is found in the search, it returns an exception.
            switch (translateList.Id_DataType)
            {
                case ComboItemList.DataType.IntegerType:
                    if (translateList.CachedList.Count == 0)
                    {
                        try
                        {
                            s1 = (from ci in translateList
                                  where (int)ci.Id == (int)x
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        catch (Exception ex) { }

                        try
                        {
                            s2 = (from ci in translateList
                                  where (int)ci.Id == (int)y
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        catch (Exception ex) { }

                    }
                    else
                    {
                        try
                        {
                            s1 = (from ci in translateList.CachedList
                                  where (int)ci.Id == (int)x
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        catch (Exception ex) { }

                        try
                        {
                            s2 = (from ci in translateList.CachedList
                                  where (int)ci.Id == (int)y
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        catch (Exception ex) { }

                    }
                    return String.Compare(s1, s2);


                case ComboItemList.DataType.GuidType:
                    if (translateList.CachedList.Count == 0)
                    {
                        try
                        {
                            s1 = (from ci in translateList
                                  where (Guid)ci.Id == (Guid)x
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        catch (Exception ex) { }

                        try
                        {
                            s2 = (from ci in translateList
                                  where (Guid)ci.Id == (Guid)y
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        catch (Exception ex) { }

                    }
                    else
                    {
                        try
                        {
                            s1 = (from ci in translateList.CachedList
                                  where (Guid)ci.Id == (Guid)x
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        catch (Exception ex) { }

                        try
                        {
                            s2 = (from ci in translateList.CachedList
                                  where (Guid)ci.Id == (Guid)y
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        catch (Exception ex) { }

                    }
                    int ret = String.Compare(s1, s2);
                    return String.Compare(s1, s2);
                case ComboItemList.DataType.StringType:
                    if (translateList.CachedList.Count == 0)
                    {
                        try
                        {
                            s1 = (from ci in translateList
                                  where (string)ci.Id == (string)x
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        catch (Exception ex) { }

                        try
                        {
                            s2 = (from ci in translateList
                                  where (string)ci.Id == (string)y
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        catch (Exception ex) { }

                    }
                    else
                    {
                        try
                        {
                            s1 = (from ci in translateList.CachedList
                                  where (string)ci.Id == (string)x
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        catch (Exception ex) { }

                        try
                        {
                            s2 = (from ci in translateList.CachedList
                                  where (string)ci.Id == (string)y
                                  select ci.ItemName).FirstOrDefault().ToString();
                        }
                        catch (Exception ex) { }

                    }
                    return String.Compare(s1, s2);
                default:
                    return 0;
            }
        }
    }

    public class MyIntSortComparer : IComparer<int?>
    {
        public ComboItemList TranslateList;
        private vXamComboColumn _col;

        public MyIntSortComparer(ComboItemList list, vXamComboColumn col)
        {
            TranslateList = list;
            _col = col;

        }
        public int Compare(int? x, int? y)
        {
            return MySortComparer.Compare(x, y, TranslateList, _col);
        }
    }

    public class MyGuidSortComparer : IComparer<Guid?>
    {
        public ComboItemList TranslateList;
        private vXamComboColumn _col;

        public MyGuidSortComparer(ComboItemList list, vXamComboColumn col)
        {
            try
            {
                TranslateList = list;
                _col = col;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
            }
        }


        public int Compare(Guid? x, Guid? y)
        {
            try
            {
                if (x != null && y != null)
                {
                    //Debugger.Break();
                }
                return MySortComparer.Compare(x, y, TranslateList, _col);
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                return 0;
            }
        }




    }

    public class MyStringSortComparer : IComparer<string>
    {
        public ComboItemList TranslateList;
        private vXamComboColumn _col;

        public MyStringSortComparer(ComboItemList list, vXamComboColumn col)
        {
            TranslateList = list;
            _col = col;

        }
        public int Compare(string x, string y)
        {
            return MySortComparer.Compare(x, y, TranslateList, _col);
        }
    }


    #endregion SortComparers




}
