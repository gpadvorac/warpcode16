﻿using System;
using System.Windows;
using System.Windows.Controls;
using Infragistics.Controls.Grids;
using System.Collections;
using vTooltipProvider;
using System.Text;
using System.Windows.Markup;
using System.Windows.Media;

namespace vControls
{
    public class vDatePickerColumn : EditableColumn, IvColumn
    {

        #region Initialize Variables

        public event EventHandler<vDatePickerColumnTextChangedEventArgs> TextChanged;

        #endregion Initialize Variables


        #region Constructor

        #endregion Constructor


        #region Properties


        #region TextValue

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty TextValueProperty = DependencyProperty.Register("TextValue",
            typeof(String),
            typeof(vDatePickerColumn),
            new PropertyMetadata(new PropertyChangedCallback(TextValueChanged)));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public String TextValue
        {
            get { return (String)this.GetValue(TextValueProperty); }
            set { this.SetValue(TextValueProperty, value); }
        }

        private static void TextValueChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vDatePickerColumn col = (vDatePickerColumn)obj;
            col.OnPropertyChanged("TextValue");
            
        }

        #endregion TextValue


        #region DateFormat

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty DateFormatProperty = DependencyProperty.Register("DateFormat",
            typeof(String),
            typeof(vDatePickerColumn),
            new PropertyMetadata(new PropertyChangedCallback(DateFormatChanged)));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public String DateFormat
        {
            get { return (String)this.GetValue(DateFormatProperty); }
            set { this.SetValue(DateFormatProperty, value); }
        }

        private static void DateFormatChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vDatePickerColumn col = (vDatePickerColumn)obj;
            col.OnPropertyChanged("DateFormat");

        }

        #endregion DateFormat


        #region ValidationState


        public static readonly DependencyProperty ValidStateAppearanceProperty = DependencyProperty.Register(
            "ValidStateAppearance",
            typeof(string),
            typeof(vDatePickerColumn),
            new PropertyMetadata(new PropertyChangedCallback(ValidStateAppearanceChanged)));


        private static void ValidStateAppearanceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            vDatePickerColumn col = (vDatePickerColumn)obj;
            col.OnPropertyChanged("ValidStateAppearance");
        }


        public string ValidStateAppearance
        {
            get { return (string)GetValue(ValidStateAppearanceProperty); }
            set
            {
                SetValue(ValidStateAppearanceProperty, value);
            }
        }




        #endregion ValidationState


        #region ToolTip Properties

        #region HeaderToolTipCaption

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipCaptionProperty = DependencyProperty.Register("HeaderToolTipCaption",
            typeof(String),
            typeof(vDatePickerColumn),
            new PropertyMetadata(""));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public String HeaderToolTipCaption
        {
            get { return (String)this.GetValue(HeaderToolTipCaptionProperty); }
            set
            {
                try
                {

                    this.SetValue(HeaderToolTipCaptionProperty, value);

                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipCaptionChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vDatePickerColumn col = (vDatePickerColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipCaption");
        //}

        #endregion HeaderToolTipCaption


        #region HeaderToolTipStringArray

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipStringArrayProperty = DependencyProperty.Register("HeaderToolTipStringArray",
            typeof(String[]),
            typeof(vDatePickerColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a string array used to generate a collectin for the tooltip content
        /// </summary>
        public String[] HeaderToolTipStringArray
        {
            get { return (String[])this.GetValue(HeaderToolTipStringArrayProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderToolTipStringArrayProperty, value);

                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipStringArrayChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vDatePickerColumn col = (vDatePickerColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipStringArray");
        //}

        #endregion HeaderToolTipStringArray


        #region HeaderToolTipItemsSource

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderToolTipItemsSourceProperty = DependencyProperty.Register("HeaderToolTipItemsSource",
            typeof(IEnumerable),
            typeof(vDatePickerColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public IEnumerable HeaderToolTipItemsSource
        {
            get { return (IEnumerable)this.GetValue(HeaderToolTipItemsSourceProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderToolTipItemsSourceProperty, value);

                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderToolTipItemsSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vDatePickerColumn col = (vDatePickerColumn)obj;
        //    col.OnPropertyChanged("HeaderToolTipItemsSource");
        //}

        #endregion HeaderToolTipItemsSource


        #region HeaderTooltipContent

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty HeaderTooltipContentProperty = DependencyProperty.Register("HeaderTooltipContent",
            typeof(IvTooltipContent),
            typeof(vDatePickerColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public IvTooltipContent HeaderTooltipContent
        {
            get { return (IvTooltipContent)this.GetValue(HeaderTooltipContentProperty); }
            set
            {
                try
                {
                    this.SetValue(HeaderTooltipContentProperty, value);

                    StringBuilder xmlCode = new StringBuilder();
                    xmlCode.AppendLine("<DataTemplate xmlns='http://schemas.microsoft.com/client/2007' xmlns:vGridCol='clr-namespace:vControls;assembly=vControlsGridColumns'>");
                    xmlCode.AppendLine("    <vGridCol:vColumnHeaderGrid  >");
                    //xmlCode.AppendLine("        <TextBlock Text=\"{Binding}\"/>");
                    xmlCode.AppendLine("        <TextBlock Text=\"" + this.HeaderText + "\"/>");
                    xmlCode.AppendLine("    </vGridCol:vColumnHeaderGrid>");
                    xmlCode.AppendLine("</DataTemplate>");
                    this.HeaderTemplate = (DataTemplate)XamlReader.Load(xmlCode.ToString());

                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                }
            }
        }

        //private static void HeaderTooltipContentChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vDatePickerColumn col = (vDatePickerColumn)obj;
        //    col.OnPropertyChanged("HeaderTooltipContent");
        //}

        #endregion HeaderTooltipContent

        #endregion ToolTip Properties


        #region Foreground

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property. 
        /// </summary>
        public static readonly DependencyProperty ForegroundProperty = DependencyProperty.Register("Foreground",
            typeof(Brush),
            typeof(vDatePickerColumn),
            new PropertyMetadata(new SolidColorBrush(SystemColors.ControlTextColor)));
        //new PropertyMetadata(new PropertyChangedCallback(ForegroundChanged)));

        /// <summary>
        /// Gets or sets a collection used to generate the content of the <see cref="ItemsControl"/> 
        /// </summary>
        public Brush Foreground
        {
            get { return (Brush)this.GetValue(ForegroundProperty); }
            set { this.SetValue(ForegroundProperty, value); }
        }

        //private static void ForegroundChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    vDatePickerColumn col = (vDatePickerColumn)obj;
        //    col.OnPropertyChanged("Foreground");

        //}

        #endregion Foreground



        #endregion Properties


        #region Events
        
        protected internal void OnTextChanged(vDatePicker sender, vDatePickerColumnTextChangedEventArgs e)
        {
            if (TextChanged != null)
            {
                this.TextChanged(sender, e);
            }
        }

        #endregion Events


        #region Overrides

        /// <summary>
        /// Generates a new <see cref="TextColumnContentProvider"/> that will be used to generate content for <see cref="Cell"/> objects for this <see cref="Column"/>.
        /// </summary>
        /// <returns></returns>
        protected override Infragistics.Controls.Grids.Primitives.ColumnContentProviderBase GenerateContentProvider()
        {

            return new vDatePickerColumnContentProvider();
            
        }



        protected override void FillAvailableFilters(FilterOperandCollection availableFilters)
        {
            try
            {
            base.FillAvailableFilters(availableFilters);
            this.FilterColumnSettings.FilterMenuCustomFilteringButtonVisibility = Visibility.Collapsed;

            availableFilters.Add(new EqualsOperand());
            availableFilters.Add(new NotEqualsOperand());
            availableFilters.Add(new GreaterThanOperand());
            availableFilters.Add(new GreaterThanOrEqualOperand());
            availableFilters.Add(new LessThanOperand());
            availableFilters.Add(new LessThanOrEqualOperand());



            //if (this.DataType != null)
            //{
            //    if (this.DataType == typeof(string))
            //    {
            //        availableFilters.Add(new StartsWithOperand());
            //        availableFilters.Add(new ContainsOperand());
            //        availableFilters.Add(new EndsWithOperand());
            //        availableFilters.Add(new DoesNotContainOperand());
            //        availableFilters.Add(new DoesNotStartWithOperand());
            //        availableFilters.Add(new DoesNotEndWithOperand());
            //    }
            //    else if (this.DataType.IsValueType)
            //    {
            //        /* Hack solution until we have columntypes to handle numbers or other datatypes.*/
            //        availableFilters.Add(new GreaterThanOperand());
            //        availableFilters.Add(new GreaterThanOrEqualOperand());
            //        availableFilters.Add(new LessThanOperand());
            //        availableFilters.Add(new LessThanOrEqualOperand());
            //    }
            //}

            }
            catch (Exception ex)
            {
            }
        }


        #endregion Overrides

    }
}
