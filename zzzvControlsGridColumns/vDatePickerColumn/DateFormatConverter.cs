﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Controls.Grids.Primitives;
using Infragistics.Controls.Grids;
using System.Diagnostics;
using TypeServices;

namespace vControls
{
   

    public class DateFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (parameter != null)
                {
                    string formatterString = parameter.ToString();

                    if (!String.IsNullOrEmpty(formatterString))
                    {

                        return String.Format(culture, String.Format("{{0:{0}}}", formatterString), value);
                    }
                }

                return (value ?? "").ToString();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    } 








}
