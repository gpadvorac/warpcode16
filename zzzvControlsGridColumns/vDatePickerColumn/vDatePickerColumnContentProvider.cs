﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Controls.Grids.Primitives;
using Infragistics.Controls.Grids;
using vControls;
using System.Diagnostics;
using TypeServices;

namespace vControls
{
    public class vDatePickerColumnContentProvider : ColumnContentProviderBase
    {

        #region Initialize Variables

        TextBlock _textDisplay;
        vDatePicker _vDatePickerEditor;
        vDatePickerColumn _column = null;
        bool _bindingSet = false;
        bool _resetBindingSet;
        string _dateFormat;
        Cell _cell;
        bool _dataWasNull;

        #endregion Initialize Variables


        #region Constructor

        public vDatePickerColumnContentProvider()
        {
            try
            {
                _textDisplay = new TextBlock();
                _vDatePickerEditor = new vDatePicker();
                _vDatePickerEditor.Margin = new Thickness(-5);
                _vDatePickerEditor.TextChanged += new TextChangedEventHandler(vDatePickerEditor_TextChanged);
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        #endregion Constructor


        #region Overrides

        #region ResolveDisplayElement

        public override FrameworkElement ResolveDisplayElement(Cell cell, System.Windows.Data.Binding cellBinding)
        {
            try
            {
                
                _cell = cell;
                _column = (vDatePickerColumn)cell.Column;
                _dateFormat = _column.DateFormat;
                _resetBindingSet = cell.Row.RowType == RowType.FilterRow;
                _textDisplay.Foreground = _column.Foreground;

                if (cellBinding != null)
                {
                    cellBinding.ValidatesOnNotifyDataErrors = cellBinding.ValidatesOnDataErrors = cellBinding.ValidatesOnExceptions = cellBinding.NotifyOnValidationError = ((vDatePickerColumn)cell.Column).AllowEditingValidation;
                    cellBinding.Converter = new DateFormatConverter();
                    cellBinding.ConverterParameter = _column.DateFormat;
                    _textDisplay.SetBinding(TextBlock.TextProperty, cellBinding);
                }
                _textDisplay.IsHitTestVisible = false;
                return _textDisplay;

                ////**<<<  This is code that should allow us to use conditional formating, but for some reason its not working
                //_cell = cell;
                //_column = (vDatePickerColumn)cell.Column;
                //_dateFormat = _column.DateFormat;
                //_resetBindingSet = cell.Row.RowType == RowType.FilterRow;
                //if (cellBinding != null)
                //{
                //    cellBinding.ValidatesOnNotifyDataErrors = cellBinding.ValidatesOnDataErrors = cellBinding.ValidatesOnExceptions = cellBinding.NotifyOnValidationError = ((vDatePickerColumn)cell.Column).AllowEditingValidation;

                //    //You cna use StringFormat property of the cellBinding to format the text to the Text,
                //    //which allows the ConditionalFormatting to evaluate the Cell's value correctly
                //    cellBinding.StringFormat = _dateFormat;
                //    //cellBinding.Converter = new DateFormatConverter();
                //    //cellBinding.ConverterParameter = _column.DateFormat;

                //    _textDisplay.SetBinding(TextBlock.TextProperty, cellBinding);

                //}
                //_textDisplay.IsHitTestVisible = false;
                //return _textDisplay;
                ////**>>>  


            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveDisplayElement



        // Added 11-15 - Valerie
        #region AdjustDisplayElement

        /// <summary>
        /// Called during EnsureContent to allow the provider a chance to modify it's display based on the current conditions.
        /// </summary>
        /// <param name="cell"></param>
        public override void AdjustDisplayElement(Cell cell)
        {
            try
            {
                // Ok, so with the perf improvements, add new row doesn't exactly play nicely, when it creates cells before the data is loaded. 
                // So what happens is we need to track when the data changes, and if it does, then make sure its binding is applied
                // and that the IsEnabled property is correct. 
                bool dataIsNull = (cell.Row.Data == null);
                if (this._dataWasNull && !dataIsNull)
                {
                    Binding binding = this.ResolveBinding(cell);

                    if (binding != null)
                    {
                        binding.ValidatesOnNotifyDataErrors = binding.ValidatesOnDataErrors = binding.ValidatesOnExceptions = binding.NotifyOnValidationError = ((vDatePickerColumn)cell.Column).AllowEditingValidation;
                        this._textDisplay.SetBinding(TextBlock.TextProperty, binding);
                    }
                    // Not applicable since the display control is a textblock
                    //this._textDisplay.IsEnabled = cell.IsEditable;
                }

                this._dataWasNull = dataIsNull;

                //***
                cell.Column.FilterColumnSettings.FilterCellValue = _vDatePickerEditor.Text;
                ////cell.Column.FilterColumnSettings.FilteringOperand.ComparisonOperatorValue

                base.AdjustDisplayElement(cell);
            }
            catch (Exception ex)
            {
            }
        }

        #endregion  AdjustDisplayElement





        #region ResolveEditorControl

        protected override FrameworkElement ResolveEditorControl(Cell cell, object editorValue, double availableWidth, double availableHeight, System.Windows.Data.Binding editorBinding)
        {
            try
            {
                if (editorBinding != null)
                {
                    editorBinding.StringFormat = _dateFormat;
                    if (cell.Row is FilterRow)
                    {
                        // makes the filter update when we select a date
                        editorBinding.UpdateSourceTrigger = UpdateSourceTrigger.Default;
                    }
                    this._vDatePickerEditor.SetBinding(vDatePicker.TextProperty, editorBinding);
                }

                Style datePickerStyle = cell.EditorStyleResolved;
                if (datePickerStyle != null)
                {
                    _vDatePickerEditor.Style = datePickerStyle;
                }
                else
                {
                    _vDatePickerEditor.ClearValue(vDatePicker.StyleProperty);
                }
                _vDatePickerEditor.IsHitTestVisible = true;

                if (this._vDatePickerEditor.GetBindingExpression(vDatePicker.TextProperty) == null)
                {
                    if (editorBinding != null)
                    {
                        this._vDatePickerEditor.SetBinding(vDatePicker.TextProperty, editorBinding);
                    }
                }

                IBusinessObject obj = cell.Row.Data as IBusinessObject;
                if (obj != null)
                {
                    _column = (vDatePickerColumn)cell.Column;
                    if (cell.Row.RowType == RowType.DataRow)
                    {
                        SetXamGridEditControlValidStateAppearance(_column.Key, _vDatePickerEditor, obj);
                    }
                }

                if (cell.IsEditing)
                {
                    this._vDatePickerEditor.Focus();
                }

                _vDatePickerEditor.Foreground = _column.Foreground;


                return _vDatePickerEditor;

                //Binding textBinding = new Binding();
                //textBinding.Path = new PropertyPath(_column.Key);
                //textBinding.Mode = BindingMode.TwoWay;
                //textBinding.Converter = new DateFormatConverter();
                //textBinding.ConverterParameter = _column.DateFormat;
                //this._vDatePickerEditor.SetBinding(vDatePicker.TextProperty, textBinding);
                //this._vDatePickerEditor.Style = cell.EditorStyleResolved;

                //if (_resetBindingSet == false)
                //{
                //    IBusinessObject obj = cell.Row.Data as IBusinessObject;
                //    if (obj == null) { return null; }
                //    SetXamGridEditControlValidStateAppearance(_column.Key, _vDatePickerEditor, obj);
                //}
                //else
                //{
                //    // Set the text from what it was the last time we entered a value.
                //    _vDatePickerEditor.Text = _textDisplay.Text;
                //}

                //return this._vDatePickerEditor;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveEditorControl


        #region ResolveValueFromEditor

        public override object ResolveValueFromEditor(Infragistics.Controls.Grids.Cell cell)
        {
            try
            {
                return this._vDatePickerEditor.Text;
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        #endregion ResolveValueFromEditor

        #endregion Overrides


        #region Event Handlers


        void vDatePickerEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (_vDatePickerEditor.Parent == null) { return; }
                if (_resetBindingSet == false)
                {
                    IBusinessObject data = _cell.Row.Data as IBusinessObject;
                    string key = _cell.Column.Key;
                    int index = _cell.Row.Index;
                    vDatePickerColumnTextChangedEventArgs args = new vDatePickerColumnTextChangedEventArgs(_cell, index, key, data, e);
                    _column.OnTextChanged(_vDatePickerEditor, args);
                }
                else
                {
                    // No need to raise the text changed event here as we dont want to set any values to the business object.

                    // The text changed - how do we refresh the filtered rows now?  we dont want to wait until the user tabs out of this control.
                    //_column.FilterColumnSettings.??????????
                    //((Infragistics.Controls.Grids.Primitives.FilterRowCell)((Infragistics.Controls.Grids.CellControl)_vTextBox.Parent).Cell);
                }
            }
            catch (Exception ex)
            {
                //Debugger.Break();
                //System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }


        #endregion Event Handlers


        #region Methods

        public static void SetXamGridEditControlValidStateAppearance(string key, vDatePicker ctl, IBusinessObject obj)
        {
            try
            {

                if (obj.IsPropertyValid(key))
                {
                    ctl.ValidStateAppearance = ValidationState.Valid;
                }
                else
                {
                    if (obj.IsPropertyDirty(key))
                    {
                        ctl.ValidStateAppearance = ValidationState.NotValidIsDirty;
                    }
                    else
                    {
                        ctl.ValidStateAppearance = ValidationState.NotValidNotDirty;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        #endregion Methods



    }
}
