using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;

// Gen Timestamp:  12/8/2012 12:09:09 AM

namespace EntityWireType 
{

    #region Entity Values Manager
    
    public class Task2Person_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "Task2Person_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public Task2Person_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false), false) 
        {
        }

//        public Task2Person_ValuesMngr(SqlDataReader reader)
//        {
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task2Person_ValuesMngr", IfxTraceCategory.Enter);
//                _current = new Task2Person_Values(reader, this);
//                _original = null;
//                _concurrent = null;
//                _state.SetNotNew();
//                _state.SetNotDirty();
//                _state.SetValid();
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task2Person_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task2Person_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
//            }
//        }


        public Task2Person_ValuesMngr(object[] currentData, EntityState state) 
            : this(currentData, null, state, false) { }
            //: this(currentData, currentData, state) { }
            //: this(currentData, null, state) { }

        public Task2Person_ValuesMngr(object[] currentData, EntityState state, bool isPartialLoad)
            : this(currentData, null, state, isPartialLoad) { }

        private Task2Person_ValuesMngr(object[] currentData, object[] originalData, EntityState state, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task2Person_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new Task2Person_Values(currentData, this, isPartialLoad) : new Task2Person_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (originalData != null ? new Task2Person_Values(originalData, this) : null);
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task2Person_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task2Person_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected Task2Person_Values _original;
        
        public Task2Person_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected Task2Person_Values _current;
        
        public Task2Person_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected Task2Person_Values _concurrent;
        
        public Task2Person_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  Tk2Pn_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  Tk2Pn_Tk_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  Tk2Pn_Pn_Id
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  PersonName
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  Tk2Pn_UserId
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  UserName
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  Tk2Pn_CreatedDate
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  Tk2Pn_Stamp
                if (_current._h != _original._h)
                {
                    return true;
                }

                return false;
            }
        }

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Enter);
              //NOTE:  This method is the ONLY method that should call the one in the values object below.
                
                if (_original != null)
                {
                    _original.FinishPartialLoad();
                }
                if (_current != null)
                {
                    _current.FinishPartialLoad();
                }
                if (_concurrent != null)
                {
                    _concurrent.FinishPartialLoad();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
        }

        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{9}{0}{8}_b{9}{1}{8}_c{9}{2}{8}_d{9}{3}{8}_e{9}{4}{8}_f{9}{5}{8}_g{9}{6}{8}_h{9}{7}",
				new object[] {
				_current._a,		  //Tk2Pn_Id
				_current._b,		  //Tk2Pn_Tk_Id
				_current._c,		  //Tk2Pn_Pn_Id
				_current._d,		  //PersonName
				_current._e,		  //Tk2Pn_UserId
				_current._f,		  //UserName
				_current._g,		  //Tk2Pn_CreatedDate
				_current._h,		  //Tk2Pn_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    
    public class Task2Person_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "Task2Person_Values";
        private Task2Person_ValuesMngr _parent = null;
        private bool _isPartialLoad = false;
        private bool _isPartialDataLoaded = false;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal Task2Person_Values() 
        {
        }

        public Task2Person_Values(object[] data, Task2Person_ValuesMngr parent)
            :this(data, parent, false)
        {  }

        public Task2Person_Values(object[] data, Task2Person_ValuesMngr parent, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task2Person_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
                _isPartialLoad = isPartialLoad;
                // Fields that are part of the parital load which will always be loaded
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Tk2Pn_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Tk2Pn_Tk_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  Tk2Pn_Pn_Id
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  PersonName
				_e = ObjectHelper.GetNullableGuidFromObjectValue(data[4]);						//  Tk2Pn_UserId
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  UserName
				_g = ObjectHelper.GetNullableDateTimeFromObjectValue(data[6]);					//  Tk2Pn_CreatedDate
				_h = ObjectHelper.GetByteArrayFromObjectValue(data[7]);						//  Tk2Pn_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task2Person_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task2Person_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - Task2Person", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Tk2Pn_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Tk2Pn_Tk_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  Tk2Pn_Pn_Id
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  PersonName
				_e = ObjectHelper.GetNullableGuidFromObjectValue(data[4]);						//  Tk2Pn_UserId
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  UserName
				_g = ObjectHelper.GetNullableDateTimeFromObjectValue(data[6]);					//  Tk2Pn_CreatedDate
				_h = ObjectHelper.GetByteArrayFromObjectValue(data[7]);						//  Tk2Pn_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - Task2Person", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - Task2Person", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - Task2Person", IfxTraceCategory.Leave);
				// No fields to load.

                if (_isPartialLoad == true && _isPartialDataLoaded == false)
                {

                    _isPartialDataLoaded = true;
                }
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - Task2Person", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods


		#region Data Members

		
		public Guid _a;			//  Tk2Pn_Id

		
		public Guid? _b;			//  Tk2Pn_Tk_Id

		
		public Guid? _c;			//  Tk2Pn_Pn_Id

		
		public String _d;			//  PersonName

		
		public Guid? _e;			//  Tk2Pn_UserId

		
		public String _f;			//  UserName

		
		public DateTime? _g;			//  Tk2Pn_CreatedDate

		
		public Byte[] _h;			//  Tk2Pn_Stamp

		
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        public bool IsPartialLoad
        {
            get { return _isPartialLoad; }
            set { _isPartialLoad = value; }
        }

        internal Task2Person_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 8; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                }
                return _list;
            }
        }

        public Guid Tk2Pn_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid Tk2Pn_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? Tk2Pn_Tk_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tk2Pn_Tk_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Guid? Tk2Pn_Pn_Id
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tk2Pn_Pn_Id_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String PersonName
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String PersonName_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public Guid? Tk2Pn_UserId
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tk2Pn_UserId_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public String UserName
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public DateTime? Tk2Pn_CreatedDate
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Tk2Pn_CreatedDate_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Byte[] Tk2Pn_Stamp
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] Tk2Pn_Stamp_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h
			};
        }

        public Task2Person_Values Clone()
        {
            return new Task2Person_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


