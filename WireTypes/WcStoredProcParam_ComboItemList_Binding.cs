using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx;

// Gen Timestamp:  1/7/2018 2:55:31 PM

namespace EntityWireType
{


    
    public class WcStoredProcParam_ComboItemList_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProcParam_ComboItemList_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcStoredProcParam_ComboItemList_Binding() { }


        public WcStoredProcParam_ComboItemList_Binding(Guid _SpP_ID, Guid _NullFK, String _SpP_Name )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_ComboItemList_Binding", IfxTraceCategory.Enter);
				_a = _SpP_ID;
				_b = _NullFK;
				_c = _SpP_Name;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_ComboItemList_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_ComboItemList_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcStoredProcParam_ComboItemList_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_ComboItemList_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  SpP_ID
				_b = (Guid)data[1];                //  NullFK
				_c = (String)data[2];                //  SpP_Name
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_ComboItemList_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_ComboItemList_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region SpP_ID

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid SpP_ID
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion SpP_ID


        #region NullFK

        private Guid _b;
//        public Guid B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Guid NullFK
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion NullFK


        #region SpP_Name

        private String _c;
//        public String C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public String SpP_Name
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion SpP_Name


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2} ", SpP_ID, NullFK, SpP_Name );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2} ", SpP_ID, NullFK, SpP_Name );
        }

    }

}
