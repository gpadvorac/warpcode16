using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx;

// Gen Timestamp:  1/17/2018 10:14:45 PM

namespace EntityWireType
{


    
    public class WcTableColumn_lstByTable_ComboItemList_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTableColumn_lstByTable_ComboItemList_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcTableColumn_lstByTable_ComboItemList_Binding() { }


        public WcTableColumn_lstByTable_ComboItemList_Binding(Guid _TbC_Id, Int32 _NullFK, String _TbC_Name )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByTable_ComboItemList_Binding", IfxTraceCategory.Enter);
				_a = _TbC_Id;
				_b = _NullFK;
				_c = _TbC_Name;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByTable_ComboItemList_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByTable_ComboItemList_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcTableColumn_lstByTable_ComboItemList_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByTable_ComboItemList_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  TbC_Id
				_b = (Int32)data[1];                //  NullFK
				_c = (String)data[2];                //  TbC_Name
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByTable_ComboItemList_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByTable_ComboItemList_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region TbC_Id

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid TbC_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion TbC_Id


        #region NullFK

        private Int32 _b;
//        public Int32 B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Int32 NullFK
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion NullFK


        #region TbC_Name

        private String _c;
//        public String C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public String TbC_Name
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion TbC_Name


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2} ", TbC_Id, NullFK, TbC_Name );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2} ", TbC_Id, NullFK, TbC_Name );
        }

    }

}
