using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;

// Gen Timestamp:  5/7/2015 12:09:30 PM

namespace EntityWireType 
{

    #region Entity Values Manager
    
    public class PersonContact_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "PersonContact_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public PersonContact_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false), false) 
        {
        }

//        public PersonContact_ValuesMngr(SqlDataReader reader)
//        {
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - PersonContact_ValuesMngr", IfxTraceCategory.Enter);
//                _current = new PersonContact_Values(reader, this);
//                _original = null;
//                _concurrent = null;
//                _state.SetNotNew();
//                _state.SetNotDirty();
//                _state.SetValid();
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - PersonContact_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - PersonContact_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
//            }
//        }


        public PersonContact_ValuesMngr(object[] currentData, EntityState state) 
            : this(currentData, null, state, false) { }
            //: this(currentData, currentData, state) { }
            //: this(currentData, null, state) { }

        public PersonContact_ValuesMngr(object[] currentData, EntityState state, bool isPartialLoad)
            : this(currentData, null, state, isPartialLoad) { }

        private PersonContact_ValuesMngr(object[] currentData, object[] originalData, EntityState state, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - PersonContact_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new PersonContact_Values(currentData, this, isPartialLoad) : new PersonContact_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (originalData != null ? new PersonContact_Values(originalData, this) : null);
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - PersonContact_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - PersonContact_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected PersonContact_Values _original;
        
        public PersonContact_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected PersonContact_Values _current;
        
        public PersonContact_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected PersonContact_Values _concurrent;
        
        public PersonContact_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  Pn_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  Pn_FName
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  Pn_LName
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  Pn_EMail
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  Pn_PhoneCell
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  Pn_PhoneOther
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  Pn_UserIdStamp
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  UserName
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  Pn_CreatedDate
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  Pn_LastModifiedDate
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  Pn_Stamp
                if (_current._k != _original._k)
                {
                    return true;
                }

                return false;
            }
        }

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Enter);
              //NOTE:  This method is the ONLY method that should call the one in the values object below.
                
                if (_original != null)
                {
                    _original.FinishPartialLoad();
                }
                if (_current != null)
                {
                    _current.FinishPartialLoad();
                }
                if (_concurrent != null)
                {
                    _concurrent.FinishPartialLoad();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
        }

        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{12}{0}{11}_b{12}{1}{11}_c{12}{2}{11}_d{12}{3}{11}_e{12}{4}{11}_f{12}{5}{11}_g{12}{6}{11}_h{12}{7}{11}_i{12}{8}{11}_j{12}{9}{11}_k{12}{10}",
				new object[] {
				_current._a,		  //Pn_Id
				_current._b,		  //Pn_FName
				_current._c,		  //Pn_LName
				_current._d,		  //Pn_EMail
				_current._e,		  //Pn_PhoneCell
				_current._f,		  //Pn_PhoneOther
				_current._g,		  //Pn_UserIdStamp
				_current._h,		  //UserName
				_current._i,		  //Pn_CreatedDate
				_current._j,		  //Pn_LastModifiedDate
				_current._k,		  //Pn_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    
    public class PersonContact_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "PersonContact_Values";
        private PersonContact_ValuesMngr _parent = null;
        private bool _isPartialLoad = false;
        private bool _isPartialDataLoaded = false;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal PersonContact_Values() 
        {
        }

        public PersonContact_Values(object[] data, PersonContact_ValuesMngr parent)
            :this(data, parent, false)
        {  }

        public PersonContact_Values(object[] data, PersonContact_ValuesMngr parent, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - PersonContact_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
                _isPartialLoad = isPartialLoad;
                // Fields that are part of the parital load which will always be loaded
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Pn_Id
				_b = ObjectHelper.GetStringFromObjectValue(data[1]);									//  Pn_FName
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  Pn_LName
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  Pn_EMail
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  Pn_PhoneCell
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  Pn_PhoneOther
				_g = ObjectHelper.GetNullableGuidFromObjectValue(data[6]);						//  Pn_UserIdStamp
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  UserName
				_i = ObjectHelper.GetNullableDateTimeFromObjectValue(data[8]);					//  Pn_CreatedDate
				_j = ObjectHelper.GetNullableDateTimeFromObjectValue(data[9]);					//  Pn_LastModifiedDate
				_k = ObjectHelper.GetByteArrayFromObjectValue(data[10]);						//  Pn_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - PersonContact_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - PersonContact_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - PersonContact", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Pn_Id
				_b = ObjectHelper.GetStringFromObjectValue(data[1]);									//  Pn_FName
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  Pn_LName
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  Pn_EMail
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  Pn_PhoneCell
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  Pn_PhoneOther
				_g = ObjectHelper.GetNullableGuidFromObjectValue(data[6]);						//  Pn_UserIdStamp
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  UserName
				_i = ObjectHelper.GetNullableDateTimeFromObjectValue(data[8]);					//  Pn_CreatedDate
				_j = ObjectHelper.GetNullableDateTimeFromObjectValue(data[9]);					//  Pn_LastModifiedDate
				_k = ObjectHelper.GetByteArrayFromObjectValue(data[10]);						//  Pn_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - PersonContact", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - PersonContact", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - PersonContact", IfxTraceCategory.Leave);
				// No fields to load.

                if (_isPartialLoad == true && _isPartialDataLoaded == false)
                {

                    _isPartialDataLoaded = true;
                }
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - PersonContact", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods


		#region Data Members

		
		public Guid _a;			//  Pn_Id

		
		public String _b;			//  Pn_FName

		
		public String _c;			//  Pn_LName

		
		public String _d;			//  Pn_EMail

		
		public String _e;			//  Pn_PhoneCell

		
		public String _f;			//  Pn_PhoneOther

		
		public Guid? _g;			//  Pn_UserIdStamp

		
		public String _h;			//  UserName

		
		public DateTime? _i;			//  Pn_CreatedDate

		
		public DateTime? _j;			//  Pn_LastModifiedDate

		
		public Byte[] _k;			//  Pn_Stamp

		
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        public bool IsPartialLoad
        {
            get { return _isPartialLoad; }
            set { _isPartialLoad = value; }
        }

        internal PersonContact_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 11; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                }
                return _list;
            }
        }

        public Guid Pn_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid Pn_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public String Pn_FName
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Pn_FName_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public String Pn_LName
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Pn_LName_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String Pn_EMail
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Pn_EMail_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String Pn_PhoneCell
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Pn_PhoneCell_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public String Pn_PhoneOther
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Pn_PhoneOther_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Guid? Pn_UserIdStamp
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Pn_UserIdStamp_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public String UserName
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public DateTime? Pn_CreatedDate
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Pn_CreatedDate_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public DateTime? Pn_LastModifiedDate
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Pn_LastModifiedDate_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public Byte[] Pn_Stamp
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] Pn_Stamp_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k
			};
        }

        public PersonContact_Values Clone()
        {
            return new PersonContact_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


