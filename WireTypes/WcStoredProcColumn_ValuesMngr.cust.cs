using System;
using System.Data;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;


// Gen Timestamp:  12/1/2016 10:43:36 AM

namespace EntityWireType
{


    #region Entity Values
    
    public partial class WcStoredProcColumn_Values : ObjectBase
    {



        public WcStoredProcColumn_Values(object[] data, DataRow rw)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcColumn_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _a = ObjectHelper.GetGuidFromObjectValue(data[0]);                              //  SpCl_Id
                _b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);                      //  SpCl_Sp_Id
                _c = ObjectHelper.GetStringFromObjectValue(data[2]);                                    //  SpCl_Name
                _d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);                           //  SpCl_ColIndex
                _e = ObjectHelper.GetStringFromObjectValue(data[4]);                                    //  SpCl_ColLetter
                _f = ObjectHelper.GetNullableIntFromObjectValue(data[5]);                           //  SpCl_DtNt_ID
                _g = ObjectHelper.GetStringFromObjectValue(data[6]);                                    //  SpCl_DtNt_ID_TextField
                _h = ObjectHelper.GetNullableIntFromObjectValue(data[7]);                           //  SpCl_DtSq_ID
                _i = ObjectHelper.GetStringFromObjectValue(data[8]);                                    //  SpCl_DtSq_ID_TextField
                _j = ObjectHelper.GetBoolFromObjectValue(data[9]);                                  //  SpCl_IsNullable
                _k = ObjectHelper.GetBoolFromObjectValue(data[10]);                                 //  SpCl_IsVisible
                _l = ObjectHelper.GetBoolFromObjectValue(data[11]);                                 //  SpCl_Browsable
                _m = ObjectHelper.GetStringFromObjectValue(data[12]);                                   //  SpCl_Notes
                _n = ObjectHelper.GetBoolFromObjectValue(data[13]);                                 //  SpCl_IsActiveRow
                _o = ObjectHelper.GetBoolFromObjectValue(data[14]);                                 //  SpCl_IsDeleted
                _p = ObjectHelper.GetNullableGuidFromObjectValue(data[15]);                     //  SpCl_CreatedUserId
                _q = ObjectHelper.GetNullableDateTimeFromObjectValue(data[16]);                 //  SpCl_CreatedDate
                _r = ObjectHelper.GetNullableGuidFromObjectValue(data[17]);                     //  SpCl_UserId
                _s = ObjectHelper.GetStringFromObjectValue(data[18]);                                   //  UserName
                _t = ObjectHelper.GetNullableDateTimeFromObjectValue(data[19]);                 //  SpCl_LastModifiedDate
                _u = ObjectHelper.GetByteArrayFromObjectValue(data[20]);                        //  SpCl_Stamp
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcColumn_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcColumn_ValuesMngr", IfxTraceCategory.Leave);
            }
        }



    }
    #endregion Entity Values

}


