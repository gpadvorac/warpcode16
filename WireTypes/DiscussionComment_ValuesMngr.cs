using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;

// Gen Timestamp:  6/18/2015 2:36:15 PM

namespace EntityWireType 
{

    #region Entity Values Manager
    
    public class DiscussionComment_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "DiscussionComment_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public DiscussionComment_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false), false) 
        {
        }

//        public DiscussionComment_ValuesMngr(SqlDataReader reader)
//        {
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - DiscussionComment_ValuesMngr", IfxTraceCategory.Enter);
//                _current = new DiscussionComment_Values(reader, this);
//                _original = null;
//                _concurrent = null;
//                _state.SetNotNew();
//                _state.SetNotDirty();
//                _state.SetValid();
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - DiscussionComment_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - DiscussionComment_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
//            }
//        }


        public DiscussionComment_ValuesMngr(object[] currentData, EntityState state) 
            : this(currentData, null, state, false) { }
            //: this(currentData, currentData, state) { }
            //: this(currentData, null, state) { }

        public DiscussionComment_ValuesMngr(object[] currentData, EntityState state, bool isPartialLoad)
            : this(currentData, null, state, isPartialLoad) { }

        private DiscussionComment_ValuesMngr(object[] currentData, object[] originalData, EntityState state, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - DiscussionComment_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new DiscussionComment_Values(currentData, this, isPartialLoad) : new DiscussionComment_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (originalData != null ? new DiscussionComment_Values(originalData, this) : null);
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - DiscussionComment_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - DiscussionComment_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected DiscussionComment_Values _original;
        
        public DiscussionComment_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected DiscussionComment_Values _current;
        
        public DiscussionComment_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected DiscussionComment_Values _concurrent;
        
        public DiscussionComment_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  DscCm_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  DscCm_Dsc_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  DscCm_DscCm_Id
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  DscCm_Comment
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  DscCm_IsActiveRow
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  DscCm_IsDeleted
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  DscCm_CreatedUserId
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  DscCm_CreatedDate
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  DscCm_UserId
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  UserName
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  DscCm_LastModifiedDate
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  DscCm_Stamp
                if (_current._l != _original._l)
                {
                    return true;
                }

                return false;
            }
        }

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Enter);
              //NOTE:  This method is the ONLY method that should call the one in the values object below.
                
                if (_original != null)
                {
                    _original.FinishPartialLoad();
                }
                if (_current != null)
                {
                    _current.FinishPartialLoad();
                }
                if (_concurrent != null)
                {
                    _concurrent.FinishPartialLoad();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
        }

        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{13}{0}{12}_b{13}{1}{12}_c{13}{2}{12}_d{13}{3}{12}_e{13}{4}{12}_f{13}{5}{12}_g{13}{6}{12}_h{13}{7}{12}_i{13}{8}{12}_j{13}{9}{12}_k{13}{10}{12}_l{13}{11}",
				new object[] {
				_current._a,		  //DscCm_Id
				_current._b,		  //DscCm_Dsc_Id
				_current._c,		  //DscCm_DscCm_Id
				_current._d,		  //DscCm_Comment
				_current._e,		  //DscCm_IsActiveRow
				_current._f,		  //DscCm_IsDeleted
				_current._g,		  //DscCm_CreatedUserId
				_current._h,		  //DscCm_CreatedDate
				_current._i,		  //DscCm_UserId
				_current._j,		  //UserName
				_current._k,		  //DscCm_LastModifiedDate
				_current._l,		  //DscCm_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    
    public class DiscussionComment_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "DiscussionComment_Values";
        private DiscussionComment_ValuesMngr _parent = null;
        private bool _isPartialLoad = false;
        private bool _isPartialDataLoaded = false;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal DiscussionComment_Values() 
        {
        }

        public DiscussionComment_Values(object[] data, DiscussionComment_ValuesMngr parent)
            :this(data, parent, false)
        {  }

        public DiscussionComment_Values(object[] data, DiscussionComment_ValuesMngr parent, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - DiscussionComment_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
                _isPartialLoad = isPartialLoad;
                // Fields that are part of the parital load which will always be loaded
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  DscCm_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  DscCm_Dsc_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  DscCm_DscCm_Id
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  DscCm_Comment
				_e = ObjectHelper.GetBoolFromObjectValue(data[4]);									//  DscCm_IsActiveRow
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  DscCm_IsDeleted
				_g = ObjectHelper.GetNullableGuidFromObjectValue(data[6]);						//  DscCm_CreatedUserId
				_h = ObjectHelper.GetNullableDateTimeFromObjectValue(data[7]);					//  DscCm_CreatedDate
				_i = ObjectHelper.GetNullableGuidFromObjectValue(data[8]);						//  DscCm_UserId
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  UserName
				_k = ObjectHelper.GetNullableDateTimeFromObjectValue(data[10]);					//  DscCm_LastModifiedDate
				_l = ObjectHelper.GetByteArrayFromObjectValue(data[11]);						//  DscCm_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - DiscussionComment_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - DiscussionComment_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - DiscussionComment", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  DscCm_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  DscCm_Dsc_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  DscCm_DscCm_Id
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  DscCm_Comment
				_e = ObjectHelper.GetBoolFromObjectValue(data[4]);									//  DscCm_IsActiveRow
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  DscCm_IsDeleted
				_g = ObjectHelper.GetNullableGuidFromObjectValue(data[6]);						//  DscCm_CreatedUserId
				_h = ObjectHelper.GetNullableDateTimeFromObjectValue(data[7]);					//  DscCm_CreatedDate
				_i = ObjectHelper.GetNullableGuidFromObjectValue(data[8]);						//  DscCm_UserId
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  UserName
				_k = ObjectHelper.GetNullableDateTimeFromObjectValue(data[10]);					//  DscCm_LastModifiedDate
				_l = ObjectHelper.GetByteArrayFromObjectValue(data[11]);						//  DscCm_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - DiscussionComment", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - DiscussionComment", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - DiscussionComment", IfxTraceCategory.Leave);
				// No fields to load.

                if (_isPartialLoad == true && _isPartialDataLoaded == false)
                {

                    _isPartialDataLoaded = true;
                }
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - DiscussionComment", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods


		#region Data Members

		
		public Guid _a;			//  DscCm_Id

		
		public Guid? _b;			//  DscCm_Dsc_Id

		
		public Guid? _c;			//  DscCm_DscCm_Id

		
		public String _d;			//  DscCm_Comment

		
		public Boolean _e;			//  DscCm_IsActiveRow

		
		public Boolean _f;			//  DscCm_IsDeleted

		
		public Guid? _g;			//  DscCm_CreatedUserId

		
		public DateTime? _h;			//  DscCm_CreatedDate

		
		public Guid? _i;			//  DscCm_UserId

		
		public String _j;			//  UserName

		
		public DateTime? _k;			//  DscCm_LastModifiedDate

		
		public Byte[] _l;			//  DscCm_Stamp

		
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        public bool IsPartialLoad
        {
            get { return _isPartialLoad; }
            set { _isPartialLoad = value; }
        }

        internal DiscussionComment_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 12; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                }
                return _list;
            }
        }

        public Guid DscCm_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid DscCm_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? DscCm_Dsc_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? DscCm_Dsc_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Guid? DscCm_DscCm_Id
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? DscCm_DscCm_Id_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String DscCm_Comment
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DscCm_Comment_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public Boolean DscCm_IsActiveRow
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean DscCm_IsActiveRow_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Boolean DscCm_IsDeleted
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean DscCm_IsDeleted_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Guid? DscCm_CreatedUserId
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? DscCm_CreatedUserId_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public DateTime? DscCm_CreatedDate
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? DscCm_CreatedDate_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Guid? DscCm_UserId
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? DscCm_UserId_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public String UserName
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public DateTime? DscCm_LastModifiedDate
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? DscCm_LastModifiedDate_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public Byte[] DscCm_Stamp
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] DscCm_Stamp_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l
			};
        }

        public DiscussionComment_Values Clone()
        {
            return new DiscussionComment_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


