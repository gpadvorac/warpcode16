using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;

// Gen Timestamp:  1/10/2015 7:11:28 PM

namespace EntityWireType 
{

    #region Entity Values Manager
    
    public class TaskCategory_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "vProjectManager";
        private static string _cn = "TaskCategory_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public TaskCategory_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false), false) 
        {
        }

//        public TaskCategory_ValuesMngr(SqlDataReader reader)
//        {
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - TaskCategory_ValuesMngr", IfxTraceCategory.Enter);
//                _current = new TaskCategory_Values(reader, this);
//                _original = null;
//                _concurrent = null;
//                _state.SetNotNew();
//                _state.SetNotDirty();
//                _state.SetValid();
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - TaskCategory_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - TaskCategory_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
//            }
//        }


        public TaskCategory_ValuesMngr(object[] currentData, EntityState state) 
            : this(currentData, null, state, false) { }
            //: this(currentData, currentData, state) { }
            //: this(currentData, null, state) { }

        public TaskCategory_ValuesMngr(object[] currentData, EntityState state, bool isPartialLoad)
            : this(currentData, null, state, isPartialLoad) { }

        private TaskCategory_ValuesMngr(object[] currentData, object[] originalData, EntityState state, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - TaskCategory_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new TaskCategory_Values(currentData, this, isPartialLoad) : new TaskCategory_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (originalData != null ? new TaskCategory_Values(originalData, this) : null);
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - TaskCategory_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - TaskCategory_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected TaskCategory_Values _original;
        
        public TaskCategory_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected TaskCategory_Values _current;
        
        public TaskCategory_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected TaskCategory_Values _concurrent;
        
        public TaskCategory_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  TkCt_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  TkCt_Name
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  TkCt_Desc
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  TkCt_IsActiveRow
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  TkCt_CreatedUserId
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  TkCt_CreatedDate
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  TkCt_UserId
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  UserName
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  TkCt_LastModifiedDate
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  TkCt_Stamp
                if (_current._j != _original._j)
                {
                    return true;
                }

                return false;
            }
        }

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Enter);
              //NOTE:  This method is the ONLY method that should call the one in the values object below.
                
                if (_original != null)
                {
                    _original.FinishPartialLoad();
                }
                if (_current != null)
                {
                    _current.FinishPartialLoad();
                }
                if (_concurrent != null)
                {
                    _concurrent.FinishPartialLoad();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
        }

        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{11}{0}{10}_b{11}{1}{10}_c{11}{2}{10}_d{11}{3}{10}_e{11}{4}{10}_f{11}{5}{10}_g{11}{6}{10}_h{11}{7}{10}_i{11}{8}{10}_j{11}{9}",
				new object[] {
				_current._a,		  //TkCt_Id
				_current._b,		  //TkCt_Name
				_current._c,		  //TkCt_Desc
				_current._d,		  //TkCt_IsActiveRow
				_current._e,		  //TkCt_CreatedUserId
				_current._f,		  //TkCt_CreatedDate
				_current._g,		  //TkCt_UserId
				_current._h,		  //UserName
				_current._i,		  //TkCt_LastModifiedDate
				_current._j,		  //TkCt_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    
    public class TaskCategory_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "vProjectManager";
        private static string _cn = "TaskCategory_Values";
        private TaskCategory_ValuesMngr _parent = null;
        private bool _isPartialLoad = false;
        private bool _isPartialDataLoaded = false;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal TaskCategory_Values() 
        {
        }

        public TaskCategory_Values(object[] data, TaskCategory_ValuesMngr parent)
            :this(data, parent, false)
        {  }

        public TaskCategory_Values(object[] data, TaskCategory_ValuesMngr parent, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - TaskCategory_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
                _isPartialLoad = isPartialLoad;
                // Fields that are part of the parital load which will always be loaded
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  TkCt_Id
				_b = ObjectHelper.GetStringFromObjectValue(data[1]);									//  TkCt_Name
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  TkCt_Desc
				_d = ObjectHelper.GetBoolFromObjectValue(data[3]);									//  TkCt_IsActiveRow
				_e = ObjectHelper.GetNullableGuidFromObjectValue(data[4]);						//  TkCt_CreatedUserId
				_f = ObjectHelper.GetNullableDateTimeFromObjectValue(data[5]);					//  TkCt_CreatedDate
				_g = ObjectHelper.GetNullableGuidFromObjectValue(data[6]);						//  TkCt_UserId
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  UserName
				_i = ObjectHelper.GetNullableDateTimeFromObjectValue(data[8]);					//  TkCt_LastModifiedDate
				_j = ObjectHelper.GetByteArrayFromObjectValue(data[9]);						//  TkCt_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - TaskCategory_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - TaskCategory_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - TaskCategory", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  TkCt_Id
				_b = ObjectHelper.GetStringFromObjectValue(data[1]);									//  TkCt_Name
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  TkCt_Desc
				_d = ObjectHelper.GetBoolFromObjectValue(data[3]);									//  TkCt_IsActiveRow
				_e = ObjectHelper.GetNullableGuidFromObjectValue(data[4]);						//  TkCt_CreatedUserId
				_f = ObjectHelper.GetNullableDateTimeFromObjectValue(data[5]);					//  TkCt_CreatedDate
				_g = ObjectHelper.GetNullableGuidFromObjectValue(data[6]);						//  TkCt_UserId
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  UserName
				_i = ObjectHelper.GetNullableDateTimeFromObjectValue(data[8]);					//  TkCt_LastModifiedDate
				_j = ObjectHelper.GetByteArrayFromObjectValue(data[9]);						//  TkCt_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - TaskCategory", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - TaskCategory", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - TaskCategory", IfxTraceCategory.Leave);
				// No fields to load.

                if (_isPartialLoad == true && _isPartialDataLoaded == false)
                {

                    _isPartialDataLoaded = true;
                }
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - TaskCategory", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods


		#region Data Members

		
		public Guid _a;			//  TkCt_Id

		
		public String _b;			//  TkCt_Name

		
		public String _c;			//  TkCt_Desc

		
		public Boolean _d;			//  TkCt_IsActiveRow

		
		public Guid? _e;			//  TkCt_CreatedUserId

		
		public DateTime? _f;			//  TkCt_CreatedDate

		
		public Guid? _g;			//  TkCt_UserId

		
		public String _h;			//  UserName

		
		public DateTime? _i;			//  TkCt_LastModifiedDate

		
		public Byte[] _j;			//  TkCt_Stamp

		
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        public bool IsPartialLoad
        {
            get { return _isPartialLoad; }
            set { _isPartialLoad = value; }
        }

        internal TaskCategory_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 10; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                }
                return _list;
            }
        }

        public Guid TkCt_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid TkCt_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public String TkCt_Name
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TkCt_Name_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public String TkCt_Desc
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TkCt_Desc_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public Boolean TkCt_IsActiveRow
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TkCt_IsActiveRow_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public Guid? TkCt_CreatedUserId
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TkCt_CreatedUserId_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public DateTime? TkCt_CreatedDate
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? TkCt_CreatedDate_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Guid? TkCt_UserId
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TkCt_UserId_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public String UserName
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public DateTime? TkCt_LastModifiedDate
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? TkCt_LastModifiedDate_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Byte[] TkCt_Stamp
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] TkCt_Stamp_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j
			};
        }

        public TaskCategory_Values Clone()
        {
            return new TaskCategory_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


