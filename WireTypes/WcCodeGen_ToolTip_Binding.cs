using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx;

// Gen Timestamp:  1/18/2018 6:19:15 PM

namespace EntityWireType
{


    
    public class WcCodeGen_ToolTip_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcCodeGen_ToolTip_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcCodeGen_ToolTip_Binding() { }


        public WcCodeGen_ToolTip_Binding(Guid _Tt_Id, Guid _Tt_TbC_Id, Int32 _Tt_Sort, String _Tt_Tip )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ToolTip_Binding", IfxTraceCategory.Enter);
				_a = _Tt_Id;
				_b = _Tt_TbC_Id;
				_c = _Tt_Sort;
				_d = _Tt_Tip;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ToolTip_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ToolTip_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcCodeGen_ToolTip_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ToolTip_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  Tt_Id
				_b = (Guid)data[1];                //  Tt_TbC_Id
				_c = (Int32)data[2];                //  Tt_Sort
				_d = (String)data[3];                //  Tt_Tip
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ToolTip_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ToolTip_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region Tt_Id

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid Tt_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Tt_Id


        #region Tt_TbC_Id

        private Guid _b;
//        public Guid B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Guid Tt_TbC_Id
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion Tt_TbC_Id


        #region Tt_Sort

        private Int32 _c;
//        public Int32 C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public Int32 Tt_Sort
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion Tt_Sort


        #region Tt_Tip

        private String _d;
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String Tt_Tip
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion Tt_Tip


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3} ", Tt_Id, Tt_TbC_Id, Tt_Sort, Tt_Tip );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3} ", Tt_Id, Tt_TbC_Id, Tt_Sort, Tt_Tip );
        }

    }

}
