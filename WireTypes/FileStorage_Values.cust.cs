﻿using System;
using System.Text;
using Ifx;
using TypeServices;

namespace EntityWireType
{
    public partial class FileStorage_Values
    {
   
  
        /// <summary>
        /// This is a test stub to make the vButtonColumn work.
        /// Lets see how we can eliminate this property and still have the vButtonColumn work correctly.
        /// </summary>
        public int ItemsCount
        {
            get { return 0; }
        }

        /// <summary>
        /// This is a test stub to make the vButtonColumn work.
        /// Lets see how we can eliminate this property and still have the vButtonColumn work correctly.
        /// </summary>
        public int ItemsCount2
        {
            get { return 0; }
        }

        public String UserStampAndFileSize
        {
            get
            {
                var sb = new StringBuilder();
                if (FS_LastModifiedDate != null)
                {
                    sb.Append(((DateTime)FS_LastModifiedDate).ToString("MMMM dd, yyyy h:mm tt"));
                }

                if (!string.IsNullOrEmpty(UserName))
                {
                    if (sb.Length == 0)
                    {
                        sb.Append(UserName);
                    }
                    else
                    {
                        sb.Append(" by " + UserName);
                    }
                }
                if (sb.Length == 0)
                {
                    sb.Append("(" + GetFileSize() + ")");
                }
                else
                {
                    sb.Append(" (" + GetFileSize() + ")");
                }
                return sb.ToString();             
            }          
        }

        public string GetFileSize()
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" }; //Longs run out around EB
            string result = "";

            if (FS_FileSize.HasValue)
            {
                if (FS_FileSize == 0)
                {
                    result = "0" + suf[0];
                }
                else
                {
                    var bytes = Math.Abs(FS_FileSize.Value);
                    var place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
                    double num = Math.Round(bytes/Math.Pow(1024, place), 1);
                    result = (Math.Sign(FS_FileSize.Value)*num) + suf[place];
                }
            }

            return result;
        }


        public static FileStorage_Values GetDefault()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                Guid Id = Guid.NewGuid();
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Enter);
                return new FileStorage_Values(new object[] 
                    {
                        Id,    //  FS_Id
						null,		//  FS_ParentType
						null,		//  FS_ParentId_Guid
						null,		//  FS_ParentId_Int
						null,		//  FS_ParentId_Object
                        //null,		//  FS_DscCm_Id
						null,		//  FS_FileName
						null,		//  FS_FilePath
						null,		//  FS_FileSize
						null,		//  FS_XrefIdentifier
						false,		//  IsNew
						null,		//  FileData
						null,		//  FS_UserId
						null,		//  UserName
						null,		//  FS_CreatedDate
						null,		//  FS_LastModifiedDate
						null,		//  FS_Stamp
                    }, null
                );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Leave);
            }
        }

        public static FileStorage_Values NewWithDefaultValues(Guid dscCmId, string parentTypeString, byte[] bytes, string name, long length)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                Guid Id = Guid.NewGuid();
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewWithDefaultValues", IfxTraceCategory.Enter);
                return new FileStorage_Values(new object[] 
                    {
                        Id,                     //  FS_Id
						null,	            	//  FS_Cust_Id
						null,	            	//  FS_Or_Id
						null,		            //  FS_Tk_Id
                        //dscCmId,	        	//  FS_DscCm_Id
						parentTypeString,		//  FS_ParentType
						name,		            //  FS_FileName
						null,		            //  FS_FilePath
						length,		            //  FS_FileSize
						null,		            //  FS_XrefIdentifier
						true,	            	//  IsNew
						bytes,		            //  FileData
						null,		//  FS_UserId
						null,		            //  UserName
						null,		            //  FS_CreatedDate
						null,	            	//  FS_LastModifiedDate
						null,	            	//  FS_Stamp
                    }, null
                );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewWithDefaultValues", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewWithDefaultValues", IfxTraceCategory.Leave);
            }
        }
    }
}
