using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;


// Gen Timestamp:  12/1/2016 10:43:36 AM

namespace EntityWireType
{

    #region Entity Values Manager
    
    public class WcStoredProcParamValueGroup_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProcParamValueGroup_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcStoredProcParamValueGroup_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcStoredProcParamValueGroup_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcStoredProcParamValueGroup_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParamValueGroup_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcStoredProcParamValueGroup_Values(currentData, this) : new WcStoredProcParamValueGroup_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcStoredProcParamValueGroup_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParamValueGroup_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParamValueGroup_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcStoredProcParamValueGroup_Values _original;
        
        public WcStoredProcParamValueGroup_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcStoredProcParamValueGroup_Values _current;
        
        public WcStoredProcParamValueGroup_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcStoredProcParamValueGroup_Values _concurrent;
        
        public WcStoredProcParamValueGroup_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  SpPVGrp_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  SpPVGrp_Sp_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  SpPVGrp_Name
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  SpPVGrp_Notes
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  SpPVGrp_IsActiveRow
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  SpPVGrp_IsDeleted
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  SpPVGrp_CreatedUserId
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  SpPVGrp_CreatedDate
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  SpPVGrp_UserId
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  UserName
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  SpPVGrp_LastModifiedDate
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  SpPVGrp_Stamp
                if (_current._l != _original._l)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{13}{0}{12}_b{13}{1}{12}_c{13}{2}{12}_d{13}{3}{12}_e{13}{4}{12}_f{13}{5}{12}_g{13}{6}{12}_h{13}{7}{12}_i{13}{8}{12}_j{13}{9}{12}_k{13}{10}{12}_l{13}{11}",
				new object[] {
				_current._a,		  //SpPVGrp_Id
				_current._b,		  //SpPVGrp_Sp_Id
				_current._c,		  //SpPVGrp_Name
				_current._d,		  //SpPVGrp_Notes
				_current._e,		  //SpPVGrp_IsActiveRow
				_current._f,		  //SpPVGrp_IsDeleted
				_current._g,		  //SpPVGrp_CreatedUserId
				_current._h,		  //SpPVGrp_CreatedDate
				_current._i,		  //SpPVGrp_UserId
				_current._j,		  //UserName
				_current._k,		  //SpPVGrp_LastModifiedDate
				_current._l,		  //SpPVGrp_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    
    public class WcStoredProcParamValueGroup_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProcParamValueGroup_Values";
        private WcStoredProcParamValueGroup_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcStoredProcParamValueGroup_Values() 
        {
        }

        //public WcStoredProcParamValueGroup_Values(object[] data, WcStoredProcParamValueGroup_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcStoredProcParamValueGroup_Values(object[] data, WcStoredProcParamValueGroup_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParamValueGroup_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  SpPVGrp_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  SpPVGrp_Sp_Id
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  SpPVGrp_Name
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  SpPVGrp_Notes
				_e = ObjectHelper.GetBoolFromObjectValue(data[4]);									//  SpPVGrp_IsActiveRow
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  SpPVGrp_IsDeleted
				_g = ObjectHelper.GetNullableGuidFromObjectValue(data[6]);						//  SpPVGrp_CreatedUserId
				_h = ObjectHelper.GetNullableDateTimeFromObjectValue(data[7]);					//  SpPVGrp_CreatedDate
				_i = ObjectHelper.GetNullableGuidFromObjectValue(data[8]);						//  SpPVGrp_UserId
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  UserName
				_k = ObjectHelper.GetNullableDateTimeFromObjectValue(data[10]);					//  SpPVGrp_LastModifiedDate
				_l = ObjectHelper.GetByteArrayFromObjectValue(data[11]);						//  SpPVGrp_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParamValueGroup_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParamValueGroup_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProcParamValueGroup", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  SpPVGrp_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  SpPVGrp_Sp_Id
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  SpPVGrp_Name
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  SpPVGrp_Notes
				_e = ObjectHelper.GetBoolFromObjectValue(data[4]);									//  SpPVGrp_IsActiveRow
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  SpPVGrp_IsDeleted
				_g = ObjectHelper.GetNullableGuidFromObjectValue(data[6]);						//  SpPVGrp_CreatedUserId
				_h = ObjectHelper.GetNullableDateTimeFromObjectValue(data[7]);					//  SpPVGrp_CreatedDate
				_i = ObjectHelper.GetNullableGuidFromObjectValue(data[8]);						//  SpPVGrp_UserId
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  UserName
				_k = ObjectHelper.GetNullableDateTimeFromObjectValue(data[10]);					//  SpPVGrp_LastModifiedDate
				_l = ObjectHelper.GetByteArrayFromObjectValue(data[11]);						//  SpPVGrp_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProcParamValueGroup", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProcParamValueGroup", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		
		public Guid _a;			//  SpPVGrp_Id

		
		public Guid? _b;			//  SpPVGrp_Sp_Id

		
		public String _c;			//  SpPVGrp_Name

		
		public String _d;			//  SpPVGrp_Notes

		
		public Boolean _e;			//  SpPVGrp_IsActiveRow

		
		public Boolean _f;			//  SpPVGrp_IsDeleted

		
		public Guid? _g;			//  SpPVGrp_CreatedUserId

		
		public DateTime? _h;			//  SpPVGrp_CreatedDate

		
		public Guid? _i;			//  SpPVGrp_UserId

		
		public String _j;			//  UserName

		
		public DateTime? _k;			//  SpPVGrp_LastModifiedDate

		
		public Byte[] _l;			//  SpPVGrp_Stamp

		
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcStoredProcParamValueGroup_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 12; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                }
                return _list;
            }
        }

        public Guid SpPVGrp_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid SpPVGrp_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? SpPVGrp_Sp_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? SpPVGrp_Sp_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public String SpPVGrp_Name
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String SpPVGrp_Name_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String SpPVGrp_Notes
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String SpPVGrp_Notes_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public Boolean SpPVGrp_IsActiveRow
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean SpPVGrp_IsActiveRow_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Boolean SpPVGrp_IsDeleted
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean SpPVGrp_IsDeleted_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Guid? SpPVGrp_CreatedUserId
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? SpPVGrp_CreatedUserId_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public DateTime? SpPVGrp_CreatedDate
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? SpPVGrp_CreatedDate_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Guid? SpPVGrp_UserId
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? SpPVGrp_UserId_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public String UserName
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public DateTime? SpPVGrp_LastModifiedDate
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? SpPVGrp_LastModifiedDate_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public Byte[] SpPVGrp_Stamp
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] SpPVGrp_Stamp_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l
			};
        }

        public WcStoredProcParamValueGroup_Values Clone()
        {
            return new WcStoredProcParamValueGroup_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


