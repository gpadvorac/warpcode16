using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx;

namespace EntityWireType
{



    public class Discussion_MyDiscussions_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "Discussion_MyDiscussions_Binding";

        #endregion Initialize Variables


        #region Constructors

        public Discussion_MyDiscussions_Binding() { }


        public Discussion_MyDiscussions_Binding(Guid _Dsc_Id, Int32? _DiscussionCount, String _DiscussionTitles, Guid? _DscXR_Pj_Id, Guid? _DscXR_Ct_Id, String _Ct_Number, Guid? _DscXR_Df_Id, Int32? _Df_Number, Guid? _DscXR_Ls_Id, String _Ls_Number, Guid? _DscXR_Ob_Id, Int32? _Ob_Number, Guid? _DscXR_Tk_Id, Int32? _Tk_Number, Guid? _DscXR_Wl_Id, String _Well, String _Dsc_Title, String _Dsc_Comment, String _Owner, String _Members)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_MyDiscussions_Binding", IfxTraceCategory.Enter);
                _a = _Dsc_Id;
                _b = _DiscussionCount;
                _c = _DiscussionTitles;
                _d = _DscXR_Pj_Id;
                _e = _DscXR_Ct_Id;
                _f = _Ct_Number;
                _g = _DscXR_Df_Id;
                _h = _Df_Number;
                _i = _DscXR_Ls_Id;
                _j = _Ls_Number;
                _k = _DscXR_Ob_Id;
                _l = _Ob_Number;
                _m = _DscXR_Tk_Id;
                _n = _Tk_Number;
                _o = _DscXR_Wl_Id;
                _p = _Well;
                _q = _Dsc_Title;
                _r = _Dsc_Comment;
                _s = _Owner;
                _t = _Members;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_MyDiscussions_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_MyDiscussions_Binding", IfxTraceCategory.Leave);
            }
        }

        public Discussion_MyDiscussions_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_MyDiscussions_Binding", IfxTraceCategory.Enter);
                _a = (Guid)data[0];                //  Dsc_Id
                _b = ObjectHelper.GetNullableIntFromObjectValue(data[1]);                 //  DiscussionCount
                _c = ObjectHelper.GetStringFromObjectValue(data[2]);                 //  DiscussionTitles
                _d = ObjectHelper.GetNullableGuidFromObjectValue(data[3]);                 //  DscXR_Pj_Id
                _e = ObjectHelper.GetNullableGuidFromObjectValue(data[4]);                 //  DscXR_Ct_Id
                _f = ObjectHelper.GetStringFromObjectValue(data[5]);                 //  Ct_Number
                _g = ObjectHelper.GetNullableGuidFromObjectValue(data[6]);                 //  DscXR_Df_Id
                _h = ObjectHelper.GetNullableIntFromObjectValue(data[7]);                 //  Df_Number
                _i = ObjectHelper.GetNullableGuidFromObjectValue(data[8]);                 //  DscXR_Ls_Id
                _j = ObjectHelper.GetStringFromObjectValue(data[9]);                 //  Ls_Number
                _k = ObjectHelper.GetNullableGuidFromObjectValue(data[10]);                 //  DscXR_Ob_Id
                _l = ObjectHelper.GetNullableIntFromObjectValue(data[11]);                 //  Ob_Number
                _m = ObjectHelper.GetNullableGuidFromObjectValue(data[12]);                 //  DscXR_Tk_Id
                _n = ObjectHelper.GetNullableIntFromObjectValue(data[13]);                 //  Tk_Number
                _o = ObjectHelper.GetNullableGuidFromObjectValue(data[14]);                 //  DscXR_Wl_Id
                _p = ObjectHelper.GetStringFromObjectValue(data[15]);                 //  Well
                _q = ObjectHelper.GetStringFromObjectValue(data[16]);                 //  Dsc_Title
                _r = ObjectHelper.GetStringFromObjectValue(data[17]);                 //  Dsc_Comment
                _s = ObjectHelper.GetStringFromObjectValue(data[18]);                 //  Owner
                _t = ObjectHelper.GetStringFromObjectValue(data[19]);                 //  Members
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_MyDiscussions_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_MyDiscussions_Binding", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region Dsc_Id

        private Guid _a;
        //        [DataMember]
        //        public Guid A
        //        {
        //            get { return _a; }
        //            set { _a = value; }
        //        }

        //[NonSerialized]
        public Guid Dsc_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Dsc_Id


        #region DiscussionCount

        private Int32? _b;
        //        [DataMember]
        //        public Int32? B
        //        {
        //            get { return _b; }
        //            set { _b = value; }
        //        }

        //[NonSerialized]
        public Int32? DiscussionCount
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion DiscussionCount


        #region DiscussionTitles

        private String _c;
        //        [DataMember]
        //        public String C
        //        {
        //            get { return _c; }
        //            set { _c = value; }
        //        }

        //[NonSerialized]
        public String DiscussionTitles
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion DiscussionTitles


        #region DscXR_Pj_Id

        private Guid? _d;
        //        [DataMember]
        //        public Guid? D
        //        {
        //            get { return _d; }
        //            set { _d = value; }
        //        }

        //[NonSerialized]
        public Guid? DscXR_Pj_Id
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion DscXR_Pj_Id


        #region DscXR_Ct_Id

        private Guid? _e;
        //        [DataMember]
        //        public Guid? E
        //        {
        //            get { return _e; }
        //            set { _e = value; }
        //        }

        //[NonSerialized]
        public Guid? DscXR_Ct_Id
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion DscXR_Ct_Id


        #region Ct_Number

        private String _f;
        //        [DataMember]
        //        public String F
        //        {
        //            get { return _f; }
        //            set { _f = value; }
        //        }

        //[NonSerialized]
        public String Ct_Number
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion Ct_Number


        #region DscXR_Df_Id

        private Guid? _g;
        //        [DataMember]
        //        public Guid? G
        //        {
        //            get { return _g; }
        //            set { _g = value; }
        //        }

        //[NonSerialized]
        public Guid? DscXR_Df_Id
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion DscXR_Df_Id


        #region Df_Number

        private Int32? _h;
        //        [DataMember]
        //        public Int32? H
        //        {
        //            get { return _h; }
        //            set { _h = value; }
        //        }

        //[NonSerialized]
        public Int32? Df_Number
        {
            get { return _h; }
            set { _h = value; }
        }

        #endregion Df_Number


        #region DscXR_Ls_Id

        private Guid? _i;
        //        [DataMember]
        //        public Guid? I
        //        {
        //            get { return _i; }
        //            set { _i = value; }
        //        }

        //[NonSerialized]
        public Guid? DscXR_Ls_Id
        {
            get { return _i; }
            set { _i = value; }
        }

        #endregion DscXR_Ls_Id


        #region Ls_Number

        private String _j;
        //        [DataMember]
        //        public String J
        //        {
        //            get { return _j; }
        //            set { _j = value; }
        //        }

        //[NonSerialized]
        public String Ls_Number
        {
            get { return _j; }
            set { _j = value; }
        }

        #endregion Ls_Number


        #region DscXR_Ob_Id

        private Guid? _k;
        //        [DataMember]
        //        public Guid? K
        //        {
        //            get { return _k; }
        //            set { _k = value; }
        //        }

        //[NonSerialized]
        public Guid? DscXR_Ob_Id
        {
            get { return _k; }
            set { _k = value; }
        }

        #endregion DscXR_Ob_Id


        #region Ob_Number

        private Int32? _l;
        //        [DataMember]
        //        public Int32? L
        //        {
        //            get { return _l; }
        //            set { _l = value; }
        //        }

        //[NonSerialized]
        public Int32? Ob_Number
        {
            get { return _l; }
            set { _l = value; }
        }

        #endregion Ob_Number


        #region DscXR_Tk_Id

        private Guid? _m;
        //        [DataMember]
        //        public Guid? M
        //        {
        //            get { return _m; }
        //            set { _m = value; }
        //        }

        //[NonSerialized]
        public Guid? DscXR_Tk_Id
        {
            get { return _m; }
            set { _m = value; }
        }

        #endregion DscXR_Tk_Id


        #region Tk_Number

        private Int32? _n;
        //        [DataMember]
        //        public Int32? N
        //        {
        //            get { return _n; }
        //            set { _n = value; }
        //        }

        //[NonSerialized]
        public Int32? Tk_Number
        {
            get { return _n; }
            set { _n = value; }
        }

        #endregion Tk_Number


        #region DscXR_Wl_Id

        private Guid? _o;
        //        [DataMember]
        //        public Guid? O
        //        {
        //            get { return _o; }
        //            set { _o = value; }
        //        }

        //[NonSerialized]
        public Guid? DscXR_Wl_Id
        {
            get { return _o; }
            set { _o = value; }
        }

        #endregion DscXR_Wl_Id


        #region Well

        private String _p;
        //        [DataMember]
        //        public String P
        //        {
        //            get { return _p; }
        //            set { _p = value; }
        //        }

        //[NonSerialized]
        public String Well
        {
            get { return _p; }
            set { _p = value; }
        }

        #endregion Well


        #region Dsc_Title

        private String _q;
        //        [DataMember]
        //        public String Q
        //        {
        //            get { return _q; }
        //            set { _q = value; }
        //        }

        //[NonSerialized]
        public String Dsc_Title
        {
            get { return _q; }
            set { _q = value; }
        }

        #endregion Dsc_Title


        #region Dsc_Comment

        private String _r;
        //        [DataMember]
        //        public String R
        //        {
        //            get { return _r; }
        //            set { _r = value; }
        //        }

        //[NonSerialized]
        public String Dsc_Comment
        {
            get { return _r; }
            set { _r = value; }
        }

        #endregion Dsc_Comment


        #region Owner

        private String _s;
        //        [DataMember]
        //        public String S
        //        {
        //            get { return _s; }
        //            set { _s = value; }
        //        }

        //[NonSerialized]
        public String Owner
        {
            get { return _s; }
            set { _s = value; }
        }

        #endregion Owner


        #region Members

        private String _t;
        //        [DataMember]
        //        public String T
        //        {
        //            get { return _t; }
        //            set { _t = value; }
        //        }

        //[NonSerialized]
        public String Members
        {
            get { return _t; }
            set { _t = value; }
        }

        #endregion Members


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11}; {12}; {13}; {14}; {15}; {16}; {17}; {18}; {19} ", Dsc_Id, DiscussionCount, DiscussionTitles, DscXR_Pj_Id, DscXR_Ct_Id, Ct_Number, DscXR_Df_Id, Df_Number, DscXR_Ls_Id, Ls_Number, DscXR_Ob_Id, Ob_Number, DscXR_Tk_Id, Tk_Number, DscXR_Wl_Id, Well, Dsc_Title, Dsc_Comment, Owner, Members);
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11}; {12}; {13}; {14}; {15}; {16}; {17}; {18}; {19} ", Dsc_Id, DiscussionCount, DiscussionTitles, DscXR_Pj_Id, DscXR_Ct_Id, Ct_Number, DscXR_Df_Id, Df_Number, DscXR_Ls_Id, Ls_Number, DscXR_Ob_Id, Ob_Number, DscXR_Tk_Id, Tk_Number, DscXR_Wl_Id, Well, Dsc_Title, Dsc_Comment, Owner, Members);
        }

    }

}
