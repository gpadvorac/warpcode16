using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx;

// Gen Timestamp:  1/18/2018 6:19:15 PM

namespace EntityWireType
{


    
    public class WcCodeGen_ComboColumn_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcCodeGen_ComboColumn_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcCodeGen_ComboColumn_Binding() { }


        public WcCodeGen_ComboColumn_Binding(Guid _TbCCmbC_Id, Guid _TbCCmbC_TbC_Id, Byte _TbCCmbC_Index, String _TbCCmbC_HeaderText, Double? _TbCCmbC_MinWidth, Double? _TbCCmbC_MaxWidth, Boolean _TbCCmbC_TextWrap )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ComboColumn_Binding", IfxTraceCategory.Enter);
				_a = _TbCCmbC_Id;
				_b = _TbCCmbC_TbC_Id;
				_c = _TbCCmbC_Index;
				_d = _TbCCmbC_HeaderText;
				_e = _TbCCmbC_MinWidth;
				_f = _TbCCmbC_MaxWidth;
				_g = _TbCCmbC_TextWrap;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ComboColumn_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ComboColumn_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcCodeGen_ComboColumn_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ComboColumn_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  TbCCmbC_Id
				_b = (Guid)data[1];                //  TbCCmbC_TbC_Id
				_c = (Byte)data[2];                //  TbCCmbC_Index
				_d = (String)data[3];                //  TbCCmbC_HeaderText
				_e = ObjectHelper.GetNullableDoubleFromObjectValue(data[4]);                 //  TbCCmbC_MinWidth
				_f = ObjectHelper.GetNullableDoubleFromObjectValue(data[5]);                 //  TbCCmbC_MaxWidth
				_g = (Boolean)data[6];                //  TbCCmbC_TextWrap
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ComboColumn_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ComboColumn_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region TbCCmbC_Id

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid TbCCmbC_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion TbCCmbC_Id


        #region TbCCmbC_TbC_Id

        private Guid _b;
//        public Guid B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Guid TbCCmbC_TbC_Id
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion TbCCmbC_TbC_Id


        #region TbCCmbC_Index

        private Byte _c;
//        public Byte C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public Byte TbCCmbC_Index
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion TbCCmbC_Index


        #region TbCCmbC_HeaderText

        private String _d;
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String TbCCmbC_HeaderText
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion TbCCmbC_HeaderText


        #region TbCCmbC_MinWidth

        private Double? _e;
//        public Double? E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public Double? TbCCmbC_MinWidth
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion TbCCmbC_MinWidth


        #region TbCCmbC_MaxWidth

        private Double? _f;
//        public Double? F
//        {
//            get { return _f; }
//            set { _f = value; }
//        }

        //[NonSerialized]
        public Double? TbCCmbC_MaxWidth
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion TbCCmbC_MaxWidth


        #region TbCCmbC_TextWrap

        private Boolean _g;
//        public Boolean G
//        {
//            get { return _g; }
//            set { _g = value; }
//        }

        //[NonSerialized]
        public Boolean TbCCmbC_TextWrap
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion TbCCmbC_TextWrap


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6} ", TbCCmbC_Id, TbCCmbC_TbC_Id, TbCCmbC_Index, TbCCmbC_HeaderText, TbCCmbC_MinWidth, TbCCmbC_MaxWidth, TbCCmbC_TextWrap );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6} ", TbCCmbC_Id, TbCCmbC_TbC_Id, TbCCmbC_Index, TbCCmbC_HeaderText, TbCCmbC_MinWidth, TbCCmbC_MaxWidth, TbCCmbC_TextWrap );
        }

    }

}
