using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;


// Gen Timestamp:  1/3/2018 11:10:45 AM

namespace EntityWireType
{

    #region Entity Values Manager
    
    public class WcApplicationVersion_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcApplicationVersion_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcApplicationVersion_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcApplicationVersion_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcApplicationVersion_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcApplicationVersion_Values(currentData, this) : new WcApplicationVersion_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcApplicationVersion_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcApplicationVersion_Values _original;
        
        public WcApplicationVersion_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcApplicationVersion_Values _current;
        
        public WcApplicationVersion_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcApplicationVersion_Values _concurrent;
        
        public WcApplicationVersion_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  ApVrsn_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  ApVrsn_Ap_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  AttachmentCount
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  AttachmentFileNames
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  DiscussionCount
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  DiscussionTitles
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  VersionNo
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  ApVrsn_MajorVersion
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  ApVrsn_MinorVersion
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  ApVrsn_VersionIteration
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  ApVrsn_Server
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  ApVrsn_DbName
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  ApVrsn_SolutionPath
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  ApVrsn_DefaultUIAssembly
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  ApVrsn_DefaultUIAssemblyPath
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  ApVrsn_DefaultWireTypePath
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  ApVrsn_WebServerURL
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  ApVrsn_WebsiteCodeFolderPath
                if (_current._r != _original._r)
                {
                    return true;
                }

                //  ApVrsn_WebserviceCodeFolderPath
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  ApVrsn_StoredProcCodeFolder
                if (_current._t != _original._t)
                {
                    return true;
                }

                //  ApVrsn_UseLegacyConnectionCode
                if (_current._u != _original._u)
                {
                    return true;
                }

                //  ApVrsn_IsMulticultural
                if (_current._v != _original._v)
                {
                    return true;
                }

                //  ApVrsn_Notes
                if (_current._w != _original._w)
                {
                    return true;
                }

                //  ApVrsn_IsActiveRow
                if (_current._x != _original._x)
                {
                    return true;
                }

                //  ApVrsn_IsDeleted
                if (_current._y != _original._y)
                {
                    return true;
                }

                //  ApVrsn_CreatedUserId
                if (_current._z != _original._z)
                {
                    return true;
                }

                //  ApVrsn_CreatedDate
                if (_current._aa != _original._aa)
                {
                    return true;
                }

                //  ApVrsn_UserId
                if (_current._ab != _original._ab)
                {
                    return true;
                }

                //  UserName
                if (_current._ac != _original._ac)
                {
                    return true;
                }

                //  ApVrsn_LastModifiedDate
                if (_current._ad != _original._ad)
                {
                    return true;
                }

                //  ApVrsn_Stamp
                if (_current._ae != _original._ae)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{32}{0}{31}_b{32}{1}{31}_c{32}{2}{31}_d{32}{3}{31}_e{32}{4}{31}_f{32}{5}{31}_g{32}{6}{31}_h{32}{7}{31}_i{32}{8}{31}_j{32}{9}{31}_k{32}{10}{31}_l{32}{11}{31}_m{32}{12}{31}_n{32}{13}{31}_o{32}{14}{31}_p{32}{15}{31}_q{32}{16}{31}_r{32}{17}{31}_s{32}{18}{31}_t{32}{19}{31}_u{32}{20}{31}_v{32}{21}{31}_w{32}{22}{31}_x{32}{23}{31}_y{32}{24}{31}_z{32}{25}{31}_aa{32}{26}{31}_ab{32}{27}{31}_ac{32}{28}{31}_ad{32}{29}{31}_ae{32}{30}",
				new object[] {
				_current._a,		  //ApVrsn_Id
				_current._b,		  //ApVrsn_Ap_Id
				_current._c,		  //AttachmentCount
				_current._d,		  //AttachmentFileNames
				_current._e,		  //DiscussionCount
				_current._f,		  //DiscussionTitles
				_current._g,		  //VersionNo
				_current._h,		  //ApVrsn_MajorVersion
				_current._i,		  //ApVrsn_MinorVersion
				_current._j,		  //ApVrsn_VersionIteration
				_current._k,		  //ApVrsn_Server
				_current._l,		  //ApVrsn_DbName
				_current._m,		  //ApVrsn_SolutionPath
				_current._n,		  //ApVrsn_DefaultUIAssembly
				_current._o,		  //ApVrsn_DefaultUIAssemblyPath
				_current._p,		  //ApVrsn_DefaultWireTypePath
				_current._q,		  //ApVrsn_WebServerURL
				_current._r,		  //ApVrsn_WebsiteCodeFolderPath
				_current._s,		  //ApVrsn_WebserviceCodeFolderPath
				_current._t,		  //ApVrsn_StoredProcCodeFolder
				_current._u,		  //ApVrsn_UseLegacyConnectionCode
				_current._v,		  //ApVrsn_IsMulticultural
				_current._w,		  //ApVrsn_Notes
				_current._x,		  //ApVrsn_IsActiveRow
				_current._y,		  //ApVrsn_IsDeleted
				_current._z,		  //ApVrsn_CreatedUserId
				_current._aa,		  //ApVrsn_CreatedDate
				_current._ab,		  //ApVrsn_UserId
				_current._ac,		  //UserName
				_current._ad,		  //ApVrsn_LastModifiedDate
				_current._ae,		  //ApVrsn_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    
    public class WcApplicationVersion_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcApplicationVersion_Values";
        private WcApplicationVersion_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcApplicationVersion_Values() 
        {
        }

        //public WcApplicationVersion_Values(object[] data, WcApplicationVersion_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcApplicationVersion_Values(object[] data, WcApplicationVersion_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  ApVrsn_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  ApVrsn_Ap_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  AttachmentCount
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  AttachmentFileNames
				_e = ObjectHelper.GetNullableIntFromObjectValue(data[4]);							//  DiscussionCount
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  DiscussionTitles
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  VersionNo
				_h = ObjectHelper.GetByteFromObjectValue(data[7]);										//  ApVrsn_MajorVersion
				_i = ObjectHelper.GetNullableByteFromObjectValue(data[8]);							//  ApVrsn_MinorVersion
				_j = ObjectHelper.GetNullableByteFromObjectValue(data[9]);							//  ApVrsn_VersionIteration
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  ApVrsn_Server
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  ApVrsn_DbName
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  ApVrsn_SolutionPath
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  ApVrsn_DefaultUIAssembly
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  ApVrsn_DefaultUIAssemblyPath
				_p = ObjectHelper.GetStringFromObjectValue(data[15]);									//  ApVrsn_DefaultWireTypePath
				_q = ObjectHelper.GetStringFromObjectValue(data[16]);									//  ApVrsn_WebServerURL
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  ApVrsn_WebsiteCodeFolderPath
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  ApVrsn_WebserviceCodeFolderPath
				_t = ObjectHelper.GetStringFromObjectValue(data[19]);									//  ApVrsn_StoredProcCodeFolder
				_u = ObjectHelper.GetBoolFromObjectValue(data[20]);									//  ApVrsn_UseLegacyConnectionCode
				_v = ObjectHelper.GetBoolFromObjectValue(data[21]);									//  ApVrsn_IsMulticultural
				_w = ObjectHelper.GetStringFromObjectValue(data[22]);									//  ApVrsn_Notes
				_x = ObjectHelper.GetBoolFromObjectValue(data[23]);									//  ApVrsn_IsActiveRow
				_y = ObjectHelper.GetBoolFromObjectValue(data[24]);									//  ApVrsn_IsDeleted
				_z = ObjectHelper.GetNullableGuidFromObjectValue(data[25]);						//  ApVrsn_CreatedUserId
				_aa = ObjectHelper.GetNullableDateTimeFromObjectValue(data[26]);					//  ApVrsn_CreatedDate
				_ab = ObjectHelper.GetNullableGuidFromObjectValue(data[27]);						//  ApVrsn_UserId
				_ac = ObjectHelper.GetStringFromObjectValue(data[28]);									//  UserName
				_ad = ObjectHelper.GetNullableDateTimeFromObjectValue(data[29]);					//  ApVrsn_LastModifiedDate
				_ae = data[30] as Byte[];						//  ApVrsn_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplicationVersion", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  ApVrsn_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  ApVrsn_Ap_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  AttachmentCount
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  AttachmentFileNames
				_e = ObjectHelper.GetNullableIntFromObjectValue(data[4]);							//  DiscussionCount
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  DiscussionTitles
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  VersionNo
				_h = ObjectHelper.GetByteFromObjectValue(data[7]);										//  ApVrsn_MajorVersion
				_i = ObjectHelper.GetNullableByteFromObjectValue(data[8]);							//  ApVrsn_MinorVersion
				_j = ObjectHelper.GetNullableByteFromObjectValue(data[9]);							//  ApVrsn_VersionIteration
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  ApVrsn_Server
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  ApVrsn_DbName
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  ApVrsn_SolutionPath
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  ApVrsn_DefaultUIAssembly
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  ApVrsn_DefaultUIAssemblyPath
				_p = ObjectHelper.GetStringFromObjectValue(data[15]);									//  ApVrsn_DefaultWireTypePath
				_q = ObjectHelper.GetStringFromObjectValue(data[16]);									//  ApVrsn_WebServerURL
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  ApVrsn_WebsiteCodeFolderPath
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  ApVrsn_WebserviceCodeFolderPath
				_t = ObjectHelper.GetStringFromObjectValue(data[19]);									//  ApVrsn_StoredProcCodeFolder
				_u = ObjectHelper.GetBoolFromObjectValue(data[20]);									//  ApVrsn_UseLegacyConnectionCode
				_v = ObjectHelper.GetBoolFromObjectValue(data[21]);									//  ApVrsn_IsMulticultural
				_w = ObjectHelper.GetStringFromObjectValue(data[22]);									//  ApVrsn_Notes
				_x = ObjectHelper.GetBoolFromObjectValue(data[23]);									//  ApVrsn_IsActiveRow
				_y = ObjectHelper.GetBoolFromObjectValue(data[24]);									//  ApVrsn_IsDeleted
				_z = ObjectHelper.GetNullableGuidFromObjectValue(data[25]);						//  ApVrsn_CreatedUserId
				_aa = ObjectHelper.GetNullableDateTimeFromObjectValue(data[26]);					//  ApVrsn_CreatedDate
				_ab = ObjectHelper.GetNullableGuidFromObjectValue(data[27]);						//  ApVrsn_UserId
				_ac = ObjectHelper.GetStringFromObjectValue(data[28]);									//  UserName
				_ad = ObjectHelper.GetNullableDateTimeFromObjectValue(data[29]);					//  ApVrsn_LastModifiedDate
				_ae = data[30] as Byte[];						//  ApVrsn_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplicationVersion", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplicationVersion", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		
		public Guid _a;			//  ApVrsn_Id

		
		public Guid? _b;			//  ApVrsn_Ap_Id

		
		public Int32? _c;			//  AttachmentCount

		
		public String _d;			//  AttachmentFileNames

		
		public Int32? _e;			//  DiscussionCount

		
		public String _f;			//  DiscussionTitles

		
		public String _g;			//  VersionNo

		
		public Byte _h;			//  ApVrsn_MajorVersion

		
		public Byte? _i;			//  ApVrsn_MinorVersion

		
		public Byte? _j;			//  ApVrsn_VersionIteration

		
		public String _k;			//  ApVrsn_Server

		
		public String _l;			//  ApVrsn_DbName

		
		public String _m;			//  ApVrsn_SolutionPath

		
		public String _n;			//  ApVrsn_DefaultUIAssembly

		
		public String _o;			//  ApVrsn_DefaultUIAssemblyPath

		
		public String _p;			//  ApVrsn_DefaultWireTypePath

		
		public String _q;			//  ApVrsn_WebServerURL

		
		public String _r;			//  ApVrsn_WebsiteCodeFolderPath

		
		public String _s;			//  ApVrsn_WebserviceCodeFolderPath

		
		public String _t;			//  ApVrsn_StoredProcCodeFolder

		
		public Boolean _u;			//  ApVrsn_UseLegacyConnectionCode

		
		public Boolean _v;			//  ApVrsn_IsMulticultural

		
		public String _w;			//  ApVrsn_Notes

		
		public Boolean _x;			//  ApVrsn_IsActiveRow

		
		public Boolean _y;			//  ApVrsn_IsDeleted

		
		public Guid? _z;			//  ApVrsn_CreatedUserId

		
		public DateTime? _aa;			//  ApVrsn_CreatedDate

		
		public Guid? _ab;			//  ApVrsn_UserId

		
		public String _ac;			//  UserName

		
		public DateTime? _ad;			//  ApVrsn_LastModifiedDate

		
		public Byte[] _ae;			//  ApVrsn_Stamp

		
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcApplicationVersion_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 31; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                    _list[18] = _s;
                    _list[19] = _t;
                    _list[20] = _u;
                    _list[21] = _v;
                    _list[22] = _w;
                    _list[23] = _x;
                    _list[24] = _y;
                    _list[25] = _z;
                    _list[26] = _aa;
                    _list[27] = _ab;
                    _list[28] = _ac;
                    _list[29] = _ad;
                    _list[30] = _ae;
                }
                return _list;
            }
        }

        public Guid ApVrsn_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid ApVrsn_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? ApVrsn_Ap_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApVrsn_Ap_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Int32? AttachmentCount
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? AttachmentCount_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String AttachmentFileNames
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String AttachmentFileNames_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public Int32? DiscussionCount
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? DiscussionCount_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public String DiscussionTitles
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DiscussionTitles_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public String VersionNo
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String VersionNo_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Byte ApVrsn_MajorVersion
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte ApVrsn_MajorVersion_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Byte? ApVrsn_MinorVersion
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte? ApVrsn_MinorVersion_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Byte? ApVrsn_VersionIteration
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte? ApVrsn_VersionIteration_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public String ApVrsn_Server
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_Server_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public String ApVrsn_DbName
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DbName_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public String ApVrsn_SolutionPath
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_SolutionPath_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public String ApVrsn_DefaultUIAssembly
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DefaultUIAssembly_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public String ApVrsn_DefaultUIAssemblyPath
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DefaultUIAssemblyPath_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public String ApVrsn_DefaultWireTypePath
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DefaultWireTypePath_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public String ApVrsn_WebServerURL
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_WebServerURL_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public String ApVrsn_WebsiteCodeFolderPath
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_WebsiteCodeFolderPath_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        public String ApVrsn_WebserviceCodeFolderPath
        {
            get { return _s; }
            set
            {
                _s = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_WebserviceCodeFolderPath_noevents
        {
            get { return _s; }
            set
            {
                _s = value;
            }
        }

        public String ApVrsn_StoredProcCodeFolder
        {
            get { return _t; }
            set
            {
                _t = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_StoredProcCodeFolder_noevents
        {
            get { return _t; }
            set
            {
                _t = value;
            }
        }

        public Boolean ApVrsn_UseLegacyConnectionCode
        {
            get { return _u; }
            set
            {
                _u = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean ApVrsn_UseLegacyConnectionCode_noevents
        {
            get { return _u; }
            set
            {
                _u = value;
            }
        }

        public Boolean ApVrsn_IsMulticultural
        {
            get { return _v; }
            set
            {
                _v = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean ApVrsn_IsMulticultural_noevents
        {
            get { return _v; }
            set
            {
                _v = value;
            }
        }

        public String ApVrsn_Notes
        {
            get { return _w; }
            set
            {
                _w = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_Notes_noevents
        {
            get { return _w; }
            set
            {
                _w = value;
            }
        }

        public Boolean ApVrsn_IsActiveRow
        {
            get { return _x; }
            set
            {
                _x = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean ApVrsn_IsActiveRow_noevents
        {
            get { return _x; }
            set
            {
                _x = value;
            }
        }

        public Boolean ApVrsn_IsDeleted
        {
            get { return _y; }
            set
            {
                _y = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean ApVrsn_IsDeleted_noevents
        {
            get { return _y; }
            set
            {
                _y = value;
            }
        }

        public Guid? ApVrsn_CreatedUserId
        {
            get { return _z; }
            set
            {
                _z = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApVrsn_CreatedUserId_noevents
        {
            get { return _z; }
            set
            {
                _z = value;
            }
        }

        public DateTime? ApVrsn_CreatedDate
        {
            get { return _aa; }
            set
            {
                _aa = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? ApVrsn_CreatedDate_noevents
        {
            get { return _aa; }
            set
            {
                _aa = value;
            }
        }

        public Guid? ApVrsn_UserId
        {
            get { return _ab; }
            set
            {
                _ab = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApVrsn_UserId_noevents
        {
            get { return _ab; }
            set
            {
                _ab = value;
            }
        }

        public String UserName
        {
            get { return _ac; }
            set
            {
                _ac = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _ac; }
            set
            {
                _ac = value;
            }
        }

        public DateTime? ApVrsn_LastModifiedDate
        {
            get { return _ad; }
            set
            {
                _ad = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? ApVrsn_LastModifiedDate_noevents
        {
            get { return _ad; }
            set
            {
                _ad = value;
            }
        }

        public Byte[] ApVrsn_Stamp
        {
            get { return _ae; }
            set
            {
                _ae = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] ApVrsn_Stamp_noevents
        {
            get { return _ae; }
            set
            {
                _ae = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r,
				_s,
				_t,
				_u,
				_v,
				_w,
				_x,
				_y,
				_z,
				_aa,
				_ab,
				_ac,
				_ad,
				_ae
			};
        }

        public WcApplicationVersion_Values Clone()
        {
            return new WcApplicationVersion_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


