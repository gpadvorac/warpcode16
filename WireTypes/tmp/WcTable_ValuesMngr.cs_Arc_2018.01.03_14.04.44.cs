using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;


// Gen Timestamp:  1/3/2018 2:03:02 PM

namespace EntityWireType
{

    #region Entity Values Manager
    
    public class WcTable_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTable_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcTable_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcTable_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcTable_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcTable_Values(currentData, this) : new WcTable_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcTable_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcTable_Values _original;
        
        public WcTable_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcTable_Values _current;
        
        public WcTable_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcTable_Values _concurrent;
        
        public WcTable_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  Tb_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  Tb_ApVrsn_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  Tb_auditTable_Id
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  AttachmentCount
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  AttachmentFileNames
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  DiscussionCount
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  DiscussionTitles
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  ImportTable
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  Tb_Name
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  TableInDatabase
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  Tb_EntityRootName
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  Tb_VariableName
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  Tb_ScreenCaption
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  Tb_Description
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  Tb_DevelopmentNote
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  Tb_UIAssemblyName
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  Tb_UINamespace
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  Tb_UIAssemblyPath
                if (_current._r != _original._r)
                {
                    return true;
                }

                //  Tb_ProxyAssemblyName
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  Tb_ProxyAssemblyPath
                if (_current._t != _original._t)
                {
                    return true;
                }

                //  Tb_WebServiceName
                if (_current._u != _original._u)
                {
                    return true;
                }

                //  Tb_WebServiceFolder
                if (_current._v != _original._v)
                {
                    return true;
                }

                //  Tb_DataServiceAssemblyName
                if (_current._w != _original._w)
                {
                    return true;
                }

                //  Tb_DataServicePath
                if (_current._x != _original._x)
                {
                    return true;
                }

                //  Tb_WireTypeAssemblyName
                if (_current._y != _original._y)
                {
                    return true;
                }

                //  Tb_WireTypePath
                if (_current._z != _original._z)
                {
                    return true;
                }

                //  Tb_UseTilesInPropsScreen
                if (_current._aa != _original._aa)
                {
                    return true;
                }

                //  Tb_UseGridColumnGroups
                if (_current._ab != _original._ab)
                {
                    return true;
                }

                //  Tb_UseGridDataSourceCombo
                if (_current._ac != _original._ac)
                {
                    return true;
                }

                //  Tb_ApCnSK_Id
                if (_current._ad != _original._ad)
                {
                    return true;
                }

                //  Tb_ApCnSK_Id_TextField
                //if (_current._ae != _original._ae)
                //{
                //    return true;
                //}

                //  Tb_UseLegacyConnectionCode
                if (_current._af != _original._af)
                {
                    return true;
                }

                //  Tb_PkIsIdentity
                if (_current._ag != _original._ag)
                {
                    return true;
                }

                //  Tb_IsVirtual
                if (_current._ah != _original._ah)
                {
                    return true;
                }

                //  Tb_IsScreenPlaceHolder
                if (_current._ai != _original._ai)
                {
                    return true;
                }

                //  Tb_IsNotEntity
                if (_current._aj != _original._aj)
                {
                    return true;
                }

                //  Tb_IsMany2Many
                if (_current._ak != _original._ak)
                {
                    return true;
                }

                //  Tb_UseLastModifiedByUserNameInSproc
                if (_current._al != _original._al)
                {
                    return true;
                }

                //  Tb_UseUserTimeStamp
                if (_current._am != _original._am)
                {
                    return true;
                }

                //  Tb_UseForAudit
                if (_current._an != _original._an)
                {
                    return true;
                }

                //  Tb_IsAllowDelete
                if (_current._ao != _original._ao)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_MenuRow_IsVisible
                if (_current._ap != _original._ap)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_GridTools_IsVisible
                if (_current._aq != _original._aq)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_SplitScreen_IsVisible
                if (_current._ar != _original._ar)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
                if (_current._asxx != _original._asxx)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_NavColumnWidth
                if (_current._at != _original._at)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_IsReadOnly
                if (_current._au != _original._au)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_IsAllowNewRow
                if (_current._av != _original._av)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_ExcelExport_IsVisible
                if (_current._aw != _original._aw)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_ColumnChooser_IsVisible
                if (_current._ax != _original._ax)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
                if (_current._ay != _original._ay)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_RefreshGrid_IsVisible
                if (_current._az != _original._az)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
                if (_current._ba != _original._ba)
                {
                    return true;
                }

                //  Tb_IsInputComplete
                if (_current._bb != _original._bb)
                {
                    return true;
                }

                //  Tb_IsCodeGen
                if (_current._bc != _original._bc)
                {
                    return true;
                }

                //  Tb_IsReadyCodeGen
                if (_current._bd != _original._bd)
                {
                    return true;
                }

                //  Tb_IsCodeGenComplete
                if (_current._be != _original._be)
                {
                    return true;
                }

                //  Tb_IsTagForCodeGen
                if (_current._bf != _original._bf)
                {
                    return true;
                }

                //  Tb_IsTagForOther
                if (_current._bg != _original._bg)
                {
                    return true;
                }

                //  Tb_TableGroups
                if (_current._bh != _original._bh)
                {
                    return true;
                }

                //  Tb_IsActiveRow
                if (_current._bi != _original._bi)
                {
                    return true;
                }

                //  Tb_IsDeleted
                if (_current._bj != _original._bj)
                {
                    return true;
                }

                //  Tb_CreatedUserId
                if (_current._bk != _original._bk)
                {
                    return true;
                }

                //  Tb_CreatedDate
                if (_current._bl != _original._bl)
                {
                    return true;
                }

                //  Tb_UserId
                if (_current._bm != _original._bm)
                {
                    return true;
                }

                //  UserName
                if (_current._bn != _original._bn)
                {
                    return true;
                }

                //  Tb_LastModifiedDate
                if (_current._bo != _original._bo)
                {
                    return true;
                }

                //  Tb_Stamp
                if (_current._bp != _original._bp)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{69}{0}{68}_b{69}{1}{68}_c{69}{2}{68}_d{69}{3}{68}_e{69}{4}{68}_f{69}{5}{68}_g{69}{6}{68}_h{69}{7}{68}_i{69}{8}{68}_j{69}{9}{68}_k{69}{10}{68}_l{69}{11}{68}_m{69}{12}{68}_n{69}{13}{68}_o{69}{14}{68}_p{69}{15}{68}_q{69}{16}{68}_r{69}{17}{68}_s{69}{18}{68}_t{69}{19}{68}_u{69}{20}{68}_v{69}{21}{68}_w{69}{22}{68}_x{69}{23}{68}_y{69}{24}{68}_z{69}{25}{68}_aa{69}{26}{68}_ab{69}{27}{68}_ac{69}{28}{68}_ad{69}{29}{68}_ae{69}{30}{68}_af{69}{31}{68}_ag{69}{32}{68}_ah{69}{33}{68}_ai{69}{34}{68}_aj{69}{35}{68}_ak{69}{36}{68}_al{69}{37}{68}_am{69}{38}{68}_an{69}{39}{68}_ao{69}{40}{68}_ap{69}{41}{68}_aq{69}{42}{68}_ar{69}{43}{68}_asxx{69}{44}{68}_at{69}{45}{68}_au{69}{46}{68}_av{69}{47}{68}_aw{69}{48}{68}_ax{69}{49}{68}_ay{69}{50}{68}_az{69}{51}{68}_ba{69}{52}{68}_bb{69}{53}{68}_bc{69}{54}{68}_bd{69}{55}{68}_be{69}{56}{68}_bf{69}{57}{68}_bg{69}{58}{68}_bh{69}{59}{68}_bi{69}{60}{68}_bj{69}{61}{68}_bk{69}{62}{68}_bl{69}{63}{68}_bm{69}{64}{68}_bn{69}{65}{68}_bo{69}{66}{68}_bp{69}{67}",
				new object[] {
				_current._a,		  //Tb_Id
				_current._b,		  //Tb_ApVrsn_Id
				_current._c,		  //Tb_auditTable_Id
				_current._d,		  //AttachmentCount
				_current._e,		  //AttachmentFileNames
				_current._f,		  //DiscussionCount
				_current._g,		  //DiscussionTitles
				_current._h,		  //ImportTable
				_current._i,		  //Tb_Name
				_current._j,		  //TableInDatabase
				_current._k,		  //Tb_EntityRootName
				_current._l,		  //Tb_VariableName
				_current._m,		  //Tb_ScreenCaption
				_current._n,		  //Tb_Description
				_current._o,		  //Tb_DevelopmentNote
				_current._p,		  //Tb_UIAssemblyName
				_current._q,		  //Tb_UINamespace
				_current._r,		  //Tb_UIAssemblyPath
				_current._s,		  //Tb_ProxyAssemblyName
				_current._t,		  //Tb_ProxyAssemblyPath
				_current._u,		  //Tb_WebServiceName
				_current._v,		  //Tb_WebServiceFolder
				_current._w,		  //Tb_DataServiceAssemblyName
				_current._x,		  //Tb_DataServicePath
				_current._y,		  //Tb_WireTypeAssemblyName
				_current._z,		  //Tb_WireTypePath
				_current._aa,		  //Tb_UseTilesInPropsScreen
				_current._ab,		  //Tb_UseGridColumnGroups
				_current._ac,		  //Tb_UseGridDataSourceCombo
				_current._ad,		  //Tb_ApCnSK_Id
				_current._ae,		  //Tb_ApCnSK_Id_TextField
				_current._af,		  //Tb_UseLegacyConnectionCode
				_current._ag,		  //Tb_PkIsIdentity
				_current._ah,		  //Tb_IsVirtual
				_current._ai,		  //Tb_IsScreenPlaceHolder
				_current._aj,		  //Tb_IsNotEntity
				_current._ak,		  //Tb_IsMany2Many
				_current._al,		  //Tb_UseLastModifiedByUserNameInSproc
				_current._am,		  //Tb_UseUserTimeStamp
				_current._an,		  //Tb_UseForAudit
				_current._ao,		  //Tb_IsAllowDelete
				_current._ap,		  //Tb_CnfgGdMnu_MenuRow_IsVisible
				_current._aq,		  //Tb_CnfgGdMnu_GridTools_IsVisible
				_current._ar,		  //Tb_CnfgGdMnu_SplitScreen_IsVisible
				_current._asxx,		  //Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
				_current._at,		  //Tb_CnfgGdMnu_NavColumnWidth
				_current._au,		  //Tb_CnfgGdMnu_IsReadOnly
				_current._av,		  //Tb_CnfgGdMnu_IsAllowNewRow
				_current._aw,		  //Tb_CnfgGdMnu_ExcelExport_IsVisible
				_current._ax,		  //Tb_CnfgGdMnu_ColumnChooser_IsVisible
				_current._ay,		  //Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
				_current._az,		  //Tb_CnfgGdMnu_RefreshGrid_IsVisible
				_current._ba,		  //Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
				_current._bb,		  //Tb_IsInputComplete
				_current._bc,		  //Tb_IsCodeGen
				_current._bd,		  //Tb_IsReadyCodeGen
				_current._be,		  //Tb_IsCodeGenComplete
				_current._bf,		  //Tb_IsTagForCodeGen
				_current._bg,		  //Tb_IsTagForOther
				_current._bh,		  //Tb_TableGroups
				_current._bi,		  //Tb_IsActiveRow
				_current._bj,		  //Tb_IsDeleted
				_current._bk,		  //Tb_CreatedUserId
				_current._bl,		  //Tb_CreatedDate
				_current._bm,		  //Tb_UserId
				_current._bn,		  //UserName
				_current._bo,		  //Tb_LastModifiedDate
				_current._bp,		  //Tb_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    
    public class WcTable_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTable_Values";
        private WcTable_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcTable_Values() 
        {
        }

        //public WcTable_Values(object[] data, WcTable_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcTable_Values(object[] data, WcTable_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Tb_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Tb_ApVrsn_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  Tb_auditTable_Id
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  AttachmentCount
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  AttachmentFileNames
				_f = ObjectHelper.GetNullableIntFromObjectValue(data[5]);							//  DiscussionCount
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  DiscussionTitles

    }
    #endregion Entity Values
}


