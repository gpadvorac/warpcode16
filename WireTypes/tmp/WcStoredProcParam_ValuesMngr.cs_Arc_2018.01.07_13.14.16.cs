using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;


// Gen Timestamp:  12/26/2017 1:50:25 PM

namespace EntityWireType
{

    #region Entity Values Manager
    
    public class WcStoredProcParam_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProcParam_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcStoredProcParam_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcStoredProcParam_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcStoredProcParam_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParam_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcStoredProcParam_Values(currentData, this) : new WcStoredProcParam_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcStoredProcParam_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParam_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParam_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcStoredProcParam_Values _original;
        
        public WcStoredProcParam_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcStoredProcParam_Values _current;
        
        public WcStoredProcParam_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcStoredProcParam_Values _concurrent;
        
        public WcStoredProcParam_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  SpP_ID
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  SpP_Sp_ID
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  SpP_SortOrder
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  SpP_Name
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  SpP_SpPDr_Id
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  SpP_SpPDr_Id_TextField
                //if (_current._f != _original._f)
                //{
                //    return true;
                //}

                //  SpP_DtNt_ID
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  SpP_DtNt_ID_TextField
                //if (_current._h != _original._h)
                //{
                //    return true;
                //}

                //  SpP_DtSq_ID
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  SpP_DtSq_ID_TextField
                //if (_current._j != _original._j)
                //{
                //    return true;
                //}

                //  SpP_IsNullable
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  SpP_DefaultValue
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  SpP_Value
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  SpP_IsActiveRow
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  SpP_IsDeleted
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  SpP_CreatedUserId
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  SpP_CreatedDate
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  SpP_UserId
                if (_current._r != _original._r)
                {
                    return true;
                }

                //  UserName
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  SpP_LastModifiedDate
                if (_current._t != _original._t)
                {
                    return true;
                }

                //  SpP_Stamp
                if (_current._u != _original._u)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{22}{0}{21}_b{22}{1}{21}_c{22}{2}{21}_d{22}{3}{21}_e{22}{4}{21}_f{22}{5}{21}_g{22}{6}{21}_h{22}{7}{21}_i{22}{8}{21}_j{22}{9}{21}_k{22}{10}{21}_l{22}{11}{21}_m{22}{12}{21}_n{22}{13}{21}_o{22}{14}{21}_p{22}{15}{21}_q{22}{16}{21}_r{22}{17}{21}_s{22}{18}{21}_t{22}{19}{21}_u{22}{20}",
				new object[] {
				_current._a,		  //SpP_ID
				_current._b,		  //SpP_Sp_ID
				_current._c,		  //SpP_SortOrder
				_current._d,		  //SpP_Name
				_current._e,		  //SpP_SpPDr_Id
				_current._f,		  //SpP_SpPDr_Id_TextField
				_current._g,		  //SpP_DtNt_ID
				_current._h,		  //SpP_DtNt_ID_TextField
				_current._i,		  //SpP_DtSq_ID
				_current._j,		  //SpP_DtSq_ID_TextField
				_current._k,		  //SpP_IsNullable
				_current._l,		  //SpP_DefaultValue
				_current._m,		  //SpP_Value
				_current._n,		  //SpP_IsActiveRow
				_current._o,		  //SpP_IsDeleted
				_current._p,		  //SpP_CreatedUserId
				_current._q,		  //SpP_CreatedDate
				_current._r,		  //SpP_UserId
				_current._s,		  //UserName
				_current._t,		  //SpP_LastModifiedDate
				_current._u,		  //SpP_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    
    public class WcStoredProcParam_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProcParam_Values";
        private WcStoredProcParam_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcStoredProcParam_Values() 
        {
        }

        //public WcStoredProcParam_Values(object[] data, WcStoredProcParam_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcStoredProcParam_Values(object[] data, WcStoredProcParam_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParam_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  SpP_ID
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  SpP_Sp_ID
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  SpP_SortOrder
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  SpP_Name
				_e = ObjectHelper.GetNullableIntFromObjectValue(data[4]);							//  SpP_SpPDr_Id
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  SpP_SpPDr_Id_TextField
				_g = ObjectHelper.GetNullableIntFromObjectValue(data[6]);							//  SpP_DtNt_ID
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  SpP_DtNt_ID_TextField
				_i = ObjectHelper.GetNullableIntFromObjectValue(data[8]);							//  SpP_DtSq_ID
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  SpP_DtSq_ID_TextField
				_k = ObjectHelper.GetBoolFromObjectValue(data[10]);									//  SpP_IsNullable
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  SpP_DefaultValue
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  SpP_Value
				_n = ObjectHelper.GetBoolFromObjectValue(data[13]);									//  SpP_IsActiveRow
				_o = ObjectHelper.GetBoolFromObjectValue(data[14]);									//  SpP_IsDeleted
				_p = ObjectHelper.GetNullableGuidFromObjectValue(data[15]);						//  SpP_CreatedUserId
				_q = ObjectHelper.GetNullableDateTimeFromObjectValue(data[16]);					//  SpP_CreatedDate
				_r = ObjectHelper.GetNullableGuidFromObjectValue(data[17]);						//  SpP_UserId
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  UserName
				_t = ObjectHelper.GetNullableDateTimeFromObjectValue(data[19]);					//  SpP_LastModifiedDate
				_u = data[20] as Byte[];						//  SpP_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParam_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParam_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProcParam", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  SpP_ID
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  SpP_Sp_ID
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  SpP_SortOrder
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  SpP_Name
				_e = ObjectHelper.GetNullableIntFromObjectValue(data[4]);							//  SpP_SpPDr_Id
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  SpP_SpPDr_Id_TextField
				_g = ObjectHelper.GetNullableIntFromObjectValue(data[6]);							//  SpP_DtNt_ID
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  SpP_DtNt_ID_TextField
				_i = ObjectHelper.GetNullableIntFromObjectValue(data[8]);							//  SpP_DtSq_ID
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  SpP_DtSq_ID_TextField
				_k = ObjectHelper.GetBoolFromObjectValue(data[10]);									//  SpP_IsNullable
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  SpP_DefaultValue
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  SpP_Value
				_n = ObjectHelper.GetBoolFromObjectValue(data[13]);									//  SpP_IsActiveRow
				_o = ObjectHelper.GetBoolFromObjectValue(data[14]);									//  SpP_IsDeleted
				_p = ObjectHelper.GetNullableGuidFromObjectValue(data[15]);						//  SpP_CreatedUserId
				_q = ObjectHelper.GetNullableDateTimeFromObjectValue(data[16]);					//  SpP_CreatedDate
				_r = ObjectHelper.GetNullableGuidFromObjectValue(data[17]);						//  SpP_UserId
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  UserName
				_t = ObjectHelper.GetNullableDateTimeFromObjectValue(data[19]);					//  SpP_LastModifiedDate
				_u = data[20] as Byte[];						//  SpP_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProcParam", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProcParam", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		
		public Guid _a;			//  SpP_ID

		
		public Guid? _b;			//  SpP_Sp_ID

		
		public Int32? _c;			//  SpP_SortOrder

		
		public String _d;			//  SpP_Name

		
		public Int32? _e;			//  SpP_SpPDr_Id

		
		public String _f;			//  SpP_SpPDr_Id_TextField

		
		public Int32? _g;			//  SpP_DtNt_ID

		
		public String _h;			//  SpP_DtNt_ID_TextField

		
		public Int32? _i;			//  SpP_DtSq_ID

		
		public String _j;			//  SpP_DtSq_ID_TextField

		
		public Boolean _k;			//  SpP_IsNullable

		
		public String _l;			//  SpP_DefaultValue

		
		public String _m;			//  SpP_Value

		
		public Boolean _n;			//  SpP_IsActiveRow

		
		public Boolean _o;			//  SpP_IsDeleted

		
		public Guid? _p;			//  SpP_CreatedUserId

		
		public DateTime? _q;			//  SpP_CreatedDate

		
		public Guid? _r;			//  SpP_UserId

		
		public String _s;			//  UserName

		
		public DateTime? _t;			//  SpP_LastModifiedDate

		
		public Byte[] _u;			//  SpP_Stamp

		
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcStoredProcParam_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 21; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                    _list[18] = _s;
                    _list[19] = _t;
                    _list[20] = _u;
                }
                return _list;
            }
        }

        public Guid SpP_ID
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid SpP_ID_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? SpP_Sp_ID
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? SpP_Sp_ID_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Int32? SpP_SortOrder
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? SpP_SortOrder_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String SpP_Name
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String SpP_Name_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public Int32? SpP_SpPDr_Id
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? SpP_SpPDr_Id_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public String SpP_SpPDr_Id_TextField
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String SpP_SpPDr_Id_TextField_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Int32? SpP_DtNt_ID
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? SpP_DtNt_ID_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public String SpP_DtNt_ID_TextField
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String SpP_DtNt_ID_TextField_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Int32? SpP_DtSq_ID
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? SpP_DtSq_ID_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public String SpP_DtSq_ID_TextField
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String SpP_DtSq_ID_TextField_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public Boolean SpP_IsNullable
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean SpP_IsNullable_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public String SpP_DefaultValue
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String SpP_DefaultValue_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public String SpP_Value
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String SpP_Value_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public Boolean SpP_IsActiveRow
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean SpP_IsActiveRow_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public Boolean SpP_IsDeleted
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean SpP_IsDeleted_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public Guid? SpP_CreatedUserId
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? SpP_CreatedUserId_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public DateTime? SpP_CreatedDate
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? SpP_CreatedDate_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public Guid? SpP_UserId
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? SpP_UserId_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        public String UserName
        {
            get { return _s; }
            set
            {
                _s = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _s; }
            set
            {
                _s = value;
            }
        }

        public DateTime? SpP_LastModifiedDate
        {
            get { return _t; }
            set
            {
                _t = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? SpP_LastModifiedDate_noevents
        {
            get { return _t; }
            set
            {
                _t = value;
            }
        }

        public Byte[] SpP_Stamp
        {
            get { return _u; }
            set
            {
                _u = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] SpP_Stamp_noevents
        {
            get { return _u; }
            set
            {
                _u = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r,
				_s,
				_t,
				_u
			};
        }

        public WcStoredProcParam_Values Clone()
        {
            return new WcStoredProcParam_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


