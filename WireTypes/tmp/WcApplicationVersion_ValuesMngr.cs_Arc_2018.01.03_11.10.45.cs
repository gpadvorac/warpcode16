using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;


// Gen Timestamp:  12/24/2016 12:36:59 AM

namespace EntityWireType
{

    #region Entity Values Manager
    
    public class WcApplicationVersion_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcApplicationVersion_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcApplicationVersion_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcApplicationVersion_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcApplicationVersion_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcApplicationVersion_Values(currentData, this) : new WcApplicationVersion_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcApplicationVersion_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcApplicationVersion_Values _original;
        
        public WcApplicationVersion_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcApplicationVersion_Values _current;
        
        public WcApplicationVersion_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcApplicationVersion_Values _concurrent;
        
        public WcApplicationVersion_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  ApVrsn_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  ApVrsn_Ap_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  AttachmentCount
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  AttachmentFileNames
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  DiscussionCount
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  DiscussionTitles
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  VersionNo
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  ApVrsn_MajorVersion
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  ApVrsn_MinorVersion
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  ApVrsn_VersionIteration
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  ApVrsn_Server
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  ApVrsn_DbName
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  ApVrsn_SolutionPath
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  ApVrsn_DefaultUIAssembly
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  ApVrsn_DefaultUIAssemblyPath
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  ApVrsn_DefaultWireTypePath
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  ApVrsn_WebServerURL
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  ApVrsn_WebsiteCodeFolderPath
                if (_current._r != _original._r)
                {
                    return true;
                }

                //  ApVrsn_WebserviceCodeFolderPath
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  ApVrsn_StoredProcCodeFolder
                if (_current._t != _original._t)
                {
                    return true;
                }

                //  ApVrsn_UseLegacyConnectionCode
                if (_current._u != _original._u)
                {
                    return true;
                }

                //  ApVrsn_DefaultConnectionCodeId
                if (_current._v != _original._v)
                {
                    return true;
                }

                //  ApVrsn_DefaultConnectionCodeId_TextField
                //if (_current._w != _original._w)
                //{
                //    return true;
                //}

                //  ApVrsn_IsMulticultural
                if (_current._x != _original._x)
                {
                    return true;
                }

                //  ApVrsn_Notes
                if (_current._y != _original._y)
                {
                    return true;
                }

                //  ApVrsn_IsActiveRow
                if (_current._z != _original._z)
                {
                    return true;
                }

                //  ApVrsn_IsDeleted
                if (_current._aa != _original._aa)
                {
                    return true;
                }

                //  ApVrsn_CreatedUserId
                if (_current._ab != _original._ab)
                {
                    return true;
                }

                //  ApVrsn_CreatedDate
                if (_current._ac != _original._ac)
                {
                    return true;
                }

                //  ApVrsn_UserId
                if (_current._ad != _original._ad)
                {
                    return true;
                }

                //  UserName
                if (_current._ae != _original._ae)
                {
                    return true;
                }

                //  ApVrsn_LastModifiedDate
                if (_current._af != _original._af)
                {
                    return true;
                }

                //  ApVrsn_Stamp
                if (_current._ag != _original._ag)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{34}{0}{33}_b{34}{1}{33}_c{34}{2}{33}_d{34}{3}{33}_e{34}{4}{33}_f{34}{5}{33}_g{34}{6}{33}_h{34}{7}{33}_i{34}{8}{33}_j{34}{9}{33}_k{34}{10}{33}_l{34}{11}{33}_m{34}{12}{33}_n{34}{13}{33}_o{34}{14}{33}_p{34}{15}{33}_q{34}{16}{33}_r{34}{17}{33}_s{34}{18}{33}_t{34}{19}{33}_u{34}{20}{33}_v{34}{21}{33}_w{34}{22}{33}_x{34}{23}{33}_y{34}{24}{33}_z{34}{25}{33}_aa{34}{26}{33}_ab{34}{27}{33}_ac{34}{28}{33}_ad{34}{29}{33}_ae{34}{30}{33}_af{34}{31}{33}_ag{34}{32}",
				new object[] {
				_current._a,		  //ApVrsn_Id
				_current._b,		  //ApVrsn_Ap_Id
				_current._c,		  //AttachmentCount
				_current._d,		  //AttachmentFileNames
				_current._e,		  //DiscussionCount
				_current._f,		  //DiscussionTitles
				_current._g,		  //VersionNo
				_current._h,		  //ApVrsn_MajorVersion
				_current._i,		  //ApVrsn_MinorVersion
				_current._j,		  //ApVrsn_VersionIteration
				_current._k,		  //ApVrsn_Server
				_current._l,		  //ApVrsn_DbName
				_current._m,		  //ApVrsn_SolutionPath
				_current._n,		  //ApVrsn_DefaultUIAssembly
				_current._o,		  //ApVrsn_DefaultUIAssemblyPath
				_current._p,		  //ApVrsn_DefaultWireTypePath
				_current._q,		  //ApVrsn_WebServerURL
				_current._r,		  //ApVrsn_WebsiteCodeFolderPath
				_current._s,		  //ApVrsn_WebserviceCodeFolderPath
				_current._t,		  //ApVrsn_StoredProcCodeFolder
				_current._u,		  //ApVrsn_UseLegacyConnectionCode
				_current._v,		  //ApVrsn_DefaultConnectionCodeId
				_current._w,		  //ApVrsn_DefaultConnectionCodeId_TextField
				_current._x,		  //ApVrsn_IsMulticultural
				_current._y,		  //ApVrsn_Notes
				_current._z,		  //ApVrsn_IsActiveRow
				_current._aa,		  //ApVrsn_IsDeleted
				_current._ab,		  //ApVrsn_CreatedUserId
				_current._ac,		  //ApVrsn_CreatedDate
				_current._ad,		  //ApVrsn_UserId
				_current._ae,		  //UserName
				_current._af,		  //ApVrsn_LastModifiedDate
				_current._ag,		  //ApVrsn_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    
    public class WcApplicationVersion_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcApplicationVersion_Values";
        private WcApplicationVersion_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcApplicationVersion_Values() 
        {
        }

        //public WcApplicationVersion_Values(object[] data, WcApplicationVersion_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcApplicationVersion_Values(object[] data, WcApplicationVersion_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  ApVrsn_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  ApVrsn_Ap_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  AttachmentCount
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  AttachmentFileNames
				_e = ObjectHelper.GetNullableIntFromObjectValue(data[4]);							//  DiscussionCount
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  DiscussionTitles
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  VersionNo
				_h = ObjectHelper.GetByteFromObjectValue(data[7]);										//  ApVrsn_MajorVersion
				_i = ObjectHelper.GetNullableByteFromObjectValue(data[8]);							//  ApVrsn_MinorVersion
				_j = ObjectHelper.GetNullableByteFromObjectValue(data[9]);							//  ApVrsn_VersionIteration
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  ApVrsn_Server
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  ApVrsn_DbName
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  ApVrsn_SolutionPath
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  ApVrsn_DefaultUIAssembly
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  ApVrsn_DefaultUIAssemblyPath
				_p = ObjectHelper.GetStringFromObjectValue(data[15]);									//  ApVrsn_DefaultWireTypePath
				_q = ObjectHelper.GetStringFromObjectValue(data[16]);									//  ApVrsn_WebServerURL
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  ApVrsn_WebsiteCodeFolderPath
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  ApVrsn_WebserviceCodeFolderPath
				_t = ObjectHelper.GetStringFromObjectValue(data[19]);									//  ApVrsn_StoredProcCodeFolder
				_u = ObjectHelper.GetBoolFromObjectValue(data[20]);									//  ApVrsn_UseLegacyConnectionCode
				_v = ObjectHelper.GetNullableGuidFromObjectValue(data[21]);						//  ApVrsn_DefaultConnectionCodeId
				_w = ObjectHelper.GetStringFromObjectValue(data[22]);									//  ApVrsn_DefaultConnectionCodeId_TextField
				_x = ObjectHelper.GetBoolFromObjectValue(data[23]);									//  ApVrsn_IsMulticultural
				_y = ObjectHelper.GetStringFromObjectValue(data[24]);									//  ApVrsn_Notes
				_z = ObjectHelper.GetBoolFromObjectValue(data[25]);									//  ApVrsn_IsActiveRow
				_aa = ObjectHelper.GetBoolFromObjectValue(data[26]);									//  ApVrsn_IsDeleted
				_ab = ObjectHelper.GetNullableGuidFromObjectValue(data[27]);						//  ApVrsn_CreatedUserId
				_ac = ObjectHelper.GetNullableDateTimeFromObjectValue(data[28]);					//  ApVrsn_CreatedDate
				_ad = ObjectHelper.GetNullableGuidFromObjectValue(data[29]);						//  ApVrsn_UserId
				_ae = ObjectHelper.GetStringFromObjectValue(data[30]);									//  UserName
				_af = ObjectHelper.GetNullableDateTimeFromObjectValue(data[31]);					//  ApVrsn_LastModifiedDate
				_ag = ObjectHelper.GetByteArrayFromObjectValue(data[32]);						//  ApVrsn_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplicationVersion", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  ApVrsn_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  ApVrsn_Ap_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  AttachmentCount
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  AttachmentFileNames
				_e = ObjectHelper.GetNullableIntFromObjectValue(data[4]);							//  DiscussionCount
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  DiscussionTitles
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  VersionNo
				_h = ObjectHelper.GetByteFromObjectValue(data[7]);										//  ApVrsn_MajorVersion
				_i = ObjectHelper.GetNullableByteFromObjectValue(data[8]);							//  ApVrsn_MinorVersion
				_j = ObjectHelper.GetNullableByteFromObjectValue(data[9]);							//  ApVrsn_VersionIteration
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  ApVrsn_Server
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  ApVrsn_DbName
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  ApVrsn_SolutionPath
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  ApVrsn_DefaultUIAssembly
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  ApVrsn_DefaultUIAssemblyPath
				_p = ObjectHelper.GetStringFromObjectValue(data[15]);									//  ApVrsn_DefaultWireTypePath
				_q = ObjectHelper.GetStringFromObjectValue(data[16]);									//  ApVrsn_WebServerURL
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  ApVrsn_WebsiteCodeFolderPath
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  ApVrsn_WebserviceCodeFolderPath
				_t = ObjectHelper.GetStringFromObjectValue(data[19]);									//  ApVrsn_StoredProcCodeFolder
				_u = ObjectHelper.GetBoolFromObjectValue(data[20]);									//  ApVrsn_UseLegacyConnectionCode
				_v = ObjectHelper.GetNullableGuidFromObjectValue(data[21]);						//  ApVrsn_DefaultConnectionCodeId
				_w = ObjectHelper.GetStringFromObjectValue(data[22]);									//  ApVrsn_DefaultConnectionCodeId_TextField
				_x = ObjectHelper.GetBoolFromObjectValue(data[23]);									//  ApVrsn_IsMulticultural
				_y = ObjectHelper.GetStringFromObjectValue(data[24]);									//  ApVrsn_Notes
				_z = ObjectHelper.GetBoolFromObjectValue(data[25]);									//  ApVrsn_IsActiveRow
				_aa = ObjectHelper.GetBoolFromObjectValue(data[26]);									//  ApVrsn_IsDeleted
				_ab = ObjectHelper.GetNullableGuidFromObjectValue(data[27]);						//  ApVrsn_CreatedUserId
				_ac = ObjectHelper.GetNullableDateTimeFromObjectValue(data[28]);					//  ApVrsn_CreatedDate
				_ad = ObjectHelper.GetNullableGuidFromObjectValue(data[29]);						//  ApVrsn_UserId
				_ae = ObjectHelper.GetStringFromObjectValue(data[30]);									//  UserName
				_af = ObjectHelper.GetNullableDateTimeFromObjectValue(data[31]);					//  ApVrsn_LastModifiedDate
				_ag = ObjectHelper.GetByteArrayFromObjectValue(data[32]);						//  ApVrsn_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplicationVersion", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplicationVersion", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		
		public Guid _a;			//  ApVrsn_Id

		
		public Guid? _b;			//  ApVrsn_Ap_Id

		
		public Int32? _c;			//  AttachmentCount

		
		public String _d;			//  AttachmentFileNames

		
		public Int32? _e;			//  DiscussionCount

		
		public String _f;			//  DiscussionTitles

		
		public String _g;			//  VersionNo

		
		public Byte _h;			//  ApVrsn_MajorVersion

		
		public Byte? _i;			//  ApVrsn_MinorVersion

		
		public Byte? _j;			//  ApVrsn_VersionIteration

		
		public String _k;			//  ApVrsn_Server

		
		public String _l;			//  ApVrsn_DbName

		
		public String _m;			//  ApVrsn_SolutionPath

		
		public String _n;			//  ApVrsn_DefaultUIAssembly

		
		public String _o;			//  ApVrsn_DefaultUIAssemblyPath

		
		public String _p;			//  ApVrsn_DefaultWireTypePath

		
		public String _q;			//  ApVrsn_WebServerURL

		
		public String _r;			//  ApVrsn_WebsiteCodeFolderPath

		
		public String _s;			//  ApVrsn_WebserviceCodeFolderPath

		
		public String _t;			//  ApVrsn_StoredProcCodeFolder

		
		public Boolean _u;			//  ApVrsn_UseLegacyConnectionCode

		
		public Guid? _v;			//  ApVrsn_DefaultConnectionCodeId

		
		public String _w;			//  ApVrsn_DefaultConnectionCodeId_TextField

		
		public Boolean _x;			//  ApVrsn_IsMulticultural

		
		public String _y;			//  ApVrsn_Notes

		
		public Boolean _z;			//  ApVrsn_IsActiveRow

		
		public Boolean _aa;			//  ApVrsn_IsDeleted

		
		public Guid? _ab;			//  ApVrsn_CreatedUserId

		
		public DateTime? _ac;			//  ApVrsn_CreatedDate

		
		public Guid? _ad;			//  ApVrsn_UserId

		
		public String _ae;			//  UserName

		
		public DateTime? _af;			//  ApVrsn_LastModifiedDate

		
		public Byte[] _ag;			//  ApVrsn_Stamp

		
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcApplicationVersion_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 33; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                    _list[18] = _s;
                    _list[19] = _t;
                    _list[20] = _u;
                    _list[21] = _v;
                    _list[22] = _w;
                    _list[23] = _x;
                    _list[24] = _y;
                    _list[25] = _z;
                    _list[26] = _aa;
                    _list[27] = _ab;
                    _list[28] = _ac;
                    _list[29] = _ad;
                    _list[30] = _ae;
                    _list[31] = _af;
                    _list[32] = _ag;
                }
                return _list;
            }
        }

        public Guid ApVrsn_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid ApVrsn_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? ApVrsn_Ap_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApVrsn_Ap_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Int32? AttachmentCount
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? AttachmentCount_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String AttachmentFileNames
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String AttachmentFileNames_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public Int32? DiscussionCount
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? DiscussionCount_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public String DiscussionTitles
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DiscussionTitles_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public String VersionNo
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String VersionNo_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Byte ApVrsn_MajorVersion
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte ApVrsn_MajorVersion_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Byte? ApVrsn_MinorVersion
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte? ApVrsn_MinorVersion_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Byte? ApVrsn_VersionIteration
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte? ApVrsn_VersionIteration_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public String ApVrsn_Server
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_Server_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public String ApVrsn_DbName
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DbName_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public String ApVrsn_SolutionPath
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_SolutionPath_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public String ApVrsn_DefaultUIAssembly
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DefaultUIAssembly_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public String ApVrsn_DefaultUIAssemblyPath
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DefaultUIAssemblyPath_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public String ApVrsn_DefaultWireTypePath
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DefaultWireTypePath_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public String ApVrsn_WebServerURL
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_WebServerURL_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public String ApVrsn_WebsiteCodeFolderPath
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_WebsiteCodeFolderPath_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        public String ApVrsn_WebserviceCodeFolderPath
        {
            get { return _s; }
            set
            {
                _s = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_WebserviceCodeFolderPath_noevents
        {
            get { return _s; }
            set
            {
                _s = value;
            }
        }

        public String ApVrsn_StoredProcCodeFolder
        {
            get { return _t; }
            set
            {
                _t = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_StoredProcCodeFolder_noevents
        {
            get { return _t; }
            set
            {
                _t = value;
            }
        }

        public Boolean ApVrsn_UseLegacyConnectionCode
        {
            get { return _u; }
            set
            {
                _u = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean ApVrsn_UseLegacyConnectionCode_noevents
        {
            get { return _u; }
            set
            {
                _u = value;
            }
        }

        public Guid? ApVrsn_DefaultConnectionCodeId
        {
            get { return _v; }
            set
            {
                _v = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApVrsn_DefaultConnectionCodeId_noevents
        {
            get { return _v; }
            set
            {
                _v = value;
            }
        }

        public String ApVrsn_DefaultConnectionCodeId_TextField
        {
            get { return _w; }
            set
            {
                _w = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DefaultConnectionCodeId_TextField_noevents
        {
            get { return _w; }
            set
            {
                _w = value;
            }
        }

        public Boolean ApVrsn_IsMulticultural
        {
            get { return _x; }
            set
            {
                _x = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean ApVrsn_IsMulticultural_noevents
        {
            get { return _x; }
            set
            {
                _x = value;
            }
        }

        public String ApVrsn_Notes
        {
            get { return _y; }
            set
            {
                _y = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_Notes_noevents
        {
            get { return _y; }
            set
            {
                _y = value;
            }
        }

        public Boolean ApVrsn_IsActiveRow
        {
            get { return _z; }
            set
            {
                _z = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean ApVrsn_IsActiveRow_noevents
        {
            get { return _z; }
            set
            {
                _z = value;
            }
        }

        public Boolean ApVrsn_IsDeleted
        {
            get { return _aa; }
            set
            {
                _aa = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean ApVrsn_IsDeleted_noevents
        {
            get { return _aa; }
            set
            {
                _aa = value;
            }
        }

        public Guid? ApVrsn_CreatedUserId
        {
            get { return _ab; }
            set
            {
                _ab = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApVrsn_CreatedUserId_noevents
        {
            get { return _ab; }
            set
            {
                _ab = value;
            }
        }

        public DateTime? ApVrsn_CreatedDate
        {
            get { return _ac; }
            set
            {
                _ac = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? ApVrsn_CreatedDate_noevents
        {
            get { return _ac; }
            set
            {
                _ac = value;
            }
        }

        public Guid? ApVrsn_UserId
        {
            get { return _ad; }
            set
            {
                _ad = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApVrsn_UserId_noevents
        {
            get { return _ad; }
            set
            {
                _ad = value;
            }
        }

        public String UserName
        {
            get { return _ae; }
            set
            {
                _ae = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _ae; }
            set
            {
                _ae = value;
            }
        }

        public DateTime? ApVrsn_LastModifiedDate
        {
            get { return _af; }
            set
            {
                _af = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? ApVrsn_LastModifiedDate_noevents
        {
            get { return _af; }
            set
            {
                _af = value;
            }
        }

        public Byte[] ApVrsn_Stamp
        {
            get { return _ag; }
            set
            {
                _ag = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] ApVrsn_Stamp_noevents
        {
            get { return _ag; }
            set
            {
                _ag = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r,
				_s,
				_t,
				_u,
				_v,
				_w,
				_x,
				_y,
				_z,
				_aa,
				_ab,
				_ac,
				_ad,
				_ae,
				_af,
				_ag
			};
        }

        public WcApplicationVersion_Values Clone()
        {
            return new WcApplicationVersion_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


