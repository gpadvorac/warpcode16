using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;


// Gen Timestamp:  12/14/2017 10:03:14 PM

namespace EntityWireType
{

    #region Entity Values Manager
    
    public class WcStoredProcGroup_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProcGroup_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcStoredProcGroup_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcStoredProcGroup_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcStoredProcGroup_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcGroup_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcStoredProcGroup_Values(currentData, this) : new WcStoredProcGroup_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcStoredProcGroup_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcGroup_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcGroup_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcStoredProcGroup_Values _original;
        
        public WcStoredProcGroup_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcStoredProcGroup_Values _current;
        
        public WcStoredProcGroup_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcStoredProcGroup_Values _concurrent;
        
        public WcStoredProcGroup_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  SpGrp_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  SpGrp_ApVrsn_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  SpGrp_SortOrder
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  SpGrp_Name
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  SpGrp_Desc
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  SpGrp_IsActiveRow
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  SpGrp_IsDeleted
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  SpGrp_CreatedUserId
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  SpGrp_CreatedDate
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  SpGrp_UserId
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  UserName
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  SpGrp_LastModifiedDate
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  SpGrp_Stamp
                if (_current._m != _original._m)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{14}{0}{13}_b{14}{1}{13}_c{14}{2}{13}_d{14}{3}{13}_e{14}{4}{13}_f{14}{5}{13}_g{14}{6}{13}_h{14}{7}{13}_i{14}{8}{13}_j{14}{9}{13}_k{14}{10}{13}_l{14}{11}{13}_m{14}{12}",
				new object[] {
				_current._a,		  //SpGrp_Id
				_current._b,		  //SpGrp_ApVrsn_Id
				_current._c,		  //SpGrp_SortOrder
				_current._d,		  //SpGrp_Name
				_current._e,		  //SpGrp_Desc
				_current._f,		  //SpGrp_IsActiveRow
				_current._g,		  //SpGrp_IsDeleted
				_current._h,		  //SpGrp_CreatedUserId
				_current._i,		  //SpGrp_CreatedDate
				_current._j,		  //SpGrp_UserId
				_current._k,		  //UserName
				_current._l,		  //SpGrp_LastModifiedDate
				_current._m,		  //SpGrp_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    
    public class WcStoredProcGroup_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProcGroup_Values";
        private WcStoredProcGroup_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcStoredProcGroup_Values() 
        {
        }

        //public WcStoredProcGroup_Values(object[] data, WcStoredProcGroup_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcStoredProcGroup_Values(object[] data, WcStoredProcGroup_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcGroup_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  SpGrp_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  SpGrp_ApVrsn_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  SpGrp_SortOrder
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  SpGrp_Name
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  SpGrp_Desc
				_f = ObjectHelper.GetNullableBoolFromObjectValue(data[5]);					//  SpGrp_IsActiveRow
				_g = ObjectHelper.GetNullableBoolFromObjectValue(data[6]);					//  SpGrp_IsDeleted
				_h = ObjectHelper.GetNullableGuidFromObjectValue(data[7]);						//  SpGrp_CreatedUserId
				_i = ObjectHelper.GetNullableDateTimeFromObjectValue(data[8]);					//  SpGrp_CreatedDate
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  SpGrp_UserId
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  UserName
				_l = ObjectHelper.GetNullableDateTimeFromObjectValue(data[11]);					//  SpGrp_LastModifiedDate
				_m = data[12] as Byte[];						//  SpGrp_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcGroup_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcGroup_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProcGroup", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  SpGrp_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  SpGrp_ApVrsn_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  SpGrp_SortOrder
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  SpGrp_Name
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  SpGrp_Desc
				_f = ObjectHelper.GetNullableBoolFromObjectValue(data[5]);					//  SpGrp_IsActiveRow
				_g = ObjectHelper.GetNullableBoolFromObjectValue(data[6]);					//  SpGrp_IsDeleted
				_h = ObjectHelper.GetNullableGuidFromObjectValue(data[7]);						//  SpGrp_CreatedUserId
				_i = ObjectHelper.GetNullableDateTimeFromObjectValue(data[8]);					//  SpGrp_CreatedDate
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  SpGrp_UserId
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  UserName
				_l = ObjectHelper.GetNullableDateTimeFromObjectValue(data[11]);					//  SpGrp_LastModifiedDate
				_m = data[12] as Byte[];						//  SpGrp_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProcGroup", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProcGroup", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		
		public Guid _a;			//  SpGrp_Id

		
		public Guid? _b;			//  SpGrp_ApVrsn_Id

		
		public Int32? _c;			//  SpGrp_SortOrder

		
		public String _d;			//  SpGrp_Name

		
		public String _e;			//  SpGrp_Desc

		
		public Boolean? _f;			//  SpGrp_IsActiveRow

		
		public Boolean? _g;			//  SpGrp_IsDeleted

		
		public Guid? _h;			//  SpGrp_CreatedUserId

		
		public DateTime? _i;			//  SpGrp_CreatedDate

		
		public Guid? _j;			//  SpGrp_UserId

		
		public String _k;			//  UserName

		
		public DateTime? _l;			//  SpGrp_LastModifiedDate

		
		public Byte[] _m;			//  SpGrp_Stamp

		
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcStoredProcGroup_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 13; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                }
                return _list;
            }
        }

        public Guid SpGrp_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid SpGrp_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? SpGrp_ApVrsn_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? SpGrp_ApVrsn_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Int32? SpGrp_SortOrder
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? SpGrp_SortOrder_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String SpGrp_Name
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String SpGrp_Name_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String SpGrp_Desc
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String SpGrp_Desc_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Boolean? SpGrp_IsActiveRow
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? SpGrp_IsActiveRow_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Boolean? SpGrp_IsDeleted
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? SpGrp_IsDeleted_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Guid? SpGrp_CreatedUserId
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? SpGrp_CreatedUserId_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public DateTime? SpGrp_CreatedDate
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? SpGrp_CreatedDate_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Guid? SpGrp_UserId
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? SpGrp_UserId_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public String UserName
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public DateTime? SpGrp_LastModifiedDate
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? SpGrp_LastModifiedDate_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public Byte[] SpGrp_Stamp
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] SpGrp_Stamp_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m
			};
        }

        public WcStoredProcGroup_Values Clone()
        {
            return new WcStoredProcGroup_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


