using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;


// Gen Timestamp:  1/11/2018 6:32:38 PM

namespace EntityWireType
{

    #region Entity Values Manager
    
    public class WcApplicationVersion_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcApplicationVersion_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcApplicationVersion_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcApplicationVersion_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcApplicationVersion_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcApplicationVersion_Values(currentData, this) : new WcApplicationVersion_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcApplicationVersion_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcApplicationVersion_Values _original;
        
        public WcApplicationVersion_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcApplicationVersion_Values _current;
        
        public WcApplicationVersion_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcApplicationVersion_Values _concurrent;
        
        public WcApplicationVersion_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  ApVrsn_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  ApVrsn_Ap_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  AttachmentCount
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  AttachmentFileNames
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  DiscussionCount
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  DiscussionTitles
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  VersionNo
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  ApVrsn_MajorVersion
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  ApVrsn_MinorVersion
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  ApVrsn_VersionIteration
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  ApVrsn_DbName
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  ApVrsn_Server
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  ApVrsn_UseLegacyConnectionCode
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  ApVrsn_SolutionPath
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  ApVrsn_DefaultUIAssembly
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  ApVrsn_DefaultUINamespace
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  ApVrsn_DefaultUIAssemblyPath
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  ApVrsn_ProxyAssemblyName
                if (_current._r != _original._r)
                {
                    return true;
                }

                //  ApVrsn_ProxyNamespace
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  ApVrsn_ProxyAssemblyPath
                if (_current._t != _original._t)
                {
                    return true;
                }

                //  ApVrsn_DefaultWireTypeAssembly
                if (_current._u != _original._u)
                {
                    return true;
                }

                //  ApVrsn_DefaultWireTypeNamespace
                if (_current._v != _original._v)
                {
                    return true;
                }

                //  ApVrsn_DefaultWireTypePath
                if (_current._w != _original._w)
                {
                    return true;
                }

                //  ApVrsn_WebServerURL
                if (_current._x != _original._x)
                {
                    return true;
                }

                //  ApVrsn_WebsiteCodeFolderPath
                if (_current._y != _original._y)
                {
                    return true;
                }

                //  ApVrsn_WebserviceCodeFolderPath
                if (_current._z != _original._z)
                {
                    return true;
                }

                //  ApVrsn_StoredProcCodeFolder
                if (_current._aa != _original._aa)
                {
                    return true;
                }

                //  ApVrsn_DataServiceAssemblyName
                if (_current._ab != _original._ab)
                {
                    return true;
                }

                //  ApVrsn_DataServiceNamespace
                if (_current._ac != _original._ac)
                {
                    return true;
                }

                //  ApVrsn_DataServicePath
                if (_current._ad != _original._ad)
                {
                    return true;
                }

                //  ApVrsn_IsMulticultural
                if (_current._ae != _original._ae)
                {
                    return true;
                }

                //  ApVrsn_Notes
                if (_current._af != _original._af)
                {
                    return true;
                }

                //  ApVrsn_IsActiveRow
                if (_current._ag != _original._ag)
                {
                    return true;
                }

                //  ApVrsn_IsDeleted
                if (_current._ah != _original._ah)
                {
                    return true;
                }

                //  ApVrsn_CreatedUserId
                if (_current._ai != _original._ai)
                {
                    return true;
                }

                //  ApVrsn_CreatedDate
                if (_current._aj != _original._aj)
                {
                    return true;
                }

                //  ApVrsn_UserId
                if (_current._ak != _original._ak)
                {
                    return true;
                }

                //  UserName
                if (_current._al != _original._al)
                {
                    return true;
                }

                //  ApVrsn_LastModifiedDate
                if (_current._am != _original._am)
                {
                    return true;
                }

                //  ApVrsn_Stamp
                if (_current._an != _original._an)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{41}{0}{40}_b{41}{1}{40}_c{41}{2}{40}_d{41}{3}{40}_e{41}{4}{40}_f{41}{5}{40}_g{41}{6}{40}_h{41}{7}{40}_i{41}{8}{40}_j{41}{9}{40}_k{41}{10}{40}_l{41}{11}{40}_m{41}{12}{40}_n{41}{13}{40}_o{41}{14}{40}_p{41}{15}{40}_q{41}{16}{40}_r{41}{17}{40}_s{41}{18}{40}_t{41}{19}{40}_u{41}{20}{40}_v{41}{21}{40}_w{41}{22}{40}_x{41}{23}{40}_y{41}{24}{40}_z{41}{25}{40}_aa{41}{26}{40}_ab{41}{27}{40}_ac{41}{28}{40}_ad{41}{29}{40}_ae{41}{30}{40}_af{41}{31}{40}_ag{41}{32}{40}_ah{41}{33}{40}_ai{41}{34}{40}_aj{41}{35}{40}_ak{41}{36}{40}_al{41}{37}{40}_am{41}{38}{40}_an{41}{39}",
				new object[] {
				_current._a,		  //ApVrsn_Id
				_current._b,		  //ApVrsn_Ap_Id
				_current._c,		  //AttachmentCount
				_current._d,		  //AttachmentFileNames
				_current._e,		  //DiscussionCount
				_current._f,		  //DiscussionTitles
				_current._g,		  //VersionNo
				_current._h,		  //ApVrsn_MajorVersion
				_current._i,		  //ApVrsn_MinorVersion
				_current._j,		  //ApVrsn_VersionIteration
				_current._k,		  //ApVrsn_DbName
				_current._l,		  //ApVrsn_Server
				_current._m,		  //ApVrsn_UseLegacyConnectionCode
				_current._n,		  //ApVrsn_SolutionPath
				_current._o,		  //ApVrsn_DefaultUIAssembly
				_current._p,		  //ApVrsn_DefaultUINamespace
				_current._q,		  //ApVrsn_DefaultUIAssemblyPath
				_current._r,		  //ApVrsn_ProxyAssemblyName
				_current._s,		  //ApVrsn_ProxyNamespace
				_current._t,		  //ApVrsn_ProxyAssemblyPath
				_current._u,		  //ApVrsn_DefaultWireTypeAssembly
				_current._v,		  //ApVrsn_DefaultWireTypeNamespace
				_current._w,		  //ApVrsn_DefaultWireTypePath
				_current._x,		  //ApVrsn_WebServerURL
				_current._y,		  //ApVrsn_WebsiteCodeFolderPath
				_current._z,		  //ApVrsn_WebserviceCodeFolderPath
				_current._aa,		  //ApVrsn_StoredProcCodeFolder
				_current._ab,		  //ApVrsn_DataServiceAssemblyName
				_current._ac,		  //ApVrsn_DataServiceNamespace
				_current._ad,		  //ApVrsn_DataServicePath
				_current._ae,		  //ApVrsn_IsMulticultural
				_current._af,		  //ApVrsn_Notes
				_current._ag,		  //ApVrsn_IsActiveRow
				_current._ah,		  //ApVrsn_IsDeleted
				_current._ai,		  //ApVrsn_CreatedUserId
				_current._aj,		  //ApVrsn_CreatedDate
				_current._ak,		  //ApVrsn_UserId
				_current._al,		  //UserName
				_current._am,		  //ApVrsn_LastModifiedDate
				_current._an,		  //ApVrsn_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    
    public class WcApplicationVersion_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcApplicationVersion_Values";
        private WcApplicationVersion_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcApplicationVersion_Values() 
        {
        }

        //public WcApplicationVersion_Values(object[] data, WcApplicationVersion_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcApplicationVersion_Values(object[] data, WcApplicationVersion_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  ApVrsn_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  ApVrsn_Ap_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  AttachmentCount
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  AttachmentFileNames
				_e = ObjectHelper.GetNullableIntFromObjectValue(data[4]);							//  DiscussionCount
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  DiscussionTitles
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  VersionNo
				_h = ObjectHelper.GetByteFromObjectValue(data[7]);										//  ApVrsn_MajorVersion
				_i = ObjectHelper.GetNullableByteFromObjectValue(data[8]);							//  ApVrsn_MinorVersion
				_j = ObjectHelper.GetNullableByteFromObjectValue(data[9]);							//  ApVrsn_VersionIteration
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  ApVrsn_DbName
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  ApVrsn_Server
				_m = ObjectHelper.GetBoolFromObjectValue(data[12]);									//  ApVrsn_UseLegacyConnectionCode
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  ApVrsn_SolutionPath
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  ApVrsn_DefaultUIAssembly
				_p = ObjectHelper.GetStringFromObjectValue(data[15]);									//  ApVrsn_DefaultUINamespace
				_q = ObjectHelper.GetStringFromObjectValue(data[16]);									//  ApVrsn_DefaultUIAssemblyPath
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  ApVrsn_ProxyAssemblyName
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  ApVrsn_ProxyNamespace
				_t = ObjectHelper.GetStringFromObjectValue(data[19]);									//  ApVrsn_ProxyAssemblyPath
				_u = ObjectHelper.GetStringFromObjectValue(data[20]);									//  ApVrsn_DefaultWireTypeAssembly
				_v = ObjectHelper.GetStringFromObjectValue(data[21]);									//  ApVrsn_DefaultWireTypeNamespace
				_w = ObjectHelper.GetStringFromObjectValue(data[22]);									//  ApVrsn_DefaultWireTypePath
				_x = ObjectHelper.GetStringFromObjectValue(data[23]);									//  ApVrsn_WebServerURL
				_y = ObjectHelper.GetStringFromObjectValue(data[24]);									//  ApVrsn_WebsiteCodeFolderPath
				_z = ObjectHelper.GetStringFromObjectValue(data[25]);									//  ApVrsn_WebserviceCodeFolderPath
				_aa = ObjectHelper.GetStringFromObjectValue(data[26]);									//  ApVrsn_StoredProcCodeFolder
				_ab = ObjectHelper.GetStringFromObjectValue(data[27]);									//  ApVrsn_DataServiceAssemblyName
				_ac = ObjectHelper.GetStringFromObjectValue(data[28]);									//  ApVrsn_DataServiceNamespace
				_ad = ObjectHelper.GetStringFromObjectValue(data[29]);									//  ApVrsn_DataServicePath
				_ae = ObjectHelper.GetBoolFromObjectValue(data[30]);									//  ApVrsn_IsMulticultural
				_af = ObjectHelper.GetStringFromObjectValue(data[31]);									//  ApVrsn_Notes
				_ag = ObjectHelper.GetBoolFromObjectValue(data[32]);									//  ApVrsn_IsActiveRow
				_ah = ObjectHelper.GetBoolFromObjectValue(data[33]);									//  ApVrsn_IsDeleted
				_ai = ObjectHelper.GetNullableGuidFromObjectValue(data[34]);						//  ApVrsn_CreatedUserId
				_aj = ObjectHelper.GetNullableDateTimeFromObjectValue(data[35]);					//  ApVrsn_CreatedDate
				_ak = ObjectHelper.GetNullableGuidFromObjectValue(data[36]);						//  ApVrsn_UserId
				_al = ObjectHelper.GetStringFromObjectValue(data[37]);									//  UserName
				_am = ObjectHelper.GetNullableDateTimeFromObjectValue(data[38]);					//  ApVrsn_LastModifiedDate
				_an = data[39] as Byte[];						//  ApVrsn_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationVersion_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplicationVersion", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  ApVrsn_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  ApVrsn_Ap_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  AttachmentCount
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  AttachmentFileNames
				_e = ObjectHelper.GetNullableIntFromObjectValue(data[4]);							//  DiscussionCount
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  DiscussionTitles
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  VersionNo
				_h = ObjectHelper.GetByteFromObjectValue(data[7]);										//  ApVrsn_MajorVersion
				_i = ObjectHelper.GetNullableByteFromObjectValue(data[8]);							//  ApVrsn_MinorVersion
				_j = ObjectHelper.GetNullableByteFromObjectValue(data[9]);							//  ApVrsn_VersionIteration
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  ApVrsn_DbName
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  ApVrsn_Server
				_m = ObjectHelper.GetBoolFromObjectValue(data[12]);									//  ApVrsn_UseLegacyConnectionCode
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  ApVrsn_SolutionPath
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  ApVrsn_DefaultUIAssembly
				_p = ObjectHelper.GetStringFromObjectValue(data[15]);									//  ApVrsn_DefaultUINamespace
				_q = ObjectHelper.GetStringFromObjectValue(data[16]);									//  ApVrsn_DefaultUIAssemblyPath
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  ApVrsn_ProxyAssemblyName
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  ApVrsn_ProxyNamespace
				_t = ObjectHelper.GetStringFromObjectValue(data[19]);									//  ApVrsn_ProxyAssemblyPath
				_u = ObjectHelper.GetStringFromObjectValue(data[20]);									//  ApVrsn_DefaultWireTypeAssembly
				_v = ObjectHelper.GetStringFromObjectValue(data[21]);									//  ApVrsn_DefaultWireTypeNamespace
				_w = ObjectHelper.GetStringFromObjectValue(data[22]);									//  ApVrsn_DefaultWireTypePath
				_x = ObjectHelper.GetStringFromObjectValue(data[23]);									//  ApVrsn_WebServerURL
				_y = ObjectHelper.GetStringFromObjectValue(data[24]);									//  ApVrsn_WebsiteCodeFolderPath
				_z = ObjectHelper.GetStringFromObjectValue(data[25]);									//  ApVrsn_WebserviceCodeFolderPath
				_aa = ObjectHelper.GetStringFromObjectValue(data[26]);									//  ApVrsn_StoredProcCodeFolder
				_ab = ObjectHelper.GetStringFromObjectValue(data[27]);									//  ApVrsn_DataServiceAssemblyName
				_ac = ObjectHelper.GetStringFromObjectValue(data[28]);									//  ApVrsn_DataServiceNamespace
				_ad = ObjectHelper.GetStringFromObjectValue(data[29]);									//  ApVrsn_DataServicePath
				_ae = ObjectHelper.GetBoolFromObjectValue(data[30]);									//  ApVrsn_IsMulticultural
				_af = ObjectHelper.GetStringFromObjectValue(data[31]);									//  ApVrsn_Notes
				_ag = ObjectHelper.GetBoolFromObjectValue(data[32]);									//  ApVrsn_IsActiveRow
				_ah = ObjectHelper.GetBoolFromObjectValue(data[33]);									//  ApVrsn_IsDeleted
				_ai = ObjectHelper.GetNullableGuidFromObjectValue(data[34]);						//  ApVrsn_CreatedUserId
				_aj = ObjectHelper.GetNullableDateTimeFromObjectValue(data[35]);					//  ApVrsn_CreatedDate
				_ak = ObjectHelper.GetNullableGuidFromObjectValue(data[36]);						//  ApVrsn_UserId
				_al = ObjectHelper.GetStringFromObjectValue(data[37]);									//  UserName
				_am = ObjectHelper.GetNullableDateTimeFromObjectValue(data[38]);					//  ApVrsn_LastModifiedDate
				_an = data[39] as Byte[];						//  ApVrsn_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplicationVersion", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplicationVersion", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		
		public Guid _a;			//  ApVrsn_Id

		
		public Guid? _b;			//  ApVrsn_Ap_Id

		
		public Int32? _c;			//  AttachmentCount

		
		public String _d;			//  AttachmentFileNames

		
		public Int32? _e;			//  DiscussionCount

		
		public String _f;			//  DiscussionTitles

		
		public String _g;			//  VersionNo

		
		public Byte _h;			//  ApVrsn_MajorVersion

		
		public Byte? _i;			//  ApVrsn_MinorVersion

		
		public Byte? _j;			//  ApVrsn_VersionIteration

		
		public String _k;			//  ApVrsn_DbName

		
		public String _l;			//  ApVrsn_Server

		
		public Boolean _m;			//  ApVrsn_UseLegacyConnectionCode

		
		public String _n;			//  ApVrsn_SolutionPath

		
		public String _o;			//  ApVrsn_DefaultUIAssembly

		
		public String _p;			//  ApVrsn_DefaultUINamespace

		
		public String _q;			//  ApVrsn_DefaultUIAssemblyPath

		
		public String _r;			//  ApVrsn_ProxyAssemblyName

		
		public String _s;			//  ApVrsn_ProxyNamespace

		
		public String _t;			//  ApVrsn_ProxyAssemblyPath

		
		public String _u;			//  ApVrsn_DefaultWireTypeAssembly

		
		public String _v;			//  ApVrsn_DefaultWireTypeNamespace

		
		public String _w;			//  ApVrsn_DefaultWireTypePath

		
		public String _x;			//  ApVrsn_WebServerURL

		
		public String _y;			//  ApVrsn_WebsiteCodeFolderPath

		
		public String _z;			//  ApVrsn_WebserviceCodeFolderPath

		
		public String _aa;			//  ApVrsn_StoredProcCodeFolder

		
		public String _ab;			//  ApVrsn_DataServiceAssemblyName

		
		public String _ac;			//  ApVrsn_DataServiceNamespace

		
		public String _ad;			//  ApVrsn_DataServicePath

		
		public Boolean _ae;			//  ApVrsn_IsMulticultural

		
		public String _af;			//  ApVrsn_Notes

		
		public Boolean _ag;			//  ApVrsn_IsActiveRow

		
		public Boolean _ah;			//  ApVrsn_IsDeleted

		
		public Guid? _ai;			//  ApVrsn_CreatedUserId

		
		public DateTime? _aj;			//  ApVrsn_CreatedDate

		
		public Guid? _ak;			//  ApVrsn_UserId

		
		public String _al;			//  UserName

		
		public DateTime? _am;			//  ApVrsn_LastModifiedDate

		
		public Byte[] _an;			//  ApVrsn_Stamp

		
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcApplicationVersion_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 40; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                    _list[18] = _s;
                    _list[19] = _t;
                    _list[20] = _u;
                    _list[21] = _v;
                    _list[22] = _w;
                    _list[23] = _x;
                    _list[24] = _y;
                    _list[25] = _z;
                    _list[26] = _aa;
                    _list[27] = _ab;
                    _list[28] = _ac;
                    _list[29] = _ad;
                    _list[30] = _ae;
                    _list[31] = _af;
                    _list[32] = _ag;
                    _list[33] = _ah;
                    _list[34] = _ai;
                    _list[35] = _aj;
                    _list[36] = _ak;
                    _list[37] = _al;
                    _list[38] = _am;
                    _list[39] = _an;
                }
                return _list;
            }
        }

        public Guid ApVrsn_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid ApVrsn_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? ApVrsn_Ap_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApVrsn_Ap_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Int32? AttachmentCount
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? AttachmentCount_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String AttachmentFileNames
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String AttachmentFileNames_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public Int32? DiscussionCount
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? DiscussionCount_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public String DiscussionTitles
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DiscussionTitles_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public String VersionNo
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String VersionNo_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Byte ApVrsn_MajorVersion
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte ApVrsn_MajorVersion_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Byte? ApVrsn_MinorVersion
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte? ApVrsn_MinorVersion_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Byte? ApVrsn_VersionIteration
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte? ApVrsn_VersionIteration_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public String ApVrsn_DbName
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DbName_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public String ApVrsn_Server
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_Server_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public Boolean ApVrsn_UseLegacyConnectionCode
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean ApVrsn_UseLegacyConnectionCode_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public String ApVrsn_SolutionPath
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_SolutionPath_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public String ApVrsn_DefaultUIAssembly
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DefaultUIAssembly_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public String ApVrsn_DefaultUINamespace
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DefaultUINamespace_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public String ApVrsn_DefaultUIAssemblyPath
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DefaultUIAssemblyPath_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public String ApVrsn_ProxyAssemblyName
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_ProxyAssemblyName_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        public String ApVrsn_ProxyNamespace
        {
            get { return _s; }
            set
            {
                _s = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_ProxyNamespace_noevents
        {
            get { return _s; }
            set
            {
                _s = value;
            }
        }

        public String ApVrsn_ProxyAssemblyPath
        {
            get { return _t; }
            set
            {
                _t = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_ProxyAssemblyPath_noevents
        {
            get { return _t; }
            set
            {
                _t = value;
            }
        }

        public String ApVrsn_DefaultWireTypeAssembly
        {
            get { return _u; }
            set
            {
                _u = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DefaultWireTypeAssembly_noevents
        {
            get { return _u; }
            set
            {
                _u = value;
            }
        }

        public String ApVrsn_DefaultWireTypeNamespace
        {
            get { return _v; }
            set
            {
                _v = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DefaultWireTypeNamespace_noevents
        {
            get { return _v; }
            set
            {
                _v = value;
            }
        }

        public String ApVrsn_DefaultWireTypePath
        {
            get { return _w; }
            set
            {
                _w = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DefaultWireTypePath_noevents
        {
            get { return _w; }
            set
            {
                _w = value;
            }
        }

        public String ApVrsn_WebServerURL
        {
            get { return _x; }
            set
            {
                _x = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_WebServerURL_noevents
        {
            get { return _x; }
            set
            {
                _x = value;
            }
        }

        public String ApVrsn_WebsiteCodeFolderPath
        {
            get { return _y; }
            set
            {
                _y = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_WebsiteCodeFolderPath_noevents
        {
            get { return _y; }
            set
            {
                _y = value;
            }
        }

        public String ApVrsn_WebserviceCodeFolderPath
        {
            get { return _z; }
            set
            {
                _z = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_WebserviceCodeFolderPath_noevents
        {
            get { return _z; }
            set
            {
                _z = value;
            }
        }

        public String ApVrsn_StoredProcCodeFolder
        {
            get { return _aa; }
            set
            {
                _aa = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_StoredProcCodeFolder_noevents
        {
            get { return _aa; }
            set
            {
                _aa = value;
            }
        }

        public String ApVrsn_DataServiceAssemblyName
        {
            get { return _ab; }
            set
            {
                _ab = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DataServiceAssemblyName_noevents
        {
            get { return _ab; }
            set
            {
                _ab = value;
            }
        }

        public String ApVrsn_DataServiceNamespace
        {
            get { return _ac; }
            set
            {
                _ac = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DataServiceNamespace_noevents
        {
            get { return _ac; }
            set
            {
                _ac = value;
            }
        }

        public String ApVrsn_DataServicePath
        {
            get { return _ad; }
            set
            {
                _ad = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_DataServicePath_noevents
        {
            get { return _ad; }
            set
            {
                _ad = value;
            }
        }

        public Boolean ApVrsn_IsMulticultural
        {
            get { return _ae; }
            set
            {
                _ae = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean ApVrsn_IsMulticultural_noevents
        {
            get { return _ae; }
            set
            {
                _ae = value;
            }
        }

        public String ApVrsn_Notes
        {
            get { return _af; }
            set
            {
                _af = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApVrsn_Notes_noevents
        {
            get { return _af; }
            set
            {
                _af = value;
            }
        }

        public Boolean ApVrsn_IsActiveRow
        {
            get { return _ag; }
            set
            {
                _ag = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean ApVrsn_IsActiveRow_noevents
        {
            get { return _ag; }
            set
            {
                _ag = value;
            }
        }

        public Boolean ApVrsn_IsDeleted
        {
            get { return _ah; }
            set
            {
                _ah = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean ApVrsn_IsDeleted_noevents
        {
            get { return _ah; }
            set
            {
                _ah = value;
            }
        }

        public Guid? ApVrsn_CreatedUserId
        {
            get { return _ai; }
            set
            {
                _ai = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApVrsn_CreatedUserId_noevents
        {
            get { return _ai; }
            set
            {
                _ai = value;
            }
        }

        public DateTime? ApVrsn_CreatedDate
        {
            get { return _aj; }
            set
            {
                _aj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? ApVrsn_CreatedDate_noevents
        {
            get { return _aj; }
            set
            {
                _aj = value;
            }
        }

        public Guid? ApVrsn_UserId
        {
            get { return _ak; }
            set
            {
                _ak = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApVrsn_UserId_noevents
        {
            get { return _ak; }
            set
            {
                _ak = value;
            }
        }

        public String UserName
        {
            get { return _al; }
            set
            {
                _al = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _al; }
            set
            {
                _al = value;
            }
        }

        public DateTime? ApVrsn_LastModifiedDate
        {
            get { return _am; }
            set
            {
                _am = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? ApVrsn_LastModifiedDate_noevents
        {
            get { return _am; }
            set
            {
                _am = value;
            }
        }

        public Byte[] ApVrsn_Stamp
        {
            get { return _an; }
            set
            {
                _an = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] ApVrsn_Stamp_noevents
        {
            get { return _an; }
            set
            {
                _an = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r,
				_s,
				_t,
				_u,
				_v,
				_w,
				_x,
				_y,
				_z,
				_aa,
				_ab,
				_ac,
				_ad,
				_ae,
				_af,
				_ag,
				_ah,
				_ai,
				_aj,
				_ak,
				_al,
				_am,
				_an
			};
        }

        public WcApplicationVersion_Values Clone()
        {
            return new WcApplicationVersion_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


