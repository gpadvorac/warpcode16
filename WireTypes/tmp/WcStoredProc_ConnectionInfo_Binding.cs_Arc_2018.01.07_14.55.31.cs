using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx;

// Gen Timestamp:  1/7/2018 10:06:48 AM

namespace EntityWireType
{


    
    public class WcStoredProc_ConnectionInfo_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProc_ConnectionInfo_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcStoredProc_ConnectionInfo_Binding() { }


        public WcStoredProc_ConnectionInfo_Binding(String _Sp_Name, String _Sp_MethodName, Boolean _Sp_IsCreateWireType, String _Sp_WireTypeName, String _DbCnSK_Server, String _DbCnSK_Database, String _DbCnSK_UserName, String _DbCnSK_Password, String _ApDbScm_Name )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ConnectionInfo_Binding", IfxTraceCategory.Enter);
				_a = _Sp_Name;
				_b = _Sp_MethodName;
				_c = _Sp_IsCreateWireType;
				_d = _Sp_WireTypeName;
				_e = _DbCnSK_Server;
				_f = _DbCnSK_Database;
				_g = _DbCnSK_UserName;
				_h = _DbCnSK_Password;
				_i = _ApDbScm_Name;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ConnectionInfo_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ConnectionInfo_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcStoredProc_ConnectionInfo_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ConnectionInfo_Binding", IfxTraceCategory.Enter);
				_a = (String)data[0];                //  Sp_Name
				_b = (String)data[1];                //  Sp_MethodName
				_c = (Boolean)data[2];                //  Sp_IsCreateWireType
				_d = (String)data[3];                //  Sp_WireTypeName
				_e = (String)data[4];                //  DbCnSK_Server
				_f = (String)data[5];                //  DbCnSK_Database
				_g = (String)data[6];                //  DbCnSK_UserName
				_h = (String)data[7];                //  DbCnSK_Password
				_i = (String)data[8];                //  ApDbScm_Name
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ConnectionInfo_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ConnectionInfo_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return null;
        }

        public void Set_Guid_Id(Guid value)
        {
            //_a = value;
        }

        public String Get_String_Id()
        {
            return _a;
        }

        public void Set_String_Id(String value)
        {
            _a = value;
        }

        public String Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region Sp_Name

        private String _a;
//        public String A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public String Sp_Name
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Sp_Name


        #region Sp_MethodName

        private String _b;
//        public String B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public String Sp_MethodName
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion Sp_MethodName


        #region Sp_IsCreateWireType

        private Boolean _c;
//        public Boolean C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public Boolean Sp_IsCreateWireType
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion Sp_IsCreateWireType


        #region Sp_WireTypeName

        private String _d;
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String Sp_WireTypeName
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion Sp_WireTypeName


        #region DbCnSK_Server

        private String _e;
//        public String E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public String DbCnSK_Server
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion DbCnSK_Server


        #region DbCnSK_Database

        private String _f;
//        public String F
//        {
//            get { return _f; }
//            set { _f = value; }
//        }

        //[NonSerialized]
        public String DbCnSK_Database
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion DbCnSK_Database


        #region DbCnSK_UserName

        private String _g;
//        public String G
//        {
//            get { return _g; }
//            set { _g = value; }
//        }

        //[NonSerialized]
        public String DbCnSK_UserName
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion DbCnSK_UserName


        #region DbCnSK_Password

        private String _h;
//        public String H
//        {
//            get { return _h; }
//            set { _h = value; }
//        }

        //[NonSerialized]
        public String DbCnSK_Password
        {
            get { return _h; }
            set { _h = value; }
        }

        #endregion DbCnSK_Password


        #region ApDbScm_Name

        private String _i;
//        public String I
//        {
//            get { return _i; }
//            set { _i = value; }
//        }

        //[NonSerialized]
        public String ApDbScm_Name
        {
            get { return _i; }
            set { _i = value; }
        }

        #endregion ApDbScm_Name


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8} ", Sp_Name, Sp_MethodName, Sp_IsCreateWireType, Sp_WireTypeName, DbCnSK_Server, DbCnSK_Database, DbCnSK_UserName, DbCnSK_Password, ApDbScm_Name );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8} ", Sp_Name, Sp_MethodName, Sp_IsCreateWireType, Sp_WireTypeName, DbCnSK_Server, DbCnSK_Database, DbCnSK_UserName, DbCnSK_Password, ApDbScm_Name );
        }

    }

}
