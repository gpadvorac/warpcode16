using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;


// Gen Timestamp:  1/6/2018 10:58:10 PM

namespace EntityWireType
{

    #region Entity Values Manager
    
    public class WcAppDbSchema_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcAppDbSchema_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcAppDbSchema_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcAppDbSchema_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcAppDbSchema_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcAppDbSchema_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcAppDbSchema_Values(currentData, this) : new WcAppDbSchema_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcAppDbSchema_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcAppDbSchema_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcAppDbSchema_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcAppDbSchema_Values _original;
        
        public WcAppDbSchema_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcAppDbSchema_Values _current;
        
        public WcAppDbSchema_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcAppDbSchema_Values _concurrent;
        
        public WcAppDbSchema_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  ApDbScm_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  ApDbScm_Ap_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  ApDbScm_Name
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  ApDbScm_Desc
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  ApDbScm_IsActiveRow
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  ApDbScm_IsDeleted
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  ApDbScm_CreatedUserId
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  ApDbScm_CreatedDate
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  ApDbScm_UserId
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  UserName
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  ApDbScm_LastModifiedDate
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  ApDbScm_Stamp
                if (_current._l != _original._l)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{13}{0}{12}_b{13}{1}{12}_c{13}{2}{12}_d{13}{3}{12}_e{13}{4}{12}_f{13}{5}{12}_g{13}{6}{12}_h{13}{7}{12}_i{13}{8}{12}_j{13}{9}{12}_k{13}{10}{12}_l{13}{11}",
				new object[] {
				_current._a,		  //ApDbScm_Id
				_current._b,		  //ApDbScm_Ap_Id
				_current._c,		  //ApDbScm_Name
				_current._d,		  //ApDbScm_Desc
				_current._e,		  //ApDbScm_IsActiveRow
				_current._f,		  //ApDbScm_IsDeleted
				_current._g,		  //ApDbScm_CreatedUserId
				_current._h,		  //ApDbScm_CreatedDate
				_current._i,		  //ApDbScm_UserId
				_current._j,		  //UserName
				_current._k,		  //ApDbScm_LastModifiedDate
				_current._l,		  //ApDbScm_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    
    public class WcAppDbSchema_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcAppDbSchema_Values";
        private WcAppDbSchema_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcAppDbSchema_Values() 
        {
        }

        //public WcAppDbSchema_Values(object[] data, WcAppDbSchema_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcAppDbSchema_Values(object[] data, WcAppDbSchema_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcAppDbSchema_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  ApDbScm_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  ApDbScm_Ap_Id
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  ApDbScm_Name
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  ApDbScm_Desc
				_e = ObjectHelper.GetNullableBoolFromObjectValue(data[4]);					//  ApDbScm_IsActiveRow
				_f = ObjectHelper.GetNullableBoolFromObjectValue(data[5]);					//  ApDbScm_IsDeleted
				_g = ObjectHelper.GetNullableGuidFromObjectValue(data[6]);						//  ApDbScm_CreatedUserId
				_h = ObjectHelper.GetNullableDateTimeFromObjectValue(data[7]);					//  ApDbScm_CreatedDate
				_i = ObjectHelper.GetNullableGuidFromObjectValue(data[8]);						//  ApDbScm_UserId
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  UserName
				_k = ObjectHelper.GetNullableDateTimeFromObjectValue(data[10]);					//  ApDbScm_LastModifiedDate
				_l = data[11] as Byte[];						//  ApDbScm_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcAppDbSchema_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcAppDbSchema_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcAppDbSchema", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  ApDbScm_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  ApDbScm_Ap_Id
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  ApDbScm_Name
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  ApDbScm_Desc
				_e = ObjectHelper.GetNullableBoolFromObjectValue(data[4]);					//  ApDbScm_IsActiveRow
				_f = ObjectHelper.GetNullableBoolFromObjectValue(data[5]);					//  ApDbScm_IsDeleted
				_g = ObjectHelper.GetNullableGuidFromObjectValue(data[6]);						//  ApDbScm_CreatedUserId
				_h = ObjectHelper.GetNullableDateTimeFromObjectValue(data[7]);					//  ApDbScm_CreatedDate
				_i = ObjectHelper.GetNullableGuidFromObjectValue(data[8]);						//  ApDbScm_UserId
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  UserName
				_k = ObjectHelper.GetNullableDateTimeFromObjectValue(data[10]);					//  ApDbScm_LastModifiedDate
				_l = data[11] as Byte[];						//  ApDbScm_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcAppDbSchema", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcAppDbSchema", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		
		public Guid _a;			//  ApDbScm_Id

		
		public Guid? _b;			//  ApDbScm_Ap_Id

		
		public String _c;			//  ApDbScm_Name

		
		public String _d;			//  ApDbScm_Desc

		
		public Boolean? _e;			//  ApDbScm_IsActiveRow

		
		public Boolean? _f;			//  ApDbScm_IsDeleted

		
		public Guid? _g;			//  ApDbScm_CreatedUserId

		
		public DateTime? _h;			//  ApDbScm_CreatedDate

		
		public Guid? _i;			//  ApDbScm_UserId

		
		public String _j;			//  UserName

		
		public DateTime? _k;			//  ApDbScm_LastModifiedDate

		
		public Byte[] _l;			//  ApDbScm_Stamp

		
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcAppDbSchema_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 12; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                }
                return _list;
            }
        }

        public Guid ApDbScm_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid ApDbScm_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? ApDbScm_Ap_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApDbScm_Ap_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public String ApDbScm_Name
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApDbScm_Name_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String ApDbScm_Desc
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApDbScm_Desc_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public Boolean? ApDbScm_IsActiveRow
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? ApDbScm_IsActiveRow_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Boolean? ApDbScm_IsDeleted
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? ApDbScm_IsDeleted_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Guid? ApDbScm_CreatedUserId
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApDbScm_CreatedUserId_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public DateTime? ApDbScm_CreatedDate
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? ApDbScm_CreatedDate_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Guid? ApDbScm_UserId
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApDbScm_UserId_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public String UserName
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public DateTime? ApDbScm_LastModifiedDate
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? ApDbScm_LastModifiedDate_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public Byte[] ApDbScm_Stamp
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] ApDbScm_Stamp_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l
			};
        }

        public WcAppDbSchema_Values Clone()
        {
            return new WcAppDbSchema_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


