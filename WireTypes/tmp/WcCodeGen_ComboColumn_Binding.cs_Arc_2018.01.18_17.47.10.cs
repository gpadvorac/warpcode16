using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx;

// Gen Timestamp:  1/18/2018 4:57:18 PM

namespace EntityWireType
{


    
    public class WcCodeGen_ComboColumn_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcCodeGen_ComboColumn_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcCodeGen_ComboColumn_Binding() { }


        public WcCodeGen_ComboColumn_Binding(Guid _TbCCmbC_Id, Guid _TbCCmbC_TbC_Id, Byte _TbCCmbC_Index, String _TbCCmbC_HeaderText, Double _TbCCmbC_MinWidth, Double _TbCCmbC_MaxWidth, Boolean _TbCCmbC_TextWrap, Boolean _TbCCmbC_IsActiveRow, Boolean _TbCCmbC_IsDeleted, Guid _TbCCmbC_CreatedUserId, DateTime _TbCCmbC_CreatedDate, Guid _TbCCmbC_UserId, String _UserName, DateTime _TbCCmbC_LastModifiedDate, Byte[] _TbCCmbC_Stamp )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ComboColumn_Binding", IfxTraceCategory.Enter);
				_a = _TbCCmbC_Id;
				_b = _TbCCmbC_TbC_Id;
				_c = _TbCCmbC_Index;
				_d = _TbCCmbC_HeaderText;
				_e = _TbCCmbC_MinWidth;
				_f = _TbCCmbC_MaxWidth;
				_g = _TbCCmbC_TextWrap;
				_h = _TbCCmbC_IsActiveRow;
				_i = _TbCCmbC_IsDeleted;
				_j = _TbCCmbC_CreatedUserId;
				_k = _TbCCmbC_CreatedDate;
				_l = _TbCCmbC_UserId;
				_m = _UserName;
				_n = _TbCCmbC_LastModifiedDate;
				_o = _TbCCmbC_Stamp;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ComboColumn_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ComboColumn_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcCodeGen_ComboColumn_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ComboColumn_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  TbCCmbC_Id
				_b = (Guid)data[1];                //  TbCCmbC_TbC_Id
				_c = (Byte)data[2];                //  TbCCmbC_Index
				_d = (String)data[3];                //  TbCCmbC_HeaderText
				_e = (Double)data[4];                //  TbCCmbC_MinWidth
				_f = (Double)data[5];                //  TbCCmbC_MaxWidth
				_g = (Boolean)data[6];                //  TbCCmbC_TextWrap
				_h = (Boolean)data[7];                //  TbCCmbC_IsActiveRow
				_i = (Boolean)data[8];                //  TbCCmbC_IsDeleted
				_j = (Guid)data[9];                //  TbCCmbC_CreatedUserId
				_k = (DateTime)data[10];                //  TbCCmbC_CreatedDate
				_l = (Guid)data[11];                //  TbCCmbC_UserId
				_m = (String)data[12];                //  UserName
				_n = (DateTime)data[13];                //  TbCCmbC_LastModifiedDate
				_o = (Byte[])data[14];                //  TbCCmbC_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ComboColumn_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ComboColumn_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region TbCCmbC_Id

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid TbCCmbC_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion TbCCmbC_Id


        #region TbCCmbC_TbC_Id

        private Guid _b;
//        public Guid B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Guid TbCCmbC_TbC_Id
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion TbCCmbC_TbC_Id


        #region TbCCmbC_Index

        private Byte _c;
//        public Byte C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public Byte TbCCmbC_Index
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion TbCCmbC_Index


        #region TbCCmbC_HeaderText

        private String _d;
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String TbCCmbC_HeaderText
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion TbCCmbC_HeaderText


        #region TbCCmbC_MinWidth

        private Double _e;
//        public Double E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public Double TbCCmbC_MinWidth
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion TbCCmbC_MinWidth


        #region TbCCmbC_MaxWidth

        private Double _f;
//        public Double F
//        {
//            get { return _f; }
//            set { _f = value; }
//        }

        //[NonSerialized]
        public Double TbCCmbC_MaxWidth
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion TbCCmbC_MaxWidth


        #region TbCCmbC_TextWrap

        private Boolean _g;
//        public Boolean G
//        {
//            get { return _g; }
//            set { _g = value; }
//        }

        //[NonSerialized]
        public Boolean TbCCmbC_TextWrap
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion TbCCmbC_TextWrap


        #region TbCCmbC_IsActiveRow

        private Boolean _h;
//        public Boolean H
//        {
//            get { return _h; }
//            set { _h = value; }
//        }

        //[NonSerialized]
        public Boolean TbCCmbC_IsActiveRow
        {
            get { return _h; }
            set { _h = value; }
        }

        #endregion TbCCmbC_IsActiveRow


        #region TbCCmbC_IsDeleted

        private Boolean _i;
//        public Boolean I
//        {
//            get { return _i; }
//            set { _i = value; }
//        }

        //[NonSerialized]
        public Boolean TbCCmbC_IsDeleted
        {
            get { return _i; }
            set { _i = value; }
        }

        #endregion TbCCmbC_IsDeleted


        #region TbCCmbC_CreatedUserId

        private Guid _j;
//        public Guid J
//        {
//            get { return _j; }
//            set { _j = value; }
//        }

        //[NonSerialized]
        public Guid TbCCmbC_CreatedUserId
        {
            get { return _j; }
            set { _j = value; }
        }

        #endregion TbCCmbC_CreatedUserId


        #region TbCCmbC_CreatedDate

        private DateTime _k;
//        public DateTime K
//        {
//            get { return _k; }
//            set { _k = value; }
//        }

        //[NonSerialized]
        public DateTime TbCCmbC_CreatedDate
        {
            get { return _k; }
            set { _k = value; }
        }

        #endregion TbCCmbC_CreatedDate


        #region TbCCmbC_UserId

        private Guid _l;
//        public Guid L
//        {
//            get { return _l; }
//            set { _l = value; }
//        }

        //[NonSerialized]
        public Guid TbCCmbC_UserId
        {
            get { return _l; }
            set { _l = value; }
        }

        #endregion TbCCmbC_UserId


        #region UserName

        private String _m;
//        public String M
//        {
//            get { return _m; }
//            set { _m = value; }
//        }

        //[NonSerialized]
        public String UserName
        {
            get { return _m; }
            set { _m = value; }
        }

        #endregion UserName


        #region TbCCmbC_LastModifiedDate

        private DateTime _n;
//        public DateTime N
//        {
//            get { return _n; }
//            set { _n = value; }
//        }

        //[NonSerialized]
        public DateTime TbCCmbC_LastModifiedDate
        {
            get { return _n; }
            set { _n = value; }
        }

        #endregion TbCCmbC_LastModifiedDate


        #region TbCCmbC_Stamp

        private Byte[] _o;
//        public Byte[] O
//        {
//            get { return _o; }
//            set { _o = value; }
//        }

        //[NonSerialized]
        public Byte[] TbCCmbC_Stamp
        {
            get { return _o; }
            set { _o = value; }
        }

        #endregion TbCCmbC_Stamp


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11}; {12}; {13}; {14} ", TbCCmbC_Id, TbCCmbC_TbC_Id, TbCCmbC_Index, TbCCmbC_HeaderText, TbCCmbC_MinWidth, TbCCmbC_MaxWidth, TbCCmbC_TextWrap, TbCCmbC_IsActiveRow, TbCCmbC_IsDeleted, TbCCmbC_CreatedUserId, TbCCmbC_CreatedDate, TbCCmbC_UserId, UserName, TbCCmbC_LastModifiedDate, TbCCmbC_Stamp );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11}; {12}; {13}; {14} ", TbCCmbC_Id, TbCCmbC_TbC_Id, TbCCmbC_Index, TbCCmbC_HeaderText, TbCCmbC_MinWidth, TbCCmbC_MaxWidth, TbCCmbC_TextWrap, TbCCmbC_IsActiveRow, TbCCmbC_IsDeleted, TbCCmbC_CreatedUserId, TbCCmbC_CreatedDate, TbCCmbC_UserId, UserName, TbCCmbC_LastModifiedDate, TbCCmbC_Stamp );
        }

    }

}
