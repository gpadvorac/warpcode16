using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx;


// Gen Timestamp:  12/18/2017 10:11:44 PM

namespace EntityWireType
{

    #region Entity Values Manager
    
    public class WcTable_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTable_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcTable_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcTable_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcTable_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcTable_Values(currentData, this) : new WcTable_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcTable_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcTable_Values _original;
        
        public WcTable_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcTable_Values _current;
        
        public WcTable_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcTable_Values _concurrent;
        
        public WcTable_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  Tb_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  Tb_ApVrsn_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  Tb_auditTable_Id
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  AttachmentCount
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  AttachmentFileNames
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  DiscussionCount
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  DiscussionTitles
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  Tb_Name
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  Tb_EntityRootName
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  Tb_VariableName
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  Tb_ScreenCaption
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  Tb_Description
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  Tb_DevelopmentNote
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  Tb_UIAssemblyName
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  Tb_UINamespace
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  Tb_UIAssemblyPath
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  Tb_WebServiceName
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  Tb_WebServiceFolder
                if (_current._r != _original._r)
                {
                    return true;
                }

                //  Tb_DataServiceAssemblyName
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  Tb_DataServicePath
                if (_current._t != _original._t)
                {
                    return true;
                }

                //  Tb_WireTypeAssemblyName
                if (_current._u != _original._u)
                {
                    return true;
                }

                //  Tb_WireTypePath
                if (_current._v != _original._v)
                {
                    return true;
                }

                //  Tb_UseTilesInPropsScreen
                if (_current._w != _original._w)
                {
                    return true;
                }

                //  Tb_UseGridColumnGroups
                if (_current._x != _original._x)
                {
                    return true;
                }

                //  Tb_UseGridDataSourceCombo
                if (_current._y != _original._y)
                {
                    return true;
                }

                //  Tb_ApCnSK_Id
                if (_current._z != _original._z)
                {
                    return true;
                }

                //  Tb_ApCnSK_Id_TextField
                //if (_current._aa != _original._aa)
                //{
                //    return true;
                //}

                //  Tb_UseLegacyConnectionCode
                if (_current._ab != _original._ab)
                {
                    return true;
                }

                //  Tb_PkIsIdentity
                if (_current._ac != _original._ac)
                {
                    return true;
                }

                //  Tb_IsVirtual
                if (_current._ad != _original._ad)
                {
                    return true;
                }

                //  Tb_IsNotEntity
                if (_current._ae != _original._ae)
                {
                    return true;
                }

                //  Tb_IsMany2Many
                if (_current._af != _original._af)
                {
                    return true;
                }

                //  Tb_UseLastModifiedByUserNameInSproc
                if (_current._ag != _original._ag)
                {
                    return true;
                }

                //  Tb_UseUserTimeStamp
                if (_current._ah != _original._ah)
                {
                    return true;
                }

                //  Tb_UseForAudit
                if (_current._ai != _original._ai)
                {
                    return true;
                }

                //  Tb_IsInputComplete
                if (_current._aj != _original._aj)
                {
                    return true;
                }

                //  Tb_IsCodeGen
                if (_current._ak != _original._ak)
                {
                    return true;
                }

                //  Tb_IsReadyCodeGen
                if (_current._al != _original._al)
                {
                    return true;
                }

                //  Tb_IsCodeGenComplete
                if (_current._am != _original._am)
                {
                    return true;
                }

                //  Tb_IsTagForCodeGen
                if (_current._an != _original._an)
                {
                    return true;
                }

                //  Tb_IsTagForOther
                if (_current._ao != _original._ao)
                {
                    return true;
                }

                //  Tb_TableGroups
                if (_current._ap != _original._ap)
                {
                    return true;
                }

                //  Tb_IsActiveRow
                if (_current._aq != _original._aq)
                {
                    return true;
                }

                //  Tb_IsDeleted
                if (_current._ar != _original._ar)
                {
                    return true;
                }

                //  Tb_CreatedUserId
                if (_current._asxx != _original._asxx)
                {
                    return true;
                }

                //  Tb_CreatedDate
                if (_current._at != _original._at)
                {
                    return true;
                }

                //  Tb_UserId
                if (_current._au != _original._au)
                {
                    return true;
                }

                //  UserName
                if (_current._av != _original._av)
                {
                    return true;
                }

                //  Tb_LastModifiedDate
                if (_current._aw != _original._aw)
                {
                    return true;
                }

                //  Tb_Stamp
                if (_current._ax != _original._ax)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{51}{0}{50}_b{51}{1}{50}_c{51}{2}{50}_d{51}{3}{50}_e{51}{4}{50}_f{51}{5}{50}_g{51}{6}{50}_h{51}{7}{50}_i{51}{8}{50}_j{51}{9}{50}_k{51}{10}{50}_l{51}{11}{50}_m{51}{12}{50}_n{51}{13}{50}_o{51}{14}{50}_p{51}{15}{50}_q{51}{16}{50}_r{51}{17}{50}_s{51}{18}{50}_t{51}{19}{50}_u{51}{20}{50}_v{51}{21}{50}_w{51}{22}{50}_x{51}{23}{50}_y{51}{24}{50}_z{51}{25}{50}_aa{51}{26}{50}_ab{51}{27}{50}_ac{51}{28}{50}_ad{51}{29}{50}_ae{51}{30}{50}_af{51}{31}{50}_ag{51}{32}{50}_ah{51}{33}{50}_ai{51}{34}{50}_aj{51}{35}{50}_ak{51}{36}{50}_al{51}{37}{50}_am{51}{38}{50}_an{51}{39}{50}_ao{51}{40}{50}_ap{51}{41}{50}_aq{51}{42}{50}_ar{51}{43}{50}_asxx{51}{44}{50}_at{51}{45}{50}_au{51}{46}{50}_av{51}{47}{50}_aw{51}{48}{50}_ax{51}{49}",
				new object[] {
				_current._a,		  //Tb_Id
				_current._b,		  //Tb_ApVrsn_Id
				_current._c,		  //Tb_auditTable_Id
				_current._d,		  //AttachmentCount
				_current._e,		  //AttachmentFileNames
				_current._f,		  //DiscussionCount
				_current._g,		  //DiscussionTitles
				_current._h,		  //Tb_Name
				_current._i,		  //Tb_EntityRootName
				_current._j,		  //Tb_VariableName
				_current._k,		  //Tb_ScreenCaption
				_current._l,		  //Tb_Description
				_current._m,		  //Tb_DevelopmentNote
				_current._n,		  //Tb_UIAssemblyName
				_current._o,		  //Tb_UINamespace
				_current._p,		  //Tb_UIAssemblyPath
				_current._q,		  //Tb_WebServiceName
				_current._r,		  //Tb_WebServiceFolder
				_current._s,		  //Tb_DataServiceAssemblyName
				_current._t,		  //Tb_DataServicePath
				_current._u,		  //Tb_WireTypeAssemblyName
				_current._v,		  //Tb_WireTypePath
				_current._w,		  //Tb_UseTilesInPropsScreen
				_current._x,		  //Tb_UseGridColumnGroups
				_current._y,		  //Tb_UseGridDataSourceCombo
				_current._z,		  //Tb_ApCnSK_Id
				_current._aa,		  //Tb_ApCnSK_Id_TextField
				_current._ab,		  //Tb_UseLegacyConnectionCode
				_current._ac,		  //Tb_PkIsIdentity
				_current._ad,		  //Tb_IsVirtual
				_current._ae,		  //Tb_IsNotEntity
				_current._af,		  //Tb_IsMany2Many
				_current._ag,		  //Tb_UseLastModifiedByUserNameInSproc
				_current._ah,		  //Tb_UseUserTimeStamp
				_current._ai,		  //Tb_UseForAudit
				_current._aj,		  //Tb_IsInputComplete
				_current._ak,		  //Tb_IsCodeGen
				_current._al,		  //Tb_IsReadyCodeGen
				_current._am,		  //Tb_IsCodeGenComplete
				_current._an,		  //Tb_IsTagForCodeGen
				_current._ao,		  //Tb_IsTagForOther
				_current._ap,		  //Tb_TableGroups
				_current._aq,		  //Tb_IsActiveRow
				_current._ar,		  //Tb_IsDeleted
				_current._asxx,		  //Tb_CreatedUserId
				_current._at,		  //Tb_CreatedDate
				_current._au,		  //Tb_UserId
				_current._av,		  //UserName
				_current._aw,		  //Tb_LastModifiedDate
				_current._ax,		  //Tb_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    
    public class WcTable_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTable_Values";
        private WcTable_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcTable_Values() 
        {
        }

        //public WcTable_Values(object[] data, WcTable_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcTable_Values(object[] data, WcTable_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Tb_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Tb_ApVrsn_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  Tb_auditTable_Id
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  AttachmentCount
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  AttachmentFileNames
				_f = ObjectHelper.GetNullableIntFromObjectValue(data[5]);							//  DiscussionCount
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  DiscussionTitles
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  Tb_Name
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  Tb_EntityRootName
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  Tb_VariableName
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  Tb_ScreenCaption
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  Tb_Description
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  Tb_DevelopmentNote
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  Tb_UIAssemblyName
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  Tb_UINamespace
				_p = ObjectHelper.GetStringFromObjectValue(data[15]);									//  Tb_UIAssemblyPath
				_q = ObjectHelper.GetStringFromObjectValue(data[16]);									//  Tb_WebServiceName
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  Tb_WebServiceFolder
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  Tb_DataServiceAssemblyName
				_t = ObjectHelper.GetStringFromObjectValue(data[19]);									//  Tb_DataServicePath
				_u = ObjectHelper.GetStringFromObjectValue(data[20]);									//  Tb_WireTypeAssemblyName
				_v = ObjectHelper.GetStringFromObjectValue(data[21]);									//  Tb_WireTypePath
				_w = ObjectHelper.GetBoolFromObjectValue(data[22]);									//  Tb_UseTilesInPropsScreen
				_x = ObjectHelper.GetBoolFromObjectValue(data[23]);									//  Tb_UseGridColumnGroups
				_y = ObjectHelper.GetBoolFromObjectValue(data[24]);									//  Tb_UseGridDataSourceCombo
				_z = ObjectHelper.GetNullableGuidFromObjectValue(data[25]);						//  Tb_ApCnSK_Id
				_aa = ObjectHelper.GetStringFromObjectValue(data[26]);									//  Tb_ApCnSK_Id_TextField
				_ab = ObjectHelper.GetBoolFromObjectValue(data[27]);									//  Tb_UseLegacyConnectionCode
				_ac = ObjectHelper.GetBoolFromObjectValue(data[28]);									//  Tb_PkIsIdentity
				_ad = ObjectHelper.GetBoolFromObjectValue(data[29]);									//  Tb_IsVirtual
				_ae = ObjectHelper.GetBoolFromObjectValue(data[30]);									//  Tb_IsNotEntity
				_af = ObjectHelper.GetBoolFromObjectValue(data[31]);									//  Tb_IsMany2Many
				_ag = ObjectHelper.GetBoolFromObjectValue(data[32]);									//  Tb_UseLastModifiedByUserNameInSproc
				_ah = ObjectHelper.GetBoolFromObjectValue(data[33]);									//  Tb_UseUserTimeStamp
				_ai = ObjectHelper.GetBoolFromObjectValue(data[34]);									//  Tb_UseForAudit
				_aj = ObjectHelper.GetNullableBoolFromObjectValue(data[35]);					//  Tb_IsInputComplete
				_ak = ObjectHelper.GetBoolFromObjectValue(data[36]);									//  Tb_IsCodeGen
				_al = ObjectHelper.GetBoolFromObjectValue(data[37]);									//  Tb_IsReadyCodeGen
				_am = ObjectHelper.GetBoolFromObjectValue(data[38]);									//  Tb_IsCodeGenComplete
				_an = ObjectHelper.GetBoolFromObjectValue(data[39]);									//  Tb_IsTagForCodeGen
				_ao = ObjectHelper.GetBoolFromObjectValue(data[40]);									//  Tb_IsTagForOther
				_ap = ObjectHelper.GetStringFromObjectValue(data[41]);									//  Tb_TableGroups
				_aq = ObjectHelper.GetBoolFromObjectValue(data[42]);									//  Tb_IsActiveRow
				_ar = ObjectHelper.GetBoolFromObjectValue(data[43]);									//  Tb_IsDeleted
				_asxx = ObjectHelper.GetNullableGuidFromObjectValue(data[44]);						//  Tb_CreatedUserId
				_at = ObjectHelper.GetNullableDateTimeFromObjectValue(data[45]);					//  Tb_CreatedDate
				_au = ObjectHelper.GetNullableGuidFromObjectValue(data[46]);						//  Tb_UserId
				_av = ObjectHelper.GetStringFromObjectValue(data[47]);									//  UserName
				_aw = ObjectHelper.GetNullableDateTimeFromObjectValue(data[48]);					//  Tb_LastModifiedDate
				_ax = data[49] as Byte[];						//  Tb_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTable", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Tb_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Tb_ApVrsn_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  Tb_auditTable_Id
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  AttachmentCount
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  AttachmentFileNames
				_f = ObjectHelper.GetNullableIntFromObjectValue(data[5]);							//  DiscussionCount
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  DiscussionTitles
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  Tb_Name
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  Tb_EntityRootName
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  Tb_VariableName
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  Tb_ScreenCaption
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  Tb_Description
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  Tb_DevelopmentNote
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  Tb_UIAssemblyName
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  Tb_UINamespace
				_p = ObjectHelper.GetStringFromObjectValue(data[15]);									//  Tb_UIAssemblyPath
				_q = ObjectHelper.GetStringFromObjectValue(data[16]);									//  Tb_WebServiceName
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  Tb_WebServiceFolder
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  Tb_DataServiceAssemblyName
				_t = ObjectHelper.GetStringFromObjectValue(data[19]);									//  Tb_DataServicePath
				_u = ObjectHelper.GetStringFromObjectValue(data[20]);									//  Tb_WireTypeAssemblyName
				_v = ObjectHelper.GetStringFromObjectValue(data[21]);									//  Tb_WireTypePath
				_w = ObjectHelper.GetBoolFromObjectValue(data[22]);									//  Tb_UseTilesInPropsScreen
				_x = ObjectHelper.GetBoolFromObjectValue(data[23]);									//  Tb_UseGridColumnGroups
				_y = ObjectHelper.GetBoolFromObjectValue(data[24]);									//  Tb_UseGridDataSourceCombo
				_z = ObjectHelper.GetNullableGuidFromObjectValue(data[25]);						//  Tb_ApCnSK_Id
				_aa = ObjectHelper.GetStringFromObjectValue(data[26]);									//  Tb_ApCnSK_Id_TextField
				_ab = ObjectHelper.GetBoolFromObjectValue(data[27]);									//  Tb_UseLegacyConnectionCode
				_ac = ObjectHelper.GetBoolFromObjectValue(data[28]);									//  Tb_PkIsIdentity
				_ad = ObjectHelper.GetBoolFromObjectValue(data[29]);									//  Tb_IsVirtual
				_ae = ObjectHelper.GetBoolFromObjectValue(data[30]);									//  Tb_IsNotEntity
				_af = ObjectHelper.GetBoolFromObjectValue(data[31]);									//  Tb_IsMany2Many
				_ag = ObjectHelper.GetBoolFromObjectValue(data[32]);									//  Tb_UseLastModifiedByUserNameInSproc
				_ah = ObjectHelper.GetBoolFromObjectValue(data[33]);									//  Tb_UseUserTimeStamp
				_ai = ObjectHelper.GetBoolFromObjectValue(data[34]);									//  Tb_UseForAudit
				_aj = ObjectHelper.GetNullableBoolFromObjectValue(data[35]);					//  Tb_IsInputComplete
				_ak = ObjectHelper.GetBoolFromObjectValue(data[36]);									//  Tb_IsCodeGen
				_al = ObjectHelper.GetBoolFromObjectValue(data[37]);									//  Tb_IsReadyCodeGen
				_am = ObjectHelper.GetBoolFromObjectValue(data[38]);									//  Tb_IsCodeGenComplete
				_an = ObjectHelper.GetBoolFromObjectValue(data[39]);									//  Tb_IsTagForCodeGen
				_ao = ObjectHelper.GetBoolFromObjectValue(data[40]);									//  Tb_IsTagForOther
				_ap = ObjectHelper.GetStringFromObjectValue(data[41]);									//  Tb_TableGroups
				_aq = ObjectHelper.GetBoolFromObjectValue(data[42]);									//  Tb_IsActiveRow
				_ar = ObjectHelper.GetBoolFromObjectValue(data[43]);									//  Tb_IsDeleted
				_asxx = ObjectHelper.GetNullableGuidFromObjectValue(data[44]);						//  Tb_CreatedUserId
				_at = ObjectHelper.GetNullableDateTimeFromObjectValue(data[45]);					//  Tb_CreatedDate
				_au = ObjectHelper.GetNullableGuidFromObjectValue(data[46]);						//  Tb_UserId
				_av = ObjectHelper.GetStringFromObjectValue(data[47]);									//  UserName
				_aw = ObjectHelper.GetNullableDateTimeFromObjectValue(data[48]);					//  Tb_LastModifiedDate
				_ax = data[49] as Byte[];						//  Tb_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTable", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTable", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		
		public Guid _a;			//  Tb_Id

		
		public Guid? _b;			//  Tb_ApVrsn_Id

		
		public Guid? _c;			//  Tb_auditTable_Id

		
		public Int32? _d;			//  AttachmentCount

		
		public String _e;			//  AttachmentFileNames

		
		public Int32? _f;			//  DiscussionCount

		
		public String _g;			//  DiscussionTitles

		
		public String _h;			//  Tb_Name

		
		public String _i;			//  Tb_EntityRootName

		
		public String _j;			//  Tb_VariableName

		
		public String _k;			//  Tb_ScreenCaption

		
		public String _l;			//  Tb_Description

		
		public String _m;			//  Tb_DevelopmentNote

		
		public String _n;			//  Tb_UIAssemblyName

		
		public String _o;			//  Tb_UINamespace

		
		public String _p;			//  Tb_UIAssemblyPath

		
		public String _q;			//  Tb_WebServiceName

		
		public String _r;			//  Tb_WebServiceFolder

		
		public String _s;			//  Tb_DataServiceAssemblyName

		
		public String _t;			//  Tb_DataServicePath

		
		public String _u;			//  Tb_WireTypeAssemblyName

		
		public String _v;			//  Tb_WireTypePath

		
		public Boolean _w;			//  Tb_UseTilesInPropsScreen

		
		public Boolean _x;			//  Tb_UseGridColumnGroups

		
		public Boolean _y;			//  Tb_UseGridDataSourceCombo

		
		public Guid? _z;			//  Tb_ApCnSK_Id

		
		public String _aa;			//  Tb_ApCnSK_Id_TextField

		
		public Boolean _ab;			//  Tb_UseLegacyConnectionCode

		
		public Boolean _ac;			//  Tb_PkIsIdentity

		
		public Boolean _ad;			//  Tb_IsVirtual

		
		public Boolean _ae;			//  Tb_IsNotEntity

		
		public Boolean _af;			//  Tb_IsMany2Many

		
		public Boolean _ag;			//  Tb_UseLastModifiedByUserNameInSproc

		
		public Boolean _ah;			//  Tb_UseUserTimeStamp

		
		public Boolean _ai;			//  Tb_UseForAudit

		
		public Boolean? _aj;			//  Tb_IsInputComplete

		
		public Boolean _ak;			//  Tb_IsCodeGen

		
		public Boolean _al;			//  Tb_IsReadyCodeGen

		
		public Boolean _am;			//  Tb_IsCodeGenComplete

		
		public Boolean _an;			//  Tb_IsTagForCodeGen

		
		public Boolean _ao;			//  Tb_IsTagForOther

		
		public String _ap;			//  Tb_TableGroups

		
		public Boolean _aq;			//  Tb_IsActiveRow

		
		public Boolean _ar;			//  Tb_IsDeleted

		
		public Guid? _asxx;			//  Tb_CreatedUserId

		
		public DateTime? _at;			//  Tb_CreatedDate

		
		public Guid? _au;			//  Tb_UserId

		
		public String _av;			//  UserName

		
		public DateTime? _aw;			//  Tb_LastModifiedDate

		
		public Byte[] _ax;			//  Tb_Stamp

		
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcTable_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 50; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                    _list[18] = _s;
                    _list[19] = _t;
                    _list[20] = _u;
                    _list[21] = _v;
                    _list[22] = _w;
                    _list[23] = _x;
                    _list[24] = _y;
                    _list[25] = _z;
                    _list[26] = _aa;
                    _list[27] = _ab;
                    _list[28] = _ac;
                    _list[29] = _ad;
                    _list[30] = _ae;
                    _list[31] = _af;
                    _list[32] = _ag;
                    _list[33] = _ah;
                    _list[34] = _ai;
                    _list[35] = _aj;
                    _list[36] = _ak;
                    _list[37] = _al;
                    _list[38] = _am;
                    _list[39] = _an;
                    _list[40] = _ao;
                    _list[41] = _ap;
                    _list[42] = _aq;
                    _list[43] = _ar;
                    _list[44] = _asxx;
                    _list[45] = _at;
                    _list[46] = _au;
                    _list[47] = _av;
                    _list[48] = _aw;
                    _list[49] = _ax;
                }
                return _list;
            }
        }

        public Guid Tb_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid Tb_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? Tb_ApVrsn_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_ApVrsn_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Guid? Tb_auditTable_Id
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_auditTable_Id_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public Int32? AttachmentCount
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? AttachmentCount_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String AttachmentFileNames
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String AttachmentFileNames_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Int32? DiscussionCount
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? DiscussionCount_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public String DiscussionTitles
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DiscussionTitles_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public String Tb_Name
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_Name_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public String Tb_EntityRootName
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_EntityRootName_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public String Tb_VariableName
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_VariableName_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public String Tb_ScreenCaption
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_ScreenCaption_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public String Tb_Description
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_Description_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public String Tb_DevelopmentNote
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_DevelopmentNote_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public String Tb_UIAssemblyName
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_UIAssemblyName_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public String Tb_UINamespace
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_UINamespace_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public String Tb_UIAssemblyPath
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_UIAssemblyPath_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public String Tb_WebServiceName
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_WebServiceName_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public String Tb_WebServiceFolder
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_WebServiceFolder_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        public String Tb_DataServiceAssemblyName
        {
            get { return _s; }
            set
            {
                _s = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_DataServiceAssemblyName_noevents
        {
            get { return _s; }
            set
            {
                _s = value;
            }
        }

        public String Tb_DataServicePath
        {
            get { return _t; }
            set
            {
                _t = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_DataServicePath_noevents
        {
            get { return _t; }
            set
            {
                _t = value;
            }
        }

        public String Tb_WireTypeAssemblyName
        {
            get { return _u; }
            set
            {
                _u = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_WireTypeAssemblyName_noevents
        {
            get { return _u; }
            set
            {
                _u = value;
            }
        }

        public String Tb_WireTypePath
        {
            get { return _v; }
            set
            {
                _v = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_WireTypePath_noevents
        {
            get { return _v; }
            set
            {
                _v = value;
            }
        }

        public Boolean Tb_UseTilesInPropsScreen
        {
            get { return _w; }
            set
            {
                _w = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseTilesInPropsScreen_noevents
        {
            get { return _w; }
            set
            {
                _w = value;
            }
        }

        public Boolean Tb_UseGridColumnGroups
        {
            get { return _x; }
            set
            {
                _x = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseGridColumnGroups_noevents
        {
            get { return _x; }
            set
            {
                _x = value;
            }
        }

        public Boolean Tb_UseGridDataSourceCombo
        {
            get { return _y; }
            set
            {
                _y = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseGridDataSourceCombo_noevents
        {
            get { return _y; }
            set
            {
                _y = value;
            }
        }

        public Guid? Tb_ApCnSK_Id
        {
            get { return _z; }
            set
            {
                _z = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_ApCnSK_Id_noevents
        {
            get { return _z; }
            set
            {
                _z = value;
            }
        }

        public String Tb_ApCnSK_Id_TextField
        {
            get { return _aa; }
            set
            {
                _aa = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_ApCnSK_Id_TextField_noevents
        {
            get { return _aa; }
            set
            {
                _aa = value;
            }
        }

        public Boolean Tb_UseLegacyConnectionCode
        {
            get { return _ab; }
            set
            {
                _ab = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseLegacyConnectionCode_noevents
        {
            get { return _ab; }
            set
            {
                _ab = value;
            }
        }

        public Boolean Tb_PkIsIdentity
        {
            get { return _ac; }
            set
            {
                _ac = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_PkIsIdentity_noevents
        {
            get { return _ac; }
            set
            {
                _ac = value;
            }
        }

        public Boolean Tb_IsVirtual
        {
            get { return _ad; }
            set
            {
                _ad = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsVirtual_noevents
        {
            get { return _ad; }
            set
            {
                _ad = value;
            }
        }

        public Boolean Tb_IsNotEntity
        {
            get { return _ae; }
            set
            {
                _ae = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsNotEntity_noevents
        {
            get { return _ae; }
            set
            {
                _ae = value;
            }
        }

        public Boolean Tb_IsMany2Many
        {
            get { return _af; }
            set
            {
                _af = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsMany2Many_noevents
        {
            get { return _af; }
            set
            {
                _af = value;
            }
        }

        public Boolean Tb_UseLastModifiedByUserNameInSproc
        {
            get { return _ag; }
            set
            {
                _ag = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseLastModifiedByUserNameInSproc_noevents
        {
            get { return _ag; }
            set
            {
                _ag = value;
            }
        }

        public Boolean Tb_UseUserTimeStamp
        {
            get { return _ah; }
            set
            {
                _ah = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseUserTimeStamp_noevents
        {
            get { return _ah; }
            set
            {
                _ah = value;
            }
        }

        public Boolean Tb_UseForAudit
        {
            get { return _ai; }
            set
            {
                _ai = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseForAudit_noevents
        {
            get { return _ai; }
            set
            {
                _ai = value;
            }
        }

        public Boolean? Tb_IsInputComplete
        {
            get { return _aj; }
            set
            {
                _aj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? Tb_IsInputComplete_noevents
        {
            get { return _aj; }
            set
            {
                _aj = value;
            }
        }

        public Boolean Tb_IsCodeGen
        {
            get { return _ak; }
            set
            {
                _ak = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsCodeGen_noevents
        {
            get { return _ak; }
            set
            {
                _ak = value;
            }
        }

        public Boolean Tb_IsReadyCodeGen
        {
            get { return _al; }
            set
            {
                _al = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsReadyCodeGen_noevents
        {
            get { return _al; }
            set
            {
                _al = value;
            }
        }

        public Boolean Tb_IsCodeGenComplete
        {
            get { return _am; }
            set
            {
                _am = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsCodeGenComplete_noevents
        {
            get { return _am; }
            set
            {
                _am = value;
            }
        }

        public Boolean Tb_IsTagForCodeGen
        {
            get { return _an; }
            set
            {
                _an = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsTagForCodeGen_noevents
        {
            get { return _an; }
            set
            {
                _an = value;
            }
        }

        public Boolean Tb_IsTagForOther
        {
            get { return _ao; }
            set
            {
                _ao = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsTagForOther_noevents
        {
            get { return _ao; }
            set
            {
                _ao = value;
            }
        }

        public String Tb_TableGroups
        {
            get { return _ap; }
            set
            {
                _ap = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_TableGroups_noevents
        {
            get { return _ap; }
            set
            {
                _ap = value;
            }
        }

        public Boolean Tb_IsActiveRow
        {
            get { return _aq; }
            set
            {
                _aq = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsActiveRow_noevents
        {
            get { return _aq; }
            set
            {
                _aq = value;
            }
        }

        public Boolean Tb_IsDeleted
        {
            get { return _ar; }
            set
            {
                _ar = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsDeleted_noevents
        {
            get { return _ar; }
            set
            {
                _ar = value;
            }
        }

        public Guid? Tb_CreatedUserId
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_CreatedUserId_noevents
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
            }
        }

        public DateTime? Tb_CreatedDate
        {
            get { return _at; }
            set
            {
                _at = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Tb_CreatedDate_noevents
        {
            get { return _at; }
            set
            {
                _at = value;
            }
        }

        public Guid? Tb_UserId
        {
            get { return _au; }
            set
            {
                _au = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_UserId_noevents
        {
            get { return _au; }
            set
            {
                _au = value;
            }
        }

        public String UserName
        {
            get { return _av; }
            set
            {
                _av = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _av; }
            set
            {
                _av = value;
            }
        }

        public DateTime? Tb_LastModifiedDate
        {
            get { return _aw; }
            set
            {
                _aw = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Tb_LastModifiedDate_noevents
        {
            get { return _aw; }
            set
            {
                _aw = value;
            }
        }

        public Byte[] Tb_Stamp
        {
            get { return _ax; }
            set
            {
                _ax = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] Tb_Stamp_noevents
        {
            get { return _ax; }
            set
            {
                _ax = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r,
				_s,
				_t,
				_u,
				_v,
				_w,
				_x,
				_y,
				_z,
				_aa,
				_ab,
				_ac,
				_ad,
				_ae,
				_af,
				_ag,
				_ah,
				_ai,
				_aj,
				_ak,
				_al,
				_am,
				_an,
				_ao,
				_ap,
				_aq,
				_ar,
				_asxx,
				_at,
				_au,
				_av,
				_aw,
				_ax
			};
        }

        public WcTable_Values Clone()
        {
            return new WcTable_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


