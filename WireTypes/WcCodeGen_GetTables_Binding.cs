using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx;

// Gen Timestamp:  1/18/2018 3:40:26 PM

namespace EntityWireType
{


    
    public class WcCodeGen_GetTables_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcCodeGen_GetTables_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcCodeGen_GetTables_Binding() { }


        public WcCodeGen_GetTables_Binding(Guid _Tb_Id, Guid _Tb_ApVrsn_Id, String _Tb_Name, String _Tb_EntityRootName, String _Tb_VariableName, String _Tb_ScreenCaption, String _Tb_Description, String _Tb_DevelopmentNote, Boolean _Tb_UseLegacyConnectionCode, Guid _Tb_DbCnSK_Id, String _Tb_DbCnSK_Id_TextField, Guid _Tb_ApDbScm_Id, Guid _Tb_ApDbScm_Id_TextField, String _Tb_UIAssemblyName, String _Tb_UINamespace, String _Tb_UIAssemblyPath, String _Tb_ProxyAssemblyName, String _Tb_ProxyNamespace, String _Tb_ProxyAssemblyPath, String _Tb_WireTypeAssemblyName, String _Tb_WireTypeNamespace, String _Tb_WireTypePath, String _Tb_WebServiceName, String _Tb_WebServiceFolder, String _Tb_DataServiceAssemblyName, String _Tb_DataServiceNamespace, String _Tb_DataServicePath, Boolean _Tb_UseTilesInPropsScreen, Boolean _Tb_UseGridColumnGroups, Boolean _Tb_UseGridDataSourceCombo, Boolean _Tb_PkIsIdentity, Boolean _Tb_IsVirtual, Boolean _Tb_IsScreenPlaceHolder, Boolean _Tb_IsNotEntity, Boolean _Tb_IsMany2Many, Boolean _Tb_UseLastModifiedByUserNameInSproc, Boolean _Tb_UseUserTimeStamp, Boolean _Tb_UseForAudit, Boolean _Tb_IsAllowDelete, Boolean _Tb_CnfgGdMnu_MenuRow_IsVisible, Boolean _Tb_CnfgGdMnu_GridTools_IsVisible, Boolean _Tb_CnfgGdMnu_SplitScreen_IsVisible, Boolean _Tb_CnfgGdMnu_SplitScreen_IsSplit_Default, Int32 _Tb_CnfgGdMnu_NavColumnWidth, Boolean _Tb_CnfgGdMnu_IsReadOnly, Boolean _Tb_CnfgGdMnu_IsAllowNewRow, Boolean _Tb_CnfgGdMnu_ExcelExport_IsVisible, Boolean _Tb_CnfgGdMnu_ColumnChooser_IsVisible, Boolean _Tb_CnfgGdMnu_ShowHideColBtn_IsVisible, Boolean _Tb_CnfgGdMnu_RefreshGrid_IsVisible, Boolean _Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible, Boolean _Tb_IsInputComplete, Boolean _Tb_IsCodeGen, Boolean _Tb_IsReadyCodeGen, Boolean _Tb_IsCodeGenComplete, Boolean _Tb_IsTagForCodeGen, Boolean _Tb_IsTagForOther, String _Tb_TableGroups, String _DbCnSK_Server, String _DbCnSK_Database, String _DbCnSK_UserName, String _DbCnSK_Password )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_GetTables_Binding", IfxTraceCategory.Enter);
				_a = _Tb_Id;
				_b = _Tb_ApVrsn_Id;
				_c = _Tb_Name;
				_d = _Tb_EntityRootName;
				_e = _Tb_VariableName;
				_f = _Tb_ScreenCaption;
				_g = _Tb_Description;
				_h = _Tb_DevelopmentNote;
				_i = _Tb_UseLegacyConnectionCode;
				_j = _Tb_DbCnSK_Id;
				_k = _Tb_DbCnSK_Id_TextField;
				_l = _Tb_ApDbScm_Id;
				_m = _Tb_ApDbScm_Id_TextField;
				_n = _Tb_UIAssemblyName;
				_o = _Tb_UINamespace;
				_p = _Tb_UIAssemblyPath;
				_q = _Tb_ProxyAssemblyName;
				_r = _Tb_ProxyNamespace;
				_s = _Tb_ProxyAssemblyPath;
				_t = _Tb_WireTypeAssemblyName;
				_u = _Tb_WireTypeNamespace;
				_v = _Tb_WireTypePath;
				_w = _Tb_WebServiceName;
				_x = _Tb_WebServiceFolder;
				_y = _Tb_DataServiceAssemblyName;
				_z = _Tb_DataServiceNamespace;
				_aa = _Tb_DataServicePath;
				_ab = _Tb_UseTilesInPropsScreen;
				_ac = _Tb_UseGridColumnGroups;
				_ad = _Tb_UseGridDataSourceCombo;
				_ae = _Tb_PkIsIdentity;
				_af = _Tb_IsVirtual;
				_ag = _Tb_IsScreenPlaceHolder;
				_ah = _Tb_IsNotEntity;
				_ai = _Tb_IsMany2Many;
				_aj = _Tb_UseLastModifiedByUserNameInSproc;
				_ak = _Tb_UseUserTimeStamp;
				_al = _Tb_UseForAudit;
				_am = _Tb_IsAllowDelete;
				_an = _Tb_CnfgGdMnu_MenuRow_IsVisible;
				_ao = _Tb_CnfgGdMnu_GridTools_IsVisible;
				_ap = _Tb_CnfgGdMnu_SplitScreen_IsVisible;
				_aq = _Tb_CnfgGdMnu_SplitScreen_IsSplit_Default;
				_ar = _Tb_CnfgGdMnu_NavColumnWidth;
				_asx = _Tb_CnfgGdMnu_IsReadOnly;
				_at = _Tb_CnfgGdMnu_IsAllowNewRow;
				_au = _Tb_CnfgGdMnu_ExcelExport_IsVisible;
				_av = _Tb_CnfgGdMnu_ColumnChooser_IsVisible;
				_aw = _Tb_CnfgGdMnu_ShowHideColBtn_IsVisible;
				_ax = _Tb_CnfgGdMnu_RefreshGrid_IsVisible;
				_ay = _Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible;
				_az = _Tb_IsInputComplete;
				_ba = _Tb_IsCodeGen;
				_bb = _Tb_IsReadyCodeGen;
				_bc = _Tb_IsCodeGenComplete;
				_bd = _Tb_IsTagForCodeGen;
				_be = _Tb_IsTagForOther;
				_bf = _Tb_TableGroups;
				_bg = _DbCnSK_Server;
				_bh = _DbCnSK_Database;
				_bi = _DbCnSK_UserName;
				_bj = _DbCnSK_Password;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_GetTables_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_GetTables_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcCodeGen_GetTables_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_GetTables_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  Tb_Id
				_b = (Guid)data[1];                //  Tb_ApVrsn_Id
				_c = (String)data[2];                //  Tb_Name
				_d = (String)data[3];                //  Tb_EntityRootName
				_e = (String)data[4];                //  Tb_VariableName
				_f = (String)data[5];                //  Tb_ScreenCaption
				_g = (String)data[6];                //  Tb_Description
				_h = (String)data[7];                //  Tb_DevelopmentNote
				_i = (Boolean)data[8];                //  Tb_UseLegacyConnectionCode
				_j = (Guid)data[9];                //  Tb_DbCnSK_Id
				_k = (String)data[10];                //  Tb_DbCnSK_Id_TextField
				_l = (Guid)data[11];                //  Tb_ApDbScm_Id
				_m = (Guid)data[12];                //  Tb_ApDbScm_Id_TextField
				_n = (String)data[13];                //  Tb_UIAssemblyName
				_o = (String)data[14];                //  Tb_UINamespace
				_p = (String)data[15];                //  Tb_UIAssemblyPath
				_q = (String)data[16];                //  Tb_ProxyAssemblyName
				_r = (String)data[17];                //  Tb_ProxyNamespace
				_s = (String)data[18];                //  Tb_ProxyAssemblyPath
				_t = (String)data[19];                //  Tb_WireTypeAssemblyName
				_u = (String)data[20];                //  Tb_WireTypeNamespace
				_v = (String)data[21];                //  Tb_WireTypePath
				_w = (String)data[22];                //  Tb_WebServiceName
				_x = (String)data[23];                //  Tb_WebServiceFolder
				_y = (String)data[24];                //  Tb_DataServiceAssemblyName
				_z = (String)data[25];                //  Tb_DataServiceNamespace
				_aa = (String)data[26];                //  Tb_DataServicePath
				_ab = (Boolean)data[27];                //  Tb_UseTilesInPropsScreen
				_ac = (Boolean)data[28];                //  Tb_UseGridColumnGroups
				_ad = (Boolean)data[29];                //  Tb_UseGridDataSourceCombo
				_ae = (Boolean)data[30];                //  Tb_PkIsIdentity
				_af = (Boolean)data[31];                //  Tb_IsVirtual
				_ag = (Boolean)data[32];                //  Tb_IsScreenPlaceHolder
				_ah = (Boolean)data[33];                //  Tb_IsNotEntity
				_ai = (Boolean)data[34];                //  Tb_IsMany2Many
				_aj = (Boolean)data[35];                //  Tb_UseLastModifiedByUserNameInSproc
				_ak = (Boolean)data[36];                //  Tb_UseUserTimeStamp
				_al = (Boolean)data[37];                //  Tb_UseForAudit
				_am = (Boolean)data[38];                //  Tb_IsAllowDelete
				_an = (Boolean)data[39];                //  Tb_CnfgGdMnu_MenuRow_IsVisible
				_ao = (Boolean)data[40];                //  Tb_CnfgGdMnu_GridTools_IsVisible
				_ap = (Boolean)data[41];                //  Tb_CnfgGdMnu_SplitScreen_IsVisible
				_aq = (Boolean)data[42];                //  Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
				_ar = (Int32)data[43];                //  Tb_CnfgGdMnu_NavColumnWidth
				_asx = (Boolean)data[44];                //  Tb_CnfgGdMnu_IsReadOnly
				_at = (Boolean)data[45];                //  Tb_CnfgGdMnu_IsAllowNewRow
				_au = (Boolean)data[46];                //  Tb_CnfgGdMnu_ExcelExport_IsVisible
				_av = (Boolean)data[47];                //  Tb_CnfgGdMnu_ColumnChooser_IsVisible
				_aw = (Boolean)data[48];                //  Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
				_ax = (Boolean)data[49];                //  Tb_CnfgGdMnu_RefreshGrid_IsVisible
				_ay = (Boolean)data[50];                //  Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
				_az = (Boolean)data[51];                //  Tb_IsInputComplete
				_ba = (Boolean)data[52];                //  Tb_IsCodeGen
				_bb = (Boolean)data[53];                //  Tb_IsReadyCodeGen
				_bc = (Boolean)data[54];                //  Tb_IsCodeGenComplete
				_bd = (Boolean)data[55];                //  Tb_IsTagForCodeGen
				_be = (Boolean)data[56];                //  Tb_IsTagForOther
				_bf = (String)data[57];                //  Tb_TableGroups
				_bg = (String)data[58];                //  DbCnSK_Server
				_bh = (String)data[59];                //  DbCnSK_Database
				_bi = (String)data[60];                //  DbCnSK_UserName
				_bj = (String)data[61];                //  DbCnSK_Password
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_GetTables_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_GetTables_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region Tb_Id

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid Tb_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Tb_Id


        #region Tb_ApVrsn_Id

        private Guid _b;
//        public Guid B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Guid Tb_ApVrsn_Id
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion Tb_ApVrsn_Id


        #region Tb_Name

        private String _c;
//        public String C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public String Tb_Name
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion Tb_Name


        #region Tb_EntityRootName

        private String _d;
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String Tb_EntityRootName
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion Tb_EntityRootName


        #region Tb_VariableName

        private String _e;
//        public String E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public String Tb_VariableName
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion Tb_VariableName


        #region Tb_ScreenCaption

        private String _f;
//        public String F
//        {
//            get { return _f; }
//            set { _f = value; }
//        }

        //[NonSerialized]
        public String Tb_ScreenCaption
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion Tb_ScreenCaption


        #region Tb_Description

        private String _g;
//        public String G
//        {
//            get { return _g; }
//            set { _g = value; }
//        }

        //[NonSerialized]
        public String Tb_Description
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion Tb_Description


        #region Tb_DevelopmentNote

        private String _h;
//        public String H
//        {
//            get { return _h; }
//            set { _h = value; }
//        }

        //[NonSerialized]
        public String Tb_DevelopmentNote
        {
            get { return _h; }
            set { _h = value; }
        }

        #endregion Tb_DevelopmentNote


        #region Tb_UseLegacyConnectionCode

        private Boolean _i;
//        public Boolean I
//        {
//            get { return _i; }
//            set { _i = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_UseLegacyConnectionCode
        {
            get { return _i; }
            set { _i = value; }
        }

        #endregion Tb_UseLegacyConnectionCode


        #region Tb_DbCnSK_Id

        private Guid _j;
//        public Guid J
//        {
//            get { return _j; }
//            set { _j = value; }
//        }

        //[NonSerialized]
        public Guid Tb_DbCnSK_Id
        {
            get { return _j; }
            set { _j = value; }
        }

        #endregion Tb_DbCnSK_Id


        #region Tb_DbCnSK_Id_TextField

        private String _k;
//        public String K
//        {
//            get { return _k; }
//            set { _k = value; }
//        }

        //[NonSerialized]
        public String Tb_DbCnSK_Id_TextField
        {
            get { return _k; }
            set { _k = value; }
        }

        #endregion Tb_DbCnSK_Id_TextField


        #region Tb_ApDbScm_Id

        private Guid _l;
//        public Guid L
//        {
//            get { return _l; }
//            set { _l = value; }
//        }

        //[NonSerialized]
        public Guid Tb_ApDbScm_Id
        {
            get { return _l; }
            set { _l = value; }
        }

        #endregion Tb_ApDbScm_Id


        #region Tb_ApDbScm_Id_TextField

        private Guid _m;
//        public Guid M
//        {
//            get { return _m; }
//            set { _m = value; }
//        }

        //[NonSerialized]
        public Guid Tb_ApDbScm_Id_TextField
        {
            get { return _m; }
            set { _m = value; }
        }

        #endregion Tb_ApDbScm_Id_TextField


        #region Tb_UIAssemblyName

        private String _n;
//        public String N
//        {
//            get { return _n; }
//            set { _n = value; }
//        }

        //[NonSerialized]
        public String Tb_UIAssemblyName
        {
            get { return _n; }
            set { _n = value; }
        }

        #endregion Tb_UIAssemblyName


        #region Tb_UINamespace

        private String _o;
//        public String O
//        {
//            get { return _o; }
//            set { _o = value; }
//        }

        //[NonSerialized]
        public String Tb_UINamespace
        {
            get { return _o; }
            set { _o = value; }
        }

        #endregion Tb_UINamespace


        #region Tb_UIAssemblyPath

        private String _p;
//        public String P
//        {
//            get { return _p; }
//            set { _p = value; }
//        }

        //[NonSerialized]
        public String Tb_UIAssemblyPath
        {
            get { return _p; }
            set { _p = value; }
        }

        #endregion Tb_UIAssemblyPath


        #region Tb_ProxyAssemblyName

        private String _q;
//        public String Q
//        {
//            get { return _q; }
//            set { _q = value; }
//        }

        //[NonSerialized]
        public String Tb_ProxyAssemblyName
        {
            get { return _q; }
            set { _q = value; }
        }

        #endregion Tb_ProxyAssemblyName


        #region Tb_ProxyNamespace

        private String _r;
//        public String R
//        {
//            get { return _r; }
//            set { _r = value; }
//        }

        //[NonSerialized]
        public String Tb_ProxyNamespace
        {
            get { return _r; }
            set { _r = value; }
        }

        #endregion Tb_ProxyNamespace


        #region Tb_ProxyAssemblyPath

        private String _s;
//        public String S
//        {
//            get { return _s; }
//            set { _s = value; }
//        }

        //[NonSerialized]
        public String Tb_ProxyAssemblyPath
        {
            get { return _s; }
            set { _s = value; }
        }

        #endregion Tb_ProxyAssemblyPath


        #region Tb_WireTypeAssemblyName

        private String _t;
//        public String T
//        {
//            get { return _t; }
//            set { _t = value; }
//        }

        //[NonSerialized]
        public String Tb_WireTypeAssemblyName
        {
            get { return _t; }
            set { _t = value; }
        }

        #endregion Tb_WireTypeAssemblyName


        #region Tb_WireTypeNamespace

        private String _u;
//        public String U
//        {
//            get { return _u; }
//            set { _u = value; }
//        }

        //[NonSerialized]
        public String Tb_WireTypeNamespace
        {
            get { return _u; }
            set { _u = value; }
        }

        #endregion Tb_WireTypeNamespace


        #region Tb_WireTypePath

        private String _v;
//        public String V
//        {
//            get { return _v; }
//            set { _v = value; }
//        }

        //[NonSerialized]
        public String Tb_WireTypePath
        {
            get { return _v; }
            set { _v = value; }
        }

        #endregion Tb_WireTypePath


        #region Tb_WebServiceName

        private String _w;
//        public String W
//        {
//            get { return _w; }
//            set { _w = value; }
//        }

        //[NonSerialized]
        public String Tb_WebServiceName
        {
            get { return _w; }
            set { _w = value; }
        }

        #endregion Tb_WebServiceName


        #region Tb_WebServiceFolder

        private String _x;
//        public String X
//        {
//            get { return _x; }
//            set { _x = value; }
//        }

        //[NonSerialized]
        public String Tb_WebServiceFolder
        {
            get { return _x; }
            set { _x = value; }
        }

        #endregion Tb_WebServiceFolder


        #region Tb_DataServiceAssemblyName

        private String _y;
//        public String Y
//        {
//            get { return _y; }
//            set { _y = value; }
//        }

        //[NonSerialized]
        public String Tb_DataServiceAssemblyName
        {
            get { return _y; }
            set { _y = value; }
        }

        #endregion Tb_DataServiceAssemblyName


        #region Tb_DataServiceNamespace

        private String _z;
//        public String Z
//        {
//            get { return _z; }
//            set { _z = value; }
//        }

        //[NonSerialized]
        public String Tb_DataServiceNamespace
        {
            get { return _z; }
            set { _z = value; }
        }

        #endregion Tb_DataServiceNamespace


        #region Tb_DataServicePath

        private String _aa;
//        public String AA
//        {
//            get { return _aa; }
//            set { _aa = value; }
//        }

        //[NonSerialized]
        public String Tb_DataServicePath
        {
            get { return _aa; }
            set { _aa = value; }
        }

        #endregion Tb_DataServicePath


        #region Tb_UseTilesInPropsScreen

        private Boolean _ab;
//        public Boolean AB
//        {
//            get { return _ab; }
//            set { _ab = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_UseTilesInPropsScreen
        {
            get { return _ab; }
            set { _ab = value; }
        }

        #endregion Tb_UseTilesInPropsScreen


        #region Tb_UseGridColumnGroups

        private Boolean _ac;
//        public Boolean AC
//        {
//            get { return _ac; }
//            set { _ac = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_UseGridColumnGroups
        {
            get { return _ac; }
            set { _ac = value; }
        }

        #endregion Tb_UseGridColumnGroups


        #region Tb_UseGridDataSourceCombo

        private Boolean _ad;
//        public Boolean AD
//        {
//            get { return _ad; }
//            set { _ad = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_UseGridDataSourceCombo
        {
            get { return _ad; }
            set { _ad = value; }
        }

        #endregion Tb_UseGridDataSourceCombo


        #region Tb_PkIsIdentity

        private Boolean _ae;
//        public Boolean AE
//        {
//            get { return _ae; }
//            set { _ae = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_PkIsIdentity
        {
            get { return _ae; }
            set { _ae = value; }
        }

        #endregion Tb_PkIsIdentity


        #region Tb_IsVirtual

        private Boolean _af;
//        public Boolean AF
//        {
//            get { return _af; }
//            set { _af = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_IsVirtual
        {
            get { return _af; }
            set { _af = value; }
        }

        #endregion Tb_IsVirtual


        #region Tb_IsScreenPlaceHolder

        private Boolean _ag;
//        public Boolean AG
//        {
//            get { return _ag; }
//            set { _ag = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_IsScreenPlaceHolder
        {
            get { return _ag; }
            set { _ag = value; }
        }

        #endregion Tb_IsScreenPlaceHolder


        #region Tb_IsNotEntity

        private Boolean _ah;
//        public Boolean AH
//        {
//            get { return _ah; }
//            set { _ah = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_IsNotEntity
        {
            get { return _ah; }
            set { _ah = value; }
        }

        #endregion Tb_IsNotEntity


        #region Tb_IsMany2Many

        private Boolean _ai;
//        public Boolean AI
//        {
//            get { return _ai; }
//            set { _ai = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_IsMany2Many
        {
            get { return _ai; }
            set { _ai = value; }
        }

        #endregion Tb_IsMany2Many


        #region Tb_UseLastModifiedByUserNameInSproc

        private Boolean _aj;
//        public Boolean AJ
//        {
//            get { return _aj; }
//            set { _aj = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_UseLastModifiedByUserNameInSproc
        {
            get { return _aj; }
            set { _aj = value; }
        }

        #endregion Tb_UseLastModifiedByUserNameInSproc


        #region Tb_UseUserTimeStamp

        private Boolean _ak;
//        public Boolean AK
//        {
//            get { return _ak; }
//            set { _ak = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_UseUserTimeStamp
        {
            get { return _ak; }
            set { _ak = value; }
        }

        #endregion Tb_UseUserTimeStamp


        #region Tb_UseForAudit

        private Boolean _al;
//        public Boolean AL
//        {
//            get { return _al; }
//            set { _al = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_UseForAudit
        {
            get { return _al; }
            set { _al = value; }
        }

        #endregion Tb_UseForAudit


        #region Tb_IsAllowDelete

        private Boolean _am;
//        public Boolean AM
//        {
//            get { return _am; }
//            set { _am = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_IsAllowDelete
        {
            get { return _am; }
            set { _am = value; }
        }

        #endregion Tb_IsAllowDelete


        #region Tb_CnfgGdMnu_MenuRow_IsVisible

        private Boolean _an;
//        public Boolean AN
//        {
//            get { return _an; }
//            set { _an = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_CnfgGdMnu_MenuRow_IsVisible
        {
            get { return _an; }
            set { _an = value; }
        }

        #endregion Tb_CnfgGdMnu_MenuRow_IsVisible


        #region Tb_CnfgGdMnu_GridTools_IsVisible

        private Boolean _ao;
//        public Boolean AO
//        {
//            get { return _ao; }
//            set { _ao = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_CnfgGdMnu_GridTools_IsVisible
        {
            get { return _ao; }
            set { _ao = value; }
        }

        #endregion Tb_CnfgGdMnu_GridTools_IsVisible


        #region Tb_CnfgGdMnu_SplitScreen_IsVisible

        private Boolean _ap;
//        public Boolean AP
//        {
//            get { return _ap; }
//            set { _ap = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_CnfgGdMnu_SplitScreen_IsVisible
        {
            get { return _ap; }
            set { _ap = value; }
        }

        #endregion Tb_CnfgGdMnu_SplitScreen_IsVisible


        #region Tb_CnfgGdMnu_SplitScreen_IsSplit_Default

        private Boolean _aq;
//        public Boolean AQ
//        {
//            get { return _aq; }
//            set { _aq = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
        {
            get { return _aq; }
            set { _aq = value; }
        }

        #endregion Tb_CnfgGdMnu_SplitScreen_IsSplit_Default


        #region Tb_CnfgGdMnu_NavColumnWidth

        private Int32 _ar;
//        public Int32 AR
//        {
//            get { return _ar; }
//            set { _ar = value; }
//        }

        //[NonSerialized]
        public Int32 Tb_CnfgGdMnu_NavColumnWidth
        {
            get { return _ar; }
            set { _ar = value; }
        }

        #endregion Tb_CnfgGdMnu_NavColumnWidth


        #region Tb_CnfgGdMnu_IsReadOnly

        private Boolean _asx;
//        public Boolean ASX
//        {
//            get { return _asx; }
//            set { _asx = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_CnfgGdMnu_IsReadOnly
        {
            get { return _asx; }
            set { _asx = value; }
        }

        #endregion Tb_CnfgGdMnu_IsReadOnly


        #region Tb_CnfgGdMnu_IsAllowNewRow

        private Boolean _at;
//        public Boolean AT
//        {
//            get { return _at; }
//            set { _at = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_CnfgGdMnu_IsAllowNewRow
        {
            get { return _at; }
            set { _at = value; }
        }

        #endregion Tb_CnfgGdMnu_IsAllowNewRow


        #region Tb_CnfgGdMnu_ExcelExport_IsVisible

        private Boolean _au;
//        public Boolean AU
//        {
//            get { return _au; }
//            set { _au = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_CnfgGdMnu_ExcelExport_IsVisible
        {
            get { return _au; }
            set { _au = value; }
        }

        #endregion Tb_CnfgGdMnu_ExcelExport_IsVisible


        #region Tb_CnfgGdMnu_ColumnChooser_IsVisible

        private Boolean _av;
//        public Boolean AV
//        {
//            get { return _av; }
//            set { _av = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_CnfgGdMnu_ColumnChooser_IsVisible
        {
            get { return _av; }
            set { _av = value; }
        }

        #endregion Tb_CnfgGdMnu_ColumnChooser_IsVisible


        #region Tb_CnfgGdMnu_ShowHideColBtn_IsVisible

        private Boolean _aw;
//        public Boolean AW
//        {
//            get { return _aw; }
//            set { _aw = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
        {
            get { return _aw; }
            set { _aw = value; }
        }

        #endregion Tb_CnfgGdMnu_ShowHideColBtn_IsVisible


        #region Tb_CnfgGdMnu_RefreshGrid_IsVisible

        private Boolean _ax;
//        public Boolean AX
//        {
//            get { return _ax; }
//            set { _ax = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_CnfgGdMnu_RefreshGrid_IsVisible
        {
            get { return _ax; }
            set { _ax = value; }
        }

        #endregion Tb_CnfgGdMnu_RefreshGrid_IsVisible


        #region Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible

        private Boolean _ay;
//        public Boolean AY
//        {
//            get { return _ay; }
//            set { _ay = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
        {
            get { return _ay; }
            set { _ay = value; }
        }

        #endregion Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible


        #region Tb_IsInputComplete

        private Boolean _az;
//        public Boolean AZ
//        {
//            get { return _az; }
//            set { _az = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_IsInputComplete
        {
            get { return _az; }
            set { _az = value; }
        }

        #endregion Tb_IsInputComplete


        #region Tb_IsCodeGen

        private Boolean _ba;
//        public Boolean BA
//        {
//            get { return _ba; }
//            set { _ba = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_IsCodeGen
        {
            get { return _ba; }
            set { _ba = value; }
        }

        #endregion Tb_IsCodeGen


        #region Tb_IsReadyCodeGen

        private Boolean _bb;
//        public Boolean BB
//        {
//            get { return _bb; }
//            set { _bb = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_IsReadyCodeGen
        {
            get { return _bb; }
            set { _bb = value; }
        }

        #endregion Tb_IsReadyCodeGen


        #region Tb_IsCodeGenComplete

        private Boolean _bc;
//        public Boolean BC
//        {
//            get { return _bc; }
//            set { _bc = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_IsCodeGenComplete
        {
            get { return _bc; }
            set { _bc = value; }
        }

        #endregion Tb_IsCodeGenComplete


        #region Tb_IsTagForCodeGen

        private Boolean _bd;
//        public Boolean BD
//        {
//            get { return _bd; }
//            set { _bd = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_IsTagForCodeGen
        {
            get { return _bd; }
            set { _bd = value; }
        }

        #endregion Tb_IsTagForCodeGen


        #region Tb_IsTagForOther

        private Boolean _be;
//        public Boolean BE
//        {
//            get { return _be; }
//            set { _be = value; }
//        }

        //[NonSerialized]
        public Boolean Tb_IsTagForOther
        {
            get { return _be; }
            set { _be = value; }
        }

        #endregion Tb_IsTagForOther


        #region Tb_TableGroups

        private String _bf;
//        public String BF
//        {
//            get { return _bf; }
//            set { _bf = value; }
//        }

        //[NonSerialized]
        public String Tb_TableGroups
        {
            get { return _bf; }
            set { _bf = value; }
        }

        #endregion Tb_TableGroups


        #region DbCnSK_Server

        private String _bg;
//        public String BG
//        {
//            get { return _bg; }
//            set { _bg = value; }
//        }

        //[NonSerialized]
        public String DbCnSK_Server
        {
            get { return _bg; }
            set { _bg = value; }
        }

        #endregion DbCnSK_Server


        #region DbCnSK_Database

        private String _bh;
//        public String BH
//        {
//            get { return _bh; }
//            set { _bh = value; }
//        }

        //[NonSerialized]
        public String DbCnSK_Database
        {
            get { return _bh; }
            set { _bh = value; }
        }

        #endregion DbCnSK_Database


        #region DbCnSK_UserName

        private String _bi;
//        public String BI
//        {
//            get { return _bi; }
//            set { _bi = value; }
//        }

        //[NonSerialized]
        public String DbCnSK_UserName
        {
            get { return _bi; }
            set { _bi = value; }
        }

        #endregion DbCnSK_UserName


        #region DbCnSK_Password

        private String _bj;
//        public String BJ
//        {
//            get { return _bj; }
//            set { _bj = value; }
//        }

        //[NonSerialized]
        public String DbCnSK_Password
        {
            get { return _bj; }
            set { _bj = value; }
        }

        #endregion DbCnSK_Password


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11}; {12}; {13}; {14}; {15}; {16}; {17}; {18}; {19}; {20}; {21}; {22}; {23}; {24}; {25}; {26}; {27}; {28}; {29}; {30}; {31}; {32}; {33}; {34}; {35}; {36}; {37}; {38}; {39}; {40}; {41}; {42}; {43}; {44}; {45}; {46}; {47}; {48}; {49}; {50}; {51}; {52}; {53}; {54}; {55}; {56}; {57}; {58}; {59}; {60}; {61} ", Tb_Id, Tb_ApVrsn_Id, Tb_Name, Tb_EntityRootName, Tb_VariableName, Tb_ScreenCaption, Tb_Description, Tb_DevelopmentNote, Tb_UseLegacyConnectionCode, Tb_DbCnSK_Id, Tb_DbCnSK_Id_TextField, Tb_ApDbScm_Id, Tb_ApDbScm_Id_TextField, Tb_UIAssemblyName, Tb_UINamespace, Tb_UIAssemblyPath, Tb_ProxyAssemblyName, Tb_ProxyNamespace, Tb_ProxyAssemblyPath, Tb_WireTypeAssemblyName, Tb_WireTypeNamespace, Tb_WireTypePath, Tb_WebServiceName, Tb_WebServiceFolder, Tb_DataServiceAssemblyName, Tb_DataServiceNamespace, Tb_DataServicePath, Tb_UseTilesInPropsScreen, Tb_UseGridColumnGroups, Tb_UseGridDataSourceCombo, Tb_PkIsIdentity, Tb_IsVirtual, Tb_IsScreenPlaceHolder, Tb_IsNotEntity, Tb_IsMany2Many, Tb_UseLastModifiedByUserNameInSproc, Tb_UseUserTimeStamp, Tb_UseForAudit, Tb_IsAllowDelete, Tb_CnfgGdMnu_MenuRow_IsVisible, Tb_CnfgGdMnu_GridTools_IsVisible, Tb_CnfgGdMnu_SplitScreen_IsVisible, Tb_CnfgGdMnu_SplitScreen_IsSplit_Default, Tb_CnfgGdMnu_NavColumnWidth, Tb_CnfgGdMnu_IsReadOnly, Tb_CnfgGdMnu_IsAllowNewRow, Tb_CnfgGdMnu_ExcelExport_IsVisible, Tb_CnfgGdMnu_ColumnChooser_IsVisible, Tb_CnfgGdMnu_ShowHideColBtn_IsVisible, Tb_CnfgGdMnu_RefreshGrid_IsVisible, Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible, Tb_IsInputComplete, Tb_IsCodeGen, Tb_IsReadyCodeGen, Tb_IsCodeGenComplete, Tb_IsTagForCodeGen, Tb_IsTagForOther, Tb_TableGroups, DbCnSK_Server, DbCnSK_Database, DbCnSK_UserName, DbCnSK_Password );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11}; {12}; {13}; {14}; {15}; {16}; {17}; {18}; {19}; {20}; {21}; {22}; {23}; {24}; {25}; {26}; {27}; {28}; {29}; {30}; {31}; {32}; {33}; {34}; {35}; {36}; {37}; {38}; {39}; {40}; {41}; {42}; {43}; {44}; {45}; {46}; {47}; {48}; {49}; {50}; {51}; {52}; {53}; {54}; {55}; {56}; {57}; {58}; {59}; {60}; {61} ", Tb_Id, Tb_ApVrsn_Id, Tb_Name, Tb_EntityRootName, Tb_VariableName, Tb_ScreenCaption, Tb_Description, Tb_DevelopmentNote, Tb_UseLegacyConnectionCode, Tb_DbCnSK_Id, Tb_DbCnSK_Id_TextField, Tb_ApDbScm_Id, Tb_ApDbScm_Id_TextField, Tb_UIAssemblyName, Tb_UINamespace, Tb_UIAssemblyPath, Tb_ProxyAssemblyName, Tb_ProxyNamespace, Tb_ProxyAssemblyPath, Tb_WireTypeAssemblyName, Tb_WireTypeNamespace, Tb_WireTypePath, Tb_WebServiceName, Tb_WebServiceFolder, Tb_DataServiceAssemblyName, Tb_DataServiceNamespace, Tb_DataServicePath, Tb_UseTilesInPropsScreen, Tb_UseGridColumnGroups, Tb_UseGridDataSourceCombo, Tb_PkIsIdentity, Tb_IsVirtual, Tb_IsScreenPlaceHolder, Tb_IsNotEntity, Tb_IsMany2Many, Tb_UseLastModifiedByUserNameInSproc, Tb_UseUserTimeStamp, Tb_UseForAudit, Tb_IsAllowDelete, Tb_CnfgGdMnu_MenuRow_IsVisible, Tb_CnfgGdMnu_GridTools_IsVisible, Tb_CnfgGdMnu_SplitScreen_IsVisible, Tb_CnfgGdMnu_SplitScreen_IsSplit_Default, Tb_CnfgGdMnu_NavColumnWidth, Tb_CnfgGdMnu_IsReadOnly, Tb_CnfgGdMnu_IsAllowNewRow, Tb_CnfgGdMnu_ExcelExport_IsVisible, Tb_CnfgGdMnu_ColumnChooser_IsVisible, Tb_CnfgGdMnu_ShowHideColBtn_IsVisible, Tb_CnfgGdMnu_RefreshGrid_IsVisible, Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible, Tb_IsInputComplete, Tb_IsCodeGen, Tb_IsReadyCodeGen, Tb_IsCodeGenComplete, Tb_IsTagForCodeGen, Tb_IsTagForOther, Tb_TableGroups, DbCnSK_Server, DbCnSK_Database, DbCnSK_UserName, DbCnSK_Password );
        }

    }

}
