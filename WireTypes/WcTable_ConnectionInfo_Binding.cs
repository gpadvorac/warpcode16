using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx;

// Gen Timestamp:  1/17/2018 10:14:45 PM

namespace EntityWireType
{


    
    public class WcTable_ConnectionInfo_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTable_ConnectionInfo_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcTable_ConnectionInfo_Binding() { }


        public WcTable_ConnectionInfo_Binding(Guid _Tb_Id, String _DbCnSK_Server, String _DbCnSK_Database, String _DbCnSK_UserName, String _DbCnSK_Password )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ConnectionInfo_Binding", IfxTraceCategory.Enter);
				_a = _Tb_Id;
				_b = _DbCnSK_Server;
				_c = _DbCnSK_Database;
				_d = _DbCnSK_UserName;
				_e = _DbCnSK_Password;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ConnectionInfo_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ConnectionInfo_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcTable_ConnectionInfo_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ConnectionInfo_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  Tb_Id
				_b = (String)data[1];                //  DbCnSK_Server
				_c = (String)data[2];                //  DbCnSK_Database
				_d = (String)data[3];                //  DbCnSK_UserName
				_e = (String)data[4];                //  DbCnSK_Password
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ConnectionInfo_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ConnectionInfo_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region Tb_Id

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid Tb_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Tb_Id


        #region DbCnSK_Server

        private String _b;
//        public String B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public String DbCnSK_Server
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion DbCnSK_Server


        #region DbCnSK_Database

        private String _c;
//        public String C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public String DbCnSK_Database
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion DbCnSK_Database


        #region DbCnSK_UserName

        private String _d;
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String DbCnSK_UserName
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion DbCnSK_UserName


        #region DbCnSK_Password

        private String _e;
//        public String E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public String DbCnSK_Password
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion DbCnSK_Password


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4} ", Tb_Id, DbCnSK_Server, DbCnSK_Database, DbCnSK_UserName, DbCnSK_Password );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4} ", Tb_Id, DbCnSK_Server, DbCnSK_Database, DbCnSK_UserName, DbCnSK_Password );
        }

    }

}
