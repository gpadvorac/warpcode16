﻿

using System;
using System.Collections.Generic;
using System.Windows.Controls;
using WarpCode;

namespace ApplicationTypeServices
{
    public class ApplicationLevelVariables
    {




        #region Velocity Framework Related




        static IMainPage _mainPg = null;
        public static IMainPage MainPg
        {
            get { return ApplicationLevelVariables._mainPg; }
            set { ApplicationLevelVariables._mainPg = value; }
        }

        static Grid _mainGrid = null;
        public static Grid MainGrid
        {
            get { return ApplicationLevelVariables._mainGrid; }
            set { ApplicationLevelVariables._mainGrid = value; }
        }
        private static bool _isShowSizeInHeader = false;

        public static bool IsShowSizeInHeader
        {
            get { return _isShowSizeInHeader; }
            set { _isShowSizeInHeader = value; }
        }

        static Guid? _licenseeId = null;
        public static Guid? LicenseeId
        {
            get { return ApplicationLevelVariables._licenseeId; }
            set { ApplicationLevelVariables._licenseeId = value; }
        }



        static bool _isTopDev = false;

        /// <summary>
        /// User is logged in as the Super Admin
        /// </summary>
        public static bool IsTopDev
        {
            get { return _isTopDev; }
            set { _isTopDev = value; }
        }

        static bool _useLicenseMode = false;
        public static bool UseLicenseMode
        {
            get { return _useLicenseMode; }
            set { _useLicenseMode = value; }
        }

        static bool _userHasValidLicense = false;
        public static bool UserHasValidLicense
        {
            get { return _userHasValidLicense; }
            set { _userHasValidLicense = value; }
        }

        static bool _userHasRoles = false;
        public static bool UserHasRoles
        {
            get { return _userHasRoles; }
            set { _userHasRoles = value; }
        }


        static bool _isViewLog_LoggingOn = false;
        public static bool IsViewLog_LoggingOn
        {
            get { return ApplicationLevelVariables._isViewLog_LoggingOn; }
            set { ApplicationLevelVariables._isViewLog_LoggingOn = value; }
        }

        static bool _showLogin = true;
        public static bool ShowLogin
        {
            get { return _showLogin; }
            set { _showLogin = value; }
        }


        static string _reportServerURL = "";
        public static string ReportServerURL
        {
            get { return ApplicationLevelVariables._reportServerURL; }
            set { ApplicationLevelVariables._reportServerURL = value; }
        }


        public static Guid IsTraceLoggingAllowed_Id = new Guid("1165225a-8ca2-4389-9841-3d204e008f64");
        static bool _isTraceLoggingAllowed = false;
        public static bool IsTraceLoggingAllowed
        {
            get { return ApplicationLevelVariables._isTraceLoggingAllowed; }
            set { ApplicationLevelVariables._isTraceLoggingAllowed = value; }
        }






        #region Column Groups

        static List<GridColumnGroupTypePreference> _gridColumnGroupTypePreferenceList = new List<GridColumnGroupTypePreference>();

        public static List<GridColumnGroupTypePreference> GridColumnGroupTypePreferenceList
        {
            get { return ApplicationLevelVariables._gridColumnGroupTypePreferenceList; }
            set { ApplicationLevelVariables._gridColumnGroupTypePreferenceList = value; }
        }

        public class GridColumnGroupTypePreference
        {
            Guid _gridId;
            int _groupType;

            public GridColumnGroupTypePreference(Guid gridId, int groupType)
            {
                _gridId = gridId;
                _groupType = groupType;
            }

            public GridColumnGroupTypePreference(object[] data)
            {
                _gridId = (Guid)data[0];
                _groupType = (int)data[1];
            }


            public Guid GridId
            {
                get { return _gridId; }
                set { _gridId = value; }
            }

            public int GroupType
            {
                get { return _groupType; }
                set { _groupType = value; }
            }
        }

        public static Guid IsAllowEditSystemColumnGroups_Id = new Guid("f6279e13-e430-4d53-8a6a-54f42ab8457c");
        static bool _isAllowEditSystemColumnGroups = false;
        public static bool IsAllowEditSystemColumnGroups
        {
            get { return ApplicationLevelVariables._isAllowEditSystemColumnGroups; }
            set { ApplicationLevelVariables._isAllowEditSystemColumnGroups = value; }
        }

        #endregion Column Groups



        #endregion Velocity Framework Related










        
        private static string _mapProxyURL = null;


        



        #region Special Variables for Pipeline


        public static Guid IsAndysRoomLink_Id = new Guid("decd415b-d8e4-492a-b080-44cd4675dcc2");
        static bool _isAndysRoomLink = false;
        public static bool IsAndysRoomLink
        {
            get { return ApplicationLevelVariables._isAndysRoomLink; }
            set { ApplicationLevelVariables._isAndysRoomLink = value; }
        }



        public static Guid IsAndysRoom_Id = new Guid("04931582-2b60-4107-836d-b24134fcf525");
        static bool _isAndysRoom = false;
        public static bool IsAndysRoom
        {
            get { return ApplicationLevelVariables._isAndysRoom; }
            set { ApplicationLevelVariables._isAndysRoom = value; }
        }

        static bool _isShowAndysRoomLink = false;
        public static bool IsShowAndysRoomLink
        {
            get { return ApplicationLevelVariables._isShowAndysRoomLink; }
            set { ApplicationLevelVariables._isShowAndysRoomLink = value; }
        }



        #endregion  Special Variables for Pipeline



        public static Guid IsDeleteDataAllowed_Id = new Guid("b0bafc53-9f06-413b-926a-2584d954da49");
        static bool _isDeleteDataAllowed = false;
        public static bool IsDeleteDataAllowed
        {
            get { return ApplicationLevelVariables._isDeleteDataAllowed; }
            set { ApplicationLevelVariables._isDeleteDataAllowed = value; }
        }

        public static Guid IsApproved_WF1_Id = new Guid("6dbcca35-9045-4b71-9286-7517c401fef3");
        static bool _isApproved_WF1 = false;
        public static bool IsApproved_WF1
        {
            get { return ApplicationLevelVariables._isApproved_WF1; }
            set { ApplicationLevelVariables._isApproved_WF1 = value; }
        }

        public static Guid IsApproved_WF2_Id = new Guid("760aa0fb-4e6d-4f5d-a883-e9f01efd0a51");
        static bool _isApproved_WF2 = false;
        public static bool IsApproved_WF2
        {
            get { return ApplicationLevelVariables._isApproved_WF2; }
            set { ApplicationLevelVariables._isApproved_WF2 = value; }
        }

        public static Guid IsApproved_WF3_Id = new Guid("fe92ea42-188d-467c-83f9-5f048f786446");
        static bool _isApproved_WF3 = false;
        public static bool IsApproved_WF3
        {
            get { return ApplicationLevelVariables._isApproved_WF3; }
            set { ApplicationLevelVariables._isApproved_WF3 = value; }
        }

        public static Guid IsApproved_WF4_Id = new Guid("09d6fb72-e3ea-4553-9571-973ea1e647a2");
        static bool _isApproved_WF4 = false;
        public static bool IsApproved_WF4
        {
            get { return ApplicationLevelVariables._isApproved_WF4; }
            set { ApplicationLevelVariables._isApproved_WF4 = value; }
        }

        



        public static string MapProxyUrl
        {
            get { return _mapProxyURL; }
            set { _mapProxyURL = value; }
        }


        public class DiscussionParentTypes
        {
            public static string NotAssigned = "NotAssigned";
            public static string Application = "Application";
            public static string ApplicationVersion = "ApplicationVersion";
            public static string Table = "Table";
            //public static string Lease = "Lease";
            //public static string Obligation = "Obligation";
            public static string Task = "Task";
            //public static string Well = "Well";
        }



    }
}
