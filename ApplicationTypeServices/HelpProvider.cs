﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Browser;

namespace ApplicationTypeServices
{
    public class HelpProvider
    {


        /*
        Help resources
// http://blogs.msdn.com/b/mikehillberg/archive/2007/07/26/a-context-sensitive-help-provider-in-wpf.aspx         * 
         * 
         * 

        */

        static string _helpUrl = "";

        public static string HelpUrl
        {
            get { return HelpProvider._helpUrl; }
            set { HelpProvider._helpUrl = value; }
        }





        #region Dependency Properties

        #region Source

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.RegisterAttached(
            "Source",
            typeof(Uri),
            typeof(HelpProvider),
            new PropertyMetadata(OnSourcePropertyChanged));

        public static void OnSourcePropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null && e.NewValue is Uri)
            {
                FrameworkElement element = sender as FrameworkElement;
                element.KeyDown += new KeyEventHandler(element_KeyDown);
            }
        }

        #endregion Source




        #region HelpIndex


        public static int? GetHelpIndex(DependencyObject obj)
        {
            try
            {
                return (int?)obj.GetValue(HelpIndexProperty);
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public static void SetHelpIndex(DependencyObject obj, int value)
        {
            obj.SetValue(HelpIndexProperty, value);
        }

        public static readonly DependencyProperty HelpIndexProperty =
            DependencyProperty.RegisterAttached(
            "HelpIndex",
            typeof(int),
            typeof(HelpProvider),
            new PropertyMetadata(OnHelpIndexPropertyChanged));

        public static void OnHelpIndexPropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null && e.NewValue is int)
            {
                FrameworkElement element = sender as FrameworkElement;
                element.KeyDown += new KeyEventHandler(element_KeyDown);
            }
        }


        #endregion HelpIndex


        #endregion Dependency Properties


        static void element_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                e.Handled = true;
                e = null;

                int? index = GetHelpIndex(sender as DependencyObject);

                if (index != null)
                {
                    ShowHelpFile((int)index);
                }
            }
        }



        public static void ShowHelpFile()
        {
            try
            {
                if (_helpUrl.Length > 0)
                {
                    Uri source = new Uri(_helpUrl, UriKind.Absolute);
                    HtmlPage.Window.Navigate(source, "new");
                }
            }
            catch (Exception ex)
            {

            }
        }


        private static void ShowHelpFile(Uri source)
        {
            HtmlPage.Window.Navigate(source, "new");
        }

        private static void ShowHelpFile(int index)
        {
            try
            {
                if (_helpUrl.Length > 0)
                {
                    Uri source = new Uri(_helpUrl + "?" + index.ToString(), UriKind.Absolute);
                    HtmlPage.Window.Navigate(source, "new");
                }
            }
            catch (Exception ex)
            {

            }
        }


    }
}
