﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace ApplicationTypeServices
{





    #region ContextMenuButtonColumnItemsSelected


    public delegate void ContextMenuButtonColumnItemsSelectedEventHandler(object sender, ContextMenuButtonColumnItemsSelectedArgs e);

    public class ContextMenuButtonColumnItemsSelectedArgs : EventArgs
    {
        private readonly object _businessObjectId;
        private readonly string _menuItem;

        public ContextMenuButtonColumnItemsSelectedArgs(object businessObjectId, string menuItem)
        {
            _businessObjectId = businessObjectId;
            _menuItem = menuItem;
        }


        public object BusinessObjectId
        {
            get { return _businessObjectId; }
        }



        public string MenuItem
        {
            get { return _menuItem; }
        }

    }

    #endregion ContextMenuButtonColumnItemsSelected













}
