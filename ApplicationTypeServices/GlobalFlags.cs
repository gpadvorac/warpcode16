﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace ApplicationTypeServices
{
    public static class GlobalFlags
    {


        static bool _isLoadingStaticLists = false;
        static bool _useSerializedLeaseData = false;

        public static bool UseSerializedLeaseData
        {
            get { return GlobalFlags._useSerializedLeaseData; }
            set { GlobalFlags._useSerializedLeaseData = value; }
        }

        public static bool IsLoadingStaticLists
        {
            get { return GlobalFlags._isLoadingStaticLists; }
            set { GlobalFlags._isLoadingStaticLists = value; }
        }

    }
}
