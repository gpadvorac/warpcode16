﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace ApplicationTypeServices
{
    public class ContextValues
    {

        static Guid? _currentProjectId = null;
        static string _currentProjectName = null;
        static Guid? _currentApId = null;
        static Guid? _currentApVersionId = null;



        public static Guid? CurrentProjectId
        {
            get { return ContextValues._currentProjectId; }
            set { ContextValues._currentProjectId = value; }
        }

        public static string CurrentProjectName
        {
            get { return ContextValues._currentProjectName; }
            set { ContextValues._currentProjectName = value; }
        }

        public static Guid? CurrentApId
        {
            get { return _currentApId; }
            set { _currentApId = value; }
        }

        public static Guid? CurrentApVersionId
        {
            get { return _currentApVersionId; }
            set { _currentApVersionId = value; }
        }


    }
}
