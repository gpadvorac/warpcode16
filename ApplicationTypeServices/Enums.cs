﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace ApplicationTypeServices
{


    public enum EntityType
    {
        None,
        Project,
        RelatedContract,
        Contract,
        ContractDt,
        Lease,
        LeaseDt,
        Well,
        WellDt,
        Party,
        Operator,
        Prospect,
        Property,
        Field,
        Defect,
        Correspondence,
        Obligation,
        Payout,
        File,
        Task,
        SurveyMap,
        AndysRoom
    }

    //public enum DiscussionViewContext
    //{
    //    General,
    //    Owner,
    //    Member
    //}

    public enum vFilterFetchParameterSource
    {
        Project = 1,
        User = 2,
        SomethingElse = 3
    }

    //public enum ReportType
    //{
    //    None,
    //    SSRS,
    //    Excel
    //    //LoadingStub,
    //    //NoItemsStub
    //}

    
    public enum FileStorageParentType
    {
        None = 1,
        Project = 2,
        Lease = 3,
        Contract = 4,
        Well = 5,
        Obligation = 6,
        Defect = 7,
        SurveyMap = 8,
        RelatedContract = 9,
        Task = 10,
        DiscussionComment=11
    }




}




