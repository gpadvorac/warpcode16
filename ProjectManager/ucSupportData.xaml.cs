﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace ProjectManager
{
    public partial class ucSupportData : UserControl
    {

        #region Initialize Variables

        private static string _as = "ProjectManager";
        private static string _cn = "ucSupportData";
        
        public event SupportFormSelectedEventHandler SupportFormSelected;

        #endregion Initialize Variables



        public ucSupportData()
        {
            InitializeComponent();
        }


        #region Events

        private void EntityLink_Click(object sender, RoutedEventArgs e)
        {
            HyperlinkButton hb = sender as HyperlinkButton;
            if (hb == null)
            {
                MessageBox.Show("Could not cast button as HyperlinkButton", "Error", MessageBoxButton.OK);
                return;
            }
            SupportFormType type = SupportFormType.None;

            string lbl = hb.Tag.ToString();

            

            switch (lbl)
            {
                case "Person":
                    type = SupportFormType.Person;
                    break;
                case "Project":
                    type = SupportFormType.Project;
                    break;
                case "TaskCategory":
                    type = SupportFormType.TaskCategory;
                    break;
            }
            RaiseSupportFormSelected(type);
        }

        void RaiseSupportFormSelected(SupportFormType typeSelected)
        {
            SupportFormSelectedArgs e = new SupportFormSelectedArgs(typeSelected);
            SupportFormSelectedEventHandler handler = SupportFormSelected;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion Events


    }


    public enum SupportFormType
    {
        None,
        Person,
        Project,
        TaskCategory
    }




    #region SupportFormSelected Event

    public delegate void SupportFormSelectedEventHandler(object sender, SupportFormSelectedArgs e);


    public class SupportFormSelectedArgs : EventArgs
    {
        private readonly Guid? _projectId;
        private readonly SupportFormType _typeSelected;



        public SupportFormSelectedArgs( SupportFormType typeSelected)
        {
            this._typeSelected = typeSelected;
        }

        public SupportFormType TypeSelected
        {
            get { return _typeSelected; }
        }


    }

    #endregion SupportFormSelected Event





}
