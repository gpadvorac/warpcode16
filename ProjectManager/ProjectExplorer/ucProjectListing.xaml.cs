﻿using System;
using System.Windows;
using System.Windows.Controls;
using TypeServices;
using Velocity.SL;
//using vSecurityClientAuthenticationSL;
using System.ComponentModel;
//using ProjectManager.Resources;
using Ifx.SL;
using vUICommon;
using EntityBll.SL;
//using vAdmin;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
//using IfxConfigSL;

using EntityWireTypeSL;
using System.Collections.ObjectModel;
using System.Diagnostics;
using UIControls;
using System.Windows.Media;
using Infragistics.Controls.Menus;
using ApplicationTypeServices;

namespace ProjectManager
{
    public partial class ucProjectListing : UserControl
    {

        #region Initialize Variables


        private static string _as = "Pipeline";
        private static string _cn = "ucProjectListing";

        public event EntitySelectedEventHandler EntitySelected;

        ProxyWrapper.CommonClientDataService_ProxyWrapper _genProxy = null;


        #endregion Initialize Variables



        public ucProjectListing()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectListing", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();

                //_genProxy = new ProxyWrapper.CommonClientDataService_ProxyWrapper();
                ////_genProxy.GetProjExp_lstProjects_ActiveCompleted += new EventHandler<GetProjExp_lstProjects_ActiveCompletedEventArgs>(GetProjExp_lstProjects_ActiveCompleted);
                //_genProxy.GetProjExp_lstProjects_ActiveProjectsByUserAccessCompleted += new EventHandler<GetProjExp_lstProjects_ActiveProjectsByUserAccessCompletedEventArgs>(GetProjExp_lstProjects_ActiveProjectsByUserAccessCompleted);
                ////_genProxy.GetProjExp_lstGroupsCompleted += new EventHandler<GetProjExp_lstGroupsCompletedEventArgs>(GetProjExp_lstGroupsCompleted);

                ////LoadProjects();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectListing", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectListing", IfxTraceCategory.Leave);
            }
        }







        #region Load Projects


        bool _isProjectsLoaded = false;

        public bool IsProjectsLoaded
        {
            get { return _isProjectsLoaded; }
            set { _isProjectsLoaded = value; }
        }

        public void LoadProjects()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadProjects", IfxTraceCategory.Enter);
                if (_isProjectsLoaded == false)
                {
                    //_genProxy.Begin_GetProjExp_lstProjects_ActiveProjectsByUserAccess((Guid)Credentials.UserId);
                    //_isProjectsLoaded = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadProjects", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadProjects", IfxTraceCategory.Leave);
            }
        }

        //void GetProjExp_lstProjects_ActiveCompleted(object sender, GetProjExp_lstProjects_ActiveCompletedEventArgs e)
        //{
        //    byte[] data = e.Result;
        //    object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
        //    if (array != null)
        //    {
        //        LoadProjectsToTree(array);
        //    }
        //}

        //void GetProjExp_lstProjects_ActiveProjectsByUserAccessCompleted(object sender, GetProjExp_lstProjects_ActiveProjectsByUserAccessCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstProjects_ActiveProjectsByUserAccessCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;
        //        object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
        //        if (array != null)
        //        {
        //            LoadProjectsToTree(array);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstProjects_ActiveProjectsByUserAccessCompleted", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstProjects_ActiveProjectsByUserAccessCompleted", IfxTraceCategory.Leave);
        //    }
        //}



        void LoadProjectsToTree(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadProjectsToTree", IfxTraceCategory.Enter);
                //ObservableCollection<ProjectExplorerNode> list = new ObservableCollection<ProjectExplorerNode>();
                //ProjectExplorerNode obj;
                //int cnt = 0;
                List<HyperlinkButton> list = new List<HyperlinkButton>();
                for (int i = 0; i <= data.GetUpperBound(0); i++)
                {

                    HyperlinkButton lb = new HyperlinkButton();
                    lb.Content = (string)((object[])data[i])[1];
                    lb.Tag = (Guid)((object[])data[i])[0];
                    lb.Click += new RoutedEventHandler(ProjectLink_Click);
                    list.Add(lb);

                    //obj = new ProjectExplorerNode((Guid)((object[])data[i])[0], NodeType.Entity, EntityType.Project, 0, (string)((object[])data[i])[1], null, (Guid)((object[])data[i])[0], null, EntityType.None, null);
                    //obj.Children = GetNewStubLoadingList();  // Keep this one, but use the next line for now.
                    ////obj.Children = TempGetGroupNodesForProject();
                    //list.Add(obj);
                    //////cnt = i;
                }
                lbxProjects.ItemsSource = list;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadProjectsToTree", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadProjectsToTree", IfxTraceCategory.Leave);
            }
        }


        public void ClearProjects()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearProjects", IfxTraceCategory.Enter);

                lbxProjects.ItemsSource = null;
                //gdEntities.Visibility = System.Windows.Visibility.Collapsed;
                _isProjectsLoaded = false;
                //if (_isProjectsLoaded == false)
                //{
                //    _genProxy.Begin_GetProjExp_lstProjects_Active();
                //    _isProjectsLoaded = true;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearProjects", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearProjects", IfxTraceCategory.Leave);
            }
        }


        #endregion Load Projects


        //#region Entities


        //private void SetEntitiesVisibility(Visibility visibility)
        //{
        //    gdEntities.Visibility = visibility;
        //}



        //void GetProjExp_lstGroupsCompleted(object sender, GetProjExp_lstGroupsCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstGroupsCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;
        //        object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
        //        if (array != null)
        //        {
        //            LoadEntityGroupsToTree(array);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstGroupsCompleted", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstGroupsCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void LoadEntityGroupsToTree(object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadEntityGroupsToTree", IfxTraceCategory.Enter);
        //        //ProjectExplorerNode obj;
        //        //ObservableCollection<ProjectExplorerNode> children = ((ProjectExplorerNode)_activeNode.Data).Children;

        //        ////  WATCH OUT !!!!  DONT CALL THIS LINE  !!!
        //        ////children.Clear();

        //        //// Hack   Hack   Hack   Hack   ...
        //        ////  use this to clear the children so we dont get a nasty exception.
        //        ////  for some dumb reason, _children.Clear(); will clear the children, but then we get an exception.
        //        //((ProjectExplorerNode)_activeNode.Data).ClearChildren();

        //        gdEntities.Visibility = System.Windows.Visibility.Visible;

        //        for (int i = 0; i <= data.GetUpperBound(0); i++)
        //        {
        //            string entity = (string)((object[])data[i])[0];
        //            int cnt = (int)((object[])data[i])[1];
        //            string cntFormat = "";
        //            if (cnt > 0)
        //            {
        //                cntFormat = "(" + cnt.ToString("#,##0") + ")";
        //            }

        //            switch (entity)
        //            {
        //                case "Contract":
        //                    lnkContracts.Content = "Contracts  " + cntFormat;
        //                    break;
        //                case "Lease":
        //                    lnkLeases.Content = "Leases  " + cntFormat;
        //                    break;
        //                case "Well":
        //                    lnkWells.Content = "Wells  " + cntFormat;
        //                    break;
        //                //case "Party":
        //                //    txbPartyCount.Text = cntFormat;
        //                //    break;
        //                //case "Operator":
        //                //    txbOperatorCount.Text = cntFormat;
        //                //    break;
        //                //case "xx3x":
        //                //    txbPropertyCount.Text = cntFormat;
        //                //    break;
        //                case "Prospect":
        //                    lnkProspects.Content = "Prospects  " + cntFormat;
        //                    break;
        //                //case "x5xx":
        //                //    txbFieldCount.Text = cntFormat;
        //                //    break;
        //                case "Question/Defect":
        //                    lnkDefects.Content = "Questions/Defects  " + cntFormat;
        //                    break;
        //                case "Obligation":
        //                    lnkObligations.Content = "Obligations/Provisions  " + cntFormat;
        //                    break;
        //                case "Payout":
        //                    lnkPayouts.Content = "Payouts  " + cntFormat;
        //                    break;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadEntityGroupsToTree", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadEntityGroupsToTree", IfxTraceCategory.Leave);
        //    }
        //}





        //#endregion Entities


        #region Events

        private void ProjectLink_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ProjectLink_Click", IfxTraceCategory.Enter);
                HyperlinkButton hb = sender as HyperlinkButton;
                if (hb == null)
                {
                    MessageBox.Show("Could not cast button as HyperlinkButton", "Error", MessageBoxButton.OK);
                    return;
                }

                Guid id = new Guid(hb.Tag.ToString());
                string lbl = hb.Content.ToString();
                ContextValues.CurrentProjectName = lbl;
                //txbProject.Text = lbl;
                //_genProxy.Begin_GetProjExp_lstGroups(id, "Project");
                RaiseEntitySelected(id, lbl, EntityType.Project);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ProjectLink_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ProjectLink_Click", IfxTraceCategory.Leave);
            }
        }


        //private void EntityLink_Click(object sender, RoutedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityLink_Click", IfxTraceCategory.Enter);
        //        HyperlinkButton hb = sender as HyperlinkButton;
        //        if (hb == null)
        //        {
        //            MessageBox.Show("Could not cast button as HyperlinkButton", "Error", MessageBoxButton.OK);
        //            return;
        //        }
        //        EntityType type = EntityType.None;

        //        string lbl = hb.Tag.ToString();

        //        switch (lbl)
        //        {
        //            case "Contract":
        //                type = EntityType.Contract;
        //                break;
        //            case "Lease":
        //                type = EntityType.Lease;
        //                break;
        //            case "Well":
        //                type = EntityType.Well;
        //                break;
        //            case "Party":
        //                type = EntityType.Party;
        //                break;
        //            case "Operator":
        //                type = EntityType.Operator;
        //                break;
        //            case "Property":
        //                type = EntityType.Property;
        //                break;
        //            case "Prospect":
        //                type = EntityType.Prospect;
        //                break;
        //            case "Field":
        //                type = EntityType.Field;
        //                break;
        //            case "Defect":
        //                type = EntityType.Defect;
        //                break;
        //            case "Obligation":
        //                type = EntityType.Obligation;
        //                break;
        //            case "Payout":
        //                type = EntityType.Payout;
        //                break;
        //            case "File":
        //                type = EntityType.File;
        //                break;
        //        }
        //        RaiseEntitySelected(null, null, type);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityLink_Click", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityLink_Click", IfxTraceCategory.Leave);
        //    }
        //}

        void RaiseEntitySelected(Guid? id, string projectName, EntityType typeSelected)
        {
            EntitySelectedArgs e = new EntitySelectedArgs(id, projectName, typeSelected);
            EntitySelectedEventHandler handler = EntitySelected;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        #endregion Events


    }





    //#region EntitySelected Event

    //public delegate void EntitySelectedEventHandler(object sender, EntitySelectedArgs e);

    //public class EntitySelectedArgs : EventArgs
    //{
    //    private readonly Guid? _projectId;
    //    private readonly string _projectName;

    //    private readonly EntityType _typeSelected;



    //    public EntitySelectedArgs(Guid? projectId, string projectName, EntityType typeSelected)
    //    {
    //        this._projectId = projectId;
    //        this._projectName = projectName;
    //        this._typeSelected = typeSelected;
    //    }

    //    public Guid? ProjectId
    //    {
    //        get { return _projectId; }
    //    }

    //    public string ProjectName
    //    {
    //        get { return _projectName; }
    //    }

    //    public EntityType TypeSelected
    //    {
    //        get { return _typeSelected; }
    //    }


    //}

    //#endregion EntitySelected Event





}
