﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Resources;
using System.Windows.Media.Imaging;
using System.IO;
using ApplicationTypeServices;

namespace ProjectManager
{
    public class ImageHelpers
    {

        static string _contract = "Contract.png";
        static string _lease = "Lease.png";
        static string _well = "Well.png";
        static string _party = "Party.png";
        static string _prospect = "Prospect.png";
        static string _property = "Property.png";
        static string _defect = "Defect.png";
        static string _payout = "Payout.png";
        static string _obligation = "Obligation.png";
        static string _field = "Field.png";
        static string _project = "ProjectExplorerSmall.png";


        static string _columnChooser = "ColumnChooser.png";
        static string _dataOperationsNarrow = "DataOperationsNarrow.png";
        static string _filterAdd = "FilterAdd.png";
        static string _fixColumns = "FixColumns.png";
        static string _fullGrid = "FullGrid.png";
        static string _gridAll = "GridAll.png";
        static string _gridNarrow = "GridNarrow.png";
        static string _gridStandard = "GridStandard.png";
        static string _gridTools = "GridTools.png";
        static string _hideColumn = "HideColumn.png";
        static string _newRowOnBottom = "NewRowOnBottom.png";
        static string _newRowOnTop = "NewRowOnTop.png";
        static string _splitScreen = "SplitScreen.png";
        static string _richGrid = "RichGrid.png";
        static string _richGridOff = "RichGridOff.png";
        static string _stopNormalGreen16 = "StopNormalGreen16.png";
        static string _stopNormalRed16 = "StopNormalRed16.png";

        static string _imageURI = "/Pipeline;component/Images/";
        ///Pipeline;component/Images/
        public enum MenuImage
        {
            ColumnChooser,
            DataOperationsNarrow,
            FilterAdd,
            FixColumns,
            FullGrid,
            GridAll,
            GridNarrow,
            GridStandard,
            GridTools,
            HideColumn,
            NewRowOnBottom,
            NewRowOnTop,
            SplitScreen,
            RichGrid,
            RichGridOff,
            StopNormalGreen16,
            StopNormalRed16
        }

        public static string GetImagePath(EntityType type)
        {
            string img = "";
            switch (type)
            {
                case EntityType.Contract:
                    img = _imageURI + _contract;
                    break;
                case EntityType.Lease:
                    img = _imageURI + _lease;
                    break;
                case EntityType.Well:
                    img = _imageURI + _well;
                    break;
                case EntityType.Party:
                    img = _imageURI + _party;
                    break;
                case EntityType.Prospect:
                    img = _imageURI + _prospect;
                    break;
                case EntityType.Property:
                    img = _imageURI + _property;
                    break;
                case EntityType.Field:
                    img = _imageURI + _field;
                    break;
                case EntityType.Defect:
                    img = _imageURI + _defect;
                    break;
                case EntityType.Obligation:
                    img = _imageURI + _obligation;
                    break;
                case EntityType.Payout:
                    img = _imageURI + _payout;
                    break;
                case EntityType.Project:
                    img = _imageURI + _project;
                    break;
            }
            return img;
        }



        public static string GetMenuImagePath(MenuImage imageName)
        {
            string img = "";
            switch (imageName)
            {
                case MenuImage.ColumnChooser:
                    img = _imageURI + _columnChooser;
                    break;
                case MenuImage.DataOperationsNarrow:
                    img = _imageURI + _dataOperationsNarrow;
                    break;
                case MenuImage.FilterAdd:
                    img = _imageURI + _filterAdd;
                    break;
                case MenuImage.FixColumns:
                    img = _imageURI + _fixColumns;
                    break;
                case MenuImage.FullGrid:
                    img = _imageURI + _fullGrid;
                    break;
                case MenuImage.GridAll:
                    img = _imageURI + _gridAll;
                    break;
                case MenuImage.GridNarrow:
                    img = _imageURI + _gridNarrow;
                    break;
                case MenuImage.GridStandard:
                    img = _imageURI + _gridStandard;
                    break;
                case MenuImage.GridTools:
                    img = _imageURI + _gridTools;
                    break;
                case MenuImage.HideColumn:
                    img = _imageURI + _hideColumn;
                    break;
                case MenuImage.NewRowOnBottom:
                    img = _imageURI + _newRowOnBottom;
                    break;
                case MenuImage.NewRowOnTop:
                    img = _imageURI + _newRowOnTop;
                    break;
                case MenuImage.SplitScreen:
                    img = _imageURI + _splitScreen;
                    break;
                case MenuImage.RichGrid:
                    img = _imageURI + _richGrid;
                    break;
                case MenuImage.RichGridOff:
                    img = _imageURI + _richGridOff;
                    break;
                case MenuImage.StopNormalGreen16:
                    img = _imageURI + _stopNormalGreen16;
                    break;
                case MenuImage.StopNormalRed16:
                    img = _imageURI + _stopNormalRed16;
                    break;
            }
            return img;
        }






        public static Image LoadImage(string imagePath)
        {
            StreamResourceInfo sr = Application.GetResourceStream(new Uri(imagePath, UriKind.Relative));

            BitmapImage bmp = null;
            if (sr != null)
            {
                using (Stream sm = sr.Stream)
                {
                    bmp = new BitmapImage();
                    bmp.SetSource(sr.Stream);
                }
            }

            return new Image { Source = bmp, Margin = new Thickness(0, 0, 0, 0) };
        }





    }
}
