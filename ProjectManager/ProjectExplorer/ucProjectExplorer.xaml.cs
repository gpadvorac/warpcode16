﻿using System;
using System.Windows;
using System.Windows.Controls;
using TypeServices;
using Velocity.SL;
//using vSecurityClientAuthenticationSL;
using System.ComponentModel;
//using ProjectManager.Resources;
using Ifx.SL;
using vUICommon;
using EntityBll.SL;
//using vAdmin;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
//using IfxConfigSL;

using EntityWireTypeSL;
using System.Collections.ObjectModel;
using System.Diagnostics;
using UIControls;
using System.Windows.Media;
using Infragistics.Controls.Menus;
using ApplicationTypeServices;

namespace ProjectManager
{
    public partial class ucProjectExplorer : UserControl
    {

        #region Initialize Variables


        private static string _as = "ProjectManager";
        private static string _cn = "ucProjectExplorer";

        public event EntitySelectedEventHandler EntitySelected;

        ProxyWrapper.CommonClientDataService_ProxyWrapper _genProxy = null;


        #endregion Initialize Variables


        public ucProjectExplorer()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectListing", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }

                _genProxy = new ProxyWrapper.CommonClientDataService_ProxyWrapper();
                //_genProxy.GetProjExp_lstGroupsCompleted += new EventHandler<GetProjExp_lstGroupsCompletedEventArgs>(GetProjExp_lstGroupsCompleted);
          
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectListing", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectListing", IfxTraceCategory.Leave);
            }
        }

        public void SetAndysRoomLink()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectListing", IfxTraceCategory.Enter);
                if (ApplicationLevelVariables.IsAndysRoomLink == true)
                {
                    lnkAndysRoom.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    lnkAndysRoom.Visibility = System.Windows.Visibility.Collapsed;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectListing", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectListing", IfxTraceCategory.Leave);
            }
        }

           

        public void ClearProjects()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearProjects", IfxTraceCategory.Enter);

                gdEntities.Visibility = System.Windows.Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearProjects", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearProjects", IfxTraceCategory.Leave);
            }
        }




        #region Entities


        private void SetEntitiesVisibility(Visibility visibility)
        {
            gdEntities.Visibility = visibility;
        }

        public void GetEntityGroups( )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityGroups", IfxTraceCategory.Enter);

                //if (ContextValues.CurrentProjectId != null)
                //{
                //    _genProxy.Begin_GetProjExp_lstGroups((Guid)ContextValues.CurrentProjectId, "Project");
                //}

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityGroups", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityGroups", IfxTraceCategory.Leave);
            }
        }

        //void GetProjExp_lstGroupsCompleted(object sender, GetProjExp_lstGroupsCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstGroupsCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;
        //        object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
        //        if (array != null)
        //        {
        //            LoadEntityGroups(array);
        //        }

        


        //        //LoadLabels();
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstGroupsCompleted", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstGroupsCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        void LoadEntityGroups(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadEntityGroups", IfxTraceCategory.Enter);
                //ProjectExplorerNode obj;
                //ObservableCollection<ProjectExplorerNode> children = ((ProjectExplorerNode)_activeNode.Data).Children;

                ////  WATCH OUT !!!!  DONT CALL THIS LINE  !!!
                ////children.Clear();

                //// Hack   Hack   Hack   Hack   ...
                ////  use this to clear the children so we dont get a nasty exception.
                ////  for some dumb reason, _children.Clear(); will clear the children, but then we get an exception.
                //((ProjectExplorerNode)_activeNode.Data).ClearChildren();

                gdEntities.Visibility = System.Windows.Visibility.Visible;
                txbProject.Text = ContextValues.CurrentProjectName;
                for (int i = 0; i <= data.GetUpperBound(0); i++)
                {
                    string entity = (string)((object[])data[i])[0];
                    string lbl = (string)((object[])data[i])[1];
                    int cnt = (int)((object[])data[i])[2];
                    string cntFormat = "";
                    if (cnt > 0)
                    {
                        cntFormat = "(" + cnt.ToString("#,##0") + ")";
                    }

                    switch (entity)
                    {
                        case "RelatedContract":
                            //lnkContracts.Content = "Sections  " + cntFormat;
                            lnkRelatedContracts.Content = lbl + "  " + cntFormat;
                            break;
                        case "Contract":
                            //lnkContracts.Content = "Sections  " + cntFormat;
                            lnkContracts.Content = lbl + "  " + cntFormat;
                            break;
                        case "Lease":
                            //lnkLeases.Content = "Agreements  " + cntFormat;
                            lnkLeases.Content = lbl + "  " + cntFormat;
                            break;
                        case "Well":
                            //lnkWells.Content = "Pipelines  " + cntFormat;
                            lnkWells.Content = lbl + "  " + cntFormat;
                            break;
                        case "Prospect":
                            //lnkProspects.Content = "Areas  " + cntFormat;
                            lnkProspects.Content = lbl + "  " + cntFormat;
                            break;
                        case "Question/Defect":
                            //lnkDefects.Content = "Issues  " + cntFormat;
                            lnkDefects.Content = lbl + "  " + cntFormat;
                            break;
                        case "Obligation":
                            //lnkObligations.Content = "Obligations/Provisions  " + cntFormat;
                            lnkObligations.Content = lbl + "  " + cntFormat;
                            break;
                        case "Task":
                            lnkTasks.Content = lbl + "  " + cntFormat;
                            break;
                        case "SurveyMap":
                            lnkSurveyMap.Content = lbl + "  " + cntFormat;
                            break;
                        case "File":
                            lnkFiles.Content = lbl;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadEntityGroups", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadEntityGroups", IfxTraceCategory.Leave);
            }
        }





        #endregion Entities


        #region Events

        //private void ProjectLink_Click(object sender, RoutedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ProjectLink_Click", IfxTraceCategory.Enter);
        //        HyperlinkButton hb = sender as HyperlinkButton;
        //        if (hb == null)
        //        {
        //            MessageBox.Show("Could not cast button as HyperlinkButton", "Error", MessageBoxButton.OK);
        //            return;
        //        }

        //        Guid id = new Guid(hb.Tag.ToString());
        //        string lbl = hb.Content.ToString();
        //        //txbProject.Text = lbl;
        //        _genProxy.Begin_GetProjExp_lstGroups(id, "Project");
        //        RaiseEntitySelected(id, lbl, EntityType.Project);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ProjectLink_Click", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ProjectLink_Click", IfxTraceCategory.Leave);
        //    }
        //}


        private void EntityLink_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityLink_Click", IfxTraceCategory.Enter);
                HyperlinkButton hb = sender as HyperlinkButton;
                if (hb == null)
                {
                    MessageBox.Show("Could not cast button as HyperlinkButton", "Error", MessageBoxButton.OK);
                    return;
                }
                EntityType type = EntityType.None;

                string lbl = hb.Tag.ToString();

                switch (lbl)
                {
                    case "RelatedContract":
                        type = EntityType.RelatedContract;
                        break;
                    case "Contract":
                        type = EntityType.Contract;
                        break;
                    case "Lease":
                        type = EntityType.Lease;
                        break;
                    case "Well":
                        type = EntityType.Well;
                        break;
                    case "Party":
                        type = EntityType.Party;
                        break;
                    case "Operator":
                        type = EntityType.Operator;
                        break;
                    case "Property":
                        type = EntityType.Property;
                        break;
                    case "Prospect":
                        type = EntityType.Prospect;
                        break;
                    case "Field":
                        type = EntityType.Field;
                        break;
                    case "Defect":
                        type = EntityType.Defect;
                        break;
                    case "Obligation":
                        type = EntityType.Obligation;
                        break;
                    case "Payout":
                        type = EntityType.Payout;
                        break;
                    case "Task":
                        type = EntityType.Task;
                        break;
                    case "File":
                        type = EntityType.File;
                        break;
                    case "SurveyMap":
                        type = EntityType.SurveyMap;
                        break;
                        
                    case "AndysRoom":
                        type = EntityType.AndysRoom;
                        break;
                }
                RaiseEntitySelected(null, null, type);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityLink_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityLink_Click", IfxTraceCategory.Leave);
            }
        }

        void RaiseEntitySelected(Guid? id, string projectName, EntityType typeSelected)
        {
            EntitySelectedArgs e = new EntitySelectedArgs(id, projectName, typeSelected);
            EntitySelectedEventHandler handler = EntitySelected;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        #endregion Events






    }





    #region EntitySelected Event

    public delegate void EntitySelectedEventHandler(object sender, EntitySelectedArgs e);

    public class EntitySelectedArgs : EventArgs
    {
        private readonly Guid? _projectId;
        private readonly string _projectName;

        private readonly EntityType _typeSelected;



        public EntitySelectedArgs(Guid? projectId, string projectName, EntityType typeSelected)
        {
            this._projectId = projectId;
            this._projectName = projectName;
            this._typeSelected = typeSelected;
        }

        public Guid? ProjectId
        {
            get { return _projectId; }
        }

        public string ProjectName
        {
            get { return _projectName; }
        }

        public EntityType TypeSelected
        {
            get { return _typeSelected; }
        }


    }

    #endregion EntitySelected Event





}
