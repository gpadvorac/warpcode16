﻿using System;
using System.Windows.Controls;
using TypeServices;
using Velocity.SL;
using System.ComponentModel;
using Ifx.SL;
using vUICommon;
using System.Collections.Generic;
using UIControls;
using ApplicationTypeServices;


namespace ProjectManager
{
    public partial class ucProjectMangager : UserControl
    {



        #region Initialize Variables

        private static string _as = "ProjectManager";
        private static string _cn = "ProjectManager";
        private UserControl _activeContent = null;  // This is the main/top level-visible content to the right of the project explorer.
        
        public event BrokenRuleEventHandler BrokenRuleChanged;
        public event CurrentEntityStateEventHandler CurrentEntityStateChanged;
       
        List<UserControl> _loadedControls = new List<UserControl>();
                 
        UserSecurityContext _userContext = new UserSecurityContext();
           
        Guid? _currentProject_Id = null;
     
        ucTask ucTk = null;
     
        
        #endregion Initialize Variables



        public ucProjectMangager()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) { return; }
            InitializeComponent();
            _userContext.SecurityArtifactsRetrieved += new SecurityArtifactsRetrievedEventHandler(UserSecurityContext_SecurityArtifactsRetrieved);

            SetSecurityState();

        }



        #region Security



        private Guid _ancestorSecurityId = new Guid("59b35b3f-5d3c-4116-bd4a-6cc76bca4cea");
        ControlCache cCache;
        EntityCache eCache = null;
        bool defaultEntityOperationPermission = true;


        private void SetSecurityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Enter);

                //Guid ancestorId = new Guid("6b28738d-e603-4b15-a57c-24b9d27bd64c");
                //_ancestorSecurityId = new Guid("59B35B3F-5D3C-4116-BD4A-6CC76BCA4CEA");
                _userContext.LoadArtifactPermissions(_ancestorSecurityId);


                //cCache = SecurityCache.GetControlGroupById(_ancestorSecurityId);
                ////EntityCache eCache = null;
                ////bool defaultEntityOperationPermission = true;
                //DP.SetControlSecurityId(cpInvoice, new Guid("56D9B644-3D48-4C0D-B8F1-A2BE8E1451C8"));
                //SecurityCache.SetWPFActionControlState(cpInvoice, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Leave);
            }
        }



        void UserSecurityContext_SecurityArtifactsRetrieved(object sender, SecurityArtifactsRetrievedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Enter);
                // MessageBox.Show("UserSecurityContext_SecurityArtifactsRetrieved");

                // Check to see what transistion state we are in.  In otherwords, are we in the process of logging in?
                //    If so, then there is a chance that we just logged out as a different user and therefore, we will need to reset all of the 
                //    controls back to the initial login state.  For example, an administrator could have been logged in and
                //    working in the User Admin area.  If the administrator logs out, and a normal user logs in, that user will 
                //    be looking at the User Admin screen with full access to it.  This check will close everything and 
                //    set it back to the original state.


                //if (Credentials.IsAuthenticated == false)
                //{
                //    foreach (UserControl item in _loadedControls)
                //    {
                //        LayoutRoot.Children.Remove(item);
                //    }

                //    ucAd = null;
                //    //    //ucRM = null;
                //    //    //ucCo = null;
                //    //    //ucPM = null;
                //    //}
                //}

                //if (Credentials.IsAuthenticated == true && SplashImage.Visibility == System.Windows.Visibility.Visible)
                //{
                //    //// The use is authenticated and the spash image is still visibel which means the user is just logging in.
                //    //tbiUsersRoles.Content = null;
                //    //ucUA = null;
                //    //tbiReportAdmin.Content = null;
                //    //ucRpt = null;
                //    //tbiArtifactPermission.Content = null;
                //    //ucAP = null;
                //    //tbiDemo.Content = null;
                //    //ucCo = null;
                //    //ucRC.ClearList();
                //    //tbcReportToolMain.SelectedIndex = 0;

                //    //// Do any additional configuration as needed - below.
                //}

                SecurityCache.AddControlCacheForAncestor(e.ArtifactAncestorData);
                cCache = SecurityCache.GetControlGroupById(e.ArtifactAncestorData.Id);


                //DP.SetControlSecurityId(btnAppAdmin, new Guid("5DF7BD0F-F8A6-43C8-BB2A-968AC098D1A5"));
                //SecurityCache.SetWPFActionControlState(btnAppAdmin, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);



                //DP.SetControlSecurityId(mnuAdminTools, new Guid("614b2a38-30cf-4ec3-b3ef-f2ac6c270fd4"));
                //SecurityCache.SetWPFActionControlState(mnuAdminTools, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //DP.SetControlSecurityId(mnuNew, new Guid("4a983f76-a2ed-4968-9457-f73482d5dae2"));
                //SecurityCache.SetWPFActionControlState(mnuNew, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //DP.SetControlSecurityId(mnuSave, new Guid("075c7bfe-fce3-4293-bb94-b1a880d9de1c"));
                //SecurityCache.SetWPFActionControlState(mnuSave, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //DP.SetControlSecurityId(mnuUnDo, new Guid("e6fa5f6e-1cac-4734-a20e-c4b3337af0c2"));
                //SecurityCache.SetWPFActionControlState(mnuUnDo, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);




                //DP.SetControlSecurityId(obMain, new Guid("b58b19b0-c710-462e-9079-157558d690f7"));
                //SecurityCache.SetWPFActionControlState(obMain, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //if (obMain.Visibility == System.Windows.Visibility.Visible)
                //{
                //    gdSplitterMain.Visibility = System.Windows.Visibility.Visible;
                //    LayoutRoot.ColumnDefinitions[0].Width = GridLength.Auto;
                //    LayoutRoot.ColumnDefinitions[1].Width = new GridLength(8);
                //}
                //else
                //{
                //    gdSplitterMain.Visibility = System.Windows.Visibility.Collapsed;
                //    LayoutRoot.ColumnDefinitions[0].Width = new GridLength(0);
                //    LayoutRoot.ColumnDefinitions[1].Width = new GridLength(0);
                //}


                //int idx_obgProjExp = 0;
                //int idx_obgReports = 1;
                //int idx_obgAdmin = 2;
                //int idx_obgSupportData = 3;

                //DP.SetControlSecurityId(obMain.Groups[idx_obgProjExp], new Guid("59662d6e-0fe8-457c-b990-89b44a861de5"));
                //SecurityCache.SetWPFActionControlState(obMain.Groups[idx_obgProjExp], EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //DP.SetControlSecurityId(obMain.Groups[idx_obgReports], new Guid("4c7f4862-4949-4312-beac-d2f289e82c33"));
                //SecurityCache.SetWPFActionControlState(obMain.Groups[idx_obgReports], EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //DP.SetControlSecurityId(obMain.Groups[idx_obgSupportData], new Guid("c86c8fba-cf44-47d1-a2ae-b27c5cc5d59f"));
                //SecurityCache.SetWPFActionControlState(obMain.Groups[idx_obgSupportData], EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //DP.SetControlSecurityId(obMain.Groups[idx_obgAdmin], new Guid("626af683-e80d-42d4-9735-57f60e7e88c2"));
                //SecurityCache.SetWPFActionControlState(obMain.Groups[idx_obgAdmin], EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);







                //DP.SetControlSecurityId(btnIfxConfig, new Guid("c6e79c37-c2cd-4c62-b080-a18b093e7132"));
                //SecurityCache.SetWPFActionControlState(btnIfxConfig, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //if (ucPrjEx != null)
                //{
                //    if (Credentials.IsAuthenticated)
                //    {
                //        ucPrjEx.LoadProjects();
                //        obMain.Groups[idx_obgProjExp].IsSelected = true;
                //    }
                //    else
                //    {
                //        ucPrjEx.ClearProjects();
                //    }
                //}

                ////if (ucPrjLst != null)
                ////{
                ////    ucPrjLst.EntitySelected += new EntitySelectedEventHandler(ucPrjLst_EntitySelected);
                ////}



                ////if (Credentials.IsAuthenticated == true)
                ////{
                ////    //ucRC.LoadReports();

                ////    //if (ucRpt != null)
                ////    //{
                ////    //    ucRpt.SetSecurityState();
                ////    //}
                ////}





            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Leave);
            }
        }
        

        #endregion Security
        

        #region Load Controls



        public void SetStateFromParent(EntityType entityType, Guid? prj_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent", IfxTraceCategory.Enter);
                if (_currentProject_Id == prj_Id && entityType == EntityType.Project) { return; }
                UserControl activeContent = null;
                if (_activeContent != null)
                {
                    activeContent = _activeContent;
                }
                _currentProject_Id = prj_Id;

                switch (entityType)
                {
                    case EntityType.Project:
                        if (ucTk != null)
                        {
                            LayoutRoot.Children.Remove(ucTk);
                            ucTk = null;
                        }

                        if (ucTk != null)
                        {
                            LayoutRoot.Children.Remove(ucTk);
                            ucTk = null;
                        }
                        _activeContent = null;
                        break;
                 

                    case EntityType.Task:
                        if (ucTk == null)
                        {
                            ucTask_Load();
                        }
                        ucTk.SetStateFromParent(prj_Id, "ucProject", null, prj_Id, null, null, null, null, null);
                        _activeContent = ucTk;
                        break;

               

                }

                if (_activeContent != null)
                {
                    _activeContent.Visibility = System.Windows.Visibility.Visible;
                }

                if (activeContent != null && activeContent != _activeContent)
                {
                    activeContent.Visibility = System.Windows.Visibility.Collapsed;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent", IfxTraceCategory.Leave);
            }
        }
        


        void ucTask_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTask_Load", IfxTraceCategory.Enter);
                if (ucTk == null)
                {
                    ucTk = new ucTask();
                    ucTk.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    ucTk.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                    LayoutRoot.Children.Add(ucTk);
                    Grid.SetRow(ucTk, 1);
                    Grid.SetColumn(ucTk, 2);

                    ucTk.InitializeSplitScreenAndReadOnlyModes(false, true, false, true);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTask_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTask_Load", IfxTraceCategory.Leave);
            }
        }


        #endregion Load Controls
        

        #region Events


        /// <summary>
        ///     Bubbles up from the business object when the current entity’s <see cref="TypeServices.EntityState">state</see> has changed. It calls the <see cref="ConfigureToCurrentEntityState">ConfigureToCurrentEntityStatemethod</see> and then
        ///     continues to bubble up to notify other parent controls.
        /// </summary>
        void OnCurrentEntityStateChanged(object sender, CurrentEntityStateArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Enter);
                CurrentEntityStateEventHandler handler = CurrentEntityStateChanged;
                CurrentEntityStateArgs args;
                args = e;
                if (handler != null)
                {
                    handler(sender, args);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     OnBrokenRuleChanged starts from the business object and bubbles up through
        ///     <see cref="ucLeaseProps">ucProps</see> (or <see cref="ucLeaseList">ucLeaseList</see> when it’s configured to add/edit data) where the
        ///     current control’s (the control being edited - TextBox, ComboBox, etc.) appearance
        ///     will be modified according to its valid state. OnBrokenRuleChanged also passes up
        ///     the broken rule text so that it can be added or removed from the control’s
        ///     (TextBox, ComboBox, etc.) <see cref="ucLeaseProps.tt_Loaded">BrokenRule
        ///     Tooltip</see>. OnBrokenRuleChanged then continues to bubble up to the top level
        ///     control (probably a window) where you have an option to use the broken rule text
        ///     for other means of notifying the user.
        /// </summary>
        void OnBrokenRuleChanged(object sender, BrokenRuleArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Enter);
                BrokenRuleEventHandler handler = BrokenRuleChanged;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Leave);
            }
        }

        #endregion Events



    }
}
