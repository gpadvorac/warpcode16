using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/7/2018 2:55:31 PM

namespace EntityWireTypeSL
{


    [DataContract]
    public class WcDataTypeDefaultMappingDotNet2Sql_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcDataTypeDefaultMappingDotNet2Sql_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcDataTypeDefaultMappingDotNet2Sql_Binding() { }


        public WcDataTypeDefaultMappingDotNet2Sql_Binding(Int32 _DtDM_Id, Int32 _DtDM_DtDtNt_Id, String _DtDM_DotNetName, Int32 _DtDM_DtSql_Id, String _DtDM_SqlName )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDefaultMappingDotNet2Sql_Binding", IfxTraceCategory.Enter);
				_a = _DtDM_Id;
				_b = _DtDM_DtDtNt_Id;
				_c = _DtDM_DotNetName;
				_d = _DtDM_DtSql_Id;
				_e = _DtDM_SqlName;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDefaultMappingDotNet2Sql_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDefaultMappingDotNet2Sql_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcDataTypeDefaultMappingDotNet2Sql_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDefaultMappingDotNet2Sql_Binding", IfxTraceCategory.Enter);
				_a = (Int32)data[0];                //  DtDM_Id
				_b = (Int32)data[1];                //  DtDM_DtDtNt_Id
				_c = (String)data[2];                //  DtDM_DotNetName
				_d = (Int32)data[3];                //  DtDM_DtSql_Id
				_e = (String)data[4];                //  DtDM_SqlName
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDefaultMappingDotNet2Sql_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDefaultMappingDotNet2Sql_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return _a;
        }

        public void Set_Int_Id(int value)
        {
            _a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return null;
        }

        public void Set_Guid_Id(Guid value)
        {
            //_a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Int32 Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region DtDM_Id

        private Int32 _a;
//        public Int32 A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Int32 DtDM_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion DtDM_Id


        #region DtDM_DtDtNt_Id

        private Int32 _b;
//        public Int32 B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Int32 DtDM_DtDtNt_Id
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion DtDM_DtDtNt_Id


        #region DtDM_DotNetName

        private String _c;
//        public String C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public String DtDM_DotNetName
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion DtDM_DotNetName


        #region DtDM_DtSql_Id

        private Int32 _d;
//        public Int32 D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public Int32 DtDM_DtSql_Id
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion DtDM_DtSql_Id


        #region DtDM_SqlName

        private String _e;
//        public String E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public String DtDM_SqlName
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion DtDM_SqlName


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4} ", DtDM_Id, DtDM_DtDtNt_Id, DtDM_DotNetName, DtDM_DtSql_Id, DtDM_SqlName );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4} ", DtDM_Id, DtDM_DtDtNt_Id, DtDM_DotNetName, DtDM_DtSql_Id, DtDM_SqlName );
        }

    }

}
