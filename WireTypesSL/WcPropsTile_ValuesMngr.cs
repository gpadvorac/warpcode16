using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  12/27/2017 6:08:14 PM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcPropsTile_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcPropsTile_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcPropsTile_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcPropsTile_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcPropsTile_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcPropsTile_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcPropsTile_Values(currentData, this) : new WcPropsTile_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcPropsTile_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcPropsTile_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcPropsTile_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcPropsTile_Values _original;
        [DataMember]
        public WcPropsTile_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcPropsTile_Values _current;
        [DataMember]
        public WcPropsTile_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcPropsTile_Values _concurrent;
        [DataMember]
        public WcPropsTile_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  PrpTl_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  PrpTl_Tb_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  PrpTl_Sort
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  PrpTl_GridName
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  PrpTl_Header
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  PrpTl_IsDefaultView
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  PrpTl_IsActiveRow
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  PrpTl_IsDeleted
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  PrpTl_CreatedUserId
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  PrpTl_CreatedDate
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  PrpTl_UserId
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  UserName
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  PrpTl_LastModifiedDate
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  PrpTl_Stamp
                if (_current._n != _original._n)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{15}{0}{14}_b{15}{1}{14}_c{15}{2}{14}_d{15}{3}{14}_e{15}{4}{14}_f{15}{5}{14}_g{15}{6}{14}_h{15}{7}{14}_i{15}{8}{14}_j{15}{9}{14}_k{15}{10}{14}_l{15}{11}{14}_m{15}{12}{14}_n{15}{13}",
				new object[] {
				_current._a,		  //PrpTl_Id
				_current._b,		  //PrpTl_Tb_Id
				_current._c,		  //PrpTl_Sort
				_current._d,		  //PrpTl_GridName
				_current._e,		  //PrpTl_Header
				_current._f,		  //PrpTl_IsDefaultView
				_current._g,		  //PrpTl_IsActiveRow
				_current._h,		  //PrpTl_IsDeleted
				_current._i,		  //PrpTl_CreatedUserId
				_current._j,		  //PrpTl_CreatedDate
				_current._k,		  //PrpTl_UserId
				_current._l,		  //UserName
				_current._m,		  //PrpTl_LastModifiedDate
				_current._n,		  //PrpTl_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcPropsTile_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcPropsTile_Values";
        private WcPropsTile_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcPropsTile_Values() 
        {
        }

        //public WcPropsTile_Values(object[] data, WcPropsTile_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcPropsTile_Values(object[] data, WcPropsTile_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcPropsTile_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  PrpTl_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  PrpTl_Tb_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  PrpTl_Sort
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  PrpTl_GridName
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  PrpTl_Header
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  PrpTl_IsDefaultView
				_g = ObjectHelper.GetBoolFromObjectValue(data[6]);									//  PrpTl_IsActiveRow
				_h = ObjectHelper.GetBoolFromObjectValue(data[7]);									//  PrpTl_IsDeleted
				_i = ObjectHelper.GetNullableGuidFromObjectValue(data[8]);						//  PrpTl_CreatedUserId
				_j = ObjectHelper.GetNullableDateTimeFromObjectValue(data[9]);					//  PrpTl_CreatedDate
				_k = ObjectHelper.GetNullableGuidFromObjectValue(data[10]);						//  PrpTl_UserId
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  UserName
				_m = ObjectHelper.GetNullableDateTimeFromObjectValue(data[12]);					//  PrpTl_LastModifiedDate
				_n = data[13] as Byte[];						//  PrpTl_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcPropsTile_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcPropsTile_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcPropsTile", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  PrpTl_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  PrpTl_Tb_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  PrpTl_Sort
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  PrpTl_GridName
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  PrpTl_Header
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  PrpTl_IsDefaultView
				_g = ObjectHelper.GetBoolFromObjectValue(data[6]);									//  PrpTl_IsActiveRow
				_h = ObjectHelper.GetBoolFromObjectValue(data[7]);									//  PrpTl_IsDeleted
				_i = ObjectHelper.GetNullableGuidFromObjectValue(data[8]);						//  PrpTl_CreatedUserId
				_j = ObjectHelper.GetNullableDateTimeFromObjectValue(data[9]);					//  PrpTl_CreatedDate
				_k = ObjectHelper.GetNullableGuidFromObjectValue(data[10]);						//  PrpTl_UserId
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  UserName
				_m = ObjectHelper.GetNullableDateTimeFromObjectValue(data[12]);					//  PrpTl_LastModifiedDate
				_n = data[13] as Byte[];						//  PrpTl_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcPropsTile", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcPropsTile", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  PrpTl_Id

		[DataMember]
		public Guid? _b;			//  PrpTl_Tb_Id

		[DataMember]
		public Int32? _c;			//  PrpTl_Sort

		[DataMember]
		public String _d;			//  PrpTl_GridName

		[DataMember]
		public String _e;			//  PrpTl_Header

		[DataMember]
		public Boolean _f;			//  PrpTl_IsDefaultView

		[DataMember]
		public Boolean _g;			//  PrpTl_IsActiveRow

		[DataMember]
		public Boolean _h;			//  PrpTl_IsDeleted

		[DataMember]
		public Guid? _i;			//  PrpTl_CreatedUserId

		[DataMember]
		public DateTime? _j;			//  PrpTl_CreatedDate

		[DataMember]
		public Guid? _k;			//  PrpTl_UserId

		[DataMember]
		public String _l;			//  UserName

		[DataMember]
		public DateTime? _m;			//  PrpTl_LastModifiedDate

		[DataMember]
		public Byte[] _n;			//  PrpTl_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcPropsTile_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 14; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                }
                return _list;
            }
        }

        public Guid PrpTl_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid PrpTl_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? PrpTl_Tb_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? PrpTl_Tb_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Int32? PrpTl_Sort
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? PrpTl_Sort_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String PrpTl_GridName
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String PrpTl_GridName_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String PrpTl_Header
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String PrpTl_Header_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Boolean PrpTl_IsDefaultView
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean PrpTl_IsDefaultView_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Boolean PrpTl_IsActiveRow
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean PrpTl_IsActiveRow_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Boolean PrpTl_IsDeleted
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean PrpTl_IsDeleted_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Guid? PrpTl_CreatedUserId
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? PrpTl_CreatedUserId_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public DateTime? PrpTl_CreatedDate
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? PrpTl_CreatedDate_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public Guid? PrpTl_UserId
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? PrpTl_UserId_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public String UserName
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public DateTime? PrpTl_LastModifiedDate
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? PrpTl_LastModifiedDate_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public Byte[] PrpTl_Stamp
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] PrpTl_Stamp_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n
			};
        }

        public WcPropsTile_Values Clone()
        {
            return new WcPropsTile_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


