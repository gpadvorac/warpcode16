using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/7/2018 10:06:48 AM

namespace EntityWireTypeSL
{


    [DataContract]
    public class WcStoredProcParamValues_lstByValueGroupId_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProcParamValues_lstByValueGroupId_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcStoredProcParamValues_lstByValueGroupId_Binding() { }


        public WcStoredProcParamValues_lstByValueGroupId_Binding(Guid _SpP_ID, Int32 _SpP_SortOrder, String _SpP_Name, Int32 _SpP_SpPDr_Id, String _SpPDr_Name, Int32 _SpP_DtNt_ID, String _DtDtNt_Name, Int32 _SpP_DtSq_ID, String _DtSql_Name, Boolean _SpP_IsNullable, String _SpP_DefaultValue, String _SpPV_Value )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValues_lstByValueGroupId_Binding", IfxTraceCategory.Enter);
				_a = _SpP_ID;
				_b = _SpP_SortOrder;
				_c = _SpP_Name;
				_d = _SpP_SpPDr_Id;
				_e = _SpPDr_Name;
				_f = _SpP_DtNt_ID;
				_g = _DtDtNt_Name;
				_h = _SpP_DtSq_ID;
				_i = _DtSql_Name;
				_j = _SpP_IsNullable;
				_k = _SpP_DefaultValue;
				_l = _SpPV_Value;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValues_lstByValueGroupId_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValues_lstByValueGroupId_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcStoredProcParamValues_lstByValueGroupId_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValues_lstByValueGroupId_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  SpP_ID
				_b = (Int32)data[1];                //  SpP_SortOrder
				_c = (String)data[2];                //  SpP_Name
				_d = (Int32)data[3];                //  SpP_SpPDr_Id
				_e = (String)data[4];                //  SpPDr_Name
				_f = (Int32)data[5];                //  SpP_DtNt_ID
				_g = (String)data[6];                //  DtDtNt_Name
				_h = (Int32)data[7];                //  SpP_DtSq_ID
				_i = (String)data[8];                //  DtSql_Name
				_j = (Boolean)data[9];                //  SpP_IsNullable
				_k = (String)data[10];                //  SpP_DefaultValue
				_l = (String)data[11];                //  SpPV_Value
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValues_lstByValueGroupId_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValues_lstByValueGroupId_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region SpP_ID

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid SpP_ID
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion SpP_ID


        #region SpP_SortOrder

        private Int32 _b;
//        public Int32 B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Int32 SpP_SortOrder
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion SpP_SortOrder


        #region SpP_Name

        private String _c;
//        public String C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public String SpP_Name
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion SpP_Name


        #region SpP_SpPDr_Id

        private Int32 _d;
//        public Int32 D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public Int32 SpP_SpPDr_Id
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion SpP_SpPDr_Id


        #region SpPDr_Name

        private String _e;
//        public String E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public String SpPDr_Name
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion SpPDr_Name


        #region SpP_DtNt_ID

        private Int32 _f;
//        public Int32 F
//        {
//            get { return _f; }
//            set { _f = value; }
//        }

        //[NonSerialized]
        public Int32 SpP_DtNt_ID
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion SpP_DtNt_ID


        #region DtDtNt_Name

        private String _g;
//        public String G
//        {
//            get { return _g; }
//            set { _g = value; }
//        }

        //[NonSerialized]
        public String DtDtNt_Name
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion DtDtNt_Name


        #region SpP_DtSq_ID

        private Int32 _h;
//        public Int32 H
//        {
//            get { return _h; }
//            set { _h = value; }
//        }

        //[NonSerialized]
        public Int32 SpP_DtSq_ID
        {
            get { return _h; }
            set { _h = value; }
        }

        #endregion SpP_DtSq_ID


        #region DtSql_Name

        private String _i;
//        public String I
//        {
//            get { return _i; }
//            set { _i = value; }
//        }

        //[NonSerialized]
        public String DtSql_Name
        {
            get { return _i; }
            set { _i = value; }
        }

        #endregion DtSql_Name


        #region SpP_IsNullable

        private Boolean _j;
//        public Boolean J
//        {
//            get { return _j; }
//            set { _j = value; }
//        }

        //[NonSerialized]
        public Boolean SpP_IsNullable
        {
            get { return _j; }
            set { _j = value; }
        }

        #endregion SpP_IsNullable


        #region SpP_DefaultValue

        private String _k;
//        public String K
//        {
//            get { return _k; }
//            set { _k = value; }
//        }

        //[NonSerialized]
        public String SpP_DefaultValue
        {
            get { return _k; }
            set { _k = value; }
        }

        #endregion SpP_DefaultValue


        #region SpPV_Value

        private String _l;
//        public String L
//        {
//            get { return _l; }
//            set { _l = value; }
//        }

        //[NonSerialized]
        public String SpPV_Value
        {
            get { return _l; }
            set { _l = value; }
        }

        #endregion SpPV_Value


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11} ", SpP_ID, SpP_SortOrder, SpP_Name, SpP_SpPDr_Id, SpPDr_Name, SpP_DtNt_ID, DtDtNt_Name, SpP_DtSq_ID, DtSql_Name, SpP_IsNullable, SpP_DefaultValue, SpPV_Value );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11} ", SpP_ID, SpP_SortOrder, SpP_Name, SpP_SpPDr_Id, SpPDr_Name, SpP_DtNt_ID, DtDtNt_Name, SpP_DtSq_ID, DtSql_Name, SpP_IsNullable, SpP_DefaultValue, SpPV_Value );
        }

    }

}
