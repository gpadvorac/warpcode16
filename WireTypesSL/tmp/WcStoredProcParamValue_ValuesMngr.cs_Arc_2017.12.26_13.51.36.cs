using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  12/1/2016 10:43:36 AM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcStoredProcParamValue_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProcParamValue_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcStoredProcParamValue_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcStoredProcParamValue_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcStoredProcParamValue_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParamValue_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcStoredProcParamValue_Values(currentData, this) : new WcStoredProcParamValue_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcStoredProcParamValue_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParamValue_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParamValue_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcStoredProcParamValue_Values _original;
        [DataMember]
        public WcStoredProcParamValue_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcStoredProcParamValue_Values _current;
        [DataMember]
        public WcStoredProcParamValue_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcStoredProcParamValue_Values _concurrent;
        [DataMember]
        public WcStoredProcParamValue_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  SpPV_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  SpPV_SpPVGrp_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  SpPV_SpP_ID
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  SpPV_Value
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  SpPV_Notes
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  SpPV_IsActiveRow
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  SpPV_IsDeleted
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  SpPV_CreatedUserId
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  SpPV_CreatedDate
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  SpPV_UserId
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  UserName
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  SpPV_LastModifiedDate
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  SpPV_Stamp
                if (_current._m != _original._m)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{14}{0}{13}_b{14}{1}{13}_c{14}{2}{13}_d{14}{3}{13}_e{14}{4}{13}_f{14}{5}{13}_g{14}{6}{13}_h{14}{7}{13}_i{14}{8}{13}_j{14}{9}{13}_k{14}{10}{13}_l{14}{11}{13}_m{14}{12}",
				new object[] {
				_current._a,		  //SpPV_Id
				_current._b,		  //SpPV_SpPVGrp_Id
				_current._c,		  //SpPV_SpP_ID
				_current._d,		  //SpPV_Value
				_current._e,		  //SpPV_Notes
				_current._f,		  //SpPV_IsActiveRow
				_current._g,		  //SpPV_IsDeleted
				_current._h,		  //SpPV_CreatedUserId
				_current._i,		  //SpPV_CreatedDate
				_current._j,		  //SpPV_UserId
				_current._k,		  //UserName
				_current._l,		  //SpPV_LastModifiedDate
				_current._m,		  //SpPV_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcStoredProcParamValue_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProcParamValue_Values";
        private WcStoredProcParamValue_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcStoredProcParamValue_Values() 
        {
        }

        //public WcStoredProcParamValue_Values(object[] data, WcStoredProcParamValue_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcStoredProcParamValue_Values(object[] data, WcStoredProcParamValue_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParamValue_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  SpPV_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  SpPV_SpPVGrp_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  SpPV_SpP_ID
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  SpPV_Value
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  SpPV_Notes
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  SpPV_IsActiveRow
				_g = ObjectHelper.GetBoolFromObjectValue(data[6]);									//  SpPV_IsDeleted
				_h = ObjectHelper.GetNullableGuidFromObjectValue(data[7]);						//  SpPV_CreatedUserId
				_i = ObjectHelper.GetNullableDateTimeFromObjectValue(data[8]);					//  SpPV_CreatedDate
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  SpPV_UserId
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  UserName
				_l = ObjectHelper.GetNullableDateTimeFromObjectValue(data[11]);					//  SpPV_LastModifiedDate
				_m = ObjectHelper.GetByteArrayFromObjectValue(data[12]);						//  SpPV_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParamValue_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProcParamValue_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProcParamValue", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  SpPV_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  SpPV_SpPVGrp_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  SpPV_SpP_ID
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  SpPV_Value
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  SpPV_Notes
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  SpPV_IsActiveRow
				_g = ObjectHelper.GetBoolFromObjectValue(data[6]);									//  SpPV_IsDeleted
				_h = ObjectHelper.GetNullableGuidFromObjectValue(data[7]);						//  SpPV_CreatedUserId
				_i = ObjectHelper.GetNullableDateTimeFromObjectValue(data[8]);					//  SpPV_CreatedDate
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  SpPV_UserId
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  UserName
				_l = ObjectHelper.GetNullableDateTimeFromObjectValue(data[11]);					//  SpPV_LastModifiedDate
				_m = ObjectHelper.GetByteArrayFromObjectValue(data[12]);						//  SpPV_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProcParamValue", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProcParamValue", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  SpPV_Id

		[DataMember]
		public Guid? _b;			//  SpPV_SpPVGrp_Id

		[DataMember]
		public Guid? _c;			//  SpPV_SpP_ID

		[DataMember]
		public String _d;			//  SpPV_Value

		[DataMember]
		public String _e;			//  SpPV_Notes

		[DataMember]
		public Boolean _f;			//  SpPV_IsActiveRow

		[DataMember]
		public Boolean _g;			//  SpPV_IsDeleted

		[DataMember]
		public Guid? _h;			//  SpPV_CreatedUserId

		[DataMember]
		public DateTime? _i;			//  SpPV_CreatedDate

		[DataMember]
		public Guid? _j;			//  SpPV_UserId

		[DataMember]
		public String _k;			//  UserName

		[DataMember]
		public DateTime? _l;			//  SpPV_LastModifiedDate

		[DataMember]
		public Byte[] _m;			//  SpPV_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcStoredProcParamValue_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 13; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                }
                return _list;
            }
        }

        public Guid SpPV_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid SpPV_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? SpPV_SpPVGrp_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? SpPV_SpPVGrp_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Guid? SpPV_SpP_ID
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? SpPV_SpP_ID_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String SpPV_Value
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String SpPV_Value_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String SpPV_Notes
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String SpPV_Notes_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Boolean SpPV_IsActiveRow
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean SpPV_IsActiveRow_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Boolean SpPV_IsDeleted
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean SpPV_IsDeleted_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Guid? SpPV_CreatedUserId
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? SpPV_CreatedUserId_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public DateTime? SpPV_CreatedDate
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? SpPV_CreatedDate_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Guid? SpPV_UserId
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? SpPV_UserId_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public String UserName
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public DateTime? SpPV_LastModifiedDate
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? SpPV_LastModifiedDate_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public Byte[] SpPV_Stamp
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] SpPV_Stamp_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m
			};
        }

        public WcStoredProcParamValue_Values Clone()
        {
            return new WcStoredProcParamValue_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


