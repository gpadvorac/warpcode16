using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  1/5/2018 1:07:38 AM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcTableColumn_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTableColumn_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcTableColumn_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcTableColumn_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcTableColumn_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumn_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcTableColumn_Values(currentData, this) : new WcTableColumn_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcTableColumn_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumn_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumn_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcTableColumn_Values _original;
        [DataMember]
        public WcTableColumn_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcTableColumn_Values _current;
        [DataMember]
        public WcTableColumn_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcTableColumn_Values _concurrent;
        [DataMember]
        public WcTableColumn_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  TbC_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  TbC_Tb_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  TbC_auditColumn_Id
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  TbC_SortOrder
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  TbC_Name
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  ColumnInDatabase
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  TbC_CtlTp_Id
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  TbC_CtlTp_Id_TextField
                //if (_current._h != _original._h)
                //{
                //    return true;
                //}

                //  TbC_IsNonvColumn
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  TbC_Description
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  TbC_DtSql_Id
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  TbC_DtSql_Id_TextField
                //if (_current._l != _original._l)
                //{
                //    return true;
                //}

                //  TbC_DtDtNt_Id
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  TbC_DtDtNt_Id_TextField
                //if (_current._n != _original._n)
                //{
                //    return true;
                //}

                //  TbC_Length
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  TbC_Precision
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  TbC_Scale
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  TbC_IsPK
                if (_current._r != _original._r)
                {
                    return true;
                }

                //  TbC_IsIdentity
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  TbC_IsFK
                if (_current._t != _original._t)
                {
                    return true;
                }

                //  TbC_IsEntityColumn
                if (_current._u != _original._u)
                {
                    return true;
                }

                //  TbC_IsSystemField
                if (_current._v != _original._v)
                {
                    return true;
                }

                //  TbC_IsValuesObjectMember
                if (_current._w != _original._w)
                {
                    return true;
                }

                //  TbC_IsInPropsScreen
                if (_current._x != _original._x)
                {
                    return true;
                }

                //  TbC_IsInNavList
                if (_current._y != _original._y)
                {
                    return true;
                }

                //  TbC_IsRequired
                if (_current._z != _original._z)
                {
                    return true;
                }

                //  TbC_BrokenRuleText
                if (_current._aa != _original._aa)
                {
                    return true;
                }

                //  TbC_AllowZero
                if (_current._ab != _original._ab)
                {
                    return true;
                }

                //  TbC_IsNullableInDb
                if (_current._ac != _original._ac)
                {
                    return true;
                }

                //  TbC_IsNullableInUI
                if (_current._ad != _original._ad)
                {
                    return true;
                }

                //  TbC_DefaultValue
                if (_current._ae != _original._ae)
                {
                    return true;
                }

                //  TbC_IsReadFromDb
                if (_current._af != _original._af)
                {
                    return true;
                }

                //  TbC_IsSendToDb
                if (_current._ag != _original._ag)
                {
                    return true;
                }

                //  TbC_IsInsertAllowed
                if (_current._ah != _original._ah)
                {
                    return true;
                }

                //  TbC_IsEditAllowed
                if (_current._ai != _original._ai)
                {
                    return true;
                }

                //  TbC_IsReadOnlyInUI
                if (_current._aj != _original._aj)
                {
                    return true;
                }

                //  TbC_UseForAudit
                if (_current._ak != _original._ak)
                {
                    return true;
                }

                //  TbC_DefaultCaption
                if (_current._al != _original._al)
                {
                    return true;
                }

                //  TbC_ColumnHeaderText
                if (_current._am != _original._am)
                {
                    return true;
                }

                //  TbC_LabelCaptionVerbose
                if (_current._an != _original._an)
                {
                    return true;
                }

                //  TbC_LabelCaptionGenerate
                if (_current._ao != _original._ao)
                {
                    return true;
                }

                //  Tbc_ShowGridColumnToolTip
                if (_current._ap != _original._ap)
                {
                    return true;
                }

                //  Tbc_ShowPropsToolTip
                if (_current._aq != _original._aq)
                {
                    return true;
                }

                //  Tbc_TooltipsRolledUp
                if (_current._ar != _original._ar)
                {
                    return true;
                }

                //  TbC_IsCreatePropsStrings
                if (_current._asxx != _original._asxx)
                {
                    return true;
                }

                //  TbC_IsCreateGridStrings
                if (_current._at != _original._at)
                {
                    return true;
                }

                //  TbC_IsAvailableForColumnGroups
                if (_current._au != _original._au)
                {
                    return true;
                }

                //  TbC_IsTextWrapInProp
                if (_current._av != _original._av)
                {
                    return true;
                }

                //  TbC_IsTextWrapInGrid
                if (_current._aw != _original._aw)
                {
                    return true;
                }

                //  TbC_TextBoxFormat
                if (_current._ax != _original._ax)
                {
                    return true;
                }

                //  TbC_TextColumnFormat
                if (_current._ay != _original._ay)
                {
                    return true;
                }

                //  TbC_ColumnWidth
                if (_current._az != _original._az)
                {
                    return true;
                }

                //  TbC_TextBoxTextAlignment
                if (_current._ba != _original._ba)
                {
                    return true;
                }

                //  TbC_ColumnTextAlignment
                if (_current._bb != _original._bb)
                {
                    return true;
                }

                //  TbC_ListStoredProc_Id
                if (_current._bc != _original._bc)
                {
                    return true;
                }

                //  TbC_ListStoredProc_Id_TextField
                //if (_current._bd != _original._bd)
                //{
                //    return true;
                //}

                //  TbC_IsStaticList
                if (_current._be != _original._be)
                {
                    return true;
                }

                //  TbC_UseNotInList
                if (_current._bf != _original._bf)
                {
                    return true;
                }

                //  TbC_UseListEditBtn
                if (_current._bg != _original._bg)
                {
                    return true;
                }

                //  TbC_ParentColumnKey
                if (_current._bh != _original._bh)
                {
                    return true;
                }

                //  TbC_ParentColumnKey_TextField
                //if (_current._bi != _original._bi)
                //{
                //    return true;
                //}

                //  TbC_UseDisplayTextFieldProperty
                if (_current._bj != _original._bj)
                {
                    return true;
                }

                //  TbC_IsDisplayTextFieldProperty
                if (_current._bk != _original._bk)
                {
                    return true;
                }

                //  TbC_ComboListTable_Id
                if (_current._bl != _original._bl)
                {
                    return true;
                }

                //  TbC_ComboListTable_Id_TextField
                //if (_current._bm != _original._bm)
                //{
                //    return true;
                //}

                //  TbC_ComboListDisplayColumn_Id
                if (_current._bn != _original._bn)
                {
                    return true;
                }

                //  TbC_ComboListDisplayColumn_Id_TextField
                //if (_current._bo != _original._bo)
                //{
                //    return true;
                //}

                //  TbC_Combo_MaxDropdownHeight
                if (_current._bp != _original._bp)
                {
                    return true;
                }

                //  TbC_Combo_MaxDropdownWidth
                if (_current._bq != _original._bq)
                {
                    return true;
                }

                //  TbC_Combo_AllowDropdownResizing
                if (_current._br != _original._br)
                {
                    return true;
                }

                //  TbC_Combo_IsResetButtonVisible
                if (_current._bs != _original._bs)
                {
                    return true;
                }

                //  TbC_IsActiveRecColumn
                if (_current._bt != _original._bt)
                {
                    return true;
                }

                //  TbC_IsDeletedColumn
                if (_current._bu != _original._bu)
                {
                    return true;
                }

                //  TbC_IsCreatedUserIdColumn
                if (_current._bv != _original._bv)
                {
                    return true;
                }

                //  TbC_IsCreatedDateColumn
                if (_current._bw != _original._bw)
                {
                    return true;
                }

                //  TbC_IsUserIdColumn
                if (_current._bx != _original._bx)
                {
                    return true;
                }

                //  TbC_IsModifiedDateColumn
                if (_current._by != _original._by)
                {
                    return true;
                }

                //  TbC_IsRowVersionStampColumn
                if (_current._bz != _original._bz)
                {
                    return true;
                }

                //  TbC_IsBrowsable
                if (_current._ca != _original._ca)
                {
                    return true;
                }

                //  TbC_DeveloperNote
                if (_current._cb != _original._cb)
                {
                    return true;
                }

                //  TbC_UserNote
                if (_current._cc != _original._cc)
                {
                    return true;
                }

                //  TbC_HelpFileAdditionalNote
                if (_current._cd != _original._cd)
                {
                    return true;
                }

                //  TbC_Notes
                if (_current._ce != _original._ce)
                {
                    return true;
                }

                //  TbC_IsInputComplete
                if (_current._cf != _original._cf)
                {
                    return true;
                }

                //  TbC_IsCodeGen
                if (_current._cg != _original._cg)
                {
                    return true;
                }

                //  TbC_IsReadyCodeGen
                if (_current._ch != _original._ch)
                {
                    return true;
                }

                //  TbC_IsCodeGenComplete
                if (_current._ci != _original._ci)
                {
                    return true;
                }

                //  TbC_IsTagForCodeGen
                if (_current._cj != _original._cj)
                {
                    return true;
                }

                //  TbC_IsTagForOther
                if (_current._ck != _original._ck)
                {
                    return true;
                }

                //  TbC_ColumnGroups
                if (_current._cl != _original._cl)
                {
                    return true;
                }

                //  TbC_IsActiveRow
                if (_current._cm != _original._cm)
                {
                    return true;
                }

                //  TbC_IsDeleted
                if (_current._cnxx != _original._cnxx)
                {
                    return true;
                }

                //  TbC_CreatedUserId
                if (_current._co != _original._co)
                {
                    return true;
                }

                //  TbC_CreatedDate
                if (_current._cp != _original._cp)
                {
                    return true;
                }

                //  TbC_UserId
                if (_current._cq != _original._cq)
                {
                    return true;
                }

                //  UserName
                if (_current._cr != _original._cr)
                {
                    return true;
                }

                //  TbC_LastModifiedDate
                if (_current._cs != _original._cs)
                {
                    return true;
                }

                //  TbC_Stamp
                if (_current._ct != _original._ct)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{100}{0}{99}_b{100}{1}{99}_c{100}{2}{99}_d{100}{3}{99}_e{100}{4}{99}_f{100}{5}{99}_g{100}{6}{99}_h{100}{7}{99}_i{100}{8}{99}_j{100}{9}{99}_k{100}{10}{99}_l{100}{11}{99}_m{100}{12}{99}_n{100}{13}{99}_o{100}{14}{99}_p{100}{15}{99}_q{100}{16}{99}_r{100}{17}{99}_s{100}{18}{99}_t{100}{19}{99}_u{100}{20}{99}_v{100}{21}{99}_w{100}{22}{99}_x{100}{23}{99}_y{100}{24}{99}_z{100}{25}{99}_aa{100}{26}{99}_ab{100}{27}{99}_ac{100}{28}{99}_ad{100}{29}{99}_ae{100}{30}{99}_af{100}{31}{99}_ag{100}{32}{99}_ah{100}{33}{99}_ai{100}{34}{99}_aj{100}{35}{99}_ak{100}{36}{99}_al{100}{37}{99}_am{100}{38}{99}_an{100}{39}{99}_ao{100}{40}{99}_ap{100}{41}{99}_aq{100}{42}{99}_ar{100}{43}{99}_asxx{100}{44}{99}_at{100}{45}{99}_au{100}{46}{99}_av{100}{47}{99}_aw{100}{48}{99}_ax{100}{49}{99}_ay{100}{50}{99}_az{100}{51}{99}_ba{100}{52}{99}_bb{100}{53}{99}_bc{100}{54}{99}_bd{100}{55}{99}_be{100}{56}{99}_bf{100}{57}{99}_bg{100}{58}{99}_bh{100}{59}{99}_bi{100}{60}{99}_bj{100}{61}{99}_bk{100}{62}{99}_bl{100}{63}{99}_bm{100}{64}{99}_bn{100}{65}{99}_bo{100}{66}{99}_bp{100}{67}{99}_bq{100}{68}{99}_br{100}{69}{99}_bs{100}{70}{99}_bt{100}{71}{99}_bu{100}{72}{99}_bv{100}{73}{99}_bw{100}{74}{99}_bx{100}{75}{99}_by{100}{76}{99}_bz{100}{77}{99}_ca{100}{78}{99}_cb{100}{79}{99}_cc{100}{80}{99}_cd{100}{81}{99}_ce{100}{82}{99}_cf{100}{83}{99}_cg{100}{84}{99}_ch{100}{85}{99}_ci{100}{86}{99}_cj{100}{87}{99}_ck{100}{88}{99}_cl{100}{89}{99}_cm{100}{90}{99}_cnxx{100}{91}{99}_co{100}{92}{99}_cp{100}{93}{99}_cq{100}{94}{99}_cr{100}{95}{99}_cs{100}{96}{99}_ct{100}{97}{99}",
				new object[] {
				_current._a,		  //TbC_Id
				_current._b,		  //TbC_Tb_Id
				_current._c,		  //TbC_auditColumn_Id
				_current._d,		  //TbC_SortOrder
				_current._e,		  //TbC_Name
				_current._f,		  //ColumnInDatabase
				_current._g,		  //TbC_CtlTp_Id
				_current._h,		  //TbC_CtlTp_Id_TextField
				_current._i,		  //TbC_IsNonvColumn
				_current._j,		  //TbC_Description
				_current._k,		  //TbC_DtSql_Id
				_current._l,		  //TbC_DtSql_Id_TextField
				_current._m,		  //TbC_DtDtNt_Id
				_current._n,		  //TbC_DtDtNt_Id_TextField
				_current._o,		  //TbC_Length
				_current._p,		  //TbC_Precision
				_current._q,		  //TbC_Scale
				_current._r,		  //TbC_IsPK
				_current._s,		  //TbC_IsIdentity
				_current._t,		  //TbC_IsFK
				_current._u,		  //TbC_IsEntityColumn
				_current._v,		  //TbC_IsSystemField
				_current._w,		  //TbC_IsValuesObjectMember
				_current._x,		  //TbC_IsInPropsScreen
				_current._y,		  //TbC_IsInNavList
				_current._z,		  //TbC_IsRequired
				_current._aa,		  //TbC_BrokenRuleText
				_current._ab,		  //TbC_AllowZero
				_current._ac,		  //TbC_IsNullableInDb
				_current._ad,		  //TbC_IsNullableInUI
				_current._ae,		  //TbC_DefaultValue
				_current._af,		  //TbC_IsReadFromDb
				_current._ag,		  //TbC_IsSendToDb
				_current._ah,		  //TbC_IsInsertAllowed
				_current._ai,		  //TbC_IsEditAllowed
				_current._aj,		  //TbC_IsReadOnlyInUI
				_current._ak,		  //TbC_UseForAudit
				_current._al,		  //TbC_DefaultCaption
				_current._am,		  //TbC_ColumnHeaderText
				_current._an,		  //TbC_LabelCaptionVerbose
				_current._ao,		  //TbC_LabelCaptionGenerate
				_current._ap,		  //Tbc_ShowGridColumnToolTip
				_current._aq,		  //Tbc_ShowPropsToolTip
				_current._ar,		  //Tbc_TooltipsRolledUp
				_current._asxx,		  //TbC_IsCreatePropsStrings
				_current._at,		  //TbC_IsCreateGridStrings
				_current._au,		  //TbC_IsAvailableForColumnGroups
				_current._av,		  //TbC_IsTextWrapInProp
				_current._aw,		  //TbC_IsTextWrapInGrid
				_current._ax,		  //TbC_TextBoxFormat
				_current._ay,		  //TbC_TextColumnFormat
				_current._az,		  //TbC_ColumnWidth
				_current._ba,		  //TbC_TextBoxTextAlignment
				_current._bb,		  //TbC_ColumnTextAlignment
				_current._bc,		  //TbC_ListStoredProc_Id
				_current._bd,		  //TbC_ListStoredProc_Id_TextField
				_current._be,		  //TbC_IsStaticList
				_current._bf,		  //TbC_UseNotInList
				_current._bg,		  //TbC_UseListEditBtn
				_current._bh,		  //TbC_ParentColumnKey
				_current._bi,		  //TbC_ParentColumnKey_TextField
				_current._bj,		  //TbC_UseDisplayTextFieldProperty
				_current._bk,		  //TbC_IsDisplayTextFieldProperty
				_current._bl,		  //TbC_ComboListTable_Id
				_current._bm,		  //TbC_ComboListTable_Id_TextField
				_current._bn,		  //TbC_ComboListDisplayColumn_Id
				_current._bo,		  //TbC_ComboListDisplayColumn_Id_TextField
				_current._bp,		  //TbC_Combo_MaxDropdownHeight
				_current._bq,		  //TbC_Combo_MaxDropdownWidth
				_current._br,		  //TbC_Combo_AllowDropdownResizing
				_current._bs,		  //TbC_Combo_IsResetButtonVisible
				_current._bt,		  //TbC_IsActiveRecColumn
				_current._bu,		  //TbC_IsDeletedColumn
				_current._bv,		  //TbC_IsCreatedUserIdColumn
				_current._bw,		  //TbC_IsCreatedDateColumn
				_current._bx,		  //TbC_IsUserIdColumn
				_current._by,		  //TbC_IsModifiedDateColumn
				_current._bz,		  //TbC_IsRowVersionStampColumn
				_current._ca,		  //TbC_IsBrowsable
				_current._cb,		  //TbC_DeveloperNote
				_current._cc,		  //TbC_UserNote
				_current._cd,		  //TbC_HelpFileAdditionalNote
				_current._ce,		  //TbC_Notes
				_current._cf,		  //TbC_IsInputComplete
				_current._cg,		  //TbC_IsCodeGen
				_current._ch,		  //TbC_IsReadyCodeGen
				_current._ci,		  //TbC_IsCodeGenComplete
				_current._cj,		  //TbC_IsTagForCodeGen
				_current._ck,		  //TbC_IsTagForOther
				_current._cl,		  //TbC_ColumnGroups
				_current._cm,		  //TbC_IsActiveRow
				_current._cnxx,		  //TbC_IsDeleted
				_current._co,		  //TbC_CreatedUserId
				_current._cp,		  //TbC_CreatedDate
				_current._cq,		  //TbC_UserId
				_current._cr,		  //UserName
				_current._cs,		  //TbC_LastModifiedDate
				_current._ct,		  //TbC_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcTableColumn_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTableColumn_Values";
        private WcTableColumn_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcTableColumn_Values() 
        {
        }

        //public WcTableColumn_Values(object[] data, WcTableColumn_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcTableColumn_Values(object[] data, WcTableColumn_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumn_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  TbC_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  TbC_Tb_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  TbC_auditColumn_Id
				_d = ObjectHelper.GetNullableShortFromObjectValue(data[3]);							//  TbC_SortOrder
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  TbC_Name
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  ColumnInDatabase
				_g = ObjectHelper.GetNullableIntFromObjectValue(data[6]);							//  TbC_CtlTp_Id
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  TbC_CtlTp_Id_TextField
				_i = ObjectHelper.GetBoolFromObjectValue(data[8]);									//  TbC_IsNonvColumn
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  TbC_Description
				_k = ObjectHelper.GetNullableIntFromObjectValue(data[10]);							//  TbC_DtSql_Id
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  TbC_DtSql_Id_TextField
				_m = ObjectHelper.GetNullableIntFromObjectValue(data[12]);							//  TbC_DtDtNt_Id
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  TbC_DtDtNt_Id_TextField
				_o = ObjectHelper.GetNullableIntFromObjectValue(data[14]);							//  TbC_Length
				_p = ObjectHelper.GetNullableIntFromObjectValue(data[15]);							//  TbC_Precision
				_q = ObjectHelper.GetNullableIntFromObjectValue(data[16]);							//  TbC_Scale
				_r = ObjectHelper.GetBoolFromObjectValue(data[17]);									//  TbC_IsPK
				_s = ObjectHelper.GetBoolFromObjectValue(data[18]);									//  TbC_IsIdentity
				_t = ObjectHelper.GetBoolFromObjectValue(data[19]);									//  TbC_IsFK
				_u = ObjectHelper.GetBoolFromObjectValue(data[20]);									//  TbC_IsEntityColumn
				_v = ObjectHelper.GetBoolFromObjectValue(data[21]);									//  TbC_IsSystemField
				_w = ObjectHelper.GetBoolFromObjectValue(data[22]);									//  TbC_IsValuesObjectMember
				_x = ObjectHelper.GetBoolFromObjectValue(data[23]);									//  TbC_IsInPropsScreen
				_y = ObjectHelper.GetBoolFromObjectValue(data[24]);									//  TbC_IsInNavList
				_z = ObjectHelper.GetBoolFromObjectValue(data[25]);									//  TbC_IsRequired
				_aa = ObjectHelper.GetStringFromObjectValue(data[26]);									//  TbC_BrokenRuleText
				_ab = ObjectHelper.GetBoolFromObjectValue(data[27]);									//  TbC_AllowZero
				_ac = ObjectHelper.GetBoolFromObjectValue(data[28]);									//  TbC_IsNullableInDb
				_ad = ObjectHelper.GetBoolFromObjectValue(data[29]);									//  TbC_IsNullableInUI
				_ae = ObjectHelper.GetStringFromObjectValue(data[30]);									//  TbC_DefaultValue
				_af = ObjectHelper.GetBoolFromObjectValue(data[31]);									//  TbC_IsReadFromDb
				_ag = ObjectHelper.GetBoolFromObjectValue(data[32]);									//  TbC_IsSendToDb
				_ah = ObjectHelper.GetBoolFromObjectValue(data[33]);									//  TbC_IsInsertAllowed
				_ai = ObjectHelper.GetBoolFromObjectValue(data[34]);									//  TbC_IsEditAllowed
				_aj = ObjectHelper.GetBoolFromObjectValue(data[35]);									//  TbC_IsReadOnlyInUI
				_ak = ObjectHelper.GetBoolFromObjectValue(data[36]);									//  TbC_UseForAudit
				_al = ObjectHelper.GetStringFromObjectValue(data[37]);									//  TbC_DefaultCaption
				_am = ObjectHelper.GetStringFromObjectValue(data[38]);									//  TbC_ColumnHeaderText
				_an = ObjectHelper.GetStringFromObjectValue(data[39]);									//  TbC_LabelCaptionVerbose
				_ao = ObjectHelper.GetBoolFromObjectValue(data[40]);									//  TbC_LabelCaptionGenerate
				_ap = ObjectHelper.GetBoolFromObjectValue(data[41]);									//  Tbc_ShowGridColumnToolTip
				_aq = ObjectHelper.GetBoolFromObjectValue(data[42]);									//  Tbc_ShowPropsToolTip
				_ar = ObjectHelper.GetStringFromObjectValue(data[43]);									//  Tbc_TooltipsRolledUp
				_asxx = ObjectHelper.GetBoolFromObjectValue(data[44]);									//  TbC_IsCreatePropsStrings
				_at = ObjectHelper.GetBoolFromObjectValue(data[45]);									//  TbC_IsCreateGridStrings
				_au = ObjectHelper.GetBoolFromObjectValue(data[46]);									//  TbC_IsAvailableForColumnGroups
				_av = ObjectHelper.GetBoolFromObjectValue(data[47]);									//  TbC_IsTextWrapInProp
				_aw = ObjectHelper.GetBoolFromObjectValue(data[48]);									//  TbC_IsTextWrapInGrid
				_ax = ObjectHelper.GetStringFromObjectValue(data[49]);									//  TbC_TextBoxFormat
				_ay = ObjectHelper.GetStringFromObjectValue(data[50]);									//  TbC_TextColumnFormat
				_az = ObjectHelper.GetNullableDoubleFromObjectValue(data[51]);					//  TbC_ColumnWidth
				_ba = ObjectHelper.GetStringFromObjectValue(data[52]);									//  TbC_TextBoxTextAlignment
				_bb = ObjectHelper.GetStringFromObjectValue(data[53]);									//  TbC_ColumnTextAlignment
				_bc = ObjectHelper.GetNullableGuidFromObjectValue(data[54]);						//  TbC_ListStoredProc_Id
				_bd = ObjectHelper.GetStringFromObjectValue(data[55]);									//  TbC_ListStoredProc_Id_TextField
				_be = ObjectHelper.GetBoolFromObjectValue(data[56]);									//  TbC_IsStaticList
				_bf = ObjectHelper.GetBoolFromObjectValue(data[57]);									//  TbC_UseNotInList
				_bg = ObjectHelper.GetBoolFromObjectValue(data[58]);									//  TbC_UseListEditBtn
				_bh = ObjectHelper.GetNullableGuidFromObjectValue(data[59]);						//  TbC_ParentColumnKey
				_bi = ObjectHelper.GetStringFromObjectValue(data[60]);									//  TbC_ParentColumnKey_TextField
				_bj = ObjectHelper.GetBoolFromObjectValue(data[61]);									//  TbC_UseDisplayTextFieldProperty
				_bk = ObjectHelper.GetBoolFromObjectValue(data[62]);									//  TbC_IsDisplayTextFieldProperty
				_bl = ObjectHelper.GetNullableGuidFromObjectValue(data[63]);						//  TbC_ComboListTable_Id
				_bm = ObjectHelper.GetStringFromObjectValue(data[64]);									//  TbC_ComboListTable_Id_TextField
				_bn = ObjectHelper.GetNullableGuidFromObjectValue(data[65]);						//  TbC_ComboListDisplayColumn_Id
				_bo = ObjectHelper.GetStringFromObjectValue(data[66]);									//  TbC_ComboListDisplayColumn_Id_TextField
				_bp = ObjectHelper.GetNullableDoubleFromObjectValue(data[67]);					//  TbC_Combo_MaxDropdownHeight
				_bq = ObjectHelper.GetNullableDoubleFromObjectValue(data[68]);					//  TbC_Combo_MaxDropdownWidth
				_br = ObjectHelper.GetBoolFromObjectValue(data[69]);									//  TbC_Combo_AllowDropdownResizing
				_bs = ObjectHelper.GetBoolFromObjectValue(data[70]);									//  TbC_Combo_IsResetButtonVisible
				_bt = ObjectHelper.GetBoolFromObjectValue(data[71]);									//  TbC_IsActiveRecColumn
				_bu = ObjectHelper.GetBoolFromObjectValue(data[72]);									//  TbC_IsDeletedColumn
				_bv = ObjectHelper.GetBoolFromObjectValue(data[73]);									//  TbC_IsCreatedUserIdColumn
				_bw = ObjectHelper.GetBoolFromObjectValue(data[74]);									//  TbC_IsCreatedDateColumn
				_bx = ObjectHelper.GetBoolFromObjectValue(data[75]);									//  TbC_IsUserIdColumn
				_by = ObjectHelper.GetBoolFromObjectValue(data[76]);									//  TbC_IsModifiedDateColumn
				_bz = ObjectHelper.GetBoolFromObjectValue(data[77]);									//  TbC_IsRowVersionStampColumn
				_ca = ObjectHelper.GetBoolFromObjectValue(data[78]);									//  TbC_IsBrowsable
				_cb = ObjectHelper.GetStringFromObjectValue(data[79]);									//  TbC_DeveloperNote
				_cc = ObjectHelper.GetStringFromObjectValue(data[80]);									//  TbC_UserNote
				_cd = ObjectHelper.GetStringFromObjectValue(data[81]);									//  TbC_HelpFileAdditionalNote
				_ce = ObjectHelper.GetStringFromObjectValue(data[82]);									//  TbC_Notes
				_cf = ObjectHelper.GetBoolFromObjectValue(data[83]);									//  TbC_IsInputComplete
				_cg = ObjectHelper.GetBoolFromObjectValue(data[84]);									//  TbC_IsCodeGen
				_ch = ObjectHelper.GetNullableBoolFromObjectValue(data[85]);					//  TbC_IsReadyCodeGen
				_ci = ObjectHelper.GetNullableBoolFromObjectValue(data[86]);					//  TbC_IsCodeGenComplete
				_cj = ObjectHelper.GetNullableBoolFromObjectValue(data[87]);					//  TbC_IsTagForCodeGen
				_ck = ObjectHelper.GetNullableBoolFromObjectValue(data[88]);					//  TbC_IsTagForOther
				_cl = ObjectHelper.GetStringFromObjectValue(data[89]);									//  TbC_ColumnGroups
				_cm = ObjectHelper.GetBoolFromObjectValue(data[90]);									//  TbC_IsActiveRow
				_cnxx = ObjectHelper.GetBoolFromObjectValue(data[91]);									//  TbC_IsDeleted
				_co = ObjectHelper.GetNullableGuidFromObjectValue(data[92]);						//  TbC_CreatedUserId
				_cp = ObjectHelper.GetNullableDateTimeFromObjectValue(data[93]);					//  TbC_CreatedDate
				_cq = ObjectHelper.GetNullableGuidFromObjectValue(data[94]);						//  TbC_UserId
				_cr = ObjectHelper.GetStringFromObjectValue(data[95]);									//  UserName
				_cs = ObjectHelper.GetNullableDateTimeFromObjectValue(data[96]);					//  TbC_LastModifiedDate
				_ct = data[97] as Byte[];						//  TbC_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumn_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumn_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableColumn", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  TbC_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  TbC_Tb_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  TbC_auditColumn_Id
				_d = ObjectHelper.GetNullableShortFromObjectValue(data[3]);							//  TbC_SortOrder
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  TbC_Name
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  ColumnInDatabase
				_g = ObjectHelper.GetNullableIntFromObjectValue(data[6]);							//  TbC_CtlTp_Id
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  TbC_CtlTp_Id_TextField
				_i = ObjectHelper.GetBoolFromObjectValue(data[8]);									//  TbC_IsNonvColumn
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  TbC_Description
				_k = ObjectHelper.GetNullableIntFromObjectValue(data[10]);							//  TbC_DtSql_Id
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  TbC_DtSql_Id_TextField
				_m = ObjectHelper.GetNullableIntFromObjectValue(data[12]);							//  TbC_DtDtNt_Id
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  TbC_DtDtNt_Id_TextField
				_o = ObjectHelper.GetNullableIntFromObjectValue(data[14]);							//  TbC_Length
				_p = ObjectHelper.GetNullableIntFromObjectValue(data[15]);							//  TbC_Precision
				_q = ObjectHelper.GetNullableIntFromObjectValue(data[16]);							//  TbC_Scale
				_r = ObjectHelper.GetBoolFromObjectValue(data[17]);									//  TbC_IsPK
				_s = ObjectHelper.GetBoolFromObjectValue(data[18]);									//  TbC_IsIdentity
				_t = ObjectHelper.GetBoolFromObjectValue(data[19]);									//  TbC_IsFK
				_u = ObjectHelper.GetBoolFromObjectValue(data[20]);									//  TbC_IsEntityColumn
				_v = ObjectHelper.GetBoolFromObjectValue(data[21]);									//  TbC_IsSystemField
				_w = ObjectHelper.GetBoolFromObjectValue(data[22]);									//  TbC_IsValuesObjectMember
				_x = ObjectHelper.GetBoolFromObjectValue(data[23]);									//  TbC_IsInPropsScreen
				_y = ObjectHelper.GetBoolFromObjectValue(data[24]);									//  TbC_IsInNavList
				_z = ObjectHelper.GetBoolFromObjectValue(data[25]);									//  TbC_IsRequired
				_aa = ObjectHelper.GetStringFromObjectValue(data[26]);									//  TbC_BrokenRuleText
				_ab = ObjectHelper.GetBoolFromObjectValue(data[27]);									//  TbC_AllowZero
				_ac = ObjectHelper.GetBoolFromObjectValue(data[28]);									//  TbC_IsNullableInDb
				_ad = ObjectHelper.GetBoolFromObjectValue(data[29]);									//  TbC_IsNullableInUI
				_ae = ObjectHelper.GetStringFromObjectValue(data[30]);									//  TbC_DefaultValue
				_af = ObjectHelper.GetBoolFromObjectValue(data[31]);									//  TbC_IsReadFromDb
				_ag = ObjectHelper.GetBoolFromObjectValue(data[32]);									//  TbC_IsSendToDb
				_ah = ObjectHelper.GetBoolFromObjectValue(data[33]);									//  TbC_IsInsertAllowed
				_ai = ObjectHelper.GetBoolFromObjectValue(data[34]);									//  TbC_IsEditAllowed
				_aj = ObjectHelper.GetBoolFromObjectValue(data[35]);									//  TbC_IsReadOnlyInUI
				_ak = ObjectHelper.GetBoolFromObjectValue(data[36]);									//  TbC_UseForAudit
				_al = ObjectHelper.GetStringFromObjectValue(data[37]);									//  TbC_DefaultCaption
				_am = ObjectHelper.GetStringFromObjectValue(data[38]);									//  TbC_ColumnHeaderText
				_an = ObjectHelper.GetStringFromObjectValue(data[39]);									//  TbC_LabelCaptionVerbose
				_ao = ObjectHelper.GetBoolFromObjectValue(data[40]);									//  TbC_LabelCaptionGenerate
				_ap = ObjectHelper.GetBoolFromObjectValue(data[41]);									//  Tbc_ShowGridColumnToolTip
				_aq = ObjectHelper.GetBoolFromObjectValue(data[42]);									//  Tbc_ShowPropsToolTip
				_ar = ObjectHelper.GetStringFromObjectValue(data[43]);									//  Tbc_TooltipsRolledUp
				_asxx = ObjectHelper.GetBoolFromObjectValue(data[44]);									//  TbC_IsCreatePropsStrings
				_at = ObjectHelper.GetBoolFromObjectValue(data[45]);									//  TbC_IsCreateGridStrings
				_au = ObjectHelper.GetBoolFromObjectValue(data[46]);									//  TbC_IsAvailableForColumnGroups
				_av = ObjectHelper.GetBoolFromObjectValue(data[47]);									//  TbC_IsTextWrapInProp
				_aw = ObjectHelper.GetBoolFromObjectValue(data[48]);									//  TbC_IsTextWrapInGrid
				_ax = ObjectHelper.GetStringFromObjectValue(data[49]);									//  TbC_TextBoxFormat
				_ay = ObjectHelper.GetStringFromObjectValue(data[50]);									//  TbC_TextColumnFormat
				_az = ObjectHelper.GetNullableDoubleFromObjectValue(data[51]);					//  TbC_ColumnWidth
				_ba = ObjectHelper.GetStringFromObjectValue(data[52]);									//  TbC_TextBoxTextAlignment
				_bb = ObjectHelper.GetStringFromObjectValue(data[53]);									//  TbC_ColumnTextAlignment
				_bc = ObjectHelper.GetNullableGuidFromObjectValue(data[54]);						//  TbC_ListStoredProc_Id
				_bd = ObjectHelper.GetStringFromObjectValue(data[55]);									//  TbC_ListStoredProc_Id_TextField
				_be = ObjectHelper.GetBoolFromObjectValue(data[56]);									//  TbC_IsStaticList
				_bf = ObjectHelper.GetBoolFromObjectValue(data[57]);									//  TbC_UseNotInList
				_bg = ObjectHelper.GetBoolFromObjectValue(data[58]);									//  TbC_UseListEditBtn
				_bh = ObjectHelper.GetNullableGuidFromObjectValue(data[59]);						//  TbC_ParentColumnKey
				_bi = ObjectHelper.GetStringFromObjectValue(data[60]);									//  TbC_ParentColumnKey_TextField
				_bj = ObjectHelper.GetBoolFromObjectValue(data[61]);									//  TbC_UseDisplayTextFieldProperty
				_bk = ObjectHelper.GetBoolFromObjectValue(data[62]);									//  TbC_IsDisplayTextFieldProperty
				_bl = ObjectHelper.GetNullableGuidFromObjectValue(data[63]);						//  TbC_ComboListTable_Id
				_bm = ObjectHelper.GetStringFromObjectValue(data[64]);									//  TbC_ComboListTable_Id_TextField
				_bn = ObjectHelper.GetNullableGuidFromObjectValue(data[65]);						//  TbC_ComboListDisplayColumn_Id
				_bo = ObjectHelper.GetStringFromObjectValue(data[66]);									//  TbC_ComboListDisplayColumn_Id_TextField
				_bp = ObjectHelper.GetNullableDoubleFromObjectValue(data[67]);					//  TbC_Combo_MaxDropdownHeight
				_bq = ObjectHelper.GetNullableDoubleFromObjectValue(data[68]);					//  TbC_Combo_MaxDropdownWidth
				_br = ObjectHelper.GetBoolFromObjectValue(data[69]);									//  TbC_Combo_AllowDropdownResizing
				_bs = ObjectHelper.GetBoolFromObjectValue(data[70]);									//  TbC_Combo_IsResetButtonVisible
				_bt = ObjectHelper.GetBoolFromObjectValue(data[71]);									//  TbC_IsActiveRecColumn
				_bu = ObjectHelper.GetBoolFromObjectValue(data[72]);									//  TbC_IsDeletedColumn
				_bv = ObjectHelper.GetBoolFromObjectValue(data[73]);									//  TbC_IsCreatedUserIdColumn
				_bw = ObjectHelper.GetBoolFromObjectValue(data[74]);									//  TbC_IsCreatedDateColumn
				_bx = ObjectHelper.GetBoolFromObjectValue(data[75]);									//  TbC_IsUserIdColumn
				_by = ObjectHelper.GetBoolFromObjectValue(data[76]);									//  TbC_IsModifiedDateColumn
				_bz = ObjectHelper.GetBoolFromObjectValue(data[77]);									//  TbC_IsRowVersionStampColumn
				_ca = ObjectHelper.GetBoolFromObjectValue(data[78]);									//  TbC_IsBrowsable
				_cb = ObjectHelper.GetStringFromObjectValue(data[79]);									//  TbC_DeveloperNote
				_cc = ObjectHelper.GetStringFromObjectValue(data[80]);									//  TbC_UserNote
				_cd = ObjectHelper.GetStringFromObjectValue(data[81]);									//  TbC_HelpFileAdditionalNote
				_ce = ObjectHelper.GetStringFromObjectValue(data[82]);									//  TbC_Notes
				_cf = ObjectHelper.GetBoolFromObjectValue(data[83]);									//  TbC_IsInputComplete
				_cg = ObjectHelper.GetBoolFromObjectValue(data[84]);									//  TbC_IsCodeGen
				_ch = ObjectHelper.GetNullableBoolFromObjectValue(data[85]);					//  TbC_IsReadyCodeGen
				_ci = ObjectHelper.GetNullableBoolFromObjectValue(data[86]);					//  TbC_IsCodeGenComplete
				_cj = ObjectHelper.GetNullableBoolFromObjectValue(data[87]);					//  TbC_IsTagForCodeGen
				_ck = ObjectHelper.GetNullableBoolFromObjectValue(data[88]);					//  TbC_IsTagForOther
				_cl = ObjectHelper.GetStringFromObjectValue(data[89]);									//  TbC_ColumnGroups
				_cm = ObjectHelper.GetBoolFromObjectValue(data[90]);									//  TbC_IsActiveRow
				_cnxx = ObjectHelper.GetBoolFromObjectValue(data[91]);									//  TbC_IsDeleted
				_co = ObjectHelper.GetNullableGuidFromObjectValue(data[92]);						//  TbC_CreatedUserId
				_cp = ObjectHelper.GetNullableDateTimeFromObjectValue(data[93]);					//  TbC_CreatedDate
				_cq = ObjectHelper.GetNullableGuidFromObjectValue(data[94]);						//  TbC_UserId
				_cr = ObjectHelper.GetStringFromObjectValue(data[95]);									//  UserName
				_cs = ObjectHelper.GetNullableDateTimeFromObjectValue(data[96]);					//  TbC_LastModifiedDate
				_ct = data[97] as Byte[];						//  TbC_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableColumn", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableColumn", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  TbC_Id

		[DataMember]
		public Guid? _b;			//  TbC_Tb_Id

		[DataMember]
		public Guid? _c;			//  TbC_auditColumn_Id

		[DataMember]
		public Int16? _d;			//  TbC_SortOrder

		[DataMember]
		public String _e;			//  TbC_Name

		[DataMember]
		public String _f;			//  ColumnInDatabase

		[DataMember]
		public Int32? _g;			//  TbC_CtlTp_Id

		[DataMember]
		public String _h;			//  TbC_CtlTp_Id_TextField

		[DataMember]
		public Boolean _i;			//  TbC_IsNonvColumn

		[DataMember]
		public String _j;			//  TbC_Description

		[DataMember]
		public Int32? _k;			//  TbC_DtSql_Id

		[DataMember]
		public String _l;			//  TbC_DtSql_Id_TextField

		[DataMember]
		public Int32? _m;			//  TbC_DtDtNt_Id

		[DataMember]
		public String _n;			//  TbC_DtDtNt_Id_TextField

		[DataMember]
		public Int32? _o;			//  TbC_Length

		[DataMember]
		public Int32? _p;			//  TbC_Precision

		[DataMember]
		public Int32? _q;			//  TbC_Scale

		[DataMember]
		public Boolean _r;			//  TbC_IsPK

		[DataMember]
		public Boolean _s;			//  TbC_IsIdentity

		[DataMember]
		public Boolean _t;			//  TbC_IsFK

		[DataMember]
		public Boolean _u;			//  TbC_IsEntityColumn

		[DataMember]
		public Boolean _v;			//  TbC_IsSystemField

		[DataMember]
		public Boolean _w;			//  TbC_IsValuesObjectMember

		[DataMember]
		public Boolean _x;			//  TbC_IsInPropsScreen

		[DataMember]
		public Boolean _y;			//  TbC_IsInNavList

		[DataMember]
		public Boolean _z;			//  TbC_IsRequired

		[DataMember]
		public String _aa;			//  TbC_BrokenRuleText

		[DataMember]
		public Boolean _ab;			//  TbC_AllowZero

		[DataMember]
		public Boolean _ac;			//  TbC_IsNullableInDb

		[DataMember]
		public Boolean _ad;			//  TbC_IsNullableInUI

		[DataMember]
		public String _ae;			//  TbC_DefaultValue

		[DataMember]
		public Boolean _af;			//  TbC_IsReadFromDb

		[DataMember]
		public Boolean _ag;			//  TbC_IsSendToDb

		[DataMember]
		public Boolean _ah;			//  TbC_IsInsertAllowed

		[DataMember]
		public Boolean _ai;			//  TbC_IsEditAllowed

		[DataMember]
		public Boolean _aj;			//  TbC_IsReadOnlyInUI

		[DataMember]
		public Boolean _ak;			//  TbC_UseForAudit

		[DataMember]
		public String _al;			//  TbC_DefaultCaption

		[DataMember]
		public String _am;			//  TbC_ColumnHeaderText

		[DataMember]
		public String _an;			//  TbC_LabelCaptionVerbose

		[DataMember]
		public Boolean _ao;			//  TbC_LabelCaptionGenerate

		[DataMember]
		public Boolean _ap;			//  Tbc_ShowGridColumnToolTip

		[DataMember]
		public Boolean _aq;			//  Tbc_ShowPropsToolTip

		[DataMember]
		public String _ar;			//  Tbc_TooltipsRolledUp

		[DataMember]
		public Boolean _asxx;			//  TbC_IsCreatePropsStrings

		[DataMember]
		public Boolean _at;			//  TbC_IsCreateGridStrings

		[DataMember]
		public Boolean _au;			//  TbC_IsAvailableForColumnGroups

		[DataMember]
		public Boolean _av;			//  TbC_IsTextWrapInProp

		[DataMember]
		public Boolean _aw;			//  TbC_IsTextWrapInGrid

		[DataMember]
		public String _ax;			//  TbC_TextBoxFormat

		[DataMember]
		public String _ay;			//  TbC_TextColumnFormat

		[DataMember]
		public Double? _az;			//  TbC_ColumnWidth

		[DataMember]
		public String _ba;			//  TbC_TextBoxTextAlignment

		[DataMember]
		public String _bb;			//  TbC_ColumnTextAlignment

		[DataMember]
		public Guid? _bc;			//  TbC_ListStoredProc_Id

		[DataMember]
		public String _bd;			//  TbC_ListStoredProc_Id_TextField

		[DataMember]
		public Boolean _be;			//  TbC_IsStaticList

		[DataMember]
		public Boolean _bf;			//  TbC_UseNotInList

		[DataMember]
		public Boolean _bg;			//  TbC_UseListEditBtn

		[DataMember]
		public Guid? _bh;			//  TbC_ParentColumnKey

		[DataMember]
		public String _bi;			//  TbC_ParentColumnKey_TextField

		[DataMember]
		public Boolean _bj;			//  TbC_UseDisplayTextFieldProperty

		[DataMember]
		public Boolean _bk;			//  TbC_IsDisplayTextFieldProperty

		[DataMember]
		public Guid? _bl;			//  TbC_ComboListTable_Id

		[DataMember]
		public String _bm;			//  TbC_ComboListTable_Id_TextField

		[DataMember]
		public Guid? _bn;			//  TbC_ComboListDisplayColumn_Id

		[DataMember]
		public String _bo;			//  TbC_ComboListDisplayColumn_Id_TextField

		[DataMember]
		public Double? _bp;			//  TbC_Combo_MaxDropdownHeight

		[DataMember]
		public Double? _bq;			//  TbC_Combo_MaxDropdownWidth

		[DataMember]
		public Boolean _br;			//  TbC_Combo_AllowDropdownResizing

		[DataMember]
		public Boolean _bs;			//  TbC_Combo_IsResetButtonVisible

		[DataMember]
		public Boolean _bt;			//  TbC_IsActiveRecColumn

		[DataMember]
		public Boolean _bu;			//  TbC_IsDeletedColumn

		[DataMember]
		public Boolean _bv;			//  TbC_IsCreatedUserIdColumn

		[DataMember]
		public Boolean _bw;			//  TbC_IsCreatedDateColumn

		[DataMember]
		public Boolean _bx;			//  TbC_IsUserIdColumn

		[DataMember]
		public Boolean _by;			//  TbC_IsModifiedDateColumn

		[DataMember]
		public Boolean _bz;			//  TbC_IsRowVersionStampColumn

		[DataMember]
		public Boolean _ca;			//  TbC_IsBrowsable

		[DataMember]
		public String _cb;			//  TbC_DeveloperNote

		[DataMember]
		public String _cc;			//  TbC_UserNote

		[DataMember]
		public String _cd;			//  TbC_HelpFileAdditionalNote

		[DataMember]
		public String _ce;			//  TbC_Notes

		[DataMember]
		public Boolean _cf;			//  TbC_IsInputComplete

		[DataMember]
		public Boolean _cg;			//  TbC_IsCodeGen

		[DataMember]
		public Boolean? _ch;			//  TbC_IsReadyCodeGen

		[DataMember]
		public Boolean? _ci;			//  TbC_IsCodeGenComplete

		[DataMember]
		public Boolean? _cj;			//  TbC_IsTagForCodeGen

		[DataMember]
		public Boolean? _ck;			//  TbC_IsTagForOther

		[DataMember]
		public String _cl;			//  TbC_ColumnGroups

		[DataMember]
		public Boolean _cm;			//  TbC_IsActiveRow

		[DataMember]
		public Boolean _cnxx;			//  TbC_IsDeleted

		[DataMember]
		public Guid? _co;			//  TbC_CreatedUserId

		[DataMember]
		public DateTime? _cp;			//  TbC_CreatedDate

		[DataMember]
		public Guid? _cq;			//  TbC_UserId

		[DataMember]
		public String _cr;			//  UserName

		[DataMember]
		public DateTime? _cs;			//  TbC_LastModifiedDate

		[DataMember]
		public Byte[] _ct;			//  TbC_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcTableColumn_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 99; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                    _list[18] = _s;
                    _list[19] = _t;
                    _list[20] = _u;
                    _list[21] = _v;
                    _list[22] = _w;
                    _list[23] = _x;
                    _list[24] = _y;
                    _list[25] = _z;
                    _list[26] = _aa;
                    _list[27] = _ab;
                    _list[28] = _ac;
                    _list[29] = _ad;
                    _list[30] = _ae;
                    _list[31] = _af;
                    _list[32] = _ag;
                    _list[33] = _ah;
                    _list[34] = _ai;
                    _list[35] = _aj;
                    _list[36] = _ak;
                    _list[37] = _al;
                    _list[38] = _am;
                    _list[39] = _an;
                    _list[40] = _ao;
                    _list[41] = _ap;
                    _list[42] = _aq;
                    _list[43] = _ar;
                    _list[44] = _asxx;
                    _list[45] = _at;
                    _list[46] = _au;
                    _list[47] = _av;
                    _list[48] = _aw;
                    _list[49] = _ax;
                    _list[50] = _ay;
                    _list[51] = _az;
                    _list[52] = _ba;
                    _list[53] = _bb;
                    _list[54] = _bc;
                    _list[55] = _bd;
                    _list[56] = _be;
                    _list[57] = _bf;
                    _list[58] = _bg;
                    _list[59] = _bh;
                    _list[60] = _bi;
                    _list[61] = _bj;
                    _list[62] = _bk;
                    _list[63] = _bl;
                    _list[64] = _bm;
                    _list[65] = _bn;
                    _list[66] = _bo;
                    _list[67] = _bp;
                    _list[68] = _bq;
                    _list[69] = _br;
                    _list[70] = _bs;
                    _list[71] = _bt;
                    _list[72] = _bu;
                    _list[73] = _bv;
                    _list[74] = _bw;
                    _list[75] = _bx;
                    _list[76] = _by;
                    _list[77] = _bz;
                    _list[78] = _ca;
                    _list[79] = _cb;
                    _list[80] = _cc;
                    _list[81] = _cd;
                    _list[82] = _ce;
                    _list[83] = _cf;
                    _list[84] = _cg;
                    _list[85] = _ch;
                    _list[86] = _ci;
                    _list[87] = _cj;
                    _list[88] = _ck;
                    _list[89] = _cl;
                    _list[90] = _cm;
                    _list[91] = _cnxx;
                    _list[92] = _co;
                    _list[93] = _cp;
                    _list[94] = _cq;
                    _list[95] = _cr;
                    _list[96] = _cs;
                    _list[97] = _ct;
                }
                return _list;
            }
        }

        public Guid TbC_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid TbC_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? TbC_Tb_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_Tb_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Guid? TbC_auditColumn_Id
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_auditColumn_Id_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public Int16? TbC_SortOrder
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int16? TbC_SortOrder_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String TbC_Name
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_Name_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public String ColumnInDatabase
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ColumnInDatabase_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Int32? TbC_CtlTp_Id
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbC_CtlTp_Id_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public String TbC_CtlTp_Id_TextField
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_CtlTp_Id_TextField_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Boolean TbC_IsNonvColumn
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsNonvColumn_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public String TbC_Description
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_Description_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public Int32? TbC_DtSql_Id
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbC_DtSql_Id_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public String TbC_DtSql_Id_TextField
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_DtSql_Id_TextField_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public Int32? TbC_DtDtNt_Id
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbC_DtDtNt_Id_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public String TbC_DtDtNt_Id_TextField
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_DtDtNt_Id_TextField_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public Int32? TbC_Length
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbC_Length_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public Int32? TbC_Precision
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbC_Precision_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public Int32? TbC_Scale
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbC_Scale_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public Boolean TbC_IsPK
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsPK_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        public Boolean TbC_IsIdentity
        {
            get { return _s; }
            set
            {
                _s = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsIdentity_noevents
        {
            get { return _s; }
            set
            {
                _s = value;
            }
        }

        public Boolean TbC_IsFK
        {
            get { return _t; }
            set
            {
                _t = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsFK_noevents
        {
            get { return _t; }
            set
            {
                _t = value;
            }
        }

        public Boolean TbC_IsEntityColumn
        {
            get { return _u; }
            set
            {
                _u = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsEntityColumn_noevents
        {
            get { return _u; }
            set
            {
                _u = value;
            }
        }

        public Boolean TbC_IsSystemField
        {
            get { return _v; }
            set
            {
                _v = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsSystemField_noevents
        {
            get { return _v; }
            set
            {
                _v = value;
            }
        }

        public Boolean TbC_IsValuesObjectMember
        {
            get { return _w; }
            set
            {
                _w = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsValuesObjectMember_noevents
        {
            get { return _w; }
            set
            {
                _w = value;
            }
        }

        public Boolean TbC_IsInPropsScreen
        {
            get { return _x; }
            set
            {
                _x = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsInPropsScreen_noevents
        {
            get { return _x; }
            set
            {
                _x = value;
            }
        }

        public Boolean TbC_IsInNavList
        {
            get { return _y; }
            set
            {
                _y = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsInNavList_noevents
        {
            get { return _y; }
            set
            {
                _y = value;
            }
        }

        public Boolean TbC_IsRequired
        {
            get { return _z; }
            set
            {
                _z = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsRequired_noevents
        {
            get { return _z; }
            set
            {
                _z = value;
            }
        }

        public String TbC_BrokenRuleText
        {
            get { return _aa; }
            set
            {
                _aa = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_BrokenRuleText_noevents
        {
            get { return _aa; }
            set
            {
                _aa = value;
            }
        }

        public Boolean TbC_AllowZero
        {
            get { return _ab; }
            set
            {
                _ab = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_AllowZero_noevents
        {
            get { return _ab; }
            set
            {
                _ab = value;
            }
        }

        public Boolean TbC_IsNullableInDb
        {
            get { return _ac; }
            set
            {
                _ac = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsNullableInDb_noevents
        {
            get { return _ac; }
            set
            {
                _ac = value;
            }
        }

        public Boolean TbC_IsNullableInUI
        {
            get { return _ad; }
            set
            {
                _ad = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsNullableInUI_noevents
        {
            get { return _ad; }
            set
            {
                _ad = value;
            }
        }

        public String TbC_DefaultValue
        {
            get { return _ae; }
            set
            {
                _ae = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_DefaultValue_noevents
        {
            get { return _ae; }
            set
            {
                _ae = value;
            }
        }

        public Boolean TbC_IsReadFromDb
        {
            get { return _af; }
            set
            {
                _af = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsReadFromDb_noevents
        {
            get { return _af; }
            set
            {
                _af = value;
            }
        }

        public Boolean TbC_IsSendToDb
        {
            get { return _ag; }
            set
            {
                _ag = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsSendToDb_noevents
        {
            get { return _ag; }
            set
            {
                _ag = value;
            }
        }

        public Boolean TbC_IsInsertAllowed
        {
            get { return _ah; }
            set
            {
                _ah = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsInsertAllowed_noevents
        {
            get { return _ah; }
            set
            {
                _ah = value;
            }
        }

        public Boolean TbC_IsEditAllowed
        {
            get { return _ai; }
            set
            {
                _ai = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsEditAllowed_noevents
        {
            get { return _ai; }
            set
            {
                _ai = value;
            }
        }

        public Boolean TbC_IsReadOnlyInUI
        {
            get { return _aj; }
            set
            {
                _aj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsReadOnlyInUI_noevents
        {
            get { return _aj; }
            set
            {
                _aj = value;
            }
        }

        public Boolean TbC_UseForAudit
        {
            get { return _ak; }
            set
            {
                _ak = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_UseForAudit_noevents
        {
            get { return _ak; }
            set
            {
                _ak = value;
            }
        }

        public String TbC_DefaultCaption
        {
            get { return _al; }
            set
            {
                _al = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_DefaultCaption_noevents
        {
            get { return _al; }
            set
            {
                _al = value;
            }
        }

        public String TbC_ColumnHeaderText
        {
            get { return _am; }
            set
            {
                _am = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ColumnHeaderText_noevents
        {
            get { return _am; }
            set
            {
                _am = value;
            }
        }

        public String TbC_LabelCaptionVerbose
        {
            get { return _an; }
            set
            {
                _an = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_LabelCaptionVerbose_noevents
        {
            get { return _an; }
            set
            {
                _an = value;
            }
        }

        public Boolean TbC_LabelCaptionGenerate
        {
            get { return _ao; }
            set
            {
                _ao = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_LabelCaptionGenerate_noevents
        {
            get { return _ao; }
            set
            {
                _ao = value;
            }
        }

        public Boolean Tbc_ShowGridColumnToolTip
        {
            get { return _ap; }
            set
            {
                _ap = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tbc_ShowGridColumnToolTip_noevents
        {
            get { return _ap; }
            set
            {
                _ap = value;
            }
        }

        public Boolean Tbc_ShowPropsToolTip
        {
            get { return _aq; }
            set
            {
                _aq = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tbc_ShowPropsToolTip_noevents
        {
            get { return _aq; }
            set
            {
                _aq = value;
            }
        }

        public String Tbc_TooltipsRolledUp
        {
            get { return _ar; }
            set
            {
                _ar = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tbc_TooltipsRolledUp_noevents
        {
            get { return _ar; }
            set
            {
                _ar = value;
            }
        }

        public Boolean TbC_IsCreatePropsStrings
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsCreatePropsStrings_noevents
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
            }
        }

        public Boolean TbC_IsCreateGridStrings
        {
            get { return _at; }
            set
            {
                _at = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsCreateGridStrings_noevents
        {
            get { return _at; }
            set
            {
                _at = value;
            }
        }

        public Boolean TbC_IsAvailableForColumnGroups
        {
            get { return _au; }
            set
            {
                _au = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsAvailableForColumnGroups_noevents
        {
            get { return _au; }
            set
            {
                _au = value;
            }
        }

        public Boolean TbC_IsTextWrapInProp
        {
            get { return _av; }
            set
            {
                _av = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsTextWrapInProp_noevents
        {
            get { return _av; }
            set
            {
                _av = value;
            }
        }

        public Boolean TbC_IsTextWrapInGrid
        {
            get { return _aw; }
            set
            {
                _aw = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsTextWrapInGrid_noevents
        {
            get { return _aw; }
            set
            {
                _aw = value;
            }
        }

        public String TbC_TextBoxFormat
        {
            get { return _ax; }
            set
            {
                _ax = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_TextBoxFormat_noevents
        {
            get { return _ax; }
            set
            {
                _ax = value;
            }
        }

        public String TbC_TextColumnFormat
        {
            get { return _ay; }
            set
            {
                _ay = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_TextColumnFormat_noevents
        {
            get { return _ay; }
            set
            {
                _ay = value;
            }
        }

        public Double? TbC_ColumnWidth
        {
            get { return _az; }
            set
            {
                _az = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Double? TbC_ColumnWidth_noevents
        {
            get { return _az; }
            set
            {
                _az = value;
            }
        }

        public String TbC_TextBoxTextAlignment
        {
            get { return _ba; }
            set
            {
                _ba = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_TextBoxTextAlignment_noevents
        {
            get { return _ba; }
            set
            {
                _ba = value;
            }
        }

        public String TbC_ColumnTextAlignment
        {
            get { return _bb; }
            set
            {
                _bb = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ColumnTextAlignment_noevents
        {
            get { return _bb; }
            set
            {
                _bb = value;
            }
        }

        public Guid? TbC_ListStoredProc_Id
        {
            get { return _bc; }
            set
            {
                _bc = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_ListStoredProc_Id_noevents
        {
            get { return _bc; }
            set
            {
                _bc = value;
            }
        }

        public String TbC_ListStoredProc_Id_TextField
        {
            get { return _bd; }
            set
            {
                _bd = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ListStoredProc_Id_TextField_noevents
        {
            get { return _bd; }
            set
            {
                _bd = value;
            }
        }

        public Boolean TbC_IsStaticList
        {
            get { return _be; }
            set
            {
                _be = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsStaticList_noevents
        {
            get { return _be; }
            set
            {
                _be = value;
            }
        }

        public Boolean TbC_UseNotInList
        {
            get { return _bf; }
            set
            {
                _bf = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_UseNotInList_noevents
        {
            get { return _bf; }
            set
            {
                _bf = value;
            }
        }

        public Boolean TbC_UseListEditBtn
        {
            get { return _bg; }
            set
            {
                _bg = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_UseListEditBtn_noevents
        {
            get { return _bg; }
            set
            {
                _bg = value;
            }
        }

        public Guid? TbC_ParentColumnKey
        {
            get { return _bh; }
            set
            {
                _bh = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_ParentColumnKey_noevents
        {
            get { return _bh; }
            set
            {
                _bh = value;
            }
        }

        public String TbC_ParentColumnKey_TextField
        {
            get { return _bi; }
            set
            {
                _bi = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ParentColumnKey_TextField_noevents
        {
            get { return _bi; }
            set
            {
                _bi = value;
            }
        }

        public Boolean TbC_UseDisplayTextFieldProperty
        {
            get { return _bj; }
            set
            {
                _bj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_UseDisplayTextFieldProperty_noevents
        {
            get { return _bj; }
            set
            {
                _bj = value;
            }
        }

        public Boolean TbC_IsDisplayTextFieldProperty
        {
            get { return _bk; }
            set
            {
                _bk = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsDisplayTextFieldProperty_noevents
        {
            get { return _bk; }
            set
            {
                _bk = value;
            }
        }

        public Guid? TbC_ComboListTable_Id
        {
            get { return _bl; }
            set
            {
                _bl = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_ComboListTable_Id_noevents
        {
            get { return _bl; }
            set
            {
                _bl = value;
            }
        }

        public String TbC_ComboListTable_Id_TextField
        {
            get { return _bm; }
            set
            {
                _bm = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ComboListTable_Id_TextField_noevents
        {
            get { return _bm; }
            set
            {
                _bm = value;
            }
        }

        public Guid? TbC_ComboListDisplayColumn_Id
        {
            get { return _bn; }
            set
            {
                _bn = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_ComboListDisplayColumn_Id_noevents
        {
            get { return _bn; }
            set
            {
                _bn = value;
            }
        }

        public String TbC_ComboListDisplayColumn_Id_TextField
        {
            get { return _bo; }
            set
            {
                _bo = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ComboListDisplayColumn_Id_TextField_noevents
        {
            get { return _bo; }
            set
            {
                _bo = value;
            }
        }

        public Double? TbC_Combo_MaxDropdownHeight
        {
            get { return _bp; }
            set
            {
                _bp = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Double? TbC_Combo_MaxDropdownHeight_noevents
        {
            get { return _bp; }
            set
            {
                _bp = value;
            }
        }

        public Double? TbC_Combo_MaxDropdownWidth
        {
            get { return _bq; }
            set
            {
                _bq = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Double? TbC_Combo_MaxDropdownWidth_noevents
        {
            get { return _bq; }
            set
            {
                _bq = value;
            }
        }

        public Boolean TbC_Combo_AllowDropdownResizing
        {
            get { return _br; }
            set
            {
                _br = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_Combo_AllowDropdownResizing_noevents
        {
            get { return _br; }
            set
            {
                _br = value;
            }
        }

        public Boolean TbC_Combo_IsResetButtonVisible
        {
            get { return _bs; }
            set
            {
                _bs = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_Combo_IsResetButtonVisible_noevents
        {
            get { return _bs; }
            set
            {
                _bs = value;
            }
        }

        public Boolean TbC_IsActiveRecColumn
        {
            get { return _bt; }
            set
            {
                _bt = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsActiveRecColumn_noevents
        {
            get { return _bt; }
            set
            {
                _bt = value;
            }
        }

        public Boolean TbC_IsDeletedColumn
        {
            get { return _bu; }
            set
            {
                _bu = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsDeletedColumn_noevents
        {
            get { return _bu; }
            set
            {
                _bu = value;
            }
        }

        public Boolean TbC_IsCreatedUserIdColumn
        {
            get { return _bv; }
            set
            {
                _bv = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsCreatedUserIdColumn_noevents
        {
            get { return _bv; }
            set
            {
                _bv = value;
            }
        }

        public Boolean TbC_IsCreatedDateColumn
        {
            get { return _bw; }
            set
            {
                _bw = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsCreatedDateColumn_noevents
        {
            get { return _bw; }
            set
            {
                _bw = value;
            }
        }

        public Boolean TbC_IsUserIdColumn
        {
            get { return _bx; }
            set
            {
                _bx = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsUserIdColumn_noevents
        {
            get { return _bx; }
            set
            {
                _bx = value;
            }
        }

        public Boolean TbC_IsModifiedDateColumn
        {
            get { return _by; }
            set
            {
                _by = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsModifiedDateColumn_noevents
        {
            get { return _by; }
            set
            {
                _by = value;
            }
        }

        public Boolean TbC_IsRowVersionStampColumn
        {
            get { return _bz; }
            set
            {
                _bz = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsRowVersionStampColumn_noevents
        {
            get { return _bz; }
            set
            {
                _bz = value;
            }
        }

        public Boolean TbC_IsBrowsable
        {
            get { return _ca; }
            set
            {
                _ca = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsBrowsable_noevents
        {
            get { return _ca; }
            set
            {
                _ca = value;
            }
        }

        public String TbC_DeveloperNote
        {
            get { return _cb; }
            set
            {
                _cb = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_DeveloperNote_noevents
        {
            get { return _cb; }
            set
            {
                _cb = value;
            }
        }

        public String TbC_UserNote
        {
            get { return _cc; }
            set
            {
                _cc = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_UserNote_noevents
        {
            get { return _cc; }
            set
            {
                _cc = value;
            }
        }

        public String TbC_HelpFileAdditionalNote
        {
            get { return _cd; }
            set
            {
                _cd = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_HelpFileAdditionalNote_noevents
        {
            get { return _cd; }
            set
            {
                _cd = value;
            }
        }

        public String TbC_Notes
        {
            get { return _ce; }
            set
            {
                _ce = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_Notes_noevents
        {
            get { return _ce; }
            set
            {
                _ce = value;
            }
        }

        public Boolean TbC_IsInputComplete
        {
            get { return _cf; }
            set
            {
                _cf = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsInputComplete_noevents
        {
            get { return _cf; }
            set
            {
                _cf = value;
            }
        }

        public Boolean TbC_IsCodeGen
        {
            get { return _cg; }
            set
            {
                _cg = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsCodeGen_noevents
        {
            get { return _cg; }
            set
            {
                _cg = value;
            }
        }

        public Boolean? TbC_IsReadyCodeGen
        {
            get { return _ch; }
            set
            {
                _ch = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? TbC_IsReadyCodeGen_noevents
        {
            get { return _ch; }
            set
            {
                _ch = value;
            }
        }

        public Boolean? TbC_IsCodeGenComplete
        {
            get { return _ci; }
            set
            {
                _ci = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? TbC_IsCodeGenComplete_noevents
        {
            get { return _ci; }
            set
            {
                _ci = value;
            }
        }

        public Boolean? TbC_IsTagForCodeGen
        {
            get { return _cj; }
            set
            {
                _cj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? TbC_IsTagForCodeGen_noevents
        {
            get { return _cj; }
            set
            {
                _cj = value;
            }
        }

        public Boolean? TbC_IsTagForOther
        {
            get { return _ck; }
            set
            {
                _ck = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? TbC_IsTagForOther_noevents
        {
            get { return _ck; }
            set
            {
                _ck = value;
            }
        }

        public String TbC_ColumnGroups
        {
            get { return _cl; }
            set
            {
                _cl = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ColumnGroups_noevents
        {
            get { return _cl; }
            set
            {
                _cl = value;
            }
        }

        public Boolean TbC_IsActiveRow
        {
            get { return _cm; }
            set
            {
                _cm = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsActiveRow_noevents
        {
            get { return _cm; }
            set
            {
                _cm = value;
            }
        }

        public Boolean TbC_IsDeleted
        {
            get { return _cnxx; }
            set
            {
                _cnxx = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsDeleted_noevents
        {
            get { return _cnxx; }
            set
            {
                _cnxx = value;
            }
        }

        public Guid? TbC_CreatedUserId
        {
            get { return _co; }
            set
            {
                _co = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_CreatedUserId_noevents
        {
            get { return _co; }
            set
            {
                _co = value;
            }
        }

        public DateTime? TbC_CreatedDate
        {
            get { return _cp; }
            set
            {
                _cp = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? TbC_CreatedDate_noevents
        {
            get { return _cp; }
            set
            {
                _cp = value;
            }
        }

        public Guid? TbC_UserId
        {
            get { return _cq; }
            set
            {
                _cq = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_UserId_noevents
        {
            get { return _cq; }
            set
            {
                _cq = value;
            }
        }

        public String UserName
        {
            get { return _cr; }
            set
            {
                _cr = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _cr; }
            set
            {
                _cr = value;
            }
        }

        public DateTime? TbC_LastModifiedDate
        {
            get { return _cs; }
            set
            {
                _cs = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? TbC_LastModifiedDate_noevents
        {
            get { return _cs; }
            set
            {
                _cs = value;
            }
        }

        public Byte[] TbC_Stamp
        {
            get { return _ct; }
            set
            {
                _ct = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] TbC_Stamp_noevents
        {
            get { return _ct; }
            set
            {
                _ct = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r,
				_s,
				_t,
				_u,
				_v,
				_w,
				_x,
				_y,
				_z,
				_aa,
				_ab,
				_ac,
				_ad,
				_ae,
				_af,
				_ag,
				_ah,
				_ai,
				_aj,
				_ak,
				_al,
				_am,
				_an,
				_ao,
				_ap,
				_aq,
				_ar,
				_asxx,
				_at,
				_au,
				_av,
				_aw,
				_ax,
				_ay,
				_az,
				_ba,
				_bb,
				_bc,
				_bd,
				_be,
				_bf,
				_bg,
				_bh,
				_bi,
				_bj,
				_bk,
				_bl,
				_bm,
				_bn,
				_bo,
				_bp,
				_bq,
				_br,
				_bs,
				_bt,
				_bu,
				_bv,
				_bw,
				_bx,
				_by,
				_bz,
				_ca,
				_cb,
				_cc,
				_cd,
				_ce,
				_cf,
				_cg,
				_ch,
				_ci,
				_cj,
				_ck,
				_cl,
				_cm,
				_cnxx,
				_co,
				_cp,
				_cq,
				_cr,
				_cs,
				_ct
			};
        }

        public WcTableColumn_Values Clone()
        {
            return new WcTableColumn_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


