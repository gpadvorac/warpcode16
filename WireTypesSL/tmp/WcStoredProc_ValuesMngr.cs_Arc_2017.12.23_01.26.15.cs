using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  12/1/2016 10:43:36 AM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcStoredProc_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProc_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcStoredProc_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcStoredProc_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcStoredProc_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcStoredProc_Values(currentData, this) : new WcStoredProc_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcStoredProc_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcStoredProc_Values _original;
        [DataMember]
        public WcStoredProc_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcStoredProc_Values _current;
        [DataMember]
        public WcStoredProc_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcStoredProc_Values _concurrent;
        [DataMember]
        public WcStoredProc_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  Sp_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  Sp_ApVrsn_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  Sp_Name
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  Sp_TypeName
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  Sp_MethodName
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  Sp_IsBuildDataAccessMethod
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  Sp_AssocEntity_Id
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  Sp_AssocEntity_Id_TextField
                //if (_current._h != _original._h)
                //{
                //    return true;
                //}

                //  Sp_IsFetchEntity
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  Sp_IsTypeComboItem
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  Sp_IsStaticList
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  Sp_SpRsTp_Id
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  Sp_SpRsTp_Id_TextField
                //if (_current._m != _original._m)
                //{
                //    return true;
                //}

                //  Sp_SpRtTp_Id
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  Sp_SpRtTp_Id_TextField
                //if (_current._o != _original._o)
                //{
                //    return true;
                //}

                //  Sp_IsInputParamsAsObjectArray
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  Sp_HasNewParams
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  Sp_IsParamsValid
                if (_current._r != _original._r)
                {
                    return true;
                }

                //  Sp_IsParamValueSetValid
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  Sp_HasNewColumns
                if (_current._t != _original._t)
                {
                    return true;
                }

                //  Sp_IsColumnsValid
                if (_current._u != _original._u)
                {
                    return true;
                }

                //  Sp_IsValid
                if (_current._v != _original._v)
                {
                    return true;
                }

                //  Sp_Notes
                if (_current._w != _original._w)
                {
                    return true;
                }

                //  Sp_IsActiveRow
                if (_current._x != _original._x)
                {
                    return true;
                }

                //  Sp_IsDeleted
                if (_current._y != _original._y)
                {
                    return true;
                }

                //  Sp_CreatedUserId
                if (_current._z != _original._z)
                {
                    return true;
                }

                //  Sp_CreatedDate
                if (_current._aa != _original._aa)
                {
                    return true;
                }

                //  Sp_UserId
                if (_current._ab != _original._ab)
                {
                    return true;
                }

                //  UserName
                if (_current._ac != _original._ac)
                {
                    return true;
                }

                //  Sp_LastModifiedDate
                if (_current._ad != _original._ad)
                {
                    return true;
                }

                //  Sp_Stamp
                if (_current._ae != _original._ae)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{32}{0}{31}_b{32}{1}{31}_c{32}{2}{31}_d{32}{3}{31}_e{32}{4}{31}_f{32}{5}{31}_g{32}{6}{31}_h{32}{7}{31}_i{32}{8}{31}_j{32}{9}{31}_k{32}{10}{31}_l{32}{11}{31}_m{32}{12}{31}_n{32}{13}{31}_o{32}{14}{31}_p{32}{15}{31}_q{32}{16}{31}_r{32}{17}{31}_s{32}{18}{31}_t{32}{19}{31}_u{32}{20}{31}_v{32}{21}{31}_w{32}{22}{31}_x{32}{23}{31}_y{32}{24}{31}_z{32}{25}{31}_aa{32}{26}{31}_ab{32}{27}{31}_ac{32}{28}{31}_ad{32}{29}{31}_ae{32}{30}",
				new object[] {
				_current._a,		  //Sp_Id
				_current._b,		  //Sp_ApVrsn_Id
				_current._c,		  //Sp_Name
				_current._d,		  //Sp_TypeName
				_current._e,		  //Sp_MethodName
				_current._f,		  //Sp_IsBuildDataAccessMethod
				_current._g,		  //Sp_AssocEntity_Id
				_current._h,		  //Sp_AssocEntity_Id_TextField
				_current._i,		  //Sp_IsFetchEntity
				_current._j,		  //Sp_IsTypeComboItem
				_current._k,		  //Sp_IsStaticList
				_current._l,		  //Sp_SpRsTp_Id
				_current._m,		  //Sp_SpRsTp_Id_TextField
				_current._n,		  //Sp_SpRtTp_Id
				_current._o,		  //Sp_SpRtTp_Id_TextField
				_current._p,		  //Sp_IsInputParamsAsObjectArray
				_current._q,		  //Sp_HasNewParams
				_current._r,		  //Sp_IsParamsValid
				_current._s,		  //Sp_IsParamValueSetValid
				_current._t,		  //Sp_HasNewColumns
				_current._u,		  //Sp_IsColumnsValid
				_current._v,		  //Sp_IsValid
				_current._w,		  //Sp_Notes
				_current._x,		  //Sp_IsActiveRow
				_current._y,		  //Sp_IsDeleted
				_current._z,		  //Sp_CreatedUserId
				_current._aa,		  //Sp_CreatedDate
				_current._ab,		  //Sp_UserId
				_current._ac,		  //UserName
				_current._ad,		  //Sp_LastModifiedDate
				_current._ae,		  //Sp_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcStoredProc_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProc_Values";
        private WcStoredProc_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcStoredProc_Values() 
        {
        }

        //public WcStoredProc_Values(object[] data, WcStoredProc_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcStoredProc_Values(object[] data, WcStoredProc_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Sp_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Sp_ApVrsn_Id
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  Sp_Name
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  Sp_TypeName
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  Sp_MethodName
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  Sp_IsBuildDataAccessMethod
				_g = ObjectHelper.GetNullableGuidFromObjectValue(data[6]);						//  Sp_AssocEntity_Id
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  Sp_AssocEntity_Id_TextField
				_i = ObjectHelper.GetBoolFromObjectValue(data[8]);									//  Sp_IsFetchEntity
				_j = ObjectHelper.GetNullableBoolFromObjectValue(data[9]);					//  Sp_IsTypeComboItem
				_k = ObjectHelper.GetBoolFromObjectValue(data[10]);									//  Sp_IsStaticList
				_l = ObjectHelper.GetNullableIntFromObjectValue(data[11]);							//  Sp_SpRsTp_Id
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  Sp_SpRsTp_Id_TextField
				_n = ObjectHelper.GetNullableIntFromObjectValue(data[13]);							//  Sp_SpRtTp_Id
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  Sp_SpRtTp_Id_TextField
				_p = ObjectHelper.GetBoolFromObjectValue(data[15]);									//  Sp_IsInputParamsAsObjectArray
				_q = ObjectHelper.GetBoolFromObjectValue(data[16]);									//  Sp_HasNewParams
				_r = ObjectHelper.GetBoolFromObjectValue(data[17]);									//  Sp_IsParamsValid
				_s = ObjectHelper.GetBoolFromObjectValue(data[18]);									//  Sp_IsParamValueSetValid
				_t = ObjectHelper.GetBoolFromObjectValue(data[19]);									//  Sp_HasNewColumns
				_u = ObjectHelper.GetBoolFromObjectValue(data[20]);									//  Sp_IsColumnsValid
				_v = ObjectHelper.GetBoolFromObjectValue(data[21]);									//  Sp_IsValid
				_w = ObjectHelper.GetStringFromObjectValue(data[22]);									//  Sp_Notes
				_x = ObjectHelper.GetBoolFromObjectValue(data[23]);									//  Sp_IsActiveRow
				_y = ObjectHelper.GetBoolFromObjectValue(data[24]);									//  Sp_IsDeleted
				_z = ObjectHelper.GetNullableGuidFromObjectValue(data[25]);						//  Sp_CreatedUserId
				_aa = ObjectHelper.GetNullableDateTimeFromObjectValue(data[26]);					//  Sp_CreatedDate
				_ab = ObjectHelper.GetNullableGuidFromObjectValue(data[27]);						//  Sp_UserId
				_ac = ObjectHelper.GetStringFromObjectValue(data[28]);									//  UserName
				_ad = ObjectHelper.GetNullableDateTimeFromObjectValue(data[29]);					//  Sp_LastModifiedDate
				_ae = ObjectHelper.GetByteArrayFromObjectValue(data[30]);						//  Sp_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProc", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Sp_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Sp_ApVrsn_Id
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  Sp_Name
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  Sp_TypeName
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  Sp_MethodName
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  Sp_IsBuildDataAccessMethod
				_g = ObjectHelper.GetNullableGuidFromObjectValue(data[6]);						//  Sp_AssocEntity_Id
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  Sp_AssocEntity_Id_TextField
				_i = ObjectHelper.GetBoolFromObjectValue(data[8]);									//  Sp_IsFetchEntity
				_j = ObjectHelper.GetNullableBoolFromObjectValue(data[9]);					//  Sp_IsTypeComboItem
				_k = ObjectHelper.GetBoolFromObjectValue(data[10]);									//  Sp_IsStaticList
				_l = ObjectHelper.GetNullableIntFromObjectValue(data[11]);							//  Sp_SpRsTp_Id
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  Sp_SpRsTp_Id_TextField
				_n = ObjectHelper.GetNullableIntFromObjectValue(data[13]);							//  Sp_SpRtTp_Id
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  Sp_SpRtTp_Id_TextField
				_p = ObjectHelper.GetBoolFromObjectValue(data[15]);									//  Sp_IsInputParamsAsObjectArray
				_q = ObjectHelper.GetBoolFromObjectValue(data[16]);									//  Sp_HasNewParams
				_r = ObjectHelper.GetBoolFromObjectValue(data[17]);									//  Sp_IsParamsValid
				_s = ObjectHelper.GetBoolFromObjectValue(data[18]);									//  Sp_IsParamValueSetValid
				_t = ObjectHelper.GetBoolFromObjectValue(data[19]);									//  Sp_HasNewColumns
				_u = ObjectHelper.GetBoolFromObjectValue(data[20]);									//  Sp_IsColumnsValid
				_v = ObjectHelper.GetBoolFromObjectValue(data[21]);									//  Sp_IsValid
				_w = ObjectHelper.GetStringFromObjectValue(data[22]);									//  Sp_Notes
				_x = ObjectHelper.GetBoolFromObjectValue(data[23]);									//  Sp_IsActiveRow
				_y = ObjectHelper.GetBoolFromObjectValue(data[24]);									//  Sp_IsDeleted
				_z = ObjectHelper.GetNullableGuidFromObjectValue(data[25]);						//  Sp_CreatedUserId
				_aa = ObjectHelper.GetNullableDateTimeFromObjectValue(data[26]);					//  Sp_CreatedDate
				_ab = ObjectHelper.GetNullableGuidFromObjectValue(data[27]);						//  Sp_UserId
				_ac = ObjectHelper.GetStringFromObjectValue(data[28]);									//  UserName
				_ad = ObjectHelper.GetNullableDateTimeFromObjectValue(data[29]);					//  Sp_LastModifiedDate
				_ae = ObjectHelper.GetByteArrayFromObjectValue(data[30]);						//  Sp_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProc", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProc", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  Sp_Id

		[DataMember]
		public Guid? _b;			//  Sp_ApVrsn_Id

		[DataMember]
		public String _c;			//  Sp_Name

		[DataMember]
		public String _d;			//  Sp_TypeName

		[DataMember]
		public String _e;			//  Sp_MethodName

		[DataMember]
		public Boolean _f;			//  Sp_IsBuildDataAccessMethod

		[DataMember]
		public Guid? _g;			//  Sp_AssocEntity_Id

		[DataMember]
		public String _h;			//  Sp_AssocEntity_Id_TextField

		[DataMember]
		public Boolean _i;			//  Sp_IsFetchEntity

		[DataMember]
		public Boolean? _j;			//  Sp_IsTypeComboItem

		[DataMember]
		public Boolean _k;			//  Sp_IsStaticList

		[DataMember]
		public Int32? _l;			//  Sp_SpRsTp_Id

		[DataMember]
		public String _m;			//  Sp_SpRsTp_Id_TextField

		[DataMember]
		public Int32? _n;			//  Sp_SpRtTp_Id

		[DataMember]
		public String _o;			//  Sp_SpRtTp_Id_TextField

		[DataMember]
		public Boolean _p;			//  Sp_IsInputParamsAsObjectArray

		[DataMember]
		public Boolean _q;			//  Sp_HasNewParams

		[DataMember]
		public Boolean _r;			//  Sp_IsParamsValid

		[DataMember]
		public Boolean _s;			//  Sp_IsParamValueSetValid

		[DataMember]
		public Boolean _t;			//  Sp_HasNewColumns

		[DataMember]
		public Boolean _u;			//  Sp_IsColumnsValid

		[DataMember]
		public Boolean _v;			//  Sp_IsValid

		[DataMember]
		public String _w;			//  Sp_Notes

		[DataMember]
		public Boolean _x;			//  Sp_IsActiveRow

		[DataMember]
		public Boolean _y;			//  Sp_IsDeleted

		[DataMember]
		public Guid? _z;			//  Sp_CreatedUserId

		[DataMember]
		public DateTime? _aa;			//  Sp_CreatedDate

		[DataMember]
		public Guid? _ab;			//  Sp_UserId

		[DataMember]
		public String _ac;			//  UserName

		[DataMember]
		public DateTime? _ad;			//  Sp_LastModifiedDate

		[DataMember]
		public Byte[] _ae;			//  Sp_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcStoredProc_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 31; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                    _list[18] = _s;
                    _list[19] = _t;
                    _list[20] = _u;
                    _list[21] = _v;
                    _list[22] = _w;
                    _list[23] = _x;
                    _list[24] = _y;
                    _list[25] = _z;
                    _list[26] = _aa;
                    _list[27] = _ab;
                    _list[28] = _ac;
                    _list[29] = _ad;
                    _list[30] = _ae;
                }
                return _list;
            }
        }

        public Guid Sp_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid Sp_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? Sp_ApVrsn_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Sp_ApVrsn_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public String Sp_Name
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_Name_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String Sp_TypeName
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_TypeName_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String Sp_MethodName
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_MethodName_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Boolean Sp_IsBuildDataAccessMethod
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsBuildDataAccessMethod_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Guid? Sp_AssocEntity_Id
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Sp_AssocEntity_Id_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public String Sp_AssocEntity_Id_TextField
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_AssocEntity_Id_TextField_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Boolean Sp_IsFetchEntity
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsFetchEntity_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Boolean? Sp_IsTypeComboItem
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? Sp_IsTypeComboItem_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public Boolean Sp_IsStaticList
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsStaticList_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public Int32? Sp_SpRsTp_Id
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? Sp_SpRsTp_Id_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public String Sp_SpRsTp_Id_TextField
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_SpRsTp_Id_TextField_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public Int32? Sp_SpRtTp_Id
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? Sp_SpRtTp_Id_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public String Sp_SpRtTp_Id_TextField
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_SpRtTp_Id_TextField_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public Boolean Sp_IsInputParamsAsObjectArray
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsInputParamsAsObjectArray_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public Boolean Sp_HasNewParams
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_HasNewParams_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public Boolean Sp_IsParamsValid
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsParamsValid_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        public Boolean Sp_IsParamValueSetValid
        {
            get { return _s; }
            set
            {
                _s = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsParamValueSetValid_noevents
        {
            get { return _s; }
            set
            {
                _s = value;
            }
        }

        public Boolean Sp_HasNewColumns
        {
            get { return _t; }
            set
            {
                _t = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_HasNewColumns_noevents
        {
            get { return _t; }
            set
            {
                _t = value;
            }
        }

        public Boolean Sp_IsColumnsValid
        {
            get { return _u; }
            set
            {
                _u = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsColumnsValid_noevents
        {
            get { return _u; }
            set
            {
                _u = value;
            }
        }

        public Boolean Sp_IsValid
        {
            get { return _v; }
            set
            {
                _v = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsValid_noevents
        {
            get { return _v; }
            set
            {
                _v = value;
            }
        }

        public String Sp_Notes
        {
            get { return _w; }
            set
            {
                _w = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_Notes_noevents
        {
            get { return _w; }
            set
            {
                _w = value;
            }
        }

        public Boolean Sp_IsActiveRow
        {
            get { return _x; }
            set
            {
                _x = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsActiveRow_noevents
        {
            get { return _x; }
            set
            {
                _x = value;
            }
        }

        public Boolean Sp_IsDeleted
        {
            get { return _y; }
            set
            {
                _y = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsDeleted_noevents
        {
            get { return _y; }
            set
            {
                _y = value;
            }
        }

        public Guid? Sp_CreatedUserId
        {
            get { return _z; }
            set
            {
                _z = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Sp_CreatedUserId_noevents
        {
            get { return _z; }
            set
            {
                _z = value;
            }
        }

        public DateTime? Sp_CreatedDate
        {
            get { return _aa; }
            set
            {
                _aa = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Sp_CreatedDate_noevents
        {
            get { return _aa; }
            set
            {
                _aa = value;
            }
        }

        public Guid? Sp_UserId
        {
            get { return _ab; }
            set
            {
                _ab = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Sp_UserId_noevents
        {
            get { return _ab; }
            set
            {
                _ab = value;
            }
        }

        public String UserName
        {
            get { return _ac; }
            set
            {
                _ac = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _ac; }
            set
            {
                _ac = value;
            }
        }

        public DateTime? Sp_LastModifiedDate
        {
            get { return _ad; }
            set
            {
                _ad = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Sp_LastModifiedDate_noevents
        {
            get { return _ad; }
            set
            {
                _ad = value;
            }
        }

        public Byte[] Sp_Stamp
        {
            get { return _ae; }
            set
            {
                _ae = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] Sp_Stamp_noevents
        {
            get { return _ae; }
            set
            {
                _ae = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r,
				_s,
				_t,
				_u,
				_v,
				_w,
				_x,
				_y,
				_z,
				_aa,
				_ab,
				_ac,
				_ad,
				_ae
			};
        }

        public WcStoredProc_Values Clone()
        {
            return new WcStoredProc_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


