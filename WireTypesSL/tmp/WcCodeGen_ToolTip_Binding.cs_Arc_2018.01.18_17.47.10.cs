using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/18/2018 4:57:18 PM

namespace EntityWireTypeSL
{


    [DataContract]
    public class WcCodeGen_ToolTip_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcCodeGen_ToolTip_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcCodeGen_ToolTip_Binding() { }


        public WcCodeGen_ToolTip_Binding(Guid _Tt_Id, Guid _Tt_TbC_Id, Int32 _Tt_Sort, String _Tt_Tip, Boolean _Tt_IsActiveRow, Boolean _Tt_IsDeleted, Guid _Tt_CreatedUserId, DateTime _Tt_CreatedDate, Guid _Tt_UserId, String _UserName, DateTime _Tt_LastModifiedDate, Byte[] _Tt_Stamp )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ToolTip_Binding", IfxTraceCategory.Enter);
				_a = _Tt_Id;
				_b = _Tt_TbC_Id;
				_c = _Tt_Sort;
				_d = _Tt_Tip;
				_e = _Tt_IsActiveRow;
				_f = _Tt_IsDeleted;
				_g = _Tt_CreatedUserId;
				_h = _Tt_CreatedDate;
				_i = _Tt_UserId;
				_j = _UserName;
				_k = _Tt_LastModifiedDate;
				_l = _Tt_Stamp;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ToolTip_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ToolTip_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcCodeGen_ToolTip_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ToolTip_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  Tt_Id
				_b = (Guid)data[1];                //  Tt_TbC_Id
				_c = (Int32)data[2];                //  Tt_Sort
				_d = (String)data[3];                //  Tt_Tip
				_e = (Boolean)data[4];                //  Tt_IsActiveRow
				_f = (Boolean)data[5];                //  Tt_IsDeleted
				_g = (Guid)data[6];                //  Tt_CreatedUserId
				_h = (DateTime)data[7];                //  Tt_CreatedDate
				_i = (Guid)data[8];                //  Tt_UserId
				_j = (String)data[9];                //  UserName
				_k = (DateTime)data[10];                //  Tt_LastModifiedDate
				_l = (Byte[])data[11];                //  Tt_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ToolTip_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ToolTip_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region Tt_Id

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid Tt_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Tt_Id


        #region Tt_TbC_Id

        private Guid _b;
//        public Guid B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Guid Tt_TbC_Id
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion Tt_TbC_Id


        #region Tt_Sort

        private Int32 _c;
//        public Int32 C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public Int32 Tt_Sort
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion Tt_Sort


        #region Tt_Tip

        private String _d;
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String Tt_Tip
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion Tt_Tip


        #region Tt_IsActiveRow

        private Boolean _e;
//        public Boolean E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public Boolean Tt_IsActiveRow
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion Tt_IsActiveRow


        #region Tt_IsDeleted

        private Boolean _f;
//        public Boolean F
//        {
//            get { return _f; }
//            set { _f = value; }
//        }

        //[NonSerialized]
        public Boolean Tt_IsDeleted
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion Tt_IsDeleted


        #region Tt_CreatedUserId

        private Guid _g;
//        public Guid G
//        {
//            get { return _g; }
//            set { _g = value; }
//        }

        //[NonSerialized]
        public Guid Tt_CreatedUserId
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion Tt_CreatedUserId


        #region Tt_CreatedDate

        private DateTime _h;
//        public DateTime H
//        {
//            get { return _h; }
//            set { _h = value; }
//        }

        //[NonSerialized]
        public DateTime Tt_CreatedDate
        {
            get { return _h; }
            set { _h = value; }
        }

        #endregion Tt_CreatedDate


        #region Tt_UserId

        private Guid _i;
//        public Guid I
//        {
//            get { return _i; }
//            set { _i = value; }
//        }

        //[NonSerialized]
        public Guid Tt_UserId
        {
            get { return _i; }
            set { _i = value; }
        }

        #endregion Tt_UserId


        #region UserName

        private String _j;
//        public String J
//        {
//            get { return _j; }
//            set { _j = value; }
//        }

        //[NonSerialized]
        public String UserName
        {
            get { return _j; }
            set { _j = value; }
        }

        #endregion UserName


        #region Tt_LastModifiedDate

        private DateTime _k;
//        public DateTime K
//        {
//            get { return _k; }
//            set { _k = value; }
//        }

        //[NonSerialized]
        public DateTime Tt_LastModifiedDate
        {
            get { return _k; }
            set { _k = value; }
        }

        #endregion Tt_LastModifiedDate


        #region Tt_Stamp

        private Byte[] _l;
//        public Byte[] L
//        {
//            get { return _l; }
//            set { _l = value; }
//        }

        //[NonSerialized]
        public Byte[] Tt_Stamp
        {
            get { return _l; }
            set { _l = value; }
        }

        #endregion Tt_Stamp


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11} ", Tt_Id, Tt_TbC_Id, Tt_Sort, Tt_Tip, Tt_IsActiveRow, Tt_IsDeleted, Tt_CreatedUserId, Tt_CreatedDate, Tt_UserId, UserName, Tt_LastModifiedDate, Tt_Stamp );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11} ", Tt_Id, Tt_TbC_Id, Tt_Sort, Tt_Tip, Tt_IsActiveRow, Tt_IsDeleted, Tt_CreatedUserId, Tt_CreatedDate, Tt_UserId, UserName, Tt_LastModifiedDate, Tt_Stamp );
        }

    }

}
