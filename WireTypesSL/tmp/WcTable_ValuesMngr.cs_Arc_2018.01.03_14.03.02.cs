using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  1/2/2018 8:00:49 PM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcTable_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTable_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcTable_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcTable_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcTable_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcTable_Values(currentData, this) : new WcTable_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcTable_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcTable_Values _original;
        [DataMember]
        public WcTable_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcTable_Values _current;
        [DataMember]
        public WcTable_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcTable_Values _concurrent;
        [DataMember]
        public WcTable_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  Tb_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  Tb_ApVrsn_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  Tb_auditTable_Id
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  AttachmentCount
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  AttachmentFileNames
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  DiscussionCount
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  DiscussionTitles
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  Tb_Name
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  Tb_EntityRootName
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  Tb_VariableName
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  Tb_ScreenCaption
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  Tb_Description
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  Tb_DevelopmentNote
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  Tb_UIAssemblyName
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  Tb_UINamespace
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  Tb_UIAssemblyPath
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  Tb_ProxyAssemblyName
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  Tb_ProxyAssemblyPath
                if (_current._r != _original._r)
                {
                    return true;
                }

                //  Tb_WebServiceName
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  Tb_WebServiceFolder
                if (_current._t != _original._t)
                {
                    return true;
                }

                //  Tb_DataServiceAssemblyName
                if (_current._u != _original._u)
                {
                    return true;
                }

                //  Tb_DataServicePath
                if (_current._v != _original._v)
                {
                    return true;
                }

                //  Tb_WireTypeAssemblyName
                if (_current._w != _original._w)
                {
                    return true;
                }

                //  Tb_WireTypePath
                if (_current._x != _original._x)
                {
                    return true;
                }

                //  Tb_UseTilesInPropsScreen
                if (_current._y != _original._y)
                {
                    return true;
                }

                //  Tb_UseGridColumnGroups
                if (_current._z != _original._z)
                {
                    return true;
                }

                //  Tb_UseGridDataSourceCombo
                if (_current._aa != _original._aa)
                {
                    return true;
                }

                //  Tb_ApCnSK_Id
                if (_current._ab != _original._ab)
                {
                    return true;
                }

                //  Tb_ApCnSK_Id_TextField
                //if (_current._ac != _original._ac)
                //{
                //    return true;
                //}

                //  Tb_UseLegacyConnectionCode
                if (_current._ad != _original._ad)
                {
                    return true;
                }

                //  Tb_PkIsIdentity
                if (_current._ae != _original._ae)
                {
                    return true;
                }

                //  Tb_IsVirtual
                if (_current._af != _original._af)
                {
                    return true;
                }

                //  Tb_IsScreenPlaceHolder
                if (_current._ag != _original._ag)
                {
                    return true;
                }

                //  Tb_IsNotEntity
                if (_current._ah != _original._ah)
                {
                    return true;
                }

                //  Tb_IsMany2Many
                if (_current._ai != _original._ai)
                {
                    return true;
                }

                //  Tb_UseLastModifiedByUserNameInSproc
                if (_current._aj != _original._aj)
                {
                    return true;
                }

                //  Tb_UseUserTimeStamp
                if (_current._ak != _original._ak)
                {
                    return true;
                }

                //  Tb_UseForAudit
                if (_current._al != _original._al)
                {
                    return true;
                }

                //  Tb_IsAllowDelete
                if (_current._am != _original._am)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_MenuRow_IsVisible
                if (_current._an != _original._an)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_GridTools_IsVisible
                if (_current._ao != _original._ao)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_SplitScreen_IsVisible
                if (_current._ap != _original._ap)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
                if (_current._aq != _original._aq)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_NavColumnWidth
                if (_current._ar != _original._ar)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_IsReadOnly
                if (_current._asxx != _original._asxx)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_IsAllowNewRow
                if (_current._at != _original._at)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_ExcelExport_IsVisible
                if (_current._au != _original._au)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_ColumnChooser_IsVisible
                if (_current._av != _original._av)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
                if (_current._aw != _original._aw)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_RefreshGrid_IsVisible
                if (_current._ax != _original._ax)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
                if (_current._ay != _original._ay)
                {
                    return true;
                }

                //  Tb_IsInputComplete
                if (_current._az != _original._az)
                {
                    return true;
                }

                //  Tb_IsCodeGen
                if (_current._ba != _original._ba)
                {
                    return true;
                }

                //  Tb_IsReadyCodeGen
                if (_current._bb != _original._bb)
                {
                    return true;
                }

                //  Tb_IsCodeGenComplete
                if (_current._bc != _original._bc)
                {
                    return true;
                }

                //  Tb_IsTagForCodeGen
                if (_current._bd != _original._bd)
                {
                    return true;
                }

                //  Tb_IsTagForOther
                if (_current._be != _original._be)
                {
                    return true;
                }

                //  Tb_TableGroups
                if (_current._bf != _original._bf)
                {
                    return true;
                }

                //  Tb_IsActiveRow
                if (_current._bg != _original._bg)
                {
                    return true;
                }

                //  Tb_IsDeleted
                if (_current._bh != _original._bh)
                {
                    return true;
                }

                //  Tb_CreatedUserId
                if (_current._bi != _original._bi)
                {
                    return true;
                }

                //  Tb_CreatedDate
                if (_current._bj != _original._bj)
                {
                    return true;
                }

                //  Tb_UserId
                if (_current._bk != _original._bk)
                {
                    return true;
                }

                //  UserName
                if (_current._bl != _original._bl)
                {
                    return true;
                }

                //  Tb_LastModifiedDate
                if (_current._bm != _original._bm)
                {
                    return true;
                }

                //  Tb_Stamp
                if (_current._bn != _original._bn)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{67}{0}{66}_b{67}{1}{66}_c{67}{2}{66}_d{67}{3}{66}_e{67}{4}{66}_f{67}{5}{66}_g{67}{6}{66}_h{67}{7}{66}_i{67}{8}{66}_j{67}{9}{66}_k{67}{10}{66}_l{67}{11}{66}_m{67}{12}{66}_n{67}{13}{66}_o{67}{14}{66}_p{67}{15}{66}_q{67}{16}{66}_r{67}{17}{66}_s{67}{18}{66}_t{67}{19}{66}_u{67}{20}{66}_v{67}{21}{66}_w{67}{22}{66}_x{67}{23}{66}_y{67}{24}{66}_z{67}{25}{66}_aa{67}{26}{66}_ab{67}{27}{66}_ac{67}{28}{66}_ad{67}{29}{66}_ae{67}{30}{66}_af{67}{31}{66}_ag{67}{32}{66}_ah{67}{33}{66}_ai{67}{34}{66}_aj{67}{35}{66}_ak{67}{36}{66}_al{67}{37}{66}_am{67}{38}{66}_an{67}{39}{66}_ao{67}{40}{66}_ap{67}{41}{66}_aq{67}{42}{66}_ar{67}{43}{66}_asxx{67}{44}{66}_at{67}{45}{66}_au{67}{46}{66}_av{67}{47}{66}_aw{67}{48}{66}_ax{67}{49}{66}_ay{67}{50}{66}_az{67}{51}{66}_ba{67}{52}{66}_bb{67}{53}{66}_bc{67}{54}{66}_bd{67}{55}{66}_be{67}{56}{66}_bf{67}{57}{66}_bg{67}{58}{66}_bh{67}{59}{66}_bi{67}{60}{66}_bj{67}{61}{66}_bk{67}{62}{66}_bl{67}{63}{66}_bm{67}{64}{66}_bn{67}{65}",
				new object[] {
				_current._a,		  //Tb_Id
				_current._b,		  //Tb_ApVrsn_Id
				_current._c,		  //Tb_auditTable_Id
				_current._d,		  //AttachmentCount
				_current._e,		  //AttachmentFileNames
				_current._f,		  //DiscussionCount
				_current._g,		  //DiscussionTitles
				_current._h,		  //Tb_Name
				_current._i,		  //Tb_EntityRootName
				_current._j,		  //Tb_VariableName
				_current._k,		  //Tb_ScreenCaption
				_current._l,		  //Tb_Description
				_current._m,		  //Tb_DevelopmentNote
				_current._n,		  //Tb_UIAssemblyName
				_current._o,		  //Tb_UINamespace
				_current._p,		  //Tb_UIAssemblyPath
				_current._q,		  //Tb_ProxyAssemblyName
				_current._r,		  //Tb_ProxyAssemblyPath
				_current._s,		  //Tb_WebServiceName
				_current._t,		  //Tb_WebServiceFolder
				_current._u,		  //Tb_DataServiceAssemblyName
				_current._v,		  //Tb_DataServicePath
				_current._w,		  //Tb_WireTypeAssemblyName
				_current._x,		  //Tb_WireTypePath
				_current._y,		  //Tb_UseTilesInPropsScreen
				_current._z,		  //Tb_UseGridColumnGroups
				_current._aa,		  //Tb_UseGridDataSourceCombo
				_current._ab,		  //Tb_ApCnSK_Id
				_current._ac,		  //Tb_ApCnSK_Id_TextField
				_current._ad,		  //Tb_UseLegacyConnectionCode
				_current._ae,		  //Tb_PkIsIdentity
				_current._af,		  //Tb_IsVirtual
				_current._ag,		  //Tb_IsScreenPlaceHolder
				_current._ah,		  //Tb_IsNotEntity
				_current._ai,		  //Tb_IsMany2Many
				_current._aj,		  //Tb_UseLastModifiedByUserNameInSproc
				_current._ak,		  //Tb_UseUserTimeStamp
				_current._al,		  //Tb_UseForAudit
				_current._am,		  //Tb_IsAllowDelete
				_current._an,		  //Tb_CnfgGdMnu_MenuRow_IsVisible
				_current._ao,		  //Tb_CnfgGdMnu_GridTools_IsVisible
				_current._ap,		  //Tb_CnfgGdMnu_SplitScreen_IsVisible
				_current._aq,		  //Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
				_current._ar,		  //Tb_CnfgGdMnu_NavColumnWidth
				_current._asxx,		  //Tb_CnfgGdMnu_IsReadOnly
				_current._at,		  //Tb_CnfgGdMnu_IsAllowNewRow
				_current._au,		  //Tb_CnfgGdMnu_ExcelExport_IsVisible
				_current._av,		  //Tb_CnfgGdMnu_ColumnChooser_IsVisible
				_current._aw,		  //Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
				_current._ax,		  //Tb_CnfgGdMnu_RefreshGrid_IsVisible
				_current._ay,		  //Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
				_current._az,		  //Tb_IsInputComplete
				_current._ba,		  //Tb_IsCodeGen
				_current._bb,		  //Tb_IsReadyCodeGen
				_current._bc,		  //Tb_IsCodeGenComplete
				_current._bd,		  //Tb_IsTagForCodeGen
				_current._be,		  //Tb_IsTagForOther
				_current._bf,		  //Tb_TableGroups
				_current._bg,		  //Tb_IsActiveRow
				_current._bh,		  //Tb_IsDeleted
				_current._bi,		  //Tb_CreatedUserId
				_current._bj,		  //Tb_CreatedDate
				_current._bk,		  //Tb_UserId
				_current._bl,		  //UserName
				_current._bm,		  //Tb_LastModifiedDate
				_current._bn,		  //Tb_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcTable_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTable_Values";
        private WcTable_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcTable_Values() 
        {
        }

        //public WcTable_Values(object[] data, WcTable_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcTable_Values(object[] data, WcTable_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Tb_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Tb_ApVrsn_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  Tb_auditTable_Id
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  AttachmentCount
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  AttachmentFileNames
				_f = ObjectHelper.GetNullableIntFromObjectValue(data[5]);							//  DiscussionCount
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  DiscussionTitles
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  Tb_Name
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  Tb_EntityRootName
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  Tb_VariableName
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  Tb_ScreenCaption
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  Tb_Description
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  Tb_DevelopmentNote
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  Tb_UIAssemblyName
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  Tb_UINamespace
				_p = ObjectHelper.GetStringFromObjectValue(data[15]);									//  Tb_UIAssemblyPath
				_q = ObjectHelper.GetStringFromObjectValue(data[16]);									//  Tb_ProxyAssemblyName
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  Tb_ProxyAssemblyPath
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  Tb_WebServiceName
				_t = ObjectHelper.GetStringFromObjectValue(data[19]);									//  Tb_WebServiceFolder
				_u = ObjectHelper.GetStringFromObjectValue(data[20]);									//  Tb_DataServiceAssemblyName
				_v = ObjectHelper.GetStringFromObjectValue(data[21]);									//  Tb_DataServicePath
				_w = ObjectHelper.GetStringFromObjectValue(data[22]);									//  Tb_WireTypeAssemblyName
				_x = ObjectHelper.GetStringFromObjectValue(data[23]);									//  Tb_WireTypePath
				_y = ObjectHelper.GetBoolFromObjectValue(data[24]);									//  Tb_UseTilesInPropsScreen
				_z = ObjectHelper.GetBoolFromObjectValue(data[25]);									//  Tb_UseGridColumnGroups
				_aa = ObjectHelper.GetBoolFromObjectValue(data[26]);									//  Tb_UseGridDataSourceCombo
				_ab = ObjectHelper.GetNullableGuidFromObjectValue(data[27]);						//  Tb_ApCnSK_Id
				_ac = ObjectHelper.GetStringFromObjectValue(data[28]);									//  Tb_ApCnSK_Id_TextField
				_ad = ObjectHelper.GetBoolFromObjectValue(data[29]);									//  Tb_UseLegacyConnectionCode
				_ae = ObjectHelper.GetBoolFromObjectValue(data[30]);									//  Tb_PkIsIdentity
				_af = ObjectHelper.GetBoolFromObjectValue(data[31]);									//  Tb_IsVirtual
				_ag = ObjectHelper.GetNullableBoolFromObjectValue(data[32]);					//  Tb_IsScreenPlaceHolder
				_ah = ObjectHelper.GetBoolFromObjectValue(data[33]);									//  Tb_IsNotEntity
				_ai = ObjectHelper.GetBoolFromObjectValue(data[34]);									//  Tb_IsMany2Many
				_aj = ObjectHelper.GetBoolFromObjectValue(data[35]);									//  Tb_UseLastModifiedByUserNameInSproc
				_ak = ObjectHelper.GetBoolFromObjectValue(data[36]);									//  Tb_UseUserTimeStamp
				_al = ObjectHelper.GetBoolFromObjectValue(data[37]);									//  Tb_UseForAudit
				_am = ObjectHelper.GetNullableBoolFromObjectValue(data[38]);					//  Tb_IsAllowDelete
				_an = ObjectHelper.GetBoolFromObjectValue(data[39]);									//  Tb_CnfgGdMnu_MenuRow_IsVisible
				_ao = ObjectHelper.GetBoolFromObjectValue(data[40]);									//  Tb_CnfgGdMnu_GridTools_IsVisible
				_ap = ObjectHelper.GetBoolFromObjectValue(data[41]);									//  Tb_CnfgGdMnu_SplitScreen_IsVisible
				_aq = ObjectHelper.GetBoolFromObjectValue(data[42]);									//  Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
				_ar = ObjectHelper.GetIntFromObjectValue(data[43]);										//  Tb_CnfgGdMnu_NavColumnWidth
				_asxx = ObjectHelper.GetBoolFromObjectValue(data[44]);									//  Tb_CnfgGdMnu_IsReadOnly
				_at = ObjectHelper.GetBoolFromObjectValue(data[45]);									//  Tb_CnfgGdMnu_IsAllowNewRow
				_au = ObjectHelper.GetBoolFromObjectValue(data[46]);									//  Tb_CnfgGdMnu_ExcelExport_IsVisible
				_av = ObjectHelper.GetBoolFromObjectValue(data[47]);									//  Tb_CnfgGdMnu_ColumnChooser_IsVisible
				_aw = ObjectHelper.GetBoolFromObjectValue(data[48]);									//  Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
				_ax = ObjectHelper.GetBoolFromObjectValue(data[49]);									//  Tb_CnfgGdMnu_RefreshGrid_IsVisible
				_ay = ObjectHelper.GetBoolFromObjectValue(data[50]);									//  Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
				_az = ObjectHelper.GetBoolFromObjectValue(data[51]);									//  Tb_IsInputComplete
				_ba = ObjectHelper.GetBoolFromObjectValue(data[52]);									//  Tb_IsCodeGen
				_bb = ObjectHelper.GetBoolFromObjectValue(data[53]);									//  Tb_IsReadyCodeGen
				_bc = ObjectHelper.GetBoolFromObjectValue(data[54]);									//  Tb_IsCodeGenComplete
				_bd = ObjectHelper.GetBoolFromObjectValue(data[55]);									//  Tb_IsTagForCodeGen
				_be = ObjectHelper.GetBoolFromObjectValue(data[56]);									//  Tb_IsTagForOther
				_bf = ObjectHelper.GetStringFromObjectValue(data[57]);									//  Tb_TableGroups
				_bg = ObjectHelper.GetBoolFromObjectValue(data[58]);									//  Tb_IsActiveRow
				_bh = ObjectHelper.GetBoolFromObjectValue(data[59]);									//  Tb_IsDeleted
				_bi = ObjectHelper.GetNullableGuidFromObjectValue(data[60]);						//  Tb_CreatedUserId
				_bj = ObjectHelper.GetNullableDateTimeFromObjectValue(data[61]);					//  Tb_CreatedDate
				_bk = ObjectHelper.GetNullableGuidFromObjectValue(data[62]);						//  Tb_UserId
				_bl = ObjectHelper.GetStringFromObjectValue(data[63]);									//  UserName
				_bm = ObjectHelper.GetNullableDateTimeFromObjectValue(data[64]);					//  Tb_LastModifiedDate
				_bn = data[65] as Byte[];						//  Tb_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTable", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Tb_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Tb_ApVrsn_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  Tb_auditTable_Id
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  AttachmentCount
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  AttachmentFileNames
				_f = ObjectHelper.GetNullableIntFromObjectValue(data[5]);							//  DiscussionCount
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  DiscussionTitles
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  Tb_Name
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  Tb_EntityRootName
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  Tb_VariableName
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  Tb_ScreenCaption
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  Tb_Description
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  Tb_DevelopmentNote
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  Tb_UIAssemblyName
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  Tb_UINamespace
				_p = ObjectHelper.GetStringFromObjectValue(data[15]);									//  Tb_UIAssemblyPath
				_q = ObjectHelper.GetStringFromObjectValue(data[16]);									//  Tb_ProxyAssemblyName
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  Tb_ProxyAssemblyPath
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  Tb_WebServiceName
				_t = ObjectHelper.GetStringFromObjectValue(data[19]);									//  Tb_WebServiceFolder
				_u = ObjectHelper.GetStringFromObjectValue(data[20]);									//  Tb_DataServiceAssemblyName
				_v = ObjectHelper.GetStringFromObjectValue(data[21]);									//  Tb_DataServicePath
				_w = ObjectHelper.GetStringFromObjectValue(data[22]);									//  Tb_WireTypeAssemblyName
				_x = ObjectHelper.GetStringFromObjectValue(data[23]);									//  Tb_WireTypePath
				_y = ObjectHelper.GetBoolFromObjectValue(data[24]);									//  Tb_UseTilesInPropsScreen
				_z = ObjectHelper.GetBoolFromObjectValue(data[25]);									//  Tb_UseGridColumnGroups
				_aa = ObjectHelper.GetBoolFromObjectValue(data[26]);									//  Tb_UseGridDataSourceCombo
				_ab = ObjectHelper.GetNullableGuidFromObjectValue(data[27]);						//  Tb_ApCnSK_Id
				_ac = ObjectHelper.GetStringFromObjectValue(data[28]);									//  Tb_ApCnSK_Id_TextField
				_ad = ObjectHelper.GetBoolFromObjectValue(data[29]);									//  Tb_UseLegacyConnectionCode
				_ae = ObjectHelper.GetBoolFromObjectValue(data[30]);									//  Tb_PkIsIdentity
				_af = ObjectHelper.GetBoolFromObjectValue(data[31]);									//  Tb_IsVirtual
				_ag = ObjectHelper.GetNullableBoolFromObjectValue(data[32]);					//  Tb_IsScreenPlaceHolder
				_ah = ObjectHelper.GetBoolFromObjectValue(data[33]);									//  Tb_IsNotEntity
				_ai = ObjectHelper.GetBoolFromObjectValue(data[34]);									//  Tb_IsMany2Many
				_aj = ObjectHelper.GetBoolFromObjectValue(data[35]);									//  Tb_UseLastModifiedByUserNameInSproc
				_ak = ObjectHelper.GetBoolFromObjectValue(data[36]);									//  Tb_UseUserTimeStamp
				_al = ObjectHelper.GetBoolFromObjectValue(data[37]);									//  Tb_UseForAudit
				_am = ObjectHelper.GetNullableBoolFromObjectValue(data[38]);					//  Tb_IsAllowDelete
				_an = ObjectHelper.GetBoolFromObjectValue(data[39]);									//  Tb_CnfgGdMnu_MenuRow_IsVisible
				_ao = ObjectHelper.GetBoolFromObjectValue(data[40]);									//  Tb_CnfgGdMnu_GridTools_IsVisible
				_ap = ObjectHelper.GetBoolFromObjectValue(data[41]);									//  Tb_CnfgGdMnu_SplitScreen_IsVisible
				_aq = ObjectHelper.GetBoolFromObjectValue(data[42]);									//  Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
				_ar = ObjectHelper.GetIntFromObjectValue(data[43]);										//  Tb_CnfgGdMnu_NavColumnWidth
				_asxx = ObjectHelper.GetBoolFromObjectValue(data[44]);									//  Tb_CnfgGdMnu_IsReadOnly
				_at = ObjectHelper.GetBoolFromObjectValue(data[45]);									//  Tb_CnfgGdMnu_IsAllowNewRow
				_au = ObjectHelper.GetBoolFromObjectValue(data[46]);									//  Tb_CnfgGdMnu_ExcelExport_IsVisible
				_av = ObjectHelper.GetBoolFromObjectValue(data[47]);									//  Tb_CnfgGdMnu_ColumnChooser_IsVisible
				_aw = ObjectHelper.GetBoolFromObjectValue(data[48]);									//  Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
				_ax = ObjectHelper.GetBoolFromObjectValue(data[49]);									//  Tb_CnfgGdMnu_RefreshGrid_IsVisible
				_ay = ObjectHelper.GetBoolFromObjectValue(data[50]);									//  Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
				_az = ObjectHelper.GetBoolFromObjectValue(data[51]);									//  Tb_IsInputComplete
				_ba = ObjectHelper.GetBoolFromObjectValue(data[52]);									//  Tb_IsCodeGen
				_bb = ObjectHelper.GetBoolFromObjectValue(data[53]);									//  Tb_IsReadyCodeGen
				_bc = ObjectHelper.GetBoolFromObjectValue(data[54]);									//  Tb_IsCodeGenComplete
				_bd = ObjectHelper.GetBoolFromObjectValue(data[55]);									//  Tb_IsTagForCodeGen
				_be = ObjectHelper.GetBoolFromObjectValue(data[56]);									//  Tb_IsTagForOther
				_bf = ObjectHelper.GetStringFromObjectValue(data[57]);									//  Tb_TableGroups
				_bg = ObjectHelper.GetBoolFromObjectValue(data[58]);									//  Tb_IsActiveRow
				_bh = ObjectHelper.GetBoolFromObjectValue(data[59]);									//  Tb_IsDeleted
				_bi = ObjectHelper.GetNullableGuidFromObjectValue(data[60]);						//  Tb_CreatedUserId
				_bj = ObjectHelper.GetNullableDateTimeFromObjectValue(data[61]);					//  Tb_CreatedDate
				_bk = ObjectHelper.GetNullableGuidFromObjectValue(data[62]);						//  Tb_UserId
				_bl = ObjectHelper.GetStringFromObjectValue(data[63]);									//  UserName
				_bm = ObjectHelper.GetNullableDateTimeFromObjectValue(data[64]);					//  Tb_LastModifiedDate
				_bn = data[65] as Byte[];						//  Tb_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTable", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTable", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  Tb_Id

		[DataMember]
		public Guid? _b;			//  Tb_ApVrsn_Id

		[DataMember]
		public Guid? _c;			//  Tb_auditTable_Id

		[DataMember]
		public Int32? _d;			//  AttachmentCount

		[DataMember]
		public String _e;			//  AttachmentFileNames

		[DataMember]
		public Int32? _f;			//  DiscussionCount

		[DataMember]
		public String _g;			//  DiscussionTitles

		[DataMember]
		public String _h;			//  Tb_Name

		[DataMember]
		public String _i;			//  Tb_EntityRootName

		[DataMember]
		public String _j;			//  Tb_VariableName

		[DataMember]
		public String _k;			//  Tb_ScreenCaption

		[DataMember]
		public String _l;			//  Tb_Description

		[DataMember]
		public String _m;			//  Tb_DevelopmentNote

		[DataMember]
		public String _n;			//  Tb_UIAssemblyName

		[DataMember]
		public String _o;			//  Tb_UINamespace

		[DataMember]
		public String _p;			//  Tb_UIAssemblyPath

		[DataMember]
		public String _q;			//  Tb_ProxyAssemblyName

		[DataMember]
		public String _r;			//  Tb_ProxyAssemblyPath

		[DataMember]
		public String _s;			//  Tb_WebServiceName

		[DataMember]
		public String _t;			//  Tb_WebServiceFolder

		[DataMember]
		public String _u;			//  Tb_DataServiceAssemblyName

		[DataMember]
		public String _v;			//  Tb_DataServicePath

		[DataMember]
		public String _w;			//  Tb_WireTypeAssemblyName

		[DataMember]
		public String _x;			//  Tb_WireTypePath

		[DataMember]
		public Boolean _y;			//  Tb_UseTilesInPropsScreen

		[DataMember]
		public Boolean _z;			//  Tb_UseGridColumnGroups

		[DataMember]
		public Boolean _aa;			//  Tb_UseGridDataSourceCombo

		[DataMember]
		public Guid? _ab;			//  Tb_ApCnSK_Id

		[DataMember]
		public String _ac;			//  Tb_ApCnSK_Id_TextField

		[DataMember]
		public Boolean _ad;			//  Tb_UseLegacyConnectionCode

		[DataMember]
		public Boolean _ae;			//  Tb_PkIsIdentity

		[DataMember]
		public Boolean _af;			//  Tb_IsVirtual

		[DataMember]
		public Boolean? _ag;			//  Tb_IsScreenPlaceHolder

		[DataMember]
		public Boolean _ah;			//  Tb_IsNotEntity

		[DataMember]
		public Boolean _ai;			//  Tb_IsMany2Many

		[DataMember]
		public Boolean _aj;			//  Tb_UseLastModifiedByUserNameInSproc

		[DataMember]
		public Boolean _ak;			//  Tb_UseUserTimeStamp

		[DataMember]
		public Boolean _al;			//  Tb_UseForAudit

		[DataMember]
		public Boolean? _am;			//  Tb_IsAllowDelete

		[DataMember]
		public Boolean _an;			//  Tb_CnfgGdMnu_MenuRow_IsVisible

		[DataMember]
		public Boolean _ao;			//  Tb_CnfgGdMnu_GridTools_IsVisible

		[DataMember]
		public Boolean _ap;			//  Tb_CnfgGdMnu_SplitScreen_IsVisible

		[DataMember]
		public Boolean _aq;			//  Tb_CnfgGdMnu_SplitScreen_IsSplit_Default

		[DataMember]
		public Int32 _ar;			//  Tb_CnfgGdMnu_NavColumnWidth

		[DataMember]
		public Boolean _asxx;			//  Tb_CnfgGdMnu_IsReadOnly

		[DataMember]
		public Boolean _at;			//  Tb_CnfgGdMnu_IsAllowNewRow

		[DataMember]
		public Boolean _au;			//  Tb_CnfgGdMnu_ExcelExport_IsVisible

		[DataMember]
		public Boolean _av;			//  Tb_CnfgGdMnu_ColumnChooser_IsVisible

		[DataMember]
		public Boolean _aw;			//  Tb_CnfgGdMnu_ShowHideColBtn_IsVisible

		[DataMember]
		public Boolean _ax;			//  Tb_CnfgGdMnu_RefreshGrid_IsVisible

		[DataMember]
		public Boolean _ay;			//  Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible

		[DataMember]
		public Boolean _az;			//  Tb_IsInputComplete

		[DataMember]
		public Boolean _ba;			//  Tb_IsCodeGen

		[DataMember]
		public Boolean _bb;			//  Tb_IsReadyCodeGen

		[DataMember]
		public Boolean _bc;			//  Tb_IsCodeGenComplete

		[DataMember]
		public Boolean _bd;			//  Tb_IsTagForCodeGen

		[DataMember]
		public Boolean _be;			//  Tb_IsTagForOther

		[DataMember]
		public String _bf;			//  Tb_TableGroups

		[DataMember]
		public Boolean _bg;			//  Tb_IsActiveRow

		[DataMember]
		public Boolean _bh;			//  Tb_IsDeleted

		[DataMember]
		public Guid? _bi;			//  Tb_CreatedUserId

		[DataMember]
		public DateTime? _bj;			//  Tb_CreatedDate

		[DataMember]
		public Guid? _bk;			//  Tb_UserId

		[DataMember]
		public String _bl;			//  UserName

		[DataMember]
		public DateTime? _bm;			//  Tb_LastModifiedDate

		[DataMember]
		public Byte[] _bn;			//  Tb_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcTable_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 66; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                    _list[18] = _s;
                    _list[19] = _t;
                    _list[20] = _u;
                    _list[21] = _v;
                    _list[22] = _w;
                    _list[23] = _x;
                    _list[24] = _y;
                    _list[25] = _z;
                    _list[26] = _aa;
                    _list[27] = _ab;
                    _list[28] = _ac;
                    _list[29] = _ad;
                    _list[30] = _ae;
                    _list[31] = _af;
                    _list[32] = _ag;
                    _list[33] = _ah;
                    _list[34] = _ai;
                    _list[35] = _aj;
                    _list[36] = _ak;
                    _list[37] = _al;
                    _list[38] = _am;
                    _list[39] = _an;
                    _list[40] = _ao;
                    _list[41] = _ap;
                    _list[42] = _aq;
                    _list[43] = _ar;
                    _list[44] = _asxx;
                    _list[45] = _at;
                    _list[46] = _au;
                    _list[47] = _av;
                    _list[48] = _aw;
                    _list[49] = _ax;
                    _list[50] = _ay;
                    _list[51] = _az;
                    _list[52] = _ba;
                    _list[53] = _bb;
                    _list[54] = _bc;
                    _list[55] = _bd;
                    _list[56] = _be;
                    _list[57] = _bf;
                    _list[58] = _bg;
                    _list[59] = _bh;
                    _list[60] = _bi;
                    _list[61] = _bj;
                    _list[62] = _bk;
                    _list[63] = _bl;
                    _list[64] = _bm;
                    _list[65] = _bn;
                }
                return _list;
            }
        }

        public Guid Tb_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid Tb_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? Tb_ApVrsn_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_ApVrsn_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Guid? Tb_auditTable_Id
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_auditTable_Id_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public Int32? AttachmentCount
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? AttachmentCount_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String AttachmentFileNames
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String AttachmentFileNames_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Int32? DiscussionCount
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? DiscussionCount_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public String DiscussionTitles
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DiscussionTitles_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public String Tb_Name
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_Name_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public String Tb_EntityRootName
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_EntityRootName_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public String Tb_VariableName
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_VariableName_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public String Tb_ScreenCaption
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_ScreenCaption_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public String Tb_Description
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_Description_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public String Tb_DevelopmentNote
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_DevelopmentNote_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public String Tb_UIAssemblyName
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_UIAssemblyName_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public String Tb_UINamespace
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_UINamespace_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public String Tb_UIAssemblyPath
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_UIAssemblyPath_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public String Tb_ProxyAssemblyName
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_ProxyAssemblyName_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public String Tb_ProxyAssemblyPath
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_ProxyAssemblyPath_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        public String Tb_WebServiceName
        {
            get { return _s; }
            set
            {
                _s = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_WebServiceName_noevents
        {
            get { return _s; }
            set
            {
                _s = value;
            }
        }

        public String Tb_WebServiceFolder
        {
            get { return _t; }
            set
            {
                _t = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_WebServiceFolder_noevents
        {
            get { return _t; }
            set
            {
                _t = value;
            }
        }

        public String Tb_DataServiceAssemblyName
        {
            get { return _u; }
            set
            {
                _u = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_DataServiceAssemblyName_noevents
        {
            get { return _u; }
            set
            {
                _u = value;
            }
        }

        public String Tb_DataServicePath
        {
            get { return _v; }
            set
            {
                _v = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_DataServicePath_noevents
        {
            get { return _v; }
            set
            {
                _v = value;
            }
        }

        public String Tb_WireTypeAssemblyName
        {
            get { return _w; }
            set
            {
                _w = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_WireTypeAssemblyName_noevents
        {
            get { return _w; }
            set
            {
                _w = value;
            }
        }

        public String Tb_WireTypePath
        {
            get { return _x; }
            set
            {
                _x = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_WireTypePath_noevents
        {
            get { return _x; }
            set
            {
                _x = value;
            }
        }

        public Boolean Tb_UseTilesInPropsScreen
        {
            get { return _y; }
            set
            {
                _y = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseTilesInPropsScreen_noevents
        {
            get { return _y; }
            set
            {
                _y = value;
            }
        }

        public Boolean Tb_UseGridColumnGroups
        {
            get { return _z; }
            set
            {
                _z = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseGridColumnGroups_noevents
        {
            get { return _z; }
            set
            {
                _z = value;
            }
        }

        public Boolean Tb_UseGridDataSourceCombo
        {
            get { return _aa; }
            set
            {
                _aa = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseGridDataSourceCombo_noevents
        {
            get { return _aa; }
            set
            {
                _aa = value;
            }
        }

        public Guid? Tb_ApCnSK_Id
        {
            get { return _ab; }
            set
            {
                _ab = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_ApCnSK_Id_noevents
        {
            get { return _ab; }
            set
            {
                _ab = value;
            }
        }

        public String Tb_ApCnSK_Id_TextField
        {
            get { return _ac; }
            set
            {
                _ac = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_ApCnSK_Id_TextField_noevents
        {
            get { return _ac; }
            set
            {
                _ac = value;
            }
        }

        public Boolean Tb_UseLegacyConnectionCode
        {
            get { return _ad; }
            set
            {
                _ad = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseLegacyConnectionCode_noevents
        {
            get { return _ad; }
            set
            {
                _ad = value;
            }
        }

        public Boolean Tb_PkIsIdentity
        {
            get { return _ae; }
            set
            {
                _ae = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_PkIsIdentity_noevents
        {
            get { return _ae; }
            set
            {
                _ae = value;
            }
        }

        public Boolean Tb_IsVirtual
        {
            get { return _af; }
            set
            {
                _af = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsVirtual_noevents
        {
            get { return _af; }
            set
            {
                _af = value;
            }
        }

        public Boolean? Tb_IsScreenPlaceHolder
        {
            get { return _ag; }
            set
            {
                _ag = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? Tb_IsScreenPlaceHolder_noevents
        {
            get { return _ag; }
            set
            {
                _ag = value;
            }
        }

        public Boolean Tb_IsNotEntity
        {
            get { return _ah; }
            set
            {
                _ah = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsNotEntity_noevents
        {
            get { return _ah; }
            set
            {
                _ah = value;
            }
        }

        public Boolean Tb_IsMany2Many
        {
            get { return _ai; }
            set
            {
                _ai = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsMany2Many_noevents
        {
            get { return _ai; }
            set
            {
                _ai = value;
            }
        }

        public Boolean Tb_UseLastModifiedByUserNameInSproc
        {
            get { return _aj; }
            set
            {
                _aj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseLastModifiedByUserNameInSproc_noevents
        {
            get { return _aj; }
            set
            {
                _aj = value;
            }
        }

        public Boolean Tb_UseUserTimeStamp
        {
            get { return _ak; }
            set
            {
                _ak = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseUserTimeStamp_noevents
        {
            get { return _ak; }
            set
            {
                _ak = value;
            }
        }

        public Boolean Tb_UseForAudit
        {
            get { return _al; }
            set
            {
                _al = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseForAudit_noevents
        {
            get { return _al; }
            set
            {
                _al = value;
            }
        }

        public Boolean? Tb_IsAllowDelete
        {
            get { return _am; }
            set
            {
                _am = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? Tb_IsAllowDelete_noevents
        {
            get { return _am; }
            set
            {
                _am = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_MenuRow_IsVisible
        {
            get { return _an; }
            set
            {
                _an = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_MenuRow_IsVisible_noevents
        {
            get { return _an; }
            set
            {
                _an = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_GridTools_IsVisible
        {
            get { return _ao; }
            set
            {
                _ao = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_GridTools_IsVisible_noevents
        {
            get { return _ao; }
            set
            {
                _ao = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_SplitScreen_IsVisible
        {
            get { return _ap; }
            set
            {
                _ap = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_SplitScreen_IsVisible_noevents
        {
            get { return _ap; }
            set
            {
                _ap = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
        {
            get { return _aq; }
            set
            {
                _aq = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_SplitScreen_IsSplit_Default_noevents
        {
            get { return _aq; }
            set
            {
                _aq = value;
            }
        }

        public Int32 Tb_CnfgGdMnu_NavColumnWidth
        {
            get { return _ar; }
            set
            {
                _ar = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32 Tb_CnfgGdMnu_NavColumnWidth_noevents
        {
            get { return _ar; }
            set
            {
                _ar = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_IsReadOnly
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_IsReadOnly_noevents
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_IsAllowNewRow
        {
            get { return _at; }
            set
            {
                _at = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_IsAllowNewRow_noevents
        {
            get { return _at; }
            set
            {
                _at = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_ExcelExport_IsVisible
        {
            get { return _au; }
            set
            {
                _au = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_ExcelExport_IsVisible_noevents
        {
            get { return _au; }
            set
            {
                _au = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_ColumnChooser_IsVisible
        {
            get { return _av; }
            set
            {
                _av = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_ColumnChooser_IsVisible_noevents
        {
            get { return _av; }
            set
            {
                _av = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
        {
            get { return _aw; }
            set
            {
                _aw = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_ShowHideColBtn_IsVisible_noevents
        {
            get { return _aw; }
            set
            {
                _aw = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_RefreshGrid_IsVisible
        {
            get { return _ax; }
            set
            {
                _ax = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_RefreshGrid_IsVisible_noevents
        {
            get { return _ax; }
            set
            {
                _ax = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
        {
            get { return _ay; }
            set
            {
                _ay = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible_noevents
        {
            get { return _ay; }
            set
            {
                _ay = value;
            }
        }

        public Boolean Tb_IsInputComplete
        {
            get { return _az; }
            set
            {
                _az = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsInputComplete_noevents
        {
            get { return _az; }
            set
            {
                _az = value;
            }
        }

        public Boolean Tb_IsCodeGen
        {
            get { return _ba; }
            set
            {
                _ba = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsCodeGen_noevents
        {
            get { return _ba; }
            set
            {
                _ba = value;
            }
        }

        public Boolean Tb_IsReadyCodeGen
        {
            get { return _bb; }
            set
            {
                _bb = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsReadyCodeGen_noevents
        {
            get { return _bb; }
            set
            {
                _bb = value;
            }
        }

        public Boolean Tb_IsCodeGenComplete
        {
            get { return _bc; }
            set
            {
                _bc = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsCodeGenComplete_noevents
        {
            get { return _bc; }
            set
            {
                _bc = value;
            }
        }

        public Boolean Tb_IsTagForCodeGen
        {
            get { return _bd; }
            set
            {
                _bd = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsTagForCodeGen_noevents
        {
            get { return _bd; }
            set
            {
                _bd = value;
            }
        }

        public Boolean Tb_IsTagForOther
        {
            get { return _be; }
            set
            {
                _be = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsTagForOther_noevents
        {
            get { return _be; }
            set
            {
                _be = value;
            }
        }

        public String Tb_TableGroups
        {
            get { return _bf; }
            set
            {
                _bf = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_TableGroups_noevents
        {
            get { return _bf; }
            set
            {
                _bf = value;
            }
        }

        public Boolean Tb_IsActiveRow
        {
            get { return _bg; }
            set
            {
                _bg = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsActiveRow_noevents
        {
            get { return _bg; }
            set
            {
                _bg = value;
            }
        }

        public Boolean Tb_IsDeleted
        {
            get { return _bh; }
            set
            {
                _bh = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsDeleted_noevents
        {
            get { return _bh; }
            set
            {
                _bh = value;
            }
        }

        public Guid? Tb_CreatedUserId
        {
            get { return _bi; }
            set
            {
                _bi = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_CreatedUserId_noevents
        {
            get { return _bi; }
            set
            {
                _bi = value;
            }
        }

        public DateTime? Tb_CreatedDate
        {
            get { return _bj; }
            set
            {
                _bj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Tb_CreatedDate_noevents
        {
            get { return _bj; }
            set
            {
                _bj = value;
            }
        }

        public Guid? Tb_UserId
        {
            get { return _bk; }
            set
            {
                _bk = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_UserId_noevents
        {
            get { return _bk; }
            set
            {
                _bk = value;
            }
        }

        public String UserName
        {
            get { return _bl; }
            set
            {
                _bl = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _bl; }
            set
            {
                _bl = value;
            }
        }

        public DateTime? Tb_LastModifiedDate
        {
            get { return _bm; }
            set
            {
                _bm = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Tb_LastModifiedDate_noevents
        {
            get { return _bm; }
            set
            {
                _bm = value;
            }
        }

        public Byte[] Tb_Stamp
        {
            get { return _bn; }
            set
            {
                _bn = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] Tb_Stamp_noevents
        {
            get { return _bn; }
            set
            {
                _bn = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r,
				_s,
				_t,
				_u,
				_v,
				_w,
				_x,
				_y,
				_z,
				_aa,
				_ab,
				_ac,
				_ad,
				_ae,
				_af,
				_ag,
				_ah,
				_ai,
				_aj,
				_ak,
				_al,
				_am,
				_an,
				_ao,
				_ap,
				_aq,
				_ar,
				_asxx,
				_at,
				_au,
				_av,
				_aw,
				_ax,
				_ay,
				_az,
				_ba,
				_bb,
				_bc,
				_bd,
				_be,
				_bf,
				_bg,
				_bh,
				_bi,
				_bj,
				_bk,
				_bl,
				_bm,
				_bn
			};
        }

        public WcTable_Values Clone()
        {
            return new WcTable_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


