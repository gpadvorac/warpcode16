using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/18/2018 6:18:09 PM

namespace EntityWireTypeSL
{


    [DataContract]
    public class WcCodeGen_ChildTables_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcCodeGen_ChildTables_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcCodeGen_ChildTables_Binding() { }


        public WcCodeGen_ChildTables_Binding(Guid _TbChd_Id, Guid _TbChd_Parent_Tb_Id, Int32 _TbChd_SortOrder, Guid _TbChd_Child_Tb_Id, String _TbChd_Child_Tb_Id_TextField, Guid _TbChd_ChildForeignKeyColumn_Id, String _TbChd_ChildForeignKeyColumn_Id_TextField, Boolean _TbChd_IsDisplayAsChildGrid, Boolean _TbChd_IsDisplayAsChildTab )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ChildTables_Binding", IfxTraceCategory.Enter);
				_a = _TbChd_Id;
				_b = _TbChd_Parent_Tb_Id;
				_c = _TbChd_SortOrder;
				_d = _TbChd_Child_Tb_Id;
				_e = _TbChd_Child_Tb_Id_TextField;
				_f = _TbChd_ChildForeignKeyColumn_Id;
				_g = _TbChd_ChildForeignKeyColumn_Id_TextField;
				_h = _TbChd_IsDisplayAsChildGrid;
				_i = _TbChd_IsDisplayAsChildTab;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ChildTables_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ChildTables_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcCodeGen_ChildTables_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ChildTables_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  TbChd_Id
				_b = (Guid)data[1];                //  TbChd_Parent_Tb_Id
				_c = (Int32)data[2];                //  TbChd_SortOrder
				_d = (Guid)data[3];                //  TbChd_Child_Tb_Id
				_e = (String)data[4];                //  TbChd_Child_Tb_Id_TextField
				_f = (Guid)data[5];                //  TbChd_ChildForeignKeyColumn_Id
				_g = (String)data[6];                //  TbChd_ChildForeignKeyColumn_Id_TextField
				_h = (Boolean)data[7];                //  TbChd_IsDisplayAsChildGrid
				_i = (Boolean)data[8];                //  TbChd_IsDisplayAsChildTab
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ChildTables_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_ChildTables_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region TbChd_Id

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid TbChd_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion TbChd_Id


        #region TbChd_Parent_Tb_Id

        private Guid _b;
//        public Guid B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Guid TbChd_Parent_Tb_Id
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion TbChd_Parent_Tb_Id


        #region TbChd_SortOrder

        private Int32 _c;
//        public Int32 C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public Int32 TbChd_SortOrder
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion TbChd_SortOrder


        #region TbChd_Child_Tb_Id

        private Guid _d;
//        public Guid D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public Guid TbChd_Child_Tb_Id
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion TbChd_Child_Tb_Id


        #region TbChd_Child_Tb_Id_TextField

        private String _e;
//        public String E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public String TbChd_Child_Tb_Id_TextField
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion TbChd_Child_Tb_Id_TextField


        #region TbChd_ChildForeignKeyColumn_Id

        private Guid _f;
//        public Guid F
//        {
//            get { return _f; }
//            set { _f = value; }
//        }

        //[NonSerialized]
        public Guid TbChd_ChildForeignKeyColumn_Id
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion TbChd_ChildForeignKeyColumn_Id


        #region TbChd_ChildForeignKeyColumn_Id_TextField

        private String _g;
//        public String G
//        {
//            get { return _g; }
//            set { _g = value; }
//        }

        //[NonSerialized]
        public String TbChd_ChildForeignKeyColumn_Id_TextField
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion TbChd_ChildForeignKeyColumn_Id_TextField


        #region TbChd_IsDisplayAsChildGrid

        private Boolean _h;
//        public Boolean H
//        {
//            get { return _h; }
//            set { _h = value; }
//        }

        //[NonSerialized]
        public Boolean TbChd_IsDisplayAsChildGrid
        {
            get { return _h; }
            set { _h = value; }
        }

        #endregion TbChd_IsDisplayAsChildGrid


        #region TbChd_IsDisplayAsChildTab

        private Boolean _i;
//        public Boolean I
//        {
//            get { return _i; }
//            set { _i = value; }
//        }

        //[NonSerialized]
        public Boolean TbChd_IsDisplayAsChildTab
        {
            get { return _i; }
            set { _i = value; }
        }

        #endregion TbChd_IsDisplayAsChildTab


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8} ", TbChd_Id, TbChd_Parent_Tb_Id, TbChd_SortOrder, TbChd_Child_Tb_Id, TbChd_Child_Tb_Id_TextField, TbChd_ChildForeignKeyColumn_Id, TbChd_ChildForeignKeyColumn_Id_TextField, TbChd_IsDisplayAsChildGrid, TbChd_IsDisplayAsChildTab );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8} ", TbChd_Id, TbChd_Parent_Tb_Id, TbChd_SortOrder, TbChd_Child_Tb_Id, TbChd_Child_Tb_Id_TextField, TbChd_ChildForeignKeyColumn_Id, TbChd_ChildForeignKeyColumn_Id_TextField, TbChd_IsDisplayAsChildGrid, TbChd_IsDisplayAsChildTab );
        }

    }

}
