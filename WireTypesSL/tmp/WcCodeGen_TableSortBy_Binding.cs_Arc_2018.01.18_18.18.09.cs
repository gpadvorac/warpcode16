using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/18/2018 5:47:10 PM

namespace EntityWireTypeSL
{


    [DataContract]
    public class WcCodeGen_TableSortBy_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcCodeGen_TableSortBy_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcCodeGen_TableSortBy_Binding() { }


        public WcCodeGen_TableSortBy_Binding(Guid _TbSb_Id, Guid _TbSb_Tb_Id, Guid _TbSb_TbC_Id, String _TbSb_TbC_Id_TextField, Int32 _TbSb_SortOrder, String _TbSb_Direction, String _TbSb_Direction_TextField, Boolean _TbSb_IsActiveRow, Boolean _TbSb_IsDeleted, Guid _TbSb_CreatedUserId, DateTime _TbSb_CreatedDate, Guid _TbSb_UserId, String _UserName, DateTime _TbSb_LastModifiedDate, Byte[] _TbSb_Stamp )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableSortBy_Binding", IfxTraceCategory.Enter);
				_a = _TbSb_Id;
				_b = _TbSb_Tb_Id;
				_c = _TbSb_TbC_Id;
				_d = _TbSb_TbC_Id_TextField;
				_e = _TbSb_SortOrder;
				_f = _TbSb_Direction;
				_g = _TbSb_Direction_TextField;
				_h = _TbSb_IsActiveRow;
				_i = _TbSb_IsDeleted;
				_j = _TbSb_CreatedUserId;
				_k = _TbSb_CreatedDate;
				_l = _TbSb_UserId;
				_m = _UserName;
				_n = _TbSb_LastModifiedDate;
				_o = _TbSb_Stamp;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableSortBy_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableSortBy_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcCodeGen_TableSortBy_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableSortBy_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  TbSb_Id
				_b = (Guid)data[1];                //  TbSb_Tb_Id
				_c = (Guid)data[2];                //  TbSb_TbC_Id
				_d = (String)data[3];                //  TbSb_TbC_Id_TextField
				_e = (Int32)data[4];                //  TbSb_SortOrder
				_f = (String)data[5];                //  TbSb_Direction
				_g = (String)data[6];                //  TbSb_Direction_TextField
				_h = (Boolean)data[7];                //  TbSb_IsActiveRow
				_i = (Boolean)data[8];                //  TbSb_IsDeleted
				_j = (Guid)data[9];                //  TbSb_CreatedUserId
				_k = (DateTime)data[10];                //  TbSb_CreatedDate
				_l = (Guid)data[11];                //  TbSb_UserId
				_m = (String)data[12];                //  UserName
				_n = (DateTime)data[13];                //  TbSb_LastModifiedDate
				_o = (Byte[])data[14];                //  TbSb_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableSortBy_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableSortBy_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region TbSb_Id

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid TbSb_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion TbSb_Id


        #region TbSb_Tb_Id

        private Guid _b;
//        public Guid B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Guid TbSb_Tb_Id
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion TbSb_Tb_Id


        #region TbSb_TbC_Id

        private Guid _c;
//        public Guid C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public Guid TbSb_TbC_Id
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion TbSb_TbC_Id


        #region TbSb_TbC_Id_TextField

        private String _d;
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String TbSb_TbC_Id_TextField
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion TbSb_TbC_Id_TextField


        #region TbSb_SortOrder

        private Int32 _e;
//        public Int32 E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public Int32 TbSb_SortOrder
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion TbSb_SortOrder


        #region TbSb_Direction

        private String _f;
//        public String F
//        {
//            get { return _f; }
//            set { _f = value; }
//        }

        //[NonSerialized]
        public String TbSb_Direction
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion TbSb_Direction


        #region TbSb_Direction_TextField

        private String _g;
//        public String G
//        {
//            get { return _g; }
//            set { _g = value; }
//        }

        //[NonSerialized]
        public String TbSb_Direction_TextField
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion TbSb_Direction_TextField


        #region TbSb_IsActiveRow

        private Boolean _h;
//        public Boolean H
//        {
//            get { return _h; }
//            set { _h = value; }
//        }

        //[NonSerialized]
        public Boolean TbSb_IsActiveRow
        {
            get { return _h; }
            set { _h = value; }
        }

        #endregion TbSb_IsActiveRow


        #region TbSb_IsDeleted

        private Boolean _i;
//        public Boolean I
//        {
//            get { return _i; }
//            set { _i = value; }
//        }

        //[NonSerialized]
        public Boolean TbSb_IsDeleted
        {
            get { return _i; }
            set { _i = value; }
        }

        #endregion TbSb_IsDeleted


        #region TbSb_CreatedUserId

        private Guid _j;
//        public Guid J
//        {
//            get { return _j; }
//            set { _j = value; }
//        }

        //[NonSerialized]
        public Guid TbSb_CreatedUserId
        {
            get { return _j; }
            set { _j = value; }
        }

        #endregion TbSb_CreatedUserId


        #region TbSb_CreatedDate

        private DateTime _k;
//        public DateTime K
//        {
//            get { return _k; }
//            set { _k = value; }
//        }

        //[NonSerialized]
        public DateTime TbSb_CreatedDate
        {
            get { return _k; }
            set { _k = value; }
        }

        #endregion TbSb_CreatedDate


        #region TbSb_UserId

        private Guid _l;
//        public Guid L
//        {
//            get { return _l; }
//            set { _l = value; }
//        }

        //[NonSerialized]
        public Guid TbSb_UserId
        {
            get { return _l; }
            set { _l = value; }
        }

        #endregion TbSb_UserId


        #region UserName

        private String _m;
//        public String M
//        {
//            get { return _m; }
//            set { _m = value; }
//        }

        //[NonSerialized]
        public String UserName
        {
            get { return _m; }
            set { _m = value; }
        }

        #endregion UserName


        #region TbSb_LastModifiedDate

        private DateTime _n;
//        public DateTime N
//        {
//            get { return _n; }
//            set { _n = value; }
//        }

        //[NonSerialized]
        public DateTime TbSb_LastModifiedDate
        {
            get { return _n; }
            set { _n = value; }
        }

        #endregion TbSb_LastModifiedDate


        #region TbSb_Stamp

        private Byte[] _o;
//        public Byte[] O
//        {
//            get { return _o; }
//            set { _o = value; }
//        }

        //[NonSerialized]
        public Byte[] TbSb_Stamp
        {
            get { return _o; }
            set { _o = value; }
        }

        #endregion TbSb_Stamp


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11}; {12}; {13}; {14} ", TbSb_Id, TbSb_Tb_Id, TbSb_TbC_Id, TbSb_TbC_Id_TextField, TbSb_SortOrder, TbSb_Direction, TbSb_Direction_TextField, TbSb_IsActiveRow, TbSb_IsDeleted, TbSb_CreatedUserId, TbSb_CreatedDate, TbSb_UserId, UserName, TbSb_LastModifiedDate, TbSb_Stamp );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11}; {12}; {13}; {14} ", TbSb_Id, TbSb_Tb_Id, TbSb_TbC_Id, TbSb_TbC_Id_TextField, TbSb_SortOrder, TbSb_Direction, TbSb_Direction_TextField, TbSb_IsActiveRow, TbSb_IsDeleted, TbSb_CreatedUserId, TbSb_CreatedDate, TbSb_UserId, UserName, TbSb_LastModifiedDate, TbSb_Stamp );
        }

    }

}
