using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/7/2018 10:06:48 AM

namespace EntityWireTypeSL
{


    [DataContract]
    public class WcStoredProcGroup_Assignments_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProcGroup_Assignments_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcStoredProcGroup_Assignments_Binding() { }


        public WcStoredProcGroup_Assignments_Binding(Guid _SpGrp_Id, Boolean _IsSelected, String _SpGrp_Name )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Assignments_Binding", IfxTraceCategory.Enter);
				_a = _SpGrp_Id;
				_b = _IsSelected;
				_c = _SpGrp_Name;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Assignments_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Assignments_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcStoredProcGroup_Assignments_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Assignments_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  SpGrp_Id
				_b = (Boolean)data[1];                //  IsSelected
				_c = (String)data[2];                //  SpGrp_Name
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Assignments_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Assignments_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region SpGrp_Id

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid SpGrp_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion SpGrp_Id


        #region IsSelected

        private Boolean _b;
//        public Boolean B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Boolean IsSelected
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion IsSelected


        #region SpGrp_Name

        private String _c;
//        public String C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public String SpGrp_Name
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion SpGrp_Name


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2} ", SpGrp_Id, IsSelected, SpGrp_Name );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2} ", SpGrp_Id, IsSelected, SpGrp_Name );
        }

    }

}
