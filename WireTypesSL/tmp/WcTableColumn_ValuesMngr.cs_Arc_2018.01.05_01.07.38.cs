using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  12/16/2017 11:40:55 PM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcTableColumn_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTableColumn_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcTableColumn_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcTableColumn_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcTableColumn_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumn_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcTableColumn_Values(currentData, this) : new WcTableColumn_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcTableColumn_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumn_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumn_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcTableColumn_Values _original;
        [DataMember]
        public WcTableColumn_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcTableColumn_Values _current;
        [DataMember]
        public WcTableColumn_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcTableColumn_Values _concurrent;
        [DataMember]
        public WcTableColumn_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  TbC_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  TbC_Tb_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  TbC_auditColumn_Id
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  TbC_SortOrder
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  TbC_Name
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  TbC_CtlTp_Id
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  TbC_CtlTp_Id_TextField
                //if (_current._g != _original._g)
                //{
                //    return true;
                //}

                //  TbC_IsNonvColumn
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  TbC_Description
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  TbC_DtSql_Id
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  TbC_DtSql_Id_TextField
                //if (_current._k != _original._k)
                //{
                //    return true;
                //}

                //  TbC_DtDtNt_Id
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  TbC_DtDtNt_Id_TextField
                //if (_current._m != _original._m)
                //{
                //    return true;
                //}

                //  TbC_Length
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  TbC_Precision
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  TbC_Scale
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  TbC_IsPK
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  TbC_IsIdentity
                if (_current._r != _original._r)
                {
                    return true;
                }

                //  TbC_IsFK
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  TbC_IsEntityColumn
                if (_current._t != _original._t)
                {
                    return true;
                }

                //  TbC_IsSystemField
                if (_current._u != _original._u)
                {
                    return true;
                }

                //  TbC_IsValuesObjectMember
                if (_current._v != _original._v)
                {
                    return true;
                }

                //  TbC_IsInPropsScreen
                if (_current._w != _original._w)
                {
                    return true;
                }

                //  TbC_IsInNavList
                if (_current._x != _original._x)
                {
                    return true;
                }

                //  TbC_IsRequired
                if (_current._y != _original._y)
                {
                    return true;
                }

                //  TbC_BrokenRuleText
                if (_current._z != _original._z)
                {
                    return true;
                }

                //  TbC_AllowZero
                if (_current._aa != _original._aa)
                {
                    return true;
                }

                //  TbC_IsNullableInDb
                if (_current._ab != _original._ab)
                {
                    return true;
                }

                //  TbC_IsNullableInUI
                if (_current._ac != _original._ac)
                {
                    return true;
                }

                //  TbC_DefaultValue
                if (_current._ad != _original._ad)
                {
                    return true;
                }

                //  TbC_IsReadFromDb
                if (_current._ae != _original._ae)
                {
                    return true;
                }

                //  TbC_IsSendToDb
                if (_current._af != _original._af)
                {
                    return true;
                }

                //  TbC_IsInsertAllowed
                if (_current._ag != _original._ag)
                {
                    return true;
                }

                //  TbC_IsEditAllowed
                if (_current._ah != _original._ah)
                {
                    return true;
                }

                //  TbC_IsReadOnlyInUI
                if (_current._ai != _original._ai)
                {
                    return true;
                }

                //  TbC_UseForAudit
                if (_current._aj != _original._aj)
                {
                    return true;
                }

                //  TbC_DefaultCaption
                if (_current._ak != _original._ak)
                {
                    return true;
                }

                //  TbC_ColumnHeaderText
                if (_current._al != _original._al)
                {
                    return true;
                }

                //  TbC_LabelCaptionVerbose
                if (_current._am != _original._am)
                {
                    return true;
                }

                //  TbC_LabelCaptionGenerate
                if (_current._an != _original._an)
                {
                    return true;
                }

                //  Tbc_ShowGridColumnToolTip
                if (_current._ao != _original._ao)
                {
                    return true;
                }

                //  Tbc_ShowPropsToolTip
                if (_current._ap != _original._ap)
                {
                    return true;
                }

                //  Tbc_TooltipsRolledUp
                if (_current._aq != _original._aq)
                {
                    return true;
                }

                //  TbC_IsCreatePropsStrings
                if (_current._ar != _original._ar)
                {
                    return true;
                }

                //  TbC_IsCreateGridStrings
                if (_current._asxx != _original._asxx)
                {
                    return true;
                }

                //  TbC_IsAvailableForColumnGroups
                if (_current._at != _original._at)
                {
                    return true;
                }

                //  TbC_IsTextWrapInProp
                if (_current._au != _original._au)
                {
                    return true;
                }

                //  TbC_IsTextWrapInGrid
                if (_current._av != _original._av)
                {
                    return true;
                }

                //  TbC_TextBoxFormat
                if (_current._aw != _original._aw)
                {
                    return true;
                }

                //  TbC_TextColumnFormat
                if (_current._ax != _original._ax)
                {
                    return true;
                }

                //  TbC_ColumnWidth
                if (_current._ay != _original._ay)
                {
                    return true;
                }

                //  TbC_TextBoxTextAlignment
                if (_current._az != _original._az)
                {
                    return true;
                }

                //  TbC_ColumnTextAlignment
                if (_current._ba != _original._ba)
                {
                    return true;
                }

                //  TbC_ListStoredProc_Id
                if (_current._bb != _original._bb)
                {
                    return true;
                }

                //  TbC_ListStoredProc_Id_TextField
                //if (_current._bc != _original._bc)
                //{
                //    return true;
                //}

                //  TbC_IsStaticList
                if (_current._bd != _original._bd)
                {
                    return true;
                }

                //  TbC_UseNotInList
                if (_current._be != _original._be)
                {
                    return true;
                }

                //  TbC_UseListEditBtn
                if (_current._bf != _original._bf)
                {
                    return true;
                }

                //  TbC_ParentColumnKey
                if (_current._bg != _original._bg)
                {
                    return true;
                }

                //  TbC_ParentColumnKey_TextField
                //if (_current._bh != _original._bh)
                //{
                //    return true;
                //}

                //  TbC_UseDisplayTextFieldProperty
                if (_current._bi != _original._bi)
                {
                    return true;
                }

                //  TbC_IsDisplayTextFieldProperty
                if (_current._bj != _original._bj)
                {
                    return true;
                }

                //  TbC_ComboListTable_Id
                if (_current._bk != _original._bk)
                {
                    return true;
                }

                //  TbC_ComboListTable_Id_TextField
                //if (_current._bl != _original._bl)
                //{
                //    return true;
                //}

                //  TbC_ComboListDisplayColumn_Id
                if (_current._bm != _original._bm)
                {
                    return true;
                }

                //  TbC_ComboListDisplayColumn_Id_TextField
                //if (_current._bn != _original._bn)
                //{
                //    return true;
                //}

                //  TbC_Combo_MaxDropdownHeight
                if (_current._bo != _original._bo)
                {
                    return true;
                }

                //  TbC_Combo_MaxDropdownWidth
                if (_current._bp != _original._bp)
                {
                    return true;
                }

                //  TbC_Combo_AllowDropdownResizing
                if (_current._bq != _original._bq)
                {
                    return true;
                }

                //  TbC_Combo_IsResetButtonVisible
                if (_current._br != _original._br)
                {
                    return true;
                }

                //  TbC_IsActiveRecColumn
                if (_current._bs != _original._bs)
                {
                    return true;
                }

                //  TbC_IsDeletedColumn
                if (_current._bt != _original._bt)
                {
                    return true;
                }

                //  TbC_IsCreatedUserIdColumn
                if (_current._bu != _original._bu)
                {
                    return true;
                }

                //  TbC_IsCreatedDateColumn
                if (_current._bv != _original._bv)
                {
                    return true;
                }

                //  TbC_IsUserIdColumn
                if (_current._bw != _original._bw)
                {
                    return true;
                }

                //  TbC_IsModifiedDateColumn
                if (_current._bx != _original._bx)
                {
                    return true;
                }

                //  TbC_IsRowVersionStampColumn
                if (_current._by != _original._by)
                {
                    return true;
                }

                //  TbC_IsBrowsable
                if (_current._bz != _original._bz)
                {
                    return true;
                }

                //  TbC_DeveloperNote
                if (_current._ca != _original._ca)
                {
                    return true;
                }

                //  TbC_UserNote
                if (_current._cb != _original._cb)
                {
                    return true;
                }

                //  TbC_HelpFileAdditionalNote
                if (_current._cc != _original._cc)
                {
                    return true;
                }

                //  TbC_Notes
                if (_current._cd != _original._cd)
                {
                    return true;
                }

                //  TbC_IsInputComplete
                if (_current._ce != _original._ce)
                {
                    return true;
                }

                //  TbC_IsCodeGen
                if (_current._cf != _original._cf)
                {
                    return true;
                }

                //  TbC_IsReadyCodeGen
                if (_current._cg != _original._cg)
                {
                    return true;
                }

                //  TbC_IsCodeGenComplete
                if (_current._ch != _original._ch)
                {
                    return true;
                }

                //  TbC_IsTagForCodeGen
                if (_current._ci != _original._ci)
                {
                    return true;
                }

                //  TbC_IsTagForOther
                if (_current._cj != _original._cj)
                {
                    return true;
                }

                //  TbC_ColumnGroups
                if (_current._ck != _original._ck)
                {
                    return true;
                }

                //  TbC_IsActiveRow
                if (_current._cl != _original._cl)
                {
                    return true;
                }

                //  TbC_IsDeleted
                if (_current._cm != _original._cm)
                {
                    return true;
                }

                //  TbC_CreatedUserId
                if (_current._cnxx != _original._cnxx)
                {
                    return true;
                }

                //  TbC_CreatedDate
                if (_current._co != _original._co)
                {
                    return true;
                }

                //  TbC_UserId
                if (_current._cp != _original._cp)
                {
                    return true;
                }

                //  UserName
                if (_current._cq != _original._cq)
                {
                    return true;
                }

                //  TbC_LastModifiedDate
                if (_current._cr != _original._cr)
                {
                    return true;
                }

                //  TbC_Stamp
                if (_current._cs != _original._cs)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{98}{0}{97}_b{98}{1}{97}_c{98}{2}{97}_d{98}{3}{97}_e{98}{4}{97}_f{98}{5}{97}_g{98}{6}{97}_h{98}{7}{97}_i{98}{8}{97}_j{98}{9}{97}_k{98}{10}{97}_l{98}{11}{97}_m{98}{12}{97}_n{98}{13}{97}_o{98}{14}{97}_p{98}{15}{97}_q{98}{16}{97}_r{98}{17}{97}_s{98}{18}{97}_t{98}{19}{97}_u{98}{20}{97}_v{98}{21}{97}_w{98}{22}{97}_x{98}{23}{97}_y{98}{24}{97}_z{98}{25}{97}_aa{98}{26}{97}_ab{98}{27}{97}_ac{98}{28}{97}_ad{98}{29}{97}_ae{98}{30}{97}_af{98}{31}{97}_ag{98}{32}{97}_ah{98}{33}{97}_ai{98}{34}{97}_aj{98}{35}{97}_ak{98}{36}{97}_al{98}{37}{97}_am{98}{38}{97}_an{98}{39}{97}_ao{98}{40}{97}_ap{98}{41}{97}_aq{98}{42}{97}_ar{98}{43}{97}_asxx{98}{44}{97}_at{98}{45}{97}_au{98}{46}{97}_av{98}{47}{97}_aw{98}{48}{97}_ax{98}{49}{97}_ay{98}{50}{97}_az{98}{51}{97}_ba{98}{52}{97}_bb{98}{53}{97}_bc{98}{54}{97}_bd{98}{55}{97}_be{98}{56}{97}_bf{98}{57}{97}_bg{98}{58}{97}_bh{98}{59}{97}_bi{98}{60}{97}_bj{98}{61}{97}_bk{98}{62}{97}_bl{98}{63}{97}_bm{98}{64}{97}_bn{98}{65}{97}_bo{98}{66}{97}_bp{98}{67}{97}_bq{98}{68}{97}_br{98}{69}{97}_bs{98}{70}{97}_bt{98}{71}{97}_bu{98}{72}{97}_bv{98}{73}{97}_bw{98}{74}{97}_bx{98}{75}{97}_by{98}{76}{97}_bz{98}{77}{97}_ca{98}{78}{97}_cb{98}{79}{97}_cc{98}{80}{97}_cd{98}{81}{97}_ce{98}{82}{97}_cf{98}{83}{97}_cg{98}{84}{97}_ch{98}{85}{97}_ci{98}{86}{97}_cj{98}{87}{97}_ck{98}{88}{97}_cl{98}{89}{97}_cm{98}{90}{97}_cnxx{98}{91}{97}_co{98}{92}{97}_cp{98}{93}{97}_cq{98}{94}{97}_cr{98}{95}{97}_cs{98}{96}",
				new object[] {
				_current._a,		  //TbC_Id
				_current._b,		  //TbC_Tb_Id
				_current._c,		  //TbC_auditColumn_Id
				_current._d,		  //TbC_SortOrder
				_current._e,		  //TbC_Name
				_current._f,		  //TbC_CtlTp_Id
				_current._g,		  //TbC_CtlTp_Id_TextField
				_current._h,		  //TbC_IsNonvColumn
				_current._i,		  //TbC_Description
				_current._j,		  //TbC_DtSql_Id
				_current._k,		  //TbC_DtSql_Id_TextField
				_current._l,		  //TbC_DtDtNt_Id
				_current._m,		  //TbC_DtDtNt_Id_TextField
				_current._n,		  //TbC_Length
				_current._o,		  //TbC_Precision
				_current._p,		  //TbC_Scale
				_current._q,		  //TbC_IsPK
				_current._r,		  //TbC_IsIdentity
				_current._s,		  //TbC_IsFK
				_current._t,		  //TbC_IsEntityColumn
				_current._u,		  //TbC_IsSystemField
				_current._v,		  //TbC_IsValuesObjectMember
				_current._w,		  //TbC_IsInPropsScreen
				_current._x,		  //TbC_IsInNavList
				_current._y,		  //TbC_IsRequired
				_current._z,		  //TbC_BrokenRuleText
				_current._aa,		  //TbC_AllowZero
				_current._ab,		  //TbC_IsNullableInDb
				_current._ac,		  //TbC_IsNullableInUI
				_current._ad,		  //TbC_DefaultValue
				_current._ae,		  //TbC_IsReadFromDb
				_current._af,		  //TbC_IsSendToDb
				_current._ag,		  //TbC_IsInsertAllowed
				_current._ah,		  //TbC_IsEditAllowed
				_current._ai,		  //TbC_IsReadOnlyInUI
				_current._aj,		  //TbC_UseForAudit
				_current._ak,		  //TbC_DefaultCaption
				_current._al,		  //TbC_ColumnHeaderText
				_current._am,		  //TbC_LabelCaptionVerbose
				_current._an,		  //TbC_LabelCaptionGenerate
				_current._ao,		  //Tbc_ShowGridColumnToolTip
				_current._ap,		  //Tbc_ShowPropsToolTip
				_current._aq,		  //Tbc_TooltipsRolledUp
				_current._ar,		  //TbC_IsCreatePropsStrings
				_current._asxx,		  //TbC_IsCreateGridStrings
				_current._at,		  //TbC_IsAvailableForColumnGroups
				_current._au,		  //TbC_IsTextWrapInProp
				_current._av,		  //TbC_IsTextWrapInGrid
				_current._aw,		  //TbC_TextBoxFormat
				_current._ax,		  //TbC_TextColumnFormat
				_current._ay,		  //TbC_ColumnWidth
				_current._az,		  //TbC_TextBoxTextAlignment
				_current._ba,		  //TbC_ColumnTextAlignment
				_current._bb,		  //TbC_ListStoredProc_Id
				_current._bc,		  //TbC_ListStoredProc_Id_TextField
				_current._bd,		  //TbC_IsStaticList
				_current._be,		  //TbC_UseNotInList
				_current._bf,		  //TbC_UseListEditBtn
				_current._bg,		  //TbC_ParentColumnKey
				_current._bh,		  //TbC_ParentColumnKey_TextField
				_current._bi,		  //TbC_UseDisplayTextFieldProperty
				_current._bj,		  //TbC_IsDisplayTextFieldProperty
				_current._bk,		  //TbC_ComboListTable_Id
				_current._bl,		  //TbC_ComboListTable_Id_TextField
				_current._bm,		  //TbC_ComboListDisplayColumn_Id
				_current._bn,		  //TbC_ComboListDisplayColumn_Id_TextField
				_current._bo,		  //TbC_Combo_MaxDropdownHeight
				_current._bp,		  //TbC_Combo_MaxDropdownWidth
				_current._bq,		  //TbC_Combo_AllowDropdownResizing
				_current._br,		  //TbC_Combo_IsResetButtonVisible
				_current._bs,		  //TbC_IsActiveRecColumn
				_current._bt,		  //TbC_IsDeletedColumn
				_current._bu,		  //TbC_IsCreatedUserIdColumn
				_current._bv,		  //TbC_IsCreatedDateColumn
				_current._bw,		  //TbC_IsUserIdColumn
				_current._bx,		  //TbC_IsModifiedDateColumn
				_current._by,		  //TbC_IsRowVersionStampColumn
				_current._bz,		  //TbC_IsBrowsable
				_current._ca,		  //TbC_DeveloperNote
				_current._cb,		  //TbC_UserNote
				_current._cc,		  //TbC_HelpFileAdditionalNote
				_current._cd,		  //TbC_Notes
				_current._ce,		  //TbC_IsInputComplete
				_current._cf,		  //TbC_IsCodeGen
				_current._cg,		  //TbC_IsReadyCodeGen
				_current._ch,		  //TbC_IsCodeGenComplete
				_current._ci,		  //TbC_IsTagForCodeGen
				_current._cj,		  //TbC_IsTagForOther
				_current._ck,		  //TbC_ColumnGroups
				_current._cl,		  //TbC_IsActiveRow
				_current._cm,		  //TbC_IsDeleted
				_current._cnxx,		  //TbC_CreatedUserId
				_current._co,		  //TbC_CreatedDate
				_current._cp,		  //TbC_UserId
				_current._cq,		  //UserName
				_current._cr,		  //TbC_LastModifiedDate
				_current._cs,		  //TbC_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcTableColumn_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTableColumn_Values";
        private WcTableColumn_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcTableColumn_Values() 
        {
        }

        //public WcTableColumn_Values(object[] data, WcTableColumn_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcTableColumn_Values(object[] data, WcTableColumn_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumn_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  TbC_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  TbC_Tb_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  TbC_auditColumn_Id
				_d = ObjectHelper.GetNullableShortFromObjectValue(data[3]);							//  TbC_SortOrder
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  TbC_Name
				_f = ObjectHelper.GetNullableIntFromObjectValue(data[5]);							//  TbC_CtlTp_Id
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  TbC_CtlTp_Id_TextField
				_h = ObjectHelper.GetBoolFromObjectValue(data[7]);									//  TbC_IsNonvColumn
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  TbC_Description
				_j = ObjectHelper.GetNullableIntFromObjectValue(data[9]);							//  TbC_DtSql_Id
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  TbC_DtSql_Id_TextField
				_l = ObjectHelper.GetNullableIntFromObjectValue(data[11]);							//  TbC_DtDtNt_Id
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  TbC_DtDtNt_Id_TextField
				_n = ObjectHelper.GetNullableIntFromObjectValue(data[13]);							//  TbC_Length
				_o = ObjectHelper.GetNullableIntFromObjectValue(data[14]);							//  TbC_Precision
				_p = ObjectHelper.GetNullableIntFromObjectValue(data[15]);							//  TbC_Scale
				_q = ObjectHelper.GetBoolFromObjectValue(data[16]);									//  TbC_IsPK
				_r = ObjectHelper.GetBoolFromObjectValue(data[17]);									//  TbC_IsIdentity
				_s = ObjectHelper.GetBoolFromObjectValue(data[18]);									//  TbC_IsFK
				_t = ObjectHelper.GetBoolFromObjectValue(data[19]);									//  TbC_IsEntityColumn
				_u = ObjectHelper.GetBoolFromObjectValue(data[20]);									//  TbC_IsSystemField
				_v = ObjectHelper.GetBoolFromObjectValue(data[21]);									//  TbC_IsValuesObjectMember
				_w = ObjectHelper.GetBoolFromObjectValue(data[22]);									//  TbC_IsInPropsScreen
				_x = ObjectHelper.GetBoolFromObjectValue(data[23]);									//  TbC_IsInNavList
				_y = ObjectHelper.GetBoolFromObjectValue(data[24]);									//  TbC_IsRequired
				_z = ObjectHelper.GetStringFromObjectValue(data[25]);									//  TbC_BrokenRuleText
				_aa = ObjectHelper.GetBoolFromObjectValue(data[26]);									//  TbC_AllowZero
				_ab = ObjectHelper.GetBoolFromObjectValue(data[27]);									//  TbC_IsNullableInDb
				_ac = ObjectHelper.GetBoolFromObjectValue(data[28]);									//  TbC_IsNullableInUI
				_ad = ObjectHelper.GetStringFromObjectValue(data[29]);									//  TbC_DefaultValue
				_ae = ObjectHelper.GetBoolFromObjectValue(data[30]);									//  TbC_IsReadFromDb
				_af = ObjectHelper.GetBoolFromObjectValue(data[31]);									//  TbC_IsSendToDb
				_ag = ObjectHelper.GetBoolFromObjectValue(data[32]);									//  TbC_IsInsertAllowed
				_ah = ObjectHelper.GetBoolFromObjectValue(data[33]);									//  TbC_IsEditAllowed
				_ai = ObjectHelper.GetBoolFromObjectValue(data[34]);									//  TbC_IsReadOnlyInUI
				_aj = ObjectHelper.GetBoolFromObjectValue(data[35]);									//  TbC_UseForAudit
				_ak = ObjectHelper.GetStringFromObjectValue(data[36]);									//  TbC_DefaultCaption
				_al = ObjectHelper.GetStringFromObjectValue(data[37]);									//  TbC_ColumnHeaderText
				_am = ObjectHelper.GetStringFromObjectValue(data[38]);									//  TbC_LabelCaptionVerbose
				_an = ObjectHelper.GetBoolFromObjectValue(data[39]);									//  TbC_LabelCaptionGenerate
				_ao = ObjectHelper.GetBoolFromObjectValue(data[40]);									//  Tbc_ShowGridColumnToolTip
				_ap = ObjectHelper.GetBoolFromObjectValue(data[41]);									//  Tbc_ShowPropsToolTip
				_aq = ObjectHelper.GetStringFromObjectValue(data[42]);									//  Tbc_TooltipsRolledUp
				_ar = ObjectHelper.GetBoolFromObjectValue(data[43]);									//  TbC_IsCreatePropsStrings
				_asxx = ObjectHelper.GetBoolFromObjectValue(data[44]);									//  TbC_IsCreateGridStrings
				_at = ObjectHelper.GetBoolFromObjectValue(data[45]);									//  TbC_IsAvailableForColumnGroups
				_au = ObjectHelper.GetBoolFromObjectValue(data[46]);									//  TbC_IsTextWrapInProp
				_av = ObjectHelper.GetBoolFromObjectValue(data[47]);									//  TbC_IsTextWrapInGrid
				_aw = ObjectHelper.GetStringFromObjectValue(data[48]);									//  TbC_TextBoxFormat
				_ax = ObjectHelper.GetStringFromObjectValue(data[49]);									//  TbC_TextColumnFormat
				_ay = ObjectHelper.GetNullableDoubleFromObjectValue(data[50]);					//  TbC_ColumnWidth
				_az = ObjectHelper.GetStringFromObjectValue(data[51]);									//  TbC_TextBoxTextAlignment
				_ba = ObjectHelper.GetStringFromObjectValue(data[52]);									//  TbC_ColumnTextAlignment
				_bb = ObjectHelper.GetNullableGuidFromObjectValue(data[53]);						//  TbC_ListStoredProc_Id
				_bc = ObjectHelper.GetStringFromObjectValue(data[54]);									//  TbC_ListStoredProc_Id_TextField
				_bd = ObjectHelper.GetBoolFromObjectValue(data[55]);									//  TbC_IsStaticList
				_be = ObjectHelper.GetBoolFromObjectValue(data[56]);									//  TbC_UseNotInList
				_bf = ObjectHelper.GetBoolFromObjectValue(data[57]);									//  TbC_UseListEditBtn
				_bg = ObjectHelper.GetNullableGuidFromObjectValue(data[58]);						//  TbC_ParentColumnKey
				_bh = ObjectHelper.GetStringFromObjectValue(data[59]);									//  TbC_ParentColumnKey_TextField
				_bi = ObjectHelper.GetBoolFromObjectValue(data[60]);									//  TbC_UseDisplayTextFieldProperty
				_bj = ObjectHelper.GetBoolFromObjectValue(data[61]);									//  TbC_IsDisplayTextFieldProperty
				_bk = ObjectHelper.GetNullableGuidFromObjectValue(data[62]);						//  TbC_ComboListTable_Id
				_bl = ObjectHelper.GetStringFromObjectValue(data[63]);									//  TbC_ComboListTable_Id_TextField
				_bm = ObjectHelper.GetNullableGuidFromObjectValue(data[64]);						//  TbC_ComboListDisplayColumn_Id
				_bn = ObjectHelper.GetStringFromObjectValue(data[65]);									//  TbC_ComboListDisplayColumn_Id_TextField
				_bo = ObjectHelper.GetNullableDoubleFromObjectValue(data[66]);					//  TbC_Combo_MaxDropdownHeight
				_bp = ObjectHelper.GetNullableDoubleFromObjectValue(data[67]);					//  TbC_Combo_MaxDropdownWidth
				_bq = ObjectHelper.GetBoolFromObjectValue(data[68]);									//  TbC_Combo_AllowDropdownResizing
				_br = ObjectHelper.GetBoolFromObjectValue(data[69]);									//  TbC_Combo_IsResetButtonVisible
				_bs = ObjectHelper.GetBoolFromObjectValue(data[70]);									//  TbC_IsActiveRecColumn
				_bt = ObjectHelper.GetBoolFromObjectValue(data[71]);									//  TbC_IsDeletedColumn
				_bu = ObjectHelper.GetBoolFromObjectValue(data[72]);									//  TbC_IsCreatedUserIdColumn
				_bv = ObjectHelper.GetBoolFromObjectValue(data[73]);									//  TbC_IsCreatedDateColumn
				_bw = ObjectHelper.GetBoolFromObjectValue(data[74]);									//  TbC_IsUserIdColumn
				_bx = ObjectHelper.GetBoolFromObjectValue(data[75]);									//  TbC_IsModifiedDateColumn
				_by = ObjectHelper.GetBoolFromObjectValue(data[76]);									//  TbC_IsRowVersionStampColumn
				_bz = ObjectHelper.GetBoolFromObjectValue(data[77]);									//  TbC_IsBrowsable
				_ca = ObjectHelper.GetStringFromObjectValue(data[78]);									//  TbC_DeveloperNote
				_cb = ObjectHelper.GetStringFromObjectValue(data[79]);									//  TbC_UserNote
				_cc = ObjectHelper.GetStringFromObjectValue(data[80]);									//  TbC_HelpFileAdditionalNote
				_cd = ObjectHelper.GetStringFromObjectValue(data[81]);									//  TbC_Notes
				_ce = ObjectHelper.GetBoolFromObjectValue(data[82]);									//  TbC_IsInputComplete
				_cf = ObjectHelper.GetBoolFromObjectValue(data[83]);									//  TbC_IsCodeGen
				_cg = ObjectHelper.GetNullableBoolFromObjectValue(data[84]);					//  TbC_IsReadyCodeGen
				_ch = ObjectHelper.GetNullableBoolFromObjectValue(data[85]);					//  TbC_IsCodeGenComplete
				_ci = ObjectHelper.GetNullableBoolFromObjectValue(data[86]);					//  TbC_IsTagForCodeGen
				_cj = ObjectHelper.GetNullableBoolFromObjectValue(data[87]);					//  TbC_IsTagForOther
				_ck = ObjectHelper.GetStringFromObjectValue(data[88]);									//  TbC_ColumnGroups
				_cl = ObjectHelper.GetBoolFromObjectValue(data[89]);									//  TbC_IsActiveRow
				_cm = ObjectHelper.GetBoolFromObjectValue(data[90]);									//  TbC_IsDeleted
				_cnxx = ObjectHelper.GetNullableGuidFromObjectValue(data[91]);						//  TbC_CreatedUserId
				_co = ObjectHelper.GetNullableDateTimeFromObjectValue(data[92]);					//  TbC_CreatedDate
				_cp = ObjectHelper.GetNullableGuidFromObjectValue(data[93]);						//  TbC_UserId
				_cq = ObjectHelper.GetStringFromObjectValue(data[94]);									//  UserName
				_cr = ObjectHelper.GetNullableDateTimeFromObjectValue(data[95]);					//  TbC_LastModifiedDate
				_cs = data[96] as Byte[];						//  TbC_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumn_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumn_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableColumn", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  TbC_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  TbC_Tb_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  TbC_auditColumn_Id
				_d = ObjectHelper.GetNullableShortFromObjectValue(data[3]);							//  TbC_SortOrder
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  TbC_Name
				_f = ObjectHelper.GetNullableIntFromObjectValue(data[5]);							//  TbC_CtlTp_Id
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  TbC_CtlTp_Id_TextField
				_h = ObjectHelper.GetBoolFromObjectValue(data[7]);									//  TbC_IsNonvColumn
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  TbC_Description
				_j = ObjectHelper.GetNullableIntFromObjectValue(data[9]);							//  TbC_DtSql_Id
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  TbC_DtSql_Id_TextField
				_l = ObjectHelper.GetNullableIntFromObjectValue(data[11]);							//  TbC_DtDtNt_Id
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  TbC_DtDtNt_Id_TextField
				_n = ObjectHelper.GetNullableIntFromObjectValue(data[13]);							//  TbC_Length
				_o = ObjectHelper.GetNullableIntFromObjectValue(data[14]);							//  TbC_Precision
				_p = ObjectHelper.GetNullableIntFromObjectValue(data[15]);							//  TbC_Scale
				_q = ObjectHelper.GetBoolFromObjectValue(data[16]);									//  TbC_IsPK
				_r = ObjectHelper.GetBoolFromObjectValue(data[17]);									//  TbC_IsIdentity
				_s = ObjectHelper.GetBoolFromObjectValue(data[18]);									//  TbC_IsFK
				_t = ObjectHelper.GetBoolFromObjectValue(data[19]);									//  TbC_IsEntityColumn
				_u = ObjectHelper.GetBoolFromObjectValue(data[20]);									//  TbC_IsSystemField
				_v = ObjectHelper.GetBoolFromObjectValue(data[21]);									//  TbC_IsValuesObjectMember
				_w = ObjectHelper.GetBoolFromObjectValue(data[22]);									//  TbC_IsInPropsScreen
				_x = ObjectHelper.GetBoolFromObjectValue(data[23]);									//  TbC_IsInNavList
				_y = ObjectHelper.GetBoolFromObjectValue(data[24]);									//  TbC_IsRequired
				_z = ObjectHelper.GetStringFromObjectValue(data[25]);									//  TbC_BrokenRuleText
				_aa = ObjectHelper.GetBoolFromObjectValue(data[26]);									//  TbC_AllowZero
				_ab = ObjectHelper.GetBoolFromObjectValue(data[27]);									//  TbC_IsNullableInDb
				_ac = ObjectHelper.GetBoolFromObjectValue(data[28]);									//  TbC_IsNullableInUI
				_ad = ObjectHelper.GetStringFromObjectValue(data[29]);									//  TbC_DefaultValue
				_ae = ObjectHelper.GetBoolFromObjectValue(data[30]);									//  TbC_IsReadFromDb
				_af = ObjectHelper.GetBoolFromObjectValue(data[31]);									//  TbC_IsSendToDb
				_ag = ObjectHelper.GetBoolFromObjectValue(data[32]);									//  TbC_IsInsertAllowed
				_ah = ObjectHelper.GetBoolFromObjectValue(data[33]);									//  TbC_IsEditAllowed
				_ai = ObjectHelper.GetBoolFromObjectValue(data[34]);									//  TbC_IsReadOnlyInUI
				_aj = ObjectHelper.GetBoolFromObjectValue(data[35]);									//  TbC_UseForAudit
				_ak = ObjectHelper.GetStringFromObjectValue(data[36]);									//  TbC_DefaultCaption
				_al = ObjectHelper.GetStringFromObjectValue(data[37]);									//  TbC_ColumnHeaderText
				_am = ObjectHelper.GetStringFromObjectValue(data[38]);									//  TbC_LabelCaptionVerbose
				_an = ObjectHelper.GetBoolFromObjectValue(data[39]);									//  TbC_LabelCaptionGenerate
				_ao = ObjectHelper.GetBoolFromObjectValue(data[40]);									//  Tbc_ShowGridColumnToolTip
				_ap = ObjectHelper.GetBoolFromObjectValue(data[41]);									//  Tbc_ShowPropsToolTip
				_aq = ObjectHelper.GetStringFromObjectValue(data[42]);									//  Tbc_TooltipsRolledUp
				_ar = ObjectHelper.GetBoolFromObjectValue(data[43]);									//  TbC_IsCreatePropsStrings
				_asxx = ObjectHelper.GetBoolFromObjectValue(data[44]);									//  TbC_IsCreateGridStrings
				_at = ObjectHelper.GetBoolFromObjectValue(data[45]);									//  TbC_IsAvailableForColumnGroups
				_au = ObjectHelper.GetBoolFromObjectValue(data[46]);									//  TbC_IsTextWrapInProp
				_av = ObjectHelper.GetBoolFromObjectValue(data[47]);									//  TbC_IsTextWrapInGrid
				_aw = ObjectHelper.GetStringFromObjectValue(data[48]);									//  TbC_TextBoxFormat
				_ax = ObjectHelper.GetStringFromObjectValue(data[49]);									//  TbC_TextColumnFormat
				_ay = ObjectHelper.GetNullableDoubleFromObjectValue(data[50]);					//  TbC_ColumnWidth
				_az = ObjectHelper.GetStringFromObjectValue(data[51]);									//  TbC_TextBoxTextAlignment
				_ba = ObjectHelper.GetStringFromObjectValue(data[52]);									//  TbC_ColumnTextAlignment
				_bb = ObjectHelper.GetNullableGuidFromObjectValue(data[53]);						//  TbC_ListStoredProc_Id
				_bc = ObjectHelper.GetStringFromObjectValue(data[54]);									//  TbC_ListStoredProc_Id_TextField
				_bd = ObjectHelper.GetBoolFromObjectValue(data[55]);									//  TbC_IsStaticList
				_be = ObjectHelper.GetBoolFromObjectValue(data[56]);									//  TbC_UseNotInList
				_bf = ObjectHelper.GetBoolFromObjectValue(data[57]);									//  TbC_UseListEditBtn
				_bg = ObjectHelper.GetNullableGuidFromObjectValue(data[58]);						//  TbC_ParentColumnKey
				_bh = ObjectHelper.GetStringFromObjectValue(data[59]);									//  TbC_ParentColumnKey_TextField
				_bi = ObjectHelper.GetBoolFromObjectValue(data[60]);									//  TbC_UseDisplayTextFieldProperty
				_bj = ObjectHelper.GetBoolFromObjectValue(data[61]);									//  TbC_IsDisplayTextFieldProperty
				_bk = ObjectHelper.GetNullableGuidFromObjectValue(data[62]);						//  TbC_ComboListTable_Id
				_bl = ObjectHelper.GetStringFromObjectValue(data[63]);									//  TbC_ComboListTable_Id_TextField
				_bm = ObjectHelper.GetNullableGuidFromObjectValue(data[64]);						//  TbC_ComboListDisplayColumn_Id
				_bn = ObjectHelper.GetStringFromObjectValue(data[65]);									//  TbC_ComboListDisplayColumn_Id_TextField
				_bo = ObjectHelper.GetNullableDoubleFromObjectValue(data[66]);					//  TbC_Combo_MaxDropdownHeight
				_bp = ObjectHelper.GetNullableDoubleFromObjectValue(data[67]);					//  TbC_Combo_MaxDropdownWidth
				_bq = ObjectHelper.GetBoolFromObjectValue(data[68]);									//  TbC_Combo_AllowDropdownResizing
				_br = ObjectHelper.GetBoolFromObjectValue(data[69]);									//  TbC_Combo_IsResetButtonVisible
				_bs = ObjectHelper.GetBoolFromObjectValue(data[70]);									//  TbC_IsActiveRecColumn
				_bt = ObjectHelper.GetBoolFromObjectValue(data[71]);									//  TbC_IsDeletedColumn
				_bu = ObjectHelper.GetBoolFromObjectValue(data[72]);									//  TbC_IsCreatedUserIdColumn
				_bv = ObjectHelper.GetBoolFromObjectValue(data[73]);									//  TbC_IsCreatedDateColumn
				_bw = ObjectHelper.GetBoolFromObjectValue(data[74]);									//  TbC_IsUserIdColumn
				_bx = ObjectHelper.GetBoolFromObjectValue(data[75]);									//  TbC_IsModifiedDateColumn
				_by = ObjectHelper.GetBoolFromObjectValue(data[76]);									//  TbC_IsRowVersionStampColumn
				_bz = ObjectHelper.GetBoolFromObjectValue(data[77]);									//  TbC_IsBrowsable
				_ca = ObjectHelper.GetStringFromObjectValue(data[78]);									//  TbC_DeveloperNote
				_cb = ObjectHelper.GetStringFromObjectValue(data[79]);									//  TbC_UserNote
				_cc = ObjectHelper.GetStringFromObjectValue(data[80]);									//  TbC_HelpFileAdditionalNote
				_cd = ObjectHelper.GetStringFromObjectValue(data[81]);									//  TbC_Notes
				_ce = ObjectHelper.GetBoolFromObjectValue(data[82]);									//  TbC_IsInputComplete
				_cf = ObjectHelper.GetBoolFromObjectValue(data[83]);									//  TbC_IsCodeGen
				_cg = ObjectHelper.GetNullableBoolFromObjectValue(data[84]);					//  TbC_IsReadyCodeGen
				_ch = ObjectHelper.GetNullableBoolFromObjectValue(data[85]);					//  TbC_IsCodeGenComplete
				_ci = ObjectHelper.GetNullableBoolFromObjectValue(data[86]);					//  TbC_IsTagForCodeGen
				_cj = ObjectHelper.GetNullableBoolFromObjectValue(data[87]);					//  TbC_IsTagForOther
				_ck = ObjectHelper.GetStringFromObjectValue(data[88]);									//  TbC_ColumnGroups
				_cl = ObjectHelper.GetBoolFromObjectValue(data[89]);									//  TbC_IsActiveRow
				_cm = ObjectHelper.GetBoolFromObjectValue(data[90]);									//  TbC_IsDeleted
				_cnxx = ObjectHelper.GetNullableGuidFromObjectValue(data[91]);						//  TbC_CreatedUserId
				_co = ObjectHelper.GetNullableDateTimeFromObjectValue(data[92]);					//  TbC_CreatedDate
				_cp = ObjectHelper.GetNullableGuidFromObjectValue(data[93]);						//  TbC_UserId
				_cq = ObjectHelper.GetStringFromObjectValue(data[94]);									//  UserName
				_cr = ObjectHelper.GetNullableDateTimeFromObjectValue(data[95]);					//  TbC_LastModifiedDate
				_cs = data[96] as Byte[];						//  TbC_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableColumn", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableColumn", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  TbC_Id

		[DataMember]
		public Guid? _b;			//  TbC_Tb_Id

		[DataMember]
		public Guid? _c;			//  TbC_auditColumn_Id

		[DataMember]
		public Int16? _d;			//  TbC_SortOrder

		[DataMember]
		public String _e;			//  TbC_Name

		[DataMember]
		public Int32? _f;			//  TbC_CtlTp_Id

		[DataMember]
		public String _g;			//  TbC_CtlTp_Id_TextField

		[DataMember]
		public Boolean _h;			//  TbC_IsNonvColumn

		[DataMember]
		public String _i;			//  TbC_Description

		[DataMember]
		public Int32? _j;			//  TbC_DtSql_Id

		[DataMember]
		public String _k;			//  TbC_DtSql_Id_TextField

		[DataMember]
		public Int32? _l;			//  TbC_DtDtNt_Id

		[DataMember]
		public String _m;			//  TbC_DtDtNt_Id_TextField

		[DataMember]
		public Int32? _n;			//  TbC_Length

		[DataMember]
		public Int32? _o;			//  TbC_Precision

		[DataMember]
		public Int32? _p;			//  TbC_Scale

		[DataMember]
		public Boolean _q;			//  TbC_IsPK

		[DataMember]
		public Boolean _r;			//  TbC_IsIdentity

		[DataMember]
		public Boolean _s;			//  TbC_IsFK

		[DataMember]
		public Boolean _t;			//  TbC_IsEntityColumn

		[DataMember]
		public Boolean _u;			//  TbC_IsSystemField

		[DataMember]
		public Boolean _v;			//  TbC_IsValuesObjectMember

		[DataMember]
		public Boolean _w;			//  TbC_IsInPropsScreen

		[DataMember]
		public Boolean _x;			//  TbC_IsInNavList

		[DataMember]
		public Boolean _y;			//  TbC_IsRequired

		[DataMember]
		public String _z;			//  TbC_BrokenRuleText

		[DataMember]
		public Boolean _aa;			//  TbC_AllowZero

		[DataMember]
		public Boolean _ab;			//  TbC_IsNullableInDb

		[DataMember]
		public Boolean _ac;			//  TbC_IsNullableInUI

		[DataMember]
		public String _ad;			//  TbC_DefaultValue

		[DataMember]
		public Boolean _ae;			//  TbC_IsReadFromDb

		[DataMember]
		public Boolean _af;			//  TbC_IsSendToDb

		[DataMember]
		public Boolean _ag;			//  TbC_IsInsertAllowed

		[DataMember]
		public Boolean _ah;			//  TbC_IsEditAllowed

		[DataMember]
		public Boolean _ai;			//  TbC_IsReadOnlyInUI

		[DataMember]
		public Boolean _aj;			//  TbC_UseForAudit

		[DataMember]
		public String _ak;			//  TbC_DefaultCaption

		[DataMember]
		public String _al;			//  TbC_ColumnHeaderText

		[DataMember]
		public String _am;			//  TbC_LabelCaptionVerbose

		[DataMember]
		public Boolean _an;			//  TbC_LabelCaptionGenerate

		[DataMember]
		public Boolean _ao;			//  Tbc_ShowGridColumnToolTip

		[DataMember]
		public Boolean _ap;			//  Tbc_ShowPropsToolTip

		[DataMember]
		public String _aq;			//  Tbc_TooltipsRolledUp

		[DataMember]
		public Boolean _ar;			//  TbC_IsCreatePropsStrings

		[DataMember]
		public Boolean _asxx;			//  TbC_IsCreateGridStrings

		[DataMember]
		public Boolean _at;			//  TbC_IsAvailableForColumnGroups

		[DataMember]
		public Boolean _au;			//  TbC_IsTextWrapInProp

		[DataMember]
		public Boolean _av;			//  TbC_IsTextWrapInGrid

		[DataMember]
		public String _aw;			//  TbC_TextBoxFormat

		[DataMember]
		public String _ax;			//  TbC_TextColumnFormat

		[DataMember]
		public Double? _ay;			//  TbC_ColumnWidth

		[DataMember]
		public String _az;			//  TbC_TextBoxTextAlignment

		[DataMember]
		public String _ba;			//  TbC_ColumnTextAlignment

		[DataMember]
		public Guid? _bb;			//  TbC_ListStoredProc_Id

		[DataMember]
		public String _bc;			//  TbC_ListStoredProc_Id_TextField

		[DataMember]
		public Boolean _bd;			//  TbC_IsStaticList

		[DataMember]
		public Boolean _be;			//  TbC_UseNotInList

		[DataMember]
		public Boolean _bf;			//  TbC_UseListEditBtn

		[DataMember]
		public Guid? _bg;			//  TbC_ParentColumnKey

		[DataMember]
		public String _bh;			//  TbC_ParentColumnKey_TextField

		[DataMember]
		public Boolean _bi;			//  TbC_UseDisplayTextFieldProperty

		[DataMember]
		public Boolean _bj;			//  TbC_IsDisplayTextFieldProperty

		[DataMember]
		public Guid? _bk;			//  TbC_ComboListTable_Id

		[DataMember]
		public String _bl;			//  TbC_ComboListTable_Id_TextField

		[DataMember]
		public Guid? _bm;			//  TbC_ComboListDisplayColumn_Id

		[DataMember]
		public String _bn;			//  TbC_ComboListDisplayColumn_Id_TextField

		[DataMember]
		public Double? _bo;			//  TbC_Combo_MaxDropdownHeight

		[DataMember]
		public Double? _bp;			//  TbC_Combo_MaxDropdownWidth

		[DataMember]
		public Boolean _bq;			//  TbC_Combo_AllowDropdownResizing

		[DataMember]
		public Boolean _br;			//  TbC_Combo_IsResetButtonVisible

		[DataMember]
		public Boolean _bs;			//  TbC_IsActiveRecColumn

		[DataMember]
		public Boolean _bt;			//  TbC_IsDeletedColumn

		[DataMember]
		public Boolean _bu;			//  TbC_IsCreatedUserIdColumn

		[DataMember]
		public Boolean _bv;			//  TbC_IsCreatedDateColumn

		[DataMember]
		public Boolean _bw;			//  TbC_IsUserIdColumn

		[DataMember]
		public Boolean _bx;			//  TbC_IsModifiedDateColumn

		[DataMember]
		public Boolean _by;			//  TbC_IsRowVersionStampColumn

		[DataMember]
		public Boolean _bz;			//  TbC_IsBrowsable

		[DataMember]
		public String _ca;			//  TbC_DeveloperNote

		[DataMember]
		public String _cb;			//  TbC_UserNote

		[DataMember]
		public String _cc;			//  TbC_HelpFileAdditionalNote

		[DataMember]
		public String _cd;			//  TbC_Notes

		[DataMember]
		public Boolean _ce;			//  TbC_IsInputComplete

		[DataMember]
		public Boolean _cf;			//  TbC_IsCodeGen

		[DataMember]
		public Boolean? _cg;			//  TbC_IsReadyCodeGen

		[DataMember]
		public Boolean? _ch;			//  TbC_IsCodeGenComplete

		[DataMember]
		public Boolean? _ci;			//  TbC_IsTagForCodeGen

		[DataMember]
		public Boolean? _cj;			//  TbC_IsTagForOther

		[DataMember]
		public String _ck;			//  TbC_ColumnGroups

		[DataMember]
		public Boolean _cl;			//  TbC_IsActiveRow

		[DataMember]
		public Boolean _cm;			//  TbC_IsDeleted

		[DataMember]
		public Guid? _cnxx;			//  TbC_CreatedUserId

		[DataMember]
		public DateTime? _co;			//  TbC_CreatedDate

		[DataMember]
		public Guid? _cp;			//  TbC_UserId

		[DataMember]
		public String _cq;			//  UserName

		[DataMember]
		public DateTime? _cr;			//  TbC_LastModifiedDate

		[DataMember]
		public Byte[] _cs;			//  TbC_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcTableColumn_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 97; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                    _list[18] = _s;
                    _list[19] = _t;
                    _list[20] = _u;
                    _list[21] = _v;
                    _list[22] = _w;
                    _list[23] = _x;
                    _list[24] = _y;
                    _list[25] = _z;
                    _list[26] = _aa;
                    _list[27] = _ab;
                    _list[28] = _ac;
                    _list[29] = _ad;
                    _list[30] = _ae;
                    _list[31] = _af;
                    _list[32] = _ag;
                    _list[33] = _ah;
                    _list[34] = _ai;
                    _list[35] = _aj;
                    _list[36] = _ak;
                    _list[37] = _al;
                    _list[38] = _am;
                    _list[39] = _an;
                    _list[40] = _ao;
                    _list[41] = _ap;
                    _list[42] = _aq;
                    _list[43] = _ar;
                    _list[44] = _asxx;
                    _list[45] = _at;
                    _list[46] = _au;
                    _list[47] = _av;
                    _list[48] = _aw;
                    _list[49] = _ax;
                    _list[50] = _ay;
                    _list[51] = _az;
                    _list[52] = _ba;
                    _list[53] = _bb;
                    _list[54] = _bc;
                    _list[55] = _bd;
                    _list[56] = _be;
                    _list[57] = _bf;
                    _list[58] = _bg;
                    _list[59] = _bh;
                    _list[60] = _bi;
                    _list[61] = _bj;
                    _list[62] = _bk;
                    _list[63] = _bl;
                    _list[64] = _bm;
                    _list[65] = _bn;
                    _list[66] = _bo;
                    _list[67] = _bp;
                    _list[68] = _bq;
                    _list[69] = _br;
                    _list[70] = _bs;
                    _list[71] = _bt;
                    _list[72] = _bu;
                    _list[73] = _bv;
                    _list[74] = _bw;
                    _list[75] = _bx;
                    _list[76] = _by;
                    _list[77] = _bz;
                    _list[78] = _ca;
                    _list[79] = _cb;
                    _list[80] = _cc;
                    _list[81] = _cd;
                    _list[82] = _ce;
                    _list[83] = _cf;
                    _list[84] = _cg;
                    _list[85] = _ch;
                    _list[86] = _ci;
                    _list[87] = _cj;
                    _list[88] = _ck;
                    _list[89] = _cl;
                    _list[90] = _cm;
                    _list[91] = _cnxx;
                    _list[92] = _co;
                    _list[93] = _cp;
                    _list[94] = _cq;
                    _list[95] = _cr;
                    _list[96] = _cs;
                }
                return _list;
            }
        }

        public Guid TbC_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid TbC_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? TbC_Tb_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_Tb_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Guid? TbC_auditColumn_Id
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_auditColumn_Id_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public Int16? TbC_SortOrder
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int16? TbC_SortOrder_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String TbC_Name
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_Name_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Int32? TbC_CtlTp_Id
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbC_CtlTp_Id_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public String TbC_CtlTp_Id_TextField
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_CtlTp_Id_TextField_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Boolean TbC_IsNonvColumn
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsNonvColumn_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public String TbC_Description
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_Description_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Int32? TbC_DtSql_Id
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbC_DtSql_Id_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public String TbC_DtSql_Id_TextField
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_DtSql_Id_TextField_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public Int32? TbC_DtDtNt_Id
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbC_DtDtNt_Id_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public String TbC_DtDtNt_Id_TextField
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_DtDtNt_Id_TextField_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public Int32? TbC_Length
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbC_Length_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public Int32? TbC_Precision
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbC_Precision_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public Int32? TbC_Scale
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbC_Scale_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public Boolean TbC_IsPK
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsPK_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public Boolean TbC_IsIdentity
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsIdentity_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        public Boolean TbC_IsFK
        {
            get { return _s; }
            set
            {
                _s = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsFK_noevents
        {
            get { return _s; }
            set
            {
                _s = value;
            }
        }

        public Boolean TbC_IsEntityColumn
        {
            get { return _t; }
            set
            {
                _t = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsEntityColumn_noevents
        {
            get { return _t; }
            set
            {
                _t = value;
            }
        }

        public Boolean TbC_IsSystemField
        {
            get { return _u; }
            set
            {
                _u = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsSystemField_noevents
        {
            get { return _u; }
            set
            {
                _u = value;
            }
        }

        public Boolean TbC_IsValuesObjectMember
        {
            get { return _v; }
            set
            {
                _v = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsValuesObjectMember_noevents
        {
            get { return _v; }
            set
            {
                _v = value;
            }
        }

        public Boolean TbC_IsInPropsScreen
        {
            get { return _w; }
            set
            {
                _w = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsInPropsScreen_noevents
        {
            get { return _w; }
            set
            {
                _w = value;
            }
        }

        public Boolean TbC_IsInNavList
        {
            get { return _x; }
            set
            {
                _x = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsInNavList_noevents
        {
            get { return _x; }
            set
            {
                _x = value;
            }
        }

        public Boolean TbC_IsRequired
        {
            get { return _y; }
            set
            {
                _y = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsRequired_noevents
        {
            get { return _y; }
            set
            {
                _y = value;
            }
        }

        public String TbC_BrokenRuleText
        {
            get { return _z; }
            set
            {
                _z = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_BrokenRuleText_noevents
        {
            get { return _z; }
            set
            {
                _z = value;
            }
        }

        public Boolean TbC_AllowZero
        {
            get { return _aa; }
            set
            {
                _aa = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_AllowZero_noevents
        {
            get { return _aa; }
            set
            {
                _aa = value;
            }
        }

        public Boolean TbC_IsNullableInDb
        {
            get { return _ab; }
            set
            {
                _ab = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsNullableInDb_noevents
        {
            get { return _ab; }
            set
            {
                _ab = value;
            }
        }

        public Boolean TbC_IsNullableInUI
        {
            get { return _ac; }
            set
            {
                _ac = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsNullableInUI_noevents
        {
            get { return _ac; }
            set
            {
                _ac = value;
            }
        }

        public String TbC_DefaultValue
        {
            get { return _ad; }
            set
            {
                _ad = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_DefaultValue_noevents
        {
            get { return _ad; }
            set
            {
                _ad = value;
            }
        }

        public Boolean TbC_IsReadFromDb
        {
            get { return _ae; }
            set
            {
                _ae = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsReadFromDb_noevents
        {
            get { return _ae; }
            set
            {
                _ae = value;
            }
        }

        public Boolean TbC_IsSendToDb
        {
            get { return _af; }
            set
            {
                _af = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsSendToDb_noevents
        {
            get { return _af; }
            set
            {
                _af = value;
            }
        }

        public Boolean TbC_IsInsertAllowed
        {
            get { return _ag; }
            set
            {
                _ag = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsInsertAllowed_noevents
        {
            get { return _ag; }
            set
            {
                _ag = value;
            }
        }

        public Boolean TbC_IsEditAllowed
        {
            get { return _ah; }
            set
            {
                _ah = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsEditAllowed_noevents
        {
            get { return _ah; }
            set
            {
                _ah = value;
            }
        }

        public Boolean TbC_IsReadOnlyInUI
        {
            get { return _ai; }
            set
            {
                _ai = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsReadOnlyInUI_noevents
        {
            get { return _ai; }
            set
            {
                _ai = value;
            }
        }

        public Boolean TbC_UseForAudit
        {
            get { return _aj; }
            set
            {
                _aj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_UseForAudit_noevents
        {
            get { return _aj; }
            set
            {
                _aj = value;
            }
        }

        public String TbC_DefaultCaption
        {
            get { return _ak; }
            set
            {
                _ak = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_DefaultCaption_noevents
        {
            get { return _ak; }
            set
            {
                _ak = value;
            }
        }

        public String TbC_ColumnHeaderText
        {
            get { return _al; }
            set
            {
                _al = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ColumnHeaderText_noevents
        {
            get { return _al; }
            set
            {
                _al = value;
            }
        }

        public String TbC_LabelCaptionVerbose
        {
            get { return _am; }
            set
            {
                _am = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_LabelCaptionVerbose_noevents
        {
            get { return _am; }
            set
            {
                _am = value;
            }
        }

        public Boolean TbC_LabelCaptionGenerate
        {
            get { return _an; }
            set
            {
                _an = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_LabelCaptionGenerate_noevents
        {
            get { return _an; }
            set
            {
                _an = value;
            }
        }

        public Boolean Tbc_ShowGridColumnToolTip
        {
            get { return _ao; }
            set
            {
                _ao = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tbc_ShowGridColumnToolTip_noevents
        {
            get { return _ao; }
            set
            {
                _ao = value;
            }
        }

        public Boolean Tbc_ShowPropsToolTip
        {
            get { return _ap; }
            set
            {
                _ap = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tbc_ShowPropsToolTip_noevents
        {
            get { return _ap; }
            set
            {
                _ap = value;
            }
        }

        public String Tbc_TooltipsRolledUp
        {
            get { return _aq; }
            set
            {
                _aq = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TooltipsRolledUp_noevents
        {
            get { return _aq; }
            set
            {
                _aq = value;
            }
        }

        public Boolean TbC_IsCreatePropsStrings
        {
            get { return _ar; }
            set
            {
                _ar = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsCreatePropsStrings_noevents
        {
            get { return _ar; }
            set
            {
                _ar = value;
            }
        }

        public Boolean TbC_IsCreateGridStrings
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsCreateGridStrings_noevents
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
            }
        }

        public Boolean TbC_IsAvailableForColumnGroups
        {
            get { return _at; }
            set
            {
                _at = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsAvailableForColumnGroups_noevents
        {
            get { return _at; }
            set
            {
                _at = value;
            }
        }

        public Boolean TbC_IsTextWrapInProp
        {
            get { return _au; }
            set
            {
                _au = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsTextWrapInProp_noevents
        {
            get { return _au; }
            set
            {
                _au = value;
            }
        }

        public Boolean TbC_IsTextWrapInGrid
        {
            get { return _av; }
            set
            {
                _av = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsTextWrapInGrid_noevents
        {
            get { return _av; }
            set
            {
                _av = value;
            }
        }

        public String TbC_TextBoxFormat
        {
            get { return _aw; }
            set
            {
                _aw = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_TextBoxFormat_noevents
        {
            get { return _aw; }
            set
            {
                _aw = value;
            }
        }

        public String TbC_TextColumnFormat
        {
            get { return _ax; }
            set
            {
                _ax = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_TextColumnFormat_noevents
        {
            get { return _ax; }
            set
            {
                _ax = value;
            }
        }

        public Double? TbC_ColumnWidth
        {
            get { return _ay; }
            set
            {
                _ay = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Double? TbC_ColumnWidth_noevents
        {
            get { return _ay; }
            set
            {
                _ay = value;
            }
        }

        public String TbC_TextBoxTextAlignment
        {
            get { return _az; }
            set
            {
                _az = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_TextBoxTextAlignment_noevents
        {
            get { return _az; }
            set
            {
                _az = value;
            }
        }

        public String TbC_ColumnTextAlignment
        {
            get { return _ba; }
            set
            {
                _ba = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ColumnTextAlignment_noevents
        {
            get { return _ba; }
            set
            {
                _ba = value;
            }
        }

        public Guid? TbC_ListStoredProc_Id
        {
            get { return _bb; }
            set
            {
                _bb = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_ListStoredProc_Id_noevents
        {
            get { return _bb; }
            set
            {
                _bb = value;
            }
        }

        public String TbC_ListStoredProc_Id_TextField
        {
            get { return _bc; }
            set
            {
                _bc = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ListStoredProc_Id_TextField_noevents
        {
            get { return _bc; }
            set
            {
                _bc = value;
            }
        }

        public Boolean TbC_IsStaticList
        {
            get { return _bd; }
            set
            {
                _bd = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsStaticList_noevents
        {
            get { return _bd; }
            set
            {
                _bd = value;
            }
        }

        public Boolean TbC_UseNotInList
        {
            get { return _be; }
            set
            {
                _be = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_UseNotInList_noevents
        {
            get { return _be; }
            set
            {
                _be = value;
            }
        }

        public Boolean TbC_UseListEditBtn
        {
            get { return _bf; }
            set
            {
                _bf = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_UseListEditBtn_noevents
        {
            get { return _bf; }
            set
            {
                _bf = value;
            }
        }

        public Guid? TbC_ParentColumnKey
        {
            get { return _bg; }
            set
            {
                _bg = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_ParentColumnKey_noevents
        {
            get { return _bg; }
            set
            {
                _bg = value;
            }
        }

        public String TbC_ParentColumnKey_TextField
        {
            get { return _bh; }
            set
            {
                _bh = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ParentColumnKey_TextField_noevents
        {
            get { return _bh; }
            set
            {
                _bh = value;
            }
        }

        public Boolean TbC_UseDisplayTextFieldProperty
        {
            get { return _bi; }
            set
            {
                _bi = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_UseDisplayTextFieldProperty_noevents
        {
            get { return _bi; }
            set
            {
                _bi = value;
            }
        }

        public Boolean TbC_IsDisplayTextFieldProperty
        {
            get { return _bj; }
            set
            {
                _bj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsDisplayTextFieldProperty_noevents
        {
            get { return _bj; }
            set
            {
                _bj = value;
            }
        }

        public Guid? TbC_ComboListTable_Id
        {
            get { return _bk; }
            set
            {
                _bk = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_ComboListTable_Id_noevents
        {
            get { return _bk; }
            set
            {
                _bk = value;
            }
        }

        public String TbC_ComboListTable_Id_TextField
        {
            get { return _bl; }
            set
            {
                _bl = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ComboListTable_Id_TextField_noevents
        {
            get { return _bl; }
            set
            {
                _bl = value;
            }
        }

        public Guid? TbC_ComboListDisplayColumn_Id
        {
            get { return _bm; }
            set
            {
                _bm = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_ComboListDisplayColumn_Id_noevents
        {
            get { return _bm; }
            set
            {
                _bm = value;
            }
        }

        public String TbC_ComboListDisplayColumn_Id_TextField
        {
            get { return _bn; }
            set
            {
                _bn = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ComboListDisplayColumn_Id_TextField_noevents
        {
            get { return _bn; }
            set
            {
                _bn = value;
            }
        }

        public Double? TbC_Combo_MaxDropdownHeight
        {
            get { return _bo; }
            set
            {
                _bo = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Double? TbC_Combo_MaxDropdownHeight_noevents
        {
            get { return _bo; }
            set
            {
                _bo = value;
            }
        }

        public Double? TbC_Combo_MaxDropdownWidth
        {
            get { return _bp; }
            set
            {
                _bp = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Double? TbC_Combo_MaxDropdownWidth_noevents
        {
            get { return _bp; }
            set
            {
                _bp = value;
            }
        }

        public Boolean TbC_Combo_AllowDropdownResizing
        {
            get { return _bq; }
            set
            {
                _bq = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_Combo_AllowDropdownResizing_noevents
        {
            get { return _bq; }
            set
            {
                _bq = value;
            }
        }

        public Boolean TbC_Combo_IsResetButtonVisible
        {
            get { return _br; }
            set
            {
                _br = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_Combo_IsResetButtonVisible_noevents
        {
            get { return _br; }
            set
            {
                _br = value;
            }
        }

        public Boolean TbC_IsActiveRecColumn
        {
            get { return _bs; }
            set
            {
                _bs = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsActiveRecColumn_noevents
        {
            get { return _bs; }
            set
            {
                _bs = value;
            }
        }

        public Boolean TbC_IsDeletedColumn
        {
            get { return _bt; }
            set
            {
                _bt = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsDeletedColumn_noevents
        {
            get { return _bt; }
            set
            {
                _bt = value;
            }
        }

        public Boolean TbC_IsCreatedUserIdColumn
        {
            get { return _bu; }
            set
            {
                _bu = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsCreatedUserIdColumn_noevents
        {
            get { return _bu; }
            set
            {
                _bu = value;
            }
        }

        public Boolean TbC_IsCreatedDateColumn
        {
            get { return _bv; }
            set
            {
                _bv = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsCreatedDateColumn_noevents
        {
            get { return _bv; }
            set
            {
                _bv = value;
            }
        }

        public Boolean TbC_IsUserIdColumn
        {
            get { return _bw; }
            set
            {
                _bw = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsUserIdColumn_noevents
        {
            get { return _bw; }
            set
            {
                _bw = value;
            }
        }

        public Boolean TbC_IsModifiedDateColumn
        {
            get { return _bx; }
            set
            {
                _bx = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsModifiedDateColumn_noevents
        {
            get { return _bx; }
            set
            {
                _bx = value;
            }
        }

        public Boolean TbC_IsRowVersionStampColumn
        {
            get { return _by; }
            set
            {
                _by = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsRowVersionStampColumn_noevents
        {
            get { return _by; }
            set
            {
                _by = value;
            }
        }

        public Boolean TbC_IsBrowsable
        {
            get { return _bz; }
            set
            {
                _bz = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsBrowsable_noevents
        {
            get { return _bz; }
            set
            {
                _bz = value;
            }
        }

        public String TbC_DeveloperNote
        {
            get { return _ca; }
            set
            {
                _ca = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_DeveloperNote_noevents
        {
            get { return _ca; }
            set
            {
                _ca = value;
            }
        }

        public String TbC_UserNote
        {
            get { return _cb; }
            set
            {
                _cb = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_UserNote_noevents
        {
            get { return _cb; }
            set
            {
                _cb = value;
            }
        }

        public String TbC_HelpFileAdditionalNote
        {
            get { return _cc; }
            set
            {
                _cc = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_HelpFileAdditionalNote_noevents
        {
            get { return _cc; }
            set
            {
                _cc = value;
            }
        }

        public String TbC_Notes
        {
            get { return _cd; }
            set
            {
                _cd = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_Notes_noevents
        {
            get { return _cd; }
            set
            {
                _cd = value;
            }
        }

        public Boolean TbC_IsInputComplete
        {
            get { return _ce; }
            set
            {
                _ce = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsInputComplete_noevents
        {
            get { return _ce; }
            set
            {
                _ce = value;
            }
        }

        public Boolean TbC_IsCodeGen
        {
            get { return _cf; }
            set
            {
                _cf = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsCodeGen_noevents
        {
            get { return _cf; }
            set
            {
                _cf = value;
            }
        }

        public Boolean? TbC_IsReadyCodeGen
        {
            get { return _cg; }
            set
            {
                _cg = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? TbC_IsReadyCodeGen_noevents
        {
            get { return _cg; }
            set
            {
                _cg = value;
            }
        }

        public Boolean? TbC_IsCodeGenComplete
        {
            get { return _ch; }
            set
            {
                _ch = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? TbC_IsCodeGenComplete_noevents
        {
            get { return _ch; }
            set
            {
                _ch = value;
            }
        }

        public Boolean? TbC_IsTagForCodeGen
        {
            get { return _ci; }
            set
            {
                _ci = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? TbC_IsTagForCodeGen_noevents
        {
            get { return _ci; }
            set
            {
                _ci = value;
            }
        }

        public Boolean? TbC_IsTagForOther
        {
            get { return _cj; }
            set
            {
                _cj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? TbC_IsTagForOther_noevents
        {
            get { return _cj; }
            set
            {
                _cj = value;
            }
        }

        public String TbC_ColumnGroups
        {
            get { return _ck; }
            set
            {
                _ck = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbC_ColumnGroups_noevents
        {
            get { return _ck; }
            set
            {
                _ck = value;
            }
        }

        public Boolean TbC_IsActiveRow
        {
            get { return _cl; }
            set
            {
                _cl = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsActiveRow_noevents
        {
            get { return _cl; }
            set
            {
                _cl = value;
            }
        }

        public Boolean TbC_IsDeleted
        {
            get { return _cm; }
            set
            {
                _cm = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbC_IsDeleted_noevents
        {
            get { return _cm; }
            set
            {
                _cm = value;
            }
        }

        public Guid? TbC_CreatedUserId
        {
            get { return _cnxx; }
            set
            {
                _cnxx = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_CreatedUserId_noevents
        {
            get { return _cnxx; }
            set
            {
                _cnxx = value;
            }
        }

        public DateTime? TbC_CreatedDate
        {
            get { return _co; }
            set
            {
                _co = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? TbC_CreatedDate_noevents
        {
            get { return _co; }
            set
            {
                _co = value;
            }
        }

        public Guid? TbC_UserId
        {
            get { return _cp; }
            set
            {
                _cp = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbC_UserId_noevents
        {
            get { return _cp; }
            set
            {
                _cp = value;
            }
        }

        public String UserName
        {
            get { return _cq; }
            set
            {
                _cq = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _cq; }
            set
            {
                _cq = value;
            }
        }

        public DateTime? TbC_LastModifiedDate
        {
            get { return _cr; }
            set
            {
                _cr = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? TbC_LastModifiedDate_noevents
        {
            get { return _cr; }
            set
            {
                _cr = value;
            }
        }

        public Byte[] TbC_Stamp
        {
            get { return _cs; }
            set
            {
                _cs = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] TbC_Stamp_noevents
        {
            get { return _cs; }
            set
            {
                _cs = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r,
				_s,
				_t,
				_u,
				_v,
				_w,
				_x,
				_y,
				_z,
				_aa,
				_ab,
				_ac,
				_ad,
				_ae,
				_af,
				_ag,
				_ah,
				_ai,
				_aj,
				_ak,
				_al,
				_am,
				_an,
				_ao,
				_ap,
				_aq,
				_ar,
				_asxx,
				_at,
				_au,
				_av,
				_aw,
				_ax,
				_ay,
				_az,
				_ba,
				_bb,
				_bc,
				_bd,
				_be,
				_bf,
				_bg,
				_bh,
				_bi,
				_bj,
				_bk,
				_bl,
				_bm,
				_bn,
				_bo,
				_bp,
				_bq,
				_br,
				_bs,
				_bt,
				_bu,
				_bv,
				_bw,
				_bx,
				_by,
				_bz,
				_ca,
				_cb,
				_cc,
				_cd,
				_ce,
				_cf,
				_cg,
				_ch,
				_ci,
				_cj,
				_ck,
				_cl,
				_cm,
				_cnxx,
				_co,
				_cp,
				_cq,
				_cr,
				_cs
			};
        }

        public WcTableColumn_Values Clone()
        {
            return new WcTableColumn_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


