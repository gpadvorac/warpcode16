using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

namespace EntityWireTypeSL
{


    [DataContract]
    public class IxExceptionTraceReview_ValuePairs_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "Ifx";
        private static string _cn = "IxExceptionTraceReview_ValuePairs_Binding";

        #endregion Initialize Variables


		#region Constructors

        public IxExceptionTraceReview_ValuePairs_Binding() { }


        public IxExceptionTraceReview_ValuePairs_Binding(Int32 _SortOrder, Guid? _TraceId, String _Key, String _Value )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IxExceptionTraceReview_ValuePairs_Binding", IfxTraceCategory.Enter);
				_a = _SortOrder;
				_b = _TraceId;
				_c = _Key;
				_d = _Value;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IxExceptionTraceReview_ValuePairs_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IxExceptionTraceReview_ValuePairs_Binding", IfxTraceCategory.Leave);
            }
		}

        public IxExceptionTraceReview_ValuePairs_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IxExceptionTraceReview_ValuePairs_Binding", IfxTraceCategory.Enter);
				_a = (Int32)data[0];                //  SortOrder
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);                 //  TraceId
				_c = (String)data[2];                //  Key
				_d = (String)data[3];                //  Value
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IxExceptionTraceReview_ValuePairs_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IxExceptionTraceReview_ValuePairs_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return _a;
        }

        public void Set_Int_Id(int value)
        {
            _a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return null;
        }

        public void Set_Guid_Id(Guid value)
        {
            //_a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Int32 Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region SortOrder

        private Int32 _a;
//        [DataMember]
//        public Int32 A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Int32 SortOrder
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion SortOrder


        #region TraceId

        private Guid? _b;
//        [DataMember]
//        public Guid? B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Guid? TraceId
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion TraceId


        #region Key

        private String _c;
//        [DataMember]
//        public String C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public String Key
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion Key


        #region Value

        private String _d;
//        [DataMember]
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String Value
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion Value


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3} ", SortOrder, TraceId, Key, Value );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3} ", SortOrder, TraceId, Key, Value );
        }

    }

}
