using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  11/17/2016 3:17:35 PM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcApplicationCulture_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcApplicationCulture_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcApplicationCulture_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcApplicationCulture_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcApplicationCulture_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationCulture_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcApplicationCulture_Values(currentData, this) : new WcApplicationCulture_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcApplicationCulture_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationCulture_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationCulture_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcApplicationCulture_Values _original;
        [DataMember]
        public WcApplicationCulture_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcApplicationCulture_Values _current;
        [DataMember]
        public WcApplicationCulture_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcApplicationCulture_Values _concurrent;
        [DataMember]
        public WcApplicationCulture_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  ApCltr_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  ApCltr_Ap_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  ApCltr_Sort
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  ApCltr_Cltr_Id
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  ApCltr_Cltr_Id_TextField
                //if (_current._e != _original._e)
                //{
                //    return true;
                //}

                //  ApCltr_Comment
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  ApCltr_IsDefault
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  ApCltr_IsActiveRow
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  ApCltr_IsDeleted
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  ApCltr_CreatedUserId
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  ApCltr_CreatedDate
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  ApCltr_UserId
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  UserName
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  ApCltr_LastModifiedDate
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  ApCltr_Stamp
                if (_current._o != _original._o)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{16}{0}{15}_b{16}{1}{15}_c{16}{2}{15}_d{16}{3}{15}_e{16}{4}{15}_f{16}{5}{15}_g{16}{6}{15}_h{16}{7}{15}_i{16}{8}{15}_j{16}{9}{15}_k{16}{10}{15}_l{16}{11}{15}_m{16}{12}{15}_n{16}{13}{15}_o{16}{14}",
				new object[] {
				_current._a,		  //ApCltr_Id
				_current._b,		  //ApCltr_Ap_Id
				_current._c,		  //ApCltr_Sort
				_current._d,		  //ApCltr_Cltr_Id
				_current._e,		  //ApCltr_Cltr_Id_TextField
				_current._f,		  //ApCltr_Comment
				_current._g,		  //ApCltr_IsDefault
				_current._h,		  //ApCltr_IsActiveRow
				_current._i,		  //ApCltr_IsDeleted
				_current._j,		  //ApCltr_CreatedUserId
				_current._k,		  //ApCltr_CreatedDate
				_current._l,		  //ApCltr_UserId
				_current._m,		  //UserName
				_current._n,		  //ApCltr_LastModifiedDate
				_current._o,		  //ApCltr_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcApplicationCulture_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcApplicationCulture_Values";
        private WcApplicationCulture_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcApplicationCulture_Values() 
        {
        }

        //public WcApplicationCulture_Values(object[] data, WcApplicationCulture_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcApplicationCulture_Values(object[] data, WcApplicationCulture_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationCulture_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  ApCltr_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  ApCltr_Ap_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  ApCltr_Sort
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  ApCltr_Cltr_Id
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  ApCltr_Cltr_Id_TextField
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  ApCltr_Comment
				_g = ObjectHelper.GetNullableBoolFromObjectValue(data[6]);					//  ApCltr_IsDefault
				_h = ObjectHelper.GetNullableBoolFromObjectValue(data[7]);					//  ApCltr_IsActiveRow
				_i = ObjectHelper.GetNullableBoolFromObjectValue(data[8]);					//  ApCltr_IsDeleted
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  ApCltr_CreatedUserId
				_k = ObjectHelper.GetNullableDateTimeFromObjectValue(data[10]);					//  ApCltr_CreatedDate
				_l = ObjectHelper.GetNullableGuidFromObjectValue(data[11]);						//  ApCltr_UserId
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  UserName
				_n = ObjectHelper.GetNullableDateTimeFromObjectValue(data[13]);					//  ApCltr_LastModifiedDate
				_o = ObjectHelper.GetByteArrayFromObjectValue(data[14]);						//  ApCltr_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationCulture_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplicationCulture_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplicationCulture", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  ApCltr_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  ApCltr_Ap_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  ApCltr_Sort
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  ApCltr_Cltr_Id
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  ApCltr_Cltr_Id_TextField
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  ApCltr_Comment
				_g = ObjectHelper.GetNullableBoolFromObjectValue(data[6]);					//  ApCltr_IsDefault
				_h = ObjectHelper.GetNullableBoolFromObjectValue(data[7]);					//  ApCltr_IsActiveRow
				_i = ObjectHelper.GetNullableBoolFromObjectValue(data[8]);					//  ApCltr_IsDeleted
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  ApCltr_CreatedUserId
				_k = ObjectHelper.GetNullableDateTimeFromObjectValue(data[10]);					//  ApCltr_CreatedDate
				_l = ObjectHelper.GetNullableGuidFromObjectValue(data[11]);						//  ApCltr_UserId
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  UserName
				_n = ObjectHelper.GetNullableDateTimeFromObjectValue(data[13]);					//  ApCltr_LastModifiedDate
				_o = ObjectHelper.GetByteArrayFromObjectValue(data[14]);						//  ApCltr_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplicationCulture", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplicationCulture", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  ApCltr_Id

		[DataMember]
		public Guid? _b;			//  ApCltr_Ap_Id

		[DataMember]
		public Int32? _c;			//  ApCltr_Sort

		[DataMember]
		public Int32? _d;			//  ApCltr_Cltr_Id

		[DataMember]
		public String _e;			//  ApCltr_Cltr_Id_TextField

		[DataMember]
		public String _f;			//  ApCltr_Comment

		[DataMember]
		public Boolean? _g;			//  ApCltr_IsDefault

		[DataMember]
		public Boolean? _h;			//  ApCltr_IsActiveRow

		[DataMember]
		public Boolean? _i;			//  ApCltr_IsDeleted

		[DataMember]
		public Guid? _j;			//  ApCltr_CreatedUserId

		[DataMember]
		public DateTime? _k;			//  ApCltr_CreatedDate

		[DataMember]
		public Guid? _l;			//  ApCltr_UserId

		[DataMember]
		public String _m;			//  UserName

		[DataMember]
		public DateTime? _n;			//  ApCltr_LastModifiedDate

		[DataMember]
		public Byte[] _o;			//  ApCltr_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcApplicationCulture_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 15; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                }
                return _list;
            }
        }

        public Guid ApCltr_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid ApCltr_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? ApCltr_Ap_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApCltr_Ap_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Int32? ApCltr_Sort
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? ApCltr_Sort_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public Int32? ApCltr_Cltr_Id
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? ApCltr_Cltr_Id_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String ApCltr_Cltr_Id_TextField
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApCltr_Cltr_Id_TextField_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public String ApCltr_Comment
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String ApCltr_Comment_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Boolean? ApCltr_IsDefault
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? ApCltr_IsDefault_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Boolean? ApCltr_IsActiveRow
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? ApCltr_IsActiveRow_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Boolean? ApCltr_IsDeleted
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? ApCltr_IsDeleted_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Guid? ApCltr_CreatedUserId
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApCltr_CreatedUserId_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public DateTime? ApCltr_CreatedDate
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? ApCltr_CreatedDate_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public Guid? ApCltr_UserId
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? ApCltr_UserId_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public String UserName
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public DateTime? ApCltr_LastModifiedDate
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? ApCltr_LastModifiedDate_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public Byte[] ApCltr_Stamp
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] ApCltr_Stamp_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o
			};
        }

        public WcApplicationCulture_Values Clone()
        {
            return new WcApplicationCulture_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


