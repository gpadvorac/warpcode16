using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/18/2018 4:29:16 PM

namespace EntityWireTypeSL
{



    public partial class WcCodeGen_Tables_Binding 
    {

        #region Initialize Variables
        
        List<WcCodeGen_TableColumn_Binding> _tableColumns = new List<WcCodeGen_TableColumn_Binding>();
        List<WcCodeGen_ChildTables_Binding> _childTables = new List<WcCodeGen_ChildTables_Binding>();
        List<WcCodeGen_TableSortBy_Binding> _tableSortBys = new List<WcCodeGen_TableSortBy_Binding>();


        #endregion Initialize Variables


        #region Constructors

        //public WcCodeGen_Tables_Binding() { }

        #endregion Constructors


        #region TableColumn

        public List<WcCodeGen_TableColumn_Binding> TableColumns
        {
            get { return _tableColumns; }
            set { _tableColumns = value; }
        }

        public void LoadTableColumns(object[] list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadTableColumns", IfxTraceCategory.Enter);
                _tableColumns.Clear();
                for (int i = 0; i < list.GetUpperBound(0) + 1; i++)
                {
                    _tableColumns.Add(new WcCodeGen_TableColumn_Binding((object[])list[i]));
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadTableColumns", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadTableColumns", IfxTraceCategory.Leave);
            }
        }

        public void LoadTableColumnsFromMasterList(List<WcCodeGen_TableColumn_Binding> list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadTableColumnsFromMasterList", IfxTraceCategory.Enter);
                _tableColumns.Clear();
                foreach (WcCodeGen_TableColumn_Binding obj in list)
                {
                    if (obj.TbC_Tb_Id == Tb_Id)
                    {
                        _tableColumns.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadTableColumnsFromMasterList", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadTableColumnsFromMasterList", IfxTraceCategory.Leave);
            }
        }
        
        #endregion TableColumn

        
        #region ChildTable

        public List<WcCodeGen_ChildTables_Binding> ChildTables
        {
            get { return _childTables; }
            set { _childTables = value; }
        }

        public void LoadChildTables(object[] list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildTables", IfxTraceCategory.Enter);
                _childTables.Clear();
                for (int i = 0; i < list.GetUpperBound(0) + 1; i++)
                {
                    _childTables.Add(new WcCodeGen_ChildTables_Binding((object[])list[i]));
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildTables", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildTables", IfxTraceCategory.Leave);
            }
        }

        public void LoadChildTablesFromMasterList(List<WcCodeGen_ChildTables_Binding> list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildTablesFromMasterList", IfxTraceCategory.Enter);
                _childTables.Clear();
                foreach (WcCodeGen_ChildTables_Binding obj in list)
                {
                    if (obj.TbChd_Parent_Tb_Id == Tb_Id)
                    {
                        _childTables.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildTablesFromMasterList", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildTablesFromMasterList", IfxTraceCategory.Leave);
            }
        }
        
        #endregion ChildTable

        
        #region TableSortBy

        public List<WcCodeGen_TableSortBy_Binding> TableSortBys
        {
            get { return _tableSortBys; }
            set { _tableSortBys = value; }
        }

        public void LoadTableSortBys(object[] list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadTableSortBys", IfxTraceCategory.Enter);
                _tableSortBys.Clear();
                for (int i = 0; i < list.GetUpperBound(0) + 1; i++)
                {
                    _tableSortBys.Add(new WcCodeGen_TableSortBy_Binding((object[])list[i]));
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadTableSortBys", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadTableSortBys", IfxTraceCategory.Leave);
            }
        }

        public void LoadTableSortBysFromMasterList(List<WcCodeGen_TableSortBy_Binding> list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadTableSortBysFromMasterList", IfxTraceCategory.Enter);
                _tableSortBys.Clear();
                foreach (WcCodeGen_TableSortBy_Binding obj in list)
                {
                    if (obj.TbSb_Tb_Id == Tb_Id)
                    {
                        _tableSortBys.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadTableSortBysFromMasterList", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadTableSortBysFromMasterList", IfxTraceCategory.Leave);
            }
        }
        
        #endregion TableSortBy
        

    }

}
