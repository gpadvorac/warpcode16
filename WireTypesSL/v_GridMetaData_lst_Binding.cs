using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

namespace EntityWireTypeSL
{


    [DataContract]
    public class v_GridMetaData_lst_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "CommonClientData";
        private static string _cn = "v_GridMetaData_lst_Binding";

        #endregion Initialize Variables


		#region Constructors

        public v_GridMetaData_lst_Binding() { }


        public v_GridMetaData_lst_Binding(Guid _v_GMD_Id, Int32 _v_GMD_GT_Id, String _v_GMD_Name, String _v_GMD_SprocName, Int32 _v_GMD_ColumnsHiden, Boolean _v_GMD_IsDefault, Int32 _v_GMD_Sort )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridMetaData_lst_Binding", IfxTraceCategory.Enter);
				_a = _v_GMD_Id;
				_b = _v_GMD_GT_Id;
				_c = _v_GMD_Name;
				_d = _v_GMD_SprocName;
				_e = _v_GMD_ColumnsHiden;
				_f = _v_GMD_IsDefault;
				_g = _v_GMD_Sort;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridMetaData_lst_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridMetaData_lst_Binding", IfxTraceCategory.Leave);
            }
		}

        public v_GridMetaData_lst_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridMetaData_lst_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  v_GMD_Id
				_b = (Int32)data[1];                //  v_GMD_GT_Id
				_c = (String)data[2];                //  v_GMD_Name
				_d = (String)data[3];                //  v_GMD_SprocName
				_e = (Int32)data[4];                //  v_GMD_ColumnsHiden
				_f = (Boolean)data[5];                //  v_GMD_IsDefault
				_g = (Int32)data[6];                //  v_GMD_Sort
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridMetaData_lst_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridMetaData_lst_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region v_GMD_Id

        private Guid _a;
//        [DataMember]
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid v_GMD_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion v_GMD_Id


        #region v_GMD_GT_Id

        private Int32 _b;
//        [DataMember]
//        public Int32 B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Int32 v_GMD_GT_Id
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion v_GMD_GT_Id


        #region v_GMD_Name

        private String _c;
//        [DataMember]
//        public String C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public String v_GMD_Name
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion v_GMD_Name


        #region v_GMD_SprocName

        private String _d;
//        [DataMember]
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String v_GMD_SprocName
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion v_GMD_SprocName


        #region v_GMD_ColumnsHiden

        private Int32 _e;
//        [DataMember]
//        public Int32 E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public Int32 v_GMD_ColumnsHiden
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion v_GMD_ColumnsHiden


        #region v_GMD_IsDefault

        private Boolean _f;
//        [DataMember]
//        public Boolean F
//        {
//            get { return _f; }
//            set { _f = value; }
//        }

        //[NonSerialized]
        public Boolean v_GMD_IsDefault
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion v_GMD_IsDefault


        #region v_GMD_Sort

        private Int32 _g;
//        [DataMember]
//        public Int32 G
//        {
//            get { return _g; }
//            set { _g = value; }
//        }

        //[NonSerialized]
        public Int32 v_GMD_Sort
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion v_GMD_Sort


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6} ", v_GMD_Id, v_GMD_GT_Id, v_GMD_Name, v_GMD_SprocName, v_GMD_ColumnsHiden, v_GMD_IsDefault, v_GMD_Sort );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6} ", v_GMD_Id, v_GMD_GT_Id, v_GMD_Name, v_GMD_SprocName, v_GMD_ColumnsHiden, v_GMD_IsDefault, v_GMD_Sort );
        }

    }

}
