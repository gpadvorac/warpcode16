using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  1/7/2018 10:04:19 PM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcTable_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTable_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcTable_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcTable_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcTable_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcTable_Values(currentData, this) : new WcTable_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcTable_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcTable_Values _original;
        [DataMember]
        public WcTable_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcTable_Values _current;
        [DataMember]
        public WcTable_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcTable_Values _concurrent;
        [DataMember]
        public WcTable_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  Tb_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  Tb_ApVrsn_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  Tb_auditTable_Id
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  AttachmentCount
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  AttachmentFileNames
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  DiscussionCount
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  DiscussionTitles
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  IsImported
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  Tb_Name
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  TableInDatabase
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  Tb_EntityRootName
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  Tb_VariableName
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  Tb_ScreenCaption
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  Tb_Description
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  Tb_DevelopmentNote
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  Tb_UseLegacyConnectionCode
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  Tb_DbCnSK_Id
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  Tb_DbCnSK_Id_TextField
                //if (_current._r != _original._r)
                //{
                //    return true;
                //}

                //  Tb_ApDbScm_Id
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  Tb_ApDbScm_Id_TextField
                //if (_current._t != _original._t)
                //{
                //    return true;
                //}

                //  Tb_UIAssemblyName
                if (_current._u != _original._u)
                {
                    return true;
                }

                //  Tb_UINamespace
                if (_current._v != _original._v)
                {
                    return true;
                }

                //  Tb_UIAssemblyPath
                if (_current._w != _original._w)
                {
                    return true;
                }

                //  Tb_ProxyAssemblyName
                if (_current._x != _original._x)
                {
                    return true;
                }

                //  Tb_ProxyNamespace
                if (_current._y != _original._y)
                {
                    return true;
                }

                //  Tb_ProxyAssemblyPath
                if (_current._z != _original._z)
                {
                    return true;
                }

                //  Tb_WireTypeAssemblyName
                if (_current._aa != _original._aa)
                {
                    return true;
                }

                //  Tb_WireTypeNamespace
                if (_current._ab != _original._ab)
                {
                    return true;
                }

                //  Tb_WireTypePath
                if (_current._ac != _original._ac)
                {
                    return true;
                }

                //  Tb_WebServiceName
                if (_current._ad != _original._ad)
                {
                    return true;
                }

                //  Tb_WebServiceFolder
                if (_current._ae != _original._ae)
                {
                    return true;
                }

                //  Tb_DataServiceAssemblyName
                if (_current._af != _original._af)
                {
                    return true;
                }

                //  Tb_DataServiceNamespace
                if (_current._ag != _original._ag)
                {
                    return true;
                }

                //  Tb_DataServicePath
                if (_current._ah != _original._ah)
                {
                    return true;
                }

                //  Tb_UseTilesInPropsScreen
                if (_current._ai != _original._ai)
                {
                    return true;
                }

                //  Tb_UseGridColumnGroups
                if (_current._aj != _original._aj)
                {
                    return true;
                }

                //  Tb_UseGridDataSourceCombo
                if (_current._ak != _original._ak)
                {
                    return true;
                }

                //  Tb_PkIsIdentity
                if (_current._al != _original._al)
                {
                    return true;
                }

                //  Tb_IsVirtual
                if (_current._am != _original._am)
                {
                    return true;
                }

                //  Tb_IsScreenPlaceHolder
                if (_current._an != _original._an)
                {
                    return true;
                }

                //  Tb_IsNotEntity
                if (_current._ao != _original._ao)
                {
                    return true;
                }

                //  Tb_IsMany2Many
                if (_current._ap != _original._ap)
                {
                    return true;
                }

                //  Tb_UseLastModifiedByUserNameInSproc
                if (_current._aq != _original._aq)
                {
                    return true;
                }

                //  Tb_UseUserTimeStamp
                if (_current._ar != _original._ar)
                {
                    return true;
                }

                //  Tb_UseForAudit
                if (_current._asxx != _original._asxx)
                {
                    return true;
                }

                //  Tb_IsAllowDelete
                if (_current._at != _original._at)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_MenuRow_IsVisible
                if (_current._au != _original._au)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_GridTools_IsVisible
                if (_current._av != _original._av)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_SplitScreen_IsVisible
                if (_current._aw != _original._aw)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
                if (_current._ax != _original._ax)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_NavColumnWidth
                if (_current._ay != _original._ay)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_IsReadOnly
                if (_current._az != _original._az)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_IsAllowNewRow
                if (_current._ba != _original._ba)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_ExcelExport_IsVisible
                if (_current._bb != _original._bb)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_ColumnChooser_IsVisible
                if (_current._bc != _original._bc)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
                if (_current._bd != _original._bd)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_RefreshGrid_IsVisible
                if (_current._be != _original._be)
                {
                    return true;
                }

                //  Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
                if (_current._bf != _original._bf)
                {
                    return true;
                }

                //  Tb_IsInputComplete
                if (_current._bg != _original._bg)
                {
                    return true;
                }

                //  Tb_IsCodeGen
                if (_current._bh != _original._bh)
                {
                    return true;
                }

                //  Tb_IsReadyCodeGen
                if (_current._bi != _original._bi)
                {
                    return true;
                }

                //  Tb_IsCodeGenComplete
                if (_current._bj != _original._bj)
                {
                    return true;
                }

                //  Tb_IsTagForCodeGen
                if (_current._bk != _original._bk)
                {
                    return true;
                }

                //  Tb_IsTagForOther
                if (_current._bl != _original._bl)
                {
                    return true;
                }

                //  Tb_TableGroups
                if (_current._bm != _original._bm)
                {
                    return true;
                }

                //  Tb_IsActiveRow
                if (_current._bn != _original._bn)
                {
                    return true;
                }

                //  Tb_IsDeleted
                if (_current._bo != _original._bo)
                {
                    return true;
                }

                //  Tb_CreatedUserId
                if (_current._bp != _original._bp)
                {
                    return true;
                }

                //  Tb_CreatedDate
                if (_current._bq != _original._bq)
                {
                    return true;
                }

                //  Tb_UserId
                if (_current._br != _original._br)
                {
                    return true;
                }

                //  UserName
                if (_current._bs != _original._bs)
                {
                    return true;
                }

                //  Tb_LastModifiedDate
                if (_current._bt != _original._bt)
                {
                    return true;
                }

                //  Tb_Stamp
                if (_current._bu != _original._bu)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{75}{0}{74}_b{75}{1}{74}_c{75}{2}{74}_d{75}{3}{74}_e{75}{4}{74}_f{75}{5}{74}_g{75}{6}{74}_h{75}{7}{74}_i{75}{8}{74}_j{75}{9}{74}_k{75}{10}{74}_l{75}{11}{74}_m{75}{12}{74}_n{75}{13}{74}_o{75}{14}{74}_p{75}{15}{74}_q{75}{16}{74}_r{75}{17}{74}_s{75}{18}{74}_t{75}{19}{74}_u{75}{20}{74}_v{75}{21}{74}_w{75}{22}{74}_x{75}{23}{74}_y{75}{24}{74}_z{75}{25}{74}_aa{75}{26}{74}_ab{75}{27}{74}_ac{75}{28}{74}_ad{75}{29}{74}_ae{75}{30}{74}_af{75}{31}{74}_ag{75}{32}{74}_ah{75}{33}{74}_ai{75}{34}{74}_aj{75}{35}{74}_ak{75}{36}{74}_al{75}{37}{74}_am{75}{38}{74}_an{75}{39}{74}_ao{75}{40}{74}_ap{75}{41}{74}_aq{75}{42}{74}_ar{75}{43}{74}_asxx{75}{44}{74}_at{75}{45}{74}_au{75}{46}{74}_av{75}{47}{74}_aw{75}{48}{74}_ax{75}{49}{74}_ay{75}{50}{74}_az{75}{51}{74}_ba{75}{52}{74}_bb{75}{53}{74}_bc{75}{54}{74}_bd{75}{55}{74}_be{75}{56}{74}_bf{75}{57}{74}_bg{75}{58}{74}_bh{75}{59}{74}_bi{75}{60}{74}_bj{75}{61}{74}_bk{75}{62}{74}_bl{75}{63}{74}_bm{75}{64}{74}_bn{75}{65}{74}_bo{75}{66}{74}_bp{75}{67}{74}_bq{75}{68}{74}_br{75}{69}{74}_bs{75}{70}{74}_bt{75}{71}{74}_bu{75}{72}{74}",
				new object[] {
				_current._a,		  //Tb_Id
				_current._b,		  //Tb_ApVrsn_Id
				_current._c,		  //Tb_auditTable_Id
				_current._d,		  //AttachmentCount
				_current._e,		  //AttachmentFileNames
				_current._f,		  //DiscussionCount
				_current._g,		  //DiscussionTitles
				_current._h,		  //IsImported
				_current._i,		  //Tb_Name
				_current._j,		  //TableInDatabase
				_current._k,		  //Tb_EntityRootName
				_current._l,		  //Tb_VariableName
				_current._m,		  //Tb_ScreenCaption
				_current._n,		  //Tb_Description
				_current._o,		  //Tb_DevelopmentNote
				_current._p,		  //Tb_UseLegacyConnectionCode
				_current._q,		  //Tb_DbCnSK_Id
				_current._r,		  //Tb_DbCnSK_Id_TextField
				_current._s,		  //Tb_ApDbScm_Id
				_current._t,		  //Tb_ApDbScm_Id_TextField
				_current._u,		  //Tb_UIAssemblyName
				_current._v,		  //Tb_UINamespace
				_current._w,		  //Tb_UIAssemblyPath
				_current._x,		  //Tb_ProxyAssemblyName
				_current._y,		  //Tb_ProxyNamespace
				_current._z,		  //Tb_ProxyAssemblyPath
				_current._aa,		  //Tb_WireTypeAssemblyName
				_current._ab,		  //Tb_WireTypeNamespace
				_current._ac,		  //Tb_WireTypePath
				_current._ad,		  //Tb_WebServiceName
				_current._ae,		  //Tb_WebServiceFolder
				_current._af,		  //Tb_DataServiceAssemblyName
				_current._ag,		  //Tb_DataServiceNamespace
				_current._ah,		  //Tb_DataServicePath
				_current._ai,		  //Tb_UseTilesInPropsScreen
				_current._aj,		  //Tb_UseGridColumnGroups
				_current._ak,		  //Tb_UseGridDataSourceCombo
				_current._al,		  //Tb_PkIsIdentity
				_current._am,		  //Tb_IsVirtual
				_current._an,		  //Tb_IsScreenPlaceHolder
				_current._ao,		  //Tb_IsNotEntity
				_current._ap,		  //Tb_IsMany2Many
				_current._aq,		  //Tb_UseLastModifiedByUserNameInSproc
				_current._ar,		  //Tb_UseUserTimeStamp
				_current._asxx,		  //Tb_UseForAudit
				_current._at,		  //Tb_IsAllowDelete
				_current._au,		  //Tb_CnfgGdMnu_MenuRow_IsVisible
				_current._av,		  //Tb_CnfgGdMnu_GridTools_IsVisible
				_current._aw,		  //Tb_CnfgGdMnu_SplitScreen_IsVisible
				_current._ax,		  //Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
				_current._ay,		  //Tb_CnfgGdMnu_NavColumnWidth
				_current._az,		  //Tb_CnfgGdMnu_IsReadOnly
				_current._ba,		  //Tb_CnfgGdMnu_IsAllowNewRow
				_current._bb,		  //Tb_CnfgGdMnu_ExcelExport_IsVisible
				_current._bc,		  //Tb_CnfgGdMnu_ColumnChooser_IsVisible
				_current._bd,		  //Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
				_current._be,		  //Tb_CnfgGdMnu_RefreshGrid_IsVisible
				_current._bf,		  //Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
				_current._bg,		  //Tb_IsInputComplete
				_current._bh,		  //Tb_IsCodeGen
				_current._bi,		  //Tb_IsReadyCodeGen
				_current._bj,		  //Tb_IsCodeGenComplete
				_current._bk,		  //Tb_IsTagForCodeGen
				_current._bl,		  //Tb_IsTagForOther
				_current._bm,		  //Tb_TableGroups
				_current._bn,		  //Tb_IsActiveRow
				_current._bo,		  //Tb_IsDeleted
				_current._bp,		  //Tb_CreatedUserId
				_current._bq,		  //Tb_CreatedDate
				_current._br,		  //Tb_UserId
				_current._bs,		  //UserName
				_current._bt,		  //Tb_LastModifiedDate
				_current._bu,		  //Tb_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcTable_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTable_Values";
        private WcTable_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcTable_Values() 
        {
        }

        //public WcTable_Values(object[] data, WcTable_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcTable_Values(object[] data, WcTable_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Tb_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Tb_ApVrsn_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  Tb_auditTable_Id
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  AttachmentCount
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  AttachmentFileNames
				_f = ObjectHelper.GetNullableIntFromObjectValue(data[5]);							//  DiscussionCount
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  DiscussionTitles
				_h = ObjectHelper.GetBoolFromObjectValue(data[7]);									//  IsImported
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  Tb_Name
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  TableInDatabase
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  Tb_EntityRootName
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  Tb_VariableName
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  Tb_ScreenCaption
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  Tb_Description
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  Tb_DevelopmentNote
				_p = ObjectHelper.GetBoolFromObjectValue(data[15]);									//  Tb_UseLegacyConnectionCode
				_q = ObjectHelper.GetNullableGuidFromObjectValue(data[16]);						//  Tb_DbCnSK_Id
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  Tb_DbCnSK_Id_TextField
				_s = ObjectHelper.GetNullableGuidFromObjectValue(data[18]);						//  Tb_ApDbScm_Id
				_t = ObjectHelper.GetStringFromObjectValue(data[19]);									//  Tb_ApDbScm_Id_TextField
				_u = ObjectHelper.GetStringFromObjectValue(data[20]);									//  Tb_UIAssemblyName
				_v = ObjectHelper.GetStringFromObjectValue(data[21]);									//  Tb_UINamespace
				_w = ObjectHelper.GetStringFromObjectValue(data[22]);									//  Tb_UIAssemblyPath
				_x = ObjectHelper.GetStringFromObjectValue(data[23]);									//  Tb_ProxyAssemblyName
				_y = ObjectHelper.GetStringFromObjectValue(data[24]);									//  Tb_ProxyNamespace
				_z = ObjectHelper.GetStringFromObjectValue(data[25]);									//  Tb_ProxyAssemblyPath
				_aa = ObjectHelper.GetStringFromObjectValue(data[26]);									//  Tb_WireTypeAssemblyName
				_ab = ObjectHelper.GetStringFromObjectValue(data[27]);									//  Tb_WireTypeNamespace
				_ac = ObjectHelper.GetStringFromObjectValue(data[28]);									//  Tb_WireTypePath
				_ad = ObjectHelper.GetStringFromObjectValue(data[29]);									//  Tb_WebServiceName
				_ae = ObjectHelper.GetStringFromObjectValue(data[30]);									//  Tb_WebServiceFolder
				_af = ObjectHelper.GetStringFromObjectValue(data[31]);									//  Tb_DataServiceAssemblyName
				_ag = ObjectHelper.GetStringFromObjectValue(data[32]);									//  Tb_DataServiceNamespace
				_ah = ObjectHelper.GetStringFromObjectValue(data[33]);									//  Tb_DataServicePath
				_ai = ObjectHelper.GetBoolFromObjectValue(data[34]);									//  Tb_UseTilesInPropsScreen
				_aj = ObjectHelper.GetBoolFromObjectValue(data[35]);									//  Tb_UseGridColumnGroups
				_ak = ObjectHelper.GetBoolFromObjectValue(data[36]);									//  Tb_UseGridDataSourceCombo
				_al = ObjectHelper.GetBoolFromObjectValue(data[37]);									//  Tb_PkIsIdentity
				_am = ObjectHelper.GetBoolFromObjectValue(data[38]);									//  Tb_IsVirtual
				_an = ObjectHelper.GetNullableBoolFromObjectValue(data[39]);					//  Tb_IsScreenPlaceHolder
				_ao = ObjectHelper.GetBoolFromObjectValue(data[40]);									//  Tb_IsNotEntity
				_ap = ObjectHelper.GetBoolFromObjectValue(data[41]);									//  Tb_IsMany2Many
				_aq = ObjectHelper.GetBoolFromObjectValue(data[42]);									//  Tb_UseLastModifiedByUserNameInSproc
				_ar = ObjectHelper.GetBoolFromObjectValue(data[43]);									//  Tb_UseUserTimeStamp
				_asxx = ObjectHelper.GetBoolFromObjectValue(data[44]);									//  Tb_UseForAudit
				_at = ObjectHelper.GetNullableBoolFromObjectValue(data[45]);					//  Tb_IsAllowDelete
				_au = ObjectHelper.GetBoolFromObjectValue(data[46]);									//  Tb_CnfgGdMnu_MenuRow_IsVisible
				_av = ObjectHelper.GetBoolFromObjectValue(data[47]);									//  Tb_CnfgGdMnu_GridTools_IsVisible
				_aw = ObjectHelper.GetBoolFromObjectValue(data[48]);									//  Tb_CnfgGdMnu_SplitScreen_IsVisible
				_ax = ObjectHelper.GetBoolFromObjectValue(data[49]);									//  Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
				_ay = ObjectHelper.GetIntFromObjectValue(data[50]);										//  Tb_CnfgGdMnu_NavColumnWidth
				_az = ObjectHelper.GetBoolFromObjectValue(data[51]);									//  Tb_CnfgGdMnu_IsReadOnly
				_ba = ObjectHelper.GetBoolFromObjectValue(data[52]);									//  Tb_CnfgGdMnu_IsAllowNewRow
				_bb = ObjectHelper.GetBoolFromObjectValue(data[53]);									//  Tb_CnfgGdMnu_ExcelExport_IsVisible
				_bc = ObjectHelper.GetBoolFromObjectValue(data[54]);									//  Tb_CnfgGdMnu_ColumnChooser_IsVisible
				_bd = ObjectHelper.GetBoolFromObjectValue(data[55]);									//  Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
				_be = ObjectHelper.GetBoolFromObjectValue(data[56]);									//  Tb_CnfgGdMnu_RefreshGrid_IsVisible
				_bf = ObjectHelper.GetBoolFromObjectValue(data[57]);									//  Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
				_bg = ObjectHelper.GetBoolFromObjectValue(data[58]);									//  Tb_IsInputComplete
				_bh = ObjectHelper.GetBoolFromObjectValue(data[59]);									//  Tb_IsCodeGen
				_bi = ObjectHelper.GetBoolFromObjectValue(data[60]);									//  Tb_IsReadyCodeGen
				_bj = ObjectHelper.GetBoolFromObjectValue(data[61]);									//  Tb_IsCodeGenComplete
				_bk = ObjectHelper.GetBoolFromObjectValue(data[62]);									//  Tb_IsTagForCodeGen
				_bl = ObjectHelper.GetBoolFromObjectValue(data[63]);									//  Tb_IsTagForOther
				_bm = ObjectHelper.GetStringFromObjectValue(data[64]);									//  Tb_TableGroups
				_bn = ObjectHelper.GetBoolFromObjectValue(data[65]);									//  Tb_IsActiveRow
				_bo = ObjectHelper.GetBoolFromObjectValue(data[66]);									//  Tb_IsDeleted
				_bp = ObjectHelper.GetNullableGuidFromObjectValue(data[67]);						//  Tb_CreatedUserId
				_bq = ObjectHelper.GetNullableDateTimeFromObjectValue(data[68]);					//  Tb_CreatedDate
				_br = ObjectHelper.GetNullableGuidFromObjectValue(data[69]);						//  Tb_UserId
				_bs = ObjectHelper.GetStringFromObjectValue(data[70]);									//  UserName
				_bt = ObjectHelper.GetNullableDateTimeFromObjectValue(data[71]);					//  Tb_LastModifiedDate
				_bu = data[72] as Byte[];						//  Tb_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTable", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Tb_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Tb_ApVrsn_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  Tb_auditTable_Id
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  AttachmentCount
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  AttachmentFileNames
				_f = ObjectHelper.GetNullableIntFromObjectValue(data[5]);							//  DiscussionCount
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  DiscussionTitles
				_h = ObjectHelper.GetBoolFromObjectValue(data[7]);									//  IsImported
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  Tb_Name
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);									//  TableInDatabase
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  Tb_EntityRootName
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  Tb_VariableName
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  Tb_ScreenCaption
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  Tb_Description
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  Tb_DevelopmentNote
				_p = ObjectHelper.GetBoolFromObjectValue(data[15]);									//  Tb_UseLegacyConnectionCode
				_q = ObjectHelper.GetNullableGuidFromObjectValue(data[16]);						//  Tb_DbCnSK_Id
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  Tb_DbCnSK_Id_TextField
				_s = ObjectHelper.GetNullableGuidFromObjectValue(data[18]);						//  Tb_ApDbScm_Id
				_t = ObjectHelper.GetStringFromObjectValue(data[19]);									//  Tb_ApDbScm_Id_TextField
				_u = ObjectHelper.GetStringFromObjectValue(data[20]);									//  Tb_UIAssemblyName
				_v = ObjectHelper.GetStringFromObjectValue(data[21]);									//  Tb_UINamespace
				_w = ObjectHelper.GetStringFromObjectValue(data[22]);									//  Tb_UIAssemblyPath
				_x = ObjectHelper.GetStringFromObjectValue(data[23]);									//  Tb_ProxyAssemblyName
				_y = ObjectHelper.GetStringFromObjectValue(data[24]);									//  Tb_ProxyNamespace
				_z = ObjectHelper.GetStringFromObjectValue(data[25]);									//  Tb_ProxyAssemblyPath
				_aa = ObjectHelper.GetStringFromObjectValue(data[26]);									//  Tb_WireTypeAssemblyName
				_ab = ObjectHelper.GetStringFromObjectValue(data[27]);									//  Tb_WireTypeNamespace
				_ac = ObjectHelper.GetStringFromObjectValue(data[28]);									//  Tb_WireTypePath
				_ad = ObjectHelper.GetStringFromObjectValue(data[29]);									//  Tb_WebServiceName
				_ae = ObjectHelper.GetStringFromObjectValue(data[30]);									//  Tb_WebServiceFolder
				_af = ObjectHelper.GetStringFromObjectValue(data[31]);									//  Tb_DataServiceAssemblyName
				_ag = ObjectHelper.GetStringFromObjectValue(data[32]);									//  Tb_DataServiceNamespace
				_ah = ObjectHelper.GetStringFromObjectValue(data[33]);									//  Tb_DataServicePath
				_ai = ObjectHelper.GetBoolFromObjectValue(data[34]);									//  Tb_UseTilesInPropsScreen
				_aj = ObjectHelper.GetBoolFromObjectValue(data[35]);									//  Tb_UseGridColumnGroups
				_ak = ObjectHelper.GetBoolFromObjectValue(data[36]);									//  Tb_UseGridDataSourceCombo
				_al = ObjectHelper.GetBoolFromObjectValue(data[37]);									//  Tb_PkIsIdentity
				_am = ObjectHelper.GetBoolFromObjectValue(data[38]);									//  Tb_IsVirtual
				_an = ObjectHelper.GetNullableBoolFromObjectValue(data[39]);					//  Tb_IsScreenPlaceHolder
				_ao = ObjectHelper.GetBoolFromObjectValue(data[40]);									//  Tb_IsNotEntity
				_ap = ObjectHelper.GetBoolFromObjectValue(data[41]);									//  Tb_IsMany2Many
				_aq = ObjectHelper.GetBoolFromObjectValue(data[42]);									//  Tb_UseLastModifiedByUserNameInSproc
				_ar = ObjectHelper.GetBoolFromObjectValue(data[43]);									//  Tb_UseUserTimeStamp
				_asxx = ObjectHelper.GetBoolFromObjectValue(data[44]);									//  Tb_UseForAudit
				_at = ObjectHelper.GetNullableBoolFromObjectValue(data[45]);					//  Tb_IsAllowDelete
				_au = ObjectHelper.GetBoolFromObjectValue(data[46]);									//  Tb_CnfgGdMnu_MenuRow_IsVisible
				_av = ObjectHelper.GetBoolFromObjectValue(data[47]);									//  Tb_CnfgGdMnu_GridTools_IsVisible
				_aw = ObjectHelper.GetBoolFromObjectValue(data[48]);									//  Tb_CnfgGdMnu_SplitScreen_IsVisible
				_ax = ObjectHelper.GetBoolFromObjectValue(data[49]);									//  Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
				_ay = ObjectHelper.GetIntFromObjectValue(data[50]);										//  Tb_CnfgGdMnu_NavColumnWidth
				_az = ObjectHelper.GetBoolFromObjectValue(data[51]);									//  Tb_CnfgGdMnu_IsReadOnly
				_ba = ObjectHelper.GetBoolFromObjectValue(data[52]);									//  Tb_CnfgGdMnu_IsAllowNewRow
				_bb = ObjectHelper.GetBoolFromObjectValue(data[53]);									//  Tb_CnfgGdMnu_ExcelExport_IsVisible
				_bc = ObjectHelper.GetBoolFromObjectValue(data[54]);									//  Tb_CnfgGdMnu_ColumnChooser_IsVisible
				_bd = ObjectHelper.GetBoolFromObjectValue(data[55]);									//  Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
				_be = ObjectHelper.GetBoolFromObjectValue(data[56]);									//  Tb_CnfgGdMnu_RefreshGrid_IsVisible
				_bf = ObjectHelper.GetBoolFromObjectValue(data[57]);									//  Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
				_bg = ObjectHelper.GetBoolFromObjectValue(data[58]);									//  Tb_IsInputComplete
				_bh = ObjectHelper.GetBoolFromObjectValue(data[59]);									//  Tb_IsCodeGen
				_bi = ObjectHelper.GetBoolFromObjectValue(data[60]);									//  Tb_IsReadyCodeGen
				_bj = ObjectHelper.GetBoolFromObjectValue(data[61]);									//  Tb_IsCodeGenComplete
				_bk = ObjectHelper.GetBoolFromObjectValue(data[62]);									//  Tb_IsTagForCodeGen
				_bl = ObjectHelper.GetBoolFromObjectValue(data[63]);									//  Tb_IsTagForOther
				_bm = ObjectHelper.GetStringFromObjectValue(data[64]);									//  Tb_TableGroups
				_bn = ObjectHelper.GetBoolFromObjectValue(data[65]);									//  Tb_IsActiveRow
				_bo = ObjectHelper.GetBoolFromObjectValue(data[66]);									//  Tb_IsDeleted
				_bp = ObjectHelper.GetNullableGuidFromObjectValue(data[67]);						//  Tb_CreatedUserId
				_bq = ObjectHelper.GetNullableDateTimeFromObjectValue(data[68]);					//  Tb_CreatedDate
				_br = ObjectHelper.GetNullableGuidFromObjectValue(data[69]);						//  Tb_UserId
				_bs = ObjectHelper.GetStringFromObjectValue(data[70]);									//  UserName
				_bt = ObjectHelper.GetNullableDateTimeFromObjectValue(data[71]);					//  Tb_LastModifiedDate
				_bu = data[72] as Byte[];						//  Tb_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTable", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTable", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  Tb_Id

		[DataMember]
		public Guid? _b;			//  Tb_ApVrsn_Id

		[DataMember]
		public Guid? _c;			//  Tb_auditTable_Id

		[DataMember]
		public Int32? _d;			//  AttachmentCount

		[DataMember]
		public String _e;			//  AttachmentFileNames

		[DataMember]
		public Int32? _f;			//  DiscussionCount

		[DataMember]
		public String _g;			//  DiscussionTitles

		[DataMember]
		public Boolean _h;			//  IsImported

		[DataMember]
		public String _i;			//  Tb_Name

		[DataMember]
		public String _j;			//  TableInDatabase

		[DataMember]
		public String _k;			//  Tb_EntityRootName

		[DataMember]
		public String _l;			//  Tb_VariableName

		[DataMember]
		public String _m;			//  Tb_ScreenCaption

		[DataMember]
		public String _n;			//  Tb_Description

		[DataMember]
		public String _o;			//  Tb_DevelopmentNote

		[DataMember]
		public Boolean _p;			//  Tb_UseLegacyConnectionCode

		[DataMember]
		public Guid? _q;			//  Tb_DbCnSK_Id

		[DataMember]
		public String _r;			//  Tb_DbCnSK_Id_TextField

		[DataMember]
		public Guid? _s;			//  Tb_ApDbScm_Id

		[DataMember]
		public String _t;			//  Tb_ApDbScm_Id_TextField

		[DataMember]
		public String _u;			//  Tb_UIAssemblyName

		[DataMember]
		public String _v;			//  Tb_UINamespace

		[DataMember]
		public String _w;			//  Tb_UIAssemblyPath

		[DataMember]
		public String _x;			//  Tb_ProxyAssemblyName

		[DataMember]
		public String _y;			//  Tb_ProxyNamespace

		[DataMember]
		public String _z;			//  Tb_ProxyAssemblyPath

		[DataMember]
		public String _aa;			//  Tb_WireTypeAssemblyName

		[DataMember]
		public String _ab;			//  Tb_WireTypeNamespace

		[DataMember]
		public String _ac;			//  Tb_WireTypePath

		[DataMember]
		public String _ad;			//  Tb_WebServiceName

		[DataMember]
		public String _ae;			//  Tb_WebServiceFolder

		[DataMember]
		public String _af;			//  Tb_DataServiceAssemblyName

		[DataMember]
		public String _ag;			//  Tb_DataServiceNamespace

		[DataMember]
		public String _ah;			//  Tb_DataServicePath

		[DataMember]
		public Boolean _ai;			//  Tb_UseTilesInPropsScreen

		[DataMember]
		public Boolean _aj;			//  Tb_UseGridColumnGroups

		[DataMember]
		public Boolean _ak;			//  Tb_UseGridDataSourceCombo

		[DataMember]
		public Boolean _al;			//  Tb_PkIsIdentity

		[DataMember]
		public Boolean _am;			//  Tb_IsVirtual

		[DataMember]
		public Boolean? _an;			//  Tb_IsScreenPlaceHolder

		[DataMember]
		public Boolean _ao;			//  Tb_IsNotEntity

		[DataMember]
		public Boolean _ap;			//  Tb_IsMany2Many

		[DataMember]
		public Boolean _aq;			//  Tb_UseLastModifiedByUserNameInSproc

		[DataMember]
		public Boolean _ar;			//  Tb_UseUserTimeStamp

		[DataMember]
		public Boolean _asxx;			//  Tb_UseForAudit

		[DataMember]
		public Boolean? _at;			//  Tb_IsAllowDelete

		[DataMember]
		public Boolean _au;			//  Tb_CnfgGdMnu_MenuRow_IsVisible

		[DataMember]
		public Boolean _av;			//  Tb_CnfgGdMnu_GridTools_IsVisible

		[DataMember]
		public Boolean _aw;			//  Tb_CnfgGdMnu_SplitScreen_IsVisible

		[DataMember]
		public Boolean _ax;			//  Tb_CnfgGdMnu_SplitScreen_IsSplit_Default

		[DataMember]
		public Int32 _ay;			//  Tb_CnfgGdMnu_NavColumnWidth

		[DataMember]
		public Boolean _az;			//  Tb_CnfgGdMnu_IsReadOnly

		[DataMember]
		public Boolean _ba;			//  Tb_CnfgGdMnu_IsAllowNewRow

		[DataMember]
		public Boolean _bb;			//  Tb_CnfgGdMnu_ExcelExport_IsVisible

		[DataMember]
		public Boolean _bc;			//  Tb_CnfgGdMnu_ColumnChooser_IsVisible

		[DataMember]
		public Boolean _bd;			//  Tb_CnfgGdMnu_ShowHideColBtn_IsVisible

		[DataMember]
		public Boolean _be;			//  Tb_CnfgGdMnu_RefreshGrid_IsVisible

		[DataMember]
		public Boolean _bf;			//  Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible

		[DataMember]
		public Boolean _bg;			//  Tb_IsInputComplete

		[DataMember]
		public Boolean _bh;			//  Tb_IsCodeGen

		[DataMember]
		public Boolean _bi;			//  Tb_IsReadyCodeGen

		[DataMember]
		public Boolean _bj;			//  Tb_IsCodeGenComplete

		[DataMember]
		public Boolean _bk;			//  Tb_IsTagForCodeGen

		[DataMember]
		public Boolean _bl;			//  Tb_IsTagForOther

		[DataMember]
		public String _bm;			//  Tb_TableGroups

		[DataMember]
		public Boolean _bn;			//  Tb_IsActiveRow

		[DataMember]
		public Boolean _bo;			//  Tb_IsDeleted

		[DataMember]
		public Guid? _bp;			//  Tb_CreatedUserId

		[DataMember]
		public DateTime? _bq;			//  Tb_CreatedDate

		[DataMember]
		public Guid? _br;			//  Tb_UserId

		[DataMember]
		public String _bs;			//  UserName

		[DataMember]
		public DateTime? _bt;			//  Tb_LastModifiedDate

		[DataMember]
		public Byte[] _bu;			//  Tb_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcTable_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 74; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                    _list[18] = _s;
                    _list[19] = _t;
                    _list[20] = _u;
                    _list[21] = _v;
                    _list[22] = _w;
                    _list[23] = _x;
                    _list[24] = _y;
                    _list[25] = _z;
                    _list[26] = _aa;
                    _list[27] = _ab;
                    _list[28] = _ac;
                    _list[29] = _ad;
                    _list[30] = _ae;
                    _list[31] = _af;
                    _list[32] = _ag;
                    _list[33] = _ah;
                    _list[34] = _ai;
                    _list[35] = _aj;
                    _list[36] = _ak;
                    _list[37] = _al;
                    _list[38] = _am;
                    _list[39] = _an;
                    _list[40] = _ao;
                    _list[41] = _ap;
                    _list[42] = _aq;
                    _list[43] = _ar;
                    _list[44] = _asxx;
                    _list[45] = _at;
                    _list[46] = _au;
                    _list[47] = _av;
                    _list[48] = _aw;
                    _list[49] = _ax;
                    _list[50] = _ay;
                    _list[51] = _az;
                    _list[52] = _ba;
                    _list[53] = _bb;
                    _list[54] = _bc;
                    _list[55] = _bd;
                    _list[56] = _be;
                    _list[57] = _bf;
                    _list[58] = _bg;
                    _list[59] = _bh;
                    _list[60] = _bi;
                    _list[61] = _bj;
                    _list[62] = _bk;
                    _list[63] = _bl;
                    _list[64] = _bm;
                    _list[65] = _bn;
                    _list[66] = _bo;
                    _list[67] = _bp;
                    _list[68] = _bq;
                    _list[69] = _br;
                    _list[70] = _bs;
                    _list[71] = _bt;
                    _list[72] = _bu;
                }
                return _list;
            }
        }

        public Guid Tb_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid Tb_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? Tb_ApVrsn_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_ApVrsn_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Guid? Tb_auditTable_Id
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_auditTable_Id_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public Int32? AttachmentCount
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? AttachmentCount_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String AttachmentFileNames
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String AttachmentFileNames_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Int32? DiscussionCount
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? DiscussionCount_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public String DiscussionTitles
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DiscussionTitles_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Boolean IsImported
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean IsImported_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public String Tb_Name
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_Name_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public String TableInDatabase
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TableInDatabase_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public String Tb_EntityRootName
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_EntityRootName_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public String Tb_VariableName
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_VariableName_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public String Tb_ScreenCaption
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_ScreenCaption_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public String Tb_Description
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_Description_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public String Tb_DevelopmentNote
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_DevelopmentNote_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public Boolean Tb_UseLegacyConnectionCode
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseLegacyConnectionCode_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public Guid? Tb_DbCnSK_Id
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_DbCnSK_Id_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public String Tb_DbCnSK_Id_TextField
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_DbCnSK_Id_TextField_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        public Guid? Tb_ApDbScm_Id
        {
            get { return _s; }
            set
            {
                _s = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_ApDbScm_Id_noevents
        {
            get { return _s; }
            set
            {
                _s = value;
            }
        }

        public String Tb_ApDbScm_Id_TextField
        {
            get { return _t; }
            set
            {
                _t = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_ApDbScm_Id_TextField_noevents
        {
            get { return _t; }
            set
            {
                _t = value;
            }
        }

        public String Tb_UIAssemblyName
        {
            get { return _u; }
            set
            {
                _u = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_UIAssemblyName_noevents
        {
            get { return _u; }
            set
            {
                _u = value;
            }
        }

        public String Tb_UINamespace
        {
            get { return _v; }
            set
            {
                _v = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_UINamespace_noevents
        {
            get { return _v; }
            set
            {
                _v = value;
            }
        }

        public String Tb_UIAssemblyPath
        {
            get { return _w; }
            set
            {
                _w = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_UIAssemblyPath_noevents
        {
            get { return _w; }
            set
            {
                _w = value;
            }
        }

        public String Tb_ProxyAssemblyName
        {
            get { return _x; }
            set
            {
                _x = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_ProxyAssemblyName_noevents
        {
            get { return _x; }
            set
            {
                _x = value;
            }
        }

        public String Tb_ProxyNamespace
        {
            get { return _y; }
            set
            {
                _y = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_ProxyNamespace_noevents
        {
            get { return _y; }
            set
            {
                _y = value;
            }
        }

        public String Tb_ProxyAssemblyPath
        {
            get { return _z; }
            set
            {
                _z = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_ProxyAssemblyPath_noevents
        {
            get { return _z; }
            set
            {
                _z = value;
            }
        }

        public String Tb_WireTypeAssemblyName
        {
            get { return _aa; }
            set
            {
                _aa = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_WireTypeAssemblyName_noevents
        {
            get { return _aa; }
            set
            {
                _aa = value;
            }
        }

        public String Tb_WireTypeNamespace
        {
            get { return _ab; }
            set
            {
                _ab = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_WireTypeNamespace_noevents
        {
            get { return _ab; }
            set
            {
                _ab = value;
            }
        }

        public String Tb_WireTypePath
        {
            get { return _ac; }
            set
            {
                _ac = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_WireTypePath_noevents
        {
            get { return _ac; }
            set
            {
                _ac = value;
            }
        }

        public String Tb_WebServiceName
        {
            get { return _ad; }
            set
            {
                _ad = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_WebServiceName_noevents
        {
            get { return _ad; }
            set
            {
                _ad = value;
            }
        }

        public String Tb_WebServiceFolder
        {
            get { return _ae; }
            set
            {
                _ae = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_WebServiceFolder_noevents
        {
            get { return _ae; }
            set
            {
                _ae = value;
            }
        }

        public String Tb_DataServiceAssemblyName
        {
            get { return _af; }
            set
            {
                _af = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_DataServiceAssemblyName_noevents
        {
            get { return _af; }
            set
            {
                _af = value;
            }
        }

        public String Tb_DataServiceNamespace
        {
            get { return _ag; }
            set
            {
                _ag = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_DataServiceNamespace_noevents
        {
            get { return _ag; }
            set
            {
                _ag = value;
            }
        }

        public String Tb_DataServicePath
        {
            get { return _ah; }
            set
            {
                _ah = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_DataServicePath_noevents
        {
            get { return _ah; }
            set
            {
                _ah = value;
            }
        }

        public Boolean Tb_UseTilesInPropsScreen
        {
            get { return _ai; }
            set
            {
                _ai = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseTilesInPropsScreen_noevents
        {
            get { return _ai; }
            set
            {
                _ai = value;
            }
        }

        public Boolean Tb_UseGridColumnGroups
        {
            get { return _aj; }
            set
            {
                _aj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseGridColumnGroups_noevents
        {
            get { return _aj; }
            set
            {
                _aj = value;
            }
        }

        public Boolean Tb_UseGridDataSourceCombo
        {
            get { return _ak; }
            set
            {
                _ak = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseGridDataSourceCombo_noevents
        {
            get { return _ak; }
            set
            {
                _ak = value;
            }
        }

        public Boolean Tb_PkIsIdentity
        {
            get { return _al; }
            set
            {
                _al = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_PkIsIdentity_noevents
        {
            get { return _al; }
            set
            {
                _al = value;
            }
        }

        public Boolean Tb_IsVirtual
        {
            get { return _am; }
            set
            {
                _am = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsVirtual_noevents
        {
            get { return _am; }
            set
            {
                _am = value;
            }
        }

        public Boolean? Tb_IsScreenPlaceHolder
        {
            get { return _an; }
            set
            {
                _an = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? Tb_IsScreenPlaceHolder_noevents
        {
            get { return _an; }
            set
            {
                _an = value;
            }
        }

        public Boolean Tb_IsNotEntity
        {
            get { return _ao; }
            set
            {
                _ao = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsNotEntity_noevents
        {
            get { return _ao; }
            set
            {
                _ao = value;
            }
        }

        public Boolean Tb_IsMany2Many
        {
            get { return _ap; }
            set
            {
                _ap = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsMany2Many_noevents
        {
            get { return _ap; }
            set
            {
                _ap = value;
            }
        }

        public Boolean Tb_UseLastModifiedByUserNameInSproc
        {
            get { return _aq; }
            set
            {
                _aq = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseLastModifiedByUserNameInSproc_noevents
        {
            get { return _aq; }
            set
            {
                _aq = value;
            }
        }

        public Boolean Tb_UseUserTimeStamp
        {
            get { return _ar; }
            set
            {
                _ar = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseUserTimeStamp_noevents
        {
            get { return _ar; }
            set
            {
                _ar = value;
            }
        }

        public Boolean Tb_UseForAudit
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_UseForAudit_noevents
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
            }
        }

        public Boolean? Tb_IsAllowDelete
        {
            get { return _at; }
            set
            {
                _at = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? Tb_IsAllowDelete_noevents
        {
            get { return _at; }
            set
            {
                _at = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_MenuRow_IsVisible
        {
            get { return _au; }
            set
            {
                _au = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_MenuRow_IsVisible_noevents
        {
            get { return _au; }
            set
            {
                _au = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_GridTools_IsVisible
        {
            get { return _av; }
            set
            {
                _av = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_GridTools_IsVisible_noevents
        {
            get { return _av; }
            set
            {
                _av = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_SplitScreen_IsVisible
        {
            get { return _aw; }
            set
            {
                _aw = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_SplitScreen_IsVisible_noevents
        {
            get { return _aw; }
            set
            {
                _aw = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
        {
            get { return _ax; }
            set
            {
                _ax = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_SplitScreen_IsSplit_Default_noevents
        {
            get { return _ax; }
            set
            {
                _ax = value;
            }
        }

        public Int32 Tb_CnfgGdMnu_NavColumnWidth
        {
            get { return _ay; }
            set
            {
                _ay = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32 Tb_CnfgGdMnu_NavColumnWidth_noevents
        {
            get { return _ay; }
            set
            {
                _ay = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_IsReadOnly
        {
            get { return _az; }
            set
            {
                _az = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_IsReadOnly_noevents
        {
            get { return _az; }
            set
            {
                _az = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_IsAllowNewRow
        {
            get { return _ba; }
            set
            {
                _ba = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_IsAllowNewRow_noevents
        {
            get { return _ba; }
            set
            {
                _ba = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_ExcelExport_IsVisible
        {
            get { return _bb; }
            set
            {
                _bb = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_ExcelExport_IsVisible_noevents
        {
            get { return _bb; }
            set
            {
                _bb = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_ColumnChooser_IsVisible
        {
            get { return _bc; }
            set
            {
                _bc = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_ColumnChooser_IsVisible_noevents
        {
            get { return _bc; }
            set
            {
                _bc = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
        {
            get { return _bd; }
            set
            {
                _bd = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_ShowHideColBtn_IsVisible_noevents
        {
            get { return _bd; }
            set
            {
                _bd = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_RefreshGrid_IsVisible
        {
            get { return _be; }
            set
            {
                _be = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_RefreshGrid_IsVisible_noevents
        {
            get { return _be; }
            set
            {
                _be = value;
            }
        }

        public Boolean Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
        {
            get { return _bf; }
            set
            {
                _bf = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible_noevents
        {
            get { return _bf; }
            set
            {
                _bf = value;
            }
        }

        public Boolean Tb_IsInputComplete
        {
            get { return _bg; }
            set
            {
                _bg = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsInputComplete_noevents
        {
            get { return _bg; }
            set
            {
                _bg = value;
            }
        }

        public Boolean Tb_IsCodeGen
        {
            get { return _bh; }
            set
            {
                _bh = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsCodeGen_noevents
        {
            get { return _bh; }
            set
            {
                _bh = value;
            }
        }

        public Boolean Tb_IsReadyCodeGen
        {
            get { return _bi; }
            set
            {
                _bi = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsReadyCodeGen_noevents
        {
            get { return _bi; }
            set
            {
                _bi = value;
            }
        }

        public Boolean Tb_IsCodeGenComplete
        {
            get { return _bj; }
            set
            {
                _bj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsCodeGenComplete_noevents
        {
            get { return _bj; }
            set
            {
                _bj = value;
            }
        }

        public Boolean Tb_IsTagForCodeGen
        {
            get { return _bk; }
            set
            {
                _bk = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsTagForCodeGen_noevents
        {
            get { return _bk; }
            set
            {
                _bk = value;
            }
        }

        public Boolean Tb_IsTagForOther
        {
            get { return _bl; }
            set
            {
                _bl = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsTagForOther_noevents
        {
            get { return _bl; }
            set
            {
                _bl = value;
            }
        }

        public String Tb_TableGroups
        {
            get { return _bm; }
            set
            {
                _bm = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tb_TableGroups_noevents
        {
            get { return _bm; }
            set
            {
                _bm = value;
            }
        }

        public Boolean Tb_IsActiveRow
        {
            get { return _bn; }
            set
            {
                _bn = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsActiveRow_noevents
        {
            get { return _bn; }
            set
            {
                _bn = value;
            }
        }

        public Boolean Tb_IsDeleted
        {
            get { return _bo; }
            set
            {
                _bo = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tb_IsDeleted_noevents
        {
            get { return _bo; }
            set
            {
                _bo = value;
            }
        }

        public Guid? Tb_CreatedUserId
        {
            get { return _bp; }
            set
            {
                _bp = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_CreatedUserId_noevents
        {
            get { return _bp; }
            set
            {
                _bp = value;
            }
        }

        public DateTime? Tb_CreatedDate
        {
            get { return _bq; }
            set
            {
                _bq = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Tb_CreatedDate_noevents
        {
            get { return _bq; }
            set
            {
                _bq = value;
            }
        }

        public Guid? Tb_UserId
        {
            get { return _br; }
            set
            {
                _br = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tb_UserId_noevents
        {
            get { return _br; }
            set
            {
                _br = value;
            }
        }

        public String UserName
        {
            get { return _bs; }
            set
            {
                _bs = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _bs; }
            set
            {
                _bs = value;
            }
        }

        public DateTime? Tb_LastModifiedDate
        {
            get { return _bt; }
            set
            {
                _bt = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Tb_LastModifiedDate_noevents
        {
            get { return _bt; }
            set
            {
                _bt = value;
            }
        }

        public Byte[] Tb_Stamp
        {
            get { return _bu; }
            set
            {
                _bu = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] Tb_Stamp_noevents
        {
            get { return _bu; }
            set
            {
                _bu = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r,
				_s,
				_t,
				_u,
				_v,
				_w,
				_x,
				_y,
				_z,
				_aa,
				_ab,
				_ac,
				_ad,
				_ae,
				_af,
				_ag,
				_ah,
				_ai,
				_aj,
				_ak,
				_al,
				_am,
				_an,
				_ao,
				_ap,
				_aq,
				_ar,
				_asxx,
				_at,
				_au,
				_av,
				_aw,
				_ax,
				_ay,
				_az,
				_ba,
				_bb,
				_bc,
				_bd,
				_be,
				_bf,
				_bg,
				_bh,
				_bi,
				_bj,
				_bk,
				_bl,
				_bm,
				_bn,
				_bo,
				_bp,
				_bq,
				_br,
				_bs,
				_bt,
				_bu
			};
        }

        public WcTable_Values Clone()
        {
            return new WcTable_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


