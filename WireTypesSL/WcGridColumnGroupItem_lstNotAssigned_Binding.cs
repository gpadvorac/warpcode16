using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/27/2017 7:49:43 PM

namespace EntityWireTypeSL
{


    [DataContract]
    public class WcGridColumnGroupItem_lstNotAssigned_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcGridColumnGroupItem_lstNotAssigned_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcGridColumnGroupItem_lstNotAssigned_Binding() { }


        public WcGridColumnGroupItem_lstNotAssigned_Binding(Guid _TbC_Id, String _TbC_Name )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroupItem_lstNotAssigned_Binding", IfxTraceCategory.Enter);
				_a = _TbC_Id;
				_b = _TbC_Name;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroupItem_lstNotAssigned_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroupItem_lstNotAssigned_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcGridColumnGroupItem_lstNotAssigned_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroupItem_lstNotAssigned_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  TbC_Id
				_b = (String)data[1];                //  TbC_Name
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroupItem_lstNotAssigned_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroupItem_lstNotAssigned_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region TbC_Id

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid TbC_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion TbC_Id


        #region TbC_Name

        private String _b;
//        public String B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public String TbC_Name
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion TbC_Name


        public override string ToString()
        {
            return String.Format("{0}; {1} ", TbC_Id, TbC_Name );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1} ", TbC_Id, TbC_Name );
        }

    }

}
