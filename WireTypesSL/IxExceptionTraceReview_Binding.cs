using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

namespace EntityWireTypeSL
{


    [DataContract]
    public class IxExceptionTraceReview_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "Ifx";
        private static string _cn = "IxExceptionTraceReview_Binding";

        #endregion Initialize Variables


		#region Constructors

        public IxExceptionTraceReview_Binding() { }


        public IxExceptionTraceReview_Binding(Int32? _Excp_SortOrder, Int32? _Trace_CreatedOrder, String _Excp_Message, String _Excp_StackTrace, String _Trace_Assembly_Name, String _Trace_Method_Name, String _UserName, DateTime? _Trace_Create_Stamp, Int32? _Trace_Ticks, String _Trace_Os_Version, String _Trace_Ip, String _Excp_Data, String _Trace_Information, String _Trace_Type_Name, String _TraceCategory_Name, String _Trace_Domain_Name, String _Trace_Os_Platform, String _Trace_Machine_Name, Guid? _Excp_Id, Guid? _Excp_Excp_Id, Guid _Trace_Id, Guid? _Trace_EnterId, Guid? _Trace_BubbledTraceEnter_Id, DateTime? _Excp_TimeStamp )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IxExceptionTraceReview_Binding", IfxTraceCategory.Enter);
				_a = _Excp_SortOrder;
				_b = _Trace_CreatedOrder;
				_c = _Excp_Message;
				_d = _Excp_StackTrace;
				_e = _Trace_Assembly_Name;
				_f = _Trace_Method_Name;
				_g = _UserName;
				_h = _Trace_Create_Stamp;
				_i = _Trace_Ticks;
				_j = _Trace_Os_Version;
				_k = _Trace_Ip;
				_l = _Excp_Data;
				_m = _Trace_Information;
				_n = _Trace_Type_Name;
				_o = _TraceCategory_Name;
				_p = _Trace_Domain_Name;
				_q = _Trace_Os_Platform;
				_r = _Trace_Machine_Name;
				_s = _Excp_Id;
				_t = _Excp_Excp_Id;
				_u = _Trace_Id;
				_v = _Trace_EnterId;
				_w = _Trace_BubbledTraceEnter_Id;
				_x = _Excp_TimeStamp;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IxExceptionTraceReview_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IxExceptionTraceReview_Binding", IfxTraceCategory.Leave);
            }
		}

        public IxExceptionTraceReview_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IxExceptionTraceReview_Binding", IfxTraceCategory.Enter);
				_a = ObjectHelper.GetNullableIntFromObjectValue(data[0]);                 //  Excp_SortOrder
				_b = ObjectHelper.GetNullableIntFromObjectValue(data[1]);                 //  Trace_CreatedOrder
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);                 //  Excp_Message
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);                 //  Excp_StackTrace
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);                 //  Trace_Assembly_Name
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);                 //  Trace_Method_Name
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);                 //  UserName
				_h = ObjectHelper.GetNullableDateTimeFromObjectValue(data[7]);                 //  Trace_Create_Stamp
				_i = ObjectHelper.GetNullableIntFromObjectValue(data[8]);                 //  Trace_Ticks
				_j = ObjectHelper.GetStringFromObjectValue(data[9]);                 //  Trace_Os_Version
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);                 //  Trace_Ip
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);                 //  Excp_Data
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);                 //  Trace_Information
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);                 //  Trace_Type_Name
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);                 //  TraceCategory_Name
				_p = ObjectHelper.GetStringFromObjectValue(data[15]);                 //  Trace_Domain_Name
				_q = ObjectHelper.GetStringFromObjectValue(data[16]);                 //  Trace_Os_Platform
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);                 //  Trace_Machine_Name
				_s = ObjectHelper.GetNullableGuidFromObjectValue(data[18]);                 //  Excp_Id
				_t = ObjectHelper.GetNullableGuidFromObjectValue(data[19]);                 //  Excp_Excp_Id
				_u = (Guid)data[20];                //  Trace_Id
				_v = ObjectHelper.GetNullableGuidFromObjectValue(data[21]);                 //  Trace_EnterId
				_w = ObjectHelper.GetNullableGuidFromObjectValue(data[22]);                 //  Trace_BubbledTraceEnter_Id
				_x = ObjectHelper.GetNullableDateTimeFromObjectValue(data[23]);                 //  Excp_TimeStamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IxExceptionTraceReview_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IxExceptionTraceReview_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return _a;
        }

        public void Set_Int_Id(int value)
        {
            _a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return null;
        }

        public void Set_Guid_Id(Guid value)
        {
            //_a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Int32? Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region Excp_SortOrder

        private Int32? _a;
//        [DataMember]
//        public Int32? A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Int32? Excp_SortOrder
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Excp_SortOrder


        #region Trace_CreatedOrder

        private Int32? _b;
//        [DataMember]
//        public Int32? B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Int32? Trace_CreatedOrder
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion Trace_CreatedOrder


        #region Excp_Message

        private String _c;
//        [DataMember]
//        public String C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public String Excp_Message
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion Excp_Message


        #region Excp_StackTrace

        private String _d;
//        [DataMember]
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String Excp_StackTrace
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion Excp_StackTrace


        #region Trace_Assembly_Name

        private String _e;
//        [DataMember]
//        public String E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public String Trace_Assembly_Name
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion Trace_Assembly_Name


        #region Trace_Method_Name

        private String _f;
//        [DataMember]
//        public String F
//        {
//            get { return _f; }
//            set { _f = value; }
//        }

        //[NonSerialized]
        public String Trace_Method_Name
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion Trace_Method_Name


        #region UserName

        private String _g;
//        [DataMember]
//        public String G
//        {
//            get { return _g; }
//            set { _g = value; }
//        }

        //[NonSerialized]
        public String UserName
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion UserName


        #region Trace_Create_Stamp

        private DateTime? _h;
//        [DataMember]
//        public DateTime? H
//        {
//            get { return _h; }
//            set { _h = value; }
//        }

        //[NonSerialized]
        public DateTime? Trace_Create_Stamp
        {
            get { return _h; }
            set { _h = value; }
        }

        #endregion Trace_Create_Stamp


        #region Trace_Ticks

        private Int32? _i;
//        [DataMember]
//        public Int32? I
//        {
//            get { return _i; }
//            set { _i = value; }
//        }

        //[NonSerialized]
        public Int32? Trace_Ticks
        {
            get { return _i; }
            set { _i = value; }
        }

        #endregion Trace_Ticks


        #region Trace_Os_Version

        private String _j;
//        [DataMember]
//        public String J
//        {
//            get { return _j; }
//            set { _j = value; }
//        }

        //[NonSerialized]
        public String Trace_Os_Version
        {
            get { return _j; }
            set { _j = value; }
        }

        #endregion Trace_Os_Version


        #region Trace_Ip

        private String _k;
//        [DataMember]
//        public String K
//        {
//            get { return _k; }
//            set { _k = value; }
//        }

        //[NonSerialized]
        public String Trace_Ip
        {
            get { return _k; }
            set { _k = value; }
        }

        #endregion Trace_Ip


        #region Excp_Data

        private String _l;
//        [DataMember]
//        public String L
//        {
//            get { return _l; }
//            set { _l = value; }
//        }

        //[NonSerialized]
        public String Excp_Data
        {
            get { return _l; }
            set { _l = value; }
        }

        #endregion Excp_Data


        #region Trace_Information

        private String _m;
//        [DataMember]
//        public String M
//        {
//            get { return _m; }
//            set { _m = value; }
//        }

        //[NonSerialized]
        public String Trace_Information
        {
            get { return _m; }
            set { _m = value; }
        }

        #endregion Trace_Information


        #region Trace_Type_Name

        private String _n;
//        [DataMember]
//        public String N
//        {
//            get { return _n; }
//            set { _n = value; }
//        }

        //[NonSerialized]
        public String Trace_Type_Name
        {
            get { return _n; }
            set { _n = value; }
        }

        #endregion Trace_Type_Name


        #region TraceCategory_Name

        private String _o;
//        [DataMember]
//        public String O
//        {
//            get { return _o; }
//            set { _o = value; }
//        }

        //[NonSerialized]
        public String TraceCategory_Name
        {
            get { return _o; }
            set { _o = value; }
        }

        #endregion TraceCategory_Name


        #region Trace_Domain_Name

        private String _p;
//        [DataMember]
//        public String P
//        {
//            get { return _p; }
//            set { _p = value; }
//        }

        //[NonSerialized]
        public String Trace_Domain_Name
        {
            get { return _p; }
            set { _p = value; }
        }

        #endregion Trace_Domain_Name


        #region Trace_Os_Platform

        private String _q;
//        [DataMember]
//        public String Q
//        {
//            get { return _q; }
//            set { _q = value; }
//        }

        //[NonSerialized]
        public String Trace_Os_Platform
        {
            get { return _q; }
            set { _q = value; }
        }

        #endregion Trace_Os_Platform


        #region Trace_Machine_Name

        private String _r;
//        [DataMember]
//        public String R
//        {
//            get { return _r; }
//            set { _r = value; }
//        }

        //[NonSerialized]
        public String Trace_Machine_Name
        {
            get { return _r; }
            set { _r = value; }
        }

        #endregion Trace_Machine_Name


        #region Excp_Id

        private Guid? _s;
//        [DataMember]
//        public Guid? S
//        {
//            get { return _s; }
//            set { _s = value; }
//        }

        //[NonSerialized]
        public Guid? Excp_Id
        {
            get { return _s; }
            set { _s = value; }
        }

        #endregion Excp_Id


        #region Excp_Excp_Id

        private Guid? _t;
//        [DataMember]
//        public Guid? T
//        {
//            get { return _t; }
//            set { _t = value; }
//        }

        //[NonSerialized]
        public Guid? Excp_Excp_Id
        {
            get { return _t; }
            set { _t = value; }
        }

        #endregion Excp_Excp_Id


        #region Trace_Id

        private Guid _u;
//        [DataMember]
//        public Guid U
//        {
//            get { return _u; }
//            set { _u = value; }
//        }

        //[NonSerialized]
        public Guid Trace_Id
        {
            get { return _u; }
            set { _u = value; }
        }

        #endregion Trace_Id


        #region Trace_EnterId

        private Guid? _v;
//        [DataMember]
//        public Guid? V
//        {
//            get { return _v; }
//            set { _v = value; }
//        }

        //[NonSerialized]
        public Guid? Trace_EnterId
        {
            get { return _v; }
            set { _v = value; }
        }

        #endregion Trace_EnterId


        #region Trace_BubbledTraceEnter_Id

        private Guid? _w;
//        [DataMember]
//        public Guid? W
//        {
//            get { return _w; }
//            set { _w = value; }
//        }

        //[NonSerialized]
        public Guid? Trace_BubbledTraceEnter_Id
        {
            get { return _w; }
            set { _w = value; }
        }

        #endregion Trace_BubbledTraceEnter_Id


        #region Excp_TimeStamp

        private DateTime? _x;
//        [DataMember]
//        public DateTime? X
//        {
//            get { return _x; }
//            set { _x = value; }
//        }

        //[NonSerialized]
        public DateTime? Excp_TimeStamp
        {
            get { return _x; }
            set { _x = value; }
        }

        #endregion Excp_TimeStamp


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11}; {12}; {13}; {14}; {15}; {16}; {17}; {18}; {19}; {20}; {21}; {22}; {23} ", Excp_SortOrder, Trace_CreatedOrder, Excp_Message, Excp_StackTrace, Trace_Assembly_Name, Trace_Method_Name, UserName, Trace_Create_Stamp, Trace_Ticks, Trace_Os_Version, Trace_Ip, Excp_Data, Trace_Information, Trace_Type_Name, TraceCategory_Name, Trace_Domain_Name, Trace_Os_Platform, Trace_Machine_Name, Excp_Id, Excp_Excp_Id, Trace_Id, Trace_EnterId, Trace_BubbledTraceEnter_Id, Excp_TimeStamp );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11}; {12}; {13}; {14}; {15}; {16}; {17}; {18}; {19}; {20}; {21}; {22}; {23} ", Excp_SortOrder, Trace_CreatedOrder, Excp_Message, Excp_StackTrace, Trace_Assembly_Name, Trace_Method_Name, UserName, Trace_Create_Stamp, Trace_Ticks, Trace_Os_Version, Trace_Ip, Excp_Data, Trace_Information, Trace_Type_Name, TraceCategory_Name, Trace_Domain_Name, Trace_Os_Platform, Trace_Machine_Name, Excp_Id, Excp_Excp_Id, Trace_Id, Trace_EnterId, Trace_BubbledTraceEnter_Id, Excp_TimeStamp );
        }

    }

}
