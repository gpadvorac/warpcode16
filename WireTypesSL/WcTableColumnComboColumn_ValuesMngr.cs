using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  1/3/2017 11:12:36 PM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcTableColumnComboColumn_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTableColumnComboColumn_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcTableColumnComboColumn_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcTableColumnComboColumn_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcTableColumnComboColumn_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumnComboColumn_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcTableColumnComboColumn_Values(currentData, this) : new WcTableColumnComboColumn_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcTableColumnComboColumn_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumnComboColumn_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumnComboColumn_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcTableColumnComboColumn_Values _original;
        [DataMember]
        public WcTableColumnComboColumn_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcTableColumnComboColumn_Values _current;
        [DataMember]
        public WcTableColumnComboColumn_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcTableColumnComboColumn_Values _concurrent;
        [DataMember]
        public WcTableColumnComboColumn_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  TbCCmbC_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  TbCCmbC_TbC_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  TbCCmbC_Index
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  TbCCmbC_HeaderText
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  TbCCmbC_MinWidth
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  TbCCmbC_MaxWidth
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  TbCCmbC_TextWrap
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  TbCCmbC_IsActiveRow
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  TbCCmbC_IsDeleted
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  TbCCmbC_CreatedUserId
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  TbCCmbC_CreatedDate
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  TbCCmbC_UserId
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  UserName
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  TbCCmbC_LastModifiedDate
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  TbCCmbC_Stamp
                if (_current._o != _original._o)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{16}{0}{15}_b{16}{1}{15}_c{16}{2}{15}_d{16}{3}{15}_e{16}{4}{15}_f{16}{5}{15}_g{16}{6}{15}_h{16}{7}{15}_i{16}{8}{15}_j{16}{9}{15}_k{16}{10}{15}_l{16}{11}{15}_m{16}{12}{15}_n{16}{13}{15}_o{16}{14}",
				new object[] {
				_current._a,		  //TbCCmbC_Id
				_current._b,		  //TbCCmbC_TbC_Id
				_current._c,		  //TbCCmbC_Index
				_current._d,		  //TbCCmbC_HeaderText
				_current._e,		  //TbCCmbC_MinWidth
				_current._f,		  //TbCCmbC_MaxWidth
				_current._g,		  //TbCCmbC_TextWrap
				_current._h,		  //TbCCmbC_IsActiveRow
				_current._i,		  //TbCCmbC_IsDeleted
				_current._j,		  //TbCCmbC_CreatedUserId
				_current._k,		  //TbCCmbC_CreatedDate
				_current._l,		  //TbCCmbC_UserId
				_current._m,		  //UserName
				_current._n,		  //TbCCmbC_LastModifiedDate
				_current._o,		  //TbCCmbC_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcTableColumnComboColumn_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTableColumnComboColumn_Values";
        private WcTableColumnComboColumn_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcTableColumnComboColumn_Values() 
        {
        }

        //public WcTableColumnComboColumn_Values(object[] data, WcTableColumnComboColumn_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcTableColumnComboColumn_Values(object[] data, WcTableColumnComboColumn_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumnComboColumn_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  TbCCmbC_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  TbCCmbC_TbC_Id
				_c = ObjectHelper.GetNullableByteFromObjectValue(data[2]);							//  TbCCmbC_Index
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  TbCCmbC_HeaderText
				_e = ObjectHelper.GetNullableDoubleFromObjectValue(data[4]);					//  TbCCmbC_MinWidth
				_f = ObjectHelper.GetNullableDoubleFromObjectValue(data[5]);					//  TbCCmbC_MaxWidth
				_g = ObjectHelper.GetNullableBoolFromObjectValue(data[6]);					//  TbCCmbC_TextWrap
				_h = ObjectHelper.GetBoolFromObjectValue(data[7]);									//  TbCCmbC_IsActiveRow
				_i = ObjectHelper.GetBoolFromObjectValue(data[8]);									//  TbCCmbC_IsDeleted
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  TbCCmbC_CreatedUserId
				_k = ObjectHelper.GetNullableDateTimeFromObjectValue(data[10]);					//  TbCCmbC_CreatedDate
				_l = ObjectHelper.GetNullableGuidFromObjectValue(data[11]);						//  TbCCmbC_UserId
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  UserName
				_n = ObjectHelper.GetNullableDateTimeFromObjectValue(data[13]);					//  TbCCmbC_LastModifiedDate
				_o = ObjectHelper.GetByteArrayFromObjectValue(data[14]);						//  TbCCmbC_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumnComboColumn_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableColumnComboColumn_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableColumnComboColumn", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  TbCCmbC_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  TbCCmbC_TbC_Id
				_c = ObjectHelper.GetNullableByteFromObjectValue(data[2]);							//  TbCCmbC_Index
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  TbCCmbC_HeaderText
				_e = ObjectHelper.GetNullableDoubleFromObjectValue(data[4]);					//  TbCCmbC_MinWidth
				_f = ObjectHelper.GetNullableDoubleFromObjectValue(data[5]);					//  TbCCmbC_MaxWidth
				_g = ObjectHelper.GetNullableBoolFromObjectValue(data[6]);					//  TbCCmbC_TextWrap
				_h = ObjectHelper.GetBoolFromObjectValue(data[7]);									//  TbCCmbC_IsActiveRow
				_i = ObjectHelper.GetBoolFromObjectValue(data[8]);									//  TbCCmbC_IsDeleted
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  TbCCmbC_CreatedUserId
				_k = ObjectHelper.GetNullableDateTimeFromObjectValue(data[10]);					//  TbCCmbC_CreatedDate
				_l = ObjectHelper.GetNullableGuidFromObjectValue(data[11]);						//  TbCCmbC_UserId
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  UserName
				_n = ObjectHelper.GetNullableDateTimeFromObjectValue(data[13]);					//  TbCCmbC_LastModifiedDate
				_o = ObjectHelper.GetByteArrayFromObjectValue(data[14]);						//  TbCCmbC_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableColumnComboColumn", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableColumnComboColumn", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  TbCCmbC_Id

		[DataMember]
		public Guid? _b;			//  TbCCmbC_TbC_Id

		[DataMember]
		public Byte? _c;			//  TbCCmbC_Index

		[DataMember]
		public String _d;			//  TbCCmbC_HeaderText

		[DataMember]
		public Double? _e;			//  TbCCmbC_MinWidth

		[DataMember]
		public Double? _f;			//  TbCCmbC_MaxWidth

		[DataMember]
		public Boolean? _g;			//  TbCCmbC_TextWrap

		[DataMember]
		public Boolean _h;			//  TbCCmbC_IsActiveRow

		[DataMember]
		public Boolean _i;			//  TbCCmbC_IsDeleted

		[DataMember]
		public Guid? _j;			//  TbCCmbC_CreatedUserId

		[DataMember]
		public DateTime? _k;			//  TbCCmbC_CreatedDate

		[DataMember]
		public Guid? _l;			//  TbCCmbC_UserId

		[DataMember]
		public String _m;			//  UserName

		[DataMember]
		public DateTime? _n;			//  TbCCmbC_LastModifiedDate

		[DataMember]
		public Byte[] _o;			//  TbCCmbC_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcTableColumnComboColumn_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 15; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                }
                return _list;
            }
        }

        public Guid TbCCmbC_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid TbCCmbC_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? TbCCmbC_TbC_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbCCmbC_TbC_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Byte? TbCCmbC_Index
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte? TbCCmbC_Index_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String TbCCmbC_HeaderText
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbCCmbC_HeaderText_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public Double? TbCCmbC_MinWidth
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Double? TbCCmbC_MinWidth_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Double? TbCCmbC_MaxWidth
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Double? TbCCmbC_MaxWidth_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Boolean? TbCCmbC_TextWrap
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? TbCCmbC_TextWrap_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Boolean TbCCmbC_IsActiveRow
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbCCmbC_IsActiveRow_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Boolean TbCCmbC_IsDeleted
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbCCmbC_IsDeleted_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Guid? TbCCmbC_CreatedUserId
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbCCmbC_CreatedUserId_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public DateTime? TbCCmbC_CreatedDate
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? TbCCmbC_CreatedDate_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public Guid? TbCCmbC_UserId
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbCCmbC_UserId_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public String UserName
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public DateTime? TbCCmbC_LastModifiedDate
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? TbCCmbC_LastModifiedDate_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public Byte[] TbCCmbC_Stamp
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] TbCCmbC_Stamp_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o
			};
        }

        public WcTableColumnComboColumn_Values Clone()
        {
            return new WcTableColumnComboColumn_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


