using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/18/2018 6:19:15 PM

namespace EntityWireTypeSL
{


    [DataContract]
    public partial class WcCodeGen_TableColumn_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcCodeGen_TableColumn_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcCodeGen_TableColumn_Binding() { }


        public WcCodeGen_TableColumn_Binding(Guid _TbC_Id, Guid _TbC_Tb_Id, Int16 _TbC_SortOrder, String _TbC_Name, Int32? _TbC_CtlTp_ID, String _TbC_CtlTp_ID_TextField, Boolean _TbC_IsNonvColumn, String _TbC_Description, Int32 _TbC_DtSql_Id, String _TbC_DtSql_Id_TextField, Int32 _TbC_DtNt_Id, String _TbC_DtNt_Id_TextField, Int32? _TbC_Length, Int32? _TbC_Precision, Int32? _TbC_Scale, Boolean _TbC_IsPK, Boolean _TbC_IsIdentity, Boolean _TbC_IsFK, Boolean _TbC_IsEntityColumn, Boolean _TbC_IsSystemField, Boolean _TbC_IsValuesObjectMember, Boolean _TbC_IsInPropsScreen, Boolean _TbC_IsInNavList, Boolean _TbC_IsRequired, String _TbC_BrokenRuleText, Boolean _TbC_AllowZero, Boolean _TbC_IsNullableInDb, Boolean _TbC_IsNullableInUI, String _TbC_DefaultValue, Boolean _TbC_IsReadFromDb, Boolean _TbC_IsSendToDb, Boolean _TbC_IsInsertAllowed, Boolean _TbC_IsEditAllowed, Boolean _TbC_IsReadOnlyInUI, Boolean _TbC_UseForAudit, String _TbC_DefaultCaption, String _TbC_ColumnHeaderText, String _TbC_LabelCaptionVerbose, Boolean _TbC_LabelCaptionGenerate, Boolean _Tbc_ShowGridColumnToolTip, Boolean _Tbc_ShowPropsToolTip, String _Tbc_TooltipsRolledUp, Boolean _TbC_IsCreatePropsStrings, Boolean _TbC_IsCreateGridStrings, Boolean _TbC_IsAvailableForColumnGroups, Boolean _TbC_IsTextWrapInProp, Boolean _TbC_IsTextWrapInGrid, String _TbC_TextBoxFormat, String _TbC_TextColumnFormat, Double? _TbC_ColumnWidth, String _TbC_TextBoxTextAlignment, String _TbC_ColumnTextAlignment, Guid? _TbC_ListStoredProc_Id, String _TbC_ListStoredProc_Id_TextField, Boolean _TbC_IsStaticList, Boolean _TbC_UseNotInList, Boolean _TbC_UseListEditBtn, Guid? _TbC_ParentColumnKey, String _TbC_ParentColumnKey_TextField, Boolean _TbC_UseDisplayTextFieldProperty, Boolean _TbC_IsDisplayTextFieldProperty, Guid? _TbC_ComboListTable_Id, String _TbC_ComboListTable_Id_TextField, String _TbC_ComboListTable_Id_PkName, Guid? _TbC_ComboListDisplayColumn_Id, String _TbC_ComboListDisplayColumn_Id_TextField, Double? _TbC_Combo_MaxDropdownHeight, Double? _TbC_Combo_MaxDropdownWidth, Boolean _TbC_Combo_AllowDropdownResizing, Boolean _TbC_Combo_IsResetButtonVisible, Boolean _TbC_IsActiveRecColumn, Boolean _TbC_IsDeletedColumn, Boolean _TbC_IsCreatedUserIdColumn, Boolean _TbC_IsCreatedDateColumn, Boolean _TbC_IsUserIdColumn, Boolean _TbC_IsModifiedDateColumn, Boolean _TbC_IsRowVersionStampColumn, Boolean _TbC_IsBrowsable, String _TbC_DeveloperNote, String _TbC_HelpFileAdditionalNote, Boolean _TbC_IsInputComplete, Boolean _TbC_IsCodeGen, Boolean _TbC_IsReadyCodeGen, Boolean _TbC_IsCodeGenComplete, Boolean _TbC_IsTagForCodeGen, Boolean _TbC_IsTagForOther, String _TbC_ColumnGroups )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableColumn_Binding", IfxTraceCategory.Enter);
				_a = _TbC_Id;
				_b = _TbC_Tb_Id;
				_c = _TbC_SortOrder;
				_d = _TbC_Name;
				_e = _TbC_CtlTp_ID;
				_f = _TbC_CtlTp_ID_TextField;
				_g = _TbC_IsNonvColumn;
				_h = _TbC_Description;
				_i = _TbC_DtSql_Id;
				_j = _TbC_DtSql_Id_TextField;
				_k = _TbC_DtNt_Id;
				_l = _TbC_DtNt_Id_TextField;
				_m = _TbC_Length;
				_n = _TbC_Precision;
				_o = _TbC_Scale;
				_p = _TbC_IsPK;
				_q = _TbC_IsIdentity;
				_r = _TbC_IsFK;
				_s = _TbC_IsEntityColumn;
				_t = _TbC_IsSystemField;
				_u = _TbC_IsValuesObjectMember;
				_v = _TbC_IsInPropsScreen;
				_w = _TbC_IsInNavList;
				_x = _TbC_IsRequired;
				_y = _TbC_BrokenRuleText;
				_z = _TbC_AllowZero;
				_aa = _TbC_IsNullableInDb;
				_ab = _TbC_IsNullableInUI;
				_ac = _TbC_DefaultValue;
				_ad = _TbC_IsReadFromDb;
				_ae = _TbC_IsSendToDb;
				_af = _TbC_IsInsertAllowed;
				_ag = _TbC_IsEditAllowed;
				_ah = _TbC_IsReadOnlyInUI;
				_ai = _TbC_UseForAudit;
				_aj = _TbC_DefaultCaption;
				_ak = _TbC_ColumnHeaderText;
				_al = _TbC_LabelCaptionVerbose;
				_am = _TbC_LabelCaptionGenerate;
				_an = _Tbc_ShowGridColumnToolTip;
				_ao = _Tbc_ShowPropsToolTip;
				_ap = _Tbc_TooltipsRolledUp;
				_aq = _TbC_IsCreatePropsStrings;
				_ar = _TbC_IsCreateGridStrings;
				_asx = _TbC_IsAvailableForColumnGroups;
				_at = _TbC_IsTextWrapInProp;
				_au = _TbC_IsTextWrapInGrid;
				_av = _TbC_TextBoxFormat;
				_aw = _TbC_TextColumnFormat;
				_ax = _TbC_ColumnWidth;
				_ay = _TbC_TextBoxTextAlignment;
				_az = _TbC_ColumnTextAlignment;
				_ba = _TbC_ListStoredProc_Id;
				_bb = _TbC_ListStoredProc_Id_TextField;
				_bc = _TbC_IsStaticList;
				_bd = _TbC_UseNotInList;
				_be = _TbC_UseListEditBtn;
				_bf = _TbC_ParentColumnKey;
				_bg = _TbC_ParentColumnKey_TextField;
				_bh = _TbC_UseDisplayTextFieldProperty;
				_bi = _TbC_IsDisplayTextFieldProperty;
				_bj = _TbC_ComboListTable_Id;
				_bk = _TbC_ComboListTable_Id_TextField;
				_bl = _TbC_ComboListTable_Id_PkName;
				_bm = _TbC_ComboListDisplayColumn_Id;
				_bn = _TbC_ComboListDisplayColumn_Id_TextField;
				_bo = _TbC_Combo_MaxDropdownHeight;
				_bp = _TbC_Combo_MaxDropdownWidth;
				_bq = _TbC_Combo_AllowDropdownResizing;
				_br = _TbC_Combo_IsResetButtonVisible;
				_bs = _TbC_IsActiveRecColumn;
				_bt = _TbC_IsDeletedColumn;
				_bu = _TbC_IsCreatedUserIdColumn;
				_bv = _TbC_IsCreatedDateColumn;
				_bw = _TbC_IsUserIdColumn;
				_bx = _TbC_IsModifiedDateColumn;
				_by = _TbC_IsRowVersionStampColumn;
				_bz = _TbC_IsBrowsable;
				_ca = _TbC_DeveloperNote;
				_cb = _TbC_HelpFileAdditionalNote;
				_cc = _TbC_IsInputComplete;
				_cd = _TbC_IsCodeGen;
				_ce = _TbC_IsReadyCodeGen;
				_cf = _TbC_IsCodeGenComplete;
				_cg = _TbC_IsTagForCodeGen;
				_ch = _TbC_IsTagForOther;
				_ci = _TbC_ColumnGroups;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableColumn_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableColumn_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcCodeGen_TableColumn_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableColumn_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  TbC_Id
				_b = (Guid)data[1];                //  TbC_Tb_Id
				_c = (Int16)data[2];                //  TbC_SortOrder
				_d = (String)data[3];                //  TbC_Name
				_e = ObjectHelper.GetNullableIntFromObjectValue(data[4]);                 //  TbC_CtlTp_ID
				_f = (String)data[5];                //  TbC_CtlTp_ID_TextField
				_g = (Boolean)data[6];                //  TbC_IsNonvColumn
				_h = (String)data[7];                //  TbC_Description
				_i = (Int32)data[8];                //  TbC_DtSql_Id
				_j = (String)data[9];                //  TbC_DtSql_Id_TextField
				_k = (Int32)data[10];                //  TbC_DtNt_Id
				_l = (String)data[11];                //  TbC_DtNt_Id_TextField
				_m = ObjectHelper.GetNullableIntFromObjectValue(data[12]);                 //  TbC_Length
				_n = ObjectHelper.GetNullableIntFromObjectValue(data[13]);                 //  TbC_Precision
				_o = ObjectHelper.GetNullableIntFromObjectValue(data[14]);                 //  TbC_Scale
				_p = (Boolean)data[15];                //  TbC_IsPK
				_q = (Boolean)data[16];                //  TbC_IsIdentity
				_r = (Boolean)data[17];                //  TbC_IsFK
				_s = (Boolean)data[18];                //  TbC_IsEntityColumn
				_t = (Boolean)data[19];                //  TbC_IsSystemField
				_u = (Boolean)data[20];                //  TbC_IsValuesObjectMember
				_v = (Boolean)data[21];                //  TbC_IsInPropsScreen
				_w = (Boolean)data[22];                //  TbC_IsInNavList
				_x = (Boolean)data[23];                //  TbC_IsRequired
				_y = (String)data[24];                //  TbC_BrokenRuleText
				_z = (Boolean)data[25];                //  TbC_AllowZero
				_aa = (Boolean)data[26];                //  TbC_IsNullableInDb
				_ab = (Boolean)data[27];                //  TbC_IsNullableInUI
				_ac = (String)data[28];                //  TbC_DefaultValue
				_ad = (Boolean)data[29];                //  TbC_IsReadFromDb
				_ae = (Boolean)data[30];                //  TbC_IsSendToDb
				_af = (Boolean)data[31];                //  TbC_IsInsertAllowed
				_ag = (Boolean)data[32];                //  TbC_IsEditAllowed
				_ah = (Boolean)data[33];                //  TbC_IsReadOnlyInUI
				_ai = (Boolean)data[34];                //  TbC_UseForAudit
				_aj = (String)data[35];                //  TbC_DefaultCaption
				_ak = (String)data[36];                //  TbC_ColumnHeaderText
				_al = (String)data[37];                //  TbC_LabelCaptionVerbose
				_am = (Boolean)data[38];                //  TbC_LabelCaptionGenerate
				_an = (Boolean)data[39];                //  Tbc_ShowGridColumnToolTip
				_ao = (Boolean)data[40];                //  Tbc_ShowPropsToolTip
				_ap = (String)data[41];                //  Tbc_TooltipsRolledUp
				_aq = (Boolean)data[42];                //  TbC_IsCreatePropsStrings
				_ar = (Boolean)data[43];                //  TbC_IsCreateGridStrings
				_asx = (Boolean)data[44];                //  TbC_IsAvailableForColumnGroups
				_at = (Boolean)data[45];                //  TbC_IsTextWrapInProp
				_au = (Boolean)data[46];                //  TbC_IsTextWrapInGrid
				_av = (String)data[47];                //  TbC_TextBoxFormat
				_aw = (String)data[48];                //  TbC_TextColumnFormat
				_ax = ObjectHelper.GetNullableDoubleFromObjectValue(data[49]);                 //  TbC_ColumnWidth
				_ay = (String)data[50];                //  TbC_TextBoxTextAlignment
				_az = (String)data[51];                //  TbC_ColumnTextAlignment
				_ba = ObjectHelper.GetNullableGuidFromObjectValue(data[52]);                 //  TbC_ListStoredProc_Id
				_bb = (String)data[53];                //  TbC_ListStoredProc_Id_TextField
				_bc = (Boolean)data[54];                //  TbC_IsStaticList
				_bd = (Boolean)data[55];                //  TbC_UseNotInList
				_be = (Boolean)data[56];                //  TbC_UseListEditBtn
				_bf = ObjectHelper.GetNullableGuidFromObjectValue(data[57]);                 //  TbC_ParentColumnKey
				_bg = (String)data[58];                //  TbC_ParentColumnKey_TextField
				_bh = (Boolean)data[59];                //  TbC_UseDisplayTextFieldProperty
				_bi = (Boolean)data[60];                //  TbC_IsDisplayTextFieldProperty
				_bj = ObjectHelper.GetNullableGuidFromObjectValue(data[61]);                 //  TbC_ComboListTable_Id
				_bk = (String)data[62];                //  TbC_ComboListTable_Id_TextField
				_bl = (String)data[63];                //  TbC_ComboListTable_Id_PkName
				_bm = ObjectHelper.GetNullableGuidFromObjectValue(data[64]);                 //  TbC_ComboListDisplayColumn_Id
				_bn = (String)data[65];                //  TbC_ComboListDisplayColumn_Id_TextField
				_bo = ObjectHelper.GetNullableDoubleFromObjectValue(data[66]);                 //  TbC_Combo_MaxDropdownHeight
				_bp = ObjectHelper.GetNullableDoubleFromObjectValue(data[67]);                 //  TbC_Combo_MaxDropdownWidth
				_bq = (Boolean)data[68];                //  TbC_Combo_AllowDropdownResizing
				_br = (Boolean)data[69];                //  TbC_Combo_IsResetButtonVisible
				_bs = (Boolean)data[70];                //  TbC_IsActiveRecColumn
				_bt = (Boolean)data[71];                //  TbC_IsDeletedColumn
				_bu = (Boolean)data[72];                //  TbC_IsCreatedUserIdColumn
				_bv = (Boolean)data[73];                //  TbC_IsCreatedDateColumn
				_bw = (Boolean)data[74];                //  TbC_IsUserIdColumn
				_bx = (Boolean)data[75];                //  TbC_IsModifiedDateColumn
				_by = (Boolean)data[76];                //  TbC_IsRowVersionStampColumn
				_bz = (Boolean)data[77];                //  TbC_IsBrowsable
				_ca = (String)data[78];                //  TbC_DeveloperNote
				_cb = (String)data[79];                //  TbC_HelpFileAdditionalNote
				_cc = (Boolean)data[80];                //  TbC_IsInputComplete
				_cd = (Boolean)data[81];                //  TbC_IsCodeGen
				_ce = (Boolean)data[82];                //  TbC_IsReadyCodeGen
				_cf = (Boolean)data[83];                //  TbC_IsCodeGenComplete
				_cg = (Boolean)data[84];                //  TbC_IsTagForCodeGen
				_ch = (Boolean)data[85];                //  TbC_IsTagForOther
				_ci = (String)data[86];                //  TbC_ColumnGroups
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableColumn_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableColumn_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region TbC_Id

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid TbC_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion TbC_Id


        #region TbC_Tb_Id

        private Guid _b;
//        public Guid B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Guid TbC_Tb_Id
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion TbC_Tb_Id


        #region TbC_SortOrder

        private Int16 _c;
//        public Int16 C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public Int16 TbC_SortOrder
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion TbC_SortOrder


        #region TbC_Name

        private String _d;
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String TbC_Name
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion TbC_Name


        #region TbC_CtlTp_ID

        private Int32? _e;
//        public Int32? E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public Int32? TbC_CtlTp_ID
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion TbC_CtlTp_ID


        #region TbC_CtlTp_ID_TextField

        private String _f;
//        public String F
//        {
//            get { return _f; }
//            set { _f = value; }
//        }

        //[NonSerialized]
        public String TbC_CtlTp_ID_TextField
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion TbC_CtlTp_ID_TextField


        #region TbC_IsNonvColumn

        private Boolean _g;
//        public Boolean G
//        {
//            get { return _g; }
//            set { _g = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsNonvColumn
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion TbC_IsNonvColumn


        #region TbC_Description

        private String _h;
//        public String H
//        {
//            get { return _h; }
//            set { _h = value; }
//        }

        //[NonSerialized]
        public String TbC_Description
        {
            get { return _h; }
            set { _h = value; }
        }

        #endregion TbC_Description


        #region TbC_DtSql_Id

        private Int32 _i;
//        public Int32 I
//        {
//            get { return _i; }
//            set { _i = value; }
//        }

        //[NonSerialized]
        public Int32 TbC_DtSql_Id
        {
            get { return _i; }
            set { _i = value; }
        }

        #endregion TbC_DtSql_Id


        #region TbC_DtSql_Id_TextField

        private String _j;
//        public String J
//        {
//            get { return _j; }
//            set { _j = value; }
//        }

        //[NonSerialized]
        public String TbC_DtSql_Id_TextField
        {
            get { return _j; }
            set { _j = value; }
        }

        #endregion TbC_DtSql_Id_TextField


        #region TbC_DtNt_Id

        private Int32 _k;
//        public Int32 K
//        {
//            get { return _k; }
//            set { _k = value; }
//        }

        //[NonSerialized]
        public Int32 TbC_DtNt_Id
        {
            get { return _k; }
            set { _k = value; }
        }

        #endregion TbC_DtNt_Id


        #region TbC_DtNt_Id_TextField

        private String _l;
//        public String L
//        {
//            get { return _l; }
//            set { _l = value; }
//        }

        //[NonSerialized]
        public String TbC_DtNt_Id_TextField
        {
            get { return _l; }
            set { _l = value; }
        }

        #endregion TbC_DtNt_Id_TextField


        #region TbC_Length

        private Int32? _m;
//        public Int32? M
//        {
//            get { return _m; }
//            set { _m = value; }
//        }

        //[NonSerialized]
        public Int32? TbC_Length
        {
            get { return _m; }
            set { _m = value; }
        }

        #endregion TbC_Length


        #region TbC_Precision

        private Int32? _n;
//        public Int32? N
//        {
//            get { return _n; }
//            set { _n = value; }
//        }

        //[NonSerialized]
        public Int32? TbC_Precision
        {
            get { return _n; }
            set { _n = value; }
        }

        #endregion TbC_Precision


        #region TbC_Scale

        private Int32? _o;
//        public Int32? O
//        {
//            get { return _o; }
//            set { _o = value; }
//        }

        //[NonSerialized]
        public Int32? TbC_Scale
        {
            get { return _o; }
            set { _o = value; }
        }

        #endregion TbC_Scale


        #region TbC_IsPK

        private Boolean _p;
//        public Boolean P
//        {
//            get { return _p; }
//            set { _p = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsPK
        {
            get { return _p; }
            set { _p = value; }
        }

        #endregion TbC_IsPK


        #region TbC_IsIdentity

        private Boolean _q;
//        public Boolean Q
//        {
//            get { return _q; }
//            set { _q = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsIdentity
        {
            get { return _q; }
            set { _q = value; }
        }

        #endregion TbC_IsIdentity


        #region TbC_IsFK

        private Boolean _r;
//        public Boolean R
//        {
//            get { return _r; }
//            set { _r = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsFK
        {
            get { return _r; }
            set { _r = value; }
        }

        #endregion TbC_IsFK


        #region TbC_IsEntityColumn

        private Boolean _s;
//        public Boolean S
//        {
//            get { return _s; }
//            set { _s = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsEntityColumn
        {
            get { return _s; }
            set { _s = value; }
        }

        #endregion TbC_IsEntityColumn


        #region TbC_IsSystemField

        private Boolean _t;
//        public Boolean T
//        {
//            get { return _t; }
//            set { _t = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsSystemField
        {
            get { return _t; }
            set { _t = value; }
        }

        #endregion TbC_IsSystemField


        #region TbC_IsValuesObjectMember

        private Boolean _u;
//        public Boolean U
//        {
//            get { return _u; }
//            set { _u = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsValuesObjectMember
        {
            get { return _u; }
            set { _u = value; }
        }

        #endregion TbC_IsValuesObjectMember


        #region TbC_IsInPropsScreen

        private Boolean _v;
//        public Boolean V
//        {
//            get { return _v; }
//            set { _v = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsInPropsScreen
        {
            get { return _v; }
            set { _v = value; }
        }

        #endregion TbC_IsInPropsScreen


        #region TbC_IsInNavList

        private Boolean _w;
//        public Boolean W
//        {
//            get { return _w; }
//            set { _w = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsInNavList
        {
            get { return _w; }
            set { _w = value; }
        }

        #endregion TbC_IsInNavList


        #region TbC_IsRequired

        private Boolean _x;
//        public Boolean X
//        {
//            get { return _x; }
//            set { _x = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsRequired
        {
            get { return _x; }
            set { _x = value; }
        }

        #endregion TbC_IsRequired


        #region TbC_BrokenRuleText

        private String _y;
//        public String Y
//        {
//            get { return _y; }
//            set { _y = value; }
//        }

        //[NonSerialized]
        public String TbC_BrokenRuleText
        {
            get { return _y; }
            set { _y = value; }
        }

        #endregion TbC_BrokenRuleText


        #region TbC_AllowZero

        private Boolean _z;
//        public Boolean Z
//        {
//            get { return _z; }
//            set { _z = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_AllowZero
        {
            get { return _z; }
            set { _z = value; }
        }

        #endregion TbC_AllowZero


        #region TbC_IsNullableInDb

        private Boolean _aa;
//        public Boolean AA
//        {
//            get { return _aa; }
//            set { _aa = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsNullableInDb
        {
            get { return _aa; }
            set { _aa = value; }
        }

        #endregion TbC_IsNullableInDb


        #region TbC_IsNullableInUI

        private Boolean _ab;
//        public Boolean AB
//        {
//            get { return _ab; }
//            set { _ab = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsNullableInUI
        {
            get { return _ab; }
            set { _ab = value; }
        }

        #endregion TbC_IsNullableInUI


        #region TbC_DefaultValue

        private String _ac;
//        public String AC
//        {
//            get { return _ac; }
//            set { _ac = value; }
//        }

        //[NonSerialized]
        public String TbC_DefaultValue
        {
            get { return _ac; }
            set { _ac = value; }
        }

        #endregion TbC_DefaultValue


        #region TbC_IsReadFromDb

        private Boolean _ad;
//        public Boolean AD
//        {
//            get { return _ad; }
//            set { _ad = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsReadFromDb
        {
            get { return _ad; }
            set { _ad = value; }
        }

        #endregion TbC_IsReadFromDb


        #region TbC_IsSendToDb

        private Boolean _ae;
//        public Boolean AE
//        {
//            get { return _ae; }
//            set { _ae = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsSendToDb
        {
            get { return _ae; }
            set { _ae = value; }
        }

        #endregion TbC_IsSendToDb


        #region TbC_IsInsertAllowed

        private Boolean _af;
//        public Boolean AF
//        {
//            get { return _af; }
//            set { _af = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsInsertAllowed
        {
            get { return _af; }
            set { _af = value; }
        }

        #endregion TbC_IsInsertAllowed


        #region TbC_IsEditAllowed

        private Boolean _ag;
//        public Boolean AG
//        {
//            get { return _ag; }
//            set { _ag = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsEditAllowed
        {
            get { return _ag; }
            set { _ag = value; }
        }

        #endregion TbC_IsEditAllowed


        #region TbC_IsReadOnlyInUI

        private Boolean _ah;
//        public Boolean AH
//        {
//            get { return _ah; }
//            set { _ah = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsReadOnlyInUI
        {
            get { return _ah; }
            set { _ah = value; }
        }

        #endregion TbC_IsReadOnlyInUI


        #region TbC_UseForAudit

        private Boolean _ai;
//        public Boolean AI
//        {
//            get { return _ai; }
//            set { _ai = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_UseForAudit
        {
            get { return _ai; }
            set { _ai = value; }
        }

        #endregion TbC_UseForAudit


        #region TbC_DefaultCaption

        private String _aj;
//        public String AJ
//        {
//            get { return _aj; }
//            set { _aj = value; }
//        }

        //[NonSerialized]
        public String TbC_DefaultCaption
        {
            get { return _aj; }
            set { _aj = value; }
        }

        #endregion TbC_DefaultCaption


        #region TbC_ColumnHeaderText

        private String _ak;
//        public String AK
//        {
//            get { return _ak; }
//            set { _ak = value; }
//        }

        //[NonSerialized]
        public String TbC_ColumnHeaderText
        {
            get { return _ak; }
            set { _ak = value; }
        }

        #endregion TbC_ColumnHeaderText


        #region TbC_LabelCaptionVerbose

        private String _al;
//        public String AL
//        {
//            get { return _al; }
//            set { _al = value; }
//        }

        //[NonSerialized]
        public String TbC_LabelCaptionVerbose
        {
            get { return _al; }
            set { _al = value; }
        }

        #endregion TbC_LabelCaptionVerbose


        #region TbC_LabelCaptionGenerate

        private Boolean _am;
//        public Boolean AM
//        {
//            get { return _am; }
//            set { _am = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_LabelCaptionGenerate
        {
            get { return _am; }
            set { _am = value; }
        }

        #endregion TbC_LabelCaptionGenerate


        #region Tbc_ShowGridColumnToolTip

        private Boolean _an;
//        public Boolean AN
//        {
//            get { return _an; }
//            set { _an = value; }
//        }

        //[NonSerialized]
        public Boolean Tbc_ShowGridColumnToolTip
        {
            get { return _an; }
            set { _an = value; }
        }

        #endregion Tbc_ShowGridColumnToolTip


        #region Tbc_ShowPropsToolTip

        private Boolean _ao;
//        public Boolean AO
//        {
//            get { return _ao; }
//            set { _ao = value; }
//        }

        //[NonSerialized]
        public Boolean Tbc_ShowPropsToolTip
        {
            get { return _ao; }
            set { _ao = value; }
        }

        #endregion Tbc_ShowPropsToolTip


        #region Tbc_TooltipsRolledUp

        private String _ap;
//        public String AP
//        {
//            get { return _ap; }
//            set { _ap = value; }
//        }

        //[NonSerialized]
        public String Tbc_TooltipsRolledUp
        {
            get { return _ap; }
            set { _ap = value; }
        }

        #endregion Tbc_TooltipsRolledUp


        #region TbC_IsCreatePropsStrings

        private Boolean _aq;
//        public Boolean AQ
//        {
//            get { return _aq; }
//            set { _aq = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsCreatePropsStrings
        {
            get { return _aq; }
            set { _aq = value; }
        }

        #endregion TbC_IsCreatePropsStrings


        #region TbC_IsCreateGridStrings

        private Boolean _ar;
//        public Boolean AR
//        {
//            get { return _ar; }
//            set { _ar = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsCreateGridStrings
        {
            get { return _ar; }
            set { _ar = value; }
        }

        #endregion TbC_IsCreateGridStrings


        #region TbC_IsAvailableForColumnGroups

        private Boolean _asx;
//        public Boolean ASX
//        {
//            get { return _asx; }
//            set { _asx = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsAvailableForColumnGroups
        {
            get { return _asx; }
            set { _asx = value; }
        }

        #endregion TbC_IsAvailableForColumnGroups


        #region TbC_IsTextWrapInProp

        private Boolean _at;
//        public Boolean AT
//        {
//            get { return _at; }
//            set { _at = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsTextWrapInProp
        {
            get { return _at; }
            set { _at = value; }
        }

        #endregion TbC_IsTextWrapInProp


        #region TbC_IsTextWrapInGrid

        private Boolean _au;
//        public Boolean AU
//        {
//            get { return _au; }
//            set { _au = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsTextWrapInGrid
        {
            get { return _au; }
            set { _au = value; }
        }

        #endregion TbC_IsTextWrapInGrid


        #region TbC_TextBoxFormat

        private String _av;
//        public String AV
//        {
//            get { return _av; }
//            set { _av = value; }
//        }

        //[NonSerialized]
        public String TbC_TextBoxFormat
        {
            get { return _av; }
            set { _av = value; }
        }

        #endregion TbC_TextBoxFormat


        #region TbC_TextColumnFormat

        private String _aw;
//        public String AW
//        {
//            get { return _aw; }
//            set { _aw = value; }
//        }

        //[NonSerialized]
        public String TbC_TextColumnFormat
        {
            get { return _aw; }
            set { _aw = value; }
        }

        #endregion TbC_TextColumnFormat


        #region TbC_ColumnWidth

        private Double? _ax;
//        public Double? AX
//        {
//            get { return _ax; }
//            set { _ax = value; }
//        }

        //[NonSerialized]
        public Double? TbC_ColumnWidth
        {
            get { return _ax; }
            set { _ax = value; }
        }

        #endregion TbC_ColumnWidth


        #region TbC_TextBoxTextAlignment

        private String _ay;
//        public String AY
//        {
//            get { return _ay; }
//            set { _ay = value; }
//        }

        //[NonSerialized]
        public String TbC_TextBoxTextAlignment
        {
            get { return _ay; }
            set { _ay = value; }
        }

        #endregion TbC_TextBoxTextAlignment


        #region TbC_ColumnTextAlignment

        private String _az;
//        public String AZ
//        {
//            get { return _az; }
//            set { _az = value; }
//        }

        //[NonSerialized]
        public String TbC_ColumnTextAlignment
        {
            get { return _az; }
            set { _az = value; }
        }

        #endregion TbC_ColumnTextAlignment


        #region TbC_ListStoredProc_Id

        private Guid? _ba;
//        public Guid? BA
//        {
//            get { return _ba; }
//            set { _ba = value; }
//        }

        //[NonSerialized]
        public Guid? TbC_ListStoredProc_Id
        {
            get { return _ba; }
            set { _ba = value; }
        }

        #endregion TbC_ListStoredProc_Id


        #region TbC_ListStoredProc_Id_TextField

        private String _bb;
//        public String BB
//        {
//            get { return _bb; }
//            set { _bb = value; }
//        }

        //[NonSerialized]
        public String TbC_ListStoredProc_Id_TextField
        {
            get { return _bb; }
            set { _bb = value; }
        }

        #endregion TbC_ListStoredProc_Id_TextField


        #region TbC_IsStaticList

        private Boolean _bc;
//        public Boolean BC
//        {
//            get { return _bc; }
//            set { _bc = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsStaticList
        {
            get { return _bc; }
            set { _bc = value; }
        }

        #endregion TbC_IsStaticList


        #region TbC_UseNotInList

        private Boolean _bd;
//        public Boolean BD
//        {
//            get { return _bd; }
//            set { _bd = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_UseNotInList
        {
            get { return _bd; }
            set { _bd = value; }
        }

        #endregion TbC_UseNotInList


        #region TbC_UseListEditBtn

        private Boolean _be;
//        public Boolean BE
//        {
//            get { return _be; }
//            set { _be = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_UseListEditBtn
        {
            get { return _be; }
            set { _be = value; }
        }

        #endregion TbC_UseListEditBtn


        #region TbC_ParentColumnKey

        private Guid? _bf;
//        public Guid? BF
//        {
//            get { return _bf; }
//            set { _bf = value; }
//        }

        //[NonSerialized]
        public Guid? TbC_ParentColumnKey
        {
            get { return _bf; }
            set { _bf = value; }
        }

        #endregion TbC_ParentColumnKey


        #region TbC_ParentColumnKey_TextField

        private String _bg;
//        public String BG
//        {
//            get { return _bg; }
//            set { _bg = value; }
//        }

        //[NonSerialized]
        public String TbC_ParentColumnKey_TextField
        {
            get { return _bg; }
            set { _bg = value; }
        }

        #endregion TbC_ParentColumnKey_TextField


        #region TbC_UseDisplayTextFieldProperty

        private Boolean _bh;
//        public Boolean BH
//        {
//            get { return _bh; }
//            set { _bh = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_UseDisplayTextFieldProperty
        {
            get { return _bh; }
            set { _bh = value; }
        }

        #endregion TbC_UseDisplayTextFieldProperty


        #region TbC_IsDisplayTextFieldProperty

        private Boolean _bi;
//        public Boolean BI
//        {
//            get { return _bi; }
//            set { _bi = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsDisplayTextFieldProperty
        {
            get { return _bi; }
            set { _bi = value; }
        }

        #endregion TbC_IsDisplayTextFieldProperty


        #region TbC_ComboListTable_Id

        private Guid? _bj;
//        public Guid? BJ
//        {
//            get { return _bj; }
//            set { _bj = value; }
//        }

        //[NonSerialized]
        public Guid? TbC_ComboListTable_Id
        {
            get { return _bj; }
            set { _bj = value; }
        }

        #endregion TbC_ComboListTable_Id


        #region TbC_ComboListTable_Id_TextField

        private String _bk;
//        public String BK
//        {
//            get { return _bk; }
//            set { _bk = value; }
//        }

        //[NonSerialized]
        public String TbC_ComboListTable_Id_TextField
        {
            get { return _bk; }
            set { _bk = value; }
        }

        #endregion TbC_ComboListTable_Id_TextField


        #region TbC_ComboListTable_Id_PkName

        private String _bl;
//        public String BL
//        {
//            get { return _bl; }
//            set { _bl = value; }
//        }

        //[NonSerialized]
        public String TbC_ComboListTable_Id_PkName
        {
            get { return _bl; }
            set { _bl = value; }
        }

        #endregion TbC_ComboListTable_Id_PkName


        #region TbC_ComboListDisplayColumn_Id

        private Guid? _bm;
//        public Guid? BM
//        {
//            get { return _bm; }
//            set { _bm = value; }
//        }

        //[NonSerialized]
        public Guid? TbC_ComboListDisplayColumn_Id
        {
            get { return _bm; }
            set { _bm = value; }
        }

        #endregion TbC_ComboListDisplayColumn_Id


        #region TbC_ComboListDisplayColumn_Id_TextField

        private String _bn;
//        public String BN
//        {
//            get { return _bn; }
//            set { _bn = value; }
//        }

        //[NonSerialized]
        public String TbC_ComboListDisplayColumn_Id_TextField
        {
            get { return _bn; }
            set { _bn = value; }
        }

        #endregion TbC_ComboListDisplayColumn_Id_TextField


        #region TbC_Combo_MaxDropdownHeight

        private Double? _bo;
//        public Double? BO
//        {
//            get { return _bo; }
//            set { _bo = value; }
//        }

        //[NonSerialized]
        public Double? TbC_Combo_MaxDropdownHeight
        {
            get { return _bo; }
            set { _bo = value; }
        }

        #endregion TbC_Combo_MaxDropdownHeight


        #region TbC_Combo_MaxDropdownWidth

        private Double? _bp;
//        public Double? BP
//        {
//            get { return _bp; }
//            set { _bp = value; }
//        }

        //[NonSerialized]
        public Double? TbC_Combo_MaxDropdownWidth
        {
            get { return _bp; }
            set { _bp = value; }
        }

        #endregion TbC_Combo_MaxDropdownWidth


        #region TbC_Combo_AllowDropdownResizing

        private Boolean _bq;
//        public Boolean BQ
//        {
//            get { return _bq; }
//            set { _bq = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_Combo_AllowDropdownResizing
        {
            get { return _bq; }
            set { _bq = value; }
        }

        #endregion TbC_Combo_AllowDropdownResizing


        #region TbC_Combo_IsResetButtonVisible

        private Boolean _br;
//        public Boolean BR
//        {
//            get { return _br; }
//            set { _br = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_Combo_IsResetButtonVisible
        {
            get { return _br; }
            set { _br = value; }
        }

        #endregion TbC_Combo_IsResetButtonVisible


        #region TbC_IsActiveRecColumn

        private Boolean _bs;
//        public Boolean BS
//        {
//            get { return _bs; }
//            set { _bs = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsActiveRecColumn
        {
            get { return _bs; }
            set { _bs = value; }
        }

        #endregion TbC_IsActiveRecColumn


        #region TbC_IsDeletedColumn

        private Boolean _bt;
//        public Boolean BT
//        {
//            get { return _bt; }
//            set { _bt = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsDeletedColumn
        {
            get { return _bt; }
            set { _bt = value; }
        }

        #endregion TbC_IsDeletedColumn


        #region TbC_IsCreatedUserIdColumn

        private Boolean _bu;
//        public Boolean BU
//        {
//            get { return _bu; }
//            set { _bu = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsCreatedUserIdColumn
        {
            get { return _bu; }
            set { _bu = value; }
        }

        #endregion TbC_IsCreatedUserIdColumn


        #region TbC_IsCreatedDateColumn

        private Boolean _bv;
//        public Boolean BV
//        {
//            get { return _bv; }
//            set { _bv = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsCreatedDateColumn
        {
            get { return _bv; }
            set { _bv = value; }
        }

        #endregion TbC_IsCreatedDateColumn


        #region TbC_IsUserIdColumn

        private Boolean _bw;
//        public Boolean BW
//        {
//            get { return _bw; }
//            set { _bw = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsUserIdColumn
        {
            get { return _bw; }
            set { _bw = value; }
        }

        #endregion TbC_IsUserIdColumn


        #region TbC_IsModifiedDateColumn

        private Boolean _bx;
//        public Boolean BX
//        {
//            get { return _bx; }
//            set { _bx = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsModifiedDateColumn
        {
            get { return _bx; }
            set { _bx = value; }
        }

        #endregion TbC_IsModifiedDateColumn


        #region TbC_IsRowVersionStampColumn

        private Boolean _by;
//        public Boolean BY
//        {
//            get { return _by; }
//            set { _by = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsRowVersionStampColumn
        {
            get { return _by; }
            set { _by = value; }
        }

        #endregion TbC_IsRowVersionStampColumn


        #region TbC_IsBrowsable

        private Boolean _bz;
//        public Boolean BZ
//        {
//            get { return _bz; }
//            set { _bz = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsBrowsable
        {
            get { return _bz; }
            set { _bz = value; }
        }

        #endregion TbC_IsBrowsable


        #region TbC_DeveloperNote

        private String _ca;
//        public String CA
//        {
//            get { return _ca; }
//            set { _ca = value; }
//        }

        //[NonSerialized]
        public String TbC_DeveloperNote
        {
            get { return _ca; }
            set { _ca = value; }
        }

        #endregion TbC_DeveloperNote


        #region TbC_HelpFileAdditionalNote

        private String _cb;
//        public String CB
//        {
//            get { return _cb; }
//            set { _cb = value; }
//        }

        //[NonSerialized]
        public String TbC_HelpFileAdditionalNote
        {
            get { return _cb; }
            set { _cb = value; }
        }

        #endregion TbC_HelpFileAdditionalNote


        #region TbC_IsInputComplete

        private Boolean _cc;
//        public Boolean CC
//        {
//            get { return _cc; }
//            set { _cc = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsInputComplete
        {
            get { return _cc; }
            set { _cc = value; }
        }

        #endregion TbC_IsInputComplete


        #region TbC_IsCodeGen

        private Boolean _cd;
//        public Boolean CD
//        {
//            get { return _cd; }
//            set { _cd = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsCodeGen
        {
            get { return _cd; }
            set { _cd = value; }
        }

        #endregion TbC_IsCodeGen


        #region TbC_IsReadyCodeGen

        private Boolean _ce;
//        public Boolean CE
//        {
//            get { return _ce; }
//            set { _ce = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsReadyCodeGen
        {
            get { return _ce; }
            set { _ce = value; }
        }

        #endregion TbC_IsReadyCodeGen


        #region TbC_IsCodeGenComplete

        private Boolean _cf;
//        public Boolean CF
//        {
//            get { return _cf; }
//            set { _cf = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsCodeGenComplete
        {
            get { return _cf; }
            set { _cf = value; }
        }

        #endregion TbC_IsCodeGenComplete


        #region TbC_IsTagForCodeGen

        private Boolean _cg;
//        public Boolean CG
//        {
//            get { return _cg; }
//            set { _cg = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsTagForCodeGen
        {
            get { return _cg; }
            set { _cg = value; }
        }

        #endregion TbC_IsTagForCodeGen


        #region TbC_IsTagForOther

        private Boolean _ch;
//        public Boolean CH
//        {
//            get { return _ch; }
//            set { _ch = value; }
//        }

        //[NonSerialized]
        public Boolean TbC_IsTagForOther
        {
            get { return _ch; }
            set { _ch = value; }
        }

        #endregion TbC_IsTagForOther


        #region TbC_ColumnGroups

        private String _ci;
//        public String CI
//        {
//            get { return _ci; }
//            set { _ci = value; }
//        }

        //[NonSerialized]
        public String TbC_ColumnGroups
        {
            get { return _ci; }
            set { _ci = value; }
        }

        #endregion TbC_ColumnGroups


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11}; {12}; {13}; {14}; {15}; {16}; {17}; {18}; {19}; {20}; {21}; {22}; {23}; {24}; {25}; {26}; {27}; {28}; {29}; {30}; {31}; {32}; {33}; {34}; {35}; {36}; {37}; {38}; {39}; {40}; {41}; {42}; {43}; {44}; {45}; {46}; {47}; {48}; {49}; {50}; {51}; {52}; {53}; {54}; {55}; {56}; {57}; {58}; {59}; {60}; {61}; {62}; {63}; {64}; {65}; {66}; {67}; {68}; {69}; {70}; {71}; {72}; {73}; {74}; {75}; {76}; {77}; {78}; {79}; {80}; {81}; {82}; {83}; {84}; {85}; {86} ", TbC_Id, TbC_Tb_Id, TbC_SortOrder, TbC_Name, TbC_CtlTp_ID, TbC_CtlTp_ID_TextField, TbC_IsNonvColumn, TbC_Description, TbC_DtSql_Id, TbC_DtSql_Id_TextField, TbC_DtNt_Id, TbC_DtNt_Id_TextField, TbC_Length, TbC_Precision, TbC_Scale, TbC_IsPK, TbC_IsIdentity, TbC_IsFK, TbC_IsEntityColumn, TbC_IsSystemField, TbC_IsValuesObjectMember, TbC_IsInPropsScreen, TbC_IsInNavList, TbC_IsRequired, TbC_BrokenRuleText, TbC_AllowZero, TbC_IsNullableInDb, TbC_IsNullableInUI, TbC_DefaultValue, TbC_IsReadFromDb, TbC_IsSendToDb, TbC_IsInsertAllowed, TbC_IsEditAllowed, TbC_IsReadOnlyInUI, TbC_UseForAudit, TbC_DefaultCaption, TbC_ColumnHeaderText, TbC_LabelCaptionVerbose, TbC_LabelCaptionGenerate, Tbc_ShowGridColumnToolTip, Tbc_ShowPropsToolTip, Tbc_TooltipsRolledUp, TbC_IsCreatePropsStrings, TbC_IsCreateGridStrings, TbC_IsAvailableForColumnGroups, TbC_IsTextWrapInProp, TbC_IsTextWrapInGrid, TbC_TextBoxFormat, TbC_TextColumnFormat, TbC_ColumnWidth, TbC_TextBoxTextAlignment, TbC_ColumnTextAlignment, TbC_ListStoredProc_Id, TbC_ListStoredProc_Id_TextField, TbC_IsStaticList, TbC_UseNotInList, TbC_UseListEditBtn, TbC_ParentColumnKey, TbC_ParentColumnKey_TextField, TbC_UseDisplayTextFieldProperty, TbC_IsDisplayTextFieldProperty, TbC_ComboListTable_Id, TbC_ComboListTable_Id_TextField, TbC_ComboListTable_Id_PkName, TbC_ComboListDisplayColumn_Id, TbC_ComboListDisplayColumn_Id_TextField, TbC_Combo_MaxDropdownHeight, TbC_Combo_MaxDropdownWidth, TbC_Combo_AllowDropdownResizing, TbC_Combo_IsResetButtonVisible, TbC_IsActiveRecColumn, TbC_IsDeletedColumn, TbC_IsCreatedUserIdColumn, TbC_IsCreatedDateColumn, TbC_IsUserIdColumn, TbC_IsModifiedDateColumn, TbC_IsRowVersionStampColumn, TbC_IsBrowsable, TbC_DeveloperNote, TbC_HelpFileAdditionalNote, TbC_IsInputComplete, TbC_IsCodeGen, TbC_IsReadyCodeGen, TbC_IsCodeGenComplete, TbC_IsTagForCodeGen, TbC_IsTagForOther, TbC_ColumnGroups );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}; {10}; {11}; {12}; {13}; {14}; {15}; {16}; {17}; {18}; {19}; {20}; {21}; {22}; {23}; {24}; {25}; {26}; {27}; {28}; {29}; {30}; {31}; {32}; {33}; {34}; {35}; {36}; {37}; {38}; {39}; {40}; {41}; {42}; {43}; {44}; {45}; {46}; {47}; {48}; {49}; {50}; {51}; {52}; {53}; {54}; {55}; {56}; {57}; {58}; {59}; {60}; {61}; {62}; {63}; {64}; {65}; {66}; {67}; {68}; {69}; {70}; {71}; {72}; {73}; {74}; {75}; {76}; {77}; {78}; {79}; {80}; {81}; {82}; {83}; {84}; {85}; {86} ", TbC_Id, TbC_Tb_Id, TbC_SortOrder, TbC_Name, TbC_CtlTp_ID, TbC_CtlTp_ID_TextField, TbC_IsNonvColumn, TbC_Description, TbC_DtSql_Id, TbC_DtSql_Id_TextField, TbC_DtNt_Id, TbC_DtNt_Id_TextField, TbC_Length, TbC_Precision, TbC_Scale, TbC_IsPK, TbC_IsIdentity, TbC_IsFK, TbC_IsEntityColumn, TbC_IsSystemField, TbC_IsValuesObjectMember, TbC_IsInPropsScreen, TbC_IsInNavList, TbC_IsRequired, TbC_BrokenRuleText, TbC_AllowZero, TbC_IsNullableInDb, TbC_IsNullableInUI, TbC_DefaultValue, TbC_IsReadFromDb, TbC_IsSendToDb, TbC_IsInsertAllowed, TbC_IsEditAllowed, TbC_IsReadOnlyInUI, TbC_UseForAudit, TbC_DefaultCaption, TbC_ColumnHeaderText, TbC_LabelCaptionVerbose, TbC_LabelCaptionGenerate, Tbc_ShowGridColumnToolTip, Tbc_ShowPropsToolTip, Tbc_TooltipsRolledUp, TbC_IsCreatePropsStrings, TbC_IsCreateGridStrings, TbC_IsAvailableForColumnGroups, TbC_IsTextWrapInProp, TbC_IsTextWrapInGrid, TbC_TextBoxFormat, TbC_TextColumnFormat, TbC_ColumnWidth, TbC_TextBoxTextAlignment, TbC_ColumnTextAlignment, TbC_ListStoredProc_Id, TbC_ListStoredProc_Id_TextField, TbC_IsStaticList, TbC_UseNotInList, TbC_UseListEditBtn, TbC_ParentColumnKey, TbC_ParentColumnKey_TextField, TbC_UseDisplayTextFieldProperty, TbC_IsDisplayTextFieldProperty, TbC_ComboListTable_Id, TbC_ComboListTable_Id_TextField, TbC_ComboListTable_Id_PkName, TbC_ComboListDisplayColumn_Id, TbC_ComboListDisplayColumn_Id_TextField, TbC_Combo_MaxDropdownHeight, TbC_Combo_MaxDropdownWidth, TbC_Combo_AllowDropdownResizing, TbC_Combo_IsResetButtonVisible, TbC_IsActiveRecColumn, TbC_IsDeletedColumn, TbC_IsCreatedUserIdColumn, TbC_IsCreatedDateColumn, TbC_IsUserIdColumn, TbC_IsModifiedDateColumn, TbC_IsRowVersionStampColumn, TbC_IsBrowsable, TbC_DeveloperNote, TbC_HelpFileAdditionalNote, TbC_IsInputComplete, TbC_IsCodeGen, TbC_IsReadyCodeGen, TbC_IsCodeGenComplete, TbC_IsTagForCodeGen, TbC_IsTagForOther, TbC_ColumnGroups );
        }

    }

}
