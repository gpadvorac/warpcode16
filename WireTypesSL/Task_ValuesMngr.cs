using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  6/18/2016 5:31:36 PM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class Task_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "Task_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public Task_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false), false) 
        {
        }

//        public Task_ValuesMngr(SqlDataReader reader)
//        {
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task_ValuesMngr", IfxTraceCategory.Enter);
//                _current = new Task_Values(reader, this);
//                _original = null;
//                _concurrent = null;
//                _state.SetNotNew();
//                _state.SetNotDirty();
//                _state.SetValid();
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
//            }
//        }


        public Task_ValuesMngr(object[] currentData, EntityState state) 
            : this(currentData, null, state, false) { }
            //: this(currentData, currentData, state) { }
            //: this(currentData, null, state) { }

        public Task_ValuesMngr(object[] currentData, EntityState state, bool isPartialLoad)
            : this(currentData, null, state, isPartialLoad) { }

        private Task_ValuesMngr(object[] currentData, object[] originalData, EntityState state, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new Task_Values(currentData, this, isPartialLoad) : new Task_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (originalData != null ? new Task_Values(originalData, this) : null);
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected Task_Values _original;
        [DataMember]
        public Task_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected Task_Values _current;
        [DataMember]
        public Task_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected Task_Values _concurrent;
        [DataMember]
        public Task_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  Tk_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  Tk_Prj_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  Tk_Tk_Id
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  AttachmentCount
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  AttachmentFileNames
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  DiscussionCount
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  DiscussionTitles
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  Tk_TkTp_Id
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  Tk_TkTp_Id_TextField
                //if (_current._i != _original._i)
                //{
                //    return true;
                //}

                //  Tk_TkCt_Id
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  Tk_TkCt_Id_TextField
                //if (_current._k != _original._k)
                //{
                //    return true;
                //}

                //  CategoryDescription
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  Tk_Number
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  Tk_Name
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  Tk_Desc
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  Tk_EstimatedTime
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  Tk_ActualTime
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  Tk_PercentComplete
                if (_current._r != _original._r)
                {
                    return true;
                }

                //  Tk_StartDate
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  Tk_Deadline
                if (_current._t != _original._t)
                {
                    return true;
                }

                //  Tk_TkSt_Id
                if (_current._u != _original._u)
                {
                    return true;
                }

                //  Tk_TkSt_Id_TextField
                //if (_current._v != _original._v)
                //{
                //    return true;
                //}

                //  Tk_TkPr_Id
                if (_current._w != _original._w)
                {
                    return true;
                }

                //  Tk_TkPr_Id_TextField
                //if (_current._x != _original._x)
                //{
                //    return true;
                //}

                //  Tk_Results
                if (_current._y != _original._y)
                {
                    return true;
                }

                //  Tk_Remarks
                if (_current._z != _original._z)
                {
                    return true;
                }

                //  Tk_Assignees
                if (_current._aa != _original._aa)
                {
                    return true;
                }

                //  Tk_IsPrivate
                if (_current._ab != _original._ab)
                {
                    return true;
                }

                //  Tk_IsActiveRow
                if (_current._ac != _original._ac)
                {
                    return true;
                }

                //  Tk_IsDeleted
                if (_current._ad != _original._ad)
                {
                    return true;
                }

                //  Tk_CreatedUserId
                if (_current._ae != _original._ae)
                {
                    return true;
                }

                //  Tk_CreatedDate
                if (_current._af != _original._af)
                {
                    return true;
                }

                //  Tk_UserId
                if (_current._ag != _original._ag)
                {
                    return true;
                }

                //  UserName
                if (_current._ah != _original._ah)
                {
                    return true;
                }

                //  Tk_LastModifiedDate
                if (_current._ai != _original._ai)
                {
                    return true;
                }

                //  Tk_Stamp
                if (_current._aj != _original._aj)
                {
                    return true;
                }

                //  Ct_Id
                if (_current._ak != _original._ak)
                {
                    return true;
                }

                //  CtD_Id
                if (_current._al != _original._al)
                {
                    return true;
                }

                //  Df_Id
                if (_current._am != _original._am)
                {
                    return true;
                }

                //  Ls_Id
                if (_current._an != _original._an)
                {
                    return true;
                }

                //  LsTr_Id
                if (_current._ao != _original._ao)
                {
                    return true;
                }

                //  Ob_Id
                if (_current._ap != _original._ap)
                {
                    return true;
                }

                //  Pp_Id
                if (_current._aq != _original._aq)
                {
                    return true;
                }

                //  Wl_Id
                if (_current._ar != _original._ar)
                {
                    return true;
                }

                //  RCt_Id
                if (_current._asxx != _original._asxx)
                {
                    return true;
                }

                return false;
            }
        }

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Enter);
              //NOTE:  This method is the ONLY method that should call the one in the values object below.
                
                if (_original != null)
                {
                    _original.FinishPartialLoad();
                }
                if (_current != null)
                {
                    _current.FinishPartialLoad();
                }
                if (_concurrent != null)
                {
                    _concurrent.FinishPartialLoad();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
        }

        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{46}{0}{45}_b{46}{1}{45}_c{46}{2}{45}_d{46}{3}{45}_e{46}{4}{45}_f{46}{5}{45}_g{46}{6}{45}_h{46}{7}{45}_i{46}{8}{45}_j{46}{9}{45}_k{46}{10}{45}_l{46}{11}{45}_m{46}{12}{45}_n{46}{13}{45}_o{46}{14}{45}_p{46}{15}{45}_q{46}{16}{45}_r{46}{17}{45}_s{46}{18}{45}_t{46}{19}{45}_u{46}{20}{45}_v{46}{21}{45}_w{46}{22}{45}_x{46}{23}{45}_y{46}{24}{45}_z{46}{25}{45}_aa{46}{26}{45}_ab{46}{27}{45}_ac{46}{28}{45}_ad{46}{29}{45}_ae{46}{30}{45}_af{46}{31}{45}_ag{46}{32}{45}_ah{46}{33}{45}_ai{46}{34}{45}_aj{46}{35}{45}_ak{46}{36}{45}_al{46}{37}{45}_am{46}{38}{45}_an{46}{39}{45}_ao{46}{40}{45}_ap{46}{41}{45}_aq{46}{42}{45}_ar{46}{43}{45}_asxx{46}{44}",
				new object[] {
				_current._a,		  //Tk_Id
				_current._b,		  //Tk_Prj_Id
				_current._c,		  //Tk_Tk_Id
				_current._d,		  //AttachmentCount
				_current._e,		  //AttachmentFileNames
				_current._f,		  //DiscussionCount
				_current._g,		  //DiscussionTitles
				_current._h,		  //Tk_TkTp_Id
				_current._i,		  //Tk_TkTp_Id_TextField
				_current._j,		  //Tk_TkCt_Id
				_current._k,		  //Tk_TkCt_Id_TextField
				_current._l,		  //CategoryDescription
				_current._m,		  //Tk_Number
				_current._n,		  //Tk_Name
				_current._o,		  //Tk_Desc
				_current._p,		  //Tk_EstimatedTime
				_current._q,		  //Tk_ActualTime
				_current._r,		  //Tk_PercentComplete
				_current._s,		  //Tk_StartDate
				_current._t,		  //Tk_Deadline
				_current._u,		  //Tk_TkSt_Id
				_current._v,		  //Tk_TkSt_Id_TextField
				_current._w,		  //Tk_TkPr_Id
				_current._x,		  //Tk_TkPr_Id_TextField
				_current._y,		  //Tk_Results
				_current._z,		  //Tk_Remarks
				_current._aa,		  //Tk_Assignees
				_current._ab,		  //Tk_IsPrivate
				_current._ac,		  //Tk_IsActiveRow
				_current._ad,		  //Tk_IsDeleted
				_current._ae,		  //Tk_CreatedUserId
				_current._af,		  //Tk_CreatedDate
				_current._ag,		  //Tk_UserId
				_current._ah,		  //UserName
				_current._ai,		  //Tk_LastModifiedDate
				_current._aj,		  //Tk_Stamp
				_current._ak,		  //Ct_Id
				_current._al,		  //CtD_Id
				_current._am,		  //Df_Id
				_current._an,		  //Ls_Id
				_current._ao,		  //LsTr_Id
				_current._ap,		  //Ob_Id
				_current._aq,		  //Pp_Id
				_current._ar,		  //Wl_Id
				_current._asxx,		  //RCt_Id
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class Task_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "Task_Values";
        private Task_ValuesMngr _parent = null;
        private bool _isPartialLoad = false;
        private bool _isPartialDataLoaded = false;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal Task_Values() 
        {
        }

        public Task_Values(object[] data, Task_ValuesMngr parent)
            :this(data, parent, false)
        {  }

        public Task_Values(object[] data, Task_ValuesMngr parent, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
                _isPartialLoad = isPartialLoad;
                // Fields that are part of the parital load which will always be loaded
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Tk_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Tk_Prj_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  Tk_Tk_Id
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  AttachmentCount
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  AttachmentFileNames
				_f = ObjectHelper.GetNullableIntFromObjectValue(data[5]);							//  DiscussionCount
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  DiscussionTitles
				_h = ObjectHelper.GetNullableIntFromObjectValue(data[7]);							//  Tk_TkTp_Id
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  Tk_TkTp_Id_TextField
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  Tk_TkCt_Id
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  Tk_TkCt_Id_TextField
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  CategoryDescription
				_m = ObjectHelper.GetNullableIntFromObjectValue(data[12]);							//  Tk_Number
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  Tk_Name
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  Tk_Desc
				_p = ObjectHelper.GetNullableDecimalFromObjectValue(data[15]);					//  Tk_EstimatedTime
				_q = ObjectHelper.GetNullableDecimalFromObjectValue(data[16]);					//  Tk_ActualTime
				_r = ObjectHelper.GetNullableDecimalFromObjectValue(data[17]);					//  Tk_PercentComplete
				_s = ObjectHelper.GetNullableDateTimeFromObjectValue(data[18]);					//  Tk_StartDate
				_t = ObjectHelper.GetNullableDateTimeFromObjectValue(data[19]);					//  Tk_Deadline
				_u = ObjectHelper.GetNullableIntFromObjectValue(data[20]);							//  Tk_TkSt_Id
				_v = ObjectHelper.GetStringFromObjectValue(data[21]);									//  Tk_TkSt_Id_TextField
				_w = ObjectHelper.GetNullableIntFromObjectValue(data[22]);							//  Tk_TkPr_Id
				_x = ObjectHelper.GetStringFromObjectValue(data[23]);									//  Tk_TkPr_Id_TextField
				_y = ObjectHelper.GetStringFromObjectValue(data[24]);									//  Tk_Results
				_z = ObjectHelper.GetStringFromObjectValue(data[25]);									//  Tk_Remarks
				_aa = ObjectHelper.GetStringFromObjectValue(data[26]);									//  Tk_Assignees
				_ab = ObjectHelper.GetBoolFromObjectValue(data[27]);									//  Tk_IsPrivate
				_ac = ObjectHelper.GetBoolFromObjectValue(data[28]);									//  Tk_IsActiveRow
				_ad = ObjectHelper.GetBoolFromObjectValue(data[29]);									//  Tk_IsDeleted
				_ae = ObjectHelper.GetNullableGuidFromObjectValue(data[30]);						//  Tk_CreatedUserId
				_af = ObjectHelper.GetNullableDateTimeFromObjectValue(data[31]);					//  Tk_CreatedDate
				_ag = ObjectHelper.GetNullableGuidFromObjectValue(data[32]);						//  Tk_UserId
				_ah = ObjectHelper.GetStringFromObjectValue(data[33]);									//  UserName
				_ai = ObjectHelper.GetNullableDateTimeFromObjectValue(data[34]);					//  Tk_LastModifiedDate
				_aj = ObjectHelper.GetByteArrayFromObjectValue(data[35]);						//  Tk_Stamp
				_ak = ObjectHelper.GetNullableGuidFromObjectValue(data[36]);						//  Ct_Id
				_al = ObjectHelper.GetNullableGuidFromObjectValue(data[37]);						//  CtD_Id
				_am = ObjectHelper.GetNullableGuidFromObjectValue(data[38]);						//  Df_Id
				_an = ObjectHelper.GetNullableGuidFromObjectValue(data[39]);						//  Ls_Id
				_ao = ObjectHelper.GetNullableGuidFromObjectValue(data[40]);						//  LsTr_Id
				_ap = ObjectHelper.GetNullableGuidFromObjectValue(data[41]);						//  Ob_Id
				_aq = ObjectHelper.GetNullableGuidFromObjectValue(data[42]);						//  Pp_Id
				_ar = ObjectHelper.GetNullableGuidFromObjectValue(data[43]);						//  Wl_Id
				_asxx = ObjectHelper.GetNullableGuidFromObjectValue(data[44]);						//  RCt_Id
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Task_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - Task", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Tk_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Tk_Prj_Id
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  Tk_Tk_Id
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  AttachmentCount
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  AttachmentFileNames
				_f = ObjectHelper.GetNullableIntFromObjectValue(data[5]);							//  DiscussionCount
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  DiscussionTitles
				_h = ObjectHelper.GetNullableIntFromObjectValue(data[7]);							//  Tk_TkTp_Id
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  Tk_TkTp_Id_TextField
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  Tk_TkCt_Id
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  Tk_TkCt_Id_TextField
				_l = ObjectHelper.GetStringFromObjectValue(data[11]);									//  CategoryDescription
				_m = ObjectHelper.GetNullableIntFromObjectValue(data[12]);							//  Tk_Number
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  Tk_Name
				_o = ObjectHelper.GetStringFromObjectValue(data[14]);									//  Tk_Desc
				_p = ObjectHelper.GetNullableDecimalFromObjectValue(data[15]);					//  Tk_EstimatedTime
				_q = ObjectHelper.GetNullableDecimalFromObjectValue(data[16]);					//  Tk_ActualTime
				_r = ObjectHelper.GetNullableDecimalFromObjectValue(data[17]);					//  Tk_PercentComplete
				_s = ObjectHelper.GetNullableDateTimeFromObjectValue(data[18]);					//  Tk_StartDate
				_t = ObjectHelper.GetNullableDateTimeFromObjectValue(data[19]);					//  Tk_Deadline
				_u = ObjectHelper.GetNullableIntFromObjectValue(data[20]);							//  Tk_TkSt_Id
				_v = ObjectHelper.GetStringFromObjectValue(data[21]);									//  Tk_TkSt_Id_TextField
				_w = ObjectHelper.GetNullableIntFromObjectValue(data[22]);							//  Tk_TkPr_Id
				_x = ObjectHelper.GetStringFromObjectValue(data[23]);									//  Tk_TkPr_Id_TextField
				_y = ObjectHelper.GetStringFromObjectValue(data[24]);									//  Tk_Results
				_z = ObjectHelper.GetStringFromObjectValue(data[25]);									//  Tk_Remarks
				_aa = ObjectHelper.GetStringFromObjectValue(data[26]);									//  Tk_Assignees
				_ab = ObjectHelper.GetBoolFromObjectValue(data[27]);									//  Tk_IsPrivate
				_ac = ObjectHelper.GetBoolFromObjectValue(data[28]);									//  Tk_IsActiveRow
				_ad = ObjectHelper.GetBoolFromObjectValue(data[29]);									//  Tk_IsDeleted
				_ae = ObjectHelper.GetNullableGuidFromObjectValue(data[30]);						//  Tk_CreatedUserId
				_af = ObjectHelper.GetNullableDateTimeFromObjectValue(data[31]);					//  Tk_CreatedDate
				_ag = ObjectHelper.GetNullableGuidFromObjectValue(data[32]);						//  Tk_UserId
				_ah = ObjectHelper.GetStringFromObjectValue(data[33]);									//  UserName
				_ai = ObjectHelper.GetNullableDateTimeFromObjectValue(data[34]);					//  Tk_LastModifiedDate
				_aj = ObjectHelper.GetByteArrayFromObjectValue(data[35]);						//  Tk_Stamp
				_ak = ObjectHelper.GetNullableGuidFromObjectValue(data[36]);						//  Ct_Id
				_al = ObjectHelper.GetNullableGuidFromObjectValue(data[37]);						//  CtD_Id
				_am = ObjectHelper.GetNullableGuidFromObjectValue(data[38]);						//  Df_Id
				_an = ObjectHelper.GetNullableGuidFromObjectValue(data[39]);						//  Ls_Id
				_ao = ObjectHelper.GetNullableGuidFromObjectValue(data[40]);						//  LsTr_Id
				_ap = ObjectHelper.GetNullableGuidFromObjectValue(data[41]);						//  Ob_Id
				_aq = ObjectHelper.GetNullableGuidFromObjectValue(data[42]);						//  Pp_Id
				_ar = ObjectHelper.GetNullableGuidFromObjectValue(data[43]);						//  Wl_Id
				_asxx = ObjectHelper.GetNullableGuidFromObjectValue(data[44]);						//  RCt_Id
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - Task", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - Task", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - Task", IfxTraceCategory.Leave);
				// No fields to load.

                if (_isPartialLoad == true && _isPartialDataLoaded == false)
                {

                    _isPartialDataLoaded = true;
                }
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - Task", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods


		#region Data Members

		[DataMember]
		public Guid _a;			//  Tk_Id

		[DataMember]
		public Guid? _b;			//  Tk_Prj_Id

		[DataMember]
		public Guid? _c;			//  Tk_Tk_Id

		[DataMember]
		public Int32? _d;			//  AttachmentCount

		[DataMember]
		public String _e;			//  AttachmentFileNames

		[DataMember]
		public Int32? _f;			//  DiscussionCount

		[DataMember]
		public String _g;			//  DiscussionTitles

		[DataMember]
		public Int32? _h;			//  Tk_TkTp_Id

		[DataMember]
		public String _i;			//  Tk_TkTp_Id_TextField

		[DataMember]
		public Guid? _j;			//  Tk_TkCt_Id

		[DataMember]
		public String _k;			//  Tk_TkCt_Id_TextField

		[DataMember]
		public String _l;			//  CategoryDescription

		[DataMember]
		public Int32? _m;			//  Tk_Number

		[DataMember]
		public String _n;			//  Tk_Name

		[DataMember]
		public String _o;			//  Tk_Desc

		[DataMember]
		public Decimal? _p;			//  Tk_EstimatedTime

		[DataMember]
		public Decimal? _q;			//  Tk_ActualTime

		[DataMember]
		public Decimal? _r;			//  Tk_PercentComplete

		[DataMember]
		public DateTime? _s;			//  Tk_StartDate

		[DataMember]
		public DateTime? _t;			//  Tk_Deadline

		[DataMember]
		public Int32? _u;			//  Tk_TkSt_Id

		[DataMember]
		public String _v;			//  Tk_TkSt_Id_TextField

		[DataMember]
		public Int32? _w;			//  Tk_TkPr_Id

		[DataMember]
		public String _x;			//  Tk_TkPr_Id_TextField

		[DataMember]
		public String _y;			//  Tk_Results

		[DataMember]
		public String _z;			//  Tk_Remarks

		[DataMember]
		public String _aa;			//  Tk_Assignees

		[DataMember]
		public Boolean _ab;			//  Tk_IsPrivate

		[DataMember]
		public Boolean _ac;			//  Tk_IsActiveRow

		[DataMember]
		public Boolean _ad;			//  Tk_IsDeleted

		[DataMember]
		public Guid? _ae;			//  Tk_CreatedUserId

		[DataMember]
		public DateTime? _af;			//  Tk_CreatedDate

		[DataMember]
		public Guid? _ag;			//  Tk_UserId

		[DataMember]
		public String _ah;			//  UserName

		[DataMember]
		public DateTime? _ai;			//  Tk_LastModifiedDate

		[DataMember]
		public Byte[] _aj;			//  Tk_Stamp

		[DataMember]
		public Guid? _ak;			//  Ct_Id

		[DataMember]
		public Guid? _al;			//  CtD_Id

		[DataMember]
		public Guid? _am;			//  Df_Id

		[DataMember]
		public Guid? _an;			//  Ls_Id

		[DataMember]
		public Guid? _ao;			//  LsTr_Id

		[DataMember]
		public Guid? _ap;			//  Ob_Id

		[DataMember]
		public Guid? _aq;			//  Pp_Id

		[DataMember]
		public Guid? _ar;			//  Wl_Id

		[DataMember]
		public Guid? _asxx;			//  RCt_Id

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        public bool IsPartialLoad
        {
            get { return _isPartialLoad; }
            set { _isPartialLoad = value; }
        }

        internal Task_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 45; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                    _list[18] = _s;
                    _list[19] = _t;
                    _list[20] = _u;
                    _list[21] = _v;
                    _list[22] = _w;
                    _list[23] = _x;
                    _list[24] = _y;
                    _list[25] = _z;
                    _list[26] = _aa;
                    _list[27] = _ab;
                    _list[28] = _ac;
                    _list[29] = _ad;
                    _list[30] = _ae;
                    _list[31] = _af;
                    _list[32] = _ag;
                    _list[33] = _ah;
                    _list[34] = _ai;
                    _list[35] = _aj;
                    _list[36] = _ak;
                    _list[37] = _al;
                    _list[38] = _am;
                    _list[39] = _an;
                    _list[40] = _ao;
                    _list[41] = _ap;
                    _list[42] = _aq;
                    _list[43] = _ar;
                    _list[44] = _asxx;
                }
                return _list;
            }
        }

        public Guid Tk_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid Tk_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? Tk_Prj_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tk_Prj_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Guid? Tk_Tk_Id
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tk_Tk_Id_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public Int32? AttachmentCount
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? AttachmentCount_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String AttachmentFileNames
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String AttachmentFileNames_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Int32? DiscussionCount
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? DiscussionCount_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public String DiscussionTitles
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DiscussionTitles_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Int32? Tk_TkTp_Id
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? Tk_TkTp_Id_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public String Tk_TkTp_Id_TextField
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tk_TkTp_Id_TextField_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Guid? Tk_TkCt_Id
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tk_TkCt_Id_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public String Tk_TkCt_Id_TextField
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tk_TkCt_Id_TextField_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public String CategoryDescription
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String CategoryDescription_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public Int32? Tk_Number
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? Tk_Number_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public String Tk_Name
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tk_Name_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public String Tk_Desc
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tk_Desc_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public Decimal? Tk_EstimatedTime
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Decimal? Tk_EstimatedTime_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public Decimal? Tk_ActualTime
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Decimal? Tk_ActualTime_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public Decimal? Tk_PercentComplete
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Decimal? Tk_PercentComplete_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        public DateTime? Tk_StartDate
        {
            get { return _s; }
            set
            {
                _s = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Tk_StartDate_noevents
        {
            get { return _s; }
            set
            {
                _s = value;
            }
        }

        public DateTime? Tk_Deadline
        {
            get { return _t; }
            set
            {
                _t = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Tk_Deadline_noevents
        {
            get { return _t; }
            set
            {
                _t = value;
            }
        }

        public Int32? Tk_TkSt_Id
        {
            get { return _u; }
            set
            {
                _u = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? Tk_TkSt_Id_noevents
        {
            get { return _u; }
            set
            {
                _u = value;
            }
        }

        public String Tk_TkSt_Id_TextField
        {
            get { return _v; }
            set
            {
                _v = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tk_TkSt_Id_TextField_noevents
        {
            get { return _v; }
            set
            {
                _v = value;
            }
        }

        public Int32? Tk_TkPr_Id
        {
            get { return _w; }
            set
            {
                _w = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? Tk_TkPr_Id_noevents
        {
            get { return _w; }
            set
            {
                _w = value;
            }
        }

        public String Tk_TkPr_Id_TextField
        {
            get { return _x; }
            set
            {
                _x = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tk_TkPr_Id_TextField_noevents
        {
            get { return _x; }
            set
            {
                _x = value;
            }
        }

        public String Tk_Results
        {
            get { return _y; }
            set
            {
                _y = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tk_Results_noevents
        {
            get { return _y; }
            set
            {
                _y = value;
            }
        }

        public String Tk_Remarks
        {
            get { return _z; }
            set
            {
                _z = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tk_Remarks_noevents
        {
            get { return _z; }
            set
            {
                _z = value;
            }
        }

        public String Tk_Assignees
        {
            get { return _aa; }
            set
            {
                _aa = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Tk_Assignees_noevents
        {
            get { return _aa; }
            set
            {
                _aa = value;
            }
        }

        public Boolean Tk_IsPrivate
        {
            get { return _ab; }
            set
            {
                _ab = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tk_IsPrivate_noevents
        {
            get { return _ab; }
            set
            {
                _ab = value;
            }
        }

        public Boolean Tk_IsActiveRow
        {
            get { return _ac; }
            set
            {
                _ac = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tk_IsActiveRow_noevents
        {
            get { return _ac; }
            set
            {
                _ac = value;
            }
        }

        public Boolean Tk_IsDeleted
        {
            get { return _ad; }
            set
            {
                _ad = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Tk_IsDeleted_noevents
        {
            get { return _ad; }
            set
            {
                _ad = value;
            }
        }

        public Guid? Tk_CreatedUserId
        {
            get { return _ae; }
            set
            {
                _ae = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tk_CreatedUserId_noevents
        {
            get { return _ae; }
            set
            {
                _ae = value;
            }
        }

        public DateTime? Tk_CreatedDate
        {
            get { return _af; }
            set
            {
                _af = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Tk_CreatedDate_noevents
        {
            get { return _af; }
            set
            {
                _af = value;
            }
        }

        public Guid? Tk_UserId
        {
            get { return _ag; }
            set
            {
                _ag = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Tk_UserId_noevents
        {
            get { return _ag; }
            set
            {
                _ag = value;
            }
        }

        public String UserName
        {
            get { return _ah; }
            set
            {
                _ah = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _ah; }
            set
            {
                _ah = value;
            }
        }

        public DateTime? Tk_LastModifiedDate
        {
            get { return _ai; }
            set
            {
                _ai = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Tk_LastModifiedDate_noevents
        {
            get { return _ai; }
            set
            {
                _ai = value;
            }
        }

        public Byte[] Tk_Stamp
        {
            get { return _aj; }
            set
            {
                _aj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] Tk_Stamp_noevents
        {
            get { return _aj; }
            set
            {
                _aj = value;
            }
        }

        public Guid? Ct_Id
        {
            get { return _ak; }
            set
            {
                _ak = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Ct_Id_noevents
        {
            get { return _ak; }
            set
            {
                _ak = value;
            }
        }

        public Guid? CtD_Id
        {
            get { return _al; }
            set
            {
                _al = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? CtD_Id_noevents
        {
            get { return _al; }
            set
            {
                _al = value;
            }
        }

        public Guid? Df_Id
        {
            get { return _am; }
            set
            {
                _am = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Df_Id_noevents
        {
            get { return _am; }
            set
            {
                _am = value;
            }
        }

        public Guid? Ls_Id
        {
            get { return _an; }
            set
            {
                _an = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Ls_Id_noevents
        {
            get { return _an; }
            set
            {
                _an = value;
            }
        }

        public Guid? LsTr_Id
        {
            get { return _ao; }
            set
            {
                _ao = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? LsTr_Id_noevents
        {
            get { return _ao; }
            set
            {
                _ao = value;
            }
        }

        public Guid? Ob_Id
        {
            get { return _ap; }
            set
            {
                _ap = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Ob_Id_noevents
        {
            get { return _ap; }
            set
            {
                _ap = value;
            }
        }

        public Guid? Pp_Id
        {
            get { return _aq; }
            set
            {
                _aq = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Pp_Id_noevents
        {
            get { return _aq; }
            set
            {
                _aq = value;
            }
        }

        public Guid? Wl_Id
        {
            get { return _ar; }
            set
            {
                _ar = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Wl_Id_noevents
        {
            get { return _ar; }
            set
            {
                _ar = value;
            }
        }

        public Guid? RCt_Id
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? RCt_Id_noevents
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r,
				_s,
				_t,
				_u,
				_v,
				_w,
				_x,
				_y,
				_z,
				_aa,
				_ab,
				_ac,
				_ad,
				_ae,
				_af,
				_ag,
				_ah,
				_ai,
				_aj,
				_ak,
				_al,
				_am,
				_an,
				_ao,
				_ap,
				_aq,
				_ar,
				_asxx
			};
        }

        public Task_Values Clone()
        {
            return new Task_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


