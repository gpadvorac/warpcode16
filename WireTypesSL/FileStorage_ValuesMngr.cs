using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;

// Gen Timestamp:  10/5/2015 8:30:54 PM

namespace EntityWireTypeSL 
{

    #region Entity Values Manager
    [DataContract]
    public class FileStorage_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "FileStorage_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public FileStorage_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false), false) 
        {
        }

//        public FileStorage_ValuesMngr(SqlDataReader reader)
//        {
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", IfxTraceCategory.Enter);
//                _current = new FileStorage_Values(reader, this);
//                _original = null;
//                _concurrent = null;
//                _state.SetNotNew();
//                _state.SetNotDirty();
//                _state.SetValid();
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
//            }
//        }


        public FileStorage_ValuesMngr(object[] currentData, EntityState state) 
            : this(currentData, null, state, false) { }
            //: this(currentData, currentData, state) { }
            //: this(currentData, null, state) { }

        public FileStorage_ValuesMngr(object[] currentData, EntityState state, bool isPartialLoad)
            : this(currentData, null, state, isPartialLoad) { }

        private FileStorage_ValuesMngr(object[] currentData, object[] originalData, EntityState state, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new FileStorage_Values(currentData, this, isPartialLoad) : new FileStorage_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (originalData != null ? new FileStorage_Values(originalData, this) : null);
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected FileStorage_Values _original;
        [DataMember]
        public FileStorage_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected FileStorage_Values _current;
        [DataMember]
        public FileStorage_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected FileStorage_Values _concurrent;
        [DataMember]
        public FileStorage_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  FS_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  FS_ParentType
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  FS_ParentId_Guid
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  FS_ParentId_Int
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  FS_ParentId_Object
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  FS_FileName
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  FS_FilePath
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  FS_FileSize
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  FS_XrefIdentifier
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  IsNew
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  FileData
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  FS_UserId
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  UserName
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  FS_CreatedDate
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  FS_LastModifiedDate
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  FS_Stamp
                if (_current._p != _original._p)
                {
                    return true;
                }

                return false;
            }
        }

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Enter);
              //NOTE:  This method is the ONLY method that should call the one in the values object below.
                
                if (_original != null)
                {
                    _original.FinishPartialLoad();
                }
                if (_current != null)
                {
                    _current.FinishPartialLoad();
                }
                if (_concurrent != null)
                {
                    _concurrent.FinishPartialLoad();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
        }

        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{17}{0}{16}_b{17}{1}{16}_c{17}{2}{16}_d{17}{3}{16}_e{17}{4}{16}_f{17}{5}{16}_g{17}{6}{16}_h{17}{7}{16}_i{17}{8}{16}_j{17}{9}{16}_k{17}{10}{16}_l{17}{11}{16}_m{17}{12}{16}_n{17}{13}{16}_o{17}{14}{16}_p{17}{15}",
				new object[] {
				_current._a,		  //FS_Id
				_current._b,		  //FS_ParentType
				_current._c,		  //FS_ParentId_Guid
				_current._d,		  //FS_ParentId_Int
				_current._e,		  //FS_ParentId_Object
				_current._f,		  //FS_FileName
				_current._g,		  //FS_FilePath
				_current._h,		  //FS_FileSize
				_current._i,		  //FS_XrefIdentifier
				_current._j,		  //IsNew
				_current._k,		  //FileData
				_current._l,		  //FS_UserId
				_current._m,		  //UserName
				_current._n,		  //FS_CreatedDate
				_current._o,		  //FS_LastModifiedDate
				_current._p,		  //FS_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public partial class FileStorage_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "FileStorage_Values";
        private FileStorage_ValuesMngr _parent = null;
        private bool _isPartialLoad = false;
        private bool _isPartialDataLoaded = false;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal FileStorage_Values() 
        {
        }

        public FileStorage_Values(object[] data, FileStorage_ValuesMngr parent)
            :this(data, parent, false)
        {  }

        public FileStorage_Values(object[] data, FileStorage_ValuesMngr parent, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
                _isPartialLoad = isPartialLoad;
                // Fields that are part of the parital load which will always be loaded
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  FS_Id
				_b = ObjectHelper.GetStringFromObjectValue(data[1]);									//  FS_ParentType
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  FS_ParentId_Guid
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  FS_ParentId_Int
				_e = ObjectHelper.GetObjectFromObjectValue(data[4]);//  FS_ParentId_Object
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  FS_FileName
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  FS_FilePath
				_h = ObjectHelper.GetNullableBigIntFromObjectValue(data[7]);					//  FS_FileSize
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  FS_XrefIdentifier
				_j = ObjectHelper.GetNullableBoolFromObjectValue(data[9]);					//  IsNew
				_k = ObjectHelper.GetByteArrayFromObjectValue(data[10]);						//  FileData
				_l = ObjectHelper.GetNullableGuidFromObjectValue(data[11]);						//  FS_UserId
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  UserName
				_n = ObjectHelper.GetNullableDateTimeFromObjectValue(data[13]);					//  FS_CreatedDate
				_o = ObjectHelper.GetNullableDateTimeFromObjectValue(data[14]);					//  FS_LastModifiedDate
				_p = ObjectHelper.GetByteArrayFromObjectValue(data[15]);						//  FS_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - FileStorage", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  FS_Id
				_b = ObjectHelper.GetStringFromObjectValue(data[1]);									//  FS_ParentType
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  FS_ParentId_Guid
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  FS_ParentId_Int
				_e = ObjectHelper.GetObjectFromObjectValue(data[4]);//  FS_ParentId_Object
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  FS_FileName
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  FS_FilePath
				_h = ObjectHelper.GetNullableBigIntFromObjectValue(data[7]);					//  FS_FileSize
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  FS_XrefIdentifier
				_j = ObjectHelper.GetNullableBoolFromObjectValue(data[9]);					//  IsNew
				_k = ObjectHelper.GetByteArrayFromObjectValue(data[10]);						//  FileData
				_l = ObjectHelper.GetNullableGuidFromObjectValue(data[11]);						//  FS_UserId
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  UserName
				_n = ObjectHelper.GetNullableDateTimeFromObjectValue(data[13]);					//  FS_CreatedDate
				_o = ObjectHelper.GetNullableDateTimeFromObjectValue(data[14]);					//  FS_LastModifiedDate
				_p = ObjectHelper.GetByteArrayFromObjectValue(data[15]);						//  FS_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - FileStorage", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - FileStorage", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - FileStorage", IfxTraceCategory.Leave);
				// No fields to load.

                if (_isPartialLoad == true && _isPartialDataLoaded == false)
                {

                    _isPartialDataLoaded = true;
                }
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - FileStorage", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods


		#region Data Members

		[DataMember]
		public Guid _a;			//  FS_Id

		[DataMember]
		public String _b;			//  FS_ParentType

		[DataMember]
		public Guid? _c;			//  FS_ParentId_Guid

		[DataMember]
		public Int32? _d;			//  FS_ParentId_Int

		[DataMember]
		public Object _e;			//  FS_ParentId_Object

		[DataMember]
		public String _f;			//  FS_FileName

		[DataMember]
		public String _g;			//  FS_FilePath

		[DataMember]
		public Int64? _h;			//  FS_FileSize

		[DataMember]
		public String _i;			//  FS_XrefIdentifier

		[DataMember]
		public Boolean? _j;			//  IsNew

		[DataMember]
		public Byte[] _k;			//  FileData

		[DataMember]
		public Guid? _l;			//  FS_UserId

		[DataMember]
		public String _m;			//  UserName

		[DataMember]
		public DateTime? _n;			//  FS_CreatedDate

		[DataMember]
		public DateTime? _o;			//  FS_LastModifiedDate

		[DataMember]
		public Byte[] _p;			//  FS_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        public bool IsPartialLoad
        {
            get { return _isPartialLoad; }
            set { _isPartialLoad = value; }
        }

        internal FileStorage_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 16; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                }
                return _list;
            }
        }

        public Guid FS_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid FS_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public String FS_ParentType
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String FS_ParentType_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Guid? FS_ParentId_Guid
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? FS_ParentId_Guid_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public Int32? FS_ParentId_Int
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? FS_ParentId_Int_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public Object FS_ParentId_Object
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Object FS_ParentId_Object_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        //public String FS_FileName
        //{
        //    get { return _f; }
        //    set
        //    {
        //        _f = value;
        //        _parent.SetIsDirtyFlag();
        //    }
        //}

        //public String FS_FileName_noevents
        //{
        //    get { return _f; }
        //    set
        //    {
        //        _f = value;
        //    }
        //}

        public String FS_FilePath
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String FS_FilePath_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Int64? FS_FileSize
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int64? FS_FileSize_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public String FS_XrefIdentifier
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String FS_XrefIdentifier_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Boolean? IsNew
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? IsNew_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public Byte[] FileData
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] FileData_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public Guid? FS_UserId
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? FS_UserId_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public String UserName
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public DateTime? FS_CreatedDate
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? FS_CreatedDate_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public DateTime? FS_LastModifiedDate
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? FS_LastModifiedDate_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public Byte[] FS_Stamp
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] FS_Stamp_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p
			};
        }

        public FileStorage_Values Clone()
        {
            return new FileStorage_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


