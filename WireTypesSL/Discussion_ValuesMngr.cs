using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;

// Gen Timestamp:  10/3/2015 2:59:02 PM

namespace EntityWireTypeSL 
{

    #region Entity Values Manager
    [DataContract]
    public class Discussion_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "Discussion_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public Discussion_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false), false) 
        {
        }

//        public Discussion_ValuesMngr(SqlDataReader reader)
//        {
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Discussion_ValuesMngr", IfxTraceCategory.Enter);
//                _current = new Discussion_Values(reader, this);
//                _original = null;
//                _concurrent = null;
//                _state.SetNotNew();
//                _state.SetNotDirty();
//                _state.SetValid();
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Discussion_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Discussion_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
//            }
//        }


        public Discussion_ValuesMngr(object[] currentData, EntityState state) 
            : this(currentData, null, state, false) { }
            //: this(currentData, currentData, state) { }
            //: this(currentData, null, state) { }

        public Discussion_ValuesMngr(object[] currentData, EntityState state, bool isPartialLoad)
            : this(currentData, null, state, isPartialLoad) { }

        private Discussion_ValuesMngr(object[] currentData, object[] originalData, EntityState state, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Discussion_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new Discussion_Values(currentData, this, isPartialLoad) : new Discussion_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (originalData != null ? new Discussion_Values(originalData, this) : null);
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Discussion_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Discussion_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected Discussion_Values _original;
        [DataMember]
        public Discussion_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected Discussion_Values _current;
        [DataMember]
        public Discussion_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected Discussion_Values _concurrent;
        [DataMember]
        public Discussion_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  Dsc_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  Dsc_ParentType
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  Dsc_ParentId_Guid
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  Dsc_ParentId_Int
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  Dsc_ParentId_Object
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  Dsc_Title
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  Dsc_Comment
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  Dsc_IsActiveRow
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  Dsc_IsDeleted
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  Dsc_CreatedUserId
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  Dsc_CreatedDate
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  Dsc_UserId
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  UserName
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  Dsc_LastModifiedDate
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  Dsc_Stamp
                if (_current._o != _original._o)
                {
                    return true;
                }

                return false;
            }
        }

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Enter);
              //NOTE:  This method is the ONLY method that should call the one in the values object below.
                
                if (_original != null)
                {
                    _original.FinishPartialLoad();
                }
                if (_current != null)
                {
                    _current.FinishPartialLoad();
                }
                if (_concurrent != null)
                {
                    _concurrent.FinishPartialLoad();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
        }

        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{16}{0}{15}_b{16}{1}{15}_c{16}{2}{15}_d{16}{3}{15}_e{16}{4}{15}_f{16}{5}{15}_g{16}{6}{15}_h{16}{7}{15}_i{16}{8}{15}_j{16}{9}{15}_k{16}{10}{15}_l{16}{11}{15}_m{16}{12}{15}_n{16}{13}{15}_o{16}{14}",
				new object[] {
				_current._a,		  //Dsc_Id
				_current._b,		  //Dsc_ParentType
				_current._c,		  //Dsc_ParentId_Guid
				_current._d,		  //Dsc_ParentId_Int
				_current._e,		  //Dsc_ParentId_Object
				_current._f,		  //Dsc_Title
				_current._g,		  //Dsc_Comment
				_current._h,		  //Dsc_IsActiveRow
				_current._i,		  //Dsc_IsDeleted
				_current._j,		  //Dsc_CreatedUserId
				_current._k,		  //Dsc_CreatedDate
				_current._l,		  //Dsc_UserId
				_current._m,		  //UserName
				_current._n,		  //Dsc_LastModifiedDate
				_current._o,		  //Dsc_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class Discussion_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "Discussion_Values";
        private Discussion_ValuesMngr _parent = null;
        private bool _isPartialLoad = false;
        private bool _isPartialDataLoaded = false;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal Discussion_Values() 
        {
        }

        public Discussion_Values(object[] data, Discussion_ValuesMngr parent)
            :this(data, parent, false)
        {  }

        public Discussion_Values(object[] data, Discussion_ValuesMngr parent, bool isPartialLoad)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Discussion_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
                _isPartialLoad = isPartialLoad;
                // Fields that are part of the parital load which will always be loaded
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Dsc_Id
				_b = ObjectHelper.GetStringFromObjectValue(data[1]);									//  Dsc_ParentType
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  Dsc_ParentId_Guid
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  Dsc_ParentId_Int
				_e = ObjectHelper.GetObjectFromObjectValue(data[4]);//  Dsc_ParentId_Object
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  Dsc_Title
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  Dsc_Comment
				_h = ObjectHelper.GetBoolFromObjectValue(data[7]);									//  Dsc_IsActiveRow
				_i = ObjectHelper.GetBoolFromObjectValue(data[8]);									//  Dsc_IsDeleted
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  Dsc_CreatedUserId
				_k = ObjectHelper.GetNullableDateTimeFromObjectValue(data[10]);					//  Dsc_CreatedDate
				_l = ObjectHelper.GetNullableGuidFromObjectValue(data[11]);						//  Dsc_UserId
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  UserName
				_n = ObjectHelper.GetNullableDateTimeFromObjectValue(data[13]);					//  Dsc_LastModifiedDate
				_o = ObjectHelper.GetByteArrayFromObjectValue(data[14]);						//  Dsc_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Discussion_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - Discussion_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - Discussion", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Dsc_Id
				_b = ObjectHelper.GetStringFromObjectValue(data[1]);									//  Dsc_ParentType
				_c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);						//  Dsc_ParentId_Guid
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  Dsc_ParentId_Int
				_e = ObjectHelper.GetObjectFromObjectValue(data[4]);//  Dsc_ParentId_Object
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  Dsc_Title
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  Dsc_Comment
				_h = ObjectHelper.GetBoolFromObjectValue(data[7]);									//  Dsc_IsActiveRow
				_i = ObjectHelper.GetBoolFromObjectValue(data[8]);									//  Dsc_IsDeleted
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  Dsc_CreatedUserId
				_k = ObjectHelper.GetNullableDateTimeFromObjectValue(data[10]);					//  Dsc_CreatedDate
				_l = ObjectHelper.GetNullableGuidFromObjectValue(data[11]);						//  Dsc_UserId
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  UserName
				_n = ObjectHelper.GetNullableDateTimeFromObjectValue(data[13]);					//  Dsc_LastModifiedDate
				_o = ObjectHelper.GetByteArrayFromObjectValue(data[14]);						//  Dsc_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - Discussion", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - Discussion", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}

        public void FinishPartialLoad()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - Discussion", IfxTraceCategory.Leave);
				// No fields to load.

                if (_isPartialLoad == true && _isPartialDataLoaded == false)
                {

                    _isPartialDataLoaded = true;
                }
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad - Discussion", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FinishPartialLoad", IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods


		#region Data Members

		[DataMember]
		public Guid _a;			//  Dsc_Id

		[DataMember]
		public String _b;			//  Dsc_ParentType

		[DataMember]
		public Guid? _c;			//  Dsc_ParentId_Guid

		[DataMember]
		public Int32? _d;			//  Dsc_ParentId_Int

		[DataMember]
		public Object _e;			//  Dsc_ParentId_Object

		[DataMember]
		public String _f;			//  Dsc_Title

		[DataMember]
		public String _g;			//  Dsc_Comment

		[DataMember]
		public Boolean _h;			//  Dsc_IsActiveRow

		[DataMember]
		public Boolean _i;			//  Dsc_IsDeleted

		[DataMember]
		public Guid? _j;			//  Dsc_CreatedUserId

		[DataMember]
		public DateTime? _k;			//  Dsc_CreatedDate

		[DataMember]
		public Guid? _l;			//  Dsc_UserId

		[DataMember]
		public String _m;			//  UserName

		[DataMember]
		public DateTime? _n;			//  Dsc_LastModifiedDate

		[DataMember]
		public Byte[] _o;			//  Dsc_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        public bool IsPartialLoad
        {
            get { return _isPartialLoad; }
            set { _isPartialLoad = value; }
        }

        internal Discussion_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 15; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                }
                return _list;
            }
        }

        public Guid Dsc_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid Dsc_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public String Dsc_ParentType
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Dsc_ParentType_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Guid? Dsc_ParentId_Guid
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Dsc_ParentId_Guid_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public Int32? Dsc_ParentId_Int
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? Dsc_ParentId_Int_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public Object Dsc_ParentId_Object
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Object Dsc_ParentId_Object_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public String Dsc_Title
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Dsc_Title_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public String Dsc_Comment
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Dsc_Comment_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Boolean Dsc_IsActiveRow
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Dsc_IsActiveRow_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Boolean Dsc_IsDeleted
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Dsc_IsDeleted_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Guid? Dsc_CreatedUserId
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Dsc_CreatedUserId_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public DateTime? Dsc_CreatedDate
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Dsc_CreatedDate_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public Guid? Dsc_UserId
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Dsc_UserId_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public String UserName
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public DateTime? Dsc_LastModifiedDate
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Dsc_LastModifiedDate_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public Byte[] Dsc_Stamp
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] Dsc_Stamp_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o
			};
        }

        public Discussion_Values Clone()
        {
            return new Discussion_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


