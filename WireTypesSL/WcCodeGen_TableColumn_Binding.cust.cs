using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/18/2018 12:50:35 PM

namespace EntityWireTypeSL
{



    public partial class WcCodeGen_TableColumn_Binding 
    {

        #region Initialize Variables

        List<WcCodeGen_ToolTip_Binding> _toolTips = new List<WcCodeGen_ToolTip_Binding>();
        List<WcCodeGen_ComboColumn_Binding> _comboColumns = new List<WcCodeGen_ComboColumn_Binding>();

        #endregion Initialize Variables


        #region Constructors

        //public WcCodeGen_TableColumn_Binding() { }



        #endregion Constructors


        #region ToolTip

        public List<WcCodeGen_ToolTip_Binding> ToolTips
        {
            get { return _toolTips; }
            set { _toolTips = value; }
        }
        
        public void LoadToolTips(object[] list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadToolTips", IfxTraceCategory.Enter);
                _toolTips.Clear();
                for (int i = 0; i < list.GetUpperBound(0) + 1; i++)
                {
                    _toolTips.Add(new WcCodeGen_ToolTip_Binding((object[])list[i]));
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadToolTips", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadToolTips", IfxTraceCategory.Leave);
            }
        }

        public void LoadToolTipsFromMasterList(List<WcCodeGen_ToolTip_Binding> list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadToolTipsFromMasterList", IfxTraceCategory.Enter);
                _toolTips.Clear();
                foreach (WcCodeGen_ToolTip_Binding obj in list)
                {
                    if (obj.Tt_TbC_Id == TbC_Id)
                    {
                        _toolTips.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadToolTipsFromMasterList", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadToolTipsFromMasterList", IfxTraceCategory.Leave);
            }
        }

        #endregion ToolTip


        #region ComboColumn

        public List<WcCodeGen_ComboColumn_Binding> ComboColumns
        {
            get { return _comboColumns; }
            set { _comboColumns = value; }
        }

        public void LoadComboColumns(object[] list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadComboColumns", IfxTraceCategory.Enter);
                _comboColumns.Clear();
                for (int i = 0; i < list.GetUpperBound(0) + 1; i++)
                {
                    _comboColumns.Add(new WcCodeGen_ComboColumn_Binding((object[])list[i]));
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadComboColumns", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadComboColumns", IfxTraceCategory.Leave);
            }
        }


        public void LoadComboColumnsFromMasterList(List<WcCodeGen_ComboColumn_Binding> list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadComboColumnsFromMasterList", IfxTraceCategory.Enter);
                _comboColumns.Clear();
                foreach (WcCodeGen_ComboColumn_Binding obj in list)
                {
                    if (obj.TbCCmbC_TbC_Id == TbC_Id)
                    {
                        _comboColumns.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadComboColumnsFromMasterList", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadComboColumnsFromMasterList", IfxTraceCategory.Leave);
            }
        }

        #endregion ComboColumn

    }

}



