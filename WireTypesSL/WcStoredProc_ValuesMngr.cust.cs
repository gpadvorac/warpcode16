﻿using System;
using System.Net;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Ifx.SL;
using TypeServices;

namespace EntityWireTypeSL
{
    public partial class WcStoredProc_Values
    {



        public WcStoredProc_Values(object[] data, WcStoredProc_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
                //*** Made this nullable
                _a = ObjectHelper.GetNullableGuidFromObjectValue(data[0]);                              //  Sp_Id
                _b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);                      //  Sp_ApVrsn_Id
                _c = ObjectHelper.GetStringFromObjectValue(data[2]);                                    //  SprocInDb
                _d = ObjectHelper.GetStringFromObjectValue(data[3]);                                    //  Sp_Name
                _e = ObjectHelper.GetBoolFromObjectValue(data[4]);                                  //  Sp_IsCodeGen
                _f = ObjectHelper.GetBoolFromObjectValue(data[5]);                                  //  Sp_IsReadyCodeGen
                _g = ObjectHelper.GetBoolFromObjectValue(data[6]);                                  //  Sp_IsCodeGenComplete
                _h = ObjectHelper.GetBoolFromObjectValue(data[7]);                                  //  Sp_IsTagForCodeGen
                _i = ObjectHelper.GetBoolFromObjectValue(data[8]);                                  //  Sp_IsTagForOther
                _j = ObjectHelper.GetStringFromObjectValue(data[9]);                                    //  Sp_SprocGroups
                _k = ObjectHelper.GetBoolFromObjectValue(data[10]);                                 //  Sp_IsBuildDataAccessMethod
                _l = ObjectHelper.GetBoolFromObjectValue(data[11]);                                 //  Sp_BuildListClass
                _m = ObjectHelper.GetBoolFromObjectValue(data[12]);                                 //  Sp_IsFetchEntity
                _n = ObjectHelper.GetBoolFromObjectValue(data[13]);                                 //  Sp_IsStaticList
                _o = ObjectHelper.GetNullableBoolFromObjectValue(data[14]);                 //  Sp_IsTypeComboItem
                _p = ObjectHelper.GetBoolFromObjectValue(data[15]);                                 //  Sp_IsCreateWireType
                _q = ObjectHelper.GetStringFromObjectValue(data[16]);                                   //  Sp_WireTypeName
                _r = ObjectHelper.GetStringFromObjectValue(data[17]);                                   //  Sp_MethodName
                _s = ObjectHelper.GetNullableGuidFromObjectValue(data[18]);                     //  Sp_AssocEntity_Id
                _t = ObjectHelper.GetStringFromObjectValue(data[19]);                                   //  Sp_AssocEntity_Id_TextField
                _u = ObjectHelper.GetStringFromObjectValue(data[20]);                                   //  Sp_AssocEntityNames
                _v = ObjectHelper.GetNullableIntFromObjectValue(data[21]);                          //  Sp_SpRsTp_Id
                _w = ObjectHelper.GetStringFromObjectValue(data[22]);                                   //  Sp_SpRsTp_Id_TextField
                _x = ObjectHelper.GetNullableIntFromObjectValue(data[23]);                          //  Sp_SpRtTp_Id
                _y = ObjectHelper.GetStringFromObjectValue(data[24]);                                   //  Sp_SpRtTp_Id_TextField
                _z = ObjectHelper.GetBoolFromObjectValue(data[25]);                                 //  Sp_IsInputParamsAsObjectArray
                _aa = ObjectHelper.GetBoolFromObjectValue(data[26]);                                    //  Sp_IsParamsAutoRefresh
                _ab = ObjectHelper.GetBoolFromObjectValue(data[27]);                                    //  Sp_HasNewParams
                _ac = ObjectHelper.GetBoolFromObjectValue(data[28]);                                    //  Sp_IsParamsValid
                _ad = ObjectHelper.GetBoolFromObjectValue(data[29]);                                    //  Sp_IsParamValueSetValid
                _ae = ObjectHelper.GetBoolFromObjectValue(data[30]);                                    //  Sp_HasNewColumns
                _af = ObjectHelper.GetBoolFromObjectValue(data[31]);                                    //  Sp_IsColumnsValid
                _ag = ObjectHelper.GetBoolFromObjectValue(data[32]);                                    //  Sp_IsValid
                _ah = ObjectHelper.GetStringFromObjectValue(data[33]);                                  //  Sp_Notes
                _ai = ObjectHelper.GetBoolFromObjectValue(data[34]);                                    //  Sp_IsActiveRow
                _aj = ObjectHelper.GetBoolFromObjectValue(data[35]);                                    //  Sp_IsDeleted
                _ak = ObjectHelper.GetNullableGuidFromObjectValue(data[36]);                        //  Sp_CreatedUserId
                _al = ObjectHelper.GetNullableDateTimeFromObjectValue(data[37]);                    //  Sp_CreatedDate
                _am = ObjectHelper.GetNullableGuidFromObjectValue(data[38]);                        //  Sp_UserId
                _an = ObjectHelper.GetStringFromObjectValue(data[39]);                                  //  UserName
                _ao = ObjectHelper.GetNullableDateTimeFromObjectValue(data[40]);                    //  Sp_LastModifiedDate
                _ap = data[41] as Byte[];                       //  Sp_Stamp
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", IfxTraceCategory.Leave);
            }
        }



        //*** Made this nullable
        public Guid? Sp_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }


        //*** Made this nullable
        [DataMember]
        public Guid? _a;            //  Sp_Id

        //*** Made this nullable
        public Guid? Sp_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }


    }
}
