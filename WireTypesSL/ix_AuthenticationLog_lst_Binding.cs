using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

namespace EntityWireTypeSL
{


    [DataContract]
    public class ix_AuthenticationLog_lst_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "Ifx";
        private static string _cn = "ix_AuthenticationLog_lst_Binding";

        #endregion Initialize Variables


		#region Constructors

        public ix_AuthenticationLog_lst_Binding() { }


        public ix_AuthenticationLog_lst_Binding(Guid _IxAuthLog_Id, Guid? _IxAuthLog_UserId, String _IxAuthLog_UserName, String _IxAuthLog_IP, String _IxAuthLog_Platform, String _IxAuthLog_Message, Boolean? _IxAuthLog_Success, DateTime? _IxAuthLog_TimeStamp )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_AuthenticationLog_lst_Binding", IfxTraceCategory.Enter);
				_a = _IxAuthLog_Id;
				_b = _IxAuthLog_UserId;
				_c = _IxAuthLog_UserName;
				_d = _IxAuthLog_IP;
				_e = _IxAuthLog_Platform;
				_f = _IxAuthLog_Message;
				_g = _IxAuthLog_Success;
				_h = _IxAuthLog_TimeStamp;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_AuthenticationLog_lst_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_AuthenticationLog_lst_Binding", IfxTraceCategory.Leave);
            }
		}

        public ix_AuthenticationLog_lst_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_AuthenticationLog_lst_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  IxAuthLog_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);                 //  IxAuthLog_UserId
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);                 //  IxAuthLog_UserName
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);                 //  IxAuthLog_IP
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);                 //  IxAuthLog_Platform
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);                 //  IxAuthLog_Message
				_g = ObjectHelper.GetNullableBoolFromObjectValue(data[6]);                 //  IxAuthLog_Success
				_h = ObjectHelper.GetNullableDateTimeFromObjectValue(data[7]);                 //  IxAuthLog_TimeStamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_AuthenticationLog_lst_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_AuthenticationLog_lst_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region IxAuthLog_Id

        private Guid _a;
//        [DataMember]
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid IxAuthLog_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion IxAuthLog_Id


        #region IxAuthLog_UserId

        private Guid? _b;
//        [DataMember]
//        public Guid? B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Guid? IxAuthLog_UserId
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion IxAuthLog_UserId


        #region IxAuthLog_UserName

        private String _c;
//        [DataMember]
//        public String C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public String IxAuthLog_UserName
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion IxAuthLog_UserName


        #region IxAuthLog_IP

        private String _d;
//        [DataMember]
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String IxAuthLog_IP
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion IxAuthLog_IP


        #region IxAuthLog_Platform

        private String _e;
//        [DataMember]
//        public String E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public String IxAuthLog_Platform
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion IxAuthLog_Platform


        #region IxAuthLog_Message

        private String _f;
//        [DataMember]
//        public String F
//        {
//            get { return _f; }
//            set { _f = value; }
//        }

        //[NonSerialized]
        public String IxAuthLog_Message
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion IxAuthLog_Message


        #region IxAuthLog_Success

        private Boolean? _g;
//        [DataMember]
//        public Boolean? G
//        {
//            get { return _g; }
//            set { _g = value; }
//        }

        //[NonSerialized]
        public Boolean? IxAuthLog_Success
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion IxAuthLog_Success


        #region IxAuthLog_TimeStamp

        private DateTime? _h;
//        [DataMember]
//        public DateTime? H
//        {
//            get { return _h; }
//            set { _h = value; }
//        }

        //[NonSerialized]
        public DateTime? IxAuthLog_TimeStamp
        {
            get { return _h; }
            set { _h = value; }
        }

        #endregion IxAuthLog_TimeStamp


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7} ", IxAuthLog_Id, IxAuthLog_UserId, IxAuthLog_UserName, IxAuthLog_IP, IxAuthLog_Platform, IxAuthLog_Message, IxAuthLog_Success, IxAuthLog_TimeStamp );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7} ", IxAuthLog_Id, IxAuthLog_UserId, IxAuthLog_UserName, IxAuthLog_IP, IxAuthLog_Platform, IxAuthLog_Message, IxAuthLog_Success, IxAuthLog_TimeStamp );
        }

    }

}
