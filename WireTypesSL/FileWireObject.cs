﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Ifx;
using Ifx.SL;
using TypeServices;
using System.Runtime.Serialization;

namespace WireTypes
{
    public class FileWireObject
    {

        #region Initialize Variables

        private static string _as = "WireTypes";
        private static string _cn = "FileWireObject";

        public string _fileName;
        public string _recordType;
        public Guid _recordId;
        public byte[] _fileData;
        public Guid _userId;
        public long _fileSize;


        #endregion Initialize Variables

        #region Constructors


        public FileWireObject() { }

        public FileWireObject(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", new ValuePair[]  {  new ValuePair("data", data) }, IfxTraceCategory.Enter);
           
                _fileName = ObjectHelper.GetStringFromObjectValue(data[0]);
                _recordType = ObjectHelper.GetStringFromObjectValue(data[1]);
                _recordId = ObjectHelper.GetGuidFromObjectValue(data[2]);
                _fileData = ObjectHelper.GetByteArrayFromObjectValue(data[3]);	
                _userId = ObjectHelper.GetGuidFromObjectValue(data[4]);
                _fileSize = ObjectHelper.GetBigIntFromObjectValue(data[5]);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", null, null, ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", IfxTraceCategory.Leave);
            }
        }

        public FileWireObject(string fileName, string recordType, Guid recordId, byte[] fileData, Guid userId, long fileSize)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", new ValuePair[] 
                { 
                    new ValuePair("fileName", fileName),
                    new ValuePair("recordType", recordType),
                    new ValuePair("recordId", recordId),
                    new ValuePair("userId", userId) }, IfxTraceCategory.Enter);
                _fileName = fileName;
                _recordType = recordType;
                _recordId = recordId;
                _fileData = fileData;
                _userId = userId;
                _fileSize = fileSize;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", null, null, ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - FileStorage_ValuesMngr", IfxTraceCategory.Leave);
            }
        }


        #endregion Constructors


        #region Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_fileName,
				_recordType,
				_recordId,
				_fileData,
				_userId,
				_fileSize
			};
        }


        #endregion Methods

        #region Properties






        [DataMember]
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        [DataMember]
        public string RecordType
        {
            get { return _recordType; }
            set { _recordType = value; }
        }

        [DataMember]
        public Guid RecordId
        {
            get { return _recordId; }
            set { _recordId = value; }
        }

        [DataMember]
        public byte[] FileData
        {
            get { return _fileData; }
            set { _fileData = value; }
        }

        [DataMember]
        public Guid UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        [DataMember]
        public long FileSize
        {
            get { return _fileSize; }
            set { _fileSize = value; }
        }

        #endregion Properties


    }
}
