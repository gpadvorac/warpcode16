using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  1/7/2018 9:27:13 PM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcTableChild_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTableChild_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcTableChild_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcTableChild_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcTableChild_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableChild_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcTableChild_Values(currentData, this) : new WcTableChild_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcTableChild_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableChild_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableChild_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcTableChild_Values _original;
        [DataMember]
        public WcTableChild_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcTableChild_Values _current;
        [DataMember]
        public WcTableChild_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcTableChild_Values _concurrent;
        [DataMember]
        public WcTableChild_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  TbChd_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  TbChd_Parent_Tb_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  TbChd_SortOrder
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  TbChd_Child_Tb_Id
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  TbChd_Child_Tb_Id_TextField
                //if (_current._e != _original._e)
                //{
                //    return true;
                //}

                //  TbChd_ChildForeignKeyColumn_Id
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  TbChd_ChildForeignKeyColumn_Id_TextField
                //if (_current._g != _original._g)
                //{
                //    return true;
                //}

                //  TbChd_ChildForeignKeyColumn22_Id
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  TbChd_ChildForeignKeyColumn22_Id_TextField
                //if (_current._i != _original._i)
                //{
                //    return true;
                //}

                //  TbChd_ChildForeignKeyColumn33_Id
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  TbChd_ChildForeignKeyColumn33_Id_TextField
                //if (_current._k != _original._k)
                //{
                //    return true;
                //}

                //  TbChd_DifferentCombo_Id
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  TbChd_DifferentCombo_Id_TextField
                //if (_current._m != _original._m)
                //{
                //    return true;
                //}

                //  TbChd_IsDisplayAsChildGrid
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  TbChd_IsDisplayAsChildTab
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  TbChd_IsActiveRow
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  TbChd_IsDeleted
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  TbChd_CreatedUserId
                if (_current._r != _original._r)
                {
                    return true;
                }

                //  TbChd_CreatedDate
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  TbChd_UserId
                if (_current._t != _original._t)
                {
                    return true;
                }

                //  UserName
                if (_current._u != _original._u)
                {
                    return true;
                }

                //  TbChd_LastModifiedDate
                if (_current._v != _original._v)
                {
                    return true;
                }

                //  TbChd_Stamp
                if (_current._w != _original._w)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{24}{0}{23}_b{24}{1}{23}_c{24}{2}{23}_d{24}{3}{23}_e{24}{4}{23}_f{24}{5}{23}_g{24}{6}{23}_h{24}{7}{23}_i{24}{8}{23}_j{24}{9}{23}_k{24}{10}{23}_l{24}{11}{23}_m{24}{12}{23}_n{24}{13}{23}_o{24}{14}{23}_p{24}{15}{23}_q{24}{16}{23}_r{24}{17}{23}_s{24}{18}{23}_t{24}{19}{23}_u{24}{20}{23}_v{24}{21}{23}_w{24}{22}",
				new object[] {
				_current._a,		  //TbChd_Id
				_current._b,		  //TbChd_Parent_Tb_Id
				_current._c,		  //TbChd_SortOrder
				_current._d,		  //TbChd_Child_Tb_Id
				_current._e,		  //TbChd_Child_Tb_Id_TextField
				_current._f,		  //TbChd_ChildForeignKeyColumn_Id
				_current._g,		  //TbChd_ChildForeignKeyColumn_Id_TextField
				_current._h,		  //TbChd_ChildForeignKeyColumn22_Id
				_current._i,		  //TbChd_ChildForeignKeyColumn22_Id_TextField
				_current._j,		  //TbChd_ChildForeignKeyColumn33_Id
				_current._k,		  //TbChd_ChildForeignKeyColumn33_Id_TextField
				_current._l,		  //TbChd_DifferentCombo_Id
				_current._m,		  //TbChd_DifferentCombo_Id_TextField
				_current._n,		  //TbChd_IsDisplayAsChildGrid
				_current._o,		  //TbChd_IsDisplayAsChildTab
				_current._p,		  //TbChd_IsActiveRow
				_current._q,		  //TbChd_IsDeleted
				_current._r,		  //TbChd_CreatedUserId
				_current._s,		  //TbChd_CreatedDate
				_current._t,		  //TbChd_UserId
				_current._u,		  //UserName
				_current._v,		  //TbChd_LastModifiedDate
				_current._w,		  //TbChd_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcTableChild_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTableChild_Values";
        private WcTableChild_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcTableChild_Values() 
        {
        }

        //public WcTableChild_Values(object[] data, WcTableChild_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcTableChild_Values(object[] data, WcTableChild_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableChild_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  TbChd_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  TbChd_Parent_Tb_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  TbChd_SortOrder
				_d = ObjectHelper.GetNullableGuidFromObjectValue(data[3]);						//  TbChd_Child_Tb_Id
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  TbChd_Child_Tb_Id_TextField
				_f = ObjectHelper.GetNullableGuidFromObjectValue(data[5]);						//  TbChd_ChildForeignKeyColumn_Id
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  TbChd_ChildForeignKeyColumn_Id_TextField
				_h = ObjectHelper.GetNullableGuidFromObjectValue(data[7]);						//  TbChd_ChildForeignKeyColumn22_Id
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  TbChd_ChildForeignKeyColumn22_Id_TextField
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  TbChd_ChildForeignKeyColumn33_Id
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  TbChd_ChildForeignKeyColumn33_Id_TextField
				_l = ObjectHelper.GetNullableGuidFromObjectValue(data[11]);						//  TbChd_DifferentCombo_Id
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  TbChd_DifferentCombo_Id_TextField
				_n = ObjectHelper.GetBoolFromObjectValue(data[13]);									//  TbChd_IsDisplayAsChildGrid
				_o = ObjectHelper.GetBoolFromObjectValue(data[14]);									//  TbChd_IsDisplayAsChildTab
				_p = ObjectHelper.GetBoolFromObjectValue(data[15]);									//  TbChd_IsActiveRow
				_q = ObjectHelper.GetBoolFromObjectValue(data[16]);									//  TbChd_IsDeleted
				_r = ObjectHelper.GetNullableGuidFromObjectValue(data[17]);						//  TbChd_CreatedUserId
				_s = ObjectHelper.GetNullableDateTimeFromObjectValue(data[18]);					//  TbChd_CreatedDate
				_t = ObjectHelper.GetNullableGuidFromObjectValue(data[19]);						//  TbChd_UserId
				_u = ObjectHelper.GetStringFromObjectValue(data[20]);									//  UserName
				_v = ObjectHelper.GetNullableDateTimeFromObjectValue(data[21]);					//  TbChd_LastModifiedDate
				_w = data[22] as Byte[];						//  TbChd_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableChild_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableChild_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableChild", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  TbChd_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  TbChd_Parent_Tb_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  TbChd_SortOrder
				_d = ObjectHelper.GetNullableGuidFromObjectValue(data[3]);						//  TbChd_Child_Tb_Id
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  TbChd_Child_Tb_Id_TextField
				_f = ObjectHelper.GetNullableGuidFromObjectValue(data[5]);						//  TbChd_ChildForeignKeyColumn_Id
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  TbChd_ChildForeignKeyColumn_Id_TextField
				_h = ObjectHelper.GetNullableGuidFromObjectValue(data[7]);						//  TbChd_ChildForeignKeyColumn22_Id
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  TbChd_ChildForeignKeyColumn22_Id_TextField
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  TbChd_ChildForeignKeyColumn33_Id
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  TbChd_ChildForeignKeyColumn33_Id_TextField
				_l = ObjectHelper.GetNullableGuidFromObjectValue(data[11]);						//  TbChd_DifferentCombo_Id
				_m = ObjectHelper.GetStringFromObjectValue(data[12]);									//  TbChd_DifferentCombo_Id_TextField
				_n = ObjectHelper.GetBoolFromObjectValue(data[13]);									//  TbChd_IsDisplayAsChildGrid
				_o = ObjectHelper.GetBoolFromObjectValue(data[14]);									//  TbChd_IsDisplayAsChildTab
				_p = ObjectHelper.GetBoolFromObjectValue(data[15]);									//  TbChd_IsActiveRow
				_q = ObjectHelper.GetBoolFromObjectValue(data[16]);									//  TbChd_IsDeleted
				_r = ObjectHelper.GetNullableGuidFromObjectValue(data[17]);						//  TbChd_CreatedUserId
				_s = ObjectHelper.GetNullableDateTimeFromObjectValue(data[18]);					//  TbChd_CreatedDate
				_t = ObjectHelper.GetNullableGuidFromObjectValue(data[19]);						//  TbChd_UserId
				_u = ObjectHelper.GetStringFromObjectValue(data[20]);									//  UserName
				_v = ObjectHelper.GetNullableDateTimeFromObjectValue(data[21]);					//  TbChd_LastModifiedDate
				_w = data[22] as Byte[];						//  TbChd_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableChild", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableChild", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  TbChd_Id

		[DataMember]
		public Guid? _b;			//  TbChd_Parent_Tb_Id

		[DataMember]
		public Int32? _c;			//  TbChd_SortOrder

		[DataMember]
		public Guid? _d;			//  TbChd_Child_Tb_Id

		[DataMember]
		public String _e;			//  TbChd_Child_Tb_Id_TextField

		[DataMember]
		public Guid? _f;			//  TbChd_ChildForeignKeyColumn_Id

		[DataMember]
		public String _g;			//  TbChd_ChildForeignKeyColumn_Id_TextField

		[DataMember]
		public Guid? _h;			//  TbChd_ChildForeignKeyColumn22_Id

		[DataMember]
		public String _i;			//  TbChd_ChildForeignKeyColumn22_Id_TextField

		[DataMember]
		public Guid? _j;			//  TbChd_ChildForeignKeyColumn33_Id

		[DataMember]
		public String _k;			//  TbChd_ChildForeignKeyColumn33_Id_TextField

		[DataMember]
		public Guid? _l;			//  TbChd_DifferentCombo_Id

		[DataMember]
		public String _m;			//  TbChd_DifferentCombo_Id_TextField

		[DataMember]
		public Boolean _n;			//  TbChd_IsDisplayAsChildGrid

		[DataMember]
		public Boolean _o;			//  TbChd_IsDisplayAsChildTab

		[DataMember]
		public Boolean _p;			//  TbChd_IsActiveRow

		[DataMember]
		public Boolean _q;			//  TbChd_IsDeleted

		[DataMember]
		public Guid? _r;			//  TbChd_CreatedUserId

		[DataMember]
		public DateTime? _s;			//  TbChd_CreatedDate

		[DataMember]
		public Guid? _t;			//  TbChd_UserId

		[DataMember]
		public String _u;			//  UserName

		[DataMember]
		public DateTime? _v;			//  TbChd_LastModifiedDate

		[DataMember]
		public Byte[] _w;			//  TbChd_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcTableChild_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 23; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                    _list[18] = _s;
                    _list[19] = _t;
                    _list[20] = _u;
                    _list[21] = _v;
                    _list[22] = _w;
                }
                return _list;
            }
        }

        public Guid TbChd_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid TbChd_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? TbChd_Parent_Tb_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbChd_Parent_Tb_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Int32? TbChd_SortOrder
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbChd_SortOrder_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public Guid? TbChd_Child_Tb_Id
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbChd_Child_Tb_Id_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String TbChd_Child_Tb_Id_TextField
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbChd_Child_Tb_Id_TextField_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Guid? TbChd_ChildForeignKeyColumn_Id
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbChd_ChildForeignKeyColumn_Id_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public String TbChd_ChildForeignKeyColumn_Id_TextField
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbChd_ChildForeignKeyColumn_Id_TextField_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Guid? TbChd_ChildForeignKeyColumn22_Id
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbChd_ChildForeignKeyColumn22_Id_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public String TbChd_ChildForeignKeyColumn22_Id_TextField
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbChd_ChildForeignKeyColumn22_Id_TextField_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Guid? TbChd_ChildForeignKeyColumn33_Id
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbChd_ChildForeignKeyColumn33_Id_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public String TbChd_ChildForeignKeyColumn33_Id_TextField
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbChd_ChildForeignKeyColumn33_Id_TextField_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public Guid? TbChd_DifferentCombo_Id
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbChd_DifferentCombo_Id_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public String TbChd_DifferentCombo_Id_TextField
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbChd_DifferentCombo_Id_TextField_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public Boolean TbChd_IsDisplayAsChildGrid
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbChd_IsDisplayAsChildGrid_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public Boolean TbChd_IsDisplayAsChildTab
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbChd_IsDisplayAsChildTab_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public Boolean TbChd_IsActiveRow
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbChd_IsActiveRow_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public Boolean TbChd_IsDeleted
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbChd_IsDeleted_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public Guid? TbChd_CreatedUserId
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbChd_CreatedUserId_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        public DateTime? TbChd_CreatedDate
        {
            get { return _s; }
            set
            {
                _s = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? TbChd_CreatedDate_noevents
        {
            get { return _s; }
            set
            {
                _s = value;
            }
        }

        public Guid? TbChd_UserId
        {
            get { return _t; }
            set
            {
                _t = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbChd_UserId_noevents
        {
            get { return _t; }
            set
            {
                _t = value;
            }
        }

        public String UserName
        {
            get { return _u; }
            set
            {
                _u = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _u; }
            set
            {
                _u = value;
            }
        }

        public DateTime? TbChd_LastModifiedDate
        {
            get { return _v; }
            set
            {
                _v = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? TbChd_LastModifiedDate_noevents
        {
            get { return _v; }
            set
            {
                _v = value;
            }
        }

        public Byte[] TbChd_Stamp
        {
            get { return _w; }
            set
            {
                _w = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] TbChd_Stamp_noevents
        {
            get { return _w; }
            set
            {
                _w = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r,
				_s,
				_t,
				_u,
				_v,
				_w
			};
        }

        public WcTableChild_Values Clone()
        {
            return new WcTableChild_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


