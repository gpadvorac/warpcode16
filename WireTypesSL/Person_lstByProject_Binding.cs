using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

namespace EntityWireTypeSL
{


    [DataContract]
    public class Person_lstByProject_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "Person_lstByProject_Binding";

        #endregion Initialize Variables


		#region Constructors

        public Person_lstByProject_Binding() { }


        public Person_lstByProject_Binding(Guid _Pn_Id, String _Pn_FName, String _Pn_LName, String _Pn_EMail )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Person_lstByProject_Binding", IfxTraceCategory.Enter);
				_a = _Pn_Id;
				_b = _Pn_FName;
				_c = _Pn_LName;
				_d = _Pn_EMail;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Person_lstByProject_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Person_lstByProject_Binding", IfxTraceCategory.Leave);
            }
		}

        public Person_lstByProject_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Person_lstByProject_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  Pn_Id
				_b = (String)data[1];                //  Pn_FName
				_c = (String)data[2];                //  Pn_LName
				_d = (String)data[3];                //  Pn_EMail
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Person_lstByProject_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Person_lstByProject_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region Pn_Id

        private Guid _a;
//        [DataMember]
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid Pn_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Pn_Id


        #region Pn_FName

        private String _b;
//        [DataMember]
//        public String B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public String Pn_FName
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion Pn_FName


        #region Pn_LName

        private String _c;
//        [DataMember]
//        public String C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public String Pn_LName
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion Pn_LName


        #region Pn_EMail

        private String _d;
//        [DataMember]
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String Pn_EMail
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion Pn_EMail


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3} ", Pn_Id, Pn_FName, Pn_LName, Pn_EMail );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3} ", Pn_Id, Pn_FName, Pn_LName, Pn_EMail );
        }

    }

}
