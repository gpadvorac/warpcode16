using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  12/29/2016 12:36:01 AM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcApplication_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcApplication_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcApplication_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcApplication_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcApplication_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplication_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcApplication_Values(currentData, this) : new WcApplication_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcApplication_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplication_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplication_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcApplication_Values _original;
        [DataMember]
        public WcApplication_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcApplication_Values _current;
        [DataMember]
        public WcApplication_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcApplication_Values _concurrent;
        [DataMember]
        public WcApplication_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  Ap_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  AttachmentCount
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  AttachmentFileNames
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  DiscussionCount
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  DiscussionTitles
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  Ap_Name
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  Ap_Desc
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  Ap_TkSt_Id
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  Ap_TkSt_Id_TextField
                //if (_current._i != _original._i)
                //{
                //    return true;
                //}

                //  Ap_v_Application_Id
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  Ap_IsActiveRow
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  Ap_IsDeleted
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  Ap_CreatedUserId
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  Ap_CreatedDate
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  Ap_UserId
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  UserName
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  Ap_LastModifiedDate
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  Ap_Stamp
                if (_current._r != _original._r)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{19}{0}{18}_b{19}{1}{18}_c{19}{2}{18}_d{19}{3}{18}_e{19}{4}{18}_f{19}{5}{18}_g{19}{6}{18}_h{19}{7}{18}_i{19}{8}{18}_j{19}{9}{18}_k{19}{10}{18}_l{19}{11}{18}_m{19}{12}{18}_n{19}{13}{18}_o{19}{14}{18}_p{19}{15}{18}_q{19}{16}{18}_r{19}{17}",
				new object[] {
				_current._a,		  //Ap_Id
				_current._b,		  //AttachmentCount
				_current._c,		  //AttachmentFileNames
				_current._d,		  //DiscussionCount
				_current._e,		  //DiscussionTitles
				_current._f,		  //Ap_Name
				_current._g,		  //Ap_Desc
				_current._h,		  //Ap_TkSt_Id
				_current._i,		  //Ap_TkSt_Id_TextField
				_current._j,		  //Ap_v_Application_Id
				_current._k,		  //Ap_IsActiveRow
				_current._l,		  //Ap_IsDeleted
				_current._m,		  //Ap_CreatedUserId
				_current._n,		  //Ap_CreatedDate
				_current._o,		  //Ap_UserId
				_current._p,		  //UserName
				_current._q,		  //Ap_LastModifiedDate
				_current._r,		  //Ap_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcApplication_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcApplication_Values";
        private WcApplication_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcApplication_Values() 
        {
        }

        //public WcApplication_Values(object[] data, WcApplication_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcApplication_Values(object[] data, WcApplication_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplication_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Ap_Id
				_b = ObjectHelper.GetNullableIntFromObjectValue(data[1]);							//  AttachmentCount
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  AttachmentFileNames
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  DiscussionCount
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  DiscussionTitles
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  Ap_Name
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  Ap_Desc
				_h = ObjectHelper.GetNullableIntFromObjectValue(data[7]);							//  Ap_TkSt_Id
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  Ap_TkSt_Id_TextField
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  Ap_v_Application_Id
				_k = ObjectHelper.GetBoolFromObjectValue(data[10]);									//  Ap_IsActiveRow
				_l = ObjectHelper.GetBoolFromObjectValue(data[11]);									//  Ap_IsDeleted
				_m = ObjectHelper.GetNullableGuidFromObjectValue(data[12]);						//  Ap_CreatedUserId
				_n = ObjectHelper.GetNullableDateTimeFromObjectValue(data[13]);					//  Ap_CreatedDate
				_o = ObjectHelper.GetNullableGuidFromObjectValue(data[14]);						//  Ap_UserId
				_p = ObjectHelper.GetStringFromObjectValue(data[15]);									//  UserName
				_q = ObjectHelper.GetNullableDateTimeFromObjectValue(data[16]);					//  Ap_LastModifiedDate
				_r = ObjectHelper.GetByteArrayFromObjectValue(data[17]);						//  Ap_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplication_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcApplication_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplication", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Ap_Id
				_b = ObjectHelper.GetNullableIntFromObjectValue(data[1]);							//  AttachmentCount
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  AttachmentFileNames
				_d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);							//  DiscussionCount
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  DiscussionTitles
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  Ap_Name
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  Ap_Desc
				_h = ObjectHelper.GetNullableIntFromObjectValue(data[7]);							//  Ap_TkSt_Id
				_i = ObjectHelper.GetStringFromObjectValue(data[8]);									//  Ap_TkSt_Id_TextField
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  Ap_v_Application_Id
				_k = ObjectHelper.GetBoolFromObjectValue(data[10]);									//  Ap_IsActiveRow
				_l = ObjectHelper.GetBoolFromObjectValue(data[11]);									//  Ap_IsDeleted
				_m = ObjectHelper.GetNullableGuidFromObjectValue(data[12]);						//  Ap_CreatedUserId
				_n = ObjectHelper.GetNullableDateTimeFromObjectValue(data[13]);					//  Ap_CreatedDate
				_o = ObjectHelper.GetNullableGuidFromObjectValue(data[14]);						//  Ap_UserId
				_p = ObjectHelper.GetStringFromObjectValue(data[15]);									//  UserName
				_q = ObjectHelper.GetNullableDateTimeFromObjectValue(data[16]);					//  Ap_LastModifiedDate
				_r = ObjectHelper.GetByteArrayFromObjectValue(data[17]);						//  Ap_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplication", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcApplication", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  Ap_Id

		[DataMember]
		public Int32? _b;			//  AttachmentCount

		[DataMember]
		public String _c;			//  AttachmentFileNames

		[DataMember]
		public Int32? _d;			//  DiscussionCount

		[DataMember]
		public String _e;			//  DiscussionTitles

		[DataMember]
		public String _f;			//  Ap_Name

		[DataMember]
		public String _g;			//  Ap_Desc

		[DataMember]
		public Int32? _h;			//  Ap_TkSt_Id

		[DataMember]
		public String _i;			//  Ap_TkSt_Id_TextField

		[DataMember]
		public Guid? _j;			//  Ap_v_Application_Id

		[DataMember]
		public Boolean _k;			//  Ap_IsActiveRow

		[DataMember]
		public Boolean _l;			//  Ap_IsDeleted

		[DataMember]
		public Guid? _m;			//  Ap_CreatedUserId

		[DataMember]
		public DateTime? _n;			//  Ap_CreatedDate

		[DataMember]
		public Guid? _o;			//  Ap_UserId

		[DataMember]
		public String _p;			//  UserName

		[DataMember]
		public DateTime? _q;			//  Ap_LastModifiedDate

		[DataMember]
		public Byte[] _r;			//  Ap_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcApplication_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 18; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                }
                return _list;
            }
        }

        public Guid Ap_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid Ap_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Int32? AttachmentCount
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? AttachmentCount_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public String AttachmentFileNames
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String AttachmentFileNames_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public Int32? DiscussionCount
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? DiscussionCount_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String DiscussionTitles
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DiscussionTitles_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public String Ap_Name
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Ap_Name_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public String Ap_Desc
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Ap_Desc_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Int32? Ap_TkSt_Id
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? Ap_TkSt_Id_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public String Ap_TkSt_Id_TextField
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Ap_TkSt_Id_TextField_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Guid? Ap_v_Application_Id
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Ap_v_Application_Id_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public Boolean Ap_IsActiveRow
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Ap_IsActiveRow_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public Boolean Ap_IsDeleted
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Ap_IsDeleted_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public Guid? Ap_CreatedUserId
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Ap_CreatedUserId_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public DateTime? Ap_CreatedDate
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Ap_CreatedDate_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public Guid? Ap_UserId
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Ap_UserId_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public String UserName
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public DateTime? Ap_LastModifiedDate
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Ap_LastModifiedDate_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public Byte[] Ap_Stamp
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] Ap_Stamp_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r
			};
        }

        public WcApplication_Values Clone()
        {
            return new WcApplication_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


