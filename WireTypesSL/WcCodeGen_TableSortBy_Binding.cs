using System;
using System.Runtime.Serialization;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/18/2018 6:19:15 PM

namespace EntityWireTypeSL
{


    [DataContract]
    public class WcCodeGen_TableSortBy_Binding : IWireTypeBinding
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcCodeGen_TableSortBy_Binding";

        #endregion Initialize Variables


		#region Constructors

        public WcCodeGen_TableSortBy_Binding() { }


        public WcCodeGen_TableSortBy_Binding(Guid _TbSb_Id, Guid _TbSb_Tb_Id, Guid _TbSb_TbC_Id, String _TbSb_TbC_Id_TextField, Int32 _TbSb_SortOrder, String _TbSb_Direction, String _TbSb_Direction_TextField )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableSortBy_Binding", IfxTraceCategory.Enter);
				_a = _TbSb_Id;
				_b = _TbSb_Tb_Id;
				_c = _TbSb_TbC_Id;
				_d = _TbSb_TbC_Id_TextField;
				_e = _TbSb_SortOrder;
				_f = _TbSb_Direction;
				_g = _TbSb_Direction_TextField;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableSortBy_Binding", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableSortBy_Binding", IfxTraceCategory.Leave);
            }
		}

        public WcCodeGen_TableSortBy_Binding(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableSortBy_Binding", IfxTraceCategory.Enter);
				_a = (Guid)data[0];                //  TbSb_Id
				_b = (Guid)data[1];                //  TbSb_Tb_Id
				_c = (Guid)data[2];                //  TbSb_TbC_Id
				_d = (String)data[3];                //  TbSb_TbC_Id_TextField
				_e = (Int32)data[4];                //  TbSb_SortOrder
				_f = (String)data[5];                //  TbSb_Direction
				_g = (String)data[6];                //  TbSb_Direction_TextField
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableSortBy_Binding", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcCodeGen_TableSortBy_Binding", IfxTraceCategory.Leave);
            }
		}

		#endregion Constructors


        #region Item Id

        public Int32? Get_Int_Id()
        {
            return null;
        }

        public void Set_Int_Id(int value)
        {
            //_a = value;
        }

        public Guid? Get_Guid_Id()
        {
            return _a;
        }

        public void Set_Guid_Id(Guid value)
        {
            _a = value;
        }

        public String Get_String_Id()
        {
            return null;
        }

        public void Set_String_Id(String value)
        {
            //_a = value;
        }

        public Guid Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion Item Id


        #region DisplayMember

        public string DisplayMember
        {
            get { return _b.ToString(); }
        }

        #endregion DisplayMember


        #region TbSb_Id

        private Guid _a;
//        public Guid A
//        {
//            get { return _a; }
//            set { _a = value; }
//        }

        //[NonSerialized]
        public Guid TbSb_Id
        {
            get { return _a; }
            set { _a = value; }
        }

        #endregion TbSb_Id


        #region TbSb_Tb_Id

        private Guid _b;
//        public Guid B
//        {
//            get { return _b; }
//            set { _b = value; }
//        }

        //[NonSerialized]
        public Guid TbSb_Tb_Id
        {
            get { return _b; }
            set { _b = value; }
        }

        #endregion TbSb_Tb_Id


        #region TbSb_TbC_Id

        private Guid _c;
//        public Guid C
//        {
//            get { return _c; }
//            set { _c = value; }
//        }

        //[NonSerialized]
        public Guid TbSb_TbC_Id
        {
            get { return _c; }
            set { _c = value; }
        }

        #endregion TbSb_TbC_Id


        #region TbSb_TbC_Id_TextField

        private String _d;
//        public String D
//        {
//            get { return _d; }
//            set { _d = value; }
//        }

        //[NonSerialized]
        public String TbSb_TbC_Id_TextField
        {
            get { return _d; }
            set { _d = value; }
        }

        #endregion TbSb_TbC_Id_TextField


        #region TbSb_SortOrder

        private Int32 _e;
//        public Int32 E
//        {
//            get { return _e; }
//            set { _e = value; }
//        }

        //[NonSerialized]
        public Int32 TbSb_SortOrder
        {
            get { return _e; }
            set { _e = value; }
        }

        #endregion TbSb_SortOrder


        #region TbSb_Direction

        private String _f;
//        public String F
//        {
//            get { return _f; }
//            set { _f = value; }
//        }

        //[NonSerialized]
        public String TbSb_Direction
        {
            get { return _f; }
            set { _f = value; }
        }

        #endregion TbSb_Direction


        #region TbSb_Direction_TextField

        private String _g;
//        public String G
//        {
//            get { return _g; }
//            set { _g = value; }
//        }

        //[NonSerialized]
        public String TbSb_Direction_TextField
        {
            get { return _g; }
            set { _g = value; }
        }

        #endregion TbSb_Direction_TextField


        public override string ToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6} ", TbSb_Id, TbSb_Tb_Id, TbSb_TbC_Id, TbSb_TbC_Id_TextField, TbSb_SortOrder, TbSb_Direction, TbSb_Direction_TextField );
        }

        public string AllFieldsToString()
        {
            return String.Format("{0}; {1}; {2}; {3}; {4}; {5}; {6} ", TbSb_Id, TbSb_Tb_Id, TbSb_TbC_Id, TbSb_TbC_Id_TextField, TbSb_SortOrder, TbSb_Direction, TbSb_Direction_TextField );
        }

    }

}
