﻿using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  1/4/2018 12:21:45 AM

namespace EntityWireTypeSL
{
    public partial class WcTable_Values
    {



        //public WcTable_Values(object[] data, WcTable_ValuesMngr parent)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
        //        _data = data;
        //        _parent = parent;
        //        //*
        //        _a = ObjectHelper.GetNullableGuidFromObjectValue(data[0]);                              //  Tb_Id
        //        _b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);                      //  Tb_ApVrsn_Id
        //        _c = ObjectHelper.GetNullableGuidFromObjectValue(data[2]);                      //  Tb_auditTable_Id
        //        _d = ObjectHelper.GetNullableIntFromObjectValue(data[3]);                           //  AttachmentCount
        //        _e = ObjectHelper.GetStringFromObjectValue(data[4]);                                    //  AttachmentFileNames
        //        _f = ObjectHelper.GetNullableIntFromObjectValue(data[5]);                           //  DiscussionCount
        //        _g = ObjectHelper.GetStringFromObjectValue(data[6]);                                    //  DiscussionTitles
        //        _h = ObjectHelper.GetStringFromObjectValue(data[7]);                                    //  Tb_Name
        //        _i = ObjectHelper.GetStringFromObjectValue(data[8]);                                    //  TableInDatabase
        //        _j = ObjectHelper.GetStringFromObjectValue(data[9]);                                    //  Tb_EntityRootName
        //        _k = ObjectHelper.GetStringFromObjectValue(data[10]);                                   //  Tb_VariableName
        //        _l = ObjectHelper.GetStringFromObjectValue(data[11]);                                   //  Tb_ScreenCaption
        //        _m = ObjectHelper.GetStringFromObjectValue(data[12]);                                   //  Tb_Description
        //        _n = ObjectHelper.GetStringFromObjectValue(data[13]);                                   //  Tb_DevelopmentNote
        //        _o = ObjectHelper.GetStringFromObjectValue(data[14]);                                   //  Tb_UIAssemblyName
        //        _p = ObjectHelper.GetStringFromObjectValue(data[15]);                                   //  Tb_UINamespace
        //        _q = ObjectHelper.GetStringFromObjectValue(data[16]);                                   //  Tb_UIAssemblyPath
        //        _r = ObjectHelper.GetStringFromObjectValue(data[17]);                                   //  Tb_ProxyAssemblyName
        //        _s = ObjectHelper.GetStringFromObjectValue(data[18]);                                   //  Tb_ProxyAssemblyPath
        //        _t = ObjectHelper.GetStringFromObjectValue(data[19]);                                   //  Tb_WebServiceName
        //        _u = ObjectHelper.GetStringFromObjectValue(data[20]);                                   //  Tb_WebServiceFolder
        //        _v = ObjectHelper.GetStringFromObjectValue(data[21]);                                   //  Tb_DataServiceAssemblyName
        //        _w = ObjectHelper.GetStringFromObjectValue(data[22]);                                   //  Tb_DataServicePath
        //        _x = ObjectHelper.GetStringFromObjectValue(data[23]);                                   //  Tb_WireTypeAssemblyName
        //        _y = ObjectHelper.GetStringFromObjectValue(data[24]);                                   //  Tb_WireTypePath
        //        _z = ObjectHelper.GetBoolFromObjectValue(data[25]);                                 //  Tb_UseTilesInPropsScreen
        //        _aa = ObjectHelper.GetBoolFromObjectValue(data[26]);                                    //  Tb_UseGridColumnGroups
        //        _ab = ObjectHelper.GetBoolFromObjectValue(data[27]);                                    //  Tb_UseGridDataSourceCombo
        //        _ac = ObjectHelper.GetNullableGuidFromObjectValue(data[28]);                        //  Tb_DbCnSK_Id
        //        _ad = ObjectHelper.GetStringFromObjectValue(data[29]);                                  //  Tb_DbCnSK_Id_TextField
        //        _ae = ObjectHelper.GetBoolFromObjectValue(data[30]);                                    //  Tb_UseLegacyConnectionCode
        //        _af = ObjectHelper.GetBoolFromObjectValue(data[31]);                                    //  Tb_PkIsIdentity
        //        _ag = ObjectHelper.GetBoolFromObjectValue(data[32]);                                    //  Tb_IsVirtual
        //        _ah = ObjectHelper.GetNullableBoolFromObjectValue(data[33]);                    //  Tb_IsScreenPlaceHolder
        //        _ai = ObjectHelper.GetBoolFromObjectValue(data[34]);                                    //  Tb_IsNotEntity
        //        _aj = ObjectHelper.GetBoolFromObjectValue(data[35]);                                    //  Tb_IsMany2Many
        //        _ak = ObjectHelper.GetBoolFromObjectValue(data[36]);                                    //  Tb_UseLastModifiedByUserNameInSproc
        //        _al = ObjectHelper.GetBoolFromObjectValue(data[37]);                                    //  Tb_UseUserTimeStamp
        //        _am = ObjectHelper.GetBoolFromObjectValue(data[38]);                                    //  Tb_UseForAudit
        //        _an = ObjectHelper.GetNullableBoolFromObjectValue(data[39]);                    //  Tb_IsAllowDelete
        //        _ao = ObjectHelper.GetBoolFromObjectValue(data[40]);                                    //  Tb_CnfgGdMnu_MenuRow_IsVisible
        //        _ap = ObjectHelper.GetBoolFromObjectValue(data[41]);                                    //  Tb_CnfgGdMnu_GridTools_IsVisible
        //        _aq = ObjectHelper.GetBoolFromObjectValue(data[42]);                                    //  Tb_CnfgGdMnu_SplitScreen_IsVisible
        //        _ar = ObjectHelper.GetBoolFromObjectValue(data[43]);                                    //  Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
        //        _asxx = ObjectHelper.GetIntFromObjectValue(data[44]);                                       //  Tb_CnfgGdMnu_NavColumnWidth
        //        _at = ObjectHelper.GetBoolFromObjectValue(data[45]);                                    //  Tb_CnfgGdMnu_IsReadOnly
        //        _au = ObjectHelper.GetBoolFromObjectValue(data[46]);                                    //  Tb_CnfgGdMnu_IsAllowNewRow
        //        _av = ObjectHelper.GetBoolFromObjectValue(data[47]);                                    //  Tb_CnfgGdMnu_ExcelExport_IsVisible
        //        _aw = ObjectHelper.GetBoolFromObjectValue(data[48]);                                    //  Tb_CnfgGdMnu_ColumnChooser_IsVisible
        //        _ax = ObjectHelper.GetBoolFromObjectValue(data[49]);                                    //  Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
        //        _ay = ObjectHelper.GetBoolFromObjectValue(data[50]);                                    //  Tb_CnfgGdMnu_RefreshGrid_IsVisible
        //        _az = ObjectHelper.GetBoolFromObjectValue(data[51]);                                    //  Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
        //        _ba = ObjectHelper.GetBoolFromObjectValue(data[52]);                                    //  Tb_IsInputComplete
        //        _bb = ObjectHelper.GetBoolFromObjectValue(data[53]);                                    //  Tb_IsCodeGen
        //        _bc = ObjectHelper.GetBoolFromObjectValue(data[54]);                                    //  Tb_IsReadyCodeGen
        //        _bd = ObjectHelper.GetBoolFromObjectValue(data[55]);                                    //  Tb_IsCodeGenComplete
        //        _be = ObjectHelper.GetBoolFromObjectValue(data[56]);                                    //  Tb_IsTagForCodeGen
        //        _bf = ObjectHelper.GetBoolFromObjectValue(data[57]);                                    //  Tb_IsTagForOther
        //        _bg = ObjectHelper.GetStringFromObjectValue(data[58]);                                  //  Tb_TableGroups
        //        _bh = ObjectHelper.GetBoolFromObjectValue(data[59]);                                    //  Tb_IsActiveRow
        //        _bi = ObjectHelper.GetBoolFromObjectValue(data[60]);                                    //  Tb_IsDeleted
        //        _bj = ObjectHelper.GetNullableGuidFromObjectValue(data[61]);                        //  Tb_CreatedUserId
        //        _bk = ObjectHelper.GetNullableDateTimeFromObjectValue(data[62]);                    //  Tb_CreatedDate
        //        _bl = ObjectHelper.GetNullableGuidFromObjectValue(data[63]);                        //  Tb_UserId
        //        _bm = ObjectHelper.GetStringFromObjectValue(data[64]);                                  //  UserName
        //        _bn = ObjectHelper.GetNullableDateTimeFromObjectValue(data[65]);                    //  Tb_LastModifiedDate
        //        _bo = data[66] as Byte[];                       //  Tb_Stamp
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTable_ValuesMngr", IfxTraceCategory.Leave);
        //    }
        //}




        //[DataMember]
        //public Guid? _a;            //  Tb_Id



        //public Guid? Tb_Id
        //{
        //    get { return _a; }
        //    set
        //    {
        //        _a = value;
        //        _parent.SetIsDirtyFlag();
        //    }
        //}

        //public Guid? Tb_Id_noevents
        //{
        //    get { return _a; }
        //    set
        //    {
        //        _a = value;
        //    }
        //}















    }
}
