using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  1/8/2018 12:51:09 AM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcStoredProc_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProc_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcStoredProc_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcStoredProc_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcStoredProc_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcStoredProc_Values(currentData, this) : new WcStoredProc_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcStoredProc_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcStoredProc_Values _original;
        [DataMember]
        public WcStoredProc_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcStoredProc_Values _current;
        [DataMember]
        public WcStoredProc_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcStoredProc_Values _concurrent;
        [DataMember]
        public WcStoredProc_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  Sp_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  Sp_ApVrsn_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  IsImported
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  SprocInDb
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  Sp_Name
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  Sp_IsCodeGen
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  Sp_IsReadyCodeGen
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  Sp_IsCodeGenComplete
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  Sp_IsTagForCodeGen
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  Sp_IsTagForOther
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  Sp_SprocGroups
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  Sp_IsBuildDataAccessMethod
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  Sp_BuildListClass
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  Sp_IsFetchEntity
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  Sp_IsStaticList
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  Sp_IsTypeComboItem
                if (_current._p != _original._p)
                {
                    return true;
                }

                //  Sp_IsCreateWireType
                if (_current._q != _original._q)
                {
                    return true;
                }

                //  Sp_WireTypeName
                if (_current._r != _original._r)
                {
                    return true;
                }

                //  Sp_MethodName
                if (_current._s != _original._s)
                {
                    return true;
                }

                //  Sp_AssocEntity_Id
                if (_current._t != _original._t)
                {
                    return true;
                }

                //  Sp_AssocEntity_Id_TextField
                //if (_current._u != _original._u)
                //{
                //    return true;
                //}

                //  Sp_AssocEntityNames
                if (_current._v != _original._v)
                {
                    return true;
                }

                //  Sp_UseLegacyConnectionCode
                if (_current._w != _original._w)
                {
                    return true;
                }

                //  Sp_DbCnSK_Id
                if (_current._x != _original._x)
                {
                    return true;
                }

                //  Sp_DbCnSK_Id_TextField
                //if (_current._y != _original._y)
                //{
                //    return true;
                //}

                //  Sp_ApDbScm_Id
                if (_current._z != _original._z)
                {
                    return true;
                }

                //  Sp_ApDbScm_Id_TextField
                //if (_current._aa != _original._aa)
                //{
                //    return true;
                //}

                //  Sp_SpRsTp_Id
                if (_current._ab != _original._ab)
                {
                    return true;
                }

                //  Sp_SpRsTp_Id_TextField
                //if (_current._ac != _original._ac)
                //{
                //    return true;
                //}

                //  Sp_SpRtTp_Id
                if (_current._ad != _original._ad)
                {
                    return true;
                }

                //  Sp_SpRtTp_Id_TextField
                //if (_current._ae != _original._ae)
                //{
                //    return true;
                //}

                //  Sp_IsInputParamsAsObjectArray
                if (_current._af != _original._af)
                {
                    return true;
                }

                //  Sp_IsParamsAutoRefresh
                if (_current._ag != _original._ag)
                {
                    return true;
                }

                //  Sp_HasNewParams
                if (_current._ah != _original._ah)
                {
                    return true;
                }

                //  Sp_IsParamsValid
                if (_current._ai != _original._ai)
                {
                    return true;
                }

                //  Sp_IsParamValueSetValid
                if (_current._aj != _original._aj)
                {
                    return true;
                }

                //  Sp_HasNewColumns
                if (_current._ak != _original._ak)
                {
                    return true;
                }

                //  Sp_IsColumnsValid
                if (_current._al != _original._al)
                {
                    return true;
                }

                //  Sp_IsValid
                if (_current._am != _original._am)
                {
                    return true;
                }

                //  Sp_Notes
                if (_current._an != _original._an)
                {
                    return true;
                }

                //  Sp_IsActiveRow
                if (_current._ao != _original._ao)
                {
                    return true;
                }

                //  Sp_IsDeleted
                if (_current._ap != _original._ap)
                {
                    return true;
                }

                //  Sp_CreatedUserId
                if (_current._aq != _original._aq)
                {
                    return true;
                }

                //  Sp_CreatedDate
                if (_current._ar != _original._ar)
                {
                    return true;
                }

                //  Sp_UserId
                if (_current._asxx != _original._asxx)
                {
                    return true;
                }

                //  UserName
                if (_current._at != _original._at)
                {
                    return true;
                }

                //  Sp_LastModifiedDate
                if (_current._au != _original._au)
                {
                    return true;
                }

                //  Sp_Stamp
                if (_current._av != _original._av)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{50}{0}{49}_b{50}{1}{49}_c{50}{2}{49}_d{50}{3}{49}_e{50}{4}{49}_f{50}{5}{49}_g{50}{6}{49}_h{50}{7}{49}_i{50}{8}{49}_j{50}{9}{49}_k{50}{10}{49}_l{50}{11}{49}_m{50}{12}{49}_n{50}{13}{49}_o{50}{14}{49}_p{50}{15}{49}_q{50}{16}{49}_r{50}{17}{49}_s{50}{18}{49}_t{50}{19}{49}_u{50}{20}{49}_v{50}{21}{49}_w{50}{22}{49}_x{50}{23}{49}_y{50}{24}{49}_z{50}{25}{49}_aa{50}{26}{49}_ab{50}{27}{49}_ac{50}{28}{49}_ad{50}{29}{49}_ae{50}{30}{49}_af{50}{31}{49}_ag{50}{32}{49}_ah{50}{33}{49}_ai{50}{34}{49}_aj{50}{35}{49}_ak{50}{36}{49}_al{50}{37}{49}_am{50}{38}{49}_an{50}{39}{49}_ao{50}{40}{49}_ap{50}{41}{49}_aq{50}{42}{49}_ar{50}{43}{49}_asxx{50}{44}{49}_at{50}{45}{49}_au{50}{46}{49}_av{50}{47}{49}",
				new object[] {
				_current._a,		  //Sp_Id
				_current._b,		  //Sp_ApVrsn_Id
				_current._c,		  //IsImported
				_current._d,		  //SprocInDb
				_current._e,		  //Sp_Name
				_current._f,		  //Sp_IsCodeGen
				_current._g,		  //Sp_IsReadyCodeGen
				_current._h,		  //Sp_IsCodeGenComplete
				_current._i,		  //Sp_IsTagForCodeGen
				_current._j,		  //Sp_IsTagForOther
				_current._k,		  //Sp_SprocGroups
				_current._l,		  //Sp_IsBuildDataAccessMethod
				_current._m,		  //Sp_BuildListClass
				_current._n,		  //Sp_IsFetchEntity
				_current._o,		  //Sp_IsStaticList
				_current._p,		  //Sp_IsTypeComboItem
				_current._q,		  //Sp_IsCreateWireType
				_current._r,		  //Sp_WireTypeName
				_current._s,		  //Sp_MethodName
				_current._t,		  //Sp_AssocEntity_Id
				_current._u,		  //Sp_AssocEntity_Id_TextField
				_current._v,		  //Sp_AssocEntityNames
				_current._w,		  //Sp_UseLegacyConnectionCode
				_current._x,		  //Sp_DbCnSK_Id
				_current._y,		  //Sp_DbCnSK_Id_TextField
				_current._z,		  //Sp_ApDbScm_Id
				_current._aa,		  //Sp_ApDbScm_Id_TextField
				_current._ab,		  //Sp_SpRsTp_Id
				_current._ac,		  //Sp_SpRsTp_Id_TextField
				_current._ad,		  //Sp_SpRtTp_Id
				_current._ae,		  //Sp_SpRtTp_Id_TextField
				_current._af,		  //Sp_IsInputParamsAsObjectArray
				_current._ag,		  //Sp_IsParamsAutoRefresh
				_current._ah,		  //Sp_HasNewParams
				_current._ai,		  //Sp_IsParamsValid
				_current._aj,		  //Sp_IsParamValueSetValid
				_current._ak,		  //Sp_HasNewColumns
				_current._al,		  //Sp_IsColumnsValid
				_current._am,		  //Sp_IsValid
				_current._an,		  //Sp_Notes
				_current._ao,		  //Sp_IsActiveRow
				_current._ap,		  //Sp_IsDeleted
				_current._aq,		  //Sp_CreatedUserId
				_current._ar,		  //Sp_CreatedDate
				_current._asxx,		  //Sp_UserId
				_current._at,		  //UserName
				_current._au,		  //Sp_LastModifiedDate
				_current._av,		  //Sp_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcStoredProc_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcStoredProc_Values";
        private WcStoredProc_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcStoredProc_Values() 
        {
        }

        //public WcStoredProc_Values(object[] data, WcStoredProc_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcStoredProc_Values(object[] data, WcStoredProc_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Sp_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Sp_ApVrsn_Id
				_c = ObjectHelper.GetBoolFromObjectValue(data[2]);									//  IsImported
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  SprocInDb
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  Sp_Name
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  Sp_IsCodeGen
				_g = ObjectHelper.GetBoolFromObjectValue(data[6]);									//  Sp_IsReadyCodeGen
				_h = ObjectHelper.GetBoolFromObjectValue(data[7]);									//  Sp_IsCodeGenComplete
				_i = ObjectHelper.GetBoolFromObjectValue(data[8]);									//  Sp_IsTagForCodeGen
				_j = ObjectHelper.GetBoolFromObjectValue(data[9]);									//  Sp_IsTagForOther
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  Sp_SprocGroups
				_l = ObjectHelper.GetBoolFromObjectValue(data[11]);									//  Sp_IsBuildDataAccessMethod
				_m = ObjectHelper.GetBoolFromObjectValue(data[12]);									//  Sp_BuildListClass
				_n = ObjectHelper.GetBoolFromObjectValue(data[13]);									//  Sp_IsFetchEntity
				_o = ObjectHelper.GetBoolFromObjectValue(data[14]);									//  Sp_IsStaticList
				_p = ObjectHelper.GetNullableBoolFromObjectValue(data[15]);					//  Sp_IsTypeComboItem
				_q = ObjectHelper.GetBoolFromObjectValue(data[16]);									//  Sp_IsCreateWireType
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  Sp_WireTypeName
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  Sp_MethodName
				_t = ObjectHelper.GetNullableGuidFromObjectValue(data[19]);						//  Sp_AssocEntity_Id
				_u = ObjectHelper.GetStringFromObjectValue(data[20]);									//  Sp_AssocEntity_Id_TextField
				_v = ObjectHelper.GetStringFromObjectValue(data[21]);									//  Sp_AssocEntityNames
				_w = ObjectHelper.GetNullableBoolFromObjectValue(data[22]);					//  Sp_UseLegacyConnectionCode
				_x = ObjectHelper.GetNullableGuidFromObjectValue(data[23]);						//  Sp_DbCnSK_Id
				_y = ObjectHelper.GetStringFromObjectValue(data[24]);									//  Sp_DbCnSK_Id_TextField
				_z = ObjectHelper.GetNullableGuidFromObjectValue(data[25]);						//  Sp_ApDbScm_Id
				_aa = ObjectHelper.GetStringFromObjectValue(data[26]);									//  Sp_ApDbScm_Id_TextField
				_ab = ObjectHelper.GetNullableIntFromObjectValue(data[27]);							//  Sp_SpRsTp_Id
				_ac = ObjectHelper.GetStringFromObjectValue(data[28]);									//  Sp_SpRsTp_Id_TextField
				_ad = ObjectHelper.GetNullableIntFromObjectValue(data[29]);							//  Sp_SpRtTp_Id
				_ae = ObjectHelper.GetStringFromObjectValue(data[30]);									//  Sp_SpRtTp_Id_TextField
				_af = ObjectHelper.GetBoolFromObjectValue(data[31]);									//  Sp_IsInputParamsAsObjectArray
				_ag = ObjectHelper.GetBoolFromObjectValue(data[32]);									//  Sp_IsParamsAutoRefresh
				_ah = ObjectHelper.GetBoolFromObjectValue(data[33]);									//  Sp_HasNewParams
				_ai = ObjectHelper.GetBoolFromObjectValue(data[34]);									//  Sp_IsParamsValid
				_aj = ObjectHelper.GetBoolFromObjectValue(data[35]);									//  Sp_IsParamValueSetValid
				_ak = ObjectHelper.GetBoolFromObjectValue(data[36]);									//  Sp_HasNewColumns
				_al = ObjectHelper.GetBoolFromObjectValue(data[37]);									//  Sp_IsColumnsValid
				_am = ObjectHelper.GetBoolFromObjectValue(data[38]);									//  Sp_IsValid
				_an = ObjectHelper.GetStringFromObjectValue(data[39]);									//  Sp_Notes
				_ao = ObjectHelper.GetBoolFromObjectValue(data[40]);									//  Sp_IsActiveRow
				_ap = ObjectHelper.GetBoolFromObjectValue(data[41]);									//  Sp_IsDeleted
				_aq = ObjectHelper.GetNullableGuidFromObjectValue(data[42]);						//  Sp_CreatedUserId
				_ar = ObjectHelper.GetNullableDateTimeFromObjectValue(data[43]);					//  Sp_CreatedDate
				_asxx = ObjectHelper.GetNullableGuidFromObjectValue(data[44]);						//  Sp_UserId
				_at = ObjectHelper.GetStringFromObjectValue(data[45]);									//  UserName
				_au = ObjectHelper.GetNullableDateTimeFromObjectValue(data[46]);					//  Sp_LastModifiedDate
				_av = data[47] as Byte[];						//  Sp_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcStoredProc_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProc", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  Sp_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  Sp_ApVrsn_Id
				_c = ObjectHelper.GetBoolFromObjectValue(data[2]);									//  IsImported
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  SprocInDb
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  Sp_Name
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  Sp_IsCodeGen
				_g = ObjectHelper.GetBoolFromObjectValue(data[6]);									//  Sp_IsReadyCodeGen
				_h = ObjectHelper.GetBoolFromObjectValue(data[7]);									//  Sp_IsCodeGenComplete
				_i = ObjectHelper.GetBoolFromObjectValue(data[8]);									//  Sp_IsTagForCodeGen
				_j = ObjectHelper.GetBoolFromObjectValue(data[9]);									//  Sp_IsTagForOther
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  Sp_SprocGroups
				_l = ObjectHelper.GetBoolFromObjectValue(data[11]);									//  Sp_IsBuildDataAccessMethod
				_m = ObjectHelper.GetBoolFromObjectValue(data[12]);									//  Sp_BuildListClass
				_n = ObjectHelper.GetBoolFromObjectValue(data[13]);									//  Sp_IsFetchEntity
				_o = ObjectHelper.GetBoolFromObjectValue(data[14]);									//  Sp_IsStaticList
				_p = ObjectHelper.GetNullableBoolFromObjectValue(data[15]);					//  Sp_IsTypeComboItem
				_q = ObjectHelper.GetBoolFromObjectValue(data[16]);									//  Sp_IsCreateWireType
				_r = ObjectHelper.GetStringFromObjectValue(data[17]);									//  Sp_WireTypeName
				_s = ObjectHelper.GetStringFromObjectValue(data[18]);									//  Sp_MethodName
				_t = ObjectHelper.GetNullableGuidFromObjectValue(data[19]);						//  Sp_AssocEntity_Id
				_u = ObjectHelper.GetStringFromObjectValue(data[20]);									//  Sp_AssocEntity_Id_TextField
				_v = ObjectHelper.GetStringFromObjectValue(data[21]);									//  Sp_AssocEntityNames
				_w = ObjectHelper.GetNullableBoolFromObjectValue(data[22]);					//  Sp_UseLegacyConnectionCode
				_x = ObjectHelper.GetNullableGuidFromObjectValue(data[23]);						//  Sp_DbCnSK_Id
				_y = ObjectHelper.GetStringFromObjectValue(data[24]);									//  Sp_DbCnSK_Id_TextField
				_z = ObjectHelper.GetNullableGuidFromObjectValue(data[25]);						//  Sp_ApDbScm_Id
				_aa = ObjectHelper.GetStringFromObjectValue(data[26]);									//  Sp_ApDbScm_Id_TextField
				_ab = ObjectHelper.GetNullableIntFromObjectValue(data[27]);							//  Sp_SpRsTp_Id
				_ac = ObjectHelper.GetStringFromObjectValue(data[28]);									//  Sp_SpRsTp_Id_TextField
				_ad = ObjectHelper.GetNullableIntFromObjectValue(data[29]);							//  Sp_SpRtTp_Id
				_ae = ObjectHelper.GetStringFromObjectValue(data[30]);									//  Sp_SpRtTp_Id_TextField
				_af = ObjectHelper.GetBoolFromObjectValue(data[31]);									//  Sp_IsInputParamsAsObjectArray
				_ag = ObjectHelper.GetBoolFromObjectValue(data[32]);									//  Sp_IsParamsAutoRefresh
				_ah = ObjectHelper.GetBoolFromObjectValue(data[33]);									//  Sp_HasNewParams
				_ai = ObjectHelper.GetBoolFromObjectValue(data[34]);									//  Sp_IsParamsValid
				_aj = ObjectHelper.GetBoolFromObjectValue(data[35]);									//  Sp_IsParamValueSetValid
				_ak = ObjectHelper.GetBoolFromObjectValue(data[36]);									//  Sp_HasNewColumns
				_al = ObjectHelper.GetBoolFromObjectValue(data[37]);									//  Sp_IsColumnsValid
				_am = ObjectHelper.GetBoolFromObjectValue(data[38]);									//  Sp_IsValid
				_an = ObjectHelper.GetStringFromObjectValue(data[39]);									//  Sp_Notes
				_ao = ObjectHelper.GetBoolFromObjectValue(data[40]);									//  Sp_IsActiveRow
				_ap = ObjectHelper.GetBoolFromObjectValue(data[41]);									//  Sp_IsDeleted
				_aq = ObjectHelper.GetNullableGuidFromObjectValue(data[42]);						//  Sp_CreatedUserId
				_ar = ObjectHelper.GetNullableDateTimeFromObjectValue(data[43]);					//  Sp_CreatedDate
				_asxx = ObjectHelper.GetNullableGuidFromObjectValue(data[44]);						//  Sp_UserId
				_at = ObjectHelper.GetStringFromObjectValue(data[45]);									//  UserName
				_au = ObjectHelper.GetNullableDateTimeFromObjectValue(data[46]);					//  Sp_LastModifiedDate
				_av = data[47] as Byte[];						//  Sp_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProc", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcStoredProc", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  Sp_Id

		[DataMember]
		public Guid? _b;			//  Sp_ApVrsn_Id

		[DataMember]
		public Boolean _c;			//  IsImported

		[DataMember]
		public String _d;			//  SprocInDb

		[DataMember]
		public String _e;			//  Sp_Name

		[DataMember]
		public Boolean _f;			//  Sp_IsCodeGen

		[DataMember]
		public Boolean _g;			//  Sp_IsReadyCodeGen

		[DataMember]
		public Boolean _h;			//  Sp_IsCodeGenComplete

		[DataMember]
		public Boolean _i;			//  Sp_IsTagForCodeGen

		[DataMember]
		public Boolean _j;			//  Sp_IsTagForOther

		[DataMember]
		public String _k;			//  Sp_SprocGroups

		[DataMember]
		public Boolean _l;			//  Sp_IsBuildDataAccessMethod

		[DataMember]
		public Boolean _m;			//  Sp_BuildListClass

		[DataMember]
		public Boolean _n;			//  Sp_IsFetchEntity

		[DataMember]
		public Boolean _o;			//  Sp_IsStaticList

		[DataMember]
		public Boolean? _p;			//  Sp_IsTypeComboItem

		[DataMember]
		public Boolean _q;			//  Sp_IsCreateWireType

		[DataMember]
		public String _r;			//  Sp_WireTypeName

		[DataMember]
		public String _s;			//  Sp_MethodName

		[DataMember]
		public Guid? _t;			//  Sp_AssocEntity_Id

		[DataMember]
		public String _u;			//  Sp_AssocEntity_Id_TextField

		[DataMember]
		public String _v;			//  Sp_AssocEntityNames

		[DataMember]
		public Boolean? _w;			//  Sp_UseLegacyConnectionCode

		[DataMember]
		public Guid? _x;			//  Sp_DbCnSK_Id

		[DataMember]
		public String _y;			//  Sp_DbCnSK_Id_TextField

		[DataMember]
		public Guid? _z;			//  Sp_ApDbScm_Id

		[DataMember]
		public String _aa;			//  Sp_ApDbScm_Id_TextField

		[DataMember]
		public Int32? _ab;			//  Sp_SpRsTp_Id

		[DataMember]
		public String _ac;			//  Sp_SpRsTp_Id_TextField

		[DataMember]
		public Int32? _ad;			//  Sp_SpRtTp_Id

		[DataMember]
		public String _ae;			//  Sp_SpRtTp_Id_TextField

		[DataMember]
		public Boolean _af;			//  Sp_IsInputParamsAsObjectArray

		[DataMember]
		public Boolean _ag;			//  Sp_IsParamsAutoRefresh

		[DataMember]
		public Boolean _ah;			//  Sp_HasNewParams

		[DataMember]
		public Boolean _ai;			//  Sp_IsParamsValid

		[DataMember]
		public Boolean _aj;			//  Sp_IsParamValueSetValid

		[DataMember]
		public Boolean _ak;			//  Sp_HasNewColumns

		[DataMember]
		public Boolean _al;			//  Sp_IsColumnsValid

		[DataMember]
		public Boolean _am;			//  Sp_IsValid

		[DataMember]
		public String _an;			//  Sp_Notes

		[DataMember]
		public Boolean _ao;			//  Sp_IsActiveRow

		[DataMember]
		public Boolean _ap;			//  Sp_IsDeleted

		[DataMember]
		public Guid? _aq;			//  Sp_CreatedUserId

		[DataMember]
		public DateTime? _ar;			//  Sp_CreatedDate

		[DataMember]
		public Guid? _asxx;			//  Sp_UserId

		[DataMember]
		public String _at;			//  UserName

		[DataMember]
		public DateTime? _au;			//  Sp_LastModifiedDate

		[DataMember]
		public Byte[] _av;			//  Sp_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcStoredProc_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 49; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                    _list[16] = _q;
                    _list[17] = _r;
                    _list[18] = _s;
                    _list[19] = _t;
                    _list[20] = _u;
                    _list[21] = _v;
                    _list[22] = _w;
                    _list[23] = _x;
                    _list[24] = _y;
                    _list[25] = _z;
                    _list[26] = _aa;
                    _list[27] = _ab;
                    _list[28] = _ac;
                    _list[29] = _ad;
                    _list[30] = _ae;
                    _list[31] = _af;
                    _list[32] = _ag;
                    _list[33] = _ah;
                    _list[34] = _ai;
                    _list[35] = _aj;
                    _list[36] = _ak;
                    _list[37] = _al;
                    _list[38] = _am;
                    _list[39] = _an;
                    _list[40] = _ao;
                    _list[41] = _ap;
                    _list[42] = _aq;
                    _list[43] = _ar;
                    _list[44] = _asxx;
                    _list[45] = _at;
                    _list[46] = _au;
                    _list[47] = _av;
                }
                return _list;
            }
        }

        public Guid Sp_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid Sp_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? Sp_ApVrsn_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Sp_ApVrsn_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Boolean IsImported
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean IsImported_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String SprocInDb
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String SprocInDb_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String Sp_Name
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_Name_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Boolean Sp_IsCodeGen
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsCodeGen_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Boolean Sp_IsReadyCodeGen
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsReadyCodeGen_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Boolean Sp_IsCodeGenComplete
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsCodeGenComplete_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Boolean Sp_IsTagForCodeGen
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsTagForCodeGen_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Boolean Sp_IsTagForOther
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsTagForOther_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public String Sp_SprocGroups
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_SprocGroups_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public Boolean Sp_IsBuildDataAccessMethod
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsBuildDataAccessMethod_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public Boolean Sp_BuildListClass
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_BuildListClass_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public Boolean Sp_IsFetchEntity
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsFetchEntity_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public Boolean Sp_IsStaticList
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsStaticList_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public Boolean? Sp_IsTypeComboItem
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? Sp_IsTypeComboItem_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        public Boolean Sp_IsCreateWireType
        {
            get { return _q; }
            set
            {
                _q = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsCreateWireType_noevents
        {
            get { return _q; }
            set
            {
                _q = value;
            }
        }

        public String Sp_WireTypeName
        {
            get { return _r; }
            set
            {
                _r = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_WireTypeName_noevents
        {
            get { return _r; }
            set
            {
                _r = value;
            }
        }

        public String Sp_MethodName
        {
            get { return _s; }
            set
            {
                _s = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_MethodName_noevents
        {
            get { return _s; }
            set
            {
                _s = value;
            }
        }

        public Guid? Sp_AssocEntity_Id
        {
            get { return _t; }
            set
            {
                _t = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Sp_AssocEntity_Id_noevents
        {
            get { return _t; }
            set
            {
                _t = value;
            }
        }

        public String Sp_AssocEntity_Id_TextField
        {
            get { return _u; }
            set
            {
                _u = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_AssocEntity_Id_TextField_noevents
        {
            get { return _u; }
            set
            {
                _u = value;
            }
        }

        public String Sp_AssocEntityNames
        {
            get { return _v; }
            set
            {
                _v = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_AssocEntityNames_noevents
        {
            get { return _v; }
            set
            {
                _v = value;
            }
        }

        public Boolean? Sp_UseLegacyConnectionCode
        {
            get { return _w; }
            set
            {
                _w = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean? Sp_UseLegacyConnectionCode_noevents
        {
            get { return _w; }
            set
            {
                _w = value;
            }
        }

        public Guid? Sp_DbCnSK_Id
        {
            get { return _x; }
            set
            {
                _x = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Sp_DbCnSK_Id_noevents
        {
            get { return _x; }
            set
            {
                _x = value;
            }
        }

        public String Sp_DbCnSK_Id_TextField
        {
            get { return _y; }
            set
            {
                _y = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_DbCnSK_Id_TextField_noevents
        {
            get { return _y; }
            set
            {
                _y = value;
            }
        }

        public Guid? Sp_ApDbScm_Id
        {
            get { return _z; }
            set
            {
                _z = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Sp_ApDbScm_Id_noevents
        {
            get { return _z; }
            set
            {
                _z = value;
            }
        }

        public String Sp_ApDbScm_Id_TextField
        {
            get { return _aa; }
            set
            {
                _aa = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_ApDbScm_Id_TextField_noevents
        {
            get { return _aa; }
            set
            {
                _aa = value;
            }
        }

        public Int32? Sp_SpRsTp_Id
        {
            get { return _ab; }
            set
            {
                _ab = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? Sp_SpRsTp_Id_noevents
        {
            get { return _ab; }
            set
            {
                _ab = value;
            }
        }

        public String Sp_SpRsTp_Id_TextField
        {
            get { return _ac; }
            set
            {
                _ac = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_SpRsTp_Id_TextField_noevents
        {
            get { return _ac; }
            set
            {
                _ac = value;
            }
        }

        public Int32? Sp_SpRtTp_Id
        {
            get { return _ad; }
            set
            {
                _ad = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? Sp_SpRtTp_Id_noevents
        {
            get { return _ad; }
            set
            {
                _ad = value;
            }
        }

        public String Sp_SpRtTp_Id_TextField
        {
            get { return _ae; }
            set
            {
                _ae = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_SpRtTp_Id_TextField_noevents
        {
            get { return _ae; }
            set
            {
                _ae = value;
            }
        }

        public Boolean Sp_IsInputParamsAsObjectArray
        {
            get { return _af; }
            set
            {
                _af = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsInputParamsAsObjectArray_noevents
        {
            get { return _af; }
            set
            {
                _af = value;
            }
        }

        public Boolean Sp_IsParamsAutoRefresh
        {
            get { return _ag; }
            set
            {
                _ag = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsParamsAutoRefresh_noevents
        {
            get { return _ag; }
            set
            {
                _ag = value;
            }
        }

        public Boolean Sp_HasNewParams
        {
            get { return _ah; }
            set
            {
                _ah = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_HasNewParams_noevents
        {
            get { return _ah; }
            set
            {
                _ah = value;
            }
        }

        public Boolean Sp_IsParamsValid
        {
            get { return _ai; }
            set
            {
                _ai = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsParamsValid_noevents
        {
            get { return _ai; }
            set
            {
                _ai = value;
            }
        }

        public Boolean Sp_IsParamValueSetValid
        {
            get { return _aj; }
            set
            {
                _aj = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsParamValueSetValid_noevents
        {
            get { return _aj; }
            set
            {
                _aj = value;
            }
        }

        public Boolean Sp_HasNewColumns
        {
            get { return _ak; }
            set
            {
                _ak = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_HasNewColumns_noevents
        {
            get { return _ak; }
            set
            {
                _ak = value;
            }
        }

        public Boolean Sp_IsColumnsValid
        {
            get { return _al; }
            set
            {
                _al = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsColumnsValid_noevents
        {
            get { return _al; }
            set
            {
                _al = value;
            }
        }

        public Boolean Sp_IsValid
        {
            get { return _am; }
            set
            {
                _am = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsValid_noevents
        {
            get { return _am; }
            set
            {
                _am = value;
            }
        }

        public String Sp_Notes
        {
            get { return _an; }
            set
            {
                _an = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String Sp_Notes_noevents
        {
            get { return _an; }
            set
            {
                _an = value;
            }
        }

        public Boolean Sp_IsActiveRow
        {
            get { return _ao; }
            set
            {
                _ao = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsActiveRow_noevents
        {
            get { return _ao; }
            set
            {
                _ao = value;
            }
        }

        public Boolean Sp_IsDeleted
        {
            get { return _ap; }
            set
            {
                _ap = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean Sp_IsDeleted_noevents
        {
            get { return _ap; }
            set
            {
                _ap = value;
            }
        }

        public Guid? Sp_CreatedUserId
        {
            get { return _aq; }
            set
            {
                _aq = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Sp_CreatedUserId_noevents
        {
            get { return _aq; }
            set
            {
                _aq = value;
            }
        }

        public DateTime? Sp_CreatedDate
        {
            get { return _ar; }
            set
            {
                _ar = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Sp_CreatedDate_noevents
        {
            get { return _ar; }
            set
            {
                _ar = value;
            }
        }

        public Guid? Sp_UserId
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? Sp_UserId_noevents
        {
            get { return _asxx; }
            set
            {
                _asxx = value;
            }
        }

        public String UserName
        {
            get { return _at; }
            set
            {
                _at = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _at; }
            set
            {
                _at = value;
            }
        }

        public DateTime? Sp_LastModifiedDate
        {
            get { return _au; }
            set
            {
                _au = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? Sp_LastModifiedDate_noevents
        {
            get { return _au; }
            set
            {
                _au = value;
            }
        }

        public Byte[] Sp_Stamp
        {
            get { return _av; }
            set
            {
                _av = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] Sp_Stamp_noevents
        {
            get { return _av; }
            set
            {
                _av = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p,
				_q,
				_r,
				_s,
				_t,
				_u,
				_v,
				_w,
				_x,
				_y,
				_z,
				_aa,
				_ab,
				_ac,
				_ad,
				_ae,
				_af,
				_ag,
				_ah,
				_ai,
				_aj,
				_ak,
				_al,
				_am,
				_an,
				_ao,
				_ap,
				_aq,
				_ar,
				_asxx,
				_at,
				_au,
				_av
			};
        }

        public WcStoredProc_Values Clone()
        {
            return new WcStoredProc_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


