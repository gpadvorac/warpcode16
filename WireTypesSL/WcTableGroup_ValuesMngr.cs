using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  12/15/2017 12:07:24 AM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcTableGroup_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTableGroup_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcTableGroup_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcTableGroup_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcTableGroup_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableGroup_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcTableGroup_Values(currentData, this) : new WcTableGroup_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcTableGroup_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableGroup_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableGroup_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcTableGroup_Values _original;
        [DataMember]
        public WcTableGroup_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcTableGroup_Values _current;
        [DataMember]
        public WcTableGroup_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcTableGroup_Values _concurrent;
        [DataMember]
        public WcTableGroup_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  TbGrp_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  TbGrp_ApVrsn_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  TbGrp_SortOrder
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  TbGrp_Name
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  TbGrp_Desc
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  TbGrp_IsActiveRow
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  TbGrp_IsDeleted
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  TbGrp_CreatedUserId
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  TbGrp_CreatedDate
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  TbGrp_UserId
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  UserName
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  TbGrp_LastModifiedDate
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  TbGrp_Stamp
                if (_current._m != _original._m)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{14}{0}{13}_b{14}{1}{13}_c{14}{2}{13}_d{14}{3}{13}_e{14}{4}{13}_f{14}{5}{13}_g{14}{6}{13}_h{14}{7}{13}_i{14}{8}{13}_j{14}{9}{13}_k{14}{10}{13}_l{14}{11}{13}_m{14}{12}",
				new object[] {
				_current._a,		  //TbGrp_Id
				_current._b,		  //TbGrp_ApVrsn_Id
				_current._c,		  //TbGrp_SortOrder
				_current._d,		  //TbGrp_Name
				_current._e,		  //TbGrp_Desc
				_current._f,		  //TbGrp_IsActiveRow
				_current._g,		  //TbGrp_IsDeleted
				_current._h,		  //TbGrp_CreatedUserId
				_current._i,		  //TbGrp_CreatedDate
				_current._j,		  //TbGrp_UserId
				_current._k,		  //UserName
				_current._l,		  //TbGrp_LastModifiedDate
				_current._m,		  //TbGrp_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcTableGroup_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTableGroup_Values";
        private WcTableGroup_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcTableGroup_Values() 
        {
        }

        //public WcTableGroup_Values(object[] data, WcTableGroup_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcTableGroup_Values(object[] data, WcTableGroup_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableGroup_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  TbGrp_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  TbGrp_ApVrsn_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  TbGrp_SortOrder
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  TbGrp_Name
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  TbGrp_Desc
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  TbGrp_IsActiveRow
				_g = ObjectHelper.GetBoolFromObjectValue(data[6]);									//  TbGrp_IsDeleted
				_h = ObjectHelper.GetNullableGuidFromObjectValue(data[7]);						//  TbGrp_CreatedUserId
				_i = ObjectHelper.GetNullableDateTimeFromObjectValue(data[8]);					//  TbGrp_CreatedDate
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  TbGrp_UserId
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  UserName
				_l = ObjectHelper.GetNullableDateTimeFromObjectValue(data[11]);					//  TbGrp_LastModifiedDate
				_m = data[12] as Byte[];						//  TbGrp_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableGroup_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcTableGroup_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableGroup", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  TbGrp_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  TbGrp_ApVrsn_Id
				_c = ObjectHelper.GetNullableIntFromObjectValue(data[2]);							//  TbGrp_SortOrder
				_d = ObjectHelper.GetStringFromObjectValue(data[3]);									//  TbGrp_Name
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  TbGrp_Desc
				_f = ObjectHelper.GetBoolFromObjectValue(data[5]);									//  TbGrp_IsActiveRow
				_g = ObjectHelper.GetBoolFromObjectValue(data[6]);									//  TbGrp_IsDeleted
				_h = ObjectHelper.GetNullableGuidFromObjectValue(data[7]);						//  TbGrp_CreatedUserId
				_i = ObjectHelper.GetNullableDateTimeFromObjectValue(data[8]);					//  TbGrp_CreatedDate
				_j = ObjectHelper.GetNullableGuidFromObjectValue(data[9]);						//  TbGrp_UserId
				_k = ObjectHelper.GetStringFromObjectValue(data[10]);									//  UserName
				_l = ObjectHelper.GetNullableDateTimeFromObjectValue(data[11]);					//  TbGrp_LastModifiedDate
				_m = data[12] as Byte[];						//  TbGrp_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableGroup", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcTableGroup", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  TbGrp_Id

		[DataMember]
		public Guid? _b;			//  TbGrp_ApVrsn_Id

		[DataMember]
		public Int32? _c;			//  TbGrp_SortOrder

		[DataMember]
		public String _d;			//  TbGrp_Name

		[DataMember]
		public String _e;			//  TbGrp_Desc

		[DataMember]
		public Boolean _f;			//  TbGrp_IsActiveRow

		[DataMember]
		public Boolean _g;			//  TbGrp_IsDeleted

		[DataMember]
		public Guid? _h;			//  TbGrp_CreatedUserId

		[DataMember]
		public DateTime? _i;			//  TbGrp_CreatedDate

		[DataMember]
		public Guid? _j;			//  TbGrp_UserId

		[DataMember]
		public String _k;			//  UserName

		[DataMember]
		public DateTime? _l;			//  TbGrp_LastModifiedDate

		[DataMember]
		public Byte[] _m;			//  TbGrp_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcTableGroup_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 13; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                }
                return _list;
            }
        }

        public Guid TbGrp_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid TbGrp_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? TbGrp_ApVrsn_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbGrp_ApVrsn_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public Int32? TbGrp_SortOrder
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Int32? TbGrp_SortOrder_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public String TbGrp_Name
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbGrp_Name_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String TbGrp_Desc
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String TbGrp_Desc_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public Boolean TbGrp_IsActiveRow
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbGrp_IsActiveRow_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public Boolean TbGrp_IsDeleted
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean TbGrp_IsDeleted_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public Guid? TbGrp_CreatedUserId
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbGrp_CreatedUserId_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public DateTime? TbGrp_CreatedDate
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? TbGrp_CreatedDate_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Guid? TbGrp_UserId
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? TbGrp_UserId_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public String UserName
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public DateTime? TbGrp_LastModifiedDate
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? TbGrp_LastModifiedDate_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public Byte[] TbGrp_Stamp
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] TbGrp_Stamp_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m
			};
        }

        public WcTableGroup_Values Clone()
        {
            return new WcTableGroup_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


