using System;
using System.Runtime.Serialization;
using TypeServices;
using Ifx.SL;


// Gen Timestamp:  1/3/2018 10:47:36 AM

namespace EntityWireTypeSL
{

    #region Entity Values Manager
    [DataContract]
    public class WcDatabaseConnectionStringKey_ValuesMngr : ObjectBase, IEntity_ValuesMngr
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcDatabaseConnectionStringKey_ValuesMngr";

        #endregion Initialize Variables



        #region Constructors

        /*
         * Construction supports 3 scenarios:
         * 1. Ad hoc: call this() - both current and original are non-null and equivalent; may want to call SetOriginalToCurrent or
         *            adjust state after the call
         * 2. Reader: populated with an object array from reader.GetValues(); standard way to construct from a select sproc; call
         *            this(object[]) which prepares the data for the wire by having original and concurrent null for wire size optimization 
         * 3. Reader: populated with original data and concurrency data similar to 2. after a failure due to a concurrency issue; call
         *            this(object[], object[], object[]); typically after this call all three value sets are different
         * 4. object[]: used from constructors in the props and server side data classes when particular object values are in mind
         *              programatically; call this(object[], state);
        */
        public WcDatabaseConnectionStringKey_ValuesMngr() 
            : this(null, null, new EntityState(true, true, false)) 
        {
        }

        public WcDatabaseConnectionStringKey_ValuesMngr(object[] currentData, EntityState state)
            : this(currentData, null, state) { }

        private WcDatabaseConnectionStringKey_ValuesMngr(object[] currentData, object[] originalData, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcDatabaseConnectionStringKey_ValuesMngr", IfxTraceCategory.Enter);
                _current = (currentData != null ? new WcDatabaseConnectionStringKey_Values(currentData, this) : new WcDatabaseConnectionStringKey_Values());
                if (originalData == null)
                {
                    _original = _current.Clone();
                }
                else
                {
                    _original = (new WcDatabaseConnectionStringKey_Values(originalData, this));
                }
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcDatabaseConnectionStringKey_ValuesMngr", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcDatabaseConnectionStringKey_ValuesMngr", new ValuePair[] {new ValuePair("_current", _current) }, IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Set Class State After Original Fetch


        public void SetClassStateAfterFetch(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Enter);
                _state = state;
                _current.Parent = this;
                _original = _current.Clone();
                _concurrent = _current.Clone();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClassStateAfterFetch", IfxTraceCategory.Leave);
            }
        }


        #endregion Set Class State After Original Fetch



        #region Properties

        protected WcDatabaseConnectionStringKey_Values _original;
        [DataMember]
        public WcDatabaseConnectionStringKey_Values O
        {
            get { return _original; }
            set 
            { 
                _original = value;
            }
        }

        protected WcDatabaseConnectionStringKey_Values _current;
        [DataMember]
        public WcDatabaseConnectionStringKey_Values C
        {
            get { return _current; }
            set 
            { 
                _current = value; 
            }
        }

        protected WcDatabaseConnectionStringKey_Values _concurrent;
        [DataMember]
        public WcDatabaseConnectionStringKey_Values X
        {
            get { return _concurrent; }
            set 
            {
                _concurrent = value;
            }
        }

        protected EntityState _state = new EntityState(false, true, false);
        public EntityState S
        {
            get { return _state; }
            set 
            { 
                _state = value; 
            }
        }
        public void ReInitializeEntityObjectState(bool isNew, bool isValid, bool isDirty)
        {
            _current.Parent = this;
            _state = new EntityState(isNew, isValid, isDirty);
        }

        #endregion Properties


        #region Replace Data Support

        public void ReplaceData(object[] data, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Enter);
                _current.ReplaceDataFromObjectArray(data);
                _original = _current.Clone();
                _concurrent = null;
                _state = state;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceData", IfxTraceCategory.Leave);
            }
        }

        #endregion Replace Data Support


        #region Rollback Support

        public void SetCurrentToOriginal()
        {
            _current = _original.Clone();
            _state.SetNotDirty();
        }

        #endregion Rollback Support



        #region Persistence Support

        public void SetOriginalToCurrent()
        {
            _original = _current.Clone();
            _state.SetNotDirty();
        }

        internal void SetIsDirtyFlag()
        {
            if (null == _original)
            {
                _state.SetDirty();
                return;
            }
            if (IsDirty == true)
            {
                _state.SetDirty();
            }
            else
            {
                _state.SetNotDirty();
            }
        }

        private bool IsDirty
        {
            get
            {

                //  DbCnSK_Id
                if (_current._a != _original._a)
                {
                    return true;
                }

                //  DbCnSK_ApVrsn_Id
                if (_current._b != _original._b)
                {
                    return true;
                }

                //  DbCnSK_Name
                if (_current._c != _original._c)
                {
                    return true;
                }

                //  DbCnSK_IsDefault
                if (_current._d != _original._d)
                {
                    return true;
                }

                //  DbCnSK_Server
                if (_current._e != _original._e)
                {
                    return true;
                }

                //  DbCnSK_Database
                if (_current._f != _original._f)
                {
                    return true;
                }

                //  DbCnSK_UserName
                if (_current._g != _original._g)
                {
                    return true;
                }

                //  DbCnSK_Password
                if (_current._h != _original._h)
                {
                    return true;
                }

                //  DbCnSK_IsActiveRow
                if (_current._i != _original._i)
                {
                    return true;
                }

                //  DbCnSK_IsDeleted
                if (_current._j != _original._j)
                {
                    return true;
                }

                //  DbCnSK_CreatedUserId
                if (_current._k != _original._k)
                {
                    return true;
                }

                //  DbCnSK_CreatedDate
                if (_current._l != _original._l)
                {
                    return true;
                }

                //  DbCnSK_UserId
                if (_current._m != _original._m)
                {
                    return true;
                }

                //  UserName
                if (_current._n != _original._n)
                {
                    return true;
                }

                //  DbCnSK_LastModifiedDate
                if (_current._o != _original._o)
                {
                    return true;
                }

                //  DbCnSK_Stamp
                if (_current._p != _original._p)
                {
                    return true;
                }

                return false;
            }
        }


        #endregion Persistence Support


        #region Concurrency Resolution Support

        public void ResolveConcurrencyIssue()
        {
            SetOriginalToConcurrent();
            // perhaps change state after states are baked
        }

        public void SetOriginalToConcurrent()
        {
            // TODO: redesign later
            _original = _concurrent.Clone();
        }

        #endregion Concurrency Resolution Support



        #region Wire Hydration Support

        protected void SetOriginalWireToCurrentWire()
        {
            _concurrent = null;
            SetOriginalToCurrent();
        }

        #endregion Serialization Overrides



        #region Serialization Overrides


        public string AllFieldsToString(string keyValueDelimiter, string keyPairDelimiter)
        {
            return String.Format("_a{17}{0}{16}_b{17}{1}{16}_c{17}{2}{16}_d{17}{3}{16}_e{17}{4}{16}_f{17}{5}{16}_g{17}{6}{16}_h{17}{7}{16}_i{17}{8}{16}_j{17}{9}{16}_k{17}{10}{16}_l{17}{11}{16}_m{17}{12}{16}_n{17}{13}{16}_o{17}{14}{16}_p{17}{15}",
				new object[] {
				_current._a,		  //DbCnSK_Id
				_current._b,		  //DbCnSK_ApVrsn_Id
				_current._c,		  //DbCnSK_Name
				_current._d,		  //DbCnSK_IsDefault
				_current._e,		  //DbCnSK_Server
				_current._f,		  //DbCnSK_Database
				_current._g,		  //DbCnSK_UserName
				_current._h,		  //DbCnSK_Password
				_current._i,		  //DbCnSK_IsActiveRow
				_current._j,		  //DbCnSK_IsDeleted
				_current._k,		  //DbCnSK_CreatedUserId
				_current._l,		  //DbCnSK_CreatedDate
				_current._m,		  //DbCnSK_UserId
				_current._n,		  //UserName
				_current._o,		  //DbCnSK_LastModifiedDate
				_current._p,		  //DbCnSK_Stamp
				keyPairDelimiter,
                keyValueDelimiter
                });
        }

        #endregion Serialization Overrides

    }
    #endregion Entity Values Manager

    #region Entity Values
    [DataContract]
    public class WcDatabaseConnectionStringKey_Values : ObjectBase
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcDatabaseConnectionStringKey_Values";
        private WcDatabaseConnectionStringKey_ValuesMngr _parent = null;
        private object[] _data = null;

        #endregion Initialize Variables


        #region Constructors

        internal WcDatabaseConnectionStringKey_Values() 
        {
        }

        //public WcDatabaseConnectionStringKey_Values(object[] data, WcDatabaseConnectionStringKey_ValuesMngr parent)
        //    :this(data, parent)
        //{  }

        public WcDatabaseConnectionStringKey_Values(object[] data, WcDatabaseConnectionStringKey_ValuesMngr parent)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcDatabaseConnectionStringKey_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                _data = data;
                _parent = parent;
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  DbCnSK_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  DbCnSK_ApVrsn_Id
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  DbCnSK_Name
				_d = ObjectHelper.GetBoolFromObjectValue(data[3]);									//  DbCnSK_IsDefault
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  DbCnSK_Server
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  DbCnSK_Database
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  DbCnSK_UserName
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  DbCnSK_Password
				_i = ObjectHelper.GetBoolFromObjectValue(data[8]);									//  DbCnSK_IsActiveRow
				_j = ObjectHelper.GetBoolFromObjectValue(data[9]);									//  DbCnSK_IsDeleted
				_k = ObjectHelper.GetNullableGuidFromObjectValue(data[10]);						//  DbCnSK_CreatedUserId
				_l = ObjectHelper.GetNullableDateTimeFromObjectValue(data[11]);					//  DbCnSK_CreatedDate
				_m = ObjectHelper.GetNullableGuidFromObjectValue(data[12]);						//  DbCnSK_UserId
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  UserName
				_o = ObjectHelper.GetNullableDateTimeFromObjectValue(data[14]);					//  DbCnSK_LastModifiedDate
				_p = data[15] as Byte[];						//  DbCnSK_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcDatabaseConnectionStringKey_ValuesMngr", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - WcDatabaseConnectionStringKey_ValuesMngr", IfxTraceCategory.Leave);
            }
		}


        #endregion Constructors


        #region Data Methods

        public void ReplaceDataFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcDatabaseConnectionStringKey", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Leave);
				_a = ObjectHelper.GetGuidFromObjectValue(data[0]);								//  DbCnSK_Id
				_b = ObjectHelper.GetNullableGuidFromObjectValue(data[1]);						//  DbCnSK_ApVrsn_Id
				_c = ObjectHelper.GetStringFromObjectValue(data[2]);									//  DbCnSK_Name
				_d = ObjectHelper.GetBoolFromObjectValue(data[3]);									//  DbCnSK_IsDefault
				_e = ObjectHelper.GetStringFromObjectValue(data[4]);									//  DbCnSK_Server
				_f = ObjectHelper.GetStringFromObjectValue(data[5]);									//  DbCnSK_Database
				_g = ObjectHelper.GetStringFromObjectValue(data[6]);									//  DbCnSK_UserName
				_h = ObjectHelper.GetStringFromObjectValue(data[7]);									//  DbCnSK_Password
				_i = ObjectHelper.GetBoolFromObjectValue(data[8]);									//  DbCnSK_IsActiveRow
				_j = ObjectHelper.GetBoolFromObjectValue(data[9]);									//  DbCnSK_IsDeleted
				_k = ObjectHelper.GetNullableGuidFromObjectValue(data[10]);						//  DbCnSK_CreatedUserId
				_l = ObjectHelper.GetNullableDateTimeFromObjectValue(data[11]);					//  DbCnSK_CreatedDate
				_m = ObjectHelper.GetNullableGuidFromObjectValue(data[12]);						//  DbCnSK_UserId
				_n = ObjectHelper.GetStringFromObjectValue(data[13]);									//  UserName
				_o = ObjectHelper.GetNullableDateTimeFromObjectValue(data[14]);					//  DbCnSK_LastModifiedDate
				_p = data[15] as Byte[];						//  DbCnSK_Stamp
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcDatabaseConnectionStringKey", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceDataFromObjectArray - WcDatabaseConnectionStringKey", new ValuePair[] {new ValuePair("_parent", _parent) }, IfxTraceCategory.Leave);
            }
		}



        #endregion Data Methods

		#region Data Members

		[DataMember]
		public Guid _a;			//  DbCnSK_Id

		[DataMember]
		public Guid? _b;			//  DbCnSK_ApVrsn_Id

		[DataMember]
		public String _c;			//  DbCnSK_Name

		[DataMember]
		public Boolean _d;			//  DbCnSK_IsDefault

		[DataMember]
		public String _e;			//  DbCnSK_Server

		[DataMember]
		public String _f;			//  DbCnSK_Database

		[DataMember]
		public String _g;			//  DbCnSK_UserName

		[DataMember]
		public String _h;			//  DbCnSK_Password

		[DataMember]
		public Boolean _i;			//  DbCnSK_IsActiveRow

		[DataMember]
		public Boolean _j;			//  DbCnSK_IsDeleted

		[DataMember]
		public Guid? _k;			//  DbCnSK_CreatedUserId

		[DataMember]
		public DateTime? _l;			//  DbCnSK_CreatedDate

		[DataMember]
		public Guid? _m;			//  DbCnSK_UserId

		[DataMember]
		public String _n;			//  UserName

		[DataMember]
		public DateTime? _o;			//  DbCnSK_LastModifiedDate

		[DataMember]
		public Byte[] _p;			//  DbCnSK_Stamp

		[DataMember]
        public int _retCd;			//  Return Code (Success, Fail, type of failure

        [DataMember]
        public int?  _GnFlg;			//  General Flag

		#endregion Data Members


		#region Data Properties

        internal WcDatabaseConnectionStringKey_ValuesMngr Parent
        {
            get { return _parent; }
            set 
            {
                // Management Code
                _parent = value; 
            }
        }

        public int PropertyCount
        {
            get { return 16; }
        }

        object[] _list;
        public object[] PropertyList
        {
            get
            {
                if (null == _list)
                {
                    _list = new object[PropertyCount];

                    _list[0] = _a;
                    _list[1] = _b;
                    _list[2] = _c;
                    _list[3] = _d;
                    _list[4] = _e;
                    _list[5] = _f;
                    _list[6] = _g;
                    _list[7] = _h;
                    _list[8] = _i;
                    _list[9] = _j;
                    _list[10] = _k;
                    _list[11] = _l;
                    _list[12] = _m;
                    _list[13] = _n;
                    _list[14] = _o;
                    _list[15] = _p;
                }
                return _list;
            }
        }

        public Guid DbCnSK_Id
        {
            get { return _a; }
            set
            {
                _a = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid DbCnSK_Id_noevents
        {
            get { return _a; }
            set
            {
                _a = value;
            }
        }

        public Guid? DbCnSK_ApVrsn_Id
        {
            get { return _b; }
            set
            {
                _b = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? DbCnSK_ApVrsn_Id_noevents
        {
            get { return _b; }
            set
            {
                _b = value;
            }
        }

        public String DbCnSK_Name
        {
            get { return _c; }
            set
            {
                _c = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DbCnSK_Name_noevents
        {
            get { return _c; }
            set
            {
                _c = value;
            }
        }

        public Boolean DbCnSK_IsDefault
        {
            get { return _d; }
            set
            {
                _d = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean DbCnSK_IsDefault_noevents
        {
            get { return _d; }
            set
            {
                _d = value;
            }
        }

        public String DbCnSK_Server
        {
            get { return _e; }
            set
            {
                _e = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DbCnSK_Server_noevents
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }

        public String DbCnSK_Database
        {
            get { return _f; }
            set
            {
                _f = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DbCnSK_Database_noevents
        {
            get { return _f; }
            set
            {
                _f = value;
            }
        }

        public String DbCnSK_UserName
        {
            get { return _g; }
            set
            {
                _g = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DbCnSK_UserName_noevents
        {
            get { return _g; }
            set
            {
                _g = value;
            }
        }

        public String DbCnSK_Password
        {
            get { return _h; }
            set
            {
                _h = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String DbCnSK_Password_noevents
        {
            get { return _h; }
            set
            {
                _h = value;
            }
        }

        public Boolean DbCnSK_IsActiveRow
        {
            get { return _i; }
            set
            {
                _i = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean DbCnSK_IsActiveRow_noevents
        {
            get { return _i; }
            set
            {
                _i = value;
            }
        }

        public Boolean DbCnSK_IsDeleted
        {
            get { return _j; }
            set
            {
                _j = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Boolean DbCnSK_IsDeleted_noevents
        {
            get { return _j; }
            set
            {
                _j = value;
            }
        }

        public Guid? DbCnSK_CreatedUserId
        {
            get { return _k; }
            set
            {
                _k = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? DbCnSK_CreatedUserId_noevents
        {
            get { return _k; }
            set
            {
                _k = value;
            }
        }

        public DateTime? DbCnSK_CreatedDate
        {
            get { return _l; }
            set
            {
                _l = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? DbCnSK_CreatedDate_noevents
        {
            get { return _l; }
            set
            {
                _l = value;
            }
        }

        public Guid? DbCnSK_UserId
        {
            get { return _m; }
            set
            {
                _m = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Guid? DbCnSK_UserId_noevents
        {
            get { return _m; }
            set
            {
                _m = value;
            }
        }

        public String UserName
        {
            get { return _n; }
            set
            {
                _n = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public String UserName_noevents
        {
            get { return _n; }
            set
            {
                _n = value;
            }
        }

        public DateTime? DbCnSK_LastModifiedDate
        {
            get { return _o; }
            set
            {
                _o = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public DateTime? DbCnSK_LastModifiedDate_noevents
        {
            get { return _o; }
            set
            {
                _o = value;
            }
        }

        public Byte[] DbCnSK_Stamp
        {
            get { return _p; }
            set
            {
                _p = value;
                _parent.SetIsDirtyFlag();
            }
        }

        public Byte[] DbCnSK_Stamp_noevents
        {
            get { return _p; }
            set
            {
                _p = value;
            }
        }

        //  Return Code (Success, Fail, type of failure
        public Int32 ReturnCode
        {
            get { return _retCd; }
            set
            {
                _retCd = value;
            }
        }
        //  General Flag
        public Int32? GeneralFlag
        {
            get { return _GnFlg; }
            set
            {
                _GnFlg = value;
            }
        }

		#endregion Data Properties



        #region Utility Methods

        public object[] GetValues()
        {
            return new object[] 
            {
				_a,
				_b,
				_c,
				_d,
				_e,
				_f,
				_g,
				_h,
				_i,
				_j,
				_k,
				_l,
				_m,
				_n,
				_o,
				_p
			};
        }

        public WcDatabaseConnectionStringKey_Values Clone()
        {
            return new WcDatabaseConnectionStringKey_Values(this.GetValues(), _parent);
        }

        #endregion Utility Methods


    }
    #endregion Entity Values
}


