﻿using Ifx.SL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using EntityWireTypeSL;
using ProxyWrapper;
using TypeServices;
using vComboDataTypes;
using vControls;
using vUICommon;
using Velocity.SL;

namespace vLicenseAssignment
{
    public partial class ucAssignUsers : UserControl
    {


        #region Initialize Variables

        private static string _as = "vLicenseAssignment";
        private static string _cn = "ucAssignUsers";

        public event PersonAssignedToLicenseEventHandler OnLicenseAssigned;

        Guid? _currentUserId = null;

        private ComboItemList _persons = new ComboItemList(ComboItemList.DataType.GuidType);

        private vLicenseAssignmentService_ProxyWrapper _proxy = new vLicenseAssignmentService_ProxyWrapper();

        private Guid? _licenseSetId = null;

        #endregion Initialize Variables




        #region Constructors

        public ucAssignUsers()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucAssignUsers", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();


                _proxy.GetPerson_ForLicenseAssignment_ComboItemListCompleted += GetPerson_ForLicenseAssignment_ComboItemListCompleted;
                _proxy.GetPerson_ForLicenseAssignmentCompleted += GetPerson_ForLicenseAssignmentCompleted;
                _proxy.Executev_LicenseClient_AssignUserCompleted += Executev_LicenseClient_AssignUserCompleted;

                //_userProfileProxy = new aspnet_RolesService_ProxyWrapper();
                //_userProfileProxy.Execute_aspnet_UsersInRoles_AssignRolesToUserCompleted += new EventHandler<Execute_aspnet_UsersInRoles_AssignRolesToUserCompletedEventArgs>(Execute_aspnet_UsersInRoles_AssignRolesToUserCompleted);
                //_userProfileProxy.Execute_aspnet_UsersInRoles_RemoveRolesFromUserCompleted += new EventHandler<Execute_aspnet_UsersInRoles_RemoveRolesFromUserCompletedEventArgs>(Execute_aspnet_UsersInRoles_RemoveRolesFromUserCompleted);
                //_userProfileProxy.Get_aspnet_RoleNames_ByUser_AssignedToUserCompleted += new EventHandler<Get_aspnet_RoleNames_ByUser_AssignedToUserCompletedEventArgs>(Get_aspnet_RoleNames_ByUser_AssignedToUserCompleted);
                //_userProfileProxy.Get_aspnet_RoleNames_ByUser_NotAssignedToUserCompleted += new EventHandler<Get_aspnet_RoleNames_ByUser_NotAssignedToUserCompletedEventArgs>(Get_aspnet_RoleNames_ByUser_NotAssignedToUserCompleted);


                ////xgdRolesAssigned.CellClicked += new EventHandler<Infragistics.Controls.Grids.CellClickedEventArgs>(xgdRolesAssigned_CellClicked);
                ////xgdRolesAvailable.CellClicked += new EventHandler<Infragistics.Controls.Grids.CellClickedEventArgs>(xgdRolesAvailable_CellClicked);

                //xgdRolesAssigned.ItemsSource = _usersAssigned;
                //xgdRolesAvailable.ItemsSource = _usersAvailable;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucAssignUsers", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucAssignUsers", IfxTraceCategory.Leave);
            }
        }

     
        #endregion Constructors


        public void LoadData(Guid? id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadData", IfxTraceCategory.Enter);

                _licenseSetId = id;
                if (id == null)
                {
                    xgdPersonsAvailable.ItemsSource = null;
                }
                else
                {
                    _proxy.Begin_GetPerson_ForLicenseAssignment((Guid)id);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadData", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadData", IfxTraceCategory.Leave);
            }
        }


        #region Grid Buttons

        private void btnAssignPerson_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAssignPerson_Click", IfxTraceCategory.Enter);

                Person_ForLicenseAssignment_Binding data = e.Data as Person_ForLicenseAssignment_Binding;
                if (data == null) { return; }

                _proxy.Begin_Executev_LicenseClient_AssignUser((Guid)_licenseSetId, data.Pn_SecurityUserId, (Guid)Credentials.UserId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAssignPerson_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAssignPerson_Click", IfxTraceCategory.Leave);
            }
        }
        
        #endregion Grid Buttons



        #region WS Callbacks

        void GetPerson_ForLicenseAssignment_ComboItemListCompleted(object sender, GetPerson_ForLicenseAssignment_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment_ComboItemListCompleted", IfxTraceCategory.Enter);
                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];

                _persons.ReplaceList(data);
                xgdPersonsAvailable.ItemsSource = _persons;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment_ComboItemListCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }


        void GetPerson_ForLicenseAssignmentCompleted(object sender, GetPerson_ForLicenseAssignmentCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignmentCompleted", IfxTraceCategory.Enter);
                //byte[] array = e.Result;
                //object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];

                //_persons.ReplaceList(data);
                //xgdPersonsAvailable.ItemsSource = _persons;

                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                if (array != null)
                {
                    List<Person_ForLicenseAssignment_Binding> list = new List<Person_ForLicenseAssignment_Binding>();
                    for (int i = 0; i < array.GetUpperBound(0) + 1; i++)
                    {
                        list.Add(new Person_ForLicenseAssignment_Binding((object[])array[i]));
                    }
                    xgdPersonsAvailable.ItemsSource = list;
                }
                else
                {
                    xgdPersonsAvailable.ItemsSource = null;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignmentCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignmentCompleted", IfxTraceCategory.Leave);
            }
        }
        


        void Executev_LicenseClient_AssignUserCompleted(object sender, Executev_LicenseClient_AssignUserCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_LicenseClient_AssignUserCompleted", IfxTraceCategory.Enter);
                object[] ret = e.Result;

                if (ret != null)
                {
                    int? success = ret[1] as int?;
                    if (success == null || success == 0)
                    {
                        string msg = "There was a problem in assigning this person.";
                        msg = msg + System.Environment.NewLine + "If this problem continues, please contact support.";
                        MessageBox.Show(msg, "Assignment Error", MessageBoxButton.OK);
                    }
                    else
                    {
                        LoadData(_licenseSetId);
                        Guid? licCl_Id = ret[0] as Guid?;
                        PersonAssignedToLicenseEventHandler handler = OnLicenseAssigned;
                        if (handler != null)
                        {
                            handler(sender, new PersonAssignedToLicenseArgs(licCl_Id, null));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_LicenseClient_AssignUserCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_LicenseClient_AssignUserCompleted", IfxTraceCategory.Leave);
            }
        }


        #endregion WS Callbacks


    }



    #region PersonAssignedToLicense


    public delegate void PersonAssignedToLicenseEventHandler(object sender, PersonAssignedToLicenseArgs e);

    public class PersonAssignedToLicenseArgs : EventArgs
    {
        private readonly Guid? _licCl_Id;
        private readonly Guid? _licClSet_Id;
        public PersonAssignedToLicenseArgs(Guid? licCl_Id, Guid?  licClSet_Id)
        {
            _licCl_Id = licCl_Id;
            _licClSet_Id = licClSet_Id;
        }

        public Guid? v_LicCl_Id
        {
            get { return _licCl_Id; }
        }

        public Guid? v_LicClSet_Id
        {
            get { return _licClSet_Id; }
        }

        
    }


    #endregion PersonAssignedToLicense



}
