﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This code was auto-generated by SlSvcUtil, version 5.0.61118.0
// 


[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
[System.ServiceModel.ServiceContractAttribute(Namespace="", ConfigurationName="vLicenseAssignmentService")]
public interface vLicenseAssignmentService
{
    
    [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="urn:vLicenseAssignmentService/GetvLicenseAssignment_ReadOnlyStaticLists", ReplyAction="urn:vLicenseAssignmentService/GetvLicenseAssignment_ReadOnlyStaticListsResponse")]
    System.IAsyncResult BeginGetvLicenseAssignment_ReadOnlyStaticLists(System.AsyncCallback callback, object asyncState);
    
    byte[] EndGetvLicenseAssignment_ReadOnlyStaticLists(System.IAsyncResult result);
    
    [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="urn:vLicenseAssignmentService/GetPerson_ForLicenseAssignment_ComboItemList", ReplyAction="urn:vLicenseAssignmentService/GetPerson_ForLicenseAssignment_ComboItemListRespons" +
        "e")]
    System.IAsyncResult BeginGetPerson_ForLicenseAssignment_ComboItemList(System.Guid LicClSet_Id, System.AsyncCallback callback, object asyncState);
    
    byte[] EndGetPerson_ForLicenseAssignment_ComboItemList(System.IAsyncResult result);
    
    [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="urn:vLicenseAssignmentService/Executev_LicenseClient_AssignUser", ReplyAction="urn:vLicenseAssignmentService/Executev_LicenseClient_AssignUserResponse")]
    [System.ServiceModel.ServiceKnownTypeAttribute(typeof(object[]))]
    System.IAsyncResult BeginExecutev_LicenseClient_AssignUser(System.Guid v_LicCl_LicClSet_Id, System.Guid v_LicCl_AssignedUserId, System.Guid v_LicCl_UserId, System.AsyncCallback callback, object asyncState);
    
    object[] EndExecutev_LicenseClient_AssignUser(System.IAsyncResult result);
    
    [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="urn:vLicenseAssignmentService/GetPerson_ForLicenseAssignment", ReplyAction="urn:vLicenseAssignmentService/GetPerson_ForLicenseAssignmentResponse")]
    System.IAsyncResult BeginGetPerson_ForLicenseAssignment(System.Guid LicClSet_Id, System.AsyncCallback callback, object asyncState);
    
    byte[] EndGetPerson_ForLicenseAssignment(System.IAsyncResult result);
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
public interface vLicenseAssignmentServiceChannel : vLicenseAssignmentService, System.ServiceModel.IClientChannel
{
}

[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
public partial class GetvLicenseAssignment_ReadOnlyStaticListsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
{
    
    private object[] results;
    
    public GetvLicenseAssignment_ReadOnlyStaticListsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState)
    {
        this.results = results;
    }
    
    public byte[] Result
    {
        get
        {
            base.RaiseExceptionIfNecessary();
            return ((byte[])(this.results[0]));
        }
    }
}

[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
public partial class GetPerson_ForLicenseAssignment_ComboItemListCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
{
    
    private object[] results;
    
    public GetPerson_ForLicenseAssignment_ComboItemListCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState)
    {
        this.results = results;
    }
    
    public byte[] Result
    {
        get
        {
            base.RaiseExceptionIfNecessary();
            return ((byte[])(this.results[0]));
        }
    }
}

[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
public partial class Executev_LicenseClient_AssignUserCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
{
    
    private object[] results;
    
    public Executev_LicenseClient_AssignUserCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState)
    {
        this.results = results;
    }
    
    public object[] Result
    {
        get
        {
            base.RaiseExceptionIfNecessary();
            return ((object[])(this.results[0]));
        }
    }
}

[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
public partial class GetPerson_ForLicenseAssignmentCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
{
    
    private object[] results;
    
    public GetPerson_ForLicenseAssignmentCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState)
    {
        this.results = results;
    }
    
    public byte[] Result
    {
        get
        {
            base.RaiseExceptionIfNecessary();
            return ((byte[])(this.results[0]));
        }
    }
}

[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
public partial class vLicenseAssignmentServiceClient : System.ServiceModel.ClientBase<vLicenseAssignmentService>, vLicenseAssignmentService
{
    
    private BeginOperationDelegate onBeginGetvLicenseAssignment_ReadOnlyStaticListsDelegate;
    
    private EndOperationDelegate onEndGetvLicenseAssignment_ReadOnlyStaticListsDelegate;
    
    private System.Threading.SendOrPostCallback onGetvLicenseAssignment_ReadOnlyStaticListsCompletedDelegate;
    
    private BeginOperationDelegate onBeginGetPerson_ForLicenseAssignment_ComboItemListDelegate;
    
    private EndOperationDelegate onEndGetPerson_ForLicenseAssignment_ComboItemListDelegate;
    
    private System.Threading.SendOrPostCallback onGetPerson_ForLicenseAssignment_ComboItemListCompletedDelegate;
    
    private BeginOperationDelegate onBeginExecutev_LicenseClient_AssignUserDelegate;
    
    private EndOperationDelegate onEndExecutev_LicenseClient_AssignUserDelegate;
    
    private System.Threading.SendOrPostCallback onExecutev_LicenseClient_AssignUserCompletedDelegate;
    
    private BeginOperationDelegate onBeginGetPerson_ForLicenseAssignmentDelegate;
    
    private EndOperationDelegate onEndGetPerson_ForLicenseAssignmentDelegate;
    
    private System.Threading.SendOrPostCallback onGetPerson_ForLicenseAssignmentCompletedDelegate;
    
    private BeginOperationDelegate onBeginOpenDelegate;
    
    private EndOperationDelegate onEndOpenDelegate;
    
    private System.Threading.SendOrPostCallback onOpenCompletedDelegate;
    
    private BeginOperationDelegate onBeginCloseDelegate;
    
    private EndOperationDelegate onEndCloseDelegate;
    
    private System.Threading.SendOrPostCallback onCloseCompletedDelegate;
    
    public vLicenseAssignmentServiceClient()
    {
    }
    
    public vLicenseAssignmentServiceClient(string endpointConfigurationName) : 
            base(endpointConfigurationName)
    {
    }
    
    public vLicenseAssignmentServiceClient(string endpointConfigurationName, string remoteAddress) : 
            base(endpointConfigurationName, remoteAddress)
    {
    }
    
    public vLicenseAssignmentServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
            base(endpointConfigurationName, remoteAddress)
    {
    }
    
    public vLicenseAssignmentServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
            base(binding, remoteAddress)
    {
    }
    
    public System.Net.CookieContainer CookieContainer
    {
        get
        {
            System.ServiceModel.Channels.IHttpCookieContainerManager httpCookieContainerManager = this.InnerChannel.GetProperty<System.ServiceModel.Channels.IHttpCookieContainerManager>();
            if ((httpCookieContainerManager != null))
            {
                return httpCookieContainerManager.CookieContainer;
            }
            else
            {
                return null;
            }
        }
        set
        {
            System.ServiceModel.Channels.IHttpCookieContainerManager httpCookieContainerManager = this.InnerChannel.GetProperty<System.ServiceModel.Channels.IHttpCookieContainerManager>();
            if ((httpCookieContainerManager != null))
            {
                httpCookieContainerManager.CookieContainer = value;
            }
            else
            {
                throw new System.InvalidOperationException("Unable to set the CookieContainer. Please make sure the binding contains an HttpC" +
                        "ookieContainerBindingElement.");
            }
        }
    }
    
    public event System.EventHandler<GetvLicenseAssignment_ReadOnlyStaticListsCompletedEventArgs> GetvLicenseAssignment_ReadOnlyStaticListsCompleted;
    
    public event System.EventHandler<GetPerson_ForLicenseAssignment_ComboItemListCompletedEventArgs> GetPerson_ForLicenseAssignment_ComboItemListCompleted;
    
    public event System.EventHandler<Executev_LicenseClient_AssignUserCompletedEventArgs> Executev_LicenseClient_AssignUserCompleted;
    
    public event System.EventHandler<GetPerson_ForLicenseAssignmentCompletedEventArgs> GetPerson_ForLicenseAssignmentCompleted;
    
    public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> OpenCompleted;
    
    public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> CloseCompleted;
    
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    System.IAsyncResult vLicenseAssignmentService.BeginGetvLicenseAssignment_ReadOnlyStaticLists(System.AsyncCallback callback, object asyncState)
    {
        return base.Channel.BeginGetvLicenseAssignment_ReadOnlyStaticLists(callback, asyncState);
    }
    
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    byte[] vLicenseAssignmentService.EndGetvLicenseAssignment_ReadOnlyStaticLists(System.IAsyncResult result)
    {
        return base.Channel.EndGetvLicenseAssignment_ReadOnlyStaticLists(result);
    }
    
    private System.IAsyncResult OnBeginGetvLicenseAssignment_ReadOnlyStaticLists(object[] inValues, System.AsyncCallback callback, object asyncState)
    {
        return ((vLicenseAssignmentService)(this)).BeginGetvLicenseAssignment_ReadOnlyStaticLists(callback, asyncState);
    }
    
    private object[] OnEndGetvLicenseAssignment_ReadOnlyStaticLists(System.IAsyncResult result)
    {
        byte[] retVal = ((vLicenseAssignmentService)(this)).EndGetvLicenseAssignment_ReadOnlyStaticLists(result);
        return new object[] {
                retVal};
    }
    
    private void OnGetvLicenseAssignment_ReadOnlyStaticListsCompleted(object state)
    {
        if ((this.GetvLicenseAssignment_ReadOnlyStaticListsCompleted != null))
        {
            InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
            this.GetvLicenseAssignment_ReadOnlyStaticListsCompleted(this, new GetvLicenseAssignment_ReadOnlyStaticListsCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
        }
    }
    
    public void GetvLicenseAssignment_ReadOnlyStaticListsAsync()
    {
        this.GetvLicenseAssignment_ReadOnlyStaticListsAsync(null);
    }
    
    public void GetvLicenseAssignment_ReadOnlyStaticListsAsync(object userState)
    {
        if ((this.onBeginGetvLicenseAssignment_ReadOnlyStaticListsDelegate == null))
        {
            this.onBeginGetvLicenseAssignment_ReadOnlyStaticListsDelegate = new BeginOperationDelegate(this.OnBeginGetvLicenseAssignment_ReadOnlyStaticLists);
        }
        if ((this.onEndGetvLicenseAssignment_ReadOnlyStaticListsDelegate == null))
        {
            this.onEndGetvLicenseAssignment_ReadOnlyStaticListsDelegate = new EndOperationDelegate(this.OnEndGetvLicenseAssignment_ReadOnlyStaticLists);
        }
        if ((this.onGetvLicenseAssignment_ReadOnlyStaticListsCompletedDelegate == null))
        {
            this.onGetvLicenseAssignment_ReadOnlyStaticListsCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnGetvLicenseAssignment_ReadOnlyStaticListsCompleted);
        }
        base.InvokeAsync(this.onBeginGetvLicenseAssignment_ReadOnlyStaticListsDelegate, null, this.onEndGetvLicenseAssignment_ReadOnlyStaticListsDelegate, this.onGetvLicenseAssignment_ReadOnlyStaticListsCompletedDelegate, userState);
    }
    
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    System.IAsyncResult vLicenseAssignmentService.BeginGetPerson_ForLicenseAssignment_ComboItemList(System.Guid LicClSet_Id, System.AsyncCallback callback, object asyncState)
    {
        return base.Channel.BeginGetPerson_ForLicenseAssignment_ComboItemList(LicClSet_Id, callback, asyncState);
    }
    
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    byte[] vLicenseAssignmentService.EndGetPerson_ForLicenseAssignment_ComboItemList(System.IAsyncResult result)
    {
        return base.Channel.EndGetPerson_ForLicenseAssignment_ComboItemList(result);
    }
    
    private System.IAsyncResult OnBeginGetPerson_ForLicenseAssignment_ComboItemList(object[] inValues, System.AsyncCallback callback, object asyncState)
    {
        System.Guid LicClSet_Id = ((System.Guid)(inValues[0]));
        return ((vLicenseAssignmentService)(this)).BeginGetPerson_ForLicenseAssignment_ComboItemList(LicClSet_Id, callback, asyncState);
    }
    
    private object[] OnEndGetPerson_ForLicenseAssignment_ComboItemList(System.IAsyncResult result)
    {
        byte[] retVal = ((vLicenseAssignmentService)(this)).EndGetPerson_ForLicenseAssignment_ComboItemList(result);
        return new object[] {
                retVal};
    }
    
    private void OnGetPerson_ForLicenseAssignment_ComboItemListCompleted(object state)
    {
        if ((this.GetPerson_ForLicenseAssignment_ComboItemListCompleted != null))
        {
            InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
            this.GetPerson_ForLicenseAssignment_ComboItemListCompleted(this, new GetPerson_ForLicenseAssignment_ComboItemListCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
        }
    }
    
    public void GetPerson_ForLicenseAssignment_ComboItemListAsync(System.Guid LicClSet_Id)
    {
        this.GetPerson_ForLicenseAssignment_ComboItemListAsync(LicClSet_Id, null);
    }
    
    public void GetPerson_ForLicenseAssignment_ComboItemListAsync(System.Guid LicClSet_Id, object userState)
    {
        if ((this.onBeginGetPerson_ForLicenseAssignment_ComboItemListDelegate == null))
        {
            this.onBeginGetPerson_ForLicenseAssignment_ComboItemListDelegate = new BeginOperationDelegate(this.OnBeginGetPerson_ForLicenseAssignment_ComboItemList);
        }
        if ((this.onEndGetPerson_ForLicenseAssignment_ComboItemListDelegate == null))
        {
            this.onEndGetPerson_ForLicenseAssignment_ComboItemListDelegate = new EndOperationDelegate(this.OnEndGetPerson_ForLicenseAssignment_ComboItemList);
        }
        if ((this.onGetPerson_ForLicenseAssignment_ComboItemListCompletedDelegate == null))
        {
            this.onGetPerson_ForLicenseAssignment_ComboItemListCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnGetPerson_ForLicenseAssignment_ComboItemListCompleted);
        }
        base.InvokeAsync(this.onBeginGetPerson_ForLicenseAssignment_ComboItemListDelegate, new object[] {
                    LicClSet_Id}, this.onEndGetPerson_ForLicenseAssignment_ComboItemListDelegate, this.onGetPerson_ForLicenseAssignment_ComboItemListCompletedDelegate, userState);
    }
    
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    System.IAsyncResult vLicenseAssignmentService.BeginExecutev_LicenseClient_AssignUser(System.Guid v_LicCl_LicClSet_Id, System.Guid v_LicCl_AssignedUserId, System.Guid v_LicCl_UserId, System.AsyncCallback callback, object asyncState)
    {
        return base.Channel.BeginExecutev_LicenseClient_AssignUser(v_LicCl_LicClSet_Id, v_LicCl_AssignedUserId, v_LicCl_UserId, callback, asyncState);
    }
    
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    object[] vLicenseAssignmentService.EndExecutev_LicenseClient_AssignUser(System.IAsyncResult result)
    {
        return base.Channel.EndExecutev_LicenseClient_AssignUser(result);
    }
    
    private System.IAsyncResult OnBeginExecutev_LicenseClient_AssignUser(object[] inValues, System.AsyncCallback callback, object asyncState)
    {
        System.Guid v_LicCl_LicClSet_Id = ((System.Guid)(inValues[0]));
        System.Guid v_LicCl_AssignedUserId = ((System.Guid)(inValues[1]));
        System.Guid v_LicCl_UserId = ((System.Guid)(inValues[2]));
        return ((vLicenseAssignmentService)(this)).BeginExecutev_LicenseClient_AssignUser(v_LicCl_LicClSet_Id, v_LicCl_AssignedUserId, v_LicCl_UserId, callback, asyncState);
    }
    
    private object[] OnEndExecutev_LicenseClient_AssignUser(System.IAsyncResult result)
    {
        object[] retVal = ((vLicenseAssignmentService)(this)).EndExecutev_LicenseClient_AssignUser(result);
        return new object[] {
                retVal};
    }
    
    private void OnExecutev_LicenseClient_AssignUserCompleted(object state)
    {
        if ((this.Executev_LicenseClient_AssignUserCompleted != null))
        {
            InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
            this.Executev_LicenseClient_AssignUserCompleted(this, new Executev_LicenseClient_AssignUserCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
        }
    }
    
    public void Executev_LicenseClient_AssignUserAsync(System.Guid v_LicCl_LicClSet_Id, System.Guid v_LicCl_AssignedUserId, System.Guid v_LicCl_UserId)
    {
        this.Executev_LicenseClient_AssignUserAsync(v_LicCl_LicClSet_Id, v_LicCl_AssignedUserId, v_LicCl_UserId, null);
    }
    
    public void Executev_LicenseClient_AssignUserAsync(System.Guid v_LicCl_LicClSet_Id, System.Guid v_LicCl_AssignedUserId, System.Guid v_LicCl_UserId, object userState)
    {
        if ((this.onBeginExecutev_LicenseClient_AssignUserDelegate == null))
        {
            this.onBeginExecutev_LicenseClient_AssignUserDelegate = new BeginOperationDelegate(this.OnBeginExecutev_LicenseClient_AssignUser);
        }
        if ((this.onEndExecutev_LicenseClient_AssignUserDelegate == null))
        {
            this.onEndExecutev_LicenseClient_AssignUserDelegate = new EndOperationDelegate(this.OnEndExecutev_LicenseClient_AssignUser);
        }
        if ((this.onExecutev_LicenseClient_AssignUserCompletedDelegate == null))
        {
            this.onExecutev_LicenseClient_AssignUserCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnExecutev_LicenseClient_AssignUserCompleted);
        }
        base.InvokeAsync(this.onBeginExecutev_LicenseClient_AssignUserDelegate, new object[] {
                    v_LicCl_LicClSet_Id,
                    v_LicCl_AssignedUserId,
                    v_LicCl_UserId}, this.onEndExecutev_LicenseClient_AssignUserDelegate, this.onExecutev_LicenseClient_AssignUserCompletedDelegate, userState);
    }
    
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    System.IAsyncResult vLicenseAssignmentService.BeginGetPerson_ForLicenseAssignment(System.Guid LicClSet_Id, System.AsyncCallback callback, object asyncState)
    {
        return base.Channel.BeginGetPerson_ForLicenseAssignment(LicClSet_Id, callback, asyncState);
    }
    
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    byte[] vLicenseAssignmentService.EndGetPerson_ForLicenseAssignment(System.IAsyncResult result)
    {
        return base.Channel.EndGetPerson_ForLicenseAssignment(result);
    }
    
    private System.IAsyncResult OnBeginGetPerson_ForLicenseAssignment(object[] inValues, System.AsyncCallback callback, object asyncState)
    {
        System.Guid LicClSet_Id = ((System.Guid)(inValues[0]));
        return ((vLicenseAssignmentService)(this)).BeginGetPerson_ForLicenseAssignment(LicClSet_Id, callback, asyncState);
    }
    
    private object[] OnEndGetPerson_ForLicenseAssignment(System.IAsyncResult result)
    {
        byte[] retVal = ((vLicenseAssignmentService)(this)).EndGetPerson_ForLicenseAssignment(result);
        return new object[] {
                retVal};
    }
    
    private void OnGetPerson_ForLicenseAssignmentCompleted(object state)
    {
        if ((this.GetPerson_ForLicenseAssignmentCompleted != null))
        {
            InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
            this.GetPerson_ForLicenseAssignmentCompleted(this, new GetPerson_ForLicenseAssignmentCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
        }
    }
    
    public void GetPerson_ForLicenseAssignmentAsync(System.Guid LicClSet_Id)
    {
        this.GetPerson_ForLicenseAssignmentAsync(LicClSet_Id, null);
    }
    
    public void GetPerson_ForLicenseAssignmentAsync(System.Guid LicClSet_Id, object userState)
    {
        if ((this.onBeginGetPerson_ForLicenseAssignmentDelegate == null))
        {
            this.onBeginGetPerson_ForLicenseAssignmentDelegate = new BeginOperationDelegate(this.OnBeginGetPerson_ForLicenseAssignment);
        }
        if ((this.onEndGetPerson_ForLicenseAssignmentDelegate == null))
        {
            this.onEndGetPerson_ForLicenseAssignmentDelegate = new EndOperationDelegate(this.OnEndGetPerson_ForLicenseAssignment);
        }
        if ((this.onGetPerson_ForLicenseAssignmentCompletedDelegate == null))
        {
            this.onGetPerson_ForLicenseAssignmentCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnGetPerson_ForLicenseAssignmentCompleted);
        }
        base.InvokeAsync(this.onBeginGetPerson_ForLicenseAssignmentDelegate, new object[] {
                    LicClSet_Id}, this.onEndGetPerson_ForLicenseAssignmentDelegate, this.onGetPerson_ForLicenseAssignmentCompletedDelegate, userState);
    }
    
    private System.IAsyncResult OnBeginOpen(object[] inValues, System.AsyncCallback callback, object asyncState)
    {
        return ((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(callback, asyncState);
    }
    
    private object[] OnEndOpen(System.IAsyncResult result)
    {
        ((System.ServiceModel.ICommunicationObject)(this)).EndOpen(result);
        return null;
    }
    
    private void OnOpenCompleted(object state)
    {
        if ((this.OpenCompleted != null))
        {
            InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
            this.OpenCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
        }
    }
    
    public void OpenAsync()
    {
        this.OpenAsync(null);
    }
    
    public void OpenAsync(object userState)
    {
        if ((this.onBeginOpenDelegate == null))
        {
            this.onBeginOpenDelegate = new BeginOperationDelegate(this.OnBeginOpen);
        }
        if ((this.onEndOpenDelegate == null))
        {
            this.onEndOpenDelegate = new EndOperationDelegate(this.OnEndOpen);
        }
        if ((this.onOpenCompletedDelegate == null))
        {
            this.onOpenCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnOpenCompleted);
        }
        base.InvokeAsync(this.onBeginOpenDelegate, null, this.onEndOpenDelegate, this.onOpenCompletedDelegate, userState);
    }
    
    private System.IAsyncResult OnBeginClose(object[] inValues, System.AsyncCallback callback, object asyncState)
    {
        return ((System.ServiceModel.ICommunicationObject)(this)).BeginClose(callback, asyncState);
    }
    
    private object[] OnEndClose(System.IAsyncResult result)
    {
        ((System.ServiceModel.ICommunicationObject)(this)).EndClose(result);
        return null;
    }
    
    private void OnCloseCompleted(object state)
    {
        if ((this.CloseCompleted != null))
        {
            InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
            this.CloseCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
        }
    }
    
    public void CloseAsync()
    {
        this.CloseAsync(null);
    }
    
    public void CloseAsync(object userState)
    {
        if ((this.onBeginCloseDelegate == null))
        {
            this.onBeginCloseDelegate = new BeginOperationDelegate(this.OnBeginClose);
        }
        if ((this.onEndCloseDelegate == null))
        {
            this.onEndCloseDelegate = new EndOperationDelegate(this.OnEndClose);
        }
        if ((this.onCloseCompletedDelegate == null))
        {
            this.onCloseCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnCloseCompleted);
        }
        base.InvokeAsync(this.onBeginCloseDelegate, null, this.onEndCloseDelegate, this.onCloseCompletedDelegate, userState);
    }
    
    protected override vLicenseAssignmentService CreateChannel()
    {
        return new vLicenseAssignmentServiceClientChannel(this);
    }
    
    private class vLicenseAssignmentServiceClientChannel : ChannelBase<vLicenseAssignmentService>, vLicenseAssignmentService
    {
        
        public vLicenseAssignmentServiceClientChannel(System.ServiceModel.ClientBase<vLicenseAssignmentService> client) : 
                base(client)
        {
        }
        
        public System.IAsyncResult BeginGetvLicenseAssignment_ReadOnlyStaticLists(System.AsyncCallback callback, object asyncState)
        {
            object[] _args = new object[0];
            System.IAsyncResult _result = base.BeginInvoke("GetvLicenseAssignment_ReadOnlyStaticLists", _args, callback, asyncState);
            return _result;
        }
        
        public byte[] EndGetvLicenseAssignment_ReadOnlyStaticLists(System.IAsyncResult result)
        {
            object[] _args = new object[0];
            byte[] _result = ((byte[])(base.EndInvoke("GetvLicenseAssignment_ReadOnlyStaticLists", _args, result)));
            return _result;
        }
        
        public System.IAsyncResult BeginGetPerson_ForLicenseAssignment_ComboItemList(System.Guid LicClSet_Id, System.AsyncCallback callback, object asyncState)
        {
            object[] _args = new object[1];
            _args[0] = LicClSet_Id;
            System.IAsyncResult _result = base.BeginInvoke("GetPerson_ForLicenseAssignment_ComboItemList", _args, callback, asyncState);
            return _result;
        }
        
        public byte[] EndGetPerson_ForLicenseAssignment_ComboItemList(System.IAsyncResult result)
        {
            object[] _args = new object[0];
            byte[] _result = ((byte[])(base.EndInvoke("GetPerson_ForLicenseAssignment_ComboItemList", _args, result)));
            return _result;
        }
        
        public System.IAsyncResult BeginExecutev_LicenseClient_AssignUser(System.Guid v_LicCl_LicClSet_Id, System.Guid v_LicCl_AssignedUserId, System.Guid v_LicCl_UserId, System.AsyncCallback callback, object asyncState)
        {
            object[] _args = new object[3];
            _args[0] = v_LicCl_LicClSet_Id;
            _args[1] = v_LicCl_AssignedUserId;
            _args[2] = v_LicCl_UserId;
            System.IAsyncResult _result = base.BeginInvoke("Executev_LicenseClient_AssignUser", _args, callback, asyncState);
            return _result;
        }
        
        public object[] EndExecutev_LicenseClient_AssignUser(System.IAsyncResult result)
        {
            object[] _args = new object[0];
            object[] _result = ((object[])(base.EndInvoke("Executev_LicenseClient_AssignUser", _args, result)));
            return _result;
        }
        
        public System.IAsyncResult BeginGetPerson_ForLicenseAssignment(System.Guid LicClSet_Id, System.AsyncCallback callback, object asyncState)
        {
            object[] _args = new object[1];
            _args[0] = LicClSet_Id;
            System.IAsyncResult _result = base.BeginInvoke("GetPerson_ForLicenseAssignment", _args, callback, asyncState);
            return _result;
        }
        
        public byte[] EndGetPerson_ForLicenseAssignment(System.IAsyncResult result)
        {
            object[] _args = new object[0];
            byte[] _result = ((byte[])(base.EndInvoke("GetPerson_ForLicenseAssignment", _args, result)));
            return _result;
        }
    }
}
