using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  3/16/2017 1:20:53 PM

namespace ProxyWrapper
{
    public partial class vLicenseAssignmentService_ProxyWrapper
    {


        #region Events
		public event System.EventHandler<GetPerson_ForLicenseAssignment_ComboItemListCompletedEventArgs> GetPerson_ForLicenseAssignment_ComboItemListCompleted;
		public event System.EventHandler<Executev_LicenseClient_AssignUserCompletedEventArgs> Executev_LicenseClient_AssignUserCompleted;
		public event System.EventHandler<GetPerson_ForLicenseAssignmentCompletedEventArgs> GetPerson_ForLicenseAssignmentCompleted;

        #endregion Events

        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #region GetPerson_ForLicenseAssignment_ComboItemList

        public void Begin_GetPerson_ForLicenseAssignment_ComboItemList(Guid LicClSet_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_ForLicenseAssignment_ComboItemList", IfxTraceCategory.Enter);
                vLicenseAssignmentServiceClient proxy = new vLicenseAssignmentServiceClient();
                AssignCredentials(proxy);
                proxy.GetPerson_ForLicenseAssignment_ComboItemListCompleted += new EventHandler<GetPerson_ForLicenseAssignment_ComboItemListCompletedEventArgs>(proxy_GetPerson_ForLicenseAssignment_ComboItemListCompleted);
                proxy.GetPerson_ForLicenseAssignment_ComboItemListAsync(LicClSet_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_ForLicenseAssignment_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_ForLicenseAssignment_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetPerson_ForLicenseAssignment_ComboItemListCompleted(object sender, GetPerson_ForLicenseAssignment_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetPerson_ForLicenseAssignment_ComboItemListCompletedEventArgs> handler = GetPerson_ForLicenseAssignment_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetPerson_ForLicenseAssignment_ComboItemList

        #region Executev_LicenseClient_AssignUser

        public void Begin_Executev_LicenseClient_AssignUser(Guid v_LicCl_LicClSet_Id, Guid v_LicCl_AssignedUserId, Guid v_LicCl_UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Executev_LicenseClient_AssignUser", IfxTraceCategory.Enter);
                vLicenseAssignmentServiceClient proxy = new vLicenseAssignmentServiceClient();
                AssignCredentials(proxy);
                proxy.Executev_LicenseClient_AssignUserCompleted += new EventHandler<Executev_LicenseClient_AssignUserCompletedEventArgs>(proxy_Executev_LicenseClient_AssignUserCompleted);
                proxy.Executev_LicenseClient_AssignUserAsync(v_LicCl_LicClSet_Id, v_LicCl_AssignedUserId, v_LicCl_UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Executev_LicenseClient_AssignUser", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Executev_LicenseClient_AssignUser", IfxTraceCategory.Leave);
            }
        }

        void proxy_Executev_LicenseClient_AssignUserCompleted(object sender, Executev_LicenseClient_AssignUserCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_LicenseClient_AssignUser", IfxTraceCategory.Enter);
                System.EventHandler<Executev_LicenseClient_AssignUserCompletedEventArgs> handler = Executev_LicenseClient_AssignUserCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_LicenseClient_AssignUser", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_LicenseClient_AssignUser", IfxTraceCategory.Leave);
            }
        }

        #endregion Executev_LicenseClient_AssignUser

        #region GetPerson_ForLicenseAssignment

        public void Begin_GetPerson_ForLicenseAssignment(Guid LicClSet_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_ForLicenseAssignment", IfxTraceCategory.Enter);
                vLicenseAssignmentServiceClient proxy = new vLicenseAssignmentServiceClient();
                AssignCredentials(proxy);
                proxy.GetPerson_ForLicenseAssignmentCompleted += new EventHandler<GetPerson_ForLicenseAssignmentCompletedEventArgs>(proxy_GetPerson_ForLicenseAssignmentCompleted);
                proxy.GetPerson_ForLicenseAssignmentAsync(LicClSet_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_ForLicenseAssignment", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_ForLicenseAssignment", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetPerson_ForLicenseAssignmentCompleted(object sender, GetPerson_ForLicenseAssignmentCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment", IfxTraceCategory.Enter);
                System.EventHandler<GetPerson_ForLicenseAssignmentCompletedEventArgs> handler = GetPerson_ForLicenseAssignmentCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment", IfxTraceCategory.Leave);
            }
        }

        #endregion GetPerson_ForLicenseAssignment

    }
}


