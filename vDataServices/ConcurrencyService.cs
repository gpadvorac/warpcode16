﻿using TypeServices;

namespace vDataServices
{
    public class ConcurrencyService
    {

        public static bool BypassConcurrencyCheck(UseConcurrencyCheck check)
        {

            if (check == UseConcurrencyCheck.BypassConcurrencyCheck)
            {
                return true;
            }
            else if (check == UseConcurrencyCheck.UseConcurrencyCheck)
            {
                return false;
            }
            else
            {
                return false;
            }
        }

    }
}
