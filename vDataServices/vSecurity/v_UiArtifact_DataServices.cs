using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;

// Gen Timestamp:  5/22/2011 2:25:17 AM

namespace vDataServices
{
    public partial class v_UiArtifact_DataServices
    {

        #region Initialize Variables

        private static string _as = "vDataServices";
        private static string _cn = "v_UiArtifact_DataServices";

        #endregion Initialize Variables

        public static object[] v_UiArtifact_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spv_UiArtifact_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetById", IfxTraceCategory.Leave);
            }
        }

        public static object[] v_UiArtifact_GetById_ObjectArray(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spv_UiArtifact_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetById_ObjectArray", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] v_UiArtifact_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spv_UiArtifact_lst ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] v_UiArtifact_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetListByFK", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spv_UiArtifact_lst ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        public static object[] v_UiArtifact_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid v_UiArtifact_Id;
            Int32 iSuccess = 0;
            bool isConcurrencyGood = true;
            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Save", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                v_UiArtifact_Values _data = new v_UiArtifact_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spv_UiArtifact_put";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
				cmd.Parameters.Add("@v_UiArtifact_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@v_UiArtifact_Application_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Parent_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Ancestor_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Platform_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Type_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Code_Id", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_SortOrder", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Tag", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Base_Visible_Flag", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Base_Enable_Flag", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Anonymous_Visible_Flag", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Anonymous_Enable_Flag", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Authenticated_Visible_Flag", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Authenticated_Enable_Flag", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_IsActiveRec", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_UiArtifact_Stamp", SqlDbType.Timestamp).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters
                #region Add Command Parameter Values
				cmd.Parameters["@v_UiArtifact_Id"].Value = _data.v_UiArtifact_Id;

				cmd.Parameters["@v_UiArtifact_Application_Id"].Value = _data.v_UiArtifact_Application_Id;

				if (null != _data.v_UiArtifact_Parent_Id)
                {
                    cmd.Parameters["@v_UiArtifact_Parent_Id"].Value = _data.v_UiArtifact_Parent_Id;
                }
                else
                {
                    cmd.Parameters["@v_UiArtifact_Parent_Id"].Value = DBNull.Value;
                }

				if (null != _data.v_UiArtifact_Ancestor_Id)
                {
                    cmd.Parameters["@v_UiArtifact_Ancestor_Id"].Value = _data.v_UiArtifact_Ancestor_Id;
                }
                else
                {
                    cmd.Parameters["@v_UiArtifact_Ancestor_Id"].Value = DBNull.Value;
                }

				cmd.Parameters["@v_UiArtifact_Platform_Id"].Value = _data.v_UiArtifact_Platform_Id;

				cmd.Parameters["@v_UiArtifact_Type_Id"].Value = _data.v_UiArtifact_Type_Id;

				if (null != _data.v_UiArtifact_Code_Id)
                {
                    cmd.Parameters["@v_UiArtifact_Code_Id"].Value = _data.v_UiArtifact_Code_Id;
                }
                else
                {
                    cmd.Parameters["@v_UiArtifact_Code_Id"].Value = DBNull.Value;
                }

				if (null != _data.v_UiArtifact_Name)
                {
                    cmd.Parameters["@v_UiArtifact_Name"].Value = _data.v_UiArtifact_Name;
                }
                else
                {
                    cmd.Parameters["@v_UiArtifact_Name"].Value = DBNull.Value;
                }

				if (null != _data.v_UiArtifact_SortOrder)
                {
                    cmd.Parameters["@v_UiArtifact_SortOrder"].Value = _data.v_UiArtifact_SortOrder;
                }
                else
                {
                    cmd.Parameters["@v_UiArtifact_SortOrder"].Value = DBNull.Value;
                }

				if (null != _data.v_UiArtifact_Tag)
                {
                    cmd.Parameters["@v_UiArtifact_Tag"].Value = _data.v_UiArtifact_Tag;
                }
                else
                {
                    cmd.Parameters["@v_UiArtifact_Tag"].Value = DBNull.Value;
                }

				cmd.Parameters["@v_UiArtifact_Base_Visible_Flag"].Value = _data.v_UiArtifact_Base_Visible_Flag;

				cmd.Parameters["@v_UiArtifact_Base_Enable_Flag"].Value = _data.v_UiArtifact_Base_Enable_Flag;

				cmd.Parameters["@v_UiArtifact_Anonymous_Visible_Flag"].Value = _data.v_UiArtifact_Anonymous_Visible_Flag;

				cmd.Parameters["@v_UiArtifact_Anonymous_Enable_Flag"].Value = _data.v_UiArtifact_Anonymous_Enable_Flag;

				cmd.Parameters["@v_UiArtifact_Authenticated_Visible_Flag"].Value = _data.v_UiArtifact_Authenticated_Visible_Flag;

				cmd.Parameters["@v_UiArtifact_Authenticated_Enable_Flag"].Value = _data.v_UiArtifact_Authenticated_Enable_Flag;

				cmd.Parameters["@v_UiArtifact_IsActiveRec"].Value = _data.v_UiArtifact_IsActiveRec;

				if (null != _data.v_UiArtifact_Stamp)
                {
                    cmd.Parameters["@v_UiArtifact_Stamp"].Value = _data.v_UiArtifact_Stamp;
                }
                else
                {
                    cmd.Parameters["@v_UiArtifact_Stamp"].Value = DBNull.Value;
                }


                cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "v_UiArtifact_Save", SQLScript.CreateScript(cmd));
                }
                cmd.ExecuteNonQuery();
                v_UiArtifact_Id = ((Guid)(cmd.Parameters["@v_UiArtifact_Id"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                {
                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                }

                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                //**//  Hack:  we need to get this true or false value from the business object by using a 
                //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
                dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
                //  Set the DataServiceInsertUpdateResponse Result
                if (iSuccess > 0)
                {
                    dataServiceResponse.Result = DataOperationResult.Success;
                    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@v_UiArtifact_Stamp"].Value));
                }
                else if (isConcurrencyGood == false)
                {
                    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
                }
                else
                {
                    dataServiceResponse.Result = DataOperationResult.HandledException;
                }
                if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
                {
                    //  we need a flag to determin if we return new data on a successful insert/update
                    dataServiceResponse.CurrentValues = v_UiArtifact_GetById_ObjectArray(v_UiArtifact_Id);
                }
                // If the default is to not return the current row, then return the Primary Key here.
                if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
                {
                    dataServiceResponse.guidPrimaryKey = v_UiArtifact_Id;
                }

                return dataServiceResponse.ReturnAsObjectArray();
            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)), new ValuePair("sqlScript", sqlScript) }, null, ex);
                IfxWrapperException.GetError(ex);
                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                dataServiceResponse.Result = DataOperationResult.UnhandledException;
                return dataServiceResponse.ReturnAsObjectArray();
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] v_UiArtifact_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] v_UiArtifact_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Remove", IfxTraceCategory.Leave);
            }
        }


		#region Other Data Access Methods

        public static object[] Getv_UiArtifactPlatform_cmb()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPlatform_cmb", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spv_UiArtifactPlatform_cmb";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPlatform_cmb", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPlatform_cmb", IfxTraceCategory.Leave);
            }
        }

        public static object[] Getv_UiArtifactType_cmbAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactType_cmbAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spv_UiArtifactType_cmbAll";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactType_cmbAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactType_cmbAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] Getv_UiArtifact_cmbAncestors_ByAp_Id(Guid Ap_Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifact_cmbAncestors_ByAp_Id", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spv_UiArtifact_cmbAncestors_ByAp_Id";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Ap_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Ap_Id"].Value = Ap_Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifact_cmbAncestors_ByAp_Id", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifact_cmbAncestors_ByAp_Id", IfxTraceCategory.Leave);
            }
        }

		#endregion Other Data Access Methods


        #region Support Methods

//        static object[] ConvertSqlDataReaderToObjectArray(SqlDataReader rdr)
//        {
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Enter);
//                List<object[]> data = new List<object[]>();
//                int index = 0;
//                while (rdr.Read())
//                {
//                    object[] values = new object[rdr.FieldCount];
//                    for (int i = 0; i < rdr.FieldCount; i++)
//                    {
//                        if (rdr[i] == DBNull.Value)
//                        {
//                            values[i] = null;
//                        }
//                        else
//                        {
//                            values[i] = rdr[i];
//                        }
//                    }
//                    data.Add(values);
//                    index++;
//                    //if (index == 2)
//                    //{
//                    //    break;
//                    //}
//                }
//                rdr.Close();
//                return data.ToArray();
//            }
//            catch(Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", ex);
//                return null;
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Leave);
//            }
//        }

        #endregion Support Methods


    }
}


