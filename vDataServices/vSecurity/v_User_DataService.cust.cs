﻿using System;
using System.Data;
using System.Data.SqlClient;
using Ifx;
//using EntityWireType;

namespace vDataServices
{
    public partial class v_User_DataService
    {

        #region Initialize Variables



        #endregion Initialize Variables

        private static bool _returnCurrentRowOnInsertUpdate = false;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }


        public static bool? v_User_GetFlag_UserMustChangePassword(Guid aspnetUserId)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_User_GetFlag_UserMustChangePassword", new ValuePair[] { new ValuePair("aspnetUserId", aspnetUserId) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spv_User_GetFlag_UserMustChangePassword";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@AspnetUser_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@UserMustChangePassword", SqlDbType.Bit).Direction = ParameterDirection.Output;
                cmd.Parameters["@AspnetUser_Id"].Value = aspnetUserId;
                cmd.ExecuteNonQuery();
                return ((bool)(cmd.Parameters["@UserMustChangePassword"].Value));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_User_GetFlag_UserMustChangePassword", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_User_GetFlag_UserMustChangePassword", IfxTraceCategory.Leave);
            }
        }




        public static Int32? v_User_Set_Flag_MustChangePassword_ToFalse(Guid aspnetUserId)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_User_Set_Flag_MustChangePassword_ToFalse", new ValuePair[] { new ValuePair("aspnetUserId", aspnetUserId) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spv_User_Set_Flag_MustChangePassword_ToFalse";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@AspnetUser_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@AspnetUser_Id"].Value = aspnetUserId;
                cmd.ExecuteNonQuery();
                return ((Int32?)(cmd.Parameters["@Success"].Value));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_User_Set_Flag_MustChangePassword_ToFalse", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_User_Set_Flag_MustChangePassword_ToFalse", IfxTraceCategory.Leave);
            }
        }



        public static Int32? v_User_AdminChangedPassword_ResetUserFlag(Guid appId, Guid aspnetUserId)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_User_AdminChangedPassword_ResetUserFlag", new ValuePair[] { new ValuePair("appId", appId), new ValuePair("aspnetUserId", aspnetUserId) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spv_User_AdminChangedPassword_ResetUserFlag";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Application_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@AspnetUser_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@Application_Id"].Value = appId;
                cmd.Parameters["@AspnetUser_Id"].Value = aspnetUserId;
                cmd.ExecuteNonQuery();
                return ((Int32?)(cmd.Parameters["@Success"].Value));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_User_AdminChangedPassword_ResetUserFlag", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_User_AdminChangedPassword_ResetUserFlag", IfxTraceCategory.Leave);
            }
        }





        public static bool? aspnet_Membership_VerifyUserHasQuestionAnswer(Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Membership_VerifyUserHasQuestionAnswer", new ValuePair[] { new ValuePair("userId", userId) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.aspnet_Membership_VerifyUserHasQuestionAnswer";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsQaUpdateRequired", SqlDbType.Bit).Direction = ParameterDirection.Output;
                cmd.Parameters["@UserId"].Value = userId;
                cmd.ExecuteNonQuery();
                return cmd.Parameters["@IsQaUpdateRequired"].Value as bool?;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Membership_VerifyUserHasQuestionAnswer", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Membership_VerifyUserHasQuestionAnswer", IfxTraceCategory.Leave);
            }
        }







    }
}
