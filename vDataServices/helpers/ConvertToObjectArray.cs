﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace vDataServices
{
    public class ConvertToObjectArray
    {



       public static object[] ConvertSqlDataReaderToObjectArray(SqlDataReader rdr)
        {
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Enter);
                List<object[]> data = new List<object[]>();
                int index = 0;
                while (rdr.Read())
                {
                    object[] values = new object[rdr.FieldCount];
                    for (int i = 0; i < rdr.FieldCount; i++)
                    {
                        try
                        {
                            if (rdr[i] == DBNull.Value)
                            {
                                values[i] = null;
                            }
                            else
                            {
                                values[i] = rdr[i];
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine(ex.ToString());

                        }
                    }
                    data.Add(values);
                    index++;
                }
                rdr.Close();
                object[] returnData = data.ToArray();
                if (returnData.Length == 0)
                {
                    return null;
                }
                else
                {
                    return returnData;
                }
                // If data has a length of zero, then the ws will get an exception client side:  "Index was outside the bounds of the array."
                //return data.ToArray();
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Catch);
                return null;
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Leave);
            }
        }





    }
}
