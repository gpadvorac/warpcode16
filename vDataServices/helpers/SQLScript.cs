﻿using System;
using System.Text;
using System.Diagnostics;
using System.Data.SqlClient;

namespace DataServices
{
    public class SQLScript
    {

        public static string CreateScript(SqlCommand cmd)
        {
            try
            {
                if (cmd == null) return "";
                StringBuilder sb = new StringBuilder();
                string sTb = "\t";
                sb.Append("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" + Environment.NewLine);
                sb.Append(cmd.CommandText + " " + Environment.NewLine);
                foreach (SqlParameter parm in cmd.Parameters)
                {
                    try
                    {
                        if (parm.Value == System.DBNull.Value || parm.Value == null)
                        {
                            sb.Append("null,  " + sTb + "-- " + parm.ParameterName + Environment.NewLine);
                        }
                        else if (parm.SqlDbType == System.Data.SqlDbType.Timestamp)
                        {
                            sb.Append("null,  " + sTb + "-- " + parm.ParameterName + Environment.NewLine);
                        }
                        else if (parm.SqlDbType == System.Data.SqlDbType.Bit)
                        {
                            if ((bool)parm.Value == true)
                            {
                                sb.Append("1,  " + sTb + "-- " + parm.ParameterName + Environment.NewLine);
                            }
                            else
                            {
                                sb.Append("0,  " + sTb + "-- " + parm.ParameterName + Environment.NewLine);
                            }
                        }
                        else if (parm.SqlDbType == System.Data.SqlDbType.VarBinary ||
                            parm.SqlDbType == System.Data.SqlDbType.VarChar ||
                            parm.SqlDbType == System.Data.SqlDbType.Xml ||
                            parm.SqlDbType == System.Data.SqlDbType.UniqueIdentifier ||
                            parm.SqlDbType == System.Data.SqlDbType.Time ||
                            parm.SqlDbType == System.Data.SqlDbType.Text ||
                            parm.SqlDbType == System.Data.SqlDbType.SmallDateTime ||
                            parm.SqlDbType == System.Data.SqlDbType.NVarChar ||
                            parm.SqlDbType == System.Data.SqlDbType.NText ||
                            parm.SqlDbType == System.Data.SqlDbType.NChar ||
                            parm.SqlDbType == System.Data.SqlDbType.DateTime2 ||
                            parm.SqlDbType == System.Data.SqlDbType.DateTime ||
                            parm.SqlDbType == System.Data.SqlDbType.Date ||
                            parm.SqlDbType == System.Data.SqlDbType.Char)
                        {
                            sb.Append("'" + parm.Value + "',  " + sTb + "-- " + parm.ParameterName + Environment.NewLine);
                        }
                        else
                        {
                            sb.Append(parm.Value + ",  " + sTb + "-- " + parm.ParameterName + Environment.NewLine);
                        }
                    }
                    catch (Exception exx)
                    {
                        Debugger.Break();
                    }
                }
                sb.Append("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" + Environment.NewLine);
                Console.WriteLine(sb.ToString());
                System.Diagnostics.Debug.WriteLine(sb.ToString());

                return sb.ToString();
            }
            catch (Exception ex)
            {
                Debugger.Break();
                return "";
            }
        }









    }
}
