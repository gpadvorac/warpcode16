using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
//using EntityWireType;
using TypeServices;
using System.Data;
using Ifx;

namespace vDataServices
{
    public partial class v_UiArtifactPermission_DataServices
    {

        #region Initialize Variables



        #endregion Initialize Variables

        private static bool _returnCurrentRowOnInsertUpdate = false;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }


        public static Guid? GetUserByName(string userName)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserByName", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.sp_GetUserByName ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@name"].Value = userName;
                cmd.ExecuteNonQuery();
                Guid? userId = ((Guid?)(cmd.Parameters["@UserId"].Value));
                return userId;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserByName", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserByName", IfxTraceCategory.Leave);
            }
        }


        //public static object[] v_UiArtifactPermission_Delete(object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    SqlConnection conn = null;
        //    Int32 iSuccess = 0;
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_Delete", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
        //        //                //SqlConnectionHelper helper = new SqlConnectionHelper();
        //        //                conn = DBConnectoinHelper.GetDBConnection();
        //        //                conn.Open();
        //        //                SqlCommand cmd = conn.CreateCommand();
        //        //                cmd.CommandText = "dbo.spv_UiArtifactPermission_del ";
        //        //                cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        //                cmd.Parameters.Add("@ID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        //                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
        //        //                cmd.Parameters["@v_UiArtifactPermission_Id"].Value = _data.C._a;
        //        //                cmd.ExecuteNonQuery();
        //        //                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_Delete", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (conn != null)
        //        {
        //            conn.Dispose();
        //        }
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_Delete", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
        //    }
        //}



        public static int v_UiArtifactPermission_Delete(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_Delete", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spv_UiArtifactPermission_del ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = id;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                return iSuccess;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_Delete", ex);
                return 0;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_Delete", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }











    }
}


