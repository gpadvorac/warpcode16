using System;
using System.Data;
using System.Data.SqlClient;
using Ifx;
//using EntityWireType;

namespace vDataServices
{
    public partial class UserProfile_DataServices
    {

        #region Initialize Variables



        #endregion Initialize Variables

        private static bool _returnCurrentRowOnInsertUpdate = false;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }


        public static object[] UserProfile_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetById", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spv_UserProfile_row", new SqlParameter("@UserId", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetById", IfxTraceCategory.Leave);
            }
        }



        public static object[] GetUserProfile_lstByAp_Id(Guid ap_Id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserProfile_lstByAp_Id", new ValuePair[] { new ValuePair("ap_Id", ap_Id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spv_UserProfile_lstByAp_Id", new SqlParameter("@Ap_Id", ap_Id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserProfile_lstByAp_Id", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserProfile_lstByAp_Id", IfxTraceCategory.Leave);
            }
        }



        public static int UserProfile_UpdateUserNamesAndActiveStatus(Guid userId, string userName, string firstName, string middleName, string lastName, bool isActiveRow, string passwordQuestion, string passwordAnswer, bool isApproved, bool isLockedOut)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            int iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_UpdateUserNamesAndActiveStatus", new ValuePair[] { new ValuePair("userId", userId) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spv_UserProfile_updateUserNamesAndActiveStatus ";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_FName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_MName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_LName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_IsActiveRow", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@PasswordQuestion", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@PasswordAnswer", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsApproved", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsLockedOut", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                cmd.Parameters["@UserId"].Value = userId;

                cmd.Parameters["@UserName"].Value = userName;

                if (firstName != null)
                {
                    cmd.Parameters["@Pn_FName"].Value = firstName;
                }
                else
                {
                    cmd.Parameters["@Pn_FName"].Value = DBNull.Value;
                }

                if (middleName != null)
                {
                    cmd.Parameters["@Pn_MName"].Value = middleName;
                }
                else
                {
                    cmd.Parameters["@Pn_MName"].Value = DBNull.Value;
                }

                if (lastName != null)
                {
                    cmd.Parameters["@Pn_LName"].Value = lastName;
                }
                else
                {
                    cmd.Parameters["@Pn_LName"].Value = DBNull.Value;
                }

                if (passwordQuestion != null)
                {
                    cmd.Parameters["@PasswordQuestion"].Value = passwordQuestion;
                }
                else
                {
                    cmd.Parameters["@PasswordQuestion"].Value = DBNull.Value;
                }


                if (passwordAnswer != null)
                {
                    cmd.Parameters["@PasswordAnswer"].Value = passwordAnswer;
                }
                else
                {
                    cmd.Parameters["@PasswordAnswer"].Value = DBNull.Value;
                }

                cmd.Parameters["@Pn_IsActiveRow"].Value = isActiveRow;
                cmd.Parameters["@IsApproved"].Value = isApproved;
                cmd.Parameters["@IsLockedOut"].Value = isLockedOut;












                cmd.ExecuteNonQuery();
                iSuccess = ((int)(cmd.Parameters["@Success"].Value));
                return iSuccess;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_UpdateUserNamesAndActiveStatus", ex);
                return 0;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_UpdateUserNamesAndActiveStatus", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }




        public static int UserProfile_updateQuestionAnswer(Guid userId, string passwordQuestion, string passwordAnswer)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            int iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_updateQuestionAnswer", new ValuePair[] { new ValuePair("userId", userId) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spv_UserProfile_updateQuestionAnswer";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@PasswordQuestion", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@PasswordAnswer", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                cmd.Parameters["@UserId"].Value = userId;

                if (passwordQuestion != null)
                {
                    cmd.Parameters["@PasswordQuestion"].Value = passwordQuestion;
                }
                else
                {
                    cmd.Parameters["@PasswordQuestion"].Value = DBNull.Value;
                }

                if (passwordAnswer != null)
                {
                    cmd.Parameters["@PasswordAnswer"].Value = passwordAnswer;
                }
                else
                {
                    cmd.Parameters["@PasswordAnswer"].Value = DBNull.Value;
                }

                cmd.ExecuteNonQuery();
                iSuccess = ((int)(cmd.Parameters["@Success"].Value));
                return iSuccess;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_updateQuestionAnswer", ex);
                return 0;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_updateQuestionAnswer", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }




        public static int Person_InsertUpdateNameOnly_BySecurityUserId(Guid userId, string userName, string firstName, string lastName)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            int iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Person_InsertUpdateNameOnly_BySecurityUserId", new ValuePair[] { new ValuePair("userId", userId) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spPerson_put_NameOnly_BySecurityUserId ";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_FName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_LName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@UserId"].Value = userId;
                cmd.Parameters["@UserName"].Value = userName;
                cmd.Parameters["@Pn_FName"].Value = firstName;
                cmd.Parameters["@Pn_LName"].Value = lastName;
                cmd.ExecuteNonQuery();
                iSuccess = ((int)(cmd.Parameters["@Success"].Value));
                return iSuccess;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Person_InsertUpdateNameOnly_BySecurityUserId", ex);
                return 0;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Person_InsertUpdateNameOnly_BySecurityUserId", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }








    }
}


