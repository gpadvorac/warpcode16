using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
//using EntityWireType;
using TypeServices;
using System.Data;
using Ifx;
using EntityWireType;

namespace vDataServices
{
    public partial class aspnet_Users_DataServices
    {

        #region Initialize Variables



        #endregion Initialize Variables

        private static bool _returnCurrentRowOnInsertUpdate = false;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }


        public static object[] aspnet_Users_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid UserId;
            Int32 iSuccess = 0;
            bool isConcurrencyGood = true;
            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Save", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                aspnet_Users_Values _data = new aspnet_Users_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.aspnet_Users_put";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
                cmd.Parameters.Add("@ApplicationId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@MobileAlias", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsAnonymous", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_FName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_MName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_LName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Password", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters
                #region Add Command Parameter Values
                cmd.Parameters["@UserId"].Value = _data.UserId;

                cmd.Parameters["@ApplicationId"].Value = _data.ApplicationId;

                cmd.Parameters["@UserName"].Value = _data.UserName;

                cmd.Parameters["@LoweredUserName"].Value = _data.LoweredUserName;

                if (null != _data.MobileAlias)
                {
                    cmd.Parameters["@MobileAlias"].Value = _data.MobileAlias;
                }
                else
                {
                    cmd.Parameters["@MobileAlias"].Value = DBNull.Value;
                }

                cmd.Parameters["@IsAnonymous"].Value = _data.IsAnonymous;

                cmd.Parameters["@LastActivityDate"].Value = _data.LastActivityDate;

                if (null != _data.Pn_Id)
                {
                    cmd.Parameters["@Pn_Id"].Value = _data.Pn_Id;
                }
                else
                {
                    cmd.Parameters["@Pn_Id"].Value = DBNull.Value;
                }

                if (null != _data.Pn_FName)
                {
                    cmd.Parameters["@Pn_FName"].Value = _data.Pn_FName;
                }
                else
                {
                    cmd.Parameters["@Pn_FName"].Value = DBNull.Value;
                }

                if (null != _data.Pn_MName)
                {
                    cmd.Parameters["@Pn_MName"].Value = _data.Pn_MName;
                }
                else
                {
                    cmd.Parameters["@Pn_MName"].Value = DBNull.Value;
                }

                if (null != _data.Pn_LName)
                {
                    cmd.Parameters["@Pn_LName"].Value = _data.Pn_LName;
                }
                else
                {
                    cmd.Parameters["@Pn_LName"].Value = DBNull.Value;
                }

                if (null != _data.Password)
                {
                    cmd.Parameters["@Password"].Value = _data.Password;
                }
                else
                {
                    cmd.Parameters["@Password"].Value = DBNull.Value;
                }

                //if (null != _data.ConfirmPassword)
                //{
                //    cmd.Parameters["@ConfirmPassword"].Value = _data.ConfirmPassword;
                //}
                //else
                //{
                //    cmd.Parameters["@ConfirmPassword"].Value = DBNull.Value;
                //}


                cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "aspnet_Users_Save", SQLScript.CreateScript(cmd));
                }
                cmd.ExecuteNonQuery();
                UserId = ((Guid)(cmd.Parameters["@UserId"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                {
                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                }

                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                //**//  Hack:  we need to get this true or false value from the business object by using a 
                //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
                dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
                //  Set the DataServiceInsertUpdateResponse Result
                if (iSuccess > 0)
                {
                    dataServiceResponse.Result = DataOperationResult.Success;
                    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@"].Value));
                }
                else if (isConcurrencyGood == false)
                {
                    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
                }
                else
                {
                    dataServiceResponse.Result = DataOperationResult.HandledException;
                }
                if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
                {
                    //  we need a flag to determin if we return new data on a successful insert/update
                    dataServiceResponse.CurrentValues = aspnet_Users_GetById_ObjectArray(UserId);
                }
                // If the default is to not return the current row, then return the Primary Key here.
                if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
                {
                    dataServiceResponse.guidPrimaryKey = UserId;
                }

                return dataServiceResponse.ReturnAsObjectArray();
            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)), new ValuePair("sqlScript", sqlScript) }, null, ex);
                IfxWrapperException.GetError(ex);
                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                dataServiceResponse.Result = DataOperationResult.UnhandledException;
                return dataServiceResponse.ReturnAsObjectArray();
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Save", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static int aspnet_Users_RegisterUser(Guid aspnetUserId, Guid ap_Id, string userName, string fName, string lName, bool isActive)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid UserId;
            Int32? iSuccess = 0;
            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_RegisterUser", null, IfxTraceCategory.Enter);
                //aspnet_Users_Values _data = new aspnet_Users_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.aspnet_Users_putRegister";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
                cmd.Parameters.Add("@ApplicationId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_FName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_LName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_IsActiveRow", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters
                #region Add Command Parameter Values
                cmd.Parameters["@UserId"].Value = aspnetUserId;

                cmd.Parameters["@ApplicationId"].Value = ap_Id;

                cmd.Parameters["@UserName"].Value = userName;

                if (null !=fName)
                {
                    cmd.Parameters["@Pn_FName"].Value = fName;
                }
                else
                {
                    cmd.Parameters["@Pn_FName"].Value = DBNull.Value;
                }

                if (null != lName)
                {
                    cmd.Parameters["@Pn_LName"].Value = lName;
                }
                else
                {
                    cmd.Parameters["@Pn_LName"].Value = DBNull.Value;
                }

                cmd.Parameters["@Pn_IsActiveRow"].Value = isActive;

                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "aspnet_Users_RegisterUser", SQLScript.CreateScript(cmd));
                }
                cmd.ExecuteNonQuery();
                UserId = ((Guid)(cmd.Parameters["@UserId"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                {
                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                }

                if (iSuccess == null)
                {
                    return 0;
                }
                else
                {
                    return (Int32)iSuccess;
                }

            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_RegisterUser", new ValuePair[] { new ValuePair("sqlScript", sqlScript) }, null, ex);
                IfxWrapperException.GetError(ex);
                return 0;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_RegisterUser", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }
        





        public static int aspnet_Users_EditUserProfile(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid UserId;
            Int32? iSuccess = 0;
            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_EditUserProfile", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                aspnet_Users_Values _data = new aspnet_Users_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.aspnet_Users_updateUserProfile";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
                //cmd.Parameters.Add("@ApplicationId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_FName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_LName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters
                #region Add Command Parameter Values
                cmd.Parameters["@UserId"].Value = _data.UserId;

                //cmd.Parameters["@ApplicationId"].Value = _data.ApplicationId;

                cmd.Parameters["@UserName"].Value = _data.UserName;

                if (null != _data.Pn_FName)
                {
                    cmd.Parameters["@Pn_FName"].Value = _data.Pn_FName;
                }
                else
                {
                    cmd.Parameters["@Pn_FName"].Value = DBNull.Value;
                }

                if (null != _data.Pn_LName)
                {
                    cmd.Parameters["@Pn_LName"].Value = _data.Pn_LName;
                }
                else
                {
                    cmd.Parameters["@Pn_LName"].Value = DBNull.Value;
                }


                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "aspnet_Users_EditUserProfile", SQLScript.CreateScript(cmd));
                }
                cmd.ExecuteNonQuery();
                UserId = ((Guid)(cmd.Parameters["@UserId"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                {
                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                }

                if (iSuccess == null)
                {
                    return 0;
                }
                else
                {
                    return (Int32)iSuccess;
                }

            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_EditUserProfile", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)), new ValuePair("sqlScript", sqlScript) }, null, ex);
                IfxWrapperException.GetError(ex);
                return 0;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_EditUserProfile", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }


        public static string GetUserEmail(Guid aspNetId)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            string userEmail = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserEmail", new ValuePair[] { new ValuePair("aspNetId", aspNetId) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.aspnet_User_getEmail_ByUserId ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@UserName", SqlDbType.NVarChar, 256).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = aspNetId;
                cmd.ExecuteNonQuery();
                userEmail = ((string)(cmd.Parameters["@UserName"].Value));
                return userEmail;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserEmail", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserEmail", IfxTraceCategory.Leave);
            }
        }


        public static object[] aspnet_Users_getBasicProfile(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetById", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "aspnet_Users_getBasicProfile", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetById", IfxTraceCategory.Leave);
            }
        }


        public static Guid? GetUserIdFromUserName(Guid ap_Id, string userName)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            string userEmail = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserEmail", new ValuePair[] { new ValuePair("ap_Id", ap_Id), new ValuePair("userName", userName) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.aspnet_User_GetIdFromUsernameAndApId";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@ApplicationId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                cmd.Parameters["@ApplicationId"].Value = ap_Id;
                cmd.Parameters["@UserName"].Value = userName;
                cmd.ExecuteNonQuery();
                if (cmd.Parameters["@UserId"].Value == System.DBNull.Value)
                {

                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserIdFromUserName",
                        new ValuePair[] { new ValuePair("ap_Id", ap_Id.ToString()), new ValuePair("userName", userName) }, null, new Exception("Stored Procedure dbo.aspnet_User_GetIdFromUsernameAndApId returned null."));
                    return null;
                }
                else
                {
                    Guid? userId = ((Guid?)(cmd.Parameters["@UserId"].Value));
                    return userId;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserEmail", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserEmail", IfxTraceCategory.Leave);
            }
        }


        public static bool aspnet_Users_VerifyUserExistsWithUserId(Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            bool exists = false;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "spaspnet_User_VerifyUserExistsWithUserId", new ValuePair[] { new ValuePair("userId", userId) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.aspnet_Users_VerifyUserExistsWithUserId ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Exists", SqlDbType.Bit).Direction = ParameterDirection.Output;
                cmd.Parameters["@UserId"].Value = userId;
                cmd.ExecuteNonQuery();
                exists = ((bool)(cmd.Parameters["@Exists"].Value));
                return exists;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_VerifyUserExistsWithUserId", ex);
                return false;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_VerifyUserExistsWithUserId", null, IfxTraceCategory.Leave);
            }
        }



        public static bool aspnet_Users_VerifyUserExistsWithUserLoginName(string userLoginName)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            bool exists = false;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_VerifyUserExistsWithUserLoginName", new ValuePair[] { new ValuePair("userLoginName", userLoginName) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.aspnet_Users_VerifyUserExistsWithUserLoginName ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Exists", SqlDbType.Bit).Direction = ParameterDirection.Output;
                cmd.Parameters["@UserName"].Value = userLoginName;
                cmd.ExecuteNonQuery();
                exists = ((bool)(cmd.Parameters["@Exists"].Value));
                return exists;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_VerifyUserExistsWithUserLoginName", ex);
                return false;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_VerifyUserExistsWithUserLoginName", null, IfxTraceCategory.Leave);
            }
        }



        public static int Person_IsUserActive(Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 isActive = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Person_IsUserActive", new ValuePair[] { new ValuePair("userId", userId) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spPerson_IsUserActive ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Pn_SecurityUserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsActive", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@Pn_SecurityUserId"].Value = userId;
                cmd.ExecuteNonQuery();
                isActive = ((Int32)(cmd.Parameters["@IsActive"].Value));
                return isActive;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Person_IsUserActive", ex);
                return -2;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Person_IsUserActive", new ValuePair[] { new ValuePair("isActive", isActive) }, IfxTraceCategory.Leave);
            }
        }

        public static Guid? GetUser2AlternateId_AltId(Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Guid? altId;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUser2AlternateId_AltId", new ValuePair[] { new ValuePair("userId", userId) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spv_User2AlternateId_GetAltId";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@AltId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@UserId"].Value = userId;
                cmd.ExecuteNonQuery();
                altId = cmd.Parameters["@AltId"].Value as Guid?;

                //if (altId == null)
                //{
                //    IfxEvent.PublishTrace(traceId, _as, _cn, "GetUser2AlternateId_AltId", new Exception("Alternate User Id not found for user " + userId.ToString()));
                //}
                return altId;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUser2AlternateId_AltId", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUser2AlternateId_AltId", null, IfxTraceCategory.Leave);
            }
        }



        public static Guid? GetPersonIdFromUserId(Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Guid? pnId;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUser2AlternateId_AltId", new ValuePair[] { new ValuePair("userId", userId) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spv_User_GetPersonId";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Pn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@UserId"].Value = userId;
                cmd.ExecuteNonQuery();
                pnId = cmd.Parameters["@Pn_Id"].Value as Guid?;

                return pnId;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUser2AlternateId_AltId", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUser2AlternateId_AltId", null, IfxTraceCategory.Leave);
            }
        }


        public static int aspnet_Users_Delete(Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Delete", new ValuePair[] { new ValuePair("userId", userId) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.aspnet_Users_delete ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                cmd.Parameters["@Id"].Value = userId;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                return iSuccess;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Delete", ex);
                return 0;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Delete", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }




    }
}


