using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;

// Gen Timestamp:  4/14/2011 10:09:15 PM

namespace vDataServices
{
    public partial class aspnet_Users_DataServices
    {

        #region Initialize Variables

        private static string _as = "vDataServices";
        private static string _cn = "aspnet_Users_DataServices";

        #endregion Initialize Variables

        public static object[] aspnet_Users_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "aspnet_Users_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetById", IfxTraceCategory.Leave);
            }
        }

        public static object[] aspnet_Users_GetById_ObjectArray(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "aspnet_Users_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetById_ObjectArray", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] aspnet_Users_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.aspnet_Users_lst ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] aspnet_Users_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetListByFK", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.aspnet_Users_lst ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        public static object[] aspnet_Users_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
//                //SqlConnectionHelper helper = new SqlConnectionHelper();
//                conn = DBConnectoinHelper.GetDBConnection();
//                conn.Open();
//                SqlCommand cmd = conn.CreateCommand();
//                cmd.CommandText = "dbo.spaspnet_Users_del ";
//                cmd.CommandType = System.Data.CommandType.StoredProcedure;
//                cmd.Parameters.Add("@ID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
//                cmd.Parameters["@UserId"].Value = _data.C._a;
//                cmd.ExecuteNonQuery();
//                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Delete", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Delete", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] aspnet_Users_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] aspnet_Users_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_Remove", IfxTraceCategory.Leave);
            }
        }


		#region Other Data Access Methods

		#endregion Other Data Access Methods


        #region Support Methods

//        static object[] ConvertSqlDataReaderToObjectArray(SqlDataReader rdr)
//        {
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Enter);
//                List<object[]> data = new List<object[]>();
//                int index = 0;
//                while (rdr.Read())
//                {
//                    object[] values = new object[rdr.FieldCount];
//                    for (int i = 0; i < rdr.FieldCount; i++)
//                    {
//                        if (rdr[i] == DBNull.Value)
//                        {
//                            values[i] = null;
//                        }
//                        else
//                        {
//                            values[i] = rdr[i];
//                        }
//                    }
//                    data.Add(values);
//                    index++;
//                    //if (index == 2)
//                    //{
//                    //    break;
//                    //}
//                }
//                rdr.Close();
//                return data.ToArray();
//            }
//            catch(Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", ex);
//                return null;
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Leave);
//            }
//        }

        #endregion Support Methods


    }
}


