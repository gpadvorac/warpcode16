using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;

// Gen Timestamp:  5/4/2011 10:22:10 PM

namespace vDataServices
{
    public partial class UserProfile_DataServices
    {

        #region Initialize Variables

        private static string _as = "vDataServices";
        private static string _cn = "UserProfile_DataServices";

        #endregion Initialize Variables
 
        public static object[] UserProfile_GetById_ObjectArray(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spUserProfile_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetById_ObjectArray", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] UserProfile_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spUserProfile_lst ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] UserProfile_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetListByFK", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spUserProfile_lst ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        public static object[] UserProfile_Save(object[] data, int check)
        {
            // we are using a combination of aspnet membership calls stored procedure calls to do this from the web service instead.
            return null;
            //Guid? traceId = Guid.NewGuid();
            //SqlConnection conn = null;
            //SqlCommand cmd = null;
            //Guid UserId;
            //Int32 iSuccess = 0;
            //bool isConcurrencyGood = true;
            //Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
            //    UserProfile_Values _data = new UserProfile_Values(data, null);
            //    conn = DBConnectoinHelper.GetDBConnection();
            //    conn.Open();
            //    cmd = conn.CreateCommand();
            //    cmd.CommandText = "dbo.spUserProfile_put";
            //    cmd.CommandType = System.Data.CommandType.StoredProcedure;
            //    #region Add Command Parameters
            //    cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
            //    cmd.Parameters.Add("@ApplicationId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
            //    cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
            //    cmd.Parameters.Add("@LastName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
            //    cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
            //    cmd.Parameters.Add("@Password", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
            //    cmd.Parameters.Add("@PasswordQuestion", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
            //    cmd.Parameters.Add("@PasswordAnswer", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
            //    cmd.Parameters.Add("@IsApproved", SqlDbType.Bit).Direction = ParameterDirection.Input;
            //    cmd.Parameters.Add("@IsLockedOut", SqlDbType.Bit).Direction = ParameterDirection.Input;
            //    cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
            //    cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
            //    cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
            //    cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

            //    #endregion Add Command Parameters
            //    #region Add Command Parameter Values
            //    if (null != _data.UserId)
            //    {
            //        cmd.Parameters["@UserId"].Value = _data.UserId;
            //    }
            //    else
            //    {
            //        cmd.Parameters["@UserId"].Value = DBNull.Value;
            //    }

            //    if (null != _data.ApplicationId)
            //    {
            //        cmd.Parameters["@ApplicationId"].Value = _data.ApplicationId;
            //    }
            //    else
            //    {
            //        cmd.Parameters["@ApplicationId"].Value = DBNull.Value;
            //    }

            //    if (null != _data.FirstName)
            //    {
            //        cmd.Parameters["@FirstName"].Value = _data.FirstName;
            //    }
            //    else
            //    {
            //        cmd.Parameters["@FirstName"].Value = DBNull.Value;
            //    }

            //    if (null != _data.LastName)
            //    {
            //        cmd.Parameters["@LastName"].Value = _data.LastName;
            //    }
            //    else
            //    {
            //        cmd.Parameters["@LastName"].Value = DBNull.Value;
            //    }

            //    cmd.Parameters["@UserName"].Value = _data.UserName;

            //    if (null != _data.Password)
            //    {
            //        cmd.Parameters["@Password"].Value = _data.Password;
            //    }
            //    else
            //    {
            //        cmd.Parameters["@Password"].Value = DBNull.Value;
            //    }

            //    if (null != _data.PasswordQuestion)
            //    {
            //        cmd.Parameters["@PasswordQuestion"].Value = _data.PasswordQuestion;
            //    }
            //    else
            //    {
            //        cmd.Parameters["@PasswordQuestion"].Value = DBNull.Value;
            //    }

            //    if (null != _data.PasswordAnswer)
            //    {
            //        cmd.Parameters["@PasswordAnswer"].Value = _data.PasswordAnswer;
            //    }
            //    else
            //    {
            //        cmd.Parameters["@PasswordAnswer"].Value = DBNull.Value;
            //    }

            //    if (null != _data.IsApproved)
            //    {
            //        cmd.Parameters["@IsApproved"].Value = _data.IsApproved;
            //    }
            //    else
            //    {
            //        cmd.Parameters["@IsApproved"].Value = DBNull.Value;
            //    }

            //    if (null != _data.IsLockedOut)
            //    {
            //        cmd.Parameters["@IsLockedOut"].Value = _data.IsLockedOut;
            //    }
            //    else
            //    {
            //        cmd.Parameters["@IsLockedOut"].Value = DBNull.Value;
            //    }


            //    cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

            //    #endregion Add Command Parameter Values

            //    if (IfxTrace._scriptInsertUpdateOperations)
            //    {
            //        IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "UserProfile_Save", SQLScript.CreateScript(cmd));
            //    }
            //    cmd.ExecuteNonQuery();
            //    UserId = ((Guid)(cmd.Parameters["@UserId"].Value));
            //    iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
            //    isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
            //    if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
            //    {
            //        guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
            //    }

            //    DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
            //    //**//  Hack:  we need to get this true or false value from the business object by using a 
            //    //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
            //    dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
            //    //  Set the DataServiceInsertUpdateResponse Result
            //    if (iSuccess > 0)
            //    {
            //        dataServiceResponse.Result = DataOperationResult.Success;
            //        dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@"].Value));
            //    }
            //    else if (isConcurrencyGood == false)
            //    {
            //        dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
            //    }
            //    else
            //    {
            //        dataServiceResponse.Result = DataOperationResult.HandledException;
            //    }
            //    if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
            //    {
            //        //  we need a flag to determin if we return new data on a successful insert/update
            //        dataServiceResponse.CurrentValues = UserProfile_GetById_ObjectArray(UserId);
            //    }
            //    // If the default is to not return the current row, then return the Primary Key here.
            //    if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
            //    {
            //        dataServiceResponse.guidPrimaryKey = UserId;
            //    }

            //    return dataServiceResponse.ReturnAsObjectArray();
            //}
            //catch (Exception ex)
            //{
            //    string sqlScript = SQLScript.CreateScript(cmd);
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)), new ValuePair("sqlScript", sqlScript) }, null, ex);
            //    IfxWrapperException.GetError(ex);
            //    DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
            //    dataServiceResponse.Result = DataOperationResult.UnhandledException;
            //    return dataServiceResponse.ReturnAsObjectArray();
            //}

            //finally
            //{
            //    if (conn != null)
            //    {
            //        conn.Dispose();
            //    }
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            //}
        }

        public static object[] UserProfile_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] UserProfile_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Remove", IfxTraceCategory.Leave);
            }
        }


		#region Other Data Access Methods

		#endregion Other Data Access Methods


        #region Support Methods

//        static object[] ConvertSqlDataReaderToObjectArray(SqlDataReader rdr)
//        {
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Enter);
//                List<object[]> data = new List<object[]>();
//                int index = 0;
//                while (rdr.Read())
//                {
//                    object[] values = new object[rdr.FieldCount];
//                    for (int i = 0; i < rdr.FieldCount; i++)
//                    {
//                        if (rdr[i] == DBNull.Value)
//                        {
//                            values[i] = null;
//                        }
//                        else
//                        {
//                            values[i] = rdr[i];
//                        }
//                    }
//                    data.Add(values);
//                    index++;
//                    //if (index == 2)
//                    //{
//                    //    break;
//                    //}
//                }
//                rdr.Close();
//                return data.ToArray();
//            }
//            catch(Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", ex);
//                return null;
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Leave);
//            }
//        }

        #endregion Support Methods


    }
}


