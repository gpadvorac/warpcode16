using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;

// Gen Timestamp:  3/25/2011 11:54:37 PM

namespace vDataServices
{
    public partial class Person_DataServices
    {

//        #region Initialize Variables

//        private static string _as = "vDataServices";
//        private static string _cn = "spPerson_DataServices";

//        #endregion Initialize Variables

//        public static object[] spPerson_GetById(Guid id)
//        {
//            Guid? traceId = Guid.NewGuid();
//            SqlConnection conn = null;
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
//                conn = DBConnectoinHelper.GetDBConnection();
//                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spPerson_row", new SqlParameter("@Id", id));
//                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_GetById", ex);
//                return null;
//            }
//            finally
//            {
//                if (conn != null)
//                {
//                    conn.Dispose();
//                }
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_GetById", IfxTraceCategory.Leave);
//            }
//        }

//        public static object[] spPerson_GetById_ObjectArray(Guid id)
//        {
//            Guid? traceId = Guid.NewGuid();
//            SqlConnection conn = null;
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
//                conn = DBConnectoinHelper.GetDBConnection();
//                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spPerson_row", new SqlParameter("@Id", id));
//                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_GetById_ObjectArray", ex);
//                return null;
//            }
//            finally
//            {
//                if (conn != null)
//                {
//                    conn.Dispose();
//                }
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_GetById_ObjectArray", IfxTraceCategory.Leave);
//            }
//        }

//        public static object[] spPerson_GetAll()
//        {
//            Guid? traceId = Guid.NewGuid();
//            SqlConnection conn = null;
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_GetAll", IfxTraceCategory.Enter);
//                conn = DBConnectoinHelper.GetDBConnection();
//                conn.Open();
//                SqlCommand cmd = conn.CreateCommand();
//                cmd.CommandText = "dbo.spPerson_lst ";
//                cmd.CommandType = System.Data.CommandType.StoredProcedure;
//                SqlDataReader rdr = cmd.ExecuteReader();
//                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_GetAll", ex);
//                return null;
//            }
//            finally
//            {
//                if (conn != null)
//                {
//                    conn.Dispose();
//                }
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_GetAll", IfxTraceCategory.Leave);
//            }
//        }

//        public static object[] spPerson_GetListByFK(Guid id)
//        {
//            Guid? traceId = Guid.NewGuid();
//            SqlConnection conn = null;
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
//                conn = DBConnectoinHelper.GetDBConnection();
//                conn.Open();
//                SqlCommand cmd = conn.CreateCommand();
//                cmd.CommandText = "dbo.spPerson_lst ";
//                cmd.CommandType = System.Data.CommandType.StoredProcedure;
//                cmd.Parameters.Add("@Pn_Co_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
//                cmd.Parameters["@Pn_Co_Id"].Value = id;
//                SqlDataReader rdr = cmd.ExecuteReader();
//                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_GetListByFK", ex);
//                return null;
//            }
//            finally
//            {
//                if (conn != null)
//                {
//                    conn.Dispose();
//                }
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_GetListByFK", IfxTraceCategory.Leave);
//            }
//        }

//        public static object[] spPerson_Save(object[] data, int check)
//        {
//            Guid? traceId = Guid.NewGuid();
//            SqlConnection conn = null;
//            SqlCommand cmd = null;
//            Guid Pn_Id;
//            Int32 iSuccess = 0;
//            bool isConcurrencyGood = true;
//            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_Save", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
//                spPerson_Values _data = new spPerson_Values(data, null);
//                conn = DBConnectoinHelper.GetDBConnection();
//                conn.Open();
//                cmd = conn.CreateCommand();
//                cmd.CommandText = "dbo.spPerson_put";
//                cmd.CommandType = System.Data.CommandType.StoredProcedure;
//                #region Add Command Parameters
//                cmd.Parameters.Add("@Pn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
//                cmd.Parameters.Add("@Pn_Co_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Pn_FName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Pn_LName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Pn_PnCs_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Pn_Title", SqlDbType.VarChar).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Pn_BirthDate", SqlDbType.DateTime).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Pn_Uid", SqlDbType.VarChar).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Pn_Pwd", SqlDbType.VarChar).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Pn_Notes", SqlDbType.VarChar).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Pn_IsActiveRow", SqlDbType.Bit).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Pn_Stamp", SqlDbType.Timestamp).Direction = ParameterDirection.InputOutput;
//                cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
//                cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
//                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

//                #endregion Add Command Parameters
//                #region Add Command Parameter Values
//                cmd.Parameters["@Pn_Id"].Value = _data.Pn_Id;

//                cmd.Parameters["@Pn_Co_Id"].Value = _data.Pn_Co_Id;

//                if (null != _data.Pn_FName)
//                {
//                    cmd.Parameters["@Pn_FName"].Value = _data.Pn_FName;
//                }
//                else
//                {
//                    cmd.Parameters["@Pn_FName"].Value = DBNull.Value;
//                }

//                if (null != _data.Pn_LName)
//                {
//                    cmd.Parameters["@Pn_LName"].Value = _data.Pn_LName;
//                }
//                else
//                {
//                    cmd.Parameters["@Pn_LName"].Value = DBNull.Value;
//                }

//                cmd.Parameters["@Pn_PnCs_Id"].Value = _data.Pn_PnCs_Id;

//                if (null != _data.Pn_Title)
//                {
//                    cmd.Parameters["@Pn_Title"].Value = _data.Pn_Title;
//                }
//                else
//                {
//                    cmd.Parameters["@Pn_Title"].Value = DBNull.Value;
//                }

//                if (null != _data.Pn_BirthDate)
//                {
//                    cmd.Parameters["@Pn_BirthDate"].Value = _data.Pn_BirthDate;
//                }
//                else
//                {
//                    cmd.Parameters["@Pn_BirthDate"].Value = DBNull.Value;
//                }

//                if (null != _data.Pn_Uid)
//                {
//                    cmd.Parameters["@Pn_Uid"].Value = _data.Pn_Uid;
//                }
//                else
//                {
//                    cmd.Parameters["@Pn_Uid"].Value = DBNull.Value;
//                }

//                if (null != _data.Pn_Pwd)
//                {
//                    cmd.Parameters["@Pn_Pwd"].Value = _data.Pn_Pwd;
//                }
//                else
//                {
//                    cmd.Parameters["@Pn_Pwd"].Value = DBNull.Value;
//                }

//                if (null != _data.Pn_Notes)
//                {
//                    cmd.Parameters["@Pn_Notes"].Value = _data.Pn_Notes;
//                }
//                else
//                {
//                    cmd.Parameters["@Pn_Notes"].Value = DBNull.Value;
//                }

//                if (null != _data.Pn_IsActiveRow)
//                {
//                    cmd.Parameters["@Pn_IsActiveRow"].Value = _data.Pn_IsActiveRow;
//                }
//                else
//                {
//                    cmd.Parameters["@Pn_IsActiveRow"].Value = DBNull.Value;
//                }

//                if (null != _data.Pn_Stamp)
//                {
//                    cmd.Parameters["@Pn_Stamp"].Value = _data.Pn_Stamp;
//                }
//                else
//                {
//                    cmd.Parameters["@Pn_Stamp"].Value = DBNull.Value;
//                }


//                cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

//                #endregion Add Command Parameter Values

//                if (IfxTrace._scriptInsertUpdateOperations)
//                {
//                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "spPerson_Save", SQLScript.CreateScript(cmd));
//                }
//                cmd.ExecuteNonQuery();
//                Pn_Id = ((Guid)(cmd.Parameters["@Pn_Id"].Value));
//                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
//                isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
//                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
//                {
//                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
//                }

//                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
//                //**//  Hack:  we need to get this true or false value from the business object by using a 
//                //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
//                dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
//                //  Set the DataServiceInsertUpdateResponse Result
//                if (iSuccess > 0)
//                {
//                    dataServiceResponse.Result = DataOperationResult.Success;
//                    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@Pn_Stamp"].Value));
//                }
//                else if (isConcurrencyGood == false)
//                {
//                    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
//                }
//                else
//                {
//                    dataServiceResponse.Result = DataOperationResult.HandledException;
//                }
//                if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
//                {
//                    //  we need a flag to determin if we return new data on a successful insert/update
//                    dataServiceResponse.CurrentValues = spPerson_GetById_ObjectArray(Pn_Id);
//                }
//                // If the default is to not return the current row, then return the Primary Key here.
//                if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
//                {
//                    dataServiceResponse.guidPrimaryKey = Pn_Id;
//                }

//                return dataServiceResponse.ReturnAsObjectArray();
//            }
//            catch (Exception ex)
//            {
//                string sqlScript = SQLScript.CreateScript(cmd);
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)), new ValuePair("sqlScript", sqlScript) }, null, ex);
//                IfxWrapperException.GetError(ex);
//                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
//                dataServiceResponse.Result = DataOperationResult.UnhandledException;
//                return dataServiceResponse.ReturnAsObjectArray();
//            }

//            finally
//            {
//                if (conn != null)
//                {
//                    conn.Dispose();
//                }
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
//            }
//        }

//        public static object[] spPerson_Delete(object[] data)
//        {
//            Guid? traceId = Guid.NewGuid();
//            SqlConnection conn = null;
//            Int32 iSuccess = 0;
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
////                //SqlConnectionHelper helper = new SqlConnectionHelper();
////                conn = DBConnectoinHelper.GetDBConnection();
////                conn.Open();
////                SqlCommand cmd = conn.CreateCommand();
////                cmd.CommandText = "dbo.spPerson_del ";
////                cmd.CommandType = System.Data.CommandType.StoredProcedure;
////                cmd.Parameters.Add("@ID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
////                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
////                cmd.Parameters["@Pn_ID"].Value = _data.C._a;
////                cmd.ExecuteNonQuery();
////                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
//                return null;
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_Delete", ex);
//                return null;
//            }
//            finally
//            {
//                if (conn != null)
//                {
//                    conn.Dispose();
//                }
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_Delete", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
//            }
//        }

//        public static object[] spPerson_Deactivate(object[] data)
//        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

//                // no code yet
//                return null;
//            }
//            catch(Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_Deactivate", ex);
//                return null;
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_Deactivate", IfxTraceCategory.Leave);
//            }
//        }

//        public static object[] spPerson_Remove(object[] data)
//        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

//                // no code yet
//                return null;
//            }
//            catch(Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_Remove", ex);
//                return null;
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "spPerson_Remove", IfxTraceCategory.Leave);
//            }
//        }


//        #region Other Data Access Methods

//        public static object[] GetspPerson_cmbByCompany(Guid Co_Id )
//        {
//            Guid? traceId = Guid.NewGuid();
//            SqlConnection conn = null;
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetspPerson_cmbByCompany", IfxTraceCategory.Enter);
//                conn = DBConnectoinHelper.GetDBConnection();
//                conn.Open();
//                SqlCommand cmd = conn.CreateCommand();
//                cmd.CommandText = "spPerson_cmbByCompany";
//                cmd.CommandType = System.Data.CommandType.StoredProcedure;
//                cmd.Parameters.Add("@Co_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
//                cmd.Parameters["@Co_Id"].Value = Co_Id;
//                SqlDataReader rdr = cmd.ExecuteReader();

//                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetspPerson_cmbByCompany", ex);
//                return null;
//            }
//            finally
//            {
//                if (conn != null)
//                {
//                    conn.Dispose();
//                }
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetspPerson_cmbByCompany", IfxTraceCategory.Leave);
//            }
//        }

//        public static object[] GetspPerson_cmbTitle()
//        {
//            Guid? traceId = Guid.NewGuid();
//            SqlConnection conn = null;
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetspPerson_cmbTitle", IfxTraceCategory.Enter);
//                conn = DBConnectoinHelper.GetDBConnection();
//                conn.Open();
//                SqlCommand cmd = conn.CreateCommand();
//                cmd.CommandText = "spPerson_cmbTitle";
//                cmd.CommandType = System.Data.CommandType.StoredProcedure;
//                SqlDataReader rdr = cmd.ExecuteReader();

//                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetspPerson_cmbTitle", ex);
//                return null;
//            }
//            finally
//            {
//                if (conn != null)
//                {
//                    conn.Dispose();
//                }
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetspPerson_cmbTitle", IfxTraceCategory.Leave);
//            }
//        }

//        public static object[] GetPersonClass_LU_cmb()
//        {
//            Guid? traceId = Guid.NewGuid();
//            SqlConnection conn = null;
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPersonClass_LU_cmb", IfxTraceCategory.Enter);
//                conn = DBConnectoinHelper.GetDBConnection();
//                conn.Open();
//                SqlCommand cmd = conn.CreateCommand();
//                cmd.CommandText = "spPersonClass_LU_cmb";
//                cmd.CommandType = System.Data.CommandType.StoredProcedure;
//                SqlDataReader rdr = cmd.ExecuteReader();

//                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPersonClass_LU_cmb", ex);
//                return null;
//            }
//            finally
//            {
//                if (conn != null)
//                {
//                    conn.Dispose();
//                }
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPersonClass_LU_cmb", IfxTraceCategory.Leave);
//            }
//        }

//        public static object[] GetspPerson_cmb()
//        {
//            Guid? traceId = Guid.NewGuid();
//            SqlConnection conn = null;
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetspPerson_cmb", IfxTraceCategory.Enter);
//                conn = DBConnectoinHelper.GetDBConnection();
//                conn.Open();
//                SqlCommand cmd = conn.CreateCommand();
//                cmd.CommandText = "spPerson_cmb";
//                cmd.CommandType = System.Data.CommandType.StoredProcedure;
//                SqlDataReader rdr = cmd.ExecuteReader();

//                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetspPerson_cmb", ex);
//                return null;
//            }
//            finally
//            {
//                if (conn != null)
//                {
//                    conn.Dispose();
//                }
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetspPerson_cmb", IfxTraceCategory.Leave);
//            }
//        }

//        #endregion Other Data Access Methods


//        #region Support Methods

////        static object[] ConvertSqlDataReaderToObjectArray(SqlDataReader rdr)
////        {
////            try
////            {
////                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Enter);
////                List<object[]> data = new List<object[]>();
////                int index = 0;
////                while (rdr.Read())
////                {
////                    object[] values = new object[rdr.FieldCount];
////                    for (int i = 0; i < rdr.FieldCount; i++)
////                    {
////                        if (rdr[i] == DBNull.Value)
////                        {
////                            values[i] = null;
////                        }
////                        else
////                        {
////                            values[i] = rdr[i];
////                        }
////                    }
////                    data.Add(values);
////                    index++;
////                    //if (index == 2)
////                    //{
////                    //    break;
////                    //}
////                }
////                rdr.Close();
////                return data.ToArray();
////            }
////            catch(Exception ex)
////            {
////                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", ex);
////                return null;
////            }
////            finally
////            {
////                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Leave);
////            }
////        }

//        #endregion Support Methods


    }
}


