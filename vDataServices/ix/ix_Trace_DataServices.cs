using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;

// Gen Timestamp:  1/12/2011 8:51:14 PM

namespace vDataServices
{
    public partial class ix_Trace_DataServices
    {

        #region Initialize Variables

        private static string _as = "vDataServices";
        private static string _cn = "ix_Trace_DataServices";

        #endregion Initialize Variables

        public static object[] ix_Trace_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_GetById", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spix_Trace_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_GetById", IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_Trace_GetById_ObjectArray(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_GetById_ObjectArray", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spix_Trace_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_GetById_ObjectArray", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_Trace_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_GetAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spix_Trace_lst ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_GetAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_Trace_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_GetListByFK", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spix_Trace_lst ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Trace_Application_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters["@Trace_Application_Id"].Value = id;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        //public static object[] ix_Trace_Save(object[] data, int check)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    SqlConnection conn = null;
        //    SqlCommand cmd = null;
        //    Guid Trace_Id;
        //    Int32 iSuccess = 0;
        //    bool isConcurrencyGood = true;
        //    Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_Save", new ValuePair[] { new ValuePair("data.ToString()", data.ToString()) }, IfxTraceCategory.Enter);
        //        ix_Trace_Values _data = new ix_Trace_Values(data, null);
        //        conn = DBConnectoinHelper.GetDBConnection();
        //        conn.Open();
        //        cmd = conn.CreateCommand();
        //        cmd.CommandText = "dbo.spix_Trace_put";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;

        //        #region Add Command Parameters
        //        cmd.Parameters.Add("@Trace_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
        //        cmd.Parameters.Add("@Trace_EnterId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Application_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Information", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Assembly_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Type_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Method_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Category_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Principal_User", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Windows_User", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Domain_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Os_Platform", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Os_ServicePack", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Os_Version", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Machine_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Ip", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Thread_Id", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Ticks", SqlDbType.Int).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Working_Set", SqlDbType.BigInt).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Process_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Event_Stamp", SqlDbType.DateTime).Direction = ParameterDirection.InputOutput;
        //        //cmd.Parameters.Add("@Trace_Create_Stamp", SqlDbType.DateTime).Direction = ParameterDirection.InputOutput;
        //        //cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
        //        //cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

        //        #endregion Add Command Parameters

        //        #region Add Command Parameter Values
        //        cmd.Parameters["@Trace_Id"].Value = _data._a;

        //        if (null != _data._b)
        //        {
        //            cmd.Parameters["@Trace_EnterId"].Value = _data._b;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_EnterId"].Value = DBNull.Value;
        //        }

        //        if (null != _data._c)
        //        {
        //            cmd.Parameters["@Trace_Application_Id"].Value = _data._c;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Application_Id"].Value = DBNull.Value;
        //        }

        //        if (null != _data._d)
        //        {
        //            cmd.Parameters["@Trace_Information"].Value = _data._d;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Information"].Value = DBNull.Value;
        //        }

        //        if (null != _data._e)
        //        {
        //            cmd.Parameters["@Trace_Assembly_Name"].Value = _data._e;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Assembly_Name"].Value = DBNull.Value;
        //        }

        //        if (null != _data._f)
        //        {
        //            cmd.Parameters["@Trace_Type_Name"].Value = _data._f;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Type_Name"].Value = DBNull.Value;
        //        }

        //        if (null != _data._g)
        //        {
        //            cmd.Parameters["@Trace_Method_Name"].Value = _data._g;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Method_Name"].Value = DBNull.Value;
        //        }

        //        if (null != _data._h)
        //        {
        //            cmd.Parameters["@Trace_Category_Id"].Value = _data._h;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Category_Id"].Value = DBNull.Value;
        //        }

        //        if (null != _data._i)
        //        {
        //            cmd.Parameters["@Trace_Principal_User"].Value = _data._i;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Principal_User"].Value = DBNull.Value;
        //        }

        //        if (null != _data._j)
        //        {
        //            cmd.Parameters["@Trace_Windows_User"].Value = _data._j;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Windows_User"].Value = DBNull.Value;
        //        }

        //        if (null != _data._k)
        //        {
        //            cmd.Parameters["@Trace_Domain_Name"].Value = _data._k;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Domain_Name"].Value = DBNull.Value;
        //        }

        //        if (null != _data._l)
        //        {
        //            cmd.Parameters["@Trace_Os_Platform"].Value = _data._l;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Os_Platform"].Value = DBNull.Value;
        //        }

        //        if (null != _data._m)
        //        {
        //            cmd.Parameters["@Trace_Os_ServicePack"].Value = _data._m;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Os_ServicePack"].Value = DBNull.Value;
        //        }

        //        if (null != _data._n)
        //        {
        //            cmd.Parameters["@Trace_Os_Version"].Value = _data._n;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Os_Version"].Value = DBNull.Value;
        //        }

        //        if (null != _data._o)
        //        {
        //            cmd.Parameters["@Trace_Machine_Name"].Value = _data._o;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Machine_Name"].Value = DBNull.Value;
        //        }

        //        if (null != _data._p)
        //        {
        //            cmd.Parameters["@Trace_Ip"].Value = _data._p;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Ip"].Value = DBNull.Value;
        //        }

        //        if (null != _data._q)
        //        {
        //            cmd.Parameters["@Trace_Thread_Id"].Value = _data._q;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Thread_Id"].Value = DBNull.Value;
        //        }

        //        if (null != _data._r)
        //        {
        //            cmd.Parameters["@Trace_Ticks"].Value = _data._r;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Ticks"].Value = DBNull.Value;
        //        }

        //        if (null != _data._s)
        //        {
        //            cmd.Parameters["@Trace_Working_Set"].Value = _data._s;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Working_Set"].Value = DBNull.Value;
        //        }

        //        if (null != _data._t)
        //        {
        //            cmd.Parameters["@Trace_Process_Id"].Value = _data._t;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Process_Id"].Value = DBNull.Value;
        //        }

        //        if (null != _data._u)
        //        {
        //            cmd.Parameters["@Trace_Event_Stamp"].Value = _data._u;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Event_Stamp"].Value = DBNull.Value;
        //        }

        //        //if (null != _data._v)
        //        //{
        //        //    cmd.Parameters["@Trace_Create_Stamp"].Value = _data._v;
        //        //}
        //        //else
        //        //{
        //        //    cmd.Parameters["@Trace_Create_Stamp"].Value = DBNull.Value;
        //        //}


        //        //cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

        //        #endregion Add Command Parameter Values

        //        if (IfxTrace._scriptInsertUpdateOperations)
        //        {
        //            IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "ix_Trace_Save", SQLScript.CreateScript(cmd));
        //        }
        //        cmd.ExecuteNonQuery();
        //        Trace_Id = ((Guid)(cmd.Parameters["@Trace_Id"].Value));
        //        iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
        //        isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
        //        if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
        //        {
        //            guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
        //        }

        //        DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
        //        //**//  Hack:  we need to get this true or false value from the business object by using a 
        //        //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
        //        dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
        //        //  Set the DataServiceInsertUpdateResponse Result
        //        if (iSuccess > 0)
        //        {
        //            dataServiceResponse.Result = DataOperationResult.Success;
        //            dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@Trace_Create_Stamp"].Value));
        //        }
        //        else if (isConcurrencyGood == false)
        //        {
        //            dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
        //        }
        //        else
        //        {
        //            dataServiceResponse.Result = DataOperationResult.HandledException;
        //        }
        //        if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
        //        {
        //            //  we need a flag to determin if we return new data on a successful insert/update
        //            dataServiceResponse.CurrentValues = ix_Trace_GetById_ObjectArray(Trace_Id);
        //        }
        //        // If the default is to not return the current row, then return the Primary Key here.
        //        if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
        //        {
        //            dataServiceResponse.guidPrimaryKey = Trace_Id;
        //        }

        //        return dataServiceResponse.ReturnAsObjectArray();
        //    }
        //    catch (Exception ex)
        //    {
        //        string sqlScript = SQLScript.CreateScript(cmd);
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data.ToString()", data.ToString()), new ValuePair("sqlScript", sqlScript) }, null, ex);
        //        IfxWrapperException.GetError(ex);
        //        DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
        //        dataServiceResponse.Result = DataOperationResult.UnhandledException;
        //        return dataServiceResponse.ReturnAsObjectArray();
        //    }

        //    finally
        //    {
        //        if (conn != null)
        //        {
        //            conn.Dispose();
        //        }
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_Save", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
        //    }
        //}

        public static object[] ix_Trace_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_Delete", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                //                //SqlConnectionHelper helper = new SqlConnectionHelper();
                //                conn = DBConnectoinHelper.GetDBConnection();
                //                conn.Open();
                //                SqlCommand cmd = conn.CreateCommand();
                //                cmd.CommandText = "dbo.spix_Trace_del ";
                //                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //                cmd.Parameters.Add("@ID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                //                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                //                cmd.Parameters["@Trace_Id"].Value = _data.C._a;
                //                cmd.ExecuteNonQuery();
                //                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_Delete", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_Delete", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_Trace_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_Deactivate", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_Trace_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_Remove", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_Trace_Remove", IfxTraceCategory.Leave);
            }
        }


        #region Other Data Access Methods

        #endregion Other Data Access Methods


        #region Support Methods

        //        static object[] ConvertSqlDataReaderToObjectArray(SqlDataReader rdr)
        //        {
        //            try
        //            {
        //                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Enter);
        //                List<object[]> data = new List<object[]>();
        //                int index = 0;
        //                while (rdr.Read())
        //                {
        //                    object[] values = new object[rdr.FieldCount];
        //                    for (int i = 0; i < rdr.FieldCount; i++)
        //                    {
        //                        if (rdr[i] == DBNull.Value)
        //                        {
        //                            values[i] = null;
        //                        }
        //                        else
        //                        {
        //                            values[i] = rdr[i];
        //                        }
        //                    }
        //                    data.Add(values);
        //                    index++;
        //                    //if (index == 2)
        //                    //{
        //                    //    break;
        //                    //}
        //                }
        //                rdr.Close();
        //                return data.ToArray();
        //            }
        //            catch(Exception ex)
        //            {
        //                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", ex);
        //                return null;
        //            }
        //            finally
        //            {
        //                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Leave);
        //            }
        //        }

        #endregion Support Methods


    }
}


