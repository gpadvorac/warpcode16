using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
//using EntityWireType;
using TypeServices;
using System.Data;
using EntityWireType;
using Ifx;

namespace vDataServices
{
    public partial class ix_Trace_DataServices
    {

        #region Initialize Variables



        #endregion Initialize Variables

        private static bool _returnCurrentRowOnInsertUpdate = false;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }

        //public static int ix_Trace_SaveList(object[] data)
        //{
        //    SqlConnection conn = null;
        //    SqlCommand cmd = null;
        //    //Guid Trace_Id;
        //    Int32 iSuccess = 0;
        //    try
        //    {
        //        //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "ix_Trace_SaveList", new ValuePair[] {new ValuePair("data.ToString()", data.ToString()) }, IfxTraceCategory.Enter);
          
        //        conn = DBConnectoinHelper.GetDBConnection();
        //        conn.Open();
        //        cmd = conn.CreateCommand();
        //        cmd.CommandText = "dbo.spix_Trace_putInsert";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;

        //        #region Add Command Parameters
        //        cmd.Parameters.Add("@Trace_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
        //        cmd.Parameters.Add("@Trace_EnterId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Application_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Session_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_CreatedOrder", SqlDbType.Int).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Information", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Assembly_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Type_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Method_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Category_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Principal_User", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Windows_User", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Domain_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Os_Platform", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Os_ServicePack", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Os_Version", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Machine_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Ip", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Thread_Id", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Ticks", SqlDbType.Int).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Working_Set", SqlDbType.BigInt).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Process_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Event_Stamp", SqlDbType.DateTime).Direction = ParameterDirection.InputOutput;
        //        //cmd.Parameters.Add("@Trace_Create_Stamp", SqlDbType.DateTime).Direction = ParameterDirection.InputOutput;
        //        //cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
        //        //cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

        //        #endregion Add Command Parameters

        //        if (data != null)
        //        {

        //            for (int iCnt = 0; iCnt <= data.GetUpperBound(0); iCnt++)
        //            {
        //                ix_Trace_Values _data = new ix_Trace_Values((object[])data[iCnt]);

        //                #region Add Command Parameter Values
        //                cmd.Parameters["@Trace_Id"].Value = _data._a;

        //                if (null != _data._b)
        //                {
        //                    cmd.Parameters["@Trace_EnterId"].Value = _data._b;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_EnterId"].Value = DBNull.Value;
        //                }

        //                if (null != _data._c)
        //                {
        //                    cmd.Parameters["@Trace_Application_Id"].Value = _data._c;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Application_Id"].Value = DBNull.Value;
        //                }

        //                if (null != _data._d)
        //                {
        //                    cmd.Parameters["@Trace_Session_Id"].Value = _data._d;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Session_Id"].Value = DBNull.Value;
        //                }

        //                cmd.Parameters["@Trace_CreatedOrder"].Value = _data._e;

        //                if (null != _data._f)
        //                {
        //                    cmd.Parameters["@Trace_Information"].Value = _data._f;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Information"].Value = DBNull.Value;
        //                }

        //                if (null != _data._g)
        //                {
        //                    cmd.Parameters["@Trace_Assembly_Name"].Value = _data._g;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Assembly_Name"].Value = DBNull.Value;
        //                }

        //                if (null != _data._h)
        //                {
        //                    cmd.Parameters["@Trace_Type_Name"].Value = _data._h;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Type_Name"].Value = DBNull.Value;
        //                }

        //                if (null != _data._i)
        //                {
        //                    cmd.Parameters["@Trace_Method_Name"].Value = _data._i;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Method_Name"].Value = DBNull.Value;
        //                }

        //                if (null != _data._j)
        //                {
        //                    cmd.Parameters["@Trace_Category_Id"].Value = _data._j;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Category_Id"].Value = DBNull.Value;
        //                }

        //                if (null != _data._k)
        //                {
        //                    cmd.Parameters["@Trace_Principal_User"].Value = _data._k;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Principal_User"].Value = DBNull.Value;
        //                }

        //                if (null != _data._l)
        //                {
        //                    cmd.Parameters["@Trace_Windows_User"].Value = _data._l;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Windows_User"].Value = DBNull.Value;
        //                }

        //                if (null != _data._m)
        //                {
        //                    cmd.Parameters["@Trace_Domain_Name"].Value = _data._m;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Domain_Name"].Value = DBNull.Value;
        //                }

        //                if (null != _data._n)
        //                {
        //                    cmd.Parameters["@Trace_Os_Platform"].Value = _data._n;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Os_Platform"].Value = DBNull.Value;
        //                }

        //                if (null != _data._o)
        //                {
        //                    cmd.Parameters["@Trace_Os_ServicePack"].Value = _data._o;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Os_ServicePack"].Value = DBNull.Value;
        //                }

        //                if (null != _data._p)
        //                {
        //                    cmd.Parameters["@Trace_Os_Version"].Value = _data._p;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Os_Version"].Value = DBNull.Value;
        //                }

        //                if (null != _data._q)
        //                {
        //                    cmd.Parameters["@Trace_Machine_Name"].Value = _data._q;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Machine_Name"].Value = DBNull.Value;
        //                }

        //                if (null != _data._r)
        //                {
        //                    cmd.Parameters["@Trace_Ip"].Value = _data._r;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Ip"].Value = DBNull.Value;
        //                }

        //                if (null != _data._s)
        //                {
        //                    cmd.Parameters["@Trace_Thread_Id"].Value = _data._s;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Thread_Id"].Value = DBNull.Value;
        //                }

        //                if (null != _data._t)
        //                {
        //                    cmd.Parameters["@Trace_Ticks"].Value = _data._t;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Ticks"].Value = DBNull.Value;
        //                }

        //                if (null != _data._u)
        //                {
        //                    cmd.Parameters["@Trace_Working_Set"].Value = _data._u;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Working_Set"].Value = DBNull.Value;
        //                }

        //                if (null != _data._v)
        //                {
        //                    cmd.Parameters["@Trace_Process_Id"].Value = _data._v;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Process_Id"].Value = DBNull.Value;
        //                }

        //                if (null != _data._w)
        //                {
        //                    cmd.Parameters["@Trace_Event_Stamp"].Value = _data._w;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Event_Stamp"].Value = DBNull.Value;
        //                }

        //                if (null != _data._x)
        //                {
        //                    cmd.Parameters["@Trace_Create_Stamp"].Value = _data._x;
        //                }
        //                else
        //                {
        //                    cmd.Parameters["@Trace_Create_Stamp"].Value = DBNull.Value;
        //                }



        //                #endregion Add Command Parameter Values


        //                if (IfxTrace._scriptInsertUpdateOperations)
        //                {
        //                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "ix_Trace_SaveList", SQLScript.CreateScript(cmd));
        //                }

        //                cmd.ExecuteNonQuery();
        //                //Trace_Id = ((Guid)(cmd.Parameters["@Trace_Id"].Value));
        //                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
        //                if (iSuccess != 1)
        //                {
        //                    throw new Exception("Insert failed");
        //                }
        //                else
        //                {
        //                    if (data[24] != null)
        //                    {
        //                        ix_TraceValuePair_DataServices.ix_TraceValuePair_SaveList((object[])data[24]);
        //                    }
        //                    if (data[25] != null)
        //                    {
        //                        ix_TraceValuePair_DataServices.ix_TraceValuePair_SaveList((object[])data[25]);
        //                    }
        //                }
        //            }
        //        }
        //        return iSuccess;
        //    }
        //    catch (Exception ex)
        //    {
        //        string sqlScript = SQLScript.CreateScript(cmd);
        //        if (IfxTrace._traceCatch) IfxEvent.PublishIfxExceptionTraceToXmlFile(_as, _cn, "ix_Trace_SaveList", null, new ValuePair[] { new ValuePair("data.ToString()", data.ToString()), new ValuePair("sqlScript", sqlScript) }, null, ex);
        //        return iSuccess;
        //    }

        //    finally
        //    {
        //        if (conn != null)
        //        {
        //            conn.Dispose();
        //        }
        //        //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "ix_Trace_SaveList", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
        //    }
        //}


        //public static int ix_Trace_Save(object[] data)
        //{
        //    SqlConnection conn = null;
        //    SqlCommand cmd = null;
        //    Guid Trace_Id;
        //    Int32 iSuccess = 0;
        //    try
        //    {
        //        //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "ix_Trace_Save", new ValuePair[] {new ValuePair("data.ToString()", data.ToString()) }, IfxTraceCategory.Enter);
        //        ix_Trace_Values _data = new ix_Trace_Values(data);
        //        conn = DBConnectoinHelper.GetDBConnection();
        //        conn.Open();
        //        cmd = conn.CreateCommand();
        //        cmd.CommandText = "dbo.spix_Trace_put";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;

        //        #region Add Command Parameters
        //        cmd.Parameters.Add("@Trace_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
        //        cmd.Parameters.Add("@Trace_EnterId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Application_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Session_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_CreatedOrder", SqlDbType.Int).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Information", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Assembly_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Type_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Method_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Category_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Principal_User", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Windows_User", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Domain_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Os_Platform", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Os_ServicePack", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Os_Version", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Machine_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Ip", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Thread_Id", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Ticks", SqlDbType.Int).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Working_Set", SqlDbType.BigInt).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Process_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Trace_Event_Stamp", SqlDbType.DateTime).Direction = ParameterDirection.InputOutput;
        //        //cmd.Parameters.Add("@Trace_Create_Stamp", SqlDbType.DateTime).Direction = ParameterDirection.InputOutput;
        //        //cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
        //        //cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

        //        #endregion Add Command Parameters

        //        #region Add Command Parameter Values
        //        cmd.Parameters["@Trace_Id"].Value = _data._a;

        //        if (null != _data._b)
        //        {
        //            cmd.Parameters["@Trace_EnterId"].Value = _data._b;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_EnterId"].Value = DBNull.Value;
        //        }

        //        if (null != _data._c)
        //        {
        //            cmd.Parameters["@Trace_Application_Id"].Value = _data._c;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Application_Id"].Value = DBNull.Value;
        //        }

        //        if (null != _data._d)
        //        {
        //            cmd.Parameters["@Trace_Session_Id"].Value = _data._d;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Session_Id"].Value = DBNull.Value;
        //        }

        //        cmd.Parameters["@Trace_CreatedOrder"].Value = _data._e;

        //        if (null != _data._f)
        //        {
        //            cmd.Parameters["@Trace_Information"].Value = _data._f;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Information"].Value = DBNull.Value;
        //        }

        //        if (null != _data._g)
        //        {
        //            cmd.Parameters["@Trace_Assembly_Name"].Value = _data._g;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Assembly_Name"].Value = DBNull.Value;
        //        }

        //        if (null != _data._h)
        //        {
        //            cmd.Parameters["@Trace_Type_Name"].Value = _data._h;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Type_Name"].Value = DBNull.Value;
        //        }

        //        if (null != _data._i)
        //        {
        //            cmd.Parameters["@Trace_Method_Name"].Value = _data._i;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Method_Name"].Value = DBNull.Value;
        //        }

        //        if (null != _data._j)
        //        {
        //            cmd.Parameters["@Trace_Category_Id"].Value = _data._j;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Category_Id"].Value = DBNull.Value;
        //        }

        //        if (null != _data._k)
        //        {
        //            cmd.Parameters["@Trace_Principal_User"].Value = _data._k;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Principal_User"].Value = DBNull.Value;
        //        }

        //        if (null != _data._l)
        //        {
        //            cmd.Parameters["@Trace_Windows_User"].Value = _data._l;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Windows_User"].Value = DBNull.Value;
        //        }

        //        if (null != _data._m)
        //        {
        //            cmd.Parameters["@Trace_Domain_Name"].Value = _data._m;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Domain_Name"].Value = DBNull.Value;
        //        }

        //        if (null != _data._n)
        //        {
        //            cmd.Parameters["@Trace_Os_Platform"].Value = _data._n;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Os_Platform"].Value = DBNull.Value;
        //        }

        //        if (null != _data._o)
        //        {
        //            cmd.Parameters["@Trace_Os_ServicePack"].Value = _data._o;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Os_ServicePack"].Value = DBNull.Value;
        //        }

        //        if (null != _data._p)
        //        {
        //            cmd.Parameters["@Trace_Os_Version"].Value = _data._p;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Os_Version"].Value = DBNull.Value;
        //        }

        //        if (null != _data._q)
        //        {
        //            cmd.Parameters["@Trace_Machine_Name"].Value = _data._q;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Machine_Name"].Value = DBNull.Value;
        //        }

        //        if (null != _data._r)
        //        {
        //            cmd.Parameters["@Trace_Ip"].Value = _data._r;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Ip"].Value = DBNull.Value;
        //        }

        //        if (null != _data._s)
        //        {
        //            cmd.Parameters["@Trace_Thread_Id"].Value = _data._s;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Thread_Id"].Value = DBNull.Value;
        //        }

        //        if (null != _data._t)
        //        {
        //            cmd.Parameters["@Trace_Ticks"].Value = _data._t;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Ticks"].Value = DBNull.Value;
        //        }

        //        if (null != _data._u)
        //        {
        //            cmd.Parameters["@Trace_Working_Set"].Value = _data._u;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Working_Set"].Value = DBNull.Value;
        //        }

        //        if (null != _data._v)
        //        {
        //            cmd.Parameters["@Trace_Process_Id"].Value = _data._v;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Process_Id"].Value = DBNull.Value;
        //        }

        //        if (null != _data._w)
        //        {
        //            cmd.Parameters["@Trace_Event_Stamp"].Value = _data._w;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Trace_Event_Stamp"].Value = DBNull.Value;
        //        }

        //        //if (null != _data._x)
        //        //{
        //        //    cmd.Parameters["@Trace_Create_Stamp"].Value = _data._x;
        //        //}
        //        //else
        //        //{
        //        //    cmd.Parameters["@Trace_Create_Stamp"].Value = DBNull.Value;
        //        //}



        //        #endregion Add Command Parameter Values

        //        if (IfxTrace._scriptInsertUpdateOperations)
        //        {
        //            IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "ix_Trace_Save", SQLScript.CreateScript(cmd));
        //        }

        //        cmd.ExecuteNonQuery();
        //        //Trace_Id = ((Guid)(cmd.Parameters["@Trace_Id"].Value));
        //        iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
        //        if (iSuccess != 1)
        //        {
        //            throw new Exception("Insert failed");
        //        }
        //        else
        //        {
        //            if (data[24] != null)
        //            {
        //                ix_TraceValuePair_DataServices.ix_TraceValuePair_SaveList((object[])data[24]);
        //            }
        //            if (data[25] != null)
        //            {
        //                ix_TraceValuePair_DataServices.ix_TraceValuePair_SaveList((object[])data[25]);
        //            }
        //        }
        //        return iSuccess;
        //    }
        //    catch (Exception ex)
        //    {
        //        string sqlScript = SQLScript.CreateScript(cmd);
        //        if (IfxTrace._traceCatch) IfxEvent.PublishIfxExceptionTraceToXmlFile(_as, _cn, "ix_Trace_Save", null, new ValuePair[] { new ValuePair("data.ToString()", data.ToString()), new ValuePair("sqlScript", sqlScript) }, null, ex);
        //        return iSuccess;
        //    }

        //    finally
        //    {
        //        if (conn != null)
        //        {
        //            conn.Dispose();
        //        }
        //        //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "ix_Trace_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
        //    }
        //}



        public static object[] GetTrace_lstBy_SessionId(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTrace_lstBy_SessionId", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spix_Trace_lstBy_SessionId ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Session_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters["@Session_Id"].Value = id;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTrace_lstBy_SessionId", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTrace_lstBy_SessionId", IfxTraceCategory.Leave);
            }
        }




        public static object[] GetTrace_lstBy_SessionId_ReadOnlyGrid(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTrace_lstBy_SessionId_ReadOnlyGrid", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spix_Trace_lstBy_SessionId_ReadOnlyGrid ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Session_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters["@Session_Id"].Value = id;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTrace_lstBy_SessionId_ReadOnlyGrid", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTrace_lstBy_SessionId_ReadOnlyGrid", IfxTraceCategory.Leave);
            }
        }



    }
}


