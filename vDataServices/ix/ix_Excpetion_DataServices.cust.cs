using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
//using EntityWireType;
using TypeServices;
using System.Data;
using Ifx;
using EntityWireType;

namespace vDataServices
{
    public partial class ix_Excpetion_DataServices
    {

        #region Initialize Variables



        #endregion Initialize Variables

        private static bool _returnCurrentRowOnInsertUpdate = false;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }




        //public static int ix_Excpetion_Save(object[] data)
        //{
        //    SqlConnection conn = null;
        //    SqlCommand cmd = null;
        //    Guid Excp_Id;
        //    Int32 iSuccess = 0;
        //    //bool isConcurrencyGood = true;
        //    //Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
        //    //Excpetion_ValuesMngr newData=null;
        //    try
        //    {
        //        //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "Excpetion_Save", new ValuePair[] {new ValuePair("data.ToString()", data.ToString()) }, IfxTraceCategory.Enter);
        //        Excpetion_Values _data = new Excpetion_Values(data);
        //        conn = DBConnectoinHelper.GetDBConnection();
        //        conn.Open();
        //        cmd = conn.CreateCommand();
        //        cmd.CommandText = "dbo.spix_Excpetion_putInsert";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        #region Add Command Parameters
        //        cmd.Parameters.Add("@Excp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
        //        cmd.Parameters.Add("@Excp_Trace_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Excp_Excp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Excp_Data", SqlDbType.VarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Excp_Message", SqlDbType.VarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Excp_StackTrace", SqlDbType.VarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Excp_TimeStamp", SqlDbType.DateTime).Direction = ParameterDirection.Input;
        //        //cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
        //        //cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

        //        #endregion Add Command Parameters

        //        #region Add Command Parameter Values
        //        cmd.Parameters["@Excp_Id"].Value = _data._a;

        //        cmd.Parameters["@Excp_Trace_Id"].Value = _data._b;

        //        if (null != _data._c)
        //        {
        //            cmd.Parameters["@Excp_Excp_Id"].Value = _data._c;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Excp_Excp_Id"].Value = DBNull.Value;
        //        }

        //        if (null != _data._d)
        //        {
        //            cmd.Parameters["@Excp_Data"].Value = _data._d;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Excp_Data"].Value = DBNull.Value;
        //        }

        //        if (null != _data._e)
        //        {
        //            cmd.Parameters["@Excp_Message"].Value = _data._e;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Excp_Message"].Value = DBNull.Value;
        //        }

        //        if (null != _data._f)
        //        {
        //            cmd.Parameters["@Excp_StackTrace"].Value = _data._f;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Excp_StackTrace"].Value = DBNull.Value;
        //        }

        //        if (null != _data._g)
        //        {
        //            cmd.Parameters["@Excp_TimeStamp"].Value = _data._g;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Excp_TimeStamp"].Value = DBNull.Value;
        //        }


        //        //cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

        //        #endregion Add Command Parameter Values

        //        //if (IfxInternalConfiguration.ScriptInsertUpdateOperations) { SQLScript.CreateScript(cmd); }
        //        cmd.ExecuteNonQuery();
        //        Excp_Id = ((Guid)(cmd.Parameters["@Excp_Id"].Value));
        //        iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
        //        if (data[7] != null)
        //        {
        //            return ix_Excpetion_Save((object[])data[7]);
        //        }
        //        return iSuccess;
        //        ////isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
        //        //if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
        //        //{
        //        //    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
        //        //}

        //        //DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
        //        ////**//  Hack:  we need to get this true or false value from the business object by using a 
        //        ////**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
        //        //dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
        //        ////  Set the DataServiceInsertUpdateResponse Result
        //        //if (iSuccess > 0)
        //        //{
        //        //    dataServiceResponse.Result = DataOperationResult.Success;
        //        //    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@"].Value));
        //        //}
        //        ////else if (isConcurrencyGood == false)
        //        ////{
        //        ////    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
        //        ////}
        //        //else
        //        //{
        //        //    dataServiceResponse.Result = DataOperationResult.HandledException;
        //        //}
        //        //if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
        //        //{
        //        //    //  we need a flag to determin if we return new data on a successful insert/update
        //        //    dataServiceResponse.CurrentValues = Excpetion_GetById_ObjectArray(Excp_Id);
        //        //}
        //        //// If the default is to not return the current row, then return the Primary Key here.
        //        //if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
        //        //{
        //        //    dataServiceResponse.guidPrimaryKey = Excp_Id;
        //        //}

        //        //return dataServiceResponse.ReturnAsObjectArray();
        //    }
        //    catch (Exception ex)
        //    {
        //        string sqlScript = SQLScript.CreateScript(cmd);
        //        //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "Excpetion_Save", new ValuePair[] {new ValuePair("check.ToString()", check.ToString()), new ValuePair("data.ToString()", data.ToString()), new ValuePair("sqlScript", sqlScript) }, IfxTraceCategory.Catch);
        //        //IfxWrapperException.GetError(ex);
        //        //DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
        //        //dataServiceResponse.Result = DataOperationResult.UnhandledException;
        //        //return dataServiceResponse.ReturnAsObjectArray();
        //        return 0;
        //    }

        //    finally
        //    {
        //        if (conn != null)
        //        {
        //            conn.Dispose();
        //        }
        //        //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "Excpetion_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
        //    }
        //}



        //public static int ix_Excpetion_SaveList(object[] data)
        //{
        //    SqlConnection conn = null;
        //    SqlCommand cmd = null;
        //    Guid Excp_Id;
        //    Int32 iSuccess = 0;
        //    //bool isConcurrencyGood = true;
        //    //Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
        //    //Excpetion_ValuesMngr newData=null;
        //    try
        //    {
        //        //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "Excpetion_Save", new ValuePair[] {new ValuePair("data.ToString()", data.ToString()) }, IfxTraceCategory.Enter);
        //        Excpetion_Values _data = new Excpetion_Values(data, null);
        //        conn = DBConnectoinHelper.GetDBConnection();
        //        conn.Open();
        //        cmd = conn.CreateCommand();
        //        cmd.CommandText = "dbo.spix_Excpetion_put";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        #region Add Command Parameters
        //        cmd.Parameters.Add("@Excp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
        //        cmd.Parameters.Add("@Excp_Trace_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Excp_Excp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Excp_Data", SqlDbType.VarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Excp_Message", SqlDbType.VarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Excp_StackTrace", SqlDbType.VarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Excp_TimeStamp", SqlDbType.DateTime).Direction = ParameterDirection.Input;
        //        //cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
        //        //cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

        //        #endregion Add Command Parameters



        //        #region Add Command Parameter Values
        //        cmd.Parameters["@Excp_Id"].Value = _data._a;

        //        cmd.Parameters["@Excp_Trace_Id"].Value = _data._b;

        //        if (null != _data._c)
        //        {
        //            cmd.Parameters["@Excp_Excp_Id"].Value = _data._c;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Excp_Excp_Id"].Value = DBNull.Value;
        //        }

        //        if (null != _data._d)
        //        {
        //            cmd.Parameters["@Excp_Data"].Value = _data._d;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Excp_Data"].Value = DBNull.Value;
        //        }

        //        if (null != _data._e)
        //        {
        //            cmd.Parameters["@Excp_Message"].Value = _data._e;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Excp_Message"].Value = DBNull.Value;
        //        }

        //        if (null != _data._f)
        //        {
        //            cmd.Parameters["@Excp_StackTrace"].Value = _data._f;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Excp_StackTrace"].Value = DBNull.Value;
        //        }

        //        if (null != _data._g)
        //        {
        //            cmd.Parameters["@Excp_TimeStamp"].Value = _data._g;
        //        }
        //        else
        //        {
        //            cmd.Parameters["@Excp_TimeStamp"].Value = DBNull.Value;
        //        }


        //        //cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

        //        #endregion Add Command Parameter Values

        //        //if (IfxInternalConfiguration.ScriptInsertUpdateOperations) { SQLScript.CreateScript(cmd); }
        //        cmd.ExecuteNonQuery();
        //        Excp_Id = ((Guid)(cmd.Parameters["@Excp_Id"].Value));
        //        iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
        //        return iSuccess;
        //        ////isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
        //        //if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
        //        //{
        //        //    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
        //        //}

        //        //DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
        //        ////**//  Hack:  we need to get this true or false value from the business object by using a 
        //        ////**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
        //        //dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
        //        ////  Set the DataServiceInsertUpdateResponse Result
        //        //if (iSuccess > 0)
        //        //{
        //        //    dataServiceResponse.Result = DataOperationResult.Success;
        //        //    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@"].Value));
        //        //}
        //        ////else if (isConcurrencyGood == false)
        //        ////{
        //        ////    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
        //        ////}
        //        //else
        //        //{
        //        //    dataServiceResponse.Result = DataOperationResult.HandledException;
        //        //}
        //        //if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
        //        //{
        //        //    //  we need a flag to determin if we return new data on a successful insert/update
        //        //    dataServiceResponse.CurrentValues = Excpetion_GetById_ObjectArray(Excp_Id);
        //        //}
        //        //// If the default is to not return the current row, then return the Primary Key here.
        //        //if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
        //        //{
        //        //    dataServiceResponse.guidPrimaryKey = Excp_Id;
        //        //}

        //        //return dataServiceResponse.ReturnAsObjectArray();
        //    }
        //    catch (Exception ex)
        //    {
        //        string sqlScript = SQLScript.CreateScript(cmd);
        //        //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "Excpetion_Save", new ValuePair[] {new ValuePair("check.ToString()", check.ToString()), new ValuePair("data.ToString()", data.ToString()), new ValuePair("sqlScript", sqlScript) }, IfxTraceCategory.Catch);
        //        //IfxWrapperException.GetError(ex);
        //        //DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
        //        //dataServiceResponse.Result = DataOperationResult.UnhandledException;
        //        //return dataServiceResponse.ReturnAsObjectArray();
        //        return 0;
        //    }

        //    finally
        //    {
        //        if (conn != null)
        //        {
        //            conn.Dispose();
        //        }
        //        //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "Excpetion_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
        //    }
        //}





        public static object[] GetExcpetion_lstBy_SessionId(Guid id)
        {
            SqlConnection conn = null;
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spix_Excpetion_lstBy_SessionId ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Session_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters["@Session_Id"].Value = id;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

                

                //***  is this faster than the line above?
                //List<object[]> list = new List<object[]>();
                //while (rdr.Read())
                //{
                //    object[] values = null;
                //    rdr.GetValues(values);
                //    list.Add(values);
                //}
                //return list.ToArray();

           
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetListByFK", IfxTraceCategory.Catch);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetListByFK", IfxTraceCategory.Leave);
            }
        }










    }
}


