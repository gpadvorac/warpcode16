using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
//using EntityWireType;
using TypeServices;
using System.Diagnostics;
using EntityWireType;
//using Ifx;

// Gen Timestamp:  1/3/2011 12:47:57 PM

namespace vDataServices
{
    public partial class ix_Excpetion_DataServices
    {

        #region Initialize Variables

        private static string _as = "vDataServices";
        private static string _cn = "ix_Excpetion_DataServices";

        #endregion Initialize Variables

        public static object[] ix_Excpetion_GetById(Guid id)
        {
            SqlConnection conn = null;
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spix_Excpetion_row", new SqlParameter("@Id", id));
                //rdr.Read();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetById", IfxTraceCategory.Catch);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetById", IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_Excpetion_GetById_ObjectArray(Guid id)
        {
            SqlConnection conn = null;
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spix_Excpetion_row", new SqlParameter("@Id", id));
                //rdr.Read();
                //object[] values = new object[rdr.FieldCount];
                //rdr.GetValues(values);
                //return values;
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetById_ObjectArray", IfxTraceCategory.Catch);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_Excpetion_GetAll()
        {
            SqlConnection conn = null;
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetAll", IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spix_Excpetion_lst ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
               // List<ix_Excpetion_ValuesMngr> list = new List<ix_Excpetion_ValuesMngr>();
               // while (rdr.Read())
               // {
               //     list.Add(new ix_Excpetion_ValuesMngr(rdr));
               // }
               // return list.ToArray();
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetAll", IfxTraceCategory.Catch);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_Excpetion_GetListByFK(Guid id)
        {
            SqlConnection conn = null;
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spix_Excpetion_lst ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Excp_Trace_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Excp_Trace_Id"].Value = id;
				SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
//                List<ix_Excpetion_ValuesMngr> list = new List<ix_Excpetion_ValuesMngr>();
//                while (rdr.Read())
//                {
//                    list.Add(new ix_Excpetion_ValuesMngr(rdr));
//                }
//                return list.ToArray();
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetListByFK", IfxTraceCategory.Catch);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_Excpetion_Delete(object[] data)
        {
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
//                //SqlConnectionHelper helper = new SqlConnectionHelper();
//                conn = DBConnectoinHelper.GetDBConnection();
//                conn.Open();
//                SqlCommand cmd = conn.CreateCommand();
//                cmd.CommandText = "dbo.spix_Excpetion_del ";
//                cmd.CommandType = System.Data.CommandType.StoredProcedure;
//                cmd.Parameters.Add("@ID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
//                cmd.Parameters["@Excp_Id"].Value = _data.C._a;
//                cmd.ExecuteNonQuery();
//                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                return null;
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_Delete", IfxTraceCategory.Catch);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_Delete", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_Excpetion_Deactivate(object[] data)
        {
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_Deactivate", IfxTraceCategory.Catch);
                return null;
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_Excpetion_Remove(object[] data)
        {
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_Remove", IfxTraceCategory.Catch);
                return null;
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "ix_Excpetion_Remove", IfxTraceCategory.Leave);
            }
        }


		#region Other Data Access Methods

		#endregion Other Data Access Methods


        #region Support Methods

//        static object[] ConvertSqlDataReaderToObjectArray(SqlDataReader rdr)
//        {
//            try
//            {
//                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Enter);
//                List<object[]> data = new List<object[]>();
//                int index = 0;
//                while (rdr.Read())
//                {
//                    object[] values = new object[rdr.FieldCount];
//                    for (int i = 0; i < rdr.FieldCount; i++)
//                    {
//                        if (rdr[i] == DBNull.Value)
//                        {
//                            values[i] = null;
//                        }
//                        else
//                        {
//                            values[i] = rdr[i];
//                        }
//                    }
//                    data.Add(values);
//                    index++;
//                    //if (index == 2)
//                    //{
//                    //    break;
//                    //}
//                }
//                rdr.Close();
//                return data.ToArray();
//            }
//            catch(Exception ex)
//            {
//                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Catch);
//                return null;
//            }
//            finally
//            {
//                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Leave);
//            }
//        }

        #endregion Support Methods


    }
}


