using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
//using EntityWireType;
using TypeServices;
using System.Data;
using Ifx;
using EntityWireType;

namespace vDataServices
{
    public partial class ix_TraceSession_DataServices
    {

        #region Initialize Variables



        #endregion Initialize Variables

        static Guid _userId = new Guid("90D39629-E263-4974-903F-EEABEBF5E72D");

        private static bool _returnCurrentRowOnInsertUpdate = false;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }


        public static int ix_TraceSession_Save(object[] data)
        {
            //Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid TraceSe_Id;
            Int32 iSuccess = 0;
            //bool isConcurrencyGood = true;
            //Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceSession_Save", new ValuePair[] { new ValuePair("data.ToString()", data.ToString()) }, IfxTraceCategory.Enter);
                ix_TraceSession_Values _data = new ix_TraceSession_Values(data);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spix_TraceSession_putInsert";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
                cmd.Parameters.Add("@TraceSe_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
                cmd.Parameters.Add("@TraceSe_Application_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@TraceSe_Name", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@TraceSe_Desc", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@TraceSe_UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@TraceSe_Created", SqlDbType.DateTime).Direction = ParameterDirection.Input;
                //cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                //cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters

                #region Add Command Parameter Values
                cmd.Parameters["@TraceSe_Id"].Value = _data._a;

                if (null != _data._b)
                {
                    cmd.Parameters["@TraceSe_Application_Id"].Value = _data._b;
                }
                else
                {
                    cmd.Parameters["@TraceSe_Application_Id"].Value = DBNull.Value;
                }

                if (null != _data._c)
                {
                    cmd.Parameters["@TraceSe_Name"].Value = _data._c;
                }
                else
                {
                    cmd.Parameters["@TraceSe_Name"].Value = DBNull.Value;
                }

                if (null != _data._d)
                {
                    cmd.Parameters["@TraceSe_Desc"].Value = _data._d;
                }
                else
                {
                    cmd.Parameters["@TraceSe_Desc"].Value = DBNull.Value;
                }

                //if (null != _data._e)
                //{
                cmd.Parameters["@TraceSe_UserId"].Value = _userId;
                //}
                //else
                //{
                //    cmd.Parameters["@TraceSe_UserId"].Value = DBNull.Value;
                //}

                if (null != _data._f)
                {
                    cmd.Parameters["@TraceSe_Created"].Value = _data._f;
                }
                else
                {
                    cmd.Parameters["@TraceSe_Created"].Value = DBNull.Value;
                }

                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "ix_TraceSession_Save", SQLScript.CreateScript(cmd));
                }
                cmd.ExecuteNonQuery();
                TraceSe_Id = ((Guid)(cmd.Parameters["@TraceSe_Id"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));

                return iSuccess;


                //isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
                //if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                //{
                //    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                //}

                //DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                ////**//  Hack:  we need to get this true or false value from the business object by using a 
                ////**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
                //dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
                ////  Set the DataServiceInsertUpdateResponse Result
                //if (iSuccess > 0)
                //{
                //    dataServiceResponse.Result = DataOperationResult.Success;
                //    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@"].Value));
                //}
                //else if (isConcurrencyGood == false)
                //{
                //    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
                //}
                //else
                //{
                //    dataServiceResponse.Result = DataOperationResult.HandledException;
                //}
                //if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
                //{
                //    //  we need a flag to determin if we return new data on a successful insert/update
                //    dataServiceResponse.CurrentValues = ix_TraceSession_GetById_ObjectArray(TraceSe_Id);
                //}
                //// If the default is to not return the current row, then return the Primary Key here.
                //if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
                //{
                //    dataServiceResponse.guidPrimaryKey = TraceSe_Id;
                //}

                //return dataServiceResponse.ReturnAsObjectArray();
            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceSession_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data.ToString()", data.ToString()), new ValuePair("sqlScript", sqlScript) }, null, ex);
                //IfxWrapperException.GetError(ex);
                //DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                //dataServiceResponse.Result = DataOperationResult.UnhandledException;
                //return dataServiceResponse.ReturnAsObjectArray();
                return iSuccess;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceSession_Save", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }




        public static object[] GetTraceSession_lstMin()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTraceSession_lstMin", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spix_TraceSession_lstMin ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTraceSession_lstMin", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTraceSession_lstMin", IfxTraceCategory.Leave);
            }
        }




    }
}


