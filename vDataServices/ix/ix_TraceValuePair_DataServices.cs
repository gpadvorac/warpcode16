using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;

// Gen Timestamp:  1/18/2011 11:03:19 AM

namespace vDataServices
{
    public partial class ix_TraceValuePair_DataServices
    {

        #region Initialize Variables

        private static string _as = "vDataServices";
        private static string _cn = "ix_TraceValuePair_DataServices";

        #endregion Initialize Variables

        public static object[] ix_TraceValuePair_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spix_TraceValuePair_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_GetById", IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_TraceValuePair_GetById_ObjectArray(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spix_TraceValuePair_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_GetById_ObjectArray", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_TraceValuePair_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_GetAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spix_TraceValuePair_lst ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_GetAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_TraceValuePair_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spix_TraceValuePair_lst ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@TraceVP_Trace_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@TraceVP_Trace_Id"].Value = id;
				SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_GetListByFK", IfxTraceCategory.Leave);
            }
        }
 
        public static object[] ix_TraceValuePair_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
//                //SqlConnectionHelper helper = new SqlConnectionHelper();
//                conn = DBConnectoinHelper.GetDBConnection();
//                conn.Open();
//                SqlCommand cmd = conn.CreateCommand();
//                cmd.CommandText = "dbo.spix_TraceValuePair_del ";
//                cmd.CommandType = System.Data.CommandType.StoredProcedure;
//                cmd.Parameters.Add("@ID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
//                cmd.Parameters["@TraceVP_Id"].Value = _data.C._a;
//                cmd.ExecuteNonQuery();
//                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_Delete", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_Delete", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_TraceValuePair_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] ix_TraceValuePair_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_Remove", IfxTraceCategory.Leave);
            }
        }


		#region Other Data Access Methods

        public static object[] GetTraceValuePairType_cmb()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTraceValuePairType_cmb", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spix_TraceValuePairType_cmb";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTraceValuePairType_cmb", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTraceValuePairType_cmb", IfxTraceCategory.Leave);
            }
        }

		#endregion Other Data Access Methods


        #region Support Methods

//        static object[] ConvertSqlDataReaderToObjectArray(SqlDataReader rdr)
//        {
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Enter);
//                List<object[]> data = new List<object[]>();
//                int index = 0;
//                while (rdr.Read())
//                {
//                    object[] values = new object[rdr.FieldCount];
//                    for (int i = 0; i < rdr.FieldCount; i++)
//                    {
//                        if (rdr[i] == DBNull.Value)
//                        {
//                            values[i] = null;
//                        }
//                        else
//                        {
//                            values[i] = rdr[i];
//                        }
//                    }
//                    data.Add(values);
//                    index++;
//                    //if (index == 2)
//                    //{
//                    //    break;
//                    //}
//                }
//                rdr.Close();
//                return data.ToArray();
//            }
//            catch(Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", ex);
//                return null;
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSqlDataReaderToObjectArray", IfxTraceCategory.Leave);
//            }
//        }

        #endregion Support Methods


    }
}


