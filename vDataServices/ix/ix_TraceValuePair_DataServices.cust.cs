using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
//using EntityWireType;
using TypeServices;
using System.Data;
using Ifx;
using EntityWireType;

namespace vDataServices
{
    public partial class ix_TraceValuePair_DataServices
    {

        #region Initialize Variables



        #endregion Initialize Variables

        private static bool _returnCurrentRowOnInsertUpdate = false;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }


        //public static int ix_TraceValuePair_Save(Guid traceId ,object[] data, IfxTraceValuePairType type)
        //{
        //    //Guid? traceId = Guid.NewGuid();
        //    SqlConnection conn = null;
        //    SqlCommand cmd = null;
        //    Guid TraceVP_Id;
        //    Int32 iSuccess = 0;
        //    try
        //    {
        //        //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_Save", new ValuePair[] { new ValuePair("data.ToString()", data.ToString()) }, IfxTraceCategory.Enter);
        //        ix_TraceValuePair_Values _data = new ix_TraceValuePair_Values(data);
        //        conn = DBConnectoinHelper.GetDBConnection();
        //        conn.Open();
        //        cmd = conn.CreateCommand();
        //        cmd.CommandText = "dbo.spix_TraceValuePair_put";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;

        //        #region Add Command Parameters
        //        cmd.Parameters.Add("@TraceVP_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
        //        cmd.Parameters.Add("@TraceVP_Trace_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@TraceVP_Key", SqlDbType.VarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@TraceVP_Value", SqlDbType.VarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@TraceVP_TypeId", SqlDbType.Int).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

        //        #endregion Add Command Parameters

        //        #region Add Command Parameter Values

        //        cmd.Parameters["@TraceVP_Id"].Value = _data._a;

        //        cmd.Parameters["@TraceVP_Trace_Id"].Value = _data._b;

        //        cmd.Parameters["@TraceVP_Key"].Value = _data._c;

        //        cmd.Parameters["@TraceVP_Value"].Value = _data._d;

        //        cmd.Parameters["@TraceVP_TypeId"].Value = (int)type;

        //        #endregion Add Command Parameter Values

        //        if (IfxTrace._scriptInsertUpdateOperations)
        //        {
        //            IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "ix_TraceValuePair_Save", SQLScript.CreateScript(cmd));
        //        }
        //        cmd.ExecuteNonQuery();
        //        TraceVP_Id = ((Guid)(cmd.Parameters["@TraceVP_Id"].Value));
        //        iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
        //        if (iSuccess != 1)
        //        {
        //            throw new Exception("Insert failed");
        //        }
        //        return iSuccess;
          
        //    }
        //    catch (Exception ex)
        //    {
        //        string sqlScript = SQLScript.CreateScript(cmd);
        //        if (IfxTrace._traceCatch) IfxEvent.PublishIfxExceptionTraceToXmlFile(_as, _cn, "ix_Trace_Save", null, new ValuePair[] { new ValuePair("data.ToString()", data.ToString()), new ValuePair("sqlScript", sqlScript) }, null, ex);
        //        return iSuccess;
        //    }

        //    finally
        //    {
        //        if (conn != null)
        //        {
        //            conn.Dispose();
        //        }
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_Save", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
        //    }
        //}




        //public static int ix_TraceValuePair_SaveList(object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    SqlConnection conn = null;
        //    SqlCommand cmd = null;
        //    Guid TraceVP_Id;
        //    Int32 iSuccess = 0;
        //    try
        //    {
        //        //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_Save", new ValuePair[] { new ValuePair("data.ToString()", data.ToString()) }, IfxTraceCategory.Enter);
               
        //        conn = DBConnectoinHelper.GetDBConnection();
        //        conn.Open();
        //        cmd = conn.CreateCommand();
        //        cmd.CommandText = "dbo.spix_TraceValuePair_put";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        #region Add Command Parameters
        //        cmd.Parameters.Add("@TraceVP_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
        //        cmd.Parameters.Add("@TraceVP_Trace_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@TraceVP_Key", SqlDbType.VarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@TraceVP_Value", SqlDbType.VarChar).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@TraceVP_TypeId", SqlDbType.Int).Direction = ParameterDirection.Input;
        //        cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

        //        #endregion Add Command Parameters


        //        if (data != null)
        //        {

        //            for (int iCnt = 0; iCnt <= data.GetUpperBound(0); iCnt++)
        //            {
        //                ix_TraceValuePair_Values _data = new ix_TraceValuePair_Values((object[])data[iCnt]);



        //                #region Add Command Parameter Values
        //                cmd.Parameters["@TraceVP_Id"].Value = _data._a;

        //                cmd.Parameters["@TraceVP_Trace_Id"].Value = _data._b;

        //                cmd.Parameters["@TraceVP_Key"].Value = _data._c;

        //                cmd.Parameters["@TraceVP_Value"].Value = _data._d;

        //                cmd.Parameters["@TraceVP_TypeId"].Value = _data._e;

        //                #endregion Add Command Parameter Values

        //                if (IfxTrace._scriptInsertUpdateOperations)
        //                {
        //                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "ix_TraceValuePair_Save", SQLScript.CreateScript(cmd));
        //                }
                        
        //                cmd.ExecuteNonQuery();
        //                //Trace_Id = ((Guid)(cmd.Parameters["@Trace_Id"].Value));
        //                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));

        //                if (iSuccess != 1)
        //                {
        //                    throw new Exception("Insert failed");
        //                }


        //            }
        //        }
                
        //        cmd.ExecuteNonQuery();
        //        TraceVP_Id = ((Guid)(cmd.Parameters["@TraceVP_Id"].Value));
        //        iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
        //        return iSuccess;
        //    }
        //    catch (Exception ex)
        //    {
        //        string sqlScript = SQLScript.CreateScript(cmd);
        //        if (IfxTrace._traceCatch) IfxEvent.PublishIfxExceptionTraceToXmlFile(_as, _cn, "ix_TraceValuePair_SaveList", null, new ValuePair[] { new ValuePair("data.ToString()", data.ToString()), new ValuePair("sqlScript", sqlScript) }, null, ex);
        //        return iSuccess;
        //    }

        //    finally
        //    {
        //        if (conn != null)
        //        {
        //            conn.Dispose();
        //        }
        //        //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ix_TraceValuePair_Save", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
        //    }
        //}



        public static object[] GetTraceValuePair_lstBy_SessionId(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTraceValuePair_lstBy_SessionId", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spix_TraceValuePair_lstBy_SessionId";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Session_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters["@Session_Id"].Value = id;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTraceValuePair_lstBy_SessionId", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTraceValuePair_lstBy_SessionId", IfxTraceCategory.Leave);
            }
        }




    }
}


