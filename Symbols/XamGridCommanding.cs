﻿using System;
using System.Windows.Input;

namespace Infragistics.Controls.Grids
{
    #region XamGridCommand

    /// <summary>
    /// An enumeration of available commands for the <see cref="XamGrid" /> object.
    /// </summary>
    public enum XamGridCommand
    {
        /// <summary>
        /// Deletes a row based on the <see cref="XamGrid.DeleteKeyAction"/> setting.
        /// </summary>
        Delete,

        /// <summary>
        /// Selects all rows of the main layout of the XamGrid
        /// </summary>
        SelectAllRows,

        /// <summary>
        /// Shows the ColumnChooser for the main layout of the XamGrid
        /// </summary>
        ShowColumnChooser,


        /// <summary>
        /// Hides the ColumnChooser
        /// </summary>
        HideColumnChooser
    }

    #endregion // XamGridCommand

    #region XamGridCommandSource

    /// <summary>
    /// The command source object for <see cref="XamGrid"/> object.
    /// </summary>
    public class XamGridCommandSource : CommandSource
    {
        /// <summary>
        /// Gets / sets the <see cref="XamGridCommand"/> which is to be executed by the command.
        /// </summary>
        public XamGridCommand CommandType { get; set; }

        protected override ICommand ResolveCommand()
        {
            switch (CommandType)
            {
                case XamGridCommand.Delete:
                    return new DeleteCommand();
                case XamGridCommand.SelectAllRows:
                    return new SelectAllRowsCommand();
                case XamGridCommand.ShowColumnChooser:
                    return new ShowColumnChooserCommand();
                case XamGridCommand.HideColumnChooser:
                    return new HideColumnChooserCommand();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    #endregion // XamGridCommandSource

    #region XamGridCommandBase

    /// <summary>
    /// Base class for all commands that deal with a <see cref="XamGrid"/> instance.
    /// </summary>
    public abstract class XamGridCommandBase : CommandBase
    {
        public override bool CanExecute(object parameter)
        {
            return true;
        }

        public override void Execute(object parameter)
        {
            XamGrid xamGrid = parameter as XamGrid;

            if (xamGrid != null)
            {
                this.Execute(xamGrid);
            }

            base.Execute(parameter);
        }

        protected abstract void Execute(XamGrid parameter);
    }

    #endregion // XamGridCommandBase

    #region SelectAllRowsCommand

    /// <summary>
    /// Selects all rows of the <see cref="XamGrid"/>.
    /// </summary>
    public class SelectAllRowsCommand : XamGridCommandBase
    {
        protected override void Execute(XamGrid grid)
        {
            grid.Rows.SelectAll();
        }
    }

    #endregion // SelectAllRowsCommand

    #region DeleteCommand

    /// <summary>
    /// Invokes the delete action based on <see cref="XamGrid.DeleteKeyAction"/>.
    /// </summary>
    public class DeleteCommand : XamGridCommandBase
    {
        protected override void Execute(XamGrid grid)
        {
            grid.DeleteKey();
        }
    }

    #endregion // DeleteCommand

    #region ShowColumnChooserCommand

    /// <summary>
    /// Shows the ColumnChooser for the main layout of the <see cref="XamGrid"/>.
    /// </summary>
    public class ShowColumnChooserCommand : XamGridCommandBase
    {
        protected override void Execute(XamGrid grid)
        {
            grid.ShowColumnChooser();
        }
    }

    #endregion // ShowColumnChooserCommand

    #region HideColumnChooserCommand

    /// <summary>
    /// Hides the ColumnChooser.
    /// </summary>
    public class HideColumnChooserCommand : XamGridCommandBase
    {
        protected override void Execute(XamGrid grid)
        {
            grid.HideColumnChooser();
        }
    }

    #endregion // HideColumnChooserCommand
}
