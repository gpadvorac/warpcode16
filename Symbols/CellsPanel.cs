using System;
using System.Windows.Input;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Windows.Controls.Primitives;
using System.Windows.Automation.Peers;



#region Infragistics Source Cleanup (Region)



#endregion // Infragistics Source Cleanup (Region)

using Infragistics.AutomationPeers;

namespace Infragistics.Controls.Grids.Primitives
{
    /// <summary>
    /// A panel that organizes the <see cref="CellBase"/>s of a <see cref="RowBase"/>.
    /// </summary>
    public class CellsPanel : Panel, ICommandTarget
    {
        #region Members

        // Minimal count of columns to render if the grid is not fully initialized.
        private const int MinColumnsToRender = 50;

        private bool _isInDesigner;
        private bool _isEnteringEditMode;
        private bool _hasInfiniteWidth;
        private bool _fillerCellNeedsToBeReArranged;
        private double _prevAvailableWidth = -1;
        private double _averageCellWidth = 0;

        //TFS 208306, 209586 The _hiddenCellRect has to be big enough to hide cells with huge height
        private Rect _hideCellRect = new Rect(-100000, -100000, 0, 0);

        private RowBase _row;
        private Collection<CellBase> _visibleCells, _visibleFixedLeftCells, _visibleFixedRightCells, _additionalCells, _lastCellsDetached, _nonReleasedCells;
        private RowsPanel _owner;
        private IProvideScrollInfo _scrollInfo;
        private RectangleGeometry _clipRG;

        #endregion // Members

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CellsPanel"/> class.
        /// </summary>
        public CellsPanel()
        {
            this._visibleCells = new Collection<CellBase>();
            this._visibleFixedLeftCells = new Collection<CellBase>();
            this._visibleFixedRightCells = new Collection<CellBase>();
            this._additionalCells = new Collection<CellBase>();
            this._clipRG = new RectangleGeometry();
            this.Clip = this._clipRG;
            this._lastCellsDetached = new Collection<CellBase>();
            this._nonReleasedCells = new Collection<CellBase>();





            this._isInDesigner = System.ComponentModel.DesignerProperties.IsInDesignTool;

        }

        #endregion // Constructor

        #region Properties

        #region Public	

        #region Row

        /// <summary>
        /// Gets a refernce to the <see cref="RowBase"/> that owns the <see cref="CellsPanel"/>.
        /// </summary>
        public RowBase Row
        {
            get { return this._row; }
        }

        #endregion // Row

        #region Owner

        /// <summary>
        /// Gets/sets the RowsPanel that owns this control.
        /// </summary>
        public RowsPanel Owner
        {
            get { return this._owner; }
            set
            {
                this._owner = value;
                if (value != null)
                {
                    this._scrollInfo = (IProvideScrollInfo)this._owner.Grid;
                }
                else
                {
                    this._scrollInfo = null;
                }
            }
        }

        #endregion // Owner

        #endregion // Public

        #region Protected

        #region VisibleCells

        /// <summary>
        /// Gets a list of <see cref="CellBase"/> objects thats are currently visible.
        /// </summary>
        protected internal Collection<CellBase> VisibleCells
        {
            get { return this._visibleCells; }
        }

        #endregion // VisibleCells

        #region VisibleFixedLeftCells

        /// <summary>
        /// Gets a list of <see cref="CellBase"/> objects that are fixed to the left.
        /// </summary>
        protected internal Collection<CellBase> VisibleFixedLeftCells
        {
            get { return this._visibleFixedLeftCells; }
        }

        #endregion // VisibleFixedLeftCells

        #region VisibleFixedRightCells

        /// <summary>
        /// Gets a list of <see cref="CellBase"/> objects that are fixed to the right. 
        /// </summary>
        protected internal Collection<CellBase> VisibleFixedRightCells
        {
            get { return this._visibleFixedRightCells; }
        }

        #endregion // VisibleFixedRightCells

        #endregion // Protected

        #region Internal

        /// <summary>
        /// Used to store off the last height that was passed into the MeasuerOverride method of this panel.
        /// </summary>
        internal double PreviousInvalidateHeight
        {
            get;
            set;
        }

        internal bool ArrangeRaised
        {
            get;
            set;
        }

        internal RectangleGeometry ClipRect
        {
            get { return this._clipRG; }
        }

        #endregion // Internal

        #region Private

        /// <summary>
        /// Get the horizontal scroll value
        /// </summary>
        private double HorizontalScrollValueSafe
        {
            get
            {
                ScrollBar bar = this._scrollInfo.HorizontalScrollBar;
                if (bar == null || bar.Maximum == 0)
                {
                    return 0;
                }

                return bar.Value;
            }
        }

        #endregion //Private

        #endregion // Properties

        #region Methods

        #region Public

        #region OnAttached

        /// <summary>
        /// Called when the <see cref="RowBase"/> is attached to the <see cref="CellsPanel"/>.
        /// </summary>
        /// <param n="row">The row that is being attached to the <see cref="CellsPanel"/></param>
        protected internal virtual void OnAttached(RowBase row)
        {
            
#region Infragistics Source Cleanup (Region)





#endregion // Infragistics Source Cleanup (Region)

            row.SkipApplyStyle = true;
            this._row = row;
            this.DataContext = this._row.Data;
            row.SkipApplyStyle = false;
        }

        #endregion // OnAttached

        #region OnReleased

        /// <summary>
        /// Called when the <see cref="RowBase"/> releases the <see cref="CellsPanel"/>.
        /// </summary>
        protected internal virtual void OnReleased()
        {
            RowBase row = this._row;
            row.IsReleasingCells = true;

            this._row = null;
            this.DataContext = null;

            foreach (CellBase cell in this._visibleFixedLeftCells)
            {
                this.ReleaseCell(cell);
            }

            foreach (CellBase cell in this._visibleCells)
            {
                this.ReleaseCell(cell);
            }

            foreach (CellBase cell in this._visibleFixedRightCells)
            {
                this.ReleaseCell(cell);
            }

            foreach (CellBase cell in this._additionalCells)
            {
                this.ReleaseCell(cell);
            }

            this._additionalCells.Clear();
            this._visibleCells.Clear();
            this._visibleFixedLeftCells.Clear();
            this._visibleFixedRightCells.Clear();
            this._lastCellsDetached.Clear();

            row.IsReleasingCells = false;
        }

        #endregion // OnReleased

        #endregion // Public

        #region Protected

        #region RenderCellsComplex

        private Size RenderCellsComplex(double availableWidth)
        {
            bool isInfinite = double.IsPositiveInfinity(availableWidth);
            double maxHeight = 0;

            RowsManagerBase manager = this.Row.Manager;
            ReadOnlyCollection<Column> visibleColumns = this.Row.Columns.VisibleColumns;
            bool hasFillerColumn = visibleColumns.Any(i => i is FillerColumn);

            // If we are measured with inf width, and the visible columns doesn't contain teh FillerColumn,
            // we'll add it to the list of visible columns so we can add it to the panel. The size of
            // the filler will be determined during arrange.
            if (isInfinite && !hasFillerColumn)
            {
                List<Column> columns = new List<Column>(visibleColumns);
                columns.Add(this.Row.Columns.FillerColumn);
                visibleColumns = new ReadOnlyCollection<Column>(columns);
                hasFillerColumn = true;
            }

            int colCount = visibleColumns.Count;

            Collection<CellBase> starColumns = new Collection<CellBase>();

            double indentation = (manager.IsFirstRowRenderingInThisLayoutCycle) ? manager.ResolveIndentation(this.Row) : manager.CachedIndentation;
            manager.CachedIndentation = indentation;

            double currentWidth = indentation;
            double fixedCellWidth = 0;

            // TFS117323 - If the visibleColumns collection doesn't contain the FillerColumn,
            // let's reset its ActualWidth so it won't interfere with any calculations
            if (!hasFillerColumn)
            {
                this.Row.Columns.FillerColumn.ActualWidth = 0;
            }

            // Render Fixed Adorner Columns First. 
            ReadOnlyCollection<Column> fixedColumns = this.Row.Columns.FixedAdornerColumns;
            foreach (Column column in fixedColumns)
            {
                if (currentWidth >= availableWidth)
                    break;

                currentWidth += this.RenderCell(column, starColumns, ref maxHeight, false, this.VisibleFixedLeftCells, isInfinite);
            }

            bool visibleFixedColumn = false;

            // Place MergedColumns to the Left of the FixedDataColumns.
            if (this.Row.ColumnLayout.Grid.GroupBySettings.GroupByOperation == GroupByOperation.MergeCells)
            {
                ReadOnlyCollection<Column> mergedColumns = this.Row.Columns.GroupByColumns[this.Row.ColumnLayout];
                foreach (Column column in mergedColumns)
                {
                    if (column.Visibility == Visibility.Visible)
                    {
                        visibleFixedColumn = true;

                        if (currentWidth >= availableWidth)
                            break;

                        currentWidth += this.RenderCell(column, starColumns, ref maxHeight, false, this.VisibleFixedLeftCells, isInfinite);
                    }
                }
            }

            // Next Render Fixed Data Columns
            FixedColumnsCollection fixedDataColumns = this.Row.Columns.FixedColumnsLeft;
            if (fixedDataColumns.Count > 0)
            {

                foreach (Column column in fixedDataColumns)
                {
                    if (column.Visibility == Visibility.Visible)
                    {
                        visibleFixedColumn = true;

                        if (currentWidth >= availableWidth)
                            break;

                        currentWidth += this.RenderCell(column, starColumns, ref maxHeight, false, this.VisibleFixedLeftCells, isInfinite);
                    }
                }
            }

            if (visibleFixedColumn)
            {
                // Reset
                this.Row.Columns.FixedBorderColumnLeft.ActualWidth = 0;
                currentWidth += this.RenderCell(this.Row.Columns.FixedBorderColumnLeft, starColumns, ref maxHeight, false, this._additionalCells, isInfinite);
                visibleFixedColumn = false;
            }

            fixedDataColumns = this.Row.Columns.FixedColumnsRight;
            if (fixedDataColumns.Count > 0)
            {
                foreach (Column column in fixedDataColumns)
                {
                    if (column.Visibility == Visibility.Visible)
                    {
                        visibleFixedColumn = true;
                        if (currentWidth >= availableWidth)
                            break;

                        currentWidth += this.RenderCell(column, starColumns, ref maxHeight, false, this.VisibleFixedRightCells, isInfinite);
                    }
                }

                if (visibleFixedColumn)
                {
                    // Reset
                    this.Row.Columns.FixedBorderColumnRight.ActualWidth = 0;
                    currentWidth += this.RenderCell(this.Row.Columns.FixedBorderColumnRight, starColumns, ref maxHeight, false, this._additionalCells, isInfinite);
                }
            }

            fixedCellWidth = currentWidth - indentation;

            int startCell = 0;
            int currentCell = 0;
            double percentScroll = 0;
            double cellWidth = 0;

            double scrollLeft = this.ResolveScrollLeft();

            if (scrollLeft == -1)
            {
                // This must be the first time we're rendering a row on this level, however, we are already horizontal scrolled
                // so lets resolve our HorizontalMax, and try again. 
                if (this.Row.RowType == RowType.DataRow)
                {
                    this.InvalidateHorizontalMax(availableWidth - (fixedCellWidth + indentation), ref maxHeight, isInfinite);
                    scrollLeft = this.ResolveScrollLeft();
                    this.Owner.MeasureCalled = false;
                    this.Owner.InvalidateMeasureInternal();
                }
                else
                {
                    this.InvalidateHorizontalMax(availableWidth - (fixedCellWidth + indentation), ref maxHeight, isInfinite);
                    scrollLeft = this.ResolveScrollLeft();
                    if (scrollLeft != 0)
                        manager.InvalidateOverrideHorizontalMax = true;
                }
            }

            ReadOnlyKeyedColumnBaseCollection<Column> allVisibleChildColumns = this.Row.Columns.AllVisibleChildColumns;

            if (!this.Owner.ReverseCellMeasure)
            {
                // Add First Cell
                int realStartIndex = Math.Max(0, (int)scrollLeft);

                Column realFirstColumn = null;

                if (realStartIndex < allVisibleChildColumns.Count)
                {
                    realFirstColumn = allVisibleChildColumns[realStartIndex];
                    Column rootColumn = realFirstColumn.ResolveRootColumn();

                    // Transform startCell to root indices
                    if (rootColumn != null && visibleColumns.Contains(rootColumn))
                    {
                        int rootStartIndex = visibleColumns.IndexOf(rootColumn);
                        startCell = rootStartIndex;
                    }
                    else
                    {
                        startCell = visibleColumns.IndexOf(realFirstColumn);
                    }
                }

                
#region Infragistics Source Cleanup (Region)


#endregion // Infragistics Source Cleanup (Region)

                startCell = startCell < 0 ? 0 : startCell;

                if (startCell < visibleColumns.Count)
                {
                    Column firstColumn = visibleColumns[startCell];
                    cellWidth = this.RenderCell(firstColumn, starColumns, ref maxHeight, false, this.VisibleCells, isInfinite);
                    currentWidth += cellWidth;

                    // Calculate PercentScroll
                    double percent = scrollLeft - (int)scrollLeft;

                    GroupColumn groupColumn = firstColumn as GroupColumn;

                    if (groupColumn != null)
                    {
                        percentScroll = 0; // reset
                        ReadOnlyKeyedColumnBaseCollection<Column> childColumns = groupColumn.AllVisibleChildColumns;

                        foreach (var childColumn in childColumns)
                        {
                            if (childColumn != realFirstColumn)
                            {
                                percentScroll += childColumn.ActualWidth;
                            }
                            else
                            {
                                percentScroll += realFirstColumn.ActualWidth * percent;
                                break;
                            }
                        }
                    }
                    else
                    {
                        percentScroll = cellWidth * percent;
                    }

                    currentWidth -= percentScroll;

                    // Use this flag to force rendering of more columns in case the columns are not fully initialized.
                    bool isHeaderOrFirstVisibleRow = this.IsHeaderOrFirstVisibleRowInParentLayout();

                    // Add Cells until there is no more width left
                    double currentCellWidth = 0;
                    for (currentCell = startCell + 1; currentCell < colCount; currentCell++)
                    {
                        Column col = visibleColumns[currentCell];

                        // Do not render filler column in case we are processing the header/first row for the first time
                        if (isHeaderOrFirstVisibleRow && col is FillerColumn && currentWidth >= availableWidth)
                            continue;

                        // render a given number of columns if we are rendering a header row or the first data row
                        // but only in case those columns haven't been rendered at all
                        if (currentWidth >= availableWidth &&
                            (col.ActualWidth > 0 ||
                             (isHeaderOrFirstVisibleRow && col.ActualWidth == 0 && currentCell >= MinColumnsToRender)))
                            break;

                        currentCellWidth = this.RenderCell(col, starColumns, ref maxHeight, false, this.VisibleCells, isInfinite);
                        currentWidth += currentCellWidth;
                    }

                    // Add the percent scroll back, so that we can truly validate if we've scrolled past the last item.
                    currentWidth += percentScroll;
                }

                startCell--;

                // Render StarCells
                // This needs to be done ealier than we previously used it, b/c now it returns a width, b/c
                // StartColumns can effect the scrolling width now, due to the fact that they support MinWidth
                currentWidth = this.SetupStarCells(availableWidth, currentWidth, ref maxHeight, starColumns);
            }
            else
            {
                if (this.Owner.ReverseColumnLayout == this.Row.ColumnLayout)
                {
                    startCell = this.Owner.ReverCellStartIndex;
                }
                else
                {
                    // In case we are rendering cells reversively but the start cell index is from different column layout
                    // we need to find the correct cell index for the current row's column layout
                    startCell = this.GetIndexOfLastColumnInDesiredView(visibleColumns, scrollLeft, availableWidth);
                }

                int visCellCount = this.Row.VisibleCells.Count;
                int visColCount = visibleColumns.Count;

                // Check and make sure that the startCell isn'type greater than the visible cells. 
                // This would happen if the cell scrolling into view, is on a different band. 
                if (startCell >= visCellCount)
                    startCell = visCellCount - 1;
                




                if (startCell >= visColCount)
                    startCell = visColCount - 1;
            }


            // If the width of all the visible cells is less then whats available in the viewport, and there are more cells in the 
            // collection, it means we've scrolled further than we needed to. Since we don't want whitespace to appear after 
            // the last cell, lets add more cells to the front.
            if (currentWidth < availableWidth && this._visibleCells.Count < colCount)
            {
                for (currentCell = startCell; currentCell >= 0; currentCell--)
                {
                    Column c = visibleColumns[currentCell];

                    // FillerColumn shouldn't have an influence on the determination if percentScroll should be reset to zero.
                    if (c is FillerColumn)
                        startCell--;

                    cellWidth = this.RenderCell(c, starColumns, ref maxHeight, true, this.VisibleCells, isInfinite);

                    // If the start cell is a group cell check if it should be partially visible.
                    if (c is GroupColumn && this.Row.ColumnLayout == this.Owner.ReverseColumnLayout && this.Owner.ReverseChildColumn != null &&
                        this.Owner.ReverseChildColumn.ParentColumn != null && currentCell == this.Owner.ReverCellStartIndex)
                    {
                        // Take only the visible width of the group cell into account.
                        var parentColumn = this.Owner.ReverseChildColumn.TopParentColumn;
                        int reverseChildColumnIndex = parentColumn.AllVisibleChildColumns.IndexOf(this.Owner.ReverseChildColumn);

                        for (int i = reverseChildColumnIndex; i >= 0; i--)
                        {
                            var column = parentColumn.AllVisibleChildColumns[i];

                            currentWidth += column.ActualWidth;
                        }
                    }
                    else
                    {
                        currentWidth += cellWidth;
                    }

                    if (currentWidth >= availableWidth)
                    {
                        if (this.Owner.ReverseCellMeasure)
                        {
                            // If manager.OverflowAdjustment is zero, assume it hasn't been set. 
                            if (manager.OverflowAdjustment == 0)
                                manager.OverflowAdjustment = currentWidth - availableWidth;
                            else
                                manager.OverflowAdjustment = Math.Min(manager.OverflowAdjustment, (currentWidth - availableWidth));

                            percentScroll = (manager.OverflowAdjustment / cellWidth);
                            if (currentCell == startCell)
                                percentScroll = 0;

                            if (this.Row.ColumnLayout == this.Owner.ReverseColumnLayout)
                            {
                                this.InvalidateHorizontalMax(availableWidth - (fixedCellWidth + indentation), ref maxHeight, isInfinite);

                                // This is for GroupCells
                                if (this.Owner.ReverseChildColumn != null && this.Owner.ReverseChildColumn.ParentColumn != null && startCell == this.Owner.ReverCellStartIndex)
                                {
                                    if (!double.IsPositiveInfinity(availableWidth))
                                    {
                                        int reverseChildColumnIndex = allVisibleChildColumns.IndexOf(this.Owner.ReverseChildColumn);
                                        CellBase childCell = this.Row.Cells[this.Owner.ReverseChildColumn];
                                        Column farLeftColumn = null;
                                        int farLeftColumnIndex = -1;

                                        double childWidth = childCell.Column.ActualWidth;
                                        double current = (availableWidth - childWidth - fixedCellWidth);
                                        double percent = 0;

                                        for (int i = reverseChildColumnIndex - 1; i >= 0; i--)
                                        {
                                            farLeftColumn = allVisibleChildColumns[i];
                                            farLeftColumnIndex = i;

                                            if (farLeftColumn.ActualWidth >= current)
                                            {
                                                percent = (farLeftColumn.ActualWidth - current) / farLeftColumn.ActualWidth;
                                                break;
                                            }

                                            current -= farLeftColumn.ActualWidth;
                                        }

                                        if (farLeftColumn != null)
                                        {
                                            this.SetScrollLeft(farLeftColumnIndex, percent);
                                        }
                                    }
                                }
                                else // This is for normal cells.
                                {
                                    GroupColumn groupColumn = c as GroupColumn;

                                    if (groupColumn != null)
                                    {
                                        ChildColumnInfo columnInfo = ResolveChildColumnInfo(groupColumn, percentScroll);
                                        this.SetScrollLeft(allVisibleChildColumns.IndexOf(columnInfo.ChildColumn), columnInfo.ChildColumnScrollPercentage);
                                    }
                                    else
                                    {
                                        this.SetScrollLeft(allVisibleChildColumns.IndexOf(c), percentScroll);
                                    }
                                }
                            }
                        }
                        else //TFS 193094 - in case we weren't doing reverse measure but got into the case where there were still available space on the right and we have rendered few more cells letsupdate the startCell
                        {
                            startCell = currentCell > 0 ? currentCell - 1 : startCell;
                            currentWidth = this.SetupStarCells(availableWidth, currentWidth, ref maxHeight, starColumns);
                        }

                        break;
                    }
                }
            }

            // Reverse rendering never takes into account the FillerCell. So lets add a seperate check and make sure that we render
            // it if its needed.
            if (this.Owner.ReverseCellMeasure)
            {
                // Render StarCells
                // This needs to be done ealier than we previously used it, b/c now it returns a width, b/c
                // StartColumns can effect the scrolling width now, due to the fact that they support MinWidth
                currentWidth = this.SetupStarCells(availableWidth, currentWidth, ref maxHeight, starColumns);
                // PK 4/24/14 tfs166027: render reversed when there are more star columns than could be positioned at the available width
                if (this.Row.IsHorizontallyAlignedBy && currentWidth >= availableWidth)
                {
                    if (this.ArrangeRaised == false && this.Row.ShouldInvalidateHorizontalScroll)
                    {
                        this.Row.Manager.OverrideHorizontalMax = -1;
                        this.InvalidateHorizontalMax(availableWidth - (fixedCellWidth + indentation), ref maxHeight, isInfinite);
                    }

                    // This is for GroupCells
                    if (this.Owner.ReverseChildColumn != null && this.Owner.ReverseChildColumn.ParentColumn != null && startCell == this.Owner.ReverCellStartIndex)
                    {
                        if (!double.IsPositiveInfinity(availableWidth))
                        {
                            int reverseChildColumnIndex = allVisibleChildColumns.IndexOf(this.Owner.ReverseChildColumn);
                            CellBase childCell = this.Row.Cells[this.Owner.ReverseChildColumn];
                            Column farLeftColumn = null;
                            int farLeftColumnIndex = -1;

                            double childWidth = childCell.Column.ActualWidth;
                            double current = (availableWidth - childWidth - fixedCellWidth);
                            double percent = 0;

                            for (int i = reverseChildColumnIndex - 1; i >= 0; i--)
                            {
                                farLeftColumn = allVisibleChildColumns[i];
                                farLeftColumnIndex = i;

                                if (farLeftColumn.ActualWidth >= current)
                                {
                                    percent = (farLeftColumn.ActualWidth - current) / farLeftColumn.ActualWidth;
                                    break;
                                }

                                current -= farLeftColumn.ActualWidth;
                            }

                            if (farLeftColumn != null)
                            {
                                this.SetScrollLeft(farLeftColumnIndex, percent);
                            }
                        }
                    }
                    else // This is for normal cells.
                    {
                        Column startColumn = visibleColumns[startCell];
                        int startColumnIndex = allVisibleChildColumns.IndexOf(startColumn);
                        Column farLeftColumn = null;
                        int farLeftColumnIndex = -1;

                        double width = startColumn.ActualWidth;
                        double current = (availableWidth - width - fixedCellWidth - indentation);
                        double percent = 0;

                        for (int i = startColumnIndex - 1; i >= 0; i--)
                        {
                            farLeftColumn = allVisibleChildColumns[i];
                            farLeftColumnIndex = i;

                            if (farLeftColumn.ActualWidth >= current)
                            {
                                percent = (farLeftColumn.ActualWidth - current) / farLeftColumn.ActualWidth;
                                break;
                            }

                            current -= farLeftColumn.ActualWidth;
                        }

                        if (farLeftColumn != null)
                        {
                            GroupColumn groupColumn = startColumn as GroupColumn;
                            if (groupColumn != null)
                            {
                                ChildColumnInfo columnInfo = ResolveChildColumnInfo(groupColumn, percent);
                                this.SetScrollLeft(farLeftColumnIndex, columnInfo.ChildColumnScrollPercentage);
                            }
                            else
                            {
                                this.SetScrollLeft(farLeftColumnIndex, percent);
                            }
                        }
                    }
                }

                if (currentWidth < availableWidth && visibleColumns.Contains(this.Row.Columns.FillerColumn))
                {
                    this.RenderCell(this.Row.Columns.FillerColumn, starColumns, ref maxHeight, false, this.VisibleCells, isInfinite);
                }
            }

            // So, now we're storing the overflow adjustment on the rows manager. 
            // This will ensure that they're the same for ever row on a band, so that the header is never off
            // when doing auto calculations. 
            manager.OverflowAdjustment = Math.Max(manager.OverflowAdjustment, (currentWidth - availableWidth));

            // don't count the FillerColumn.
            if (isInfinite)
                colCount--;

            manager.ScrollableCellCount = colCount;

            manager.VisibleCellCount = this._visibleCells.Count;
            manager.CurrentVisibleWidth = currentWidth - indentation - fixedCellWidth;

            manager.IndexOfFirstColumnRendered = startCell + 1;

            manager.TotalColumnsRendered = this.VisibleCells.Count;
            manager.RowWidth = currentWidth;

            // Calculate the width of all visible columns so the max value for horizontal scrolling can be determined
            // according to the size of the viewport
            double availWidth = availableWidth - (fixedCellWidth + indentation);
            // the OverrideHorizontalMax is already calculated -> skip creating/releasing cells overhead of this calculation
            if (this._prevAvailableWidth != availWidth || this.Row.Manager.OverrideHorizontalMax == -1)
                this.InvalidateHorizontalMax(availWidth, ref maxHeight, isInfinite);

            return new Size(currentWidth, maxHeight);
        }

        #endregion // RenderCellsComplex

        #region RenderCells

        /// <summary>
        /// Lays out which cells will be displayed in the given viewport. 
        /// </summary>
        /// <param propertyName="availableWidth">The total width that the cells have to work with.</param>
        protected virtual Size RenderCells(double availableWidth)
        {
            RowsManagerBase manager = this.Row.Manager;
            Size returnSize;
            if (manager.IsFirstRowRenderingInThisLayoutCycle || this.Owner.ReverseCellMeasure)
            {
                // In case the view size has changed invalidate the horizontal scroller as it's value is used in measure and arrange.
                // If the horizontal scrollbar value is incorrect the rendered columns also will be incorrect.
                if (availableWidth != this._scrollInfo.HorizontalScrollBar.ViewportSize)
                {
                    double maxHeight = 0;
                    bool isInfinite = double.IsPositiveInfinity(availableWidth);
                    this.InvalidateHorizontalMax(availableWidth, ref maxHeight, isInfinite);
                }

                returnSize = this.RenderCellsComplex(availableWidth);
                manager.IsFirstRowRenderingInThisLayoutCycle = false;
            }
            else
            {
                // AS 2/10/14 TFS162859
                // We should proactively release scrollable cells so that we have them available for use when we 
                // go to attach new cells that are now in view.
                //
                if (this.Row.ColumnLayout.Grid.CellControlGenerationModeInternal == CellControlGenerationMode.Recycle)
                {
                    for (int i = _previousCells.Count - 1; i >= 0; i--)
                    {
                        var cell = _previousCells[i];
                        var visIndex = cell.Column.VisibleColumnIndex;

                        if (visIndex >= 0 &&
                            (visIndex < manager.IndexOfFirstColumnRendered ||
                            visIndex >= manager.IndexOfFirstColumnRendered + manager.TotalColumnsRendered))
                        {
                            this.ReleaseCell(cell);
                        }
                    }
                }

                if (manager.InvalidateOverrideHorizontalMax)
                {
                    manager.InvalidateOverrideHorizontalMax = false;
                    manager.OverrideHorizontalMax = -1;
                }

                bool isInfinite = double.IsPositiveInfinity(availableWidth);

                ReadOnlyCollection<Column> visibleColumns = this.Row.Columns.VisibleColumns;
                double maxHeight = 0;
                Collection<CellBase> starColumns = new Collection<CellBase>();

                // If we are measured with inf width, and the visible columns doesn't contain teh FillerColumn,
                // we'll add it to the list of visible columns so we can add it to the panel. The size of
                // the filler will be determined during arrange.
                if (isInfinite && !visibleColumns.Any(i => i is FillerColumn))
                {
                    List<Column> columns = new List<Column>(visibleColumns);
                    columns.Add(this.Row.Columns.FillerColumn);
                    visibleColumns = new ReadOnlyCollection<Column>(columns);
                }

                double width = manager.CachedIndentation;

                // Render Fixed Adorner Columns First. 
                ReadOnlyCollection<Column> fixedColumns = this.Row.Columns.FixedAdornerColumns;
                foreach (Column col in fixedColumns)
                {
                    double currentActualWidth = col.ActualWidth;

                    this.RenderCell(col, starColumns, ref maxHeight, false, this.VisibleFixedLeftCells, isInfinite);

                    if (currentActualWidth != col.ActualWidth)
                    {
                        this.VisibleFixedLeftCells.Clear();
                        return this.RenderCellsComplex(availableWidth);
                    }

                    if (col.WidthResolved.WidthType != ColumnWidthType.Star || isInfinite)
                        width += currentActualWidth;
                }

                bool visibleFixedColumn = false;

                // Place MergedColumns to the Left of the FixedDataColumns.
                if (this.Row.ColumnLayout.Grid.GroupBySettings.GroupByOperation == GroupByOperation.MergeCells)
                {
                    ReadOnlyCollection<Column> mergedColumns = this.Row.Columns.GroupByColumns[this.Row.ColumnLayout];
                    foreach (Column col in mergedColumns)
                    {
                        if (col.Visibility == Visibility.Visible)
                        {
                            visibleFixedColumn = true;

                            double currentActualWidth = col.ActualWidth;

                            this.RenderCell(col, starColumns, ref maxHeight, false, this.VisibleFixedLeftCells, isInfinite);

                            if (currentActualWidth != col.ActualWidth)
                            {
                                this.VisibleFixedLeftCells.Clear();
                                return this.RenderCellsComplex(availableWidth);
                            }

                            if (col.WidthResolved.WidthType != ColumnWidthType.Star || isInfinite)
                                width += currentActualWidth;
                        }
                    }
                }

                // Next Render Fixed Data Columns
                FixedColumnsCollection fixedDataColumns = this.Row.Columns.FixedColumnsLeft;
                if (fixedDataColumns.Count > 0)
                {

                    foreach (Column col in fixedDataColumns)
                    {
                        if (col.Visibility == Visibility.Visible)
                        {
                            double currentActualWidth = col.ActualWidth;

                            visibleFixedColumn = true;
                            this.RenderCell(col, starColumns, ref maxHeight, false, this.VisibleFixedLeftCells, isInfinite);

                            if (currentActualWidth != col.ActualWidth)
                            {
                                this.ReleaseCells(this.VisibleFixedLeftCells);
                                return this.RenderCellsComplex(availableWidth);
                            }

                            if (col.WidthResolved.WidthType != ColumnWidthType.Star || isInfinite)
                                width += currentActualWidth;
                        }
                    }
                }

                if (visibleFixedColumn)
                {
                    // Reset
                    this.Row.Columns.FixedBorderColumnLeft.ActualWidth = 0;

                    width += this.RenderCell(this.Row.Columns.FixedBorderColumnLeft, starColumns, ref maxHeight, false, this._additionalCells, isInfinite);
                }

                fixedDataColumns = this.Row.Columns.FixedColumnsRight;
                if (fixedDataColumns.Count > 0)
                {
                    visibleFixedColumn = false;
                    foreach (Column col in fixedDataColumns)
                    {
                        if (col.Visibility == Visibility.Visible)
                        {
                            double currentActualWidth = col.ActualWidth;

                            visibleFixedColumn = true;

                            this.RenderCell(col, starColumns, ref maxHeight, false, this.VisibleFixedRightCells, isInfinite);

                            if (currentActualWidth != col.ActualWidth)
                            {
                                this.ReleaseCells(this.VisibleFixedLeftCells);
                                this.ReleaseCells(this.VisibleFixedRightCells);
                                return this.RenderCellsComplex(availableWidth);
                            }

                            if (col.WidthResolved.WidthType != ColumnWidthType.Star || isInfinite)
                                width += currentActualWidth;
                        }
                    }

                    if (visibleFixedColumn)
                    {
                        // Reset
                        this.Row.Columns.FixedBorderColumnRight.ActualWidth = 0;

                        width += this.RenderCell(this.Row.Columns.FixedBorderColumnRight, starColumns, ref maxHeight, false, this._additionalCells, isInfinite);
                    }
                }

                double scrollLeft = this.ResolveScrollLeft();

                if (scrollLeft == -1)
                {
                    // This must be the first time we're rendering a row on this level, however, we are already horizontal scrolled
                    // so lets resolve our HorizontalMax, and try again. 
                    if (this.Row.RowType == RowType.DataRow)
                    {
                        this.InvalidateHorizontalMax(availableWidth - width, ref maxHeight, isInfinite);
                        scrollLeft = this.ResolveScrollLeft();
                        this.Owner.MeasureCalled = false;
                        this.Owner.InvalidateMeasureInternal();
                    }
                }

                int count = manager.TotalColumnsRendered + manager.IndexOfFirstColumnRendered;

                if (count > visibleColumns.Count)
                    count = visibleColumns.Count;

                bool isConditionalFormatting = manager.ColumnLayout != null && manager.ColumnLayout.Grid != null && manager.ColumnLayout.Grid.ConditionalFormattingSettings.AllowConditionalFormatting;

                for (int i = manager.IndexOfFirstColumnRendered; i < count; i++)
                {
                    Column col = visibleColumns[i];

                    double currentActualWidth = col.ActualWidth;

                    this.RenderCell(col, starColumns, ref maxHeight, false, this.VisibleCells, isInfinite);

                    // A.I: Fixing issue 160131





                    bool checkFillerColumn = true;


                    if (currentActualWidth != col.ActualWidth && checkFillerColumn)
                    {
                        this.ReleaseCells(this.VisibleFixedLeftCells);
                        this.ReleaseCells(this.VisibleFixedRightCells);
                        this.ReleaseCells(this.VisibleCells);

                        if (isConditionalFormatting)
                        {
                            this.DataContext = null;
                            this.DataContext = this.Row.Data;
                        }

                        return this.RenderCellsComplex(availableWidth);
                    }

                    if (col.WidthResolved.WidthType != ColumnWidthType.Star)
                        width += currentActualWidth;
                }

                manager.RowWidth = width;

                // Render StarCells
                this.SetupStarCells(availableWidth, manager.RowWidth, ref maxHeight, starColumns);

                returnSize = new Size(manager.RowWidth, maxHeight);
            }

            return returnSize;
        }

        #endregion // RenderCells			

        #region RenderCell

        /// <summary>
        /// Displays the <see cref="CellBase"/> for the specified <see cref="ColumnBase"/>.
        /// </summary>
        /// <param name="column">The <see cref="Column"/></param>
        /// <param name="starColumns">A list of cells that have a width of type star.</param>
        /// <param name="maxHeight">The height of the largest cell, if the cell's height that is being rendered is larger, maxHeight should be adjusted.</param>
        /// <param name="insert">Whether or not the cell should be added, or inserted at the first position of the specified visible cells.</param>
        /// <param name="visibleCells">The collection of cells that rendered cell should be added to.</param>
        /// <param name="isInfinite">Lets the method know if the available width is infinite.</param>
        /// <returns>The width that that <see cref="CellBase"/> is consuming.</returns>
        protected virtual double RenderCell(Column column, Collection<CellBase> starColumns, ref double maxHeight, bool insert, Collection<CellBase> visibleCells, bool isInfinite)
        {
            return this.RenderCell(column, starColumns, ref maxHeight, insert, visibleCells, isInfinite, false);
        }

        /// <summary>
        /// Displays the <see cref="CellBase"/> for the specified <see cref="ColumnBase"/>.
        /// </summary>
        /// <param name="column">The <see cref="Column"/></param>
        /// <param name="starColumns">A list of cells that have a width of type star.</param>
        /// <param name="maxHeight">The height of the largest cell, if the cell's height that is being rendered is larger, maxHeight should be adjusted.</param>
        /// <param name="insert">Whether or not the cell should be added, or inserted at the first position of the specified visible cells.</param>
        /// <param name="visibleCells">The collection of cells that rendered cell should be added to.</param>
        /// <param name="isInfinite">Lets the method know if the available width is infinite.</param>
        /// <param name="suppressCellControlAttached">Whether or not the CellControlAttached event should be fired when the CellControl is attached.</param>
        /// <returns>The width that that <see cref="CellBase"/> is consuming.</returns>
        protected virtual double RenderCell(Column column, Collection<CellBase> starColumns, ref double maxHeight, bool insert, Collection<CellBase> visibleCells, bool isInfinite, bool suppressCellControlAttached)
        {
            double prevActualWidth = column.ActualWidth;

            

            ColumnWidth colWidth = column.WidthResolved;
            ColumnWidthType widthType = colWidth.WidthType;

            double widthToMeasure = double.PositiveInfinity;

            if (widthType == ColumnWidthType.Numeric)
                widthToMeasure = colWidth.Value;
            else if (isInfinite && widthType == ColumnWidthType.Star)
                widthType = ColumnWidthType.Auto;
            else if (widthType == ColumnWidthType.InitialAuto && column.IsInitialAutoSet && column.ActualWidth != 0)
                widthToMeasure = column.ActualWidth;

            // widthToMeasure should be in the interval [column.MinimumWidth; column.MaximumWidth]
            if (!double.IsInfinity(widthToMeasure))
            {
                widthToMeasure = Math.Max(column.MinimumWidth, Math.Min(column.MaximumWidth, widthToMeasure));
            }

            

            double heightToMeasure = double.PositiveInfinity;
            RowHeight rowHeight = this.Row.HeightResolved;
            if (rowHeight.HeightType == RowHeightType.Numeric)
                heightToMeasure = rowHeight.Value;

            if (column.Visibility == Visibility.Collapsed)
                return 0;

            
            
            

            CellBase cell = this.Row.ResolveCell(column);
            if (cell != null)
            {
                if (!insert)
                    visibleCells.Add(cell);
                else
                    visibleCells.Insert(0, cell);

                Size sizeToMeasure = Size.Empty;

                bool manuallyInvokeMeasure = false;
                bool forceMeasureInvalidation = cell is GroupCell && this.Owner.ShouldInvalidateGroupCellMeasure;

                if (cell.Control == null)
                {
                    cell.SuppressCellControlAttached = suppressCellControlAttached;
                    bool isNew = RecyclingManager.Manager.AttachElement(cell, this);
                    cell.Control.EnsureContent();
                    cell.EnsureCurrentState();
                    forceMeasureInvalidation |= (!isNew && cell is GroupCell); // force measure if we are re-attaching a group cell



#region Infragistics Source Cleanup (Region)







#endregion // Infragistics Source Cleanup (Region)


                    cell.MeasuringSize = Size.Empty;
                    sizeToMeasure = new Size(widthToMeasure, heightToMeasure);
                }
                else
                {
                    cell.ApplyStyle();
                    cell.Control.EnsureContent();
                    cell.EnsureCurrentState();

                    if (cell.MeasuringSize.IsEmpty)
                        sizeToMeasure = new Size(widthToMeasure, heightToMeasure);
                    else
                        sizeToMeasure = cell.MeasuringSize;

                    manuallyInvokeMeasure = true;
                }

                if (forceMeasureInvalidation)
                {
                    cell.Control.Measure(new Size(1, 1));
                }

                if (widthType != ColumnWidthType.Star)
                {
                    cell.Control.MeasureRaised = false;
                    cell.Control.Measure(sizeToMeasure);

                    // Check to see that cell was actually measured.
                    // If measure wasn't called, that's our first clue, as this cell was just attached. 
                    if (!cell.Control.MeasureRaised)
                    {
                        // If it's content is smaller than the desired size, thats our second clue.
                        FrameworkElement elem = cell.Control.Content as FrameworkElement;

                        // In some cases, depeneding on what type of element we're dealing with, the elements don't always invalidate properly
                        // So i added another case, which is kind of random, but if the width of the element is less than 10, then we're going to invalidate it.
                        if (elem != null && ((elem.DesiredSize.Width > cell.Control.DesiredSize.Width) || elem.DesiredSize.Width < 10))
                        {
                            // So, set an invalid size first
                            cell.Control.Measure(new Size(1, 1));

                            // Then reapply the valid size, this will ensure that measure is called. 
                            cell.Control.Measure(sizeToMeasure);
                        }

                        if (manuallyInvokeMeasure)
                            cell.Control.ManuallyInvokeMeasure(sizeToMeasure);
                    }
                }

                CellControlBase control = cell.Control;
                double width = control.DesiredSize.Width;



#region Infragistics Source Cleanup (Region)




#endregion // Infragistics Source Cleanup (Region)


                // Added some Validation for Columns what support Overflow tooltips
                if (column.AllowToolTips == AllowToolTips.Overflow)
                {
                    CellControl cc = control as CellControl;
                    // Only supported on Cells
                    if (cc != null)
                    {
                        // Make sure we didn't alreayd measure agains it's max
                        if ((widthToMeasure != double.PositiveInfinity || heightToMeasure != double.PositiveInfinity) && cell.MeasuringSize.IsEmpty)
                        {
                            // Measure against it's max
                            control.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));

                            // Store off value
                            double desiredWidth = control.DesiredSize.Width;

                            // Measure against its reality
                            control.Measure(new Size(widthToMeasure, heightToMeasure));

                            // Compare
                            if (desiredWidth > width && (double.IsPositiveInfinity(widthToMeasure) || widthToMeasure < desiredWidth))
                            {
                                cc.ShowToolTip();
                            }
                            else
                            {
                                cc.HideToolTip();
                            }
                        }
                    }

                }

                if (isInfinite && widthType == ColumnWidthType.Star)
                {
                    widthType = ColumnWidthType.Auto;
                    if (width < control.ActualWidth)
                        width = control.ActualWidth;
                }

                // Let's set the width of the FillerColumn to 0 for now,
                // we'll make sure to size it propertly in arrange
                if (isInfinite && column is FillerColumn)
                {
                    width = 0;
                    column.ActualWidth = width;
                }

                switch (widthType)
                {
                    case ColumnWidthType.InitialAuto:

                        if (column.ActualWidth == 0)
                            column.IsInitialAutoSet = false;

                        if (!column.IsInitialAutoSet && column.ActualWidth < width)
                            column.ActualWidth = width;
                        break;

                    case ColumnWidthType.Auto:

                        if (column.ActualWidth < width)
                            column.ActualWidth = width;
                        break;

                    case ColumnWidthType.Numeric:

                        column.ActualWidth = cell.Column.WidthResolved.Value;

                        break;

                    case ColumnWidthType.SizeToHeader:

                        if (this.Row.RowType == RowType.HeaderRow)
                        {
                            if (column.ActualWidth < width)
                                column.ActualWidth = width;
                        }

                        break;

                    case ColumnWidthType.SizeToCells:

                        if (!(this.Row.RowType == RowType.HeaderRow) && !(this.Row.RowType == RowType.FooterRow))
                        {
                            if (column.ActualWidth < width)
                                column.ActualWidth = width;
                        }

                        break;

                    case ColumnWidthType.Star:

                        if (starColumns != null)
                            starColumns.Add(cell);

                        return 0;
                }

                if (column.ActualWidth < column.MinimumWidth)
                    column.ActualWidth = column.MinimumWidth;

                if (column.ActualWidth > column.MaximumWidth)
                    column.ActualWidth = column.MaximumWidth;

                // So the Width of a Column has changed...better make sure that the HorizontalMax is still valid. 
                if (prevActualWidth != column.ActualWidth)
                {
                    if (!(column is FixedBorderColumn))
                        this.Row.Manager.OverrideHorizontalMax = -1;
                }

                // calculate the average width of all rendered cells
                // it will be used for cells which are not yet rendered when measuring the scrollbar max
                this.SetAverageColumnWidth(column);

                maxHeight = Math.Max(maxHeight, cell.Control.DesiredSize.Height);
                return column.ActualWidth;
            }

            return 0;
        }

        #endregion // RenderCell		

        #region OnCellMouseOver


#region Infragistics Source Cleanup (Region)




#endregion // Infragistics Source Cleanup (Region)

        /// <summary>
        /// Called when a <see cref="CellBase"/> is moused over.
        /// All Cells of the <see cref="RowBase"/> will then go to it's "MouseOver" VisualState.
        /// </summary>
        /// <param name="cell">The cell that was moused over.</param>
        protected virtual void OnCellMouseOver(CellBase cell)

        {
            ColumnLayout colLayout = cell.Row.ColumnLayout;

            //Determine if we have Hover Set for AllowEditing
            //TFS178207 - use colLayout.EditingSettings.AllowEditingResolved so we could get the value of the setting in the current layout.
            bool isHover = colLayout.EditingSettings.AllowEditingResolved == EditingType.Hover;

            if (this.Row.ResolveRowHover == RowHoverType.Row)
            {
                foreach (CellBase visibleCell in this._visibleFixedLeftCells)
                    visibleCell.EnsureCurrentState();

                foreach (CellBase visibleCell in this._visibleCells)
                    visibleCell.EnsureCurrentState();

                foreach (CellBase visibleCell in this._visibleFixedRightCells)
                    visibleCell.EnsureCurrentState();

                



                if (isHover && !this._isEnteringEditMode)
                {
                    //If hover is set, we need to see if there is actually an editable cell on this row.
                    Row row = this.Row as Row;

                    if (row != null)
                    {
                        CellBase editableCell = cell;
                        if (!cell.IsEditable)
                            editableCell = row.VisibleCells.Where(c => c.IsEditable).FirstOrDefault();

                        if (editableCell != null)
                        {
                            this._isEnteringEditMode = true;
                            colLayout.Grid.EnterEditMode(row, editableCell);
                            this._isEnteringEditMode = false;
                            editableCell.Control.ContentProvider.FocusEditor();
                        }

                    }
                }
            }
            else if (this.Row.ResolveRowHover == RowHoverType.Cell)
            {
                cell.EnsureCurrentState();
                if (isHover)
                    colLayout.Grid.EnterEditMode(cell);
            }
        }

        #endregion // OnCellMouseOver

        #region OnCellMouseLeave


#region Infragistics Source Cleanup (Region)




#endregion // Infragistics Source Cleanup (Region)

        /// <summary>
        /// Called when the mouse leaves a <see cref="CellBase"/>.
        /// All Cells of the <see cref="RowBase"/> will then go to it's "Normal" VisualState.
        /// </summary>
        /// <param name="cell">The cell that was moused over.</param>
        /// <param name="newCell">The new cell that is moused over.</param>
        protected virtual void OnCellMouseLeave(CellBase cell, CellBase newCell)

        {
            ColumnLayout colLayout = cell.Row.ColumnLayout;

            bool isHover = colLayout != null && colLayout.Grid.EditingSettings.AllowEditing == EditingType.Hover;

            if (this.Row.ResolveRowHover == RowHoverType.Row)
            {
                foreach (CellBase visibleCell in this._visibleFixedLeftCells)
                    visibleCell.EnsureCurrentState();

                foreach (CellBase visibleCell in this._visibleCells)
                    visibleCell.EnsureCurrentState();

                foreach (CellBase visibleCell in this._visibleFixedRightCells)
                    visibleCell.EnsureCurrentState();
            }
            else
                cell.EnsureCurrentState();

            


            if (isHover && colLayout.Grid.RowHover == RowHoverType.Cell && newCell != null)
            {
                colLayout.Grid.ExitEditModeInternal(false);
            }
        }

        #endregion // OnCellMouseLeave

        #region SetupStarCells

        /// <summary>
        /// Loops through the start columns and updates their width appropriately. 
        /// </summary>
        /// <param propertyName="availableWidth"></param>
        /// <param propertyName="currentWidth"></param>
        /// <param propertyName="maxHeight"></param>
        /// <param propertyName="starColumns"></param>
        protected double SetupStarCells(double availableWidth, double currentWidth, ref double maxHeight, Collection<CellBase> starColumns)
        {
            // Now that we've rendered all the cells, lets see if any of the cells are of type 
            // Star, and allow them to fill the remaining space.
            double remainingWidth = availableWidth - currentWidth;
            double usedWidth = remainingWidth;
            double remainder = 0;
            if (remainingWidth > 0)
            {
                if (starColumns.Count > 0)
                {
                    double divider = 0;

                    // Calculate the divider
                    foreach (CellBase cell in starColumns)
                    {
                        divider += cell.Column.WidthResolved.Value;
                    }

                    if (divider != 0)
                    {
                        double individualWidth = remainingWidth / divider;

                        bool reEvaluate = false;

                        List<CellBase> actualStarCols = new List<CellBase>(starColumns);


                        do
                        {
                            reEvaluate = false;
                            List<CellBase> newList = new List<CellBase>();

                            // Walk through all the start cells and evaluate whether they will participate in star sizing. 
                            // If the their min width is larger than the available width alloted to it, then it will not particapte
                            // and the remaining width and divider need to be updated appropriately. 
                            // We need to do this in side a while stmt, so that we account for the new remaining width if it changes
                            foreach (CellBase cell in actualStarCols)
                            {
                                double requestedValue = (int)individualWidth * cell.Column.WidthResolved.Value;
                                if (requestedValue < cell.Column.MinimumWidth)
                                {
                                    remainingWidth -= cell.Column.MinimumWidth;
                                    divider -= cell.Column.WidthResolved.Value;
                                    cell.Column.ActualWidth = cell.Column.MinimumWidth;

                                    if (!this.Row.Manager.StarColumnWidths.ContainsKey(cell.Column))
                                        this.Row.Manager.StarColumnWidths.Add(cell.Column, cell.Column.MinimumWidth);
                                    else
                                        this.Row.Manager.StarColumnWidths[cell.Column] = cell.Column.MinimumWidth;

                                    reEvaluate = true;
                                }
                                else
                                    newList.Add(cell);
                            }

                            actualStarCols = newList;
                            individualWidth = remainingWidth / divider;

                        } while (reEvaluate);

                        if (remainingWidth > 0)
                        {
                            if (actualStarCols.Count > 0)
                            {
                                foreach (CellBase cell in actualStarCols)
                                {
                                    // We don't want to add decimal values to the widths as it can mess with the way the cells look
                                    // So, let's store the remainder.
                                    cell.Column.ActualWidth = (int)individualWidth * cell.Column.WidthResolved.Value;

                                    if (!this.Row.Manager.StarColumnWidths.ContainsKey(cell.Column))
                                        this.Row.Manager.StarColumnWidths.Add(cell.Column, cell.Column.ActualWidth);
                                    else
                                        this.Row.Manager.StarColumnWidths[cell.Column] = cell.Column.ActualWidth;

                                    remainder += ((individualWidth * cell.Column.WidthResolved.Value) - cell.Column.ActualWidth);
                                }

                                if (remainder > 0)
                                {
                                    double perColumn = Math.Floor(remainder / actualStarCols.Count);
                                    remainder = remainder - actualStarCols.Count * perColumn;

                                    foreach (var cell in actualStarCols)
                                    {
                                        cell.Column.ActualWidth += perColumn;

                                        if (!this.Row.Manager.StarColumnWidths.ContainsKey(cell.Column))
                                            this.Row.Manager.StarColumnWidths.Add(cell.Column, cell.Column.ActualWidth);
                                        else
                                            this.Row.Manager.StarColumnWidths[cell.Column] = cell.Column.ActualWidth;
                                    }
                                }

                                // We'll add the last few decimal points to the last star column. 
                                actualStarCols[actualStarCols.Count - 1].Column.ActualWidth += remainder;
                            }
                        }
                        else
                        {
                            // After min widths were calculated, we ran out of width, so no star widths
                            usedWidth = 0;
                            foreach (CellBase cell in actualStarCols)
                            {
                                // These min width here is probably 0
                                cell.Column.ActualWidth = cell.Column.MinimumWidth;

                                if (!this.Row.Manager.StarColumnWidths.ContainsKey(cell.Column))
                                    this.Row.Manager.StarColumnWidths.Add(cell.Column, cell.Column.ActualWidth);
                                else
                                    this.Row.Manager.StarColumnWidths[cell.Column] = cell.Column.ActualWidth;
                            }
                        }

                        // Now loop through and make sure these columns are measured correctly, so that all their content is visible.
                        foreach (CellBase cell in starColumns)
                        {
                            double width = 0;
                            if (this.Row.Manager.StarColumnWidths.ContainsKey(cell.Column))
                                width = this.Row.Manager.StarColumnWidths[cell.Column];

                            if (cell.Column.AllowToolTips == AllowToolTips.Overflow)
                            {
                                CellControl cc = cell.Control as CellControl;
                                // Only supported on Cells
                                if (cc != null)
                                {
                                    // Measure against it's max
                                    cc.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));

                                    // Store off value
                                    double desiredWidth = cc.DesiredSize.Width;

                                    // Compare
                                    if (desiredWidth > width)
                                    {
                                        cc.ShowToolTip();
                                    }
                                    else
                                    {
                                        cc.HideToolTip();
                                    }
                                }
                            }

                            cell.Control.MeasureRaised = false;
                            cell.Control.Measure(new Size(width, double.PositiveInfinity));

                            // Check to see that cell was actually measured.
                            // If measure wasn't called, that's our first clue, as this cell was just attached. 
                            if (!cell.Control.MeasureRaised)
                            {
                                // If it's content is smaller than the desired size, thats our second clue.
                                FrameworkElement elem = cell.Control.Content as FrameworkElement;

                                // In some cases, depeneding on what type of element we're dealing with, the elements don't always invalidate properly
                                // So i added another case, which is kind of random, but if the width of the element is less than 10, then we're going to invalidate it.
                                if (elem != null && ((elem.DesiredSize.Width > cell.Control.DesiredSize.Width) || elem.DesiredSize.Width < 10))
                                {
                                    // So, set an invalid size first
                                    cell.Control.Measure(new Size(1, 1));

                                    // Then reapply the valid size, this will ensure that measure is called. 
                                    cell.Control.Measure(new Size(width, double.PositiveInfinity));
                                }
                            }


                            usedWidth -= width;
                            maxHeight = Math.Max(maxHeight, cell.Control.DesiredSize.Height);
                        }
                    }
                }
            }
            else
            {
                // No remaining width?
                // Then we need to set each column's width to the minimum width (Not 0)
                usedWidth = 0;
                foreach (CellBase cell in starColumns)
                {
                    cell.Column.ActualWidth = cell.Column.MinimumWidth;

                    if (cell.Column.AllowToolTips == AllowToolTips.Overflow)
                    {
                        CellControl cc = cell.Control as CellControl;
                        // Only supported on Cells
                        if (cc != null)
                        {
                            // Measure against it's max
                            cc.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));

                            // Store off value
                            double desiredWidth = cc.DesiredSize.Width;

                            // Compare
                            if (desiredWidth > cell.Column.ActualWidth)
                            {
                                cc.ShowToolTip();
                            }
                            else
                            {
                                cc.HideToolTip();
                            }
                        }
                    }


                    if (!this.Row.Manager.StarColumnWidths.ContainsKey(cell.Column))
                        this.Row.Manager.StarColumnWidths.Add(cell.Column, cell.Column.ActualWidth);
                    else
                        this.Row.Manager.StarColumnWidths[cell.Column] = cell.Column.ActualWidth;

                    cell.Control.Measure(new Size(cell.Column.ActualWidth, double.PositiveInfinity));
                    maxHeight = Math.Max(maxHeight, cell.Control.DesiredSize.Height);

                    usedWidth -= cell.Column.ActualWidth;
                }
                this.Row.Columns.FillerColumn.ActualWidth = 0;
            }

            if (double.IsInfinity(usedWidth))
                usedWidth = 0;

            return currentWidth + Math.Abs(usedWidth);
        }

        #endregion // SetupStarCells

        #region SupportsCommand

        /// <summary>
        /// Returns if the object will support a given command type.
        /// </summary>
        /// <param propertyName="command">The command to be validated.</param>
        /// <returns>True if the object recognizes the command as actionable against it.</returns>
        protected virtual bool SupportsCommand(ICommand command)
        {
            return (command is RowCommandBase);
        }
        #endregion // SupportsCommand

        #region  GetParameter
        /// <summary>
        /// Returns the object that defines the parameters necessary to execute the command.
        /// </summary>
        /// <param propertyName="source">The CommandSource object which defines the command to be executed.</param>
        /// <returns>The object necessary for the command to complete.</returns>
        protected virtual object GetParameter(CommandSource source)
        {
            return this.Row;
        }
        #endregion // GetParameter

        #endregion // Protected

        #region Private

        internal void ReleaseCell(CellBase cell)
        {
            if (cell.Control != null && cell.Control.Parent == this)
            {
                if (!RecyclingManager.Manager.ReleaseElement(cell, this))
                    this._nonReleasedCells.Add(cell);
            }
        }

        private void ReleaseCells(Collection<CellBase> cells)
        {
            foreach (CellBase cell in cells)
            {
                this.ReleaseCell(cell);
            }

            cells.Clear();
        }

        /// <summary>
        /// Gets the position of the horizontal scrollbar as a number indicating the first visible column index and column's visible portion
        /// </summary>
        /// <returns>Double number which integral part indicates the index of the first visible column and the fraction part indicates the percentage of the visible part</returns>
        private double ResolveScrollLeft()
        {
            ScrollBar bar = this._scrollInfo.HorizontalScrollBar;

            if (bar != null)
            {
                RowsManagerBase horizontalManager = this.Owner.HorizontalRowsManager;

                if (horizontalManager == null || horizontalManager == this.Row.Manager)
                {
                    return this.GetColumnIndexFromScrollValue();
                }

                if (bar.Value == 0 || bar.Maximum == 0)
                    return 0;

                if (this.Row.Manager.OverrideHorizontalMax == -1)
                    return -1;

                return this.GetColumnIndexFromScrollValue();
            }

            return 0;
        }

        /// <summary>
        /// Gets the horizontal scroll position as persentage
        /// </summary>
        private double GetHorizontalScrollValuePercentage(double originalValue, double barMaximum)
        {
            if (barMaximum == 0)
            {
                return 0;
            }

            return originalValue / barMaximum;
        }


        /// <summary>
        /// Gets the horizontal scrollbar value according to the currently rendered column layout maximal scrollbar value
        /// </summary>
        /// <returns>Scrollbar value relative to the currently rendered column layout maximal scrollbar value</returns>
        private double GetHorizontalScrollValueFromHorizontalMax(double originalValue)
        {
            if (originalValue == 0)
                return 0;

            var horizontalBar = this._scrollInfo.HorizontalScrollBar;
            if (this.Row.Manager.OverrideHorizontalMax == -1 || horizontalBar.Maximum == this.Row.Manager.OverrideHorizontalMax)
            {
                // Return the current scrollbar value without any adjustments because we are rendering the row,
                // which was used to calculate the horizontal scrollbar maximum
                return originalValue;
            }

            var scrollValuePercentage = this.GetHorizontalScrollValuePercentage(originalValue, horizontalBar.Maximum);

            // We need to adjust the horizontal scrollbar value according to the calculated horizontal max value
            // for the currently rendered column layout because the max value could be different for each column layout
            return scrollValuePercentage * this.Row.Manager.OverrideHorizontalMax;
        }

        /// <summary>
        /// Gets the position of the horizontal scrollbar relative to the visible column
        /// </summary>
        /// <returns>Integral part of the returned value represents the index of the column and the fraction represents the percentage of the column's visible part</returns>
        private double GetColumnIndexFromScrollValue()
        {
            var scrollPos = this.GetHorizontalScrollValueFromHorizontalMax(this.HorizontalScrollValueSafe);
            if (scrollPos == 0)
                return 0;

            ColumnBaseCollection columns = this.Row.Columns;

            double currentWidth = 0;
            double columnWidth = 0;
            for (int i = 0; i < columns.VisibleColumns.Count; i++)
            {
                var column = columns.VisibleColumns[i];

                columnWidth = this.GetColumnWidthForHorizontalScrolling(column);

                // find the column which contains the current scrollbar position
                if (currentWidth + columnWidth > scrollPos)
                {
                    // calculate how much of the column is currently visible
                    var columnVisiblePart = scrollPos - currentWidth;
                    var fraction = columnVisiblePart / columnWidth;

                    var groupColumn = column as GroupColumn;
                    if (groupColumn != null)
                    {
                        // this is a group column so find which one of it's children contains the scrollbar position
                        var colInfo = this.ResolveChildColumnInfo(groupColumn, fraction);

                        // TFS 208146 - In some cases (when we hide child columns for example) child scroll info could not be determined at this point.
                        if (colInfo == null)
                            return i;

                        var colRealIndex = columns.AllVisibleChildColumns.IndexOf(colInfo.ChildColumn);

                        // return double consisting of real index of the column and the percentage of it's visible portion
                        return colRealIndex + colInfo.ChildColumnScrollPercentage;
                    }
                    else
                    {
                        // TFS 121178 - use the real column index when calculating the horizontal scroll value even this is not a group column.
                        // There might be a group columns before that and is such cases the index would be incorrect.
                        int colRealIndex = columns.AllVisibleChildColumns.IndexOf(column);
                        // return double consisting of index of the column and the percentage of it's visible portion
                        return colRealIndex + fraction;
                    }
                }

                currentWidth += columnWidth;
            }

            return this.Row.Columns.AllVisibleChildColumns.Count - 1;
        }

        /// <summary>
        /// Gets the scroll value calculated from a given scroll percentage.
        /// </summary>
        /// <param name="scrollColumnIndex">Decimal part of represents the index of the column and the fraction represents the percentage of the column's visible part</param>
        /// <returns>The value of scroll bar</returns>
        private double GetScrollValueFromColumnIndex(double scrollColumnIndex)
        {
            var scrollBar = this._scrollInfo.HorizontalScrollBar;
            if (scrollBar == null || scrollBar.Maximum == 0)
                return 0;

            var columnIndex = (int)scrollColumnIndex;
            var scrollPercent = scrollColumnIndex - columnIndex;

            if (columnIndex < 0 || columnIndex >= this.Row.Columns.AllVisibleChildColumns.Count)
            {
                throw new ArgumentException("scrollColumnIndex");
            }

            // calculate the actual size of the columns up to the given column index
            double currentWidth = this.Row.Columns.AllVisibleChildColumns.Take(columnIndex).Sum(x => x.ActualWidth);
            var partialyVisibleColumn = this.Row.Columns.AllVisibleChildColumns[columnIndex];
            currentWidth += partialyVisibleColumn.ActualWidth * scrollPercent;

            return currentWidth;
        }

        /// <summary>
        /// Gets the index of the last column visible in view.
        /// </summary>
        /// <param name="allVisibleColumns">The list of all columns to be used in calculation.</param>
        /// <param name="scrollPosition">Number which integral part points to the index of the first column in view and the fraction part shows how much of the column should be visible (in percentage).</param>
        /// <param name="availableWidth">Available width in the view.</param>
        /// <returns>The index of the last column which fits (even partially) in the desired view size.</returns>
        private int GetIndexOfLastColumnInDesiredView(ReadOnlyCollection<Column> allVisibleColumns, double scrollPosition, double availableWidth)
        {
            if (!allVisibleColumns.Any())
            {
                throw new ArgumentException("visibleColumns");
            }

            var columnIndex = (int)scrollPosition;
            var columnVisiblePercentage = scrollPosition - columnIndex;

            if (columnIndex < 0 || columnIndex >= allVisibleColumns.Count)
            {
                throw new ArgumentException("scrollPosition");
            }

            // Width of the first partially visible column
            var width = allVisibleColumns[columnIndex].ActualWidth * columnVisiblePercentage;

            // cycle through the rest of the visible columns to find the index of the last column fitting in the available space
            while (width < availableWidth && columnIndex < allVisibleColumns.Count - 1)
            {
                columnIndex++;
                width += allVisibleColumns[columnIndex].ActualWidth;
            }

            return columnIndex;
        }



#region Infragistics Source Cleanup (Region)




#endregion // Infragistics Source Cleanup (Region)

        internal void SetScrollLeft(double scrollLeft, double percent = 0)
        {
            //TFS 206805 = prevent erroneous scroll percentages from being passed through
            if (percent > 1)
            {
                percent = 1;
            }
            scrollLeft = scrollLeft + percent;

            ScrollBar bar = this._scrollInfo.HorizontalScrollBar;

            if (bar != null && this.Row.Manager.ScrollableCellCount - 1 > 0)
            {
                double value = 0;
                RowsManagerBase horizontalManager = this.Owner.HorizontalRowsManager;
                if (horizontalManager == null || horizontalManager == this.Row.Manager)
                {
                    value = this.GetScrollValueFromColumnIndex(scrollLeft);
                }
                else
                {
                    // PK tfs172998: update the maximum of the scrollbar, if set, so the value properly matches the desired scroll
                    if (horizontalManager.OverrideHorizontalMax != -1)
                        bar.Maximum = horizontalManager.OverrideHorizontalMax;

                    var scrollValue = this.GetScrollValueFromColumnIndex(scrollLeft);

                    // Calibrate the scroll value so it corresponds to the current OverrideHorizontalMax
                    // because the scrollbar maximum could be set by a different column layout
                    value = (scrollValue / this.Row.Manager.OverrideHorizontalMax) * bar.Maximum;
                }

                if (double.IsNaN(value) || double.IsInfinity(value))
                    value = 0;

                bar.Value = value;
            }
        }

        private ChildColumnInfo ResolveChildColumnInfo(GroupColumn groupColumn, double percent)
        {
            if (!(percent >= 0 && percent <= 1))
            {
                throw new ArgumentException("percent");
            }
            ReadOnlyKeyedColumnBaseCollection<Column> childColumns = groupColumn.AllVisibleChildColumns;

            double totalWidth = groupColumn.ActualWidth;
            double left = 0;
            ChildColumnInfo info = null;

            VerifyColumnsActualWidths(groupColumn);

            // If the group column is still not rendered and doesn't have ActualWidth use its childer widths as total width
            if (totalWidth == 0)
            {
                totalWidth = childColumns.Sum(x => x.ActualWidth);
            }

            for (int i = 0; i < childColumns.Count; i++)
            {
                Column childColumn = childColumns[i];

                double right = left + childColumn.ActualWidth;

                if (left / totalWidth <= percent && percent <= right / totalWidth)
                {
                    double columnScrollPercentage = ((percent * totalWidth) - left) / childColumn.ActualWidth;

                    info = new ChildColumnInfo(childColumn, columnScrollPercentage, i);
                    break;
                }

                left = right;
            }

            return info;
        }

        /// <summary>
        /// Verifies the actual width of the columns.
        /// </summary>
        /// <param name="groupColumn">The group column.</param>
        private void VerifyColumnsActualWidths(GroupColumn groupColumn)
        {
            ReadOnlyKeyedColumnBaseCollection<Column> childColumns = groupColumn.AllVisibleChildColumns;
            for (int i = 0; i < childColumns.Count; i++)
            {
                Column childColumn = childColumns[i];

                // PK tfs 166613: update the ActualWidth of the control if reset and not restored on resizing
                if (childColumn.ActualWidth == 0 && groupColumn.ActualWidth > 0)
                {
                    GroupColumnsCollection allColumns = new GroupColumnsCollection();
                    foreach (Column col in groupColumn.AllColumns)
                    {
                        allColumns.Add(col);
                    }

                    GroupColumnPanel.InitializeColumnActualWidths(allColumns, this.Row);
                    break;
                }
            }
        }

        /// <summary>
        /// Invalidates the horizontal maximum.
        /// </summary>
        /// <param name="availWidth">Width of the avail.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="isInfinite">if set to <c>true</c> [is infinite].</param>
        private void InvalidateHorizontalMax(double availWidth, ref double maxHeight, bool isInfinite)
        {
            if (this._prevAvailableWidth != availWidth)
                this.Row.Manager.OverrideHorizontalMax = -1;

            this._prevAvailableWidth = availWidth;

            if (this.Row.Manager.OverrideHorizontalMax == -1 && !isInfinite)
            {
                List<CellBase> previouslyLastCells = new List<CellBase>(this._lastCellsDetached);
                this._lastCellsDetached.Clear();

                // So, if there is a star column, then we shouldn't set the HorizontalMax.
                foreach (Column col in this.Row.Columns.VisibleColumns)
                {
                    // Check to make sure its not a FillerColumn
                    // Check to see if its a Star Column
                    // Check to see that the column's actual width isn't its minwidth.
                    if (!(col is FillerColumn) && col.WidthResolved.WidthType == ColumnWidthType.Star && col.ActualWidth > col.MinimumWidth)
                    {
                        // If there isn't a control attached, then its safe to assume that we need to set the HorizontalMax
                        // As obviously there are cells left to be rendered, which means Horizontal scrollbar is needed. 
                        CellBase cell = this.Row.Cells[col];
                        if (cell.Control != null)
                        {
                            this.Row.Manager.OverrideHorizontalMax = -1;
                            return;
                        }
                    }
                }

                Collection<CellBase> fakeStarColumns = new Collection<CellBase>();

                double cellWidth = 0;
                double allCellsWidth = 0;
                double visibleCellsWidth = 0;
                Column currentColumn;

                // Calculate the width of all visible columns so the max value for horizontal scrolling can be determined.
                for (int currentColumnIndex = 0; currentColumnIndex < this.Row.Columns.VisibleColumns.Count; currentColumnIndex++)
                {
                    currentColumn = this.Row.Columns.VisibleColumns[currentColumnIndex];

                    var isColumnInView = currentColumnIndex >= this.Row.Manager.IndexOfFirstColumnRendered &&
                                         visibleCellsWidth < availWidth;

                    cellWidth = 0;
                    // Render only the cells which are currently in view and aren't in star column.
                    // Otherwise for performance reasons just use their actual width (or average cell width if not yet rendered) without rendering it.
                    if (currentColumn.WidthResolved.WidthType != ColumnWidthType.Star && isColumnInView)
                    {
                        cellWidth = this.RenderCell(currentColumn, fakeStarColumns, ref maxHeight, false, this._lastCellsDetached, isInfinite, true);
                    }
                    else if (!(currentColumn is FillerColumn))
                    {
                        cellWidth = this.GetColumnWidthForHorizontalScrolling(currentColumn);
                    }

                    allCellsWidth += cellWidth;

                    if (isColumnInView)
                    {
                        visibleCellsWidth += cellWidth;
                    }
                }

                int count = this._lastCellsDetached.Count - 1;

                for (int i = count; i >= 0; i--)
                {
                    CellBase cell = this._lastCellsDetached[i];

                    cell.SuppressCellControlAttached = false;

                    if (this.VisibleCells.Contains(cell))
                    {
                        this._lastCellsDetached.Remove(cell);

                        Cell cellToAttach = cell as Cell;
                        ColumnLayout columnLayout = this.Row.Manager.ColumnLayout;

                        // We suppressed CellControlAttached for this cell. We need to make sure that
                        // CellControl attached is fired, because this cell is visible and we will use it.
                        if (cellToAttach != null && columnLayout != null && columnLayout.Grid != null)
                        {
                            this.Row.Manager.ColumnLayout.Grid.OnCellControlAttached(cellToAttach);
                        }
                    }
                    else
                    {
                        this.ReleaseCell(cell);
                    }
                }

                // We cache off the last rows, so that we don't have to keep sizing them to adjust
                // the Vertical scrollbar. But, something changed in the rows collection now, so lets
                // start over.
                foreach (CellBase cell in previouslyLastCells)
                {
                    if (!this.VisibleCells.Contains(cell) && !this.VisibleFixedLeftCells.Contains(cell) && !this.VisibleFixedRightCells.Contains(cell) && cell.Control != null)
                    {
                        this.ReleaseCell(cell);
                    }
                }

                // Calculate the new max value of the horizontal scrollbar according to the size of the viewport
                // Viewport size is equal to the currently available width
                this.Row.Manager.OverrideHorizontalMax = allCellsWidth - availWidth;
                if (this.Row.Manager.OverrideHorizontalMax < 0)
                    this.Row.Manager.OverrideHorizontalMax = 0;
            }
        }

        /// <summary>
        /// Get the width of a given column depending on whether the column is already rendered, if there is a minimal width set or using average cell width if not yet rendered.
        /// </summary>
        /// <returns>Returns the average cell width of the all rendered cells if the cell is not yet rendered. If rendered returns the cell's actual width. Returns column's minimal width (if set) in case the actual/average width is lower.</returns>
        private double GetColumnWidthForHorizontalScrolling(Column column)
        {
            double width = 0;

            if (column is GroupColumn)
            {
                var group = (GroupColumn)column;
                foreach (var child in group.AllVisibleChildColumns)
                {
                    width += this.GetColumnWidthForHorizontalScrolling(child);
                }
            }
            else
            {
                //TFS 193094 - do not use average width for star columns 
                width = column.ActualWidth != 0 || column.WidthResolved.WidthType == ColumnWidthType.Star ? column.ActualWidth : this._averageCellWidth;
            }

            return (width > column.MinimumWidth) ? width : column.MinimumWidth;
        }

        private void SetAverageColumnWidth(Column column)
        {
            if (column is GroupColumn)
            {
                var group = (GroupColumn)column;
                foreach (var child in group.AllVisibleChildColumns)
                {
                    this.SetAverageColumnWidth(child);
                }
            }
            else
            {
                if (this._averageCellWidth == 0)
                {
                    this._averageCellWidth = column.ActualWidth;
                }
                else
                {
                    this._averageCellWidth = (this._averageCellWidth + column.ActualWidth) / 2;
                }
            }
        }

        /// <summary>
        /// Check whether the current panel represents a header row of the first of visible rows.
        /// </summary>
        /// <returns>Returns whether the presented row is a header row or the first visible row.</returns>
        private bool IsHeaderOrFirstVisibleRowInParentLayout()
        {
            bool result = this.Row.RowType == RowType.HeaderRow || this.Row.RowType == RowType.ColumnLayoutHeaderRow;

            if (!result && this.Parent != null && this.Parent is RowsPanel)
            {
                result = ((RowsPanel)this.Parent).VisibleRows.FirstOrDefault() == this.Row;
            }

            return result;
        }

        #endregion // Private

        #region Internal

        internal void InternalCellMouseEnter(CellBase cell)
        {



            this.OnCellMouseOver(cell);

        }

        internal void InternalCellMouseLeave(CellBase cell, CellBase newCell)
        {



            this.OnCellMouseLeave(cell, newCell);

        }

        internal void RenderCell(CellBase cell)
        {
            double maxHeight = 0;

            Column col = cell.Column;
            while (col.ParentColumn != null)
                col = col.ParentColumn;

            bool resetFlag = true;
            RowsPanel rowsPanel = this.Owner;

            if (rowsPanel != null)
            {
                resetFlag = (rowsPanel.ShouldInvalidateGroupCellMeasure != true);
                rowsPanel.ShouldInvalidateGroupCellMeasure = true;
            }

            this.RenderCell(col, new Collection<CellBase>(), ref maxHeight, false, this._visibleCells, false);

            if (rowsPanel != null && resetFlag)
            {
                rowsPanel.ShouldInvalidateGroupCellMeasure = false;
            }
        }

        #endregion // Internal

        #endregion // Methods

        #region Overrides

        // AS 2/10/14 TFS162859
        private readonly List<CellBase> _previousCells = new List<CellBase>();

        #region MeasureOverride
        /// <summary>
        /// Provides the behavior for the "measure" pass of the <see cref="CellsPanel"/>.
        /// </summary>
        /// <param propertyName="availableSize">The available size that this object can give to child objects. Infinity can be specified
        /// as a value to indicate the object will size to whatever content is available.</param>
        /// <returns></returns>
        protected override Size MeasureOverride(Size availableSize)
        {
            bool isInfinite = this._hasInfiniteWidth = double.IsInfinity(availableSize.Width);

            // TFS 178570 - adding a check for ColumnLayout as it holds the visible columns needed for measuring
            if (this.Row == null || this.Row.ColumnLayout == null)
                return base.MeasureOverride(availableSize);

            this.PreviousInvalidateHeight = availableSize.Height;

            // If the Owning Panel, didn'type invoke this measure, then we need to make sure that it does get measured, so that all of the cells 
            // In the visible grid have the same ColumnWidth.
            if (this.Owner != null && !this.Owner.InLayoutPhase)
                this.Owner.InvalidateMeasureInternal();

            List<CellBase> previousCells = new List<CellBase>(this._visibleCells);
            previousCells.AddRange(this._visibleFixedLeftCells);
            previousCells.AddRange(this._additionalCells);
            previousCells.AddRange(this._visibleFixedRightCells);
            this._additionalCells.Clear();
            this._visibleCells.Clear();
            this._visibleFixedLeftCells.Clear();
            this._visibleFixedRightCells.Clear();

            // AS 2/10/14 TFS162859
            // Temporarily cache the cells we released.
            //
            _previousCells.AddRange(previousCells);

            Size rowSize = this.RenderCells(availableSize.Width);

            _previousCells.Clear(); // AS 2/10/14 TFS162859

            switch (this.Row.HeightResolved.HeightType)
            {
                case RowHeightType.SizeToLargestCell:
                    this.Row.ActualHeight = rowSize.Height = Math.Max(Math.Max(this.Row.ActualHeight, rowSize.Height), this.Row.MinimumRowHeightResolved);
                    break;

                case RowHeightType.Dynamic:
                    rowSize.Height = Math.Max(rowSize.Height, this.Row.MinimumRowHeightResolved);
                    this.Row.ActualHeight = rowSize.Height;
                    break;

                case RowHeightType.Numeric:
                    this.Row.ActualHeight = rowSize.Height = this.Row.HeightResolved.Value;
                    break;
            }

            foreach (CellBase cell in previousCells)
            {
                if (!this._visibleCells.Contains(cell) && !this._visibleFixedLeftCells.Contains(cell) && !this._visibleFixedRightCells.Contains(cell) && !this._additionalCells.Contains(cell))
                {
                    this.ReleaseCell(cell);
                }
            }

            double width = availableSize.Width;
            if (isInfinite)
                width = rowSize.Width;
            return new Size(width, rowSize.Height);
        }

        #endregion // MeasureOverride

        #region ArrangeOverride

        /// <summary>
        /// Arranges each <see cref="CellBase"/> that should be visible, one next to  the other, similar to a 
        /// Horizontal <see cref="StackPanel"/>.
        /// </summary>
        /// <param propertyName="finalSize">
        /// The final area within the parent that this object 
        /// should use to arrange itself and its children.
        /// </param>
        /// <returns></returns>
        protected override Size ArrangeOverride(Size finalSize)
        {

            this.ArrangeRaised = true;

            // TFS 178570 - adding a check for ColumnLayout as it holds the visible columns needed for arranging
            if (this.Row == null || this.Row.ColumnLayout == null)
                return finalSize;

            // Move all Cells that aren't being used, out of view. 
            // Note: we have to use GetAvailElements, instead of recent, b/c if we get taken out of the visual tree,
            // and then added back, all elements that were out of view, will not be arranged where we had arranged them.
            List<FrameworkElement> unusedCells = RecyclingManager.Manager.GetAvailableElements(this);
            foreach (FrameworkElement cell in unusedCells)
            {
                cell.Arrange(this._hideCellRect);
            }

            // Some cells, like cells that are in edit mode, may choose to not get released. 
            // If thats the case, then we need to make sure that they get placed out of view. 
            foreach (CellBase cell in this._nonReleasedCells)
            {
                if (cell.Control != null)
                    cell.Control.Arrange(this._hideCellRect);
            }

            this._nonReleasedCells.Clear();

            double left = this.Row.Manager.CachedIndentation;

            //Hide the Indentation.
            double clipWidth = finalSize.Width - left;
            if (clipWidth < 0)
                clipWidth = 0;
            this._clipRG.Rect = new Rect(left, 0, clipWidth, finalSize.Height);

            bool showFixedSep = false;

            // Position the Left Fixed Cells.
            foreach (CellBase cell in this._visibleFixedLeftCells)
            {
                Column col = cell.Column;
                FrameworkElement elem = cell.Control;
                double actualWidth = col.ActualWidth;

                if (col.IsGroupBy || col.IsFixed == FixedState.Left)
                    showFixedSep = true;

                if (col != null && col.IsMoving)
                    elem.Arrange(new Rect(left, 0, 0, 0));
                else
                {
                    double height = Math.Max(finalSize.Height, elem.DesiredSize.Height);

                    double leftVal = left;

                    // An Animation must be occurring, so lets arrange these columns into place gradually.
                    if (col != null && col.PercentMoved > 0)
                    {
                        if (!col.ReverseMove)
                            leftVal = (left - col.MovingColumnsWidth) + (col.MovingColumnsWidth * col.PercentMoved);
                        else
                            leftVal = (left + col.MovingColumnsWidth) - (col.MovingColumnsWidth * col.PercentMoved);
                    }

                    elem.Arrange(new Rect(leftVal, 0, actualWidth, height));
                }

                left += actualWidth;
                Canvas.SetZIndex(elem, 1);
            }

            // Place an Indicator to seperate the Left Fixed Cells
            if (showFixedSep)
            {
                foreach (CellBase cell in this._additionalCells)
                {
                    if (cell.Column == this.Row.Columns.FixedBorderColumnLeft)
                    {
                        FrameworkElement elem = cell.Control;
                        double actualWidth = cell.Column.ActualWidth;
                        elem.Arrange(new Rect(left, 0, actualWidth, finalSize.Height));
                        left += actualWidth;
                        Canvas.SetZIndex(elem, 1);
                        break;
                    }
                }
            }

            // Position the Right Fixed Cells.
            double right = finalSize.Width;

            double fillerColumnWidth = this.Row.Columns.FillerColumn.ActualWidth;
            if (fillerColumnWidth > 0 && !this._hasInfiniteWidth)
                right -= fillerColumnWidth;

            foreach (CellBase cell in this._visibleFixedRightCells)
            {
                Column col = cell.Column;
                FrameworkElement elem = cell.Control;
                double actualWidth = cell.Column.ActualWidth;
                double rightVal = right;
                right -= actualWidth;

                if (col != null && col.IsMoving)
                    elem.Arrange(new Rect(right, 0, 0, 0));
                else
                {
                    // An Animation must be occurring, so lets arrange these columns into place gradually.
                    if (col != null && col.PercentMoved > 0)
                    {
                        if (!col.ReverseMove)
                            rightVal = (rightVal + col.MovingColumnsWidth) - (col.MovingColumnsWidth * col.PercentMoved);
                        else
                            rightVal = (rightVal - col.MovingColumnsWidth) + (col.MovingColumnsWidth * col.PercentMoved);
                    }

                    rightVal -= actualWidth;

                    elem.Arrange(new Rect(rightVal, 0, actualWidth, Math.Max(finalSize.Height, elem.DesiredSize.Height)));
                }

                Canvas.SetZIndex(elem, 2);
            }

            // Place an Indicator to seperate the Right Fixed Cells
            if (this.Row.Columns.FixedColumnsRight.Count > 0)
            {
                foreach (CellBase cell in this._additionalCells)
                {
                    if (cell.Column == this.Row.Columns.FixedBorderColumnRight)
                    {
                        FrameworkElement elem = cell.Control;
                        double actualWidth = cell.Column.ActualWidth;
                        right -= actualWidth;
                        elem.Arrange(new Rect(right, 0, actualWidth, finalSize.Height));
                        Canvas.SetZIndex(elem, 2);
                        break;
                    }
                }
            }

            if (this.Row.CanScrollHorizontally)
            {
                // Calculate the offset LeftValue, for the first cell 
                ScrollBar horiztonalSB = this._scrollInfo.HorizontalScrollBar;
                if (horiztonalSB != null && horiztonalSB.Visibility == Visibility.Visible)
                {
                    if (this.Row.Manager.OverrideHorizontalMax > 0)
                    {
                        // If the horizontal scrollbar is visible, it's safe to say that we don't need the FillerColumn
                        // if we don't do this, then there is a chance that the scroll left changed between measuring and arranging
                        // due to column's resizing. 
                        this.Row.Columns.FillerColumn.ActualWidth = 0;
                    }


                    if (horiztonalSB.Value != horiztonalSB.Maximum)
                    {
                        if (this._visibleCells.Count > 0)
                        {
                            double scrollLeft = this.ResolveScrollLeft();
                            double cellScrollPercentage = scrollLeft - (int)scrollLeft;
                            double leftVal = this._visibleCells[0].Column.ActualWidth * cellScrollPercentage;

                            GroupColumn groupColumn = this._visibleCells[0].Column as GroupColumn;

                            if (groupColumn != null)
                            {
                                leftVal = 0; // Reset leftVal
                                ReadOnlyKeyedColumnBaseCollection<Column> allChildColumns = this.Row.Columns.AllVisibleChildColumns;
                                Column realFirstColumn = allChildColumns[(int)scrollLeft];
                                ReadOnlyKeyedColumnBaseCollection<Column> childColumns = groupColumn.AllVisibleChildColumns;

                                foreach (var childColumn in childColumns)
                                {
                                    if (realFirstColumn != childColumn)
                                    {
                                        leftVal += childColumn.ActualWidth;
                                    }
                                    else
                                    {
                                        leftVal += realFirstColumn.ActualWidth * cellScrollPercentage;
                                        break;
                                    }
                                }
                            }

                            if ((this.Row.Manager.CurrentVisibleWidth + left) > right)
                                left += -leftVal;
                        }
                    }
                    else
                    {
                        // We've reached the last child, so lets make sure its visible. 
                        if ((this.Row.Manager.CurrentVisibleWidth + left) > right)
                            left -= this.Row.Manager.OverflowAdjustment;
                    }

                }
            }

            // Position the Scrollable Cells
            foreach (CellBase cell in this._visibleCells)
            {
                Column col = cell.Column;

                if (!col.IsInitialAutoSet)
                {
                    bool ignoreAutoSet = false;

                    RowsManager manager = this.Row.Manager as RowsManager;
                    if (manager != null)
                    {
                        ignoreAutoSet = manager.RegisteredTopRows.Contains(this.Row);

                        if (!ignoreAutoSet)
                            ignoreAutoSet = manager.RegisteredBottomRows.Contains(this.Row);
                    }

                    
                    
                    CellBase cellRef = cell;
                    RowsPanel owner = this.Owner;

                    Action dispatchAction = new Action(() =>
                        {
                            CellControlBase control = cellRef.Control;

                            if (owner != null && !owner.ReverseCellMeasure && !ignoreAutoSet && control != null && control.IsCellLoaded)
                            {
                                col.IsInitialAutoSet = true;
                            }
                        });

                    if (this._isInDesigner)
                    {
                        dispatchAction.Invoke();
                    }
                    else
                    {
                        this.Dispatcher.BeginInvoke(dispatchAction);
                    }
                }

                CellControlBase elem = cell.Control;
                double actualWidth = cell.Column.ActualWidth;

                if (cell.Column.WidthResolved.WidthType == ColumnWidthType.Star)
                {
                    if (this.Row.Manager.StarColumnWidths.ContainsKey(cell.Column))
                        actualWidth = this.Row.Manager.StarColumnWidths[cell.Column];
                }

                if (col != null && col.IsMoving)
                    elem.Arrange(new Rect(left, 0, 0, 0));
                else
                {
                    double leftVal = left;
                    // An Animation must be occurring, so lets arrange these columns into place gradually.
                    if (col != null && col.PercentMoved > 0)
                    {
                        if (!col.ReverseMove)
                            leftVal = (left - col.MovingColumnsWidth) + (col.MovingColumnsWidth * col.PercentMoved);
                        else
                            leftVal = (left + col.MovingColumnsWidth) - (col.MovingColumnsWidth * col.PercentMoved);
                    }

                    if (col is FillerColumn)
                    {
                        if (this._hasInfiniteWidth)
                            continue;

                        leftVal = finalSize.Width - actualWidth;
                    }

                    elem.Arrange(new Rect(leftVal, 0, actualWidth, Math.Max(finalSize.Height, elem.DesiredSize.Height)));
                }

                left += actualWidth;
                Canvas.SetZIndex(elem, 0);
            }

            // If there is more space, let's arrange the FillerColumn to take it
            // TFS167973 - KK 01.04.2014 - we need to arrange the filler even if we're using all the
            // space we're granted but that wasn't the case during the previous arrange.
            if (this._hasInfiniteWidth && (left < finalSize.Width || this._fillerCellNeedsToBeReArranged))
            {
                if (left < finalSize.Width)
                    this._fillerCellNeedsToBeReArranged = true;

                FillerColumn fillerColumn = this.Row.Columns.FillerColumn;
                CellBase fillerCell = this.Row.Cells[fillerColumn];
                CellControlBase fillerControl = fillerCell.Control;

                if (fillerControl != null)
                {
                    double width = finalSize.Width - left;
                    width = width < 0 ? 0 : width;
                    fillerColumn.SetActualWidthSilently(width);
                    fillerControl.Arrange(new Rect(left, 0, width, Math.Max(finalSize.Height, fillerControl.DesiredSize.Height)));
                    Canvas.SetZIndex(fillerControl, 0);
                }
            }
            else
            {
                this._fillerCellNeedsToBeReArranged = false;
            }

            return finalSize;
        }
        #endregion // ArrangeOverride

        #region OnCreateAutomationPeer
        /// <summary>
        /// When implemented in a derived class, returns class-specific <see cref="T:System.Windows.Automation.Peers.AutomationPeer"/> implementations for the Silverlight automation infrastructure.
        /// </summary>
        /// <returns>
        /// The class-specific <see cref="T:System.Windows.Automation.Peers.AutomationPeer"/> subclass to return.
        /// </returns>
        protected override AutomationPeer OnCreateAutomationPeer()
        {
            return new CellsPanelAutomationPeer(this);
        }
        #endregion //OnCreateAutomationPeer

        #endregion // Overrides

        #region ICommandTarget Members

        bool ICommandTarget.SupportsCommand(ICommand command)
        {
            return this.SupportsCommand(command);
        }

        object ICommandTarget.GetParameter(CommandSource source)
        {
            return this.GetParameter(source);
        }

        #endregion

        #region Classes
        /// <summary>
        /// Information about a <see cref="Column"/> that's a child of a <see cref="GroupColumn"/>.
        /// </summary>
        private class ChildColumnInfo
        {
            internal ChildColumnInfo(Column column, double scrollPercentage, int offset)
            {
                this.ChildColumn = column;
                this.ChildColumnScrollPercentage = scrollPercentage;
                this.ChildColumnIndexOffset = offset;
            }

            /// <summary>
            /// The position of the column relative to the GroupColumn
            /// </summary>
            internal int ChildColumnIndexOffset { get; private set; }

            /// <summary>
            /// Calculated scroll percentage. A value between 0.0 and 1.0
            /// </summary>
            internal double ChildColumnScrollPercentage { get; private set; }

            internal Column ChildColumn { get; private set; }
        }
        #endregion
    }
}
