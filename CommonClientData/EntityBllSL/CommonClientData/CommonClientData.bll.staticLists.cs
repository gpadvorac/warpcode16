using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;

// Gen Timestamp:  12/27/2016 12:47:09 PM

namespace EntityBll.SL
{

    public partial class CommonClientData_Bll_staticLists
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "CommonClientData";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "CommonClientData_Bll_staticLists";



        private static ComboItemList _taskStatus_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _taskStatus_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _wcControlType_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _wcControlType_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _wcDataTypeDotNet_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _wcDataTypeDotNet_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _wcDataTypeSQL_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _wcDataTypeSQL_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _wcStoredProcParamDirection_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _wcStoredProcParamDirection_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _wcStoredProcResultType_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _wcStoredProcResultType_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _wcStoredProcReturnType_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _wcStoredProcReturnType_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _wcTableColumnComboListType_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.ByteType);
        private static bool _wcTableColumnComboListType_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _wcTableSortByDirection_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.StringType);
        private static bool _wcTableSortByDirection_ComboItemList_BindingList_HasItems = true;

        static bool isDataLoaded = false;
        static ProxyWrapper.CommonClientDataService_ProxyWrapper _staticCommonClientDataProxy = null;

        #endregion Initialize Variables


        #region Load Data

        public static void InitializeProxyWrapper()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Enter);

                if (_staticCommonClientDataProxy == null)
                {
                    _staticCommonClientDataProxy = new ProxyWrapper.CommonClientDataService_ProxyWrapper();
                    _staticCommonClientDataProxy.GetCommonClientData_ReadOnlyStaticListsCompleted += new EventHandler<GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs>(GetCommonClientData_ReadOnlyStaticListsCompleted);
                    _staticCommonClientDataProxy.GetTaskStatus_ComboItemListCompleted += new EventHandler<GetTaskStatus_ComboItemListCompletedEventArgs>(GetTaskStatus_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetWcControlType_ComboItemListCompleted += new EventHandler<GetWcControlType_ComboItemListCompletedEventArgs>(GetWcControlType_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetWcDataTypeDotNet_ComboItemListCompleted += new EventHandler<GetWcDataTypeDotNet_ComboItemListCompletedEventArgs>(GetWcDataTypeDotNet_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetWcDataTypeSQL_ComboItemListCompleted += new EventHandler<GetWcDataTypeSQL_ComboItemListCompletedEventArgs>(GetWcDataTypeSQL_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetWcStoredProcParamDirection_ComboItemListCompleted += new EventHandler<GetWcStoredProcParamDirection_ComboItemListCompletedEventArgs>(GetWcStoredProcParamDirection_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetWcStoredProcResultType_ComboItemListCompleted += new EventHandler<GetWcStoredProcResultType_ComboItemListCompletedEventArgs>(GetWcStoredProcResultType_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetWcStoredProcReturnType_ComboItemListCompleted += new EventHandler<GetWcStoredProcReturnType_ComboItemListCompletedEventArgs>(GetWcStoredProcReturnType_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetWcTableColumnComboListType_ComboItemListCompleted += new EventHandler<GetWcTableColumnComboListType_ComboItemListCompletedEventArgs>(GetWcTableColumnComboListType_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetWcTableSortByDirection_ComboItemListCompleted += new EventHandler<GetWcTableSortByDirection_ComboItemListCompletedEventArgs>(GetWcTableSortByDirection_ComboItemListCompleted);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Leave);
            }
        }

        public static void LoadStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Enter);

                if (_staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }

                _staticCommonClientDataProxy.Begin_GetCommonClientData_ReadOnlyStaticLists();

                LoadStaticLists_Custom();
                isDataLoaded = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Leave);
            }
        }

        //static void GetCommonClientData_ReadOnlyStaticListsCompleted(object sender, GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
        //        byte[] array = e.Result;
        //        object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
        //        if (data != null)
        //        {


        //            // TaskStatus_ComboItemList
        //            _taskStatus_ComboItemList_BindingList.IsRefreshingData = true;
        //            _taskStatus_ComboItemList_BindingList.CachedList.Clear();
        //            _taskStatus_ComboItemList_BindingList.Clear();
        //            _taskStatus_ComboItemList_BindingList.ReplaceList((object[])data[0]);

        //            _taskStatus_ComboItemList_BindingList.IsRefreshingData = false;

        //            // WcControlType_ComboItemList
        //            _wcControlType_ComboItemList_BindingList.IsRefreshingData = true;
        //            _wcControlType_ComboItemList_BindingList.CachedList.Clear();
        //            _wcControlType_ComboItemList_BindingList.Clear();
        //            _wcControlType_ComboItemList_BindingList.ReplaceList((object[])data[1]);

        //            _wcControlType_ComboItemList_BindingList.IsRefreshingData = false;

        //            // WcDataTypeDotNet_ComboItemList
        //            _wcDataTypeDotNet_ComboItemList_BindingList.IsRefreshingData = true;
        //            _wcDataTypeDotNet_ComboItemList_BindingList.CachedList.Clear();
        //            _wcDataTypeDotNet_ComboItemList_BindingList.Clear();
        //            _wcDataTypeDotNet_ComboItemList_BindingList.ReplaceList((object[])data[2]);

        //            _wcDataTypeDotNet_ComboItemList_BindingList.IsRefreshingData = false;

        //            // WcDataTypeSQL_ComboItemList
        //            _wcDataTypeSQL_ComboItemList_BindingList.IsRefreshingData = true;
        //            _wcDataTypeSQL_ComboItemList_BindingList.CachedList.Clear();
        //            _wcDataTypeSQL_ComboItemList_BindingList.Clear();
        //            _wcDataTypeSQL_ComboItemList_BindingList.ReplaceList((object[])data[3]);

        //            _wcDataTypeSQL_ComboItemList_BindingList.IsRefreshingData = false;

        //            // WcStoredProcParamDirection_ComboItemList
        //            _wcStoredProcParamDirection_ComboItemList_BindingList.IsRefreshingData = true;
        //            _wcStoredProcParamDirection_ComboItemList_BindingList.CachedList.Clear();
        //            _wcStoredProcParamDirection_ComboItemList_BindingList.Clear();
        //            _wcStoredProcParamDirection_ComboItemList_BindingList.ReplaceList((object[])data[4]);

        //            _wcStoredProcParamDirection_ComboItemList_BindingList.IsRefreshingData = false;

        //            // WcStoredProcResultType_ComboItemList
        //            _wcStoredProcResultType_ComboItemList_BindingList.IsRefreshingData = true;
        //            _wcStoredProcResultType_ComboItemList_BindingList.CachedList.Clear();
        //            _wcStoredProcResultType_ComboItemList_BindingList.Clear();
        //            _wcStoredProcResultType_ComboItemList_BindingList.ReplaceList((object[])data[5]);

        //            _wcStoredProcResultType_ComboItemList_BindingList.IsRefreshingData = false;

        //            // WcStoredProcReturnType_ComboItemList
        //            _wcStoredProcReturnType_ComboItemList_BindingList.IsRefreshingData = true;
        //            _wcStoredProcReturnType_ComboItemList_BindingList.CachedList.Clear();
        //            _wcStoredProcReturnType_ComboItemList_BindingList.Clear();
        //            _wcStoredProcReturnType_ComboItemList_BindingList.ReplaceList((object[])data[6]);

        //            _wcStoredProcReturnType_ComboItemList_BindingList.IsRefreshingData = false;

        //            // WcTableColumnComboListType_ComboItemList
        //            _wcTableColumnComboListType_ComboItemList_BindingList.IsRefreshingData = true;
        //            _wcTableColumnComboListType_ComboItemList_BindingList.CachedList.Clear();
        //            _wcTableColumnComboListType_ComboItemList_BindingList.Clear();
        //            _wcTableColumnComboListType_ComboItemList_BindingList.ReplaceList((object[])data[7]);

        //            _wcTableColumnComboListType_ComboItemList_BindingList.IsRefreshingData = false;

        //            // WcTableSortByDirection_ComboItemList
        //            _wcTableSortByDirection_ComboItemList_BindingList.IsRefreshingData = true;
        //            _wcTableSortByDirection_ComboItemList_BindingList.CachedList.Clear();
        //            _wcTableSortByDirection_ComboItemList_BindingList.Clear();
        //            _wcTableSortByDirection_ComboItemList_BindingList.ReplaceList((object[])data[8]);

        //            _wcTableSortByDirection_ComboItemList_BindingList.IsRefreshingData = false;
        //            isDataLoaded = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticListsCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        #endregion Load Data


        #region Properties

        public static bool IsDataLoaded
        {
            get { return isDataLoaded; }
        }

                    // TaskStatus_ComboItemList
        public static ComboItemList TaskStatus_ComboItemList_BindingListProperty
        {
            get
            {
                return _taskStatus_ComboItemList_BindingList;
            }
            set
            {
                _taskStatus_ComboItemList_BindingList = value;
            }
        }

                    // WcControlType_ComboItemList
        public static ComboItemList WcControlType_ComboItemList_BindingListProperty
        {
            get
            {
                return _wcControlType_ComboItemList_BindingList;
            }
            set
            {
                _wcControlType_ComboItemList_BindingList = value;
            }
        }

                    // WcDataTypeDotNet_ComboItemList
        public static ComboItemList WcDataTypeDotNet_ComboItemList_BindingListProperty
        {
            get
            {
                return _wcDataTypeDotNet_ComboItemList_BindingList;
            }
            set
            {
                _wcDataTypeDotNet_ComboItemList_BindingList = value;
            }
        }

                    // WcDataTypeSQL_ComboItemList
        public static ComboItemList WcDataTypeSQL_ComboItemList_BindingListProperty
        {
            get
            {
                return _wcDataTypeSQL_ComboItemList_BindingList;
            }
            set
            {
                _wcDataTypeSQL_ComboItemList_BindingList = value;
            }
        }

                    // WcStoredProcParamDirection_ComboItemList
        public static ComboItemList WcStoredProcParamDirection_ComboItemList_BindingListProperty
        {
            get
            {
                return _wcStoredProcParamDirection_ComboItemList_BindingList;
            }
            set
            {
                _wcStoredProcParamDirection_ComboItemList_BindingList = value;
            }
        }

                    // WcStoredProcResultType_ComboItemList
        public static ComboItemList WcStoredProcResultType_ComboItemList_BindingListProperty
        {
            get
            {
                return _wcStoredProcResultType_ComboItemList_BindingList;
            }
            set
            {
                _wcStoredProcResultType_ComboItemList_BindingList = value;
            }
        }

                    // WcStoredProcReturnType_ComboItemList
        public static ComboItemList WcStoredProcReturnType_ComboItemList_BindingListProperty
        {
            get
            {
                return _wcStoredProcReturnType_ComboItemList_BindingList;
            }
            set
            {
                _wcStoredProcReturnType_ComboItemList_BindingList = value;
            }
        }

                    // WcTableColumnComboListType_ComboItemList
        public static ComboItemList WcTableColumnComboListType_ComboItemList_BindingListProperty
        {
            get
            {
                return _wcTableColumnComboListType_ComboItemList_BindingList;
            }
            set
            {
                _wcTableColumnComboListType_ComboItemList_BindingList = value;
            }
        }

                    // WcTableSortByDirection_ComboItemList
        public static ComboItemList WcTableSortByDirection_ComboItemList_BindingListProperty
        {
            get
            {
                return _wcTableSortByDirection_ComboItemList_BindingList;
            }
            set
            {
                _wcTableSortByDirection_ComboItemList_BindingList = value;
            }
        }

        #endregion Properties


        #region RefreshLists



        #region TaskStatus_ComboItemList

        public static void Refresh_TaskStatus_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskStatus_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetTaskStatus_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskStatus_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskStatus_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetTaskStatus_ComboItemListCompleted(object sender, GetTaskStatus_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _taskStatus_ComboItemList_BindingList.IsRefreshingData = true;
                _taskStatus_ComboItemList_BindingList.ReplaceList(data);
                _taskStatus_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TaskStatus_ComboItemList



        #region WcControlType_ComboItemList

        public static void Refresh_WcControlType_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcControlType_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetWcControlType_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcControlType_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcControlType_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetWcControlType_ComboItemListCompleted(object sender, GetWcControlType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcControlType_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _wcControlType_ComboItemList_BindingList.IsRefreshingData = true;
                _wcControlType_ComboItemList_BindingList.ReplaceList(data);
                _wcControlType_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcControlType_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcControlType_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcControlType_ComboItemList



        #region WcDataTypeDotNet_ComboItemList

        public static void Refresh_WcDataTypeDotNet_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcDataTypeDotNet_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetWcDataTypeDotNet_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcDataTypeDotNet_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcDataTypeDotNet_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetWcDataTypeDotNet_ComboItemListCompleted(object sender, GetWcDataTypeDotNet_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDotNet_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _wcDataTypeDotNet_ComboItemList_BindingList.IsRefreshingData = true;
                _wcDataTypeDotNet_ComboItemList_BindingList.ReplaceList(data);
                _wcDataTypeDotNet_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDotNet_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDotNet_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcDataTypeDotNet_ComboItemList



        #region WcDataTypeSQL_ComboItemList

        public static void Refresh_WcDataTypeSQL_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcDataTypeSQL_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetWcDataTypeSQL_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcDataTypeSQL_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcDataTypeSQL_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetWcDataTypeSQL_ComboItemListCompleted(object sender, GetWcDataTypeSQL_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeSQL_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _wcDataTypeSQL_ComboItemList_BindingList.IsRefreshingData = true;
                _wcDataTypeSQL_ComboItemList_BindingList.ReplaceList(data);
                _wcDataTypeSQL_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeSQL_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeSQL_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcDataTypeSQL_ComboItemList



        #region WcStoredProcParamDirection_ComboItemList

        public static void Refresh_WcStoredProcParamDirection_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcStoredProcParamDirection_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetWcStoredProcParamDirection_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcStoredProcParamDirection_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcStoredProcParamDirection_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetWcStoredProcParamDirection_ComboItemListCompleted(object sender, GetWcStoredProcParamDirection_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParamDirection_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _wcStoredProcParamDirection_ComboItemList_BindingList.IsRefreshingData = true;
                _wcStoredProcParamDirection_ComboItemList_BindingList.ReplaceList(data);
                _wcStoredProcParamDirection_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParamDirection_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParamDirection_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamDirection_ComboItemList



        #region WcStoredProcResultType_ComboItemList

        public static void Refresh_WcStoredProcResultType_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcStoredProcResultType_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetWcStoredProcResultType_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcStoredProcResultType_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcStoredProcResultType_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetWcStoredProcResultType_ComboItemListCompleted(object sender, GetWcStoredProcResultType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcResultType_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _wcStoredProcResultType_ComboItemList_BindingList.IsRefreshingData = true;
                _wcStoredProcResultType_ComboItemList_BindingList.ReplaceList(data);
                _wcStoredProcResultType_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcResultType_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcResultType_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcResultType_ComboItemList



        #region WcStoredProcReturnType_ComboItemList

        public static void Refresh_WcStoredProcReturnType_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcStoredProcReturnType_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetWcStoredProcReturnType_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcStoredProcReturnType_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcStoredProcReturnType_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetWcStoredProcReturnType_ComboItemListCompleted(object sender, GetWcStoredProcReturnType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcReturnType_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _wcStoredProcReturnType_ComboItemList_BindingList.IsRefreshingData = true;
                _wcStoredProcReturnType_ComboItemList_BindingList.ReplaceList(data);
                _wcStoredProcReturnType_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcReturnType_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcReturnType_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcReturnType_ComboItemList



        #region WcTableColumnComboListType_ComboItemList

        public static void Refresh_WcTableColumnComboListType_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcTableColumnComboListType_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetWcTableColumnComboListType_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcTableColumnComboListType_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcTableColumnComboListType_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetWcTableColumnComboListType_ComboItemListCompleted(object sender, GetWcTableColumnComboListType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnComboListType_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _wcTableColumnComboListType_ComboItemList_BindingList.IsRefreshingData = true;
                _wcTableColumnComboListType_ComboItemList_BindingList.ReplaceList(data);
                _wcTableColumnComboListType_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnComboListType_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnComboListType_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnComboListType_ComboItemList



        #region WcTableSortByDirection_ComboItemList

        public static void Refresh_WcTableSortByDirection_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcTableSortByDirection_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetWcTableSortByDirection_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcTableSortByDirection_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcTableSortByDirection_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetWcTableSortByDirection_ComboItemListCompleted(object sender, GetWcTableSortByDirection_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableSortByDirection_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _wcTableSortByDirection_ComboItemList_BindingList.IsRefreshingData = true;
                _wcTableSortByDirection_ComboItemList_BindingList.ReplaceList(data);
                _wcTableSortByDirection_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableSortByDirection_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableSortByDirection_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableSortByDirection_ComboItemList



        #endregion RefreshLists


    }

    #region Classes for Binding

#endregion Classes for Binding

}

