using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  4/23/2012 3:14:06 PM

namespace EntityBll.SL
{
    public partial class CommonClientData_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_Stub2 = 50;
		private const string BROKENRULE_Stub1_Required = "Id is a required field.";
		private string BROKENRULE_Stub2_TextLength = "Name has a maximum text length of  '" + STRINGSIZE_Stub2 + "'.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("Stub1", BROKENRULE_Stub1_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Stub2", BROKENRULE_Stub2_TextLength);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void Stub1_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Stub1_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Stub1");
                string newBrokenRules = "";
                
                if (Stub1 == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Stub1", BROKENRULE_Stub1_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Stub1", BROKENRULE_Stub1_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Stub1");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Stub1", _brokenRuleManager.IsPropertyValid("Stub1"), IsPropertyDirty("Stub1"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Stub1_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Stub1_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Stub2_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Stub2_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Stub2");
                string newBrokenRules = "";
                				int len = 0;
                if (Stub2 != null)
                {
                    len = Stub2.Length;
                }

                if (len > STRINGSIZE_Stub2)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Stub2", BROKENRULE_Stub2_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Stub2", BROKENRULE_Stub2_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Stub2");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Stub2", _brokenRuleManager.IsPropertyValid("Stub2"), IsPropertyDirty("Stub2"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Stub2_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Stub2_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


