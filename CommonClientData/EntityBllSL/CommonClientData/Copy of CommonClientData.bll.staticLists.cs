using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;

// Gen Timestamp:  2/6/2013 12:37:46 PM

namespace EntityBll.SL
{

    public partial class CommonClientData_Bll_staticLists
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "CommonClientData";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "CommonClientData_Bll_staticLists";



        private static ComboItemList _addition_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _addition_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _agencyType_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _agencyType_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _area_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _area_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _block_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _block_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _county_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _county_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _dataStatus_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _dataStatus_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _field_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _field_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _flagStatus_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _flagStatus_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _formation_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _formation_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _operator_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _operator_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _ownerShipType_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _ownerShipType_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _party_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _party_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _rangeDirection_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.StringType);
        private static bool _rangeDirection_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _recordingType_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _recordingType_ComboItemList_BindingList_HasItems = true;

        private static ObservableCollection<Report_lstRptExplorer_Binding> _report_lstRptExplorer_BindingList = new ObservableCollection<Report_lstRptExplorer_Binding>();
        private static bool _report_lstRptExplorer_BindingList_HasItems = true;

        private static ObservableCollection<ReportGroup_lstRptExplorer_Binding> _reportGroup_lstRptExplorer_BindingList = new ObservableCollection<ReportGroup_lstRptExplorer_Binding>();
        private static bool _reportGroup_lstRptExplorer_BindingList_HasItems = true;

        private static ComboItemList _state_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _state_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _stateCounty_ComboItemList_BindingList = new ComboItemList();
        private static bool _stateCounty_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _townshipDirection_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.StringType);
        private static bool _townshipDirection_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _v_SysDotNetDataType_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.StringType);
        private static bool _v_SysDotNetDataType_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _v_SysSqlDataType_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.StringType);
        private static bool _v_SysSqlDataType_ComboItemList_BindingList_HasItems = true;

        static bool isDataLoaded = false;
        static ProxyWrapper.CommonClientDataService_ProxyWrapper _staticCommonClientDataProxy = null;

        #endregion Initialize Variables


        #region Load Data

        public static void InitializeProxyWrapper()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Enter);

                if (_staticCommonClientDataProxy == null)
                {
                    _staticCommonClientDataProxy = new ProxyWrapper.CommonClientDataService_ProxyWrapper();
                    _staticCommonClientDataProxy.GetCommonClientData_ReadOnlyStaticListsCompleted += new EventHandler<GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs>(GetCommonClientData_ReadOnlyStaticListsCompleted);
                    _staticCommonClientDataProxy.GetAddition_ComboItemListCompleted += new EventHandler<GetAddition_ComboItemListCompletedEventArgs>(GetAddition_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetAgencyType_ComboItemListCompleted += new EventHandler<GetAgencyType_ComboItemListCompletedEventArgs>(GetAgencyType_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetArea_ComboItemListCompleted += new EventHandler<GetArea_ComboItemListCompletedEventArgs>(GetArea_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetBlock_ComboItemListCompleted += new EventHandler<GetBlock_ComboItemListCompletedEventArgs>(GetBlock_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetCounty_ComboItemListCompleted += new EventHandler<GetCounty_ComboItemListCompletedEventArgs>(GetCounty_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetDataStatus_ComboItemListCompleted += new EventHandler<GetDataStatus_ComboItemListCompletedEventArgs>(GetDataStatus_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetField_ComboItemListCompleted += new EventHandler<GetField_ComboItemListCompletedEventArgs>(GetField_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetFlagStatus_ComboItemListCompleted += new EventHandler<GetFlagStatus_ComboItemListCompletedEventArgs>(GetFlagStatus_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetFormation_ComboItemListCompleted += new EventHandler<GetFormation_ComboItemListCompletedEventArgs>(GetFormation_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetOperator_ComboItemListCompleted += new EventHandler<GetOperator_ComboItemListCompletedEventArgs>(GetOperator_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetOwnerShipType_ComboItemListCompleted += new EventHandler<GetOwnerShipType_ComboItemListCompletedEventArgs>(GetOwnerShipType_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetParty_ComboItemListCompleted += new EventHandler<GetParty_ComboItemListCompletedEventArgs>(GetParty_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetRangeDirection_ComboItemListCompleted += new EventHandler<GetRangeDirection_ComboItemListCompletedEventArgs>(GetRangeDirection_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetRecordingType_ComboItemListCompleted += new EventHandler<GetRecordingType_ComboItemListCompletedEventArgs>(GetRecordingType_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetReport_lstRptExplorerCompleted += new EventHandler<GetReport_lstRptExplorerCompletedEventArgs>(GetReport_lstRptExplorerCompleted);
                    _staticCommonClientDataProxy.GetReportGroup_lstRptExplorerCompleted += new EventHandler<GetReportGroup_lstRptExplorerCompletedEventArgs>(GetReportGroup_lstRptExplorerCompleted);
                    _staticCommonClientDataProxy.GetState_ComboItemListCompleted += new EventHandler<GetState_ComboItemListCompletedEventArgs>(GetState_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetStateCounty_ComboItemListCompleted += new EventHandler<GetStateCounty_ComboItemListCompletedEventArgs>(GetStateCounty_ComboItemListCompleted);
                    _staticCommonClientDataProxy.GetTownshipDirection_ComboItemListCompleted += new EventHandler<GetTownshipDirection_ComboItemListCompletedEventArgs>(GetTownshipDirection_ComboItemListCompleted);
                    _staticCommonClientDataProxy.Getv_SysDotNetDataType_ComboItemListCompleted += new EventHandler<Getv_SysDotNetDataType_ComboItemListCompletedEventArgs>(Getv_SysDotNetDataType_ComboItemListCompleted);
                    _staticCommonClientDataProxy.Getv_SysSqlDataType_ComboItemListCompleted += new EventHandler<Getv_SysSqlDataType_ComboItemListCompletedEventArgs>(Getv_SysSqlDataType_ComboItemListCompleted);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Leave);
            }
        }

        public static void LoadStaticLists(Guid Prj_Id , Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Enter);

                if (_staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }

                _staticCommonClientDataProxy.Begin_GetCommonClientData_ReadOnlyStaticLists(Prj_Id , UserId );

                LoadStaticLists_Custom();
                isDataLoaded = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Leave);
            }
        }

        static void GetCommonClientData_ReadOnlyStaticListsCompleted(object sender, GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                if (data != null)
                {


                    // Addition_ComboItemList
                    _addition_ComboItemList_BindingList.IsRefreshingData = true;
                    _addition_ComboItemList_BindingList.CachedList.Clear();
                    _addition_ComboItemList_BindingList.Clear();
                    _addition_ComboItemList_BindingList.ReplaceList((object[])data[0]);

                    _addition_ComboItemList_BindingList.IsRefreshingData = false;

                    // AgencyType_ComboItemList
                    _agencyType_ComboItemList_BindingList.IsRefreshingData = true;
                    _agencyType_ComboItemList_BindingList.CachedList.Clear();
                    _agencyType_ComboItemList_BindingList.Clear();
                    _agencyType_ComboItemList_BindingList.ReplaceList((object[])data[1]);

                    _agencyType_ComboItemList_BindingList.IsRefreshingData = false;

                    // Area_ComboItemList
                    _area_ComboItemList_BindingList.IsRefreshingData = true;
                    _area_ComboItemList_BindingList.CachedList.Clear();
                    _area_ComboItemList_BindingList.Clear();
                    _area_ComboItemList_BindingList.ReplaceList((object[])data[2]);

                    _area_ComboItemList_BindingList.IsRefreshingData = false;

                    // Block_ComboItemList
                    _block_ComboItemList_BindingList.IsRefreshingData = true;
                    _block_ComboItemList_BindingList.CachedList.Clear();
                    _block_ComboItemList_BindingList.Clear();
                    _block_ComboItemList_BindingList.ReplaceList((object[])data[3]);

                    _block_ComboItemList_BindingList.IsRefreshingData = false;

                    // County_ComboItemList
                    _county_ComboItemList_BindingList.IsRefreshingData = true;
                    _county_ComboItemList_BindingList.CachedList.Clear();
                    _county_ComboItemList_BindingList.Clear();
                    _county_ComboItemList_BindingList.ReplaceList((object[])data[4]);

                    _county_ComboItemList_BindingList.IsRefreshingData = false;

                    // DataStatus_ComboItemList
                    _dataStatus_ComboItemList_BindingList.IsRefreshingData = true;
                    _dataStatus_ComboItemList_BindingList.CachedList.Clear();
                    _dataStatus_ComboItemList_BindingList.Clear();
                    _dataStatus_ComboItemList_BindingList.ReplaceList((object[])data[5]);

                    _dataStatus_ComboItemList_BindingList.IsRefreshingData = false;

                    // Field_ComboItemList
                    _field_ComboItemList_BindingList.IsRefreshingData = true;
                    _field_ComboItemList_BindingList.CachedList.Clear();
                    _field_ComboItemList_BindingList.Clear();
                    _field_ComboItemList_BindingList.ReplaceList((object[])data[6]);

                    _field_ComboItemList_BindingList.IsRefreshingData = false;

                    // FlagStatus_ComboItemList
                    _flagStatus_ComboItemList_BindingList.IsRefreshingData = true;
                    _flagStatus_ComboItemList_BindingList.CachedList.Clear();
                    _flagStatus_ComboItemList_BindingList.Clear();
                    _flagStatus_ComboItemList_BindingList.ReplaceList((object[])data[7]);

                    _flagStatus_ComboItemList_BindingList.IsRefreshingData = false;

                    // Formation_ComboItemList
                    _formation_ComboItemList_BindingList.IsRefreshingData = true;
                    _formation_ComboItemList_BindingList.CachedList.Clear();
                    _formation_ComboItemList_BindingList.Clear();
                    _formation_ComboItemList_BindingList.ReplaceList((object[])data[8]);

                    _formation_ComboItemList_BindingList.IsRefreshingData = false;

                    // Operator_ComboItemList
                    _operator_ComboItemList_BindingList.IsRefreshingData = true;
                    _operator_ComboItemList_BindingList.CachedList.Clear();
                    _operator_ComboItemList_BindingList.Clear();
                    _operator_ComboItemList_BindingList.ReplaceList((object[])data[9]);

                    _operator_ComboItemList_BindingList.IsRefreshingData = false;

                    // OwnerShipType_ComboItemList
                    _ownerShipType_ComboItemList_BindingList.IsRefreshingData = true;
                    _ownerShipType_ComboItemList_BindingList.CachedList.Clear();
                    _ownerShipType_ComboItemList_BindingList.Clear();
                    _ownerShipType_ComboItemList_BindingList.ReplaceList((object[])data[10]);

                    _ownerShipType_ComboItemList_BindingList.IsRefreshingData = false;

                    // Party_ComboItemList
                    _party_ComboItemList_BindingList.IsRefreshingData = true;
                    _party_ComboItemList_BindingList.CachedList.Clear();
                    _party_ComboItemList_BindingList.Clear();
                    _party_ComboItemList_BindingList.ReplaceList((object[])data[11]);

                    _party_ComboItemList_BindingList.IsRefreshingData = false;

                    // RangeDirection_ComboItemList
                    _rangeDirection_ComboItemList_BindingList.IsRefreshingData = true;
                    _rangeDirection_ComboItemList_BindingList.CachedList.Clear();
                    _rangeDirection_ComboItemList_BindingList.Clear();
                    _rangeDirection_ComboItemList_BindingList.ReplaceList((object[])data[12]);

                    _rangeDirection_ComboItemList_BindingList.IsRefreshingData = false;

                    // RecordingType_ComboItemList
                    _recordingType_ComboItemList_BindingList.IsRefreshingData = true;
                    _recordingType_ComboItemList_BindingList.CachedList.Clear();
                    _recordingType_ComboItemList_BindingList.Clear();
                    _recordingType_ComboItemList_BindingList.ReplaceList((object[])data[13]);

                    _recordingType_ComboItemList_BindingList.IsRefreshingData = false;

                    // Report_lstRptExplorer
                    _report_lstRptExplorer_BindingList.Clear();
                    if (data[14] != null)
                    {
                        for (int i = 0; i <= ((object[])data[14]).GetUpperBound(0); i++)
                        {
                            _report_lstRptExplorer_BindingList.Add(new Report_lstRptExplorer_Binding((object[])((object[])data[14])[i]));
                        }
                    }


                    // ReportGroup_lstRptExplorer
                    _reportGroup_lstRptExplorer_BindingList.Clear();
                    if (data[15] != null)
                    {
                        for (int i = 0; i <= ((object[])data[15]).GetUpperBound(0); i++)
                        {
                            _reportGroup_lstRptExplorer_BindingList.Add(new ReportGroup_lstRptExplorer_Binding((object[])((object[])data[15])[i]));
                        }
                    }


                    // State_ComboItemList
                    _state_ComboItemList_BindingList.IsRefreshingData = true;
                    _state_ComboItemList_BindingList.CachedList.Clear();
                    _state_ComboItemList_BindingList.Clear();
                    _state_ComboItemList_BindingList.ReplaceList((object[])data[16]);

                    _state_ComboItemList_BindingList.IsRefreshingData = false;

                    // StateCounty_ComboItemList
                    _stateCounty_ComboItemList_BindingList.IsRefreshingData = true;
                    _stateCounty_ComboItemList_BindingList.CachedList.Clear();
                    _stateCounty_ComboItemList_BindingList.Clear();
                    _stateCounty_ComboItemList_BindingList.ReplaceList((object[])data[17]);

                    _stateCounty_ComboItemList_BindingList.IsRefreshingData = false;

                    // TownshipDirection_ComboItemList
                    _townshipDirection_ComboItemList_BindingList.IsRefreshingData = true;
                    _townshipDirection_ComboItemList_BindingList.CachedList.Clear();
                    _townshipDirection_ComboItemList_BindingList.Clear();
                    _townshipDirection_ComboItemList_BindingList.ReplaceList((object[])data[18]);

                    _townshipDirection_ComboItemList_BindingList.IsRefreshingData = false;

                    // v_SysDotNetDataType_ComboItemList
                    _v_SysDotNetDataType_ComboItemList_BindingList.IsRefreshingData = true;
                    _v_SysDotNetDataType_ComboItemList_BindingList.CachedList.Clear();
                    _v_SysDotNetDataType_ComboItemList_BindingList.Clear();
                    _v_SysDotNetDataType_ComboItemList_BindingList.ReplaceList((object[])data[19]);

                    _v_SysDotNetDataType_ComboItemList_BindingList.IsRefreshingData = false;

                    // v_SysSqlDataType_ComboItemList
                    _v_SysSqlDataType_ComboItemList_BindingList.IsRefreshingData = true;
                    _v_SysSqlDataType_ComboItemList_BindingList.CachedList.Clear();
                    _v_SysSqlDataType_ComboItemList_BindingList.Clear();
                    _v_SysSqlDataType_ComboItemList_BindingList.ReplaceList((object[])data[20]);

                    _v_SysSqlDataType_ComboItemList_BindingList.IsRefreshingData = false;
                    isDataLoaded = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Load Data


        #region Properties

        public static bool IsDataLoaded
        {
            get { return isDataLoaded; }
        }

                    // Addition_ComboItemList
        public static ComboItemList Addition_ComboItemList_BindingListProperty
        {
            get
            {
                return _addition_ComboItemList_BindingList;
            }
            set
            {
                _addition_ComboItemList_BindingList = value;
            }
        }

                    // AgencyType_ComboItemList
        public static ComboItemList AgencyType_ComboItemList_BindingListProperty
        {
            get
            {
                return _agencyType_ComboItemList_BindingList;
            }
            set
            {
                _agencyType_ComboItemList_BindingList = value;
            }
        }

                    // Area_ComboItemList
        public static ComboItemList Area_ComboItemList_BindingListProperty
        {
            get
            {
                return _area_ComboItemList_BindingList;
            }
            set
            {
                _area_ComboItemList_BindingList = value;
            }
        }

                    // Block_ComboItemList
        public static ComboItemList Block_ComboItemList_BindingListProperty
        {
            get
            {
                return _block_ComboItemList_BindingList;
            }
            set
            {
                _block_ComboItemList_BindingList = value;
            }
        }

                    // County_ComboItemList
        public static ComboItemList County_ComboItemList_BindingListProperty
        {
            get
            {
                return _county_ComboItemList_BindingList;
            }
            set
            {
                _county_ComboItemList_BindingList = value;
            }
        }

                    // DataStatus_ComboItemList
        public static ComboItemList DataStatus_ComboItemList_BindingListProperty
        {
            get
            {
                return _dataStatus_ComboItemList_BindingList;
            }
            set
            {
                _dataStatus_ComboItemList_BindingList = value;
            }
        }

                    // Field_ComboItemList
        public static ComboItemList Field_ComboItemList_BindingListProperty
        {
            get
            {
                return _field_ComboItemList_BindingList;
            }
            set
            {
                _field_ComboItemList_BindingList = value;
            }
        }

                    // FlagStatus_ComboItemList
        public static ComboItemList FlagStatus_ComboItemList_BindingListProperty
        {
            get
            {
                return _flagStatus_ComboItemList_BindingList;
            }
            set
            {
                _flagStatus_ComboItemList_BindingList = value;
            }
        }

                    // Formation_ComboItemList
        public static ComboItemList Formation_ComboItemList_BindingListProperty
        {
            get
            {
                return _formation_ComboItemList_BindingList;
            }
            set
            {
                _formation_ComboItemList_BindingList = value;
            }
        }

                    // Operator_ComboItemList
        public static ComboItemList Operator_ComboItemList_BindingListProperty
        {
            get
            {
                return _operator_ComboItemList_BindingList;
            }
            set
            {
                _operator_ComboItemList_BindingList = value;
            }
        }

                    // OwnerShipType_ComboItemList
        public static ComboItemList OwnerShipType_ComboItemList_BindingListProperty
        {
            get
            {
                return _ownerShipType_ComboItemList_BindingList;
            }
            set
            {
                _ownerShipType_ComboItemList_BindingList = value;
            }
        }

                    // Party_ComboItemList
        public static ComboItemList Party_ComboItemList_BindingListProperty
        {
            get
            {
                return _party_ComboItemList_BindingList;
            }
            set
            {
                _party_ComboItemList_BindingList = value;
            }
        }

                    // RangeDirection_ComboItemList
        public static ComboItemList RangeDirection_ComboItemList_BindingListProperty
        {
            get
            {
                return _rangeDirection_ComboItemList_BindingList;
            }
            set
            {
                _rangeDirection_ComboItemList_BindingList = value;
            }
        }

                    // RecordingType_ComboItemList
        public static ComboItemList RecordingType_ComboItemList_BindingListProperty
        {
            get
            {
                return _recordingType_ComboItemList_BindingList;
            }
            set
            {
                _recordingType_ComboItemList_BindingList = value;
            }
        }

                    // Report_lstRptExplorer
        public static ObservableCollection<Report_lstRptExplorer_Binding> Report_lstRptExplorer_BindingListProperty
        {
            get
            {
                return _report_lstRptExplorer_BindingList;
            }
            set
            {
                _report_lstRptExplorer_BindingList = value;
            }
        }

                    // ReportGroup_lstRptExplorer
        public static ObservableCollection<ReportGroup_lstRptExplorer_Binding> ReportGroup_lstRptExplorer_BindingListProperty
        {
            get
            {
                return _reportGroup_lstRptExplorer_BindingList;
            }
            set
            {
                _reportGroup_lstRptExplorer_BindingList = value;
            }
        }

                    // State_ComboItemList
        public static ComboItemList State_ComboItemList_BindingListProperty
        {
            get
            {
                return _state_ComboItemList_BindingList;
            }
            set
            {
                _state_ComboItemList_BindingList = value;
            }
        }

                    // StateCounty_ComboItemList
        public static ComboItemList StateCounty_ComboItemList_BindingListProperty
        {
            get
            {
                return _stateCounty_ComboItemList_BindingList;
            }
            set
            {
                _stateCounty_ComboItemList_BindingList = value;
            }
        }

                    // TownshipDirection_ComboItemList
        public static ComboItemList TownshipDirection_ComboItemList_BindingListProperty
        {
            get
            {
                return _townshipDirection_ComboItemList_BindingList;
            }
            set
            {
                _townshipDirection_ComboItemList_BindingList = value;
            }
        }

                    // v_SysDotNetDataType_ComboItemList
        public static ComboItemList v_SysDotNetDataType_ComboItemList_BindingListProperty
        {
            get
            {
                return _v_SysDotNetDataType_ComboItemList_BindingList;
            }
            set
            {
                _v_SysDotNetDataType_ComboItemList_BindingList = value;
            }
        }

                    // v_SysSqlDataType_ComboItemList
        public static ComboItemList v_SysSqlDataType_ComboItemList_BindingListProperty
        {
            get
            {
                return _v_SysSqlDataType_ComboItemList_BindingList;
            }
            set
            {
                _v_SysSqlDataType_ComboItemList_BindingList = value;
            }
        }

        #endregion Properties


        #region RefreshLists



        #region Addition_ComboItemList

        public static void Refresh_Addition_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAddition_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetAddition_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAddition_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAddition_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetAddition_ComboItemListCompleted(object sender, GetAddition_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAddition_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _addition_ComboItemList_BindingList.IsRefreshingData = true;
                _addition_ComboItemList_BindingList.ReplaceList(data);
                _addition_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAddition_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAddition_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Addition_ComboItemList



        #region AgencyType_ComboItemList

        public static void Refresh_AgencyType_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAgencyType_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetAgencyType_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAgencyType_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAgencyType_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetAgencyType_ComboItemListCompleted(object sender, GetAgencyType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAgencyType_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _agencyType_ComboItemList_BindingList.IsRefreshingData = true;
                _agencyType_ComboItemList_BindingList.ReplaceList(data);
                _agencyType_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAgencyType_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAgencyType_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion AgencyType_ComboItemList



        #region Area_ComboItemList

        public static void Refresh_Area_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetArea_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetArea_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetArea_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetArea_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetArea_ComboItemListCompleted(object sender, GetArea_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetArea_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _area_ComboItemList_BindingList.IsRefreshingData = true;
                _area_ComboItemList_BindingList.ReplaceList(data);
                _area_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetArea_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetArea_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Area_ComboItemList



        #region Block_ComboItemList

        public static void Refresh_Block_ComboItemList_BindingList(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBlock_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetBlock_ComboItemList(Prj_Id );

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBlock_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBlock_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetBlock_ComboItemListCompleted(object sender, GetBlock_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBlock_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _block_ComboItemList_BindingList.IsRefreshingData = true;
                _block_ComboItemList_BindingList.ReplaceList(data);
                _block_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBlock_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBlock_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Block_ComboItemList



        #region County_ComboItemList

        public static void Refresh_County_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCounty_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetCounty_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCounty_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCounty_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetCounty_ComboItemListCompleted(object sender, GetCounty_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCounty_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _county_ComboItemList_BindingList.IsRefreshingData = true;
                _county_ComboItemList_BindingList.ReplaceList(data);
                _county_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCounty_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCounty_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion County_ComboItemList



        #region DataStatus_ComboItemList

        public static void Refresh_DataStatus_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataStatus_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetDataStatus_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataStatus_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataStatus_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetDataStatus_ComboItemListCompleted(object sender, GetDataStatus_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataStatus_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _dataStatus_ComboItemList_BindingList.IsRefreshingData = true;
                _dataStatus_ComboItemList_BindingList.ReplaceList(data);
                _dataStatus_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataStatus_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataStatus_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion DataStatus_ComboItemList



        #region Field_ComboItemList

        public static void Refresh_Field_ComboItemList_BindingList(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetField_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetField_ComboItemList(Prj_Id );

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetField_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetField_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetField_ComboItemListCompleted(object sender, GetField_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetField_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _field_ComboItemList_BindingList.IsRefreshingData = true;
                _field_ComboItemList_BindingList.ReplaceList(data);
                _field_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetField_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetField_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Field_ComboItemList



        #region FlagStatus_ComboItemList

        public static void Refresh_FlagStatus_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFlagStatus_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetFlagStatus_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFlagStatus_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFlagStatus_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetFlagStatus_ComboItemListCompleted(object sender, GetFlagStatus_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFlagStatus_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _flagStatus_ComboItemList_BindingList.IsRefreshingData = true;
                _flagStatus_ComboItemList_BindingList.ReplaceList(data);
                _flagStatus_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFlagStatus_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFlagStatus_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion FlagStatus_ComboItemList



        #region Formation_ComboItemList

        public static void Refresh_Formation_ComboItemList_BindingList(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFormation_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetFormation_ComboItemList(Prj_Id );

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFormation_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFormation_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetFormation_ComboItemListCompleted(object sender, GetFormation_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFormation_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _formation_ComboItemList_BindingList.IsRefreshingData = true;
                _formation_ComboItemList_BindingList.ReplaceList(data);
                _formation_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFormation_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFormation_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Formation_ComboItemList



        #region Operator_ComboItemList

        public static void Refresh_Operator_ComboItemList_BindingList(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOperator_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetOperator_ComboItemList(Prj_Id );

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOperator_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOperator_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetOperator_ComboItemListCompleted(object sender, GetOperator_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOperator_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _operator_ComboItemList_BindingList.IsRefreshingData = true;
                _operator_ComboItemList_BindingList.ReplaceList(data);
                _operator_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOperator_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOperator_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Operator_ComboItemList



        #region OwnerShipType_ComboItemList

        public static void Refresh_OwnerShipType_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOwnerShipType_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetOwnerShipType_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOwnerShipType_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOwnerShipType_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetOwnerShipType_ComboItemListCompleted(object sender, GetOwnerShipType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOwnerShipType_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _ownerShipType_ComboItemList_BindingList.IsRefreshingData = true;
                _ownerShipType_ComboItemList_BindingList.ReplaceList(data);
                _ownerShipType_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOwnerShipType_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOwnerShipType_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion OwnerShipType_ComboItemList



        #region Party_ComboItemList

        public static void Refresh_Party_ComboItemList_BindingList(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParty_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetParty_ComboItemList(Prj_Id );

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParty_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParty_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetParty_ComboItemListCompleted(object sender, GetParty_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParty_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _party_ComboItemList_BindingList.IsRefreshingData = true;
                _party_ComboItemList_BindingList.ReplaceList(data);
                _party_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParty_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParty_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Party_ComboItemList



        #region RangeDirection_ComboItemList

        public static void Refresh_RangeDirection_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRangeDirection_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetRangeDirection_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRangeDirection_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRangeDirection_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetRangeDirection_ComboItemListCompleted(object sender, GetRangeDirection_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRangeDirection_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _rangeDirection_ComboItemList_BindingList.IsRefreshingData = true;
                _rangeDirection_ComboItemList_BindingList.ReplaceList(data);
                _rangeDirection_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRangeDirection_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRangeDirection_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion RangeDirection_ComboItemList



        #region RecordingType_ComboItemList

        public static void Refresh_RecordingType_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRecordingType_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetRecordingType_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRecordingType_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRecordingType_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetRecordingType_ComboItemListCompleted(object sender, GetRecordingType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRecordingType_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _recordingType_ComboItemList_BindingList.IsRefreshingData = true;
                _recordingType_ComboItemList_BindingList.ReplaceList(data);
                _recordingType_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRecordingType_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRecordingType_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion RecordingType_ComboItemList



        #region Report_lstRptExplorer

        public static void Refresh_Report_lstRptExplorer_BindingList(Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReport_lstRptExplorer_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetReport_lstRptExplorer(UserId );

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReport_lstRptExplorer_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReport_lstRptExplorer_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetReport_lstRptExplorerCompleted(object sender, GetReport_lstRptExplorerCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReport_lstRptExplorerCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];

                _report_lstRptExplorer_BindingList.Clear();
                for (int i = 0; i <= (data).GetUpperBound(0); i++)
                {
                    _report_lstRptExplorer_BindingList.Add(new Report_lstRptExplorer_Binding((object[])data[i]));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReport_lstRptExplorerCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReport_lstRptExplorerCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Report_lstRptExplorer



        #region ReportGroup_lstRptExplorer

        public static void Refresh_ReportGroup_lstRptExplorer_BindingList(Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportGroup_lstRptExplorer_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetReportGroup_lstRptExplorer(UserId );

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportGroup_lstRptExplorer_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportGroup_lstRptExplorer_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetReportGroup_lstRptExplorerCompleted(object sender, GetReportGroup_lstRptExplorerCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportGroup_lstRptExplorerCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];

                _reportGroup_lstRptExplorer_BindingList.Clear();
                for (int i = 0; i <= (data).GetUpperBound(0); i++)
                {
                    _reportGroup_lstRptExplorer_BindingList.Add(new ReportGroup_lstRptExplorer_Binding((object[])data[i]));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportGroup_lstRptExplorerCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportGroup_lstRptExplorerCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion ReportGroup_lstRptExplorer



        #region State_ComboItemList

        public static void Refresh_State_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetState_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetState_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetState_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetState_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetState_ComboItemListCompleted(object sender, GetState_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetState_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _state_ComboItemList_BindingList.IsRefreshingData = true;
                _state_ComboItemList_BindingList.ReplaceList(data);
                _state_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetState_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetState_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion State_ComboItemList



        #region StateCounty_ComboItemList

        public static void Refresh_StateCounty_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStateCounty_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetStateCounty_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStateCounty_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStateCounty_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetStateCounty_ComboItemListCompleted(object sender, GetStateCounty_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStateCounty_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _stateCounty_ComboItemList_BindingList.IsRefreshingData = true;
                _stateCounty_ComboItemList_BindingList.ReplaceList(data);
                _stateCounty_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStateCounty_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStateCounty_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion StateCounty_ComboItemList



        #region TownshipDirection_ComboItemList

        public static void Refresh_TownshipDirection_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTownshipDirection_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_GetTownshipDirection_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTownshipDirection_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTownshipDirection_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetTownshipDirection_ComboItemListCompleted(object sender, GetTownshipDirection_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTownshipDirection_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _townshipDirection_ComboItemList_BindingList.IsRefreshingData = true;
                _townshipDirection_ComboItemList_BindingList.ReplaceList(data);
                _townshipDirection_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTownshipDirection_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTownshipDirection_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TownshipDirection_ComboItemList



        #region v_SysDotNetDataType_ComboItemList

        public static void Refresh_v_SysDotNetDataType_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysDotNetDataType_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_Getv_SysDotNetDataType_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysDotNetDataType_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysDotNetDataType_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void Getv_SysDotNetDataType_ComboItemListCompleted(object sender, Getv_SysDotNetDataType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysDotNetDataType_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _v_SysDotNetDataType_ComboItemList_BindingList.IsRefreshingData = true;
                _v_SysDotNetDataType_ComboItemList_BindingList.ReplaceList(data);
                _v_SysDotNetDataType_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysDotNetDataType_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysDotNetDataType_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion v_SysDotNetDataType_ComboItemList



        #region v_SysSqlDataType_ComboItemList

        public static void Refresh_v_SysSqlDataType_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysSqlDataType_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticCommonClientDataProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticCommonClientDataProxy.Begin_Getv_SysSqlDataType_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysSqlDataType_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysSqlDataType_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void Getv_SysSqlDataType_ComboItemListCompleted(object sender, Getv_SysSqlDataType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysSqlDataType_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _v_SysSqlDataType_ComboItemList_BindingList.IsRefreshingData = true;
                _v_SysSqlDataType_ComboItemList_BindingList.ReplaceList(data);
                _v_SysSqlDataType_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysSqlDataType_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysSqlDataType_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion v_SysSqlDataType_ComboItemList



        #endregion RefreshLists


    }

    #region Classes for Binding

    #region Report_lstRptExplorer_Binding

    public partial class Report_lstRptExplorer_Binding_List : ObservableCollection<Report_lstRptExplorer_Binding>
    {
        private static string _as = "CommonClientData";
        private static string _cn = "Report_lstRptExplorer_Binding_List";

        public Report_lstRptExplorer_Binding_List()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Report_lstRptExplorer_Binding_List", IfxTraceCategory.Enter);
                if (CommonClientData_Bll_staticLists.Report_lstRptExplorer_BindingListProperty == null)
                {
                    Clear();
                    return;
                }
                if (CommonClientData_Bll_staticLists.Report_lstRptExplorer_BindingListProperty.Count == 0)
                {
                    Clear();
                    return;
                }

                foreach (Report_lstRptExplorer_Binding obj in CommonClientData_Bll_staticLists.Report_lstRptExplorer_BindingListProperty)
                {
                    base.Add(obj);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Report_lstRptExplorer_Binding_List", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Report_lstRptExplorer_Binding_List", IfxTraceCategory.Leave);
            }
        }

        public void ReplaceList(object[] list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceList", IfxTraceCategory.Enter);
                base.Clear();
                for (int i = 0; i <= list.GetUpperBound(0); i++)
                {
                    base.Add(new Report_lstRptExplorer_Binding((object[])list[i]));
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceList", IfxTraceCategory.Leave);
            }
        }
    }

    #endregion Report_lstRptExplorer_Binding

    #region ReportGroup_lstRptExplorer_Binding

    public partial class ReportGroup_lstRptExplorer_Binding_List : ObservableCollection<ReportGroup_lstRptExplorer_Binding>
    {
        private static string _as = "CommonClientData";
        private static string _cn = "ReportGroup_lstRptExplorer_Binding_List";

        public ReportGroup_lstRptExplorer_Binding_List()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReportGroup_lstRptExplorer_Binding_List", IfxTraceCategory.Enter);
                if (CommonClientData_Bll_staticLists.ReportGroup_lstRptExplorer_BindingListProperty == null)
                {
                    Clear();
                    return;
                }
                if (CommonClientData_Bll_staticLists.ReportGroup_lstRptExplorer_BindingListProperty.Count == 0)
                {
                    Clear();
                    return;
                }

                foreach (ReportGroup_lstRptExplorer_Binding obj in CommonClientData_Bll_staticLists.ReportGroup_lstRptExplorer_BindingListProperty)
                {
                    base.Add(obj);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReportGroup_lstRptExplorer_Binding_List", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReportGroup_lstRptExplorer_Binding_List", IfxTraceCategory.Leave);
            }
        }

        public void ReplaceList(object[] list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceList", IfxTraceCategory.Enter);
                base.Clear();
                for (int i = 0; i <= list.GetUpperBound(0); i++)
                {
                    base.Add(new ReportGroup_lstRptExplorer_Binding((object[])list[i]));
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceList", IfxTraceCategory.Leave);
            }
        }
    }

    #endregion ReportGroup_lstRptExplorer_Binding

#endregion Classes for Binding

}

