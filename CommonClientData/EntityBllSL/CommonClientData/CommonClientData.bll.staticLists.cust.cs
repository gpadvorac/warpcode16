﻿using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;
using vDataType;

// Gen Timestamp:  12/14/2011 8:27:18 PM

namespace EntityBll.SL
{

    public partial class CommonClientData_Bll_staticLists
    {

        #region Initialize Variables

        // We could not codegen the FK parameters becuase the associated entity ID was CommonClientData 
        /// <summary>
        /// The extra parameter passed in here sets a flag which determins this is a child list.
        /// Then the cached list will be populated which is needed for this to be filtered by the parent ID
        /// </summary>
        private static ComboItemList _county_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);
        private static bool _county_ComboItemList_BindingList_HasItems = true;

        private static ObservableCollection<SimpleDataType> _simpleDataType_BindingList = new ObservableCollection<SimpleDataType>();
        private static ObservableCollection<DotNetDataType> _dotNetDataType_BindingList = new ObservableCollection<DotNetDataType>();
        private static ObservableCollection<SqlDataType> _sqlDataType_BindingList = new ObservableCollection<SqlDataType>();

        //public event System.EventHandler<GetCommonClientData_ReadOnlyStaticLists_AllCompletedEventArgs> GetCommonClientData_ReadOnlyStaticLists_AllCompleted;


        #endregion Initialize Variables


        //public static void InitializeProxyWrapper()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Enter);

        //        if (_staticCommonClientDataProxy == null)
        //        {
        //            _staticCommonClientDataProxy = new ProxyWrapper.CommonClientDataService_ProxyWrapper();
        //            _staticCommonClientDataProxy.GetCommonClientData_ReadOnlyStaticListsCompleted += new EventHandler<GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs>(GetCommonClientData_ReadOnlyStaticListsCompleted);
        //            _staticCommonClientDataProxy.GetAddition_ComboItemListCompleted += new EventHandler<GetAddition_ComboItemListCompletedEventArgs>(GetAddition_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetAgencyType_ComboItemListCompleted += new EventHandler<GetAgencyType_ComboItemListCompletedEventArgs>(GetAgencyType_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetArea_ComboItemListCompleted += new EventHandler<GetArea_ComboItemListCompletedEventArgs>(GetArea_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetBlock_ComboItemListCompleted += new EventHandler<GetBlock_ComboItemListCompletedEventArgs>(GetBlock_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetCounty_ComboItemListCompleted += new EventHandler<GetCounty_ComboItemListCompletedEventArgs>(GetCounty_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetDataStatus_ComboItemListCompleted += new EventHandler<GetDataStatus_ComboItemListCompletedEventArgs>(GetDataStatus_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetField_ComboItemListCompleted += new EventHandler<GetField_ComboItemListCompletedEventArgs>(GetField_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetFlagStatus_ComboItemListCompleted += new EventHandler<GetFlagStatus_ComboItemListCompletedEventArgs>(GetFlagStatus_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetFormation_ComboItemListCompleted += new EventHandler<GetFormation_ComboItemListCompletedEventArgs>(GetFormation_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetOperator_ComboItemListCompleted += new EventHandler<GetOperator_ComboItemListCompletedEventArgs>(GetOperator_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetOwnerShipType_ComboItemListCompleted += new EventHandler<GetOwnerShipType_ComboItemListCompletedEventArgs>(GetOwnerShipType_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetParty_ComboItemListCompleted += new EventHandler<GetParty_ComboItemListCompletedEventArgs>(GetParty_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetRangeDirection_ComboItemListCompleted += new EventHandler<GetRangeDirection_ComboItemListCompletedEventArgs>(GetRangeDirection_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetRecordingType_ComboItemListCompleted += new EventHandler<GetRecordingType_ComboItemListCompletedEventArgs>(GetRecordingType_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetState_ComboItemListCompleted += new EventHandler<GetState_ComboItemListCompletedEventArgs>(GetState_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetStateCounty_ComboItemListCompleted += new EventHandler<GetStateCounty_ComboItemListCompletedEventArgs>(GetStateCounty_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.GetTownshipDirection_ComboItemListCompleted += new EventHandler<GetTownshipDirection_ComboItemListCompletedEventArgs>(GetTownshipDirection_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.Getv_GridMetaData_lstCompleted += new EventHandler<Getv_GridMetaData_lstCompletedEventArgs>(Getv_GridMetaData_lstCompleted);
        //            _staticCommonClientDataProxy.Getv_Report_lstRptExplorerCompleted += new EventHandler<Getv_Report_lstRptExplorerCompletedEventArgs>(Getv_Report_lstRptExplorerCompleted);
        //            _staticCommonClientDataProxy.Getv_Report_lstAvailableForExplorerCompleted += new EventHandler<Getv_Report_lstAvailableForExplorerCompletedEventArgs>(Getv_Report_lstAvailableForExplorerCompleted);
        //            _staticCommonClientDataProxy.Getv_Report_lstRptExplorer_RestrictedCompleted += new EventHandler<Getv_Report_lstRptExplorer_RestrictedCompletedEventArgs>(Getv_Report_lstRptExplorer_RestrictedCompleted);
        //            _staticCommonClientDataProxy.Getv_ReportGroup_lstRptExplorerCompleted += new EventHandler<Getv_ReportGroup_lstRptExplorerCompletedEventArgs>(Getv_ReportGroup_lstRptExplorerCompleted);
        //            _staticCommonClientDataProxy.Getv_ReportGroup_lstRptExplorer_RestrictedCompleted += new EventHandler<Getv_ReportGroup_lstRptExplorer_RestrictedCompletedEventArgs>(Getv_ReportGroup_lstRptExplorer_RestrictedCompleted);
        //            _staticCommonClientDataProxy.Getv_SysDotNetDataType_ComboItemListCompleted += new EventHandler<Getv_SysDotNetDataType_ComboItemListCompletedEventArgs>(Getv_SysDotNetDataType_ComboItemListCompleted);
        //            _staticCommonClientDataProxy.Getv_SysSqlDataType_ComboItemListCompleted += new EventHandler<Getv_SysSqlDataType_ComboItemListCompletedEventArgs>(Getv_SysSqlDataType_ComboItemListCompleted);


        //            //_staticCommonClientDataProxy.GetCommonClientData_ReadOnlyStaticLists_AllCompleted += new EventHandler<GetCommonClientData_ReadOnlyStaticLists_AllCompletedEventArgs>(GetCommonClientData_ReadOnlyStaticLists_AllCompleted);

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Leave);
        //    }
        //}
        
        static void LoadSimpleDataTypeList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadSimpleDataTypeList", IfxTraceCategory.Enter);
                _simpleDataType_BindingList.Add(new SimpleDataType(SimpleDataTypeName.None));
                _simpleDataType_BindingList.Add(new SimpleDataType(SimpleDataTypeName.Integer));
                _simpleDataType_BindingList.Add(new SimpleDataType(SimpleDataTypeName.Guid));
                _simpleDataType_BindingList.Add(new SimpleDataType(SimpleDataTypeName.String));
                _simpleDataType_BindingList.Add(new SimpleDataType(SimpleDataTypeName.DateTime));
                _simpleDataType_BindingList.Add(new SimpleDataType(SimpleDataTypeName.Double));
                _simpleDataType_BindingList.Add(new SimpleDataType(SimpleDataTypeName.Boolean));
                _simpleDataType_BindingList.Add(new SimpleDataType(SimpleDataTypeName.Time));
                _simpleDataType_BindingList.Add(new SimpleDataType(SimpleDataTypeName.Object));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadSimpleDataTypeList", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadSimpleDataTypeList", IfxTraceCategory.Leave);
            }
        }

        static void LoadDotNameDataTypeList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadDotNameDataTypeList", IfxTraceCategory.Enter);
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.Boolean));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.Byte));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.Char));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.String));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.Decimal));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.Single));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.Double));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.SByte));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.Int16));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.Int32));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.Int64));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.Guid));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.DateTime));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.TimeSpan));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.Object));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.ByteArray));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.CharArray));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.StringArray));
                _dotNetDataType_BindingList.Add(new DotNetDataType(DotNetDataTypeName.ObjectArray));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadDotNameDataTypeList", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadDotNameDataTypeList", IfxTraceCategory.Leave);
            }
        }

        static void LoadSqlDataTypeList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadSqlDataTypeList", IfxTraceCategory.Enter);

                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.BigInt));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Binary));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Bit));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Char));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.DateTime));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Decimal));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Float));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Image));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Int));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Money));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.NChar));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.NText));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.NVarChar));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Real));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.UniqueIdentifier));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.SmallDateTime));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.SmallInt));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.SmallMoney));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Text));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Timestamp));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.TinyInt));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.VarBinary));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.VarChar));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Variant));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Xml));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Udt));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Structured));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Date));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.Time));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.DateTime2));
                _sqlDataType_BindingList.Add(new SqlDataType(SqlDataTypeName.DateTimeOffset));

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadSqlDataTypeList", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadSqlDataTypeList", IfxTraceCategory.Leave);
            }
        }
        
        public static ObservableCollection<SimpleDataType> SimpleDataType_BindingList
        {
            get { return CommonClientData_Bll_staticLists._simpleDataType_BindingList; }
            set { CommonClientData_Bll_staticLists._simpleDataType_BindingList = value; }
        }

        public static ObservableCollection<DotNetDataType> DotNetDataType_BindingList
        {
            get { return CommonClientData_Bll_staticLists._dotNetDataType_BindingList; }
            set { CommonClientData_Bll_staticLists._dotNetDataType_BindingList = value; }
        }

        public static ObservableCollection<SqlDataType> SqlDataType_BindingList
        {
            get { return CommonClientData_Bll_staticLists._sqlDataType_BindingList; }
            set { CommonClientData_Bll_staticLists._sqlDataType_BindingList = value; }
        }
        
        static void LoadStaticLists_Custom()
    {
        Guid? traceId = Guid.NewGuid();
        try
        {
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", IfxTraceCategory.Enter);

            //_v_Report_lstRptExplorer_Restricted_BindingList = null;
            //_v_Report_lstRptExplorer_Restricted_BindingList = new ObservableCollection<v_Report_lstRptExplorer_Restricted_Binding>();

        
        }
        catch (Exception ex)
        {
            if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", ex);
            throw IfxWrapperException.GetError(ex, (Guid)traceId);
        }
        finally
        {
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", IfxTraceCategory.Leave);
        }
    }

        //public static void LoadStaticLists_All(Guid Prj_Id, Guid UserId)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Enter);

        //        if (_staticCommonClientDataProxy == null)
        //        {
        //            InitializeProxyWrapper();
        //        }

        //        _staticCommonClientDataProxy.Begin_GetCommonClientData_ReadOnlyStaticLists_All(Prj_Id, UserId);

        //        LoadStaticLists_Custom();
        //        isDataLoaded = false;

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Leave);
        //    }
        //}
        
        static void GetCommonClientData_ReadOnlyStaticListsCompleted(object sender, GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                if (data != null)
                {

                    GetCommonClientData_ReadOnlyStaticListsLoadData(data);

                    //// Addition_ComboItemList
                    //_addition_ComboItemList_BindingList.IsRefreshingData = true;
                    //_addition_ComboItemList_BindingList.CachedList.Clear();
                    //_addition_ComboItemList_BindingList.Clear();
                    //_addition_ComboItemList_BindingList.ReplaceList((object[])data[0]);

                    //_addition_ComboItemList_BindingList.IsRefreshingData = false;

                    //// AgencyType_ComboItemList
                    //_agencyType_ComboItemList_BindingList.IsRefreshingData = true;
                    //_agencyType_ComboItemList_BindingList.CachedList.Clear();
                    //_agencyType_ComboItemList_BindingList.Clear();
                    //_agencyType_ComboItemList_BindingList.ReplaceList((object[])data[1]);

                    //_agencyType_ComboItemList_BindingList.IsRefreshingData = false;

                    //// Area_ComboItemList
                    //_area_ComboItemList_BindingList.IsRefreshingData = true;
                    //_area_ComboItemList_BindingList.CachedList.Clear();
                    //_area_ComboItemList_BindingList.Clear();
                    //_area_ComboItemList_BindingList.ReplaceList((object[])data[2]);

                    //_area_ComboItemList_BindingList.IsRefreshingData = false;

                    //// Block_ComboItemList
                    //_block_ComboItemList_BindingList.IsRefreshingData = true;
                    //_block_ComboItemList_BindingList.CachedList.Clear();
                    //_block_ComboItemList_BindingList.Clear();
                    //_block_ComboItemList_BindingList.ReplaceList((object[])data[3]);

                    //_block_ComboItemList_BindingList.IsRefreshingData = false;

                    //// County_ComboItemList
                    //_county_ComboItemList_BindingList.IsRefreshingData = true;
                    //_county_ComboItemList_BindingList.CachedList.Clear();
                    //_county_ComboItemList_BindingList.Clear();
                    //_county_ComboItemList_BindingList.ReplaceList((object[])data[4]);

                    //_county_ComboItemList_BindingList.IsRefreshingData = false;

                    //// DataStatus_ComboItemList
                    //_dataStatus_ComboItemList_BindingList.IsRefreshingData = true;
                    //_dataStatus_ComboItemList_BindingList.CachedList.Clear();
                    //_dataStatus_ComboItemList_BindingList.Clear();
                    //_dataStatus_ComboItemList_BindingList.ReplaceList((object[])data[5]);

                    //_dataStatus_ComboItemList_BindingList.IsRefreshingData = false;

                    //// Field_ComboItemList
                    //_field_ComboItemList_BindingList.IsRefreshingData = true;
                    //_field_ComboItemList_BindingList.CachedList.Clear();
                    //_field_ComboItemList_BindingList.Clear();
                    //_field_ComboItemList_BindingList.ReplaceList((object[])data[6]);

                    //_field_ComboItemList_BindingList.IsRefreshingData = false;

                    //// FlagStatus_ComboItemList
                    //_flagStatus_ComboItemList_BindingList.IsRefreshingData = true;
                    //_flagStatus_ComboItemList_BindingList.CachedList.Clear();
                    //_flagStatus_ComboItemList_BindingList.Clear();
                    //_flagStatus_ComboItemList_BindingList.ReplaceList((object[])data[7]);

                    //_flagStatus_ComboItemList_BindingList.IsRefreshingData = false;

                    //// Formation_ComboItemList
                    //_formation_ComboItemList_BindingList.IsRefreshingData = true;
                    //_formation_ComboItemList_BindingList.CachedList.Clear();
                    //_formation_ComboItemList_BindingList.Clear();
                    //_formation_ComboItemList_BindingList.ReplaceList((object[])data[8]);

                    //_formation_ComboItemList_BindingList.IsRefreshingData = false;

                    //// Operator_ComboItemList
                    //_operator_ComboItemList_BindingList.IsRefreshingData = true;
                    //_operator_ComboItemList_BindingList.CachedList.Clear();
                    //_operator_ComboItemList_BindingList.Clear();
                    //_operator_ComboItemList_BindingList.ReplaceList((object[])data[9]);

                    //_operator_ComboItemList_BindingList.IsRefreshingData = false;

                    //// OwnerShipType_ComboItemList
                    //_ownerShipType_ComboItemList_BindingList.IsRefreshingData = true;
                    //_ownerShipType_ComboItemList_BindingList.CachedList.Clear();
                    //_ownerShipType_ComboItemList_BindingList.Clear();
                    //_ownerShipType_ComboItemList_BindingList.ReplaceList((object[])data[10]);

                    //_ownerShipType_ComboItemList_BindingList.IsRefreshingData = false;

                    //// Party_ComboItemList
                    //_party_ComboItemList_BindingList.IsRefreshingData = true;
                    //_party_ComboItemList_BindingList.CachedList.Clear();
                    //_party_ComboItemList_BindingList.Clear();
                    //_party_ComboItemList_BindingList.ReplaceList((object[])data[11]);

                    //_party_ComboItemList_BindingList.IsRefreshingData = false;

                    //// RangeDirection_ComboItemList
                    //_rangeDirection_ComboItemList_BindingList.IsRefreshingData = true;
                    //_rangeDirection_ComboItemList_BindingList.CachedList.Clear();
                    //_rangeDirection_ComboItemList_BindingList.Clear();
                    //_rangeDirection_ComboItemList_BindingList.ReplaceList((object[])data[12]);

                    //_rangeDirection_ComboItemList_BindingList.IsRefreshingData = false;

                    //// RecordingType_ComboItemList
                    //_recordingType_ComboItemList_BindingList.IsRefreshingData = true;
                    //_recordingType_ComboItemList_BindingList.CachedList.Clear();
                    //_recordingType_ComboItemList_BindingList.Clear();
                    //_recordingType_ComboItemList_BindingList.ReplaceList((object[])data[13]);

                    //_recordingType_ComboItemList_BindingList.IsRefreshingData = false;

                    //// State_ComboItemList
                    //_state_ComboItemList_BindingList.IsRefreshingData = true;
                    //_state_ComboItemList_BindingList.CachedList.Clear();
                    //_state_ComboItemList_BindingList.Clear();
                    //_state_ComboItemList_BindingList.ReplaceList((object[])data[14]);

                    //_state_ComboItemList_BindingList.IsRefreshingData = false;

                    //// StateCounty_ComboItemList
                    //_stateCounty_ComboItemList_BindingList.IsRefreshingData = true;
                    //_stateCounty_ComboItemList_BindingList.CachedList.Clear();
                    //_stateCounty_ComboItemList_BindingList.Clear();
                    //_stateCounty_ComboItemList_BindingList.ReplaceList((object[])data[15]);

                    //_stateCounty_ComboItemList_BindingList.IsRefreshingData = false;

                    //// TownshipDirection_ComboItemList
                    //_townshipDirection_ComboItemList_BindingList.IsRefreshingData = true;
                    //_townshipDirection_ComboItemList_BindingList.CachedList.Clear();
                    //_townshipDirection_ComboItemList_BindingList.Clear();
                    //_townshipDirection_ComboItemList_BindingList.ReplaceList((object[])data[16]);

                    //_townshipDirection_ComboItemList_BindingList.IsRefreshingData = false;

                    //// v_GridMetaData_lst
                    //_v_GridMetaData_lst_BindingList.Clear();
                    //if (data[17] != null)
                    //{
                    //    for (int i = 0; i <= ((object[])data[17]).GetUpperBound(0); i++)
                    //    {
                    //        _v_GridMetaData_lst_BindingList.Add(new v_GridMetaData_lst_Binding((object[])((object[])data[17])[i]));
                    //    }
                    //}


                    //// v_Report_lstRptExplorer
                    //_v_Report_lstRptExplorer_BindingList.Clear();
                    //if (data[18] != null)
                    //{
                    //    for (int i = 0; i <= ((object[])data[18]).GetUpperBound(0); i++)
                    //    {
                    //        _v_Report_lstRptExplorer_BindingList.Add(new v_Report_lstRptExplorer_Binding((object[])((object[])data[18])[i]));
                    //    }
                    //}


                    //// v_Report_lstRptExplorer_AvailableForExplorer
                    //_v_Report_lstRptExplorer_AvailableForExplorer_BindingList.Clear();
                    //if (data[19] != null)
                    //{
                    //    for (int i = 0; i <= ((object[])data[19]).GetUpperBound(0); i++)
                    //    {
                    //        _v_Report_lstRptExplorer_AvailableForExplorer_BindingList.Add(new v_Report_lstRptExplorer_AvailableForExplorer_Binding((object[])((object[])data[19])[i]));
                    //    }
                    //}


                    //// v_Report_lstRptExplorer_Restricted
                    //_v_Report_lstRptExplorer_Restricted_BindingList.Clear();
                    //if (data[20] != null)
                    //{
                    //    for (int i = 0; i <= ((object[])data[20]).GetUpperBound(0); i++)
                    //    {
                    //        _v_Report_lstRptExplorer_Restricted_BindingList.Add(new v_Report_lstRptExplorer_Restricted_Binding((object[])((object[])data[20])[i]));
                    //    }
                    //}


                    //// v_ReportGroup_lstRptExplorer
                    //_v_ReportGroup_lstRptExplorer_BindingList.Clear();
                    //if (data[21] != null)
                    //{
                    //    for (int i = 0; i <= ((object[])data[21]).GetUpperBound(0); i++)
                    //    {
                    //        _v_ReportGroup_lstRptExplorer_BindingList.Add(new v_ReportGroup_lstRptExplorer_Binding((object[])((object[])data[21])[i]));
                    //    }
                    //}


                    //// v_ReportGroup_lstRptExplorer_Restricted
                    //_v_ReportGroup_lstRptExplorer_Restricted_BindingList.Clear();
                    //if (data[22] != null)
                    //{
                    //    for (int i = 0; i <= ((object[])data[22]).GetUpperBound(0); i++)
                    //    {
                    //        _v_ReportGroup_lstRptExplorer_Restricted_BindingList.Add(new v_ReportGroup_lstRptExplorer_Restricted_Binding((object[])((object[])data[22])[i]));
                    //    }
                    //}


                    //// v_SysDotNetDataType_ComboItemList
                    //_v_SysDotNetDataType_ComboItemList_BindingList.IsRefreshingData = true;
                    //_v_SysDotNetDataType_ComboItemList_BindingList.CachedList.Clear();
                    //_v_SysDotNetDataType_ComboItemList_BindingList.Clear();
                    //_v_SysDotNetDataType_ComboItemList_BindingList.ReplaceList((object[])data[23]);

                    //_v_SysDotNetDataType_ComboItemList_BindingList.IsRefreshingData = false;

                    //// v_SysSqlDataType_ComboItemList
                    //_v_SysSqlDataType_ComboItemList_BindingList.IsRefreshingData = true;
                    //_v_SysSqlDataType_ComboItemList_BindingList.CachedList.Clear();
                    //_v_SysSqlDataType_ComboItemList_BindingList.Clear();
                    //_v_SysSqlDataType_ComboItemList_BindingList.ReplaceList((object[])data[24]);

                    //_v_SysSqlDataType_ComboItemList_BindingList.IsRefreshingData = false;
                    //isDataLoaded = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }
        
        public static void GetCommonClientData_ReadOnlyStaticListsLoadData(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticListsLoadData", IfxTraceCategory.Enter);
                if (data != null)
                {


                    // TaskStatus_ComboItemList
                    _taskStatus_ComboItemList_BindingList.IsRefreshingData = true;
                    _taskStatus_ComboItemList_BindingList.CachedList.Clear();
                    _taskStatus_ComboItemList_BindingList.Clear();
                    _taskStatus_ComboItemList_BindingList.ReplaceList((object[])data[0]);

                    _taskStatus_ComboItemList_BindingList.IsRefreshingData = false;

                    // WcControlType_ComboItemList
                    _wcControlType_ComboItemList_BindingList.IsRefreshingData = true;
                    _wcControlType_ComboItemList_BindingList.CachedList.Clear();
                    _wcControlType_ComboItemList_BindingList.Clear();
                    _wcControlType_ComboItemList_BindingList.ReplaceList((object[])data[1]);

                    _wcControlType_ComboItemList_BindingList.IsRefreshingData = false;

                    // WcDataTypeDotNet_ComboItemList
                    _wcDataTypeDotNet_ComboItemList_BindingList.IsRefreshingData = true;
                    _wcDataTypeDotNet_ComboItemList_BindingList.CachedList.Clear();
                    _wcDataTypeDotNet_ComboItemList_BindingList.Clear();
                    _wcDataTypeDotNet_ComboItemList_BindingList.ReplaceList((object[])data[2]);

                    _wcDataTypeDotNet_ComboItemList_BindingList.IsRefreshingData = false;

                    // WcDataTypeSQL_ComboItemList
                    _wcDataTypeSQL_ComboItemList_BindingList.IsRefreshingData = true;
                    _wcDataTypeSQL_ComboItemList_BindingList.CachedList.Clear();
                    _wcDataTypeSQL_ComboItemList_BindingList.Clear();
                    _wcDataTypeSQL_ComboItemList_BindingList.ReplaceList((object[])data[3]);

                    _wcDataTypeSQL_ComboItemList_BindingList.IsRefreshingData = false;

                    // WcStoredProcParamDirection_ComboItemList
                    _wcStoredProcParamDirection_ComboItemList_BindingList.IsRefreshingData = true;
                    _wcStoredProcParamDirection_ComboItemList_BindingList.CachedList.Clear();
                    _wcStoredProcParamDirection_ComboItemList_BindingList.Clear();
                    _wcStoredProcParamDirection_ComboItemList_BindingList.ReplaceList((object[])data[4]);

                    _wcStoredProcParamDirection_ComboItemList_BindingList.IsRefreshingData = false;

                    // WcStoredProcResultType_ComboItemList
                    _wcStoredProcResultType_ComboItemList_BindingList.IsRefreshingData = true;
                    _wcStoredProcResultType_ComboItemList_BindingList.CachedList.Clear();
                    _wcStoredProcResultType_ComboItemList_BindingList.Clear();
                    _wcStoredProcResultType_ComboItemList_BindingList.ReplaceList((object[])data[5]);

                    _wcStoredProcResultType_ComboItemList_BindingList.IsRefreshingData = false;

                    // WcStoredProcReturnType_ComboItemList
                    _wcStoredProcReturnType_ComboItemList_BindingList.IsRefreshingData = true;
                    _wcStoredProcReturnType_ComboItemList_BindingList.CachedList.Clear();
                    _wcStoredProcReturnType_ComboItemList_BindingList.Clear();
                    _wcStoredProcReturnType_ComboItemList_BindingList.ReplaceList((object[])data[6]);

                    _wcStoredProcReturnType_ComboItemList_BindingList.IsRefreshingData = false;

                    // WcTableColumnComboListType_ComboItemList
                    _wcTableColumnComboListType_ComboItemList_BindingList.IsRefreshingData = true;
                    _wcTableColumnComboListType_ComboItemList_BindingList.CachedList.Clear();
                    _wcTableColumnComboListType_ComboItemList_BindingList.Clear();
                    _wcTableColumnComboListType_ComboItemList_BindingList.ReplaceList((object[])data[7]);

                    _wcTableColumnComboListType_ComboItemList_BindingList.IsRefreshingData = false;

                    // WcTableSortByDirection_ComboItemList
                    _wcTableSortByDirection_ComboItemList_BindingList.IsRefreshingData = true;
                    _wcTableSortByDirection_ComboItemList_BindingList.CachedList.Clear();
                    _wcTableSortByDirection_ComboItemList_BindingList.Clear();
                    _wcTableSortByDirection_ComboItemList_BindingList.ReplaceList((object[])data[8]);

                    _wcTableSortByDirection_ComboItemList_BindingList.IsRefreshingData = false;
                    isDataLoaded = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticListsLoadData", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticListsLoadData", IfxTraceCategory.Leave);
            }
        }




    }

}
