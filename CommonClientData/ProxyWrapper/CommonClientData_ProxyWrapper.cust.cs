using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/2/2011 7:46:34 PM

namespace ProxyWrapper
{
    public partial class CommonClientDataService_ProxyWrapper
    {

        #region Initialize Variables


        #endregion Initialize Variables

        #region Events

        public event System.EventHandler<GetHelpDocumentationPathCompletedEventArgs> GetHelpDocumentationPathCompleted;


        #endregion Events


        #region Service Calls


        #region GetHelpDocumentationPath

        public void Begin_GetHelpDocumentationPath()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetHelpDocumentationPath", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetHelpDocumentationPathCompleted += new EventHandler<GetHelpDocumentationPathCompletedEventArgs>(proxy_GetHelpDocumentationPathCompleted);
                proxy.GetHelpDocumentationPathAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetHelpDocumentationPath", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetHelpDocumentationPath", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetHelpDocumentationPathCompleted(object sender, GetHelpDocumentationPathCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetHelpDocumentationPathCompleted", IfxTraceCategory.Enter);
                System.EventHandler<GetHelpDocumentationPathCompletedEventArgs> handler = GetHelpDocumentationPathCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetHelpDocumentationPathCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetHelpDocumentationPathCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion GetHelpDocumentationPath





        #endregion Service Calls





    }
}


