using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  2/6/2013 12:01:10 PM

namespace ProxyWrapper
{
    public partial class CommonClientDataService_ProxyWrapper
    {

        #region Initialize Variables


        #endregion Initialize Variables

        #region Events

        public event System.EventHandler<GetHelpDocumentationPathCompletedEventArgs> GetHelpDocumentationPathCompleted;
        //public event System.EventHandler<GetCustomGridData_CompressedCompletedEventArgs> GetCustomGridData_CompressedCompleted;
        //public event System.EventHandler<GetCommonClientData_ReadOnlyStaticLists_AllCompletedEventArgs> GetCommonClientData_ReadOnlyStaticLists_AllCompleted;


        #endregion Events


        #region Service Calls


        #region GetHelpDocumentationPath

        public void Begin_GetHelpDocumentationPath()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetHelpDocumentationPath", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetHelpDocumentationPathCompleted += new EventHandler<GetHelpDocumentationPathCompletedEventArgs>(proxy_GetHelpDocumentationPathCompleted);
                proxy.GetHelpDocumentationPathAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetHelpDocumentationPath", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetHelpDocumentationPath", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetHelpDocumentationPathCompleted(object sender, GetHelpDocumentationPathCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetHelpDocumentationPathCompleted", IfxTraceCategory.Enter);
                System.EventHandler<GetHelpDocumentationPathCompletedEventArgs> handler = GetHelpDocumentationPathCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetHelpDocumentationPathCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetHelpDocumentationPathCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion GetHelpDocumentationPath



        //#region GetCustomGridData_Compressed

        //public void Begin_GetCustomGridData_Compressed(Guid Prj_Id, string SprocName)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetCustomGridData_Compressed", IfxTraceCategory.Enter);
        //        CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.GetCustomGridData_CompressedCompleted += new EventHandler<GetCustomGridData_CompressedCompletedEventArgs>(proxy_GetCustomGridData_CompressedCompleted);
        //        proxy.GetCustomGridData_CompressedAsync(Prj_Id, SprocName);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetCustomGridData_Compressed", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetCustomGridData_Compressed", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_GetCustomGridData_CompressedCompleted(object sender, GetCustomGridData_CompressedCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCustomGridData_Compressed", IfxTraceCategory.Enter);
        //        System.EventHandler<GetCustomGridData_CompressedCompletedEventArgs> handler = GetCustomGridData_CompressedCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCustomGridData_Compressed", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCustomGridData_Compressed", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion GetCustomGridData_Compressed



        //#region GetCommonClientData_ReadOnlyStaticLists_All

        //public void Begin_GetCommonClientData_ReadOnlyStaticLists_All(Guid Prj_Id, Guid UserId)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetCommonClientData_ReadOnlyStaticLists_All", IfxTraceCategory.Enter);
        //        CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.GetCommonClientData_ReadOnlyStaticLists_AllCompleted += new EventHandler<GetCommonClientData_ReadOnlyStaticLists_AllCompletedEventArgs>(proxy_GetCommonClientData_ReadOnlyStaticLists_AllCompleted);
        //        proxy.GetCommonClientData_ReadOnlyStaticLists_AllAsync(Prj_Id, UserId);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetCommonClientData_ReadOnlyStaticLists_All", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetCommonClientData_ReadOnlyStaticLists_All", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_GetCommonClientData_ReadOnlyStaticLists_AllCompleted(object sender, GetCommonClientData_ReadOnlyStaticLists_AllCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticLists_All", IfxTraceCategory.Enter);
        //        System.EventHandler<GetCommonClientData_ReadOnlyStaticLists_AllCompletedEventArgs> handler = GetCommonClientData_ReadOnlyStaticLists_AllCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticLists_All", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCommonClientData_ReadOnlyStaticLists_All", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion GetCommonClientData_ReadOnlyStaticLists_All



        #endregion Service Calls





    }
}


