using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/27/2016 12:46:48 PM

namespace ProxyWrapper
{
    public partial class CommonClientDataService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs> GetCommonClientData_ReadOnlyStaticListsCompleted;
		public event System.EventHandler<GetWcTableSortByDirection_ComboItemListCompletedEventArgs> GetWcTableSortByDirection_ComboItemListCompleted;
		public event System.EventHandler<GetWcControlType_ComboItemListCompletedEventArgs> GetWcControlType_ComboItemListCompleted;
		public event System.EventHandler<GetWcDataTypeDotNet_ComboItemListCompletedEventArgs> GetWcDataTypeDotNet_ComboItemListCompleted;
		public event System.EventHandler<GetWcDataTypeSQL_ComboItemListCompletedEventArgs> GetWcDataTypeSQL_ComboItemListCompleted;
		public event System.EventHandler<GetWcStoredProcResultType_ComboItemListCompletedEventArgs> GetWcStoredProcResultType_ComboItemListCompleted;
		public event System.EventHandler<GetWcStoredProcReturnType_ComboItemListCompletedEventArgs> GetWcStoredProcReturnType_ComboItemListCompleted;
		public event System.EventHandler<GetWcTableColumnComboListType_ComboItemListCompletedEventArgs> GetWcTableColumnComboListType_ComboItemListCompleted;
		public event System.EventHandler<GetWcStoredProcParamDirection_ComboItemListCompletedEventArgs> GetWcStoredProcParamDirection_ComboItemListCompleted;
		public event System.EventHandler<GetTaskStatus_ComboItemListCompletedEventArgs> GetTaskStatus_ComboItemListCompleted;

        #endregion Events

        #region ReadOnlyStaticLists

        public void Begin_GetCommonClientData_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetCommonClientData_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetCommonClientData_ReadOnlyStaticListsCompleted += new EventHandler<GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs>(proxy_GetCommonClientData_ReadOnlyStaticListsCompleted);
                proxy.GetCommonClientData_ReadOnlyStaticListsAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetCommonClientData_ReadOnlyStaticLists", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetCommonClientData_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetCommonClientData_ReadOnlyStaticListsCompleted(object sender, GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetCommonClientData_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                System.EventHandler<GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs> handler = GetCommonClientData_ReadOnlyStaticListsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetCommonClientData_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetCommonClientData_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion ReadOnlyStaticLists

        #region GetWcTableSortByDirection_ComboItemList

        public void Begin_GetWcTableSortByDirection_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableSortByDirection_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcTableSortByDirection_ComboItemListCompleted += new EventHandler<GetWcTableSortByDirection_ComboItemListCompletedEventArgs>(proxy_GetWcTableSortByDirection_ComboItemListCompleted);
                proxy.GetWcTableSortByDirection_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableSortByDirection_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableSortByDirection_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcTableSortByDirection_ComboItemListCompleted(object sender, GetWcTableSortByDirection_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableSortByDirection_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcTableSortByDirection_ComboItemListCompletedEventArgs> handler = GetWcTableSortByDirection_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableSortByDirection_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableSortByDirection_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcTableSortByDirection_ComboItemList

        #region GetWcControlType_ComboItemList

        public void Begin_GetWcControlType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcControlType_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcControlType_ComboItemListCompleted += new EventHandler<GetWcControlType_ComboItemListCompletedEventArgs>(proxy_GetWcControlType_ComboItemListCompleted);
                proxy.GetWcControlType_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcControlType_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcControlType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcControlType_ComboItemListCompleted(object sender, GetWcControlType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcControlType_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcControlType_ComboItemListCompletedEventArgs> handler = GetWcControlType_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcControlType_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcControlType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcControlType_ComboItemList

        #region GetWcDataTypeDotNet_ComboItemList

        public void Begin_GetWcDataTypeDotNet_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcDataTypeDotNet_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcDataTypeDotNet_ComboItemListCompleted += new EventHandler<GetWcDataTypeDotNet_ComboItemListCompletedEventArgs>(proxy_GetWcDataTypeDotNet_ComboItemListCompleted);
                proxy.GetWcDataTypeDotNet_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcDataTypeDotNet_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcDataTypeDotNet_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcDataTypeDotNet_ComboItemListCompleted(object sender, GetWcDataTypeDotNet_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDotNet_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcDataTypeDotNet_ComboItemListCompletedEventArgs> handler = GetWcDataTypeDotNet_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDotNet_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDotNet_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcDataTypeDotNet_ComboItemList

        #region GetWcDataTypeSQL_ComboItemList

        public void Begin_GetWcDataTypeSQL_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcDataTypeSQL_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcDataTypeSQL_ComboItemListCompleted += new EventHandler<GetWcDataTypeSQL_ComboItemListCompletedEventArgs>(proxy_GetWcDataTypeSQL_ComboItemListCompleted);
                proxy.GetWcDataTypeSQL_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcDataTypeSQL_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcDataTypeSQL_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcDataTypeSQL_ComboItemListCompleted(object sender, GetWcDataTypeSQL_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeSQL_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcDataTypeSQL_ComboItemListCompletedEventArgs> handler = GetWcDataTypeSQL_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeSQL_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeSQL_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcDataTypeSQL_ComboItemList

        #region GetWcStoredProcResultType_ComboItemList

        public void Begin_GetWcStoredProcResultType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcResultType_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcStoredProcResultType_ComboItemListCompleted += new EventHandler<GetWcStoredProcResultType_ComboItemListCompletedEventArgs>(proxy_GetWcStoredProcResultType_ComboItemListCompleted);
                proxy.GetWcStoredProcResultType_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcResultType_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcResultType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcStoredProcResultType_ComboItemListCompleted(object sender, GetWcStoredProcResultType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcResultType_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcStoredProcResultType_ComboItemListCompletedEventArgs> handler = GetWcStoredProcResultType_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcResultType_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcResultType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcStoredProcResultType_ComboItemList

        #region GetWcStoredProcReturnType_ComboItemList

        public void Begin_GetWcStoredProcReturnType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcReturnType_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcStoredProcReturnType_ComboItemListCompleted += new EventHandler<GetWcStoredProcReturnType_ComboItemListCompletedEventArgs>(proxy_GetWcStoredProcReturnType_ComboItemListCompleted);
                proxy.GetWcStoredProcReturnType_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcReturnType_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcReturnType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcStoredProcReturnType_ComboItemListCompleted(object sender, GetWcStoredProcReturnType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcReturnType_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcStoredProcReturnType_ComboItemListCompletedEventArgs> handler = GetWcStoredProcReturnType_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcReturnType_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcReturnType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcStoredProcReturnType_ComboItemList

        #region GetWcTableColumnComboListType_ComboItemList

        public void Begin_GetWcTableColumnComboListType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumnComboListType_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcTableColumnComboListType_ComboItemListCompleted += new EventHandler<GetWcTableColumnComboListType_ComboItemListCompletedEventArgs>(proxy_GetWcTableColumnComboListType_ComboItemListCompleted);
                proxy.GetWcTableColumnComboListType_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumnComboListType_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumnComboListType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcTableColumnComboListType_ComboItemListCompleted(object sender, GetWcTableColumnComboListType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnComboListType_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcTableColumnComboListType_ComboItemListCompletedEventArgs> handler = GetWcTableColumnComboListType_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnComboListType_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnComboListType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcTableColumnComboListType_ComboItemList

        #region GetWcStoredProcParamDirection_ComboItemList

        public void Begin_GetWcStoredProcParamDirection_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcParamDirection_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcStoredProcParamDirection_ComboItemListCompleted += new EventHandler<GetWcStoredProcParamDirection_ComboItemListCompletedEventArgs>(proxy_GetWcStoredProcParamDirection_ComboItemListCompleted);
                proxy.GetWcStoredProcParamDirection_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcParamDirection_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcParamDirection_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcStoredProcParamDirection_ComboItemListCompleted(object sender, GetWcStoredProcParamDirection_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParamDirection_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcStoredProcParamDirection_ComboItemListCompletedEventArgs> handler = GetWcStoredProcParamDirection_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParamDirection_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParamDirection_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcStoredProcParamDirection_ComboItemList

        #region GetTaskStatus_ComboItemList

        public void Begin_GetTaskStatus_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskStatus_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetTaskStatus_ComboItemListCompleted += new EventHandler<GetTaskStatus_ComboItemListCompletedEventArgs>(proxy_GetTaskStatus_ComboItemListCompleted);
                proxy.GetTaskStatus_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskStatus_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskStatus_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTaskStatus_ComboItemListCompleted(object sender, GetTaskStatus_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetTaskStatus_ComboItemListCompletedEventArgs> handler = GetTaskStatus_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTaskStatus_ComboItemList

    }
}


