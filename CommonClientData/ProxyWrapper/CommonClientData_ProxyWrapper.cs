using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  4/18/2012 10:33:07 AM

namespace ProxyWrapper
{
    public partial class CommonClientDataService_ProxyWrapper
    {


        #region Events

        //public event System.EventHandler<CommonClientData_GetByIdCompletedEventArgs> CommonClientData_GetByIdCompleted;
        //public event System.EventHandler<CommonClientData_GetAllCompletedEventArgs> CommonClientData_GetAllCompleted;
        //public event System.EventHandler<CommonClientData_GetListByFKCompletedEventArgs> CommonClientData_GetListByFKCompleted;
        //public event System.EventHandler<CommonClientData_SaveCompletedEventArgs> CommonClientData_SaveCompleted;
        //public event System.EventHandler<CommonClientData_DeleteCompletedEventArgs> CommonClientData_DeleteCompleted;
        //public event System.EventHandler<CommonClientData_DeactivateCompletedEventArgs> CommonClientData_DeactivateCompleted;
        //public event System.EventHandler<CommonClientData_RemoveCompletedEventArgs> CommonClientData_RemoveCompleted;

        public event System.EventHandler<GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs> GetCommonClientData_ReadOnlyStaticListsCompleted;
		public event System.EventHandler<GetProjExp_lstProjects_ActiveCompletedEventArgs> GetProjExp_lstProjects_ActiveCompleted;
		public event System.EventHandler<GetProjExp_lstGroupsCompletedEventArgs> GetProjExp_lstGroupsCompleted;
		public event System.EventHandler<GetAgencyType_ComboItemListCompletedEventArgs> GetAgencyType_ComboItemListCompleted;
		public event System.EventHandler<GetState_ComboItemListCompletedEventArgs> GetState_ComboItemListCompleted;
		public event System.EventHandler<GetCounty_ComboItemListCompletedEventArgs> GetCounty_ComboItemListCompleted;
		public event System.EventHandler<GetDataStatus_ComboItemListCompletedEventArgs> GetDataStatus_ComboItemListCompleted;
		public event System.EventHandler<GetRecordingType_ComboItemListCompletedEventArgs> GetRecordingType_ComboItemListCompleted;
		public event System.EventHandler<GetOwnerShipType_ComboItemListCompletedEventArgs> GetOwnerShipType_ComboItemListCompleted;
		public event System.EventHandler<GetArea_ComboItemListCompletedEventArgs> GetArea_ComboItemListCompleted;
		public event System.EventHandler<GetBlock_ComboItemListCompletedEventArgs> GetBlock_ComboItemListCompleted;
		public event System.EventHandler<GetAddition_ComboItemListCompletedEventArgs> GetAddition_ComboItemListCompleted;
		public event System.EventHandler<GetRangeDirection_ComboItemListCompletedEventArgs> GetRangeDirection_ComboItemListCompleted;
		public event System.EventHandler<GetTownshipDirection_ComboItemListCompletedEventArgs> GetTownshipDirection_ComboItemListCompleted;
		public event System.EventHandler<GetField_ComboItemListCompletedEventArgs> GetField_ComboItemListCompleted;
		public event System.EventHandler<GetFormation_ComboItemListCompletedEventArgs> GetFormation_ComboItemListCompleted;
		public event System.EventHandler<GetOperator_ComboItemListCompletedEventArgs> GetOperator_ComboItemListCompleted;
		public event System.EventHandler<GetParty_ComboItemListCompletedEventArgs> GetParty_ComboItemListCompleted;
		public event System.EventHandler<GetFlagStatus_ComboItemListCompletedEventArgs> GetFlagStatus_ComboItemListCompleted;
		public event System.EventHandler<v_ReportGroup_lstActiveCompletedEventArgs> v_ReportGroup_lstActiveCompleted;
		public event System.EventHandler<v_Report_lstRptExplorerCompletedEventArgs> v_Report_lstRptExplorerCompleted;
		public event System.EventHandler<Getv_SysDotNetDataType_ComboItemListCompletedEventArgs> Getv_SysDotNetDataType_ComboItemListCompleted;
		public event System.EventHandler<Getv_SysSqlDataType_ComboItemListCompletedEventArgs> Getv_SysSqlDataType_ComboItemListCompleted;
		public event System.EventHandler<GetStateCounty_ComboItemListCompletedEventArgs> GetStateCounty_ComboItemListCompleted;

        #endregion Events


        #region Service Calls

        #region Standard Service Calls

        //#region CommonClientData_GetById

        //public void Begin_CommonClientData_GetById(Guid Id)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_GetById", IfxTraceCategory.Enter);
        //        CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.CommonClientData_GetByIdCompleted += new EventHandler<CommonClientData_GetByIdCompletedEventArgs>(proxy_CommonClientData_GetByIdCompleted);
        //        proxy.CommonClientData_GetByIdAsync(Id);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_GetById", ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_GetById", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_CommonClientData_GetByIdCompleted(object sender, CommonClientData_GetByIdCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_GetByIdCompleted", IfxTraceCategory.Enter);
        //        System.EventHandler<CommonClientData_GetByIdCompletedEventArgs> handler = CommonClientData_GetByIdCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_GetByIdCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_GetByIdCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion CommonClientData_GetById

        //#region CommonClientData_GetAll

        //public void Begin_CommonClientData_GetAll()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_GetAll", IfxTraceCategory.Enter);
        //        CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.CommonClientData_GetAllCompleted += new EventHandler<CommonClientData_GetAllCompletedEventArgs>(proxy_CommonClientData_GetAllCompleted);
        //        proxy.CommonClientData_GetAllAsync();
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_GetAll", ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_GetAll", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_CommonClientData_GetAllCompleted(object sender, CommonClientData_GetAllCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_GetAllCompleted", IfxTraceCategory.Enter);
        //        System.EventHandler<CommonClientData_GetAllCompletedEventArgs> handler = CommonClientData_GetAllCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_GetAllCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_GetAllCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion CommonClientData_GetAll

        //#region CommonClientData_GetListByFK

        //public void Begin_CommonClientData_GetListByFK()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_GetListByFK", IfxTraceCategory.Enter);
        //        CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.CommonClientData_GetListByFKCompleted += new EventHandler<CommonClientData_GetListByFKCompletedEventArgs>(proxy_CommonClientData_GetListByFKCompleted);
        //        proxy.CommonClientData_GetListByFKAsync();
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_GetListByFK", ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_GetListByFK", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_CommonClientData_GetListByFKCompleted(object sender, CommonClientData_GetListByFKCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_GetListByFKCompleted", IfxTraceCategory.Enter);
        //        System.EventHandler<CommonClientData_GetListByFKCompletedEventArgs> handler = CommonClientData_GetListByFKCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_GetListByFKCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_GetListByFKCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion CommonClientData_GetListByFK

        //#region CommonClientData_Save

        //public void Begin_CommonClientData_Save(object[] data, int check)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_Save", IfxTraceCategory.Enter);
        //        CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
        //        AssignCredentials(proxy);
        //        //proxy.CommonClientData_SaveCompleted += new EventHandler<CommonClientData_SaveCompletedEventArgs>(proxy_CommonClientData_SaveCompleted);
        //        proxy.CommonClientData_SaveCompleted += new EventHandler<CommonClientData_SaveCompletedEventArgs>(proxy_CommonClientData_SaveCompleted);
        //        proxy.CommonClientData_SaveAsync(data, check);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_Save", ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_Save", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_CommonClientData_SaveCompleted(object sender, CommonClientData_SaveCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_SaveCompleted", IfxTraceCategory.Enter);
        //        System.EventHandler<CommonClientData_SaveCompletedEventArgs> handler = CommonClientData_SaveCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_SaveCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_SaveCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion CommonClientData_Save

        //#region CommonClientData_Delete

        //public void Begin_CommonClientData_Delete(object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_DeleteCompleted", IfxTraceCategory.Enter);
        //        CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.CommonClientData_DeleteCompleted += new EventHandler<CommonClientData_DeleteCompletedEventArgs>(proxy_CommonClientData_DeleteCompleted);
        //        proxy.CommonClientData_DeleteAsync(data);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_DeleteCompleted", ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_DeleteCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_CommonClientData_DeleteCompleted(object sender, CommonClientData_DeleteCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_DeleteCompleted", IfxTraceCategory.Enter);
        //        System.EventHandler<CommonClientData_DeleteCompletedEventArgs> handler = CommonClientData_DeleteCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_DeleteCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_DeleteCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion CommonClientData_Delete

        //#region CommonClientData_Deactivate

        //public void Begin_CommonClientData_Deactivate(object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_Deactivate", IfxTraceCategory.Enter);
        //    CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
        //    AssignCredentials(proxy);
        //    proxy.CommonClientData_DeactivateCompleted += new EventHandler<CommonClientData_DeactivateCompletedEventArgs>(proxy_CommonClientData_DeactivateCompleted);
        //    proxy.CommonClientData_DeactivateAsync(data);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_Deactivate", ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_Deactivate", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_CommonClientData_DeactivateCompleted(object sender, CommonClientData_DeactivateCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_DeactivateCompleted", IfxTraceCategory.Enter);
        //        System.EventHandler<CommonClientData_DeactivateCompletedEventArgs> handler = CommonClientData_DeactivateCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_DeactivateCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_DeactivateCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion CommonClientData_Deactivate

        //#region CommonClientData_Remove

        //public void Begin_CommonClientData_Remove(object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_Remove", IfxTraceCategory.Enter);
        //        CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.CommonClientData_RemoveCompleted += new EventHandler<CommonClientData_RemoveCompletedEventArgs>(proxy_CommonClientData_RemoveCompleted);
        //        proxy.CommonClientData_RemoveAsync(data);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_Remove", ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_CommonClientData_Remove", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_CommonClientData_RemoveCompleted(object sender, CommonClientData_RemoveCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_RemoveCompleted", IfxTraceCategory.Enter);
        //        System.EventHandler<CommonClientData_RemoveCompletedEventArgs> handler = CommonClientData_RemoveCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_RemoveCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CommonClientData_RemoveCompleted", IfxTraceCategory.Leave);
        //    }
        //}
        //#endregion CommonClientData_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists

        public void Begin_GetCommonClientData_ReadOnlyStaticLists(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadLists", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetCommonClientData_ReadOnlyStaticListsCompleted += new EventHandler<GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs>(proxy_GetCommonClientData_ReadOnlyStaticListsCompleted);
                proxy.GetCommonClientData_ReadOnlyStaticListsAsync(Prj_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadLists", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadLists", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetCommonClientData_ReadOnlyStaticListsCompleted(object sender, GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadLists", IfxTraceCategory.Enter);
                System.EventHandler<GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs> handler = GetCommonClientData_ReadOnlyStaticListsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadLists", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadLists", IfxTraceCategory.Leave);
            }
        }

        #endregion ReadOnlyStaticLists

        #region GetProjExp_lstProjects_Active

        public void Begin_GetProjExp_lstProjects_Active()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetProjExp_lstProjects_Active", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetProjExp_lstProjects_ActiveCompleted += new EventHandler<GetProjExp_lstProjects_ActiveCompletedEventArgs>(proxy_GetProjExp_lstProjects_ActiveCompleted);
                proxy.GetProjExp_lstProjects_ActiveAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetProjExp_lstProjects_Active", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetProjExp_lstProjects_Active", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetProjExp_lstProjects_ActiveCompleted(object sender, GetProjExp_lstProjects_ActiveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstProjects_Active", IfxTraceCategory.Enter);
                System.EventHandler<GetProjExp_lstProjects_ActiveCompletedEventArgs> handler = GetProjExp_lstProjects_ActiveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstProjects_Active", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstProjects_Active", IfxTraceCategory.Leave);
            }
        }

        #endregion GetProjExp_lstProjects_Active

        #region GetProjExp_lstGroups

        public void Begin_GetProjExp_lstGroups(Guid Id, String EntityType )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetProjExp_lstGroups", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetProjExp_lstGroupsCompleted += new EventHandler<GetProjExp_lstGroupsCompletedEventArgs>(proxy_GetProjExp_lstGroupsCompleted);
                proxy.GetProjExp_lstGroupsAsync(Id, EntityType );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetProjExp_lstGroups", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetProjExp_lstGroups", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetProjExp_lstGroupsCompleted(object sender, GetProjExp_lstGroupsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstGroups", IfxTraceCategory.Enter);
                System.EventHandler<GetProjExp_lstGroupsCompletedEventArgs> handler = GetProjExp_lstGroupsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstGroups", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProjExp_lstGroups", IfxTraceCategory.Leave);
            }
        }

        #endregion GetProjExp_lstGroups

        #region GetAgencyType_ComboItemList

        public void Begin_GetAgencyType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetAgencyType_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetAgencyType_ComboItemListCompleted += new EventHandler<GetAgencyType_ComboItemListCompletedEventArgs>(proxy_GetAgencyType_ComboItemListCompleted);
                proxy.GetAgencyType_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetAgencyType_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetAgencyType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetAgencyType_ComboItemListCompleted(object sender, GetAgencyType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAgencyType_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetAgencyType_ComboItemListCompletedEventArgs> handler = GetAgencyType_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAgencyType_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAgencyType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetAgencyType_ComboItemList

        #region GetState_ComboItemList

        public void Begin_GetState_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetState_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetState_ComboItemListCompleted += new EventHandler<GetState_ComboItemListCompletedEventArgs>(proxy_GetState_ComboItemListCompleted);
                proxy.GetState_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetState_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetState_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetState_ComboItemListCompleted(object sender, GetState_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetState_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetState_ComboItemListCompletedEventArgs> handler = GetState_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetState_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetState_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetState_ComboItemList

        #region GetCounty_ComboItemList

        public void Begin_GetCounty_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetCounty_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetCounty_ComboItemListCompleted += new EventHandler<GetCounty_ComboItemListCompletedEventArgs>(proxy_GetCounty_ComboItemListCompleted);
                proxy.GetCounty_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetCounty_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetCounty_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetCounty_ComboItemListCompleted(object sender, GetCounty_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCounty_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetCounty_ComboItemListCompletedEventArgs> handler = GetCounty_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCounty_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCounty_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetCounty_ComboItemList

        #region GetDataStatus_ComboItemList

        public void Begin_GetDataStatus_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDataStatus_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetDataStatus_ComboItemListCompleted += new EventHandler<GetDataStatus_ComboItemListCompletedEventArgs>(proxy_GetDataStatus_ComboItemListCompleted);
                proxy.GetDataStatus_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDataStatus_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDataStatus_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDataStatus_ComboItemListCompleted(object sender, GetDataStatus_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataStatus_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetDataStatus_ComboItemListCompletedEventArgs> handler = GetDataStatus_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataStatus_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataStatus_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDataStatus_ComboItemList

        #region GetRecordingType_ComboItemList

        public void Begin_GetRecordingType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetRecordingType_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetRecordingType_ComboItemListCompleted += new EventHandler<GetRecordingType_ComboItemListCompletedEventArgs>(proxy_GetRecordingType_ComboItemListCompleted);
                proxy.GetRecordingType_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetRecordingType_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetRecordingType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetRecordingType_ComboItemListCompleted(object sender, GetRecordingType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRecordingType_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetRecordingType_ComboItemListCompletedEventArgs> handler = GetRecordingType_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRecordingType_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRecordingType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetRecordingType_ComboItemList

        #region GetOwnerShipType_ComboItemList

        public void Begin_GetOwnerShipType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetOwnerShipType_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetOwnerShipType_ComboItemListCompleted += new EventHandler<GetOwnerShipType_ComboItemListCompletedEventArgs>(proxy_GetOwnerShipType_ComboItemListCompleted);
                proxy.GetOwnerShipType_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetOwnerShipType_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetOwnerShipType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetOwnerShipType_ComboItemListCompleted(object sender, GetOwnerShipType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOwnerShipType_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetOwnerShipType_ComboItemListCompletedEventArgs> handler = GetOwnerShipType_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOwnerShipType_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOwnerShipType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetOwnerShipType_ComboItemList

        #region GetArea_ComboItemList

        public void Begin_GetArea_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetArea_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetArea_ComboItemListCompleted += new EventHandler<GetArea_ComboItemListCompletedEventArgs>(proxy_GetArea_ComboItemListCompleted);
                proxy.GetArea_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetArea_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetArea_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetArea_ComboItemListCompleted(object sender, GetArea_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetArea_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetArea_ComboItemListCompletedEventArgs> handler = GetArea_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetArea_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetArea_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetArea_ComboItemList

        #region GetBlock_ComboItemList

        public void Begin_GetBlock_ComboItemList(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetBlock_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetBlock_ComboItemListCompleted += new EventHandler<GetBlock_ComboItemListCompletedEventArgs>(proxy_GetBlock_ComboItemListCompleted);
                proxy.GetBlock_ComboItemListAsync(Prj_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetBlock_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetBlock_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetBlock_ComboItemListCompleted(object sender, GetBlock_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBlock_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetBlock_ComboItemListCompletedEventArgs> handler = GetBlock_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBlock_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBlock_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetBlock_ComboItemList

        #region GetAddition_ComboItemList

        public void Begin_GetAddition_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetAddition_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetAddition_ComboItemListCompleted += new EventHandler<GetAddition_ComboItemListCompletedEventArgs>(proxy_GetAddition_ComboItemListCompleted);
                proxy.GetAddition_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetAddition_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetAddition_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetAddition_ComboItemListCompleted(object sender, GetAddition_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAddition_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetAddition_ComboItemListCompletedEventArgs> handler = GetAddition_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAddition_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAddition_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetAddition_ComboItemList

        #region GetRangeDirection_ComboItemList

        public void Begin_GetRangeDirection_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetRangeDirection_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetRangeDirection_ComboItemListCompleted += new EventHandler<GetRangeDirection_ComboItemListCompletedEventArgs>(proxy_GetRangeDirection_ComboItemListCompleted);
                proxy.GetRangeDirection_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetRangeDirection_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetRangeDirection_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetRangeDirection_ComboItemListCompleted(object sender, GetRangeDirection_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRangeDirection_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetRangeDirection_ComboItemListCompletedEventArgs> handler = GetRangeDirection_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRangeDirection_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRangeDirection_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetRangeDirection_ComboItemList

        #region GetTownshipDirection_ComboItemList

        public void Begin_GetTownshipDirection_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTownshipDirection_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetTownshipDirection_ComboItemListCompleted += new EventHandler<GetTownshipDirection_ComboItemListCompletedEventArgs>(proxy_GetTownshipDirection_ComboItemListCompleted);
                proxy.GetTownshipDirection_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTownshipDirection_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTownshipDirection_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTownshipDirection_ComboItemListCompleted(object sender, GetTownshipDirection_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTownshipDirection_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetTownshipDirection_ComboItemListCompletedEventArgs> handler = GetTownshipDirection_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTownshipDirection_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTownshipDirection_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTownshipDirection_ComboItemList

        #region GetField_ComboItemList

        public void Begin_GetField_ComboItemList(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetField_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetField_ComboItemListCompleted += new EventHandler<GetField_ComboItemListCompletedEventArgs>(proxy_GetField_ComboItemListCompleted);
                proxy.GetField_ComboItemListAsync(Prj_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetField_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetField_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetField_ComboItemListCompleted(object sender, GetField_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetField_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetField_ComboItemListCompletedEventArgs> handler = GetField_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetField_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetField_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetField_ComboItemList

        #region GetFormation_ComboItemList

        public void Begin_GetFormation_ComboItemList(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetFormation_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetFormation_ComboItemListCompleted += new EventHandler<GetFormation_ComboItemListCompletedEventArgs>(proxy_GetFormation_ComboItemListCompleted);
                proxy.GetFormation_ComboItemListAsync(Prj_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetFormation_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetFormation_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetFormation_ComboItemListCompleted(object sender, GetFormation_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFormation_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetFormation_ComboItemListCompletedEventArgs> handler = GetFormation_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFormation_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFormation_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetFormation_ComboItemList

        #region GetOperator_ComboItemList

        public void Begin_GetOperator_ComboItemList(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetOperator_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetOperator_ComboItemListCompleted += new EventHandler<GetOperator_ComboItemListCompletedEventArgs>(proxy_GetOperator_ComboItemListCompleted);
                proxy.GetOperator_ComboItemListAsync(Prj_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetOperator_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetOperator_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetOperator_ComboItemListCompleted(object sender, GetOperator_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOperator_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetOperator_ComboItemListCompletedEventArgs> handler = GetOperator_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOperator_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOperator_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetOperator_ComboItemList

        #region GetParty_ComboItemList

        public void Begin_GetParty_ComboItemList(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetParty_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetParty_ComboItemListCompleted += new EventHandler<GetParty_ComboItemListCompletedEventArgs>(proxy_GetParty_ComboItemListCompleted);
                proxy.GetParty_ComboItemListAsync(Prj_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetParty_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetParty_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetParty_ComboItemListCompleted(object sender, GetParty_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParty_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetParty_ComboItemListCompletedEventArgs> handler = GetParty_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParty_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParty_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetParty_ComboItemList

        #region GetFlagStatus_ComboItemList

        public void Begin_GetFlagStatus_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetFlagStatus_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetFlagStatus_ComboItemListCompleted += new EventHandler<GetFlagStatus_ComboItemListCompletedEventArgs>(proxy_GetFlagStatus_ComboItemListCompleted);
                proxy.GetFlagStatus_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetFlagStatus_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetFlagStatus_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetFlagStatus_ComboItemListCompleted(object sender, GetFlagStatus_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFlagStatus_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetFlagStatus_ComboItemListCompletedEventArgs> handler = GetFlagStatus_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFlagStatus_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFlagStatus_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetFlagStatus_ComboItemList

        #region v_ReportGroup_lstActive

        public void Begin_v_ReportGroup_lstActive()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_ReportGroup_lstActive", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.v_ReportGroup_lstActiveCompleted += new EventHandler<v_ReportGroup_lstActiveCompletedEventArgs>(proxy_v_ReportGroup_lstActiveCompleted);
                proxy.v_ReportGroup_lstActiveAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_ReportGroup_lstActive", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_ReportGroup_lstActive", IfxTraceCategory.Leave);
            }
        }

        void proxy_v_ReportGroup_lstActiveCompleted(object sender, v_ReportGroup_lstActiveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_lstActive", IfxTraceCategory.Enter);
                System.EventHandler<v_ReportGroup_lstActiveCompletedEventArgs> handler = v_ReportGroup_lstActiveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_lstActive", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_lstActive", IfxTraceCategory.Leave);
            }
        }

        #endregion v_ReportGroup_lstActive

        #region v_Report_lstRptExplorer

        public void Begin_v_Report_lstRptExplorer()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_Report_lstRptExplorer", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.v_Report_lstRptExplorerCompleted += new EventHandler<v_Report_lstRptExplorerCompletedEventArgs>(proxy_v_Report_lstRptExplorerCompleted);
                proxy.v_Report_lstRptExplorerAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_Report_lstRptExplorer", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_v_Report_lstRptExplorer", IfxTraceCategory.Leave);
            }
        }

        void proxy_v_Report_lstRptExplorerCompleted(object sender, v_Report_lstRptExplorerCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_lstRptExplorer", IfxTraceCategory.Enter);
                System.EventHandler<v_Report_lstRptExplorerCompletedEventArgs> handler = v_Report_lstRptExplorerCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_lstRptExplorer", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_lstRptExplorer", IfxTraceCategory.Leave);
            }
        }

        #endregion v_Report_lstRptExplorer

        #region Getv_SysDotNetDataType_ComboItemList

        public void Begin_Getv_SysDotNetDataType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_SysDotNetDataType_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.Getv_SysDotNetDataType_ComboItemListCompleted += new EventHandler<Getv_SysDotNetDataType_ComboItemListCompletedEventArgs>(proxy_Getv_SysDotNetDataType_ComboItemListCompleted);
                proxy.Getv_SysDotNetDataType_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_SysDotNetDataType_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_SysDotNetDataType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_Getv_SysDotNetDataType_ComboItemListCompleted(object sender, Getv_SysDotNetDataType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysDotNetDataType_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<Getv_SysDotNetDataType_ComboItemListCompletedEventArgs> handler = Getv_SysDotNetDataType_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysDotNetDataType_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysDotNetDataType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion Getv_SysDotNetDataType_ComboItemList

        #region Getv_SysSqlDataType_ComboItemList

        public void Begin_Getv_SysSqlDataType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_SysSqlDataType_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.Getv_SysSqlDataType_ComboItemListCompleted += new EventHandler<Getv_SysSqlDataType_ComboItemListCompletedEventArgs>(proxy_Getv_SysSqlDataType_ComboItemListCompleted);
                proxy.Getv_SysSqlDataType_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_SysSqlDataType_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Getv_SysSqlDataType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_Getv_SysSqlDataType_ComboItemListCompleted(object sender, Getv_SysSqlDataType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysSqlDataType_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<Getv_SysSqlDataType_ComboItemListCompletedEventArgs> handler = Getv_SysSqlDataType_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysSqlDataType_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysSqlDataType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion Getv_SysSqlDataType_ComboItemList

        #region GetStateCounty_ComboItemList

        public void Begin_GetStateCounty_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetStateCounty_ComboItemList", IfxTraceCategory.Enter);
                CommonClientDataServiceClient proxy = new CommonClientDataServiceClient();
                AssignCredentials(proxy);
                proxy.GetStateCounty_ComboItemListCompleted += new EventHandler<GetStateCounty_ComboItemListCompletedEventArgs>(proxy_GetStateCounty_ComboItemListCompleted);
                proxy.GetStateCounty_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetStateCounty_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetStateCounty_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetStateCounty_ComboItemListCompleted(object sender, GetStateCounty_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStateCounty_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetStateCounty_ComboItemListCompletedEventArgs> handler = GetStateCounty_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStateCounty_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStateCounty_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetStateCounty_ComboItemList

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


