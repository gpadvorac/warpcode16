﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Ifx.SL;
using Velocity.SL;
using vDP;
using vUICommon;
using System.ComponentModel;

namespace WarpCode
{
    public partial class ucAdminButtons : UserControl
    {

        private static string _as = "WarpCode";
        private static string _cn = "ucAdminButtons";

        public event AdminButtonClickedEventHandler AdminButtonClicked;
        UserSecurityContext _userContext = new UserSecurityContext();



        public ucAdminButtons()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucAdminButtons", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();
                _userContext.SecurityArtifactsRetrieved += new SecurityArtifactsRetrievedEventHandler(UserSecurityContext_SecurityArtifactsRetrieved);

                SetSecurityState();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucAdminButtons", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucAdminButtons", IfxTraceCategory.Leave);
            }
        }

        private void btnProjectMembers_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent_AdminButtonClicked("btnProjectMembers");
        }

        private void btnAppAdmin_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent_AdminButtonClicked("btnAppAdmin");
        }

        private void btnFilterAdmin_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent_AdminButtonClicked("btnFilterAdmin");
        }

        private void btnReportAdmin_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent_AdminButtonClicked("btnReportAdmin");
        }



        private void btnJobAdmin_OnClick(object sender, RoutedEventArgs e)
        {
            RaiseEvent_AdminButtonClicked("btnJobAdmin");
        }


        private void btnIfxDashboard_OnClick(object sender, RoutedEventArgs e)
        {
            RaiseEvent_AdminButtonClicked("btnIfxDashboard");
        }




        void RaiseEvent_AdminButtonClicked(string buttonName)
        {
            try
            {
                AdminButtonClickedEventHandler handler = AdminButtonClicked;
                if (handler != null)
                {
                    handler(this, new AdminButtonClickedArgs(buttonName));
                }
            }
            catch (Exception ex)
            {     }
        }








        private Guid _ancestorSecurityId = new Guid("59b35b3f-5d3c-4116-bd4a-6cc76bca4cea");
        ControlCache cCache;
        EntityCache eCache = null;
        bool defaultEntityOperationPermission = true;


        public void SetSecurityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Enter);
                _userContext.LoadArtifactPermissions(_ancestorSecurityId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Leave);
            }
        }

        void UserSecurityContext_SecurityArtifactsRetrieved(object sender, SecurityArtifactsRetrievedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Enter);

                SecurityCache.AddControlCacheForAncestor(e.ArtifactAncestorData);
                cCache = SecurityCache.GetControlGroupById(e.ArtifactAncestorData.Id);

                //  Leave this one disabled and use the one in the outlook bar.  We can use this button in emergancy by setting its xaml to visible.
                // Application Admin button
                DP.SetControlSecurityId(btnAppAdmin, new Guid("5DF7BD0F-F8A6-43C8-BB2A-968AC098D1A5"));
                SecurityCache.SetWPFActionControlState(btnAppAdmin, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Project Members button
                DP.SetControlSecurityId(btnProjectMembers, new Guid("82b8a961-c939-422b-b53e-ff0a4f6aa824"));
                SecurityCache.SetWPFActionControlState(btnProjectMembers, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Report Admin button
                DP.SetControlSecurityId(btnReportAdmin, new Guid("93e256f9-6509-4fc6-8b88-18a6fada8336"));
                SecurityCache.SetWPFActionControlState(btnReportAdmin, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Filter Admin button
                DP.SetControlSecurityId(btnFilterAdmin, new Guid("1fe2640c-7ada-4630-858d-0db0e0dcc2dd"));
                SecurityCache.SetWPFActionControlState(btnFilterAdmin, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Job Admin button
                DP.SetControlSecurityId(btnFilterAdmin, new Guid("2dafe1aa-37cb-44ee-a0b8-121a0365b942"));
                SecurityCache.SetWPFActionControlState(btnJobAdmin, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Ifx Dashboard button
                DP.SetControlSecurityId(btnFilterAdmin, new Guid("96d830bd-20db-4823-aa4c-05e927790378"));
                SecurityCache.SetWPFActionControlState(btnIfxDashboard, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Leave);
            }
        }
















    }








    #region AdminButtonClicked


    public delegate void AdminButtonClickedEventHandler(object sender, AdminButtonClickedArgs e);

    public class AdminButtonClickedArgs : EventArgs
    {
        private readonly string _buttonName = "";


        public AdminButtonClickedArgs(string buttonName)
        {
            _buttonName = buttonName;
        }


        public string ButtonName
        {
            get { return _buttonName; }
        } 

    }

    #endregion AdminButtonClicked











}
