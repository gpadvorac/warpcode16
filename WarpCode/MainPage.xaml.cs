﻿using System;
using System.Windows;
using System.Windows.Controls;
using TypeServices;
using Velocity.SL;
//using vSecurityClientAuthenticationSL;
using System.ComponentModel;
using WarpCode.Resources;
using Ifx.SL;
using vUICommon;
using EntityBll.SL;
using vAdmin;
using System.Windows.Media.Imaging;
using IfxConfigSL;
//using Reports;
using System.Diagnostics;
using UIControls;
using System.Windows.Media;
using Infragistics.Controls.Menus;
using ProjectManager;
using ApplicationTypeServices;
using System.Reflection;
using Reports;
using vFilterAdmin;
using vReportAdminSL;
using vFilterDataManager;
using vFilterControls;
using ProxyWrapper;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using IfxDashboard;
using Infragistics.Controls.Interactions;
using vComboDataTypes;
using vScheduleManager;
using vDialogControl;
using vNotification;
using vSecurityClientUserAdminSL;
using WindowState = Infragistics.Controls.Interactions.WindowState;

namespace WarpCode
{
    public partial class MainPage : UserControl, IMainPage
    {

        //  code to get the assembly version
        //  http://forums.silverlight.net/t/128861.aspx/1


        #region Initialize Variables

        private static string _as = "WarpCode";
        private static string _cn = "MainPage";
        private IEntityControl _activeEntityControl;
        private UserControl _activeContent = null;  // This is the main/top level-visible content to the right of the project explorer.


        public event BrokenRuleEventHandler BrokenRuleChanged;
        public event CrudFailedEventHandler CrudFailed;
        public event CurrentEntityStateEventHandler CurrentEntityStateChanged;
        /// <summary>
        /// 	<para>A flag telling us if this is the active entity control. A complex screen can
        ///     have many entity controls each with additional nested entity controls. Only one
        ///     entity control can be active at a time. When a user clicks or tabs into a,
        ///     EntityList, EntityProps, or any other child control of an entity control, this flag
        ///     is set to true. As code bubbles up or tunnels down through the many layers of WPF
        ///     elements, its often important to know when its entering the active entity
        ///     control.</para>
        /// 	<para>
        ///         Also see <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> which bubbles
        ///         up to the top level element (typically the main window) and passes a reference
        ///         up a reference of the active entity control.
        ///     </para>
        /// </summary>
        private bool _isActiveEntityControl = false;

        private IEntitiyPropertiesControl _activePropertiesControl;

        string _windowTitle = "WarpCode";

        bool _isSplitSreenMode = false;

        UserSecurityContext _userContext = new UserSecurityContext();

        //CommonClientDataService_ProxyWrapper _commonDataProxy = null;
        vGridColumnGroupService_ProxyWrapper _gridColumnGroupProxy = null;

        ucAdmin ucAd = null;
        //ucAppAdmin _ucApAd = null;

        ucProjectContacts ucMembers = null;
        //ucReportManager ucRM = null;
        //ucCompany ucCo = null;
        //ucProjectManager ucPM = null;

        ucProjectMangager ucPM = null;

        ucReportManager ucRM = null;

        ucReportExplorer ucRX = null;
        ucFilterManager ucFM = null;
        ucv_FilterSessionMD ucFA = null;

        ucv_Report ucRptAdm = null;
        ucFiles ucFl = null;

        vDialog _saveTractDialog = null;
        //SupportDataWindow _supportDataDialog = null;
        vDialog _supportDataDialog = null;

        ucAdminButtons ucAdBtn = null;
      


        private ucDashBaord _ucDashB = null;
        private ucPmDashboard ucDB = null;

        private vScheduleJobService_ProxyWrapper _scheduleJobProxyWrapper = null;
        private ComboItemList _jobs = new ComboItemList();

        private ucv_ScheduleJob ucJob = null;
        private ucJobList ucJobs = null;
        private ucv_NotificationEvent ucNtfcEvent = null;

        private ucTask ucTk = null;
        private ucDiscussionsMy ucDisc = null;

        private DiscussionSupportService_ProxyWrapper _discussionProxy;

        private ucWcApplication _ucApp = null;
        private ucWcTable _ucTb = null;



        Guid _reportModuleId = new Guid("C3A713BD-77C6-4336-877D-F1FFD655229C");

        #endregion Initialize Variables


        

        public MainPage()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "MainPage", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();

                ApplicationLevelVariables.MainGrid = this.LayoutRoot;

                LayoutRoot.ColumnDefinitions[0].Width = new GridLength(0);
                LayoutRoot.ColumnDefinitions[1].Width = new GridLength(0);

                obMain.SelectedGroupChanged += new EventHandler<SelectedGroupChangedEventArgs>(obMain_SelectedGroupChanged);
                Credentials.ApplicationId = new Guid(AppResource.ApplicationId);
                _userContext.SecurityArtifactsRetrieved += new SecurityArtifactsRetrievedEventHandler(UserSecurityContext_SecurityArtifactsRetrieved);

                SetSecurityState();


                ConfigureToCurrentEntityState(EntityStateSwitch.None);
                this.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(MainPage_MouseLeftButtonDown);

                //_commonDataProxy = new CommonClientDataService_ProxyWrapper();
              
                
                //  Not being used yet
                //_commonDataProxy.GetHelpDocumentationPathCompleted += new EventHandler<GetHelpDocumentationPathCompletedEventArgs>(GetHelpDocumentationPathCompleted);
                //_commonDataProxy.Begin_GetHelpDocumentationPath();

                //*** for the next line, search for   SupportDataHelper.ReloadStaticSupportData();
                //_commonDataProxy.GetCommonClientData_ReadOnlyStaticLists_AllCompleted += _commonDataProxy_GetCommonClientData_ReadOnlyStaticLists_AllCompleted;


                this.Loaded += new RoutedEventHandler(MainPage_Loaded);

                //LayoutRoot.SetValue(HelpProvider.HelpIndexProperty, 0);

                ucAdBtn = new ucAdminButtons();
                obMain.Groups[idx_obgAdmin].Content = ucAdBtn;
                ucAdBtn.AdminButtonClicked += new AdminButtonClickedEventHandler(ucAdBtn_AdminButtonClicked);

                _gridColumnGroupProxy = new vGridColumnGroupService_ProxyWrapper();
                _gridColumnGroupProxy.Getv_GridColumnGroupTypeUserPreference_lstByUserIdCompleted += Getv_GridColumnGroupTypeUserPreference_lstByUserIdCompleted;

                // This will cause the report server URL to be fetched and persited in a static variable.
                // There are times when the URL is not fetched in time when calling a report and therefore causes an error.
                Reports.ReportProvider rptProvider = new Reports.ReportProvider();

                ApplicationLevelVariables.MainPg = this;

                _discussionProxy = new DiscussionSupportService_ProxyWrapper();
                //_discussionProxy.GetDiscussion_lstBy_ProjectCompleted += _discussionProxy_GetDiscussion_lstBy_ProjectCompleted;

                CommonClientData_Bll_staticLists.LoadStaticLists();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "MainPage", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "MainPage", IfxTraceCategory.Leave);
            }
        }



        //private void ckLeaseCompression_Checked(object sender, RoutedEventArgs e)
        //{
        //    ApplicationLevelVariables.UseLeaseCompression = ckLeaseCompression.IsChecked.Value;
        //}



        void GetHelpDocumentationPathCompleted(object sender, GetHelpDocumentationPathCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetHelpDocumentationPathCompleted", IfxTraceCategory.Enter);

                if (e.Result != null)
                {
                    HelpProvider.HelpUrl = e.Result;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetHelpDocumentationPathCompleted", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetHelpDocumentationPathCompleted", IfxTraceCategory.Leave);
            }
        }



        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "MainPage_Loaded", IfxTraceCategory.Enter);
                //Guid prj_Id = new Guid("27CA7F13-DE80-4D21-8BB5-3BECE450AE68");
                //Lease_Bll_staticLists.LoadStaticLists(prj_Id);
                //CommonClientData_Bll_staticLists.LoadStaticLists(prj_Id);

                Assembly assembly = Assembly.GetExecutingAssembly();
                String version = assembly.FullName.Split(',')[1];
                String fullversion = version.Split('=')[1];
                mnuAssembly.Header = "Assembly Version: " + version;

                v_FilterSessionMD_Bll_staticLists.LoadStaticLists();






            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "MainPage_Loaded", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "MainPage_Loaded", IfxTraceCategory.Leave);
            }
        }

        private void ucPrjLst_Loaded(object sender, RoutedEventArgs e)
        {
            ucPrjLst = sender as ucProjectListing; ;
            ucPrjLst.EntitySelected += new EntitySelectedEventHandler(ucPrjLst_EntitySelected);
        }

        //private void ucPrjExp_Loaded(object sender, RoutedEventArgs e)
        //{
        //    ucPrjExp = sender as ucProjectExplorer; ;
        //    ucPrjExp.EntitySelected += new EntitySelectedEventHandler(ucPrjLst_EntitySelected);
 
        //}




        void ucPrjLst_EntitySelected(object sender, EntitySelectedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucPrjLst_EntitySelected", IfxTraceCategory.Enter);
                if (ucPM == null)
                {
                    ucProjectMangager_Load();
                }
                if (e.TypeSelected == EntityType.Project)
                {
                    if (ContextValues.CurrentProjectId == e.ProjectId)
                    {
                        return;
                    }
                    ContextValues.CurrentProjectId = e.ProjectId;
                    ContextValues.CurrentProjectName = e.ProjectName;

                    //CommonClientData_Bll_staticLists.LoadStaticLists_All((Guid)ContextValues.CurrentProjectId, (Guid)Credentials.UserId);
                    //_commonDataProxy.Begin_GetCommonClientData_ReadOnlyStaticLists_All((Guid)ContextValues.CurrentProjectId, (Guid)Credentials.UserId);

                    SupportDataHelper.ReloadStaticSupportData();

              
                    
                    //CommonClientData_Bll_staticLists.LoadStaticLists((Guid)ContextValues.CurrentProjectId, (Guid)Credentials.UserId);
                    //Contract_Bll_staticLists.LoadStaticLists();
                    //ContractDtFlag_Bll_staticLists.LoadStaticLists((Guid)ContextValues.CurrentProjectId);
                    //ContractFlag_Bll_staticLists.LoadStaticLists((Guid)ContextValues.CurrentProjectId);
                 

                    switch (obMain.SelectedGroup.Name)
                    {
                        case "obgProjExp":
                            //ucPrjExp.GetEntityGroups();
                            //ucPM.SetStateFromParent(e.TypeSelected, ContextValues.CurrentProjectId);
                            //// ucPM might not be the active content, so hide what ever is the active content instead.
                            //if (_activeContent != null)
                            //{
                            //    _activeContent.Visibility = System.Windows.Visibility.Collapsed;
                            //}
                            break;
                        case "obgReports":
                            ucRX_Load();
                            if (_activeContent != null)
                            {
                                _activeContent.Visibility = System.Windows.Visibility.Collapsed;
                            }
                            // This is needed for now becuase we are updating filters frequently and this will allow users to get the updates without needing to close and reopen the browser.
                            FilterDataProvider obj = new FilterDataProvider();
                            obj.RefreshFilterCacheFromServer();

                            break;
                        case "obgSupportData":

                            break;
                        case "obgAdmin":
                            if (_activeContent != null)
                            {
                                _activeContent.Visibility = System.Windows.Visibility.Collapsed;
                            }

                            break;
                    
                    }

                }
                else if (e.TypeSelected == EntityType.File)
                {
                    ucFl_Load();
                }
                else
                {
                    DisplayActiveUserControl(ucPM);
                    ucPM.SetStateFromParent(e.TypeSelected, ContextValues.CurrentProjectId);
                }

                if (ucAd != null)
                {
                    ucAd.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucPrjLst_EntitySelected", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucPrjLst_EntitySelected", IfxTraceCategory.Leave);
            }
        }


        void Getv_GridColumnGroupTypeUserPreference_lstByUserIdCompleted(object sender, Getv_GridColumnGroupTypeUserPreference_lstByUserIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_lstByUserIdCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                ApplicationLevelVariables.GridColumnGroupTypePreferenceList.Clear();

                if (array != null && array.Length > 0)
                {
                    for (int i = 0; i < array.Length; i++)
                    {
                        object[] obj = (object[])array[i];
                        if (obj != null && obj[0] != null && obj[1] != null)
                        {
                            ApplicationLevelVariables.GridColumnGroupTypePreferenceList.Add(new ApplicationLevelVariables.GridColumnGroupTypePreference(obj));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_lstByUserIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_lstByUserIdCompleted", IfxTraceCategory.Leave);
            }
        }




        #region Login - Out


        private void BtnTest2_OnClick(object sender, RoutedEventArgs e)
        {
            hlbLogin_Click(sender, e);
        }

        LoginRegistrationWindow loginRegForm = null;
        private void hlbLogin_Click(object sender, RoutedEventArgs e)
        {
            if (hlbLogin.Content.ToString() == "Login")
            {
                loginRegForm = new LoginRegistrationWindow(LoginRegProfileViewContext.Login);
                loginRegForm.Authenticated += new AuthenticatedEventHandler(loginRegForm_Authenticated);
                loginRegForm.Registered += new RegisteredEventHandler(loginRegForm_Registered);
                loginRegForm.ViewContextMode = LoginRegProfileViewContext.Login;
                loginRegForm.Show();
                IncreaseIsolatedStorageToDefaultSize();
            }
            else
            {
                hlbLogin.Content = "Login";
                hlbProfile.Visibility = Visibility.Collapsed;

                UserSecurityContext.ResetCurrent();
                SetSecurityState();
            }
        }


        void IncreaseIsolatedStorageToDefaultSize()
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IncreaseIsolatedStorageToDefaultSize", IfxTraceCategory.Enter);

            //    try
            //    {
            //        using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            //        {
            //            // Request 15MB more space in bytes.
            //            Int64 spaceToAdd = 15242880;
            //            Int64 curAvail = store.AvailableFreeSpace;

            //            if (curAvail < spaceToAdd)
            //            {

            //                // Request more quota space.
            //                if (!store.IncreaseQuotaTo(store.Quota + spaceToAdd))
            //                {
            //                    // The user clicked NO to the
            //                    // host's prompt to approve the quota increase.
            //                }
            //                else
            //                {
            //                    // The user clicked YES to the
            //                    // host's prompt to approve the quota increase.
            //                }
            //            }
            //        }
            //    }

            //    catch (IsolatedStorageException iex)
            //    {
            //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IncreaseIsolatedStorageToDefaultSize", iex);
            //        throw IfxWrapperException.GetError(iex, (Guid)traceId);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IncreaseIsolatedStorageToDefaultSize", ex);
            //    throw IfxWrapperException.GetError(ex, (Guid)traceId);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IncreaseIsolatedStorageToDefaultSize", IfxTraceCategory.Leave);
            //}
        }


        private void hlbProfile_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "hlbProfile_Click", IfxTraceCategory.Enter);
                ucv_UserProfile_UserEditProps profile = new ucv_UserProfile_UserEditProps();
                profile.LoadUserProfile();
                var dlg = vDialogHelper.InitializeEntityDialog(profile, "Edit Your Profile", null);
                dlg.Show();
                profile.CloseThis += (o, args) =>
                {
                    dlg.Close();
                    profile = null;
                    dlg = null;
                };
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "hlbProfile_Click", ex);
                //ExceptionHelper.NotifyUserAnExceptionOccured();
                MessageBox.Show(_as + "." + _cn + "hlbProfile_Click:  There was an exception:  " + ex.ToString());
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "hlbProfile_Click", IfxTraceCategory.Leave);
            }
        }

        void loginRegForm_Registered(object sender, RegisteredArgs e)
        {
            if (e.Success == RegisterationSuccess.Success)
            {
                hlbLogin.Content = "Log out";
                hlbProfile.Visibility = Visibility.Visible;
                SetSecurityState();
            }
        }

        void loginRegForm_Authenticated(object sender, AuthenticatedArgs e)
        {
            if (e.Success == AuthenticationSuccess.Success)
            {
                hlbLogin.Content = "Log out";
                hlbProfile.Visibility = Visibility.Visible;
                SetSecurityState();
            }
        }

        #endregion Login - Out


        #region Load Projects


        bool _isProjectsLoaded = false;

        public bool IsProjectsLoaded
        {
            get { return _isProjectsLoaded; }
            set { _isProjectsLoaded = value; }
        }

        #endregion Load Projects


        #region Open Screens

        private void btnProjects_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnProjects_Click", IfxTraceCategory.Enter);

                //ucPM_Load();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnProjects_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnProjects_Click", IfxTraceCategory.Leave);
            }
        }




        private void btnAppAdmin_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAppAdmin_Click", IfxTraceCategory.Enter);

                ucApAd_Load();
                //_ucApAd.InitializeDefaultScren();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAppAdmin_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAppAdmin_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnSysAdmin_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnSysAdmin_Click", IfxTraceCategory.Enter);

                ucAd_Load();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnSysAdmin_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnSysAdmin_Click", IfxTraceCategory.Leave);
            }
        }

        //private void btnFilterAdmin_Click(object sender, RoutedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnFilterAdmin_Click", IfxTraceCategory.Enter);

        //        ucFAM_Load();

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnFilterAdmin_Click", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnFilterAdmin_Click", IfxTraceCategory.Leave);
        //    }
        //}

        //private void btnReportAdmin_Click(object sender, RoutedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnReportAdmin_Click", IfxTraceCategory.Enter);

        //        ucRptAdm_Load();

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnReportAdmin_Click", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnReportAdmin_Click", IfxTraceCategory.Leave);
        //    }
        //}

        private void btnWalkthruAdmin_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnWalkthruAdmin_Click", IfxTraceCategory.Enter);

                //ucWkAdm_Load();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnWalkthruAdmin_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnWalkthruAdmin_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnJobAdmin_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnJobAdmin_OnClick", IfxTraceCategory.Enter);

                ucJob_Load();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnJobAdmin_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnJobAdmin_OnClick", IfxTraceCategory.Leave);
            }
        }



        private void btnIfxDashboard_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnIfxDashboard_OnClick", IfxTraceCategory.Enter);

                ucExcLog_Load();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnIfxDashboard_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnIfxDashboard_OnClick", IfxTraceCategory.Leave);
            }
        }


        void ucAdBtn_AdminButtonClicked(object sender, AdminButtonClickedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucAdBtn_AdminButtonClicked", IfxTraceCategory.Enter);

                switch (e.ButtonName)
                {
                    case "btnProjectMembers":
                        ucMembers_Load();
                        break;
                    case "btnAppAdmin":
                        ucAd_Load();
                        break;
                    case "btnFilterAdmin":
                        ucFAM_Load();
                        break;
                    case "btnReportAdmin":
                        ucRptAdm_Load();
                        break;
                    case "btnJobAdmin":
                        ucJob_Load();
                        break;
                    case "btnIfxDashboard":
                        _ucDashB_Load();
                        break;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucAdBtn_AdminButtonClicked", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucAdBtn_AdminButtonClicked", IfxTraceCategory.Leave);
            }
        }



        #endregion Open Screens

        
        #region Ifx
        
        private void btnIfxConfig_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnIfxConfig_Click", IfxTraceCategory.Enter);

                Load_ucIfxLog();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnIfxConfig_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnIfxConfig_Click", IfxTraceCategory.Leave);
            }
        }

        //private vDialog _wsConfigDialog = null;
        //void Load_ucIfxLog()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Load_ucIfxLog", IfxTraceCategory.Enter);

        //        if (_wsConfigDialog != null)
        //        {
        //            _wsConfigDialog.WindowState = WindowState.Normal;
        //            return;
        //        }
        //        ucIfxConfiguration cnfg = new ucIfxConfiguration();
        //        cnfg.EnableDisableTraceButtons += cnfg_EnableDisableTraceButtons;
        //        _wsConfigDialog = new vDialog
        //        {
        //            Content = cnfg,
        //            Header = "Logs and Log Configuration",
        //            HeaderIconVisibility = Visibility.Collapsed,
        //            MinimizeButtonVisibility = Visibility.Collapsed,
        //            MaximizeButtonVisibility = Visibility.Collapsed,
        //            StartupPosition = StartupPosition.Center
        //        };
        //        _wsConfigDialog.WindowStateChanged += _wsConfigDialog_WindowStateChanged;
        //        _wsConfigDialog.Show();
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Load_ucIfxLog", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Load_ucIfxLog", IfxTraceCategory.Leave);
        //    }
        //}


        cwConfig cwC = null;
        void Load_ucIfxLog()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Load_ucIfxLog", IfxTraceCategory.Enter);

                cwC = new cwConfig();
                cwC.Show();
                cwC.EnableDisableTraceButtons += new EventHandler<RoutedEventArgs>(ucIfxLog_EnableDisableTraceButtons);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Load_ucIfxLog", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Load_ucIfxLog", IfxTraceCategory.Leave);
            }
        }


        public event EventHandler<RoutedEventArgs> EnableDisableTraceButtons;

        void ucIfxLog_EnableDisableTraceButtons(object sender, RoutedEventArgs e)
        {
            try
            {
                EventHandler<RoutedEventArgs> handler = EnableDisableTraceButtons;
                if (null != handler)
                {
                    handler(this, new RoutedEventArgs());
                }
            }
            catch (Exception ex)
            {

            }
        }




        void cnfg_EnableDisableTraceButtons(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "cnfg_EnableDisableTraceButtons", IfxTraceCategory.Enter);
                if (mnuStartStopTrace.Visibility == System.Windows.Visibility.Visible)
                {
                    mnuStartStopTrace.Visibility = System.Windows.Visibility.Collapsed;
                }
                else
                {
                    mnuStartStopTrace.Visibility = System.Windows.Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "cnfg_EnableDisableTraceButtons", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "cnfg_EnableDisableTraceButtons", IfxTraceCategory.Leave);
            }
        }
        
        //void _wsConfigDialog_WindowStateChanged(object sender, WindowStateChangedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_wsConfigDialog_WindowStateChanged", IfxTraceCategory.Enter);

        //        if (e.NewWindowState == Infragistics.Controls.Interactions.WindowState.Hidden)
        //        {
        //            sender = null;
        //            _wsConfigDialog = null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_wsConfigDialog_WindowStateChanged", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_scheduleDialog_WindowStateChanged", IfxTraceCategory.Leave);
        //    }
        //}

        
        //void ucIfxLog_EnableDisableTraceButtons(object sender, RoutedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucIfxLog_EnableDisableTraceButtons", IfxTraceCategory.Enter);
        //        if (mnuStartStopTrace.Visibility == System.Windows.Visibility.Visible)
        //        {
        //            mnuStartStopTrace.Visibility = System.Windows.Visibility.Collapsed;
        //        }
        //        else
        //        {
        //            mnuStartStopTrace.Visibility = System.Windows.Visibility.Visible;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucIfxLog_EnableDisableTraceButtons", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucIfxLog_EnableDisableTraceButtons", IfxTraceCategory.Leave);
        //    }
        //}
        
        private void btnStartStopTrace_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnStartStopTrace_Click", IfxTraceCategory.Enter);

                StartStopTraceSession();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnStartStopTrace_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnStartStopTrace_Click", IfxTraceCategory.Leave);
            }
        }
        
        private void StartStopTraceSession()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "StartStopTraceSession", IfxTraceCategory.Enter);
                //IfxTrace._saveTraceListFromSession = !IfxTrace._saveTraceListFromSession;
                if (IfxTrace._isTraceSessionStarted == true)
                {
                    //IfxTrace._saveTraceListFromSession = false;

                    //imgStartTrace = vVelocityImageProvider.LoadImage(vVelocityImageProvider.GetMenuVelocityImagePath(vVelocityImageProvider.MenuImage.StopNormalGreen16));
                    imgStartTrace.Source = new BitmapImage(vVelocityImageProvider.GetRelativeUri(vVelocityImageProvider.MenuImage.StopNormalGreen16));

                    // Prompt user for comment and save location
                    if (IfxEvent.TraceList.Count == 0)
                    {
                        MessageBox.Show("No Trace Items have been captured yet.  Make sure the correct 'Trave Events' have been turned on.", "No Trace Captured", MessageBoxButton.OKCancel);
                        return;
                    }
                    OpenSaveTraceWindow();
                }
                else
                {
                    //IfxTrace._saveTraceListFromSession = true;
                    // Clear the list as we are starting a new one now.
                    IfxEvent.TraceSession = null;
                    IfxEvent.TraceSession = new IfxTraceSession();
                    IfxEvent.TraceList.Clear();
                    IfxTrace.ResetTraceCounter();
                    //imgStartTrace = vVelocityImageProvider.LoadImage(vVelocityImageProvider.GetMenuVelocityImagePath(vVelocityImageProvider.MenuImage.StopNormalRed16));
                    imgStartTrace.Source = new BitmapImage(vVelocityImageProvider.GetRelativeUri(vVelocityImageProvider.MenuImage.StopNormalRed16));
                }
                IfxTrace._isTraceSessionStarted = !IfxTrace._isTraceSessionStarted;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "StartStopTraceSession", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "StartStopTraceSession", IfxTraceCategory.Leave);
            }
        }
        
        ucSaveTrace _ucSave = null;
        private void OpenSaveTraceWindow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "StartStopTraceSession", IfxTraceCategory.Enter);
                if (_saveTractDialog != null)
                {
                    _saveTractDialog.WindowState = WindowState.Normal;
                    return;
                }
                _ucSave = new ucSaveTrace();
                _saveTractDialog = new vDialog()
                {
                    Content = _ucSave,
                    StartupPosition = StartupPosition.Center
                };
                _saveTractDialog.WindowStateChanged += _saveTractDialog_WindowStateChanged;
                _saveTractDialog.Show();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "StartStopTraceSession", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "StartStopTraceSession", IfxTraceCategory.Leave);
            }
        }

        void _saveTractDialog_WindowStateChanged(object sender, WindowStateChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_saveTractDialog_WindowStateChanged", IfxTraceCategory.Enter);

                if (e.NewWindowState == Infragistics.Controls.Interactions.WindowState.Hidden)
                {
                    _ucSave = null;
                    sender = null;
                    _saveTractDialog = null;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_saveTractDialog_WindowStateChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_saveTractDialog_WindowStateChanged", IfxTraceCategory.Leave);
            }
        }
        
        
        #endregion Ifx 
                
       
        #region Load Controls
        
        void ucMembers_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucMembers_Load", IfxTraceCategory.Enter);
                if (ucMembers == null)
                {
                    ucMembers = new ucProjectContacts();
                    LayoutRoot.Children.Add(ucMembers);
                    Grid.SetRow(ucMembers, 1);
                    Grid.SetColumn(ucMembers, 2);
                }

                DisplayActiveUserControl(ucMembers);
                ucMembers.LoadLists();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucMembers_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucMembers_Load", IfxTraceCategory.Leave);
            }
        }
        
        void ucAd_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucAd_Load", IfxTraceCategory.Enter);
                if (ucAd == null)
                {
                    ucAd = new ucAdmin();
                    LayoutRoot.Children.Add(ucAd);
                    Grid.SetRow(ucAd, 1);
                    Grid.SetColumn(ucAd, 2);
                }

                DisplayActiveUserControl(ucAd);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucAd_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucAd_Load", IfxTraceCategory.Leave);
            }
        }

        void ucApAd_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucApAd_Load", IfxTraceCategory.Enter);
                //if (_ucApAd == null)
                //{
                //    _ucApAd = new ucAppAdmin();
                //    _loadedControls.Add(_ucApAd);
                //    LayoutRoot.Children.Add(_ucApAd);
                //    Grid.SetRow(_ucApAd, 1);
                //    Grid.SetColumn(_ucApAd, 2);
                //}
                //DisplayActiveUserControl(_ucApAd);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucApAd_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucApAd_Load", IfxTraceCategory.Leave);
            }
        }


        private void ucExcLog_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucExcLog_Load", IfxTraceCategory.Enter);

                if (_ucDashB == null)
                {
                    _ucDashB = new ucDashBaord();
                    LayoutRoot.Children.Add(_ucDashB);
                    Grid.SetRow(_ucDashB, 1);
                    Grid.SetColumn(_ucDashB, 2);
                }
                DisplayActiveUserControl(_ucDashB);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucExcLog_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucExcLog_Load", IfxTraceCategory.Leave);
            }
        }



        void ucProjectMangager_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectMangager_Load", IfxTraceCategory.Enter);
                if (ucPM == null)
                {
                    ucPM = new ucProjectMangager();
                    ucPM.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    ucPM.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                    ucPM.Visibility = System.Windows.Visibility.Collapsed;
                    LayoutRoot.Children.Add(ucPM);
                    Grid.SetRow(ucPM, 1);
                    Grid.SetColumn(ucPM, 2);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectMangager_Load", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectMangager_Load", IfxTraceCategory.Leave);
            }
        }
        
        void ucRM_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucRM_Load", IfxTraceCategory.Enter);
                if (ucRM == null)
                {
                    ucRM = new ucReportManager();

                    LayoutRoot.Children.Add(ucRM);
                    Grid.SetRow(ucRM, 1);
                    Grid.SetColumn(ucRM, 2);
                }
                ucRM.LoadReports();

                DisplayActiveUserControl(ucRM);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucRM_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucRM_Load", IfxTraceCategory.Leave);
            }
        }

        void ucRX_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucRX_Load", IfxTraceCategory.Enter);

                if (gdReports == null)
                {
                    //SolidColorBrush _defaultForeColor = new SolidColorBrush(Color.FromArgb(180, 0, 78, 137));
                    gdReports = (Grid)obMain.Groups[idx_obgReports].Content;
                    txbProject1 = new TextBlock();
                    txbProject1.Margin = new Thickness(5, 8, 2, 0);
                    //txbProject1.FontWeight = FontWeights.Bold;
                    txbProject1.FontSize = 9;
                    txbProject1.Foreground = new SolidColorBrush(Color.FromArgb(150, 49, 127, 160));
                    //Get font color
                    txbProject2 = new TextBlock();
                    txbProject2.Margin = new Thickness(5, 3, 5, 12);
                    txbProject2.FontWeight = FontWeights.Bold;
                    txbProject2.FontSize = 12;
                    txbProject2.Foreground = new SolidColorBrush(Color.FromArgb(255, 49, 127, 160));

                    gdReports.Children.Add(txbProject1);
                    gdReports.Children.Add(txbProject2);
                    Grid.SetRow(txbProject1, 0);
                    Grid.SetRow(txbProject2, 1);
                    obMain.Groups[idx_obgReports].Content = gdReports;
                }

                if (ContextValues.CurrentProjectId == null)
                {
                    gdReports.RowDefinitions[0].Height = new GridLength(10);
                    txbProject1.Text = "";
                    txbProject2.Text = "Select a project first";
                    return;
                }
                else
                {
                    gdReports.RowDefinitions[0].Height = new GridLength(0, GridUnitType.Auto);
                    txbProject1.Text = "Current Project";
                    txbProject2.Text = ContextValues.CurrentProjectName;
                    if (ucRX == null)
                    {
                        ucRX = new ucReportExplorer();
                        gdReports.Children.Add(ucRX);
                        Grid.SetRow(ucRX, 2);

                        // Load the Filter Manager, but dont show it
                        if (ucFM == null)
                        {
                            ucFM_Load();
                            ucRX.SetFilterManager(ucFM);
                        }
                    }

                }


                ucRX.LoadReportNodes(_reportModuleId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucRX_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucRX_Load", IfxTraceCategory.Leave);
            }
        }
      
        
        void ucFM_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFM_Load", IfxTraceCategory.Enter);
                if (ucFM == null)
                {
                    ucFM = new ucFilterManager();
                    ucFM.Visibility = System.Windows.Visibility.Collapsed;
                    LayoutRoot.Children.Add(ucFM);
                    Grid.SetRow(ucFM, 1);
                    Grid.SetColumn(ucFM, 2);
                    ucFM.InUse += new EventHandler(ucFM_InUse);
                }
                // Don't display it yet, just load it ready to display
                //DisplayActiveUserControl(ucFM);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFM_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFM_Load", IfxTraceCategory.Leave);
            }
        }

        void ucFM_InUse(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFM_InUse", IfxTraceCategory.Enter);
                DisplayActiveUserControl(ucFM);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFM_InUse", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFM_InUse", IfxTraceCategory.Leave);
            }
        }
        
        void ucRptAdm_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucRptAdm_Load", IfxTraceCategory.Enter);
                if (ucRptAdm == null)
                {
                    ucRptAdm = new ucv_Report();
                    LayoutRoot.Children.Add(ucRptAdm);
                    Grid.SetRow(ucRptAdm, 1);
                    Grid.SetColumn(ucRptAdm, 2);
                }

                ucRptAdm.SetStateFromParent(null, Credentials.ApplicationId, null, null, "");
                DisplayActiveUserControl(ucRptAdm);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucRptAdm_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucRptAdm_Load", IfxTraceCategory.Leave);
            }
        }





        private void ucJob_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucJob_Load", IfxTraceCategory.Enter);

                if (ucJob == null)
                {
                    ucJob = new ucv_ScheduleJob();
                    LayoutRoot.Children.Add(ucJob);
                    Grid.SetRow(ucJob, 1);
                    Grid.SetColumn(ucJob, 2);
                }
                ucJob.SetStateFromParent(null, Credentials.ApplicationId, null, null, "");
                DisplayActiveUserControl(ucJob);
                v_Schedule_Bll_staticLists.LoadStaticLists();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucJob_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucJob_Load", IfxTraceCategory.Leave);
            }
        }
        private void ucJobs_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucJobs_Load", IfxTraceCategory.Enter);

                if (ucJobs == null)
                {
                    ucJobs = new ucJobList();
                    ucJobs.ShowNotificationEvents += ucJobs_ShowNotificationEvents;
                    obMain.Groups[idx_obgSchedule].Content = ucJobs;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucJobs_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucJobs_Load", IfxTraceCategory.Leave);
            }
        }



        void ucJobs_ShowNotificationEvents(object sender, ShowNotificationEventsArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNtf_ShowNotificationEvents", IfxTraceCategory.Enter);

                ucNtfcEvent_Load();
                //ucNtfcEvent.LoadForSendEvent(e.NotificationId, e.NotificationTitle);
                ucNtfcEvent.SetStateFromParent(null, e.NotificationId, null, null, null);
                ucNtfcEvent.SetEventTitle(e.NotificationTitle);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNtf_ShowNotificationEvents", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNtf_ShowNotificationEvents", IfxTraceCategory.Leave);
            }
        }

        void ucNtfcEvent_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNtfcEvent_Load", IfxTraceCategory.Enter);
                if (ucNtfcEvent == null)
                {
                    ucNtfcEvent = new ucv_NotificationEvent();

                    LayoutRoot.Children.Add(ucNtfcEvent);
                    Grid.SetRow(ucNtfcEvent, 1);
                    Grid.SetColumn(ucNtfcEvent, 2);
                }
                DisplayActiveUserControl(ucNtfcEvent);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNtfcEvent_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNtfcEvent_Load", IfxTraceCategory.Leave);
            }
        }



        private void _ucDashB_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_ucDashB_Load", IfxTraceCategory.Enter);

                if (_ucDashB == null)
                {
                    _ucDashB = new ucDashBaord();
                    LayoutRoot.Children.Add(_ucDashB);
                    Grid.SetRow(_ucDashB, 1);
                    Grid.SetColumn(_ucDashB, 2);
                }
                DisplayActiveUserControl(_ucDashB);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_ucDashB_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_ucDashB_Load", IfxTraceCategory.Leave);
            }
        }


        private void ucDB_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucDB_Load", IfxTraceCategory.Enter);

                if (ucDB == null)
                {
                    ucDB = new ucPmDashboard();
                    LayoutRoot.Children.Add(ucDB);
                    Grid.SetRow(ucDB, 1);
                    Grid.SetColumn(ucDB, 2);
                    //ucDB.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                }
                DisplayActiveUserControl(ucDB);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucDB_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucDB_Load", IfxTraceCategory.Leave);
            }
        }


        void ucFl_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFl_Load", IfxTraceCategory.Enter);
                if (ucFl == null)
                {
                    ucFl = new ucFiles();
                    LayoutRoot.Children.Add(ucFl);
                    Grid.SetRow(ucFl, 1);
                    Grid.SetColumn(ucFl, 2);
                }
                ucFl.SetStateFromParent(null, "Project", null, ContextValues.CurrentProjectId, null, null, null, null, null);
                DisplayActiveUserControl(ucFl);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFl_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFl_Load", IfxTraceCategory.Leave);
            }
        }




        private void ucTk_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTk_Load", IfxTraceCategory.Enter);

                if (ucTk == null)
                {

                    ucTk = new ucTask();
                    ucTk.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    ucTk.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                    LayoutRoot.Children.Add(ucTk);
                    Grid.SetRow(ucTk, 1);
                    Grid.SetColumn(ucTk, 2);

                }
                //DisplayActiveUserControl(ucTk);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTk_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTk_Load", IfxTraceCategory.Leave);
            }
        }


        private void ucDisc_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucDisc_Load", IfxTraceCategory.Enter);

                if (ucDisc == null)
                {
                    ucDisc = new ucDiscussionsMy();
                    LayoutRoot.Children.Add(ucDisc);
                    Grid.SetRow(ucDisc, 1);
                    Grid.SetColumn(ucDisc, 2);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucDisc_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucDisc_Load", IfxTraceCategory.Leave);
            }
        }



        private void ucApp_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucApp_Load", IfxTraceCategory.Enter);

                if (_ucApp == null)
                {

                    _ucApp = new ucWcApplication();
                    _ucApp.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    _ucApp.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                    _ucApp.ChangeParentNavGridExpansion += ucNav_ChangeParentNavGridExpansion;
                    LayoutRoot.Children.Add(_ucApp);
                    Grid.SetRow(_ucApp, 1);
                    Grid.SetColumn(_ucApp, 2);

                }
                //DisplayActiveUserControl(_ucApp);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucApp_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucApp_Load", IfxTraceCategory.Leave);
            }
        }



        private void ucTb_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTb_Load", IfxTraceCategory.Enter);

                if (_ucTb == null)
                {

                    _ucTb = new ucWcTable();
                    _ucTb.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    _ucTb.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                    _ucTb.ChangeParentNavGridExpansion += ucNav_ChangeParentNavGridExpansion;
                    LayoutRoot.Children.Add(_ucTb);
                    Grid.SetRow(_ucTb, 1);
                    Grid.SetColumn(_ucTb, 2);

                }
                //DisplayActiveUserControl(_ucApp);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTb_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTb_Load", IfxTraceCategory.Leave);
            }
        }




        void DisplayActiveUserControl(UserControl uc)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DisplayActiveUserControl", IfxTraceCategory.Enter);
                UserControl activeContent = uc;
                activeContent.Visibility = System.Windows.Visibility.Visible;
                if (_activeContent != null && _activeContent != activeContent)
                {
                    _activeContent.Visibility = System.Windows.Visibility.Collapsed;
                }
                _activeContent = activeContent;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DisplayActiveUserControl", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DisplayActiveUserControl", IfxTraceCategory.Leave);
            }
        }
        
        #endregion Load Controls


        #region Data Operations



        void NewEntityRow()
        {
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "NewEntityRow", IfxTraceCategory.Enter);
                //  For security.  if the props control is disabled, then the user doesnt have permission to add a the row.
                if (((Control)_activeEntityControl.GetPropsControl()).IsEnabled == false) return;
                ((IEntityControl)_activeEntityControl).NewEntityRow();
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "NewEntityRow", IfxTraceCategory.Catch);
                //throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "NewEntityRow", IfxTraceCategory.Leave);
            }
        }

        void Save()
        {
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "Save", IfxTraceCategory.Enter);
                ((IEntityControl)_activeEntityControl).Save();
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "Save", IfxTraceCategory.Catch);
                //throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        void UnDo()
        {
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "UnDo", IfxTraceCategory.Enter);
                ((IEntityControl)_activeEntityControl).UnDo();
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "UnDo", IfxTraceCategory.Catch);
                //throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "UnDo", IfxTraceCategory.Leave);
            }
        }

        void Delete()
        {
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "Delete", IfxTraceCategory.Enter);
                //((IEntityControl)_activeEntityControl).Delete();
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "Delete", IfxTraceCategory.Catch);
                //throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "Delete", IfxTraceCategory.Leave);
            }
        }



        #endregion Data Operations


        #region Window Methods and Events

        void MainPage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "MainPage_MouseLeftButtonDown", IfxTraceCategory.Enter);

                FindActivePropertiesControlFromParent("MainPage_MouseLeftButtonDown", e.OriginalSource);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "MainPage_MouseLeftButtonDown", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "MainPage_MouseLeftButtonDown", IfxTraceCategory.Leave);
            }
        }
        
        private void ucSprtData_SupportFormSelected(object sender, SupportFormSelectedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucSprtData_SupportFormSelected", IfxTraceCategory.Enter);
                string msgSelectProject = "Please select a project first.";
                
                IEntityControl ucEnt = null;
                ICustomListControl ucLst = null;
                _supportDataDialog = new vDialog
                {
                    MinimizeButtonVisibility = Visibility.Collapsed,
                    StartupPosition = StartupPosition.Center,
                    HeaderIconVisibility = Visibility.Collapsed
                };
                _supportDataDialog.WindowStateChanged += _supportDataDialog_WindowStateChanged;


                switch (e.TypeSelected)
                {

                 
                    case SupportFormType.Person:
                        ucLst = new ucPersonContactList();
                        ((ucPersonContactList)ucLst).SetStateFromParent(null, null, null, null, null, null, null, null);
                        _supportDataDialog.Content = ucLst;
                        _supportDataDialog.Header = "Person";
                        _supportDataDialog.Height = 400;
                        _supportDataDialog.Width = 690;
                        break;
                    case SupportFormType.Project:
                        ucLst = new ucProjectList();
                        ((ucProjectList)ucLst).SetStateFromParent(null, null, null, null, null, null, null, null);
                        _supportDataDialog.Content = ucLst;
                        _supportDataDialog.Header = "Project";
                        _supportDataDialog.Height = 400;
                        _supportDataDialog.Width = 320;
                        break;
                


                    case SupportFormType.TaskCategory:
                        ucLst = new ucTaskCategoryList();
                        ((ucTaskCategoryList)ucLst).SetStateFromParent(null, null, null, null, null, null, null, null);
                        _supportDataDialog.Content = ucLst;
                        _supportDataDialog.Header = "Task Category";
                        _supportDataDialog.Height = 400;
                        _supportDataDialog.Width = 520;
                        break;



                    default:
                        _supportDataDialog.Content = null;
                        _supportDataDialog = null;
                        return;
                        
                }

                _supportDataDialog.Show();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucSprtData_SupportFormSelected", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucSprtData_SupportFormSelected", IfxTraceCategory.Leave);
            }
        }

        void OpenSupportWindow_MissingProjectId( ref vDialog spWn)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OpenSupportWindow_MissingProjectId", IfxTraceCategory.Enter);

                MessageBox.Show("Please select a project first.", "Missing Project", MessageBoxButton.OK);
                spWn.Content = null;
                spWn = null;
                return;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OpenSupportWindow_MissingProjectId", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OpenSupportWindow_MissingProjectId", IfxTraceCategory.Leave);
            }
        }

        void _supportDataDialog_WindowStateChanged(object sender, WindowStateChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_supportDataDialog_WindowStateChanged", IfxTraceCategory.Enter);

                if (e.NewWindowState == Infragistics.Controls.Interactions.WindowState.Hidden)
                {
                    
                    string type = ((vDialog)sender).Content.ToString();

                    switch (type)
                    {
                     
                        //case "UIControls.ucRecordingTypeList":
                        //    CommonClientData_Bll_staticLists.Refresh_RecordingType_ComboItemList_BindingList();
                        //    break;
                        //case "UIControls.ucState":
                        //    CommonClientData_Bll_staticLists.Refresh_State_ComboItemList_BindingList();
                        //    CommonClientData_Bll_staticLists.Refresh_StateCounty_ComboItemList_BindingList();
                        //    CommonClientData_Bll_staticLists.Refresh_County_ComboItemList_BindingList();
                        //    break;
                        //case "UIControls.ucUnit":
                        //    Well_Bll_staticLists.Refresh_Unit_ComboItemList_BindingList((Guid)ContextValues.CurrentProjectId);
                        //    Well_Bll_staticLists.Refresh_UnitTract_ComboItemList_BindingList((Guid)ContextValues.CurrentProjectId);
                        //    break;
                        //case "UIControls.ucWellAltCategory1List":
                        //    Well_Bll_staticLists.Refresh_WellAltCategory1_ComboItemList_BindingList((Guid)ContextValues.CurrentProjectId);
                        //    break;
                     

                        case "vProjectManager.ucTaskCategoryList":
                            Task_Bll_staticLists.Refresh_TaskCategory_ComboItemList_BindingList();
                            break;

                    

                    }
                    
                    sender = null;
                    _supportDataDialog = null;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_supportDataDialog_WindowStateChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_supportDataDialog_WindowStateChanged", IfxTraceCategory.Leave);
            }
        }

        void obMain_SelectedGroupChanged(object sender, SelectedGroupChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "obMain_SelectedGroupChanged", IfxTraceCategory.Enter);


                switch (e.NewSelectedOutlookBarGroup.Name)
                {
                    case "obgProjExp":
                        //if (ucPrjExp != null && ContextValues.CurrentProjectId!=null)
                        //{
                        //    ucPrjExp.GetEntityGroups();
                        //    ucPM.SetStateFromParent(EntityType.Project, ContextValues.CurrentProjectId);
                        //    // ucPM might not be the active content, so hide what ever is the active content instead.
                        //    if (_activeContent != null)
                        //    {
                        //        _activeContent.Visibility = System.Windows.Visibility.Collapsed;
                        //    }
                        //}
                        break;
                    case "obgReports":
                        ucRX_Load();
                        if (_activeContent != null)
                        {
                            _activeContent.Visibility = System.Windows.Visibility.Collapsed;
                        }
                        // This is needed for now becuase we are updating filters frequently and this will allow users to get the updates without needing to close and reopen the browser.
                        FilterDataProvider obj = new FilterDataProvider();
                        obj.RefreshFilterCacheFromServer();
                        break;
                    case "obgNotification":
                        v_Schedule_Bll_staticLists.LoadStaticLists();
                        ucJobs_Load();
                        break;
                    case "obgAdmin":
                        if (_activeContent != null)
                        {
                            _activeContent.Visibility = System.Windows.Visibility.Collapsed;
                        }
                        break;
                }
                
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "obMain_SelectedGroupChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "obMain_SelectedGroupChanged", IfxTraceCategory.Leave);
            }
        }
        
  
        #region State Related

        void ConfigureToCurrentEntityState(EntityStateSwitch state)
        {
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ConfigureToCurrentEntityState", IfxTraceCategory.Enter);

                switch (state)
                {
                    case EntityStateSwitch.None:
                        NewButton_IsEnabled = true; ;
                        SaveButton_IsEnabled = false;
                        UnDoButton_IsEnabled = false;
                        obMain.IsEnabled = true;
                        break;
                    case EntityStateSwitch.NewInvalidNotDirty:
                        NewButton_IsEnabled = false;
                        SaveButton_IsEnabled = false;
                        UnDoButton_IsEnabled = false;
                        obMain.IsEnabled = false;
                        break;
                    case EntityStateSwitch.NewValidNotDirty:
                        NewButton_IsEnabled = false;
                        SaveButton_IsEnabled = false;
                        UnDoButton_IsEnabled = false;
                        obMain.IsEnabled = false;
                        break;
                    case EntityStateSwitch.NewValidDirty:
                        NewButton_IsEnabled = false;
                        SaveButton_IsEnabled = true;
                        UnDoButton_IsEnabled = true;
                        obMain.IsEnabled = false;
                        break;
                    case EntityStateSwitch.NewInvalidDirty:
                        NewButton_IsEnabled = false;
                        SaveButton_IsEnabled = false;
                        UnDoButton_IsEnabled = true;
                        obMain.IsEnabled = false;
                        break;
                    case EntityStateSwitch.ExistingInvalidDirty:
                        NewButton_IsEnabled = false;
                        SaveButton_IsEnabled = false;
                        UnDoButton_IsEnabled = true;
                        obMain.IsEnabled = false;
                        break;
                    case EntityStateSwitch.ExistingValidDirty:
                        NewButton_IsEnabled = false;
                        SaveButton_IsEnabled = true;
                        UnDoButton_IsEnabled = true;
                        obMain.IsEnabled = false;
                        break;
                    case EntityStateSwitch.ExistingValidNotDirty:
                        NewButton_IsEnabled = true;
                        SaveButton_IsEnabled = false;
                        UnDoButton_IsEnabled = false;
                        obMain.IsEnabled = true;
                        break;
                }


            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_ns, _cn, "ConfigureToCurrentEntityState", IfxTraceCategory.Catch);
                //throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_ns, _cn, "ConfigureToCurrentEntityState", IfxTraceCategory.Leave);
            }
        }


        void SetActivePropertiesControl(IEntitiyPropertiesControl ctl)
        {
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "SetActivePropertiesControl", IfxTraceCategory.Enter);
                if (ctl == null)
                {
                    if (_activePropertiesControl != null)
                    {
                        _activePropertiesControl.SetIsActivePropertiesControl(false);
                        //  call something here to set the windows state
                        ConfigureToCurrentEntityState(EntityStateSwitch.None);
                    }
                    else
                    {
                        //  There was no active props control so there's nothing to do.
                        return;
                    }
                }
                else if (_activePropertiesControl == null)
                {
                    _activePropertiesControl = ctl;
                    _activePropertiesControl.SetIsActivePropertiesControl(true);
                    //_activePropertiesControl.RaiseCurrentEntityStateChanged();
                }
                else if (ctl != _activePropertiesControl)
                {
                    _activePropertiesControl.SetIsActivePropertiesControl(false);
                    _activePropertiesControl = ctl;
                    _activePropertiesControl.SetIsActivePropertiesControl(true);
                    //_activePropertiesControl.RaiseCurrentEntityStateChanged();
                }

           
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_ns, _cn, "SetActivePropertiesControl", IfxTraceCategory.Catch);
                //throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_ns, _cn, "SetActivePropertiesControl", IfxTraceCategory.Leave);
            }
        }

        void SetActiveEntityControl(IEntityControl ctl)
        {
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "SetActiveEntityControl", IfxTraceCategory.Enter);
                if (ctl == null)
                {
                    if (_activeEntityControl != null)
                    {
                        // First clear the props control variable
                        if (_activePropertiesControl != null)
                        {
                            _activePropertiesControl.SetIsActivePropertiesControl(false);
                            _activePropertiesControl = null;
                        }
                        _activeEntityControl.SetIsActiveEntityControl(false);
                        //  call something here to set the windows state
                        ConfigureToCurrentEntityState(EntityStateSwitch.None);
                    }
                    else
                    {
                        //  There was no active props control so there's nothing to do.
                        return;
                    }
                }
                else if (_activeEntityControl == null)
                {
                    _activeEntityControl = ctl;
                    _activeEntityControl.SetIsActiveEntityControl(true);
                    //_activePropertiesControl.RaiseCurrentEntityStateChanged();
                }
                else if (ctl != _activeEntityControl)
                {
                    //  Set props control stuff first
                    if (_activePropertiesControl != null)
                    {
                        _activePropertiesControl.SetIsActivePropertiesControl(false);
                    }

                    if (_activeEntityControl.GetIsPropsTabSelected() == true)
                    {
                        _activePropertiesControl = _activeEntityControl.GetPropsControl();
                    }

                    if (_activePropertiesControl != null)  //added this
                    {
                        _activePropertiesControl.SetIsActivePropertiesControl(true);
                    }


                    //  Set Entity control stuff next
                    _activeEntityControl.SetIsActiveEntityControl(false);
                    _activeEntityControl = ctl;
                    _activeEntityControl.SetIsActiveEntityControl(true);
                    //_activePropertiesControl.RaiseCurrentEntityStateChanged();
                }
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_ns, _cn, "SetActiveEntityControl", IfxTraceCategory.Catch);
                //throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_ns, _cn, "SetActiveEntityControl", IfxTraceCategory.Leave);
            }
        }

        void ExitApp()
        {
            //Application.Current.Shutdown();
            //Application.Current.
        }



        #region Manage Button States

        bool NewButton_IsEnabled
        {
            get
            {
                return mnuNew.IsEnabled;
            }
            set
            {
                if (value == true)
                {
                    mnuNew.IsEnabled = true;
                    //imgBtnNew.Source = bmNew;
                }
                else
                {
                    mnuNew.IsEnabled = false;
                    //imgBtnNew.Source = bmNewDis;
                }
            }
        }

        bool SaveButton_IsEnabled
        {
            get
            {
                return mnuSave.IsEnabled;
            }
            set
            {
                if (value == true)
                {
                    mnuSave.IsEnabled = true;
                    //imgBtnSave.Source = bmSave;
                }
                else
                {
                    mnuSave.IsEnabled = false;
                    //imgBtnSave.Source = bmSaveDis;
                }
            }
        }

        bool UnDoButton_IsEnabled
        {
            get
            {
                return mnuUnDo.IsEnabled;
            }
            set
            {
                if (value == true)
                {
                    mnuUnDo.IsEnabled = true;
                    //imgBtnUnDo.Source = bmUnDo;
                }
                else
                {
                    mnuUnDo.IsEnabled = false;
                    //imgBtnUnDo.Source = bmUnDoDis;
                }
            }
        }

        #endregion Manage Button States


        #endregion  State Related




        #region Collapse - Expand Parents


        public void CollapseExapndNavGrid(CollapseExpandParentEntityOptions option)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CollapseNavGrid", IfxTraceCategory.Enter);

                if (option == CollapseExpandParentEntityOptions.Collapse)
                {
                    gdSplitterMain.CollapseSplitter();
                }
                else if (option == CollapseExpandParentEntityOptions.Expand)
                {
                    gdSplitterMain.ExpandSplitter();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CollapseNavGrid", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CollapseNavGrid", IfxTraceCategory.Leave);
            }
        }


        //public void CollapseExpandAllParentNavGrids(CollapseExpandParentEntityOptions option)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CollapseAllParentNavGrids", IfxTraceCategory.Enter);

        //        Raise_ParentNavGridExpansionChanged(option);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CollapseAllParentNavGrids", ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CollapseAllParentNavGrids", IfxTraceCategory.Leave);
        //    }
        //}


        void ucNav_ChangeParentNavGridExpansion(object sender, CollapseExpandAllParentNavGridsArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Raise_ParentNavGridExpansionChanged", IfxTraceCategory.Enter);
                //if (sender != ucNav)
                //{
                CollapseExapndNavGrid(e.Option);
                //}
                //Raise_ParentNavGridExpansionChanged(e.Option);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Raise_ParentNavGridExpansionChanged", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Raise_ParentNavGridExpansionChanged", IfxTraceCategory.Leave);
            }
        }



        #endregion Collapse - Expand Parents



        #endregion Window Methods and Events

        
        #region Data Events


        //*** lots of new code here for SL and for having Entity Controls that are just containers for other entity controls.
        //       for more related code, see the Tab Control _SelectionChanged event for entity state changed code
        void FindActivePropertiesControlFromParent(string prefix, object obj)
        {
            DependencyObject element;
            try
            {
                // Silverlight and the Logical Tree 
                // http://themechanicalbride.blogspot.com/2008/10/silverlight-and-logical-tree.html




                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_as, _cn, "FindActivePropertiesControlFromParent", IfxTraceCategory.Enter);
                if (_activePropertiesControl != null && _activePropertiesControl.HasBusinessObject() == true && (_activePropertiesControl.GetEntityState().IsDirty() == true || _activePropertiesControl.GetEntityState().IsNew()))
                {
                    //  There is an active props control and it's dirty which means we cant navigate away from it.
                    //  or it's new which means we need to click UnDo to get rid of it or Save to make it not dirty
                    //  everything should already be locked down so all we need to do is get out of here.
                    return;
                }

                if (obj.GetType().ToString() == "System.Windows.Controls.Image")
                {
                    if (((Image)obj).Name == "mnuNewImage")
                    {
                        // This should be the only case where we need to do this as the line above did not catch it.
                        // When the user clicks on the 'New' menu button, the entity state can not be dirty and can not also be new which is one of the conditions to return.
                        // I we are clicking on the 'New' menu button, return as we dont want to run any of this code.
                        return;
                    }
                }

                element = (DependencyObject)obj;
                //Loop through all the parent elements looking for the first properties control we find.
                //  If we find one, set it as the active props control, otherwise, set the window's state to 'no props control is active'.
                while (element != null)
                {
                    if (element is IEntitiyPropertiesControl)
                    {
                        //  We found it so now set  as the active props control.
                        SetActivePropertiesControl((IEntitiyPropertiesControl)element);
                        ConfigureToCurrentEntityState(_activePropertiesControl.GetEntityStateSwitch());
                        //_activePropertiesControl.RaiseCurrentEntityStateChanged();
                        return;
                    }
                    else if (element is IEntityControl)
                    {

                        SetActiveEntityControl((IEntityControl)element);

                        if (!(null == _activeEntityControl.GetPropsControl()))
                        {
                            // See if splitScreen is true and props control is available
                            if (_activeEntityControl.GetIsInSplitScreenMode() && _activeEntityControl.GetIsPropsTabSelected())
                            {
                                // We found the properties control.  Now check to see if it has a business object.
                                if (true == _activeEntityControl.GetPropsControl().HasBusinessObject())
                                {
                                    ConfigureToCurrentEntityState(_activeEntityControl.GetPropsControl().GetEntityStateSwitch());
                                    return;
                                }
                                else
                                {
                                    // ucProps had no business object of some reason, but we still need to call the next method.
                                    ConfigureToCurrentEntityState(EntityStateSwitch.None);
                                    return;
                                }
                            }
                            else
                            {
                                // Props control is not available to the user at this time.
                                ConfigureToCurrentEntityState(EntityStateSwitch.None);
                                return;
                            }
                        }
                        //_activeEntityControl.RaiseCurrentEntityStateChanged();
                        // Props control is not available to the user at this time.
                        //ConfigureToCurrentEntityState(EntityStateSwitch.None);
                        SetActivePropertiesControl(null);
                        return;
                    }

                    else
                    {
                        // use this line instead of the 4 lines below and see how it goes.  Becuase we dont have LogicalTreeHelper, Visual, Visual3D in SL
                        element = VisualTreeHelper.GetParent(element);
                    }


                    //else if (element is Visual || element is Visual3D)
                    //{
                    //    element = VisualTreeHelper.GetParent(element);
                    //}
                    //else
                    //{
                    //    element = LogicalTreeHelper.GetParent(element);
                    //}
                    // for testing only, print the name of this elements.
                    if (element != null)
                    {
                        System.Diagnostics.Debug.WriteLine(prefix + ";  " + element.ToString());
                        //Console.WriteLine(prefix + ";  " + element.ToString());
                        if (element.ToString() == "UIControls.ucAddress_LU")
                        {
                            string str = "xx";
                        }
                    }
                }
                // No props control was found so set the window state accordingly.
                SetActivePropertiesControl(null);
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_as, _cn, "FindActivePropertiesControlFromParent", IfxTraceCategory.Catch);
                //throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_as, _cn, "FindActivePropertiesControlFromParent", IfxTraceCategory.Leave);
            }
        }

        void OnCurrentEntityStateChanged(object sender, CurrentEntityStateArgs e)
        {
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Enter);
                _activeEntityControl = e.ActiveEntityControl;
                SetActivePropertiesControl(e.ActivePropertiesControl);
                //SetActiveContextEntityControl(e.ActiveEntityControl);
                //lblTestInfo.Content = _activePropertiesControl.ToString();

                ConfigureToCurrentEntityState(e.State);
                //ConfigureWindowForActiveControl();
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_ns, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Catch);
                //throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_ns, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Leave);
            }
        }

        void OnBrokenRuleChanged(object sender, BrokenRuleArgs e)
        {
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Enter);

                //SetBrokenRuleText(e.Rule);
                //BrokenRuleEventHandler handler = BrokenRuleChanged;
                //if (handler != null)
                //{
                //    handler(sender, e);
                //}
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_ns, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Catch);
                //throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_ns, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Leave);
            }
        }

        void OnCrudFailed(object sender, CrudFailedArgs e)
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "OnCrudFailed", IfxTraceCategory.Enter);
            MessageBox.Show(e.FailedReasonText, "Data Save Error", MessageBoxButton.OK);

        }


        #endregion Data Events


        #region Menues
        

        private void XamMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "XamMenuItem_Click", IfxTraceCategory.Enter);
                XamMenuItem mnu = (XamMenuItem)sender;
                switch (mnu.Name)
                {
                    case "mnuClose":

                        break;
                    case "mnuNew":
                        //NewEntityRow();
                        break;
                    case "mnuSave":
                        //Save();
                        break;
                    case "mnuUnDo":
                        //UnDo();
                        break;
                    //case "mnuLoadRichGrid":
                    //    LoadTestRichGrid();
                    //    break;
                    case "mnuManageLogs":
                        Load_ucIfxLog();
                        break;
                    case "mnuServerTests":
                        Load_ucSvrT();
                        break;
                    case "mnuStartStopTrace":
                        StartStopTraceSession();
                        break;
                    case "mnuHelp":
                        HelpProvider.ShowHelpFile();
                        break;

                }
            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_ns, _cn, "XamMenuItem_Click", IfxTraceCategory.Catch);
                //throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                //if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_ns, _cn, "XamMenuItem_Click", IfxTraceCategory.Leave);
            }
        }
        
        private vDialog _serverTestDialog = null;
        void Load_ucSvrT()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_serverTestDialog", IfxTraceCategory.Enter);
                ucServerTests st = new ucServerTests();
                _serverTestDialog = new vDialog
                {
                Content = st
                };
                _serverTestDialog.WindowStateChanged += _serverTestDialog_WindowStateChanged;
                _serverTestDialog.Show();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_serverTestDialog", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_serverTestDialog", IfxTraceCategory.Leave);
            }
        }
        
        void _serverTestDialog_WindowStateChanged(object sender, WindowStateChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_serverTestDialog_WindowStateChanged", IfxTraceCategory.Enter);

                if (e.NewWindowState == Infragistics.Controls.Interactions.WindowState.Hidden)
                {
                    sender = null;
                    _serverTestDialog = null;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_serverTestDialog_WindowStateChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_serverTestDialog_WindowStateChanged", IfxTraceCategory.Leave);
            }
        }
        
        #endregion Menues



        #region obMain Button Commands

        private void BtnTable_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnTable_OnClick", IfxTraceCategory.Enter);

                if (_ucTb == null)
                {
                    ucTb_Load();
                }
                DisplayActiveUserControl(_ucTb);
                //0b5fca02-514c-4c1e-acd3-2be5d069a294
                //_ucTb.SetStateFromParent(null, null, null, null, null, null, "");
                _ucTb.SetStateFromParent(null, "MainPage", null, new Guid("0b5fca02-514c-4c1e-acd3-2be5d069a294"), null, null, null, null, "");
                //WcTable_Bll_staticLists.Refresh_WcTable_ComboItemList_BindingList(new Guid("0b5fca02-514c-4c1e-acd3-2be5d069a294"));

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnTable_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnTable_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void BtnPmDashboard_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnMyTasks_OnClick", IfxTraceCategory.Enter);

                ucDB_Load();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnMyTasks_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnMyTasks_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void BtnDevAppListing_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnMyTasks_OnClick", IfxTraceCategory.Enter);

                //if (ContextValues.CurrentProjectId == null)
                //{
                //    Task_Bll_staticLists.LoadStaticLists();
                //}

                if (_ucApp == null)
                {
                    ucApp_Load();
                }
                //_ucApp.UseForEntityColumns = true;
                DisplayActiveUserControl(_ucApp);
                _ucApp.SetStateFromParent(null, null, null, null, null, null, "");



            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnMyTasks_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnMyTasks_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void BtnMyTasks_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnMyTasks_OnClick", IfxTraceCategory.Enter);

                if (ContextValues.CurrentProjectId == null)
                {
                    Task_Bll_staticLists.LoadStaticLists();
                }

                if (ucTk == null)
                {
                    ucTk_Load();
                }
                ucTk.UseForEntityColumns = true;
                DisplayActiveUserControl(ucTk);
                ucTk.SetStateFromParent(null, "MyTasks", null, (Guid)Credentials.UserId, null, null, null, null, "");


            
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnMyTasks_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnMyTasks_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void BtnProjectTasks_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnProjectTasks_OnClick", IfxTraceCategory.Enter);
                if (ContextValues.CurrentProjectId == null)
                {
                    MessageBox.Show("Select a project first.", "Select Project", MessageBoxButton.OK);
                    return;
                }


                if (ucTk == null)
                {
                    ucTk_Load();
                }
                ucTk.UseForEntityColumns = false;
                DisplayActiveUserControl(ucTk);
                ucTk.SetStateFromParent(null, "ProjectOnly", null, (Guid)ContextValues.CurrentProjectId, null, null, null, null, "");

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnProjectTasks_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnProjectTasks_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void BtnAllTasks_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnAllTasks_OnClick", IfxTraceCategory.Enter);
                if (ContextValues.CurrentProjectId == null)
                {
                    MessageBox.Show("Select a project first.", "Select Project", MessageBoxButton.OK);
                    return;
                }

                if (ucTk == null)
                {
                    ucTk_Load();
                }
                ucTk.UseForEntityColumns = true;
                DisplayActiveUserControl(ucTk);
                ucTk.SetStateFromParent(null, "All", null, (Guid)ContextValues.CurrentProjectId, null, null, null, null, "");

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnAllTasks_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnAllTasks_OnClick", IfxTraceCategory.Leave);
            }
        }


        private void BtnMyDiscussions_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnMyDiscussions_OnClick", IfxTraceCategory.Enter);

                if (ucDisc == null)
                {
                    ucDisc_Load();
                }
                ucDisc.ViewContext = vDiscussionDialog.DiscussionViewModel.DiscussionViewContext.Owner;
                ucDisc.LoadGrid_MyDiscusssions();
                DisplayActiveUserControl(ucDisc);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnMyDiscussions_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnMyDiscussions_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void BtnDiscussionsImIn_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnDiscussionsImIn_OnClick", IfxTraceCategory.Enter);


                if (ucDisc == null)
                {
                    ucDisc_Load();
                }
                ucDisc.ViewContext = vDiscussionDialog.DiscussionViewModel.DiscussionViewContext.Member;
                ucDisc.LoadGrid_DiscusssionsImIn();
                DisplayActiveUserControl(ucDisc);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnDiscussionsImIn_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnDiscussionsImIn_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void BtnProjectDiscussions_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnProjectDiscussions_OnClick", IfxTraceCategory.Enter);

                if (ContextValues.CurrentProjectId == null)
                {
                    MessageBox.Show("Select a project first.", "Select Project", MessageBoxButton.OK);
                    return;
                }
                //_discussionProxy.Begin_GetDiscussion_lstBy_Project((Guid)ContextValues.CurrentProjectId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnProjectDiscussions_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnProjectDiscussions_OnClick", IfxTraceCategory.Leave);
            }
        }


        //void _discussionProxy_GetDiscussion_lstBy_ProjectCompleted(object sender, GetDiscussion_lstBy_ProjectCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_discussionProxy_GetDiscussion_lstBy_ProjectCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;
        //        var array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

        //        //var discussions = new Discussion_List();
        //        //if (array != null)
        //        //{
        //        //    discussions.ReplaceList(array);
        //        //}

        //        var discussionDialog = vDialogHelper.InitializeDiscussionDialog(array, vDialogMode.Unrestricted, (Guid)ContextValues.CurrentProjectId,
        //            ApplicationTypeServices.ApplicationLevelVariables.DiscussionParentTypes.Project, ContextValues.CurrentProjectName);

        //        if (discussionDialog.ShowDialog() == true)
        //        {
        //            discussionDialog.Result.RequestClose += (_, __) =>
        //            {
        //                discussionDialog = null;
        //            };
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_discussionProxy_GetDiscussion_lstBy_ProjectCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_discussionProxy_GetDiscussion_lstBy_ProjectCompleted", IfxTraceCategory.Leave);
        //    }
        //}

    

        #endregion obMain Button Commands



        #region Filter Admin Temp Code


        private void btnFilterAdmin_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFAM_Load", IfxTraceCategory.Enter);

                ucFAM_Load();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFAM_Load", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFAM_Load", IfxTraceCategory.Leave);
            }
        }


        void ucFAM_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFAM_Load", IfxTraceCategory.Enter);
                if (ucFA == null)
                {
                    ucFA = new ucv_FilterSessionMD();
                    ucFA.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    ucFA.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                    ucFA.CrudFailed += new CrudFailedEventHandler(OnCrudFailed);
                    ucFA.SetStateFromParent(null, null, null, null, "");
                    LayoutRoot.Children.Add(ucFA);
                    Grid.SetRow(ucFA, 1);
                    Grid.SetColumn(ucFA, 2);
                }

                DisplayActiveUserControl(ucFA);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFAM_Load", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFAM_Load", IfxTraceCategory.Leave);
            }
        }


        private void btnReportAdmin_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFAM_Load", IfxTraceCategory.Enter);

                ucRptAdm_Load();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFAM_Load", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFAM_Load", IfxTraceCategory.Leave);
            }
        }


        #endregion Filter Admin Temp Code


        private vDialog _scheduleDialog = null;
        private void btnTest_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnTest_Click", IfxTraceCategory.Enter);
                if (_scheduleDialog != null)
                {
                    _scheduleDialog.WindowState = WindowState.Normal;
                    return;
                }
                ucv_Schedule ucSch = new ucv_Schedule();
                 ucSch.SetStateFromParent(null, null, null, null, "");
                _scheduleDialog = new vDialog
                {
                    Content = ucSch,
                    Header = "Schedule",
                    HeaderIconVisibility = Visibility.Collapsed
                };
                _scheduleDialog.WindowStateChanged += _scheduleDialog_WindowStateChanged;
                _scheduleDialog.Show();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnTest_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnTest_Click", IfxTraceCategory.Leave);
            }
        }
        
        void _scheduleDialog_WindowStateChanged(object sender, WindowStateChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_scheduleDialog_WindowStateChanged", IfxTraceCategory.Enter);

                if (e.NewWindowState == Infragistics.Controls.Interactions.WindowState.Hidden)
                {
                    sender = null;
                    _scheduleDialog = null;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_scheduleDialog_WindowStateChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_scheduleDialog_WindowStateChanged", IfxTraceCategory.Leave);
            }
        }



        /// <summary>
        /// This was code from the Production app, but it might be good to add to our prototype
        /// </summary>
        /// <param name="entType"></param>
        /// <param name="parentType"></param>
        /// <param name="id"></param>
        /// <param name="caption"></param>
        public void OpenEntityInPopup(string entType, string parentType, Guid id, string caption)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OpenEntityInPopup", IfxTraceCategory.Enter);
                UserControl uc = null;
                int width = 100;
                int height = 100;
                string headerText = "";
                //switch (entType)
                //{
                  
                //    //case "ucContractDt":
                //    //    ucContractDt ucCtD = new ucContractDt();
                //    //    width = 945;
                //    //    height = 680;
                //    //    ucCtD.ConfigureForSingleRowOnly(id);
                //    //    uc = ucCtD;
                //    //    headerText = caption;
                //    //    break;
                 
                //    //case "ucLeaseTract":
                //    //    ucLeaseTract ucLsTr = new ucLeaseTract();
                //    //    width = 979;
                //    //    height = 643;
                //    //    ucLsTr.ConfigureForSingleRowOnly(id);
                //    //    uc = ucLsTr;
                //    //    headerText = caption;
                //    //    break;
                //    //case "ucObligation":
                //    //    ucObligation ucOb = new ucObligation();
                //    //    width = 827;
                //    //    height = 484;
                //    //    ucOb.ConfigureForSingleRowOnly(id);
                //    //    uc = ucOb;
                //    //    headerText = caption;
                //    //    break;
                  
                //}
                var dialog = vDialogHelper.InitializeEntityDialog(uc, width, height, headerText, null);
                dialog.Show();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OpenEntityInPopup", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OpenEntityInPopup", IfxTraceCategory.Leave);
            }
        }




        #region Audit



        #endregion Audit




        #region Bubble Exception Test

        private void BtnBubbleException_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnBubbleException_OnClick", IfxTraceCategory.Enter);

                BubbleTest1();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnBubbleException_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnBubbleException_OnClick", IfxTraceCategory.Leave);
            }
        }

        void BubbleTest1()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTest1", IfxTraceCategory.Enter);

                BubbleTest1A();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTest1", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTest1", IfxTraceCategory.Leave);
            }
        }

        void BubbleTest1A()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTest1A", IfxTraceCategory.Enter);

                BubbleTest1B();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTest1A", new ValuePair[] { new ValuePair("this", this.ToString()), new ValuePair("data", "More data2"), new ValuePair("sqlScript", "The script2") }, null, ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTest1A", IfxTraceCategory.Leave);
            }
        }

        void BubbleTest1B()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTest1B", IfxTraceCategory.Enter);

                BubbleTestC();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTest1B", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTest1B", IfxTraceCategory.Leave);
            }
        }

        void BubbleTestC()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTestC", IfxTraceCategory.Enter);

                throw new Exception("This is a test exception thrown to start a bubble up trace.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTestC", new ValuePair[] { new ValuePair("check.ToString()", "Some data"), new ValuePair("data", "More data"), new ValuePair("sqlScript", "The script") }, null, ex);
                //return;
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTestC", IfxTraceCategory.Leave);
            }
        }


        //new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, 



        //void BubbleTest2()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTest2", IfxTraceCategory.Enter);


        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTest2", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BubbleTest2", IfxTraceCategory.Leave);
        //    }
        //}



        #endregion Bubble Exception Test


    }
}
