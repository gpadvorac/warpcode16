﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Velocity.SL;
using vDP;
using Ifx.SL;
using vUICommon;
using ApplicationTypeServices;
using vFilterDataManager;
using EntityBll.SL;
using UIControls;


namespace WarpCode
{
    public partial class MainPage
    {





        private Guid _ancestorSecurityId = new Guid("59b35b3f-5d3c-4116-bd4a-6cc76bca4cea");
        ControlCache cCache;
        EntityCache eCache = null;
        bool defaultEntityOperationPermission = true;

        int idx_obgProjExp = 0;
        //int idx_obgMaps = 1;
        int idx_obgPM = 1;
        int idx_obgReports = 2;
        int idx_obgSupportData = 4;
        int idx_obgSchedule = 4;
        int idx_obgAdmin = 5;
        int idx_obgAudit = 6;


        private void SetSecurityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Enter);
                _userContext.LoadArtifactPermissions(_ancestorSecurityId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Leave);
            }
        }
        
        void UserSecurityContext_SecurityArtifactsRetrieved(object sender, SecurityArtifactsRetrievedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Enter);

                SecurityCache.AddControlCacheForAncestor(e.ArtifactAncestorData);
                cCache = SecurityCache.GetControlGroupById(e.ArtifactAncestorData.Id);

                ////  Leave this one disabled and use the one in the outlook bar.  We can use this button in emergancy by setting its xaml to visible.
                //// Application Admin button
                //DP.SetControlSecurityId(btnAppAdmin, new Guid("5DF7BD0F-F8A6-43C8-BB2A-968AC098D1A5"));
                //SecurityCache.SetWPFActionControlState(btnAppAdmin, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //// Project Members button
                //DP.SetControlSecurityId(btnProjectMembers, new Guid("82b8a961-c939-422b-b53e-ff0a4f6aa824"));
                //SecurityCache.SetWPFActionControlState(btnProjectMembers, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //// Report Admin button
                //DP.SetControlSecurityId(btnReportAdmin, new Guid("93e256f9-6509-4fc6-8b88-18a6fada8336"));
                //SecurityCache.SetWPFActionControlState(btnReportAdmin, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //// Filter Admin button
                //DP.SetControlSecurityId(btnFilterAdmin, new Guid("1fe2640c-7ada-4630-858d-0db0e0dcc2dd"));
                //SecurityCache.SetWPFActionControlState(btnFilterAdmin, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Admin Tools
                DP.SetControlSecurityId(mnuAdminTools, new Guid("614b2a38-30cf-4ec3-b3ef-f2ac6c270fd4"));
                SecurityCache.SetWPFActionControlState(mnuAdminTools, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //  NEED TO FIGURE OUT HOW TO DO THIS.

                //// New Buton
                //DP.SetControlSecurityId(mnuNew, new Guid("4a983f76-a2ed-4968-9457-f73482d5dae2"));
                //SecurityCache.SetWPFActionControlState(mnuNew, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //// Save Button
                //DP.SetControlSecurityId(mnuSave, new Guid("075c7bfe-fce3-4293-bb94-b1a880d9de1c"));
                //SecurityCache.SetWPFActionControlState(mnuSave, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //// UnDo Button
                //DP.SetControlSecurityId(mnuUnDo, new Guid("e6fa5f6e-1cac-4734-a20e-c4b3337af0c2"));
                //SecurityCache.SetWPFActionControlState(mnuUnDo, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Outlook Bar
                DP.SetControlSecurityId(obMain, new Guid("b58b19b0-c710-462e-9079-157558d690f7"));
                SecurityCache.SetWPFActionControlState(obMain, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);


                //// Flag to turn off Obligation Type Updates
                //DP.SetControlSecurityId(btnDisableObligationTypeAutoUpdates, new Guid("4564b67d-c9af-4933-9ca9-85b6f20f2bd6"));
                //SecurityCache.SetWPFActionControlState(btnDisableObligationTypeAutoUpdates, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //if (btnDisableObligationTypeAutoUpdates.Visibility == System.Windows.Visibility.Visible)
                //{
                //    ApplicationLevelVariables.Flag_StopObligatinTypeDataSourceUpdateEvents = true;
                //}

                //// Flag to allowing users to delete data
                //DP.SetControlSecurityId(btnDelete, new Guid("b0bafc53-9f06-413b-926a-2584d954da49"));
                //SecurityCache.SetWPFActionControlState(btnDelete, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);
                ApplicationLevelVariables.IsDeleteDataAllowed = SecurityCache.IsEnabled(ApplicationLevelVariables.IsDeleteDataAllowed_Id);
                //if (btnDelete.Visibility == System.Windows.Visibility.Visible)
                //{
                //    ApplicationLevelVariables.IsDeleteDataAllowed = true;
                //}
                //else
                //{
                //    ApplicationLevelVariables.IsDeleteDataAllowed = false;
                //}


                //// SET APPLICATION LEVEL VARIABLES
                //ApplicationLevelVariables.IsApproved_WF1 = SecurityCache.IsEnabled(ApplicationLevelVariables.IsApproved_WF1_Id);
                //ApplicationLevelVariables.IsApproved_WF2 = SecurityCache.IsEnabled(ApplicationLevelVariables.IsApproved_WF2_Id);
                //ApplicationLevelVariables.IsApproved_WF3 = SecurityCache.IsEnabled(ApplicationLevelVariables.IsApproved_WF3_Id);
                //ApplicationLevelVariables.IsApproved_WF4 = SecurityCache.IsEnabled(ApplicationLevelVariables.IsApproved_WF4_Id);
                ApplicationLevelVariables.IsAllowEditSystemColumnGroups = SecurityCache.IsEnabled(ApplicationLevelVariables.IsAllowEditSystemColumnGroups_Id);
                //ApplicationLevelVariables.IsAndysRoom = SecurityCache.IsEnabled(ApplicationLevelVariables.IsAndysRoom_Id);
                //ApplicationLevelVariables.IsAndysRoomLink = SecurityCache.IsEnabled(ApplicationLevelVariables.IsAndysRoomLink_Id);


                if (ucAdBtn != null)
                {
                    ucAdBtn.SetSecurityState();
                }

                if (obMain.Visibility == System.Windows.Visibility.Visible)
                {
                    gdSplitterMain.Visibility = System.Windows.Visibility.Visible;
                    LayoutRoot.ColumnDefinitions[0].Width = GridLength.Auto;
                    LayoutRoot.ColumnDefinitions[1].Width = new GridLength(8);
                }
                else
                {
                    gdSplitterMain.Visibility = System.Windows.Visibility.Collapsed;
                    LayoutRoot.ColumnDefinitions[0].Width = new GridLength(0);
                    LayoutRoot.ColumnDefinitions[1].Width = new GridLength(0);
                }


                // Project Explorer
                DP.SetControlSecurityId(obMain.Groups[idx_obgProjExp], new Guid("59662d6e-0fe8-457c-b990-89b44a861de5"));
                SecurityCache.SetWPFActionControlState(obMain.Groups[idx_obgProjExp], EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Reports
                DP.SetControlSecurityId(obMain.Groups[idx_obgReports], new Guid("4c7f4862-4949-4312-beac-d2f289e82c33"));
                SecurityCache.SetWPFActionControlState(obMain.Groups[idx_obgReports], EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Support Data
                DP.SetControlSecurityId(obMain.Groups[idx_obgSupportData], new Guid("c86c8fba-cf44-47d1-a2ae-b27c5cc5d59f"));
                SecurityCache.SetWPFActionControlState(obMain.Groups[idx_obgSupportData], EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Project Management
                DP.SetControlSecurityId(obMain.Groups[idx_obgPM], new Guid("626af683-e80d-42d4-9735-57f60e7e88c2"));
                SecurityCache.SetWPFActionControlState(obMain.Groups[idx_obgPM], EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Jobs and Shcedules
                DP.SetControlSecurityId(obMain.Groups[idx_obgSchedule], new Guid("626af683-e80d-42d4-9735-57f60e7e88c2"));
                SecurityCache.SetWPFActionControlState(obMain.Groups[idx_obgSchedule], EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Project Admin
                DP.SetControlSecurityId(obMain.Groups[idx_obgAdmin], new Guid("626af683-e80d-42d4-9735-57f60e7e88c2"));
                SecurityCache.SetWPFActionControlState(obMain.Groups[idx_obgAdmin], EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Logs button
                DP.SetControlSecurityId(btnIfxConfig, new Guid("c6e79c37-c2cd-4c62-b080-a18b093e7132"));
                SecurityCache.SetWPFActionControlState(btnIfxConfig, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                // Clear controls
                if (Credentials.IsAuthenticated)
                {
                    ucPrjLst.LoadProjects();
                    obMain.Groups[idx_obgProjExp].IsSelected = true;

                    FilterDataProvider obj = new FilterDataProvider();
                    obj.RefreshFilterCacheFromServer();

                    _gridColumnGroupProxy.Begin_Getv_GridColumnGroupTypeUserPreference_lstByUserId((Guid)Credentials.UserId);
                    v_ReportGroup_Bll_staticLists.LoadStaticLists((Guid)Credentials.UserId);
                    v_Report_Bll_staticLists.LoadStaticLists((Guid)Credentials.UserId);

                    //// Load dashboard
                    //ucDB_Load();

                }
                else
                {

                    // Clear out global context values:
                    ContextValues.CurrentProjectId = null;
                    ContextValues.CurrentProjectName = null;
                    
                    if (ucAd != null)
                    {
                        LayoutRoot.Children.Remove(ucAd);
                        ucAd = null;
                    }
                    if (ucPM != null)
                    {
                        LayoutRoot.Children.Remove(ucPM);
                        ucPM = null;
                    }
                    if (ucMembers != null)
                    {
                        LayoutRoot.Children.Remove(ucMembers);
                        ucMembers = null;
                    }

                    if (ucRM != null)
                    {
                        LayoutRoot.Children.Remove(ucRM);
                        ucRM = null;
                    }

                    if (ucRX != null)
                    {
                        LayoutRoot.Children.Remove(ucRX);
                        ucRX = null;
                    }

                    if (ucFM != null)
                    {
                        LayoutRoot.Children.Remove(ucFM);
                        ucFM = null;
                    }

                    if (ucFA != null)
                    {
                        LayoutRoot.Children.Remove(ucFA);
                        ucFA = null;
                    }

                    if (ucRptAdm != null)
                    {
                        LayoutRoot.Children.Remove(ucRptAdm);
                        ucRptAdm = null;
                    }

                    if (ucFl != null)
                    {
                        LayoutRoot.Children.Remove(ucFl);
                        ucFl = null;
                    }

                    if (_supportDataDialog != null)
                    {
                        LayoutRoot.Children.Remove(_supportDataDialog);
                        _supportDataDialog = null;
                    }

                 

                    if (_ucDashB != null)
                    {
                        LayoutRoot.Children.Remove(_ucDashB);
                        _ucDashB = null;
                    }


                    if (ucTk != null)
                    {
                        LayoutRoot.Children.Remove(ucTk);
                        ucTk = null;
                    }

                    if (ucDisc != null)
                    {
                        LayoutRoot.Children.Remove(ucDisc);
                        ucDisc = null;
                    }

                    if (ucNtfcEvent != null)
                    {
                        LayoutRoot.Children.Remove(ucNtfcEvent);
                        ucNtfcEvent = null;
                    }

                    if (ucJobs != null)
                    {
                        LayoutRoot.Children.Remove(ucJobs);
                        ucJobs = null;
                    }

                    if (ucJob != null)
                    {
                        LayoutRoot.Children.Remove(ucJob);
                        ucJob = null;
                    }

                    //if (ucPrjLst != null)
                    //{
                    //    ucPrjExp.ClearProjects();
                    //}

                    if (ucDB != null)
                    {
                        LayoutRoot.Children.Remove(ucDB);
                        ucDB = null;
                    }

                    if (_ucApp != null)
                    {
                        LayoutRoot.Children.Remove(_ucApp);
                        _ucApp = null;
                    }

                    if (_ucTb != null)
                    {
                        LayoutRoot.Children.Remove(_ucTb);
                        _ucTb = null;
                    }
                    
                }







                //if (Credentials.UserName == "georgep@nwis.net" || Credentials.UserName == "cp@tmf.com")
                //{
                //    ckLeaseCompression.Visibility = Visibility.Visible;
                //}
                //else
                //{
                //    ckLeaseCompression.Visibility = Visibility.Collapsed;
                //}



            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Leave);
            }
        }



    }
}
