using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;
using vDataServices;

// Gen Timestamp:  3/22/2017 1:00:49 AM

namespace DataServices
{
    public partial class vLicenseSupport_DataServices
    {

        #region Initialize Variables

        private static string _as = "vDataServicesSysAp";
        private static string _cn = "vLicenseSupport_DataServices";

        #endregion Initialize Variables


		#region Other Data Access Methods

        public static object[]  GetPerson_ForLicenseAssignment_ComboItemList(Guid LicClSet_Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "vSysApGen.spPerson_ForLicenseAssignment_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@LicClSet_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@LicClSet_Id"].Value = LicClSet_Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  Executev_LicenseClient_AssignUser(Guid v_LicCl_LicClSet_Id, Guid v_LicCl_AssignedUserId, Guid v_LicCl_UserId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_LicenseClient_AssignUser", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "vSysApGen.spv_LicenseClient_AssignUser";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@v_LicCl_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@v_LicCl_LicClSet_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_LicCl_AssignedUserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@v_LicCl_UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
				cmd.Parameters["@v_LicCl_LicClSet_Id"].Value = v_LicCl_LicClSet_Id;
				cmd.Parameters["@v_LicCl_AssignedUserId"].Value = v_LicCl_AssignedUserId;
				cmd.Parameters["@v_LicCl_UserId"].Value = v_LicCl_UserId;
				cmd.ExecuteNonQuery();
				Guid v_LicCl_Id = ((Guid)(cmd.Parameters["@v_LicCl_Id"].Value));
				Int32 Success = ((Int32)(cmd.Parameters["@Success"].Value));
				//Guid ErrorLogId = ((Guid)(cmd.Parameters["@ErrorLogId"].Value));
				object[] ret = new object[2];
				ret[0] = v_LicCl_Id;
				ret[1] = Success;
				return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_LicenseClient_AssignUser", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_LicenseClient_AssignUser", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetPerson_ForLicenseAssignment(Guid LicClSet_Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "vSysApGen.spPerson_ForLicenseAssignment";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@LicClSet_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@LicClSet_Id"].Value = LicClSet_Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment", IfxTraceCategory.Leave);
            }
        }

		#endregion Other Data Access Methods

    }
}


