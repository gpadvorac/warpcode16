﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using Infragistics.Controls.Grids;
using Infragistics.Documents.Excel;
//using Infragistics.Documents.Reports.Excel;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using Ifx.SL;
using vUICommon;
using TypeServices;
using System.Windows.Media;

namespace vXamGridExcel
{
    public class ExcelExportHelper
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "vXamGridExcel";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "ExcelExportHelper";


        #endregion Initialize Variables


        static public void ExportGridToExcel(XamGrid grid)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExportGridToExcel", IfxTraceCategory.Enter);
                SaveFileDialog dialog = new SaveFileDialog();
                OpenFileDialog openfileDialog = new OpenFileDialog();
                dialog = new SaveFileDialog { Filter = "Excel files|*.xls", DefaultExt = "xls" };


                bool? showDialog = dialog.ShowDialog();
                if (showDialog == true)
                {




                    Workbook dataWorkbook = CreateExcelWorkbook(grid);
                    if (dataWorkbook == null)
                    {
                        MessageBox.Show("There was a problem in creating the Workbook.  If this problem continies, please contact support.", "Error", MessageBoxButton.OK);
                        return;
                    }

                    SaveExcelToFile(dialog, openfileDialog, dataWorkbook);


                    //Workbook workbook1;
                    //if (openfileDialog.ShowDialog() == true)
                    //{
                    //    // If user selected a file get the stream of that file
                    //    FileStream stream = openfileDialog.File.OpenRead();

                    //    // Load workbook with data
                    //    workbook1 = Workbook.Load(stream);

                    //    stream.Close();
                    //}


                }


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExportGridToExcel", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
                Debugger.Break();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExportGridToExcel", IfxTraceCategory.Leave);
            }
        }

        static Workbook CreateExcelWorkbook(XamGrid grid)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateExcelWorkbook", IfxTraceCategory.Enter);
                //dialog = new SaveFileDialog();
                if (grid.Rows.Count == 0)
                {
                    MessageBox.Show("There are no rows to export.", "No Data", MessageBoxButton.OK);
                    return null;
                }

                Workbook dataWorkbook = new Workbook();
                Worksheet sheetOne = dataWorkbook.Worksheets.Add("Data Sheet");

                //// Export to Excel2007 format 
                //if (ComboBox_ExcelFormat.SelectedIndex == 0)
                //{
                //dataWorkbook.SetCurrentFormat(WorkbookFormat.Excel2007);
                //}

                //Freeze header row
                sheetOne.DisplayOptions.PanesAreFrozen = true;
                sheetOne.DisplayOptions.FrozenPaneSettings.FrozenRows = 1;

                // Build Column List
                sheetOne.DefaultColumnWidth = 5000;
                sheetOne.Columns[1].Width = 8500;
                sheetOne.Columns[3].Width = 10000;
                int currentColumn = 1;
                List<int> columnIndexes = new List<int>();
                int cnt = 0;
                // We are going to add the Entity ID column before we add the grid columns
                //string idHeader = ((IBusinessObject)grid.Rows[0].Data).item
                SetCellValue(sheetOne.Rows[0].Cells[0], "ID");
                // Add the column headers
                foreach (Column column in grid.Columns)
                {
                    if (column.Visibility == Visibility.Visible && column.Key != "RichGrid")
                    {
                        SetCellValue(sheetOne.Rows[0].Cells[currentColumn], column.HeaderText);
                        currentColumn++;
                        columnIndexes.Add(cnt);
                    }
                    cnt++;
                }

                // Export Data From Grid
                int currentRow = 1;
                foreach (Row row in grid.Rows)
                {
                    int currentCell = 1;
                    IBusinessObject obj = row.Data as IBusinessObject;
                    // if this isnt a grid data row, then dont run our logic
                    if (obj != null)
                    {
                        SetCellValue(sheetOne.Rows[currentRow].Cells[0], ((IBusinessObject)row.Data).GetPropertyFormattedStringValueByKey("EntityId"));
                        foreach (int colIdx in columnIndexes)
                        {
                            try
                            {
                                string value = null;
                                if (obj.GetPropertyFormattedStringValueByKey(row.Cells[colIdx].Column.Key) != null)
                                {
                                    value = obj.GetPropertyFormattedStringValueByKey(row.Cells[colIdx].Column.Key).ToString();
                                }
                                else
                                {
                                    try
                                    {
                                        if (row.Cells[colIdx].Control != null && row.Cells[colIdx].Control.Content != null)
                                        {
                                            switch (row.Cells[colIdx].Control.Content.GetType().ToString())
                                            {
                                                case "System.Windows.Controls.TextBlock":
                                                    TextBlock txb = ((CellControl)row.Cells[colIdx].Control).Content as TextBlock;
                                                    if (txb != null)
                                                    {
                                                        value = txb.Text;
                                                    }
                                                    break;
                                                case "System.Windows.Controls.TextBox":
                                                    TextBox tbx = ((CellControl)row.Cells[colIdx].Control).Content as TextBox;
                                                    if (tbx != null)
                                                    {
                                                        value = tbx.Text;
                                                    }
                                                    break;

                                                case "System.Windows.Controls.CheckBox":
                                                    CheckBox ck = ((CellControl)row.Cells[colIdx].Control).Content as CheckBox;
                                                    if (ck != null)
                                                    {
                                                        if (ck.IsChecked != null)
                                                        {
                                                            value = ck.IsChecked.ToString();
                                                        }
                                                    }
                                                    break;
                                                case "System.Windows.Controls.DatePicker":
                                                    DatePicker dpk = ((CellControl)row.Cells[colIdx].Control).Content as DatePicker;
                                                    if (dpk != null)
                                                    {
                                                        value = dpk.Text;
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    catch (Exception xx)
                                    {
                                        //string str = "xx";

                                        //System.Diagnostics.Debug.WriteLine(xx.ToString());
                                        //Debugger.Break();
                                    }
                                }
                                if (value != null)
                                {
                                    SetCellValue(sheetOne.Rows[currentRow].Cells[currentCell], value);
                                }
                            }
                            catch (Exception ex)
                            {
                                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExportGridToExcel", ex);
                                ExceptionHelper.NotifyUserAnExceptionOccured();
                                Debugger.Break();
                                break;
                            }
                            currentCell++;
                        }
                        currentRow++;
                    }
                }
                return dataWorkbook;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateExcelWorkbook", ex);
                Debugger.Break();
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateExcelWorkbook", IfxTraceCategory.Leave);
            }
        }

        static public void ExportArrayToExcel(SaveFileDialog dialog, OpenFileDialog openfileDialog, object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExportArrayToExcel", IfxTraceCategory.Enter);
                //  element 0 = row count
                //  element 1 = column headers
                //  element 2 = column data types
                //  element 3 = rows of data

                if (data == null || (int)data[0] == 0)
                {
                    MessageBox.Show("No rows of data was returned.", "No Data", MessageBoxButton.OK);
                    return;
                }

                //if (((object[])data[2]) == null || ((object[])data[2]).Length == 0)
                //{
                //    MessageBox.Show("No rows of data were returned.", "No Data", MessageBoxButton.OK);
                //    return ;
                //}



                //if (((int)data[0]) > 65530)
                //{
                //    MessageBox.Show("The maximum number of rows in a spreadsheet is 65,530.   This call would have returned " + ((int)data[0]).ToString("#,###") + " rows.  Please add filters to reduce the number of rows returned.", "Too Much Data", MessageBoxButton.OK);
                //    return;
                //}


                Workbook dataWorkbook = CreateExcelWorkbookFromArray((object[])data[1], (object[])data[2], (object[])data[3]);
                if (dataWorkbook == null)
                {
                    MessageBox.Show("There was a problem in creating the Workbook.  If this problem continies, please contact support.", "Error", MessageBoxButton.OK);
                    return;
                }

                SaveExcelToFile(dialog, openfileDialog, dataWorkbook);
      
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExportArrayToExcel", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
                Debugger.Break();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExportArrayToExcel", IfxTraceCategory.Leave);
            }
        }

        static Workbook CreateExcelWorkbookFromArray(object[] colHeaders, object[] colDataTypes, object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateExcelWorkbookFromArray", IfxTraceCategory.Enter);
                if (data == null || data.Length == 0)
                {
                    MessageBox.Show("There are no rows to export.", "No Data", MessageBoxButton.OK);
                    return null;
                }


                Workbook dataWorkbook = new Workbook();
                dataWorkbook.SetCurrentFormat(WorkbookFormat.Excel2007);
                Worksheet sheetOne = dataWorkbook.Worksheets.Add("Data Sheet");


                //Freeze header row
                sheetOne.DisplayOptions.PanesAreFrozen = true;
                sheetOne.DisplayOptions.FrozenPaneSettings.FrozenRows = 1;

                // Build Column List
                sheetOne.DefaultColumnWidth = 5000;
                //sheetOne.Columns[1].Width = 8500;
                //sheetOne.Columns[3].Width = 10000;

                // Note:  we add "1" to the colHeaders Length becuase the 1st  column of data starts in the 2nd column of the spreadsheet.  we start with a blank column.
                for (int i = 0; i < colHeaders.Length + 1; i++)
                {
                    sheetOne.Columns[i].Width = 5000;
                }




                int currentColumn = 1;
                List<int> columnIndexes = new List<int>();




                //XamGridExcelExporter excelExporter = new XamGridExcelExporter();

                //WorksheetCellFormatSettings headerFormat = new WorksheetCellFormatSettings();
                //headerFormat.FillPattern = FillPatternStyle.Solid;
                //headerFormat.FillPatternForegroundColor = Color.FromArgb(255, 58, 181, 233);
                //headerFormat.BottomBorderStyle = CellBorderLineStyle.Thin;
                //headerFormat.BottomBorderColor = Colors.Black;

                //WorkbookFontSettings fontSettings = new WorkbookFontSettings();
                //fontSettings.Bold = ExcelDefaultableBoolean.True;
                //fontSettings.Color = Colors.White;
                //fontSettings.Height = 200;

                //headerFormat.Font = fontSettings;

                //excelExporter.ExportLayoutSettings.HeaderRowCellFormat = headerFormat;


                ////excelExporter.Export(grid, workBook);



                int cnt = 0;
                foreach (object headerText in colHeaders)
                {
                    SetCellValue(sheetOne.Rows[0].Cells[currentColumn], headerText.ToString());
                    //  Format columns  (doesnt seem to work)
                    sheetOne.Columns[currentColumn].CellFormat.ShrinkToFit = ExcelDefaultableBoolean.False;
                    sheetOne.Columns[currentColumn].CellFormat.Font.Name = "Calibri";
                  
                    currentColumn++;
                    columnIndexes.Add(cnt);
                    cnt++;
                }

                // Export Data From Grid
                int currentRow = 1;
                foreach (object[] row in data)
                {
                    int currentCell = 1;
                    foreach (object cel in row)
                    {
                        try
                        {

                            
                            SetCellValue(sheetOne.Rows[currentRow].Cells[currentCell], cel);
                        }
                        catch (Exception ex)
                        {
                            if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExportGridToExcel", ex);
                            ExceptionHelper.NotifyUserAnExceptionOccured();
                            Debugger.Break();
                            break;
                        }
                        currentCell++;
                    }
                    currentRow++;
                }

                //  Try to Format columns again.   (doesnt seem to work)
                foreach (var item in sheetOne.Columns)
                {
                    item.CellFormat.ShrinkToFit = ExcelDefaultableBoolean.False;
                    item.CellFormat.Font.Name = "Calibri";
                }

                return dataWorkbook;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateExcelWorkbookFromArray", ex);
                Debugger.Break();
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateExcelWorkbookFromArray", IfxTraceCategory.Leave);
            }
        }

        
        //static public void ExportGridToExcel(XamGrid grid)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExportGridToExcel", IfxTraceCategory.Enter);
        //        dialog = new SaveFileDialog();
        //        if (grid.Rows.Count == 0)
        //        {
        //            MessageBox.Show("There are no rows to export.", "No Data", MessageBoxButton.OK);
        //            return;
        //        }

        //        Workbook dataWorkbook = new Workbook();
        //        Worksheet sheetOne = dataWorkbook.Worksheets.Add("Data Sheet");

        //        //// Export to Excel2007 format 
        //        //if (ComboBox_ExcelFormat.SelectedIndex == 0)
        //        //{
        //        //dataWorkbook.SetCurrentFormat(WorkbookFormat.Excel2007);
        //        //}

        //        //Freeze header row
        //        sheetOne.DisplayOptions.PanesAreFrozen = true;
        //        sheetOne.DisplayOptions.FrozenPaneSettings.FrozenRows = 1;

        //        // Build Column List
        //        sheetOne.DefaultColumnWidth = 5000;
        //        sheetOne.Columns[1].Width = 8500;
        //        sheetOne.Columns[3].Width = 10000;
        //        int currentColumn = 1;
        //        List<int> columnIndexes = new List<int>();

        //        int cnt = 0;


        //        // We are going to add the Entity ID column before we add the grid columns
        //        //string idHeader = ((IBusinessObject)grid.Rows[0].Data).item
        //        SetCellValue(sheetOne.Rows[0].Cells[0], "ID");
        //        // Add the column headers
        //        foreach (Column column in grid.Columns)
        //        {
        //            if (column.Visibility == Visibility.Visible && column.Key != "RichGrid")
        //            {
        //                SetCellValue(sheetOne.Rows[0].Cells[currentColumn], column.HeaderText);
        //                currentColumn++;
        //                columnIndexes.Add(cnt);
        //            }
        //            cnt++;
        //        }

        //        #region Speed Text

        //        //// Speed Text
        //        //DateTime dt = new DateTime();
        //        //Row rowx = grid.Rows[2];
        //        //object obj = null;
        //        //System.Diagnostics.Debug.WriteLine("Text 1 Start:  Min " + DateTime.Now.Minute.ToString() + ", Sec " + DateTime.Now.Second.ToString() + ", Mil" + DateTime.Now.Minute.ToString());
        //        //try
        //        //{
        //        //    obj = null;
        //        //    for (int i = 0; i < 300000; i++)
        //        //    {
        //        //        obj = rowx.Cells[9].Value;
        //        //    }
        //        //}
        //        //catch (Exception ex)
        //        //{
        //        //    Debugger.Break();
        //        //}
        //        //System.Diagnostics.Debug.WriteLine("Text 1 A End:  Min " + DateTime.Now.Minute.ToString() + ", Sec " + DateTime.Now.Second.ToString() + ", Mil" + DateTime.Now.Minute.ToString());

        //        //try
        //        //{
        //        //    obj = null;
        //        //    for (int i = 0; i < 300000; i++)
        //        //    {
        //        //        obj = rowx.Cells[9].Value;
        //        //    }
        //        //}
        //        //catch (Exception ex)
        //        //{
        //        //    Debugger.Break();
        //        //}
        //        //System.Diagnostics.Debug.WriteLine("Text 1 B End:  Min " + DateTime.Now.Minute.ToString() + ", Sec " + DateTime.Now.Second.ToString() + ", Mil" + DateTime.Now.Minute.ToString());


        //        //try
        //        //{
        //        //    obj = null;
        //        //    for (int i = 0; i < 300000; i++)
        //        //    {
        //        //        obj = rowx.Cells[9].Value;
        //        //    }
        //        //}
        //        //catch (Exception ex)
        //        //{
        //        //    Debugger.Break();
        //        //}
        //        //System.Diagnostics.Debug.WriteLine("Text 1 C End:  Min " + DateTime.Now.Minute.ToString() + ", Sec " + DateTime.Now.Second.ToString() + ", Mil" + DateTime.Now.Minute.ToString());



        //        //try
        //        //{
        //        //    obj = null;
        //        //    for (int i = 0; i < 300000; i++)
        //        //    {
        //        //        obj = ((TextBlock)rowx.Cells[9].Control.Content).Text;
        //        //    }
        //        //}
        //        //catch (Exception ex2)
        //        //{
        //        //    Debugger.Break();
        //        //}
        //        //System.Diagnostics.Debug.WriteLine("Text 2 A End:  Min " + DateTime.Now.Minute.ToString() + ", Sec " + DateTime.Now.Second.ToString() + ", Mil" + DateTime.Now.Minute.ToString());




        //        //try
        //        //{
        //        //    obj = null;
        //        //    for (int i = 0; i < 300000; i++)
        //        //    {
        //        //        obj = ((TextBlock)rowx.Cells[9].Control.Content).Text;

        //        //    }
        //        //}
        //        //catch (Exception ex2)
        //        //{
        //        //    Debugger.Break();
        //        //}
        //        //System.Diagnostics.Debug.WriteLine("Text 2 B End:  Min " + DateTime.Now.Minute.ToString() + ", Sec " + DateTime.Now.Second.ToString() + ", Mil" + DateTime.Now.Minute.ToString());


        //        //try
        //        //{
        //        //    obj = null;
        //        //    for (int i = 0; i < 300000; i++)
        //        //    {
        //        //        if (rowx.Cells[9].Control.Content is CheckBox)
        //        //        {
        //        //            //SetCellValue(sheetOne.Rows[currentRow].Cells[currentCell], ((TextBlock)row.Cells[colIdx].Control.Content).Text);
        //        //            obj = ((CheckBox)rowx.Cells[9].Control.Content).IsChecked;

        //        //        }
        //        //        else if (rowx.Cells[9].Control.Content is TextBlock)
        //        //        {
        //        //            //SetCellValue(sheetOne.Rows[currentRow].Cells[currentCell], ((CheckBox)row.Cells[colIdx].Control.Content).IsChecked);
        //        //            obj = ((TextBlock)rowx.Cells[9].Control.Content).Text;

        //        //        }
        //        //        else
        //        //        {

        //        //        }

        //        //    }
        //        //}
        //        //catch (Exception ex2)
        //        //{
        //        //    Debugger.Break();
        //        //}
        //        //System.Diagnostics.Debug.WriteLine("Text 2 C End:  Min " + DateTime.Now.Minute.ToString() + ", Sec " + DateTime.Now.Second.ToString() + ", Mil" + DateTime.Now.Minute.ToString());



        //        //return;

        //        #endregion Speed Text

        //        // Export Data From Grid
        //        int currentRow = 1;
        //        foreach (Row row in grid.Rows)
        //        {
        //            int currentCell = 1;


        //            SetCellValue(sheetOne.Rows[currentRow].Cells[0], ((IBusinessObject)row.Data).GetPropertyFormattedStringValueByKey("EntityId"));

        //            foreach (int colIdx in columnIndexes)
        //            {
        //                //  ((TextBlock)rowx.Cells[colIdx].Control.Content).Text;


        //                try
        //                {

        //                    string value = null;

        //                    if (((IBusinessObject)row.Data).GetPropertyValueByKey(row.Cells[colIdx].Column.Key) != null)
        //                    {
        //                        value = ((IBusinessObject)row.Data).GetPropertyFormattedStringValueByKey(row.Cells[colIdx].Column.Key).ToString();
        //                    }



        //                    SetCellValue(sheetOne.Rows[currentRow].Cells[currentCell], value);


        //                    //if (row.Cells[colIdx].Control == null)
        //                    //{
        //                    //    SetCellValue(sheetOne.Rows[currentRow].Cells[currentCell], null);
        //                    //}
        //                    //else if (row.Cells[colIdx].Control.Content is TextBlock)
        //                    //{
        //                    //    SetCellValue(sheetOne.Rows[currentRow].Cells[currentCell], ((TextBlock)row.Cells[colIdx].Control.Content).Text);
        //                    //}
        //                    //else if (row.Cells[colIdx].Control.Content is CheckBox)
        //                    //{
        //                    //    SetCellValue(sheetOne.Rows[currentRow].Cells[currentCell], ((CheckBox)row.Cells[colIdx].Control.Content).IsChecked);
        //                    //}
        //                    //else
        //                    //{
        //                    //    Debugger.Break();
        //                    //    throw new Exception("ExportGridToExcel():  Looping thru the columns to write each cells value, the cell's content type was not a TextBox or a CheckBox.  The cell's column key is: " + row.Cells[colIdx].Column.Key);
        //                    //}
        //                }
        //                catch (Exception ex)
        //                {
        //                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExportGridToExcel", ex);
        //                    ExceptionHelper.NotifyUserAnExceptionOccured();
        //                    Debugger.Break();
        //                    break;
        //                }

        //                //SetCellValue(sheetOne.Rows[currentRow].Cells[currentCell], ((TextBlock)row.Cells[colIdx].Control.Content).Text);
        //                //SetCellValue(sheetOne.Rows[currentRow].Cells[currentCell], row.Cells[colIdx].Value);
        //                currentCell++;
        //            }
        //            currentRow++;



        //            //foreach (Cell cell in row.Cells)
        //            //{
        //            //    if (cell.Column.Visibility == Visibility.Visible)
        //            //    {
        //            //        //if (cell.Column.Key == "RichGrid")
        //            //        //{
        //            //        //    Debugger.Break();
        //            //        //}

        //            //        SetCellValue(sheetOne.Rows[currentRow].Cells[currentCell], cell.Value);
        //            //        currentCell++;
        //            //    }
        //            //}
        //        }

        //        SaveExport(dataWorkbook);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExportGridToExcel", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //        Debugger.Break();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExportGridToExcel", IfxTraceCategory.Leave);
        //    }
        //}

        static private void SetCellValue(WorksheetCell cell, object value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCellValue", IfxTraceCategory.Enter);
                cell.Value = value;
                //cell.CellFormat.ShrinkToFit = ExcelDefaultableBoolean.False;
                //cell.CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
                //cell.CellFormat.Alignment = HorizontalCellAlignment.Center;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCellValue", ex);
                Debugger.Break();
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCellValue", IfxTraceCategory.Leave);
            }
        }

  

        static void SaveExcelToFile(SaveFileDialog dialog, OpenFileDialog openfileDialog, Workbook dataWorkbook)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveExcelToFile", IfxTraceCategory.Enter);
                dataWorkbook.SetCurrentFormat(WorkbookFormat.Excel2007);
                    using (Stream exportStream = dialog.OpenFile())
                    {
                        dataWorkbook.Save(exportStream);
                        exportStream.Close();

                        ////  THIS DOESNT WORK YET
                        //Workbook workbook1;

                        //// Open dialog
                        //if (openfileDialog.ShowDialog() == true)
                        //{
                        //    // If user selected a file get the stream of that file
                        //    FileStream stream = openfileDialog.File.OpenRead();

                        //    // Load workbook with data
                        //    workbook1 = Workbook.Load(stream);

                        //    stream.Close();
                        //}


                }



            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveExcelToFile", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
                Debugger.Break();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveExcelToFile", IfxTraceCategory.Leave);
            }
        }




    }
}
