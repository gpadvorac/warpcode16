﻿using EntityWireTypeSL;
using Ifx.SL;
using System;
using System.Globalization;
using System.Windows.Data;

namespace UIControls
{
    public class FileTypeImageConverter : IValueConverter
    {
        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "FileTypeImageConverter";


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Convert", IfxTraceCategory.Enter);

                if (value == null) { return null; }
                FileStorage_Values obj = value as FileStorage_Values;
                if (obj == null) { return null; }

                string fileName = obj.FS_FileName;
                if (fileName == null) { return null; }
                string ext = GetFileExtention(fileName);
                if (ext == null) { return null; }
                switch (ext.ToLower())
                {
                    case "log":
                        return "/UIControls;component/images/FileTXT.png";
                    case "txt":
                        return "/UIControls;component/images/FileTXT.png";

                    case "doc":
                        return "/UIControls;component/images/FileWord.png";
                    case "rtf":
                        return "/UIControls;component/images/FileWord.png";
                    case "docx":
                        return "/UIControls;component/images/FileWord.png";
                    case "docm":
                        return "/UIControls;component/images/FileWord.png";
                    case "dotx":
                        return "/UIControls;component/images/FileWord.png";
                    case "dotm":
                        return "/UIControls;component/images/FileWord.png";

                    case "pdf":
                        return "/UIControls;component/images/FilePDF.png";
                    case "fdf":
                        return "/UIControls;component/images/FilePDF.png";

                    case "xls":
                        return "/UIControls;component/images/FileExcel.png";
                    case "xlsx":
                        return "/UIControls;component/images/FileExcel.png";
                    case "xlsm":
                        return "/UIControls;component/images/FileExcel.png";
                    case "xlsb":
                        return "/UIControls;component/images/FileExcel.png";
                    //case "mhtml":
                    //    return "/UIControls;component/images/FileExcel.png";
                    case "sltx":
                        return "/UIControls;component/images/FileExcel.png";
                    case "xltm":
                        return "/UIControls;component/images/FileExcel.png";
                    case "xlt":
                        return "/UIControls;component/images/FileExcel.png";
                    case "csv":
                        return "/UIControls;component/images/FileExcel.png";

                    case "gif":
                        return "/UIControls;component/images/FileGif.png";

                    case "jpg":
                        return "/UIControls;component/images/FileJPG.png";
                    case "jpeg":
                        return "/UIControls;component/images/FileJPG.png";
                    case "jpe":
                        return "/UIControls;component/images/FileJPG.png";

                    case "png":
                        return "/UIControls;component/images/FilePNG.png";

                    case "tiff":
                        return "/UIControls;component/images/FileIMG.png";
                    case "tif":
                        return "/UIControls;component/images/FileIMG.png";
                    case "bmp":
                        return "/UIControls;component/images/FileIMG.png";
                    case "cur":
                        return "/UIControls;component/images/FileIMG.png";
                    case "ico":
                        return "/UIControls;component/images/FileIMG.png";
                }
                return "/UIControls;component/images/File.png";
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Convert", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Convert", IfxTraceCategory.Leave);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        /// <summary>
        /// A Helper method to get the file extention from the file name
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        string GetFileExtention(string fileName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileExtention", IfxTraceCategory.Enter);

                int pos = fileName.LastIndexOf(".");
                if (pos < 1)
                {
                    return null;
                }

                pos = pos + 1;
                if (pos == fileName.Length)
                {
                    return null;
                }

                string fileExtention = fileName.Substring(pos, fileName.Length - pos);
                return fileExtention;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileExtention", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
                return "";
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileExtention", IfxTraceCategory.Leave);
            }
        }










    }

}
