using System.Windows.Controls;
using System.Windows.Interactivity;
using UIControls.Controls.Entity.FileStorage;
using vFileStorageDialog;

namespace UIControls
{
    public class InvokeClickCommandAction : TriggerAction<HyperlinkButton>
    {
        protected override void Invoke(object parameter)
        {
            var f = AssociatedObject.DataContext as FileStorageViewModel;
            if (f != null)
            {
                FilesHelper.OpenFileInBrowser(f.FileStorageValues);
            }
        }
    }
}