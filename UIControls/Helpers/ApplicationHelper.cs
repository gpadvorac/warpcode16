using System.Collections.Generic;
//using UIControls.Models;

namespace UIControls
{
    public class ApplicationHelper
    {
        public static int Row { get; set; }

        public static int UserId { get; set; }

        public static string GetImagePathFromExtencion(string ext)
        {
            switch (ext.ToLower())
            {
                case "log":
                    return "Resources/Images/FileIcons/FileTXT.png";
                case "txt":
                    return "Resources/Images/FileIcons/FileTXT.png";

                case "doc":
                    return "Resources/Images/FileIcons/FileWord.png";
                case "rtf":
                    return "Resources/Images/FileIcons/FileWord.png";
                case "docx":
                    return "Resources/Images/FileIcons/FileWord.png";
                case "docm":
                    return "Resources/Images/FileIcons/FileWord.png";
                case "dotx":
                    return "Resources/Images/FileIcons/FileWord.png";
                case "dotm":
                    return "Resources/Images/FileIcons/FileWord.png";

                case "pdf":
                    return "Resources/Images/FileIcons/FilePDF.png";
                case "fdf":
                    return "Resources/Images/FileIcons/FilePDF.png";

                case "xls":
                    return "Resources/Images/FileIcons/FileExcel.png";
                case "xlsx":
                    return "Resources/Images/FileIcons/FileExcel.png";
                case "xlsm":
                    return "Resources/Images/FileIcons/FileExcel.png";
                case "xlsb":
                    return "Resources/Images/FileIcons/FileExcel.png";
                //case "mhtml":
                //    return "Resources/Images/FileIcons/FileExcel.png";
                case "sltx":
                    return "Resources/Images/FileIcons/FileExcel.png";
                case "xltm":
                    return "Resources/Images/FileIcons/FileExcel.png";
                case "xlt":
                    return "Resources/Images/FileIcons/FileExcel.png";
                case "csv":
                    return "Resources/Images/FileIcons/FileExcel.png";

                case "gif":
                    return "Resources/Images/FileIcons/FileGif.png";

                case "jpg":
                    return "Resources/Images/FileIcons/FileJPG.png";
                case "jpeg":
                    return "Resources/Images/FileIcons/FileJPG.png";
                case "jpe":
                    return "Resources/Images/FileIcons/FileJPG.png";

                case "png":
                    return "Resources/Images/FileIcons/FilePNG.png";

                case "tiff":
                    return "Resources/Images/FileIcons/FileIMG.png";
                case "tif":
                    return "Resources/Images/FileIcons/FileIMG.png";
                case "bmp":
                    return "Resources/Images/FileIcons/FileIMG.png";
                case "cur":
                    return "Resources/Images/FileIcons/FileIMG.png";
                case "ico":
                    return "Resources/Images/FileIcons/FileIMG.png";

            }
            return null;
        }
        #region Singleton

        private static ApplicationHelper _instance;

        public static ApplicationHelper Instance
        {
            get { return _instance ?? (_instance = new ApplicationHelper()); }
        }

        private ApplicationHelper()
        {
        }

        #endregion

    }
}