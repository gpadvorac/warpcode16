﻿using System;
using System.Net;

namespace UIControls
{



    #region ClosePopUp


    public delegate void ClosePopUpEventHandler(object sender, ClosePopUpArgs e);

    public class ClosePopUpArgs : EventArgs
    {
       
        public ClosePopUpArgs()
        {
         
        }

    }


    #endregion ClosePopUp




    #region TaskAssigneesUpdated


    public delegate void TaskAssigneesUpdatedEventHandler(object sender, TaskAssigneesUpdatedArgs e);

    public class TaskAssigneesUpdatedArgs : EventArgs
    {
        private readonly Guid _tk_Id;
        //private readonly Guid? _id_Id;
        private readonly Guid? _prj_Id;

        public TaskAssigneesUpdatedArgs(Guid tk_Id, Guid? prj_Id)
        {
            _tk_Id = tk_Id;
            //_id_Id = id_Id;
            _prj_Id = prj_Id;
        }


        public Guid Tk_Id
        {
            get { return _tk_Id; }
        }

        //public Guid? Id_Id
        //{
        //    get { return _id_Id; }
        //}

        public Guid? Prj_Id
        {
            get { return _prj_Id; }
        }

    }

    #endregion TaskAssigneesUpdated




    #region ComboListRefreshCompleted


    public delegate void ComboListRefreshCompletedEventHandler(object sender, ComboListRefreshCompletedArgs e);

    public class ComboListRefreshCompletedArgs : EventArgs
    {
        private readonly string _key;

        public ComboListRefreshCompletedArgs(string key)
        {
            _key = key;
        
        }


        public string Key
        {
            get { return _key; }
        }

      

    }

    #endregion ComboListRefreshCompleted




}




namespace EntityBll.SL
{



    #region WellInterestUpdated


    public delegate void WellInterestUpdatedEventHandler(object sender, WellInterestUpdatedArgs e);

    public class WellInterestUpdatedArgs : EventArgs
    {
        private readonly Guid _wl_Id;
        public WellInterestUpdatedArgs(Guid wl_Id)
        {
            _wl_Id = wl_Id;
        }

        public Guid Wl_Id
        {
            get { return _wl_Id; }
        }
    }


    #endregion WellInterestUpdated




    #region LeaseTractInterestUpdated


    public delegate void LeaseTractInterestUpdatedEventHandler(object sender, LeaseTractInterestUpdatedArgs e);

    public class LeaseTractInterestUpdatedArgs : EventArgs
    {
        private readonly Guid? _ls_Id;
        private readonly Guid _lsTr_Id;

        public LeaseTractInterestUpdatedArgs(Guid? ls_Id, Guid lsTr_Id)
        {
            _ls_Id = ls_Id;
            _lsTr_Id = lsTr_Id;
        }

        public Guid? Ls_Id
        {
            get { return _ls_Id; }
        }

        public Guid LsTr_Id
        {
            get { return _lsTr_Id; }
        }
    }


    #endregion LeaseTractInterestUpdated





    #region LeaseTractUpdated

    /// <summary>
    /// When the Tract is inserted or updated, a Tract Trigger fires which rolls up data to the Lease.
    /// This causes the TimeStamp on the lease to be changed to a new value.  
    /// This new value must be syncronized the lease, otherwise a broken concurrency event will fire if the lease is modified.
    /// </summary>
    /// <param name="sender">LeaseTract.bll</param>
    /// <param name="e">Ls_Id</param>
    /// <param name="e">Ls_Stamp</param>
    public delegate void LeaseTractUpdatedEventHandler(object sender, LeaseTractUpdatedArgs e);

    public class LeaseTractUpdatedArgs : EventArgs
    {
        private readonly Guid? _ls_Id;
        private readonly byte[] _ls_Stamp;

        public LeaseTractUpdatedArgs(Guid? ls_Id, byte[] ls_Stamp)
        {
            _ls_Id = ls_Id;
            _ls_Stamp = ls_Stamp;
        }

        public Guid? Ls_Id
        {
            get { return _ls_Id; }
        }

        public byte[] Ls_Stamp
        {
            get { return _ls_Stamp; }
        }

    }


    #endregion LeaseTractUpdated






    #region GroupAssignmentUpdated

    /// <summary>
    /// When the Tract is inserted or updated, a Tract Trigger fires which rolls up data to the Lease.
    /// This causes the TimeStamp on the lease to be changed to a new value.  
    /// This new value must be syncronized the lease, otherwise a broken concurrency event will fire if the lease is modified.
    /// </summary>
    /// <param name="sender">LeaseTract.bll</param>
    /// <param name="e">Ls_Id</param>
    /// <param name="e">Ls_Stamp</param>
    public delegate void GroupAssignmentUpdatedEventHandler(object sender, GroupAssignmentUpdatedArgs e);

    public class GroupAssignmentUpdatedArgs : EventArgs
    {

        //private readonly Guid? _ls_Id;
        //private readonly byte[] _ls_Stamp;

        //public GroupAssignmentUpdatedArgs(Guid? ls_Id, byte[] ls_Stamp)
        //{
        //    //_ls_Id = ls_Id;
        //    //_ls_Stamp = ls_Stamp;
        //}


        public GroupAssignmentUpdatedArgs()
        {
            //_ls_Id = ls_Id;
            //_ls_Stamp = ls_Stamp;
        }



        //public Guid? Ls_Id
        //{
        //    get { return _ls_Id; }
        //}

        //public byte[] Ls_Stamp
        //{
        //    get { return _ls_Stamp; }
        //}

    }


    #endregion GroupAssignmentUpdated





    

    //#region CustomGridSelectedItem


    //public delegate void CustomGridSelectedItemEventHandler(object sender, CustomGridSelectedItemArgs e);

    //public class CustomGridSelectedItemArgs : EventArgs
    //{
    //    private readonly Guid _id;
    //    public CustomGridSelectedItemArgs(Guid id)
    //    {
    //        _id = id;
    //    }

    //    public Guid Id
    //    {
    //        get { return _id; }
    //    }
    //}


    //#endregion CustomGridSelectedItem















}
