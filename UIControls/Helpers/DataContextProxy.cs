﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace UIControls
{
    public class DataContextProxy : DependencyObject
    {
        /// <summary>
        /// Using a DependencyProperty as the backing store for ProxiedObject.    
        /// This enables animation, styling, binding, etc...   
        /// </summary>
        public static readonly DependencyProperty ProxiedObjectProperty = DependencyProperty.Register("ProxiedObject", typeof(object), typeof(DataContextProxy), null);

        /// <summary>
        /// Gets or sets the Proxy object
        /// </summary>
        public object ProxiedObject
        {
            get { return GetValue(ProxiedObjectProperty); }
            set { SetValue(ProxiedObjectProperty, value); }
        }
    }

    public class Test : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}