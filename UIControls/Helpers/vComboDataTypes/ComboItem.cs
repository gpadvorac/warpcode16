﻿
using System;
using System.Diagnostics;
namespace vComboDataTypes
{

    public class ComboItem 
    {
        object _id;
        object _fK;
        string _itemName;
        string _desc;
        string _desc2;
        string _desc3;
        string _desc4;
        string _desc5;
        string _desc6;
        string _desc7;
        string _desc8;
        string _desc9;
        string _desc10;
        string _desc11;
        string _desc12;
        string _desc13;
        string _desc14;
        string _desc15;


        public ComboItem()
        {

        }

        public ComboItem(object id, string itemName)
        {
            try
            {
            _id = id;
            _itemName = itemName;
            }
            catch (Exception ex)
            {

            }
        }

        public ComboItem(object id, object fk, string itemName)
        {
            try
            {
                _id = id;
                _fK = fk;
                _itemName = itemName;
            }
            catch (Exception ex)
            {

            }
        }


        public ComboItem(object id, object fk, string itemName, string desc)
        {
            try
            {
                _id = id;
                _fK = fk;
                _itemName = itemName;
                _desc = desc;
            }
            catch (Exception ex)
            {

            }
        }

        public ComboItem(object id, object fk, string itemName, string desc, string desc2)
        {
            try
            {
                _id = id;
                _fK = fk;
                _itemName = itemName;
                _desc = desc;
                _desc2 = desc2;
            }
            catch (Exception ex)
            {

            }
        }

        public ComboItem(object id, object fk, string itemName, string desc, string desc2, string desc3)
        {
            try
            {
                _id = id;
                _fK = fk;
                _itemName = itemName;
                _desc = desc;
                _desc2 = desc2;
                _desc3 = desc3;
            }
            catch (Exception ex)
            {

            }
        }

        public ComboItem(object id, object fk, string itemName, string desc, string desc2, string desc3, string desc4)
        {
            try
            {
                _id = id;
                _fK = fk;
                _itemName = itemName;
                _desc = desc;
                _desc2 = desc2;
                _desc3 = desc3;
                _desc4 = desc4;
            }
            catch (Exception ex)
            {

            }
        }

        public ComboItem(object[] data)
        {
            try
            {
                _id = data[0];
                _fK = data[1];
                //_itemName = (string)data[2];
                if (data[2] != null)
                {
                    _itemName = data[2].ToString();
                }
                else
                {
                    _itemName = null;
                }
                
                switch (data.Length)
                {
                    case 4:
                        if (data[3] != null)
                        {
                            _desc = data[3].ToString();
                        }
                        else
                        {
                            _desc = null;
                        }
                        break;
                    case 5:
                        if (data[3] != null)
                        {
                            _desc = data[3].ToString();
                        }
                        else
                        {
                            _desc = null;
                        }

                        if (data[4] != null)
                        {
                            _desc2 = data[4].ToString();
                        }
                        else
                        {
                            _desc2 = null;
                        }
                        break;
                    case 6:
                        if (data[3] != null)
                        {
                            _desc = data[3].ToString();
                        }
                        else
                        {
                            _desc = null;
                        }

                        if (data[4] != null)
                        {
                            _desc2 = data[4].ToString();
                        }
                        else
                        {
                            _desc2 = null;
                        }

                        if (data[5] != null)
                        {
                            _desc3 = data[5].ToString();
                        }
                        else
                        {
                            _desc3 = null;
                        }
                        break;
                    case 7:
                        if (data[3] != null)
                        {
                            _desc = data[3].ToString();
                        }
                        else
                        {
                            _desc = null;
                        }

                        if (data[4] != null)
                        {
                            _desc2 = data[4].ToString();
                        }
                        else
                        {
                            _desc2 = null;
                        }

                        if (data[5] != null)
                        {
                            _desc3 = data[5].ToString();
                        }
                        else
                        {
                            _desc3 = null;
                        }

                        if (data[6] != null)
                        {
                            _desc4 = data[6].ToString();
                        }
                        else
                        {
                            _desc4 = null;
                        }
                        break;
                    case 8:
                        if (data[3] != null)
                        {
                            _desc = data[3].ToString();
                        }
                        else
                        {
                            _desc = null;
                        }

                        if (data[4] != null)
                        {
                            _desc2 = data[4].ToString();
                        }
                        else
                        {
                            _desc2 = null;
                        }

                        if (data[5] != null)
                        {
                            _desc3 = data[5].ToString();
                        }
                        else
                        {
                            _desc3 = null;
                        }

                        if (data[6] != null)
                        {
                            _desc4 = data[6].ToString();
                        }
                        else
                        {
                            _desc4 = null;
                        }

                        if (data[7] != null)
                        {
                            _desc5 = data[7].ToString();
                        }
                        else
                        {
                            _desc5 = null;
                        }
                        break;
                    case 9:
                        if (data[3] != null)
                        {
                            _desc = data[3].ToString();
                        }
                        else
                        {
                            _desc = null;
                        }

                        if (data[4] != null)
                        {
                            _desc2 = data[4].ToString();
                        }
                        else
                        {
                            _desc2 = null;
                        }

                        if (data[5] != null)
                        {
                            _desc3 = data[5].ToString();
                        }
                        else
                        {
                            _desc3 = null;
                        }

                        if (data[6] != null)
                        {
                            _desc4 = data[6].ToString();
                        }
                        else
                        {
                            _desc4 = null;
                        }

                        if (data[7] != null)
                        {
                            _desc5 = data[7].ToString();
                        }
                        else
                        {
                            _desc5 = null;
                        }

                        if (data[8] != null)
                        {
                            _desc6 = data[8].ToString();
                        }
                        else
                        {
                            _desc6 = null;
                        }
                        break;
                    case 10:
                        if (data[3] != null)
                        {
                            _desc = data[3].ToString();
                        }
                        else
                        {
                            _desc = null;
                        }

                        if (data[4] != null)
                        {
                            _desc2 = data[4].ToString();
                        }
                        else
                        {
                            _desc2 = null;
                        }

                        if (data[5] != null)
                        {
                            _desc3 = data[5].ToString();
                        }
                        else
                        {
                            _desc3 = null;
                        }

                        if (data[6] != null)
                        {
                            _desc4 = data[6].ToString();
                        }
                        else
                        {
                            _desc4 = null;
                        }

                        if (data[7] != null)
                        {
                            _desc5 = data[7].ToString();
                        }
                        else
                        {
                            _desc5 = null;
                        }

                        if (data[8] != null)
                        {
                            _desc6 = data[8].ToString();
                        }
                        else
                        {
                            _desc6 = null;
                        }

                        if (data[9] != null)
                        {
                            _desc7 = data[9].ToString();
                        }
                        else
                        {
                            _desc7 = null;
                        }
                        break;
                    case 11:
                        if (data[3] != null)
                        {
                            _desc = data[3].ToString();
                        }
                        else
                        {
                            _desc = null;
                        }

                        if (data[4] != null)
                        {
                            _desc2 = data[4].ToString();
                        }
                        else
                        {
                            _desc2 = null;
                        }

                        if (data[5] != null)
                        {
                            _desc3 = data[5].ToString();
                        }
                        else
                        {
                            _desc3 = null;
                        }

                        if (data[6] != null)
                        {
                            _desc4 = data[6].ToString();
                        }
                        else
                        {
                            _desc4 = null;
                        }

                        if (data[7] != null)
                        {
                            _desc5 = data[7].ToString();
                        }
                        else
                        {
                            _desc5 = null;
                        }

                        if (data[8] != null)
                        {
                            _desc6 = data[8].ToString();
                        }
                        else
                        {
                            _desc6 = null;
                        }

                        if (data[9] != null)
                        {
                            _desc7 = data[9].ToString();
                        }
                        else
                        {
                            _desc7 = null;
                        }

                        if (data[10] != null)
                        {
                            _desc8 = data[10].ToString();
                        }
                        else
                        {
                            _desc8 = null;
                        }
                        break;
                    case 12:
                        if (data[3] != null)
                        {
                            _desc = data[3].ToString();
                        }
                        else
                        {
                            _desc = null;
                        }

                        if (data[4] != null)
                        {
                            _desc2 = data[4].ToString();
                        }
                        else
                        {
                            _desc2 = null;
                        }

                        if (data[5] != null)
                        {
                            _desc3 = data[5].ToString();
                        }
                        else
                        {
                            _desc3 = null;
                        }

                        if (data[6] != null)
                        {
                            _desc4 = data[6].ToString();
                        }
                        else
                        {
                            _desc4 = null;
                        }

                        if (data[7] != null)
                        {
                            _desc5 = data[7].ToString();
                        }
                        else
                        {
                            _desc5 = null;
                        }

                        if (data[8] != null)
                        {
                            _desc6 = data[8].ToString();
                        }
                        else
                        {
                            _desc6 = null;
                        }

                        if (data[9] != null)
                        {
                            _desc7 = data[9].ToString();
                        }
                        else
                        {
                            _desc7 = null;
                        }

                        if (data[10] != null)
                        {
                            _desc8 = data[10].ToString();
                        }
                        else
                        {
                            _desc8 = null;
                        }

                        if (data[11] != null)
                        {
                            _desc9 = data[11].ToString();
                        }
                        else
                        {
                            _desc9 = null;
                        }
                        break;
                    case 13:
                        if (data[3] != null)
                        {
                            _desc = data[3].ToString();
                        }
                        else
                        {
                            _desc = null;
                        }

                        if (data[4] != null)
                        {
                            _desc2 = data[4].ToString();
                        }
                        else
                        {
                            _desc2 = null;
                        }

                        if (data[5] != null)
                        {
                            _desc3 = data[5].ToString();
                        }
                        else
                        {
                            _desc3 = null;
                        }

                        if (data[6] != null)
                        {
                            _desc4 = data[6].ToString();
                        }
                        else
                        {
                            _desc4 = null;
                        }

                        if (data[7] != null)
                        {
                            _desc5 = data[7].ToString();
                        }
                        else
                        {
                            _desc5 = null;
                        }

                        if (data[8] != null)
                        {
                            _desc6 = data[8].ToString();
                        }
                        else
                        {
                            _desc6 = null;
                        }

                        if (data[9] != null)
                        {
                            _desc7 = data[9].ToString();
                        }
                        else
                        {
                            _desc7 = null;
                        }

                        if (data[10] != null)
                        {
                            _desc8 = data[10].ToString();
                        }
                        else
                        {
                            _desc8 = null;
                        }

                        if (data[11] != null)
                        {
                            _desc9 = data[11].ToString();
                        }
                        else
                        {
                            _desc9 = null;
                        }

                        if (data[12] != null)
                        {
                            _desc10 = data[12].ToString();
                        }
                        else
                        {
                            _desc10 = null;
                        }
                        break;
                    case 14:
                        if (data[3] != null)
                        {
                            _desc = data[3].ToString();
                        }
                        else
                        {
                            _desc = null;
                        }

                        if (data[4] != null)
                        {
                            _desc2 = data[4].ToString();
                        }
                        else
                        {
                            _desc2 = null;
                        }

                        if (data[5] != null)
                        {
                            _desc3 = data[5].ToString();
                        }
                        else
                        {
                            _desc3 = null;
                        }

                        if (data[6] != null)
                        {
                            _desc4 = data[6].ToString();
                        }
                        else
                        {
                            _desc4 = null;
                        }

                        if (data[7] != null)
                        {
                            _desc5 = data[7].ToString();
                        }
                        else
                        {
                            _desc5 = null;
                        }

                        if (data[8] != null)
                        {
                            _desc6 = data[8].ToString();
                        }
                        else
                        {
                            _desc6 = null;
                        }

                        if (data[9] != null)
                        {
                            _desc7 = data[9].ToString();
                        }
                        else
                        {
                            _desc7 = null;
                        }

                        if (data[10] != null)
                        {
                            _desc8 = data[10].ToString();
                        }
                        else
                        {
                            _desc8 = null;
                        }

                        if (data[11] != null)
                        {
                            _desc9 = data[11].ToString();
                        }
                        else
                        {
                            _desc9 = null;
                        }

                        if (data[12] != null)
                        {
                            _desc10 = data[12].ToString();
                        }
                        else
                        {
                            _desc10 = null;
                        }

                        if (data[13] != null)
                        {
                            _desc11 = data[13].ToString();
                        }
                        else
                        {
                            _desc11 = null;
                        }
                        break;
                    case 15:
                        if (data[3] != null)
                        {
                            _desc = data[3].ToString();
                        }
                        else
                        {
                            _desc = null;
                        }

                        if (data[4] != null)
                        {
                            _desc2 = data[4].ToString();
                        }
                        else
                        {
                            _desc2 = null;
                        }

                        if (data[5] != null)
                        {
                            _desc3 = data[5].ToString();
                        }
                        else
                        {
                            _desc3 = null;
                        }

                        if (data[6] != null)
                        {
                            _desc4 = data[6].ToString();
                        }
                        else
                        {
                            _desc4 = null;
                        }

                        if (data[7] != null)
                        {
                            _desc5 = data[7].ToString();
                        }
                        else
                        {
                            _desc5 = null;
                        }

                        if (data[8] != null)
                        {
                            _desc6 = data[8].ToString();
                        }
                        else
                        {
                            _desc6 = null;
                        }

                        if (data[9] != null)
                        {
                            _desc7 = data[9].ToString();
                        }
                        else
                        {
                            _desc7 = null;
                        }

                        if (data[10] != null)
                        {
                            _desc8 = data[10].ToString();
                        }
                        else
                        {
                            _desc8 = null;
                        }

                        if (data[11] != null)
                        {
                            _desc9 = data[11].ToString();
                        }
                        else
                        {
                            _desc9 = null;
                        }

                        if (data[12] != null)
                        {
                            _desc10 = data[12].ToString();
                        }
                        else
                        {
                            _desc10 = null;
                        }

                        if (data[13] != null)
                        {
                            _desc11 = data[13].ToString();
                        }
                        else
                        {
                            _desc11 = null;
                        }

                        if (data[14] != null)
                        {
                            _desc12 = data[14].ToString();
                        }
                        else
                        {
                            _desc12 = null;
                        }
                        break;
                    case 16:
                        if (data[3] != null)
                        {
                            _desc = data[3].ToString();
                        }
                        else
                        {
                            _desc = null;
                        }

                        if (data[4] != null)
                        {
                            _desc2 = data[4].ToString();
                        }
                        else
                        {
                            _desc2 = null;
                        }

                        if (data[5] != null)
                        {
                            _desc3 = data[5].ToString();
                        }
                        else
                        {
                            _desc3 = null;
                        }

                        if (data[6] != null)
                        {
                            _desc4 = data[6].ToString();
                        }
                        else
                        {
                            _desc4 = null;
                        }

                        if (data[7] != null)
                        {
                            _desc5 = data[7].ToString();
                        }
                        else
                        {
                            _desc5 = null;
                        }

                        if (data[8] != null)
                        {
                            _desc6 = data[8].ToString();
                        }
                        else
                        {
                            _desc6 = null;
                        }

                        if (data[9] != null)
                        {
                            _desc7 = data[9].ToString();
                        }
                        else
                        {
                            _desc7 = null;
                        }

                        if (data[10] != null)
                        {
                            _desc8 = data[10].ToString();
                        }
                        else
                        {
                            _desc8 = null;
                        }

                        if (data[11] != null)
                        {
                            _desc9 = data[11].ToString();
                        }
                        else
                        {
                            _desc9 = null;
                        }

                        if (data[12] != null)
                        {
                            _desc10 = data[12].ToString();
                        }
                        else
                        {
                            _desc10 = null;
                        }

                        if (data[13] != null)
                        {
                            _desc11 = data[13].ToString();
                        }
                        else
                        {
                            _desc11 = null;
                        }

                        if (data[15] != null)
                        {
                            _desc13 = data[15].ToString();
                        }
                        else
                        {
                            _desc13 = null;
                        }
                        break;
                }













                //if (data.Length == 4)
                //{
                //    if (data[3] != null)
                //    {
                //        _desc = data[3] as string;
                //    }
                //}
                //if (data.Length == 5)
                //{
                //    if (data[4] != null)
                //    {
                //        _desc = data[3] as string;
                //        _desc2 = data[4] as string;
                //    }
                //}
                //if (data.Length == 6)
                //{
                //    if (data[5] != null)
                //    {
                //        _desc = data[3] as string;
                //        _desc2 = data[4] as string;
                //        _desc3 = data[5] as string;
                //    }
                //}
                //if (data.Length == 7)
                //{
                //    if (data[6] != null)
                //    {
                //        _desc = data[3] as string;
                //        _desc2 = data[4] as string;
                //        _desc3 = data[5] as string;
                //        _desc4 = data[6] as string;
                //    }
                //}
            }
            catch (Exception ex)
            {

            }
        }

        public object Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string ItemName
        {
            get { return _itemName; }
            set { _itemName = value; }
        }

        public object FK
        {
            get { return _fK; }
            set { _fK = value; }
        }

        public string Desc
        {
            get { return _desc; }
            set { _desc = value; }
        }


        public string Desc2
        {
            get { return _desc2; }
            set { _desc2 = value; }
        }

        public string Desc3
        {
            get { return _desc3; }
            set { _desc3 = value; }
        }

        public string Desc4
        {
            get { return _desc4; }
            set { _desc4 = value; }
        }

        public string Desc5
        {
            get { return _desc5; }
            set { _desc5 = value; }
        }

        public string Desc6
        {
            get { return _desc6; }
            set { _desc6 = value; }
        }

        public string Desc7
        {
            get { return _desc7; }
            set { _desc7 = value; }
        }

        public string Desc8
        {
            get { return _desc8; }
            set { _desc8 = value; }
        }

        public string Desc9
        {
            get { return _desc9; }
            set { _desc9 = value; }
        }

        public string Desc10
        {
            get { return _desc10; }
            set { _desc10 = value; }
        }

        public string Desc11
        {
            get { return _desc11; }
            set { _desc11 = value; }
        }

        public string Desc12
        {
            get { return _desc12; }
            set { _desc12 = value; }
        }

        public string Desc13
        {
            get { return _desc13; }
            set { _desc13 = value; }
        }

        public string Desc14
        {
            get { return _desc14; }
            set { _desc14 = value; }
        }

        public string Desc15
        {
            get { return _desc15; }
            set { _desc15 = value; }
        }
    }

}
