﻿using System;
using System.Windows.Data;
using Infragistics;
using System.Diagnostics;
namespace vComboDataTypes
{
    public class ComboItemForGroupByConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                ComboItemList ls = parameter as ComboItemList;
                GroupByDataContext dc = value as GroupByDataContext;

                if (ls != null && dc != null)
                {
                    foreach (ComboItem item in ls)
                    {
                        if (dc.Value == null)
                        {
                            return "";
                        }

                        if (item.Id.ToString().Equals(dc.Value.ToString()))
                        {
                            return item.ItemName;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                // log exception - doent throw it
                //  throw new Exception("Exception was thrown from vComboDataTypes.ComboItemForGroupByConverter.Convert()", ex);
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                throw new NotImplementedException("vComboDataTypes.ComboItemForGroupByConverter.ConvertBack NotImplementedException");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
