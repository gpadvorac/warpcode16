﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationTypeServices;
using EntityBll.SL;
using Ifx.SL;
using Infragistics.Controls.Interactions;
using vDialogControl;
using vDiscussionDialog;
using vUICommon;

namespace UIControls
{
    public class vDialogHelper
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "vDialogHelper";

        //private static readonly int _headerHeight = 24;
        //private static readonly Brush _headerForeground = new SolidColorBrush(Colors.White);
        //private static readonly Thickness _thickness = new Thickness(0);

        #endregion Initialize Variables

        static public vDialog InitializeAttachmentDialog(UserControl ctl, string parentType, string headerText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeAttachmentDialog", IfxTraceCategory.Enter);
                var attachmentDialog = new vDialog
                {
                    StartupPosition = StartupPosition.Center,
                    //MinimizeButtonVisibility = System.Windows.Visibility.Collapsed,
                    //MaximizeButtonVisibility = System.Windows.Visibility.Collapsed,
                    IsShowSizeInHeader = ApplicationTypeServices.ApplicationLevelVariables.IsShowSizeInHeader,
                    MinHeight = 250,
                    Height = 450,
                    Width = 550,
                    //HeaderHeight = _headerHeight,
                    //Padding = _thickness,
                    //HeaderForeground = _headerForeground,
                    IsModal = true,
                    Content = ctl
                };
                if (!string.IsNullOrEmpty(headerText))
                {
                    attachmentDialog.Header = "Attachments For " + parentType + ":  " + headerText;
                }
                else
                {
                    attachmentDialog.Header = "Attachments For " + parentType;
                }
                attachmentDialog.WindowStateChanged += dialog_WindowStateChanged;
                return attachmentDialog;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeAttachmentDialog", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeAttachmentDialog", IfxTraceCategory.Leave);
            }
        }

        static public DiscussionDialogDirector InitializeDiscussionDialog(object[] array, vDialogMode mode, Guid parentId, string type, string headerText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeDiscussionDialog", IfxTraceCategory.Enter);
                var discussions = new Discussion_List();
                //if (array != null)
                //{
                //    discussions.ReplaceList(array);
                //}
                var discussionDialog = new DiscussionDialogDirector(array, mode, parentId, type, DiscussionViewModel.DiscussionViewContext.General, null);
                //var discussionDialog = new DiscussionDialogDirector(null, parentId, null, null, null);
                discussionDialog.Dialog.IsShowSizeInHeader =
                    ApplicationTypeServices.ApplicationLevelVariables.IsShowSizeInHeader;
                discussionDialog.Dialog.IsModal = true;
                discussionDialog.Dialog.StartupPosition = StartupPosition.Center;
                //discussionDialog.Dialog.HeaderHeight = _headerHeight;
                discussionDialog.Dialog.Padding = new Thickness(0, 0, 0, 10);
                //discussionDialog.Dialog.HeaderForeground = _headerForeground;

                if (!string.IsNullOrEmpty(headerText))
                {
                    discussionDialog.Dialog.Header = "Discussions For " + type + ":  " + headerText;
                }
                else
                {
                    discussionDialog.Dialog.Header = "Discussions For " + type;
                }
                discussionDialog.Dialog.WindowStateChanged += dialog_WindowStateChanged;
                return discussionDialog;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeDiscussionDialog", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeDiscussionDialog", IfxTraceCategory.Leave);
            }
        }


        static public vDialog InitializeEntityDialog(UserControl ctl, int? width, int? height, string headerEntityName, string headerRecordText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeEntityDialog", IfxTraceCategory.Enter);
                var dialog = new vDialog
                {
                    StartupPosition = StartupPosition.Center,
                    //MinimizeButtonVisibility = System.Windows.Visibility.Collapsed,
                    //MaximizeButtonVisibility = System.Windows.Visibility.Collapsed,
                    IsShowSizeInHeader = ApplicationTypeServices.ApplicationLevelVariables.IsShowSizeInHeader,
                    //HeaderHeight = _headerHeight,
                    //Padding = _thickness,
                    //HeaderForeground = _headerForeground,
                    Background = new SolidColorBrush(Colors.White),
                    IsModal = true,
                    Content = ctl
                };
                if (height != null)
                {
                    dialog.Height = (int)height;
                }
                if (width != null)
                {
                    dialog.Width = (int)width;
                }

                if (!string.IsNullOrEmpty(headerRecordText))
                {
                    dialog.Header = headerEntityName + "For:   " + headerRecordText;
                }
                else
                {
                    dialog.Header = headerEntityName;
                }
                dialog.WindowStateChanged += dialog_WindowStateChanged;
                return dialog;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeEntityDialog", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeEntityDialog", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// Added for the Edit Profile dialog
        /// </summary>
        /// <param name="ctl"></param>
        /// <param name="headerEntityName"></param>
        /// <param name="headerRecordText"></param>
        /// <returns></returns>
        static public vDialog InitializeEntityDialog(UserControl ctl, string headerEntityName, string headerRecordText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeEntityDialog", IfxTraceCategory.Enter);
                var dialog = new vDialog
                {
                    IsShowSizeInHeader = ApplicationLevelVariables.IsShowSizeInHeader,
                    //HeaderHeight = _headerHeight,
                    //Padding = _thickness,
                    //HeaderForeground = _headerForeground,
                    Background = new SolidColorBrush(Colors.White),
                    Content = ctl
                };

                if (!string.IsNullOrEmpty(headerRecordText))
                {
                    dialog.Header = headerEntityName + "For:   " + headerRecordText;
                }
                else
                {
                    dialog.Header = headerEntityName;
                }
                dialog.WindowStateChanged += dialog_WindowStateChanged;
                return dialog;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeEntityDialog", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeEntityDialog", IfxTraceCategory.Leave);
            }
        }




        static void dialog_WindowStateChanged(object sender, WindowStateChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "dialog_WindowStateChanged", IfxTraceCategory.Enter);

                if (e.NewWindowState == Infragistics.Controls.Interactions.WindowState.Hidden)
                {
                    ((vDialog)sender).Content = null;
                    sender = null;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "dialog_WindowStateChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "dialog_WindowStateChanged", IfxTraceCategory.Leave);
            }
        }



        //static public vDialog InitializeStandardDialog(UserControl ctl, int width, int height, string headerEntityName, string headerRecordText)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeEntityDialog", IfxTraceCategory.Enter);

        //        var attachmentDialog = new vDialog
        //        {
        //            StartupPosition = StartupPosition.Center,
        //            MinimizeButtonVisibility = System.Windows.Visibility.Collapsed,
        //            MaximizeButtonVisibility = System.Windows.Visibility.Collapsed,
        //            Height = height,
        //            Width = width,
        //            HeaderHeight = 24,
        //            Padding = new Thickness(0),
        //            HeaderForeground = new SolidColorBrush(Colors.White),
        //            IsModal = true,
        //            Content = ctl
        //        };


        //        if (!string.IsNullOrEmpty(headerRecordText))
        //        {
        //            attachmentDialog.Header = headerEntityName + "For:   " + headerRecordText;
        //        }
        //        else
        //        {
        //            attachmentDialog.Header = headerEntityName;
        //        }



        //        return attachmentDialog;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeEntityDialog", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeEntityDialog", IfxTraceCategory.Leave);
        //    }
        //}


    }
}
