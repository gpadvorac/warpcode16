﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UIControls.Globalization.WcStoredProcParam {
    using System;
    
    
    /// <summary>
    /// A strongly-typed resource class, for looking up localized strings, formatting them, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilderEx class via the ResXFileCodeGeneratorEx custom tool.
    // To add or remove a member, edit your .ResX file then rerun the ResXFileCodeGeneratorEx custom tool or rebuild your VS.NET project.
    // Copyright (c) Dmytro Kryvko 2006-2017 (http://dmytro.kryvko.googlepages.com/)
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("DMKSoftware.CodeGenerators.Tools.StronglyTypedResourceBuilderEx", "2.6.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
#if !SILVERLIGHT
    [global::System.Reflection.ObfuscationAttribute(Exclude=true, ApplyToMembers=true)]
#endif
    [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
    public partial class StringsWcStoredProcParamList {
        
        private static global::System.Resources.ResourceManager _resourceManager;
        
        private static object _internalSyncObject;
        
        private static global::System.Globalization.CultureInfo _resourceCulture;
        
        /// <summary>
        /// Initializes a StringsWcStoredProcParamList object.
        /// </summary>
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public StringsWcStoredProcParamList() {
        }
        
        /// <summary>
        /// Thread safe lock object used by this class.
        /// </summary>
        public static object InternalSyncObject {
            get {
                if (object.ReferenceEquals(_internalSyncObject, null)) {
                    global::System.Threading.Interlocked.CompareExchange(ref _internalSyncObject, new object(), null);
                }
                return _internalSyncObject;
            }
        }
        
        /// <summary>
        /// Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(_resourceManager, null)) {
                    global::System.Threading.Monitor.Enter(InternalSyncObject);
                    try {
                        if (object.ReferenceEquals(_resourceManager, null)) {
                            global::System.Threading.Interlocked.Exchange(ref _resourceManager, new global::System.Resources.ResourceManager("UIControls.Globalization.WcStoredProcParam.StringsWcStoredProcParamList", typeof(StringsWcStoredProcParamList).Assembly));
                        }
                    }
                    finally {
                        global::System.Threading.Monitor.Exit(InternalSyncObject);
                    }
                }
                return _resourceManager;
            }
        }
        
        /// <summary>
        /// Overrides the current thread's CurrentUICulture property for all
        /// resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return _resourceCulture;
            }
            set {
                _resourceCulture = value;
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Default Value'.
        /// </summary>
        public static string SpP_DefaultValue {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_DefaultValue, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Default Value'.
        /// </summary>
        public static string SpP_DefaultValue_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_DefaultValue_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'DotNet Type'.
        /// </summary>
        public static string SpP_DtNt_Id {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_DtNt_Id, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'DotNet Type'.
        /// </summary>
        public static string SpP_DtNt_Id_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_DtNt_Id_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'SQL Type'.
        /// </summary>
        public static string SpP_DtSql_Id {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_DtSql_Id, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'SQL Type'.
        /// </summary>
        public static string SpP_DtSql_Id_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_DtSql_Id_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Act rec'.
        /// </summary>
        public static string SpP_IsActiveRow {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_IsActiveRow, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is an active record'.
        /// </summary>
        public static string SpP_IsActiveRow_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_IsActiveRow_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Nullable'.
        /// </summary>
        public static string SpP_IsNullable {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_IsNullable, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Nullable'.
        /// </summary>
        public static string SpP_IsNullable_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_IsNullable_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Last edit'.
        /// </summary>
        public static string SpP_LastModifiedDate {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_LastModifiedDate, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Time last edit was made'.
        /// </summary>
        public static string SpP_LastModifiedDate_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_LastModifiedDate_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Name'.
        /// </summary>
        public static string SpP_Name {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_Name, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Name'.
        /// </summary>
        public static string SpP_Name_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_Name_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Sort Order'.
        /// </summary>
        public static string SpP_SortOrder {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_SortOrder, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Sort Order'.
        /// </summary>
        public static string SpP_SortOrder_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_SortOrder_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Direction'.
        /// </summary>
        public static string SpP_SpPDr_Id {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_SpPDr_Id, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Direction'.
        /// </summary>
        public static string SpP_SpPDr_Id_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_SpPDr_Id_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Value'.
        /// </summary>
        public static string SpP_Value {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_Value, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Value'.
        /// </summary>
        public static string SpP_Value_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.SpP_Value_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Email/User name'.
        /// </summary>
        public static string UserName {
            get {
                return ResourceManager.GetString(ResourceNames.UserName, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Email/User name'.
        /// </summary>
        public static string UserName_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.UserName_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Lists all the resource names as constant string fields.
        /// </summary>
        public class ResourceNames {
            
            /// <summary>
            /// Stores the resource name 'SpP_DefaultValue'.
            /// </summary>
            public const string SpP_DefaultValue = "SpP_DefaultValue";
            
            /// <summary>
            /// Stores the resource name 'SpP_DefaultValue_Vbs'.
            /// </summary>
            public const string SpP_DefaultValue_Vbs = "SpP_DefaultValue_Vbs";
            
            /// <summary>
            /// Stores the resource name 'SpP_DtNt_Id'.
            /// </summary>
            public const string SpP_DtNt_Id = "SpP_DtNt_Id";
            
            /// <summary>
            /// Stores the resource name 'SpP_DtNt_Id_Vbs'.
            /// </summary>
            public const string SpP_DtNt_Id_Vbs = "SpP_DtNt_Id_Vbs";
            
            /// <summary>
            /// Stores the resource name 'SpP_DtSql_Id'.
            /// </summary>
            public const string SpP_DtSql_Id = "SpP_DtSql_Id";
            
            /// <summary>
            /// Stores the resource name 'SpP_DtSql_Id_Vbs'.
            /// </summary>
            public const string SpP_DtSql_Id_Vbs = "SpP_DtSql_Id_Vbs";
            
            /// <summary>
            /// Stores the resource name 'SpP_IsActiveRow'.
            /// </summary>
            public const string SpP_IsActiveRow = "SpP_IsActiveRow";
            
            /// <summary>
            /// Stores the resource name 'SpP_IsActiveRow_Vbs'.
            /// </summary>
            public const string SpP_IsActiveRow_Vbs = "SpP_IsActiveRow_Vbs";
            
            /// <summary>
            /// Stores the resource name 'SpP_IsNullable'.
            /// </summary>
            public const string SpP_IsNullable = "SpP_IsNullable";
            
            /// <summary>
            /// Stores the resource name 'SpP_IsNullable_Vbs'.
            /// </summary>
            public const string SpP_IsNullable_Vbs = "SpP_IsNullable_Vbs";
            
            /// <summary>
            /// Stores the resource name 'SpP_LastModifiedDate'.
            /// </summary>
            public const string SpP_LastModifiedDate = "SpP_LastModifiedDate";
            
            /// <summary>
            /// Stores the resource name 'SpP_LastModifiedDate_Vbs'.
            /// </summary>
            public const string SpP_LastModifiedDate_Vbs = "SpP_LastModifiedDate_Vbs";
            
            /// <summary>
            /// Stores the resource name 'SpP_Name'.
            /// </summary>
            public const string SpP_Name = "SpP_Name";
            
            /// <summary>
            /// Stores the resource name 'SpP_Name_Vbs'.
            /// </summary>
            public const string SpP_Name_Vbs = "SpP_Name_Vbs";
            
            /// <summary>
            /// Stores the resource name 'SpP_SortOrder'.
            /// </summary>
            public const string SpP_SortOrder = "SpP_SortOrder";
            
            /// <summary>
            /// Stores the resource name 'SpP_SortOrder_Vbs'.
            /// </summary>
            public const string SpP_SortOrder_Vbs = "SpP_SortOrder_Vbs";
            
            /// <summary>
            /// Stores the resource name 'SpP_SpPDr_Id'.
            /// </summary>
            public const string SpP_SpPDr_Id = "SpP_SpPDr_Id";
            
            /// <summary>
            /// Stores the resource name 'SpP_SpPDr_Id_Vbs'.
            /// </summary>
            public const string SpP_SpPDr_Id_Vbs = "SpP_SpPDr_Id_Vbs";
            
            /// <summary>
            /// Stores the resource name 'SpP_Value'.
            /// </summary>
            public const string SpP_Value = "SpP_Value";
            
            /// <summary>
            /// Stores the resource name 'SpP_Value_Vbs'.
            /// </summary>
            public const string SpP_Value_Vbs = "SpP_Value_Vbs";
            
            /// <summary>
            /// Stores the resource name 'UserName'.
            /// </summary>
            public const string UserName = "UserName";
            
            /// <summary>
            /// Stores the resource name 'UserName_Vbs'.
            /// </summary>
            public const string UserName_Vbs = "UserName_Vbs";
        }
    }
}
