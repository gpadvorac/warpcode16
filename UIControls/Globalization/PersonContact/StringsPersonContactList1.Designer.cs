﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UIControls.Globalization.PersonContact {
    using System;
    
    
    /// <summary>
    /// A strongly-typed resource class, for looking up localized strings, formatting them, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilderEx class via the ResXFileCodeGeneratorEx custom tool.
    // To add or remove a member, edit your .ResX file then rerun the ResXFileCodeGeneratorEx custom tool or rebuild your VS.NET project.
    // Copyright (c) Dmytro Kryvko 2006-2017 (http://dmytro.kryvko.googlepages.com/)
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("DMKSoftware.CodeGenerators.Tools.StronglyTypedResourceBuilderEx", "2.6.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
#if !SILVERLIGHT
    [global::System.Reflection.ObfuscationAttribute(Exclude=true, ApplyToMembers=true)]
#endif
    [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
    public partial class StringsPersonContactList {
        
        private static global::System.Resources.ResourceManager _resourceManager;
        
        private static object _internalSyncObject;
        
        private static global::System.Globalization.CultureInfo _resourceCulture;
        
        /// <summary>
        /// Initializes a StringsPersonContactList object.
        /// </summary>
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public StringsPersonContactList() {
        }
        
        /// <summary>
        /// Thread safe lock object used by this class.
        /// </summary>
        public static object InternalSyncObject {
            get {
                if (object.ReferenceEquals(_internalSyncObject, null)) {
                    global::System.Threading.Interlocked.CompareExchange(ref _internalSyncObject, new object(), null);
                }
                return _internalSyncObject;
            }
        }
        
        /// <summary>
        /// Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(_resourceManager, null)) {
                    global::System.Threading.Monitor.Enter(InternalSyncObject);
                    try {
                        if (object.ReferenceEquals(_resourceManager, null)) {
                            global::System.Threading.Interlocked.Exchange(ref _resourceManager, new global::System.Resources.ResourceManager("UIControls.Globalization.PersonContact.StringsPersonContactList", typeof(StringsPersonContactList).Assembly));
                        }
                    }
                    finally {
                        global::System.Threading.Monitor.Exit(InternalSyncObject);
                    }
                }
                return _resourceManager;
            }
        }
        
        /// <summary>
        /// Overrides the current thread's CurrentUICulture property for all
        /// resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return _resourceCulture;
            }
            set {
                _resourceCulture = value;
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'EMail'.
        /// </summary>
        public static string Pn_EMail {
            get {
                return ResourceManager.GetString(ResourceNames.Pn_EMail, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'EMail'.
        /// </summary>
        public static string Pn_EMail_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Pn_EMail_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'First name'.
        /// </summary>
        public static string Pn_FName {
            get {
                return ResourceManager.GetString(ResourceNames.Pn_FName, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'First name'.
        /// </summary>
        public static string Pn_FName_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Pn_FName_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Last name'.
        /// </summary>
        public static string Pn_LName {
            get {
                return ResourceManager.GetString(ResourceNames.Pn_LName, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Last name'.
        /// </summary>
        public static string Pn_LName_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Pn_LName_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Cell'.
        /// </summary>
        public static string Pn_PhoneCell {
            get {
                return ResourceManager.GetString(ResourceNames.Pn_PhoneCell, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Cell'.
        /// </summary>
        public static string Pn_PhoneCell_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Pn_PhoneCell_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Phone other'.
        /// </summary>
        public static string Pn_PhoneOther {
            get {
                return ResourceManager.GetString(ResourceNames.Pn_PhoneOther, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Phone other'.
        /// </summary>
        public static string Pn_PhoneOther_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Pn_PhoneOther_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Lists all the resource names as constant string fields.
        /// </summary>
        public class ResourceNames {
            
            /// <summary>
            /// Stores the resource name 'Pn_EMail'.
            /// </summary>
            public const string Pn_EMail = "Pn_EMail";
            
            /// <summary>
            /// Stores the resource name 'Pn_EMail_Vbs'.
            /// </summary>
            public const string Pn_EMail_Vbs = "Pn_EMail_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Pn_FName'.
            /// </summary>
            public const string Pn_FName = "Pn_FName";
            
            /// <summary>
            /// Stores the resource name 'Pn_FName_Vbs'.
            /// </summary>
            public const string Pn_FName_Vbs = "Pn_FName_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Pn_LName'.
            /// </summary>
            public const string Pn_LName = "Pn_LName";
            
            /// <summary>
            /// Stores the resource name 'Pn_LName_Vbs'.
            /// </summary>
            public const string Pn_LName_Vbs = "Pn_LName_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Pn_PhoneCell'.
            /// </summary>
            public const string Pn_PhoneCell = "Pn_PhoneCell";
            
            /// <summary>
            /// Stores the resource name 'Pn_PhoneCell_Vbs'.
            /// </summary>
            public const string Pn_PhoneCell_Vbs = "Pn_PhoneCell_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Pn_PhoneOther'.
            /// </summary>
            public const string Pn_PhoneOther = "Pn_PhoneOther";
            
            /// <summary>
            /// Stores the resource name 'Pn_PhoneOther_Vbs'.
            /// </summary>
            public const string Pn_PhoneOther_Vbs = "Pn_PhoneOther_Vbs";
        }
    }
}
