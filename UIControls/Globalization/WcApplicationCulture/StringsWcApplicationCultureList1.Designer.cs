﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UIControls.Globalization.WcApplicationCulture {
    using System;
    
    
    /// <summary>
    /// A strongly-typed resource class, for looking up localized strings, formatting them, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilderEx class via the ResXFileCodeGeneratorEx custom tool.
    // To add or remove a member, edit your .ResX file then rerun the ResXFileCodeGeneratorEx custom tool or rebuild your VS.NET project.
    // Copyright (c) Dmytro Kryvko 2006-2017 (http://dmytro.kryvko.googlepages.com/)
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("DMKSoftware.CodeGenerators.Tools.StronglyTypedResourceBuilderEx", "2.6.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
#if !SILVERLIGHT
    [global::System.Reflection.ObfuscationAttribute(Exclude=true, ApplyToMembers=true)]
#endif
    [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
    public partial class StringsWcApplicationCultureList {
        
        private static global::System.Resources.ResourceManager _resourceManager;
        
        private static object _internalSyncObject;
        
        private static global::System.Globalization.CultureInfo _resourceCulture;
        
        /// <summary>
        /// Initializes a StringsWcApplicationCultureList object.
        /// </summary>
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public StringsWcApplicationCultureList() {
        }
        
        /// <summary>
        /// Thread safe lock object used by this class.
        /// </summary>
        public static object InternalSyncObject {
            get {
                if (object.ReferenceEquals(_internalSyncObject, null)) {
                    global::System.Threading.Interlocked.CompareExchange(ref _internalSyncObject, new object(), null);
                }
                return _internalSyncObject;
            }
        }
        
        /// <summary>
        /// Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(_resourceManager, null)) {
                    global::System.Threading.Monitor.Enter(InternalSyncObject);
                    try {
                        if (object.ReferenceEquals(_resourceManager, null)) {
                            global::System.Threading.Interlocked.Exchange(ref _resourceManager, new global::System.Resources.ResourceManager("UIControls.Globalization.WcApplicationCulture.StringsWcApplicationCultureList", typeof(StringsWcApplicationCultureList).Assembly));
                        }
                    }
                    finally {
                        global::System.Threading.Monitor.Exit(InternalSyncObject);
                    }
                }
                return _resourceManager;
            }
        }
        
        /// <summary>
        /// Overrides the current thread's CurrentUICulture property for all
        /// resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return _resourceCulture;
            }
            set {
                _resourceCulture = value;
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Culture'.
        /// </summary>
        public static string ApCltr_Cltr_Id {
            get {
                return ResourceManager.GetString(ResourceNames.ApCltr_Cltr_Id, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Culture'.
        /// </summary>
        public static string ApCltr_Cltr_Id_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.ApCltr_Cltr_Id_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Comment'.
        /// </summary>
        public static string ApCltr_Comment {
            get {
                return ResourceManager.GetString(ResourceNames.ApCltr_Comment, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Comment'.
        /// </summary>
        public static string ApCltr_Comment_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.ApCltr_Comment_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Act rec'.
        /// </summary>
        public static string ApCltr_IsActiveRow {
            get {
                return ResourceManager.GetString(ResourceNames.ApCltr_IsActiveRow, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is an active record'.
        /// </summary>
        public static string ApCltr_IsActiveRow_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.ApCltr_IsActiveRow_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Default'.
        /// </summary>
        public static string ApCltr_IsDefault {
            get {
                return ResourceManager.GetString(ResourceNames.ApCltr_IsDefault, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Default'.
        /// </summary>
        public static string ApCltr_IsDefault_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.ApCltr_IsDefault_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Last edit'.
        /// </summary>
        public static string ApCltr_LastModifiedDate {
            get {
                return ResourceManager.GetString(ResourceNames.ApCltr_LastModifiedDate, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Time last edit was made'.
        /// </summary>
        public static string ApCltr_LastModifiedDate_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.ApCltr_LastModifiedDate_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Sort Order'.
        /// </summary>
        public static string ApCltr_Sort {
            get {
                return ResourceManager.GetString(ResourceNames.ApCltr_Sort, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Sort Order'.
        /// </summary>
        public static string ApCltr_Sort_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.ApCltr_Sort_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Email/User name'.
        /// </summary>
        public static string UserName {
            get {
                return ResourceManager.GetString(ResourceNames.UserName, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Email/User name'.
        /// </summary>
        public static string UserName_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.UserName_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Lists all the resource names as constant string fields.
        /// </summary>
        public class ResourceNames {
            
            /// <summary>
            /// Stores the resource name 'ApCltr_Cltr_Id'.
            /// </summary>
            public const string ApCltr_Cltr_Id = "ApCltr_Cltr_Id";
            
            /// <summary>
            /// Stores the resource name 'ApCltr_Cltr_Id_Vbs'.
            /// </summary>
            public const string ApCltr_Cltr_Id_Vbs = "ApCltr_Cltr_Id_Vbs";
            
            /// <summary>
            /// Stores the resource name 'ApCltr_Comment'.
            /// </summary>
            public const string ApCltr_Comment = "ApCltr_Comment";
            
            /// <summary>
            /// Stores the resource name 'ApCltr_Comment_Vbs'.
            /// </summary>
            public const string ApCltr_Comment_Vbs = "ApCltr_Comment_Vbs";
            
            /// <summary>
            /// Stores the resource name 'ApCltr_IsActiveRow'.
            /// </summary>
            public const string ApCltr_IsActiveRow = "ApCltr_IsActiveRow";
            
            /// <summary>
            /// Stores the resource name 'ApCltr_IsActiveRow_Vbs'.
            /// </summary>
            public const string ApCltr_IsActiveRow_Vbs = "ApCltr_IsActiveRow_Vbs";
            
            /// <summary>
            /// Stores the resource name 'ApCltr_IsDefault'.
            /// </summary>
            public const string ApCltr_IsDefault = "ApCltr_IsDefault";
            
            /// <summary>
            /// Stores the resource name 'ApCltr_IsDefault_Vbs'.
            /// </summary>
            public const string ApCltr_IsDefault_Vbs = "ApCltr_IsDefault_Vbs";
            
            /// <summary>
            /// Stores the resource name 'ApCltr_LastModifiedDate'.
            /// </summary>
            public const string ApCltr_LastModifiedDate = "ApCltr_LastModifiedDate";
            
            /// <summary>
            /// Stores the resource name 'ApCltr_LastModifiedDate_Vbs'.
            /// </summary>
            public const string ApCltr_LastModifiedDate_Vbs = "ApCltr_LastModifiedDate_Vbs";
            
            /// <summary>
            /// Stores the resource name 'ApCltr_Sort'.
            /// </summary>
            public const string ApCltr_Sort = "ApCltr_Sort";
            
            /// <summary>
            /// Stores the resource name 'ApCltr_Sort_Vbs'.
            /// </summary>
            public const string ApCltr_Sort_Vbs = "ApCltr_Sort_Vbs";
            
            /// <summary>
            /// Stores the resource name 'UserName'.
            /// </summary>
            public const string UserName = "UserName";
            
            /// <summary>
            /// Stores the resource name 'UserName_Vbs'.
            /// </summary>
            public const string UserName_Vbs = "UserName_Vbs";
        }
    }
}
