﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UIControls.Globalization.TaskCategory {
    using System;
    
    
    /// <summary>
    /// A strongly-typed resource class, for looking up localized strings, formatting them, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilderEx class via the ResXFileCodeGeneratorEx custom tool.
    // To add or remove a member, edit your .ResX file then rerun the ResXFileCodeGeneratorEx custom tool or rebuild your VS.NET project.
    // Copyright (c) Dmytro Kryvko 2006-2017 (http://dmytro.kryvko.googlepages.com/)
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("DMKSoftware.CodeGenerators.Tools.StronglyTypedResourceBuilderEx", "2.6.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
#if !SILVERLIGHT
    [global::System.Reflection.ObfuscationAttribute(Exclude=true, ApplyToMembers=true)]
#endif
    [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
    public partial class StringsTaskCategoryProps {
        
        private static global::System.Resources.ResourceManager _resourceManager;
        
        private static object _internalSyncObject;
        
        private static global::System.Globalization.CultureInfo _resourceCulture;
        
        /// <summary>
        /// Initializes a StringsTaskCategoryProps object.
        /// </summary>
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public StringsTaskCategoryProps() {
        }
        
        /// <summary>
        /// Thread safe lock object used by this class.
        /// </summary>
        public static object InternalSyncObject {
            get {
                if (object.ReferenceEquals(_internalSyncObject, null)) {
                    global::System.Threading.Interlocked.CompareExchange(ref _internalSyncObject, new object(), null);
                }
                return _internalSyncObject;
            }
        }
        
        /// <summary>
        /// Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(_resourceManager, null)) {
                    global::System.Threading.Monitor.Enter(InternalSyncObject);
                    try {
                        if (object.ReferenceEquals(_resourceManager, null)) {
                            global::System.Threading.Interlocked.Exchange(ref _resourceManager, new global::System.Resources.ResourceManager("UIControls.Globalization.TaskCategory.StringsTaskCategoryProps", typeof(StringsTaskCategoryProps).Assembly));
                        }
                    }
                    finally {
                        global::System.Threading.Monitor.Exit(InternalSyncObject);
                    }
                }
                return _resourceManager;
            }
        }
        
        /// <summary>
        /// Overrides the current thread's CurrentUICulture property for all
        /// resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return _resourceCulture;
            }
            set {
                _resourceCulture = value;
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Description'.
        /// </summary>
        public static string TkCt_Desc {
            get {
                return ResourceManager.GetString(ResourceNames.TkCt_Desc, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Description'.
        /// </summary>
        public static string TkCt_Desc_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.TkCt_Desc_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Active record'.
        /// </summary>
        public static string TkCt_IsActiveRow {
            get {
                return ResourceManager.GetString(ResourceNames.TkCt_IsActiveRow, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is an active record'.
        /// </summary>
        public static string TkCt_IsActiveRow_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.TkCt_IsActiveRow_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Name'.
        /// </summary>
        public static string TkCt_Name {
            get {
                return ResourceManager.GetString(ResourceNames.TkCt_Name, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Name'.
        /// </summary>
        public static string TkCt_Name_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.TkCt_Name_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Lists all the resource names as constant string fields.
        /// </summary>
        public class ResourceNames {
            
            /// <summary>
            /// Stores the resource name 'TkCt_Desc'.
            /// </summary>
            public const string TkCt_Desc = "TkCt_Desc";
            
            /// <summary>
            /// Stores the resource name 'TkCt_Desc_Vbs'.
            /// </summary>
            public const string TkCt_Desc_Vbs = "TkCt_Desc_Vbs";
            
            /// <summary>
            /// Stores the resource name 'TkCt_IsActiveRow'.
            /// </summary>
            public const string TkCt_IsActiveRow = "TkCt_IsActiveRow";
            
            /// <summary>
            /// Stores the resource name 'TkCt_IsActiveRow_Vbs'.
            /// </summary>
            public const string TkCt_IsActiveRow_Vbs = "TkCt_IsActiveRow_Vbs";
            
            /// <summary>
            /// Stores the resource name 'TkCt_Name'.
            /// </summary>
            public const string TkCt_Name = "TkCt_Name";
            
            /// <summary>
            /// Stores the resource name 'TkCt_Name_Vbs'.
            /// </summary>
            public const string TkCt_Name_Vbs = "TkCt_Name_Vbs";
        }
    }
}
