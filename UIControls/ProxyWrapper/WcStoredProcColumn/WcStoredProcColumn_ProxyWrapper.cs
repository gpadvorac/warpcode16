using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/26/2017 1:21:55 PM

namespace ProxyWrapper
{
    public partial class WcStoredProcColumnService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcStoredProcColumn_GetByIdCompletedEventArgs> WcStoredProcColumn_GetByIdCompleted;
        public event System.EventHandler<WcStoredProcColumn_GetAllCompletedEventArgs> WcStoredProcColumn_GetAllCompleted;
        public event System.EventHandler<WcStoredProcColumn_GetListByFKCompletedEventArgs> WcStoredProcColumn_GetListByFKCompleted;
        public event System.EventHandler<WcStoredProcColumn_SaveCompletedEventArgs> WcStoredProcColumn_SaveCompleted;
        public event System.EventHandler<WcStoredProcColumn_DeleteCompletedEventArgs> WcStoredProcColumn_DeleteCompleted;
        public event System.EventHandler<WcStoredProcColumn_SetIsDeletedCompletedEventArgs> WcStoredProcColumn_SetIsDeletedCompleted;
        public event System.EventHandler<WcStoredProcColumn_DeactivateCompletedEventArgs> WcStoredProcColumn_DeactivateCompleted;
        public event System.EventHandler<WcStoredProcColumn_RemoveCompletedEventArgs> WcStoredProcColumn_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcStoredProcColumn_GetById

        public void Begin_WcStoredProcColumn_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_GetById", IfxTraceCategory.Enter);
                WcStoredProcColumnServiceClient proxy = new WcStoredProcColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcColumn_GetByIdCompleted += new EventHandler<WcStoredProcColumn_GetByIdCompletedEventArgs>(proxy_WcStoredProcColumn_GetByIdCompleted);
                proxy.WcStoredProcColumn_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcColumn_GetByIdCompleted(object sender, WcStoredProcColumn_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcColumn_GetByIdCompletedEventArgs> handler = WcStoredProcColumn_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcColumn_GetById

        #region WcStoredProcColumn_GetAll

        public void Begin_WcStoredProcColumn_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_GetAll", IfxTraceCategory.Enter);
                WcStoredProcColumnServiceClient proxy = new WcStoredProcColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcColumn_GetAllCompleted += new EventHandler<WcStoredProcColumn_GetAllCompletedEventArgs>(proxy_WcStoredProcColumn_GetAllCompleted);
                proxy.WcStoredProcColumn_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcColumn_GetAllCompleted(object sender, WcStoredProcColumn_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcColumn_GetAllCompletedEventArgs> handler = WcStoredProcColumn_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcColumn_GetAll

        #region WcStoredProcColumn_GetListByFK

        public void Begin_WcStoredProcColumn_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_GetListByFK", IfxTraceCategory.Enter);
                WcStoredProcColumnServiceClient proxy = new WcStoredProcColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcColumn_GetListByFKCompleted += new EventHandler<WcStoredProcColumn_GetListByFKCompletedEventArgs>(proxy_WcStoredProcColumn_GetListByFKCompleted);
                proxy.WcStoredProcColumn_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcColumn_GetListByFKCompleted(object sender, WcStoredProcColumn_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcColumn_GetListByFKCompletedEventArgs> handler = WcStoredProcColumn_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcColumn_GetListByFK

        #region WcStoredProcColumn_Save

        public void Begin_WcStoredProcColumn_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_Save", IfxTraceCategory.Enter);
                WcStoredProcColumnServiceClient proxy = new WcStoredProcColumnServiceClient();
                AssignCredentials(proxy);
                //proxy.WcStoredProcColumn_SaveCompleted += new EventHandler<WcStoredProcColumn_SaveCompletedEventArgs>(proxy_WcStoredProcColumn_SaveCompleted);
                proxy.WcStoredProcColumn_SaveCompleted += new EventHandler<WcStoredProcColumn_SaveCompletedEventArgs>(proxy_WcStoredProcColumn_SaveCompleted);
                proxy.WcStoredProcColumn_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcColumn_SaveCompleted(object sender, WcStoredProcColumn_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcColumn_SaveCompletedEventArgs> handler = WcStoredProcColumn_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcColumn_Save

        #region WcStoredProcColumn_Delete

        public void Begin_WcStoredProcColumn_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_DeleteCompleted", IfxTraceCategory.Enter);
                WcStoredProcColumnServiceClient proxy = new WcStoredProcColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcColumn_DeleteCompleted += new EventHandler<WcStoredProcColumn_DeleteCompletedEventArgs>(proxy_WcStoredProcColumn_DeleteCompleted);
                proxy.WcStoredProcColumn_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcColumn_DeleteCompleted(object sender, WcStoredProcColumn_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcColumn_DeleteCompletedEventArgs> handler = WcStoredProcColumn_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcColumn_Delete

        #region WcStoredProcColumn_SetIsDeleted

        public void Begin_WcStoredProcColumn_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcStoredProcColumnServiceClient proxy = new WcStoredProcColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcColumn_SetIsDeletedCompleted += new EventHandler<WcStoredProcColumn_SetIsDeletedCompletedEventArgs>(proxy_WcStoredProcColumn_SetIsDeletedCompleted);
                proxy.WcStoredProcColumn_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcColumn_SetIsDeletedCompleted(object sender, WcStoredProcColumn_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcColumn_SetIsDeletedCompletedEventArgs> handler = WcStoredProcColumn_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcColumn_SetIsDeleted

        #region WcStoredProcColumn_Deactivate

        public void Begin_WcStoredProcColumn_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_Deactivate", IfxTraceCategory.Enter);
            WcStoredProcColumnServiceClient proxy = new WcStoredProcColumnServiceClient();
            AssignCredentials(proxy);
            proxy.WcStoredProcColumn_DeactivateCompleted += new EventHandler<WcStoredProcColumn_DeactivateCompletedEventArgs>(proxy_WcStoredProcColumn_DeactivateCompleted);
            proxy.WcStoredProcColumn_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcColumn_DeactivateCompleted(object sender, WcStoredProcColumn_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcColumn_DeactivateCompletedEventArgs> handler = WcStoredProcColumn_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcColumn_Deactivate

        #region WcStoredProcColumn_Remove

        public void Begin_WcStoredProcColumn_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_Remove", IfxTraceCategory.Enter);
                WcStoredProcColumnServiceClient proxy = new WcStoredProcColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcColumn_RemoveCompleted += new EventHandler<WcStoredProcColumn_RemoveCompletedEventArgs>(proxy_WcStoredProcColumn_RemoveCompleted);
                proxy.WcStoredProcColumn_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcColumn_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcColumn_RemoveCompleted(object sender, WcStoredProcColumn_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcColumn_RemoveCompletedEventArgs> handler = WcStoredProcColumn_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcStoredProcColumn_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


