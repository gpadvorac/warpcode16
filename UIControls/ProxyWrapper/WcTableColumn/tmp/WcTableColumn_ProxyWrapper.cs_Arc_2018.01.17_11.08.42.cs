using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  1/17/2018 11:05:59 AM

namespace ProxyWrapper
{
    public partial class WcTableColumnService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcTableColumn_GetByIdCompletedEventArgs> WcTableColumn_GetByIdCompleted;
        public event System.EventHandler<WcTableColumn_GetAllCompletedEventArgs> WcTableColumn_GetAllCompleted;
        public event System.EventHandler<WcTableColumn_GetListByFKCompletedEventArgs> WcTableColumn_GetListByFKCompleted;
        public event System.EventHandler<WcTableColumn_SaveCompletedEventArgs> WcTableColumn_SaveCompleted;
        public event System.EventHandler<WcTableColumn_DeleteCompletedEventArgs> WcTableColumn_DeleteCompleted;
        public event System.EventHandler<WcTableColumn_SetIsDeletedCompletedEventArgs> WcTableColumn_SetIsDeletedCompleted;
        public event System.EventHandler<WcTableColumn_DeactivateCompletedEventArgs> WcTableColumn_DeactivateCompleted;
        public event System.EventHandler<WcTableColumn_RemoveCompletedEventArgs> WcTableColumn_RemoveCompleted;
		public event System.EventHandler<ExecutewcTableColumnGroup_AssignColumnsCompletedEventArgs> ExecutewcTableColumnGroup_AssignColumnsCompleted;
		public event System.EventHandler<GetwcTableColumnGroup_AssignmentsCompletedEventArgs> GetwcTableColumnGroup_AssignmentsCompleted;
		public event System.EventHandler<GetWcTableColumn_lstNamesFromAppAndDBCompletedEventArgs> GetWcTableColumn_lstNamesFromAppAndDBCompleted;
		public event System.EventHandler<ExecuteWcTableColumn_InsertListOfColumnsFromDbCompletedEventArgs> ExecuteWcTableColumn_InsertListOfColumnsFromDbCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcTableColumn_GetById

        public void Begin_WcTableColumn_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetById", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumn_GetByIdCompleted += new EventHandler<WcTableColumn_GetByIdCompletedEventArgs>(proxy_WcTableColumn_GetByIdCompleted);
                proxy.WcTableColumn_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_GetByIdCompleted(object sender, WcTableColumn_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_GetByIdCompletedEventArgs> handler = WcTableColumn_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_GetById

        #region WcTableColumn_GetAll

        public void Begin_WcTableColumn_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetAll", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumn_GetAllCompleted += new EventHandler<WcTableColumn_GetAllCompletedEventArgs>(proxy_WcTableColumn_GetAllCompleted);
                proxy.WcTableColumn_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_GetAllCompleted(object sender, WcTableColumn_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_GetAllCompletedEventArgs> handler = WcTableColumn_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_GetAll

        #region WcTableColumn_GetListByFK

        public void Begin_WcTableColumn_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetListByFK", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumn_GetListByFKCompleted += new EventHandler<WcTableColumn_GetListByFKCompletedEventArgs>(proxy_WcTableColumn_GetListByFKCompleted);
                proxy.WcTableColumn_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_GetListByFKCompleted(object sender, WcTableColumn_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_GetListByFKCompletedEventArgs> handler = WcTableColumn_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_GetListByFK

        #region WcTableColumn_Save

        public void Begin_WcTableColumn_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Save", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                //proxy.WcTableColumn_SaveCompleted += new EventHandler<WcTableColumn_SaveCompletedEventArgs>(proxy_WcTableColumn_SaveCompleted);
                proxy.WcTableColumn_SaveCompleted += new EventHandler<WcTableColumn_SaveCompletedEventArgs>(proxy_WcTableColumn_SaveCompleted);
                proxy.WcTableColumn_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_SaveCompleted(object sender, WcTableColumn_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_SaveCompletedEventArgs> handler = WcTableColumn_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_Save

        #region WcTableColumn_Delete

        public void Begin_WcTableColumn_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_DeleteCompleted", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumn_DeleteCompleted += new EventHandler<WcTableColumn_DeleteCompletedEventArgs>(proxy_WcTableColumn_DeleteCompleted);
                proxy.WcTableColumn_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_DeleteCompleted(object sender, WcTableColumn_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_DeleteCompletedEventArgs> handler = WcTableColumn_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_Delete

        #region WcTableColumn_SetIsDeleted

        public void Begin_WcTableColumn_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumn_SetIsDeletedCompleted += new EventHandler<WcTableColumn_SetIsDeletedCompletedEventArgs>(proxy_WcTableColumn_SetIsDeletedCompleted);
                proxy.WcTableColumn_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_SetIsDeletedCompleted(object sender, WcTableColumn_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_SetIsDeletedCompletedEventArgs> handler = WcTableColumn_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_SetIsDeleted

        #region WcTableColumn_Deactivate

        public void Begin_WcTableColumn_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Deactivate", IfxTraceCategory.Enter);
            WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
            AssignCredentials(proxy);
            proxy.WcTableColumn_DeactivateCompleted += new EventHandler<WcTableColumn_DeactivateCompletedEventArgs>(proxy_WcTableColumn_DeactivateCompleted);
            proxy.WcTableColumn_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_DeactivateCompleted(object sender, WcTableColumn_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_DeactivateCompletedEventArgs> handler = WcTableColumn_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_Deactivate

        #region WcTableColumn_Remove

        public void Begin_WcTableColumn_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Remove", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumn_RemoveCompleted += new EventHandler<WcTableColumn_RemoveCompletedEventArgs>(proxy_WcTableColumn_RemoveCompleted);
                proxy.WcTableColumn_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_RemoveCompleted(object sender, WcTableColumn_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_RemoveCompletedEventArgs> handler = WcTableColumn_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcTableColumn_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #region ExecutewcTableColumnGroup_AssignColumns

        public void Begin_ExecutewcTableColumnGroup_AssignColumns(Boolean Insert, Guid TbCGrp2TbC_TbCGrp_Id, Guid TbCGrp2TbC_TbC_Id, Guid TbCGrp2TbC_CreatedUserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecutewcTableColumnGroup_AssignColumns", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.ExecutewcTableColumnGroup_AssignColumnsCompleted += new EventHandler<ExecutewcTableColumnGroup_AssignColumnsCompletedEventArgs>(proxy_ExecutewcTableColumnGroup_AssignColumnsCompleted);
                proxy.ExecutewcTableColumnGroup_AssignColumnsAsync(Insert, TbCGrp2TbC_TbCGrp_Id, TbCGrp2TbC_TbC_Id, TbCGrp2TbC_CreatedUserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecutewcTableColumnGroup_AssignColumns", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecutewcTableColumnGroup_AssignColumns", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecutewcTableColumnGroup_AssignColumnsCompleted(object sender, ExecutewcTableColumnGroup_AssignColumnsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableColumnGroup_AssignColumns", IfxTraceCategory.Enter);
                System.EventHandler<ExecutewcTableColumnGroup_AssignColumnsCompletedEventArgs> handler = ExecutewcTableColumnGroup_AssignColumnsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableColumnGroup_AssignColumns", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableColumnGroup_AssignColumns", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecutewcTableColumnGroup_AssignColumns

        #region GetwcTableColumnGroup_Assignments

        public void Begin_GetwcTableColumnGroup_Assignments(Guid Tb_Id, Guid TbC_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetwcTableColumnGroup_Assignments", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.GetwcTableColumnGroup_AssignmentsCompleted += new EventHandler<GetwcTableColumnGroup_AssignmentsCompletedEventArgs>(proxy_GetwcTableColumnGroup_AssignmentsCompleted);
                proxy.GetwcTableColumnGroup_AssignmentsAsync(Tb_Id, TbC_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetwcTableColumnGroup_Assignments", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetwcTableColumnGroup_Assignments", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetwcTableColumnGroup_AssignmentsCompleted(object sender, GetwcTableColumnGroup_AssignmentsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableColumnGroup_Assignments", IfxTraceCategory.Enter);
                System.EventHandler<GetwcTableColumnGroup_AssignmentsCompletedEventArgs> handler = GetwcTableColumnGroup_AssignmentsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableColumnGroup_Assignments", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableColumnGroup_Assignments", IfxTraceCategory.Leave);
            }
        }

        #endregion GetwcTableColumnGroup_Assignments

        #region GetWcTableColumn_lstNamesFromAppAndDB

        public void Begin_GetWcTableColumn_lstNamesFromAppAndDB(Guid Tb_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumn_lstNamesFromAppAndDB", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcTableColumn_lstNamesFromAppAndDBCompleted += new EventHandler<GetWcTableColumn_lstNamesFromAppAndDBCompletedEventArgs>(proxy_GetWcTableColumn_lstNamesFromAppAndDBCompleted);
                proxy.GetWcTableColumn_lstNamesFromAppAndDBAsync(Tb_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumn_lstNamesFromAppAndDB", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumn_lstNamesFromAppAndDB", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcTableColumn_lstNamesFromAppAndDBCompleted(object sender, GetWcTableColumn_lstNamesFromAppAndDBCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstNamesFromAppAndDB", IfxTraceCategory.Enter);
                System.EventHandler<GetWcTableColumn_lstNamesFromAppAndDBCompletedEventArgs> handler = GetWcTableColumn_lstNamesFromAppAndDBCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstNamesFromAppAndDB", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstNamesFromAppAndDB", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcTableColumn_lstNamesFromAppAndDB

        #region ExecuteWcTableColumn_InsertListOfColumnsFromDb

        public void Begin_ExecuteWcTableColumn_InsertListOfColumnsFromDb(Guid Tb_Id, Guid? TbC_Id, String ColumnName, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcTableColumn_InsertListOfColumnsFromDb", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.ExecuteWcTableColumn_InsertListOfColumnsFromDbCompleted += new EventHandler<ExecuteWcTableColumn_InsertListOfColumnsFromDbCompletedEventArgs>(proxy_ExecuteWcTableColumn_InsertListOfColumnsFromDbCompleted);
                proxy.ExecuteWcTableColumn_InsertListOfColumnsFromDbAsync(Tb_Id, TbC_Id, ColumnName, UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcTableColumn_InsertListOfColumnsFromDb", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcTableColumn_InsertListOfColumnsFromDb", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecuteWcTableColumn_InsertListOfColumnsFromDbCompleted(object sender, ExecuteWcTableColumn_InsertListOfColumnsFromDbCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_InsertListOfColumnsFromDb", IfxTraceCategory.Enter);
                System.EventHandler<ExecuteWcTableColumn_InsertListOfColumnsFromDbCompletedEventArgs> handler = ExecuteWcTableColumn_InsertListOfColumnsFromDbCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_InsertListOfColumnsFromDb", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_InsertListOfColumnsFromDb", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecuteWcTableColumn_InsertListOfColumnsFromDb

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


