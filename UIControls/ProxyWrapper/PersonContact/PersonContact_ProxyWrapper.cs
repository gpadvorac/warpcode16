using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  5/7/2015 3:07:47 PM

namespace ProxyWrapper
{
    public partial class PersonContactService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<PersonContact_GetByIdCompletedEventArgs> PersonContact_GetByIdCompleted;
        public event System.EventHandler<PersonContact_GetAllCompletedEventArgs> PersonContact_GetAllCompleted;
        public event System.EventHandler<PersonContact_GetListByFKCompletedEventArgs> PersonContact_GetListByFKCompleted;
        public event System.EventHandler<PersonContact_SaveCompletedEventArgs> PersonContact_SaveCompleted;
        public event System.EventHandler<PersonContact_DeleteCompletedEventArgs> PersonContact_DeleteCompleted;
        public event System.EventHandler<PersonContact_SetIsDeletedCompletedEventArgs> PersonContact_SetIsDeletedCompleted;
        public event System.EventHandler<PersonContact_DeactivateCompletedEventArgs> PersonContact_DeactivateCompleted;
        public event System.EventHandler<PersonContact_RemoveCompletedEventArgs> PersonContact_RemoveCompleted;

        public event System.EventHandler<GetPersonContact_ReadOnlyStaticListsCompletedEventArgs> GetPersonContact_ReadOnlyStaticListsCompleted;
		public event System.EventHandler<GetPerson_LU_ComboItemListCompletedEventArgs> GetPerson_LU_ComboItemListCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region PersonContact_GetById

        public void Begin_PersonContact_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_GetById", IfxTraceCategory.Enter);
                PersonContactServiceClient proxy = new PersonContactServiceClient();
                AssignCredentials(proxy);
                proxy.PersonContact_GetByIdCompleted += new EventHandler<PersonContact_GetByIdCompletedEventArgs>(proxy_PersonContact_GetByIdCompleted);
                proxy.PersonContact_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_GetById", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_PersonContact_GetByIdCompleted(object sender, PersonContact_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<PersonContact_GetByIdCompletedEventArgs> handler = PersonContact_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion PersonContact_GetById

        #region PersonContact_GetAll

        public void Begin_PersonContact_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_GetAll", IfxTraceCategory.Enter);
                PersonContactServiceClient proxy = new PersonContactServiceClient();
                AssignCredentials(proxy);
                proxy.PersonContact_GetAllCompleted += new EventHandler<PersonContact_GetAllCompletedEventArgs>(proxy_PersonContact_GetAllCompleted);
                proxy.PersonContact_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_GetAll", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_PersonContact_GetAllCompleted(object sender, PersonContact_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<PersonContact_GetAllCompletedEventArgs> handler = PersonContact_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion PersonContact_GetAll

        #region PersonContact_GetListByFK

        public void Begin_PersonContact_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_GetListByFK", IfxTraceCategory.Enter);
                PersonContactServiceClient proxy = new PersonContactServiceClient();
                AssignCredentials(proxy);
                proxy.PersonContact_GetListByFKCompleted += new EventHandler<PersonContact_GetListByFKCompletedEventArgs>(proxy_PersonContact_GetListByFKCompleted);
                proxy.PersonContact_GetListByFKAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_PersonContact_GetListByFKCompleted(object sender, PersonContact_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<PersonContact_GetListByFKCompletedEventArgs> handler = PersonContact_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion PersonContact_GetListByFK

        #region PersonContact_Save

        public void Begin_PersonContact_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_Save", IfxTraceCategory.Enter);
                PersonContactServiceClient proxy = new PersonContactServiceClient();
                AssignCredentials(proxy);
                //proxy.PersonContact_SaveCompleted += new EventHandler<PersonContact_SaveCompletedEventArgs>(proxy_PersonContact_SaveCompleted);
                proxy.PersonContact_SaveCompleted += new EventHandler<PersonContact_SaveCompletedEventArgs>(proxy_PersonContact_SaveCompleted);
                proxy.PersonContact_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_Save", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_PersonContact_SaveCompleted(object sender, PersonContact_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<PersonContact_SaveCompletedEventArgs> handler = PersonContact_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion PersonContact_Save

        #region PersonContact_Delete

        public void Begin_PersonContact_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_DeleteCompleted", IfxTraceCategory.Enter);
                PersonContactServiceClient proxy = new PersonContactServiceClient();
                AssignCredentials(proxy);
                proxy.PersonContact_DeleteCompleted += new EventHandler<PersonContact_DeleteCompletedEventArgs>(proxy_PersonContact_DeleteCompleted);
                proxy.PersonContact_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_PersonContact_DeleteCompleted(object sender, PersonContact_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<PersonContact_DeleteCompletedEventArgs> handler = PersonContact_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion PersonContact_Delete

        #region PersonContact_SetIsDeleted

        public void Begin_PersonContact_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                PersonContactServiceClient proxy = new PersonContactServiceClient();
                AssignCredentials(proxy);
                proxy.PersonContact_SetIsDeletedCompleted += new EventHandler<PersonContact_SetIsDeletedCompletedEventArgs>(proxy_PersonContact_SetIsDeletedCompleted);
                proxy.PersonContact_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_PersonContact_SetIsDeletedCompleted(object sender, PersonContact_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<PersonContact_SetIsDeletedCompletedEventArgs> handler = PersonContact_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion PersonContact_SetIsDeleted

        #region PersonContact_Deactivate

        public void Begin_PersonContact_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_Deactivate", IfxTraceCategory.Enter);
            PersonContactServiceClient proxy = new PersonContactServiceClient();
            AssignCredentials(proxy);
            proxy.PersonContact_DeactivateCompleted += new EventHandler<PersonContact_DeactivateCompletedEventArgs>(proxy_PersonContact_DeactivateCompleted);
            proxy.PersonContact_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_Deactivate", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_PersonContact_DeactivateCompleted(object sender, PersonContact_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<PersonContact_DeactivateCompletedEventArgs> handler = PersonContact_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion PersonContact_Deactivate

        #region PersonContact_Remove

        public void Begin_PersonContact_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_Remove", IfxTraceCategory.Enter);
                PersonContactServiceClient proxy = new PersonContactServiceClient();
                AssignCredentials(proxy);
                proxy.PersonContact_RemoveCompleted += new EventHandler<PersonContact_RemoveCompletedEventArgs>(proxy_PersonContact_RemoveCompleted);
                proxy.PersonContact_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_Remove", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_PersonContact_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_PersonContact_RemoveCompleted(object sender, PersonContact_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<PersonContact_RemoveCompletedEventArgs> handler = PersonContact_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion PersonContact_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists

        public void Begin_GetPersonContact_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPersonContact_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                PersonContactServiceClient proxy = new PersonContactServiceClient();
                AssignCredentials(proxy);
                proxy.GetPersonContact_ReadOnlyStaticListsCompleted += new EventHandler<GetPersonContact_ReadOnlyStaticListsCompletedEventArgs>(proxy_GetPersonContact_ReadOnlyStaticListsCompleted);
                proxy.GetPersonContact_ReadOnlyStaticListsAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPersonContact_ReadOnlyStaticLists", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPersonContact_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetPersonContact_ReadOnlyStaticListsCompleted(object sender, GetPersonContact_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetPersonContact_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                System.EventHandler<GetPersonContact_ReadOnlyStaticListsCompletedEventArgs> handler = GetPersonContact_ReadOnlyStaticListsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetPersonContact_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetPersonContact_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion ReadOnlyStaticLists

        #region GetPerson_LU_ComboItemList

        public void Begin_GetPerson_LU_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_LU_ComboItemList", IfxTraceCategory.Enter);
                PersonContactServiceClient proxy = new PersonContactServiceClient();
                AssignCredentials(proxy);
                proxy.GetPerson_LU_ComboItemListCompleted += new EventHandler<GetPerson_LU_ComboItemListCompletedEventArgs>(proxy_GetPerson_LU_ComboItemListCompleted);
                proxy.GetPerson_LU_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_LU_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_LU_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetPerson_LU_ComboItemListCompleted(object sender, GetPerson_LU_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_LU_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetPerson_LU_ComboItemListCompletedEventArgs> handler = GetPerson_LU_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_LU_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_LU_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetPerson_LU_ComboItemList

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


