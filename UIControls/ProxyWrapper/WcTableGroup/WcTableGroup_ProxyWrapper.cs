using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/14/2017 10:08:51 PM

namespace ProxyWrapper
{
    public partial class WcTableGroupService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcTableGroup_GetByIdCompletedEventArgs> WcTableGroup_GetByIdCompleted;
        public event System.EventHandler<WcTableGroup_GetAllCompletedEventArgs> WcTableGroup_GetAllCompleted;
        public event System.EventHandler<WcTableGroup_GetListByFKCompletedEventArgs> WcTableGroup_GetListByFKCompleted;
        public event System.EventHandler<WcTableGroup_SaveCompletedEventArgs> WcTableGroup_SaveCompleted;
        public event System.EventHandler<WcTableGroup_DeleteCompletedEventArgs> WcTableGroup_DeleteCompleted;
        public event System.EventHandler<WcTableGroup_SetIsDeletedCompletedEventArgs> WcTableGroup_SetIsDeletedCompleted;
        public event System.EventHandler<WcTableGroup_DeactivateCompletedEventArgs> WcTableGroup_DeactivateCompleted;
        public event System.EventHandler<WcTableGroup_RemoveCompletedEventArgs> WcTableGroup_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcTableGroup_GetById

        public void Begin_WcTableGroup_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_GetById", IfxTraceCategory.Enter);
                WcTableGroupServiceClient proxy = new WcTableGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableGroup_GetByIdCompleted += new EventHandler<WcTableGroup_GetByIdCompletedEventArgs>(proxy_WcTableGroup_GetByIdCompleted);
                proxy.WcTableGroup_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableGroup_GetByIdCompleted(object sender, WcTableGroup_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableGroup_GetByIdCompletedEventArgs> handler = WcTableGroup_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableGroup_GetById

        #region WcTableGroup_GetAll

        public void Begin_WcTableGroup_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_GetAll", IfxTraceCategory.Enter);
                WcTableGroupServiceClient proxy = new WcTableGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableGroup_GetAllCompleted += new EventHandler<WcTableGroup_GetAllCompletedEventArgs>(proxy_WcTableGroup_GetAllCompleted);
                proxy.WcTableGroup_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableGroup_GetAllCompleted(object sender, WcTableGroup_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableGroup_GetAllCompletedEventArgs> handler = WcTableGroup_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableGroup_GetAll

        #region WcTableGroup_GetListByFK

        public void Begin_WcTableGroup_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_GetListByFK", IfxTraceCategory.Enter);
                WcTableGroupServiceClient proxy = new WcTableGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableGroup_GetListByFKCompleted += new EventHandler<WcTableGroup_GetListByFKCompletedEventArgs>(proxy_WcTableGroup_GetListByFKCompleted);
                proxy.WcTableGroup_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableGroup_GetListByFKCompleted(object sender, WcTableGroup_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableGroup_GetListByFKCompletedEventArgs> handler = WcTableGroup_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableGroup_GetListByFK

        #region WcTableGroup_Save

        public void Begin_WcTableGroup_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_Save", IfxTraceCategory.Enter);
                WcTableGroupServiceClient proxy = new WcTableGroupServiceClient();
                AssignCredentials(proxy);
                //proxy.WcTableGroup_SaveCompleted += new EventHandler<WcTableGroup_SaveCompletedEventArgs>(proxy_WcTableGroup_SaveCompleted);
                proxy.WcTableGroup_SaveCompleted += new EventHandler<WcTableGroup_SaveCompletedEventArgs>(proxy_WcTableGroup_SaveCompleted);
                proxy.WcTableGroup_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableGroup_SaveCompleted(object sender, WcTableGroup_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableGroup_SaveCompletedEventArgs> handler = WcTableGroup_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableGroup_Save

        #region WcTableGroup_Delete

        public void Begin_WcTableGroup_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_DeleteCompleted", IfxTraceCategory.Enter);
                WcTableGroupServiceClient proxy = new WcTableGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableGroup_DeleteCompleted += new EventHandler<WcTableGroup_DeleteCompletedEventArgs>(proxy_WcTableGroup_DeleteCompleted);
                proxy.WcTableGroup_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableGroup_DeleteCompleted(object sender, WcTableGroup_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableGroup_DeleteCompletedEventArgs> handler = WcTableGroup_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableGroup_Delete

        #region WcTableGroup_SetIsDeleted

        public void Begin_WcTableGroup_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcTableGroupServiceClient proxy = new WcTableGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableGroup_SetIsDeletedCompleted += new EventHandler<WcTableGroup_SetIsDeletedCompletedEventArgs>(proxy_WcTableGroup_SetIsDeletedCompleted);
                proxy.WcTableGroup_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableGroup_SetIsDeletedCompleted(object sender, WcTableGroup_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableGroup_SetIsDeletedCompletedEventArgs> handler = WcTableGroup_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableGroup_SetIsDeleted

        #region WcTableGroup_Deactivate

        public void Begin_WcTableGroup_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_Deactivate", IfxTraceCategory.Enter);
            WcTableGroupServiceClient proxy = new WcTableGroupServiceClient();
            AssignCredentials(proxy);
            proxy.WcTableGroup_DeactivateCompleted += new EventHandler<WcTableGroup_DeactivateCompletedEventArgs>(proxy_WcTableGroup_DeactivateCompleted);
            proxy.WcTableGroup_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableGroup_DeactivateCompleted(object sender, WcTableGroup_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableGroup_DeactivateCompletedEventArgs> handler = WcTableGroup_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableGroup_Deactivate

        #region WcTableGroup_Remove

        public void Begin_WcTableGroup_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_Remove", IfxTraceCategory.Enter);
                WcTableGroupServiceClient proxy = new WcTableGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableGroup_RemoveCompleted += new EventHandler<WcTableGroup_RemoveCompletedEventArgs>(proxy_WcTableGroup_RemoveCompleted);
                proxy.WcTableGroup_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableGroup_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableGroup_RemoveCompleted(object sender, WcTableGroup_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableGroup_RemoveCompletedEventArgs> handler = WcTableGroup_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableGroup_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcTableGroup_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


