using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/26/2017 1:16:03 PM

namespace ProxyWrapper
{
    public partial class WcStoredProcParamService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcStoredProcParam_GetByIdCompletedEventArgs> WcStoredProcParam_GetByIdCompleted;
        public event System.EventHandler<WcStoredProcParam_GetAllCompletedEventArgs> WcStoredProcParam_GetAllCompleted;
        public event System.EventHandler<WcStoredProcParam_GetListByFKCompletedEventArgs> WcStoredProcParam_GetListByFKCompleted;
        public event System.EventHandler<WcStoredProcParam_SaveCompletedEventArgs> WcStoredProcParam_SaveCompleted;
        public event System.EventHandler<WcStoredProcParam_DeleteCompletedEventArgs> WcStoredProcParam_DeleteCompleted;
        public event System.EventHandler<WcStoredProcParam_SetIsDeletedCompletedEventArgs> WcStoredProcParam_SetIsDeletedCompleted;
        public event System.EventHandler<WcStoredProcParam_DeactivateCompletedEventArgs> WcStoredProcParam_DeactivateCompleted;
        public event System.EventHandler<WcStoredProcParam_RemoveCompletedEventArgs> WcStoredProcParam_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcStoredProcParam_GetById

        public void Begin_WcStoredProcParam_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_GetById", IfxTraceCategory.Enter);
                WcStoredProcParamServiceClient proxy = new WcStoredProcParamServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParam_GetByIdCompleted += new EventHandler<WcStoredProcParam_GetByIdCompletedEventArgs>(proxy_WcStoredProcParam_GetByIdCompleted);
                proxy.WcStoredProcParam_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParam_GetByIdCompleted(object sender, WcStoredProcParam_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParam_GetByIdCompletedEventArgs> handler = WcStoredProcParam_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParam_GetById

        #region WcStoredProcParam_GetAll

        public void Begin_WcStoredProcParam_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_GetAll", IfxTraceCategory.Enter);
                WcStoredProcParamServiceClient proxy = new WcStoredProcParamServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParam_GetAllCompleted += new EventHandler<WcStoredProcParam_GetAllCompletedEventArgs>(proxy_WcStoredProcParam_GetAllCompleted);
                proxy.WcStoredProcParam_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParam_GetAllCompleted(object sender, WcStoredProcParam_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParam_GetAllCompletedEventArgs> handler = WcStoredProcParam_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParam_GetAll

        #region WcStoredProcParam_GetListByFK

        public void Begin_WcStoredProcParam_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_GetListByFK", IfxTraceCategory.Enter);
                WcStoredProcParamServiceClient proxy = new WcStoredProcParamServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParam_GetListByFKCompleted += new EventHandler<WcStoredProcParam_GetListByFKCompletedEventArgs>(proxy_WcStoredProcParam_GetListByFKCompleted);
                proxy.WcStoredProcParam_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParam_GetListByFKCompleted(object sender, WcStoredProcParam_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParam_GetListByFKCompletedEventArgs> handler = WcStoredProcParam_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParam_GetListByFK

        #region WcStoredProcParam_Save

        public void Begin_WcStoredProcParam_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_Save", IfxTraceCategory.Enter);
                WcStoredProcParamServiceClient proxy = new WcStoredProcParamServiceClient();
                AssignCredentials(proxy);
                //proxy.WcStoredProcParam_SaveCompleted += new EventHandler<WcStoredProcParam_SaveCompletedEventArgs>(proxy_WcStoredProcParam_SaveCompleted);
                proxy.WcStoredProcParam_SaveCompleted += new EventHandler<WcStoredProcParam_SaveCompletedEventArgs>(proxy_WcStoredProcParam_SaveCompleted);
                proxy.WcStoredProcParam_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParam_SaveCompleted(object sender, WcStoredProcParam_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParam_SaveCompletedEventArgs> handler = WcStoredProcParam_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParam_Save

        #region WcStoredProcParam_Delete

        public void Begin_WcStoredProcParam_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_DeleteCompleted", IfxTraceCategory.Enter);
                WcStoredProcParamServiceClient proxy = new WcStoredProcParamServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParam_DeleteCompleted += new EventHandler<WcStoredProcParam_DeleteCompletedEventArgs>(proxy_WcStoredProcParam_DeleteCompleted);
                proxy.WcStoredProcParam_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParam_DeleteCompleted(object sender, WcStoredProcParam_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParam_DeleteCompletedEventArgs> handler = WcStoredProcParam_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParam_Delete

        #region WcStoredProcParam_SetIsDeleted

        public void Begin_WcStoredProcParam_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcStoredProcParamServiceClient proxy = new WcStoredProcParamServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParam_SetIsDeletedCompleted += new EventHandler<WcStoredProcParam_SetIsDeletedCompletedEventArgs>(proxy_WcStoredProcParam_SetIsDeletedCompleted);
                proxy.WcStoredProcParam_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParam_SetIsDeletedCompleted(object sender, WcStoredProcParam_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParam_SetIsDeletedCompletedEventArgs> handler = WcStoredProcParam_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParam_SetIsDeleted

        #region WcStoredProcParam_Deactivate

        public void Begin_WcStoredProcParam_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_Deactivate", IfxTraceCategory.Enter);
            WcStoredProcParamServiceClient proxy = new WcStoredProcParamServiceClient();
            AssignCredentials(proxy);
            proxy.WcStoredProcParam_DeactivateCompleted += new EventHandler<WcStoredProcParam_DeactivateCompletedEventArgs>(proxy_WcStoredProcParam_DeactivateCompleted);
            proxy.WcStoredProcParam_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParam_DeactivateCompleted(object sender, WcStoredProcParam_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParam_DeactivateCompletedEventArgs> handler = WcStoredProcParam_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParam_Deactivate

        #region WcStoredProcParam_Remove

        public void Begin_WcStoredProcParam_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_Remove", IfxTraceCategory.Enter);
                WcStoredProcParamServiceClient proxy = new WcStoredProcParamServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParam_RemoveCompleted += new EventHandler<WcStoredProcParam_RemoveCompletedEventArgs>(proxy_WcStoredProcParam_RemoveCompleted);
                proxy.WcStoredProcParam_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParam_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParam_RemoveCompleted(object sender, WcStoredProcParam_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParam_RemoveCompletedEventArgs> handler = WcStoredProcParam_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParam_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcStoredProcParam_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


