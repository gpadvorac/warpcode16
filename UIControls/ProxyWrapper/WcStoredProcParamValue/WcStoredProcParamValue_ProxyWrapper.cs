using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/26/2017 1:52:34 PM

namespace ProxyWrapper
{
    public partial class WcStoredProcParamValueService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcStoredProcParamValue_GetByIdCompletedEventArgs> WcStoredProcParamValue_GetByIdCompleted;
        public event System.EventHandler<WcStoredProcParamValue_GetAllCompletedEventArgs> WcStoredProcParamValue_GetAllCompleted;
        public event System.EventHandler<WcStoredProcParamValue_GetListByFKCompletedEventArgs> WcStoredProcParamValue_GetListByFKCompleted;
        public event System.EventHandler<WcStoredProcParamValue_SaveCompletedEventArgs> WcStoredProcParamValue_SaveCompleted;
        public event System.EventHandler<WcStoredProcParamValue_DeleteCompletedEventArgs> WcStoredProcParamValue_DeleteCompleted;
        public event System.EventHandler<WcStoredProcParamValue_SetIsDeletedCompletedEventArgs> WcStoredProcParamValue_SetIsDeletedCompleted;
        public event System.EventHandler<WcStoredProcParamValue_DeactivateCompletedEventArgs> WcStoredProcParamValue_DeactivateCompleted;
        public event System.EventHandler<WcStoredProcParamValue_RemoveCompletedEventArgs> WcStoredProcParamValue_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcStoredProcParamValue_GetById

        public void Begin_WcStoredProcParamValue_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_GetById", IfxTraceCategory.Enter);
                WcStoredProcParamValueServiceClient proxy = new WcStoredProcParamValueServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParamValue_GetByIdCompleted += new EventHandler<WcStoredProcParamValue_GetByIdCompletedEventArgs>(proxy_WcStoredProcParamValue_GetByIdCompleted);
                proxy.WcStoredProcParamValue_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValue_GetByIdCompleted(object sender, WcStoredProcParamValue_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValue_GetByIdCompletedEventArgs> handler = WcStoredProcParamValue_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValue_GetById

        #region WcStoredProcParamValue_GetAll

        public void Begin_WcStoredProcParamValue_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_GetAll", IfxTraceCategory.Enter);
                WcStoredProcParamValueServiceClient proxy = new WcStoredProcParamValueServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParamValue_GetAllCompleted += new EventHandler<WcStoredProcParamValue_GetAllCompletedEventArgs>(proxy_WcStoredProcParamValue_GetAllCompleted);
                proxy.WcStoredProcParamValue_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValue_GetAllCompleted(object sender, WcStoredProcParamValue_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValue_GetAllCompletedEventArgs> handler = WcStoredProcParamValue_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValue_GetAll

        #region WcStoredProcParamValue_GetListByFK

        public void Begin_WcStoredProcParamValue_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_GetListByFK", IfxTraceCategory.Enter);
                WcStoredProcParamValueServiceClient proxy = new WcStoredProcParamValueServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParamValue_GetListByFKCompleted += new EventHandler<WcStoredProcParamValue_GetListByFKCompletedEventArgs>(proxy_WcStoredProcParamValue_GetListByFKCompleted);
                proxy.WcStoredProcParamValue_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValue_GetListByFKCompleted(object sender, WcStoredProcParamValue_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValue_GetListByFKCompletedEventArgs> handler = WcStoredProcParamValue_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValue_GetListByFK

        #region WcStoredProcParamValue_Save

        public void Begin_WcStoredProcParamValue_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_Save", IfxTraceCategory.Enter);
                WcStoredProcParamValueServiceClient proxy = new WcStoredProcParamValueServiceClient();
                AssignCredentials(proxy);
                //proxy.WcStoredProcParamValue_SaveCompleted += new EventHandler<WcStoredProcParamValue_SaveCompletedEventArgs>(proxy_WcStoredProcParamValue_SaveCompleted);
                proxy.WcStoredProcParamValue_SaveCompleted += new EventHandler<WcStoredProcParamValue_SaveCompletedEventArgs>(proxy_WcStoredProcParamValue_SaveCompleted);
                proxy.WcStoredProcParamValue_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValue_SaveCompleted(object sender, WcStoredProcParamValue_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValue_SaveCompletedEventArgs> handler = WcStoredProcParamValue_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValue_Save

        #region WcStoredProcParamValue_Delete

        public void Begin_WcStoredProcParamValue_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_DeleteCompleted", IfxTraceCategory.Enter);
                WcStoredProcParamValueServiceClient proxy = new WcStoredProcParamValueServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParamValue_DeleteCompleted += new EventHandler<WcStoredProcParamValue_DeleteCompletedEventArgs>(proxy_WcStoredProcParamValue_DeleteCompleted);
                proxy.WcStoredProcParamValue_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValue_DeleteCompleted(object sender, WcStoredProcParamValue_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValue_DeleteCompletedEventArgs> handler = WcStoredProcParamValue_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValue_Delete

        #region WcStoredProcParamValue_SetIsDeleted

        public void Begin_WcStoredProcParamValue_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcStoredProcParamValueServiceClient proxy = new WcStoredProcParamValueServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParamValue_SetIsDeletedCompleted += new EventHandler<WcStoredProcParamValue_SetIsDeletedCompletedEventArgs>(proxy_WcStoredProcParamValue_SetIsDeletedCompleted);
                proxy.WcStoredProcParamValue_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValue_SetIsDeletedCompleted(object sender, WcStoredProcParamValue_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValue_SetIsDeletedCompletedEventArgs> handler = WcStoredProcParamValue_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValue_SetIsDeleted

        #region WcStoredProcParamValue_Deactivate

        public void Begin_WcStoredProcParamValue_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_Deactivate", IfxTraceCategory.Enter);
            WcStoredProcParamValueServiceClient proxy = new WcStoredProcParamValueServiceClient();
            AssignCredentials(proxy);
            proxy.WcStoredProcParamValue_DeactivateCompleted += new EventHandler<WcStoredProcParamValue_DeactivateCompletedEventArgs>(proxy_WcStoredProcParamValue_DeactivateCompleted);
            proxy.WcStoredProcParamValue_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValue_DeactivateCompleted(object sender, WcStoredProcParamValue_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValue_DeactivateCompletedEventArgs> handler = WcStoredProcParamValue_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValue_Deactivate

        #region WcStoredProcParamValue_Remove

        public void Begin_WcStoredProcParamValue_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_Remove", IfxTraceCategory.Enter);
                WcStoredProcParamValueServiceClient proxy = new WcStoredProcParamValueServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParamValue_RemoveCompleted += new EventHandler<WcStoredProcParamValue_RemoveCompletedEventArgs>(proxy_WcStoredProcParamValue_RemoveCompleted);
                proxy.WcStoredProcParamValue_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValue_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValue_RemoveCompleted(object sender, WcStoredProcParamValue_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValue_RemoveCompletedEventArgs> handler = WcStoredProcParamValue_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValue_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcStoredProcParamValue_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


