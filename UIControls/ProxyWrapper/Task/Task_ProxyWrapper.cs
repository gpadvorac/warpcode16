using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  6/21/2016 10:59:43 AM

namespace ProxyWrapper
{
    public partial class TaskService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<Task_GetByIdCompletedEventArgs> Task_GetByIdCompleted;
        public event System.EventHandler<Task_GetAllCompletedEventArgs> Task_GetAllCompleted;
        public event System.EventHandler<Task_GetListByFKCompletedEventArgs> Task_GetListByFKCompleted;
        public event System.EventHandler<Task_SaveCompletedEventArgs> Task_SaveCompleted;
        public event System.EventHandler<Task_DeleteCompletedEventArgs> Task_DeleteCompleted;
        public event System.EventHandler<Task_SetIsDeletedCompletedEventArgs> Task_SetIsDeletedCompleted;
        public event System.EventHandler<Task_DeactivateCompletedEventArgs> Task_DeactivateCompleted;
        public event System.EventHandler<Task_RemoveCompletedEventArgs> Task_RemoveCompleted;

        public event System.EventHandler<GetTask_ReadOnlyStaticListsCompletedEventArgs> GetTask_ReadOnlyStaticListsCompleted;
		public event System.EventHandler<GetTaskCategory_ComboItemListCompletedEventArgs> GetTaskCategory_ComboItemListCompleted;
		public event System.EventHandler<GetTaskPriority_ComboItemListCompletedEventArgs> GetTaskPriority_ComboItemListCompleted;
		public event System.EventHandler<GetTaskStatus_ComboItemListCompletedEventArgs> GetTaskStatus_ComboItemListCompleted;
		public event System.EventHandler<GetTaskType_ComboItemListCompletedEventArgs> GetTaskType_ComboItemListCompleted;
		public event System.EventHandler<GetTask_lstByContractCompletedEventArgs> GetTask_lstByContractCompleted;
		public event System.EventHandler<GetTask_lstByContractDtCompletedEventArgs> GetTask_lstByContractDtCompleted;
		public event System.EventHandler<GetTask_lstByDefectCompletedEventArgs> GetTask_lstByDefectCompleted;
		public event System.EventHandler<GetTask_lstByLeaseCompletedEventArgs> GetTask_lstByLeaseCompleted;
		public event System.EventHandler<GetTask_lstByLeaseTractCompletedEventArgs> GetTask_lstByLeaseTractCompleted;
		public event System.EventHandler<GetTask_lstByObligationCompletedEventArgs> GetTask_lstByObligationCompleted;
		public event System.EventHandler<GetTask_lstByPersonCompletedEventArgs> GetTask_lstByPersonCompleted;
		public event System.EventHandler<GetTask_lstByProspectCompletedEventArgs> GetTask_lstByProspectCompleted;
		public event System.EventHandler<GetTask_lstByWellCompletedEventArgs> GetTask_lstByWellCompleted;
		public event System.EventHandler<ExecuteTask_XRef_AssignDefectInSectionCompletedEventArgs> ExecuteTask_XRef_AssignDefectInSectionCompleted;
		public event System.EventHandler<ExecuteTask_XRef_RemoveDefectCompletedEventArgs> ExecuteTask_XRef_RemoveDefectCompleted;
		public event System.EventHandler<ExecuteTask_XRef_AssignContractCompletedEventArgs> ExecuteTask_XRef_AssignContractCompleted;
		public event System.EventHandler<ExecuteTask_XRef_RemoveContractCompletedEventArgs> ExecuteTask_XRef_RemoveContractCompleted;
		public event System.EventHandler<GetTask_lstByMyTasksCompletedEventArgs> GetTask_lstByMyTasksCompleted;
		public event System.EventHandler<GetTask_lstByProjectOnlyCompletedEventArgs> GetTask_lstByProjectOnlyCompleted;
		public event System.EventHandler<GetTask_lstByRelatedContractCompletedEventArgs> GetTask_lstByRelatedContractCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region Task_GetById

        public void Begin_Task_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_GetById", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.Task_GetByIdCompleted += new EventHandler<Task_GetByIdCompletedEventArgs>(proxy_Task_GetByIdCompleted);
                proxy.Task_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task_GetByIdCompleted(object sender, Task_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task_GetByIdCompletedEventArgs> handler = Task_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Task_GetById

        #region Task_GetAll

        public void Begin_Task_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_GetAll", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.Task_GetAllCompleted += new EventHandler<Task_GetAllCompletedEventArgs>(proxy_Task_GetAllCompleted);
                proxy.Task_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task_GetAllCompleted(object sender, Task_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task_GetAllCompletedEventArgs> handler = Task_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Task_GetAll

        #region Task_GetListByFK

        public void Begin_Task_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_GetListByFK", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.Task_GetListByFKCompleted += new EventHandler<Task_GetListByFKCompletedEventArgs>(proxy_Task_GetListByFKCompleted);
                proxy.Task_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task_GetListByFKCompleted(object sender, Task_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task_GetListByFKCompletedEventArgs> handler = Task_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Task_GetListByFK

        #region Task_Save

        public void Begin_Task_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_Save", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                //proxy.Task_SaveCompleted += new EventHandler<Task_SaveCompletedEventArgs>(proxy_Task_SaveCompleted);
                proxy.Task_SaveCompleted += new EventHandler<Task_SaveCompletedEventArgs>(proxy_Task_SaveCompleted);
                proxy.Task_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task_SaveCompleted(object sender, Task_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task_SaveCompletedEventArgs> handler = Task_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Task_Save

        #region Task_Delete

        public void Begin_Task_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_DeleteCompleted", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.Task_DeleteCompleted += new EventHandler<Task_DeleteCompletedEventArgs>(proxy_Task_DeleteCompleted);
                proxy.Task_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task_DeleteCompleted(object sender, Task_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task_DeleteCompletedEventArgs> handler = Task_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Task_Delete

        #region Task_SetIsDeleted

        public void Begin_Task_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.Task_SetIsDeletedCompleted += new EventHandler<Task_SetIsDeletedCompletedEventArgs>(proxy_Task_SetIsDeletedCompleted);
                proxy.Task_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task_SetIsDeletedCompleted(object sender, Task_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task_SetIsDeletedCompletedEventArgs> handler = Task_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Task_SetIsDeleted

        #region Task_Deactivate

        public void Begin_Task_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_Deactivate", IfxTraceCategory.Enter);
            TaskServiceClient proxy = new TaskServiceClient();
            AssignCredentials(proxy);
            proxy.Task_DeactivateCompleted += new EventHandler<Task_DeactivateCompletedEventArgs>(proxy_Task_DeactivateCompleted);
            proxy.Task_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task_DeactivateCompleted(object sender, Task_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task_DeactivateCompletedEventArgs> handler = Task_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Task_Deactivate

        #region Task_Remove

        public void Begin_Task_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_Remove", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.Task_RemoveCompleted += new EventHandler<Task_RemoveCompletedEventArgs>(proxy_Task_RemoveCompleted);
                proxy.Task_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task_RemoveCompleted(object sender, Task_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task_RemoveCompletedEventArgs> handler = Task_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion Task_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists

        public void Begin_GetTask_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTask_ReadOnlyStaticListsCompleted += new EventHandler<GetTask_ReadOnlyStaticListsCompletedEventArgs>(proxy_GetTask_ReadOnlyStaticListsCompleted);
                proxy.GetTask_ReadOnlyStaticListsAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_ReadOnlyStaticLists", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTask_ReadOnlyStaticListsCompleted(object sender, GetTask_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetTask_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                System.EventHandler<GetTask_ReadOnlyStaticListsCompletedEventArgs> handler = GetTask_ReadOnlyStaticListsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetTask_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetTask_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion ReadOnlyStaticLists

        #region GetTaskCategory_ComboItemList

        public void Begin_GetTaskCategory_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskCategory_ComboItemList", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTaskCategory_ComboItemListCompleted += new EventHandler<GetTaskCategory_ComboItemListCompletedEventArgs>(proxy_GetTaskCategory_ComboItemListCompleted);
                proxy.GetTaskCategory_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskCategory_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskCategory_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTaskCategory_ComboItemListCompleted(object sender, GetTaskCategory_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskCategory_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetTaskCategory_ComboItemListCompletedEventArgs> handler = GetTaskCategory_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskCategory_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskCategory_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTaskCategory_ComboItemList

        #region GetTaskPriority_ComboItemList

        public void Begin_GetTaskPriority_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskPriority_ComboItemList", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTaskPriority_ComboItemListCompleted += new EventHandler<GetTaskPriority_ComboItemListCompletedEventArgs>(proxy_GetTaskPriority_ComboItemListCompleted);
                proxy.GetTaskPriority_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskPriority_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskPriority_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTaskPriority_ComboItemListCompleted(object sender, GetTaskPriority_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskPriority_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetTaskPriority_ComboItemListCompletedEventArgs> handler = GetTaskPriority_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskPriority_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskPriority_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTaskPriority_ComboItemList

        #region GetTaskStatus_ComboItemList

        public void Begin_GetTaskStatus_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskStatus_ComboItemList", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTaskStatus_ComboItemListCompleted += new EventHandler<GetTaskStatus_ComboItemListCompletedEventArgs>(proxy_GetTaskStatus_ComboItemListCompleted);
                proxy.GetTaskStatus_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskStatus_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskStatus_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTaskStatus_ComboItemListCompleted(object sender, GetTaskStatus_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetTaskStatus_ComboItemListCompletedEventArgs> handler = GetTaskStatus_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTaskStatus_ComboItemList

        #region GetTaskType_ComboItemList

        public void Begin_GetTaskType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskType_ComboItemList", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTaskType_ComboItemListCompleted += new EventHandler<GetTaskType_ComboItemListCompletedEventArgs>(proxy_GetTaskType_ComboItemListCompleted);
                proxy.GetTaskType_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskType_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTaskType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTaskType_ComboItemListCompleted(object sender, GetTaskType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskType_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetTaskType_ComboItemListCompletedEventArgs> handler = GetTaskType_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskType_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTaskType_ComboItemList

        #region GetTask_lstByContract

        public void Begin_GetTask_lstByContract(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByContract", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTask_lstByContractCompleted += new EventHandler<GetTask_lstByContractCompletedEventArgs>(proxy_GetTask_lstByContractCompleted);
                proxy.GetTask_lstByContractAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByContract", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByContract", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTask_lstByContractCompleted(object sender, GetTask_lstByContractCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByContract", IfxTraceCategory.Enter);
                System.EventHandler<GetTask_lstByContractCompletedEventArgs> handler = GetTask_lstByContractCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByContract", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByContract", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTask_lstByContract

        #region GetTask_lstByContractDt

        public void Begin_GetTask_lstByContractDt(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByContractDt", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTask_lstByContractDtCompleted += new EventHandler<GetTask_lstByContractDtCompletedEventArgs>(proxy_GetTask_lstByContractDtCompleted);
                proxy.GetTask_lstByContractDtAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByContractDt", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByContractDt", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTask_lstByContractDtCompleted(object sender, GetTask_lstByContractDtCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByContractDt", IfxTraceCategory.Enter);
                System.EventHandler<GetTask_lstByContractDtCompletedEventArgs> handler = GetTask_lstByContractDtCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByContractDt", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByContractDt", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTask_lstByContractDt

        #region GetTask_lstByDefect

        public void Begin_GetTask_lstByDefect(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByDefect", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTask_lstByDefectCompleted += new EventHandler<GetTask_lstByDefectCompletedEventArgs>(proxy_GetTask_lstByDefectCompleted);
                proxy.GetTask_lstByDefectAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByDefect", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByDefect", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTask_lstByDefectCompleted(object sender, GetTask_lstByDefectCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByDefect", IfxTraceCategory.Enter);
                System.EventHandler<GetTask_lstByDefectCompletedEventArgs> handler = GetTask_lstByDefectCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByDefect", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByDefect", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTask_lstByDefect

        #region GetTask_lstByLease

        public void Begin_GetTask_lstByLease(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByLease", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTask_lstByLeaseCompleted += new EventHandler<GetTask_lstByLeaseCompletedEventArgs>(proxy_GetTask_lstByLeaseCompleted);
                proxy.GetTask_lstByLeaseAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByLease", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByLease", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTask_lstByLeaseCompleted(object sender, GetTask_lstByLeaseCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByLease", IfxTraceCategory.Enter);
                System.EventHandler<GetTask_lstByLeaseCompletedEventArgs> handler = GetTask_lstByLeaseCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByLease", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByLease", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTask_lstByLease

        #region GetTask_lstByLeaseTract

        public void Begin_GetTask_lstByLeaseTract(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByLeaseTract", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTask_lstByLeaseTractCompleted += new EventHandler<GetTask_lstByLeaseTractCompletedEventArgs>(proxy_GetTask_lstByLeaseTractCompleted);
                proxy.GetTask_lstByLeaseTractAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByLeaseTract", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByLeaseTract", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTask_lstByLeaseTractCompleted(object sender, GetTask_lstByLeaseTractCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByLeaseTract", IfxTraceCategory.Enter);
                System.EventHandler<GetTask_lstByLeaseTractCompletedEventArgs> handler = GetTask_lstByLeaseTractCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByLeaseTract", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByLeaseTract", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTask_lstByLeaseTract

        #region GetTask_lstByObligation

        public void Begin_GetTask_lstByObligation(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByObligation", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTask_lstByObligationCompleted += new EventHandler<GetTask_lstByObligationCompletedEventArgs>(proxy_GetTask_lstByObligationCompleted);
                proxy.GetTask_lstByObligationAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByObligation", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByObligation", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTask_lstByObligationCompleted(object sender, GetTask_lstByObligationCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByObligation", IfxTraceCategory.Enter);
                System.EventHandler<GetTask_lstByObligationCompletedEventArgs> handler = GetTask_lstByObligationCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByObligation", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByObligation", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTask_lstByObligation

        #region GetTask_lstByPerson

        public void Begin_GetTask_lstByPerson(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByPerson", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTask_lstByPersonCompleted += new EventHandler<GetTask_lstByPersonCompletedEventArgs>(proxy_GetTask_lstByPersonCompleted);
                proxy.GetTask_lstByPersonAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByPerson", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByPerson", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTask_lstByPersonCompleted(object sender, GetTask_lstByPersonCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByPerson", IfxTraceCategory.Enter);
                System.EventHandler<GetTask_lstByPersonCompletedEventArgs> handler = GetTask_lstByPersonCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByPerson", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByPerson", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTask_lstByPerson

        #region GetTask_lstByProspect

        public void Begin_GetTask_lstByProspect(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByProspect", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTask_lstByProspectCompleted += new EventHandler<GetTask_lstByProspectCompletedEventArgs>(proxy_GetTask_lstByProspectCompleted);
                proxy.GetTask_lstByProspectAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByProspect", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByProspect", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTask_lstByProspectCompleted(object sender, GetTask_lstByProspectCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProspect", IfxTraceCategory.Enter);
                System.EventHandler<GetTask_lstByProspectCompletedEventArgs> handler = GetTask_lstByProspectCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProspect", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProspect", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTask_lstByProspect

        #region GetTask_lstByWell

        public void Begin_GetTask_lstByWell(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByWell", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTask_lstByWellCompleted += new EventHandler<GetTask_lstByWellCompletedEventArgs>(proxy_GetTask_lstByWellCompleted);
                proxy.GetTask_lstByWellAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByWell", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByWell", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTask_lstByWellCompleted(object sender, GetTask_lstByWellCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByWell", IfxTraceCategory.Enter);
                System.EventHandler<GetTask_lstByWellCompletedEventArgs> handler = GetTask_lstByWellCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByWell", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByWell", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTask_lstByWell

        #region ExecuteTask_XRef_AssignDefectInSection

        public void Begin_ExecuteTask_XRef_AssignDefectInSection(Guid Prj_Id, Guid Tk_Id, Guid Ct_Id, Int32 DefectNo, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask_XRef_AssignDefectInSection", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.ExecuteTask_XRef_AssignDefectInSectionCompleted += new EventHandler<ExecuteTask_XRef_AssignDefectInSectionCompletedEventArgs>(proxy_ExecuteTask_XRef_AssignDefectInSectionCompleted);
                proxy.ExecuteTask_XRef_AssignDefectInSectionAsync(Prj_Id, Tk_Id, Ct_Id, DefectNo, UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask_XRef_AssignDefectInSection", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask_XRef_AssignDefectInSection", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecuteTask_XRef_AssignDefectInSectionCompleted(object sender, ExecuteTask_XRef_AssignDefectInSectionCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_AssignDefectInSection", IfxTraceCategory.Enter);
                System.EventHandler<ExecuteTask_XRef_AssignDefectInSectionCompletedEventArgs> handler = ExecuteTask_XRef_AssignDefectInSectionCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_AssignDefectInSection", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_AssignDefectInSection", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecuteTask_XRef_AssignDefectInSection

        #region ExecuteTask_XRef_RemoveDefect

        public void Begin_ExecuteTask_XRef_RemoveDefect(Guid Tk_Id, Guid Df_Id, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask_XRef_RemoveDefect", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.ExecuteTask_XRef_RemoveDefectCompleted += new EventHandler<ExecuteTask_XRef_RemoveDefectCompletedEventArgs>(proxy_ExecuteTask_XRef_RemoveDefectCompleted);
                proxy.ExecuteTask_XRef_RemoveDefectAsync(Tk_Id, Df_Id, UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask_XRef_RemoveDefect", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask_XRef_RemoveDefect", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecuteTask_XRef_RemoveDefectCompleted(object sender, ExecuteTask_XRef_RemoveDefectCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_RemoveDefect", IfxTraceCategory.Enter);
                System.EventHandler<ExecuteTask_XRef_RemoveDefectCompletedEventArgs> handler = ExecuteTask_XRef_RemoveDefectCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_RemoveDefect", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_RemoveDefect", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecuteTask_XRef_RemoveDefect

        #region ExecuteTask_XRef_AssignContract

        public void Begin_ExecuteTask_XRef_AssignContract(Guid Prj_Id, Guid Tk_Id, String SectionNo, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask_XRef_AssignContract", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.ExecuteTask_XRef_AssignContractCompleted += new EventHandler<ExecuteTask_XRef_AssignContractCompletedEventArgs>(proxy_ExecuteTask_XRef_AssignContractCompleted);
                proxy.ExecuteTask_XRef_AssignContractAsync(Prj_Id, Tk_Id, SectionNo, UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask_XRef_AssignContract", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask_XRef_AssignContract", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecuteTask_XRef_AssignContractCompleted(object sender, ExecuteTask_XRef_AssignContractCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_AssignContract", IfxTraceCategory.Enter);
                System.EventHandler<ExecuteTask_XRef_AssignContractCompletedEventArgs> handler = ExecuteTask_XRef_AssignContractCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_AssignContract", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_AssignContract", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecuteTask_XRef_AssignContract

        #region ExecuteTask_XRef_RemoveContract

        public void Begin_ExecuteTask_XRef_RemoveContract(Guid Tk_Id, Guid Ct_Id, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask_XRef_RemoveContract", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.ExecuteTask_XRef_RemoveContractCompleted += new EventHandler<ExecuteTask_XRef_RemoveContractCompletedEventArgs>(proxy_ExecuteTask_XRef_RemoveContractCompleted);
                proxy.ExecuteTask_XRef_RemoveContractAsync(Tk_Id, Ct_Id, UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask_XRef_RemoveContract", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask_XRef_RemoveContract", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecuteTask_XRef_RemoveContractCompleted(object sender, ExecuteTask_XRef_RemoveContractCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_RemoveContract", IfxTraceCategory.Enter);
                System.EventHandler<ExecuteTask_XRef_RemoveContractCompletedEventArgs> handler = ExecuteTask_XRef_RemoveContractCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_RemoveContract", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_RemoveContract", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecuteTask_XRef_RemoveContract

        #region GetTask_lstByMyTasks

        public void Begin_GetTask_lstByMyTasks(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByMyTasks", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTask_lstByMyTasksCompleted += new EventHandler<GetTask_lstByMyTasksCompletedEventArgs>(proxy_GetTask_lstByMyTasksCompleted);
                proxy.GetTask_lstByMyTasksAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByMyTasks", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByMyTasks", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTask_lstByMyTasksCompleted(object sender, GetTask_lstByMyTasksCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByMyTasks", IfxTraceCategory.Enter);
                System.EventHandler<GetTask_lstByMyTasksCompletedEventArgs> handler = GetTask_lstByMyTasksCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByMyTasks", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByMyTasks", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTask_lstByMyTasks

        #region GetTask_lstByProjectOnly

        public void Begin_GetTask_lstByProjectOnly(Guid Tk_Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByProjectOnly", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTask_lstByProjectOnlyCompleted += new EventHandler<GetTask_lstByProjectOnlyCompletedEventArgs>(proxy_GetTask_lstByProjectOnlyCompleted);
                proxy.GetTask_lstByProjectOnlyAsync(Tk_Prj_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByProjectOnly", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByProjectOnly", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTask_lstByProjectOnlyCompleted(object sender, GetTask_lstByProjectOnlyCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProjectOnly", IfxTraceCategory.Enter);
                System.EventHandler<GetTask_lstByProjectOnlyCompletedEventArgs> handler = GetTask_lstByProjectOnlyCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProjectOnly", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProjectOnly", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTask_lstByProjectOnly

        #region GetTask_lstByRelatedContract

        public void Begin_GetTask_lstByRelatedContract(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByRelatedContract", IfxTraceCategory.Enter);
                TaskServiceClient proxy = new TaskServiceClient();
                AssignCredentials(proxy);
                proxy.GetTask_lstByRelatedContractCompleted += new EventHandler<GetTask_lstByRelatedContractCompletedEventArgs>(proxy_GetTask_lstByRelatedContractCompleted);
                proxy.GetTask_lstByRelatedContractAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByRelatedContract", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByRelatedContract", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTask_lstByRelatedContractCompleted(object sender, GetTask_lstByRelatedContractCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByRelatedContract", IfxTraceCategory.Enter);
                System.EventHandler<GetTask_lstByRelatedContractCompletedEventArgs> handler = GetTask_lstByRelatedContractCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByRelatedContract", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByRelatedContract", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTask_lstByRelatedContract

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


