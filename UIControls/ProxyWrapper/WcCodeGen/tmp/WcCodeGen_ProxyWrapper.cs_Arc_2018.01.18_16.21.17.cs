using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  1/18/2018 3:40:26 PM

namespace ProxyWrapper
{
    public partial class WcCodeGenService_ProxyWrapper
    {


        #region Events
		public event System.EventHandler<GetWcCg_GetTableColumnsCompletedEventArgs> GetWcCg_GetTableColumnsCompleted;
		public event System.EventHandler<GetWcCg_GetTablesCompletedEventArgs> GetWcCg_GetTablesCompleted;

        #endregion Events

        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #region GetWcCg_GetTableColumns

        public void Begin_GetWcCg_GetTableColumns(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_GetTableColumns", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCg_GetTableColumnsCompleted += new EventHandler<GetWcCg_GetTableColumnsCompletedEventArgs>(proxy_GetWcCg_GetTableColumnsCompleted);
                proxy.GetWcCg_GetTableColumnsAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_GetTableColumns", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_GetTableColumns", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCg_GetTableColumnsCompleted(object sender, GetWcCg_GetTableColumnsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTableColumns", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCg_GetTableColumnsCompletedEventArgs> handler = GetWcCg_GetTableColumnsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTableColumns", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTableColumns", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCg_GetTableColumns

        #region GetWcCg_GetTables

        public void Begin_GetWcCg_GetTables(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_GetTables", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCg_GetTablesCompleted += new EventHandler<GetWcCg_GetTablesCompletedEventArgs>(proxy_GetWcCg_GetTablesCompleted);
                proxy.GetWcCg_GetTablesAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_GetTables", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_GetTables", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCg_GetTablesCompleted(object sender, GetWcCg_GetTablesCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTables", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCg_GetTablesCompletedEventArgs> handler = GetWcCg_GetTablesCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTables", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTables", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCg_GetTables

    }
}


