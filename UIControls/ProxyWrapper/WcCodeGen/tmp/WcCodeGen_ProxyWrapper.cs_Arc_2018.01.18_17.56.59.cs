using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  1/18/2018 4:57:18 PM

namespace ProxyWrapper
{
    public partial class WcCodeGenService_ProxyWrapper
    {


        #region Events
		public event System.EventHandler<GetWcCg_TableColumnsCompletedEventArgs> GetWcCg_TableColumnsCompleted;
		public event System.EventHandler<GetWcCg_TablesCompletedEventArgs> GetWcCg_TablesCompleted;
		public event System.EventHandler<GetWcCg_ToolTipsCompletedEventArgs> GetWcCg_ToolTipsCompleted;
		public event System.EventHandler<GetWcCg_TableColumnComboColumnCompletedEventArgs> GetWcCg_TableColumnComboColumnCompleted;
		public event System.EventHandler<GetWcCg_TableSortBysCompletedEventArgs> GetWcCg_TableSortBysCompleted;
		public event System.EventHandler<GetWcCg_ChildTablesCompletedEventArgs> GetWcCg_ChildTablesCompleted;

        #endregion Events

        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #region GetWcCg_TableColumns

        public void Begin_GetWcCg_TableColumns(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableColumns", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCg_TableColumnsCompleted += new EventHandler<GetWcCg_TableColumnsCompletedEventArgs>(proxy_GetWcCg_TableColumnsCompleted);
                proxy.GetWcCg_TableColumnsAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableColumns", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableColumns", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCg_TableColumnsCompleted(object sender, GetWcCg_TableColumnsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableColumns", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCg_TableColumnsCompletedEventArgs> handler = GetWcCg_TableColumnsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableColumns", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableColumns", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCg_TableColumns

        #region GetWcCg_Tables

        public void Begin_GetWcCg_Tables(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_Tables", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCg_TablesCompleted += new EventHandler<GetWcCg_TablesCompletedEventArgs>(proxy_GetWcCg_TablesCompleted);
                proxy.GetWcCg_TablesAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_Tables", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_Tables", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCg_TablesCompleted(object sender, GetWcCg_TablesCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_Tables", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCg_TablesCompletedEventArgs> handler = GetWcCg_TablesCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_Tables", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_Tables", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCg_Tables

        #region GetWcCg_ToolTips

        public void Begin_GetWcCg_ToolTips(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_ToolTips", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCg_ToolTipsCompleted += new EventHandler<GetWcCg_ToolTipsCompletedEventArgs>(proxy_GetWcCg_ToolTipsCompleted);
                proxy.GetWcCg_ToolTipsAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_ToolTips", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_ToolTips", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCg_ToolTipsCompleted(object sender, GetWcCg_ToolTipsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_ToolTips", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCg_ToolTipsCompletedEventArgs> handler = GetWcCg_ToolTipsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_ToolTips", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_ToolTips", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCg_ToolTips

        #region GetWcCg_TableColumnComboColumn

        public void Begin_GetWcCg_TableColumnComboColumn(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableColumnComboColumn", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCg_TableColumnComboColumnCompleted += new EventHandler<GetWcCg_TableColumnComboColumnCompletedEventArgs>(proxy_GetWcCg_TableColumnComboColumnCompleted);
                proxy.GetWcCg_TableColumnComboColumnAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableColumnComboColumn", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableColumnComboColumn", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCg_TableColumnComboColumnCompleted(object sender, GetWcCg_TableColumnComboColumnCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableColumnComboColumn", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCg_TableColumnComboColumnCompletedEventArgs> handler = GetWcCg_TableColumnComboColumnCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableColumnComboColumn", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableColumnComboColumn", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCg_TableColumnComboColumn

        #region GetWcCg_TableSortBys

        public void Begin_GetWcCg_TableSortBys(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableSortBys", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCg_TableSortBysCompleted += new EventHandler<GetWcCg_TableSortBysCompletedEventArgs>(proxy_GetWcCg_TableSortBysCompleted);
                proxy.GetWcCg_TableSortBysAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableSortBys", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableSortBys", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCg_TableSortBysCompleted(object sender, GetWcCg_TableSortBysCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableSortBys", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCg_TableSortBysCompletedEventArgs> handler = GetWcCg_TableSortBysCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableSortBys", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableSortBys", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCg_TableSortBys

        #region GetWcCg_ChildTables

        public void Begin_GetWcCg_ChildTables(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_ChildTables", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCg_ChildTablesCompleted += new EventHandler<GetWcCg_ChildTablesCompletedEventArgs>(proxy_GetWcCg_ChildTablesCompleted);
                proxy.GetWcCg_ChildTablesAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_ChildTables", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_ChildTables", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCg_ChildTablesCompleted(object sender, GetWcCg_ChildTablesCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_ChildTables", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCg_ChildTablesCompletedEventArgs> handler = GetWcCg_ChildTablesCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_ChildTables", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_ChildTables", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCg_ChildTables

    }
}


