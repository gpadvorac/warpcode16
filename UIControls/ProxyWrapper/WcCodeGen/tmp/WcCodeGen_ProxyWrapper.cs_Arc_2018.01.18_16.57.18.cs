using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  1/18/2018 4:21:17 PM

namespace ProxyWrapper
{
    public partial class WcCodeGenService_ProxyWrapper
    {


        #region Events
		public event System.EventHandler<GetWcCg_GetTableColumnsCompletedEventArgs> GetWcCg_GetTableColumnsCompleted;
		public event System.EventHandler<GetWcCg_GetTablesCompletedEventArgs> GetWcCg_GetTablesCompleted;
		public event System.EventHandler<GetWcCg_ToolTipsCompletedEventArgs> GetWcCg_ToolTipsCompleted;
		public event System.EventHandler<GetWcCg_TableColumnComboColumnCompletedEventArgs> GetWcCg_TableColumnComboColumnCompleted;
		public event System.EventHandler<GetWcCg_TableSortBysCompletedEventArgs> GetWcCg_TableSortBysCompleted;

        #endregion Events

        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #region GetWcCg_GetTableColumns

        public void Begin_GetWcCg_GetTableColumns(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_GetTableColumns", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCg_GetTableColumnsCompleted += new EventHandler<GetWcCg_GetTableColumnsCompletedEventArgs>(proxy_GetWcCg_GetTableColumnsCompleted);
                proxy.GetWcCg_GetTableColumnsAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_GetTableColumns", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_GetTableColumns", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCg_GetTableColumnsCompleted(object sender, GetWcCg_GetTableColumnsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTableColumns", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCg_GetTableColumnsCompletedEventArgs> handler = GetWcCg_GetTableColumnsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTableColumns", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTableColumns", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCg_GetTableColumns

        #region GetWcCg_GetTables

        public void Begin_GetWcCg_GetTables(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_GetTables", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCg_GetTablesCompleted += new EventHandler<GetWcCg_GetTablesCompletedEventArgs>(proxy_GetWcCg_GetTablesCompleted);
                proxy.GetWcCg_GetTablesAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_GetTables", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_GetTables", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCg_GetTablesCompleted(object sender, GetWcCg_GetTablesCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTables", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCg_GetTablesCompletedEventArgs> handler = GetWcCg_GetTablesCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTables", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTables", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCg_GetTables

        #region GetWcCg_ToolTips

        public void Begin_GetWcCg_ToolTips(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_ToolTips", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCg_ToolTipsCompleted += new EventHandler<GetWcCg_ToolTipsCompletedEventArgs>(proxy_GetWcCg_ToolTipsCompleted);
                proxy.GetWcCg_ToolTipsAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_ToolTips", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_ToolTips", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCg_ToolTipsCompleted(object sender, GetWcCg_ToolTipsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_ToolTips", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCg_ToolTipsCompletedEventArgs> handler = GetWcCg_ToolTipsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_ToolTips", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_ToolTips", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCg_ToolTips

        #region GetWcCg_TableColumnComboColumn

        public void Begin_GetWcCg_TableColumnComboColumn(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableColumnComboColumn", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCg_TableColumnComboColumnCompleted += new EventHandler<GetWcCg_TableColumnComboColumnCompletedEventArgs>(proxy_GetWcCg_TableColumnComboColumnCompleted);
                proxy.GetWcCg_TableColumnComboColumnAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableColumnComboColumn", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableColumnComboColumn", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCg_TableColumnComboColumnCompleted(object sender, GetWcCg_TableColumnComboColumnCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableColumnComboColumn", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCg_TableColumnComboColumnCompletedEventArgs> handler = GetWcCg_TableColumnComboColumnCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableColumnComboColumn", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableColumnComboColumn", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCg_TableColumnComboColumn

        #region GetWcCg_TableSortBys

        public void Begin_GetWcCg_TableSortBys(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableSortBys", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCg_TableSortBysCompleted += new EventHandler<GetWcCg_TableSortBysCompletedEventArgs>(proxy_GetWcCg_TableSortBysCompleted);
                proxy.GetWcCg_TableSortBysAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableSortBys", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCg_TableSortBys", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCg_TableSortBysCompleted(object sender, GetWcCg_TableSortBysCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableSortBys", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCg_TableSortBysCompletedEventArgs> handler = GetWcCg_TableSortBysCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableSortBys", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableSortBys", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCg_TableSortBys

    }
}


