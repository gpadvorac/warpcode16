using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  1/12/2018 12:54:39 PM

namespace ProxyWrapper
{
    public partial class WcCodeGenService_ProxyWrapper
    {

        #region Initialize Variables

        public event System.EventHandler<GetTable_TableColumnMetadataCompletedEventArgs> GetTable_TableColumnMetadataCompleted;


        #endregion Initialize Variables


        #region GetTable_TableColumnMetadata

        public void Begin_GetTable_TableColumnMetadata(Guid apVrsn_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTable_TableColumnMetadata", IfxTraceCategory.Enter);
                WcCodeGenServiceClient proxy = new WcCodeGenServiceClient();
                AssignCredentials(proxy);
                proxy.GetTable_TableColumnMetadataCompleted += new EventHandler<GetTable_TableColumnMetadataCompletedEventArgs>(proxy_GetTable_TableColumnMetadataCompleted);
                proxy.GetTable_TableColumnMetadataAsync(apVrsn_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTable_TableColumnMetadata", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTable_TableColumnMetadata", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetTable_TableColumnMetadataCompleted(object sender, GetTable_TableColumnMetadataCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTable_TableColumnMetadata", IfxTraceCategory.Enter);
                System.EventHandler<GetTable_TableColumnMetadataCompletedEventArgs> handler = GetTable_TableColumnMetadataCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTable_TableColumnMetadata", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTable_TableColumnMetadata", IfxTraceCategory.Leave);
            }
        }

        #endregion GetTable_TableColumnMetadata




    }
}


