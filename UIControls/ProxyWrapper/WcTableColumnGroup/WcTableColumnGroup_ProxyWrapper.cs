using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/15/2017 5:06:08 PM

namespace ProxyWrapper
{
    public partial class WcTableColumnGroupService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcTableColumnGroup_GetByIdCompletedEventArgs> WcTableColumnGroup_GetByIdCompleted;
        public event System.EventHandler<WcTableColumnGroup_GetAllCompletedEventArgs> WcTableColumnGroup_GetAllCompleted;
        public event System.EventHandler<WcTableColumnGroup_GetListByFKCompletedEventArgs> WcTableColumnGroup_GetListByFKCompleted;
        public event System.EventHandler<WcTableColumnGroup_SaveCompletedEventArgs> WcTableColumnGroup_SaveCompleted;
        public event System.EventHandler<WcTableColumnGroup_DeleteCompletedEventArgs> WcTableColumnGroup_DeleteCompleted;
        public event System.EventHandler<WcTableColumnGroup_SetIsDeletedCompletedEventArgs> WcTableColumnGroup_SetIsDeletedCompleted;
        public event System.EventHandler<WcTableColumnGroup_DeactivateCompletedEventArgs> WcTableColumnGroup_DeactivateCompleted;
        public event System.EventHandler<WcTableColumnGroup_RemoveCompletedEventArgs> WcTableColumnGroup_RemoveCompleted;
		public event System.EventHandler<GetWcTableColumnGroup_lstByParentIdCompletedEventArgs> GetWcTableColumnGroup_lstByParentIdCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcTableColumnGroup_GetById

        public void Begin_WcTableColumnGroup_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_GetById", IfxTraceCategory.Enter);
                WcTableColumnGroupServiceClient proxy = new WcTableColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumnGroup_GetByIdCompleted += new EventHandler<WcTableColumnGroup_GetByIdCompletedEventArgs>(proxy_WcTableColumnGroup_GetByIdCompleted);
                proxy.WcTableColumnGroup_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnGroup_GetByIdCompleted(object sender, WcTableColumnGroup_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnGroup_GetByIdCompletedEventArgs> handler = WcTableColumnGroup_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnGroup_GetById

        #region WcTableColumnGroup_GetAll

        public void Begin_WcTableColumnGroup_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_GetAll", IfxTraceCategory.Enter);
                WcTableColumnGroupServiceClient proxy = new WcTableColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumnGroup_GetAllCompleted += new EventHandler<WcTableColumnGroup_GetAllCompletedEventArgs>(proxy_WcTableColumnGroup_GetAllCompleted);
                proxy.WcTableColumnGroup_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnGroup_GetAllCompleted(object sender, WcTableColumnGroup_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnGroup_GetAllCompletedEventArgs> handler = WcTableColumnGroup_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnGroup_GetAll

        #region WcTableColumnGroup_GetListByFK

        public void Begin_WcTableColumnGroup_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_GetListByFK", IfxTraceCategory.Enter);
                WcTableColumnGroupServiceClient proxy = new WcTableColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumnGroup_GetListByFKCompleted += new EventHandler<WcTableColumnGroup_GetListByFKCompletedEventArgs>(proxy_WcTableColumnGroup_GetListByFKCompleted);
                proxy.WcTableColumnGroup_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnGroup_GetListByFKCompleted(object sender, WcTableColumnGroup_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnGroup_GetListByFKCompletedEventArgs> handler = WcTableColumnGroup_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnGroup_GetListByFK

        #region WcTableColumnGroup_Save

        public void Begin_WcTableColumnGroup_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_Save", IfxTraceCategory.Enter);
                WcTableColumnGroupServiceClient proxy = new WcTableColumnGroupServiceClient();
                AssignCredentials(proxy);
                //proxy.WcTableColumnGroup_SaveCompleted += new EventHandler<WcTableColumnGroup_SaveCompletedEventArgs>(proxy_WcTableColumnGroup_SaveCompleted);
                proxy.WcTableColumnGroup_SaveCompleted += new EventHandler<WcTableColumnGroup_SaveCompletedEventArgs>(proxy_WcTableColumnGroup_SaveCompleted);
                proxy.WcTableColumnGroup_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnGroup_SaveCompleted(object sender, WcTableColumnGroup_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnGroup_SaveCompletedEventArgs> handler = WcTableColumnGroup_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnGroup_Save

        #region WcTableColumnGroup_Delete

        public void Begin_WcTableColumnGroup_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_DeleteCompleted", IfxTraceCategory.Enter);
                WcTableColumnGroupServiceClient proxy = new WcTableColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumnGroup_DeleteCompleted += new EventHandler<WcTableColumnGroup_DeleteCompletedEventArgs>(proxy_WcTableColumnGroup_DeleteCompleted);
                proxy.WcTableColumnGroup_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnGroup_DeleteCompleted(object sender, WcTableColumnGroup_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnGroup_DeleteCompletedEventArgs> handler = WcTableColumnGroup_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnGroup_Delete

        #region WcTableColumnGroup_SetIsDeleted

        public void Begin_WcTableColumnGroup_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcTableColumnGroupServiceClient proxy = new WcTableColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumnGroup_SetIsDeletedCompleted += new EventHandler<WcTableColumnGroup_SetIsDeletedCompletedEventArgs>(proxy_WcTableColumnGroup_SetIsDeletedCompleted);
                proxy.WcTableColumnGroup_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnGroup_SetIsDeletedCompleted(object sender, WcTableColumnGroup_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnGroup_SetIsDeletedCompletedEventArgs> handler = WcTableColumnGroup_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnGroup_SetIsDeleted

        #region WcTableColumnGroup_Deactivate

        public void Begin_WcTableColumnGroup_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_Deactivate", IfxTraceCategory.Enter);
            WcTableColumnGroupServiceClient proxy = new WcTableColumnGroupServiceClient();
            AssignCredentials(proxy);
            proxy.WcTableColumnGroup_DeactivateCompleted += new EventHandler<WcTableColumnGroup_DeactivateCompletedEventArgs>(proxy_WcTableColumnGroup_DeactivateCompleted);
            proxy.WcTableColumnGroup_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnGroup_DeactivateCompleted(object sender, WcTableColumnGroup_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnGroup_DeactivateCompletedEventArgs> handler = WcTableColumnGroup_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnGroup_Deactivate

        #region WcTableColumnGroup_Remove

        public void Begin_WcTableColumnGroup_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_Remove", IfxTraceCategory.Enter);
                WcTableColumnGroupServiceClient proxy = new WcTableColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumnGroup_RemoveCompleted += new EventHandler<WcTableColumnGroup_RemoveCompletedEventArgs>(proxy_WcTableColumnGroup_RemoveCompleted);
                proxy.WcTableColumnGroup_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnGroup_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnGroup_RemoveCompleted(object sender, WcTableColumnGroup_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnGroup_RemoveCompletedEventArgs> handler = WcTableColumnGroup_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcTableColumnGroup_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #region GetWcTableColumnGroup_lstByParentId

        public void Begin_GetWcTableColumnGroup_lstByParentId(Guid? TbCGrp_ApVrsn_Id, Guid? TbCGrp_Tb_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumnGroup_lstByParentId", IfxTraceCategory.Enter);
                WcTableColumnGroupServiceClient proxy = new WcTableColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcTableColumnGroup_lstByParentIdCompleted += new EventHandler<GetWcTableColumnGroup_lstByParentIdCompletedEventArgs>(proxy_GetWcTableColumnGroup_lstByParentIdCompleted);
                proxy.GetWcTableColumnGroup_lstByParentIdAsync(TbCGrp_ApVrsn_Id, TbCGrp_Tb_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumnGroup_lstByParentId", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumnGroup_lstByParentId", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcTableColumnGroup_lstByParentIdCompleted(object sender, GetWcTableColumnGroup_lstByParentIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnGroup_lstByParentId", IfxTraceCategory.Enter);
                System.EventHandler<GetWcTableColumnGroup_lstByParentIdCompletedEventArgs> handler = GetWcTableColumnGroup_lstByParentIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnGroup_lstByParentId", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnGroup_lstByParentId", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcTableColumnGroup_lstByParentId

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


