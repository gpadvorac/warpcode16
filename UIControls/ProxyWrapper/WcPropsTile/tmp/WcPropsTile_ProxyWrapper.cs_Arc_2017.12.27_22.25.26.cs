using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/27/2017 6:34:56 PM

namespace ProxyWrapper
{
    public partial class WcPropsTileService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcPropsTile_GetByIdCompletedEventArgs> WcPropsTile_GetByIdCompleted;
        public event System.EventHandler<WcPropsTile_GetAllCompletedEventArgs> WcPropsTile_GetAllCompleted;
        public event System.EventHandler<WcPropsTile_GetListByFKCompletedEventArgs> WcPropsTile_GetListByFKCompleted;
        public event System.EventHandler<WcPropsTile_SaveCompletedEventArgs> WcPropsTile_SaveCompleted;
        public event System.EventHandler<WcPropsTile_DeleteCompletedEventArgs> WcPropsTile_DeleteCompleted;
        public event System.EventHandler<WcPropsTile_SetIsDeletedCompletedEventArgs> WcPropsTile_SetIsDeletedCompleted;
        public event System.EventHandler<WcPropsTile_DeactivateCompletedEventArgs> WcPropsTile_DeactivateCompleted;
        public event System.EventHandler<WcPropsTile_RemoveCompletedEventArgs> WcPropsTile_RemoveCompleted;
		public event System.EventHandler<GetWcPropsTileItem_lstAssignedCompletedEventArgs> GetWcPropsTileItem_lstAssignedCompleted;
		public event System.EventHandler<GetWcPropsTileItem_lstNotAssignedCompletedEventArgs> GetWcPropsTileItem_lstNotAssignedCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcPropsTile_GetById

        public void Begin_WcPropsTile_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_GetById", IfxTraceCategory.Enter);
                WcPropsTileServiceClient proxy = new WcPropsTileServiceClient();
                AssignCredentials(proxy);
                proxy.WcPropsTile_GetByIdCompleted += new EventHandler<WcPropsTile_GetByIdCompletedEventArgs>(proxy_WcPropsTile_GetByIdCompleted);
                proxy.WcPropsTile_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcPropsTile_GetByIdCompleted(object sender, WcPropsTile_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcPropsTile_GetByIdCompletedEventArgs> handler = WcPropsTile_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcPropsTile_GetById

        #region WcPropsTile_GetAll

        public void Begin_WcPropsTile_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_GetAll", IfxTraceCategory.Enter);
                WcPropsTileServiceClient proxy = new WcPropsTileServiceClient();
                AssignCredentials(proxy);
                proxy.WcPropsTile_GetAllCompleted += new EventHandler<WcPropsTile_GetAllCompletedEventArgs>(proxy_WcPropsTile_GetAllCompleted);
                proxy.WcPropsTile_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcPropsTile_GetAllCompleted(object sender, WcPropsTile_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcPropsTile_GetAllCompletedEventArgs> handler = WcPropsTile_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcPropsTile_GetAll

        #region WcPropsTile_GetListByFK

        public void Begin_WcPropsTile_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_GetListByFK", IfxTraceCategory.Enter);
                WcPropsTileServiceClient proxy = new WcPropsTileServiceClient();
                AssignCredentials(proxy);
                proxy.WcPropsTile_GetListByFKCompleted += new EventHandler<WcPropsTile_GetListByFKCompletedEventArgs>(proxy_WcPropsTile_GetListByFKCompleted);
                proxy.WcPropsTile_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcPropsTile_GetListByFKCompleted(object sender, WcPropsTile_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcPropsTile_GetListByFKCompletedEventArgs> handler = WcPropsTile_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcPropsTile_GetListByFK

        #region WcPropsTile_Save

        public void Begin_WcPropsTile_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_Save", IfxTraceCategory.Enter);
                WcPropsTileServiceClient proxy = new WcPropsTileServiceClient();
                AssignCredentials(proxy);
                //proxy.WcPropsTile_SaveCompleted += new EventHandler<WcPropsTile_SaveCompletedEventArgs>(proxy_WcPropsTile_SaveCompleted);
                proxy.WcPropsTile_SaveCompleted += new EventHandler<WcPropsTile_SaveCompletedEventArgs>(proxy_WcPropsTile_SaveCompleted);
                proxy.WcPropsTile_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcPropsTile_SaveCompleted(object sender, WcPropsTile_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcPropsTile_SaveCompletedEventArgs> handler = WcPropsTile_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcPropsTile_Save

        #region WcPropsTile_Delete

        public void Begin_WcPropsTile_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_DeleteCompleted", IfxTraceCategory.Enter);
                WcPropsTileServiceClient proxy = new WcPropsTileServiceClient();
                AssignCredentials(proxy);
                proxy.WcPropsTile_DeleteCompleted += new EventHandler<WcPropsTile_DeleteCompletedEventArgs>(proxy_WcPropsTile_DeleteCompleted);
                proxy.WcPropsTile_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcPropsTile_DeleteCompleted(object sender, WcPropsTile_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcPropsTile_DeleteCompletedEventArgs> handler = WcPropsTile_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcPropsTile_Delete

        #region WcPropsTile_SetIsDeleted

        public void Begin_WcPropsTile_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcPropsTileServiceClient proxy = new WcPropsTileServiceClient();
                AssignCredentials(proxy);
                proxy.WcPropsTile_SetIsDeletedCompleted += new EventHandler<WcPropsTile_SetIsDeletedCompletedEventArgs>(proxy_WcPropsTile_SetIsDeletedCompleted);
                proxy.WcPropsTile_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcPropsTile_SetIsDeletedCompleted(object sender, WcPropsTile_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcPropsTile_SetIsDeletedCompletedEventArgs> handler = WcPropsTile_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcPropsTile_SetIsDeleted

        #region WcPropsTile_Deactivate

        public void Begin_WcPropsTile_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_Deactivate", IfxTraceCategory.Enter);
            WcPropsTileServiceClient proxy = new WcPropsTileServiceClient();
            AssignCredentials(proxy);
            proxy.WcPropsTile_DeactivateCompleted += new EventHandler<WcPropsTile_DeactivateCompletedEventArgs>(proxy_WcPropsTile_DeactivateCompleted);
            proxy.WcPropsTile_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcPropsTile_DeactivateCompleted(object sender, WcPropsTile_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcPropsTile_DeactivateCompletedEventArgs> handler = WcPropsTile_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcPropsTile_Deactivate

        #region WcPropsTile_Remove

        public void Begin_WcPropsTile_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_Remove", IfxTraceCategory.Enter);
                WcPropsTileServiceClient proxy = new WcPropsTileServiceClient();
                AssignCredentials(proxy);
                proxy.WcPropsTile_RemoveCompleted += new EventHandler<WcPropsTile_RemoveCompletedEventArgs>(proxy_WcPropsTile_RemoveCompleted);
                proxy.WcPropsTile_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcPropsTile_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcPropsTile_RemoveCompleted(object sender, WcPropsTile_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcPropsTile_RemoveCompletedEventArgs> handler = WcPropsTile_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcPropsTile_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #region GetWcPropsTileItem_lstAssigned

        public void Begin_GetWcPropsTileItem_lstAssigned(Guid PrpTl_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcPropsTileItem_lstAssigned", IfxTraceCategory.Enter);
                WcPropsTileServiceClient proxy = new WcPropsTileServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcPropsTileItem_lstAssignedCompleted += new EventHandler<GetWcPropsTileItem_lstAssignedCompletedEventArgs>(proxy_GetWcPropsTileItem_lstAssignedCompleted);
                proxy.GetWcPropsTileItem_lstAssignedAsync(PrpTl_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcPropsTileItem_lstAssigned", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcPropsTileItem_lstAssigned", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcPropsTileItem_lstAssignedCompleted(object sender, GetWcPropsTileItem_lstAssignedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcPropsTileItem_lstAssigned", IfxTraceCategory.Enter);
                System.EventHandler<GetWcPropsTileItem_lstAssignedCompletedEventArgs> handler = GetWcPropsTileItem_lstAssignedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcPropsTileItem_lstAssigned", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcPropsTileItem_lstAssigned", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcPropsTileItem_lstAssigned

        #region GetWcPropsTileItem_lstNotAssigned

        public void Begin_GetWcPropsTileItem_lstNotAssigned(Guid Tb_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcPropsTileItem_lstNotAssigned", IfxTraceCategory.Enter);
                WcPropsTileServiceClient proxy = new WcPropsTileServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcPropsTileItem_lstNotAssignedCompleted += new EventHandler<GetWcPropsTileItem_lstNotAssignedCompletedEventArgs>(proxy_GetWcPropsTileItem_lstNotAssignedCompleted);
                proxy.GetWcPropsTileItem_lstNotAssignedAsync(Tb_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcPropsTileItem_lstNotAssigned", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcPropsTileItem_lstNotAssigned", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcPropsTileItem_lstNotAssignedCompleted(object sender, GetWcPropsTileItem_lstNotAssignedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcPropsTileItem_lstNotAssigned", IfxTraceCategory.Enter);
                System.EventHandler<GetWcPropsTileItem_lstNotAssignedCompletedEventArgs> handler = GetWcPropsTileItem_lstNotAssignedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcPropsTileItem_lstNotAssigned", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcPropsTileItem_lstNotAssigned", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcPropsTileItem_lstNotAssigned

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


