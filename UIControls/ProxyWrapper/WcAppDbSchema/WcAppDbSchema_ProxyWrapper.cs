using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  1/7/2018 10:51:55 PM

namespace ProxyWrapper
{
    public partial class WcAppDbSchemaService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcAppDbSchema_GetByIdCompletedEventArgs> WcAppDbSchema_GetByIdCompleted;
        public event System.EventHandler<WcAppDbSchema_GetAllCompletedEventArgs> WcAppDbSchema_GetAllCompleted;
        public event System.EventHandler<WcAppDbSchema_GetListByFKCompletedEventArgs> WcAppDbSchema_GetListByFKCompleted;
        public event System.EventHandler<WcAppDbSchema_SaveCompletedEventArgs> WcAppDbSchema_SaveCompleted;
        public event System.EventHandler<WcAppDbSchema_DeleteCompletedEventArgs> WcAppDbSchema_DeleteCompleted;
        public event System.EventHandler<WcAppDbSchema_SetIsDeletedCompletedEventArgs> WcAppDbSchema_SetIsDeletedCompleted;
        public event System.EventHandler<WcAppDbSchema_DeactivateCompletedEventArgs> WcAppDbSchema_DeactivateCompleted;
        public event System.EventHandler<WcAppDbSchema_RemoveCompletedEventArgs> WcAppDbSchema_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcAppDbSchema_GetById

        public void Begin_WcAppDbSchema_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_GetById", IfxTraceCategory.Enter);
                WcAppDbSchemaServiceClient proxy = new WcAppDbSchemaServiceClient();
                AssignCredentials(proxy);
                proxy.WcAppDbSchema_GetByIdCompleted += new EventHandler<WcAppDbSchema_GetByIdCompletedEventArgs>(proxy_WcAppDbSchema_GetByIdCompleted);
                proxy.WcAppDbSchema_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcAppDbSchema_GetByIdCompleted(object sender, WcAppDbSchema_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcAppDbSchema_GetByIdCompletedEventArgs> handler = WcAppDbSchema_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcAppDbSchema_GetById

        #region WcAppDbSchema_GetAll

        public void Begin_WcAppDbSchema_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_GetAll", IfxTraceCategory.Enter);
                WcAppDbSchemaServiceClient proxy = new WcAppDbSchemaServiceClient();
                AssignCredentials(proxy);
                proxy.WcAppDbSchema_GetAllCompleted += new EventHandler<WcAppDbSchema_GetAllCompletedEventArgs>(proxy_WcAppDbSchema_GetAllCompleted);
                proxy.WcAppDbSchema_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcAppDbSchema_GetAllCompleted(object sender, WcAppDbSchema_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcAppDbSchema_GetAllCompletedEventArgs> handler = WcAppDbSchema_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcAppDbSchema_GetAll

        #region WcAppDbSchema_GetListByFK

        public void Begin_WcAppDbSchema_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_GetListByFK", IfxTraceCategory.Enter);
                WcAppDbSchemaServiceClient proxy = new WcAppDbSchemaServiceClient();
                AssignCredentials(proxy);
                proxy.WcAppDbSchema_GetListByFKCompleted += new EventHandler<WcAppDbSchema_GetListByFKCompletedEventArgs>(proxy_WcAppDbSchema_GetListByFKCompleted);
                proxy.WcAppDbSchema_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcAppDbSchema_GetListByFKCompleted(object sender, WcAppDbSchema_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcAppDbSchema_GetListByFKCompletedEventArgs> handler = WcAppDbSchema_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcAppDbSchema_GetListByFK

        #region WcAppDbSchema_Save

        public void Begin_WcAppDbSchema_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_Save", IfxTraceCategory.Enter);
                WcAppDbSchemaServiceClient proxy = new WcAppDbSchemaServiceClient();
                AssignCredentials(proxy);
                //proxy.WcAppDbSchema_SaveCompleted += new EventHandler<WcAppDbSchema_SaveCompletedEventArgs>(proxy_WcAppDbSchema_SaveCompleted);
                proxy.WcAppDbSchema_SaveCompleted += new EventHandler<WcAppDbSchema_SaveCompletedEventArgs>(proxy_WcAppDbSchema_SaveCompleted);
                proxy.WcAppDbSchema_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcAppDbSchema_SaveCompleted(object sender, WcAppDbSchema_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcAppDbSchema_SaveCompletedEventArgs> handler = WcAppDbSchema_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcAppDbSchema_Save

        #region WcAppDbSchema_Delete

        public void Begin_WcAppDbSchema_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_DeleteCompleted", IfxTraceCategory.Enter);
                WcAppDbSchemaServiceClient proxy = new WcAppDbSchemaServiceClient();
                AssignCredentials(proxy);
                proxy.WcAppDbSchema_DeleteCompleted += new EventHandler<WcAppDbSchema_DeleteCompletedEventArgs>(proxy_WcAppDbSchema_DeleteCompleted);
                proxy.WcAppDbSchema_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcAppDbSchema_DeleteCompleted(object sender, WcAppDbSchema_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcAppDbSchema_DeleteCompletedEventArgs> handler = WcAppDbSchema_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcAppDbSchema_Delete

        #region WcAppDbSchema_SetIsDeleted

        public void Begin_WcAppDbSchema_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcAppDbSchemaServiceClient proxy = new WcAppDbSchemaServiceClient();
                AssignCredentials(proxy);
                proxy.WcAppDbSchema_SetIsDeletedCompleted += new EventHandler<WcAppDbSchema_SetIsDeletedCompletedEventArgs>(proxy_WcAppDbSchema_SetIsDeletedCompleted);
                proxy.WcAppDbSchema_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcAppDbSchema_SetIsDeletedCompleted(object sender, WcAppDbSchema_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcAppDbSchema_SetIsDeletedCompletedEventArgs> handler = WcAppDbSchema_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcAppDbSchema_SetIsDeleted

        #region WcAppDbSchema_Deactivate

        public void Begin_WcAppDbSchema_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_Deactivate", IfxTraceCategory.Enter);
            WcAppDbSchemaServiceClient proxy = new WcAppDbSchemaServiceClient();
            AssignCredentials(proxy);
            proxy.WcAppDbSchema_DeactivateCompleted += new EventHandler<WcAppDbSchema_DeactivateCompletedEventArgs>(proxy_WcAppDbSchema_DeactivateCompleted);
            proxy.WcAppDbSchema_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcAppDbSchema_DeactivateCompleted(object sender, WcAppDbSchema_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcAppDbSchema_DeactivateCompletedEventArgs> handler = WcAppDbSchema_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcAppDbSchema_Deactivate

        #region WcAppDbSchema_Remove

        public void Begin_WcAppDbSchema_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_Remove", IfxTraceCategory.Enter);
                WcAppDbSchemaServiceClient proxy = new WcAppDbSchemaServiceClient();
                AssignCredentials(proxy);
                proxy.WcAppDbSchema_RemoveCompleted += new EventHandler<WcAppDbSchema_RemoveCompletedEventArgs>(proxy_WcAppDbSchema_RemoveCompleted);
                proxy.WcAppDbSchema_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcAppDbSchema_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcAppDbSchema_RemoveCompleted(object sender, WcAppDbSchema_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcAppDbSchema_RemoveCompletedEventArgs> handler = WcAppDbSchema_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcAppDbSchema_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


