using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/2/2016 1:01:24 PM

namespace ProxyWrapper
{
    public partial class WcTableService_ProxyWrapper
    {

        #region Initialize Variables


        public event System.EventHandler<TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs> TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted;
        public event System.EventHandler<TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs> TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted;
        public event System.EventHandler<TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs> TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted;



        #endregion Initialize Variables





        #region Combo Datasources


        #region TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList

        /// <summary>
        /// http://stackoverflow.com/questions/1449183/asynchronous-callback-async-end-called-with-an-iasyncresult-from-a-different-be
        /// </summary>
        /// <param name="Tb_Id"></param>
        /// <returns></returns>
        public byte[] TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList_Syncronous(Guid Tb_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                WcTableService asInterface = proxy;
                byte[] result = asInterface.EndTbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList(
                    asInterface.BeginTbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList(Tb_Id, null, null));
                proxy.CloseAsync();
                return result;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public void Begin_TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList(Guid Tb_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted += proxy_TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted;
                proxy.TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListAsync(Tb_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted(object sender, TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs> handler = TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList


        #region TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList

        public void Begin_TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList(Guid Tb_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted += proxy_TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted;
                proxy.TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListAsync(Tb_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted(object sender, TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs> handler = TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList


        #region TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList

        public void Begin_TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList(Guid Tb_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted += proxy_TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted;
                proxy.TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListAsync(Tb_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted(object sender, TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs> handler = TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList


        #endregion Combo Datasources



    }
}


