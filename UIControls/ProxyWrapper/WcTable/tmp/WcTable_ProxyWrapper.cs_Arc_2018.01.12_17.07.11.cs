using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  1/11/2018 6:38:10 PM

namespace ProxyWrapper
{
    public partial class WcTableService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcTable_GetByIdCompletedEventArgs> WcTable_GetByIdCompleted;
        public event System.EventHandler<WcTable_GetAllCompletedEventArgs> WcTable_GetAllCompleted;
        public event System.EventHandler<WcTable_GetListByFKCompletedEventArgs> WcTable_GetListByFKCompleted;
        public event System.EventHandler<WcTable_SaveCompletedEventArgs> WcTable_SaveCompleted;
        public event System.EventHandler<WcTable_DeleteCompletedEventArgs> WcTable_DeleteCompleted;
        public event System.EventHandler<WcTable_SetIsDeletedCompletedEventArgs> WcTable_SetIsDeletedCompleted;
        public event System.EventHandler<WcTable_DeactivateCompletedEventArgs> WcTable_DeactivateCompleted;
        public event System.EventHandler<WcTable_RemoveCompletedEventArgs> WcTable_RemoveCompleted;

        public event System.EventHandler<GetWcTable_ReadOnlyStaticListsCompletedEventArgs> GetWcTable_ReadOnlyStaticListsCompleted;
		public event System.EventHandler<GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs> GetWcTableColumn_lstByTable_ComboItemListCompleted;
		public event System.EventHandler<GetwcTableGroup_AssignmentsCompletedEventArgs> GetwcTableGroup_AssignmentsCompleted;
		public event System.EventHandler<ExecutewcTableGroup_AssignTablesCompletedEventArgs> ExecutewcTableGroup_AssignTablesCompleted;
		public event System.EventHandler<GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyIdCompletedEventArgs> GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyIdCompleted;
		public event System.EventHandler<ExecuteWcTable_InsertTable_ByConnectionKeyIdCompletedEventArgs> ExecuteWcTable_InsertTable_ByConnectionKeyIdCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcTable_GetById

        public void Begin_WcTable_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_GetById", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.WcTable_GetByIdCompleted += new EventHandler<WcTable_GetByIdCompletedEventArgs>(proxy_WcTable_GetByIdCompleted);
                proxy.WcTable_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTable_GetByIdCompleted(object sender, WcTable_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTable_GetByIdCompletedEventArgs> handler = WcTable_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTable_GetById

        #region WcTable_GetAll

        public void Begin_WcTable_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_GetAll", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.WcTable_GetAllCompleted += new EventHandler<WcTable_GetAllCompletedEventArgs>(proxy_WcTable_GetAllCompleted);
                proxy.WcTable_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTable_GetAllCompleted(object sender, WcTable_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTable_GetAllCompletedEventArgs> handler = WcTable_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTable_GetAll

        #region WcTable_GetListByFK

        public void Begin_WcTable_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_GetListByFK", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.WcTable_GetListByFKCompleted += new EventHandler<WcTable_GetListByFKCompletedEventArgs>(proxy_WcTable_GetListByFKCompleted);
                proxy.WcTable_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTable_GetListByFKCompleted(object sender, WcTable_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTable_GetListByFKCompletedEventArgs> handler = WcTable_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTable_GetListByFK

        #region WcTable_Save

        public void Begin_WcTable_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_Save", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                //proxy.WcTable_SaveCompleted += new EventHandler<WcTable_SaveCompletedEventArgs>(proxy_WcTable_SaveCompleted);
                proxy.WcTable_SaveCompleted += new EventHandler<WcTable_SaveCompletedEventArgs>(proxy_WcTable_SaveCompleted);
                proxy.WcTable_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTable_SaveCompleted(object sender, WcTable_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTable_SaveCompletedEventArgs> handler = WcTable_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTable_Save

        #region WcTable_Delete

        public void Begin_WcTable_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_DeleteCompleted", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.WcTable_DeleteCompleted += new EventHandler<WcTable_DeleteCompletedEventArgs>(proxy_WcTable_DeleteCompleted);
                proxy.WcTable_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTable_DeleteCompleted(object sender, WcTable_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTable_DeleteCompletedEventArgs> handler = WcTable_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTable_Delete

        #region WcTable_SetIsDeleted

        public void Begin_WcTable_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.WcTable_SetIsDeletedCompleted += new EventHandler<WcTable_SetIsDeletedCompletedEventArgs>(proxy_WcTable_SetIsDeletedCompleted);
                proxy.WcTable_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTable_SetIsDeletedCompleted(object sender, WcTable_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTable_SetIsDeletedCompletedEventArgs> handler = WcTable_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTable_SetIsDeleted

        #region WcTable_Deactivate

        public void Begin_WcTable_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_Deactivate", IfxTraceCategory.Enter);
            WcTableServiceClient proxy = new WcTableServiceClient();
            AssignCredentials(proxy);
            proxy.WcTable_DeactivateCompleted += new EventHandler<WcTable_DeactivateCompletedEventArgs>(proxy_WcTable_DeactivateCompleted);
            proxy.WcTable_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTable_DeactivateCompleted(object sender, WcTable_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTable_DeactivateCompletedEventArgs> handler = WcTable_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTable_Deactivate

        #region WcTable_Remove

        public void Begin_WcTable_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_Remove", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.WcTable_RemoveCompleted += new EventHandler<WcTable_RemoveCompletedEventArgs>(proxy_WcTable_RemoveCompleted);
                proxy.WcTable_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTable_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTable_RemoveCompleted(object sender, WcTable_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTable_RemoveCompletedEventArgs> handler = WcTable_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcTable_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists

        public void Begin_GetWcTable_ReadOnlyStaticLists(Guid Tb_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTable_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcTable_ReadOnlyStaticListsCompleted += new EventHandler<GetWcTable_ReadOnlyStaticListsCompletedEventArgs>(proxy_GetWcTable_ReadOnlyStaticListsCompleted);
                proxy.GetWcTable_ReadOnlyStaticListsAsync(Tb_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTable_ReadOnlyStaticLists", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTable_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcTable_ReadOnlyStaticListsCompleted(object sender, GetWcTable_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetWcTable_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                System.EventHandler<GetWcTable_ReadOnlyStaticListsCompletedEventArgs> handler = GetWcTable_ReadOnlyStaticListsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetWcTable_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetWcTable_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion ReadOnlyStaticLists

        #region GetWcTableColumn_lstByTable_ComboItemList

        public void Begin_GetWcTableColumn_lstByTable_ComboItemList(Guid Tb_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcTableColumn_lstByTable_ComboItemListCompleted += new EventHandler<GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs>(proxy_GetWcTableColumn_lstByTable_ComboItemListCompleted);
                proxy.GetWcTableColumn_lstByTable_ComboItemListAsync(Tb_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumn_lstByTable_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcTableColumn_lstByTable_ComboItemListCompleted(object sender, GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs> handler = GetWcTableColumn_lstByTable_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByTable_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcTableColumn_lstByTable_ComboItemList

        #region GetwcTableGroup_Assignments

        public void Begin_GetwcTableGroup_Assignments(Guid ApVrsn_Id, Guid Tb_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetwcTableGroup_Assignments", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.GetwcTableGroup_AssignmentsCompleted += new EventHandler<GetwcTableGroup_AssignmentsCompletedEventArgs>(proxy_GetwcTableGroup_AssignmentsCompleted);
                proxy.GetwcTableGroup_AssignmentsAsync(ApVrsn_Id, Tb_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetwcTableGroup_Assignments", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetwcTableGroup_Assignments", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetwcTableGroup_AssignmentsCompleted(object sender, GetwcTableGroup_AssignmentsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableGroup_Assignments", IfxTraceCategory.Enter);
                System.EventHandler<GetwcTableGroup_AssignmentsCompletedEventArgs> handler = GetwcTableGroup_AssignmentsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableGroup_Assignments", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableGroup_Assignments", IfxTraceCategory.Leave);
            }
        }

        #endregion GetwcTableGroup_Assignments

        #region ExecutewcTableGroup_AssignTables

        public void Begin_ExecutewcTableGroup_AssignTables(Boolean Insert, Guid TbGrp2Tb_TbGrp_Id, Guid TbGrp2Tb_Tb_Id, Guid TbGrp2Tb_CreatedUserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecutewcTableGroup_AssignTables", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.ExecutewcTableGroup_AssignTablesCompleted += new EventHandler<ExecutewcTableGroup_AssignTablesCompletedEventArgs>(proxy_ExecutewcTableGroup_AssignTablesCompleted);
                proxy.ExecutewcTableGroup_AssignTablesAsync(Insert, TbGrp2Tb_TbGrp_Id, TbGrp2Tb_Tb_Id, TbGrp2Tb_CreatedUserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecutewcTableGroup_AssignTables", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecutewcTableGroup_AssignTables", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecutewcTableGroup_AssignTablesCompleted(object sender, ExecutewcTableGroup_AssignTablesCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableGroup_AssignTables", IfxTraceCategory.Enter);
                System.EventHandler<ExecutewcTableGroup_AssignTablesCompletedEventArgs> handler = ExecutewcTableGroup_AssignTablesCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableGroup_AssignTables", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableGroup_AssignTables", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecutewcTableGroup_AssignTables

        #region GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyId

        public void Begin_GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyId(Guid DbCnSK_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyId", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyIdCompleted += new EventHandler<GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyIdCompletedEventArgs>(proxy_GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyIdCompleted);
                proxy.GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyIdAsync(DbCnSK_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyId", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyId", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyIdCompleted(object sender, GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyId", IfxTraceCategory.Enter);
                System.EventHandler<GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyIdCompletedEventArgs> handler = GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyId", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyId", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyId

        #region ExecuteWcTable_InsertTable_ByConnectionKeyId

        public void Begin_ExecuteWcTable_InsertTable_ByConnectionKeyId(Guid DbCnSK_Id, String TableName, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcTable_InsertTable_ByConnectionKeyId", IfxTraceCategory.Enter);
                WcTableServiceClient proxy = new WcTableServiceClient();
                AssignCredentials(proxy);
                proxy.ExecuteWcTable_InsertTable_ByConnectionKeyIdCompleted += new EventHandler<ExecuteWcTable_InsertTable_ByConnectionKeyIdCompletedEventArgs>(proxy_ExecuteWcTable_InsertTable_ByConnectionKeyIdCompleted);
                proxy.ExecuteWcTable_InsertTable_ByConnectionKeyIdAsync(DbCnSK_Id, TableName, UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcTable_InsertTable_ByConnectionKeyId", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcTable_InsertTable_ByConnectionKeyId", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecuteWcTable_InsertTable_ByConnectionKeyIdCompleted(object sender, ExecuteWcTable_InsertTable_ByConnectionKeyIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTable_InsertTable_ByConnectionKeyId", IfxTraceCategory.Enter);
                System.EventHandler<ExecuteWcTable_InsertTable_ByConnectionKeyIdCompletedEventArgs> handler = ExecuteWcTable_InsertTable_ByConnectionKeyIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTable_InsertTable_ByConnectionKeyId", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTable_InsertTable_ByConnectionKeyId", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecuteWcTable_InsertTable_ByConnectionKeyId

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


