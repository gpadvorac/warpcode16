using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  11/17/2016 8:48:42 PM

namespace ProxyWrapper
{
    public partial class DiscussionSupportService_ProxyWrapper
    {


        #region Events
		public event System.EventHandler<GetDiscussion_DiscussionsImInCompletedEventArgs> GetDiscussion_DiscussionsImInCompleted;
		public event System.EventHandler<GetDiscussion_lstBy_ApplicationCompletedEventArgs> GetDiscussion_lstBy_ApplicationCompleted;
		public event System.EventHandler<GetDiscussion_lstBy_ApplicationMemberCompletedEventArgs> GetDiscussion_lstBy_ApplicationMemberCompleted;
		public event System.EventHandler<GetDiscussion_lstBy_ApplicationMyCompletedEventArgs> GetDiscussion_lstBy_ApplicationMyCompleted;
		public event System.EventHandler<GetDiscussion_lstBy_ApplicationVersionCompletedEventArgs> GetDiscussion_lstBy_ApplicationVersionCompleted;
		public event System.EventHandler<GetDiscussion_lstBy_ApplicationVersionMemberCompletedEventArgs> GetDiscussion_lstBy_ApplicationVersionMemberCompleted;
		public event System.EventHandler<GetDiscussion_lstBy_ApplicationVersionMyCompletedEventArgs> GetDiscussion_lstBy_ApplicationVersionMyCompleted;
		public event System.EventHandler<GetDiscussion_lstBy_OwnerCompletedEventArgs> GetDiscussion_lstBy_OwnerCompleted;
		public event System.EventHandler<GetDiscussion_lstBy_TableCompletedEventArgs> GetDiscussion_lstBy_TableCompleted;
		public event System.EventHandler<GetDiscussion_lstBy_TableMemberCompletedEventArgs> GetDiscussion_lstBy_TableMemberCompleted;
		public event System.EventHandler<GetDiscussion_lstBy_TableMyCompletedEventArgs> GetDiscussion_lstBy_TableMyCompleted;
		public event System.EventHandler<GetDiscussion_lstBy_TaskCompletedEventArgs> GetDiscussion_lstBy_TaskCompleted;
		public event System.EventHandler<GetDiscussion_lstBy_TaskMyCompletedEventArgs> GetDiscussion_lstBy_TaskMyCompleted;
		public event System.EventHandler<GetDiscussion_MyDiscussionsCompletedEventArgs> GetDiscussion_MyDiscussionsCompleted;
		public event System.EventHandler<GetDiscussion_lstBy_TaskMemberCompletedEventArgs> GetDiscussion_lstBy_TaskMemberCompleted;

        #endregion Events

        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #region GetDiscussion_DiscussionsImIn

        public void Begin_GetDiscussion_DiscussionsImIn(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_DiscussionsImIn", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_DiscussionsImInCompleted += new EventHandler<GetDiscussion_DiscussionsImInCompletedEventArgs>(proxy_GetDiscussion_DiscussionsImInCompleted);
                proxy.GetDiscussion_DiscussionsImInAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_DiscussionsImIn", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_DiscussionsImIn", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_DiscussionsImInCompleted(object sender, GetDiscussion_DiscussionsImInCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_DiscussionsImInCompletedEventArgs> handler = GetDiscussion_DiscussionsImInCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_DiscussionsImIn

        #region GetDiscussion_lstBy_Application

        public void Begin_GetDiscussion_lstBy_Application(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_Application", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_lstBy_ApplicationCompleted += new EventHandler<GetDiscussion_lstBy_ApplicationCompletedEventArgs>(proxy_GetDiscussion_lstBy_ApplicationCompleted);
                proxy.GetDiscussion_lstBy_ApplicationAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_Application", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_Application", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_lstBy_ApplicationCompleted(object sender, GetDiscussion_lstBy_ApplicationCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Application", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_lstBy_ApplicationCompletedEventArgs> handler = GetDiscussion_lstBy_ApplicationCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Application", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Application", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_lstBy_Application

        #region GetDiscussion_lstBy_ApplicationMember

        public void Begin_GetDiscussion_lstBy_ApplicationMember(Guid Id, Guid MemberId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationMember", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_lstBy_ApplicationMemberCompleted += new EventHandler<GetDiscussion_lstBy_ApplicationMemberCompletedEventArgs>(proxy_GetDiscussion_lstBy_ApplicationMemberCompleted);
                proxy.GetDiscussion_lstBy_ApplicationMemberAsync(Id, MemberId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationMember", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationMember", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_lstBy_ApplicationMemberCompleted(object sender, GetDiscussion_lstBy_ApplicationMemberCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMember", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_lstBy_ApplicationMemberCompletedEventArgs> handler = GetDiscussion_lstBy_ApplicationMemberCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMember", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMember", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_lstBy_ApplicationMember

        #region GetDiscussion_lstBy_ApplicationMy

        public void Begin_GetDiscussion_lstBy_ApplicationMy(Guid Id, Guid OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationMy", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_lstBy_ApplicationMyCompleted += new EventHandler<GetDiscussion_lstBy_ApplicationMyCompletedEventArgs>(proxy_GetDiscussion_lstBy_ApplicationMyCompleted);
                proxy.GetDiscussion_lstBy_ApplicationMyAsync(Id, OwnerId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationMy", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationMy", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_lstBy_ApplicationMyCompleted(object sender, GetDiscussion_lstBy_ApplicationMyCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMy", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_lstBy_ApplicationMyCompletedEventArgs> handler = GetDiscussion_lstBy_ApplicationMyCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMy", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMy", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_lstBy_ApplicationMy

        #region GetDiscussion_lstBy_ApplicationVersion

        public void Begin_GetDiscussion_lstBy_ApplicationVersion(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationVersion", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_lstBy_ApplicationVersionCompleted += new EventHandler<GetDiscussion_lstBy_ApplicationVersionCompletedEventArgs>(proxy_GetDiscussion_lstBy_ApplicationVersionCompleted);
                proxy.GetDiscussion_lstBy_ApplicationVersionAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationVersion", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationVersion", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_lstBy_ApplicationVersionCompleted(object sender, GetDiscussion_lstBy_ApplicationVersionCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersion", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_lstBy_ApplicationVersionCompletedEventArgs> handler = GetDiscussion_lstBy_ApplicationVersionCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersion", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersion", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_lstBy_ApplicationVersion

        #region GetDiscussion_lstBy_ApplicationVersionMember

        public void Begin_GetDiscussion_lstBy_ApplicationVersionMember(Guid Id, Guid MemberId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationVersionMember", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_lstBy_ApplicationVersionMemberCompleted += new EventHandler<GetDiscussion_lstBy_ApplicationVersionMemberCompletedEventArgs>(proxy_GetDiscussion_lstBy_ApplicationVersionMemberCompleted);
                proxy.GetDiscussion_lstBy_ApplicationVersionMemberAsync(Id, MemberId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationVersionMember", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationVersionMember", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_lstBy_ApplicationVersionMemberCompleted(object sender, GetDiscussion_lstBy_ApplicationVersionMemberCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMember", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_lstBy_ApplicationVersionMemberCompletedEventArgs> handler = GetDiscussion_lstBy_ApplicationVersionMemberCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMember", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMember", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_lstBy_ApplicationVersionMember

        #region GetDiscussion_lstBy_ApplicationVersionMy

        public void Begin_GetDiscussion_lstBy_ApplicationVersionMy(Guid Id, Guid OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationVersionMy", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_lstBy_ApplicationVersionMyCompleted += new EventHandler<GetDiscussion_lstBy_ApplicationVersionMyCompletedEventArgs>(proxy_GetDiscussion_lstBy_ApplicationVersionMyCompleted);
                proxy.GetDiscussion_lstBy_ApplicationVersionMyAsync(Id, OwnerId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationVersionMy", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_ApplicationVersionMy", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_lstBy_ApplicationVersionMyCompleted(object sender, GetDiscussion_lstBy_ApplicationVersionMyCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMy", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_lstBy_ApplicationVersionMyCompletedEventArgs> handler = GetDiscussion_lstBy_ApplicationVersionMyCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMy", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMy", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_lstBy_ApplicationVersionMy

        #region GetDiscussion_lstBy_Owner

        public void Begin_GetDiscussion_lstBy_Owner(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_Owner", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_lstBy_OwnerCompleted += new EventHandler<GetDiscussion_lstBy_OwnerCompletedEventArgs>(proxy_GetDiscussion_lstBy_OwnerCompleted);
                proxy.GetDiscussion_lstBy_OwnerAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_Owner", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_Owner", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_lstBy_OwnerCompleted(object sender, GetDiscussion_lstBy_OwnerCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Owner", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_lstBy_OwnerCompletedEventArgs> handler = GetDiscussion_lstBy_OwnerCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Owner", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Owner", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_lstBy_Owner

        #region GetDiscussion_lstBy_Table

        public void Begin_GetDiscussion_lstBy_Table(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_Table", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_lstBy_TableCompleted += new EventHandler<GetDiscussion_lstBy_TableCompletedEventArgs>(proxy_GetDiscussion_lstBy_TableCompleted);
                proxy.GetDiscussion_lstBy_TableAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_Table", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_Table", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_lstBy_TableCompleted(object sender, GetDiscussion_lstBy_TableCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Table", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_lstBy_TableCompletedEventArgs> handler = GetDiscussion_lstBy_TableCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Table", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Table", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_lstBy_Table

        #region GetDiscussion_lstBy_TableMember

        public void Begin_GetDiscussion_lstBy_TableMember(Guid Id, Guid MemberId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_TableMember", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_lstBy_TableMemberCompleted += new EventHandler<GetDiscussion_lstBy_TableMemberCompletedEventArgs>(proxy_GetDiscussion_lstBy_TableMemberCompleted);
                proxy.GetDiscussion_lstBy_TableMemberAsync(Id, MemberId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_TableMember", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_TableMember", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_lstBy_TableMemberCompleted(object sender, GetDiscussion_lstBy_TableMemberCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMember", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_lstBy_TableMemberCompletedEventArgs> handler = GetDiscussion_lstBy_TableMemberCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMember", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMember", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_lstBy_TableMember

        #region GetDiscussion_lstBy_TableMy

        public void Begin_GetDiscussion_lstBy_TableMy(Guid Id, Guid OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_TableMy", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_lstBy_TableMyCompleted += new EventHandler<GetDiscussion_lstBy_TableMyCompletedEventArgs>(proxy_GetDiscussion_lstBy_TableMyCompleted);
                proxy.GetDiscussion_lstBy_TableMyAsync(Id, OwnerId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_TableMy", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_TableMy", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_lstBy_TableMyCompleted(object sender, GetDiscussion_lstBy_TableMyCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMy", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_lstBy_TableMyCompletedEventArgs> handler = GetDiscussion_lstBy_TableMyCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMy", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMy", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_lstBy_TableMy

        #region GetDiscussion_lstBy_Task

        public void Begin_GetDiscussion_lstBy_Task(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_Task", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_lstBy_TaskCompleted += new EventHandler<GetDiscussion_lstBy_TaskCompletedEventArgs>(proxy_GetDiscussion_lstBy_TaskCompleted);
                proxy.GetDiscussion_lstBy_TaskAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_Task", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_Task", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_lstBy_TaskCompleted(object sender, GetDiscussion_lstBy_TaskCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_lstBy_TaskCompletedEventArgs> handler = GetDiscussion_lstBy_TaskCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_lstBy_Task

        #region GetDiscussion_lstBy_TaskMy

        public void Begin_GetDiscussion_lstBy_TaskMy(Guid Id, Guid OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_TaskMy", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_lstBy_TaskMyCompleted += new EventHandler<GetDiscussion_lstBy_TaskMyCompletedEventArgs>(proxy_GetDiscussion_lstBy_TaskMyCompleted);
                proxy.GetDiscussion_lstBy_TaskMyAsync(Id, OwnerId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_TaskMy", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_TaskMy", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_lstBy_TaskMyCompleted(object sender, GetDiscussion_lstBy_TaskMyCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_lstBy_TaskMyCompletedEventArgs> handler = GetDiscussion_lstBy_TaskMyCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_lstBy_TaskMy

        #region GetDiscussion_MyDiscussions

        public void Begin_GetDiscussion_MyDiscussions(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_MyDiscussions", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_MyDiscussionsCompleted += new EventHandler<GetDiscussion_MyDiscussionsCompletedEventArgs>(proxy_GetDiscussion_MyDiscussionsCompleted);
                proxy.GetDiscussion_MyDiscussionsAsync(Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_MyDiscussions", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_MyDiscussions", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_MyDiscussionsCompleted(object sender, GetDiscussion_MyDiscussionsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_MyDiscussionsCompletedEventArgs> handler = GetDiscussion_MyDiscussionsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_MyDiscussions

        #region GetDiscussion_lstBy_TaskMember

        public void Begin_GetDiscussion_lstBy_TaskMember(Guid Id, Guid MemberId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_TaskMember", IfxTraceCategory.Enter);
                DiscussionSupportServiceClient proxy = new DiscussionSupportServiceClient();
                AssignCredentials(proxy);
                proxy.GetDiscussion_lstBy_TaskMemberCompleted += new EventHandler<GetDiscussion_lstBy_TaskMemberCompletedEventArgs>(proxy_GetDiscussion_lstBy_TaskMemberCompleted);
                proxy.GetDiscussion_lstBy_TaskMemberAsync(Id, MemberId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_TaskMember", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetDiscussion_lstBy_TaskMember", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetDiscussion_lstBy_TaskMemberCompleted(object sender, GetDiscussion_lstBy_TaskMemberCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", IfxTraceCategory.Enter);
                System.EventHandler<GetDiscussion_lstBy_TaskMemberCompletedEventArgs> handler = GetDiscussion_lstBy_TaskMemberCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", IfxTraceCategory.Leave);
            }
        }

        #endregion GetDiscussion_lstBy_TaskMember

    }
}


