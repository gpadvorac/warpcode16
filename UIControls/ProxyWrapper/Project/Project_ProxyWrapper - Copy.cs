using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  3/1/2013 9:08:16 AM

namespace ProxyWrapper
{
    public partial class ProjectService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<Project_GetByIdCompletedEventArgs> Project_GetByIdCompleted;
        public event System.EventHandler<Project_GetAllCompletedEventArgs> Project_GetAllCompleted;
        public event System.EventHandler<Project_GetListByFKCompletedEventArgs> Project_GetListByFKCompleted;
        public event System.EventHandler<Project_SaveCompletedEventArgs> Project_SaveCompleted;
        public event System.EventHandler<Project_DeleteCompletedEventArgs> Project_DeleteCompleted;
        public event System.EventHandler<Project_DeactivateCompletedEventArgs> Project_DeactivateCompleted;
        public event System.EventHandler<Project_RemoveCompletedEventArgs> Project_RemoveCompleted;
		public event System.EventHandler<GetPerson_lstByProjectCompletedEventArgs> GetPerson_lstByProjectCompleted;
		public event System.EventHandler<GetPerson_lstNotAssignedToProjectCompletedEventArgs> GetPerson_lstNotAssignedToProjectCompleted;
		public event System.EventHandler<ExecuteProject2Person_assignPersonCompletedEventArgs> ExecuteProject2Person_assignPersonCompleted;
		public event System.EventHandler<ExecuteProject2Person_removePersonCompletedEventArgs> ExecuteProject2Person_removePersonCompleted;
		public event System.EventHandler<GetProject_lstByUserAccessCompletedEventArgs> GetProject_lstByUserAccessCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region Project_GetById

        public void Begin_Project_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_GetById", IfxTraceCategory.Enter);
                ProjectServiceClient proxy = new ProjectServiceClient();
                AssignCredentials(proxy);
                proxy.Project_GetByIdCompleted += new EventHandler<Project_GetByIdCompletedEventArgs>(proxy_Project_GetByIdCompleted);
                proxy.Project_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_Project_GetByIdCompleted(object sender, Project_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Project_GetByIdCompletedEventArgs> handler = Project_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Project_GetById

        #region Project_GetAll

        public void Begin_Project_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_GetAll", IfxTraceCategory.Enter);
                ProjectServiceClient proxy = new ProjectServiceClient();
                AssignCredentials(proxy);
                proxy.Project_GetAllCompleted += new EventHandler<Project_GetAllCompletedEventArgs>(proxy_Project_GetAllCompleted);
                proxy.Project_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_Project_GetAllCompleted(object sender, Project_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Project_GetAllCompletedEventArgs> handler = Project_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Project_GetAll

        #region Project_GetListByFK

        public void Begin_Project_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_GetListByFK", IfxTraceCategory.Enter);
                ProjectServiceClient proxy = new ProjectServiceClient();
                AssignCredentials(proxy);
                proxy.Project_GetListByFKCompleted += new EventHandler<Project_GetListByFKCompletedEventArgs>(proxy_Project_GetListByFKCompleted);
                proxy.Project_GetListByFKAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_Project_GetListByFKCompleted(object sender, Project_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Project_GetListByFKCompletedEventArgs> handler = Project_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Project_GetListByFK

        #region Project_Save

        public void Begin_Project_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_Save", IfxTraceCategory.Enter);
                ProjectServiceClient proxy = new ProjectServiceClient();
                AssignCredentials(proxy);
                //proxy.Project_SaveCompleted += new EventHandler<Project_SaveCompletedEventArgs>(proxy_Project_SaveCompleted);
                proxy.Project_SaveCompleted += new EventHandler<Project_SaveCompletedEventArgs>(proxy_Project_SaveCompleted);
                proxy.Project_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_Project_SaveCompleted(object sender, Project_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Project_SaveCompletedEventArgs> handler = Project_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Project_Save

        #region Project_Delete

        public void Begin_Project_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_DeleteCompleted", IfxTraceCategory.Enter);
                ProjectServiceClient proxy = new ProjectServiceClient();
                AssignCredentials(proxy);
                proxy.Project_DeleteCompleted += new EventHandler<Project_DeleteCompletedEventArgs>(proxy_Project_DeleteCompleted);
                proxy.Project_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_Project_DeleteCompleted(object sender, Project_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Project_DeleteCompletedEventArgs> handler = Project_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Project_Delete

        #region Project_Deactivate

        public void Begin_Project_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_Deactivate", IfxTraceCategory.Enter);
            ProjectServiceClient proxy = new ProjectServiceClient();
            AssignCredentials(proxy);
            proxy.Project_DeactivateCompleted += new EventHandler<Project_DeactivateCompletedEventArgs>(proxy_Project_DeactivateCompleted);
            proxy.Project_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_Project_DeactivateCompleted(object sender, Project_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Project_DeactivateCompletedEventArgs> handler = Project_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Project_Deactivate

        #region Project_Remove

        public void Begin_Project_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_Remove", IfxTraceCategory.Enter);
                ProjectServiceClient proxy = new ProjectServiceClient();
                AssignCredentials(proxy);
                proxy.Project_RemoveCompleted += new EventHandler<Project_RemoveCompletedEventArgs>(proxy_Project_RemoveCompleted);
                proxy.Project_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Project_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_Project_RemoveCompleted(object sender, Project_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Project_RemoveCompletedEventArgs> handler = Project_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion Project_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #region GetPerson_lstByProject

        public void Begin_GetPerson_lstByProject(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_lstByProject", IfxTraceCategory.Enter);
                ProjectServiceClient proxy = new ProjectServiceClient();
                AssignCredentials(proxy);
                proxy.GetPerson_lstByProjectCompleted += new EventHandler<GetPerson_lstByProjectCompletedEventArgs>(proxy_GetPerson_lstByProjectCompleted);
                proxy.GetPerson_lstByProjectAsync(Prj_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_lstByProject", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_lstByProject", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetPerson_lstByProjectCompleted(object sender, GetPerson_lstByProjectCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstByProject", IfxTraceCategory.Enter);
                System.EventHandler<GetPerson_lstByProjectCompletedEventArgs> handler = GetPerson_lstByProjectCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstByProject", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstByProject", IfxTraceCategory.Leave);
            }
        }

        #endregion GetPerson_lstByProject

        #region GetPerson_lstNotAssignedToProject

        public void Begin_GetPerson_lstNotAssignedToProject(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_lstNotAssignedToProject", IfxTraceCategory.Enter);
                ProjectServiceClient proxy = new ProjectServiceClient();
                AssignCredentials(proxy);
                proxy.GetPerson_lstNotAssignedToProjectCompleted += new EventHandler<GetPerson_lstNotAssignedToProjectCompletedEventArgs>(proxy_GetPerson_lstNotAssignedToProjectCompleted);
                proxy.GetPerson_lstNotAssignedToProjectAsync(Prj_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_lstNotAssignedToProject", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_lstNotAssignedToProject", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetPerson_lstNotAssignedToProjectCompleted(object sender, GetPerson_lstNotAssignedToProjectCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToProject", IfxTraceCategory.Enter);
                System.EventHandler<GetPerson_lstNotAssignedToProjectCompletedEventArgs> handler = GetPerson_lstNotAssignedToProjectCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToProject", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToProject", IfxTraceCategory.Leave);
            }
        }

        #endregion GetPerson_lstNotAssignedToProject

        #region ExecuteProject2Person_assignPerson

        public void Begin_ExecuteProject2Person_assignPerson(Guid Prj2Pn_Prj_Id, Guid Prj2Pn_Pn_Id, Guid Prj2Pn_UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteProject2Person_assignPerson", IfxTraceCategory.Enter);
                ProjectServiceClient proxy = new ProjectServiceClient();
                AssignCredentials(proxy);
                proxy.ExecuteProject2Person_assignPersonCompleted += new EventHandler<ExecuteProject2Person_assignPersonCompletedEventArgs>(proxy_ExecuteProject2Person_assignPersonCompleted);
                proxy.ExecuteProject2Person_assignPersonAsync(Prj2Pn_Prj_Id, Prj2Pn_Pn_Id, Prj2Pn_UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteProject2Person_assignPerson", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteProject2Person_assignPerson", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecuteProject2Person_assignPersonCompleted(object sender, ExecuteProject2Person_assignPersonCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteProject2Person_assignPerson", IfxTraceCategory.Enter);
                System.EventHandler<ExecuteProject2Person_assignPersonCompletedEventArgs> handler = ExecuteProject2Person_assignPersonCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteProject2Person_assignPerson", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteProject2Person_assignPerson", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecuteProject2Person_assignPerson

        #region ExecuteProject2Person_removePerson

        public void Begin_ExecuteProject2Person_removePerson(Guid Prj_Id, Guid Pn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteProject2Person_removePerson", IfxTraceCategory.Enter);
                ProjectServiceClient proxy = new ProjectServiceClient();
                AssignCredentials(proxy);
                proxy.ExecuteProject2Person_removePersonCompleted += new EventHandler<ExecuteProject2Person_removePersonCompletedEventArgs>(proxy_ExecuteProject2Person_removePersonCompleted);
                proxy.ExecuteProject2Person_removePersonAsync(Prj_Id, Pn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteProject2Person_removePerson", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteProject2Person_removePerson", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecuteProject2Person_removePersonCompleted(object sender, ExecuteProject2Person_removePersonCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteProject2Person_removePerson", IfxTraceCategory.Enter);
                System.EventHandler<ExecuteProject2Person_removePersonCompletedEventArgs> handler = ExecuteProject2Person_removePersonCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteProject2Person_removePerson", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteProject2Person_removePerson", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecuteProject2Person_removePerson

        #region GetProject_lstByUserAccess

        public void Begin_GetProject_lstByUserAccess(Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetProject_lstByUserAccess", IfxTraceCategory.Enter);
                ProjectServiceClient proxy = new ProjectServiceClient();
                AssignCredentials(proxy);
                proxy.GetProject_lstByUserAccessCompleted += new EventHandler<GetProject_lstByUserAccessCompletedEventArgs>(proxy_GetProject_lstByUserAccessCompleted);
                proxy.GetProject_lstByUserAccessAsync(UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetProject_lstByUserAccess", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetProject_lstByUserAccess", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetProject_lstByUserAccessCompleted(object sender, GetProject_lstByUserAccessCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProject_lstByUserAccess", IfxTraceCategory.Enter);
                System.EventHandler<GetProject_lstByUserAccessCompletedEventArgs> handler = GetProject_lstByUserAccessCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProject_lstByUserAccess", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProject_lstByUserAccess", IfxTraceCategory.Leave);
            }
        }

        #endregion GetProject_lstByUserAccess

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


