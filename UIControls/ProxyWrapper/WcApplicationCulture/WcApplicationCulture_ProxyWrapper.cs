using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/31/2016 8:14:14 PM

namespace ProxyWrapper
{
    public partial class WcApplicationCultureService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcApplicationCulture_GetByIdCompletedEventArgs> WcApplicationCulture_GetByIdCompleted;
        public event System.EventHandler<WcApplicationCulture_GetAllCompletedEventArgs> WcApplicationCulture_GetAllCompleted;
        public event System.EventHandler<WcApplicationCulture_GetListByFKCompletedEventArgs> WcApplicationCulture_GetListByFKCompleted;
        public event System.EventHandler<WcApplicationCulture_SaveCompletedEventArgs> WcApplicationCulture_SaveCompleted;
        public event System.EventHandler<WcApplicationCulture_DeleteCompletedEventArgs> WcApplicationCulture_DeleteCompleted;
        public event System.EventHandler<WcApplicationCulture_SetIsDeletedCompletedEventArgs> WcApplicationCulture_SetIsDeletedCompleted;
        public event System.EventHandler<WcApplicationCulture_DeactivateCompletedEventArgs> WcApplicationCulture_DeactivateCompleted;
        public event System.EventHandler<WcApplicationCulture_RemoveCompletedEventArgs> WcApplicationCulture_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcApplicationCulture_GetById

        public void Begin_WcApplicationCulture_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_GetById", IfxTraceCategory.Enter);
                WcApplicationCultureServiceClient proxy = new WcApplicationCultureServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationCulture_GetByIdCompleted += new EventHandler<WcApplicationCulture_GetByIdCompletedEventArgs>(proxy_WcApplicationCulture_GetByIdCompleted);
                proxy.WcApplicationCulture_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationCulture_GetByIdCompleted(object sender, WcApplicationCulture_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationCulture_GetByIdCompletedEventArgs> handler = WcApplicationCulture_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationCulture_GetById

        #region WcApplicationCulture_GetAll

        public void Begin_WcApplicationCulture_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_GetAll", IfxTraceCategory.Enter);
                WcApplicationCultureServiceClient proxy = new WcApplicationCultureServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationCulture_GetAllCompleted += new EventHandler<WcApplicationCulture_GetAllCompletedEventArgs>(proxy_WcApplicationCulture_GetAllCompleted);
                proxy.WcApplicationCulture_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationCulture_GetAllCompleted(object sender, WcApplicationCulture_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationCulture_GetAllCompletedEventArgs> handler = WcApplicationCulture_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationCulture_GetAll

        #region WcApplicationCulture_GetListByFK

        public void Begin_WcApplicationCulture_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_GetListByFK", IfxTraceCategory.Enter);
                WcApplicationCultureServiceClient proxy = new WcApplicationCultureServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationCulture_GetListByFKCompleted += new EventHandler<WcApplicationCulture_GetListByFKCompletedEventArgs>(proxy_WcApplicationCulture_GetListByFKCompleted);
                proxy.WcApplicationCulture_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationCulture_GetListByFKCompleted(object sender, WcApplicationCulture_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationCulture_GetListByFKCompletedEventArgs> handler = WcApplicationCulture_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationCulture_GetListByFK

        #region WcApplicationCulture_Save

        public void Begin_WcApplicationCulture_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_Save", IfxTraceCategory.Enter);
                WcApplicationCultureServiceClient proxy = new WcApplicationCultureServiceClient();
                AssignCredentials(proxy);
                //proxy.WcApplicationCulture_SaveCompleted += new EventHandler<WcApplicationCulture_SaveCompletedEventArgs>(proxy_WcApplicationCulture_SaveCompleted);
                proxy.WcApplicationCulture_SaveCompleted += new EventHandler<WcApplicationCulture_SaveCompletedEventArgs>(proxy_WcApplicationCulture_SaveCompleted);
                proxy.WcApplicationCulture_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationCulture_SaveCompleted(object sender, WcApplicationCulture_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationCulture_SaveCompletedEventArgs> handler = WcApplicationCulture_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationCulture_Save

        #region WcApplicationCulture_Delete

        public void Begin_WcApplicationCulture_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_DeleteCompleted", IfxTraceCategory.Enter);
                WcApplicationCultureServiceClient proxy = new WcApplicationCultureServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationCulture_DeleteCompleted += new EventHandler<WcApplicationCulture_DeleteCompletedEventArgs>(proxy_WcApplicationCulture_DeleteCompleted);
                proxy.WcApplicationCulture_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationCulture_DeleteCompleted(object sender, WcApplicationCulture_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationCulture_DeleteCompletedEventArgs> handler = WcApplicationCulture_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationCulture_Delete

        #region WcApplicationCulture_SetIsDeleted

        public void Begin_WcApplicationCulture_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcApplicationCultureServiceClient proxy = new WcApplicationCultureServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationCulture_SetIsDeletedCompleted += new EventHandler<WcApplicationCulture_SetIsDeletedCompletedEventArgs>(proxy_WcApplicationCulture_SetIsDeletedCompleted);
                proxy.WcApplicationCulture_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationCulture_SetIsDeletedCompleted(object sender, WcApplicationCulture_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationCulture_SetIsDeletedCompletedEventArgs> handler = WcApplicationCulture_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationCulture_SetIsDeleted

        #region WcApplicationCulture_Deactivate

        public void Begin_WcApplicationCulture_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_Deactivate", IfxTraceCategory.Enter);
            WcApplicationCultureServiceClient proxy = new WcApplicationCultureServiceClient();
            AssignCredentials(proxy);
            proxy.WcApplicationCulture_DeactivateCompleted += new EventHandler<WcApplicationCulture_DeactivateCompletedEventArgs>(proxy_WcApplicationCulture_DeactivateCompleted);
            proxy.WcApplicationCulture_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationCulture_DeactivateCompleted(object sender, WcApplicationCulture_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationCulture_DeactivateCompletedEventArgs> handler = WcApplicationCulture_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationCulture_Deactivate

        #region WcApplicationCulture_Remove

        public void Begin_WcApplicationCulture_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_Remove", IfxTraceCategory.Enter);
                WcApplicationCultureServiceClient proxy = new WcApplicationCultureServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationCulture_RemoveCompleted += new EventHandler<WcApplicationCulture_RemoveCompletedEventArgs>(proxy_WcApplicationCulture_RemoveCompleted);
                proxy.WcApplicationCulture_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationCulture_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationCulture_RemoveCompleted(object sender, WcApplicationCulture_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationCulture_RemoveCompletedEventArgs> handler = WcApplicationCulture_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationCulture_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcApplicationCulture_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


