using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  1/3/2018 11:07:42 PM

namespace ProxyWrapper
{
    public partial class WcApplicationVersionService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcApplicationVersion_GetByIdCompletedEventArgs> WcApplicationVersion_GetByIdCompleted;
        public event System.EventHandler<WcApplicationVersion_GetAllCompletedEventArgs> WcApplicationVersion_GetAllCompleted;
        public event System.EventHandler<WcApplicationVersion_GetListByFKCompletedEventArgs> WcApplicationVersion_GetListByFKCompleted;
        public event System.EventHandler<WcApplicationVersion_SaveCompletedEventArgs> WcApplicationVersion_SaveCompleted;
        public event System.EventHandler<WcApplicationVersion_DeleteCompletedEventArgs> WcApplicationVersion_DeleteCompleted;
        public event System.EventHandler<WcApplicationVersion_SetIsDeletedCompletedEventArgs> WcApplicationVersion_SetIsDeletedCompleted;
        public event System.EventHandler<WcApplicationVersion_DeactivateCompletedEventArgs> WcApplicationVersion_DeactivateCompleted;
        public event System.EventHandler<WcApplicationVersion_RemoveCompletedEventArgs> WcApplicationVersion_RemoveCompleted;

        public event System.EventHandler<GetWcApplicationVersion_ReadOnlyStaticListsCompletedEventArgs> GetWcApplicationVersion_ReadOnlyStaticListsCompleted;
		public event System.EventHandler<GetWcStoredProc_ComboItemListCompletedEventArgs> GetWcStoredProc_ComboItemListCompleted;
		public event System.EventHandler<GetWcTableColumn_lstByApVersion_Id_ComboItemListCompletedEventArgs> GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted;
		public event System.EventHandler<GetWcDatabaseConnectionStringKey_ComboItemListCompletedEventArgs> GetWcDatabaseConnectionStringKey_ComboItemListCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcApplicationVersion_GetById

        public void Begin_WcApplicationVersion_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_GetById", IfxTraceCategory.Enter);
                WcApplicationVersionServiceClient proxy = new WcApplicationVersionServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationVersion_GetByIdCompleted += new EventHandler<WcApplicationVersion_GetByIdCompletedEventArgs>(proxy_WcApplicationVersion_GetByIdCompleted);
                proxy.WcApplicationVersion_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationVersion_GetByIdCompleted(object sender, WcApplicationVersion_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationVersion_GetByIdCompletedEventArgs> handler = WcApplicationVersion_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationVersion_GetById

        #region WcApplicationVersion_GetAll

        public void Begin_WcApplicationVersion_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_GetAll", IfxTraceCategory.Enter);
                WcApplicationVersionServiceClient proxy = new WcApplicationVersionServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationVersion_GetAllCompleted += new EventHandler<WcApplicationVersion_GetAllCompletedEventArgs>(proxy_WcApplicationVersion_GetAllCompleted);
                proxy.WcApplicationVersion_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationVersion_GetAllCompleted(object sender, WcApplicationVersion_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationVersion_GetAllCompletedEventArgs> handler = WcApplicationVersion_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationVersion_GetAll

        #region WcApplicationVersion_GetListByFK

        public void Begin_WcApplicationVersion_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_GetListByFK", IfxTraceCategory.Enter);
                WcApplicationVersionServiceClient proxy = new WcApplicationVersionServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationVersion_GetListByFKCompleted += new EventHandler<WcApplicationVersion_GetListByFKCompletedEventArgs>(proxy_WcApplicationVersion_GetListByFKCompleted);
                proxy.WcApplicationVersion_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationVersion_GetListByFKCompleted(object sender, WcApplicationVersion_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationVersion_GetListByFKCompletedEventArgs> handler = WcApplicationVersion_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationVersion_GetListByFK

        #region WcApplicationVersion_Save

        public void Begin_WcApplicationVersion_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_Save", IfxTraceCategory.Enter);
                WcApplicationVersionServiceClient proxy = new WcApplicationVersionServiceClient();
                AssignCredentials(proxy);
                //proxy.WcApplicationVersion_SaveCompleted += new EventHandler<WcApplicationVersion_SaveCompletedEventArgs>(proxy_WcApplicationVersion_SaveCompleted);
                proxy.WcApplicationVersion_SaveCompleted += new EventHandler<WcApplicationVersion_SaveCompletedEventArgs>(proxy_WcApplicationVersion_SaveCompleted);
                proxy.WcApplicationVersion_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationVersion_SaveCompleted(object sender, WcApplicationVersion_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationVersion_SaveCompletedEventArgs> handler = WcApplicationVersion_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationVersion_Save

        #region WcApplicationVersion_Delete

        public void Begin_WcApplicationVersion_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_DeleteCompleted", IfxTraceCategory.Enter);
                WcApplicationVersionServiceClient proxy = new WcApplicationVersionServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationVersion_DeleteCompleted += new EventHandler<WcApplicationVersion_DeleteCompletedEventArgs>(proxy_WcApplicationVersion_DeleteCompleted);
                proxy.WcApplicationVersion_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationVersion_DeleteCompleted(object sender, WcApplicationVersion_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationVersion_DeleteCompletedEventArgs> handler = WcApplicationVersion_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationVersion_Delete

        #region WcApplicationVersion_SetIsDeleted

        public void Begin_WcApplicationVersion_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcApplicationVersionServiceClient proxy = new WcApplicationVersionServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationVersion_SetIsDeletedCompleted += new EventHandler<WcApplicationVersion_SetIsDeletedCompletedEventArgs>(proxy_WcApplicationVersion_SetIsDeletedCompleted);
                proxy.WcApplicationVersion_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationVersion_SetIsDeletedCompleted(object sender, WcApplicationVersion_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationVersion_SetIsDeletedCompletedEventArgs> handler = WcApplicationVersion_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationVersion_SetIsDeleted

        #region WcApplicationVersion_Deactivate

        public void Begin_WcApplicationVersion_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_Deactivate", IfxTraceCategory.Enter);
            WcApplicationVersionServiceClient proxy = new WcApplicationVersionServiceClient();
            AssignCredentials(proxy);
            proxy.WcApplicationVersion_DeactivateCompleted += new EventHandler<WcApplicationVersion_DeactivateCompletedEventArgs>(proxy_WcApplicationVersion_DeactivateCompleted);
            proxy.WcApplicationVersion_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationVersion_DeactivateCompleted(object sender, WcApplicationVersion_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationVersion_DeactivateCompletedEventArgs> handler = WcApplicationVersion_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationVersion_Deactivate

        #region WcApplicationVersion_Remove

        public void Begin_WcApplicationVersion_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_Remove", IfxTraceCategory.Enter);
                WcApplicationVersionServiceClient proxy = new WcApplicationVersionServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationVersion_RemoveCompleted += new EventHandler<WcApplicationVersion_RemoveCompletedEventArgs>(proxy_WcApplicationVersion_RemoveCompleted);
                proxy.WcApplicationVersion_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationVersion_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationVersion_RemoveCompleted(object sender, WcApplicationVersion_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationVersion_RemoveCompletedEventArgs> handler = WcApplicationVersion_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcApplicationVersion_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists

        public void Begin_GetWcApplicationVersion_ReadOnlyStaticLists(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcApplicationVersion_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                WcApplicationVersionServiceClient proxy = new WcApplicationVersionServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcApplicationVersion_ReadOnlyStaticListsCompleted += new EventHandler<GetWcApplicationVersion_ReadOnlyStaticListsCompletedEventArgs>(proxy_GetWcApplicationVersion_ReadOnlyStaticListsCompleted);
                proxy.GetWcApplicationVersion_ReadOnlyStaticListsAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcApplicationVersion_ReadOnlyStaticLists", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcApplicationVersion_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcApplicationVersion_ReadOnlyStaticListsCompleted(object sender, GetWcApplicationVersion_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetWcApplicationVersion_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                System.EventHandler<GetWcApplicationVersion_ReadOnlyStaticListsCompletedEventArgs> handler = GetWcApplicationVersion_ReadOnlyStaticListsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetWcApplicationVersion_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetWcApplicationVersion_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion ReadOnlyStaticLists

        #region GetWcStoredProc_ComboItemList

        public void Begin_GetWcStoredProc_ComboItemList(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProc_ComboItemList", IfxTraceCategory.Enter);
                WcApplicationVersionServiceClient proxy = new WcApplicationVersionServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcStoredProc_ComboItemListCompleted += new EventHandler<GetWcStoredProc_ComboItemListCompletedEventArgs>(proxy_GetWcStoredProc_ComboItemListCompleted);
                proxy.GetWcStoredProc_ComboItemListAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProc_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProc_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcStoredProc_ComboItemListCompleted(object sender, GetWcStoredProc_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcStoredProc_ComboItemListCompletedEventArgs> handler = GetWcStoredProc_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcStoredProc_ComboItemList

        #region GetWcTableColumn_lstByApVersion_Id_ComboItemList

        public void Begin_GetWcTableColumn_lstByApVersion_Id_ComboItemList(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumn_lstByApVersion_Id_ComboItemList", IfxTraceCategory.Enter);
                WcApplicationVersionServiceClient proxy = new WcApplicationVersionServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted += new EventHandler<GetWcTableColumn_lstByApVersion_Id_ComboItemListCompletedEventArgs>(proxy_GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted);
                proxy.GetWcTableColumn_lstByApVersion_Id_ComboItemListAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumn_lstByApVersion_Id_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcTableColumn_lstByApVersion_Id_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted(object sender, GetWcTableColumn_lstByApVersion_Id_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByApVersion_Id_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcTableColumn_lstByApVersion_Id_ComboItemListCompletedEventArgs> handler = GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByApVersion_Id_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByApVersion_Id_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcTableColumn_lstByApVersion_Id_ComboItemList

        #region GetWcDatabaseConnectionStringKey_ComboItemList

        public void Begin_GetWcDatabaseConnectionStringKey_ComboItemList(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcDatabaseConnectionStringKey_ComboItemList", IfxTraceCategory.Enter);
                WcApplicationVersionServiceClient proxy = new WcApplicationVersionServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcDatabaseConnectionStringKey_ComboItemListCompleted += new EventHandler<GetWcDatabaseConnectionStringKey_ComboItemListCompletedEventArgs>(proxy_GetWcDatabaseConnectionStringKey_ComboItemListCompleted);
                proxy.GetWcDatabaseConnectionStringKey_ComboItemListAsync(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcDatabaseConnectionStringKey_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcDatabaseConnectionStringKey_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcDatabaseConnectionStringKey_ComboItemListCompleted(object sender, GetWcDatabaseConnectionStringKey_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDatabaseConnectionStringKey_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcDatabaseConnectionStringKey_ComboItemListCompletedEventArgs> handler = GetWcDatabaseConnectionStringKey_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDatabaseConnectionStringKey_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDatabaseConnectionStringKey_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcDatabaseConnectionStringKey_ComboItemList

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


