using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  1/3/2018 10:23:58 AM

namespace ProxyWrapper
{
    public partial class WcDatabaseConnectionStringKeyService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcDatabaseConnectionStringKey_GetByIdCompletedEventArgs> WcDatabaseConnectionStringKey_GetByIdCompleted;
        public event System.EventHandler<WcDatabaseConnectionStringKey_GetAllCompletedEventArgs> WcDatabaseConnectionStringKey_GetAllCompleted;
        public event System.EventHandler<WcDatabaseConnectionStringKey_GetListByFKCompletedEventArgs> WcDatabaseConnectionStringKey_GetListByFKCompleted;
        public event System.EventHandler<WcDatabaseConnectionStringKey_SaveCompletedEventArgs> WcDatabaseConnectionStringKey_SaveCompleted;
        public event System.EventHandler<WcDatabaseConnectionStringKey_DeleteCompletedEventArgs> WcDatabaseConnectionStringKey_DeleteCompleted;
        public event System.EventHandler<WcDatabaseConnectionStringKey_SetIsDeletedCompletedEventArgs> WcDatabaseConnectionStringKey_SetIsDeletedCompleted;
        public event System.EventHandler<WcDatabaseConnectionStringKey_DeactivateCompletedEventArgs> WcDatabaseConnectionStringKey_DeactivateCompleted;
        public event System.EventHandler<WcDatabaseConnectionStringKey_RemoveCompletedEventArgs> WcDatabaseConnectionStringKey_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcDatabaseConnectionStringKey_GetById

        public void Begin_WcDatabaseConnectionStringKey_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_GetById", IfxTraceCategory.Enter);
                WcDatabaseConnectionStringKeyServiceClient proxy = new WcDatabaseConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                proxy.WcDatabaseConnectionStringKey_GetByIdCompleted += new EventHandler<WcDatabaseConnectionStringKey_GetByIdCompletedEventArgs>(proxy_WcDatabaseConnectionStringKey_GetByIdCompleted);
                proxy.WcDatabaseConnectionStringKey_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcDatabaseConnectionStringKey_GetByIdCompleted(object sender, WcDatabaseConnectionStringKey_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcDatabaseConnectionStringKey_GetByIdCompletedEventArgs> handler = WcDatabaseConnectionStringKey_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcDatabaseConnectionStringKey_GetById

        #region WcDatabaseConnectionStringKey_GetAll

        public void Begin_WcDatabaseConnectionStringKey_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_GetAll", IfxTraceCategory.Enter);
                WcDatabaseConnectionStringKeyServiceClient proxy = new WcDatabaseConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                proxy.WcDatabaseConnectionStringKey_GetAllCompleted += new EventHandler<WcDatabaseConnectionStringKey_GetAllCompletedEventArgs>(proxy_WcDatabaseConnectionStringKey_GetAllCompleted);
                proxy.WcDatabaseConnectionStringKey_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcDatabaseConnectionStringKey_GetAllCompleted(object sender, WcDatabaseConnectionStringKey_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcDatabaseConnectionStringKey_GetAllCompletedEventArgs> handler = WcDatabaseConnectionStringKey_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcDatabaseConnectionStringKey_GetAll

        #region WcDatabaseConnectionStringKey_GetListByFK

        public void Begin_WcDatabaseConnectionStringKey_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_GetListByFK", IfxTraceCategory.Enter);
                WcDatabaseConnectionStringKeyServiceClient proxy = new WcDatabaseConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                proxy.WcDatabaseConnectionStringKey_GetListByFKCompleted += new EventHandler<WcDatabaseConnectionStringKey_GetListByFKCompletedEventArgs>(proxy_WcDatabaseConnectionStringKey_GetListByFKCompleted);
                proxy.WcDatabaseConnectionStringKey_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcDatabaseConnectionStringKey_GetListByFKCompleted(object sender, WcDatabaseConnectionStringKey_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcDatabaseConnectionStringKey_GetListByFKCompletedEventArgs> handler = WcDatabaseConnectionStringKey_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcDatabaseConnectionStringKey_GetListByFK

        #region WcDatabaseConnectionStringKey_Save

        public void Begin_WcDatabaseConnectionStringKey_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_Save", IfxTraceCategory.Enter);
                WcDatabaseConnectionStringKeyServiceClient proxy = new WcDatabaseConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                //proxy.WcDatabaseConnectionStringKey_SaveCompleted += new EventHandler<WcDatabaseConnectionStringKey_SaveCompletedEventArgs>(proxy_WcDatabaseConnectionStringKey_SaveCompleted);
                proxy.WcDatabaseConnectionStringKey_SaveCompleted += new EventHandler<WcDatabaseConnectionStringKey_SaveCompletedEventArgs>(proxy_WcDatabaseConnectionStringKey_SaveCompleted);
                proxy.WcDatabaseConnectionStringKey_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcDatabaseConnectionStringKey_SaveCompleted(object sender, WcDatabaseConnectionStringKey_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcDatabaseConnectionStringKey_SaveCompletedEventArgs> handler = WcDatabaseConnectionStringKey_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcDatabaseConnectionStringKey_Save

        #region WcDatabaseConnectionStringKey_Delete

        public void Begin_WcDatabaseConnectionStringKey_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_DeleteCompleted", IfxTraceCategory.Enter);
                WcDatabaseConnectionStringKeyServiceClient proxy = new WcDatabaseConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                proxy.WcDatabaseConnectionStringKey_DeleteCompleted += new EventHandler<WcDatabaseConnectionStringKey_DeleteCompletedEventArgs>(proxy_WcDatabaseConnectionStringKey_DeleteCompleted);
                proxy.WcDatabaseConnectionStringKey_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcDatabaseConnectionStringKey_DeleteCompleted(object sender, WcDatabaseConnectionStringKey_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcDatabaseConnectionStringKey_DeleteCompletedEventArgs> handler = WcDatabaseConnectionStringKey_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcDatabaseConnectionStringKey_Delete

        #region WcDatabaseConnectionStringKey_SetIsDeleted

        public void Begin_WcDatabaseConnectionStringKey_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcDatabaseConnectionStringKeyServiceClient proxy = new WcDatabaseConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                proxy.WcDatabaseConnectionStringKey_SetIsDeletedCompleted += new EventHandler<WcDatabaseConnectionStringKey_SetIsDeletedCompletedEventArgs>(proxy_WcDatabaseConnectionStringKey_SetIsDeletedCompleted);
                proxy.WcDatabaseConnectionStringKey_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcDatabaseConnectionStringKey_SetIsDeletedCompleted(object sender, WcDatabaseConnectionStringKey_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcDatabaseConnectionStringKey_SetIsDeletedCompletedEventArgs> handler = WcDatabaseConnectionStringKey_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcDatabaseConnectionStringKey_SetIsDeleted

        #region WcDatabaseConnectionStringKey_Deactivate

        public void Begin_WcDatabaseConnectionStringKey_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_Deactivate", IfxTraceCategory.Enter);
            WcDatabaseConnectionStringKeyServiceClient proxy = new WcDatabaseConnectionStringKeyServiceClient();
            AssignCredentials(proxy);
            proxy.WcDatabaseConnectionStringKey_DeactivateCompleted += new EventHandler<WcDatabaseConnectionStringKey_DeactivateCompletedEventArgs>(proxy_WcDatabaseConnectionStringKey_DeactivateCompleted);
            proxy.WcDatabaseConnectionStringKey_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcDatabaseConnectionStringKey_DeactivateCompleted(object sender, WcDatabaseConnectionStringKey_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcDatabaseConnectionStringKey_DeactivateCompletedEventArgs> handler = WcDatabaseConnectionStringKey_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcDatabaseConnectionStringKey_Deactivate

        #region WcDatabaseConnectionStringKey_Remove

        public void Begin_WcDatabaseConnectionStringKey_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_Remove", IfxTraceCategory.Enter);
                WcDatabaseConnectionStringKeyServiceClient proxy = new WcDatabaseConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                proxy.WcDatabaseConnectionStringKey_RemoveCompleted += new EventHandler<WcDatabaseConnectionStringKey_RemoveCompletedEventArgs>(proxy_WcDatabaseConnectionStringKey_RemoveCompleted);
                proxy.WcDatabaseConnectionStringKey_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcDatabaseConnectionStringKey_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcDatabaseConnectionStringKey_RemoveCompleted(object sender, WcDatabaseConnectionStringKey_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcDatabaseConnectionStringKey_RemoveCompletedEventArgs> handler = WcDatabaseConnectionStringKey_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcDatabaseConnectionStringKey_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


