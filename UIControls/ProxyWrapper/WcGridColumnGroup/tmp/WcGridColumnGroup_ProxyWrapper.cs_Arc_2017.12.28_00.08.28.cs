using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/27/2017 7:49:43 PM

namespace ProxyWrapper
{
    public partial class WcGridColumnGroupService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcGridColumnGroup_GetByIdCompletedEventArgs> WcGridColumnGroup_GetByIdCompleted;
        public event System.EventHandler<WcGridColumnGroup_GetAllCompletedEventArgs> WcGridColumnGroup_GetAllCompleted;
        public event System.EventHandler<WcGridColumnGroup_GetListByFKCompletedEventArgs> WcGridColumnGroup_GetListByFKCompleted;
        public event System.EventHandler<WcGridColumnGroup_SaveCompletedEventArgs> WcGridColumnGroup_SaveCompleted;
        public event System.EventHandler<WcGridColumnGroup_DeleteCompletedEventArgs> WcGridColumnGroup_DeleteCompleted;
        public event System.EventHandler<WcGridColumnGroup_SetIsDeletedCompletedEventArgs> WcGridColumnGroup_SetIsDeletedCompleted;
        public event System.EventHandler<WcGridColumnGroup_DeactivateCompletedEventArgs> WcGridColumnGroup_DeactivateCompleted;
        public event System.EventHandler<WcGridColumnGroup_RemoveCompletedEventArgs> WcGridColumnGroup_RemoveCompleted;
		public event System.EventHandler<ExecuteWcGridColumnGroup_AssignItemsCompletedEventArgs> ExecuteWcGridColumnGroup_AssignItemsCompleted;
		public event System.EventHandler<GetWcGridColumnGroupItem_lstAssignedCompletedEventArgs> GetWcGridColumnGroupItem_lstAssignedCompleted;
		public event System.EventHandler<GetWcGridColumnGroupItem_lstNotAssignedCompletedEventArgs> GetWcGridColumnGroupItem_lstNotAssignedCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcGridColumnGroup_GetById

        public void Begin_WcGridColumnGroup_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_GetById", IfxTraceCategory.Enter);
                WcGridColumnGroupServiceClient proxy = new WcGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcGridColumnGroup_GetByIdCompleted += new EventHandler<WcGridColumnGroup_GetByIdCompletedEventArgs>(proxy_WcGridColumnGroup_GetByIdCompleted);
                proxy.WcGridColumnGroup_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcGridColumnGroup_GetByIdCompleted(object sender, WcGridColumnGroup_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcGridColumnGroup_GetByIdCompletedEventArgs> handler = WcGridColumnGroup_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcGridColumnGroup_GetById

        #region WcGridColumnGroup_GetAll

        public void Begin_WcGridColumnGroup_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_GetAll", IfxTraceCategory.Enter);
                WcGridColumnGroupServiceClient proxy = new WcGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcGridColumnGroup_GetAllCompleted += new EventHandler<WcGridColumnGroup_GetAllCompletedEventArgs>(proxy_WcGridColumnGroup_GetAllCompleted);
                proxy.WcGridColumnGroup_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcGridColumnGroup_GetAllCompleted(object sender, WcGridColumnGroup_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcGridColumnGroup_GetAllCompletedEventArgs> handler = WcGridColumnGroup_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcGridColumnGroup_GetAll

        #region WcGridColumnGroup_GetListByFK

        public void Begin_WcGridColumnGroup_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_GetListByFK", IfxTraceCategory.Enter);
                WcGridColumnGroupServiceClient proxy = new WcGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcGridColumnGroup_GetListByFKCompleted += new EventHandler<WcGridColumnGroup_GetListByFKCompletedEventArgs>(proxy_WcGridColumnGroup_GetListByFKCompleted);
                proxy.WcGridColumnGroup_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcGridColumnGroup_GetListByFKCompleted(object sender, WcGridColumnGroup_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcGridColumnGroup_GetListByFKCompletedEventArgs> handler = WcGridColumnGroup_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcGridColumnGroup_GetListByFK

        #region WcGridColumnGroup_Save

        public void Begin_WcGridColumnGroup_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_Save", IfxTraceCategory.Enter);
                WcGridColumnGroupServiceClient proxy = new WcGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                //proxy.WcGridColumnGroup_SaveCompleted += new EventHandler<WcGridColumnGroup_SaveCompletedEventArgs>(proxy_WcGridColumnGroup_SaveCompleted);
                proxy.WcGridColumnGroup_SaveCompleted += new EventHandler<WcGridColumnGroup_SaveCompletedEventArgs>(proxy_WcGridColumnGroup_SaveCompleted);
                proxy.WcGridColumnGroup_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcGridColumnGroup_SaveCompleted(object sender, WcGridColumnGroup_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcGridColumnGroup_SaveCompletedEventArgs> handler = WcGridColumnGroup_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcGridColumnGroup_Save

        #region WcGridColumnGroup_Delete

        public void Begin_WcGridColumnGroup_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_DeleteCompleted", IfxTraceCategory.Enter);
                WcGridColumnGroupServiceClient proxy = new WcGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcGridColumnGroup_DeleteCompleted += new EventHandler<WcGridColumnGroup_DeleteCompletedEventArgs>(proxy_WcGridColumnGroup_DeleteCompleted);
                proxy.WcGridColumnGroup_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcGridColumnGroup_DeleteCompleted(object sender, WcGridColumnGroup_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcGridColumnGroup_DeleteCompletedEventArgs> handler = WcGridColumnGroup_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcGridColumnGroup_Delete

        #region WcGridColumnGroup_SetIsDeleted

        public void Begin_WcGridColumnGroup_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcGridColumnGroupServiceClient proxy = new WcGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcGridColumnGroup_SetIsDeletedCompleted += new EventHandler<WcGridColumnGroup_SetIsDeletedCompletedEventArgs>(proxy_WcGridColumnGroup_SetIsDeletedCompleted);
                proxy.WcGridColumnGroup_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcGridColumnGroup_SetIsDeletedCompleted(object sender, WcGridColumnGroup_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcGridColumnGroup_SetIsDeletedCompletedEventArgs> handler = WcGridColumnGroup_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcGridColumnGroup_SetIsDeleted

        #region WcGridColumnGroup_Deactivate

        public void Begin_WcGridColumnGroup_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_Deactivate", IfxTraceCategory.Enter);
            WcGridColumnGroupServiceClient proxy = new WcGridColumnGroupServiceClient();
            AssignCredentials(proxy);
            proxy.WcGridColumnGroup_DeactivateCompleted += new EventHandler<WcGridColumnGroup_DeactivateCompletedEventArgs>(proxy_WcGridColumnGroup_DeactivateCompleted);
            proxy.WcGridColumnGroup_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcGridColumnGroup_DeactivateCompleted(object sender, WcGridColumnGroup_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcGridColumnGroup_DeactivateCompletedEventArgs> handler = WcGridColumnGroup_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcGridColumnGroup_Deactivate

        #region WcGridColumnGroup_Remove

        public void Begin_WcGridColumnGroup_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_Remove", IfxTraceCategory.Enter);
                WcGridColumnGroupServiceClient proxy = new WcGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcGridColumnGroup_RemoveCompleted += new EventHandler<WcGridColumnGroup_RemoveCompletedEventArgs>(proxy_WcGridColumnGroup_RemoveCompleted);
                proxy.WcGridColumnGroup_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcGridColumnGroup_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcGridColumnGroup_RemoveCompleted(object sender, WcGridColumnGroup_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcGridColumnGroup_RemoveCompletedEventArgs> handler = WcGridColumnGroup_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcGridColumnGroup_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #region ExecuteWcGridColumnGroup_AssignItems

        public void Begin_ExecuteWcGridColumnGroup_AssignItems(Boolean Insert, Guid GrdColGrpItm_Id, Guid GrdColGrpItm_Grp_Id, Guid GrdColGrpItm_TbC_Id, Guid GrdColGrpItm_UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcGridColumnGroup_AssignItems", IfxTraceCategory.Enter);
                WcGridColumnGroupServiceClient proxy = new WcGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.ExecuteWcGridColumnGroup_AssignItemsCompleted += new EventHandler<ExecuteWcGridColumnGroup_AssignItemsCompletedEventArgs>(proxy_ExecuteWcGridColumnGroup_AssignItemsCompleted);
                proxy.ExecuteWcGridColumnGroup_AssignItemsAsync(Insert, GrdColGrpItm_Id, GrdColGrpItm_Grp_Id, GrdColGrpItm_TbC_Id, GrdColGrpItm_UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcGridColumnGroup_AssignItems", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcGridColumnGroup_AssignItems", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecuteWcGridColumnGroup_AssignItemsCompleted(object sender, ExecuteWcGridColumnGroup_AssignItemsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcGridColumnGroup_AssignItems", IfxTraceCategory.Enter);
                System.EventHandler<ExecuteWcGridColumnGroup_AssignItemsCompletedEventArgs> handler = ExecuteWcGridColumnGroup_AssignItemsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcGridColumnGroup_AssignItems", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcGridColumnGroup_AssignItems", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecuteWcGridColumnGroup_AssignItems

        #region GetWcGridColumnGroupItem_lstAssigned

        public void Begin_GetWcGridColumnGroupItem_lstAssigned(Guid Grp_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcGridColumnGroupItem_lstAssigned", IfxTraceCategory.Enter);
                WcGridColumnGroupServiceClient proxy = new WcGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcGridColumnGroupItem_lstAssignedCompleted += new EventHandler<GetWcGridColumnGroupItem_lstAssignedCompletedEventArgs>(proxy_GetWcGridColumnGroupItem_lstAssignedCompleted);
                proxy.GetWcGridColumnGroupItem_lstAssignedAsync(Grp_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcGridColumnGroupItem_lstAssigned", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcGridColumnGroupItem_lstAssigned", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcGridColumnGroupItem_lstAssignedCompleted(object sender, GetWcGridColumnGroupItem_lstAssignedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstAssigned", IfxTraceCategory.Enter);
                System.EventHandler<GetWcGridColumnGroupItem_lstAssignedCompletedEventArgs> handler = GetWcGridColumnGroupItem_lstAssignedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstAssigned", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstAssigned", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcGridColumnGroupItem_lstAssigned

        #region GetWcGridColumnGroupItem_lstNotAssigned

        public void Begin_GetWcGridColumnGroupItem_lstNotAssigned(Guid Tb_Id, Guid Grp_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcGridColumnGroupItem_lstNotAssigned", IfxTraceCategory.Enter);
                WcGridColumnGroupServiceClient proxy = new WcGridColumnGroupServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcGridColumnGroupItem_lstNotAssignedCompleted += new EventHandler<GetWcGridColumnGroupItem_lstNotAssignedCompletedEventArgs>(proxy_GetWcGridColumnGroupItem_lstNotAssignedCompleted);
                proxy.GetWcGridColumnGroupItem_lstNotAssignedAsync(Tb_Id, Grp_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcGridColumnGroupItem_lstNotAssigned", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcGridColumnGroupItem_lstNotAssigned", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcGridColumnGroupItem_lstNotAssignedCompleted(object sender, GetWcGridColumnGroupItem_lstNotAssignedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstNotAssigned", IfxTraceCategory.Enter);
                System.EventHandler<GetWcGridColumnGroupItem_lstNotAssignedCompletedEventArgs> handler = GetWcGridColumnGroupItem_lstNotAssignedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstNotAssigned", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstNotAssigned", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcGridColumnGroupItem_lstNotAssigned

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


