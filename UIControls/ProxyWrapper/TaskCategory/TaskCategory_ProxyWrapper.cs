using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  2/7/2015 8:57:19 PM

namespace ProxyWrapper
{
    public partial class TaskCategoryService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<TaskCategory_GetByIdCompletedEventArgs> TaskCategory_GetByIdCompleted;
        public event System.EventHandler<TaskCategory_GetAllCompletedEventArgs> TaskCategory_GetAllCompleted;
        public event System.EventHandler<TaskCategory_GetListByFKCompletedEventArgs> TaskCategory_GetListByFKCompleted;
        public event System.EventHandler<TaskCategory_SaveCompletedEventArgs> TaskCategory_SaveCompleted;
        public event System.EventHandler<TaskCategory_DeleteCompletedEventArgs> TaskCategory_DeleteCompleted;
        public event System.EventHandler<TaskCategory_SetIsDeletedCompletedEventArgs> TaskCategory_SetIsDeletedCompleted;
        public event System.EventHandler<TaskCategory_DeactivateCompletedEventArgs> TaskCategory_DeactivateCompleted;
        public event System.EventHandler<TaskCategory_RemoveCompletedEventArgs> TaskCategory_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region TaskCategory_GetById

        public void Begin_TaskCategory_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_GetById", IfxTraceCategory.Enter);
                TaskCategoryServiceClient proxy = new TaskCategoryServiceClient();
                AssignCredentials(proxy);
                proxy.TaskCategory_GetByIdCompleted += new EventHandler<TaskCategory_GetByIdCompletedEventArgs>(proxy_TaskCategory_GetByIdCompleted);
                proxy.TaskCategory_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_GetById", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_TaskCategory_GetByIdCompleted(object sender, TaskCategory_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<TaskCategory_GetByIdCompletedEventArgs> handler = TaskCategory_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TaskCategory_GetById

        #region TaskCategory_GetAll

        public void Begin_TaskCategory_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_GetAll", IfxTraceCategory.Enter);
                TaskCategoryServiceClient proxy = new TaskCategoryServiceClient();
                AssignCredentials(proxy);
                proxy.TaskCategory_GetAllCompleted += new EventHandler<TaskCategory_GetAllCompletedEventArgs>(proxy_TaskCategory_GetAllCompleted);
                proxy.TaskCategory_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_GetAll", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_TaskCategory_GetAllCompleted(object sender, TaskCategory_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<TaskCategory_GetAllCompletedEventArgs> handler = TaskCategory_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TaskCategory_GetAll

        #region TaskCategory_GetListByFK

        public void Begin_TaskCategory_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_GetListByFK", IfxTraceCategory.Enter);
                TaskCategoryServiceClient proxy = new TaskCategoryServiceClient();
                AssignCredentials(proxy);
                proxy.TaskCategory_GetListByFKCompleted += new EventHandler<TaskCategory_GetListByFKCompletedEventArgs>(proxy_TaskCategory_GetListByFKCompleted);
                proxy.TaskCategory_GetListByFKAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_TaskCategory_GetListByFKCompleted(object sender, TaskCategory_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<TaskCategory_GetListByFKCompletedEventArgs> handler = TaskCategory_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TaskCategory_GetListByFK

        #region TaskCategory_Save

        public void Begin_TaskCategory_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_Save", IfxTraceCategory.Enter);
                TaskCategoryServiceClient proxy = new TaskCategoryServiceClient();
                AssignCredentials(proxy);
                //proxy.TaskCategory_SaveCompleted += new EventHandler<TaskCategory_SaveCompletedEventArgs>(proxy_TaskCategory_SaveCompleted);
                proxy.TaskCategory_SaveCompleted += new EventHandler<TaskCategory_SaveCompletedEventArgs>(proxy_TaskCategory_SaveCompleted);
                proxy.TaskCategory_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_Save", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_TaskCategory_SaveCompleted(object sender, TaskCategory_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<TaskCategory_SaveCompletedEventArgs> handler = TaskCategory_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TaskCategory_Save

        #region TaskCategory_Delete

        public void Begin_TaskCategory_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_DeleteCompleted", IfxTraceCategory.Enter);
                TaskCategoryServiceClient proxy = new TaskCategoryServiceClient();
                AssignCredentials(proxy);
                proxy.TaskCategory_DeleteCompleted += new EventHandler<TaskCategory_DeleteCompletedEventArgs>(proxy_TaskCategory_DeleteCompleted);
                proxy.TaskCategory_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_TaskCategory_DeleteCompleted(object sender, TaskCategory_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<TaskCategory_DeleteCompletedEventArgs> handler = TaskCategory_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TaskCategory_Delete

        #region TaskCategory_SetIsDeleted

        public void Begin_TaskCategory_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                TaskCategoryServiceClient proxy = new TaskCategoryServiceClient();
                AssignCredentials(proxy);
                proxy.TaskCategory_SetIsDeletedCompleted += new EventHandler<TaskCategory_SetIsDeletedCompletedEventArgs>(proxy_TaskCategory_SetIsDeletedCompleted);
                proxy.TaskCategory_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_TaskCategory_SetIsDeletedCompleted(object sender, TaskCategory_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<TaskCategory_SetIsDeletedCompletedEventArgs> handler = TaskCategory_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TaskCategory_SetIsDeleted

        #region TaskCategory_Deactivate

        public void Begin_TaskCategory_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_Deactivate", IfxTraceCategory.Enter);
            TaskCategoryServiceClient proxy = new TaskCategoryServiceClient();
            AssignCredentials(proxy);
            proxy.TaskCategory_DeactivateCompleted += new EventHandler<TaskCategory_DeactivateCompletedEventArgs>(proxy_TaskCategory_DeactivateCompleted);
            proxy.TaskCategory_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_Deactivate", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_TaskCategory_DeactivateCompleted(object sender, TaskCategory_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<TaskCategory_DeactivateCompletedEventArgs> handler = TaskCategory_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TaskCategory_Deactivate

        #region TaskCategory_Remove

        public void Begin_TaskCategory_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_Remove", IfxTraceCategory.Enter);
                TaskCategoryServiceClient proxy = new TaskCategoryServiceClient();
                AssignCredentials(proxy);
                proxy.TaskCategory_RemoveCompleted += new EventHandler<TaskCategory_RemoveCompletedEventArgs>(proxy_TaskCategory_RemoveCompleted);
                proxy.TaskCategory_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_Remove", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_TaskCategory_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_TaskCategory_RemoveCompleted(object sender, TaskCategory_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<TaskCategory_RemoveCompletedEventArgs> handler = TaskCategory_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion TaskCategory_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


