using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/14/2017 10:08:51 PM

namespace ProxyWrapper
{
    public partial class WcStoredProcGroupService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcStoredProcGroup_GetByIdCompletedEventArgs> WcStoredProcGroup_GetByIdCompleted;
        public event System.EventHandler<WcStoredProcGroup_GetAllCompletedEventArgs> WcStoredProcGroup_GetAllCompleted;
        public event System.EventHandler<WcStoredProcGroup_GetListByFKCompletedEventArgs> WcStoredProcGroup_GetListByFKCompleted;
        public event System.EventHandler<WcStoredProcGroup_SaveCompletedEventArgs> WcStoredProcGroup_SaveCompleted;
        public event System.EventHandler<WcStoredProcGroup_DeleteCompletedEventArgs> WcStoredProcGroup_DeleteCompleted;
        public event System.EventHandler<WcStoredProcGroup_SetIsDeletedCompletedEventArgs> WcStoredProcGroup_SetIsDeletedCompleted;
        public event System.EventHandler<WcStoredProcGroup_DeactivateCompletedEventArgs> WcStoredProcGroup_DeactivateCompleted;
        public event System.EventHandler<WcStoredProcGroup_RemoveCompletedEventArgs> WcStoredProcGroup_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcStoredProcGroup_GetById

        public void Begin_WcStoredProcGroup_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_GetById", IfxTraceCategory.Enter);
                WcStoredProcGroupServiceClient proxy = new WcStoredProcGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcGroup_GetByIdCompleted += new EventHandler<WcStoredProcGroup_GetByIdCompletedEventArgs>(proxy_WcStoredProcGroup_GetByIdCompleted);
                proxy.WcStoredProcGroup_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcGroup_GetByIdCompleted(object sender, WcStoredProcGroup_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcGroup_GetByIdCompletedEventArgs> handler = WcStoredProcGroup_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcGroup_GetById

        #region WcStoredProcGroup_GetAll

        public void Begin_WcStoredProcGroup_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_GetAll", IfxTraceCategory.Enter);
                WcStoredProcGroupServiceClient proxy = new WcStoredProcGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcGroup_GetAllCompleted += new EventHandler<WcStoredProcGroup_GetAllCompletedEventArgs>(proxy_WcStoredProcGroup_GetAllCompleted);
                proxy.WcStoredProcGroup_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcGroup_GetAllCompleted(object sender, WcStoredProcGroup_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcGroup_GetAllCompletedEventArgs> handler = WcStoredProcGroup_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcGroup_GetAll

        #region WcStoredProcGroup_GetListByFK

        public void Begin_WcStoredProcGroup_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_GetListByFK", IfxTraceCategory.Enter);
                WcStoredProcGroupServiceClient proxy = new WcStoredProcGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcGroup_GetListByFKCompleted += new EventHandler<WcStoredProcGroup_GetListByFKCompletedEventArgs>(proxy_WcStoredProcGroup_GetListByFKCompleted);
                proxy.WcStoredProcGroup_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcGroup_GetListByFKCompleted(object sender, WcStoredProcGroup_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcGroup_GetListByFKCompletedEventArgs> handler = WcStoredProcGroup_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcGroup_GetListByFK

        #region WcStoredProcGroup_Save

        public void Begin_WcStoredProcGroup_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_Save", IfxTraceCategory.Enter);
                WcStoredProcGroupServiceClient proxy = new WcStoredProcGroupServiceClient();
                AssignCredentials(proxy);
                //proxy.WcStoredProcGroup_SaveCompleted += new EventHandler<WcStoredProcGroup_SaveCompletedEventArgs>(proxy_WcStoredProcGroup_SaveCompleted);
                proxy.WcStoredProcGroup_SaveCompleted += new EventHandler<WcStoredProcGroup_SaveCompletedEventArgs>(proxy_WcStoredProcGroup_SaveCompleted);
                proxy.WcStoredProcGroup_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcGroup_SaveCompleted(object sender, WcStoredProcGroup_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcGroup_SaveCompletedEventArgs> handler = WcStoredProcGroup_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcGroup_Save

        #region WcStoredProcGroup_Delete

        public void Begin_WcStoredProcGroup_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_DeleteCompleted", IfxTraceCategory.Enter);
                WcStoredProcGroupServiceClient proxy = new WcStoredProcGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcGroup_DeleteCompleted += new EventHandler<WcStoredProcGroup_DeleteCompletedEventArgs>(proxy_WcStoredProcGroup_DeleteCompleted);
                proxy.WcStoredProcGroup_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcGroup_DeleteCompleted(object sender, WcStoredProcGroup_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcGroup_DeleteCompletedEventArgs> handler = WcStoredProcGroup_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcGroup_Delete

        #region WcStoredProcGroup_SetIsDeleted

        public void Begin_WcStoredProcGroup_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcStoredProcGroupServiceClient proxy = new WcStoredProcGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcGroup_SetIsDeletedCompleted += new EventHandler<WcStoredProcGroup_SetIsDeletedCompletedEventArgs>(proxy_WcStoredProcGroup_SetIsDeletedCompleted);
                proxy.WcStoredProcGroup_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcGroup_SetIsDeletedCompleted(object sender, WcStoredProcGroup_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcGroup_SetIsDeletedCompletedEventArgs> handler = WcStoredProcGroup_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcGroup_SetIsDeleted

        #region WcStoredProcGroup_Deactivate

        public void Begin_WcStoredProcGroup_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_Deactivate", IfxTraceCategory.Enter);
            WcStoredProcGroupServiceClient proxy = new WcStoredProcGroupServiceClient();
            AssignCredentials(proxy);
            proxy.WcStoredProcGroup_DeactivateCompleted += new EventHandler<WcStoredProcGroup_DeactivateCompletedEventArgs>(proxy_WcStoredProcGroup_DeactivateCompleted);
            proxy.WcStoredProcGroup_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcGroup_DeactivateCompleted(object sender, WcStoredProcGroup_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcGroup_DeactivateCompletedEventArgs> handler = WcStoredProcGroup_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcGroup_Deactivate

        #region WcStoredProcGroup_Remove

        public void Begin_WcStoredProcGroup_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_Remove", IfxTraceCategory.Enter);
                WcStoredProcGroupServiceClient proxy = new WcStoredProcGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcGroup_RemoveCompleted += new EventHandler<WcStoredProcGroup_RemoveCompletedEventArgs>(proxy_WcStoredProcGroup_RemoveCompleted);
                proxy.WcStoredProcGroup_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcGroup_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcGroup_RemoveCompleted(object sender, WcStoredProcGroup_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcGroup_RemoveCompletedEventArgs> handler = WcStoredProcGroup_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcStoredProcGroup_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


