using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/23/2017 1:26:42 AM

namespace ProxyWrapper
{
    public partial class WcStoredProcService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcStoredProc_GetByIdCompletedEventArgs> WcStoredProc_GetByIdCompleted;
        public event System.EventHandler<WcStoredProc_GetAllCompletedEventArgs> WcStoredProc_GetAllCompleted;
        public event System.EventHandler<WcStoredProc_GetListByFKCompletedEventArgs> WcStoredProc_GetListByFKCompleted;
        public event System.EventHandler<WcStoredProc_SaveCompletedEventArgs> WcStoredProc_SaveCompleted;
        public event System.EventHandler<WcStoredProc_DeleteCompletedEventArgs> WcStoredProc_DeleteCompleted;
        public event System.EventHandler<WcStoredProc_SetIsDeletedCompletedEventArgs> WcStoredProc_SetIsDeletedCompleted;
        public event System.EventHandler<WcStoredProc_DeactivateCompletedEventArgs> WcStoredProc_DeactivateCompleted;
        public event System.EventHandler<WcStoredProc_RemoveCompletedEventArgs> WcStoredProc_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcStoredProc_GetById

        public void Begin_WcStoredProc_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetById", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProc_GetByIdCompleted += new EventHandler<WcStoredProc_GetByIdCompletedEventArgs>(proxy_WcStoredProc_GetByIdCompleted);
                proxy.WcStoredProc_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_GetByIdCompleted(object sender, WcStoredProc_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_GetByIdCompletedEventArgs> handler = WcStoredProc_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_GetById

        #region WcStoredProc_GetAll

        public void Begin_WcStoredProc_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetAll", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProc_GetAllCompleted += new EventHandler<WcStoredProc_GetAllCompletedEventArgs>(proxy_WcStoredProc_GetAllCompleted);
                proxy.WcStoredProc_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_GetAllCompleted(object sender, WcStoredProc_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_GetAllCompletedEventArgs> handler = WcStoredProc_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_GetAll

        #region WcStoredProc_GetListByFK

        public void Begin_WcStoredProc_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetListByFK", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProc_GetListByFKCompleted += new EventHandler<WcStoredProc_GetListByFKCompletedEventArgs>(proxy_WcStoredProc_GetListByFKCompleted);
                proxy.WcStoredProc_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_GetListByFKCompleted(object sender, WcStoredProc_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_GetListByFKCompletedEventArgs> handler = WcStoredProc_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_GetListByFK

        #region WcStoredProc_Save

        public void Begin_WcStoredProc_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Save", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                //proxy.WcStoredProc_SaveCompleted += new EventHandler<WcStoredProc_SaveCompletedEventArgs>(proxy_WcStoredProc_SaveCompleted);
                proxy.WcStoredProc_SaveCompleted += new EventHandler<WcStoredProc_SaveCompletedEventArgs>(proxy_WcStoredProc_SaveCompleted);
                proxy.WcStoredProc_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_SaveCompleted(object sender, WcStoredProc_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_SaveCompletedEventArgs> handler = WcStoredProc_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_Save

        #region WcStoredProc_Delete

        public void Begin_WcStoredProc_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_DeleteCompleted", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProc_DeleteCompleted += new EventHandler<WcStoredProc_DeleteCompletedEventArgs>(proxy_WcStoredProc_DeleteCompleted);
                proxy.WcStoredProc_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_DeleteCompleted(object sender, WcStoredProc_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_DeleteCompletedEventArgs> handler = WcStoredProc_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_Delete

        #region WcStoredProc_SetIsDeleted

        public void Begin_WcStoredProc_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProc_SetIsDeletedCompleted += new EventHandler<WcStoredProc_SetIsDeletedCompletedEventArgs>(proxy_WcStoredProc_SetIsDeletedCompleted);
                proxy.WcStoredProc_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_SetIsDeletedCompleted(object sender, WcStoredProc_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_SetIsDeletedCompletedEventArgs> handler = WcStoredProc_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_SetIsDeleted

        #region WcStoredProc_Deactivate

        public void Begin_WcStoredProc_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Deactivate", IfxTraceCategory.Enter);
            WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
            AssignCredentials(proxy);
            proxy.WcStoredProc_DeactivateCompleted += new EventHandler<WcStoredProc_DeactivateCompletedEventArgs>(proxy_WcStoredProc_DeactivateCompleted);
            proxy.WcStoredProc_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_DeactivateCompleted(object sender, WcStoredProc_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_DeactivateCompletedEventArgs> handler = WcStoredProc_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_Deactivate

        #region WcStoredProc_Remove

        public void Begin_WcStoredProc_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Remove", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProc_RemoveCompleted += new EventHandler<WcStoredProc_RemoveCompletedEventArgs>(proxy_WcStoredProc_RemoveCompleted);
                proxy.WcStoredProc_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_RemoveCompleted(object sender, WcStoredProc_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_RemoveCompletedEventArgs> handler = WcStoredProc_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcStoredProc_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


