using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  1/9/2018 12:29:00 AM

namespace ProxyWrapper
{
    public partial class WcStoredProcService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcStoredProc_GetByIdCompletedEventArgs> WcStoredProc_GetByIdCompleted;
        public event System.EventHandler<WcStoredProc_GetAllCompletedEventArgs> WcStoredProc_GetAllCompleted;
        public event System.EventHandler<WcStoredProc_GetListByFKCompletedEventArgs> WcStoredProc_GetListByFKCompleted;
        public event System.EventHandler<WcStoredProc_SaveCompletedEventArgs> WcStoredProc_SaveCompleted;
        public event System.EventHandler<WcStoredProc_DeleteCompletedEventArgs> WcStoredProc_DeleteCompleted;
        public event System.EventHandler<WcStoredProc_SetIsDeletedCompletedEventArgs> WcStoredProc_SetIsDeletedCompleted;
        public event System.EventHandler<WcStoredProc_DeactivateCompletedEventArgs> WcStoredProc_DeactivateCompleted;
        public event System.EventHandler<WcStoredProc_RemoveCompletedEventArgs> WcStoredProc_RemoveCompleted;
		public event System.EventHandler<GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompletedEventArgs> GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted;
		public event System.EventHandler<GetWcStoredProcParam_ComboItemListCompletedEventArgs> GetWcStoredProcParam_ComboItemListCompleted;
		public event System.EventHandler<GetWcStoredProcGroup_AssignmentsCompletedEventArgs> GetWcStoredProcGroup_AssignmentsCompleted;
		public event System.EventHandler<ExecutewcStoredProcGroup_AssignSprocsCompletedEventArgs> ExecutewcStoredProcGroup_AssignSprocsCompleted;
		public event System.EventHandler<GetWcStoredProc_ConnectionInfoCompletedEventArgs> GetWcStoredProc_ConnectionInfoCompleted;
		public event System.EventHandler<GetWcDataTypeDefaultMappingDotNet2Sql_lstCompletedEventArgs> GetWcDataTypeDefaultMappingDotNet2Sql_lstCompleted;
		public event System.EventHandler<ExecuteWcStoredProc_importFromSourceDbCompletedEventArgs> ExecuteWcStoredProc_importFromSourceDbCompleted;
		public event System.EventHandler<ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompletedEventArgs> ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcStoredProc_GetById

        public void Begin_WcStoredProc_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetById", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProc_GetByIdCompleted += new EventHandler<WcStoredProc_GetByIdCompletedEventArgs>(proxy_WcStoredProc_GetByIdCompleted);
                proxy.WcStoredProc_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_GetByIdCompleted(object sender, WcStoredProc_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_GetByIdCompletedEventArgs> handler = WcStoredProc_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_GetById

        #region WcStoredProc_GetAll

        public void Begin_WcStoredProc_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetAll", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProc_GetAllCompleted += new EventHandler<WcStoredProc_GetAllCompletedEventArgs>(proxy_WcStoredProc_GetAllCompleted);
                proxy.WcStoredProc_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_GetAllCompleted(object sender, WcStoredProc_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_GetAllCompletedEventArgs> handler = WcStoredProc_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_GetAll

        #region WcStoredProc_GetListByFK

        public void Begin_WcStoredProc_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetListByFK", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProc_GetListByFKCompleted += new EventHandler<WcStoredProc_GetListByFKCompletedEventArgs>(proxy_WcStoredProc_GetListByFKCompleted);
                proxy.WcStoredProc_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_GetListByFKCompleted(object sender, WcStoredProc_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_GetListByFKCompletedEventArgs> handler = WcStoredProc_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_GetListByFK

        #region WcStoredProc_Save

        public void Begin_WcStoredProc_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Save", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                //proxy.WcStoredProc_SaveCompleted += new EventHandler<WcStoredProc_SaveCompletedEventArgs>(proxy_WcStoredProc_SaveCompleted);
                proxy.WcStoredProc_SaveCompleted += new EventHandler<WcStoredProc_SaveCompletedEventArgs>(proxy_WcStoredProc_SaveCompleted);
                proxy.WcStoredProc_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_SaveCompleted(object sender, WcStoredProc_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_SaveCompletedEventArgs> handler = WcStoredProc_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_Save

        #region WcStoredProc_Delete

        public void Begin_WcStoredProc_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_DeleteCompleted", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProc_DeleteCompleted += new EventHandler<WcStoredProc_DeleteCompletedEventArgs>(proxy_WcStoredProc_DeleteCompleted);
                proxy.WcStoredProc_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_DeleteCompleted(object sender, WcStoredProc_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_DeleteCompletedEventArgs> handler = WcStoredProc_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_Delete

        #region WcStoredProc_SetIsDeleted

        public void Begin_WcStoredProc_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProc_SetIsDeletedCompleted += new EventHandler<WcStoredProc_SetIsDeletedCompletedEventArgs>(proxy_WcStoredProc_SetIsDeletedCompleted);
                proxy.WcStoredProc_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_SetIsDeletedCompleted(object sender, WcStoredProc_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_SetIsDeletedCompletedEventArgs> handler = WcStoredProc_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_SetIsDeleted

        #region WcStoredProc_Deactivate

        public void Begin_WcStoredProc_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Deactivate", IfxTraceCategory.Enter);
            WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
            AssignCredentials(proxy);
            proxy.WcStoredProc_DeactivateCompleted += new EventHandler<WcStoredProc_DeactivateCompletedEventArgs>(proxy_WcStoredProc_DeactivateCompleted);
            proxy.WcStoredProc_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_DeactivateCompleted(object sender, WcStoredProc_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_DeactivateCompletedEventArgs> handler = WcStoredProc_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_Deactivate

        #region WcStoredProc_Remove

        public void Begin_WcStoredProc_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Remove", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProc_RemoveCompleted += new EventHandler<WcStoredProc_RemoveCompletedEventArgs>(proxy_WcStoredProc_RemoveCompleted);
                proxy.WcStoredProc_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProc_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProc_RemoveCompleted(object sender, WcStoredProc_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProc_RemoveCompletedEventArgs> handler = WcStoredProc_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcStoredProc_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #region GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyId

        public void Begin_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyId(Guid ApVrsn_Id, Guid? DbCnSK_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyId", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted += new EventHandler<GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompletedEventArgs>(proxy_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted);
                proxy.GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdAsync(ApVrsn_Id, DbCnSK_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyId", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyId", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted(object sender, GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyId", IfxTraceCategory.Enter);
                System.EventHandler<GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompletedEventArgs> handler = GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyId", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyId", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyId

        #region GetWcStoredProcParam_ComboItemList

        public void Begin_GetWcStoredProcParam_ComboItemList(Guid Sp_ID )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcParam_ComboItemList", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcStoredProcParam_ComboItemListCompleted += new EventHandler<GetWcStoredProcParam_ComboItemListCompletedEventArgs>(proxy_GetWcStoredProcParam_ComboItemListCompleted);
                proxy.GetWcStoredProcParam_ComboItemListAsync(Sp_ID );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcParam_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcParam_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcStoredProcParam_ComboItemListCompleted(object sender, GetWcStoredProcParam_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParam_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcStoredProcParam_ComboItemListCompletedEventArgs> handler = GetWcStoredProcParam_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParam_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParam_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcStoredProcParam_ComboItemList

        #region GetWcStoredProcGroup_Assignments

        public void Begin_GetWcStoredProcGroup_Assignments(Guid ApVrsn_Id, Guid Sp_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcGroup_Assignments", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcStoredProcGroup_AssignmentsCompleted += new EventHandler<GetWcStoredProcGroup_AssignmentsCompletedEventArgs>(proxy_GetWcStoredProcGroup_AssignmentsCompleted);
                proxy.GetWcStoredProcGroup_AssignmentsAsync(ApVrsn_Id, Sp_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcGroup_Assignments", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProcGroup_Assignments", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcStoredProcGroup_AssignmentsCompleted(object sender, GetWcStoredProcGroup_AssignmentsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcGroup_Assignments", IfxTraceCategory.Enter);
                System.EventHandler<GetWcStoredProcGroup_AssignmentsCompletedEventArgs> handler = GetWcStoredProcGroup_AssignmentsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcGroup_Assignments", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcGroup_Assignments", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcStoredProcGroup_Assignments

        #region ExecutewcStoredProcGroup_AssignSprocs

        public void Begin_ExecutewcStoredProcGroup_AssignSprocs(Boolean Insert, Guid SpGrp2Sp_SpGrp_Id, Guid SpGrp2Sp_Sp_Id, Guid SpGrp2Sp_CreatedUserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecutewcStoredProcGroup_AssignSprocs", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.ExecutewcStoredProcGroup_AssignSprocsCompleted += new EventHandler<ExecutewcStoredProcGroup_AssignSprocsCompletedEventArgs>(proxy_ExecutewcStoredProcGroup_AssignSprocsCompleted);
                proxy.ExecutewcStoredProcGroup_AssignSprocsAsync(Insert, SpGrp2Sp_SpGrp_Id, SpGrp2Sp_Sp_Id, SpGrp2Sp_CreatedUserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecutewcStoredProcGroup_AssignSprocs", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecutewcStoredProcGroup_AssignSprocs", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecutewcStoredProcGroup_AssignSprocsCompleted(object sender, ExecutewcStoredProcGroup_AssignSprocsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcStoredProcGroup_AssignSprocs", IfxTraceCategory.Enter);
                System.EventHandler<ExecutewcStoredProcGroup_AssignSprocsCompletedEventArgs> handler = ExecutewcStoredProcGroup_AssignSprocsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcStoredProcGroup_AssignSprocs", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcStoredProcGroup_AssignSprocs", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecutewcStoredProcGroup_AssignSprocs

        #region GetWcStoredProc_ConnectionInfo

        public void Begin_GetWcStoredProc_ConnectionInfo(Guid Sp_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProc_ConnectionInfo", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcStoredProc_ConnectionInfoCompleted += new EventHandler<GetWcStoredProc_ConnectionInfoCompletedEventArgs>(proxy_GetWcStoredProc_ConnectionInfoCompleted);
                proxy.GetWcStoredProc_ConnectionInfoAsync(Sp_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProc_ConnectionInfo", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcStoredProc_ConnectionInfo", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcStoredProc_ConnectionInfoCompleted(object sender, GetWcStoredProc_ConnectionInfoCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ConnectionInfo", IfxTraceCategory.Enter);
                System.EventHandler<GetWcStoredProc_ConnectionInfoCompletedEventArgs> handler = GetWcStoredProc_ConnectionInfoCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ConnectionInfo", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ConnectionInfo", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcStoredProc_ConnectionInfo

        #region GetWcDataTypeDefaultMappingDotNet2Sql_lst

        public void Begin_GetWcDataTypeDefaultMappingDotNet2Sql_lst()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcDataTypeDefaultMappingDotNet2Sql_lst", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcDataTypeDefaultMappingDotNet2Sql_lstCompleted += new EventHandler<GetWcDataTypeDefaultMappingDotNet2Sql_lstCompletedEventArgs>(proxy_GetWcDataTypeDefaultMappingDotNet2Sql_lstCompleted);
                proxy.GetWcDataTypeDefaultMappingDotNet2Sql_lstAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcDataTypeDefaultMappingDotNet2Sql_lst", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcDataTypeDefaultMappingDotNet2Sql_lst", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcDataTypeDefaultMappingDotNet2Sql_lstCompleted(object sender, GetWcDataTypeDefaultMappingDotNet2Sql_lstCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDefaultMappingDotNet2Sql_lst", IfxTraceCategory.Enter);
                System.EventHandler<GetWcDataTypeDefaultMappingDotNet2Sql_lstCompletedEventArgs> handler = GetWcDataTypeDefaultMappingDotNet2Sql_lstCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDefaultMappingDotNet2Sql_lst", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDefaultMappingDotNet2Sql_lst", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcDataTypeDefaultMappingDotNet2Sql_lst

        #region ExecuteWcStoredProc_importFromSourceDb

        public void Begin_ExecuteWcStoredProc_importFromSourceDb(Guid ApVrsn_Id, Guid? DbCnSK_Id, String SprocName, Int32 ImportType, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcStoredProc_importFromSourceDb", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.ExecuteWcStoredProc_importFromSourceDbCompleted += new EventHandler<ExecuteWcStoredProc_importFromSourceDbCompletedEventArgs>(proxy_ExecuteWcStoredProc_importFromSourceDbCompleted);
                proxy.ExecuteWcStoredProc_importFromSourceDbAsync(ApVrsn_Id, DbCnSK_Id, SprocName, ImportType, UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcStoredProc_importFromSourceDb", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcStoredProc_importFromSourceDb", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecuteWcStoredProc_importFromSourceDbCompleted(object sender, ExecuteWcStoredProc_importFromSourceDbCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcStoredProc_importFromSourceDb", IfxTraceCategory.Enter);
                System.EventHandler<ExecuteWcStoredProc_importFromSourceDbCompletedEventArgs> handler = ExecuteWcStoredProc_importFromSourceDbCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcStoredProc_importFromSourceDb", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcStoredProc_importFromSourceDb", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecuteWcStoredProc_importFromSourceDb

        #region ExecuteWcStoredProc_ParamAndValueGroup_Refresh

        public void Begin_ExecuteWcStoredProc_ParamAndValueGroup_Refresh(Guid Sp_Id, Int32 RefreshType, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcStoredProc_ParamAndValueGroup_Refresh", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompleted += new EventHandler<ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompletedEventArgs>(proxy_ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompleted);
                proxy.ExecuteWcStoredProc_ParamAndValueGroup_RefreshAsync(Sp_Id, RefreshType, UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcStoredProc_ParamAndValueGroup_Refresh", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteWcStoredProc_ParamAndValueGroup_Refresh", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompleted(object sender, ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcStoredProc_ParamAndValueGroup_Refresh", IfxTraceCategory.Enter);
                System.EventHandler<ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompletedEventArgs> handler = ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcStoredProc_ParamAndValueGroup_Refresh", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcStoredProc_ParamAndValueGroup_Refresh", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecuteWcStoredProc_ParamAndValueGroup_Refresh

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


