using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/23/2017 1:26:42 AM

namespace ProxyWrapper
{
    public partial class WcStoredProcService_ProxyWrapper
    {

        #region Initialize Variables

        public event System.EventHandler<RefreshStoredProcedureColumnsCompletedEventArgs> RefreshStoredProcedureColumnsCompleted;

        #endregion Initialize Variables




        #region RefreshStoredProcedureColumns

        public void Begin_RefreshStoredProcedureColumns(Guid sp_Id, Guid? DbCnSK_Id, Guid userId, int refreshType)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_RefreshStoredProcedureColumns", IfxTraceCategory.Enter);
                WcStoredProcServiceClient proxy = new WcStoredProcServiceClient();
                AssignCredentials(proxy);
                proxy.RefreshStoredProcedureColumnsCompleted += new EventHandler<RefreshStoredProcedureColumnsCompletedEventArgs>(proxy_RefreshStoredProcedureColumnsCompleted);
                proxy.RefreshStoredProcedureColumnsAsync(sp_Id, DbCnSK_Id, userId, refreshType);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_RefreshStoredProcedureColumns", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_RefreshStoredProcedureColumns", IfxTraceCategory.Leave);
            }
        }

        void proxy_RefreshStoredProcedureColumnsCompleted(object sender, RefreshStoredProcedureColumnsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshStoredProcedureColumns", IfxTraceCategory.Enter);
                System.EventHandler<RefreshStoredProcedureColumnsCompletedEventArgs> handler = RefreshStoredProcedureColumnsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshStoredProcedureColumns", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshStoredProcedureColumns", IfxTraceCategory.Leave);
            }
        }

        #endregion RefreshStoredProcedureColumns





    }
}


