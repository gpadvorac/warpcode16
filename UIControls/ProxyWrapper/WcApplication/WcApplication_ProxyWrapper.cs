using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  1/11/2018 7:49:07 PM

namespace ProxyWrapper
{
    public partial class WcApplicationService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcApplication_GetByIdCompletedEventArgs> WcApplication_GetByIdCompleted;
        public event System.EventHandler<WcApplication_GetAllCompletedEventArgs> WcApplication_GetAllCompleted;
        public event System.EventHandler<WcApplication_GetListByFKCompletedEventArgs> WcApplication_GetListByFKCompleted;
        public event System.EventHandler<WcApplication_SaveCompletedEventArgs> WcApplication_SaveCompleted;
        public event System.EventHandler<WcApplication_DeleteCompletedEventArgs> WcApplication_DeleteCompleted;
        public event System.EventHandler<WcApplication_SetIsDeletedCompletedEventArgs> WcApplication_SetIsDeletedCompleted;
        public event System.EventHandler<WcApplication_DeactivateCompletedEventArgs> WcApplication_DeactivateCompleted;
        public event System.EventHandler<WcApplication_RemoveCompletedEventArgs> WcApplication_RemoveCompleted;

        public event System.EventHandler<GetWcApplication_ReadOnlyStaticListsCompletedEventArgs> GetWcApplication_ReadOnlyStaticListsCompleted;
		public event System.EventHandler<GetWcCulture_ComboItemListCompletedEventArgs> GetWcCulture_ComboItemListCompleted;
		public event System.EventHandler<GetWcAppDbSchema_ComboItemListCompletedEventArgs> GetWcAppDbSchema_ComboItemListCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcApplication_GetById

        public void Begin_WcApplication_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_GetById", IfxTraceCategory.Enter);
                WcApplicationServiceClient proxy = new WcApplicationServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplication_GetByIdCompleted += new EventHandler<WcApplication_GetByIdCompletedEventArgs>(proxy_WcApplication_GetByIdCompleted);
                proxy.WcApplication_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplication_GetByIdCompleted(object sender, WcApplication_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplication_GetByIdCompletedEventArgs> handler = WcApplication_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplication_GetById

        #region WcApplication_GetAll

        public void Begin_WcApplication_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_GetAll", IfxTraceCategory.Enter);
                WcApplicationServiceClient proxy = new WcApplicationServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplication_GetAllCompleted += new EventHandler<WcApplication_GetAllCompletedEventArgs>(proxy_WcApplication_GetAllCompleted);
                proxy.WcApplication_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplication_GetAllCompleted(object sender, WcApplication_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplication_GetAllCompletedEventArgs> handler = WcApplication_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplication_GetAll

        #region WcApplication_GetListByFK

        public void Begin_WcApplication_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_GetListByFK", IfxTraceCategory.Enter);
                WcApplicationServiceClient proxy = new WcApplicationServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplication_GetListByFKCompleted += new EventHandler<WcApplication_GetListByFKCompletedEventArgs>(proxy_WcApplication_GetListByFKCompleted);
                proxy.WcApplication_GetListByFKAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplication_GetListByFKCompleted(object sender, WcApplication_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplication_GetListByFKCompletedEventArgs> handler = WcApplication_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplication_GetListByFK

        #region WcApplication_Save

        public void Begin_WcApplication_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_Save", IfxTraceCategory.Enter);
                WcApplicationServiceClient proxy = new WcApplicationServiceClient();
                AssignCredentials(proxy);
                //proxy.WcApplication_SaveCompleted += new EventHandler<WcApplication_SaveCompletedEventArgs>(proxy_WcApplication_SaveCompleted);
                proxy.WcApplication_SaveCompleted += new EventHandler<WcApplication_SaveCompletedEventArgs>(proxy_WcApplication_SaveCompleted);
                proxy.WcApplication_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplication_SaveCompleted(object sender, WcApplication_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplication_SaveCompletedEventArgs> handler = WcApplication_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplication_Save

        #region WcApplication_Delete

        public void Begin_WcApplication_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_DeleteCompleted", IfxTraceCategory.Enter);
                WcApplicationServiceClient proxy = new WcApplicationServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplication_DeleteCompleted += new EventHandler<WcApplication_DeleteCompletedEventArgs>(proxy_WcApplication_DeleteCompleted);
                proxy.WcApplication_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplication_DeleteCompleted(object sender, WcApplication_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplication_DeleteCompletedEventArgs> handler = WcApplication_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplication_Delete

        #region WcApplication_SetIsDeleted

        public void Begin_WcApplication_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcApplicationServiceClient proxy = new WcApplicationServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplication_SetIsDeletedCompleted += new EventHandler<WcApplication_SetIsDeletedCompletedEventArgs>(proxy_WcApplication_SetIsDeletedCompleted);
                proxy.WcApplication_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplication_SetIsDeletedCompleted(object sender, WcApplication_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplication_SetIsDeletedCompletedEventArgs> handler = WcApplication_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplication_SetIsDeleted

        #region WcApplication_Deactivate

        public void Begin_WcApplication_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_Deactivate", IfxTraceCategory.Enter);
            WcApplicationServiceClient proxy = new WcApplicationServiceClient();
            AssignCredentials(proxy);
            proxy.WcApplication_DeactivateCompleted += new EventHandler<WcApplication_DeactivateCompletedEventArgs>(proxy_WcApplication_DeactivateCompleted);
            proxy.WcApplication_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplication_DeactivateCompleted(object sender, WcApplication_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplication_DeactivateCompletedEventArgs> handler = WcApplication_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplication_Deactivate

        #region WcApplication_Remove

        public void Begin_WcApplication_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_Remove", IfxTraceCategory.Enter);
                WcApplicationServiceClient proxy = new WcApplicationServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplication_RemoveCompleted += new EventHandler<WcApplication_RemoveCompletedEventArgs>(proxy_WcApplication_RemoveCompleted);
                proxy.WcApplication_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplication_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplication_RemoveCompleted(object sender, WcApplication_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplication_RemoveCompletedEventArgs> handler = WcApplication_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcApplication_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists

        public void Begin_GetWcApplication_ReadOnlyStaticLists(Guid Ap_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcApplication_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                WcApplicationServiceClient proxy = new WcApplicationServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcApplication_ReadOnlyStaticListsCompleted += new EventHandler<GetWcApplication_ReadOnlyStaticListsCompletedEventArgs>(proxy_GetWcApplication_ReadOnlyStaticListsCompleted);
                proxy.GetWcApplication_ReadOnlyStaticListsAsync(Ap_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcApplication_ReadOnlyStaticLists", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcApplication_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcApplication_ReadOnlyStaticListsCompleted(object sender, GetWcApplication_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetWcApplication_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                System.EventHandler<GetWcApplication_ReadOnlyStaticListsCompletedEventArgs> handler = GetWcApplication_ReadOnlyStaticListsCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetWcApplication_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "proxy_GetWcApplication_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion ReadOnlyStaticLists

        #region GetWcCulture_ComboItemList

        public void Begin_GetWcCulture_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCulture_ComboItemList", IfxTraceCategory.Enter);
                WcApplicationServiceClient proxy = new WcApplicationServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcCulture_ComboItemListCompleted += new EventHandler<GetWcCulture_ComboItemListCompletedEventArgs>(proxy_GetWcCulture_ComboItemListCompleted);
                proxy.GetWcCulture_ComboItemListAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCulture_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcCulture_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcCulture_ComboItemListCompleted(object sender, GetWcCulture_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCulture_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcCulture_ComboItemListCompletedEventArgs> handler = GetWcCulture_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCulture_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCulture_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcCulture_ComboItemList

        #region GetWcAppDbSchema_ComboItemList

        public void Begin_GetWcAppDbSchema_ComboItemList(Guid Ap_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcAppDbSchema_ComboItemList", IfxTraceCategory.Enter);
                WcApplicationServiceClient proxy = new WcApplicationServiceClient();
                AssignCredentials(proxy);
                proxy.GetWcAppDbSchema_ComboItemListCompleted += new EventHandler<GetWcAppDbSchema_ComboItemListCompletedEventArgs>(proxy_GetWcAppDbSchema_ComboItemListCompleted);
                proxy.GetWcAppDbSchema_ComboItemListAsync(Ap_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcAppDbSchema_ComboItemList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetWcAppDbSchema_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetWcAppDbSchema_ComboItemListCompleted(object sender, GetWcAppDbSchema_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcAppDbSchema_ComboItemList", IfxTraceCategory.Enter);
                System.EventHandler<GetWcAppDbSchema_ComboItemListCompletedEventArgs> handler = GetWcAppDbSchema_ComboItemListCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcAppDbSchema_ComboItemList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcAppDbSchema_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        #endregion GetWcAppDbSchema_ComboItemList

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


