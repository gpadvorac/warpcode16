using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;

// Gen Timestamp:  1/7/2018 9:20:12 PM

namespace EntityBll.SL
{

    public partial class WcApplication_Bll_staticLists
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcApplication_Bll_staticLists";



        private static ComboItemList _wcApplicationConnectionStringKey_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _wcApplicationConnectionStringKey_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _wcCulture_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _wcCulture_ComboItemList_BindingList_HasItems = true;

        static bool isDataLoaded = false;
        static ProxyWrapper.WcApplicationService_ProxyWrapper _staticWcApplicationProxy = null;

        #endregion Initialize Variables


        #region Load Data

        public static void InitializeProxyWrapper()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Enter);

                if (_staticWcApplicationProxy == null)
                {
                    _staticWcApplicationProxy = new ProxyWrapper.WcApplicationService_ProxyWrapper();
                    _staticWcApplicationProxy.GetWcApplication_ReadOnlyStaticListsCompleted += new EventHandler<GetWcApplication_ReadOnlyStaticListsCompletedEventArgs>(GetWcApplication_ReadOnlyStaticListsCompleted);
                    _staticWcApplicationProxy.GetWcApplicationConnectionStringKey_ComboItemListCompleted += new EventHandler<GetWcApplicationConnectionStringKey_ComboItemListCompletedEventArgs>(GetWcApplicationConnectionStringKey_ComboItemListCompleted);
                    _staticWcApplicationProxy.GetWcCulture_ComboItemListCompleted += new EventHandler<GetWcCulture_ComboItemListCompletedEventArgs>(GetWcCulture_ComboItemListCompleted);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Leave);
            }
        }

        public static void LoadStaticLists(Guid ApCnSK_Ap_ID )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Enter);

                if (_staticWcApplicationProxy == null)
                {
                    InitializeProxyWrapper();
                }

                _staticWcApplicationProxy.Begin_GetWcApplication_ReadOnlyStaticLists(ApCnSK_Ap_ID );

                LoadStaticLists_Custom();
                isDataLoaded = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Leave);
            }
        }

        static void GetWcApplication_ReadOnlyStaticListsCompleted(object sender, GetWcApplication_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcApplication_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                if (data != null)
                {


                    // WcApplicationConnectionStringKey_ComboItemList
                    _wcApplicationConnectionStringKey_ComboItemList_BindingList.IsRefreshingData = true;
                    _wcApplicationConnectionStringKey_ComboItemList_BindingList.CachedList.Clear();
                    _wcApplicationConnectionStringKey_ComboItemList_BindingList.Clear();
                    _wcApplicationConnectionStringKey_ComboItemList_BindingList.ReplaceList((object[])data[0]);

                    _wcApplicationConnectionStringKey_ComboItemList_BindingList.IsRefreshingData = false;

                    // WcCulture_ComboItemList
                    _wcCulture_ComboItemList_BindingList.IsRefreshingData = true;
                    _wcCulture_ComboItemList_BindingList.CachedList.Clear();
                    _wcCulture_ComboItemList_BindingList.Clear();
                    _wcCulture_ComboItemList_BindingList.ReplaceList((object[])data[1]);

                    _wcCulture_ComboItemList_BindingList.IsRefreshingData = false;
                    isDataLoaded = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcApplication_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcApplication_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Load Data


        #region Properties

        public static bool IsDataLoaded
        {
            get { return isDataLoaded; }
        }

                    // WcApplicationConnectionStringKey_ComboItemList
        public static ComboItemList WcApplicationConnectionStringKey_ComboItemList_BindingListProperty
        {
            get
            {
                return _wcApplicationConnectionStringKey_ComboItemList_BindingList;
            }
            set
            {
                _wcApplicationConnectionStringKey_ComboItemList_BindingList = value;
            }
        }

                    // WcCulture_ComboItemList
        public static ComboItemList WcCulture_ComboItemList_BindingListProperty
        {
            get
            {
                return _wcCulture_ComboItemList_BindingList;
            }
            set
            {
                _wcCulture_ComboItemList_BindingList = value;
            }
        }

        #endregion Properties


        #region RefreshLists



        #region WcApplicationConnectionStringKey_ComboItemList

        public static void Refresh_WcApplicationConnectionStringKey_ComboItemList_BindingList(Guid ApCnSK_Ap_ID )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcApplicationConnectionStringKey_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticWcApplicationProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticWcApplicationProxy.Begin_GetWcApplicationConnectionStringKey_ComboItemList(ApCnSK_Ap_ID );

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcApplicationConnectionStringKey_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcApplicationConnectionStringKey_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetWcApplicationConnectionStringKey_ComboItemListCompleted(object sender, GetWcApplicationConnectionStringKey_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcApplicationConnectionStringKey_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _wcApplicationConnectionStringKey_ComboItemList_BindingList.IsRefreshingData = true;
                _wcApplicationConnectionStringKey_ComboItemList_BindingList.ReplaceList(data);
                _wcApplicationConnectionStringKey_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcApplicationConnectionStringKey_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcApplicationConnectionStringKey_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationConnectionStringKey_ComboItemList



        #region WcCulture_ComboItemList

        public static void Refresh_WcCulture_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcCulture_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticWcApplicationProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticWcApplicationProxy.Begin_GetWcCulture_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcCulture_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcCulture_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetWcCulture_ComboItemListCompleted(object sender, GetWcCulture_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCulture_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _wcCulture_ComboItemList_BindingList.IsRefreshingData = true;
                _wcCulture_ComboItemList_BindingList.ReplaceList(data);
                _wcCulture_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCulture_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCulture_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcCulture_ComboItemList



        #endregion RefreshLists


    }

    #region Classes for Binding

#endregion Classes for Binding

}

