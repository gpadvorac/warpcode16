using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/7/2018 11:06:07 PM

namespace EntityBll.SL
{
    public partial class WcApplication_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_Ap_Name = 75;
		public const int STRINGSIZE_Ap_Desc = 200;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_Ap_Id_Required = "ID is a required field.";
		private const string BROKENRULE_AttachmentCount_ZeroNotAllowed = "Attachments:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_AttachmentFileNames_Required = "AttachmentFileNames is a required field.";
		private const string BROKENRULE_DiscussionCount_Required = "Discussions is a required field.";
		private const string BROKENRULE_DiscussionCount_ZeroNotAllowed = "Discussions:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_DiscussionTitles_Required = "DiscussionTitles is a required field.";
		private string BROKENRULE_Ap_Name_TextLength = "Name has a maximum text length of  '" + STRINGSIZE_Ap_Name + "'.";
		private const string BROKENRULE_Ap_Name_Required = "Name is a required field.";
		private string BROKENRULE_Ap_Desc_TextLength = "Description has a maximum text length of  '" + STRINGSIZE_Ap_Desc + "'.";
		private const string BROKENRULE_Ap_TkSt_Id_ZeroNotAllowed = "Task Status Test:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_Ap_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_Ap_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_Ap_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("Ap_Id", BROKENRULE_Ap_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("AttachmentFileNames", BROKENRULE_AttachmentFileNames_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("AttachmentFileNames", BROKENRULE_AttachmentFileNames_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("DiscussionTitles", BROKENRULE_DiscussionTitles_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("DiscussionTitles", BROKENRULE_DiscussionTitles_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Ap_Name", BROKENRULE_Ap_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Ap_Name", BROKENRULE_Ap_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Ap_Desc", BROKENRULE_Ap_Desc_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Ap_TkSt_Id", BROKENRULE_Ap_TkSt_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("Ap_IsActiveRow", BROKENRULE_Ap_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Ap_IsDeleted", BROKENRULE_Ap_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Ap_Stamp", BROKENRULE_Ap_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void Ap_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_Id");
                string newBrokenRules = "";
                
                if (Ap_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Ap_Id", BROKENRULE_Ap_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Ap_Id", BROKENRULE_Ap_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Ap_Id", _brokenRuleManager.IsPropertyValid("Ap_Id"), IsPropertyDirty("Ap_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void AttachmentCount_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("AttachmentCount");
                string newBrokenRules = "";
                
                if (AttachmentCount == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("AttachmentCount");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("AttachmentCount", _brokenRuleManager.IsPropertyValid("AttachmentCount"), IsPropertyDirty("AttachmentCount"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", IfxTraceCategory.Leave);
            }
        }

        private void AttachmentFileNames_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentFileNames_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("AttachmentFileNames");
                string newBrokenRules = "";
                				int len = 0;
                if (AttachmentFileNames != null)
                {
                    len = AttachmentFileNames.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("AttachmentFileNames", BROKENRULE_AttachmentFileNames_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("AttachmentFileNames", BROKENRULE_AttachmentFileNames_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("AttachmentFileNames");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("AttachmentFileNames", _brokenRuleManager.IsPropertyValid("AttachmentFileNames"), IsPropertyDirty("AttachmentFileNames"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentFileNames_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentFileNames_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DiscussionCount_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DiscussionCount");
                string newBrokenRules = "";
                
                if (DiscussionCount == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_Required);
                }

                if (DiscussionCount == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DiscussionCount");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DiscussionCount", _brokenRuleManager.IsPropertyValid("DiscussionCount"), IsPropertyDirty("DiscussionCount"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DiscussionTitles_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionTitles_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DiscussionTitles");
                string newBrokenRules = "";
                				int len = 0;
                if (DiscussionTitles != null)
                {
                    len = DiscussionTitles.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DiscussionTitles", BROKENRULE_DiscussionTitles_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DiscussionTitles", BROKENRULE_DiscussionTitles_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DiscussionTitles");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DiscussionTitles", _brokenRuleManager.IsPropertyValid("DiscussionTitles"), IsPropertyDirty("DiscussionTitles"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionTitles_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionTitles_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Ap_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (Ap_Name != null)
                {
                    len = Ap_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Ap_Name", BROKENRULE_Ap_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Ap_Name", BROKENRULE_Ap_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Ap_Name", BROKENRULE_Ap_Name_Required);
                    if (len > STRINGSIZE_Ap_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Ap_Name", BROKENRULE_Ap_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Ap_Name", BROKENRULE_Ap_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Ap_Name", _brokenRuleManager.IsPropertyValid("Ap_Name"), IsPropertyDirty("Ap_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Ap_Desc_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_Desc_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_Desc");
                string newBrokenRules = "";
                				int len = 0;
                if (Ap_Desc != null)
                {
                    len = Ap_Desc.Length;
                }

                if (len > STRINGSIZE_Ap_Desc)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Ap_Desc", BROKENRULE_Ap_Desc_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Ap_Desc", BROKENRULE_Ap_Desc_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_Desc");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Ap_Desc", _brokenRuleManager.IsPropertyValid("Ap_Desc"), IsPropertyDirty("Ap_Desc"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_Desc_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_Desc_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Ap_TkSt_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_TkSt_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_TkSt_Id");
                string newBrokenRules = "";
                
                if (Ap_TkSt_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Ap_TkSt_Id", BROKENRULE_Ap_TkSt_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Ap_TkSt_Id", BROKENRULE_Ap_TkSt_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_TkSt_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Ap_TkSt_Id", _brokenRuleManager.IsPropertyValid("Ap_TkSt_Id"), IsPropertyDirty("Ap_TkSt_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_TkSt_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_TkSt_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Ap_TkSt_Id_TextField_Validate()
        {
        }

        private void Ap_v_Application_Id_Validate()
        {
        }

        private void Ap_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_IsActiveRow");
                string newBrokenRules = "";
                
                if (Ap_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Ap_IsActiveRow", BROKENRULE_Ap_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Ap_IsActiveRow", BROKENRULE_Ap_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Ap_IsActiveRow", _brokenRuleManager.IsPropertyValid("Ap_IsActiveRow"), IsPropertyDirty("Ap_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Ap_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_IsDeleted");
                string newBrokenRules = "";
                
                if (Ap_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Ap_IsDeleted", BROKENRULE_Ap_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Ap_IsDeleted", BROKENRULE_Ap_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Ap_IsDeleted", _brokenRuleManager.IsPropertyValid("Ap_IsDeleted"), IsPropertyDirty("Ap_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Ap_CreatedUserId_Validate()
        {
        }

        private void Ap_CreatedDate_Validate()
        {
        }

        private void Ap_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Ap_LastModifiedDate_Validate()
        {
        }

        private void Ap_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_Stamp");
                string newBrokenRules = "";
                
                if (Ap_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Ap_Stamp", BROKENRULE_Ap_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Ap_Stamp", BROKENRULE_Ap_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Ap_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Ap_Stamp", _brokenRuleManager.IsPropertyValid("Ap_Stamp"), IsPropertyDirty("Ap_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Ap_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


