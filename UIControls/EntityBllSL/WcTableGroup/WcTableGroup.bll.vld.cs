using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/15/2017 12:04:55 AM

namespace EntityBll.SL
{
    public partial class WcTableGroup_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_TbGrp_Name = 50;
		public const int STRINGSIZE_TbGrp_Desc = 200;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_TbGrp_Id_Required = "Id is a required field.";
		private const string BROKENRULE_TbGrp_ApVrsn_Id_Required = "ApVrsn_Id is a required field.";
		private const string BROKENRULE_TbGrp_SortOrder_ZeroNotAllowed = "Sort Order:  '0'  (zero) is not allowed.";
		private string BROKENRULE_TbGrp_Name_TextLength = "Table Group Name has a maximum text length of  '" + STRINGSIZE_TbGrp_Name + "'.";
		private string BROKENRULE_TbGrp_Desc_TextLength = "Description has a maximum text length of  '" + STRINGSIZE_TbGrp_Desc + "'.";
		private const string BROKENRULE_TbGrp_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_TbGrp_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_TbGrp_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("TbGrp_Id", BROKENRULE_TbGrp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbGrp_ApVrsn_Id", BROKENRULE_TbGrp_ApVrsn_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbGrp_SortOrder", BROKENRULE_TbGrp_SortOrder_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbGrp_Name", BROKENRULE_TbGrp_Name_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbGrp_Desc", BROKENRULE_TbGrp_Desc_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbGrp_IsActiveRow", BROKENRULE_TbGrp_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbGrp_IsDeleted", BROKENRULE_TbGrp_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbGrp_Stamp", BROKENRULE_TbGrp_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void TbGrp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_Id");
                string newBrokenRules = "";
                
                if (TbGrp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbGrp_Id", BROKENRULE_TbGrp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbGrp_Id", BROKENRULE_TbGrp_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbGrp_Id", _brokenRuleManager.IsPropertyValid("TbGrp_Id"), IsPropertyDirty("TbGrp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbGrp_ApVrsn_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_ApVrsn_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_ApVrsn_Id");
                string newBrokenRules = "";
                
                if (TbGrp_ApVrsn_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbGrp_ApVrsn_Id", BROKENRULE_TbGrp_ApVrsn_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbGrp_ApVrsn_Id", BROKENRULE_TbGrp_ApVrsn_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_ApVrsn_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbGrp_ApVrsn_Id", _brokenRuleManager.IsPropertyValid("TbGrp_ApVrsn_Id"), IsPropertyDirty("TbGrp_ApVrsn_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_ApVrsn_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_ApVrsn_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbGrp_SortOrder_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_SortOrder_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_SortOrder");
                string newBrokenRules = "";
                
                if (TbGrp_SortOrder == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbGrp_SortOrder", BROKENRULE_TbGrp_SortOrder_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbGrp_SortOrder", BROKENRULE_TbGrp_SortOrder_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_SortOrder");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbGrp_SortOrder", _brokenRuleManager.IsPropertyValid("TbGrp_SortOrder"), IsPropertyDirty("TbGrp_SortOrder"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_SortOrder_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_SortOrder_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbGrp_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (TbGrp_Name != null)
                {
                    len = TbGrp_Name.Length;
                }

                if (len > STRINGSIZE_TbGrp_Name)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbGrp_Name", BROKENRULE_TbGrp_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbGrp_Name", BROKENRULE_TbGrp_Name_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbGrp_Name", _brokenRuleManager.IsPropertyValid("TbGrp_Name"), IsPropertyDirty("TbGrp_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbGrp_Desc_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_Desc_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_Desc");
                string newBrokenRules = "";
                				int len = 0;
                if (TbGrp_Desc != null)
                {
                    len = TbGrp_Desc.Length;
                }

                if (len > STRINGSIZE_TbGrp_Desc)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbGrp_Desc", BROKENRULE_TbGrp_Desc_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbGrp_Desc", BROKENRULE_TbGrp_Desc_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_Desc");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbGrp_Desc", _brokenRuleManager.IsPropertyValid("TbGrp_Desc"), IsPropertyDirty("TbGrp_Desc"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_Desc_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_Desc_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbGrp_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_IsActiveRow");
                string newBrokenRules = "";
                
                if (TbGrp_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbGrp_IsActiveRow", BROKENRULE_TbGrp_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbGrp_IsActiveRow", BROKENRULE_TbGrp_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbGrp_IsActiveRow", _brokenRuleManager.IsPropertyValid("TbGrp_IsActiveRow"), IsPropertyDirty("TbGrp_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbGrp_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_IsDeleted");
                string newBrokenRules = "";
                
                if (TbGrp_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbGrp_IsDeleted", BROKENRULE_TbGrp_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbGrp_IsDeleted", BROKENRULE_TbGrp_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbGrp_IsDeleted", _brokenRuleManager.IsPropertyValid("TbGrp_IsDeleted"), IsPropertyDirty("TbGrp_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbGrp_CreatedUserId_Validate()
        {
        }

        private void TbGrp_CreatedDate_Validate()
        {
        }

        private void TbGrp_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbGrp_LastModifiedDate_Validate()
        {
        }

        private void TbGrp_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_Stamp");
                string newBrokenRules = "";
                
                if (TbGrp_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbGrp_Stamp", BROKENRULE_TbGrp_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbGrp_Stamp", BROKENRULE_TbGrp_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbGrp_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbGrp_Stamp", _brokenRuleManager.IsPropertyValid("TbGrp_Stamp"), IsPropertyDirty("TbGrp_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbGrp_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


