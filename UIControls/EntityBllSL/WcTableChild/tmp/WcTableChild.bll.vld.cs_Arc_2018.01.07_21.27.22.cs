using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/7/2018 9:26:31 PM

namespace EntityBll.SL
{
    public partial class WcTableChild_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_TbChd_Id_Required = "Id is a required field.";
		private const string BROKENRULE_TbChd_SortOrder_Required = "Sort Order is a required field.";
		private const string BROKENRULE_TbChd_SortOrder_ZeroNotAllowed = "Sort Order:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_TbChd_Child_Tb_Id_Required = "Child Table is a required field.";
		private const string BROKENRULE_TbChd_IsDisplayAsChildGrid_Required = "Display As Child Grid is a required field.";
		private const string BROKENRULE_TbChd_IsDisplayAsChildTab_Required = "Display As Child Tab is a required field.";
		private const string BROKENRULE_TbChd_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_TbChd_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_TbChd_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("TbChd_Id", BROKENRULE_TbChd_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbChd_Parent_Tb_Id", BROKENRULE_TbChd_Parent_Tb_Id_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("TbChd_SortOrder", BROKENRULE_TbChd_SortOrder_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbChd_SortOrder", BROKENRULE_TbChd_SortOrder_ZeroNotAllowed);
				_brokenRuleManager.AddBrokenRuleForProperty("TbChd_Child_Tb_Id", BROKENRULE_TbChd_Child_Tb_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbChd_IsDisplayAsChildGrid", BROKENRULE_TbChd_IsDisplayAsChildGrid_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbChd_IsDisplayAsChildTab", BROKENRULE_TbChd_IsDisplayAsChildTab_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbChd_IsActiveRow", BROKENRULE_TbChd_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbChd_IsDeleted", BROKENRULE_TbChd_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbChd_Stamp", BROKENRULE_TbChd_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void TbChd_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_Id");
                string newBrokenRules = "";
                
                if (TbChd_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbChd_Id", BROKENRULE_TbChd_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbChd_Id", BROKENRULE_TbChd_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbChd_Id", _brokenRuleManager.IsPropertyValid("TbChd_Id"), IsPropertyDirty("TbChd_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbChd_Parent_Tb_Id_Validate()
        {
        }

        private void TbChd_SortOrder_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_SortOrder_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_SortOrder");
                string newBrokenRules = "";
                
                if (TbChd_SortOrder == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbChd_SortOrder", BROKENRULE_TbChd_SortOrder_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbChd_SortOrder", BROKENRULE_TbChd_SortOrder_Required);
                }

                if (TbChd_SortOrder == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbChd_SortOrder", BROKENRULE_TbChd_SortOrder_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbChd_SortOrder", BROKENRULE_TbChd_SortOrder_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_SortOrder");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbChd_SortOrder", _brokenRuleManager.IsPropertyValid("TbChd_SortOrder"), IsPropertyDirty("TbChd_SortOrder"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_SortOrder_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_SortOrder_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbChd_Child_Tb_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_Child_Tb_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_Child_Tb_Id");
                string newBrokenRules = "";
                
                if (TbChd_Child_Tb_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbChd_Child_Tb_Id", BROKENRULE_TbChd_Child_Tb_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbChd_Child_Tb_Id", BROKENRULE_TbChd_Child_Tb_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_Child_Tb_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbChd_Child_Tb_Id", _brokenRuleManager.IsPropertyValid("TbChd_Child_Tb_Id"), IsPropertyDirty("TbChd_Child_Tb_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_Child_Tb_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_Child_Tb_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbChd_Child_Tb_Id_TextField_Validate()
        {
        }

        private void TbChd_ChildForeignKeyColumn_Id_Validate()
        {
        }

        private void TbChd_ChildForeignKeyColumn_Id_TextField_Validate()
        {
        }

        private void TbChd_ChildForeignKeyColumn22_Id_Validate()
        {
        }

        private void TbChd_ChildForeignKeyColumn22_Id_TextField_Validate()
        {
        }

        private void TbChd_ChildForeignKeyColumn33_Id_Validate()
        {
        }

        private void TbChd_ChildForeignKeyColumn33_Id_TextField_Validate()
        {
        }

        private void TbChd_DifferentCombo_Id_Validate()
        {
        }

        private void TbChd_DifferentCombo_Id_TextField_Validate()
        {
        }

        private void TbChd_IsDisplayAsChildGrid_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_IsDisplayAsChildGrid_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_IsDisplayAsChildGrid");
                string newBrokenRules = "";
                
                if (TbChd_IsDisplayAsChildGrid == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbChd_IsDisplayAsChildGrid", BROKENRULE_TbChd_IsDisplayAsChildGrid_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbChd_IsDisplayAsChildGrid", BROKENRULE_TbChd_IsDisplayAsChildGrid_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_IsDisplayAsChildGrid");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbChd_IsDisplayAsChildGrid", _brokenRuleManager.IsPropertyValid("TbChd_IsDisplayAsChildGrid"), IsPropertyDirty("TbChd_IsDisplayAsChildGrid"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_IsDisplayAsChildGrid_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_IsDisplayAsChildGrid_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbChd_IsDisplayAsChildTab_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_IsDisplayAsChildTab_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_IsDisplayAsChildTab");
                string newBrokenRules = "";
                
                if (TbChd_IsDisplayAsChildTab == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbChd_IsDisplayAsChildTab", BROKENRULE_TbChd_IsDisplayAsChildTab_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbChd_IsDisplayAsChildTab", BROKENRULE_TbChd_IsDisplayAsChildTab_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_IsDisplayAsChildTab");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbChd_IsDisplayAsChildTab", _brokenRuleManager.IsPropertyValid("TbChd_IsDisplayAsChildTab"), IsPropertyDirty("TbChd_IsDisplayAsChildTab"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_IsDisplayAsChildTab_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_IsDisplayAsChildTab_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbChd_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_IsActiveRow");
                string newBrokenRules = "";
                
                if (TbChd_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbChd_IsActiveRow", BROKENRULE_TbChd_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbChd_IsActiveRow", BROKENRULE_TbChd_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbChd_IsActiveRow", _brokenRuleManager.IsPropertyValid("TbChd_IsActiveRow"), IsPropertyDirty("TbChd_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbChd_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_IsDeleted");
                string newBrokenRules = "";
                
                if (TbChd_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbChd_IsDeleted", BROKENRULE_TbChd_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbChd_IsDeleted", BROKENRULE_TbChd_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbChd_IsDeleted", _brokenRuleManager.IsPropertyValid("TbChd_IsDeleted"), IsPropertyDirty("TbChd_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbChd_CreatedUserId_Validate()
        {
        }

        private void TbChd_CreatedDate_Validate()
        {
        }

        private void TbChd_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbChd_LastModifiedDate_Validate()
        {
        }

        private void TbChd_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_Stamp");
                string newBrokenRules = "";
                
                if (TbChd_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbChd_Stamp", BROKENRULE_TbChd_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbChd_Stamp", BROKENRULE_TbChd_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbChd_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbChd_Stamp", _brokenRuleManager.IsPropertyValid("TbChd_Stamp"), IsPropertyDirty("TbChd_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


