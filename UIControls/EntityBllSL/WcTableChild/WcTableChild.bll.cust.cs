using System;
using TypeServices;
using System.ComponentModel;
using Ifx.SL;
using ProxyWrapper;
using UIControls;
using vComboDataTypes;
using Velocity.SL;

// Gen Timestamp:  12/2/2016 3:31:22 PM

namespace EntityBll.SL
{


    public partial class WcTableChild_Bll //: BusinessObjectBase, ITraceItem, IBusinessObject, IBusinessObjectV2, INotifyPropertyChanged , IEditableObject, IDataErrorInfo
    {




        #region Initialize Custom Variables

        public event ComboListRefreshCompletedEventHandler ComboListRefreshCompleted;

        #endregion Initialize Custom Variables


        #region General Methods


        void InitializeClass_Cust()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", IfxTraceCategory.Enter);
                _WcTableProxy = new WcTableService_ProxyWrapper();
                _WcApplicationProxy = new WcApplicationService_ProxyWrapper();
                _WcTableProxy.TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted += TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted;
                _WcTableProxy.TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted += TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted;
                _WcTableProxy.TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted += TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", IfxTraceCategory.Leave);
            }
        }
                    public void SetStandingFK(string parentType, Guid fk)
                    {
                        Guid? traceId = Guid.NewGuid();
                        try
                        {
                            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Enter);
                            _parentEntityType = parentType;
                            _StandingFK = fk;
                            SetStandingFK();
                        }
                        catch (Exception ex)
                        {
                            if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", ex);
				   // throw IfxWrapperException.GetError(ex);
                        }
                        finally
                        {
                            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Leave);
                        }
                    }
        public void SetStandingFK()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Enter);
//                //switch (_parentEntityType)
//                //{
//                //    case ParentType.Company:
//                //        _data.C._b = _StandingFK;
//                //        break;
//                //    case ParentType.PersonContact:
//                //        _data.C._c = _StandingFK;
//                //        break;
//                //}      
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Leave);
//            }
        }

        // Alternate FKs
        public void SetOtherParentKeys(string parentType, Guid id)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", IfxTraceCategory.Enter);
//                //_parentEntityType = parentType;
//                //switch (_parentEntityType)
//                //{
//                //    case "ucWellDt":
//                //        _data.C.WlD_Id_noevents = id;
//                //        break;
//                //    case "ucContractDt":
//                //        _data.C.CtD_Id_noevents = id;
//                //        break;
//                //}
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", IfxTraceCategory.Leave);
//            }
        }



        #endregion General Methods


        #region Data Access Methods


        #endregion Data Access Methods


        #region Method Extentions For Custom Code



        private void SetCustomDefaultBrokenRules()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", IfxTraceCategory.Enter);
//
//         
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", IfxTraceCategory.Leave);
//            }
        }




        private void CustomPropertySetterCode(string propName)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", IfxTraceCategory.Enter);
//                switch(propName)
//                {
//                    case "xxxx":
//
//                        break;
//
//                }
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", IfxTraceCategory.Leave);
//            }
        }

        private void UnDoCustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", IfxTraceCategory.Enter);
//   
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", IfxTraceCategory.Leave);
//            }
        }

        private void RefreshFieldsCustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", IfxTraceCategory.Enter);
//   
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", IfxTraceCategory.Leave);
//            }
        }

        private void NewEntityRowCustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", IfxTraceCategory.Enter);
//                //SetStandingFK();  //Unrem this when we have FKs
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", IfxTraceCategory.Leave);
//            }
        }

        private bool IsPropertyDirtyCustomCode(string propertyName)
        {
            ////  This is called from 'IsPropertyDirty' for calculated fields that are not represented in the wire objects.  They will always require custom code here.
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", IfxTraceCategory.Enter);
            //    switch (propertyName)
            //    {
            //        case "xxxx":
            //            //  return something
            //            break;
            //        default:
            //            throw new Exception(_as + "." + _cn + ".IsPropertyDirtyCustomCode:  Missing code and logic for " + propertyName + ".");
            //    }
                return false;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        public string GetPropertyValueByKeyCustomCode(string key)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", IfxTraceCategory.Enter);
            //    //switch (key)
            //    //{
            //    //    case "xxxx":
            //    //        return null;
            //    //        return xxx;
            //    //}
                return null;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        public string GetPropertyFormattedStringValueByKeyCustomCode(string key)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", IfxTraceCategory.Enter);
            //    //switch (key)
            //    //{
            //    //    case "xxxx":
            //    //        return null;
            //    //        return xxx;
            //    //}
                return null;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        #endregion Method Extentions For Custom Code


        #region Code Gen Methods For Modification to be copied to here


        #endregion Code Gen Methods For Modification to be copied to here


        #region Code Gen Properties For Modification to be copied to here


        #endregion Code Gen Properties For Modification to be copied to here


        #region Code Gen Validation Logic For Modification to be copied to here


        #endregion Code Gen Validation Logic For Modification to be copied to here


        #region Properties


        #endregion Properties


        #region Validation Logic


        #endregion Validation Logic


        #region Custom Code


        #region Load On Demand Combo Lists


        private WcTableService_ProxyWrapper _WcTableProxy = null;
        private WcApplicationService_ProxyWrapper _WcApplicationProxy = null;
        
        private ComboItemList _TbChd_DifferentCombo_Id_ComboItemList = new ComboItemList(ComboItemList.DataType.GuidType);



        #region TbChd_ChildForeignKeyColumn_Id Combo List

        private ComboItemList _TbChd_ChildForeignKeyColumn_Id_ComboItemList = new ComboItemList(ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);

        public ComboItemList TbChdChildForeignKeyColumnIdComboItemList
        {
            get { return _TbChd_ChildForeignKeyColumn_Id_ComboItemList; }
            set { _TbChd_ChildForeignKeyColumn_Id_ComboItemList = value; }
        }


        public object GetComboItemsSourceFromDatabase(string columnKey)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetComboItemsSourceFromDatabase()", IfxTraceCategory.Enter);
                if (TbChd_Child_Tb_Id != null)
                {
                    byte[] data = _WcTableProxy.TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList_Syncronous((Guid)TbChd_Child_Tb_Id);
                    if (data != null)
                    {
                        object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                        _TbChd_ChildForeignKeyColumn_Id_ComboItemList.ReplaceList(array);
                    }
                    else
                    {
                        _TbChd_ChildForeignKeyColumn_Id_ComboItemList.Clear();
                    }



                    //_WcTableProxy.Begin_TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList((Guid)TbChd_Child_Tb_Id);

                    //_WcTableProxy.TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted += (o, args) =>
                    //{
                    //    byte[] data = args.Result;
                    //    if (data != null)
                    //    {
                    //        object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                    //        _TbChd_ChildForeignKeyColumn_Id_ComboItemList.ReplaceList(array);
                    //    }
                    //    else
                    //    {
                    //        _TbChd_ChildForeignKeyColumn_Id_ComboItemList.Clear();
                    //    }
                    //};
                }
                else
                {
                    _TbChd_ChildForeignKeyColumn_Id_ComboItemList.Clear();
                }
                return _TbChd_ChildForeignKeyColumn_Id_ComboItemList;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetComboItemsSourceFromDatabase()", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetComboItemsSourceFromDatabase()", IfxTraceCategory.Leave);
            }
        }

        public void TbChd_ChildForeignKeyColumn_Id_ComboItemList_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn_Id_DataSource()", IfxTraceCategory.Enter);
                if (TbChd_Child_Tb_Id != null)
                {
                    _WcTableProxy.Begin_TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList((Guid)TbChd_Child_Tb_Id);
                }
                else
                {
                    _TbChd_ChildForeignKeyColumn_Id_ComboItemList.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn_Id_DataSource()", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn_Id_DataSource()", IfxTraceCategory.Leave);
            }
        }
        
        void TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted(object sender, TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChildProxy_GetWcTableColumn_lstByTable_ComboItemListCompleted", IfxTraceCategory.Enter);


                byte[] data = e.Result;
                if (data != null)
                {
                    object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                    _TbChd_ChildForeignKeyColumn_Id_ComboItemList.ReplaceList(array);
                }
                else
                {
                    _TbChd_ChildForeignKeyColumn_Id_ComboItemList.Clear();
                }
                RaiseEventComboListRefreshCompleted("TbChd_ChildForeignKeyColumn_Id");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChildProxy_GetWcTableColumn_lstByTable_ComboItemListCompleted", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChildProxy_GetWcTableColumn_lstByTable_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TbChd_ChildForeignKeyColumn_Id Combo List

        
        #region TbChd_ChildForeignKeyColumn22_Id Combo List

        private ComboItemList _TbChd_ChildForeignKeyColumn22_Id_ComboItemList = new ComboItemList(ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);
       
        public ComboItemList TbChdChildForeignKeyColumn22IdComboItemList
        {
            get { return _TbChd_ChildForeignKeyColumn22_Id_ComboItemList; }
            set { _TbChd_ChildForeignKeyColumn22_Id_ComboItemList = value; }
        }

        void TbChd_ChildForeignKeyColumn22_Id_ComboItemList_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn22_Id_DataSource()", IfxTraceCategory.Enter);
                if (TbChd_Child_Tb_Id != null)
                {
                    _WcTableProxy.Begin_TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList((Guid)TbChd_Child_Tb_Id);
                }
                else
                {
                    _TbChd_ChildForeignKeyColumn22_Id_ComboItemList.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn22_Id_DataSource()", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn22_Id_DataSource()", IfxTraceCategory.Leave);
            }
        }

        void TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted(object sender, TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                if (data != null)
                {
                    object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                    _TbChd_ChildForeignKeyColumn22_Id_ComboItemList.ReplaceList(array);
                }
                else
                {
                    _TbChd_ChildForeignKeyColumn22_Id_ComboItemList.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }
        
        #endregion TbChd_ChildForeignKeyColumn22_Id Combo List
        

        #region TbChd_ChildForeignKeyColumn33_Id Combo List

        private ComboItemList _TbChd_ChildForeignKeyColumn33_Id_ComboItemList = new ComboItemList(ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);

        public ComboItemList TbChdChildForeignKeyColumn33IdComboItemList
        {
            get { return _TbChd_ChildForeignKeyColumn33_Id_ComboItemList; }
            set { _TbChd_ChildForeignKeyColumn33_Id_ComboItemList = value; }
        }

        void TbChd_ChildForeignKeyColumn33_Id_ComboItemList_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn33_Id_DataSource()", IfxTraceCategory.Enter);
                if (TbChd_Parent_Tb_Id != null)
                {
                    _WcTableProxy.Begin_TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList((Guid)TbChd_Parent_Tb_Id);
                }
                else
                {
                    _TbChd_ChildForeignKeyColumn33_Id_ComboItemList.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn33_Id_DataSource()", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn33_Id_DataSource()", IfxTraceCategory.Leave);
            }
        }

        void TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted(object sender, TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                if (data != null)
                {
                    object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                    _TbChd_ChildForeignKeyColumn33_Id_ComboItemList.ReplaceList(array);
                }
                else
                {
                    _TbChd_ChildForeignKeyColumn33_Id_ComboItemList.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TbChd_ChildForeignKeyColumn33_Id Combo List

        void RaiseEventComboListRefreshCompleted(string key)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventComboListRefreshCompleted", IfxTraceCategory.Enter);
            ComboListRefreshCompletedEventHandler handler = ComboListRefreshCompleted;
            if (handler != null)
            {
                handler(this, new ComboListRefreshCompletedArgs(key));
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventComboListRefreshCompleted", IfxTraceCategory.Leave);
        }

        #endregion Load On Demand Combo Lists


        #endregion Custom Code


        #region ITraceItem Members

        void GetTraceItemsShortListCustom()
        {
            //  Add custom trace items here.
            //_traceItems.Add("xxxxxxxxx", xxxxxxxxx, TraceDataTypes.String);
            //_traceItems.Add("SomeChildObject", new SomeChildObject(), TraceDataTypes.ITraceItem);
        }

        void GetTraceItemsLongListCustom()
        {
            //  Add custom trace items here.
            //_traceItems.Add("xxxxxxxxx", xxxxxxxxx.ToString, TraceDataTypes.String);
            //_traceItems.Add("SomeChildObject", new SomeChildObject(), TraceDataTypes.ITraceItem);
        }

        #endregion ITraceItem Members

    }

}


