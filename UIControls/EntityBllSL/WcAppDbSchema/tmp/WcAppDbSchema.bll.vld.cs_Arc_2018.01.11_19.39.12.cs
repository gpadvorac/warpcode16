using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/11/2018 7:38:29 PM

namespace EntityBll.SL
{
    public partial class WcAppDbSchema_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_ApDbScm_Name = 50;
		public const int STRINGSIZE_ApDbScm_Desc = 200;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_ApDbScm_Id_Required = "Id is a required field.";
		private const string BROKENRULE_ApDbScm_Ap_Id_Required = "Ap_Id is a required field.";
		private string BROKENRULE_ApDbScm_Name_TextLength = "Name has a maximum text length of  '" + STRINGSIZE_ApDbScm_Name + "'.";
		private const string BROKENRULE_ApDbScm_Name_Required = "Name is a required field.";
		private string BROKENRULE_ApDbScm_Desc_TextLength = "Description has a maximum text length of  '" + STRINGSIZE_ApDbScm_Desc + "'.";
		private const string BROKENRULE_ApDbScm_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_ApDbScm_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_ApDbScm_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_Id", BROKENRULE_ApDbScm_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_Ap_Id", BROKENRULE_ApDbScm_Ap_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_Name", BROKENRULE_ApDbScm_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_Name", BROKENRULE_ApDbScm_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_Desc", BROKENRULE_ApDbScm_Desc_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_IsActiveRow", BROKENRULE_ApDbScm_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_IsDeleted", BROKENRULE_ApDbScm_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_Stamp", BROKENRULE_ApDbScm_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void ApDbScm_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_Id");
                string newBrokenRules = "";
                
                if (ApDbScm_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_Id", BROKENRULE_ApDbScm_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApDbScm_Id", BROKENRULE_ApDbScm_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApDbScm_Id", _brokenRuleManager.IsPropertyValid("ApDbScm_Id"), IsPropertyDirty("ApDbScm_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApDbScm_Ap_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Ap_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_Ap_Id");
                string newBrokenRules = "";
                
                if (ApDbScm_Ap_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_Ap_Id", BROKENRULE_ApDbScm_Ap_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApDbScm_Ap_Id", BROKENRULE_ApDbScm_Ap_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_Ap_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApDbScm_Ap_Id", _brokenRuleManager.IsPropertyValid("ApDbScm_Ap_Id"), IsPropertyDirty("ApDbScm_Ap_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Ap_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Ap_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApDbScm_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (ApDbScm_Name != null)
                {
                    len = ApDbScm_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_Name", BROKENRULE_ApDbScm_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApDbScm_Name", BROKENRULE_ApDbScm_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApDbScm_Name", BROKENRULE_ApDbScm_Name_Required);
                    if (len > STRINGSIZE_ApDbScm_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_Name", BROKENRULE_ApDbScm_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApDbScm_Name", BROKENRULE_ApDbScm_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApDbScm_Name", _brokenRuleManager.IsPropertyValid("ApDbScm_Name"), IsPropertyDirty("ApDbScm_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApDbScm_Desc_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Desc_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_Desc");
                string newBrokenRules = "";
                				int len = 0;
                if (ApDbScm_Desc != null)
                {
                    len = ApDbScm_Desc.Length;
                }

                if (len > STRINGSIZE_ApDbScm_Desc)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_Desc", BROKENRULE_ApDbScm_Desc_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApDbScm_Desc", BROKENRULE_ApDbScm_Desc_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_Desc");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApDbScm_Desc", _brokenRuleManager.IsPropertyValid("ApDbScm_Desc"), IsPropertyDirty("ApDbScm_Desc"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Desc_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Desc_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApDbScm_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_IsActiveRow");
                string newBrokenRules = "";
                
                if (ApDbScm_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_IsActiveRow", BROKENRULE_ApDbScm_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApDbScm_IsActiveRow", BROKENRULE_ApDbScm_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApDbScm_IsActiveRow", _brokenRuleManager.IsPropertyValid("ApDbScm_IsActiveRow"), IsPropertyDirty("ApDbScm_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApDbScm_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_IsDeleted");
                string newBrokenRules = "";
                
                if (ApDbScm_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_IsDeleted", BROKENRULE_ApDbScm_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApDbScm_IsDeleted", BROKENRULE_ApDbScm_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApDbScm_IsDeleted", _brokenRuleManager.IsPropertyValid("ApDbScm_IsDeleted"), IsPropertyDirty("ApDbScm_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApDbScm_CreatedUserId_Validate()
        {
        }

        private void ApDbScm_CreatedDate_Validate()
        {
        }

        private void ApDbScm_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApDbScm_LastModifiedDate_Validate()
        {
        }

        private void ApDbScm_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_Stamp");
                string newBrokenRules = "";
                
                if (ApDbScm_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApDbScm_Stamp", BROKENRULE_ApDbScm_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApDbScm_Stamp", BROKENRULE_ApDbScm_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApDbScm_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApDbScm_Stamp", _brokenRuleManager.IsPropertyValid("ApDbScm_Stamp"), IsPropertyDirty("ApDbScm_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApDbScm_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


