using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/26/2017 1:16:12 PM

namespace EntityBll.SL
{
    public partial class WcStoredProcParam_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_SpP_Name = 50;
		public const int STRINGSIZE_SpP_DefaultValue = 100;
		public const int STRINGSIZE_SpP_Value = 300;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_SpP_ID_Required = "ID is a required field.";
		private const string BROKENRULE_SpP_Sp_ID_Required = "Sp_ID is a required field.";
		private const string BROKENRULE_SpP_SortOrder_ZeroNotAllowed = "Sort Order:  '0'  (zero) is not allowed.";
		private string BROKENRULE_SpP_Name_TextLength = "Name has a maximum text length of  '" + STRINGSIZE_SpP_Name + "'.";
		private const string BROKENRULE_SpP_Name_Required = "Name is a required field.";
		private const string BROKENRULE_SpP_SpPDr_Id_ZeroNotAllowed = "Direction:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_SpP_DtNt_ID_ZeroNotAllowed = "DotNet Type:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_SpP_DtSq_ID_ZeroNotAllowed = "SQL Type:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_SpP_IsNullable_Required = "Nullable is a required field.";
		private string BROKENRULE_SpP_DefaultValue_TextLength = "Default Value has a maximum text length of  '" + STRINGSIZE_SpP_DefaultValue + "'.";
		private string BROKENRULE_SpP_Value_TextLength = "Value has a maximum text length of  '" + STRINGSIZE_SpP_Value + "'.";
		private const string BROKENRULE_SpP_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_SpP_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_SpP_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("SpP_ID", BROKENRULE_SpP_ID_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpP_Sp_ID", BROKENRULE_SpP_Sp_ID_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpP_SortOrder", BROKENRULE_SpP_SortOrder_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpP_Name", BROKENRULE_SpP_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("SpP_Name", BROKENRULE_SpP_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpP_SpPDr_Id", BROKENRULE_SpP_SpPDr_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpP_DtNt_ID", BROKENRULE_SpP_DtNt_ID_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpP_DtSq_ID", BROKENRULE_SpP_DtSq_ID_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpP_IsNullable", BROKENRULE_SpP_IsNullable_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpP_DefaultValue", BROKENRULE_SpP_DefaultValue_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpP_Value", BROKENRULE_SpP_Value_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpP_IsActiveRow", BROKENRULE_SpP_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpP_IsDeleted", BROKENRULE_SpP_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpP_Stamp", BROKENRULE_SpP_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void SpP_ID_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_ID_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_ID");
                string newBrokenRules = "";
                
                if (SpP_ID == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpP_ID", BROKENRULE_SpP_ID_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_ID", BROKENRULE_SpP_ID_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_ID");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpP_ID", _brokenRuleManager.IsPropertyValid("SpP_ID"), IsPropertyDirty("SpP_ID"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_ID_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_ID_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpP_Sp_ID_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_Sp_ID_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_Sp_ID");
                string newBrokenRules = "";
                
                if (SpP_Sp_ID == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpP_Sp_ID", BROKENRULE_SpP_Sp_ID_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_Sp_ID", BROKENRULE_SpP_Sp_ID_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_Sp_ID");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpP_Sp_ID", _brokenRuleManager.IsPropertyValid("SpP_Sp_ID"), IsPropertyDirty("SpP_Sp_ID"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_Sp_ID_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_Sp_ID_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpP_SortOrder_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_SortOrder_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_SortOrder");
                string newBrokenRules = "";
                
                if (SpP_SortOrder == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpP_SortOrder", BROKENRULE_SpP_SortOrder_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_SortOrder", BROKENRULE_SpP_SortOrder_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_SortOrder");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpP_SortOrder", _brokenRuleManager.IsPropertyValid("SpP_SortOrder"), IsPropertyDirty("SpP_SortOrder"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_SortOrder_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_SortOrder_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpP_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (SpP_Name != null)
                {
                    len = SpP_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpP_Name", BROKENRULE_SpP_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_Name", BROKENRULE_SpP_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_Name", BROKENRULE_SpP_Name_Required);
                    if (len > STRINGSIZE_SpP_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("SpP_Name", BROKENRULE_SpP_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("SpP_Name", BROKENRULE_SpP_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpP_Name", _brokenRuleManager.IsPropertyValid("SpP_Name"), IsPropertyDirty("SpP_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpP_SpPDr_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_SpPDr_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_SpPDr_Id");
                string newBrokenRules = "";
                
                if (SpP_SpPDr_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpP_SpPDr_Id", BROKENRULE_SpP_SpPDr_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_SpPDr_Id", BROKENRULE_SpP_SpPDr_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_SpPDr_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpP_SpPDr_Id", _brokenRuleManager.IsPropertyValid("SpP_SpPDr_Id"), IsPropertyDirty("SpP_SpPDr_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_SpPDr_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_SpPDr_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpP_SpPDr_Id_TextField_Validate()
        {
        }

        private void SpP_DtNt_ID_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_DtNt_ID_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_DtNt_ID");
                string newBrokenRules = "";
                
                if (SpP_DtNt_ID == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpP_DtNt_ID", BROKENRULE_SpP_DtNt_ID_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_DtNt_ID", BROKENRULE_SpP_DtNt_ID_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_DtNt_ID");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpP_DtNt_ID", _brokenRuleManager.IsPropertyValid("SpP_DtNt_ID"), IsPropertyDirty("SpP_DtNt_ID"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_DtNt_ID_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_DtNt_ID_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpP_DtNt_ID_TextField_Validate()
        {
        }

        private void SpP_DtSq_ID_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_DtSq_ID_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_DtSq_ID");
                string newBrokenRules = "";
                
                if (SpP_DtSq_ID == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpP_DtSq_ID", BROKENRULE_SpP_DtSq_ID_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_DtSq_ID", BROKENRULE_SpP_DtSq_ID_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_DtSq_ID");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpP_DtSq_ID", _brokenRuleManager.IsPropertyValid("SpP_DtSq_ID"), IsPropertyDirty("SpP_DtSq_ID"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_DtSq_ID_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_DtSq_ID_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpP_DtSq_ID_TextField_Validate()
        {
        }

        private void SpP_IsNullable_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_IsNullable_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_IsNullable");
                string newBrokenRules = "";
                
                if (SpP_IsNullable == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpP_IsNullable", BROKENRULE_SpP_IsNullable_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_IsNullable", BROKENRULE_SpP_IsNullable_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_IsNullable");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpP_IsNullable", _brokenRuleManager.IsPropertyValid("SpP_IsNullable"), IsPropertyDirty("SpP_IsNullable"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_IsNullable_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_IsNullable_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpP_DefaultValue_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_DefaultValue_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_DefaultValue");
                string newBrokenRules = "";
                				int len = 0;
                if (SpP_DefaultValue != null)
                {
                    len = SpP_DefaultValue.Length;
                }

                if (len > STRINGSIZE_SpP_DefaultValue)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpP_DefaultValue", BROKENRULE_SpP_DefaultValue_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_DefaultValue", BROKENRULE_SpP_DefaultValue_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_DefaultValue");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpP_DefaultValue", _brokenRuleManager.IsPropertyValid("SpP_DefaultValue"), IsPropertyDirty("SpP_DefaultValue"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_DefaultValue_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_DefaultValue_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpP_Value_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_Value_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_Value");
                string newBrokenRules = "";
                				int len = 0;
                if (SpP_Value != null)
                {
                    len = SpP_Value.Length;
                }

                if (len > STRINGSIZE_SpP_Value)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpP_Value", BROKENRULE_SpP_Value_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_Value", BROKENRULE_SpP_Value_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_Value");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpP_Value", _brokenRuleManager.IsPropertyValid("SpP_Value"), IsPropertyDirty("SpP_Value"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_Value_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_Value_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpP_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_IsActiveRow");
                string newBrokenRules = "";
                
                if (SpP_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpP_IsActiveRow", BROKENRULE_SpP_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_IsActiveRow", BROKENRULE_SpP_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpP_IsActiveRow", _brokenRuleManager.IsPropertyValid("SpP_IsActiveRow"), IsPropertyDirty("SpP_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpP_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_IsDeleted");
                string newBrokenRules = "";
                
                if (SpP_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpP_IsDeleted", BROKENRULE_SpP_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_IsDeleted", BROKENRULE_SpP_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpP_IsDeleted", _brokenRuleManager.IsPropertyValid("SpP_IsDeleted"), IsPropertyDirty("SpP_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpP_CreatedUserId_Validate()
        {
        }

        private void SpP_CreatedDate_Validate()
        {
        }

        private void SpP_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpP_LastModifiedDate_Validate()
        {
        }

        private void SpP_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_Stamp");
                string newBrokenRules = "";
                
                if (SpP_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpP_Stamp", BROKENRULE_SpP_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpP_Stamp", BROKENRULE_SpP_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpP_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpP_Stamp", _brokenRuleManager.IsPropertyValid("SpP_Stamp"), IsPropertyDirty("SpP_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpP_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


