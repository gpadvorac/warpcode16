using System;
using TypeServices;
using System.ComponentModel;
using Ifx.SL;
using Velocity.SL;

// Gen Timestamp:  12/14/2017 10:20:14 PM

namespace EntityBll.SL
{


    public partial class WcStoredProcGroup_Bll //: BusinessObjectBase, ITraceItem, IBusinessObject, IBusinessObjectV2, INotifyPropertyChanged , IEditableObject, IDataErrorInfo
    {




        #region Initialize Custom Variables



        #endregion Initialize Custom Variables


        #region General Methods


        void InitializeClass_Cust()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", IfxTraceCategory.Enter);
//      
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", IfxTraceCategory.Leave);
//            }
        }

        void CustomConstructor()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructor", IfxTraceCategory.Enter);
//      
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructor", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructor", IfxTraceCategory.Leave);
//            }
        }


        public void SetStandingFK(string parentType, Guid fk)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Enter);
                _parentEntityType = parentType;
                _StandingFK = fk;
                SetStandingFK();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Leave);
            }
        }

        public void SetStandingFK()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Enter);
//                //switch (_parentEntityType)
//                //{
//                //    case ParentType.Company:
//                //        _data.C._b = _StandingFK;
//                //        break;
//                //    case ParentType.PersonContact:
//                //        _data.C._c = _StandingFK;
//                //        break;
//                //}      
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Leave);
//            }
        }

        // Alternate FKs
        public void SetOtherParentKeys(string parentType, Guid id)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", IfxTraceCategory.Enter);
//                //_parentEntityType = parentType;
//                //switch (_parentEntityType)
//                //{
//                //    case "ucWellDt":
//                //        _data.C.WlD_Id_noevents = id;
//                //        break;
//                //    case "ucContractDt":
//                //        _data.C.CtD_Id_noevents = id;
//                //        break;
//                //}
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", IfxTraceCategory.Leave);
//            }
        }



        #endregion General Methods


        #region Data Access Methods


        #endregion Data Access Methods


        #region Method Extentions For Custom Code



        private void SetCustomDefaultBrokenRules()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", IfxTraceCategory.Enter);
//
//         
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", IfxTraceCategory.Leave);
//            }
        }




        private void CustomPropertySetterCode(string propName)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", IfxTraceCategory.Enter);
//                switch(propName)
//                {
//                    case "xxxx":
//
//                        break;
//
//                }
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", IfxTraceCategory.Leave);
//            }
        }

        private void UnDoCustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", IfxTraceCategory.Enter);
//   
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", IfxTraceCategory.Leave);
//            }
        }

        private void RefreshFieldsCustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", IfxTraceCategory.Enter);
//   
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", IfxTraceCategory.Leave);
//            }
        }

        private void NewEntityRowCustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", IfxTraceCategory.Enter);
//                //SetStandingFK();  //Unrem this when we have FKs
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", IfxTraceCategory.Leave);
//            }
        }

        private bool IsPropertyDirtyCustomCode(string propertyName)
        {
            ////  This is called from 'IsPropertyDirty' for calculated fields that are not represented in the wire objects.  They will always require custom code here.
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", IfxTraceCategory.Enter);
            //    switch (propertyName)
            //    {
            //        case "xxxx":
            //            //  return something
            //            break;
            //        default:
            //            throw new Exception(_as + "." + _cn + ".IsPropertyDirtyCustomCode:  Missing code and logic for " + propertyName + ".");
            //    }
                return false;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        public string GetPropertyValueByKeyCustomCode(string key)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", IfxTraceCategory.Enter);
            //    //switch (key)
            //    //{
            //    //    case "xxxx":
            //    //        return null;
            //    //        return xxx;
            //    //}
                return null;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        public string GetPropertyFormattedStringValueByKeyCustomCode(string key)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", IfxTraceCategory.Enter);
            //    //switch (key)
            //    //{
            //    //    case "xxxx":
            //    //        return null;
            //    //        return xxx;
            //    //}
                return null;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        #endregion Method Extentions For Custom Code


        #region Code Gen Methods For Modification to be copied to here


        #endregion Code Gen Methods For Modification to be copied to here


        #region Code Gen Properties For Modification to be copied to here


        #endregion Code Gen Properties For Modification to be copied to here


        #region Code Gen Validation Logic For Modification to be copied to here


        #endregion Code Gen Validation Logic For Modification to be copied to here


        #region Properties

        public string DataRowName
        {
            //get { return v_LicAp_Name; }
            get { return null; }
        }

        #endregion Properties


        #region Validation Logic


        #endregion Validation Logic


        #region Custom Code




        #endregion Custom Code


        #region ITraceItem Members

        void GetTraceItemsShortListCustom()
        {
            //  Add custom trace items here.
            //_traceItems.Add("xxxxxxxxx", xxxxxxxxx, TraceDataTypes.String);
            //_traceItems.Add("SomeChildObject", new SomeChildObject(), TraceDataTypes.ITraceItem);
        }

        void GetTraceItemsLongListCustom()
        {
            //  Add custom trace items here.
            //_traceItems.Add("xxxxxxxxx", xxxxxxxxx.ToString, TraceDataTypes.String);
            //_traceItems.Add("SomeChildObject", new SomeChildObject(), TraceDataTypes.ITraceItem);
        }

        #endregion ITraceItem Members
    }

}


