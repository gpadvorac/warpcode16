using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/15/2017 12:04:55 AM

namespace EntityBll.SL
{
    public partial class WcStoredProcGroup_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_SpGrp_Name = 50;
		public const int STRINGSIZE_SpGrp_Desc = 200;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_SpGrp_Id_Required = "Id is a required field.";
		private const string BROKENRULE_SpGrp_ApVrsn_Id_Required = "ApVrsn_Id is a required field.";
		private const string BROKENRULE_SpGrp_SortOrder_ZeroNotAllowed = "Sort Order:  '0'  (zero) is not allowed.";
		private string BROKENRULE_SpGrp_Name_TextLength = "Stored Proc Group Name has a maximum text length of  '" + STRINGSIZE_SpGrp_Name + "'.";
		private string BROKENRULE_SpGrp_Desc_TextLength = "Description has a maximum text length of  '" + STRINGSIZE_SpGrp_Desc + "'.";
		private const string BROKENRULE_SpGrp_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_SpGrp_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_SpGrp_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("SpGrp_Id", BROKENRULE_SpGrp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpGrp_ApVrsn_Id", BROKENRULE_SpGrp_ApVrsn_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpGrp_SortOrder", BROKENRULE_SpGrp_SortOrder_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpGrp_Name", BROKENRULE_SpGrp_Name_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpGrp_Desc", BROKENRULE_SpGrp_Desc_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpGrp_IsActiveRow", BROKENRULE_SpGrp_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpGrp_IsDeleted", BROKENRULE_SpGrp_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpGrp_Stamp", BROKENRULE_SpGrp_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void SpGrp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_Id");
                string newBrokenRules = "";
                
                if (SpGrp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpGrp_Id", BROKENRULE_SpGrp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpGrp_Id", BROKENRULE_SpGrp_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpGrp_Id", _brokenRuleManager.IsPropertyValid("SpGrp_Id"), IsPropertyDirty("SpGrp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpGrp_ApVrsn_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_ApVrsn_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_ApVrsn_Id");
                string newBrokenRules = "";
                
                if (SpGrp_ApVrsn_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpGrp_ApVrsn_Id", BROKENRULE_SpGrp_ApVrsn_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpGrp_ApVrsn_Id", BROKENRULE_SpGrp_ApVrsn_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_ApVrsn_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpGrp_ApVrsn_Id", _brokenRuleManager.IsPropertyValid("SpGrp_ApVrsn_Id"), IsPropertyDirty("SpGrp_ApVrsn_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_ApVrsn_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_ApVrsn_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpGrp_SortOrder_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_SortOrder_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_SortOrder");
                string newBrokenRules = "";
                
                if (SpGrp_SortOrder == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpGrp_SortOrder", BROKENRULE_SpGrp_SortOrder_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpGrp_SortOrder", BROKENRULE_SpGrp_SortOrder_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_SortOrder");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpGrp_SortOrder", _brokenRuleManager.IsPropertyValid("SpGrp_SortOrder"), IsPropertyDirty("SpGrp_SortOrder"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_SortOrder_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_SortOrder_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpGrp_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (SpGrp_Name != null)
                {
                    len = SpGrp_Name.Length;
                }

                if (len > STRINGSIZE_SpGrp_Name)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpGrp_Name", BROKENRULE_SpGrp_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpGrp_Name", BROKENRULE_SpGrp_Name_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpGrp_Name", _brokenRuleManager.IsPropertyValid("SpGrp_Name"), IsPropertyDirty("SpGrp_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpGrp_Desc_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_Desc_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_Desc");
                string newBrokenRules = "";
                				int len = 0;
                if (SpGrp_Desc != null)
                {
                    len = SpGrp_Desc.Length;
                }

                if (len > STRINGSIZE_SpGrp_Desc)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpGrp_Desc", BROKENRULE_SpGrp_Desc_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpGrp_Desc", BROKENRULE_SpGrp_Desc_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_Desc");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpGrp_Desc", _brokenRuleManager.IsPropertyValid("SpGrp_Desc"), IsPropertyDirty("SpGrp_Desc"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_Desc_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_Desc_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpGrp_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_IsActiveRow");
                string newBrokenRules = "";
                
                if (SpGrp_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpGrp_IsActiveRow", BROKENRULE_SpGrp_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpGrp_IsActiveRow", BROKENRULE_SpGrp_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpGrp_IsActiveRow", _brokenRuleManager.IsPropertyValid("SpGrp_IsActiveRow"), IsPropertyDirty("SpGrp_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpGrp_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_IsDeleted");
                string newBrokenRules = "";
                
                if (SpGrp_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpGrp_IsDeleted", BROKENRULE_SpGrp_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpGrp_IsDeleted", BROKENRULE_SpGrp_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpGrp_IsDeleted", _brokenRuleManager.IsPropertyValid("SpGrp_IsDeleted"), IsPropertyDirty("SpGrp_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpGrp_CreatedUserId_Validate()
        {
        }

        private void SpGrp_CreatedDate_Validate()
        {
        }

        private void SpGrp_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpGrp_LastModifiedDate_Validate()
        {
        }

        private void SpGrp_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_Stamp");
                string newBrokenRules = "";
                
                if (SpGrp_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpGrp_Stamp", BROKENRULE_SpGrp_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpGrp_Stamp", BROKENRULE_SpGrp_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpGrp_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpGrp_Stamp", _brokenRuleManager.IsPropertyValid("SpGrp_Stamp"), IsPropertyDirty("SpGrp_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpGrp_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


