using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/1/2016 11:56:53 AM

namespace EntityBll.SL
{
    public partial class WcStoredProcParamValueGroup_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_SpPVGrp_Name = 50;
		public const int STRINGSIZE_SpPVGrp_Notes = 255;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_SpPVGrp_Id_Required = "Id is a required field.";
		private const string BROKENRULE_SpPVGrp_Sp_Id_Required = "Sp_Id is a required field.";
		private string BROKENRULE_SpPVGrp_Name_TextLength = "Name has a maximum text length of  '" + STRINGSIZE_SpPVGrp_Name + "'.";
		private const string BROKENRULE_SpPVGrp_Name_Required = "Name is a required field.";
		private string BROKENRULE_SpPVGrp_Notes_TextLength = "Notes has a maximum text length of  '" + STRINGSIZE_SpPVGrp_Notes + "'.";
		private const string BROKENRULE_SpPVGrp_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_SpPVGrp_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_SpPVGrp_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_Id", BROKENRULE_SpPVGrp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_Sp_Id", BROKENRULE_SpPVGrp_Sp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_Name", BROKENRULE_SpPVGrp_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_Name", BROKENRULE_SpPVGrp_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_Notes", BROKENRULE_SpPVGrp_Notes_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_IsActiveRow", BROKENRULE_SpPVGrp_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_IsDeleted", BROKENRULE_SpPVGrp_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_Stamp", BROKENRULE_SpPVGrp_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void SpPVGrp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_Id");
                string newBrokenRules = "";
                
                if (SpPVGrp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_Id", BROKENRULE_SpPVGrp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPVGrp_Id", BROKENRULE_SpPVGrp_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPVGrp_Id", _brokenRuleManager.IsPropertyValid("SpPVGrp_Id"), IsPropertyDirty("SpPVGrp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPVGrp_Sp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Sp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_Sp_Id");
                string newBrokenRules = "";
                
                if (SpPVGrp_Sp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_Sp_Id", BROKENRULE_SpPVGrp_Sp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPVGrp_Sp_Id", BROKENRULE_SpPVGrp_Sp_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_Sp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPVGrp_Sp_Id", _brokenRuleManager.IsPropertyValid("SpPVGrp_Sp_Id"), IsPropertyDirty("SpPVGrp_Sp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Sp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Sp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPVGrp_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (SpPVGrp_Name != null)
                {
                    len = SpPVGrp_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_Name", BROKENRULE_SpPVGrp_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPVGrp_Name", BROKENRULE_SpPVGrp_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPVGrp_Name", BROKENRULE_SpPVGrp_Name_Required);
                    if (len > STRINGSIZE_SpPVGrp_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_Name", BROKENRULE_SpPVGrp_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("SpPVGrp_Name", BROKENRULE_SpPVGrp_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPVGrp_Name", _brokenRuleManager.IsPropertyValid("SpPVGrp_Name"), IsPropertyDirty("SpPVGrp_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPVGrp_Notes_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Notes_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_Notes");
                string newBrokenRules = "";
                				int len = 0;
                if (SpPVGrp_Notes != null)
                {
                    len = SpPVGrp_Notes.Length;
                }

                if (len > STRINGSIZE_SpPVGrp_Notes)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_Notes", BROKENRULE_SpPVGrp_Notes_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPVGrp_Notes", BROKENRULE_SpPVGrp_Notes_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_Notes");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPVGrp_Notes", _brokenRuleManager.IsPropertyValid("SpPVGrp_Notes"), IsPropertyDirty("SpPVGrp_Notes"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Notes_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Notes_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPVGrp_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_IsActiveRow");
                string newBrokenRules = "";
                
                if (SpPVGrp_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_IsActiveRow", BROKENRULE_SpPVGrp_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPVGrp_IsActiveRow", BROKENRULE_SpPVGrp_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPVGrp_IsActiveRow", _brokenRuleManager.IsPropertyValid("SpPVGrp_IsActiveRow"), IsPropertyDirty("SpPVGrp_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPVGrp_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_IsDeleted");
                string newBrokenRules = "";
                
                if (SpPVGrp_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_IsDeleted", BROKENRULE_SpPVGrp_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPVGrp_IsDeleted", BROKENRULE_SpPVGrp_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPVGrp_IsDeleted", _brokenRuleManager.IsPropertyValid("SpPVGrp_IsDeleted"), IsPropertyDirty("SpPVGrp_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPVGrp_CreatedUserId_Validate()
        {
        }

        private void SpPVGrp_CreatedDate_Validate()
        {
        }

        private void SpPVGrp_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPVGrp_LastModifiedDate_Validate()
        {
        }

        private void SpPVGrp_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_Stamp");
                string newBrokenRules = "";
                
                if (SpPVGrp_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPVGrp_Stamp", BROKENRULE_SpPVGrp_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPVGrp_Stamp", BROKENRULE_SpPVGrp_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPVGrp_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPVGrp_Stamp", _brokenRuleManager.IsPropertyValid("SpPVGrp_Stamp"), IsPropertyDirty("SpPVGrp_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPVGrp_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


