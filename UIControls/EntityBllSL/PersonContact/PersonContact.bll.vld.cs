using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  5/7/2015 12:09:30 PM

namespace EntityBll.SL
{
    public partial class PersonContact_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_Pn_FName = 100;
		public const int STRINGSIZE_Pn_LName = 100;
		public const int STRINGSIZE_Pn_EMail = 256;
		public const int STRINGSIZE_Pn_PhoneCell = 20;
		public const int STRINGSIZE_Pn_PhoneOther = 20;
		public const int STRINGSIZE_UserName = 100;
		private const string BROKENRULE_Pn_Id_Required = "Id is a required field.";
		private string BROKENRULE_Pn_FName_TextLength = "First name has a maximum text length of  '" + STRINGSIZE_Pn_FName + "'.";
		private string BROKENRULE_Pn_LName_TextLength = "Last name has a maximum text length of  '" + STRINGSIZE_Pn_LName + "'.";
		private string BROKENRULE_Pn_EMail_TextLength = "EMail has a maximum text length of  '" + STRINGSIZE_Pn_EMail + "'.";
		private string BROKENRULE_Pn_PhoneCell_TextLength = "Cell has a maximum text length of  '" + STRINGSIZE_Pn_PhoneCell + "'.";
		private string BROKENRULE_Pn_PhoneOther_TextLength = "Phone other has a maximum text length of  '" + STRINGSIZE_Pn_PhoneOther + "'.";
		private string BROKENRULE_UserName_TextLength = "User Name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_Pn_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("Pn_Id", BROKENRULE_Pn_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Pn_FName", BROKENRULE_Pn_FName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Pn_LName", BROKENRULE_Pn_LName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Pn_EMail", BROKENRULE_Pn_EMail_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Pn_PhoneCell", BROKENRULE_Pn_PhoneCell_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Pn_PhoneOther", BROKENRULE_Pn_PhoneOther_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Pn_Stamp", BROKENRULE_Pn_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void Pn_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_Id");
                string newBrokenRules = "";
                
                if (Pn_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Pn_Id", BROKENRULE_Pn_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Pn_Id", BROKENRULE_Pn_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Pn_Id", _brokenRuleManager.IsPropertyValid("Pn_Id"), IsPropertyDirty("Pn_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Pn_FName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_FName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_FName");
                string newBrokenRules = "";
                				int len = 0;
                if (Pn_FName != null)
                {
                    len = Pn_FName.Length;
                }

                if (len > STRINGSIZE_Pn_FName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Pn_FName", BROKENRULE_Pn_FName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Pn_FName", BROKENRULE_Pn_FName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_FName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Pn_FName", _brokenRuleManager.IsPropertyValid("Pn_FName"), IsPropertyDirty("Pn_FName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_FName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_FName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Pn_LName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_LName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_LName");
                string newBrokenRules = "";
                				int len = 0;
                if (Pn_LName != null)
                {
                    len = Pn_LName.Length;
                }

                if (len > STRINGSIZE_Pn_LName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Pn_LName", BROKENRULE_Pn_LName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Pn_LName", BROKENRULE_Pn_LName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_LName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Pn_LName", _brokenRuleManager.IsPropertyValid("Pn_LName"), IsPropertyDirty("Pn_LName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_LName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_LName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Pn_EMail_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_EMail_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_EMail");
                string newBrokenRules = "";
                				int len = 0;
                if (Pn_EMail != null)
                {
                    len = Pn_EMail.Length;
                }

                if (len > STRINGSIZE_Pn_EMail)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Pn_EMail", BROKENRULE_Pn_EMail_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Pn_EMail", BROKENRULE_Pn_EMail_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_EMail");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Pn_EMail", _brokenRuleManager.IsPropertyValid("Pn_EMail"), IsPropertyDirty("Pn_EMail"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_EMail_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_EMail_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Pn_PhoneCell_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_PhoneCell_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_PhoneCell");
                string newBrokenRules = "";
                				int len = 0;
                if (Pn_PhoneCell != null)
                {
                    len = Pn_PhoneCell.Length;
                }

                if (len > STRINGSIZE_Pn_PhoneCell)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Pn_PhoneCell", BROKENRULE_Pn_PhoneCell_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Pn_PhoneCell", BROKENRULE_Pn_PhoneCell_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_PhoneCell");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Pn_PhoneCell", _brokenRuleManager.IsPropertyValid("Pn_PhoneCell"), IsPropertyDirty("Pn_PhoneCell"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_PhoneCell_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_PhoneCell_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Pn_PhoneOther_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_PhoneOther_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_PhoneOther");
                string newBrokenRules = "";
                				int len = 0;
                if (Pn_PhoneOther != null)
                {
                    len = Pn_PhoneOther.Length;
                }

                if (len > STRINGSIZE_Pn_PhoneOther)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Pn_PhoneOther", BROKENRULE_Pn_PhoneOther_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Pn_PhoneOther", BROKENRULE_Pn_PhoneOther_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_PhoneOther");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Pn_PhoneOther", _brokenRuleManager.IsPropertyValid("Pn_PhoneOther"), IsPropertyDirty("Pn_PhoneOther"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_PhoneOther_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_PhoneOther_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Pn_UserIdStamp_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Pn_CreatedDate_Validate()
        {
        }

        private void Pn_LastModifiedDate_Validate()
        {
        }

        private void Pn_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_Stamp");
                string newBrokenRules = "";
                
                if (Pn_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Pn_Stamp", BROKENRULE_Pn_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Pn_Stamp", BROKENRULE_Pn_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Pn_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Pn_Stamp", _brokenRuleManager.IsPropertyValid("Pn_Stamp"), IsPropertyDirty("Pn_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Pn_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


