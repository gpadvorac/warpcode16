using System;
using System.Collections.Generic;
using TypeServices;
using System.ComponentModel;
using System.Diagnostics;
using EntityWireTypeSL;
using Ifx.SL;
using Velocity.SL;
using ProxyWrapper;
using vComboDataTypes;

// Gen Timestamp:  1/5/2018 4:11:49 PM

namespace EntityBll.SL
{


    public partial class WcTableColumn_Bll : BusinessObjectBase, ITraceItem, IBusinessObject, IBusinessObjectV2, INotifyPropertyChanged , IEditableObject
    {


        #region Initialize Variables

        /// <summary>
        /// This is a private instance of TraceItemList which is normaly used in every entity
        /// business type and is used to provide rich information abou the current state and values
        /// in this business object when included in a trace call. To see exactly what data will be
        /// provided in a trace, look at the GetTraceItemsShortList method in the WcTableColumn_Bll code
        /// file.
        /// </summary>
        /// <seealso cref="!:file://D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Optimized_Deep_Tracing_of_Business_Object.html" cat="My Test Category">My Test Caption</seealso>
        TraceItemList _traceItems;
        /// <seealso cref="EntityWireType.WcTableColumn_Values">WcTableColumn_Values Class</seealso>
        /// <summary>
        ///     This is an instance of the wire type (<see cref="EntityWireType.WcTableColumn_Values">WcTableColumn_Values</see>) that’s wrapped by the Values
        ///     Manager (<see cref="EntityWireType.WcTableColumn_ValuesMngr">WcTableColumn_ValuesMngr</see>) and
        ///     used to Plug-n-Play into this business object.
        /// </summary>
        private WcTableColumn_ValuesMngr _data = null;

        /// <summary>See <see cref="OnCrudFailed">OnCrudFailed</see> for information.</summary>
        public event CrudFailedEventHandler CrudFailed;

        public event AsyncSaveCompleteEventHandler AsyncSaveComplete;

        public event AsyncSaveWithResponseCompleteEventHandler AsyncSaveWithResponseComplete;

        public event EntityRowReceivedEventHandler EntityRowReceived;


        /// <summary>
        ///     See <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> for
        ///     information.
        /// </summary>
        public event CurrentEntityStateEventHandler CurrentEntityStateChanged;
        /// <summary>See <see cref="OnBrokenRuleChanged">OnBrokenRuleChanged</see> for information.</summary>
        public event BrokenRuleEventHandler BrokenRuleChanged;
        /// <summary>
        /// 	<para>
        ///         Raised by the This event is currently <see cref="OnPropertyValueChanged">OnPropertyValueChanged</see> method, its currently not
        ///         being used and may be obsolete.
        ///     </para>
        /// </summary>
        public event PropertyValueChangedEventHandler PropertyValueChanged;
        /// <summary>This event is currently not being used and may be obsolete.</summary>
        public event BusinessObjectUpdatedEventHandler BusinessObjectUpdated;
        /// <summary>
        /// 	<para>
        ///         Required by the INotifyPropertyChanged interface, this event is raise by the
        ///         <see cref="Notify">Notify</see> method (also part of the INotifyPropertyChanged
        ///         interface. The Notify method is called in nearly all public data field
        ///         properties. The INotifyPropertyChanged interface is implemented to make
        ///         WcTableColumn_Bll more extendable.
        ///     </para>
        /// 	<para>The <strong>INotifyPropertyChanged</strong> interface is used to notify
        ///     clients, typically binding clients, that a property value has changed. For more
        ///     information, search INotifyPropertyChanged on <strong>MSDN</strong>.</para>
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 	<para>
        ///         Private field for the <see cref="ActiveRestrictedStringProperty">ActiveRestrictedStringProperty</see> property.
        ///         See it’s documentation for details.
        ///     </para>
        /// 	<para></para>
        /// </summary>
        private string _activeRestrictedStringProperty;
        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcTableColumn_Bll";
        /// <summary>
        /// 	<para>
        ///         Used in the validation section of this class in the WcTableColumn.bll.vld.cs code file,
        ///         this event notifies event handlers that a data field’s valid state has changed
        ///         (valid or not valid).<br/>
        ///         For more information about this event, see <see cref="TypeServices.ControlValidStateChangedEventHandler">CurrentEntityStateEventHandler</see>.<br/>
        ///         For more information on how it’s used, see these methods:
        ///     </para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="NewEntityRow">NewEntityRow</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="GetEntityRow">GetEntityRow</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="Save">Save</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableColumnProps.RaiseCurrentEntityStateChanged">ucWcTableColumnProps.RaiseCurrentEntityStateChanged()</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableColumnProps.RaiseCurrentEntityStateChanged">ucWcTableColumnProps.RaiseCurrentEntityStateChanged(EntityStateSwitch
        ///             state)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public event ControlValidStateChangedEventHandler ControlValidStateChanged;
        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableColumnProps.OnRestrictedTextLengthChanged">ucWcTableColumnProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableColumnProps.SetRestrictedStringLengthText">ucWcTableColumnProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public event RestrictedTextLengthChangedEventHandler RestrictedTextLengthChanged;

        UserSecurityContext _context = null;

        private object _parentEditObject = null;
        private ParentEditObjectType _parentType = ParentEditObjectType.None;

        private WcTableColumnService_ProxyWrapper _wcTableColumnProxy = null;



        Guid _StandingFK = Guid.Empty;
        string _parentEntityType = "";

        private WcToolTipService_ProxyWrapper _proxyWcToolTipService;
            
        private WcTableColumnComboColumnService_ProxyWrapper _proxyWcTableColumnComboColumnService;
            
        #endregion Initialize Variables

            
        #region Initialize Class

        public WcTableColumn_Bll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Bll()", IfxTraceCategory.Enter);
                _context = UserSecurityContext.GetCurrent();
                InitializeClass();
//                WcTableColumnService_ProxyWrapper _wcTableColumnProxy = new  WcTableColumnService_ProxyWrapper();
//                _wcTableColumnProxy.WcTableColumn_SaveCompleted += new EventHandler<WcTableColumn_SaveCompletedEventArgs>(SaveCompleted);
                _proxyWcToolTipService = new WcToolTipService_ProxyWrapper();
                _proxyWcToolTipService.WcToolTip_GetListByFKCompleted += _proxyWcToolTipService_WcToolTip_GetListByFKCompleted;
                _children_WcToolTip.Add(new WcToolTip_Bll());
            
                _proxyWcTableColumnComboColumnService = new WcTableColumnComboColumnService_ProxyWrapper();
                _proxyWcTableColumnComboColumnService.WcTableColumnComboColumn_GetListByFKCompleted += _proxyWcTableColumnComboColumnService_WcTableColumnComboColumn_GetListByFKCompleted;
                _children_WcTableColumnComboColumn.Add(new WcTableColumnComboColumn_Bll());
            
                SetNewEntityRow();

                CustomConstructor();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Bll()", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Bll()", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>
        ///         Passes in the <see cref="EntityWireType.WcTableColumn_ValuesMngr">WcTableColumn_ValuesMngr</see> type and <see cref="TypeServices.EntityState">EntityState</see>. The WcTableColumn_ValuesMngr type is the
        ///         data object used in a ‘Plug-n-Play’ fashion for loading data into WcTableColumn_Bll
        ///         fast and efficiently. EntityState sets WcTableColumn_Bll’s state.
        ///     </para>
        /// 	<para>Used by the following methods call this constructor:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="FromWire">FromWire</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcTableColumn_List.Fill">Fill</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcTableColumn_List.ReplaceList">ReplaceList(WcTableColumn_ValuesMngr[]
        ///             list)</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcTableColumn_List.ReplaceList">ReplaceList(IEntity_ValuesMngr[]
        ///             list)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public WcTableColumn_Bll(WcTableColumn_ValuesMngr valueObject, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Bll(WcTableColumn_ValuesMngr valueObject, EntityState state)", IfxTraceCategory.Enter);
                _context = UserSecurityContext.GetCurrent(); 
                _data = valueObject;
                InitializeClass(state);
               // _data.SetClassStateAfterFetch(state);
//                 WcTableColumnService_ProxyWrapper _wcTableColumnProxy = new  WcTableColumnService_ProxyWrapper();
//                _wcTableColumnProxy.WcTableColumn_SaveCompleted += new EventHandler<WcTableColumn_SaveCompletedEventArgs>(SaveCompleted);
                _proxyWcToolTipService = new WcToolTipService_ProxyWrapper();
                _proxyWcToolTipService.WcToolTip_GetListByFKCompleted += _proxyWcToolTipService_WcToolTip_GetListByFKCompleted;
                _children_WcToolTip.Add(new WcToolTip_Bll());
            
                _proxyWcTableColumnComboColumnService = new WcTableColumnComboColumnService_ProxyWrapper();
                _proxyWcTableColumnComboColumnService.WcTableColumnComboColumn_GetListByFKCompleted += _proxyWcTableColumnComboColumnService_WcTableColumnComboColumn_GetListByFKCompleted;
                _children_WcTableColumnComboColumn.Add(new WcTableColumnComboColumn_Bll());
            
                CustomConstructor();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Bll(WcTableColumn_ValuesMngr valueObject, EntityState state)", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Bll(WcTableColumn_ValuesMngr valueObject, EntityState state)", IfxTraceCategory.Leave);
            }
        }


        private void InitializeClass()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Enter);
                InitializeClass(new EntityState(true, true, false));
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Leave);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Leave);
            }
        }



        /// <summary>Called from the constructor, events are wired here.</summary>
        private void InitializeClass(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass(EntityState state)", IfxTraceCategory.Enter);
                if (_data == null)
                {
                    _data = GetDefault();
                    //**  This is a hack.  We should set the state elsewhere, however, when we get this from accross the wire,
                    //      we loose the state and therefore are forced to always call SetClassStateAfterFetch.
                   // _data.SetClassStateAfterFetch(state);
                }
                _data.SetClassStateAfterFetch(state);
                BrokenRuleManagerProperty = new BrokenRuleManager(this);
                _brokenRuleManager.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                //TODO: need to code it for a dynamic creation for an insert
                //TODO: need to move save, assignwithid, update, delete, deactivate, persist (generic, check the state) into this class
                //TODO: need to move the various lists of apps to get into an applicationprovider class for the client side that thunks to the proxywrapper
                //TODO: need to move field level validation into the setters and call the object validation on any persist operation
                //TODO:need to implement the two eh and tracing interfaces on the object
                InitializeClass_Cust();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityState", IfxTraceCategory.Leave);
            }
        }

        private  WcTableColumnService_ProxyWrapper WcTableColumnProxy
        {
            get
            {
                if (_wcTableColumnProxy == null)
                {
                    _wcTableColumnProxy = new  WcTableColumnService_ProxyWrapper();
                    _wcTableColumnProxy.WcTableColumn_SaveCompleted += new EventHandler<WcTableColumn_SaveCompletedEventArgs>(SaveCompleted);
                    _wcTableColumnProxy.WcTableColumn_GetByIdCompleted += new EventHandler<WcTableColumn_GetByIdCompletedEventArgs>(GetEntityRowResponse);

                }

                return _wcTableColumnProxy;
            }
        }

        #endregion  Initialize Class


		#region CRUD Methods



        /// <returns><see cref="EntityWireType.WcTableColumn_ValuesMngr">WcTableColumn_ValuesMngr</see></returns>
        /// <summary>
        ///     When this business type is set to a new state, a new data type also known as a wire
        ///     type (<see cref="EntityWireType.WcTableColumn_Values">WcTableColumn_Values</see>) is created, new
        ///     Id value is created, default values are set and appropriate state setting are made.
        ///     WcTableColumn_Values is wrapped in the <see cref="EntityWireType.WcTableColumn_ValuesMngr">WcTableColumn_ValuesMngr</see> type and returned.
        /// </summary>
        private WcTableColumn_ValuesMngr GetDefault()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                Guid Id = GetNewID();
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Enter);
                return new WcTableColumn_ValuesMngr(new object[] 
                    {
                        Id,    //  TbC_Id
						null,		//  TbC_Tb_Id
						null,		//  TbC_auditColumn_Id
						null,		//  TbC_SortOrder
						0,		//  DbSortOrder
						false,		//  IsImported
						null,		//  TbC_Name
						null,		//  ColumnInDatabase
						null,		//  TbC_CtlTp_Id
						null,		//  TbC_CtlTp_Id_TextField
						false,		//  TbC_IsNonvColumn
						null,		//  TbC_Description
						null,		//  TbC_DtSql_Id
						null,		//  TbC_DtSql_Id_TextField
						null,		//  TbC_DtDtNt_Id
						null,		//  TbC_DtDtNt_Id_TextField
						null,		//  TbC_Length
						null,		//  TbC_Precision
						null,		//  TbC_Scale
						false,		//  TbC_IsPK
						false,		//  TbC_IsIdentity
						false,		//  TbC_IsFK
						true,		//  TbC_IsEntityColumn
						true,		//  TbC_IsSystemField
						true,		//  TbC_IsValuesObjectMember
						true,		//  TbC_IsInPropsScreen
						true,		//  TbC_IsInNavList
						false,		//  TbC_IsRequired
						null,		//  TbC_BrokenRuleText
						false,		//  TbC_AllowZero
						false,		//  TbC_IsNullableInDb
						false,		//  TbC_IsNullableInUI
						null,		//  TbC_DefaultValue
						true,		//  TbC_IsReadFromDb
						true,		//  TbC_IsSendToDb
						true,		//  TbC_IsInsertAllowed
						true,		//  TbC_IsEditAllowed
						false,		//  TbC_IsReadOnlyInUI
						false,		//  TbC_UseForAudit
						null,		//  TbC_DefaultCaption
						null,		//  TbC_ColumnHeaderText
						null,		//  TbC_LabelCaptionVerbose
						true,		//  TbC_LabelCaptionGenerate
						false,		//  Tbc_ShowGridColumnToolTip
						false,		//  Tbc_ShowPropsToolTip
						null,		//  Tbc_TooltipsRolledUp
						true,		//  TbC_IsCreatePropsStrings
						true,		//  TbC_IsCreateGridStrings
						true,		//  TbC_IsAvailableForColumnGroups
						false,		//  TbC_IsTextWrapInProp
						false,		//  TbC_IsTextWrapInGrid
						null,		//  TbC_TextBoxFormat
						null,		//  TbC_TextColumnFormat
						(double)-1,		//  TbC_ColumnWidth
						null,		//  TbC_TextBoxTextAlignment
						null,		//  TbC_ColumnTextAlignment
						null,		//  TbC_ListStoredProc_Id
						null,		//  TbC_ListStoredProc_Id_TextField
						false,		//  TbC_IsStaticList
						false,		//  TbC_UseNotInList
						false,		//  TbC_UseListEditBtn
						null,		//  TbC_ParentColumnKey
						null,		//  TbC_ParentColumnKey_TextField
						false,		//  TbC_UseDisplayTextFieldProperty
						false,		//  TbC_IsDisplayTextFieldProperty
						null,		//  TbC_ComboListTable_Id
						null,		//  TbC_ComboListTable_Id_TextField
						null,		//  TbC_ComboListDisplayColumn_Id
						null,		//  TbC_ComboListDisplayColumn_Id_TextField
						(double)0,		//  TbC_Combo_MaxDropdownHeight
						(double)0,		//  TbC_Combo_MaxDropdownWidth
						false,		//  TbC_Combo_AllowDropdownResizing
						false,		//  TbC_Combo_IsResetButtonVisible
						false,		//  TbC_IsActiveRecColumn
						false,		//  TbC_IsDeletedColumn
						false,		//  TbC_IsCreatedUserIdColumn
						false,		//  TbC_IsCreatedDateColumn
						false,		//  TbC_IsUserIdColumn
						false,		//  TbC_IsModifiedDateColumn
						false,		//  TbC_IsRowVersionStampColumn
						true,		//  TbC_IsBrowsable
						null,		//  TbC_DeveloperNote
						null,		//  TbC_UserNote
						null,		//  TbC_HelpFileAdditionalNote
						null,		//  TbC_Notes
						false,		//  TbC_IsInputComplete
						true,		//  TbC_IsCodeGen
						null,		//  TbC_IsReadyCodeGen
						null,		//  TbC_IsCodeGenComplete
						null,		//  TbC_IsTagForCodeGen
						null,		//  TbC_IsTagForOther
						null,		//  TbC_ColumnGroups
						true,		//  TbC_IsActiveRow
						false,		//  TbC_IsDeleted
						null,		//  TbC_CreatedUserId
						null,		//  TbC_CreatedDate
						null,		//  TbC_UserId
						null,		//  UserName
						null,		//  TbC_LastModifiedDate
						null,		//  TbC_Stamp
                    }, new EntityState(true, true, false)
                );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// Called by any parent object that wants to clear out the existing data and state,
        /// and replace all with new state and data object.
        /// </summary>
        public void NewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Enter);
                _data = GetDefault();
                NewEntityRowCustomCode();
                SetDefaultBrokenRules();
                //RaiseEventCurrentEntityStateChanged(EntityStateSwitch.NewInvalidNotDirty);
                RaiseEventCurrentEntityStateChanged(_data.S.Switch);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// This is called from the parameterless constructor.  It's assumed that a data object will not be passed (which would have
        /// cause the EntityState to be configured properly) and therefore this new entity must be properly configured.
        /// </summary>
        public void SetNewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", IfxTraceCategory.Enter);
                NewEntityRowCustomCode();
                SetDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", IfxTraceCategory.Leave);
            }
        }


        public void GetEntityRow(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", new ValuePair[] {new ValuePair("Id", Id) }, IfxTraceCategory.Enter);

                WcTableColumnProxy.Begin_WcTableColumn_GetById(Id);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", IfxTraceCategory.Leave);
            }
        }

        public void GetEntityRow(long Id)
        {
            throw new NotImplementedException("WcTableColumn_Bll GetEntityRow(long Id) Not Implemented");
        }

        public void GetEntityRow(int Id)
        {
            throw new NotImplementedException("WcTableColumn_Bll GetEntityRow(int Id) Not Implemented");
        }

        public void GetEntityRow(short Id)
        {
            throw new NotImplementedException("WcTableColumn_Bll GetEntityRow(short Id) Not Implemented");
        }

        public void GetEntityRow(byte Id)
        {
            throw new NotImplementedException("WcTableColumn_Bll GetEntityRow(byte Id) Not Implemented");
        }

        public void GetEntityRow(object Id)
        {
            throw new NotImplementedException("WcTableColumn_Bll GetEntityRow(object Id) Not Implemented");
        }

        private void GetEntityRowResponse(object sender, WcTableColumn_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                if (data == null)
                {
                    // Alert UI
                    return;
                }
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array == null)
                {
                    // Alert UI
                    return;
                }
                _brokenRuleManager.ClearAllBrokenRules();
                _data.ReplaceData((object[])array[0], new EntityState(false, true, false));
                RaiseEventEntityRowReceived();
                RefreshFields();
                // Do we still need this next line?
                //RaiseEventCurrentEntityStateChanged(_data.S.Switch);

                //  ToDo:  Later when we have a Foreign Key, do something like this:  DataProps.TbC_Tb_ID = DataProps.standing_FK;
                //              How do we handle the standing FK in the new framework?
 
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", IfxTraceCategory.Leave);
            }
        }



        /// <summary>
        ///     Called by <see cref="GetDefault">GetDefault</see> and creates a new WcTableColumn Id value
        ///     for a new data object (<see cref="EntityWireType.WcTableColumn_ValuesMngr">WcTableColumn_ValuesMngr</see>).
        /// </summary>
        private Guid GetNewID()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", IfxTraceCategory.Enter);
                return Guid.NewGuid();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Un-does the current data edits and restores everything back to the previous saved
        ///     or new <see cref="TypeServices.EntityState">state</see>. This will include calling
        ///     <see cref="TypeServices.BrokenRuleManager.ClearAllBrokenRules">_brokenRuleManager.ClearAllBrokenRules</see>
        ///     to clear any BrokenRules or calling <see cref="SetDefaultBrokenRules">SetDefaultBrokenRules</see> to set any default BrokenRules;
        ///     and raising the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event is raised to
        ///     notify the parent objects that the state has changed.
        /// </summary>
        public void UnDo()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Enter);
                EntityStateSwitch es = _data.S.Switch;
                _data.SetCurrentToOriginal();
                if (_data.S.IsNew() == true)
                {
                    SetDefaultBrokenRules();
                }
                else
                {
                    _brokenRuleManager.ClearAllBrokenRules();
                }
                CheckEntityState(es);

                UnDoCustomCode();
                RefreshFields();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Call the Notify method for all visible fields.  This allows the fields/properties to be refreshed in grids and other controls that are consuming them via databinding.
        /// </summary>
        public void RefreshFields()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", IfxTraceCategory.Enter);
				Notify("ImportColumn");
				Notify("TbC_SortOrder");
				Notify("DbSortOrder");
				Notify("TbC_Name");
				Notify("ColumnInDatabase");
				Notify("TbC_CtlTp_Id");
				Notify("TbC_CtlTp_Id_TextField");
				Notify("TbC_IsNonvColumn");
				Notify("TbC_Description");
				Notify("TbC_DtSql_Id");
				Notify("TbC_DtSql_Id_TextField");
				Notify("TbC_DtDtNt_Id");
				Notify("TbC_DtDtNt_Id_TextField");
				Notify("TbC_Length");
				Notify("TbC_Precision");
				Notify("TbC_Scale");
				Notify("TbC_IsPK");
				Notify("TbC_IsIdentity");
				Notify("TbC_IsFK");
				Notify("TbC_IsEntityColumn");
				Notify("TbC_IsSystemField");
				Notify("TbC_IsValuesObjectMember");
				Notify("TbC_IsInPropsScreen");
				Notify("TbC_IsInNavList");
				Notify("TbC_IsRequired");
				Notify("TbC_BrokenRuleText");
				Notify("TbC_AllowZero");
				Notify("TbC_IsNullableInDb");
				Notify("TbC_IsNullableInUI");
				Notify("TbC_DefaultValue");
				Notify("TbC_IsReadFromDb");
				Notify("TbC_IsSendToDb");
				Notify("TbC_IsInsertAllowed");
				Notify("TbC_IsEditAllowed");
				Notify("TbC_IsReadOnlyInUI");
				Notify("TbC_UseForAudit");
				Notify("TbC_DefaultCaption");
				Notify("TbC_ColumnHeaderText");
				Notify("TbC_LabelCaptionVerbose");
				Notify("TbC_LabelCaptionGenerate");
				Notify("Tbc_ShowGridColumnToolTip");
				Notify("Tbc_ShowPropsToolTip");
				Notify("Tbc_TooltipsRolledUp");
				Notify("TbC_IsCreatePropsStrings");
				Notify("TbC_IsCreateGridStrings");
				Notify("TbC_IsAvailableForColumnGroups");
				Notify("TbC_IsTextWrapInProp");
				Notify("TbC_IsTextWrapInGrid");
				Notify("TbC_TextBoxFormat");
				Notify("TbC_TextColumnFormat");
				Notify("TbC_ColumnWidth");
				Notify("TbC_TextBoxTextAlignment");
				Notify("TbC_ColumnTextAlignment");
				Notify("TbC_ListStoredProc_Id");
				Notify("TbC_ListStoredProc_Id_TextField");
				Notify("TbC_IsStaticList");
				Notify("TbC_UseNotInList");
				Notify("TbC_UseListEditBtn");
				Notify("TbC_ParentColumnKey");
				Notify("TbC_ParentColumnKey_TextField");
				Notify("TbC_UseDisplayTextFieldProperty");
				Notify("TbC_IsDisplayTextFieldProperty");
				Notify("TbC_ComboListTable_Id");
				Notify("TbC_ComboListTable_Id_TextField");
				Notify("TbC_ComboListDisplayColumn_Id");
				Notify("TbC_ComboListDisplayColumn_Id_TextField");
				Notify("TbC_Combo_MaxDropdownHeight");
				Notify("TbC_Combo_MaxDropdownWidth");
				Notify("TbC_Combo_AllowDropdownResizing");
				Notify("TbC_Combo_IsResetButtonVisible");
				Notify("TbC_IsActiveRecColumn");
				Notify("TbC_IsDeletedColumn");
				Notify("TbC_IsCreatedUserIdColumn");
				Notify("TbC_IsCreatedDateColumn");
				Notify("TbC_IsUserIdColumn");
				Notify("TbC_IsModifiedDateColumn");
				Notify("TbC_IsRowVersionStampColumn");
				Notify("TbC_IsBrowsable");
				Notify("TbC_DeveloperNote");
				Notify("TbC_UserNote");
				Notify("TbC_HelpFileAdditionalNote");
				Notify("TbC_Notes");
				Notify("TbC_IsInputComplete");
				Notify("TbC_IsCodeGen");
				Notify("TbC_IsReadyCodeGen");
				Notify("TbC_IsCodeGenComplete");
				Notify("TbC_IsTagForCodeGen");
				Notify("TbC_IsTagForOther");
				Notify("TbC_ColumnGroups");
				Notify("TbC_IsActiveRow");
				Notify("UserName");
				Notify("TbC_LastModifiedDate");

                RefreshFieldsCustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Called from <see cref="wnConcurrencyManager">wnConcurrencyManager</see> and therefore does not need
        ///     to pass in the Parent and Parent Type parameters.
        ///     Saves the current data to the data store, configures the appropriate state and
        ///     calls <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see> to
        ///     notify the parent object of the state change.
        /// </summary>
        public void Save(UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                _data.C.TbC_UserId_noevents = Credentials.UserId;
                WcTableColumnProxy.Begin_WcTableColumn_Save(_data.C.GetValues(), (int)check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Saves the current data to the data store, configures the appropriate state and
        ///     calls <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see> to
        ///     notify the parent object of the state change.
        /// </summary>
        public void Save(object parentEditObject, ParentEditObjectType parentType,  UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                _parentEditObject = parentEditObject;
                _parentType = parentType;
                _data.C.TbC_UserId_noevents = Credentials.UserId;
                WcTableColumnProxy.Begin_WcTableColumn_Save(_data.C.GetValues(), (int)check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        void SaveCompleted(object sender, WcTableColumn_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", IfxTraceCategory.Enter);
                DataServiceInsertUpdateResponse newData = null;
                if (e.Result == null)
                {
                    // return some message to the calling object
                    newData = new DataServiceInsertUpdateResponse();
                    newData.Result = DataOperationResult.HandledException;
                    //RaiseEventAsyncSaveComplete(newData.Result, null);
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);
                }
                else
                {
                    newData = new DataServiceInsertUpdateResponse();
                    byte[] data = e.Result;
                    object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                    newData.SetFromObjectArray(array);
                }

                // Get the Result status
                if (newData.Result == DataOperationResult.Success)
                {
                    if (newData.ReturnCurrentRowOnInsertUpdate == true)
                    {
                        // If we returned the current row from the data store, then syncronize the wire object with it.
                        _data.C.ReplaceDataFromObjectArray(newData.CurrentValues);
                        RefreshFields();
                    }
                    else
                    {
                        // Otherwise:
                        //  Set the PK value from the data store incase we did an insert.
                        //  In theory the guid was already created by this busness object, but we're setting it here
                        //  just in case some code changed somewhere and it was created on the server.
                        _data.C._a = newData.guidPrimaryKey;
                        //  Set the new TimeStamp value
                        _data.C.TbC_Stamp_noevents = newData.CurrentTimestamp;
                    }
                    _data.S.SetNotNew();
                    _data.S.SetValid();
                    _data.S.SetNotDirty();
                    _data.SetOriginalToCurrent();
                  
                    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);

                    //if (_parentType == ParentEditObjectType.EntitiyPropertiesControl)
                    //{
                    //    // A EntitiyPropertiesControl called the same method, therefore raise the following event
                    //    // allowing all parent conrol to reconfigure thier entity state.
                    //    // If this was called from editing a grid control, then this is not nessesacy since we 
                    //    // dont change the state of surrounding controls.
                    //    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                    //
                    //    // Rasie this event to the props control can call it's SetState method to refresh the UI
                    //    // The Entity List control will be updated via the PropertyChanged event which gets called from the RefreshFields call above.
                    //    if (newData.ReturnCurrentRowOnInsertUpdate == true)
                    //    {
                    //        RaiseEventAsyncSaveWithResponseComplete(newData, null);
                    //    }
                    //
                    //}
                    //else
                    //{
                    //    RaiseEventAsyncSaveComplete(newData.Result, null);
                    //}
                }
                else
                {
                    // Add the current date from the database to the wire obect's concurrent property
                    //if (newData.Result == DataOperationResult.ConcurrencyFailure)
                    //{
                    //    _data.X.ReplaceDataFromObjectArray(newData.CurrentValues);
                    //}
                    // ToDo:  we need to develope logic and code to pass user friendly error message 
                    // instead of null when the save failed.

                    // We had some type of failure, so raise this event regardless of the parent type 
                    // so the UI can notify the user.
                    //RaiseEventAsyncSaveComplete(newData.Result, null);
                    //*** use this instead
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _parentEditObject = null;
                _parentType = ParentEditObjectType.None;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", IfxTraceCategory.Leave);
            }
        }







        /// <summary>
        /// Calls a Delete method on the server to delete this entity from the data
        /// store.
        /// </summary>
        /// <returns>1 = Success, 2 = Failed</returns>
        public int  Entity_Delete()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", IfxTraceCategory.Enter);
//                // add credentials
//                WcTableColumn_ValuesMngr retObject = ProxyWrapper.EntityProxyWrapper.WcTableColumn_Delete(_data);
//                return 1;
//                //  Needs further design.  What do we do when the delete fails or there is an concurrency issue?
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Just a stub and not being used. Consider renaming this to ActivatOrDeactivate so
        /// it can be used either way.
        /// </summary>
        /// <returns>Returns 0 because this method is not currently functional.</returns>
        public int Entity_Deactivate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", IfxTraceCategory.Enter);
                //  This is currently setup to return a wire object, however, we might only need to return an int flag
                //  Needs further design
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", IfxTraceCategory.Leave);
            }
        }

        /// <returns>Returns 0 because this method is not currently functional.</returns>
        /// <summary>Just a stub and not being used.</summary>
        public int Entity_Remove()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", IfxTraceCategory.Enter);
                //  This is currently setup to return a wire object, however, we might only need to return an int flag
                //  Needs further design
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Copies the values from another data object (<see cref="EntityWireType.WcTableColumn_ValuesMngr">WcTableColumn_ValuesMngr</see>) to the one plugged into
        ///     this business object. This is called from the Save method because it returns a
        ///     fresh copy of this entity’s data from the data store incase modifications were made
        ///     in the process of saving.
        /// </summary>
        /// <param name="thisData">
        /// The data object (WcTableColumn_ValuesMngr) plugged into this business object which data
        /// will be copied to.
        /// </param>
        /// <param name="newData">The data object (WcTableColumn_ValuesMngr) which data will be copied from.</param>
        private void SyncValueObjectCurrentProperties(WcTableColumn_ValuesMngr thisData, WcTableColumn_ValuesMngr newData)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", IfxTraceCategory.Enter);
				thisData.C.TbC_Id_noevents = newData.C.TbC_Id_noevents;
				thisData.C.TbC_Tb_Id_noevents = newData.C.TbC_Tb_Id_noevents;
				thisData.C.TbC_auditColumn_Id_noevents = newData.C.TbC_auditColumn_Id_noevents;
				thisData.C.TbC_SortOrder_noevents = newData.C.TbC_SortOrder_noevents;
				thisData.C.DbSortOrder_noevents = newData.C.DbSortOrder_noevents;
				thisData.C.IsImported_noevents = newData.C.IsImported_noevents;
				thisData.C.TbC_Name_noevents = newData.C.TbC_Name_noevents;
				thisData.C.ColumnInDatabase_noevents = newData.C.ColumnInDatabase_noevents;
				thisData.C.TbC_CtlTp_Id_noevents = newData.C.TbC_CtlTp_Id_noevents;
				thisData.C.TbC_CtlTp_Id_TextField_noevents = newData.C.TbC_CtlTp_Id_TextField_noevents;
				thisData.C.TbC_IsNonvColumn_noevents = newData.C.TbC_IsNonvColumn_noevents;
				thisData.C.TbC_Description_noevents = newData.C.TbC_Description_noevents;
				thisData.C.TbC_DtSql_Id_noevents = newData.C.TbC_DtSql_Id_noevents;
				thisData.C.TbC_DtSql_Id_TextField_noevents = newData.C.TbC_DtSql_Id_TextField_noevents;
				thisData.C.TbC_DtDtNt_Id_noevents = newData.C.TbC_DtDtNt_Id_noevents;
				thisData.C.TbC_DtDtNt_Id_TextField_noevents = newData.C.TbC_DtDtNt_Id_TextField_noevents;
				thisData.C.TbC_Length_noevents = newData.C.TbC_Length_noevents;
				thisData.C.TbC_Precision_noevents = newData.C.TbC_Precision_noevents;
				thisData.C.TbC_Scale_noevents = newData.C.TbC_Scale_noevents;
				thisData.C.TbC_IsPK_noevents = newData.C.TbC_IsPK_noevents;
				thisData.C.TbC_IsIdentity_noevents = newData.C.TbC_IsIdentity_noevents;
				thisData.C.TbC_IsFK_noevents = newData.C.TbC_IsFK_noevents;
				thisData.C.TbC_IsEntityColumn_noevents = newData.C.TbC_IsEntityColumn_noevents;
				thisData.C.TbC_IsSystemField_noevents = newData.C.TbC_IsSystemField_noevents;
				thisData.C.TbC_IsValuesObjectMember_noevents = newData.C.TbC_IsValuesObjectMember_noevents;
				thisData.C.TbC_IsInPropsScreen_noevents = newData.C.TbC_IsInPropsScreen_noevents;
				thisData.C.TbC_IsInNavList_noevents = newData.C.TbC_IsInNavList_noevents;
				thisData.C.TbC_IsRequired_noevents = newData.C.TbC_IsRequired_noevents;
				thisData.C.TbC_BrokenRuleText_noevents = newData.C.TbC_BrokenRuleText_noevents;
				thisData.C.TbC_AllowZero_noevents = newData.C.TbC_AllowZero_noevents;
				thisData.C.TbC_IsNullableInDb_noevents = newData.C.TbC_IsNullableInDb_noevents;
				thisData.C.TbC_IsNullableInUI_noevents = newData.C.TbC_IsNullableInUI_noevents;
				thisData.C.TbC_DefaultValue_noevents = newData.C.TbC_DefaultValue_noevents;
				thisData.C.TbC_IsReadFromDb_noevents = newData.C.TbC_IsReadFromDb_noevents;
				thisData.C.TbC_IsSendToDb_noevents = newData.C.TbC_IsSendToDb_noevents;
				thisData.C.TbC_IsInsertAllowed_noevents = newData.C.TbC_IsInsertAllowed_noevents;
				thisData.C.TbC_IsEditAllowed_noevents = newData.C.TbC_IsEditAllowed_noevents;
				thisData.C.TbC_IsReadOnlyInUI_noevents = newData.C.TbC_IsReadOnlyInUI_noevents;
				thisData.C.TbC_UseForAudit_noevents = newData.C.TbC_UseForAudit_noevents;
				thisData.C.TbC_DefaultCaption_noevents = newData.C.TbC_DefaultCaption_noevents;
				thisData.C.TbC_ColumnHeaderText_noevents = newData.C.TbC_ColumnHeaderText_noevents;
				thisData.C.TbC_LabelCaptionVerbose_noevents = newData.C.TbC_LabelCaptionVerbose_noevents;
				thisData.C.TbC_LabelCaptionGenerate_noevents = newData.C.TbC_LabelCaptionGenerate_noevents;
				thisData.C.Tbc_ShowGridColumnToolTip_noevents = newData.C.Tbc_ShowGridColumnToolTip_noevents;
				thisData.C.Tbc_ShowPropsToolTip_noevents = newData.C.Tbc_ShowPropsToolTip_noevents;
				thisData.C.Tbc_TooltipsRolledUp_noevents = newData.C.Tbc_TooltipsRolledUp_noevents;
				thisData.C.TbC_IsCreatePropsStrings_noevents = newData.C.TbC_IsCreatePropsStrings_noevents;
				thisData.C.TbC_IsCreateGridStrings_noevents = newData.C.TbC_IsCreateGridStrings_noevents;
				thisData.C.TbC_IsAvailableForColumnGroups_noevents = newData.C.TbC_IsAvailableForColumnGroups_noevents;
				thisData.C.TbC_IsTextWrapInProp_noevents = newData.C.TbC_IsTextWrapInProp_noevents;
				thisData.C.TbC_IsTextWrapInGrid_noevents = newData.C.TbC_IsTextWrapInGrid_noevents;
				thisData.C.TbC_TextBoxFormat_noevents = newData.C.TbC_TextBoxFormat_noevents;
				thisData.C.TbC_TextColumnFormat_noevents = newData.C.TbC_TextColumnFormat_noevents;
				thisData.C.TbC_ColumnWidth_noevents = newData.C.TbC_ColumnWidth_noevents;
				thisData.C.TbC_TextBoxTextAlignment_noevents = newData.C.TbC_TextBoxTextAlignment_noevents;
				thisData.C.TbC_ColumnTextAlignment_noevents = newData.C.TbC_ColumnTextAlignment_noevents;
				thisData.C.TbC_ListStoredProc_Id_noevents = newData.C.TbC_ListStoredProc_Id_noevents;
				thisData.C.TbC_ListStoredProc_Id_TextField_noevents = newData.C.TbC_ListStoredProc_Id_TextField_noevents;
				thisData.C.TbC_IsStaticList_noevents = newData.C.TbC_IsStaticList_noevents;
				thisData.C.TbC_UseNotInList_noevents = newData.C.TbC_UseNotInList_noevents;
				thisData.C.TbC_UseListEditBtn_noevents = newData.C.TbC_UseListEditBtn_noevents;
				thisData.C.TbC_ParentColumnKey_noevents = newData.C.TbC_ParentColumnKey_noevents;
				thisData.C.TbC_ParentColumnKey_TextField_noevents = newData.C.TbC_ParentColumnKey_TextField_noevents;
				thisData.C.TbC_UseDisplayTextFieldProperty_noevents = newData.C.TbC_UseDisplayTextFieldProperty_noevents;
				thisData.C.TbC_IsDisplayTextFieldProperty_noevents = newData.C.TbC_IsDisplayTextFieldProperty_noevents;
				thisData.C.TbC_ComboListTable_Id_noevents = newData.C.TbC_ComboListTable_Id_noevents;
				thisData.C.TbC_ComboListTable_Id_TextField_noevents = newData.C.TbC_ComboListTable_Id_TextField_noevents;
				thisData.C.TbC_ComboListDisplayColumn_Id_noevents = newData.C.TbC_ComboListDisplayColumn_Id_noevents;
				thisData.C.TbC_ComboListDisplayColumn_Id_TextField_noevents = newData.C.TbC_ComboListDisplayColumn_Id_TextField_noevents;
				thisData.C.TbC_Combo_MaxDropdownHeight_noevents = newData.C.TbC_Combo_MaxDropdownHeight_noevents;
				thisData.C.TbC_Combo_MaxDropdownWidth_noevents = newData.C.TbC_Combo_MaxDropdownWidth_noevents;
				thisData.C.TbC_Combo_AllowDropdownResizing_noevents = newData.C.TbC_Combo_AllowDropdownResizing_noevents;
				thisData.C.TbC_Combo_IsResetButtonVisible_noevents = newData.C.TbC_Combo_IsResetButtonVisible_noevents;
				thisData.C.TbC_IsActiveRecColumn_noevents = newData.C.TbC_IsActiveRecColumn_noevents;
				thisData.C.TbC_IsDeletedColumn_noevents = newData.C.TbC_IsDeletedColumn_noevents;
				thisData.C.TbC_IsCreatedUserIdColumn_noevents = newData.C.TbC_IsCreatedUserIdColumn_noevents;
				thisData.C.TbC_IsCreatedDateColumn_noevents = newData.C.TbC_IsCreatedDateColumn_noevents;
				thisData.C.TbC_IsUserIdColumn_noevents = newData.C.TbC_IsUserIdColumn_noevents;
				thisData.C.TbC_IsModifiedDateColumn_noevents = newData.C.TbC_IsModifiedDateColumn_noevents;
				thisData.C.TbC_IsRowVersionStampColumn_noevents = newData.C.TbC_IsRowVersionStampColumn_noevents;
				thisData.C.TbC_IsBrowsable_noevents = newData.C.TbC_IsBrowsable_noevents;
				thisData.C.TbC_DeveloperNote_noevents = newData.C.TbC_DeveloperNote_noevents;
				thisData.C.TbC_UserNote_noevents = newData.C.TbC_UserNote_noevents;
				thisData.C.TbC_HelpFileAdditionalNote_noevents = newData.C.TbC_HelpFileAdditionalNote_noevents;
				thisData.C.TbC_Notes_noevents = newData.C.TbC_Notes_noevents;
				thisData.C.TbC_IsInputComplete_noevents = newData.C.TbC_IsInputComplete_noevents;
				thisData.C.TbC_IsCodeGen_noevents = newData.C.TbC_IsCodeGen_noevents;
				thisData.C.TbC_IsReadyCodeGen_noevents = newData.C.TbC_IsReadyCodeGen_noevents;
				thisData.C.TbC_IsCodeGenComplete_noevents = newData.C.TbC_IsCodeGenComplete_noevents;
				thisData.C.TbC_IsTagForCodeGen_noevents = newData.C.TbC_IsTagForCodeGen_noevents;
				thisData.C.TbC_IsTagForOther_noevents = newData.C.TbC_IsTagForOther_noevents;
				thisData.C.TbC_ColumnGroups_noevents = newData.C.TbC_ColumnGroups_noevents;
				thisData.C.TbC_IsActiveRow_noevents = newData.C.TbC_IsActiveRow_noevents;
				thisData.C.TbC_IsDeleted_noevents = newData.C.TbC_IsDeleted_noevents;
				thisData.C.TbC_CreatedUserId_noevents = newData.C.TbC_CreatedUserId_noevents;
				thisData.C.TbC_CreatedDate_noevents = newData.C.TbC_CreatedDate_noevents;
				thisData.C.TbC_UserId_noevents = newData.C.TbC_UserId_noevents;
				thisData.C.UserName_noevents = newData.C.UserName_noevents;
				thisData.C.TbC_LastModifiedDate_noevents = newData.C.TbC_LastModifiedDate_noevents;
				thisData.C.TbC_Stamp_noevents = newData.C.TbC_Stamp_noevents;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", IfxTraceCategory.Leave);
            }
        }


		#endregion CRUD Methods


		#region General Methods and Properties


		#region Wire Methods

        /// <summary>
        /// Returns a business object implementing the IBusinessObject interface from a wire
        /// object. This assumes the WcTableColumn_ValuesMngr being passed in has been fetched from a
        /// reliable data store or some other reliable source which gives us appropriate data and
        /// state.
        /// </summary>
        public static WcTableColumn_Bll FromWire(WcTableColumn_ValuesMngr valueObject)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", new ValuePair[] {new ValuePair("valueObject", valueObject) }, IfxTraceCategory.Enter);
                //  This assumes that each WcTableColumn_ValuesMngr has been fetched from a db or some other reliable source which gives us the following state
                //  Hack, Hack;  we have to add the state here becuase of how its handled (or not handled) in WcTableColumn_ValuesMngr due to the problem of loosing this value when passed accross the wire
                WcTableColumn_Bll obj = new WcTableColumn_Bll(valueObject, new EntityState(false, true, false));
                return obj;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Returns the current wire object plugged into this business object.</summary>
        public WcTableColumn_ValuesMngr ToWire()
        {
            return _data;
        }

        /// <summary>The wire object property.</summary>
        public WcTableColumn_ValuesMngr Wire
        {
            get { return _data; }
            set
            {
                _data = value;
            }
        }        

		#endregion Wire Methods


        /// <overloads>Get a list of current BrokenRules for this entity.</overloads>
        /// <summary>Retuns the current BrokenRules as list of strings.</summary>
        public List<string> GetBrokenRulesForEntity()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRulesForEntity();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Pass in a valid format as a string and return the current BrokenRules in a
        /// formatted list of strings.
        /// </summary>
        public List<string> GetBrokenRulesForEntity(string format)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", new ValuePair[] {new ValuePair("format", format) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRulesForEntity(format);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Leave);
            }
        }

        // get the current list of broken rules for a property
        /// <summary>Pass in a property name and return a list of its current BrokenRules.</summary>
        public List<vRuleItem> GetBrokenRulesForProperty(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRuleListForProperty(propertyName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Pass in a property name to find out if it’s valid or not.</summary>
        /// <returns>true = valid, false = not valid</returns>
        public bool IsPropertyValid(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.IsPropertyValid(propertyName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", IfxTraceCategory.Leave);
            }
        }

        public bool IsPropertyDirty(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);

                if (propertyName == null)
                {
                    throw new Exception("WcTableColumn_Bll.IsPropertyDirty: Parameter 'propertyName' had no value.");
                }
                else if (propertyName.Trim().Length == 0)
                {
                    throw new Exception("WcTableColumn_Bll.IsPropertyDirty: Parameter 'propertyName' had no value.");
                }

                switch (propertyName)
                {
                    case "TbC_Tb_Id":
                        if (_data.C.TbC_Tb_Id != _data.O.TbC_Tb_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_auditColumn_Id":
                        if (_data.C.TbC_auditColumn_Id != _data.O.TbC_auditColumn_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ImportColumn":
                        return IsPropertyDirtyCustomCode(propertyName);
                    case "TbC_SortOrder":
                        if (_data.C.TbC_SortOrder != _data.O.TbC_SortOrder)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "DbSortOrder":
                        if (_data.C.DbSortOrder != _data.O.DbSortOrder)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "IsImported":
                        if (_data.C.IsImported != _data.O.IsImported)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_Name":
                        if (_data.C.TbC_Name != _data.O.TbC_Name)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ColumnInDatabase":
                        if (_data.C.ColumnInDatabase != _data.O.ColumnInDatabase)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_CtlTp_Id":
                        if (_data.C.TbC_CtlTp_Id != _data.O.TbC_CtlTp_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsNonvColumn":
                        if (_data.C.TbC_IsNonvColumn != _data.O.TbC_IsNonvColumn)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_Description":
                        if (_data.C.TbC_Description != _data.O.TbC_Description)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_DtSql_Id":
                        if (_data.C.TbC_DtSql_Id != _data.O.TbC_DtSql_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_DtDtNt_Id":
                        if (_data.C.TbC_DtDtNt_Id != _data.O.TbC_DtDtNt_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_Length":
                        if (_data.C.TbC_Length != _data.O.TbC_Length)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_Precision":
                        if (_data.C.TbC_Precision != _data.O.TbC_Precision)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_Scale":
                        if (_data.C.TbC_Scale != _data.O.TbC_Scale)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsPK":
                        if (_data.C.TbC_IsPK != _data.O.TbC_IsPK)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsIdentity":
                        if (_data.C.TbC_IsIdentity != _data.O.TbC_IsIdentity)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsFK":
                        if (_data.C.TbC_IsFK != _data.O.TbC_IsFK)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsEntityColumn":
                        if (_data.C.TbC_IsEntityColumn != _data.O.TbC_IsEntityColumn)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsSystemField":
                        if (_data.C.TbC_IsSystemField != _data.O.TbC_IsSystemField)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsValuesObjectMember":
                        if (_data.C.TbC_IsValuesObjectMember != _data.O.TbC_IsValuesObjectMember)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsInPropsScreen":
                        if (_data.C.TbC_IsInPropsScreen != _data.O.TbC_IsInPropsScreen)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsInNavList":
                        if (_data.C.TbC_IsInNavList != _data.O.TbC_IsInNavList)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsRequired":
                        if (_data.C.TbC_IsRequired != _data.O.TbC_IsRequired)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_BrokenRuleText":
                        if (_data.C.TbC_BrokenRuleText != _data.O.TbC_BrokenRuleText)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_AllowZero":
                        if (_data.C.TbC_AllowZero != _data.O.TbC_AllowZero)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsNullableInDb":
                        if (_data.C.TbC_IsNullableInDb != _data.O.TbC_IsNullableInDb)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsNullableInUI":
                        if (_data.C.TbC_IsNullableInUI != _data.O.TbC_IsNullableInUI)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_DefaultValue":
                        if (_data.C.TbC_DefaultValue != _data.O.TbC_DefaultValue)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsReadFromDb":
                        if (_data.C.TbC_IsReadFromDb != _data.O.TbC_IsReadFromDb)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsSendToDb":
                        if (_data.C.TbC_IsSendToDb != _data.O.TbC_IsSendToDb)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsInsertAllowed":
                        if (_data.C.TbC_IsInsertAllowed != _data.O.TbC_IsInsertAllowed)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsEditAllowed":
                        if (_data.C.TbC_IsEditAllowed != _data.O.TbC_IsEditAllowed)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsReadOnlyInUI":
                        if (_data.C.TbC_IsReadOnlyInUI != _data.O.TbC_IsReadOnlyInUI)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_UseForAudit":
                        if (_data.C.TbC_UseForAudit != _data.O.TbC_UseForAudit)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_DefaultCaption":
                        if (_data.C.TbC_DefaultCaption != _data.O.TbC_DefaultCaption)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_ColumnHeaderText":
                        if (_data.C.TbC_ColumnHeaderText != _data.O.TbC_ColumnHeaderText)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_LabelCaptionVerbose":
                        if (_data.C.TbC_LabelCaptionVerbose != _data.O.TbC_LabelCaptionVerbose)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_LabelCaptionGenerate":
                        if (_data.C.TbC_LabelCaptionGenerate != _data.O.TbC_LabelCaptionGenerate)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tbc_ShowGridColumnToolTip":
                        if (_data.C.Tbc_ShowGridColumnToolTip != _data.O.Tbc_ShowGridColumnToolTip)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tbc_ShowPropsToolTip":
                        if (_data.C.Tbc_ShowPropsToolTip != _data.O.Tbc_ShowPropsToolTip)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tbc_TooltipsRolledUp":
                        if (_data.C.Tbc_TooltipsRolledUp != _data.O.Tbc_TooltipsRolledUp)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsCreatePropsStrings":
                        if (_data.C.TbC_IsCreatePropsStrings != _data.O.TbC_IsCreatePropsStrings)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsCreateGridStrings":
                        if (_data.C.TbC_IsCreateGridStrings != _data.O.TbC_IsCreateGridStrings)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsAvailableForColumnGroups":
                        if (_data.C.TbC_IsAvailableForColumnGroups != _data.O.TbC_IsAvailableForColumnGroups)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsTextWrapInProp":
                        if (_data.C.TbC_IsTextWrapInProp != _data.O.TbC_IsTextWrapInProp)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsTextWrapInGrid":
                        if (_data.C.TbC_IsTextWrapInGrid != _data.O.TbC_IsTextWrapInGrid)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_TextBoxFormat":
                        if (_data.C.TbC_TextBoxFormat != _data.O.TbC_TextBoxFormat)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_TextColumnFormat":
                        if (_data.C.TbC_TextColumnFormat != _data.O.TbC_TextColumnFormat)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_ColumnWidth":
                        if (_data.C.TbC_ColumnWidth != _data.O.TbC_ColumnWidth)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_TextBoxTextAlignment":
                        if (_data.C.TbC_TextBoxTextAlignment != _data.O.TbC_TextBoxTextAlignment)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_ColumnTextAlignment":
                        if (_data.C.TbC_ColumnTextAlignment != _data.O.TbC_ColumnTextAlignment)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_ListStoredProc_Id":
                        if (_data.C.TbC_ListStoredProc_Id != _data.O.TbC_ListStoredProc_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsStaticList":
                        if (_data.C.TbC_IsStaticList != _data.O.TbC_IsStaticList)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_UseNotInList":
                        if (_data.C.TbC_UseNotInList != _data.O.TbC_UseNotInList)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_UseListEditBtn":
                        if (_data.C.TbC_UseListEditBtn != _data.O.TbC_UseListEditBtn)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_ParentColumnKey":
                        if (_data.C.TbC_ParentColumnKey != _data.O.TbC_ParentColumnKey)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_UseDisplayTextFieldProperty":
                        if (_data.C.TbC_UseDisplayTextFieldProperty != _data.O.TbC_UseDisplayTextFieldProperty)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsDisplayTextFieldProperty":
                        if (_data.C.TbC_IsDisplayTextFieldProperty != _data.O.TbC_IsDisplayTextFieldProperty)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_ComboListTable_Id":
                        if (_data.C.TbC_ComboListTable_Id != _data.O.TbC_ComboListTable_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_ComboListDisplayColumn_Id":
                        if (_data.C.TbC_ComboListDisplayColumn_Id != _data.O.TbC_ComboListDisplayColumn_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_Combo_MaxDropdownHeight":
                        if (_data.C.TbC_Combo_MaxDropdownHeight != _data.O.TbC_Combo_MaxDropdownHeight)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_Combo_MaxDropdownWidth":
                        if (_data.C.TbC_Combo_MaxDropdownWidth != _data.O.TbC_Combo_MaxDropdownWidth)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_Combo_AllowDropdownResizing":
                        if (_data.C.TbC_Combo_AllowDropdownResizing != _data.O.TbC_Combo_AllowDropdownResizing)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_Combo_IsResetButtonVisible":
                        if (_data.C.TbC_Combo_IsResetButtonVisible != _data.O.TbC_Combo_IsResetButtonVisible)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsActiveRecColumn":
                        if (_data.C.TbC_IsActiveRecColumn != _data.O.TbC_IsActiveRecColumn)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsDeletedColumn":
                        if (_data.C.TbC_IsDeletedColumn != _data.O.TbC_IsDeletedColumn)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsCreatedUserIdColumn":
                        if (_data.C.TbC_IsCreatedUserIdColumn != _data.O.TbC_IsCreatedUserIdColumn)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsCreatedDateColumn":
                        if (_data.C.TbC_IsCreatedDateColumn != _data.O.TbC_IsCreatedDateColumn)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsUserIdColumn":
                        if (_data.C.TbC_IsUserIdColumn != _data.O.TbC_IsUserIdColumn)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsModifiedDateColumn":
                        if (_data.C.TbC_IsModifiedDateColumn != _data.O.TbC_IsModifiedDateColumn)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsRowVersionStampColumn":
                        if (_data.C.TbC_IsRowVersionStampColumn != _data.O.TbC_IsRowVersionStampColumn)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsBrowsable":
                        if (_data.C.TbC_IsBrowsable != _data.O.TbC_IsBrowsable)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_DeveloperNote":
                        if (_data.C.TbC_DeveloperNote != _data.O.TbC_DeveloperNote)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_UserNote":
                        if (_data.C.TbC_UserNote != _data.O.TbC_UserNote)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_HelpFileAdditionalNote":
                        if (_data.C.TbC_HelpFileAdditionalNote != _data.O.TbC_HelpFileAdditionalNote)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_Notes":
                        if (_data.C.TbC_Notes != _data.O.TbC_Notes)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsInputComplete":
                        if (_data.C.TbC_IsInputComplete != _data.O.TbC_IsInputComplete)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsCodeGen":
                        if (_data.C.TbC_IsCodeGen != _data.O.TbC_IsCodeGen)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsReadyCodeGen":
                        if (_data.C.TbC_IsReadyCodeGen != _data.O.TbC_IsReadyCodeGen)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsCodeGenComplete":
                        if (_data.C.TbC_IsCodeGenComplete != _data.O.TbC_IsCodeGenComplete)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsTagForCodeGen":
                        if (_data.C.TbC_IsTagForCodeGen != _data.O.TbC_IsTagForCodeGen)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsTagForOther":
                        if (_data.C.TbC_IsTagForOther != _data.O.TbC_IsTagForOther)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_ColumnGroups":
                        if (_data.C.TbC_ColumnGroups != _data.O.TbC_ColumnGroups)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsActiveRow":
                        if (_data.C.TbC_IsActiveRow != _data.O.TbC_IsActiveRow)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_IsDeleted":
                        if (_data.C.TbC_IsDeleted != _data.O.TbC_IsDeleted)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "TbC_CreatedUserId":
                        if (_data.C.TbC_CreatedUserId != _data.O.TbC_CreatedUserId)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "UserName":
                        if (_data.C.UserName != _data.O.UserName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    default:
                        throw new Exception("WcTableColumn_Bll.IsPropertyDirty found no matching propery name for " + propertyName);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", IfxTraceCategory.Leave);
            }
        }

        public void SetDateFromString(string propName, string value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", new ValuePair[] {new ValuePair("value", value), new ValuePair("propName", propName) }, IfxTraceCategory.Enter);
                DateTime? dt = null;
                if (BLLHelper.IsDate(value) == true)
                {
                    dt = DateTime.Parse(value);
                }
                switch (propName)
                {
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", IfxTraceCategory.Leave);
            }
        }

        public object GetPropertyValueByKey(string key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", new ValuePair[] {new ValuePair("key", key) }, IfxTraceCategory.Enter);
                switch (key)
                {
                    case "TbC_SortOrder":
                        return TbC_SortOrder;

                    case "DbSortOrder":
                        return DbSortOrder;

                    case "TbC_Name":
                        return TbC_Name;

                    case "ColumnInDatabase":
                        return ColumnInDatabase;

                    case "TbC_CtlTp_Id":
                        return TbC_CtlTp_Id;

                    case "TbC_IsNonvColumn":
                        return TbC_IsNonvColumn;

                    case "TbC_Description":
                        return TbC_Description;

                    case "TbC_DtSql_Id":
                        return TbC_DtSql_Id;

                    case "TbC_DtDtNt_Id":
                        return TbC_DtDtNt_Id;

                    case "TbC_Length":
                        return TbC_Length;

                    case "TbC_Precision":
                        return TbC_Precision;

                    case "TbC_Scale":
                        return TbC_Scale;

                    case "TbC_IsPK":
                        return TbC_IsPK;

                    case "TbC_IsIdentity":
                        return TbC_IsIdentity;

                    case "TbC_IsFK":
                        return TbC_IsFK;

                    case "TbC_IsEntityColumn":
                        return TbC_IsEntityColumn;

                    case "TbC_IsSystemField":
                        return TbC_IsSystemField;

                    case "TbC_IsValuesObjectMember":
                        return TbC_IsValuesObjectMember;

                    case "TbC_IsInPropsScreen":
                        return TbC_IsInPropsScreen;

                    case "TbC_IsInNavList":
                        return TbC_IsInNavList;

                    case "TbC_IsRequired":
                        return TbC_IsRequired;

                    case "TbC_BrokenRuleText":
                        return TbC_BrokenRuleText;

                    case "TbC_AllowZero":
                        return TbC_AllowZero;

                    case "TbC_IsNullableInDb":
                        return TbC_IsNullableInDb;

                    case "TbC_IsNullableInUI":
                        return TbC_IsNullableInUI;

                    case "TbC_DefaultValue":
                        return TbC_DefaultValue;

                    case "TbC_IsReadFromDb":
                        return TbC_IsReadFromDb;

                    case "TbC_IsSendToDb":
                        return TbC_IsSendToDb;

                    case "TbC_IsInsertAllowed":
                        return TbC_IsInsertAllowed;

                    case "TbC_IsEditAllowed":
                        return TbC_IsEditAllowed;

                    case "TbC_IsReadOnlyInUI":
                        return TbC_IsReadOnlyInUI;

                    case "TbC_UseForAudit":
                        return TbC_UseForAudit;

                    case "TbC_DefaultCaption":
                        return TbC_DefaultCaption;

                    case "TbC_ColumnHeaderText":
                        return TbC_ColumnHeaderText;

                    case "TbC_LabelCaptionVerbose":
                        return TbC_LabelCaptionVerbose;

                    case "TbC_LabelCaptionGenerate":
                        return TbC_LabelCaptionGenerate;

                    case "Tbc_ShowGridColumnToolTip":
                        return Tbc_ShowGridColumnToolTip;

                    case "Tbc_ShowPropsToolTip":
                        return Tbc_ShowPropsToolTip;

                    case "Tbc_TooltipsRolledUp":
                        return Tbc_TooltipsRolledUp;

                    case "TbC_IsCreatePropsStrings":
                        return TbC_IsCreatePropsStrings;

                    case "TbC_IsCreateGridStrings":
                        return TbC_IsCreateGridStrings;

                    case "TbC_IsAvailableForColumnGroups":
                        return TbC_IsAvailableForColumnGroups;

                    case "TbC_IsTextWrapInProp":
                        return TbC_IsTextWrapInProp;

                    case "TbC_IsTextWrapInGrid":
                        return TbC_IsTextWrapInGrid;

                    case "TbC_TextBoxFormat":
                        return TbC_TextBoxFormat;

                    case "TbC_TextColumnFormat":
                        return TbC_TextColumnFormat;

                    case "TbC_ColumnWidth":
                        return TbC_ColumnWidth;

                    case "TbC_TextBoxTextAlignment":
                        return TbC_TextBoxTextAlignment;

                    case "TbC_ColumnTextAlignment":
                        return TbC_ColumnTextAlignment;

                    case "TbC_ListStoredProc_Id":
                        return TbC_ListStoredProc_Id;

                    case "TbC_IsStaticList":
                        return TbC_IsStaticList;

                    case "TbC_UseNotInList":
                        return TbC_UseNotInList;

                    case "TbC_UseListEditBtn":
                        return TbC_UseListEditBtn;

                    case "TbC_ParentColumnKey":
                        return TbC_ParentColumnKey;

                    case "TbC_UseDisplayTextFieldProperty":
                        return TbC_UseDisplayTextFieldProperty;

                    case "TbC_IsDisplayTextFieldProperty":
                        return TbC_IsDisplayTextFieldProperty;

                    case "TbC_ComboListTable_Id":
                        return TbC_ComboListTable_Id;

                    case "TbC_ComboListDisplayColumn_Id":
                        return TbC_ComboListDisplayColumn_Id;

                    case "TbC_Combo_MaxDropdownHeight":
                        return TbC_Combo_MaxDropdownHeight;

                    case "TbC_Combo_MaxDropdownWidth":
                        return TbC_Combo_MaxDropdownWidth;

                    case "TbC_Combo_AllowDropdownResizing":
                        return TbC_Combo_AllowDropdownResizing;

                    case "TbC_Combo_IsResetButtonVisible":
                        return TbC_Combo_IsResetButtonVisible;

                    case "TbC_IsActiveRecColumn":
                        return TbC_IsActiveRecColumn;

                    case "TbC_IsDeletedColumn":
                        return TbC_IsDeletedColumn;

                    case "TbC_IsCreatedUserIdColumn":
                        return TbC_IsCreatedUserIdColumn;

                    case "TbC_IsCreatedDateColumn":
                        return TbC_IsCreatedDateColumn;

                    case "TbC_IsUserIdColumn":
                        return TbC_IsUserIdColumn;

                    case "TbC_IsModifiedDateColumn":
                        return TbC_IsModifiedDateColumn;

                    case "TbC_IsRowVersionStampColumn":
                        return TbC_IsRowVersionStampColumn;

                    case "TbC_IsBrowsable":
                        return TbC_IsBrowsable;

                    case "TbC_DeveloperNote":
                        return TbC_DeveloperNote;

                    case "TbC_UserNote":
                        return TbC_UserNote;

                    case "TbC_HelpFileAdditionalNote":
                        return TbC_HelpFileAdditionalNote;

                    case "TbC_Notes":
                        return TbC_Notes;

                    case "TbC_IsInputComplete":
                        return TbC_IsInputComplete;

                    case "TbC_IsCodeGen":
                        return TbC_IsCodeGen;

                    case "TbC_IsReadyCodeGen":
                        return TbC_IsReadyCodeGen;

                    case "TbC_IsCodeGenComplete":
                        return TbC_IsCodeGenComplete;

                    case "TbC_IsTagForCodeGen":
                        return TbC_IsTagForCodeGen;

                    case "TbC_IsTagForOther":
                        return TbC_IsTagForOther;

                    case "TbC_ColumnGroups":
                        return TbC_ColumnGroups;

                    case "TbC_IsActiveRow":
                        return TbC_IsActiveRow;

                    case "UserName":
                        return UserName;

                    case "TbC_LastModifiedDate":
                        return TbC_LastModifiedDate;

                    default:
                        return GetPropertyValueByKeyCustomCode(key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", IfxTraceCategory.Leave);
            }
        }

        public string GetPropertyFormattedStringValueByKey(string key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", new ValuePair[] {new ValuePair("key", key) }, IfxTraceCategory.Enter);
                switch (key)
                {
                    case "EntityId":
                        return TbC_Id.ToString();

                    case "TbC_SortOrder":
                        if (TbC_SortOrder == null)
                        {
                            return null;
                        }
                        else
                        {
                            return TbC_SortOrder.ToString();
                        }

                    case "DbSortOrder":
                        return DbSortOrder.ToString();

                    case "TbC_Name":
                        return TbC_Name;

                    case "ColumnInDatabase":
                        return ColumnInDatabase;

                    case "TbC_CtlTp_Id":
                        return TbC_CtlTp_Id_TextField;

                    case "TbC_IsNonvColumn":
                        return TbC_IsNonvColumn.ToString();

                    case "TbC_Description":
                        return TbC_Description;

                    case "TbC_DtSql_Id":
                        return TbC_DtSql_Id_TextField;

                    case "TbC_DtDtNt_Id":
                        return TbC_DtDtNt_Id_TextField;

                    case "TbC_Length":
                        if (TbC_Length == null)
                        {
                            return null;
                        }
                        else
                        {
                            return TbC_Length.ToString();
                        }

                    case "TbC_Precision":
                        if (TbC_Precision == null)
                        {
                            return null;
                        }
                        else
                        {
                            return TbC_Precision.ToString();
                        }

                    case "TbC_Scale":
                        if (TbC_Scale == null)
                        {
                            return null;
                        }
                        else
                        {
                            return TbC_Scale.ToString();
                        }

                    case "TbC_IsPK":
                        return TbC_IsPK.ToString();

                    case "TbC_IsIdentity":
                        return TbC_IsIdentity.ToString();

                    case "TbC_IsFK":
                        return TbC_IsFK.ToString();

                    case "TbC_IsEntityColumn":
                        return TbC_IsEntityColumn.ToString();

                    case "TbC_IsSystemField":
                        return TbC_IsSystemField.ToString();

                    case "TbC_IsValuesObjectMember":
                        return TbC_IsValuesObjectMember.ToString();

                    case "TbC_IsInPropsScreen":
                        return TbC_IsInPropsScreen.ToString();

                    case "TbC_IsInNavList":
                        return TbC_IsInNavList.ToString();

                    case "TbC_IsRequired":
                        return TbC_IsRequired.ToString();

                    case "TbC_BrokenRuleText":
                        return TbC_BrokenRuleText;

                    case "TbC_AllowZero":
                        return TbC_AllowZero.ToString();

                    case "TbC_IsNullableInDb":
                        return TbC_IsNullableInDb.ToString();

                    case "TbC_IsNullableInUI":
                        return TbC_IsNullableInUI.ToString();

                    case "TbC_DefaultValue":
                        return TbC_DefaultValue;

                    case "TbC_IsReadFromDb":
                        return TbC_IsReadFromDb.ToString();

                    case "TbC_IsSendToDb":
                        return TbC_IsSendToDb.ToString();

                    case "TbC_IsInsertAllowed":
                        return TbC_IsInsertAllowed.ToString();

                    case "TbC_IsEditAllowed":
                        return TbC_IsEditAllowed.ToString();

                    case "TbC_IsReadOnlyInUI":
                        return TbC_IsReadOnlyInUI.ToString();

                    case "TbC_UseForAudit":
                        return TbC_UseForAudit.ToString();

                    case "TbC_DefaultCaption":
                        return TbC_DefaultCaption;

                    case "TbC_ColumnHeaderText":
                        return TbC_ColumnHeaderText;

                    case "TbC_LabelCaptionVerbose":
                        return TbC_LabelCaptionVerbose;

                    case "TbC_LabelCaptionGenerate":
                        return TbC_LabelCaptionGenerate.ToString();

                    case "Tbc_ShowGridColumnToolTip":
                        return Tbc_ShowGridColumnToolTip.ToString();

                    case "Tbc_ShowPropsToolTip":
                        return Tbc_ShowPropsToolTip.ToString();

                    case "Tbc_TooltipsRolledUp":
                        return Tbc_TooltipsRolledUp;

                    case "TbC_IsCreatePropsStrings":
                        return TbC_IsCreatePropsStrings.ToString();

                    case "TbC_IsCreateGridStrings":
                        return TbC_IsCreateGridStrings.ToString();

                    case "TbC_IsAvailableForColumnGroups":
                        return TbC_IsAvailableForColumnGroups.ToString();

                    case "TbC_IsTextWrapInProp":
                        return TbC_IsTextWrapInProp.ToString();

                    case "TbC_IsTextWrapInGrid":
                        return TbC_IsTextWrapInGrid.ToString();

                    case "TbC_TextBoxFormat":
                        return TbC_TextBoxFormat;

                    case "TbC_TextColumnFormat":
                        return TbC_TextColumnFormat;

                    case "TbC_ColumnWidth":
                        return TbC_ColumnWidth.ToString();

                    case "TbC_TextBoxTextAlignment":
                        return TbC_TextBoxTextAlignment;

                    case "TbC_ColumnTextAlignment":
                        return TbC_ColumnTextAlignment;

                    case "TbC_ListStoredProc_Id":
                        return TbC_ListStoredProc_Id_TextField;

                    case "TbC_IsStaticList":
                        return TbC_IsStaticList.ToString();

                    case "TbC_UseNotInList":
                        return TbC_UseNotInList.ToString();

                    case "TbC_UseListEditBtn":
                        return TbC_UseListEditBtn.ToString();

                    case "TbC_ParentColumnKey":
                        return TbC_ParentColumnKey_TextField;

                    case "TbC_UseDisplayTextFieldProperty":
                        return TbC_UseDisplayTextFieldProperty.ToString();

                    case "TbC_IsDisplayTextFieldProperty":
                        return TbC_IsDisplayTextFieldProperty.ToString();

                    case "TbC_ComboListTable_Id":
                        return TbC_ComboListTable_Id_TextField;

                    case "TbC_ComboListDisplayColumn_Id":
                        return TbC_ComboListDisplayColumn_Id_TextField;

                    case "TbC_Combo_MaxDropdownHeight":
                        return TbC_Combo_MaxDropdownHeight.ToString();

                    case "TbC_Combo_MaxDropdownWidth":
                        return TbC_Combo_MaxDropdownWidth.ToString();

                    case "TbC_Combo_AllowDropdownResizing":
                        return TbC_Combo_AllowDropdownResizing.ToString();

                    case "TbC_Combo_IsResetButtonVisible":
                        return TbC_Combo_IsResetButtonVisible.ToString();

                    case "TbC_IsActiveRecColumn":
                        return TbC_IsActiveRecColumn.ToString();

                    case "TbC_IsDeletedColumn":
                        return TbC_IsDeletedColumn.ToString();

                    case "TbC_IsCreatedUserIdColumn":
                        return TbC_IsCreatedUserIdColumn.ToString();

                    case "TbC_IsCreatedDateColumn":
                        return TbC_IsCreatedDateColumn.ToString();

                    case "TbC_IsUserIdColumn":
                        return TbC_IsUserIdColumn.ToString();

                    case "TbC_IsModifiedDateColumn":
                        return TbC_IsModifiedDateColumn.ToString();

                    case "TbC_IsRowVersionStampColumn":
                        return TbC_IsRowVersionStampColumn.ToString();

                    case "TbC_IsBrowsable":
                        return TbC_IsBrowsable.ToString();

                    case "TbC_DeveloperNote":
                        return TbC_DeveloperNote;

                    case "TbC_UserNote":
                        return TbC_UserNote;

                    case "TbC_HelpFileAdditionalNote":
                        return TbC_HelpFileAdditionalNote;

                    case "TbC_Notes":
                        return TbC_Notes;

                    case "TbC_IsInputComplete":
                        return TbC_IsInputComplete.ToString();

                    case "TbC_IsCodeGen":
                        return TbC_IsCodeGen.ToString();

                    case "TbC_IsReadyCodeGen":
                        return TbC_IsReadyCodeGen.ToString();

                    case "TbC_IsCodeGenComplete":
                        return TbC_IsCodeGenComplete.ToString();

                    case "TbC_IsTagForCodeGen":
                        return TbC_IsTagForCodeGen.ToString();

                    case "TbC_IsTagForOther":
                        return TbC_IsTagForOther.ToString();

                    case "TbC_ColumnGroups":
                        return TbC_ColumnGroups;

                    case "TbC_IsActiveRow":
                        return TbC_IsActiveRow.ToString();

                    case "UserName":
                        return UserName;

                    case "TbC_LastModifiedDate":
                        if (TbC_LastModifiedDate == null)
                        {
                            return null;
                        }
                        else
                        {
                            return TbC_LastModifiedDate.ToString();
                        }

                    default:
                        return GetPropertyFormattedStringValueByKeyCustomCode(key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", IfxTraceCategory.Leave);
            }
        }

        
        #region General Properties

        /// <summary>
        ///     Returns a list of current BrokenRules for this entity as a list of <see cref="TypeServices.vRuleItem">vRuleItem</see> types.
        /// </summary>
        [Browsable(false)]
        public List<vRuleItem> GetBrokenRuleListForEntity()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", IfxTraceCategory.Enter);
                return BrokenRuleManagerProperty.GetBrokenRuleListForEntity();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", IfxTraceCategory.Leave);
            }
        }

        #endregion General Properties        

		#endregion General Methods and Properties


		#region Events


        /// <summary>
        ///     Raises the <see cref="CrudFailed">CrudFailed</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnCrudFailed(object sender, CrudFailedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Enter);
            CrudFailedEventHandler handler = CrudFailed;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Leave);
        }

        // THIS IS NOW OBSOLETE
        /// <summary>
        ///     Calls the <see cref="OnAsyncSaveComplete">OnAsyncSaveComplete</see> to raise the <see cref="AsyncSaveComplete">AsyncSaveComplete</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventAsyncSaveComplete(DataOperationResult result, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveComplete", IfxTraceCategory.Enter);
            AsyncSaveCompleteArgs e = new AsyncSaveCompleteArgs(result, failedReasonText);
            OnAsyncSaveComplete(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveComplete", IfxTraceCategory.Leave);
        }

        // THIS IS NOW OBSOLETE
        /// <summary>
        ///     Raises the <see cref="AsyncSaveComplete">AsyncSaveComplete</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnAsyncSaveComplete(object sender, AsyncSaveCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveComplete", IfxTraceCategory.Enter);
            AsyncSaveCompleteEventHandler handler = AsyncSaveComplete;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveComplete", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Calls the <see cref="OnAsyncSaveWithResponseComplete">OnAsyncSaveWithResponseComplete</see> to raise the <see cref="AsyncSaveWithResponseComplete">AsyncSaveWithResponseComplete</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventAsyncSaveWithResponseComplete(DataServiceInsertUpdateResponse response, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
            AsyncSaveWithResponseCompleteArgs e = new AsyncSaveWithResponseCompleteArgs(response, failedReasonText);
            OnAsyncSaveWithResponseComplete(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Raises the <see cref="AsyncSaveWithResponseComplete">AsyncSaveWithResponseComplete</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnAsyncSaveWithResponseComplete(object sender, AsyncSaveWithResponseCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
            AsyncSaveWithResponseCompleteEventHandler handler = AsyncSaveWithResponseComplete;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
        }










        void RaiseEventEntityRowReceived()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventEntityRowReceived", IfxTraceCategory.Enter);
            EntityRowReceivedEventHandler handler = EntityRowReceived;
            if (handler != null)
            {
                handler(this, new EntityRowReceivedArgs());
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventEntityRowReceived", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Calls the <see cref="OnCrudFailed">OnCrudFailed</see> to raise the <see cref="CrudFailed">CrudFailed</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventCrudFailed(int failureCode, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCrudFailed", IfxTraceCategory.Enter);
            CrudFailedArgs e = new CrudFailedArgs(failureCode, failedReasonText);
            OnCrudFailed(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCrudFailed", IfxTraceCategory.Leave);
        }


        /// <summary>
        ///     Calls the <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see>
        ///     method to raise the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event passing in a
        ///     reference of this business object for the ActiveBusinessObject parameter. As it
        ///     bubbles up to an Entity Properties control, that control will pass in a reference
        ///     of itself for the ActivePropertiesControl parameter. As it bubbles up to the Entity
        ///     Manager control, that control will pass in a reference of itself for the
        ///     ActiveEntityControl parameter. It should continue to bubble up to the top level
        ///     control. This notifies all controls along the about which controls are active and
        ///     the current state so they can always be configures accordingly. Now that the top
        ///     level control (perhaps the main application window) has a reference to these 3
        ///     important objects, it can easily communicate with then as the use interacts with
        ///     the application.
        /// </summary>        
        void RaiseEventCurrentEntityStateChanged(EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCurrentEntityStateChanged", new ValuePair[] {new ValuePair("state", state) }, IfxTraceCategory.Enter);
            CurrentEntityStateArgs e = new CurrentEntityStateArgs(state, null, null, this);
            OnCurrentEntityStateChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCurrentEntityStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     Raises the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see>
        ///     event passing in a reference of this business object for the ActiveBusinessObject
        ///     parameter. As it bubbles up to an Entity Properties control, that control will pass
        ///     in a reference of itself for the ActivePropertiesControl parameter. As it bubbles
        ///     up to the Entity Manager control, that control will pass in a reference of itself
        ///     for the ActiveEntityControl parameter. It should continue to bubble up to the top
        ///     level control. This notifies all controls along the about which controls are active
        ///     and the current state so they can always be configures accordingly. Now that the
        ///     top level control (perhaps the main application window) has a reference to these 3
        ///     important objects, it can easily communicate with then as the use interacts with
        ///     the application.
        /// </summary>
        void OnCurrentEntityStateChanged(object sender, CurrentEntityStateArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", new ValuePair[] {new ValuePair("e.State", e.State) }, IfxTraceCategory.Enter);
            CurrentEntityStateEventHandler handler = CurrentEntityStateChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     This method is obsolete and is replaced with <see cref="RaiseEventControlValidStateChanged">RaiseEventControlValidStateChanged</see> which
        ///     is called in the validation section of this class in the partial class
        ///     WcTableColumn.bll.vld.cs code file.
        /// </summary>
        void RaiseEventBrokenRuleChanged(string rule)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventBrokenRuleChanged", new ValuePair[] {new ValuePair("rule", rule) }, IfxTraceCategory.Enter);
            BrokenRuleArgs e = new BrokenRuleArgs(rule);
            OnBrokenRuleChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventBrokenRuleChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>This method is mostly obsolete, but remains because it’s an easy way to be
        ///     notified of the current broken rule.</para>
        /// 	<para>
        ///         This method is hooked to the <see cref="BrokenRuleChanged">BrokenRuleChanged</see> event which is initiated by
        ///         BrokenRuleManager and bubbles up to the business object and then to this
        ///         control (ucWcTableColumnProps) (or in some cases a ucEntityList control when it’s in
        ///         Read/Write mode).
        ///     </para>
        /// 	<para><br/>
        ///     For information on how Broken Rules are currently being managed, see (in the order
        ///     of execution):</para>
        /// 	<list type="bullet">
        /// 		<item>In the business object’s data property – FieldName_Validate();</item>
        /// 		<item>
        ///             In the FieldName_Validate method this line executes:<br/>
        ///             _brokenRuleManager.<see cref="TypeServices.BrokenRuleManager.AddBrokenRuleForProperty">AddBrokenRuleForProperty</see>("FieldName",
        ///             BROKENRULE_ FieldName _SomeRuleName);
        ///         </item>
        /// 		<item>
        /// 			<see cref="TypeServices.BrokenRuleManager.SetEntityValidState">BrokenRuleManager.SetEntityValidState()</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
       private void OnBrokenRuleChanged(object sender, BrokenRuleArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Enter);
            BrokenRuleEventHandler handler = BrokenRuleChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// Notifies an event handler that a property value as change. Currently not being
        /// uses.
        /// </summary>
        private void OnPropertyValueChanged(object sender, PropertyValueChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnPropertyValueChanged", new ValuePair[] {new ValuePair("e.PropertyName", e.PropertyName), new ValuePair("e.IsValid", e.IsValid), new ValuePair("e.BrokenRules", e.BrokenRules) }, IfxTraceCategory.Enter);
            PropertyValueChangedEventHandler handler = PropertyValueChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnPropertyValueChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     Replaces the RaiseEventBrokenRuleChanged method and used in the validation section
        ///     of this class in the partial class WcTableColumn.bll.vld.cs code file. Validation code will
        ///     pass in property name and an isValid flag. This will call the <see cref="OnControlValidStateChanged">OnControlValidStateChanged</see> method which will
        ///     raise the <see cref="ControlValidStateChanged">ControlValidStateChanged</see> event
        ///     up to the parent. The parent will then take the appropriate actions such as setting
        ///     the control’s valid/not valid appearance.
        /// </summary>
        void RaiseEventControlValidStateChanged(string propertyName, bool isValid, bool isDirty)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventControlValidStateChanged", new ValuePair[] {new ValuePair("isValid", isValid) }, IfxTraceCategory.Enter);
            ControlValidStateChangedArgs e = new ControlValidStateChangedArgs(propertyName, isValid, isDirty);
            OnControlValidStateChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventControlValidStateChanged", IfxTraceCategory.Leave);
        }       

        /// <summary>
        /// Raise the ControlValidStateChanged event up to the parent. The parent will then
        /// take the appropriate actions such as setting the control’s valid/not valid
        /// appearance.
        /// </summary>
        private void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", new ValuePair[] {new ValuePair("e.IsValid", e.IsValid) }, IfxTraceCategory.Enter);
            ControlValidStateChangedEventHandler handler = ControlValidStateChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableColumnProps.OnRestrictedTextLengthChanged">ucWcTableColumnProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableColumnProps.SetRestrictedStringLengthText">ucWcTableColumnProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void RaiseEventRestrictedTextLengthChanged()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Enter);
            RestrictedTextLengthChangedArgs e = new RestrictedTextLengthChangedArgs();
            RestrictedTextLengthChangedEventHandler handler = RestrictedTextLengthChanged;
            if (handler != null)
            {
                handler(this, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<param name="oldVal">The original property value.</param>
        /// 	<param name="newVal">The new property value.</param>        
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableColumnProps.OnRestrictedTextLengthChanged">ucWcTableColumnProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableColumnProps.SetRestrictedStringLengthText">ucWcTableColumnProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void RaiseEventRestrictedTextLengthChanged(string oldVal, string newVal)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Enter);
            bool lenChanged = false;
            if (oldVal == null && newVal != null)
            {
                if (newVal.Trim().Length > 0)
                {
                    lenChanged = true;
                }
            }
            else if (newVal == null && oldVal != null)
            {
                if (oldVal.Trim().Length > 0)
                {
                    lenChanged = true;
                }
            }
            else if (oldVal != null && newVal != null)
            {
                if (oldVal.Trim().Length != newVal.Trim().Length)
                {
                    lenChanged = true;
                }
            }
            if (lenChanged == true)
            {
                RestrictedTextLengthChangedArgs e = new RestrictedTextLengthChangedArgs();
                RestrictedTextLengthChangedEventHandler handler = RestrictedTextLengthChanged;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Leave);
        }        


		#endregion Events


		#region Flag Props


        /// <summary>
        ///     The name of the (text type – TextBox, TextBlock, etc.) control in the UI that
        ///     currently has the focus. This is used by the <see cref="ActiveRestrictedStringPropertyLength">ActiveRestrictedStringPropertyLength</see>
        ///     method to which returns the remaining available text length for the text control
        ///     with that name. It was decided to persist the name of the UI control in this
        ///     business object property rather than in the UI because it seemed to make sense to
        ///     have all of this logic centralized, and also because it simplifies things.
        /// </summary>
        [Browsable(false)]
        public string ActiveRestrictedStringProperty
        {
            get { return _activeRestrictedStringProperty; }
            set 
            { 
                _activeRestrictedStringProperty = value;
            }
        }

        /// <summary>
        ///     Returns the remaining available text length for the text control named by
        ///     <see cref="ActiveRestrictedStringProperty">ActiveRestrictedStringProperty</see>.
        /// </summary>
        [Browsable(false)]
        public int ActiveRestrictedStringPropertyLength
        {
            get
            {
                switch (_activeRestrictedStringProperty)
                {
                    case "TbC_Name":
                        if (_data.C.TbC_Name == null)
                        {
                            return STRINGSIZE_TbC_Name;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_Name - _data.C.TbC_Name.Length);
                        }
                        break;
                    
                    case "TbC_Description":
                        if (_data.C.TbC_Description == null)
                        {
                            return STRINGSIZE_TbC_Description;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_Description - _data.C.TbC_Description.Length);
                        }
                        break;
                    
                    case "TbC_BrokenRuleText":
                        if (_data.C.TbC_BrokenRuleText == null)
                        {
                            return STRINGSIZE_TbC_BrokenRuleText;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_BrokenRuleText - _data.C.TbC_BrokenRuleText.Length);
                        }
                        break;
                    
                    case "TbC_DefaultValue":
                        if (_data.C.TbC_DefaultValue == null)
                        {
                            return STRINGSIZE_TbC_DefaultValue;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_DefaultValue - _data.C.TbC_DefaultValue.Length);
                        }
                        break;
                    
                    case "TbC_DefaultCaption":
                        if (_data.C.TbC_DefaultCaption == null)
                        {
                            return STRINGSIZE_TbC_DefaultCaption;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_DefaultCaption - _data.C.TbC_DefaultCaption.Length);
                        }
                        break;
                    
                    case "TbC_ColumnHeaderText":
                        if (_data.C.TbC_ColumnHeaderText == null)
                        {
                            return STRINGSIZE_TbC_ColumnHeaderText;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_ColumnHeaderText - _data.C.TbC_ColumnHeaderText.Length);
                        }
                        break;
                    
                    case "TbC_LabelCaptionVerbose":
                        if (_data.C.TbC_LabelCaptionVerbose == null)
                        {
                            return STRINGSIZE_TbC_LabelCaptionVerbose;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_LabelCaptionVerbose - _data.C.TbC_LabelCaptionVerbose.Length);
                        }
                        break;
                    
                    case "TbC_TextBoxFormat":
                        if (_data.C.TbC_TextBoxFormat == null)
                        {
                            return STRINGSIZE_TbC_TextBoxFormat;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_TextBoxFormat - _data.C.TbC_TextBoxFormat.Length);
                        }
                        break;
                    
                    case "TbC_TextColumnFormat":
                        if (_data.C.TbC_TextColumnFormat == null)
                        {
                            return STRINGSIZE_TbC_TextColumnFormat;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_TextColumnFormat - _data.C.TbC_TextColumnFormat.Length);
                        }
                        break;
                    
                    case "TbC_TextBoxTextAlignment":
                        if (_data.C.TbC_TextBoxTextAlignment == null)
                        {
                            return STRINGSIZE_TbC_TextBoxTextAlignment;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_TextBoxTextAlignment - _data.C.TbC_TextBoxTextAlignment.Length);
                        }
                        break;
                    
                    case "TbC_ColumnTextAlignment":
                        if (_data.C.TbC_ColumnTextAlignment == null)
                        {
                            return STRINGSIZE_TbC_ColumnTextAlignment;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_ColumnTextAlignment - _data.C.TbC_ColumnTextAlignment.Length);
                        }
                        break;
                    
                    case "TbC_DeveloperNote":
                        if (_data.C.TbC_DeveloperNote == null)
                        {
                            return STRINGSIZE_TbC_DeveloperNote;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_DeveloperNote - _data.C.TbC_DeveloperNote.Length);
                        }
                        break;
                    
                    case "TbC_UserNote":
                        if (_data.C.TbC_UserNote == null)
                        {
                            return STRINGSIZE_TbC_UserNote;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_UserNote - _data.C.TbC_UserNote.Length);
                        }
                        break;
                    
                    case "TbC_HelpFileAdditionalNote":
                        if (_data.C.TbC_HelpFileAdditionalNote == null)
                        {
                            return STRINGSIZE_TbC_HelpFileAdditionalNote;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_HelpFileAdditionalNote - _data.C.TbC_HelpFileAdditionalNote.Length);
                        }
                        break;
                    
                    case "TbC_Notes":
                        if (_data.C.TbC_Notes == null)
                        {
                            return STRINGSIZE_TbC_Notes;
                        }
                        else
                        {
                            return (STRINGSIZE_TbC_Notes - _data.C.TbC_Notes.Length);
                        }
                        break;
                    }
                return 0;
            }
        }

 

		#endregion Flag Props


		#region Data Props


        /// <summary>
        ///     Called by data field properties in their getter block. This method will determine
        ///     if he <see cref="TypeServices.EntityState">state</see> has changed by the getter
        ///     being called. If it has changed, the <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see>
        ///     method will be called to notify the object using this business object.
        /// </summary>
       private void CheckEntityState(EntityStateSwitch es)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", new ValuePair[] {new ValuePair("_data.S.Switch", _data.S.Switch), new ValuePair("es", es) }, IfxTraceCategory.Enter);
                if (es != _data.S.Switch)
                {
                    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// This property does nothing and is used as a stub for a 'Rich Data Grid Column' binding to this entity
        /// </summary>
        public string RichGrid
        {
            get { return ""; }
        }



        public string DataRowName
        {
            get { return "No Name Selected"; }
        }




        private IBusinessObjectV2 _parentBusinessObject = null;

        public IBusinessObjectV2 ParentBusinessObject
        {
            get { return _parentBusinessObject; }
            set { _parentBusinessObject = value; }
        }

       public Guid? GuidPrimaryKey()
       {
           return _data.C.TbC_Id; 
       }

       public long? LongPrimaryKey()
       {
           throw new NotImplementedException("WcTableColumn_Bll LongPrimaryKey() Not Implemented"); 
       }

       public int? IntPrimaryKey()
       {
           throw new NotImplementedException("WcTableColumn_Bll LongPrimaryKey() Not Implemented"); 
       }

       public short? ShortPrimaryKey()
       {
           throw new NotImplementedException("WcTableColumn_Bll LongPrimaryKey() Not Implemented"); 
       }

       public byte? BytePrimaryKey()
       {
           throw new NotImplementedException("WcTableColumn_Bll LongPrimaryKey() Not Implemented"); 
       }

       public object ObjectPrimaryKey()
       {
           return _data.C.TbC_Id;
       }


        /// <summary>
        /// This is the Standing Foreign Key property. This value remains constant when
        /// calling the new method where all data fields are cleared and set to their ‘new’ default
        /// values. This allows creating new entities for the same parent. When an entity is
        /// fetched from the data store and used as the current entity in this business object, the
        /// Standing Foreign Key value is reset using the value from the fetched entity.
        /// </summary>
        public Guid StandingFK
        {
            get { return _StandingFK; }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    _StandingFK = value;
                    _data.C.TbC_Tb_Id_noevents = _StandingFK;
                    _data.O.TbC_Tb_Id_noevents = _StandingFK;
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", IfxTraceCategory.Leave);
                }
            }
        }

       /// <summary>
       /// 	<para>The Primary Key for WcTableColumn</para>
       /// </summary>
        public Guid TbC_Id
        {
            get
            {
                return _data.C.TbC_Id;
            }
        }

		
        public Guid? TbC_Tb_Id
        {
            get
            {
                return _data.C.TbC_Tb_Id;
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="TbC_auditColumn_Id">TbC_auditColumn_Id</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the TbC_auditColumn_Id field. If it’s not numeric, then TbC_auditColumn_Id is set to
        ///     null.</para>
        /// </summary>
        public String TbC_auditColumn_Id_asString
        {
            get
            {
                if (null == _data.C.TbC_auditColumn_Id)
                {
                    return "";
                }
                else
                {
                    return _data.C.TbC_auditColumn_Id.ToString();
                }
            }
        }

		
        public Guid? TbC_auditColumn_Id
        {
            get
            {
                return _data.C.TbC_auditColumn_Id;
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="TbC_SortOrder">TbC_SortOrder</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the TbC_SortOrder field. If it’s not numeric, then TbC_SortOrder is set to
        ///     null.</para>
        /// </summary>
        public String TbC_SortOrder_asString
        {
            get
            {
                if (null == _data.C.TbC_SortOrder)
                {
                    return "";
                }
                else
                {
                    return _data.C.TbC_SortOrder.ToString();
                }
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_SortOrder - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
					if (BLLHelper.IsNumber(value))
                    {
                        try
                        {
							Double newValNotNull;
                            Double.TryParse(Convert.ToString(value), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.CurrentInfo, out newValNotNull);
                            TbC_SortOrder = (Int16)newValNotNull;
                        }
                        catch (FormatException e)
                        {
                            TbC_SortOrder = null;
                        }
                    }
                    else
                    {
                        TbC_SortOrder = null;
                    }
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_SortOrder - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_SortOrder - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Int16? TbC_SortOrder
        {
            get
            {
                return _data.C.TbC_SortOrder;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_SortOrder - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_SortOrder == value) { return; }
                    _data.C.TbC_SortOrder = value;
                    CustomPropertySetterCode("TbC_SortOrder");
                    TbC_SortOrder_Validate();
                    CheckEntityState(es);
                    Notify("TbC_SortOrder");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_SortOrder - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_SortOrder - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public Int32 DbSortOrder
        {
            get
            {
                return _data.C.DbSortOrder;
            }
        }


        public Boolean IsImported
        {
            get
            {
                return _data.C.IsImported;
            }
        }

		
        public String TbC_Name
        {
            get
            {
                return _data.C.TbC_Name;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Name - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_Name == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_Name == null || _data.C.TbC_Name.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_Name == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_Name = null;
                    }
                    else
                    {
                        _data.C.TbC_Name = value.Trim();
                    }
                    //else if ((_data.C.TbC_Name == null || _data.C.TbC_Name.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_Name == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_Name == null || _data.C.TbC_Name.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_Name = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_Name = value;
                    //}
                    CustomPropertySetterCode("TbC_Name");
                    TbC_Name_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_Name, _data.C.TbC_Name);
                        
					Notify("TbC_Name");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Name - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Name - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public String ColumnInDatabase
        {
            get
            {
                return _data.C.ColumnInDatabase;
            }
        }


        public String TbC_CtlTp_Id_TextField
        {
            get
            {
                return _data.C.TbC_CtlTp_Id_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_CtlTp_Id Setter", IfxTraceCategory.Enter);
                    if (_data.C.TbC_CtlTp_Id_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.TbC_CtlTp_Id_TextField == null || _data.C.TbC_CtlTp_Id_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.TbC_CtlTp_Id_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.TbC_CtlTp_Id_TextField = value;
                    }
                    CustomPropertySetterCode("TbC_CtlTp_Id_TextField");
                    Notify("TbC_CtlTp_Id_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_CtlTp_Id Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_CtlTp_Id Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Int32? TbC_CtlTp_Id
        {
            get
            {
                return _data.C.TbC_CtlTp_Id;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_CtlTp_Id - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_CtlTp_Id == value) { return; }
                    _data.C.TbC_CtlTp_Id = value;
                    CustomPropertySetterCode("TbC_CtlTp_Id");
                    TbC_CtlTp_Id_Validate();
                    CheckEntityState(es);
                    Notify("TbC_CtlTp_Id");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = CommonClientData_Bll_staticLists.WcControlType_ComboItemList_BindingListProperty.FindItemById(_data.C.TbC_CtlTp_Id);
                    if (obj == null)
                    {
                        TbC_CtlTp_Id_TextField = "";
                    }
                    else
                    {
                        TbC_CtlTp_Id_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_CtlTp_Id - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_CtlTp_Id - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsNonvColumn
        {
            get
            {
                return _data.C.TbC_IsNonvColumn;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNonvColumn - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsNonvColumn == value) { return; }
                    _data.C.TbC_IsNonvColumn = value;
                    CustomPropertySetterCode("TbC_IsNonvColumn");
                    TbC_IsNonvColumn_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsNonvColumn");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNonvColumn - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNonvColumn - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_Description
        {
            get
            {
                return _data.C.TbC_Description;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Description - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_Description == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_Description == null || _data.C.TbC_Description.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_Description == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_Description = null;
                    }
                    else
                    {
                        _data.C.TbC_Description = value.Trim();
                    }
                    //else if ((_data.C.TbC_Description == null || _data.C.TbC_Description.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_Description == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_Description == null || _data.C.TbC_Description.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_Description = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_Description = value;
                    //}
                    CustomPropertySetterCode("TbC_Description");
                    TbC_Description_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_Description, _data.C.TbC_Description);
                        
					Notify("TbC_Description");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Description - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Description - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public String TbC_DtSql_Id_TextField
        {
            get
            {
                return _data.C.TbC_DtSql_Id_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtSql_Id Setter", IfxTraceCategory.Enter);
                    if (_data.C.TbC_DtSql_Id_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.TbC_DtSql_Id_TextField == null || _data.C.TbC_DtSql_Id_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.TbC_DtSql_Id_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.TbC_DtSql_Id_TextField = value;
                    }
                    CustomPropertySetterCode("TbC_DtSql_Id_TextField");
                    Notify("TbC_DtSql_Id_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtSql_Id Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtSql_Id Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Int32? TbC_DtSql_Id
        {
            get
            {
                return _data.C.TbC_DtSql_Id;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtSql_Id - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_DtSql_Id == value) { return; }
                    _data.C.TbC_DtSql_Id = value;
                    CustomPropertySetterCode("TbC_DtSql_Id");
                    TbC_DtSql_Id_Validate();
                    CheckEntityState(es);
                    Notify("TbC_DtSql_Id");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty.FindItemById(_data.C.TbC_DtSql_Id);
                    if (obj == null)
                    {
                        TbC_DtSql_Id_TextField = "";
                    }
                    else
                    {
                        TbC_DtSql_Id_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtSql_Id - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtSql_Id - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public String TbC_DtDtNt_Id_TextField
        {
            get
            {
                return _data.C.TbC_DtDtNt_Id_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtDtNt_Id Setter", IfxTraceCategory.Enter);
                    if (_data.C.TbC_DtDtNt_Id_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.TbC_DtDtNt_Id_TextField == null || _data.C.TbC_DtDtNt_Id_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.TbC_DtDtNt_Id_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.TbC_DtDtNt_Id_TextField = value;
                    }
                    CustomPropertySetterCode("TbC_DtDtNt_Id_TextField");
                    Notify("TbC_DtDtNt_Id_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtDtNt_Id Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtDtNt_Id Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Int32? TbC_DtDtNt_Id
        {
            get
            {
                return _data.C.TbC_DtDtNt_Id;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtDtNt_Id - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_DtDtNt_Id == value) { return; }
                    _data.C.TbC_DtDtNt_Id = value;
                    CustomPropertySetterCode("TbC_DtDtNt_Id");
                    TbC_DtDtNt_Id_Validate();
                    CheckEntityState(es);
                    Notify("TbC_DtDtNt_Id");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty.FindItemById(_data.C.TbC_DtDtNt_Id);
                    if (obj == null)
                    {
                        TbC_DtDtNt_Id_TextField = "";
                    }
                    else
                    {
                        TbC_DtDtNt_Id_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtDtNt_Id - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtDtNt_Id - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="TbC_Length">TbC_Length</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the TbC_Length field. If it’s not numeric, then TbC_Length is set to
        ///     null.</para>
        /// </summary>
        public String TbC_Length_asString
        {
            get
            {
                if (null == _data.C.TbC_Length)
                {
                    return "";
                }
                else
                {
                    return _data.C.TbC_Length.ToString();
                }
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Length - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
					if (BLLHelper.IsNumber(value))
                    {
                        try
                        {
							Double newValNotNull;
                            Double.TryParse(Convert.ToString(value), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.CurrentInfo, out newValNotNull);
                            TbC_Length = (Int32)newValNotNull;
                        }
                        catch (FormatException e)
                        {
                            TbC_Length = null;
                        }
                    }
                    else
                    {
                        TbC_Length = null;
                    }
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Length - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Length - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Int32? TbC_Length
        {
            get
            {
                return _data.C.TbC_Length;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Length - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_Length == value) { return; }
                    _data.C.TbC_Length = value;
                    CustomPropertySetterCode("TbC_Length");
                    TbC_Length_Validate();
                    CheckEntityState(es);
                    Notify("TbC_Length");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Length - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Length - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="TbC_Precision">TbC_Precision</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the TbC_Precision field. If it’s not numeric, then TbC_Precision is set to
        ///     null.</para>
        /// </summary>
        public String TbC_Precision_asString
        {
            get
            {
                if (null == _data.C.TbC_Precision)
                {
                    return "";
                }
                else
                {
                    return _data.C.TbC_Precision.ToString();
                }
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Precision - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
					if (BLLHelper.IsNumber(value))
                    {
                        try
                        {
							Double newValNotNull;
                            Double.TryParse(Convert.ToString(value), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.CurrentInfo, out newValNotNull);
                            TbC_Precision = (Int32)newValNotNull;
                        }
                        catch (FormatException e)
                        {
                            TbC_Precision = null;
                        }
                    }
                    else
                    {
                        TbC_Precision = null;
                    }
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Precision - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Precision - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Int32? TbC_Precision
        {
            get
            {
                return _data.C.TbC_Precision;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Precision - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_Precision == value) { return; }
                    _data.C.TbC_Precision = value;
                    CustomPropertySetterCode("TbC_Precision");
                    TbC_Precision_Validate();
                    CheckEntityState(es);
                    Notify("TbC_Precision");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Precision - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Precision - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="TbC_Scale">TbC_Scale</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the TbC_Scale field. If it’s not numeric, then TbC_Scale is set to
        ///     null.</para>
        /// </summary>
        public String TbC_Scale_asString
        {
            get
            {
                if (null == _data.C.TbC_Scale)
                {
                    return "";
                }
                else
                {
                    return _data.C.TbC_Scale.ToString();
                }
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Scale - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
					if (BLLHelper.IsNumber(value))
                    {
                        try
                        {
							Double newValNotNull;
                            Double.TryParse(Convert.ToString(value), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.CurrentInfo, out newValNotNull);
                            TbC_Scale = (Int32)newValNotNull;
                        }
                        catch (FormatException e)
                        {
                            TbC_Scale = null;
                        }
                    }
                    else
                    {
                        TbC_Scale = null;
                    }
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Scale - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Scale - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Int32? TbC_Scale
        {
            get
            {
                return _data.C.TbC_Scale;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Scale - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_Scale == value) { return; }
                    _data.C.TbC_Scale = value;
                    CustomPropertySetterCode("TbC_Scale");
                    TbC_Scale_Validate();
                    CheckEntityState(es);
                    Notify("TbC_Scale");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Scale - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Scale - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsPK
        {
            get
            {
                return _data.C.TbC_IsPK;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsPK - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsPK == value) { return; }
                    _data.C.TbC_IsPK = value;
                    CustomPropertySetterCode("TbC_IsPK");
                    TbC_IsPK_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsPK");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsPK - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsPK - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsIdentity
        {
            get
            {
                return _data.C.TbC_IsIdentity;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsIdentity - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsIdentity == value) { return; }
                    _data.C.TbC_IsIdentity = value;
                    CustomPropertySetterCode("TbC_IsIdentity");
                    TbC_IsIdentity_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsIdentity");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsIdentity - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsIdentity - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsFK
        {
            get
            {
                return _data.C.TbC_IsFK;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsFK - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsFK == value) { return; }
                    _data.C.TbC_IsFK = value;
                    CustomPropertySetterCode("TbC_IsFK");
                    TbC_IsFK_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsFK");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsFK - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsFK - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsEntityColumn
        {
            get
            {
                return _data.C.TbC_IsEntityColumn;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsEntityColumn - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsEntityColumn == value) { return; }
                    _data.C.TbC_IsEntityColumn = value;
                    CustomPropertySetterCode("TbC_IsEntityColumn");
                    TbC_IsEntityColumn_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsEntityColumn");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsEntityColumn - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsEntityColumn - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsSystemField
        {
            get
            {
                return _data.C.TbC_IsSystemField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsSystemField - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsSystemField == value) { return; }
                    _data.C.TbC_IsSystemField = value;
                    CustomPropertySetterCode("TbC_IsSystemField");
                    TbC_IsSystemField_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsSystemField");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsSystemField - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsSystemField - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsValuesObjectMember
        {
            get
            {
                return _data.C.TbC_IsValuesObjectMember;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsValuesObjectMember - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsValuesObjectMember == value) { return; }
                    _data.C.TbC_IsValuesObjectMember = value;
                    CustomPropertySetterCode("TbC_IsValuesObjectMember");
                    TbC_IsValuesObjectMember_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsValuesObjectMember");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsValuesObjectMember - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsValuesObjectMember - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsInPropsScreen
        {
            get
            {
                return _data.C.TbC_IsInPropsScreen;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInPropsScreen - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsInPropsScreen == value) { return; }
                    _data.C.TbC_IsInPropsScreen = value;
                    CustomPropertySetterCode("TbC_IsInPropsScreen");
                    TbC_IsInPropsScreen_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsInPropsScreen");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInPropsScreen - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInPropsScreen - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsInNavList
        {
            get
            {
                return _data.C.TbC_IsInNavList;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInNavList - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsInNavList == value) { return; }
                    _data.C.TbC_IsInNavList = value;
                    CustomPropertySetterCode("TbC_IsInNavList");
                    TbC_IsInNavList_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsInNavList");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInNavList - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInNavList - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsRequired
        {
            get
            {
                return _data.C.TbC_IsRequired;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsRequired - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsRequired == value) { return; }
                    _data.C.TbC_IsRequired = value;
                    CustomPropertySetterCode("TbC_IsRequired");
                    TbC_IsRequired_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsRequired");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsRequired - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsRequired - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_BrokenRuleText
        {
            get
            {
                return _data.C.TbC_BrokenRuleText;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_BrokenRuleText - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_BrokenRuleText == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_BrokenRuleText == null || _data.C.TbC_BrokenRuleText.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_BrokenRuleText == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_BrokenRuleText = null;
                    }
                    else
                    {
                        _data.C.TbC_BrokenRuleText = value.Trim();
                    }
                    //else if ((_data.C.TbC_BrokenRuleText == null || _data.C.TbC_BrokenRuleText.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_BrokenRuleText == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_BrokenRuleText == null || _data.C.TbC_BrokenRuleText.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_BrokenRuleText = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_BrokenRuleText = value;
                    //}
                    CustomPropertySetterCode("TbC_BrokenRuleText");
                    TbC_BrokenRuleText_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_BrokenRuleText, _data.C.TbC_BrokenRuleText);
                        
					Notify("TbC_BrokenRuleText");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_BrokenRuleText - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_BrokenRuleText - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_AllowZero
        {
            get
            {
                return _data.C.TbC_AllowZero;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_AllowZero - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_AllowZero == value) { return; }
                    _data.C.TbC_AllowZero = value;
                    CustomPropertySetterCode("TbC_AllowZero");
                    TbC_AllowZero_Validate();
                    CheckEntityState(es);
                    Notify("TbC_AllowZero");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_AllowZero - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_AllowZero - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsNullableInDb
        {
            get
            {
                return _data.C.TbC_IsNullableInDb;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNullableInDb - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsNullableInDb == value) { return; }
                    _data.C.TbC_IsNullableInDb = value;
                    CustomPropertySetterCode("TbC_IsNullableInDb");
                    TbC_IsNullableInDb_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsNullableInDb");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNullableInDb - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNullableInDb - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsNullableInUI
        {
            get
            {
                return _data.C.TbC_IsNullableInUI;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNullableInUI - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsNullableInUI == value) { return; }
                    _data.C.TbC_IsNullableInUI = value;
                    CustomPropertySetterCode("TbC_IsNullableInUI");
                    TbC_IsNullableInUI_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsNullableInUI");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNullableInUI - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNullableInUI - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_DefaultValue
        {
            get
            {
                return _data.C.TbC_DefaultValue;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DefaultValue - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_DefaultValue == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_DefaultValue == null || _data.C.TbC_DefaultValue.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_DefaultValue == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_DefaultValue = null;
                    }
                    else
                    {
                        _data.C.TbC_DefaultValue = value.Trim();
                    }
                    //else if ((_data.C.TbC_DefaultValue == null || _data.C.TbC_DefaultValue.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_DefaultValue == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_DefaultValue == null || _data.C.TbC_DefaultValue.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_DefaultValue = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_DefaultValue = value;
                    //}
                    CustomPropertySetterCode("TbC_DefaultValue");
                    TbC_DefaultValue_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_DefaultValue, _data.C.TbC_DefaultValue);
                        
					Notify("TbC_DefaultValue");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DefaultValue - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DefaultValue - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsReadFromDb
        {
            get
            {
                return _data.C.TbC_IsReadFromDb;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadFromDb - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsReadFromDb == value) { return; }
                    _data.C.TbC_IsReadFromDb = value;
                    CustomPropertySetterCode("TbC_IsReadFromDb");
                    TbC_IsReadFromDb_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsReadFromDb");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadFromDb - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadFromDb - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsSendToDb
        {
            get
            {
                return _data.C.TbC_IsSendToDb;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsSendToDb - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsSendToDb == value) { return; }
                    _data.C.TbC_IsSendToDb = value;
                    CustomPropertySetterCode("TbC_IsSendToDb");
                    TbC_IsSendToDb_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsSendToDb");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsSendToDb - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsSendToDb - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsInsertAllowed
        {
            get
            {
                return _data.C.TbC_IsInsertAllowed;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInsertAllowed - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsInsertAllowed == value) { return; }
                    _data.C.TbC_IsInsertAllowed = value;
                    CustomPropertySetterCode("TbC_IsInsertAllowed");
                    TbC_IsInsertAllowed_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsInsertAllowed");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInsertAllowed - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInsertAllowed - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsEditAllowed
        {
            get
            {
                return _data.C.TbC_IsEditAllowed;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsEditAllowed - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsEditAllowed == value) { return; }
                    _data.C.TbC_IsEditAllowed = value;
                    CustomPropertySetterCode("TbC_IsEditAllowed");
                    TbC_IsEditAllowed_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsEditAllowed");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsEditAllowed - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsEditAllowed - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsReadOnlyInUI
        {
            get
            {
                return _data.C.TbC_IsReadOnlyInUI;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadOnlyInUI - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsReadOnlyInUI == value) { return; }
                    _data.C.TbC_IsReadOnlyInUI = value;
                    CustomPropertySetterCode("TbC_IsReadOnlyInUI");
                    TbC_IsReadOnlyInUI_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsReadOnlyInUI");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadOnlyInUI - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadOnlyInUI - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_UseForAudit
        {
            get
            {
                return _data.C.TbC_UseForAudit;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseForAudit - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_UseForAudit == value) { return; }
                    _data.C.TbC_UseForAudit = value;
                    CustomPropertySetterCode("TbC_UseForAudit");
                    TbC_UseForAudit_Validate();
                    CheckEntityState(es);
                    Notify("TbC_UseForAudit");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseForAudit - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseForAudit - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_DefaultCaption
        {
            get
            {
                return _data.C.TbC_DefaultCaption;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DefaultCaption - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_DefaultCaption == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_DefaultCaption == null || _data.C.TbC_DefaultCaption.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_DefaultCaption == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_DefaultCaption = null;
                    }
                    else
                    {
                        _data.C.TbC_DefaultCaption = value.Trim();
                    }
                    //else if ((_data.C.TbC_DefaultCaption == null || _data.C.TbC_DefaultCaption.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_DefaultCaption == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_DefaultCaption == null || _data.C.TbC_DefaultCaption.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_DefaultCaption = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_DefaultCaption = value;
                    //}
                    CustomPropertySetterCode("TbC_DefaultCaption");
                    TbC_DefaultCaption_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_DefaultCaption, _data.C.TbC_DefaultCaption);
                        
					Notify("TbC_DefaultCaption");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DefaultCaption - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DefaultCaption - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_ColumnHeaderText
        {
            get
            {
                return _data.C.TbC_ColumnHeaderText;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnHeaderText - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_ColumnHeaderText == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_ColumnHeaderText == null || _data.C.TbC_ColumnHeaderText.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_ColumnHeaderText == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_ColumnHeaderText = null;
                    }
                    else
                    {
                        _data.C.TbC_ColumnHeaderText = value.Trim();
                    }
                    //else if ((_data.C.TbC_ColumnHeaderText == null || _data.C.TbC_ColumnHeaderText.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_ColumnHeaderText == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_ColumnHeaderText == null || _data.C.TbC_ColumnHeaderText.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_ColumnHeaderText = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_ColumnHeaderText = value;
                    //}
                    CustomPropertySetterCode("TbC_ColumnHeaderText");
                    TbC_ColumnHeaderText_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_ColumnHeaderText, _data.C.TbC_ColumnHeaderText);
                        
					Notify("TbC_ColumnHeaderText");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnHeaderText - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnHeaderText - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_LabelCaptionVerbose
        {
            get
            {
                return _data.C.TbC_LabelCaptionVerbose;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_LabelCaptionVerbose - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_LabelCaptionVerbose == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_LabelCaptionVerbose == null || _data.C.TbC_LabelCaptionVerbose.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_LabelCaptionVerbose == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_LabelCaptionVerbose = null;
                    }
                    else
                    {
                        _data.C.TbC_LabelCaptionVerbose = value.Trim();
                    }
                    //else if ((_data.C.TbC_LabelCaptionVerbose == null || _data.C.TbC_LabelCaptionVerbose.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_LabelCaptionVerbose == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_LabelCaptionVerbose == null || _data.C.TbC_LabelCaptionVerbose.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_LabelCaptionVerbose = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_LabelCaptionVerbose = value;
                    //}
                    CustomPropertySetterCode("TbC_LabelCaptionVerbose");
                    TbC_LabelCaptionVerbose_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_LabelCaptionVerbose, _data.C.TbC_LabelCaptionVerbose);
                        
					Notify("TbC_LabelCaptionVerbose");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_LabelCaptionVerbose - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_LabelCaptionVerbose - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_LabelCaptionGenerate
        {
            get
            {
                return _data.C.TbC_LabelCaptionGenerate;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_LabelCaptionGenerate - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_LabelCaptionGenerate == value) { return; }
                    _data.C.TbC_LabelCaptionGenerate = value;
                    CustomPropertySetterCode("TbC_LabelCaptionGenerate");
                    TbC_LabelCaptionGenerate_Validate();
                    CheckEntityState(es);
                    Notify("TbC_LabelCaptionGenerate");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_LabelCaptionGenerate - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_LabelCaptionGenerate - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tbc_ShowGridColumnToolTip
        {
            get
            {
                return _data.C.Tbc_ShowGridColumnToolTip;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_ShowGridColumnToolTip - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tbc_ShowGridColumnToolTip == value) { return; }
                    _data.C.Tbc_ShowGridColumnToolTip = value;
                    CustomPropertySetterCode("Tbc_ShowGridColumnToolTip");
                    Tbc_ShowGridColumnToolTip_Validate();
                    CheckEntityState(es);
                    Notify("Tbc_ShowGridColumnToolTip");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_ShowGridColumnToolTip - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_ShowGridColumnToolTip - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tbc_ShowPropsToolTip
        {
            get
            {
                return _data.C.Tbc_ShowPropsToolTip;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_ShowPropsToolTip - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tbc_ShowPropsToolTip == value) { return; }
                    _data.C.Tbc_ShowPropsToolTip = value;
                    CustomPropertySetterCode("Tbc_ShowPropsToolTip");
                    Tbc_ShowPropsToolTip_Validate();
                    CheckEntityState(es);
                    Notify("Tbc_ShowPropsToolTip");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_ShowPropsToolTip - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_ShowPropsToolTip - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tbc_TooltipsRolledUp
        {
            get
            {
                return _data.C.Tbc_TooltipsRolledUp;
            }
        }

		
        public Boolean TbC_IsCreatePropsStrings
        {
            get
            {
                return _data.C.TbC_IsCreatePropsStrings;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatePropsStrings - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsCreatePropsStrings == value) { return; }
                    _data.C.TbC_IsCreatePropsStrings = value;
                    CustomPropertySetterCode("TbC_IsCreatePropsStrings");
                    TbC_IsCreatePropsStrings_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsCreatePropsStrings");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatePropsStrings - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatePropsStrings - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsCreateGridStrings
        {
            get
            {
                return _data.C.TbC_IsCreateGridStrings;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreateGridStrings - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsCreateGridStrings == value) { return; }
                    _data.C.TbC_IsCreateGridStrings = value;
                    CustomPropertySetterCode("TbC_IsCreateGridStrings");
                    TbC_IsCreateGridStrings_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsCreateGridStrings");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreateGridStrings - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreateGridStrings - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsAvailableForColumnGroups
        {
            get
            {
                return _data.C.TbC_IsAvailableForColumnGroups;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsAvailableForColumnGroups - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsAvailableForColumnGroups == value) { return; }
                    _data.C.TbC_IsAvailableForColumnGroups = value;
                    CustomPropertySetterCode("TbC_IsAvailableForColumnGroups");
                    TbC_IsAvailableForColumnGroups_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsAvailableForColumnGroups");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsAvailableForColumnGroups - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsAvailableForColumnGroups - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsTextWrapInProp
        {
            get
            {
                return _data.C.TbC_IsTextWrapInProp;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTextWrapInProp - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsTextWrapInProp == value) { return; }
                    _data.C.TbC_IsTextWrapInProp = value;
                    CustomPropertySetterCode("TbC_IsTextWrapInProp");
                    TbC_IsTextWrapInProp_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsTextWrapInProp");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTextWrapInProp - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTextWrapInProp - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsTextWrapInGrid
        {
            get
            {
                return _data.C.TbC_IsTextWrapInGrid;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTextWrapInGrid - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsTextWrapInGrid == value) { return; }
                    _data.C.TbC_IsTextWrapInGrid = value;
                    CustomPropertySetterCode("TbC_IsTextWrapInGrid");
                    TbC_IsTextWrapInGrid_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsTextWrapInGrid");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTextWrapInGrid - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTextWrapInGrid - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_TextBoxFormat
        {
            get
            {
                return _data.C.TbC_TextBoxFormat;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextBoxFormat - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_TextBoxFormat == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_TextBoxFormat == null || _data.C.TbC_TextBoxFormat.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_TextBoxFormat == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_TextBoxFormat = null;
                    }
                    else
                    {
                        _data.C.TbC_TextBoxFormat = value.Trim();
                    }
                    //else if ((_data.C.TbC_TextBoxFormat == null || _data.C.TbC_TextBoxFormat.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_TextBoxFormat == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_TextBoxFormat == null || _data.C.TbC_TextBoxFormat.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_TextBoxFormat = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_TextBoxFormat = value;
                    //}
                    CustomPropertySetterCode("TbC_TextBoxFormat");
                    TbC_TextBoxFormat_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_TextBoxFormat, _data.C.TbC_TextBoxFormat);
                        
					Notify("TbC_TextBoxFormat");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextBoxFormat - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextBoxFormat - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_TextColumnFormat
        {
            get
            {
                return _data.C.TbC_TextColumnFormat;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextColumnFormat - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_TextColumnFormat == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_TextColumnFormat == null || _data.C.TbC_TextColumnFormat.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_TextColumnFormat == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_TextColumnFormat = null;
                    }
                    else
                    {
                        _data.C.TbC_TextColumnFormat = value.Trim();
                    }
                    //else if ((_data.C.TbC_TextColumnFormat == null || _data.C.TbC_TextColumnFormat.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_TextColumnFormat == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_TextColumnFormat == null || _data.C.TbC_TextColumnFormat.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_TextColumnFormat = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_TextColumnFormat = value;
                    //}
                    CustomPropertySetterCode("TbC_TextColumnFormat");
                    TbC_TextColumnFormat_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_TextColumnFormat, _data.C.TbC_TextColumnFormat);
                        
					Notify("TbC_TextColumnFormat");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextColumnFormat - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextColumnFormat - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="TbC_ColumnWidth">TbC_ColumnWidth</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the TbC_ColumnWidth field. If it’s not numeric, then TbC_ColumnWidth is set to
        ///     null.</para>
        /// </summary>
        public String TbC_ColumnWidth_asString
        {
            get
            {
                if (null == _data.C.TbC_ColumnWidth)
                {
                    return "";
                }
                else
                {
                    return _data.C.TbC_ColumnWidth.ToString();
                }
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnWidth - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    string _tbC_ColumnWidth_OriginalValue = "";
                    Double? newVal = null;
                    if (value != null)
                    {
                        if (value.Trim() == ".")
                        {
                            _tbC_ColumnWidth_OriginalValue = ".";
                        }
                        else if (value.Trim() == ",")
                        {
                            _tbC_ColumnWidth_OriginalValue = ",";
                        }

                        else if (BLLHelper.IsNumber(value))
                        {
                            try
                            {
                                _tbC_ColumnWidth_OriginalValue = value;
                                Double newValNotNull;
                                Double.TryParse(Convert.ToString(value), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.CurrentInfo, out newValNotNull);
                                newVal = newValNotNull;
                            }
                            catch (FormatException e)
                            {
                            }
                        }
                    }

                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_ColumnWidth == newVal) { return; }
                    _data.C.TbC_ColumnWidth = newVal;
                    CustomPropertySetterCode("TbC_ColumnWidth");
                    TbC_ColumnWidth_Validate();
                    CheckEntityState(es);

                    //  allow zero, therefore, notify UI if user input zero
                    if (_tbC_ColumnWidth_OriginalValue != "." && _tbC_ColumnWidth_OriginalValue != ",")
                    {
                        Notify("TbC_ColumnWidth_asString");
                    }
 
                //if (newVal !=0 && _tbC_ColumnWidth_OriginalValue != ".")
                //{
                //    Notify("TbC_ColumnWidth");
                //}
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnWidth - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnWidth - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Double? TbC_ColumnWidth
        {
            get
            {
                return _data.C.TbC_ColumnWidth;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnWidth - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_ColumnWidth == value) { return; }
                    _data.C.TbC_ColumnWidth = value;
                    CustomPropertySetterCode("TbC_ColumnWidth");
                    TbC_ColumnWidth_Validate();
                    CheckEntityState(es);
                    Notify("TbC_ColumnWidth");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnWidth - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnWidth - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_TextBoxTextAlignment
        {
            get
            {
                return _data.C.TbC_TextBoxTextAlignment;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextBoxTextAlignment - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_TextBoxTextAlignment == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_TextBoxTextAlignment == null || _data.C.TbC_TextBoxTextAlignment.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_TextBoxTextAlignment == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_TextBoxTextAlignment = null;
                    }
                    else
                    {
                        _data.C.TbC_TextBoxTextAlignment = value.Trim();
                    }
                    //else if ((_data.C.TbC_TextBoxTextAlignment == null || _data.C.TbC_TextBoxTextAlignment.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_TextBoxTextAlignment == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_TextBoxTextAlignment == null || _data.C.TbC_TextBoxTextAlignment.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_TextBoxTextAlignment = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_TextBoxTextAlignment = value;
                    //}
                    CustomPropertySetterCode("TbC_TextBoxTextAlignment");
                    TbC_TextBoxTextAlignment_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_TextBoxTextAlignment, _data.C.TbC_TextBoxTextAlignment);
                        
					Notify("TbC_TextBoxTextAlignment");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextBoxTextAlignment - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextBoxTextAlignment - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_ColumnTextAlignment
        {
            get
            {
                return _data.C.TbC_ColumnTextAlignment;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnTextAlignment - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_ColumnTextAlignment == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_ColumnTextAlignment == null || _data.C.TbC_ColumnTextAlignment.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_ColumnTextAlignment == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_ColumnTextAlignment = null;
                    }
                    else
                    {
                        _data.C.TbC_ColumnTextAlignment = value.Trim();
                    }
                    //else if ((_data.C.TbC_ColumnTextAlignment == null || _data.C.TbC_ColumnTextAlignment.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_ColumnTextAlignment == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_ColumnTextAlignment == null || _data.C.TbC_ColumnTextAlignment.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_ColumnTextAlignment = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_ColumnTextAlignment = value;
                    //}
                    CustomPropertySetterCode("TbC_ColumnTextAlignment");
                    TbC_ColumnTextAlignment_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_ColumnTextAlignment, _data.C.TbC_ColumnTextAlignment);
                        
					Notify("TbC_ColumnTextAlignment");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnTextAlignment - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnTextAlignment - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public String TbC_ListStoredProc_Id_TextField
        {
            get
            {
                return _data.C.TbC_ListStoredProc_Id_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ListStoredProc_Id Setter", IfxTraceCategory.Enter);
                    if (_data.C.TbC_ListStoredProc_Id_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.TbC_ListStoredProc_Id_TextField == null || _data.C.TbC_ListStoredProc_Id_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.TbC_ListStoredProc_Id_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.TbC_ListStoredProc_Id_TextField = value;
                    }
                    CustomPropertySetterCode("TbC_ListStoredProc_Id_TextField");
                    Notify("TbC_ListStoredProc_Id_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ListStoredProc_Id Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ListStoredProc_Id Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Guid? TbC_ListStoredProc_Id
        {
            get
            {
                return _data.C.TbC_ListStoredProc_Id;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ListStoredProc_Id - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_ListStoredProc_Id == value) { return; }
                    _data.C.TbC_ListStoredProc_Id = value;
                    CustomPropertySetterCode("TbC_ListStoredProc_Id");
                    TbC_ListStoredProc_Id_Validate();
                    CheckEntityState(es);
                    Notify("TbC_ListStoredProc_Id");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = WcApplicationVersion_Bll_staticLists.WcStoredProc_ComboItemList_BindingListProperty.FindItemById(_data.C.TbC_ListStoredProc_Id);
                    if (obj == null)
                    {
                        TbC_ListStoredProc_Id_TextField = "";
                    }
                    else
                    {
                        TbC_ListStoredProc_Id_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ListStoredProc_Id - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ListStoredProc_Id - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsStaticList
        {
            get
            {
                return _data.C.TbC_IsStaticList;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsStaticList - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsStaticList == value) { return; }
                    _data.C.TbC_IsStaticList = value;
                    CustomPropertySetterCode("TbC_IsStaticList");
                    TbC_IsStaticList_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsStaticList");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsStaticList - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsStaticList - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_UseNotInList
        {
            get
            {
                return _data.C.TbC_UseNotInList;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseNotInList - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_UseNotInList == value) { return; }
                    _data.C.TbC_UseNotInList = value;
                    CustomPropertySetterCode("TbC_UseNotInList");
                    TbC_UseNotInList_Validate();
                    CheckEntityState(es);
                    Notify("TbC_UseNotInList");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseNotInList - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseNotInList - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_UseListEditBtn
        {
            get
            {
                return _data.C.TbC_UseListEditBtn;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseListEditBtn - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_UseListEditBtn == value) { return; }
                    _data.C.TbC_UseListEditBtn = value;
                    CustomPropertySetterCode("TbC_UseListEditBtn");
                    TbC_UseListEditBtn_Validate();
                    CheckEntityState(es);
                    Notify("TbC_UseListEditBtn");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseListEditBtn - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseListEditBtn - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public String TbC_ParentColumnKey_TextField
        {
            get
            {
                return _data.C.TbC_ParentColumnKey_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ParentColumnKey Setter", IfxTraceCategory.Enter);
                    if (_data.C.TbC_ParentColumnKey_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.TbC_ParentColumnKey_TextField == null || _data.C.TbC_ParentColumnKey_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.TbC_ParentColumnKey_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.TbC_ParentColumnKey_TextField = value;
                    }
                    CustomPropertySetterCode("TbC_ParentColumnKey_TextField");
                    Notify("TbC_ParentColumnKey_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ParentColumnKey Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ParentColumnKey Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Guid? TbC_ParentColumnKey
        {
            get
            {
                return _data.C.TbC_ParentColumnKey;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ParentColumnKey - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_ParentColumnKey == value) { return; }
                    _data.C.TbC_ParentColumnKey = value;
                    CustomPropertySetterCode("TbC_ParentColumnKey");
                    TbC_ParentColumnKey_Validate();
                    CheckEntityState(es);
                    Notify("TbC_ParentColumnKey");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.FindItemById(_data.C.TbC_ParentColumnKey);
                    if (obj == null)
                    {
                        TbC_ParentColumnKey_TextField = "";
                    }
                    else
                    {
                        TbC_ParentColumnKey_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ParentColumnKey - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ParentColumnKey - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_UseDisplayTextFieldProperty
        {
            get
            {
                return _data.C.TbC_UseDisplayTextFieldProperty;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseDisplayTextFieldProperty - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_UseDisplayTextFieldProperty == value) { return; }
                    _data.C.TbC_UseDisplayTextFieldProperty = value;
                    CustomPropertySetterCode("TbC_UseDisplayTextFieldProperty");
                    TbC_UseDisplayTextFieldProperty_Validate();
                    CheckEntityState(es);
                    Notify("TbC_UseDisplayTextFieldProperty");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseDisplayTextFieldProperty - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseDisplayTextFieldProperty - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsDisplayTextFieldProperty
        {
            get
            {
                return _data.C.TbC_IsDisplayTextFieldProperty;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDisplayTextFieldProperty - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsDisplayTextFieldProperty == value) { return; }
                    _data.C.TbC_IsDisplayTextFieldProperty = value;
                    CustomPropertySetterCode("TbC_IsDisplayTextFieldProperty");
                    TbC_IsDisplayTextFieldProperty_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsDisplayTextFieldProperty");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDisplayTextFieldProperty - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDisplayTextFieldProperty - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public String TbC_ComboListTable_Id_TextField
        {
            get
            {
                return _data.C.TbC_ComboListTable_Id_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListTable_Id Setter", IfxTraceCategory.Enter);
                    if (_data.C.TbC_ComboListTable_Id_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.TbC_ComboListTable_Id_TextField == null || _data.C.TbC_ComboListTable_Id_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.TbC_ComboListTable_Id_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.TbC_ComboListTable_Id_TextField = value;
                    }
                    CustomPropertySetterCode("TbC_ComboListTable_Id_TextField");
                    Notify("TbC_ComboListTable_Id_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListTable_Id Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListTable_Id Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Guid? TbC_ComboListTable_Id
        {
            get
            {
                return _data.C.TbC_ComboListTable_Id;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListTable_Id - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_ComboListTable_Id == value) { return; }
                    _data.C.TbC_ComboListTable_Id = value;
                    CustomPropertySetterCode("TbC_ComboListTable_Id");
                    TbC_ComboListTable_Id_Validate();
                    CheckEntityState(es);
                    Notify("TbC_ComboListTable_Id");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty.FindItemById(_data.C.TbC_ComboListTable_Id);
                    if (obj == null)
                    {
                        TbC_ComboListTable_Id_TextField = "";
                    }
                    else
                    {
                        TbC_ComboListTable_Id_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListTable_Id - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListTable_Id - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public String TbC_ComboListDisplayColumn_Id_TextField
        {
            get
            {
                return _data.C.TbC_ComboListDisplayColumn_Id_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListDisplayColumn_Id Setter", IfxTraceCategory.Enter);
                    if (_data.C.TbC_ComboListDisplayColumn_Id_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.TbC_ComboListDisplayColumn_Id_TextField == null || _data.C.TbC_ComboListDisplayColumn_Id_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.TbC_ComboListDisplayColumn_Id_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.TbC_ComboListDisplayColumn_Id_TextField = value;
                    }
                    CustomPropertySetterCode("TbC_ComboListDisplayColumn_Id_TextField");
                    Notify("TbC_ComboListDisplayColumn_Id_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListDisplayColumn_Id Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListDisplayColumn_Id Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Guid? TbC_ComboListDisplayColumn_Id
        {
            get
            {
                return _data.C.TbC_ComboListDisplayColumn_Id;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListDisplayColumn_Id - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_ComboListDisplayColumn_Id == value) { return; }
                    _data.C.TbC_ComboListDisplayColumn_Id = value;
                    CustomPropertySetterCode("TbC_ComboListDisplayColumn_Id");
                    TbC_ComboListDisplayColumn_Id_Validate();
                    CheckEntityState(es);
                    Notify("TbC_ComboListDisplayColumn_Id");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.FindItemById(_data.C.TbC_ComboListDisplayColumn_Id);
                    if (obj == null)
                    {
                        TbC_ComboListDisplayColumn_Id_TextField = "";
                    }
                    else
                    {
                        TbC_ComboListDisplayColumn_Id_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListDisplayColumn_Id - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListDisplayColumn_Id - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="TbC_Combo_MaxDropdownHeight">TbC_Combo_MaxDropdownHeight</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the TbC_Combo_MaxDropdownHeight field. If it’s not numeric, then TbC_Combo_MaxDropdownHeight is set to
        ///     null.</para>
        /// </summary>
        public String TbC_Combo_MaxDropdownHeight_asString
        {
            get
            {
                if (null == _data.C.TbC_Combo_MaxDropdownHeight)
                {
                    return "";
                }
                else
                {
                    return _data.C.TbC_Combo_MaxDropdownHeight.ToString();
                }
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownHeight - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    string _tbC_Combo_MaxDropdownHeight_OriginalValue = "";
                    Double? newVal = null;
                    if (value != null)
                    {
                        if (value.Trim() == ".")
                        {
                            _tbC_Combo_MaxDropdownHeight_OriginalValue = ".";
                        }
                        else if (value.Trim() == ",")
                        {
                            _tbC_Combo_MaxDropdownHeight_OriginalValue = ",";
                        }

                        else if (BLLHelper.IsNumber(value))
                        {
                            try
                            {
                                _tbC_Combo_MaxDropdownHeight_OriginalValue = value;
                                Double newValNotNull;
                                Double.TryParse(Convert.ToString(value), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.CurrentInfo, out newValNotNull);
                                newVal = newValNotNull;
                            }
                            catch (FormatException e)
                            {
                            }
                        }
                    }

                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_Combo_MaxDropdownHeight == newVal) { return; }
                    _data.C.TbC_Combo_MaxDropdownHeight = newVal;
                    CustomPropertySetterCode("TbC_Combo_MaxDropdownHeight");
                    TbC_Combo_MaxDropdownHeight_Validate();
                    CheckEntityState(es);

                    //  allow zero, therefore, notify UI if user input zero
                    if (_tbC_Combo_MaxDropdownHeight_OriginalValue != "." && _tbC_Combo_MaxDropdownHeight_OriginalValue != ",")
                    {
                        Notify("TbC_Combo_MaxDropdownHeight_asString");
                    }
 
                //if (newVal !=0 && _tbC_Combo_MaxDropdownHeight_OriginalValue != ".")
                //{
                //    Notify("TbC_Combo_MaxDropdownHeight");
                //}
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownHeight - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownHeight - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Double? TbC_Combo_MaxDropdownHeight
        {
            get
            {
                return _data.C.TbC_Combo_MaxDropdownHeight;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownHeight - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_Combo_MaxDropdownHeight == value) { return; }
                    _data.C.TbC_Combo_MaxDropdownHeight = value;
                    CustomPropertySetterCode("TbC_Combo_MaxDropdownHeight");
                    TbC_Combo_MaxDropdownHeight_Validate();
                    CheckEntityState(es);
                    Notify("TbC_Combo_MaxDropdownHeight");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownHeight - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownHeight - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="TbC_Combo_MaxDropdownWidth">TbC_Combo_MaxDropdownWidth</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the TbC_Combo_MaxDropdownWidth field. If it’s not numeric, then TbC_Combo_MaxDropdownWidth is set to
        ///     null.</para>
        /// </summary>
        public String TbC_Combo_MaxDropdownWidth_asString
        {
            get
            {
                if (null == _data.C.TbC_Combo_MaxDropdownWidth)
                {
                    return "";
                }
                else
                {
                    return _data.C.TbC_Combo_MaxDropdownWidth.ToString();
                }
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownWidth - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    string _tbC_Combo_MaxDropdownWidth_OriginalValue = "";
                    Double? newVal = null;
                    if (value != null)
                    {
                        if (value.Trim() == ".")
                        {
                            _tbC_Combo_MaxDropdownWidth_OriginalValue = ".";
                        }
                        else if (value.Trim() == ",")
                        {
                            _tbC_Combo_MaxDropdownWidth_OriginalValue = ",";
                        }

                        else if (BLLHelper.IsNumber(value))
                        {
                            try
                            {
                                _tbC_Combo_MaxDropdownWidth_OriginalValue = value;
                                Double newValNotNull;
                                Double.TryParse(Convert.ToString(value), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.CurrentInfo, out newValNotNull);
                                newVal = newValNotNull;
                            }
                            catch (FormatException e)
                            {
                            }
                        }
                    }

                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_Combo_MaxDropdownWidth == newVal) { return; }
                    _data.C.TbC_Combo_MaxDropdownWidth = newVal;
                    CustomPropertySetterCode("TbC_Combo_MaxDropdownWidth");
                    TbC_Combo_MaxDropdownWidth_Validate();
                    CheckEntityState(es);

                    //  allow zero, therefore, notify UI if user input zero
                    if (_tbC_Combo_MaxDropdownWidth_OriginalValue != "." && _tbC_Combo_MaxDropdownWidth_OriginalValue != ",")
                    {
                        Notify("TbC_Combo_MaxDropdownWidth_asString");
                    }
 
                //if (newVal !=0 && _tbC_Combo_MaxDropdownWidth_OriginalValue != ".")
                //{
                //    Notify("TbC_Combo_MaxDropdownWidth");
                //}
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownWidth - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownWidth - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Double? TbC_Combo_MaxDropdownWidth
        {
            get
            {
                return _data.C.TbC_Combo_MaxDropdownWidth;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownWidth - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_Combo_MaxDropdownWidth == value) { return; }
                    _data.C.TbC_Combo_MaxDropdownWidth = value;
                    CustomPropertySetterCode("TbC_Combo_MaxDropdownWidth");
                    TbC_Combo_MaxDropdownWidth_Validate();
                    CheckEntityState(es);
                    Notify("TbC_Combo_MaxDropdownWidth");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownWidth - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownWidth - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_Combo_AllowDropdownResizing
        {
            get
            {
                return _data.C.TbC_Combo_AllowDropdownResizing;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_AllowDropdownResizing - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_Combo_AllowDropdownResizing == value) { return; }
                    _data.C.TbC_Combo_AllowDropdownResizing = value;
                    CustomPropertySetterCode("TbC_Combo_AllowDropdownResizing");
                    TbC_Combo_AllowDropdownResizing_Validate();
                    CheckEntityState(es);
                    Notify("TbC_Combo_AllowDropdownResizing");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_AllowDropdownResizing - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_AllowDropdownResizing - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_Combo_IsResetButtonVisible
        {
            get
            {
                return _data.C.TbC_Combo_IsResetButtonVisible;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_IsResetButtonVisible - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_Combo_IsResetButtonVisible == value) { return; }
                    _data.C.TbC_Combo_IsResetButtonVisible = value;
                    CustomPropertySetterCode("TbC_Combo_IsResetButtonVisible");
                    TbC_Combo_IsResetButtonVisible_Validate();
                    CheckEntityState(es);
                    Notify("TbC_Combo_IsResetButtonVisible");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_IsResetButtonVisible - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_IsResetButtonVisible - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsActiveRecColumn
        {
            get
            {
                return _data.C.TbC_IsActiveRecColumn;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsActiveRecColumn - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsActiveRecColumn == value) { return; }
                    _data.C.TbC_IsActiveRecColumn = value;
                    CustomPropertySetterCode("TbC_IsActiveRecColumn");
                    TbC_IsActiveRecColumn_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsActiveRecColumn");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsActiveRecColumn - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsActiveRecColumn - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsDeletedColumn
        {
            get
            {
                return _data.C.TbC_IsDeletedColumn;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDeletedColumn - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsDeletedColumn == value) { return; }
                    _data.C.TbC_IsDeletedColumn = value;
                    CustomPropertySetterCode("TbC_IsDeletedColumn");
                    TbC_IsDeletedColumn_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsDeletedColumn");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDeletedColumn - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDeletedColumn - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsCreatedUserIdColumn
        {
            get
            {
                return _data.C.TbC_IsCreatedUserIdColumn;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatedUserIdColumn - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsCreatedUserIdColumn == value) { return; }
                    _data.C.TbC_IsCreatedUserIdColumn = value;
                    CustomPropertySetterCode("TbC_IsCreatedUserIdColumn");
                    TbC_IsCreatedUserIdColumn_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsCreatedUserIdColumn");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatedUserIdColumn - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatedUserIdColumn - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsCreatedDateColumn
        {
            get
            {
                return _data.C.TbC_IsCreatedDateColumn;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatedDateColumn - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsCreatedDateColumn == value) { return; }
                    _data.C.TbC_IsCreatedDateColumn = value;
                    CustomPropertySetterCode("TbC_IsCreatedDateColumn");
                    TbC_IsCreatedDateColumn_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsCreatedDateColumn");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatedDateColumn - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatedDateColumn - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsUserIdColumn
        {
            get
            {
                return _data.C.TbC_IsUserIdColumn;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsUserIdColumn - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsUserIdColumn == value) { return; }
                    _data.C.TbC_IsUserIdColumn = value;
                    CustomPropertySetterCode("TbC_IsUserIdColumn");
                    TbC_IsUserIdColumn_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsUserIdColumn");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsUserIdColumn - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsUserIdColumn - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsModifiedDateColumn
        {
            get
            {
                return _data.C.TbC_IsModifiedDateColumn;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsModifiedDateColumn - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsModifiedDateColumn == value) { return; }
                    _data.C.TbC_IsModifiedDateColumn = value;
                    CustomPropertySetterCode("TbC_IsModifiedDateColumn");
                    TbC_IsModifiedDateColumn_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsModifiedDateColumn");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsModifiedDateColumn - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsModifiedDateColumn - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsRowVersionStampColumn
        {
            get
            {
                return _data.C.TbC_IsRowVersionStampColumn;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsRowVersionStampColumn - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsRowVersionStampColumn == value) { return; }
                    _data.C.TbC_IsRowVersionStampColumn = value;
                    CustomPropertySetterCode("TbC_IsRowVersionStampColumn");
                    TbC_IsRowVersionStampColumn_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsRowVersionStampColumn");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsRowVersionStampColumn - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsRowVersionStampColumn - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsBrowsable
        {
            get
            {
                return _data.C.TbC_IsBrowsable;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsBrowsable - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsBrowsable == value) { return; }
                    _data.C.TbC_IsBrowsable = value;
                    CustomPropertySetterCode("TbC_IsBrowsable");
                    TbC_IsBrowsable_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsBrowsable");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsBrowsable - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsBrowsable - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_DeveloperNote
        {
            get
            {
                return _data.C.TbC_DeveloperNote;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DeveloperNote - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_DeveloperNote == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_DeveloperNote == null || _data.C.TbC_DeveloperNote.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_DeveloperNote == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_DeveloperNote = null;
                    }
                    else
                    {
                        _data.C.TbC_DeveloperNote = value.Trim();
                    }
                    //else if ((_data.C.TbC_DeveloperNote == null || _data.C.TbC_DeveloperNote.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_DeveloperNote == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_DeveloperNote == null || _data.C.TbC_DeveloperNote.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_DeveloperNote = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_DeveloperNote = value;
                    //}
                    CustomPropertySetterCode("TbC_DeveloperNote");
                    TbC_DeveloperNote_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_DeveloperNote, _data.C.TbC_DeveloperNote);
                        
					Notify("TbC_DeveloperNote");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DeveloperNote - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DeveloperNote - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_UserNote
        {
            get
            {
                return _data.C.TbC_UserNote;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UserNote - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_UserNote == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_UserNote == null || _data.C.TbC_UserNote.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_UserNote == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_UserNote = null;
                    }
                    else
                    {
                        _data.C.TbC_UserNote = value.Trim();
                    }
                    //else if ((_data.C.TbC_UserNote == null || _data.C.TbC_UserNote.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_UserNote == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_UserNote == null || _data.C.TbC_UserNote.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_UserNote = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_UserNote = value;
                    //}
                    CustomPropertySetterCode("TbC_UserNote");
                    TbC_UserNote_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_UserNote, _data.C.TbC_UserNote);
                        
					Notify("TbC_UserNote");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UserNote - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UserNote - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_HelpFileAdditionalNote
        {
            get
            {
                return _data.C.TbC_HelpFileAdditionalNote;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_HelpFileAdditionalNote - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_HelpFileAdditionalNote == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_HelpFileAdditionalNote == null || _data.C.TbC_HelpFileAdditionalNote.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_HelpFileAdditionalNote == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_HelpFileAdditionalNote = null;
                    }
                    else
                    {
                        _data.C.TbC_HelpFileAdditionalNote = value.Trim();
                    }
                    //else if ((_data.C.TbC_HelpFileAdditionalNote == null || _data.C.TbC_HelpFileAdditionalNote.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_HelpFileAdditionalNote == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_HelpFileAdditionalNote == null || _data.C.TbC_HelpFileAdditionalNote.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_HelpFileAdditionalNote = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_HelpFileAdditionalNote = value;
                    //}
                    CustomPropertySetterCode("TbC_HelpFileAdditionalNote");
                    TbC_HelpFileAdditionalNote_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_HelpFileAdditionalNote, _data.C.TbC_HelpFileAdditionalNote);
                        
					Notify("TbC_HelpFileAdditionalNote");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_HelpFileAdditionalNote - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_HelpFileAdditionalNote - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_Notes
        {
            get
            {
                return _data.C.TbC_Notes;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Notes - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_Notes == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.TbC_Notes == null || _data.C.TbC_Notes.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.TbC_Notes == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.TbC_Notes = null;
                    }
                    else
                    {
                        _data.C.TbC_Notes = value.Trim();
                    }
                    //else if ((_data.C.TbC_Notes == null || _data.C.TbC_Notes.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.TbC_Notes == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.TbC_Notes == null || _data.C.TbC_Notes.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.TbC_Notes = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.TbC_Notes = value;
                    //}
                    CustomPropertySetterCode("TbC_Notes");
                    TbC_Notes_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.TbC_Notes, _data.C.TbC_Notes);
                        
					Notify("TbC_Notes");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Notes - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Notes - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsInputComplete
        {
            get
            {
                return _data.C.TbC_IsInputComplete;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInputComplete - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsInputComplete == value) { return; }
                    _data.C.TbC_IsInputComplete = value;
                    CustomPropertySetterCode("TbC_IsInputComplete");
                    TbC_IsInputComplete_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsInputComplete");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInputComplete - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInputComplete - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsCodeGen
        {
            get
            {
                return _data.C.TbC_IsCodeGen;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGen - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsCodeGen == value) { return; }
                    _data.C.TbC_IsCodeGen = value;
                    CustomPropertySetterCode("TbC_IsCodeGen");
                    TbC_IsCodeGen_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsCodeGen");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGen - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGen - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean? TbC_IsReadyCodeGen
        {
            get
            {
                return _data.C.TbC_IsReadyCodeGen;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadyCodeGen - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsReadyCodeGen == value) { return; }
                    _data.C.TbC_IsReadyCodeGen = value;
                    CustomPropertySetterCode("TbC_IsReadyCodeGen");
                    TbC_IsReadyCodeGen_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsReadyCodeGen");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadyCodeGen - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadyCodeGen - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean? TbC_IsCodeGenComplete
        {
            get
            {
                return _data.C.TbC_IsCodeGenComplete;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGenComplete - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsCodeGenComplete == value) { return; }
                    _data.C.TbC_IsCodeGenComplete = value;
                    CustomPropertySetterCode("TbC_IsCodeGenComplete");
                    TbC_IsCodeGenComplete_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsCodeGenComplete");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGenComplete - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGenComplete - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean? TbC_IsTagForCodeGen
        {
            get
            {
                return _data.C.TbC_IsTagForCodeGen;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTagForCodeGen - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsTagForCodeGen == value) { return; }
                    _data.C.TbC_IsTagForCodeGen = value;
                    CustomPropertySetterCode("TbC_IsTagForCodeGen");
                    TbC_IsTagForCodeGen_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsTagForCodeGen");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTagForCodeGen - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTagForCodeGen - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean? TbC_IsTagForOther
        {
            get
            {
                return _data.C.TbC_IsTagForOther;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTagForOther - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsTagForOther == value) { return; }
                    _data.C.TbC_IsTagForOther = value;
                    CustomPropertySetterCode("TbC_IsTagForOther");
                    TbC_IsTagForOther_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsTagForOther");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTagForOther - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTagForOther - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String TbC_ColumnGroups
        {
            get
            {
                return _data.C.TbC_ColumnGroups;
            }
        }

		
        public Boolean TbC_IsActiveRow
        {
            get
            {
                return _data.C.TbC_IsActiveRow;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsActiveRow - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsActiveRow == value) { return; }
                    _data.C.TbC_IsActiveRow = value;
                    CustomPropertySetterCode("TbC_IsActiveRow");
                    TbC_IsActiveRow_Validate();
                    CheckEntityState(es);
                    Notify("TbC_IsActiveRow");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsActiveRow - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsActiveRow - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean TbC_IsDeleted
        {
            get
            {
                return _data.C.TbC_IsDeleted;
            }
        }

		
        public Guid? TbC_CreatedUserId
        {
            get
            {
                return _data.C.TbC_CreatedUserId;
            }
        }


		/// <summary>
        ///  Use this property when binding in Grids so the Notify method won't prevent you from entering complete dates or the year part of a date.  Notify will refresh the UI cutting off anything after the first diget of the month.
        /// </summary>
        public String TbC_CreatedDate_asString
        {
            get
            {
                if (null == _data.C.TbC_CreatedDate)
                {
                    return "";
                }
                else
                {
                    return _data.C.TbC_CreatedDate.ToString();
                }
            }
        }

		
        public DateTime? TbC_CreatedDate
        {
            get
            {
                return _data.C.TbC_CreatedDate;
            }
        }

		
        public Guid? TbC_UserId
        {
            get
            {
                return _data.C.TbC_UserId;
            }
        }


        public String UserName
        {
            get
            {
                return _data.C.UserName;
            }
        }


		/// <summary>
        ///  Use this property when binding in Grids so the Notify method won't prevent you from entering complete dates or the year part of a date.  Notify will refresh the UI cutting off anything after the first diget of the month.
        /// </summary>
        public String TbC_LastModifiedDate_asString
        {
            get
            {
                if (null == _data.C.TbC_LastModifiedDate)
                {
                    return "";
                }
                else
                {
                    return _data.C.TbC_LastModifiedDate.ToString();
                }
            }
        }

		
        public DateTime? TbC_LastModifiedDate
        {
            get
            {
                return _data.C.TbC_LastModifiedDate;
            }
        }


        public Byte[] TbC_Stamp
        {
            get
            {
                return _data.C.TbC_Stamp;
            }
        }



		#endregion Data Props


		#region Concurrency and Data State


        private bool DeterminConcurrencyCheck(UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", IfxTraceCategory.Enter);
                switch (check)
                {
                    case UseConcurrencyCheck.UseDefaultSetting:
                        return true;
                    case UseConcurrencyCheck.UseConcurrencyCheck:
                        return true;
                    case UseConcurrencyCheck.BypassConcurrencyCheck:
                        return false;
                }
                return false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     This is a reference to the current data (<see cref="EntityWireType.WcTableColumn_Values">WcTableColumn_Values</see>) for this business object
        ///     (WcTableColumn_Bll). The Values Manager (<see cref="EntityWireType.WcTableColumn_ValuesMngr">WcTableColumn_ValuesMngr</see>) has three sets of WcTableColumn
        ///     data used for managing state and concurrency. For more information about the other
        ///     two sets of data, <see cref="Original">Original</see> and <see cref="Concurrency">Concurrency</see>, and how they are used, see the overviews of
        ///     <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Concurrency_Overview.html">
        ///     Concurrency</a> and <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Wire_Object_Overview.html">
        ///     Wire Objects</a>.
        /// </summary>
        /// <seealso cref="Original">Original Property</seealso>
        /// <seealso cref="Concurrency">Concurrency Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcTableColumn_Values Current
        {
            get
            {
                return _data.C;
            }
            set
            {
                //Management Code
                _data.C = value;
            }
        }

        /// <summary>
        ///     This is a reference to the original data (<see cref="EntityWireType.WcTableColumn_Values">WcTableColumn_Values</see>) for this business object
        ///     (WcTableColumn_Bll) which was either data for a ‘new-not dirty’ WcTableColumn or data for a WcTableColumn
        ///     retrieved from the data store. The Values Manager (<see cref="EntityWireType.WcTableColumn_ValuesMngr">WcTableColumn_ValuesMngr</see>) has three sets of WcTableColumn
        ///     data used for managing state and concurrency. For more information about the other
        ///     two sets of data, <see cref="Current">Current</see> and <see cref="Concurrency">Concurrency</see>, and how they are used, see the overview of
        ///     Concurrency and Wire Objects.
        /// </summary>
        /// <seealso cref="Current">Current Property</seealso>
        /// <seealso cref="Concurrency">Concurrency Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcTableColumn_Values Original
        {
            get
            {
                return _data.O;
            }
            set
            {
                //Management Code
                _data.O = value;
            }
        }

        /// <summary>*** Need to review before documenting ***</summary>
        /// <seealso cref="Current">Current Property</seealso>
        /// <seealso cref="Original">Original Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcTableColumn_Values Concurrency
        {
            get
            {
                return _data.X;
            }
            set
            {
                //Management Code
                _data.X = value;
            }
        }

        /// <value>
        ///     This class manages the <see cref="TypeServices.EntityState">EntityStateSwitch</see>
        ///     and is used to describe the current <see cref="TypeServices.EntityState">state</see> of this business object such as IsDirty,
        ///     IsValid and IsNew. The combinations of these six possible values describe the
        ///     entity state and are used for logic in configuring settings in the business object
        ///     as well as the UI.
        /// </value>
        /// <seealso cref="EntityWireType.WcTableColumn_ValuesMngr">WcTableColumn_ValuesMngr Class</seealso>
        [Browsable(false)]
        public override EntityState State
        {
            get
            {
                return _data.S;
            }
            set
            {
                //Management Code
                _data.S = value;
            }
        }

        /// <summary>
        ///     This is the combination of IsDirty, IsValid and IsNew and is managed by the
        ///     <see cref="TypeServices.EntityState">EntityState</see> class. The combinations of
        ///     these six possible values describe the entity <see cref="TypeServices.EntityState">state</see> and are used for logic in configuring
        ///     settings in the business object as well as the UI.
        /// </summary>
        /// <seealso cref="EntityWireType.WcTableColumn_ValuesMngr">WcTableColumn_ValuesMngr Class</seealso>
        public EntityStateSwitch StateSwitch
        {
            get
            {
                return _data.S.Switch;
            }
        }




		#endregion Concurrency and Data State

        [Browsable(false)]
        public override object this[string name]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                //Management Code
                throw new NotImplementedException();
            }
        }


		#region ITraceItem Members


        /// <summary>
        /// 	<para>
        ///         Used in IFX (the instrumentation framework) for tracing. This returns the short
        ///         version of the <see cref="ManagementServices.TraceItemList">TraceItemList</see>. The ‘Short Version’
        ///         returns only the entity’s data fields, <see cref="TypeServices.BrokenRuleManager.GetBrokenRuleListForEntity">BrokenRules</see>
        ///         and <see cref="TypeServices.EntityStateSwitch">EntityStateSwitch</see>.
        ///         However, it also calls the <see cref="GetTraceItemsShortListCustom">GetTraceItemsShortListCustom</see> method in the
        ///         WcTableColumn.bll.cust.cs code file so a developer can customize the list by adding
        ///         additional items to it.
        ///     </para>
        /// 	<para>
        ///         Keep in mind that this is the short versions so don’t get carried away adding
        ///         to many things to it. For a robust and extensive list of items to record in the
        ///         trace, use the <see cref="GetTraceItemsLongList">GetTraceItemsLongList</see>
        ///         and <see cref="GetTraceItemsLongListCustom">GetTraceItemsLongListCustom</see>
        ///         methods.
        ///     </para>
        /// </summary>
        public TraceItemList GetTraceItemsShortList()
        {
            if (null == _traceItems)
            {
                _traceItems = new TraceItemList("WcTableColumn_Bll");
            }
            else
            {
                _traceItems.Clear();
            }
            _traceItems.Add("StateSwitch", StateSwitch.ToString(), TraceDataTypes.String);
            _traceItems.Add("Broken Rules", BrokenRuleManagerProperty.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_Id", _data.C.TbC_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_Tb_Id", _data.C.TbC_Tb_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_auditColumn_Id", _data.C.TbC_auditColumn_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_SortOrder", _data.C.TbC_SortOrder.ToString(), TraceDataTypes.Int16);
			_traceItems.Add("DbSortOrder", _data.C.DbSortOrder.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("IsImported", _data.C.IsImported.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_Name", _data.C.TbC_Name.ToString(), TraceDataTypes.String);
			_traceItems.Add("ColumnInDatabase", _data.C.ColumnInDatabase.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_CtlTp_Id", _data.C.TbC_CtlTp_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("TbC_CtlTp_Id_TextField", _data.C.TbC_CtlTp_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_IsNonvColumn", _data.C.TbC_IsNonvColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_Description", _data.C.TbC_Description.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_DtSql_Id", _data.C.TbC_DtSql_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("TbC_DtSql_Id_TextField", _data.C.TbC_DtSql_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_DtDtNt_Id", _data.C.TbC_DtDtNt_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("TbC_DtDtNt_Id_TextField", _data.C.TbC_DtDtNt_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_Length", _data.C.TbC_Length.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("TbC_Precision", _data.C.TbC_Precision.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("TbC_Scale", _data.C.TbC_Scale.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("TbC_IsPK", _data.C.TbC_IsPK.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsIdentity", _data.C.TbC_IsIdentity.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsFK", _data.C.TbC_IsFK.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsEntityColumn", _data.C.TbC_IsEntityColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsSystemField", _data.C.TbC_IsSystemField.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsValuesObjectMember", _data.C.TbC_IsValuesObjectMember.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsInPropsScreen", _data.C.TbC_IsInPropsScreen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsInNavList", _data.C.TbC_IsInNavList.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsRequired", _data.C.TbC_IsRequired.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_BrokenRuleText", _data.C.TbC_BrokenRuleText.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_AllowZero", _data.C.TbC_AllowZero.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsNullableInDb", _data.C.TbC_IsNullableInDb.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsNullableInUI", _data.C.TbC_IsNullableInUI.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_DefaultValue", _data.C.TbC_DefaultValue.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_IsReadFromDb", _data.C.TbC_IsReadFromDb.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsSendToDb", _data.C.TbC_IsSendToDb.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsInsertAllowed", _data.C.TbC_IsInsertAllowed.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsEditAllowed", _data.C.TbC_IsEditAllowed.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsReadOnlyInUI", _data.C.TbC_IsReadOnlyInUI.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_UseForAudit", _data.C.TbC_UseForAudit.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_DefaultCaption", _data.C.TbC_DefaultCaption.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_ColumnHeaderText", _data.C.TbC_ColumnHeaderText.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_LabelCaptionVerbose", _data.C.TbC_LabelCaptionVerbose.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_LabelCaptionGenerate", _data.C.TbC_LabelCaptionGenerate.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tbc_ShowGridColumnToolTip", _data.C.Tbc_ShowGridColumnToolTip.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tbc_ShowPropsToolTip", _data.C.Tbc_ShowPropsToolTip.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tbc_TooltipsRolledUp", _data.C.Tbc_TooltipsRolledUp.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_IsCreatePropsStrings", _data.C.TbC_IsCreatePropsStrings.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsCreateGridStrings", _data.C.TbC_IsCreateGridStrings.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsAvailableForColumnGroups", _data.C.TbC_IsAvailableForColumnGroups.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsTextWrapInProp", _data.C.TbC_IsTextWrapInProp.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsTextWrapInGrid", _data.C.TbC_IsTextWrapInGrid.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_TextBoxFormat", _data.C.TbC_TextBoxFormat.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_TextColumnFormat", _data.C.TbC_TextColumnFormat.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_ColumnWidth", _data.C.TbC_ColumnWidth.ToString(), TraceDataTypes.Double);
			_traceItems.Add("TbC_TextBoxTextAlignment", _data.C.TbC_TextBoxTextAlignment.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_ColumnTextAlignment", _data.C.TbC_ColumnTextAlignment.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_ListStoredProc_Id", _data.C.TbC_ListStoredProc_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_ListStoredProc_Id_TextField", _data.C.TbC_ListStoredProc_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_IsStaticList", _data.C.TbC_IsStaticList.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_UseNotInList", _data.C.TbC_UseNotInList.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_UseListEditBtn", _data.C.TbC_UseListEditBtn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_ParentColumnKey", _data.C.TbC_ParentColumnKey.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_ParentColumnKey_TextField", _data.C.TbC_ParentColumnKey_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_UseDisplayTextFieldProperty", _data.C.TbC_UseDisplayTextFieldProperty.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsDisplayTextFieldProperty", _data.C.TbC_IsDisplayTextFieldProperty.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_ComboListTable_Id", _data.C.TbC_ComboListTable_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_ComboListTable_Id_TextField", _data.C.TbC_ComboListTable_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_ComboListDisplayColumn_Id", _data.C.TbC_ComboListDisplayColumn_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_ComboListDisplayColumn_Id_TextField", _data.C.TbC_ComboListDisplayColumn_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_Combo_MaxDropdownHeight", _data.C.TbC_Combo_MaxDropdownHeight.ToString(), TraceDataTypes.Double);
			_traceItems.Add("TbC_Combo_MaxDropdownWidth", _data.C.TbC_Combo_MaxDropdownWidth.ToString(), TraceDataTypes.Double);
			_traceItems.Add("TbC_Combo_AllowDropdownResizing", _data.C.TbC_Combo_AllowDropdownResizing.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_Combo_IsResetButtonVisible", _data.C.TbC_Combo_IsResetButtonVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsActiveRecColumn", _data.C.TbC_IsActiveRecColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsDeletedColumn", _data.C.TbC_IsDeletedColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsCreatedUserIdColumn", _data.C.TbC_IsCreatedUserIdColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsCreatedDateColumn", _data.C.TbC_IsCreatedDateColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsUserIdColumn", _data.C.TbC_IsUserIdColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsModifiedDateColumn", _data.C.TbC_IsModifiedDateColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsRowVersionStampColumn", _data.C.TbC_IsRowVersionStampColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsBrowsable", _data.C.TbC_IsBrowsable.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_DeveloperNote", _data.C.TbC_DeveloperNote.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_UserNote", _data.C.TbC_UserNote.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_HelpFileAdditionalNote", _data.C.TbC_HelpFileAdditionalNote.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_Notes", _data.C.TbC_Notes.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_IsInputComplete", _data.C.TbC_IsInputComplete.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsCodeGen", _data.C.TbC_IsCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsReadyCodeGen", _data.C.TbC_IsReadyCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsCodeGenComplete", _data.C.TbC_IsCodeGenComplete.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsTagForCodeGen", _data.C.TbC_IsTagForCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsTagForOther", _data.C.TbC_IsTagForOther.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_ColumnGroups", _data.C.TbC_ColumnGroups.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_IsActiveRow", _data.C.TbC_IsActiveRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsDeleted", _data.C.TbC_IsDeleted.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_CreatedUserId", _data.C.TbC_CreatedUserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_CreatedDate", _data.C.TbC_CreatedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("TbC_UserId", _data.C.TbC_UserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("UserName", _data.C.UserName.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_LastModifiedDate", _data.C.TbC_LastModifiedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("TbC_Stamp", _data.C.TbC_Stamp.ToString(), TraceDataTypes.ByteArray);
			GetTraceItemsShortListCustom();
            return _traceItems;
        }

        /// <summary>
        /// 	<para>
        ///         Used in IFX (the instrumentation framework) for tracing. By default, this is
        ///         code-genned to return the same information as <see cref="GetTraceItemsShortList">GetTraceItemsShortList</see>, but is intended for
        ///         developers to add additional informaiton that would be helpful in a trace. This
        ///         method also calls the <see cref="GetTraceItemsLongListCustom">GetTraceItemsLongListCustom</see> method in the
        ///         WcTableColumn.bll.cust.cs code file so a developer can customize the list by adding
        ///         additional items to it.
        ///     </para>
        /// 	<para>
        ///         Remember: <see cref="GetTraceItemsShortList">GetTraceItemsShortList</see> is
        ///         intended for a limited list of items (the basic items) to trace and
        ///         <strong>GetTraceItemsLongList</strong> is intended for a robust and extensive
        ///         list of items to record in the trace.
        ///     </para>
        /// </summary>
        public TraceItemList GetTraceItemsLongList()
        {
            if (null == _traceItems)
            {
                _traceItems = new TraceItemList("WcTableColumn_Bll");
            }
            else
            {
                _traceItems.Clear();
            }
            _traceItems.Add("StateSwitch", StateSwitch.ToString(), TraceDataTypes.String);
            _traceItems.Add("Broken Rules", BrokenRuleManagerProperty.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_Id", _data.C.TbC_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_Tb_Id", _data.C.TbC_Tb_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_auditColumn_Id", _data.C.TbC_auditColumn_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_SortOrder", _data.C.TbC_SortOrder.ToString(), TraceDataTypes.Int16);
			_traceItems.Add("DbSortOrder", _data.C.DbSortOrder.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("IsImported", _data.C.IsImported.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_Name", _data.C.TbC_Name.ToString(), TraceDataTypes.String);
			_traceItems.Add("ColumnInDatabase", _data.C.ColumnInDatabase.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_CtlTp_Id", _data.C.TbC_CtlTp_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("TbC_CtlTp_Id_TextField", _data.C.TbC_CtlTp_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_IsNonvColumn", _data.C.TbC_IsNonvColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_Description", _data.C.TbC_Description.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_DtSql_Id", _data.C.TbC_DtSql_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("TbC_DtSql_Id_TextField", _data.C.TbC_DtSql_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_DtDtNt_Id", _data.C.TbC_DtDtNt_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("TbC_DtDtNt_Id_TextField", _data.C.TbC_DtDtNt_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_Length", _data.C.TbC_Length.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("TbC_Precision", _data.C.TbC_Precision.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("TbC_Scale", _data.C.TbC_Scale.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("TbC_IsPK", _data.C.TbC_IsPK.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsIdentity", _data.C.TbC_IsIdentity.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsFK", _data.C.TbC_IsFK.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsEntityColumn", _data.C.TbC_IsEntityColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsSystemField", _data.C.TbC_IsSystemField.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsValuesObjectMember", _data.C.TbC_IsValuesObjectMember.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsInPropsScreen", _data.C.TbC_IsInPropsScreen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsInNavList", _data.C.TbC_IsInNavList.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsRequired", _data.C.TbC_IsRequired.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_BrokenRuleText", _data.C.TbC_BrokenRuleText.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_AllowZero", _data.C.TbC_AllowZero.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsNullableInDb", _data.C.TbC_IsNullableInDb.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsNullableInUI", _data.C.TbC_IsNullableInUI.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_DefaultValue", _data.C.TbC_DefaultValue.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_IsReadFromDb", _data.C.TbC_IsReadFromDb.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsSendToDb", _data.C.TbC_IsSendToDb.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsInsertAllowed", _data.C.TbC_IsInsertAllowed.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsEditAllowed", _data.C.TbC_IsEditAllowed.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsReadOnlyInUI", _data.C.TbC_IsReadOnlyInUI.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_UseForAudit", _data.C.TbC_UseForAudit.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_DefaultCaption", _data.C.TbC_DefaultCaption.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_ColumnHeaderText", _data.C.TbC_ColumnHeaderText.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_LabelCaptionVerbose", _data.C.TbC_LabelCaptionVerbose.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_LabelCaptionGenerate", _data.C.TbC_LabelCaptionGenerate.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tbc_ShowGridColumnToolTip", _data.C.Tbc_ShowGridColumnToolTip.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tbc_ShowPropsToolTip", _data.C.Tbc_ShowPropsToolTip.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tbc_TooltipsRolledUp", _data.C.Tbc_TooltipsRolledUp.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_IsCreatePropsStrings", _data.C.TbC_IsCreatePropsStrings.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsCreateGridStrings", _data.C.TbC_IsCreateGridStrings.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsAvailableForColumnGroups", _data.C.TbC_IsAvailableForColumnGroups.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsTextWrapInProp", _data.C.TbC_IsTextWrapInProp.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsTextWrapInGrid", _data.C.TbC_IsTextWrapInGrid.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_TextBoxFormat", _data.C.TbC_TextBoxFormat.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_TextColumnFormat", _data.C.TbC_TextColumnFormat.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_ColumnWidth", _data.C.TbC_ColumnWidth.ToString(), TraceDataTypes.Double);
			_traceItems.Add("TbC_TextBoxTextAlignment", _data.C.TbC_TextBoxTextAlignment.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_ColumnTextAlignment", _data.C.TbC_ColumnTextAlignment.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_ListStoredProc_Id", _data.C.TbC_ListStoredProc_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_ListStoredProc_Id_TextField", _data.C.TbC_ListStoredProc_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_IsStaticList", _data.C.TbC_IsStaticList.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_UseNotInList", _data.C.TbC_UseNotInList.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_UseListEditBtn", _data.C.TbC_UseListEditBtn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_ParentColumnKey", _data.C.TbC_ParentColumnKey.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_ParentColumnKey_TextField", _data.C.TbC_ParentColumnKey_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_UseDisplayTextFieldProperty", _data.C.TbC_UseDisplayTextFieldProperty.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsDisplayTextFieldProperty", _data.C.TbC_IsDisplayTextFieldProperty.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_ComboListTable_Id", _data.C.TbC_ComboListTable_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_ComboListTable_Id_TextField", _data.C.TbC_ComboListTable_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_ComboListDisplayColumn_Id", _data.C.TbC_ComboListDisplayColumn_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_ComboListDisplayColumn_Id_TextField", _data.C.TbC_ComboListDisplayColumn_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_Combo_MaxDropdownHeight", _data.C.TbC_Combo_MaxDropdownHeight.ToString(), TraceDataTypes.Double);
			_traceItems.Add("TbC_Combo_MaxDropdownWidth", _data.C.TbC_Combo_MaxDropdownWidth.ToString(), TraceDataTypes.Double);
			_traceItems.Add("TbC_Combo_AllowDropdownResizing", _data.C.TbC_Combo_AllowDropdownResizing.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_Combo_IsResetButtonVisible", _data.C.TbC_Combo_IsResetButtonVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsActiveRecColumn", _data.C.TbC_IsActiveRecColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsDeletedColumn", _data.C.TbC_IsDeletedColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsCreatedUserIdColumn", _data.C.TbC_IsCreatedUserIdColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsCreatedDateColumn", _data.C.TbC_IsCreatedDateColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsUserIdColumn", _data.C.TbC_IsUserIdColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsModifiedDateColumn", _data.C.TbC_IsModifiedDateColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsRowVersionStampColumn", _data.C.TbC_IsRowVersionStampColumn.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsBrowsable", _data.C.TbC_IsBrowsable.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_DeveloperNote", _data.C.TbC_DeveloperNote.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_UserNote", _data.C.TbC_UserNote.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_HelpFileAdditionalNote", _data.C.TbC_HelpFileAdditionalNote.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_Notes", _data.C.TbC_Notes.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_IsInputComplete", _data.C.TbC_IsInputComplete.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsCodeGen", _data.C.TbC_IsCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsReadyCodeGen", _data.C.TbC_IsReadyCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsCodeGenComplete", _data.C.TbC_IsCodeGenComplete.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsTagForCodeGen", _data.C.TbC_IsTagForCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsTagForOther", _data.C.TbC_IsTagForOther.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_ColumnGroups", _data.C.TbC_ColumnGroups.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_IsActiveRow", _data.C.TbC_IsActiveRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_IsDeleted", _data.C.TbC_IsDeleted.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("TbC_CreatedUserId", _data.C.TbC_CreatedUserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("TbC_CreatedDate", _data.C.TbC_CreatedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("TbC_UserId", _data.C.TbC_UserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("UserName", _data.C.UserName.ToString(), TraceDataTypes.String);
			_traceItems.Add("TbC_LastModifiedDate", _data.C.TbC_LastModifiedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("TbC_Stamp", _data.C.TbC_Stamp.ToString(), TraceDataTypes.ByteArray);
			GetTraceItemsLongListCustom();
            return _traceItems;
        }

        public object[] GetTraceData()
        {
            object[] data = new object[99];
            data[0] = new object[] { "StateSwitch", StateSwitch.ToString() };
            data[1] = new object[] { "BrokenRuleManagerProperty", BrokenRuleManagerProperty.ToString() };
            data[2] = new object[] { "TbC_Id", _data.C.TbC_Id.ToString() };
            data[3] = new object[] { "TbC_Tb_Id", _data.C.TbC_Tb_Id == null ? "<Null>" : _data.C.TbC_Tb_Id.ToString() };
            data[4] = new object[] { "TbC_auditColumn_Id", _data.C.TbC_auditColumn_Id == null ? "<Null>" : _data.C.TbC_auditColumn_Id.ToString() };
            data[5] = new object[] { "TbC_SortOrder", _data.C.TbC_SortOrder == null ? "<Null>" : _data.C.TbC_SortOrder.ToString() };
            data[6] = new object[] { "DbSortOrder", _data.C.DbSortOrder.ToString() };
            data[7] = new object[] { "IsImported", _data.C.IsImported.ToString() };
            data[8] = new object[] { "TbC_Name", _data.C.TbC_Name == null ? "<Null>" : _data.C.TbC_Name.ToString() };
            data[9] = new object[] { "ColumnInDatabase", _data.C.ColumnInDatabase == null ? "<Null>" : _data.C.ColumnInDatabase.ToString() };
            data[10] = new object[] { "TbC_CtlTp_Id", _data.C.TbC_CtlTp_Id == null ? "<Null>" : _data.C.TbC_CtlTp_Id.ToString() };
            data[11] = new object[] { "TbC_CtlTp_Id_TextField", _data.C.TbC_CtlTp_Id_TextField == null ? "<Null>" : _data.C.TbC_CtlTp_Id_TextField.ToString() };
            data[12] = new object[] { "TbC_IsNonvColumn", _data.C.TbC_IsNonvColumn.ToString() };
            data[13] = new object[] { "TbC_Description", _data.C.TbC_Description == null ? "<Null>" : _data.C.TbC_Description.ToString() };
            data[14] = new object[] { "TbC_DtSql_Id", _data.C.TbC_DtSql_Id == null ? "<Null>" : _data.C.TbC_DtSql_Id.ToString() };
            data[15] = new object[] { "TbC_DtSql_Id_TextField", _data.C.TbC_DtSql_Id_TextField == null ? "<Null>" : _data.C.TbC_DtSql_Id_TextField.ToString() };
            data[16] = new object[] { "TbC_DtDtNt_Id", _data.C.TbC_DtDtNt_Id == null ? "<Null>" : _data.C.TbC_DtDtNt_Id.ToString() };
            data[17] = new object[] { "TbC_DtDtNt_Id_TextField", _data.C.TbC_DtDtNt_Id_TextField == null ? "<Null>" : _data.C.TbC_DtDtNt_Id_TextField.ToString() };
            data[18] = new object[] { "TbC_Length", _data.C.TbC_Length == null ? "<Null>" : _data.C.TbC_Length.ToString() };
            data[19] = new object[] { "TbC_Precision", _data.C.TbC_Precision == null ? "<Null>" : _data.C.TbC_Precision.ToString() };
            data[20] = new object[] { "TbC_Scale", _data.C.TbC_Scale == null ? "<Null>" : _data.C.TbC_Scale.ToString() };
            data[21] = new object[] { "TbC_IsPK", _data.C.TbC_IsPK.ToString() };
            data[22] = new object[] { "TbC_IsIdentity", _data.C.TbC_IsIdentity.ToString() };
            data[23] = new object[] { "TbC_IsFK", _data.C.TbC_IsFK.ToString() };
            data[24] = new object[] { "TbC_IsEntityColumn", _data.C.TbC_IsEntityColumn.ToString() };
            data[25] = new object[] { "TbC_IsSystemField", _data.C.TbC_IsSystemField.ToString() };
            data[26] = new object[] { "TbC_IsValuesObjectMember", _data.C.TbC_IsValuesObjectMember.ToString() };
            data[27] = new object[] { "TbC_IsInPropsScreen", _data.C.TbC_IsInPropsScreen.ToString() };
            data[28] = new object[] { "TbC_IsInNavList", _data.C.TbC_IsInNavList.ToString() };
            data[29] = new object[] { "TbC_IsRequired", _data.C.TbC_IsRequired.ToString() };
            data[30] = new object[] { "TbC_BrokenRuleText", _data.C.TbC_BrokenRuleText == null ? "<Null>" : _data.C.TbC_BrokenRuleText.ToString() };
            data[31] = new object[] { "TbC_AllowZero", _data.C.TbC_AllowZero.ToString() };
            data[32] = new object[] { "TbC_IsNullableInDb", _data.C.TbC_IsNullableInDb.ToString() };
            data[33] = new object[] { "TbC_IsNullableInUI", _data.C.TbC_IsNullableInUI.ToString() };
            data[34] = new object[] { "TbC_DefaultValue", _data.C.TbC_DefaultValue == null ? "<Null>" : _data.C.TbC_DefaultValue.ToString() };
            data[35] = new object[] { "TbC_IsReadFromDb", _data.C.TbC_IsReadFromDb.ToString() };
            data[36] = new object[] { "TbC_IsSendToDb", _data.C.TbC_IsSendToDb.ToString() };
            data[37] = new object[] { "TbC_IsInsertAllowed", _data.C.TbC_IsInsertAllowed.ToString() };
            data[38] = new object[] { "TbC_IsEditAllowed", _data.C.TbC_IsEditAllowed.ToString() };
            data[39] = new object[] { "TbC_IsReadOnlyInUI", _data.C.TbC_IsReadOnlyInUI.ToString() };
            data[40] = new object[] { "TbC_UseForAudit", _data.C.TbC_UseForAudit.ToString() };
            data[41] = new object[] { "TbC_DefaultCaption", _data.C.TbC_DefaultCaption == null ? "<Null>" : _data.C.TbC_DefaultCaption.ToString() };
            data[42] = new object[] { "TbC_ColumnHeaderText", _data.C.TbC_ColumnHeaderText == null ? "<Null>" : _data.C.TbC_ColumnHeaderText.ToString() };
            data[43] = new object[] { "TbC_LabelCaptionVerbose", _data.C.TbC_LabelCaptionVerbose == null ? "<Null>" : _data.C.TbC_LabelCaptionVerbose.ToString() };
            data[44] = new object[] { "TbC_LabelCaptionGenerate", _data.C.TbC_LabelCaptionGenerate.ToString() };
            data[45] = new object[] { "Tbc_ShowGridColumnToolTip", _data.C.Tbc_ShowGridColumnToolTip.ToString() };
            data[46] = new object[] { "Tbc_ShowPropsToolTip", _data.C.Tbc_ShowPropsToolTip.ToString() };
            data[47] = new object[] { "Tbc_TooltipsRolledUp", _data.C.Tbc_TooltipsRolledUp == null ? "<Null>" : _data.C.Tbc_TooltipsRolledUp.ToString() };
            data[48] = new object[] { "TbC_IsCreatePropsStrings", _data.C.TbC_IsCreatePropsStrings.ToString() };
            data[49] = new object[] { "TbC_IsCreateGridStrings", _data.C.TbC_IsCreateGridStrings.ToString() };
            data[50] = new object[] { "TbC_IsAvailableForColumnGroups", _data.C.TbC_IsAvailableForColumnGroups.ToString() };
            data[51] = new object[] { "TbC_IsTextWrapInProp", _data.C.TbC_IsTextWrapInProp.ToString() };
            data[52] = new object[] { "TbC_IsTextWrapInGrid", _data.C.TbC_IsTextWrapInGrid.ToString() };
            data[53] = new object[] { "TbC_TextBoxFormat", _data.C.TbC_TextBoxFormat == null ? "<Null>" : _data.C.TbC_TextBoxFormat.ToString() };
            data[54] = new object[] { "TbC_TextColumnFormat", _data.C.TbC_TextColumnFormat == null ? "<Null>" : _data.C.TbC_TextColumnFormat.ToString() };
            data[55] = new object[] { "TbC_ColumnWidth", _data.C.TbC_ColumnWidth.ToString() };
            data[56] = new object[] { "TbC_TextBoxTextAlignment", _data.C.TbC_TextBoxTextAlignment == null ? "<Null>" : _data.C.TbC_TextBoxTextAlignment.ToString() };
            data[57] = new object[] { "TbC_ColumnTextAlignment", _data.C.TbC_ColumnTextAlignment == null ? "<Null>" : _data.C.TbC_ColumnTextAlignment.ToString() };
            data[58] = new object[] { "TbC_ListStoredProc_Id", _data.C.TbC_ListStoredProc_Id == null ? "<Null>" : _data.C.TbC_ListStoredProc_Id.ToString() };
            data[59] = new object[] { "TbC_ListStoredProc_Id_TextField", _data.C.TbC_ListStoredProc_Id_TextField == null ? "<Null>" : _data.C.TbC_ListStoredProc_Id_TextField.ToString() };
            data[60] = new object[] { "TbC_IsStaticList", _data.C.TbC_IsStaticList.ToString() };
            data[61] = new object[] { "TbC_UseNotInList", _data.C.TbC_UseNotInList.ToString() };
            data[62] = new object[] { "TbC_UseListEditBtn", _data.C.TbC_UseListEditBtn.ToString() };
            data[63] = new object[] { "TbC_ParentColumnKey", _data.C.TbC_ParentColumnKey == null ? "<Null>" : _data.C.TbC_ParentColumnKey.ToString() };
            data[64] = new object[] { "TbC_ParentColumnKey_TextField", _data.C.TbC_ParentColumnKey_TextField == null ? "<Null>" : _data.C.TbC_ParentColumnKey_TextField.ToString() };
            data[65] = new object[] { "TbC_UseDisplayTextFieldProperty", _data.C.TbC_UseDisplayTextFieldProperty.ToString() };
            data[66] = new object[] { "TbC_IsDisplayTextFieldProperty", _data.C.TbC_IsDisplayTextFieldProperty.ToString() };
            data[67] = new object[] { "TbC_ComboListTable_Id", _data.C.TbC_ComboListTable_Id == null ? "<Null>" : _data.C.TbC_ComboListTable_Id.ToString() };
            data[68] = new object[] { "TbC_ComboListTable_Id_TextField", _data.C.TbC_ComboListTable_Id_TextField == null ? "<Null>" : _data.C.TbC_ComboListTable_Id_TextField.ToString() };
            data[69] = new object[] { "TbC_ComboListDisplayColumn_Id", _data.C.TbC_ComboListDisplayColumn_Id == null ? "<Null>" : _data.C.TbC_ComboListDisplayColumn_Id.ToString() };
            data[70] = new object[] { "TbC_ComboListDisplayColumn_Id_TextField", _data.C.TbC_ComboListDisplayColumn_Id_TextField == null ? "<Null>" : _data.C.TbC_ComboListDisplayColumn_Id_TextField.ToString() };
            data[71] = new object[] { "TbC_Combo_MaxDropdownHeight", _data.C.TbC_Combo_MaxDropdownHeight.ToString() };
            data[72] = new object[] { "TbC_Combo_MaxDropdownWidth", _data.C.TbC_Combo_MaxDropdownWidth.ToString() };
            data[73] = new object[] { "TbC_Combo_AllowDropdownResizing", _data.C.TbC_Combo_AllowDropdownResizing.ToString() };
            data[74] = new object[] { "TbC_Combo_IsResetButtonVisible", _data.C.TbC_Combo_IsResetButtonVisible.ToString() };
            data[75] = new object[] { "TbC_IsActiveRecColumn", _data.C.TbC_IsActiveRecColumn.ToString() };
            data[76] = new object[] { "TbC_IsDeletedColumn", _data.C.TbC_IsDeletedColumn.ToString() };
            data[77] = new object[] { "TbC_IsCreatedUserIdColumn", _data.C.TbC_IsCreatedUserIdColumn.ToString() };
            data[78] = new object[] { "TbC_IsCreatedDateColumn", _data.C.TbC_IsCreatedDateColumn.ToString() };
            data[79] = new object[] { "TbC_IsUserIdColumn", _data.C.TbC_IsUserIdColumn.ToString() };
            data[80] = new object[] { "TbC_IsModifiedDateColumn", _data.C.TbC_IsModifiedDateColumn.ToString() };
            data[81] = new object[] { "TbC_IsRowVersionStampColumn", _data.C.TbC_IsRowVersionStampColumn.ToString() };
            data[82] = new object[] { "TbC_IsBrowsable", _data.C.TbC_IsBrowsable.ToString() };
            data[83] = new object[] { "TbC_DeveloperNote", _data.C.TbC_DeveloperNote == null ? "<Null>" : _data.C.TbC_DeveloperNote.ToString() };
            data[84] = new object[] { "TbC_UserNote", _data.C.TbC_UserNote == null ? "<Null>" : _data.C.TbC_UserNote.ToString() };
            data[85] = new object[] { "TbC_HelpFileAdditionalNote", _data.C.TbC_HelpFileAdditionalNote == null ? "<Null>" : _data.C.TbC_HelpFileAdditionalNote.ToString() };
            data[86] = new object[] { "TbC_Notes", _data.C.TbC_Notes == null ? "<Null>" : _data.C.TbC_Notes.ToString() };
            data[87] = new object[] { "TbC_IsInputComplete", _data.C.TbC_IsInputComplete.ToString() };
            data[88] = new object[] { "TbC_IsCodeGen", _data.C.TbC_IsCodeGen.ToString() };
            data[89] = new object[] { "TbC_IsReadyCodeGen", _data.C.TbC_IsReadyCodeGen.ToString() };
            data[90] = new object[] { "TbC_IsCodeGenComplete", _data.C.TbC_IsCodeGenComplete.ToString() };
            data[91] = new object[] { "TbC_IsTagForCodeGen", _data.C.TbC_IsTagForCodeGen.ToString() };
            data[92] = new object[] { "TbC_IsTagForOther", _data.C.TbC_IsTagForOther.ToString() };
            data[93] = new object[] { "TbC_ColumnGroups", _data.C.TbC_ColumnGroups == null ? "<Null>" : _data.C.TbC_ColumnGroups.ToString() };
            data[94] = new object[] { "TbC_IsActiveRow", _data.C.TbC_IsActiveRow.ToString() };
            data[95] = new object[] { "TbC_IsDeleted", _data.C.TbC_IsDeleted.ToString() };
            data[96] = new object[] { "TbC_CreatedUserId", _data.C.TbC_CreatedUserId == null ? "<Null>" : _data.C.TbC_CreatedUserId.ToString() };
            data[97] = new object[] { "TbC_CreatedDate", _data.C.TbC_CreatedDate == null ? "<Null>" : _data.C.TbC_CreatedDate.ToString() };
            data[98] = new object[] { "TbC_UserId", _data.C.TbC_UserId == null ? "<Null>" : _data.C.TbC_UserId.ToString() };
            data[99] = new object[] { "UserName", _data.C.UserName == null ? "<Null>" : _data.C.UserName.ToString() };
            data[100] = new object[] { "TbC_LastModifiedDate", _data.C.TbC_LastModifiedDate == null ? "<Null>" : _data.C.TbC_LastModifiedDate.ToString() };
            return data;
        }


		#endregion ITraceItem Members


		#region INotifyPropertyChanged Members

        /// <summary>
        /// 	<para>
        ///         This method raises the <see cref="PropertyChanged">PropertyChanged</see> event
        ///         and is required by the INotifyPropertyChanged interface. It’s called in nearly
        ///         all public data field properties.
        ///     </para>
        /// 	<para>The <strong>INotifyPropertyChanged</strong> interface is used to notify
        ///     clients, typically binding clients, that a property value has changed. For more
        ///     information, search INotifyPropertyChanged on <strong>MSDN</strong>.</para>
        /// </summary>
        protected void Notify(string propName)
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }


		#endregion INotifyPropertyChanged Members


		#region INotifyDataErrorInfo Members

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        //public void RaiseErrorsChanged(string propertyName)
        //{
        //    if (ErrorsChanged != null)
        //        ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        //}

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            //throw new NotImplementedException();
            if (_brokenRuleManager.GetBrokenRulesForProperty(propertyName).Count == 0)
            {
                return "";
            }
            else
            {

                return _brokenRuleManager.GetBrokenRulesForProperty(propertyName)[0].ToString();
            }
        }

        public bool HasErrors
        {
            get { return _brokenRuleManager.IsEntityValid(); }
        }


		#endregion INotifyDataErrorInfo Members


        #region List Methods

        #region IEditableObject Members

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="CancelEdit">CancelEdit</see> and <see cref="EndEdit">EndEdit</see>.
        ///     </para>
        /// 	<para>Begins an edit on an object.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void BeginEdit()
        {
//            Guid? traceId = Guid.NewGuid();
//            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BeginEdit", IfxTraceCategory.Enter);
//            Console.WriteLine("BeginEdit");
//            //***  ToDo:
//            // Set the parent type below, then when properties are being edited, raise an event to sync fields in the props control.
//            _parentType = ParentEditObjectType.EntitiyListControl;
//
//            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BeginEdit", IfxTraceCategory.Leave);
        }

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="BeginEdit">BeginEdit</see> and <see cref="EndEdit">EndEdit</see>.
        ///     </para>
        /// 	<para>Discards changes since the last BeginEdit call.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void CancelEdit()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelEdit", IfxTraceCategory.Enter);
            UnDo();
            _parentType = ParentEditObjectType.None;

            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelEdit", IfxTraceCategory.Leave);
        }

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="BeginEdit">BeginEdit</see> and <see cref="CancelEdit">CancelEdit</see>.
        ///     </para>
        /// 	<para>Pushes changes since the last BeginEdit or IBindingList.AddNew call into the
        ///     underlying object.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void EndEdit()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", IfxTraceCategory.Enter);
//                if (_data.S.IsDirty() && _data.S.IsValid())
//                {
//                    // ToDo - This needs more testing for Silverlight
//                    Save(null, ParentEditObjectType.EntitiyListControl, UseConcurrencyCheck.UseDefaultSetting);
//
//                    //DataServiceInsertUpdateResponseClientSide result = Save(UseConcurrencyCheck.UseDefaultSetting);
//                    //if (result.Result != DataOperationResult.Success)
//                    //{
//                    //    UnDo();
//                    //    Debugger.Break();
//                    //}
//                }
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", IfxTraceCategory.Leave);
//            }
        }

        #endregion IEditableObject Members

        #endregion List Methods



        #region Children_WcToolTip


        WcToolTip_List _children_WcToolTip = new WcToolTip_List();


        public WcToolTip_List Children_WcToolTip
        {
            get { return _children_WcToolTip; }
            set { _children_WcToolTip = value; }
        }

        public void Get_Children_WcToolTip()
        {
            _proxyWcToolTipService.Begin_WcToolTip_GetListByFK(TbC_Id);
        }
        

        void _proxyWcToolTipService_WcToolTip_GetListByFKCompleted(object sender, WcToolTip_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcToolTipService_WcToolTip_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array != null)
                {
                    Children_WcToolTip.ReplaceList(array);
                }
                else
                {
                    Children_WcToolTip.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcToolTipService_WcToolTip_GetListByFKCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcToolTipService_WcToolTip_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }


            #endregion Children_WcToolTip

            

        #region Children_WcTableColumnComboColumn


        WcTableColumnComboColumn_List _children_WcTableColumnComboColumn = new WcTableColumnComboColumn_List();


        public WcTableColumnComboColumn_List Children_WcTableColumnComboColumn
        {
            get { return _children_WcTableColumnComboColumn; }
            set { _children_WcTableColumnComboColumn = value; }
        }

        public void Get_Children_WcTableColumnComboColumn()
        {
            _proxyWcTableColumnComboColumnService.Begin_WcTableColumnComboColumn_GetListByFK(TbC_Id);
        }
        

        void _proxyWcTableColumnComboColumnService_WcTableColumnComboColumn_GetListByFKCompleted(object sender, WcTableColumnComboColumn_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableColumnComboColumnService_WcTableColumnComboColumn_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array != null)
                {
                    Children_WcTableColumnComboColumn.ReplaceList(array);
                }
                else
                {
                    Children_WcTableColumnComboColumn.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableColumnComboColumnService_WcTableColumnComboColumn_GetListByFKCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableColumnComboColumnService_WcTableColumnComboColumn_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }


            #endregion Children_WcTableColumnComboColumn

            

//        #region IDataErrorInfo Members
//
//        /// <summary>
//        /// 	<para>Part of the <strong>IDataErrorInfo</strong> Interface. Gets an error message
//        ///     indicating what is wrong with this object.</para>
//        /// 	<para>For more information, search <strong>IDataErrorInfo</strong> on
//        ///     <strong>MSDN</strong>.</para>
//        /// </summary>
//        public string Error
//        {
//            get { throw new NotImplementedException(); }
//        }
//
//        /// <summary>
//        /// 	<para>Part of the <strong>IDataErrorInfo</strong> Interface. Gets the error message
//        ///     for the property with the given name.</para>
//        /// 	<para>For more information, search <strong>IDataErrorInfo</strong> on
//        ///     <strong>MSDN</strong>.</para>
//        /// </summary>
//        string IDataErrorInfo.this[string columnName]
//        {
//            get
//            {
//                if (!_brokenRuleManager.IsPropertyValid(columnName))
//                {
//                    //string err = "line 1 First validation msg" + Environment.NewLine + "line 2 Second validation msg";
//                    //return err;
//                    return _brokenRuleManager.GetBrokenRulesForProperty(columnName)[0].ToString();
//                }
//                else
//                {
//                    return null;
//                }
//            }
//        }
//
//        #endregion



    }

}



