using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/12/2018 5:07:57 PM

namespace EntityBll.SL
{
    public partial class WcTableColumn_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_TbC_Name = 100;
		public const int STRINGSIZE_ColumnInDatabase = 128;
		public const int STRINGSIZE_TbC_Description = 1000;
		public const int STRINGSIZE_TbC_BrokenRuleText = 255;
		public const int STRINGSIZE_TbC_DefaultValue = 1000;
		public const int STRINGSIZE_TbC_DefaultCaption = 100;
		public const int STRINGSIZE_TbC_ColumnHeaderText = 100;
		public const int STRINGSIZE_TbC_LabelCaptionVerbose = 250;
		public const int STRINGSIZE_Tbc_TooltipsRolledUp = 4000;
		public const int STRINGSIZE_TbC_TextBoxFormat = 50;
		public const int STRINGSIZE_TbC_TextColumnFormat = 50;
		public const int STRINGSIZE_TbC_TextBoxTextAlignment = 10;
		public const int STRINGSIZE_TbC_ColumnTextAlignment = 10;
		public const int STRINGSIZE_TbC_DeveloperNote = 1000;
		public const int STRINGSIZE_TbC_UserNote = 1000;
		public const int STRINGSIZE_TbC_HelpFileAdditionalNote = 1000;
		public const int STRINGSIZE_TbC_Notes = 1000;
		public const int STRINGSIZE_TbC_ColumnGroups = 500;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_TbC_Id_Required = "Id is a required field.";
		private const string BROKENRULE_TbC_SortOrder_Required = "Sort Order is a required field.";
		private const string BROKENRULE_TbC_SortOrder_ZeroNotAllowed = "Sort Order:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_DbSortOrder_Required = "Db Sort Order is a required field.";
		private const string BROKENRULE_IsImported_Required = "Is Imported is a required field.";
		private string BROKENRULE_TbC_Name_TextLength = "WC Column Name has a maximum text length of  '" + STRINGSIZE_TbC_Name + "'.";
		private const string BROKENRULE_TbC_Name_Required = "WC Column Name is a required field.";
		private string BROKENRULE_ColumnInDatabase_TextLength = "Column In Source Db has a maximum text length of  '" + STRINGSIZE_ColumnInDatabase + "'.";
		private const string BROKENRULE_TbC_CtlTp_Id_Required = "Control Type is a required field.";
		private const string BROKENRULE_TbC_IsNonvColumn_Required = "Is Non-vColumn is a required field.";
		private string BROKENRULE_TbC_Description_TextLength = "Description has a maximum text length of  '" + STRINGSIZE_TbC_Description + "'.";
		private const string BROKENRULE_TbC_DtSql_Id_Required = "SQL Data Type is a required field.";
		private const string BROKENRULE_TbC_DtSql_Id_ZeroNotAllowed = "SQL Data Type:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_TbC_DtNt_Id_ZeroNotAllowed = "DotNet Data Type:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_TbC_Scale_ZeroNotAllowed = "Scale:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_TbC_IsPK_Required = "Is The Primary Key is a required field.";
		private const string BROKENRULE_TbC_IsIdentity_Required = "Is Identity is a required field.";
		private const string BROKENRULE_TbC_IsFK_Required = "Is The Foreign Key is a required field.";
		private const string BROKENRULE_TbC_IsEntityColumn_Required = "Is Entity Column is a required field.";
		private const string BROKENRULE_TbC_IsSystemField_Required = "Is System Field is a required field.";
		private const string BROKENRULE_TbC_IsValuesObjectMember_Required = "Is Values Object Member is a required field.";
		private const string BROKENRULE_TbC_IsInPropsScreen_Required = "Is In Props Screen is a required field.";
		private const string BROKENRULE_TbC_IsInNavList_Required = "Is In NavList is a required field.";
		private const string BROKENRULE_TbC_IsRequired_Required = "Is Required is a required field.";
		private string BROKENRULE_TbC_BrokenRuleText_TextLength = "Broken Rule Text has a maximum text length of  '" + STRINGSIZE_TbC_BrokenRuleText + "'.";
		private const string BROKENRULE_TbC_AllowZero_Required = "Allow Zero is a required field.";
		private const string BROKENRULE_TbC_IsNullableInDb_Required = "Is Nullable In Db is a required field.";
		private const string BROKENRULE_TbC_IsNullableInUI_Required = "Is Nullable In UI is a required field.";
		private string BROKENRULE_TbC_DefaultValue_TextLength = "Default Value has a maximum text length of  '" + STRINGSIZE_TbC_DefaultValue + "'.";
		private const string BROKENRULE_TbC_IsReadFromDb_Required = "Is Read From Db is a required field.";
		private const string BROKENRULE_TbC_IsSendToDb_Required = "Is Send To Db is a required field.";
		private const string BROKENRULE_TbC_IsInsertAllowed_Required = "Is Insert Allowed is a required field.";
		private const string BROKENRULE_TbC_IsEditAllowed_Required = "Is Edit Allowed is a required field.";
		private const string BROKENRULE_TbC_IsReadOnlyInUI_Required = "Is Read Only In The UI is a required field.";
		private const string BROKENRULE_TbC_UseForAudit_Required = "Use For Audit is a required field.";
		private string BROKENRULE_TbC_DefaultCaption_TextLength = "Default Caption has a maximum text length of  '" + STRINGSIZE_TbC_DefaultCaption + "'.";
		private string BROKENRULE_TbC_ColumnHeaderText_TextLength = "Column Header Text has a maximum text length of  '" + STRINGSIZE_TbC_ColumnHeaderText + "'.";
		private string BROKENRULE_TbC_LabelCaptionVerbose_TextLength = "Verbose Caption has a maximum text length of  '" + STRINGSIZE_TbC_LabelCaptionVerbose + "'.";
		private const string BROKENRULE_TbC_LabelCaptionGenerate_Required = "Generate Label Caption is a required field.";
		private const string BROKENRULE_Tbc_ShowGridColumnToolTip_Required = "Show ToolTip In Grid Column is a required field.";
		private const string BROKENRULE_Tbc_ShowPropsToolTip_Required = "Show Props Tooltip is a required field.";
		private string BROKENRULE_Tbc_TooltipsRolledUp_TextLength = "Tooltips Rolled Up has a maximum text length of  '" + STRINGSIZE_Tbc_TooltipsRolledUp + "'.";
		private const string BROKENRULE_TbC_IsCreatePropsStrings_Required = "Create Props Resource Strings is a required field.";
		private const string BROKENRULE_TbC_IsCreateGridStrings_Required = "Create Grid Col. Resource Strings is a required field.";
		private const string BROKENRULE_TbC_IsAvailableForColumnGroups_Required = "Is Available For Column Groups is a required field.";
		private const string BROKENRULE_TbC_IsTextWrapInProp_Required = "Is TextWrap In Props is a required field.";
		private const string BROKENRULE_TbC_IsTextWrapInGrid_Required = "Is TextWrap In Grid is a required field.";
		private string BROKENRULE_TbC_TextBoxFormat_TextLength = "TextBox Format has a maximum text length of  '" + STRINGSIZE_TbC_TextBoxFormat + "'.";
		private string BROKENRULE_TbC_TextColumnFormat_TextLength = "TextColumn Format has a maximum text length of  '" + STRINGSIZE_TbC_TextColumnFormat + "'.";
		private const string BROKENRULE_TbC_ColumnWidth_Required = "Column Width is a required field.";
		private const string BROKENRULE_TbC_ColumnWidth_ZeroNotAllowed = "Column Width:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_TbC_ColumnWidth_NullNotAllowed = "Column Width 'Null' is not allowed.";
		private string BROKENRULE_TbC_TextBoxTextAlignment_TextLength = "TextBox TextAlignment has a maximum text length of  '" + STRINGSIZE_TbC_TextBoxTextAlignment + "'.";
		private string BROKENRULE_TbC_ColumnTextAlignment_TextLength = "Column TextAlignment has a maximum text length of  '" + STRINGSIZE_TbC_ColumnTextAlignment + "'.";
		private const string BROKENRULE_TbC_IsStaticList_Required = "Is Static List is a required field.";
		private const string BROKENRULE_TbC_UseNotInList_Required = "Use Not-In-List is a required field.";
		private const string BROKENRULE_TbC_UseListEditBtn_Required = "Use List Edit Button is a required field.";
		private const string BROKENRULE_TbC_UseDisplayTextFieldProperty_Required = "Use DisplayTextField Property is a required field.";
		private const string BROKENRULE_TbC_IsDisplayTextFieldProperty_Required = "Is DisplayTextField Property is a required field.";
		private const string BROKENRULE_TbC_Combo_MaxDropdownHeight_Required = "Combo Max Dropdown Height is a required field.";
		private const string BROKENRULE_TbC_Combo_MaxDropdownHeight_NullNotAllowed = "Combo Max Dropdown Height 'Null' is not allowed.";
		private const string BROKENRULE_TbC_Combo_MaxDropdownWidth_Required = "Combo Max Dropdown Width is a required field.";
		private const string BROKENRULE_TbC_Combo_MaxDropdownWidth_NullNotAllowed = "Combo Max Dropdown Width 'Null' is not allowed.";
		private const string BROKENRULE_TbC_Combo_AllowDropdownResizing_Required = "Allow Dropdown Resizing is a required field.";
		private const string BROKENRULE_TbC_Combo_IsResetButtonVisible_Required = "Is Reset Button Visible is a required field.";
		private const string BROKENRULE_TbC_IsActiveRecColumn_Required = "Is Active Rec Column is a required field.";
		private const string BROKENRULE_TbC_IsDeletedColumn_Required = "Is Deleted Column is a required field.";
		private const string BROKENRULE_TbC_IsCreatedUserIdColumn_Required = "Is Created UserId Column is a required field.";
		private const string BROKENRULE_TbC_IsCreatedDateColumn_Required = "Is Created Date Column is a required field.";
		private const string BROKENRULE_TbC_IsUserIdColumn_Required = "Is Last Modified by UserId Column is a required field.";
		private const string BROKENRULE_TbC_IsModifiedDateColumn_Required = "Is Last Modified Date Column is a required field.";
		private const string BROKENRULE_TbC_IsRowVersionStampColumn_Required = "Is Row Version Stamp Column is a required field.";
		private const string BROKENRULE_TbC_IsBrowsable_Required = "Is Browsable is a required field.";
		private string BROKENRULE_TbC_DeveloperNote_TextLength = "Developer Note has a maximum text length of  '" + STRINGSIZE_TbC_DeveloperNote + "'.";
		private string BROKENRULE_TbC_UserNote_TextLength = "User Note has a maximum text length of  '" + STRINGSIZE_TbC_UserNote + "'.";
		private string BROKENRULE_TbC_HelpFileAdditionalNote_TextLength = "Help File Additional Note has a maximum text length of  '" + STRINGSIZE_TbC_HelpFileAdditionalNote + "'.";
		private string BROKENRULE_TbC_Notes_TextLength = "Notes has a maximum text length of  '" + STRINGSIZE_TbC_Notes + "'.";
		private const string BROKENRULE_TbC_IsInputComplete_Required = "Input Complete is a required field.";
		private const string BROKENRULE_TbC_IsCodeGen_Required = "Is CodeGen is a required field.";
		private const string BROKENRULE_TbC_IsReadyCodeGen_Required = "Is Ready For CodeGen is a required field.";
		private const string BROKENRULE_TbC_IsCodeGenComplete_Required = "Is CodeGen Complete is a required field.";
		private const string BROKENRULE_TbC_IsTagForCodeGen_Required = "Is Tag For CodeGen is a required field.";
		private const string BROKENRULE_TbC_IsTagForOther_Required = "Is Tag For Other is a required field.";
		private string BROKENRULE_TbC_ColumnGroups_TextLength = "Table Column Groups has a maximum text length of  '" + STRINGSIZE_TbC_ColumnGroups + "'.";
		private const string BROKENRULE_TbC_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_TbC_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_TbC_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_Id", BROKENRULE_TbC_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_Tb_Id", BROKENRULE_TbC_Tb_Id_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("TbC_SortOrder", BROKENRULE_TbC_SortOrder_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_SortOrder", BROKENRULE_TbC_SortOrder_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("DbSortOrder", BROKENRULE_DbSortOrder_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("IsImported", BROKENRULE_IsImported_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_Name", BROKENRULE_TbC_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("TbC_Name", BROKENRULE_TbC_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ColumnInDatabase", BROKENRULE_ColumnInDatabase_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("TbC_CtlTp_Id", BROKENRULE_TbC_CtlTp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsNonvColumn", BROKENRULE_TbC_IsNonvColumn_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_Description", BROKENRULE_TbC_Description_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("TbC_DtSql_Id", BROKENRULE_TbC_DtSql_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_DtSql_Id", BROKENRULE_TbC_DtSql_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_DtNt_Id", BROKENRULE_TbC_DtNt_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_Scale", BROKENRULE_TbC_Scale_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsPK", BROKENRULE_TbC_IsPK_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsIdentity", BROKENRULE_TbC_IsIdentity_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsFK", BROKENRULE_TbC_IsFK_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsEntityColumn", BROKENRULE_TbC_IsEntityColumn_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsSystemField", BROKENRULE_TbC_IsSystemField_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsValuesObjectMember", BROKENRULE_TbC_IsValuesObjectMember_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsInPropsScreen", BROKENRULE_TbC_IsInPropsScreen_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsInNavList", BROKENRULE_TbC_IsInNavList_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsRequired", BROKENRULE_TbC_IsRequired_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_BrokenRuleText", BROKENRULE_TbC_BrokenRuleText_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_AllowZero", BROKENRULE_TbC_AllowZero_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsNullableInDb", BROKENRULE_TbC_IsNullableInDb_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsNullableInUI", BROKENRULE_TbC_IsNullableInUI_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_DefaultValue", BROKENRULE_TbC_DefaultValue_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsReadFromDb", BROKENRULE_TbC_IsReadFromDb_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsSendToDb", BROKENRULE_TbC_IsSendToDb_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsInsertAllowed", BROKENRULE_TbC_IsInsertAllowed_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsEditAllowed", BROKENRULE_TbC_IsEditAllowed_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsReadOnlyInUI", BROKENRULE_TbC_IsReadOnlyInUI_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_UseForAudit", BROKENRULE_TbC_UseForAudit_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_DefaultCaption", BROKENRULE_TbC_DefaultCaption_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_ColumnHeaderText", BROKENRULE_TbC_ColumnHeaderText_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_LabelCaptionVerbose", BROKENRULE_TbC_LabelCaptionVerbose_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_LabelCaptionGenerate", BROKENRULE_TbC_LabelCaptionGenerate_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tbc_ShowGridColumnToolTip", BROKENRULE_Tbc_ShowGridColumnToolTip_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tbc_ShowPropsToolTip", BROKENRULE_Tbc_ShowPropsToolTip_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tbc_TooltipsRolledUp", BROKENRULE_Tbc_TooltipsRolledUp_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsCreatePropsStrings", BROKENRULE_TbC_IsCreatePropsStrings_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsCreateGridStrings", BROKENRULE_TbC_IsCreateGridStrings_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsAvailableForColumnGroups", BROKENRULE_TbC_IsAvailableForColumnGroups_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsTextWrapInProp", BROKENRULE_TbC_IsTextWrapInProp_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsTextWrapInGrid", BROKENRULE_TbC_IsTextWrapInGrid_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_TextBoxFormat", BROKENRULE_TbC_TextBoxFormat_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_TextColumnFormat", BROKENRULE_TbC_TextColumnFormat_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_ColumnWidth", BROKENRULE_TbC_ColumnWidth_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_ColumnWidth", BROKENRULE_TbC_ColumnWidth_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_ColumnWidth", BROKENRULE_TbC_ColumnWidth_NullNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_TextBoxTextAlignment", BROKENRULE_TbC_TextBoxTextAlignment_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_ColumnTextAlignment", BROKENRULE_TbC_ColumnTextAlignment_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsStaticList", BROKENRULE_TbC_IsStaticList_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_UseNotInList", BROKENRULE_TbC_UseNotInList_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_UseListEditBtn", BROKENRULE_TbC_UseListEditBtn_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_UseDisplayTextFieldProperty", BROKENRULE_TbC_UseDisplayTextFieldProperty_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsDisplayTextFieldProperty", BROKENRULE_TbC_IsDisplayTextFieldProperty_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_Combo_MaxDropdownHeight", BROKENRULE_TbC_Combo_MaxDropdownHeight_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_Combo_MaxDropdownHeight", BROKENRULE_TbC_Combo_MaxDropdownHeight_NullNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_Combo_MaxDropdownWidth", BROKENRULE_TbC_Combo_MaxDropdownWidth_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_Combo_MaxDropdownWidth", BROKENRULE_TbC_Combo_MaxDropdownWidth_NullNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_Combo_AllowDropdownResizing", BROKENRULE_TbC_Combo_AllowDropdownResizing_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_Combo_IsResetButtonVisible", BROKENRULE_TbC_Combo_IsResetButtonVisible_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsActiveRecColumn", BROKENRULE_TbC_IsActiveRecColumn_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsDeletedColumn", BROKENRULE_TbC_IsDeletedColumn_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsCreatedUserIdColumn", BROKENRULE_TbC_IsCreatedUserIdColumn_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsCreatedDateColumn", BROKENRULE_TbC_IsCreatedDateColumn_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsUserIdColumn", BROKENRULE_TbC_IsUserIdColumn_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsModifiedDateColumn", BROKENRULE_TbC_IsModifiedDateColumn_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsRowVersionStampColumn", BROKENRULE_TbC_IsRowVersionStampColumn_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsBrowsable", BROKENRULE_TbC_IsBrowsable_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_DeveloperNote", BROKENRULE_TbC_DeveloperNote_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_UserNote", BROKENRULE_TbC_UserNote_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_HelpFileAdditionalNote", BROKENRULE_TbC_HelpFileAdditionalNote_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_Notes", BROKENRULE_TbC_Notes_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsInputComplete", BROKENRULE_TbC_IsInputComplete_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsCodeGen", BROKENRULE_TbC_IsCodeGen_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsReadyCodeGen", BROKENRULE_TbC_IsReadyCodeGen_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsCodeGenComplete", BROKENRULE_TbC_IsCodeGenComplete_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsTagForCodeGen", BROKENRULE_TbC_IsTagForCodeGen_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsTagForOther", BROKENRULE_TbC_IsTagForOther_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_ColumnGroups", BROKENRULE_TbC_ColumnGroups_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsActiveRow", BROKENRULE_TbC_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_IsDeleted", BROKENRULE_TbC_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbC_Stamp", BROKENRULE_TbC_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void TbC_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Id");
                string newBrokenRules = "";
                
                if (TbC_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_Id", BROKENRULE_TbC_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Id", BROKENRULE_TbC_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_Id", _brokenRuleManager.IsPropertyValid("TbC_Id"), IsPropertyDirty("TbC_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_Tb_Id_Validate()
        {
        }

        private void TbC_auditColumn_Id_Validate()
        {
        }

        private void ImportColumn_Validate()
        {
        }

        private void TbC_SortOrder_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_SortOrder_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_SortOrder");
                string newBrokenRules = "";
                
                if (TbC_SortOrder == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_SortOrder", BROKENRULE_TbC_SortOrder_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_SortOrder", BROKENRULE_TbC_SortOrder_Required);
                }

                if (TbC_SortOrder == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_SortOrder", BROKENRULE_TbC_SortOrder_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_SortOrder", BROKENRULE_TbC_SortOrder_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_SortOrder");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_SortOrder", _brokenRuleManager.IsPropertyValid("TbC_SortOrder"), IsPropertyDirty("TbC_SortOrder"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_SortOrder_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_SortOrder_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DbSortOrder_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DbSortOrder_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbSortOrder");
                string newBrokenRules = "";
                
                if (DbSortOrder == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DbSortOrder", BROKENRULE_DbSortOrder_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DbSortOrder", BROKENRULE_DbSortOrder_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbSortOrder");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DbSortOrder", _brokenRuleManager.IsPropertyValid("DbSortOrder"), IsPropertyDirty("DbSortOrder"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DbSortOrder_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DbSortOrder_Validate", IfxTraceCategory.Leave);
            }
        }

        private void IsImported_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsImported_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("IsImported");
                string newBrokenRules = "";
                
                if (IsImported == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("IsImported", BROKENRULE_IsImported_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("IsImported", BROKENRULE_IsImported_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("IsImported");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("IsImported", _brokenRuleManager.IsPropertyValid("IsImported"), IsPropertyDirty("IsImported"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsImported_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsImported_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_Name != null)
                {
                    len = TbC_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_Name", BROKENRULE_TbC_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Name", BROKENRULE_TbC_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Name", BROKENRULE_TbC_Name_Required);
                    if (len > STRINGSIZE_TbC_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("TbC_Name", BROKENRULE_TbC_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Name", BROKENRULE_TbC_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_Name", _brokenRuleManager.IsPropertyValid("TbC_Name"), IsPropertyDirty("TbC_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ColumnInDatabase_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ColumnInDatabase_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ColumnInDatabase");
                string newBrokenRules = "";
                				int len = 0;
                if (ColumnInDatabase != null)
                {
                    len = ColumnInDatabase.Length;
                }

                if (len > STRINGSIZE_ColumnInDatabase)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ColumnInDatabase", BROKENRULE_ColumnInDatabase_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ColumnInDatabase", BROKENRULE_ColumnInDatabase_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ColumnInDatabase");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ColumnInDatabase", _brokenRuleManager.IsPropertyValid("ColumnInDatabase"), IsPropertyDirty("ColumnInDatabase"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ColumnInDatabase_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ColumnInDatabase_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_CtlTp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_CtlTp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_CtlTp_Id");
                string newBrokenRules = "";
                
                if (TbC_CtlTp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_CtlTp_Id", BROKENRULE_TbC_CtlTp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_CtlTp_Id", BROKENRULE_TbC_CtlTp_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_CtlTp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_CtlTp_Id", _brokenRuleManager.IsPropertyValid("TbC_CtlTp_Id"), IsPropertyDirty("TbC_CtlTp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_CtlTp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_CtlTp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_CtlTp_Id_TextField_Validate()
        {
        }

        private void TbC_IsNonvColumn_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNonvColumn_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsNonvColumn");
                string newBrokenRules = "";
                
                if (TbC_IsNonvColumn == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsNonvColumn", BROKENRULE_TbC_IsNonvColumn_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsNonvColumn", BROKENRULE_TbC_IsNonvColumn_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsNonvColumn");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsNonvColumn", _brokenRuleManager.IsPropertyValid("TbC_IsNonvColumn"), IsPropertyDirty("TbC_IsNonvColumn"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNonvColumn_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNonvColumn_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_Description_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Description_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Description");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_Description != null)
                {
                    len = TbC_Description.Length;
                }

                if (len > STRINGSIZE_TbC_Description)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_Description", BROKENRULE_TbC_Description_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Description", BROKENRULE_TbC_Description_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Description");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_Description", _brokenRuleManager.IsPropertyValid("TbC_Description"), IsPropertyDirty("TbC_Description"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Description_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Description_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_DtSql_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtSql_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_DtSql_Id");
                string newBrokenRules = "";
                
                if (TbC_DtSql_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_DtSql_Id", BROKENRULE_TbC_DtSql_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_DtSql_Id", BROKENRULE_TbC_DtSql_Id_Required);
                }

                if (TbC_DtSql_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_DtSql_Id", BROKENRULE_TbC_DtSql_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_DtSql_Id", BROKENRULE_TbC_DtSql_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_DtSql_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_DtSql_Id", _brokenRuleManager.IsPropertyValid("TbC_DtSql_Id"), IsPropertyDirty("TbC_DtSql_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtSql_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtSql_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_DtSql_Id_TextField_Validate()
        {
        }

        private void TbC_DtNt_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtNt_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_DtNt_Id");
                string newBrokenRules = "";
                
                if (TbC_DtNt_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_DtNt_Id", BROKENRULE_TbC_DtNt_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_DtNt_Id", BROKENRULE_TbC_DtNt_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_DtNt_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_DtNt_Id", _brokenRuleManager.IsPropertyValid("TbC_DtNt_Id"), IsPropertyDirty("TbC_DtNt_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtNt_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtNt_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_DtNt_Id_TextField_Validate()
        {
        }

        private void TbC_Length_Validate()
        {
        }

        private void TbC_Precision_Validate()
        {
        }

        private void TbC_Scale_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Scale_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Scale");
                string newBrokenRules = "";
                
                if (TbC_Scale == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_Scale", BROKENRULE_TbC_Scale_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Scale", BROKENRULE_TbC_Scale_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Scale");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_Scale", _brokenRuleManager.IsPropertyValid("TbC_Scale"), IsPropertyDirty("TbC_Scale"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Scale_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Scale_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsPK_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsPK_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsPK");
                string newBrokenRules = "";
                
                if (TbC_IsPK == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsPK", BROKENRULE_TbC_IsPK_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsPK", BROKENRULE_TbC_IsPK_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsPK");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsPK", _brokenRuleManager.IsPropertyValid("TbC_IsPK"), IsPropertyDirty("TbC_IsPK"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsPK_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsPK_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsIdentity_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsIdentity_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsIdentity");
                string newBrokenRules = "";
                
                if (TbC_IsIdentity == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsIdentity", BROKENRULE_TbC_IsIdentity_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsIdentity", BROKENRULE_TbC_IsIdentity_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsIdentity");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsIdentity", _brokenRuleManager.IsPropertyValid("TbC_IsIdentity"), IsPropertyDirty("TbC_IsIdentity"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsIdentity_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsIdentity_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsFK_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsFK_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsFK");
                string newBrokenRules = "";
                
                if (TbC_IsFK == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsFK", BROKENRULE_TbC_IsFK_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsFK", BROKENRULE_TbC_IsFK_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsFK");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsFK", _brokenRuleManager.IsPropertyValid("TbC_IsFK"), IsPropertyDirty("TbC_IsFK"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsFK_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsFK_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsEntityColumn_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsEntityColumn_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsEntityColumn");
                string newBrokenRules = "";
                
                if (TbC_IsEntityColumn == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsEntityColumn", BROKENRULE_TbC_IsEntityColumn_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsEntityColumn", BROKENRULE_TbC_IsEntityColumn_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsEntityColumn");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsEntityColumn", _brokenRuleManager.IsPropertyValid("TbC_IsEntityColumn"), IsPropertyDirty("TbC_IsEntityColumn"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsEntityColumn_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsEntityColumn_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsSystemField_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsSystemField_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsSystemField");
                string newBrokenRules = "";
                
                if (TbC_IsSystemField == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsSystemField", BROKENRULE_TbC_IsSystemField_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsSystemField", BROKENRULE_TbC_IsSystemField_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsSystemField");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsSystemField", _brokenRuleManager.IsPropertyValid("TbC_IsSystemField"), IsPropertyDirty("TbC_IsSystemField"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsSystemField_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsSystemField_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsValuesObjectMember_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsValuesObjectMember_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsValuesObjectMember");
                string newBrokenRules = "";
                
                if (TbC_IsValuesObjectMember == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsValuesObjectMember", BROKENRULE_TbC_IsValuesObjectMember_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsValuesObjectMember", BROKENRULE_TbC_IsValuesObjectMember_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsValuesObjectMember");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsValuesObjectMember", _brokenRuleManager.IsPropertyValid("TbC_IsValuesObjectMember"), IsPropertyDirty("TbC_IsValuesObjectMember"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsValuesObjectMember_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsValuesObjectMember_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsInPropsScreen_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInPropsScreen_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsInPropsScreen");
                string newBrokenRules = "";
                
                if (TbC_IsInPropsScreen == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsInPropsScreen", BROKENRULE_TbC_IsInPropsScreen_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsInPropsScreen", BROKENRULE_TbC_IsInPropsScreen_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsInPropsScreen");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsInPropsScreen", _brokenRuleManager.IsPropertyValid("TbC_IsInPropsScreen"), IsPropertyDirty("TbC_IsInPropsScreen"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInPropsScreen_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInPropsScreen_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsInNavList_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInNavList_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsInNavList");
                string newBrokenRules = "";
                
                if (TbC_IsInNavList == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsInNavList", BROKENRULE_TbC_IsInNavList_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsInNavList", BROKENRULE_TbC_IsInNavList_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsInNavList");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsInNavList", _brokenRuleManager.IsPropertyValid("TbC_IsInNavList"), IsPropertyDirty("TbC_IsInNavList"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInNavList_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInNavList_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsRequired_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsRequired_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsRequired");
                string newBrokenRules = "";
                
                if (TbC_IsRequired == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsRequired", BROKENRULE_TbC_IsRequired_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsRequired", BROKENRULE_TbC_IsRequired_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsRequired");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsRequired", _brokenRuleManager.IsPropertyValid("TbC_IsRequired"), IsPropertyDirty("TbC_IsRequired"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsRequired_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsRequired_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_BrokenRuleText_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_BrokenRuleText_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_BrokenRuleText");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_BrokenRuleText != null)
                {
                    len = TbC_BrokenRuleText.Length;
                }

                if (len > STRINGSIZE_TbC_BrokenRuleText)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_BrokenRuleText", BROKENRULE_TbC_BrokenRuleText_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_BrokenRuleText", BROKENRULE_TbC_BrokenRuleText_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_BrokenRuleText");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_BrokenRuleText", _brokenRuleManager.IsPropertyValid("TbC_BrokenRuleText"), IsPropertyDirty("TbC_BrokenRuleText"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_BrokenRuleText_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_BrokenRuleText_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_AllowZero_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_AllowZero_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_AllowZero");
                string newBrokenRules = "";
                
                if (TbC_AllowZero == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_AllowZero", BROKENRULE_TbC_AllowZero_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_AllowZero", BROKENRULE_TbC_AllowZero_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_AllowZero");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_AllowZero", _brokenRuleManager.IsPropertyValid("TbC_AllowZero"), IsPropertyDirty("TbC_AllowZero"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_AllowZero_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_AllowZero_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsNullableInDb_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNullableInDb_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsNullableInDb");
                string newBrokenRules = "";
                
                if (TbC_IsNullableInDb == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsNullableInDb", BROKENRULE_TbC_IsNullableInDb_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsNullableInDb", BROKENRULE_TbC_IsNullableInDb_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsNullableInDb");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsNullableInDb", _brokenRuleManager.IsPropertyValid("TbC_IsNullableInDb"), IsPropertyDirty("TbC_IsNullableInDb"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNullableInDb_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNullableInDb_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsNullableInUI_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNullableInUI_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsNullableInUI");
                string newBrokenRules = "";
                
                if (TbC_IsNullableInUI == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsNullableInUI", BROKENRULE_TbC_IsNullableInUI_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsNullableInUI", BROKENRULE_TbC_IsNullableInUI_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsNullableInUI");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsNullableInUI", _brokenRuleManager.IsPropertyValid("TbC_IsNullableInUI"), IsPropertyDirty("TbC_IsNullableInUI"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNullableInUI_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsNullableInUI_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_DefaultValue_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DefaultValue_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_DefaultValue");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_DefaultValue != null)
                {
                    len = TbC_DefaultValue.Length;
                }

                if (len > STRINGSIZE_TbC_DefaultValue)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_DefaultValue", BROKENRULE_TbC_DefaultValue_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_DefaultValue", BROKENRULE_TbC_DefaultValue_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_DefaultValue");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_DefaultValue", _brokenRuleManager.IsPropertyValid("TbC_DefaultValue"), IsPropertyDirty("TbC_DefaultValue"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DefaultValue_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DefaultValue_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsReadFromDb_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadFromDb_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsReadFromDb");
                string newBrokenRules = "";
                
                if (TbC_IsReadFromDb == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsReadFromDb", BROKENRULE_TbC_IsReadFromDb_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsReadFromDb", BROKENRULE_TbC_IsReadFromDb_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsReadFromDb");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsReadFromDb", _brokenRuleManager.IsPropertyValid("TbC_IsReadFromDb"), IsPropertyDirty("TbC_IsReadFromDb"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadFromDb_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadFromDb_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsSendToDb_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsSendToDb_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsSendToDb");
                string newBrokenRules = "";
                
                if (TbC_IsSendToDb == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsSendToDb", BROKENRULE_TbC_IsSendToDb_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsSendToDb", BROKENRULE_TbC_IsSendToDb_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsSendToDb");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsSendToDb", _brokenRuleManager.IsPropertyValid("TbC_IsSendToDb"), IsPropertyDirty("TbC_IsSendToDb"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsSendToDb_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsSendToDb_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsInsertAllowed_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInsertAllowed_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsInsertAllowed");
                string newBrokenRules = "";
                
                if (TbC_IsInsertAllowed == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsInsertAllowed", BROKENRULE_TbC_IsInsertAllowed_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsInsertAllowed", BROKENRULE_TbC_IsInsertAllowed_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsInsertAllowed");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsInsertAllowed", _brokenRuleManager.IsPropertyValid("TbC_IsInsertAllowed"), IsPropertyDirty("TbC_IsInsertAllowed"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInsertAllowed_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInsertAllowed_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsEditAllowed_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsEditAllowed_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsEditAllowed");
                string newBrokenRules = "";
                
                if (TbC_IsEditAllowed == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsEditAllowed", BROKENRULE_TbC_IsEditAllowed_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsEditAllowed", BROKENRULE_TbC_IsEditAllowed_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsEditAllowed");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsEditAllowed", _brokenRuleManager.IsPropertyValid("TbC_IsEditAllowed"), IsPropertyDirty("TbC_IsEditAllowed"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsEditAllowed_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsEditAllowed_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsReadOnlyInUI_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadOnlyInUI_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsReadOnlyInUI");
                string newBrokenRules = "";
                
                if (TbC_IsReadOnlyInUI == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsReadOnlyInUI", BROKENRULE_TbC_IsReadOnlyInUI_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsReadOnlyInUI", BROKENRULE_TbC_IsReadOnlyInUI_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsReadOnlyInUI");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsReadOnlyInUI", _brokenRuleManager.IsPropertyValid("TbC_IsReadOnlyInUI"), IsPropertyDirty("TbC_IsReadOnlyInUI"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadOnlyInUI_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadOnlyInUI_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_UseForAudit_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseForAudit_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_UseForAudit");
                string newBrokenRules = "";
                
                if (TbC_UseForAudit == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_UseForAudit", BROKENRULE_TbC_UseForAudit_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_UseForAudit", BROKENRULE_TbC_UseForAudit_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_UseForAudit");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_UseForAudit", _brokenRuleManager.IsPropertyValid("TbC_UseForAudit"), IsPropertyDirty("TbC_UseForAudit"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseForAudit_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseForAudit_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_DefaultCaption_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DefaultCaption_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_DefaultCaption");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_DefaultCaption != null)
                {
                    len = TbC_DefaultCaption.Length;
                }

                if (len > STRINGSIZE_TbC_DefaultCaption)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_DefaultCaption", BROKENRULE_TbC_DefaultCaption_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_DefaultCaption", BROKENRULE_TbC_DefaultCaption_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_DefaultCaption");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_DefaultCaption", _brokenRuleManager.IsPropertyValid("TbC_DefaultCaption"), IsPropertyDirty("TbC_DefaultCaption"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DefaultCaption_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DefaultCaption_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_ColumnHeaderText_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnHeaderText_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_ColumnHeaderText");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_ColumnHeaderText != null)
                {
                    len = TbC_ColumnHeaderText.Length;
                }

                if (len > STRINGSIZE_TbC_ColumnHeaderText)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_ColumnHeaderText", BROKENRULE_TbC_ColumnHeaderText_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_ColumnHeaderText", BROKENRULE_TbC_ColumnHeaderText_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_ColumnHeaderText");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_ColumnHeaderText", _brokenRuleManager.IsPropertyValid("TbC_ColumnHeaderText"), IsPropertyDirty("TbC_ColumnHeaderText"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnHeaderText_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnHeaderText_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_LabelCaptionVerbose_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_LabelCaptionVerbose_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_LabelCaptionVerbose");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_LabelCaptionVerbose != null)
                {
                    len = TbC_LabelCaptionVerbose.Length;
                }

                if (len > STRINGSIZE_TbC_LabelCaptionVerbose)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_LabelCaptionVerbose", BROKENRULE_TbC_LabelCaptionVerbose_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_LabelCaptionVerbose", BROKENRULE_TbC_LabelCaptionVerbose_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_LabelCaptionVerbose");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_LabelCaptionVerbose", _brokenRuleManager.IsPropertyValid("TbC_LabelCaptionVerbose"), IsPropertyDirty("TbC_LabelCaptionVerbose"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_LabelCaptionVerbose_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_LabelCaptionVerbose_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_LabelCaptionGenerate_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_LabelCaptionGenerate_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_LabelCaptionGenerate");
                string newBrokenRules = "";
                
                if (TbC_LabelCaptionGenerate == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_LabelCaptionGenerate", BROKENRULE_TbC_LabelCaptionGenerate_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_LabelCaptionGenerate", BROKENRULE_TbC_LabelCaptionGenerate_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_LabelCaptionGenerate");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_LabelCaptionGenerate", _brokenRuleManager.IsPropertyValid("TbC_LabelCaptionGenerate"), IsPropertyDirty("TbC_LabelCaptionGenerate"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_LabelCaptionGenerate_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_LabelCaptionGenerate_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tbc_ShowGridColumnToolTip_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_ShowGridColumnToolTip_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tbc_ShowGridColumnToolTip");
                string newBrokenRules = "";
                
                if (Tbc_ShowGridColumnToolTip == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tbc_ShowGridColumnToolTip", BROKENRULE_Tbc_ShowGridColumnToolTip_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tbc_ShowGridColumnToolTip", BROKENRULE_Tbc_ShowGridColumnToolTip_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tbc_ShowGridColumnToolTip");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tbc_ShowGridColumnToolTip", _brokenRuleManager.IsPropertyValid("Tbc_ShowGridColumnToolTip"), IsPropertyDirty("Tbc_ShowGridColumnToolTip"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_ShowGridColumnToolTip_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_ShowGridColumnToolTip_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tbc_ShowPropsToolTip_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_ShowPropsToolTip_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tbc_ShowPropsToolTip");
                string newBrokenRules = "";
                
                if (Tbc_ShowPropsToolTip == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tbc_ShowPropsToolTip", BROKENRULE_Tbc_ShowPropsToolTip_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tbc_ShowPropsToolTip", BROKENRULE_Tbc_ShowPropsToolTip_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tbc_ShowPropsToolTip");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tbc_ShowPropsToolTip", _brokenRuleManager.IsPropertyValid("Tbc_ShowPropsToolTip"), IsPropertyDirty("Tbc_ShowPropsToolTip"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_ShowPropsToolTip_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_ShowPropsToolTip_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tbc_TooltipsRolledUp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_TooltipsRolledUp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tbc_TooltipsRolledUp");
                string newBrokenRules = "";
                				int len = 0;
                if (Tbc_TooltipsRolledUp != null)
                {
                    len = Tbc_TooltipsRolledUp.Length;
                }

                if (len > STRINGSIZE_Tbc_TooltipsRolledUp)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tbc_TooltipsRolledUp", BROKENRULE_Tbc_TooltipsRolledUp_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tbc_TooltipsRolledUp", BROKENRULE_Tbc_TooltipsRolledUp_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tbc_TooltipsRolledUp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tbc_TooltipsRolledUp", _brokenRuleManager.IsPropertyValid("Tbc_TooltipsRolledUp"), IsPropertyDirty("Tbc_TooltipsRolledUp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_TooltipsRolledUp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tbc_TooltipsRolledUp_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsCreatePropsStrings_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatePropsStrings_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsCreatePropsStrings");
                string newBrokenRules = "";
                
                if (TbC_IsCreatePropsStrings == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsCreatePropsStrings", BROKENRULE_TbC_IsCreatePropsStrings_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsCreatePropsStrings", BROKENRULE_TbC_IsCreatePropsStrings_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsCreatePropsStrings");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsCreatePropsStrings", _brokenRuleManager.IsPropertyValid("TbC_IsCreatePropsStrings"), IsPropertyDirty("TbC_IsCreatePropsStrings"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatePropsStrings_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatePropsStrings_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsCreateGridStrings_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreateGridStrings_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsCreateGridStrings");
                string newBrokenRules = "";
                
                if (TbC_IsCreateGridStrings == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsCreateGridStrings", BROKENRULE_TbC_IsCreateGridStrings_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsCreateGridStrings", BROKENRULE_TbC_IsCreateGridStrings_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsCreateGridStrings");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsCreateGridStrings", _brokenRuleManager.IsPropertyValid("TbC_IsCreateGridStrings"), IsPropertyDirty("TbC_IsCreateGridStrings"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreateGridStrings_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreateGridStrings_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsAvailableForColumnGroups_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsAvailableForColumnGroups_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsAvailableForColumnGroups");
                string newBrokenRules = "";
                
                if (TbC_IsAvailableForColumnGroups == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsAvailableForColumnGroups", BROKENRULE_TbC_IsAvailableForColumnGroups_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsAvailableForColumnGroups", BROKENRULE_TbC_IsAvailableForColumnGroups_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsAvailableForColumnGroups");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsAvailableForColumnGroups", _brokenRuleManager.IsPropertyValid("TbC_IsAvailableForColumnGroups"), IsPropertyDirty("TbC_IsAvailableForColumnGroups"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsAvailableForColumnGroups_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsAvailableForColumnGroups_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsTextWrapInProp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTextWrapInProp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsTextWrapInProp");
                string newBrokenRules = "";
                
                if (TbC_IsTextWrapInProp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsTextWrapInProp", BROKENRULE_TbC_IsTextWrapInProp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsTextWrapInProp", BROKENRULE_TbC_IsTextWrapInProp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsTextWrapInProp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsTextWrapInProp", _brokenRuleManager.IsPropertyValid("TbC_IsTextWrapInProp"), IsPropertyDirty("TbC_IsTextWrapInProp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTextWrapInProp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTextWrapInProp_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsTextWrapInGrid_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTextWrapInGrid_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsTextWrapInGrid");
                string newBrokenRules = "";
                
                if (TbC_IsTextWrapInGrid == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsTextWrapInGrid", BROKENRULE_TbC_IsTextWrapInGrid_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsTextWrapInGrid", BROKENRULE_TbC_IsTextWrapInGrid_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsTextWrapInGrid");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsTextWrapInGrid", _brokenRuleManager.IsPropertyValid("TbC_IsTextWrapInGrid"), IsPropertyDirty("TbC_IsTextWrapInGrid"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTextWrapInGrid_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTextWrapInGrid_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_TextBoxFormat_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextBoxFormat_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_TextBoxFormat");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_TextBoxFormat != null)
                {
                    len = TbC_TextBoxFormat.Length;
                }

                if (len > STRINGSIZE_TbC_TextBoxFormat)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_TextBoxFormat", BROKENRULE_TbC_TextBoxFormat_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_TextBoxFormat", BROKENRULE_TbC_TextBoxFormat_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_TextBoxFormat");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_TextBoxFormat", _brokenRuleManager.IsPropertyValid("TbC_TextBoxFormat"), IsPropertyDirty("TbC_TextBoxFormat"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextBoxFormat_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextBoxFormat_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_TextColumnFormat_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextColumnFormat_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_TextColumnFormat");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_TextColumnFormat != null)
                {
                    len = TbC_TextColumnFormat.Length;
                }

                if (len > STRINGSIZE_TbC_TextColumnFormat)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_TextColumnFormat", BROKENRULE_TbC_TextColumnFormat_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_TextColumnFormat", BROKENRULE_TbC_TextColumnFormat_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_TextColumnFormat");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_TextColumnFormat", _brokenRuleManager.IsPropertyValid("TbC_TextColumnFormat"), IsPropertyDirty("TbC_TextColumnFormat"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextColumnFormat_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextColumnFormat_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_ColumnWidth_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnWidth_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_ColumnWidth");
                string newBrokenRules = "";
                
                if (TbC_ColumnWidth == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_ColumnWidth", BROKENRULE_TbC_ColumnWidth_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_ColumnWidth", BROKENRULE_TbC_ColumnWidth_Required);
                }

                if (TbC_ColumnWidth == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_ColumnWidth", BROKENRULE_TbC_ColumnWidth_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_ColumnWidth", BROKENRULE_TbC_ColumnWidth_ZeroNotAllowed);
                }


                if (TbC_ColumnWidth == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_ColumnWidth", BROKENRULE_TbC_ColumnWidth_NullNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_ColumnWidth", BROKENRULE_TbC_ColumnWidth_NullNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_ColumnWidth");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_ColumnWidth", _brokenRuleManager.IsPropertyValid("TbC_ColumnWidth"), IsPropertyDirty("TbC_ColumnWidth"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnWidth_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnWidth_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_TextBoxTextAlignment_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextBoxTextAlignment_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_TextBoxTextAlignment");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_TextBoxTextAlignment != null)
                {
                    len = TbC_TextBoxTextAlignment.Length;
                }

                if (len > STRINGSIZE_TbC_TextBoxTextAlignment)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_TextBoxTextAlignment", BROKENRULE_TbC_TextBoxTextAlignment_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_TextBoxTextAlignment", BROKENRULE_TbC_TextBoxTextAlignment_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_TextBoxTextAlignment");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_TextBoxTextAlignment", _brokenRuleManager.IsPropertyValid("TbC_TextBoxTextAlignment"), IsPropertyDirty("TbC_TextBoxTextAlignment"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextBoxTextAlignment_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_TextBoxTextAlignment_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_ColumnTextAlignment_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnTextAlignment_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_ColumnTextAlignment");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_ColumnTextAlignment != null)
                {
                    len = TbC_ColumnTextAlignment.Length;
                }

                if (len > STRINGSIZE_TbC_ColumnTextAlignment)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_ColumnTextAlignment", BROKENRULE_TbC_ColumnTextAlignment_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_ColumnTextAlignment", BROKENRULE_TbC_ColumnTextAlignment_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_ColumnTextAlignment");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_ColumnTextAlignment", _brokenRuleManager.IsPropertyValid("TbC_ColumnTextAlignment"), IsPropertyDirty("TbC_ColumnTextAlignment"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnTextAlignment_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnTextAlignment_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_ListStoredProc_Id_Validate()
        {
        }

        private void TbC_ListStoredProc_Id_TextField_Validate()
        {
        }

        private void TbC_IsStaticList_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsStaticList_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsStaticList");
                string newBrokenRules = "";
                
                if (TbC_IsStaticList == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsStaticList", BROKENRULE_TbC_IsStaticList_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsStaticList", BROKENRULE_TbC_IsStaticList_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsStaticList");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsStaticList", _brokenRuleManager.IsPropertyValid("TbC_IsStaticList"), IsPropertyDirty("TbC_IsStaticList"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsStaticList_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsStaticList_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_UseNotInList_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseNotInList_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_UseNotInList");
                string newBrokenRules = "";
                
                if (TbC_UseNotInList == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_UseNotInList", BROKENRULE_TbC_UseNotInList_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_UseNotInList", BROKENRULE_TbC_UseNotInList_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_UseNotInList");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_UseNotInList", _brokenRuleManager.IsPropertyValid("TbC_UseNotInList"), IsPropertyDirty("TbC_UseNotInList"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseNotInList_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseNotInList_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_UseListEditBtn_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseListEditBtn_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_UseListEditBtn");
                string newBrokenRules = "";
                
                if (TbC_UseListEditBtn == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_UseListEditBtn", BROKENRULE_TbC_UseListEditBtn_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_UseListEditBtn", BROKENRULE_TbC_UseListEditBtn_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_UseListEditBtn");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_UseListEditBtn", _brokenRuleManager.IsPropertyValid("TbC_UseListEditBtn"), IsPropertyDirty("TbC_UseListEditBtn"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseListEditBtn_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseListEditBtn_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_ParentColumnKey_Validate()
        {
        }

        private void TbC_ParentColumnKey_TextField_Validate()
        {
        }

        private void TbC_UseDisplayTextFieldProperty_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseDisplayTextFieldProperty_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_UseDisplayTextFieldProperty");
                string newBrokenRules = "";
                
                if (TbC_UseDisplayTextFieldProperty == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_UseDisplayTextFieldProperty", BROKENRULE_TbC_UseDisplayTextFieldProperty_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_UseDisplayTextFieldProperty", BROKENRULE_TbC_UseDisplayTextFieldProperty_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_UseDisplayTextFieldProperty");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_UseDisplayTextFieldProperty", _brokenRuleManager.IsPropertyValid("TbC_UseDisplayTextFieldProperty"), IsPropertyDirty("TbC_UseDisplayTextFieldProperty"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseDisplayTextFieldProperty_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UseDisplayTextFieldProperty_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsDisplayTextFieldProperty_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDisplayTextFieldProperty_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsDisplayTextFieldProperty");
                string newBrokenRules = "";
                
                if (TbC_IsDisplayTextFieldProperty == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsDisplayTextFieldProperty", BROKENRULE_TbC_IsDisplayTextFieldProperty_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsDisplayTextFieldProperty", BROKENRULE_TbC_IsDisplayTextFieldProperty_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsDisplayTextFieldProperty");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsDisplayTextFieldProperty", _brokenRuleManager.IsPropertyValid("TbC_IsDisplayTextFieldProperty"), IsPropertyDirty("TbC_IsDisplayTextFieldProperty"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDisplayTextFieldProperty_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDisplayTextFieldProperty_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_ComboListTable_Id_Validate()
        {
        }

        private void TbC_ComboListTable_Id_TextField_Validate()
        {
        }

        private void TbC_ComboListDisplayColumn_Id_Validate()
        {
        }

        private void TbC_ComboListDisplayColumn_Id_TextField_Validate()
        {
        }

        private void TbC_Combo_MaxDropdownHeight_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownHeight_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Combo_MaxDropdownHeight");
                string newBrokenRules = "";
                
                if (TbC_Combo_MaxDropdownHeight == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_Combo_MaxDropdownHeight", BROKENRULE_TbC_Combo_MaxDropdownHeight_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Combo_MaxDropdownHeight", BROKENRULE_TbC_Combo_MaxDropdownHeight_Required);
                }


                if (TbC_Combo_MaxDropdownHeight == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_Combo_MaxDropdownHeight", BROKENRULE_TbC_Combo_MaxDropdownHeight_NullNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Combo_MaxDropdownHeight", BROKENRULE_TbC_Combo_MaxDropdownHeight_NullNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Combo_MaxDropdownHeight");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_Combo_MaxDropdownHeight", _brokenRuleManager.IsPropertyValid("TbC_Combo_MaxDropdownHeight"), IsPropertyDirty("TbC_Combo_MaxDropdownHeight"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownHeight_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownHeight_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_Combo_MaxDropdownWidth_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownWidth_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Combo_MaxDropdownWidth");
                string newBrokenRules = "";
                
                if (TbC_Combo_MaxDropdownWidth == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_Combo_MaxDropdownWidth", BROKENRULE_TbC_Combo_MaxDropdownWidth_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Combo_MaxDropdownWidth", BROKENRULE_TbC_Combo_MaxDropdownWidth_Required);
                }


                if (TbC_Combo_MaxDropdownWidth == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_Combo_MaxDropdownWidth", BROKENRULE_TbC_Combo_MaxDropdownWidth_NullNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Combo_MaxDropdownWidth", BROKENRULE_TbC_Combo_MaxDropdownWidth_NullNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Combo_MaxDropdownWidth");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_Combo_MaxDropdownWidth", _brokenRuleManager.IsPropertyValid("TbC_Combo_MaxDropdownWidth"), IsPropertyDirty("TbC_Combo_MaxDropdownWidth"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownWidth_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_MaxDropdownWidth_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_Combo_AllowDropdownResizing_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_AllowDropdownResizing_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Combo_AllowDropdownResizing");
                string newBrokenRules = "";
                
                if (TbC_Combo_AllowDropdownResizing == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_Combo_AllowDropdownResizing", BROKENRULE_TbC_Combo_AllowDropdownResizing_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Combo_AllowDropdownResizing", BROKENRULE_TbC_Combo_AllowDropdownResizing_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Combo_AllowDropdownResizing");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_Combo_AllowDropdownResizing", _brokenRuleManager.IsPropertyValid("TbC_Combo_AllowDropdownResizing"), IsPropertyDirty("TbC_Combo_AllowDropdownResizing"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_AllowDropdownResizing_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_AllowDropdownResizing_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_Combo_IsResetButtonVisible_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_IsResetButtonVisible_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Combo_IsResetButtonVisible");
                string newBrokenRules = "";
                
                if (TbC_Combo_IsResetButtonVisible == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_Combo_IsResetButtonVisible", BROKENRULE_TbC_Combo_IsResetButtonVisible_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Combo_IsResetButtonVisible", BROKENRULE_TbC_Combo_IsResetButtonVisible_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Combo_IsResetButtonVisible");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_Combo_IsResetButtonVisible", _brokenRuleManager.IsPropertyValid("TbC_Combo_IsResetButtonVisible"), IsPropertyDirty("TbC_Combo_IsResetButtonVisible"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_IsResetButtonVisible_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Combo_IsResetButtonVisible_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsActiveRecColumn_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsActiveRecColumn_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsActiveRecColumn");
                string newBrokenRules = "";
                
                if (TbC_IsActiveRecColumn == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsActiveRecColumn", BROKENRULE_TbC_IsActiveRecColumn_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsActiveRecColumn", BROKENRULE_TbC_IsActiveRecColumn_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsActiveRecColumn");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsActiveRecColumn", _brokenRuleManager.IsPropertyValid("TbC_IsActiveRecColumn"), IsPropertyDirty("TbC_IsActiveRecColumn"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsActiveRecColumn_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsActiveRecColumn_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsDeletedColumn_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDeletedColumn_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsDeletedColumn");
                string newBrokenRules = "";
                
                if (TbC_IsDeletedColumn == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsDeletedColumn", BROKENRULE_TbC_IsDeletedColumn_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsDeletedColumn", BROKENRULE_TbC_IsDeletedColumn_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsDeletedColumn");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsDeletedColumn", _brokenRuleManager.IsPropertyValid("TbC_IsDeletedColumn"), IsPropertyDirty("TbC_IsDeletedColumn"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDeletedColumn_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDeletedColumn_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsCreatedUserIdColumn_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatedUserIdColumn_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsCreatedUserIdColumn");
                string newBrokenRules = "";
                
                if (TbC_IsCreatedUserIdColumn == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsCreatedUserIdColumn", BROKENRULE_TbC_IsCreatedUserIdColumn_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsCreatedUserIdColumn", BROKENRULE_TbC_IsCreatedUserIdColumn_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsCreatedUserIdColumn");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsCreatedUserIdColumn", _brokenRuleManager.IsPropertyValid("TbC_IsCreatedUserIdColumn"), IsPropertyDirty("TbC_IsCreatedUserIdColumn"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatedUserIdColumn_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatedUserIdColumn_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsCreatedDateColumn_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatedDateColumn_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsCreatedDateColumn");
                string newBrokenRules = "";
                
                if (TbC_IsCreatedDateColumn == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsCreatedDateColumn", BROKENRULE_TbC_IsCreatedDateColumn_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsCreatedDateColumn", BROKENRULE_TbC_IsCreatedDateColumn_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsCreatedDateColumn");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsCreatedDateColumn", _brokenRuleManager.IsPropertyValid("TbC_IsCreatedDateColumn"), IsPropertyDirty("TbC_IsCreatedDateColumn"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatedDateColumn_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCreatedDateColumn_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsUserIdColumn_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsUserIdColumn_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsUserIdColumn");
                string newBrokenRules = "";
                
                if (TbC_IsUserIdColumn == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsUserIdColumn", BROKENRULE_TbC_IsUserIdColumn_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsUserIdColumn", BROKENRULE_TbC_IsUserIdColumn_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsUserIdColumn");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsUserIdColumn", _brokenRuleManager.IsPropertyValid("TbC_IsUserIdColumn"), IsPropertyDirty("TbC_IsUserIdColumn"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsUserIdColumn_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsUserIdColumn_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsModifiedDateColumn_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsModifiedDateColumn_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsModifiedDateColumn");
                string newBrokenRules = "";
                
                if (TbC_IsModifiedDateColumn == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsModifiedDateColumn", BROKENRULE_TbC_IsModifiedDateColumn_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsModifiedDateColumn", BROKENRULE_TbC_IsModifiedDateColumn_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsModifiedDateColumn");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsModifiedDateColumn", _brokenRuleManager.IsPropertyValid("TbC_IsModifiedDateColumn"), IsPropertyDirty("TbC_IsModifiedDateColumn"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsModifiedDateColumn_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsModifiedDateColumn_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsRowVersionStampColumn_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsRowVersionStampColumn_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsRowVersionStampColumn");
                string newBrokenRules = "";
                
                if (TbC_IsRowVersionStampColumn == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsRowVersionStampColumn", BROKENRULE_TbC_IsRowVersionStampColumn_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsRowVersionStampColumn", BROKENRULE_TbC_IsRowVersionStampColumn_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsRowVersionStampColumn");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsRowVersionStampColumn", _brokenRuleManager.IsPropertyValid("TbC_IsRowVersionStampColumn"), IsPropertyDirty("TbC_IsRowVersionStampColumn"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsRowVersionStampColumn_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsRowVersionStampColumn_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsBrowsable_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsBrowsable_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsBrowsable");
                string newBrokenRules = "";
                
                if (TbC_IsBrowsable == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsBrowsable", BROKENRULE_TbC_IsBrowsable_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsBrowsable", BROKENRULE_TbC_IsBrowsable_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsBrowsable");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsBrowsable", _brokenRuleManager.IsPropertyValid("TbC_IsBrowsable"), IsPropertyDirty("TbC_IsBrowsable"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsBrowsable_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsBrowsable_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_DeveloperNote_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DeveloperNote_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_DeveloperNote");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_DeveloperNote != null)
                {
                    len = TbC_DeveloperNote.Length;
                }

                if (len > STRINGSIZE_TbC_DeveloperNote)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_DeveloperNote", BROKENRULE_TbC_DeveloperNote_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_DeveloperNote", BROKENRULE_TbC_DeveloperNote_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_DeveloperNote");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_DeveloperNote", _brokenRuleManager.IsPropertyValid("TbC_DeveloperNote"), IsPropertyDirty("TbC_DeveloperNote"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DeveloperNote_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DeveloperNote_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_UserNote_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UserNote_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_UserNote");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_UserNote != null)
                {
                    len = TbC_UserNote.Length;
                }

                if (len > STRINGSIZE_TbC_UserNote)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_UserNote", BROKENRULE_TbC_UserNote_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_UserNote", BROKENRULE_TbC_UserNote_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_UserNote");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_UserNote", _brokenRuleManager.IsPropertyValid("TbC_UserNote"), IsPropertyDirty("TbC_UserNote"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UserNote_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_UserNote_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_HelpFileAdditionalNote_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_HelpFileAdditionalNote_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_HelpFileAdditionalNote");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_HelpFileAdditionalNote != null)
                {
                    len = TbC_HelpFileAdditionalNote.Length;
                }

                if (len > STRINGSIZE_TbC_HelpFileAdditionalNote)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_HelpFileAdditionalNote", BROKENRULE_TbC_HelpFileAdditionalNote_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_HelpFileAdditionalNote", BROKENRULE_TbC_HelpFileAdditionalNote_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_HelpFileAdditionalNote");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_HelpFileAdditionalNote", _brokenRuleManager.IsPropertyValid("TbC_HelpFileAdditionalNote"), IsPropertyDirty("TbC_HelpFileAdditionalNote"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_HelpFileAdditionalNote_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_HelpFileAdditionalNote_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_Notes_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Notes_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Notes");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_Notes != null)
                {
                    len = TbC_Notes.Length;
                }

                if (len > STRINGSIZE_TbC_Notes)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_Notes", BROKENRULE_TbC_Notes_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Notes", BROKENRULE_TbC_Notes_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Notes");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_Notes", _brokenRuleManager.IsPropertyValid("TbC_Notes"), IsPropertyDirty("TbC_Notes"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Notes_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Notes_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsInputComplete_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInputComplete_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsInputComplete");
                string newBrokenRules = "";
                
                if (TbC_IsInputComplete == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsInputComplete", BROKENRULE_TbC_IsInputComplete_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsInputComplete", BROKENRULE_TbC_IsInputComplete_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsInputComplete");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsInputComplete", _brokenRuleManager.IsPropertyValid("TbC_IsInputComplete"), IsPropertyDirty("TbC_IsInputComplete"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInputComplete_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsInputComplete_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsCodeGen_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGen_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsCodeGen");
                string newBrokenRules = "";
                
                if (TbC_IsCodeGen == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsCodeGen", BROKENRULE_TbC_IsCodeGen_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsCodeGen", BROKENRULE_TbC_IsCodeGen_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsCodeGen");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsCodeGen", _brokenRuleManager.IsPropertyValid("TbC_IsCodeGen"), IsPropertyDirty("TbC_IsCodeGen"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGen_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGen_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsReadyCodeGen_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadyCodeGen_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsReadyCodeGen");
                string newBrokenRules = "";
                
                if (TbC_IsReadyCodeGen == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsReadyCodeGen", BROKENRULE_TbC_IsReadyCodeGen_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsReadyCodeGen", BROKENRULE_TbC_IsReadyCodeGen_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsReadyCodeGen");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsReadyCodeGen", _brokenRuleManager.IsPropertyValid("TbC_IsReadyCodeGen"), IsPropertyDirty("TbC_IsReadyCodeGen"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadyCodeGen_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsReadyCodeGen_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsCodeGenComplete_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGenComplete_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsCodeGenComplete");
                string newBrokenRules = "";
                
                if (TbC_IsCodeGenComplete == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsCodeGenComplete", BROKENRULE_TbC_IsCodeGenComplete_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsCodeGenComplete", BROKENRULE_TbC_IsCodeGenComplete_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsCodeGenComplete");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsCodeGenComplete", _brokenRuleManager.IsPropertyValid("TbC_IsCodeGenComplete"), IsPropertyDirty("TbC_IsCodeGenComplete"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGenComplete_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGenComplete_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsTagForCodeGen_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTagForCodeGen_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsTagForCodeGen");
                string newBrokenRules = "";
                
                if (TbC_IsTagForCodeGen == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsTagForCodeGen", BROKENRULE_TbC_IsTagForCodeGen_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsTagForCodeGen", BROKENRULE_TbC_IsTagForCodeGen_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsTagForCodeGen");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsTagForCodeGen", _brokenRuleManager.IsPropertyValid("TbC_IsTagForCodeGen"), IsPropertyDirty("TbC_IsTagForCodeGen"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTagForCodeGen_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTagForCodeGen_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsTagForOther_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTagForOther_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsTagForOther");
                string newBrokenRules = "";
                
                if (TbC_IsTagForOther == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsTagForOther", BROKENRULE_TbC_IsTagForOther_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsTagForOther", BROKENRULE_TbC_IsTagForOther_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsTagForOther");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsTagForOther", _brokenRuleManager.IsPropertyValid("TbC_IsTagForOther"), IsPropertyDirty("TbC_IsTagForOther"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTagForOther_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsTagForOther_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_ColumnGroups_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnGroups_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_ColumnGroups");
                string newBrokenRules = "";
                				int len = 0;
                if (TbC_ColumnGroups != null)
                {
                    len = TbC_ColumnGroups.Length;
                }

                if (len > STRINGSIZE_TbC_ColumnGroups)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_ColumnGroups", BROKENRULE_TbC_ColumnGroups_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_ColumnGroups", BROKENRULE_TbC_ColumnGroups_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_ColumnGroups");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_ColumnGroups", _brokenRuleManager.IsPropertyValid("TbC_ColumnGroups"), IsPropertyDirty("TbC_ColumnGroups"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnGroups_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ColumnGroups_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsActiveRow");
                string newBrokenRules = "";
                
                if (TbC_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsActiveRow", BROKENRULE_TbC_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsActiveRow", BROKENRULE_TbC_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsActiveRow", _brokenRuleManager.IsPropertyValid("TbC_IsActiveRow"), IsPropertyDirty("TbC_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsDeleted");
                string newBrokenRules = "";
                
                if (TbC_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_IsDeleted", BROKENRULE_TbC_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_IsDeleted", BROKENRULE_TbC_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_IsDeleted", _brokenRuleManager.IsPropertyValid("TbC_IsDeleted"), IsPropertyDirty("TbC_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_CreatedUserId_Validate()
        {
        }

        private void TbC_CreatedDate_Validate()
        {
        }

        private void TbC_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbC_LastModifiedDate_Validate()
        {
        }

        private void TbC_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Stamp");
                string newBrokenRules = "";
                
                if (TbC_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbC_Stamp", BROKENRULE_TbC_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbC_Stamp", BROKENRULE_TbC_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbC_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbC_Stamp", _brokenRuleManager.IsPropertyValid("TbC_Stamp"), IsPropertyDirty("TbC_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


