using System;
using TypeServices;
using System.ComponentModel;
using System.Windows;
using Ifx.SL;
using vUICommon;
using Velocity.SL;

// Gen Timestamp:  12/17/2017 8:46:28 PM

namespace EntityBll.SL
{


    public partial class WcTableColumn_Bll //: BusinessObjectBase, ITraceItem, IBusinessObject, IBusinessObjectV2, INotifyPropertyChanged , IEditableObject, IDataErrorInfo
    {




        #region Initialize Custom Variables



        #endregion Initialize Custom Variables


        #region General Methods


        void InitializeClass_Cust()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", IfxTraceCategory.Enter);
//      
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", IfxTraceCategory.Leave);
//            }
        }

        void CustomConstructor()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructor", IfxTraceCategory.Enter);


                WcTableColumnProxy.ExecuteWcTableColumn_InsertListOfColumnsFromDbCompleted += WcTableColumnProxy_ExecuteWcTableColumn_InsertListOfColumnsFromDbCompleted;
                //WcTableColumnProxy.ExecuteWcTableColumn_Create_TextFieldPropertyCompleted += WcTableColumnProxy_ExecuteWcTableColumn_Create_TextFieldPropertyCompleted;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructor", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructor", IfxTraceCategory.Leave);
            }
        }

        public void SetStandingFK(string parentType, Guid fk)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Enter);
                _parentEntityType = parentType;
                _StandingFK = fk;
                SetStandingFK();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Leave);
            }
        }

        public void SetStandingFK()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Enter);
//                //switch (_parentEntityType)
//                //{
//                //    case ParentType.Company:
//                //        _data.C._b = _StandingFK;
//                //        break;
//                //    case ParentType.PersonContact:
//                //        _data.C._c = _StandingFK;
//                //        break;
//                //}      
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Leave);
//            }
        }

        // Alternate FKs
        public void SetOtherParentKeys(string parentType, Guid id)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", IfxTraceCategory.Enter);
//                //_parentEntityType = parentType;
//                //switch (_parentEntityType)
//                //{
//                //    case "ucWellDt":
//                //        _data.C.WlD_Id_noevents = id;
//                //        break;
//                //    case "ucContractDt":
//                //        _data.C.CtD_Id_noevents = id;
//                //        break;
//                //}
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", IfxTraceCategory.Leave);
//            }
        }



        #endregion General Methods


        #region Data Access Methods


        #endregion Data Access Methods


        #region Method Extentions For Custom Code



        private void SetCustomDefaultBrokenRules()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", IfxTraceCategory.Enter);
//
//         
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", IfxTraceCategory.Leave);
//            }
        }




        private void CustomPropertySetterCode(string propName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", IfxTraceCategory.Enter);
                //switch (propName)
                //{
                //    case "TbC_UseDisplayTextFieldProperty":
                //        if (_data.C.TbC_UseDisplayTextFieldProperty == true)
                //        {
                //            WcTableColumnProxy.Begin_ExecuteWcTableColumn_Create_TextFieldProperty(TbC_Id);
                //        }
                //        break;

                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", IfxTraceCategory.Leave);
            }
        }

        private void UnDoCustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", IfxTraceCategory.Enter);
//   
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", IfxTraceCategory.Leave);
//            }
        }

        private void RefreshFieldsCustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", IfxTraceCategory.Enter);
//   
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", IfxTraceCategory.Leave);
//            }
        }

        private void NewEntityRowCustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", IfxTraceCategory.Enter);
//                //SetStandingFK();  //Unrem this when we have FKs
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", IfxTraceCategory.Leave);
//            }
        }

        private bool IsPropertyDirtyCustomCode(string propertyName)
        {
            ////  This is called from 'IsPropertyDirty' for calculated fields that are not represented in the wire objects.  They will always require custom code here.
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", IfxTraceCategory.Enter);
            //    switch (propertyName)
            //    {
            //        case "xxxx":
            //            //  return something
            //            break;
            //        default:
            //            throw new Exception(_as + "." + _cn + ".IsPropertyDirtyCustomCode:  Missing code and logic for " + propertyName + ".");
            //    }
                return false;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        public string GetPropertyValueByKeyCustomCode(string key)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", IfxTraceCategory.Enter);
            //    //switch (key)
            //    //{
            //    //    case "xxxx":
            //    //        return null;
            //    //        return xxx;
            //    //}
                return null;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        public string GetPropertyFormattedStringValueByKeyCustomCode(string key)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", IfxTraceCategory.Enter);
            //    //switch (key)
            //    //{
            //    //    case "xxxx":
            //    //        return null;
            //    //        return xxx;
            //    //}
                return null;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        #endregion Method Extentions For Custom Code


        #region Code Gen Methods For Modification to be copied to here


        #endregion Code Gen Methods For Modification to be copied to here


        #region Code Gen Properties For Modification to be copied to here


        #endregion Code Gen Properties For Modification to be copied to here


        #region Code Gen Validation Logic For Modification to be copied to here


        #endregion Code Gen Validation Logic For Modification to be copied to here


        #region Properties

        //public string DataRowName
        //{
        //    get { return "No Name Selected"; }
        //}



        public Boolean TbC_IsCodeGen_noevents
        {
            get
            {
                return _data.C.TbC_IsCodeGen;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGen - Setter", new ValuePair[] { new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.TbC_IsCodeGen == value) { return; }
                    _data.C.TbC_IsCodeGen_noevents = value;
                    //CustomPropertySetterCode("TbC_IsCodeGen");
                    //TbC_IsCodeGen_Validate();
                    //CheckEntityState(es);
                    Notify("TbC_IsCodeGen");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGen - Setter", ex);
                    throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_IsCodeGen - Setter", IfxTraceCategory.Leave);
                }
            }
        }




        #endregion Properties


        #region Validation Logic


        #endregion Validation Logic


        #region Custom Code



        // import table from here
        public void ImportSelectedTableColumn()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSelectedTable", IfxTraceCategory.Enter);

                if (IsImported ==true || ColumnInDatabase == null || TbC_Tb_Id == null)
                {
                    // Do something?
                    return;
                }

                WcTableColumnProxy.Begin_ExecuteWcTableColumn_InsertListOfColumnsFromDb((Guid)TbC_Tb_Id, TbC_Id, ColumnInDatabase, (Guid)Credentials.UserId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSelectedTable", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSelectedTable", IfxTraceCategory.Leave);
            }
        }





        private void WcTableColumnProxy_ExecuteWcTableColumn_InsertListOfColumnsFromDbCompleted(object sender, ExecuteWcTableColumn_InsertListOfColumnsFromDbCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnProxy_ExecuteWcTableColumn_InsertListOfColumnsFromDbCompleted", IfxTraceCategory.Enter);
                object[] data = e.Result;
                Guid? tbC_Id = data[0] as Guid?;
                Int32? success = data[1] as int?;

                if (success != 1)
                {
                    // The import failed.
                    // Do something.

                }
                else
                {
                    GetEntityRow((Guid)tbC_Id);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnProxy_ExecuteWcTableColumn_InsertListOfColumnsFromDbCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnProxy_ExecuteWcTableColumn_InsertListOfColumnsFromDbCompleted", IfxTraceCategory.Leave);
            }
        }


        //private void WcTableColumnProxy_ExecuteWcTableColumn_Create_TextFieldPropertyCompleted(object sender, ExecuteWcTableColumn_Create_TextFieldPropertyCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnProxy_ExecuteWcTableColumn_Create_TextFieldPropertyCompleted", IfxTraceCategory.Enter);
        //        object[] data = e.Result;
        //        Guid? tbC_Id = data[0] as Guid?;
        //        //Int32? success = data[1] as int?;

        //        if (tbC_Id == null)
        //        {
        //            MessageBox.Show("Adding the text column failed.", "Insert Error", MessageBoxButton.OK);
        //            // The import failed.
        //            // Do something.

        //        }
        //        else
        //        {
        //            GetEntityRow((Guid)tbC_Id);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnProxy_ExecuteWcTableColumn_Create_TextFieldPropertyCompleted", ex);
        //        IfxWrapperException.GetError(ex, (Guid)traceId);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnProxy_ExecuteWcTableColumn_Create_TextFieldPropertyCompleted", IfxTraceCategory.Leave);
        //    }
        //}



        #endregion Custom Code


        #region ITraceItem Members

        void GetTraceItemsShortListCustom()
        {
            //  Add custom trace items here.
            //_traceItems.Add("xxxxxxxxx", xxxxxxxxx, TraceDataTypes.String);
            //_traceItems.Add("SomeChildObject", new SomeChildObject(), TraceDataTypes.ITraceItem);
        }

        void GetTraceItemsLongListCustom()
        {
            //  Add custom trace items here.
            //_traceItems.Add("xxxxxxxxx", xxxxxxxxx.ToString, TraceDataTypes.String);
            //_traceItems.Add("SomeChildObject", new SomeChildObject(), TraceDataTypes.ITraceItem);
        }

        #endregion ITraceItem Members
    }

}


