using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/31/2016 8:21:53 PM

namespace EntityBll.SL
{
    public partial class WcApplicationCulture_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_ApCltr_Comment = 250;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_ApCltr_Sort_ZeroNotAllowed = "Sort Order:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_ApCltr_Cltr_Id_Required = "Culture is a required field.";
		private const string BROKENRULE_ApCltr_Cltr_Id_ZeroNotAllowed = "Culture:  '0'  (zero) is not allowed.";
		private string BROKENRULE_ApCltr_Comment_TextLength = "Comment has a maximum text length of  '" + STRINGSIZE_ApCltr_Comment + "'.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_ApCltr_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCltr_Id", BROKENRULE_ApCltr_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCltr_Ap_Id", BROKENRULE_ApCltr_Ap_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCltr_Sort", BROKENRULE_ApCltr_Sort_ZeroNotAllowed);
				_brokenRuleManager.AddBrokenRuleForProperty("ApCltr_Cltr_Id", BROKENRULE_ApCltr_Cltr_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCltr_Cltr_Id", BROKENRULE_ApCltr_Cltr_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCltr_Comment", BROKENRULE_ApCltr_Comment_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCltr_Stamp", BROKENRULE_ApCltr_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void ApCltr_Id_Validate()
        {
        }

        private void ApCltr_Ap_Id_Validate()
        {
        }

        private void ApCltr_Sort_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCltr_Sort_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCltr_Sort");
                string newBrokenRules = "";
                
                if (ApCltr_Sort == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCltr_Sort", BROKENRULE_ApCltr_Sort_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCltr_Sort", BROKENRULE_ApCltr_Sort_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCltr_Sort");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApCltr_Sort", _brokenRuleManager.IsPropertyValid("ApCltr_Sort"), IsPropertyDirty("ApCltr_Sort"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCltr_Sort_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCltr_Sort_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApCltr_Cltr_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCltr_Cltr_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCltr_Cltr_Id");
                string newBrokenRules = "";
                
                if (ApCltr_Cltr_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCltr_Cltr_Id", BROKENRULE_ApCltr_Cltr_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCltr_Cltr_Id", BROKENRULE_ApCltr_Cltr_Id_Required);
                }

                if (ApCltr_Cltr_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCltr_Cltr_Id", BROKENRULE_ApCltr_Cltr_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCltr_Cltr_Id", BROKENRULE_ApCltr_Cltr_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCltr_Cltr_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApCltr_Cltr_Id", _brokenRuleManager.IsPropertyValid("ApCltr_Cltr_Id"), IsPropertyDirty("ApCltr_Cltr_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCltr_Cltr_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCltr_Cltr_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApCltr_Cltr_Id_TextField_Validate()
        {
        }

        private void ApCltr_Comment_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCltr_Comment_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCltr_Comment");
                string newBrokenRules = "";
                				int len = 0;
                if (ApCltr_Comment != null)
                {
                    len = ApCltr_Comment.Length;
                }

                if (len > STRINGSIZE_ApCltr_Comment)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCltr_Comment", BROKENRULE_ApCltr_Comment_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCltr_Comment", BROKENRULE_ApCltr_Comment_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCltr_Comment");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApCltr_Comment", _brokenRuleManager.IsPropertyValid("ApCltr_Comment"), IsPropertyDirty("ApCltr_Comment"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCltr_Comment_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCltr_Comment_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApCltr_IsDefault_Validate()
        {
        }

        private void ApCltr_IsActiveRow_Validate()
        {
        }

        private void ApCltr_IsDeleted_Validate()
        {
        }

        private void ApCltr_CreatedUserId_Validate()
        {
        }

        private void ApCltr_CreatedDate_Validate()
        {
        }

        private void ApCltr_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApCltr_LastModifiedDate_Validate()
        {
        }

        private void ApCltr_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCltr_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCltr_Stamp");
                string newBrokenRules = "";
                
                if (ApCltr_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCltr_Stamp", BROKENRULE_ApCltr_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCltr_Stamp", BROKENRULE_ApCltr_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCltr_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApCltr_Stamp", _brokenRuleManager.IsPropertyValid("ApCltr_Stamp"), IsPropertyDirty("ApCltr_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCltr_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCltr_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


