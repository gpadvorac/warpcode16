using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/26/2017 1:21:55 PM

namespace EntityBll.SL
{
    public partial class WcStoredProcColumn_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_SpCl_Name = 128;
		public const int STRINGSIZE_SpCl_ColLetter = 3;
		public const int STRINGSIZE_SpCl_Notes = 255;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_SpCl_Id_Required = "Id is a required field.";
		private const string BROKENRULE_SpCl_Sp_Id_Required = "Sp_Id is a required field.";
		private string BROKENRULE_SpCl_Name_TextLength = "Name has a maximum text length of  '" + STRINGSIZE_SpCl_Name + "'.";
		private const string BROKENRULE_SpCl_Name_Required = "Name is a required field.";
		private string BROKENRULE_SpCl_ColLetter_TextLength = "Column  Letter has a maximum text length of  '" + STRINGSIZE_SpCl_ColLetter + "'.";
		private const string BROKENRULE_SpCl_DtNt_Id_ZeroNotAllowed = "DotNet Type:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_SpCl_DtSql_Id_ZeroNotAllowed = "SQL Type:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_SpCl_IsNullable_Required = "Nullable is a required field.";
		private const string BROKENRULE_SpCl_IsVisible_Required = "Visible is a required field.";
		private const string BROKENRULE_SpCl_Browsable_Required = "Browsable is a required field.";
		private string BROKENRULE_SpCl_Notes_TextLength = "Notes has a maximum text length of  '" + STRINGSIZE_SpCl_Notes + "'.";
		private const string BROKENRULE_SpCl_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_SpCl_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_SpCl_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("SpCl_Id", BROKENRULE_SpCl_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpCl_Sp_Id", BROKENRULE_SpCl_Sp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpCl_Name", BROKENRULE_SpCl_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("SpCl_Name", BROKENRULE_SpCl_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpCl_ColLetter", BROKENRULE_SpCl_ColLetter_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpCl_DtNt_Id", BROKENRULE_SpCl_DtNt_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpCl_DtSql_Id", BROKENRULE_SpCl_DtSql_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpCl_IsNullable", BROKENRULE_SpCl_IsNullable_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpCl_IsVisible", BROKENRULE_SpCl_IsVisible_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpCl_Browsable", BROKENRULE_SpCl_Browsable_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpCl_Notes", BROKENRULE_SpCl_Notes_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpCl_IsActiveRow", BROKENRULE_SpCl_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpCl_IsDeleted", BROKENRULE_SpCl_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpCl_Stamp", BROKENRULE_SpCl_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void SpCl_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_Id");
                string newBrokenRules = "";
                
                if (SpCl_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpCl_Id", BROKENRULE_SpCl_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_Id", BROKENRULE_SpCl_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpCl_Id", _brokenRuleManager.IsPropertyValid("SpCl_Id"), IsPropertyDirty("SpCl_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpCl_Sp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Sp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_Sp_Id");
                string newBrokenRules = "";
                
                if (SpCl_Sp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpCl_Sp_Id", BROKENRULE_SpCl_Sp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_Sp_Id", BROKENRULE_SpCl_Sp_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_Sp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpCl_Sp_Id", _brokenRuleManager.IsPropertyValid("SpCl_Sp_Id"), IsPropertyDirty("SpCl_Sp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Sp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Sp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpCl_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (SpCl_Name != null)
                {
                    len = SpCl_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpCl_Name", BROKENRULE_SpCl_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_Name", BROKENRULE_SpCl_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_Name", BROKENRULE_SpCl_Name_Required);
                    if (len > STRINGSIZE_SpCl_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("SpCl_Name", BROKENRULE_SpCl_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_Name", BROKENRULE_SpCl_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpCl_Name", _brokenRuleManager.IsPropertyValid("SpCl_Name"), IsPropertyDirty("SpCl_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpCl_ColIndex_Validate()
        {
        }

        private void SpCl_ColLetter_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_ColLetter_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_ColLetter");
                string newBrokenRules = "";
                				int len = 0;
                if (SpCl_ColLetter != null)
                {
                    len = SpCl_ColLetter.Length;
                }

                if (len > STRINGSIZE_SpCl_ColLetter)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpCl_ColLetter", BROKENRULE_SpCl_ColLetter_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_ColLetter", BROKENRULE_SpCl_ColLetter_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_ColLetter");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpCl_ColLetter", _brokenRuleManager.IsPropertyValid("SpCl_ColLetter"), IsPropertyDirty("SpCl_ColLetter"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_ColLetter_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_ColLetter_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpCl_DtNt_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtNt_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_DtNt_Id");
                string newBrokenRules = "";
                
                if (SpCl_DtNt_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpCl_DtNt_Id", BROKENRULE_SpCl_DtNt_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_DtNt_Id", BROKENRULE_SpCl_DtNt_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_DtNt_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpCl_DtNt_Id", _brokenRuleManager.IsPropertyValid("SpCl_DtNt_Id"), IsPropertyDirty("SpCl_DtNt_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtNt_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtNt_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpCl_DtNt_Id_TextField_Validate()
        {
        }

        private void SpCl_DtSql_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtSql_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_DtSql_Id");
                string newBrokenRules = "";
                
                if (SpCl_DtSql_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpCl_DtSql_Id", BROKENRULE_SpCl_DtSql_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_DtSql_Id", BROKENRULE_SpCl_DtSql_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_DtSql_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpCl_DtSql_Id", _brokenRuleManager.IsPropertyValid("SpCl_DtSql_Id"), IsPropertyDirty("SpCl_DtSql_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtSql_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtSql_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpCl_DtSql_Id_TextField_Validate()
        {
        }

        private void SpCl_IsNullable_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsNullable_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_IsNullable");
                string newBrokenRules = "";
                
                if (SpCl_IsNullable == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpCl_IsNullable", BROKENRULE_SpCl_IsNullable_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_IsNullable", BROKENRULE_SpCl_IsNullable_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_IsNullable");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpCl_IsNullable", _brokenRuleManager.IsPropertyValid("SpCl_IsNullable"), IsPropertyDirty("SpCl_IsNullable"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsNullable_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsNullable_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpCl_IsVisible_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsVisible_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_IsVisible");
                string newBrokenRules = "";
                
                if (SpCl_IsVisible == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpCl_IsVisible", BROKENRULE_SpCl_IsVisible_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_IsVisible", BROKENRULE_SpCl_IsVisible_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_IsVisible");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpCl_IsVisible", _brokenRuleManager.IsPropertyValid("SpCl_IsVisible"), IsPropertyDirty("SpCl_IsVisible"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsVisible_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsVisible_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpCl_Browsable_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Browsable_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_Browsable");
                string newBrokenRules = "";
                
                if (SpCl_Browsable == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpCl_Browsable", BROKENRULE_SpCl_Browsable_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_Browsable", BROKENRULE_SpCl_Browsable_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_Browsable");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpCl_Browsable", _brokenRuleManager.IsPropertyValid("SpCl_Browsable"), IsPropertyDirty("SpCl_Browsable"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Browsable_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Browsable_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpCl_Notes_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Notes_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_Notes");
                string newBrokenRules = "";
                				int len = 0;
                if (SpCl_Notes != null)
                {
                    len = SpCl_Notes.Length;
                }

                if (len > STRINGSIZE_SpCl_Notes)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpCl_Notes", BROKENRULE_SpCl_Notes_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_Notes", BROKENRULE_SpCl_Notes_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_Notes");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpCl_Notes", _brokenRuleManager.IsPropertyValid("SpCl_Notes"), IsPropertyDirty("SpCl_Notes"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Notes_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Notes_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpCl_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_IsActiveRow");
                string newBrokenRules = "";
                
                if (SpCl_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpCl_IsActiveRow", BROKENRULE_SpCl_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_IsActiveRow", BROKENRULE_SpCl_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpCl_IsActiveRow", _brokenRuleManager.IsPropertyValid("SpCl_IsActiveRow"), IsPropertyDirty("SpCl_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpCl_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_IsDeleted");
                string newBrokenRules = "";
                
                if (SpCl_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpCl_IsDeleted", BROKENRULE_SpCl_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_IsDeleted", BROKENRULE_SpCl_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpCl_IsDeleted", _brokenRuleManager.IsPropertyValid("SpCl_IsDeleted"), IsPropertyDirty("SpCl_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpCl_CreatedUserId_Validate()
        {
        }

        private void SpCl_CreatedDate_Validate()
        {
        }

        private void SpCl_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpCl_LastModifiedDate_Validate()
        {
        }

        private void SpCl_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_Stamp");
                string newBrokenRules = "";
                
                if (SpCl_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpCl_Stamp", BROKENRULE_SpCl_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpCl_Stamp", BROKENRULE_SpCl_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpCl_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpCl_Stamp", _brokenRuleManager.IsPropertyValid("SpCl_Stamp"), IsPropertyDirty("SpCl_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


