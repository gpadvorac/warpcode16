using System;
using System.Collections.Generic;
using TypeServices;
using System.ComponentModel;
using System.Diagnostics;
using EntityWireTypeSL;
using Ifx.SL;
using Velocity.SL;
using ProxyWrapper;
using vComboDataTypes;

// Gen Timestamp:  12/26/2017 1:21:55 PM

namespace EntityBll.SL
{


    public partial class WcStoredProcColumn_Bll : BusinessObjectBase, ITraceItem, IBusinessObject, IBusinessObjectV2, INotifyPropertyChanged , IEditableObject
    {


        #region Initialize Variables

        /// <summary>
        /// This is a private instance of TraceItemList which is normaly used in every entity
        /// business type and is used to provide rich information abou the current state and values
        /// in this business object when included in a trace call. To see exactly what data will be
        /// provided in a trace, look at the GetTraceItemsShortList method in the WcStoredProcColumn_Bll code
        /// file.
        /// </summary>
        /// <seealso cref="!:file://D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Optimized_Deep_Tracing_of_Business_Object.html" cat="My Test Category">My Test Caption</seealso>
        TraceItemList _traceItems;
        /// <seealso cref="EntityWireType.WcStoredProcColumn_Values">WcStoredProcColumn_Values Class</seealso>
        /// <summary>
        ///     This is an instance of the wire type (<see cref="EntityWireType.WcStoredProcColumn_Values">WcStoredProcColumn_Values</see>) that’s wrapped by the Values
        ///     Manager (<see cref="EntityWireType.WcStoredProcColumn_ValuesMngr">WcStoredProcColumn_ValuesMngr</see>) and
        ///     used to Plug-n-Play into this business object.
        /// </summary>
        private WcStoredProcColumn_ValuesMngr _data = null;

        /// <summary>See <see cref="OnCrudFailed">OnCrudFailed</see> for information.</summary>
        public event CrudFailedEventHandler CrudFailed;

        public event AsyncSaveCompleteEventHandler AsyncSaveComplete;

        public event AsyncSaveWithResponseCompleteEventHandler AsyncSaveWithResponseComplete;

        public event EntityRowReceivedEventHandler EntityRowReceived;


        /// <summary>
        ///     See <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> for
        ///     information.
        /// </summary>
        public event CurrentEntityStateEventHandler CurrentEntityStateChanged;
        /// <summary>See <see cref="OnBrokenRuleChanged">OnBrokenRuleChanged</see> for information.</summary>
        public event BrokenRuleEventHandler BrokenRuleChanged;
        /// <summary>
        /// 	<para>
        ///         Raised by the This event is currently <see cref="OnPropertyValueChanged">OnPropertyValueChanged</see> method, its currently not
        ///         being used and may be obsolete.
        ///     </para>
        /// </summary>
        public event PropertyValueChangedEventHandler PropertyValueChanged;
        /// <summary>This event is currently not being used and may be obsolete.</summary>
        public event BusinessObjectUpdatedEventHandler BusinessObjectUpdated;
        /// <summary>
        /// 	<para>
        ///         Required by the INotifyPropertyChanged interface, this event is raise by the
        ///         <see cref="Notify">Notify</see> method (also part of the INotifyPropertyChanged
        ///         interface. The Notify method is called in nearly all public data field
        ///         properties. The INotifyPropertyChanged interface is implemented to make
        ///         WcStoredProcColumn_Bll more extendable.
        ///     </para>
        /// 	<para>The <strong>INotifyPropertyChanged</strong> interface is used to notify
        ///     clients, typically binding clients, that a property value has changed. For more
        ///     information, search INotifyPropertyChanged on <strong>MSDN</strong>.</para>
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 	<para>
        ///         Private field for the <see cref="ActiveRestrictedStringProperty">ActiveRestrictedStringProperty</see> property.
        ///         See it’s documentation for details.
        ///     </para>
        /// 	<para></para>
        /// </summary>
        private string _activeRestrictedStringProperty;
        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcStoredProcColumn_Bll";
        /// <summary>
        /// 	<para>
        ///         Used in the validation section of this class in the WcStoredProcColumn.bll.vld.cs code file,
        ///         this event notifies event handlers that a data field’s valid state has changed
        ///         (valid or not valid).<br/>
        ///         For more information about this event, see <see cref="TypeServices.ControlValidStateChangedEventHandler">CurrentEntityStateEventHandler</see>.<br/>
        ///         For more information on how it’s used, see these methods:
        ///     </para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="NewEntityRow">NewEntityRow</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="GetEntityRow">GetEntityRow</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="Save">Save</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcColumnProps.RaiseCurrentEntityStateChanged">ucWcStoredProcColumnProps.RaiseCurrentEntityStateChanged()</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcColumnProps.RaiseCurrentEntityStateChanged">ucWcStoredProcColumnProps.RaiseCurrentEntityStateChanged(EntityStateSwitch
        ///             state)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public event ControlValidStateChangedEventHandler ControlValidStateChanged;
        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcColumnProps.OnRestrictedTextLengthChanged">ucWcStoredProcColumnProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcColumnProps.SetRestrictedStringLengthText">ucWcStoredProcColumnProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public event RestrictedTextLengthChangedEventHandler RestrictedTextLengthChanged;

        UserSecurityContext _context = null;

        private object _parentEditObject = null;
        private ParentEditObjectType _parentType = ParentEditObjectType.None;

        private WcStoredProcColumnService_ProxyWrapper _wcStoredProcColumnProxy = null;



        Guid _StandingFK = Guid.Empty;
        string _parentEntityType = "";

        #endregion Initialize Variables

            
        #region Initialize Class

        public WcStoredProcColumn_Bll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_Bll()", IfxTraceCategory.Enter);
                _context = UserSecurityContext.GetCurrent();
                InitializeClass();
//                WcStoredProcColumnService_ProxyWrapper _wcStoredProcColumnProxy = new  WcStoredProcColumnService_ProxyWrapper();
//                _wcStoredProcColumnProxy.WcStoredProcColumn_SaveCompleted += new EventHandler<WcStoredProcColumn_SaveCompletedEventArgs>(SaveCompleted);
                SetNewEntityRow();

                CustomConstructor();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_Bll()", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_Bll()", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>
        ///         Passes in the <see cref="EntityWireType.WcStoredProcColumn_ValuesMngr">WcStoredProcColumn_ValuesMngr</see> type and <see cref="TypeServices.EntityState">EntityState</see>. The WcStoredProcColumn_ValuesMngr type is the
        ///         data object used in a ‘Plug-n-Play’ fashion for loading data into WcStoredProcColumn_Bll
        ///         fast and efficiently. EntityState sets WcStoredProcColumn_Bll’s state.
        ///     </para>
        /// 	<para>Used by the following methods call this constructor:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="FromWire">FromWire</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcStoredProcColumn_List.Fill">Fill</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcStoredProcColumn_List.ReplaceList">ReplaceList(WcStoredProcColumn_ValuesMngr[]
        ///             list)</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcStoredProcColumn_List.ReplaceList">ReplaceList(IEntity_ValuesMngr[]
        ///             list)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public WcStoredProcColumn_Bll(WcStoredProcColumn_ValuesMngr valueObject, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_Bll(WcStoredProcColumn_ValuesMngr valueObject, EntityState state)", IfxTraceCategory.Enter);
                _context = UserSecurityContext.GetCurrent(); 
                _data = valueObject;
                InitializeClass(state);
               // _data.SetClassStateAfterFetch(state);
//                 WcStoredProcColumnService_ProxyWrapper _wcStoredProcColumnProxy = new  WcStoredProcColumnService_ProxyWrapper();
//                _wcStoredProcColumnProxy.WcStoredProcColumn_SaveCompleted += new EventHandler<WcStoredProcColumn_SaveCompletedEventArgs>(SaveCompleted);
                CustomConstructor();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_Bll(WcStoredProcColumn_ValuesMngr valueObject, EntityState state)", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcColumn_Bll(WcStoredProcColumn_ValuesMngr valueObject, EntityState state)", IfxTraceCategory.Leave);
            }
        }


        private void InitializeClass()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Enter);
                InitializeClass(new EntityState(true, true, false));
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Leave);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Leave);
            }
        }



        /// <summary>Called from the constructor, events are wired here.</summary>
        private void InitializeClass(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass(EntityState state)", IfxTraceCategory.Enter);
                if (_data == null)
                {
                    _data = GetDefault();
                    //**  This is a hack.  We should set the state elsewhere, however, when we get this from accross the wire,
                    //      we loose the state and therefore are forced to always call SetClassStateAfterFetch.
                   // _data.SetClassStateAfterFetch(state);
                }
                _data.SetClassStateAfterFetch(state);
                BrokenRuleManagerProperty = new BrokenRuleManager(this);
                _brokenRuleManager.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                //TODO: need to code it for a dynamic creation for an insert
                //TODO: need to move save, assignwithid, update, delete, deactivate, persist (generic, check the state) into this class
                //TODO: need to move the various lists of apps to get into an applicationprovider class for the client side that thunks to the proxywrapper
                //TODO: need to move field level validation into the setters and call the object validation on any persist operation
                //TODO:need to implement the two eh and tracing interfaces on the object
                InitializeClass_Cust();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityState", IfxTraceCategory.Leave);
            }
        }

        private  WcStoredProcColumnService_ProxyWrapper WcStoredProcColumnProxy
        {
            get
            {
                if (_wcStoredProcColumnProxy == null)
                {
                    _wcStoredProcColumnProxy = new  WcStoredProcColumnService_ProxyWrapper();
                    _wcStoredProcColumnProxy.WcStoredProcColumn_SaveCompleted += new EventHandler<WcStoredProcColumn_SaveCompletedEventArgs>(SaveCompleted);
                    _wcStoredProcColumnProxy.WcStoredProcColumn_GetByIdCompleted += new EventHandler<WcStoredProcColumn_GetByIdCompletedEventArgs>(GetEntityRowResponse);

                }

                return _wcStoredProcColumnProxy;
            }
        }

        #endregion  Initialize Class


		#region CRUD Methods



        /// <returns><see cref="EntityWireType.WcStoredProcColumn_ValuesMngr">WcStoredProcColumn_ValuesMngr</see></returns>
        /// <summary>
        ///     When this business type is set to a new state, a new data type also known as a wire
        ///     type (<see cref="EntityWireType.WcStoredProcColumn_Values">WcStoredProcColumn_Values</see>) is created, new
        ///     Id value is created, default values are set and appropriate state setting are made.
        ///     WcStoredProcColumn_Values is wrapped in the <see cref="EntityWireType.WcStoredProcColumn_ValuesMngr">WcStoredProcColumn_ValuesMngr</see> type and returned.
        /// </summary>
        private WcStoredProcColumn_ValuesMngr GetDefault()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                Guid Id = GetNewID();
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Enter);
                return new WcStoredProcColumn_ValuesMngr(new object[] 
                    {
                        Id,    //  SpCl_Id
						null,		//  SpCl_Sp_Id
						null,		//  SpCl_Name
						null,		//  SpCl_ColIndex
						null,		//  SpCl_ColLetter
						null,		//  SpCl_DtNt_Id
						null,		//  SpCl_DtNt_Id_TextField
						null,		//  SpCl_DtSql_Id
						null,		//  SpCl_DtSql_Id_TextField
						true,		//  SpCl_IsNullable
						true,		//  SpCl_IsVisible
						true,		//  SpCl_Browsable
						null,		//  SpCl_Notes
						true,		//  SpCl_IsActiveRow
						false,		//  SpCl_IsDeleted
						null,		//  SpCl_CreatedUserId
						null,		//  SpCl_CreatedDate
						null,		//  SpCl_UserId
						null,		//  UserName
						null,		//  SpCl_LastModifiedDate
						null,		//  SpCl_Stamp
                    }, new EntityState(true, true, false)
                );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// Called by any parent object that wants to clear out the existing data and state,
        /// and replace all with new state and data object.
        /// </summary>
        public void NewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Enter);
                _data = GetDefault();
                NewEntityRowCustomCode();
                SetDefaultBrokenRules();
                //RaiseEventCurrentEntityStateChanged(EntityStateSwitch.NewInvalidNotDirty);
                RaiseEventCurrentEntityStateChanged(_data.S.Switch);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// This is called from the parameterless constructor.  It's assumed that a data object will not be passed (which would have
        /// cause the EntityState to be configured properly) and therefore this new entity must be properly configured.
        /// </summary>
        public void SetNewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", IfxTraceCategory.Enter);
                NewEntityRowCustomCode();
                SetDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", IfxTraceCategory.Leave);
            }
        }


        public void GetEntityRow(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", new ValuePair[] {new ValuePair("Id", Id) }, IfxTraceCategory.Enter);

                WcStoredProcColumnProxy.Begin_WcStoredProcColumn_GetById(Id);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", IfxTraceCategory.Leave);
            }
        }

        public void GetEntityRow(long Id)
        {
            throw new NotImplementedException("WcStoredProcColumn_Bll GetEntityRow(long Id) Not Implemented");
        }

        public void GetEntityRow(int Id)
        {
            throw new NotImplementedException("WcStoredProcColumn_Bll GetEntityRow(int Id) Not Implemented");
        }

        public void GetEntityRow(short Id)
        {
            throw new NotImplementedException("WcStoredProcColumn_Bll GetEntityRow(short Id) Not Implemented");
        }

        public void GetEntityRow(byte Id)
        {
            throw new NotImplementedException("WcStoredProcColumn_Bll GetEntityRow(byte Id) Not Implemented");
        }

        public void GetEntityRow(object Id)
        {
            throw new NotImplementedException("WcStoredProcColumn_Bll GetEntityRow(object Id) Not Implemented");
        }

        private void GetEntityRowResponse(object sender, WcStoredProcColumn_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                if (data == null)
                {
                    // Alert UI
                    return;
                }
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array == null)
                {
                    // Alert UI
                    return;
                }
                _brokenRuleManager.ClearAllBrokenRules();
                _data.ReplaceData((object[])array[0], new EntityState(false, true, false));
                RaiseEventEntityRowReceived();
                RefreshFields();
                // Do we still need this next line?
                //RaiseEventCurrentEntityStateChanged(_data.S.Switch);

                //  ToDo:  Later when we have a Foreign Key, do something like this:  DataProps.TbC_Tb_Id = DataProps.standing_FK;
                //              How do we handle the standing FK in the new framework?
 
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", IfxTraceCategory.Leave);
            }
        }



        /// <summary>
        ///     Called by <see cref="GetDefault">GetDefault</see> and creates a new WcStoredProcColumn Id value
        ///     for a new data object (<see cref="EntityWireType.WcStoredProcColumn_ValuesMngr">WcStoredProcColumn_ValuesMngr</see>).
        /// </summary>
        private Guid GetNewID()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", IfxTraceCategory.Enter);
                return Guid.NewGuid();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Un-does the current data edits and restores everything back to the previous saved
        ///     or new <see cref="TypeServices.EntityState">state</see>. This will include calling
        ///     <see cref="TypeServices.BrokenRuleManager.ClearAllBrokenRules">_brokenRuleManager.ClearAllBrokenRules</see>
        ///     to clear any BrokenRules or calling <see cref="SetDefaultBrokenRules">SetDefaultBrokenRules</see> to set any default BrokenRules;
        ///     and raising the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event is raised to
        ///     notify the parent objects that the state has changed.
        /// </summary>
        public void UnDo()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Enter);
                EntityStateSwitch es = _data.S.Switch;
                _data.SetCurrentToOriginal();
                if (_data.S.IsNew() == true)
                {
                    SetDefaultBrokenRules();
                }
                else
                {
                    _brokenRuleManager.ClearAllBrokenRules();
                }
                CheckEntityState(es);

                UnDoCustomCode();
                RefreshFields();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Call the Notify method for all visible fields.  This allows the fields/properties to be refreshed in grids and other controls that are consuming them via databinding.
        /// </summary>
        public void RefreshFields()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", IfxTraceCategory.Enter);
				Notify("SpCl_Name");
				Notify("SpCl_ColIndex");
				Notify("SpCl_ColLetter");
				Notify("SpCl_DtNt_Id");
				Notify("SpCl_DtNt_Id_TextField");
				Notify("SpCl_DtSql_Id");
				Notify("SpCl_DtSql_Id_TextField");
				Notify("SpCl_IsNullable");
				Notify("SpCl_IsVisible");
				Notify("SpCl_Browsable");
				Notify("SpCl_Notes");
				Notify("SpCl_IsActiveRow");
				Notify("UserName");
				Notify("SpCl_LastModifiedDate");

                RefreshFieldsCustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Called from <see cref="wnConcurrencyManager">wnConcurrencyManager</see> and therefore does not need
        ///     to pass in the Parent and Parent Type parameters.
        ///     Saves the current data to the data store, configures the appropriate state and
        ///     calls <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see> to
        ///     notify the parent object of the state change.
        /// </summary>
        public void Save(UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                _data.C.SpCl_UserId_noevents = Credentials.UserId;
                WcStoredProcColumnProxy.Begin_WcStoredProcColumn_Save(_data.C.GetValues(), (int)check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Saves the current data to the data store, configures the appropriate state and
        ///     calls <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see> to
        ///     notify the parent object of the state change.
        /// </summary>
        public void Save(object parentEditObject, ParentEditObjectType parentType,  UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                _parentEditObject = parentEditObject;
                _parentType = parentType;
                _data.C.SpCl_UserId_noevents = Credentials.UserId;
                WcStoredProcColumnProxy.Begin_WcStoredProcColumn_Save(_data.C.GetValues(), (int)check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        void SaveCompleted(object sender, WcStoredProcColumn_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", IfxTraceCategory.Enter);
                DataServiceInsertUpdateResponse newData = null;
                if (e.Result == null)
                {
                    // return some message to the calling object
                    newData = new DataServiceInsertUpdateResponse();
                    newData.Result = DataOperationResult.HandledException;
                    //RaiseEventAsyncSaveComplete(newData.Result, null);
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);
                }
                else
                {
                    newData = new DataServiceInsertUpdateResponse();
                    byte[] data = e.Result;
                    object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                    newData.SetFromObjectArray(array);
                }

                // Get the Result status
                if (newData.Result == DataOperationResult.Success)
                {
                    if (newData.ReturnCurrentRowOnInsertUpdate == true)
                    {
                        // If we returned the current row from the data store, then syncronize the wire object with it.
                        _data.C.ReplaceDataFromObjectArray(newData.CurrentValues);
                        RefreshFields();
                    }
                    else
                    {
                        // Otherwise:
                        //  Set the PK value from the data store incase we did an insert.
                        //  In theory the guid was already created by this busness object, but we're setting it here
                        //  just in case some code changed somewhere and it was created on the server.
                        _data.C._a = newData.guidPrimaryKey;
                        //  Set the new TimeStamp value
                        _data.C.SpCl_Stamp_noevents = newData.CurrentTimestamp;
                    }
                    _data.S.SetNotNew();
                    _data.S.SetValid();
                    _data.S.SetNotDirty();
                    _data.SetOriginalToCurrent();
                  
                    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);

                    //if (_parentType == ParentEditObjectType.EntitiyPropertiesControl)
                    //{
                    //    // A EntitiyPropertiesControl called the same method, therefore raise the following event
                    //    // allowing all parent conrol to reconfigure thier entity state.
                    //    // If this was called from editing a grid control, then this is not nessesacy since we 
                    //    // dont change the state of surrounding controls.
                    //    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                    //
                    //    // Rasie this event to the props control can call it's SetState method to refresh the UI
                    //    // The Entity List control will be updated via the PropertyChanged event which gets called from the RefreshFields call above.
                    //    if (newData.ReturnCurrentRowOnInsertUpdate == true)
                    //    {
                    //        RaiseEventAsyncSaveWithResponseComplete(newData, null);
                    //    }
                    //
                    //}
                    //else
                    //{
                    //    RaiseEventAsyncSaveComplete(newData.Result, null);
                    //}
                }
                else
                {
                    // Add the current date from the database to the wire obect's concurrent property
                    //if (newData.Result == DataOperationResult.ConcurrencyFailure)
                    //{
                    //    _data.X.ReplaceDataFromObjectArray(newData.CurrentValues);
                    //}
                    // ToDo:  we need to develope logic and code to pass user friendly error message 
                    // instead of null when the save failed.

                    // We had some type of failure, so raise this event regardless of the parent type 
                    // so the UI can notify the user.
                    //RaiseEventAsyncSaveComplete(newData.Result, null);
                    //*** use this instead
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _parentEditObject = null;
                _parentType = ParentEditObjectType.None;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", IfxTraceCategory.Leave);
            }
        }







        /// <summary>
        /// Calls a Delete method on the server to delete this entity from the data
        /// store.
        /// </summary>
        /// <returns>1 = Success, 2 = Failed</returns>
        public int  Entity_Delete()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", IfxTraceCategory.Enter);
//                // add credentials
//                WcStoredProcColumn_ValuesMngr retObject = ProxyWrapper.EntityProxyWrapper.WcStoredProcColumn_Delete(_data);
//                return 1;
//                //  Needs further design.  What do we do when the delete fails or there is an concurrency issue?
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Just a stub and not being used. Consider renaming this to ActivatOrDeactivate so
        /// it can be used either way.
        /// </summary>
        /// <returns>Returns 0 because this method is not currently functional.</returns>
        public int Entity_Deactivate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", IfxTraceCategory.Enter);
                //  This is currently setup to return a wire object, however, we might only need to return an int flag
                //  Needs further design
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", IfxTraceCategory.Leave);
            }
        }

        /// <returns>Returns 0 because this method is not currently functional.</returns>
        /// <summary>Just a stub and not being used.</summary>
        public int Entity_Remove()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", IfxTraceCategory.Enter);
                //  This is currently setup to return a wire object, however, we might only need to return an int flag
                //  Needs further design
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Copies the values from another data object (<see cref="EntityWireType.WcStoredProcColumn_ValuesMngr">WcStoredProcColumn_ValuesMngr</see>) to the one plugged into
        ///     this business object. This is called from the Save method because it returns a
        ///     fresh copy of this entity’s data from the data store incase modifications were made
        ///     in the process of saving.
        /// </summary>
        /// <param name="thisData">
        /// The data object (WcStoredProcColumn_ValuesMngr) plugged into this business object which data
        /// will be copied to.
        /// </param>
        /// <param name="newData">The data object (WcStoredProcColumn_ValuesMngr) which data will be copied from.</param>
        private void SyncValueObjectCurrentProperties(WcStoredProcColumn_ValuesMngr thisData, WcStoredProcColumn_ValuesMngr newData)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", IfxTraceCategory.Enter);
				thisData.C.SpCl_Id_noevents = newData.C.SpCl_Id_noevents;
				thisData.C.SpCl_Sp_Id_noevents = newData.C.SpCl_Sp_Id_noevents;
				thisData.C.SpCl_Name_noevents = newData.C.SpCl_Name_noevents;
				thisData.C.SpCl_ColIndex_noevents = newData.C.SpCl_ColIndex_noevents;
				thisData.C.SpCl_ColLetter_noevents = newData.C.SpCl_ColLetter_noevents;
				thisData.C.SpCl_DtNt_Id_noevents = newData.C.SpCl_DtNt_Id_noevents;
				thisData.C.SpCl_DtNt_Id_TextField_noevents = newData.C.SpCl_DtNt_Id_TextField_noevents;
				thisData.C.SpCl_DtSql_Id_noevents = newData.C.SpCl_DtSql_Id_noevents;
				thisData.C.SpCl_DtSql_Id_TextField_noevents = newData.C.SpCl_DtSql_Id_TextField_noevents;
				thisData.C.SpCl_IsNullable_noevents = newData.C.SpCl_IsNullable_noevents;
				thisData.C.SpCl_IsVisible_noevents = newData.C.SpCl_IsVisible_noevents;
				thisData.C.SpCl_Browsable_noevents = newData.C.SpCl_Browsable_noevents;
				thisData.C.SpCl_Notes_noevents = newData.C.SpCl_Notes_noevents;
				thisData.C.SpCl_IsActiveRow_noevents = newData.C.SpCl_IsActiveRow_noevents;
				thisData.C.SpCl_IsDeleted_noevents = newData.C.SpCl_IsDeleted_noevents;
				thisData.C.SpCl_CreatedUserId_noevents = newData.C.SpCl_CreatedUserId_noevents;
				thisData.C.SpCl_CreatedDate_noevents = newData.C.SpCl_CreatedDate_noevents;
				thisData.C.SpCl_UserId_noevents = newData.C.SpCl_UserId_noevents;
				thisData.C.UserName_noevents = newData.C.UserName_noevents;
				thisData.C.SpCl_LastModifiedDate_noevents = newData.C.SpCl_LastModifiedDate_noevents;
				thisData.C.SpCl_Stamp_noevents = newData.C.SpCl_Stamp_noevents;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", IfxTraceCategory.Leave);
            }
        }


		#endregion CRUD Methods


		#region General Methods and Properties


		#region Wire Methods

        /// <summary>
        /// Returns a business object implementing the IBusinessObject interface from a wire
        /// object. This assumes the WcStoredProcColumn_ValuesMngr being passed in has been fetched from a
        /// reliable data store or some other reliable source which gives us appropriate data and
        /// state.
        /// </summary>
        public static WcStoredProcColumn_Bll FromWire(WcStoredProcColumn_ValuesMngr valueObject)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", new ValuePair[] {new ValuePair("valueObject", valueObject) }, IfxTraceCategory.Enter);
                //  This assumes that each WcStoredProcColumn_ValuesMngr has been fetched from a db or some other reliable source which gives us the following state
                //  Hack, Hack;  we have to add the state here becuase of how its handled (or not handled) in WcStoredProcColumn_ValuesMngr due to the problem of loosing this value when passed accross the wire
                WcStoredProcColumn_Bll obj = new WcStoredProcColumn_Bll(valueObject, new EntityState(false, true, false));
                return obj;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Returns the current wire object plugged into this business object.</summary>
        public WcStoredProcColumn_ValuesMngr ToWire()
        {
            return _data;
        }

        /// <summary>The wire object property.</summary>
        public WcStoredProcColumn_ValuesMngr Wire
        {
            get { return _data; }
            set
            {
                _data = value;
            }
        }        

		#endregion Wire Methods


        /// <overloads>Get a list of current BrokenRules for this entity.</overloads>
        /// <summary>Retuns the current BrokenRules as list of strings.</summary>
        public List<string> GetBrokenRulesForEntity()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRulesForEntity();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Pass in a valid format as a string and return the current BrokenRules in a
        /// formatted list of strings.
        /// </summary>
        public List<string> GetBrokenRulesForEntity(string format)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", new ValuePair[] {new ValuePair("format", format) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRulesForEntity(format);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Leave);
            }
        }

        // get the current list of broken rules for a property
        /// <summary>Pass in a property name and return a list of its current BrokenRules.</summary>
        public List<vRuleItem> GetBrokenRulesForProperty(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRuleListForProperty(propertyName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Pass in a property name to find out if it’s valid or not.</summary>
        /// <returns>true = valid, false = not valid</returns>
        public bool IsPropertyValid(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.IsPropertyValid(propertyName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", IfxTraceCategory.Leave);
            }
        }

        public bool IsPropertyDirty(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);

                if (propertyName == null)
                {
                    throw new Exception("WcStoredProcColumn_Bll.IsPropertyDirty: Parameter 'propertyName' had no value.");
                }
                else if (propertyName.Trim().Length == 0)
                {
                    throw new Exception("WcStoredProcColumn_Bll.IsPropertyDirty: Parameter 'propertyName' had no value.");
                }

                switch (propertyName)
                {
                    case "SpCl_Sp_Id":
                        if (_data.C.SpCl_Sp_Id != _data.O.SpCl_Sp_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "SpCl_Name":
                        if (_data.C.SpCl_Name != _data.O.SpCl_Name)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "SpCl_ColIndex":
                        if (_data.C.SpCl_ColIndex != _data.O.SpCl_ColIndex)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "SpCl_ColLetter":
                        if (_data.C.SpCl_ColLetter != _data.O.SpCl_ColLetter)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "SpCl_DtNt_Id":
                        if (_data.C.SpCl_DtNt_Id != _data.O.SpCl_DtNt_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "SpCl_DtSql_Id":
                        if (_data.C.SpCl_DtSql_Id != _data.O.SpCl_DtSql_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "SpCl_IsNullable":
                        if (_data.C.SpCl_IsNullable != _data.O.SpCl_IsNullable)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "SpCl_IsVisible":
                        if (_data.C.SpCl_IsVisible != _data.O.SpCl_IsVisible)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "SpCl_Browsable":
                        if (_data.C.SpCl_Browsable != _data.O.SpCl_Browsable)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "SpCl_Notes":
                        if (_data.C.SpCl_Notes != _data.O.SpCl_Notes)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "SpCl_IsActiveRow":
                        if (_data.C.SpCl_IsActiveRow != _data.O.SpCl_IsActiveRow)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "SpCl_IsDeleted":
                        if (_data.C.SpCl_IsDeleted != _data.O.SpCl_IsDeleted)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "SpCl_CreatedUserId":
                        if (_data.C.SpCl_CreatedUserId != _data.O.SpCl_CreatedUserId)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "UserName":
                        if (_data.C.UserName != _data.O.UserName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    default:
                        throw new Exception("WcStoredProcColumn_Bll.IsPropertyDirty found no matching propery name for " + propertyName);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", IfxTraceCategory.Leave);
            }
        }

        public void SetDateFromString(string propName, string value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", new ValuePair[] {new ValuePair("value", value), new ValuePair("propName", propName) }, IfxTraceCategory.Enter);
                DateTime? dt = null;
                if (BLLHelper.IsDate(value) == true)
                {
                    dt = DateTime.Parse(value);
                }
                switch (propName)
                {
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", IfxTraceCategory.Leave);
            }
        }

        public object GetPropertyValueByKey(string key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", new ValuePair[] {new ValuePair("key", key) }, IfxTraceCategory.Enter);
                switch (key)
                {
                    case "SpCl_Name":
                        return SpCl_Name;

                    case "SpCl_ColIndex":
                        return SpCl_ColIndex;

                    case "SpCl_ColLetter":
                        return SpCl_ColLetter;

                    case "SpCl_DtNt_Id":
                        return SpCl_DtNt_Id;

                    case "SpCl_DtSql_Id":
                        return SpCl_DtSql_Id;

                    case "SpCl_IsNullable":
                        return SpCl_IsNullable;

                    case "SpCl_IsVisible":
                        return SpCl_IsVisible;

                    case "SpCl_Browsable":
                        return SpCl_Browsable;

                    case "SpCl_Notes":
                        return SpCl_Notes;

                    case "SpCl_IsActiveRow":
                        return SpCl_IsActiveRow;

                    case "UserName":
                        return UserName;

                    case "SpCl_LastModifiedDate":
                        return SpCl_LastModifiedDate;

                    default:
                        return GetPropertyValueByKeyCustomCode(key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", IfxTraceCategory.Leave);
            }
        }

        public string GetPropertyFormattedStringValueByKey(string key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", new ValuePair[] {new ValuePair("key", key) }, IfxTraceCategory.Enter);
                switch (key)
                {
                    case "EntityId":
                        return SpCl_Id.ToString();

                    case "SpCl_Name":
                        return SpCl_Name;

                    case "SpCl_ColIndex":
                        if (SpCl_ColIndex == null)
                        {
                            return null;
                        }
                        else
                        {
                            return SpCl_ColIndex.ToString();
                        }

                    case "SpCl_ColLetter":
                        return SpCl_ColLetter;

                    case "SpCl_DtNt_Id":
                        return SpCl_DtNt_Id_TextField;

                    case "SpCl_DtSql_Id":
                        return SpCl_DtSql_Id_TextField;

                    case "SpCl_IsNullable":
                        return SpCl_IsNullable.ToString();

                    case "SpCl_IsVisible":
                        return SpCl_IsVisible.ToString();

                    case "SpCl_Browsable":
                        return SpCl_Browsable.ToString();

                    case "SpCl_Notes":
                        return SpCl_Notes;

                    case "SpCl_IsActiveRow":
                        return SpCl_IsActiveRow.ToString();

                    case "UserName":
                        return UserName;

                    case "SpCl_LastModifiedDate":
                        if (SpCl_LastModifiedDate == null)
                        {
                            return null;
                        }
                        else
                        {
                            return SpCl_LastModifiedDate.ToString();
                        }

                    default:
                        return GetPropertyFormattedStringValueByKeyCustomCode(key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", IfxTraceCategory.Leave);
            }
        }

        
        #region General Properties

        /// <summary>
        ///     Returns a list of current BrokenRules for this entity as a list of <see cref="TypeServices.vRuleItem">vRuleItem</see> types.
        /// </summary>
        [Browsable(false)]
        public List<vRuleItem> GetBrokenRuleListForEntity()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", IfxTraceCategory.Enter);
                return BrokenRuleManagerProperty.GetBrokenRuleListForEntity();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", IfxTraceCategory.Leave);
            }
        }

        #endregion General Properties        

		#endregion General Methods and Properties


		#region Events


        /// <summary>
        ///     Raises the <see cref="CrudFailed">CrudFailed</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnCrudFailed(object sender, CrudFailedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Enter);
            CrudFailedEventHandler handler = CrudFailed;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Leave);
        }

        // THIS IS NOW OBSOLETE
        /// <summary>
        ///     Calls the <see cref="OnAsyncSaveComplete">OnAsyncSaveComplete</see> to raise the <see cref="AsyncSaveComplete">AsyncSaveComplete</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventAsyncSaveComplete(DataOperationResult result, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveComplete", IfxTraceCategory.Enter);
            AsyncSaveCompleteArgs e = new AsyncSaveCompleteArgs(result, failedReasonText);
            OnAsyncSaveComplete(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveComplete", IfxTraceCategory.Leave);
        }

        // THIS IS NOW OBSOLETE
        /// <summary>
        ///     Raises the <see cref="AsyncSaveComplete">AsyncSaveComplete</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnAsyncSaveComplete(object sender, AsyncSaveCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveComplete", IfxTraceCategory.Enter);
            AsyncSaveCompleteEventHandler handler = AsyncSaveComplete;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveComplete", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Calls the <see cref="OnAsyncSaveWithResponseComplete">OnAsyncSaveWithResponseComplete</see> to raise the <see cref="AsyncSaveWithResponseComplete">AsyncSaveWithResponseComplete</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventAsyncSaveWithResponseComplete(DataServiceInsertUpdateResponse response, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
            AsyncSaveWithResponseCompleteArgs e = new AsyncSaveWithResponseCompleteArgs(response, failedReasonText);
            OnAsyncSaveWithResponseComplete(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Raises the <see cref="AsyncSaveWithResponseComplete">AsyncSaveWithResponseComplete</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnAsyncSaveWithResponseComplete(object sender, AsyncSaveWithResponseCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
            AsyncSaveWithResponseCompleteEventHandler handler = AsyncSaveWithResponseComplete;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
        }










        void RaiseEventEntityRowReceived()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventEntityRowReceived", IfxTraceCategory.Enter);
            EntityRowReceivedEventHandler handler = EntityRowReceived;
            if (handler != null)
            {
                handler(this, new EntityRowReceivedArgs());
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventEntityRowReceived", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Calls the <see cref="OnCrudFailed">OnCrudFailed</see> to raise the <see cref="CrudFailed">CrudFailed</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventCrudFailed(int failureCode, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCrudFailed", IfxTraceCategory.Enter);
            CrudFailedArgs e = new CrudFailedArgs(failureCode, failedReasonText);
            OnCrudFailed(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCrudFailed", IfxTraceCategory.Leave);
        }


        /// <summary>
        ///     Calls the <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see>
        ///     method to raise the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event passing in a
        ///     reference of this business object for the ActiveBusinessObject parameter. As it
        ///     bubbles up to an Entity Properties control, that control will pass in a reference
        ///     of itself for the ActivePropertiesControl parameter. As it bubbles up to the Entity
        ///     Manager control, that control will pass in a reference of itself for the
        ///     ActiveEntityControl parameter. It should continue to bubble up to the top level
        ///     control. This notifies all controls along the about which controls are active and
        ///     the current state so they can always be configures accordingly. Now that the top
        ///     level control (perhaps the main application window) has a reference to these 3
        ///     important objects, it can easily communicate with then as the use interacts with
        ///     the application.
        /// </summary>        
        void RaiseEventCurrentEntityStateChanged(EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCurrentEntityStateChanged", new ValuePair[] {new ValuePair("state", state) }, IfxTraceCategory.Enter);
            CurrentEntityStateArgs e = new CurrentEntityStateArgs(state, null, null, this);
            OnCurrentEntityStateChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCurrentEntityStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     Raises the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see>
        ///     event passing in a reference of this business object for the ActiveBusinessObject
        ///     parameter. As it bubbles up to an Entity Properties control, that control will pass
        ///     in a reference of itself for the ActivePropertiesControl parameter. As it bubbles
        ///     up to the Entity Manager control, that control will pass in a reference of itself
        ///     for the ActiveEntityControl parameter. It should continue to bubble up to the top
        ///     level control. This notifies all controls along the about which controls are active
        ///     and the current state so they can always be configures accordingly. Now that the
        ///     top level control (perhaps the main application window) has a reference to these 3
        ///     important objects, it can easily communicate with then as the use interacts with
        ///     the application.
        /// </summary>
        void OnCurrentEntityStateChanged(object sender, CurrentEntityStateArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", new ValuePair[] {new ValuePair("e.State", e.State) }, IfxTraceCategory.Enter);
            CurrentEntityStateEventHandler handler = CurrentEntityStateChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     This method is obsolete and is replaced with <see cref="RaiseEventControlValidStateChanged">RaiseEventControlValidStateChanged</see> which
        ///     is called in the validation section of this class in the partial class
        ///     WcStoredProcColumn.bll.vld.cs code file.
        /// </summary>
        void RaiseEventBrokenRuleChanged(string rule)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventBrokenRuleChanged", new ValuePair[] {new ValuePair("rule", rule) }, IfxTraceCategory.Enter);
            BrokenRuleArgs e = new BrokenRuleArgs(rule);
            OnBrokenRuleChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventBrokenRuleChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>This method is mostly obsolete, but remains because it’s an easy way to be
        ///     notified of the current broken rule.</para>
        /// 	<para>
        ///         This method is hooked to the <see cref="BrokenRuleChanged">BrokenRuleChanged</see> event which is initiated by
        ///         BrokenRuleManager and bubbles up to the business object and then to this
        ///         control (ucWcStoredProcColumnProps) (or in some cases a ucEntityList control when it’s in
        ///         Read/Write mode).
        ///     </para>
        /// 	<para><br/>
        ///     For information on how Broken Rules are currently being managed, see (in the order
        ///     of execution):</para>
        /// 	<list type="bullet">
        /// 		<item>In the business object’s data property – FieldName_Validate();</item>
        /// 		<item>
        ///             In the FieldName_Validate method this line executes:<br/>
        ///             _brokenRuleManager.<see cref="TypeServices.BrokenRuleManager.AddBrokenRuleForProperty">AddBrokenRuleForProperty</see>("FieldName",
        ///             BROKENRULE_ FieldName _SomeRuleName);
        ///         </item>
        /// 		<item>
        /// 			<see cref="TypeServices.BrokenRuleManager.SetEntityValidState">BrokenRuleManager.SetEntityValidState()</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
       private void OnBrokenRuleChanged(object sender, BrokenRuleArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Enter);
            BrokenRuleEventHandler handler = BrokenRuleChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// Notifies an event handler that a property value as change. Currently not being
        /// uses.
        /// </summary>
        private void OnPropertyValueChanged(object sender, PropertyValueChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnPropertyValueChanged", new ValuePair[] {new ValuePair("e.PropertyName", e.PropertyName), new ValuePair("e.IsValid", e.IsValid), new ValuePair("e.BrokenRules", e.BrokenRules) }, IfxTraceCategory.Enter);
            PropertyValueChangedEventHandler handler = PropertyValueChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnPropertyValueChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     Replaces the RaiseEventBrokenRuleChanged method and used in the validation section
        ///     of this class in the partial class WcStoredProcColumn.bll.vld.cs code file. Validation code will
        ///     pass in property name and an isValid flag. This will call the <see cref="OnControlValidStateChanged">OnControlValidStateChanged</see> method which will
        ///     raise the <see cref="ControlValidStateChanged">ControlValidStateChanged</see> event
        ///     up to the parent. The parent will then take the appropriate actions such as setting
        ///     the control’s valid/not valid appearance.
        /// </summary>
        void RaiseEventControlValidStateChanged(string propertyName, bool isValid, bool isDirty)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventControlValidStateChanged", new ValuePair[] {new ValuePair("isValid", isValid) }, IfxTraceCategory.Enter);
            ControlValidStateChangedArgs e = new ControlValidStateChangedArgs(propertyName, isValid, isDirty);
            OnControlValidStateChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventControlValidStateChanged", IfxTraceCategory.Leave);
        }       

        /// <summary>
        /// Raise the ControlValidStateChanged event up to the parent. The parent will then
        /// take the appropriate actions such as setting the control’s valid/not valid
        /// appearance.
        /// </summary>
        private void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", new ValuePair[] {new ValuePair("e.IsValid", e.IsValid) }, IfxTraceCategory.Enter);
            ControlValidStateChangedEventHandler handler = ControlValidStateChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcColumnProps.OnRestrictedTextLengthChanged">ucWcStoredProcColumnProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcColumnProps.SetRestrictedStringLengthText">ucWcStoredProcColumnProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void RaiseEventRestrictedTextLengthChanged()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Enter);
            RestrictedTextLengthChangedArgs e = new RestrictedTextLengthChangedArgs();
            RestrictedTextLengthChangedEventHandler handler = RestrictedTextLengthChanged;
            if (handler != null)
            {
                handler(this, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<param name="oldVal">The original property value.</param>
        /// 	<param name="newVal">The new property value.</param>        
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcColumnProps.OnRestrictedTextLengthChanged">ucWcStoredProcColumnProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcColumnProps.SetRestrictedStringLengthText">ucWcStoredProcColumnProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void RaiseEventRestrictedTextLengthChanged(string oldVal, string newVal)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Enter);
            bool lenChanged = false;
            if (oldVal == null && newVal != null)
            {
                if (newVal.Trim().Length > 0)
                {
                    lenChanged = true;
                }
            }
            else if (newVal == null && oldVal != null)
            {
                if (oldVal.Trim().Length > 0)
                {
                    lenChanged = true;
                }
            }
            else if (oldVal != null && newVal != null)
            {
                if (oldVal.Trim().Length != newVal.Trim().Length)
                {
                    lenChanged = true;
                }
            }
            if (lenChanged == true)
            {
                RestrictedTextLengthChangedArgs e = new RestrictedTextLengthChangedArgs();
                RestrictedTextLengthChangedEventHandler handler = RestrictedTextLengthChanged;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Leave);
        }        


		#endregion Events


		#region Flag Props


        /// <summary>
        ///     The name of the (text type – TextBox, TextBlock, etc.) control in the UI that
        ///     currently has the focus. This is used by the <see cref="ActiveRestrictedStringPropertyLength">ActiveRestrictedStringPropertyLength</see>
        ///     method to which returns the remaining available text length for the text control
        ///     with that name. It was decided to persist the name of the UI control in this
        ///     business object property rather than in the UI because it seemed to make sense to
        ///     have all of this logic centralized, and also because it simplifies things.
        /// </summary>
        [Browsable(false)]
        public string ActiveRestrictedStringProperty
        {
            get { return _activeRestrictedStringProperty; }
            set 
            { 
                _activeRestrictedStringProperty = value;
            }
        }

        /// <summary>
        ///     Returns the remaining available text length for the text control named by
        ///     <see cref="ActiveRestrictedStringProperty">ActiveRestrictedStringProperty</see>.
        /// </summary>
        [Browsable(false)]
        public int ActiveRestrictedStringPropertyLength
        {
            get
            {
                switch (_activeRestrictedStringProperty)
                {
                    case "SpCl_Name":
                        if (_data.C.SpCl_Name == null)
                        {
                            return STRINGSIZE_SpCl_Name;
                        }
                        else
                        {
                            return (STRINGSIZE_SpCl_Name - _data.C.SpCl_Name.Length);
                        }
                        break;
                    
                    case "SpCl_ColLetter":
                        if (_data.C.SpCl_ColLetter == null)
                        {
                            return STRINGSIZE_SpCl_ColLetter;
                        }
                        else
                        {
                            return (STRINGSIZE_SpCl_ColLetter - _data.C.SpCl_ColLetter.Length);
                        }
                        break;
                    
                    case "SpCl_Notes":
                        if (_data.C.SpCl_Notes == null)
                        {
                            return STRINGSIZE_SpCl_Notes;
                        }
                        else
                        {
                            return (STRINGSIZE_SpCl_Notes - _data.C.SpCl_Notes.Length);
                        }
                        break;
                    }
                return 0;
            }
        }

 

		#endregion Flag Props


		#region Data Props


        /// <summary>
        ///     Called by data field properties in their getter block. This method will determine
        ///     if he <see cref="TypeServices.EntityState">state</see> has changed by the getter
        ///     being called. If it has changed, the <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see>
        ///     method will be called to notify the object using this business object.
        /// </summary>
       private void CheckEntityState(EntityStateSwitch es)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", new ValuePair[] {new ValuePair("_data.S.Switch", _data.S.Switch), new ValuePair("es", es) }, IfxTraceCategory.Enter);
                if (es != _data.S.Switch)
                {
                    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// This property does nothing and is used as a stub for a 'Rich Data Grid Column' binding to this entity
        /// </summary>
        public string RichGrid
        {
            get { return ""; }
        }



        public string DataRowName
        {
            get { return "No Name Selected"; }
        }




        private IBusinessObjectV2 _parentBusinessObject = null;

        public IBusinessObjectV2 ParentBusinessObject
        {
            get { return _parentBusinessObject; }
            set { _parentBusinessObject = value; }
        }

       public Guid? GuidPrimaryKey()
       {
           return _data.C.SpCl_Id; 
       }

       public long? LongPrimaryKey()
       {
           throw new NotImplementedException("WcStoredProcColumn_Bll LongPrimaryKey() Not Implemented"); 
       }

       public int? IntPrimaryKey()
       {
           throw new NotImplementedException("WcStoredProcColumn_Bll LongPrimaryKey() Not Implemented"); 
       }

       public short? ShortPrimaryKey()
       {
           throw new NotImplementedException("WcStoredProcColumn_Bll LongPrimaryKey() Not Implemented"); 
       }

       public byte? BytePrimaryKey()
       {
           throw new NotImplementedException("WcStoredProcColumn_Bll LongPrimaryKey() Not Implemented"); 
       }

       public object ObjectPrimaryKey()
       {
           return _data.C.SpCl_Id;
       }


        /// <summary>
        /// This is the Standing Foreign Key property. This value remains constant when
        /// calling the new method where all data fields are cleared and set to their ‘new’ default
        /// values. This allows creating new entities for the same parent. When an entity is
        /// fetched from the data store and used as the current entity in this business object, the
        /// Standing Foreign Key value is reset using the value from the fetched entity.
        /// </summary>
        public Guid StandingFK
        {
            get { return _StandingFK; }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    _StandingFK = value;
                    _data.C.SpCl_Sp_Id_noevents = _StandingFK;
                    _data.O.SpCl_Sp_Id_noevents = _StandingFK;
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", IfxTraceCategory.Leave);
                }
            }
        }

       /// <summary>
       /// 	<para>The Primary Key for WcStoredProcColumn</para>
       /// </summary>
        public Guid SpCl_Id
        {
            get
            {
                return _data.C.SpCl_Id;
            }
        }

		
        public Guid? SpCl_Sp_Id
        {
            get
            {
                return _data.C.SpCl_Sp_Id;
            }
        }

		
        public String SpCl_Name
        {
            get
            {
                return _data.C.SpCl_Name;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Name - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.SpCl_Name == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.SpCl_Name == null || _data.C.SpCl_Name.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.SpCl_Name == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.SpCl_Name = null;
                    }
                    else
                    {
                        _data.C.SpCl_Name = value.Trim();
                    }
                    //else if ((_data.C.SpCl_Name == null || _data.C.SpCl_Name.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.SpCl_Name == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.SpCl_Name == null || _data.C.SpCl_Name.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.SpCl_Name = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.SpCl_Name = value;
                    //}
                    CustomPropertySetterCode("SpCl_Name");
                    SpCl_Name_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.SpCl_Name, _data.C.SpCl_Name);
                        
					Notify("SpCl_Name");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Name - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Name - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="SpCl_ColIndex">SpCl_ColIndex</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the SpCl_ColIndex field. If it’s not numeric, then SpCl_ColIndex is set to
        ///     null.</para>
        /// </summary>
        public String SpCl_ColIndex_asString
        {
            get
            {
                if (null == _data.C.SpCl_ColIndex)
                {
                    return "";
                }
                else
                {
                    return _data.C.SpCl_ColIndex.ToString();
                }
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_ColIndex - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
					if (BLLHelper.IsNumber(value))
                    {
                        try
                        {
							Double newValNotNull;
                            Double.TryParse(Convert.ToString(value), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.CurrentInfo, out newValNotNull);
                            SpCl_ColIndex = (Int32)newValNotNull;
                        }
                        catch (FormatException e)
                        {
                            SpCl_ColIndex = null;
                        }
                    }
                    else
                    {
                        SpCl_ColIndex = null;
                    }
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_ColIndex - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_ColIndex - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Int32? SpCl_ColIndex
        {
            get
            {
                return _data.C.SpCl_ColIndex;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_ColIndex - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.SpCl_ColIndex == value) { return; }
                    _data.C.SpCl_ColIndex = value;
                    CustomPropertySetterCode("SpCl_ColIndex");
                    SpCl_ColIndex_Validate();
                    CheckEntityState(es);
                    Notify("SpCl_ColIndex");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_ColIndex - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_ColIndex - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String SpCl_ColLetter
        {
            get
            {
                return _data.C.SpCl_ColLetter;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_ColLetter - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.SpCl_ColLetter == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.SpCl_ColLetter == null || _data.C.SpCl_ColLetter.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.SpCl_ColLetter == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.SpCl_ColLetter = null;
                    }
                    else
                    {
                        _data.C.SpCl_ColLetter = value.Trim();
                    }
                    //else if ((_data.C.SpCl_ColLetter == null || _data.C.SpCl_ColLetter.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.SpCl_ColLetter == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.SpCl_ColLetter == null || _data.C.SpCl_ColLetter.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.SpCl_ColLetter = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.SpCl_ColLetter = value;
                    //}
                    CustomPropertySetterCode("SpCl_ColLetter");
                    SpCl_ColLetter_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.SpCl_ColLetter, _data.C.SpCl_ColLetter);
                        
					Notify("SpCl_ColLetter");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_ColLetter - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_ColLetter - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public String SpCl_DtNt_Id_TextField
        {
            get
            {
                return _data.C.SpCl_DtNt_Id_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtNt_Id Setter", IfxTraceCategory.Enter);
                    if (_data.C.SpCl_DtNt_Id_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.SpCl_DtNt_Id_TextField == null || _data.C.SpCl_DtNt_Id_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.SpCl_DtNt_Id_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.SpCl_DtNt_Id_TextField = value;
                    }
                    CustomPropertySetterCode("SpCl_DtNt_Id_TextField");
                    Notify("SpCl_DtNt_Id_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtNt_Id Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtNt_Id Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Int32? SpCl_DtNt_Id
        {
            get
            {
                return _data.C.SpCl_DtNt_Id;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtNt_Id - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.SpCl_DtNt_Id == value) { return; }
                    _data.C.SpCl_DtNt_Id = value;
                    CustomPropertySetterCode("SpCl_DtNt_Id");
                    SpCl_DtNt_Id_Validate();
                    CheckEntityState(es);
                    Notify("SpCl_DtNt_Id");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty.FindItemById(_data.C.SpCl_DtNt_Id);
                    if (obj == null)
                    {
                        SpCl_DtNt_Id_TextField = "";
                    }
                    else
                    {
                        SpCl_DtNt_Id_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtNt_Id - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtNt_Id - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public String SpCl_DtSql_Id_TextField
        {
            get
            {
                return _data.C.SpCl_DtSql_Id_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtSql_Id Setter", IfxTraceCategory.Enter);
                    if (_data.C.SpCl_DtSql_Id_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.SpCl_DtSql_Id_TextField == null || _data.C.SpCl_DtSql_Id_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.SpCl_DtSql_Id_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.SpCl_DtSql_Id_TextField = value;
                    }
                    CustomPropertySetterCode("SpCl_DtSql_Id_TextField");
                    Notify("SpCl_DtSql_Id_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtSql_Id Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtSql_Id Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Int32? SpCl_DtSql_Id
        {
            get
            {
                return _data.C.SpCl_DtSql_Id;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtSql_Id - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.SpCl_DtSql_Id == value) { return; }
                    _data.C.SpCl_DtSql_Id = value;
                    CustomPropertySetterCode("SpCl_DtSql_Id");
                    SpCl_DtSql_Id_Validate();
                    CheckEntityState(es);
                    Notify("SpCl_DtSql_Id");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty.FindItemById(_data.C.SpCl_DtSql_Id);
                    if (obj == null)
                    {
                        SpCl_DtSql_Id_TextField = "";
                    }
                    else
                    {
                        SpCl_DtSql_Id_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtSql_Id - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_DtSql_Id - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean SpCl_IsNullable
        {
            get
            {
                return _data.C.SpCl_IsNullable;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsNullable - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.SpCl_IsNullable == value) { return; }
                    _data.C.SpCl_IsNullable = value;
                    CustomPropertySetterCode("SpCl_IsNullable");
                    SpCl_IsNullable_Validate();
                    CheckEntityState(es);
                    Notify("SpCl_IsNullable");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsNullable - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsNullable - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean SpCl_IsVisible
        {
            get
            {
                return _data.C.SpCl_IsVisible;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsVisible - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.SpCl_IsVisible == value) { return; }
                    _data.C.SpCl_IsVisible = value;
                    CustomPropertySetterCode("SpCl_IsVisible");
                    SpCl_IsVisible_Validate();
                    CheckEntityState(es);
                    Notify("SpCl_IsVisible");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsVisible - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsVisible - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean SpCl_Browsable
        {
            get
            {
                return _data.C.SpCl_Browsable;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Browsable - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.SpCl_Browsable == value) { return; }
                    _data.C.SpCl_Browsable = value;
                    CustomPropertySetterCode("SpCl_Browsable");
                    SpCl_Browsable_Validate();
                    CheckEntityState(es);
                    Notify("SpCl_Browsable");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Browsable - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Browsable - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String SpCl_Notes
        {
            get
            {
                return _data.C.SpCl_Notes;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Notes - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.SpCl_Notes == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.SpCl_Notes == null || _data.C.SpCl_Notes.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.SpCl_Notes == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.SpCl_Notes = null;
                    }
                    else
                    {
                        _data.C.SpCl_Notes = value.Trim();
                    }
                    //else if ((_data.C.SpCl_Notes == null || _data.C.SpCl_Notes.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.SpCl_Notes == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.SpCl_Notes == null || _data.C.SpCl_Notes.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.SpCl_Notes = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.SpCl_Notes = value;
                    //}
                    CustomPropertySetterCode("SpCl_Notes");
                    SpCl_Notes_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.SpCl_Notes, _data.C.SpCl_Notes);
                        
					Notify("SpCl_Notes");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Notes - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_Notes - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean SpCl_IsActiveRow
        {
            get
            {
                return _data.C.SpCl_IsActiveRow;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsActiveRow - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.SpCl_IsActiveRow == value) { return; }
                    _data.C.SpCl_IsActiveRow = value;
                    CustomPropertySetterCode("SpCl_IsActiveRow");
                    SpCl_IsActiveRow_Validate();
                    CheckEntityState(es);
                    Notify("SpCl_IsActiveRow");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsActiveRow - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpCl_IsActiveRow - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean SpCl_IsDeleted
        {
            get
            {
                return _data.C.SpCl_IsDeleted;
            }
        }

		
        public Guid? SpCl_CreatedUserId
        {
            get
            {
                return _data.C.SpCl_CreatedUserId;
            }
        }


		/// <summary>
        ///  Use this property when binding in Grids so the Notify method won't prevent you from entering complete dates or the year part of a date.  Notify will refresh the UI cutting off anything after the first diget of the month.
        /// </summary>
        public String SpCl_CreatedDate_asString
        {
            get
            {
                if (null == _data.C.SpCl_CreatedDate)
                {
                    return "";
                }
                else
                {
                    return _data.C.SpCl_CreatedDate.ToString();
                }
            }
        }

		
        public DateTime? SpCl_CreatedDate
        {
            get
            {
                return _data.C.SpCl_CreatedDate;
            }
        }

		
        public Guid? SpCl_UserId
        {
            get
            {
                return _data.C.SpCl_UserId;
            }
        }


        public String UserName
        {
            get
            {
                return _data.C.UserName;
            }
        }


		/// <summary>
        ///  Use this property when binding in Grids so the Notify method won't prevent you from entering complete dates or the year part of a date.  Notify will refresh the UI cutting off anything after the first diget of the month.
        /// </summary>
        public String SpCl_LastModifiedDate_asString
        {
            get
            {
                if (null == _data.C.SpCl_LastModifiedDate)
                {
                    return "";
                }
                else
                {
                    return _data.C.SpCl_LastModifiedDate.ToString();
                }
            }
        }

		
        public DateTime? SpCl_LastModifiedDate
        {
            get
            {
                return _data.C.SpCl_LastModifiedDate;
            }
        }


        public Byte[] SpCl_Stamp
        {
            get
            {
                return _data.C.SpCl_Stamp;
            }
        }



		#endregion Data Props


		#region Concurrency and Data State


        private bool DeterminConcurrencyCheck(UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", IfxTraceCategory.Enter);
                switch (check)
                {
                    case UseConcurrencyCheck.UseDefaultSetting:
                        return true;
                    case UseConcurrencyCheck.UseConcurrencyCheck:
                        return true;
                    case UseConcurrencyCheck.BypassConcurrencyCheck:
                        return false;
                }
                return false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     This is a reference to the current data (<see cref="EntityWireType.WcStoredProcColumn_Values">WcStoredProcColumn_Values</see>) for this business object
        ///     (WcStoredProcColumn_Bll). The Values Manager (<see cref="EntityWireType.WcStoredProcColumn_ValuesMngr">WcStoredProcColumn_ValuesMngr</see>) has three sets of WcStoredProcColumn
        ///     data used for managing state and concurrency. For more information about the other
        ///     two sets of data, <see cref="Original">Original</see> and <see cref="Concurrency">Concurrency</see>, and how they are used, see the overviews of
        ///     <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Concurrency_Overview.html">
        ///     Concurrency</a> and <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Wire_Object_Overview.html">
        ///     Wire Objects</a>.
        /// </summary>
        /// <seealso cref="Original">Original Property</seealso>
        /// <seealso cref="Concurrency">Concurrency Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcStoredProcColumn_Values Current
        {
            get
            {
                return _data.C;
            }
            set
            {
                //Management Code
                _data.C = value;
            }
        }

        /// <summary>
        ///     This is a reference to the original data (<see cref="EntityWireType.WcStoredProcColumn_Values">WcStoredProcColumn_Values</see>) for this business object
        ///     (WcStoredProcColumn_Bll) which was either data for a ‘new-not dirty’ WcStoredProcColumn or data for a WcStoredProcColumn
        ///     retrieved from the data store. The Values Manager (<see cref="EntityWireType.WcStoredProcColumn_ValuesMngr">WcStoredProcColumn_ValuesMngr</see>) has three sets of WcStoredProcColumn
        ///     data used for managing state and concurrency. For more information about the other
        ///     two sets of data, <see cref="Current">Current</see> and <see cref="Concurrency">Concurrency</see>, and how they are used, see the overview of
        ///     Concurrency and Wire Objects.
        /// </summary>
        /// <seealso cref="Current">Current Property</seealso>
        /// <seealso cref="Concurrency">Concurrency Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcStoredProcColumn_Values Original
        {
            get
            {
                return _data.O;
            }
            set
            {
                //Management Code
                _data.O = value;
            }
        }

        /// <summary>*** Need to review before documenting ***</summary>
        /// <seealso cref="Current">Current Property</seealso>
        /// <seealso cref="Original">Original Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcStoredProcColumn_Values Concurrency
        {
            get
            {
                return _data.X;
            }
            set
            {
                //Management Code
                _data.X = value;
            }
        }

        /// <value>
        ///     This class manages the <see cref="TypeServices.EntityState">EntityStateSwitch</see>
        ///     and is used to describe the current <see cref="TypeServices.EntityState">state</see> of this business object such as IsDirty,
        ///     IsValid and IsNew. The combinations of these six possible values describe the
        ///     entity state and are used for logic in configuring settings in the business object
        ///     as well as the UI.
        /// </value>
        /// <seealso cref="EntityWireType.WcStoredProcColumn_ValuesMngr">WcStoredProcColumn_ValuesMngr Class</seealso>
        [Browsable(false)]
        public override EntityState State
        {
            get
            {
                return _data.S;
            }
            set
            {
                //Management Code
                _data.S = value;
            }
        }

        /// <summary>
        ///     This is the combination of IsDirty, IsValid and IsNew and is managed by the
        ///     <see cref="TypeServices.EntityState">EntityState</see> class. The combinations of
        ///     these six possible values describe the entity <see cref="TypeServices.EntityState">state</see> and are used for logic in configuring
        ///     settings in the business object as well as the UI.
        /// </summary>
        /// <seealso cref="EntityWireType.WcStoredProcColumn_ValuesMngr">WcStoredProcColumn_ValuesMngr Class</seealso>
        public EntityStateSwitch StateSwitch
        {
            get
            {
                return _data.S.Switch;
            }
        }




		#endregion Concurrency and Data State

        [Browsable(false)]
        public override object this[string name]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                //Management Code
                throw new NotImplementedException();
            }
        }


		#region ITraceItem Members


        /// <summary>
        /// 	<para>
        ///         Used in IFX (the instrumentation framework) for tracing. This returns the short
        ///         version of the <see cref="ManagementServices.TraceItemList">TraceItemList</see>. The ‘Short Version’
        ///         returns only the entity’s data fields, <see cref="TypeServices.BrokenRuleManager.GetBrokenRuleListForEntity">BrokenRules</see>
        ///         and <see cref="TypeServices.EntityStateSwitch">EntityStateSwitch</see>.
        ///         However, it also calls the <see cref="GetTraceItemsShortListCustom">GetTraceItemsShortListCustom</see> method in the
        ///         WcStoredProcColumn.bll.cust.cs code file so a developer can customize the list by adding
        ///         additional items to it.
        ///     </para>
        /// 	<para>
        ///         Keep in mind that this is the short versions so don’t get carried away adding
        ///         to many things to it. For a robust and extensive list of items to record in the
        ///         trace, use the <see cref="GetTraceItemsLongList">GetTraceItemsLongList</see>
        ///         and <see cref="GetTraceItemsLongListCustom">GetTraceItemsLongListCustom</see>
        ///         methods.
        ///     </para>
        /// </summary>
        public TraceItemList GetTraceItemsShortList()
        {
            if (null == _traceItems)
            {
                _traceItems = new TraceItemList("WcStoredProcColumn_Bll");
            }
            else
            {
                _traceItems.Clear();
            }
            _traceItems.Add("StateSwitch", StateSwitch.ToString(), TraceDataTypes.String);
            _traceItems.Add("Broken Rules", BrokenRuleManagerProperty.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_Id", _data.C.SpCl_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("SpCl_Sp_Id", _data.C.SpCl_Sp_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("SpCl_Name", _data.C.SpCl_Name.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_ColIndex", _data.C.SpCl_ColIndex.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("SpCl_ColLetter", _data.C.SpCl_ColLetter.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_DtNt_Id", _data.C.SpCl_DtNt_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("SpCl_DtNt_Id_TextField", _data.C.SpCl_DtNt_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_DtSql_Id", _data.C.SpCl_DtSql_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("SpCl_DtSql_Id_TextField", _data.C.SpCl_DtSql_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_IsNullable", _data.C.SpCl_IsNullable.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("SpCl_IsVisible", _data.C.SpCl_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("SpCl_Browsable", _data.C.SpCl_Browsable.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("SpCl_Notes", _data.C.SpCl_Notes.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_IsActiveRow", _data.C.SpCl_IsActiveRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("SpCl_IsDeleted", _data.C.SpCl_IsDeleted.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("SpCl_CreatedUserId", _data.C.SpCl_CreatedUserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("SpCl_CreatedDate", _data.C.SpCl_CreatedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("SpCl_UserId", _data.C.SpCl_UserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("UserName", _data.C.UserName.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_LastModifiedDate", _data.C.SpCl_LastModifiedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("SpCl_Stamp", _data.C.SpCl_Stamp.ToString(), TraceDataTypes.ByteArray);
			GetTraceItemsShortListCustom();
            return _traceItems;
        }

        /// <summary>
        /// 	<para>
        ///         Used in IFX (the instrumentation framework) for tracing. By default, this is
        ///         code-genned to return the same information as <see cref="GetTraceItemsShortList">GetTraceItemsShortList</see>, but is intended for
        ///         developers to add additional informaiton that would be helpful in a trace. This
        ///         method also calls the <see cref="GetTraceItemsLongListCustom">GetTraceItemsLongListCustom</see> method in the
        ///         WcStoredProcColumn.bll.cust.cs code file so a developer can customize the list by adding
        ///         additional items to it.
        ///     </para>
        /// 	<para>
        ///         Remember: <see cref="GetTraceItemsShortList">GetTraceItemsShortList</see> is
        ///         intended for a limited list of items (the basic items) to trace and
        ///         <strong>GetTraceItemsLongList</strong> is intended for a robust and extensive
        ///         list of items to record in the trace.
        ///     </para>
        /// </summary>
        public TraceItemList GetTraceItemsLongList()
        {
            if (null == _traceItems)
            {
                _traceItems = new TraceItemList("WcStoredProcColumn_Bll");
            }
            else
            {
                _traceItems.Clear();
            }
            _traceItems.Add("StateSwitch", StateSwitch.ToString(), TraceDataTypes.String);
            _traceItems.Add("Broken Rules", BrokenRuleManagerProperty.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_Id", _data.C.SpCl_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("SpCl_Sp_Id", _data.C.SpCl_Sp_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("SpCl_Name", _data.C.SpCl_Name.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_ColIndex", _data.C.SpCl_ColIndex.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("SpCl_ColLetter", _data.C.SpCl_ColLetter.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_DtNt_Id", _data.C.SpCl_DtNt_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("SpCl_DtNt_Id_TextField", _data.C.SpCl_DtNt_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_DtSql_Id", _data.C.SpCl_DtSql_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("SpCl_DtSql_Id_TextField", _data.C.SpCl_DtSql_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_IsNullable", _data.C.SpCl_IsNullable.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("SpCl_IsVisible", _data.C.SpCl_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("SpCl_Browsable", _data.C.SpCl_Browsable.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("SpCl_Notes", _data.C.SpCl_Notes.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_IsActiveRow", _data.C.SpCl_IsActiveRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("SpCl_IsDeleted", _data.C.SpCl_IsDeleted.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("SpCl_CreatedUserId", _data.C.SpCl_CreatedUserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("SpCl_CreatedDate", _data.C.SpCl_CreatedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("SpCl_UserId", _data.C.SpCl_UserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("UserName", _data.C.UserName.ToString(), TraceDataTypes.String);
			_traceItems.Add("SpCl_LastModifiedDate", _data.C.SpCl_LastModifiedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("SpCl_Stamp", _data.C.SpCl_Stamp.ToString(), TraceDataTypes.ByteArray);
			GetTraceItemsLongListCustom();
            return _traceItems;
        }

        public object[] GetTraceData()
        {
            object[] data = new object[20];
            data[0] = new object[] { "StateSwitch", StateSwitch.ToString() };
            data[1] = new object[] { "BrokenRuleManagerProperty", BrokenRuleManagerProperty.ToString() };
            data[2] = new object[] { "SpCl_Id", _data.C.SpCl_Id.ToString() };
            data[3] = new object[] { "SpCl_Sp_Id", _data.C.SpCl_Sp_Id.ToString() };
            data[4] = new object[] { "SpCl_Name", _data.C.SpCl_Name == null ? "<Null>" : _data.C.SpCl_Name.ToString() };
            data[5] = new object[] { "SpCl_ColIndex", _data.C.SpCl_ColIndex == null ? "<Null>" : _data.C.SpCl_ColIndex.ToString() };
            data[6] = new object[] { "SpCl_ColLetter", _data.C.SpCl_ColLetter == null ? "<Null>" : _data.C.SpCl_ColLetter.ToString() };
            data[7] = new object[] { "SpCl_DtNt_Id", _data.C.SpCl_DtNt_Id == null ? "<Null>" : _data.C.SpCl_DtNt_Id.ToString() };
            data[8] = new object[] { "SpCl_DtNt_Id_TextField", _data.C.SpCl_DtNt_Id_TextField == null ? "<Null>" : _data.C.SpCl_DtNt_Id_TextField.ToString() };
            data[9] = new object[] { "SpCl_DtSql_Id", _data.C.SpCl_DtSql_Id == null ? "<Null>" : _data.C.SpCl_DtSql_Id.ToString() };
            data[10] = new object[] { "SpCl_DtSql_Id_TextField", _data.C.SpCl_DtSql_Id_TextField == null ? "<Null>" : _data.C.SpCl_DtSql_Id_TextField.ToString() };
            data[11] = new object[] { "SpCl_IsNullable", _data.C.SpCl_IsNullable.ToString() };
            data[12] = new object[] { "SpCl_IsVisible", _data.C.SpCl_IsVisible.ToString() };
            data[13] = new object[] { "SpCl_Browsable", _data.C.SpCl_Browsable.ToString() };
            data[14] = new object[] { "SpCl_Notes", _data.C.SpCl_Notes == null ? "<Null>" : _data.C.SpCl_Notes.ToString() };
            data[15] = new object[] { "SpCl_IsActiveRow", _data.C.SpCl_IsActiveRow.ToString() };
            data[16] = new object[] { "SpCl_IsDeleted", _data.C.SpCl_IsDeleted.ToString() };
            data[17] = new object[] { "SpCl_CreatedUserId", _data.C.SpCl_CreatedUserId == null ? "<Null>" : _data.C.SpCl_CreatedUserId.ToString() };
            data[18] = new object[] { "SpCl_CreatedDate", _data.C.SpCl_CreatedDate == null ? "<Null>" : _data.C.SpCl_CreatedDate.ToString() };
            data[19] = new object[] { "SpCl_UserId", _data.C.SpCl_UserId == null ? "<Null>" : _data.C.SpCl_UserId.ToString() };
            data[20] = new object[] { "UserName", _data.C.UserName == null ? "<Null>" : _data.C.UserName.ToString() };
            data[21] = new object[] { "SpCl_LastModifiedDate", _data.C.SpCl_LastModifiedDate == null ? "<Null>" : _data.C.SpCl_LastModifiedDate.ToString() };
            return data;
        }


		#endregion ITraceItem Members


		#region INotifyPropertyChanged Members

        /// <summary>
        /// 	<para>
        ///         This method raises the <see cref="PropertyChanged">PropertyChanged</see> event
        ///         and is required by the INotifyPropertyChanged interface. It’s called in nearly
        ///         all public data field properties.
        ///     </para>
        /// 	<para>The <strong>INotifyPropertyChanged</strong> interface is used to notify
        ///     clients, typically binding clients, that a property value has changed. For more
        ///     information, search INotifyPropertyChanged on <strong>MSDN</strong>.</para>
        /// </summary>
        protected void Notify(string propName)
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }


		#endregion INotifyPropertyChanged Members


		#region INotifyDataErrorInfo Members

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        //public void RaiseErrorsChanged(string propertyName)
        //{
        //    if (ErrorsChanged != null)
        //        ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        //}

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            //throw new NotImplementedException();
            if (_brokenRuleManager.GetBrokenRulesForProperty(propertyName).Count == 0)
            {
                return "";
            }
            else
            {

                return _brokenRuleManager.GetBrokenRulesForProperty(propertyName)[0].ToString();
            }
        }

        public bool HasErrors
        {
            get { return _brokenRuleManager.IsEntityValid(); }
        }


		#endregion INotifyDataErrorInfo Members


        #region List Methods

        #region IEditableObject Members

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="CancelEdit">CancelEdit</see> and <see cref="EndEdit">EndEdit</see>.
        ///     </para>
        /// 	<para>Begins an edit on an object.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void BeginEdit()
        {
//            Guid? traceId = Guid.NewGuid();
//            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BeginEdit", IfxTraceCategory.Enter);
//            Console.WriteLine("BeginEdit");
//            //***  ToDo:
//            // Set the parent type below, then when properties are being edited, raise an event to sync fields in the props control.
//            _parentType = ParentEditObjectType.EntitiyListControl;
//
//            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BeginEdit", IfxTraceCategory.Leave);
        }

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="BeginEdit">BeginEdit</see> and <see cref="EndEdit">EndEdit</see>.
        ///     </para>
        /// 	<para>Discards changes since the last BeginEdit call.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void CancelEdit()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelEdit", IfxTraceCategory.Enter);
            UnDo();
            _parentType = ParentEditObjectType.None;

            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelEdit", IfxTraceCategory.Leave);
        }

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="BeginEdit">BeginEdit</see> and <see cref="CancelEdit">CancelEdit</see>.
        ///     </para>
        /// 	<para>Pushes changes since the last BeginEdit or IBindingList.AddNew call into the
        ///     underlying object.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void EndEdit()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", IfxTraceCategory.Enter);
//                if (_data.S.IsDirty() && _data.S.IsValid())
//                {
//                    // ToDo - This needs more testing for Silverlight
//                    Save(null, ParentEditObjectType.EntitiyListControl, UseConcurrencyCheck.UseDefaultSetting);
//
//                    //DataServiceInsertUpdateResponseClientSide result = Save(UseConcurrencyCheck.UseDefaultSetting);
//                    //if (result.Result != DataOperationResult.Success)
//                    //{
//                    //    UnDo();
//                    //    Debugger.Break();
//                    //}
//                }
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", IfxTraceCategory.Leave);
//            }
        }

        #endregion IEditableObject Members

        #endregion List Methods



//        #region IDataErrorInfo Members
//
//        /// <summary>
//        /// 	<para>Part of the <strong>IDataErrorInfo</strong> Interface. Gets an error message
//        ///     indicating what is wrong with this object.</para>
//        /// 	<para>For more information, search <strong>IDataErrorInfo</strong> on
//        ///     <strong>MSDN</strong>.</para>
//        /// </summary>
//        public string Error
//        {
//            get { throw new NotImplementedException(); }
//        }
//
//        /// <summary>
//        /// 	<para>Part of the <strong>IDataErrorInfo</strong> Interface. Gets the error message
//        ///     for the property with the given name.</para>
//        /// 	<para>For more information, search <strong>IDataErrorInfo</strong> on
//        ///     <strong>MSDN</strong>.</para>
//        /// </summary>
//        string IDataErrorInfo.this[string columnName]
//        {
//            get
//            {
//                if (!_brokenRuleManager.IsPropertyValid(columnName))
//                {
//                    //string err = "line 1 First validation msg" + Environment.NewLine + "line 2 Second validation msg";
//                    //return err;
//                    return _brokenRuleManager.GetBrokenRulesForProperty(columnName)[0].ToString();
//                }
//                else
//                {
//                    return null;
//                }
//            }
//        }
//
//        #endregion



    }

}



