using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/2/2016 3:31:22 PM

namespace EntityBll.SL
{
    public partial class WcTableColumnComboColumn_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_TbCCmbC_HeaderText = 128;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_TbCCmbC_Id_Required = "Id is a required field.";
		private const string BROKENRULE_TbCCmbC_TbC_Id_Required = "TbC_Id is a required field.";
		private const string BROKENRULE_TbCCmbC_Index_Required = "Index is a required field.";
		private string BROKENRULE_TbCCmbC_HeaderText_TextLength = "Header Text has a maximum text length of  '" + STRINGSIZE_TbCCmbC_HeaderText + "'.";
		private const string BROKENRULE_TbCCmbC_HeaderText_Required = "Header Text is a required field.";
		private const string BROKENRULE_TbCCmbC_MinWidth_Required = "MinWidth is a required field.";
		private const string BROKENRULE_TbCCmbC_MinWidth_ZeroNotAllowed = "MinWidth:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_TbCCmbC_MaxWidth_Required = "MaxWidth is a required field.";
		private const string BROKENRULE_TbCCmbC_MaxWidth_ZeroNotAllowed = "MaxWidth:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_TbCCmbC_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_TbCCmbC_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_TbCCmbC_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_Id", BROKENRULE_TbCCmbC_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_TbC_Id", BROKENRULE_TbCCmbC_TbC_Id_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_Index", BROKENRULE_TbCCmbC_Index_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_HeaderText", BROKENRULE_TbCCmbC_HeaderText_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_HeaderText", BROKENRULE_TbCCmbC_HeaderText_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_MinWidth", BROKENRULE_TbCCmbC_MinWidth_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_MinWidth", BROKENRULE_TbCCmbC_MinWidth_ZeroNotAllowed);
				_brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_MaxWidth", BROKENRULE_TbCCmbC_MaxWidth_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_MaxWidth", BROKENRULE_TbCCmbC_MaxWidth_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_IsActiveRow", BROKENRULE_TbCCmbC_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_IsDeleted", BROKENRULE_TbCCmbC_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_Stamp", BROKENRULE_TbCCmbC_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void TbCCmbC_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_Id");
                string newBrokenRules = "";
                
                if (TbCCmbC_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_Id", BROKENRULE_TbCCmbC_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCCmbC_Id", BROKENRULE_TbCCmbC_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCCmbC_Id", _brokenRuleManager.IsPropertyValid("TbCCmbC_Id"), IsPropertyDirty("TbCCmbC_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCCmbC_TbC_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_TbC_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_TbC_Id");
                string newBrokenRules = "";
                
                if (TbCCmbC_TbC_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_TbC_Id", BROKENRULE_TbCCmbC_TbC_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCCmbC_TbC_Id", BROKENRULE_TbCCmbC_TbC_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_TbC_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCCmbC_TbC_Id", _brokenRuleManager.IsPropertyValid("TbCCmbC_TbC_Id"), IsPropertyDirty("TbCCmbC_TbC_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_TbC_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_TbC_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCCmbC_Index_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_Index_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_Index");
                string newBrokenRules = "";
                
                if (TbCCmbC_Index == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_Index", BROKENRULE_TbCCmbC_Index_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCCmbC_Index", BROKENRULE_TbCCmbC_Index_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_Index");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCCmbC_Index", _brokenRuleManager.IsPropertyValid("TbCCmbC_Index"), IsPropertyDirty("TbCCmbC_Index"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_Index_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_Index_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCCmbC_HeaderText_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_HeaderText_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_HeaderText");
                string newBrokenRules = "";
                				int len = 0;
                if (TbCCmbC_HeaderText != null)
                {
                    len = TbCCmbC_HeaderText.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_HeaderText", BROKENRULE_TbCCmbC_HeaderText_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCCmbC_HeaderText", BROKENRULE_TbCCmbC_HeaderText_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCCmbC_HeaderText", BROKENRULE_TbCCmbC_HeaderText_Required);
                    if (len > STRINGSIZE_TbCCmbC_HeaderText)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_HeaderText", BROKENRULE_TbCCmbC_HeaderText_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("TbCCmbC_HeaderText", BROKENRULE_TbCCmbC_HeaderText_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_HeaderText");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCCmbC_HeaderText", _brokenRuleManager.IsPropertyValid("TbCCmbC_HeaderText"), IsPropertyDirty("TbCCmbC_HeaderText"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_HeaderText_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_HeaderText_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCCmbC_MinWidth_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_MinWidth_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_MinWidth");
                string newBrokenRules = "";
                
                if (TbCCmbC_MinWidth == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_MinWidth", BROKENRULE_TbCCmbC_MinWidth_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCCmbC_MinWidth", BROKENRULE_TbCCmbC_MinWidth_Required);
                }

                if (TbCCmbC_MinWidth == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_MinWidth", BROKENRULE_TbCCmbC_MinWidth_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCCmbC_MinWidth", BROKENRULE_TbCCmbC_MinWidth_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_MinWidth");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCCmbC_MinWidth", _brokenRuleManager.IsPropertyValid("TbCCmbC_MinWidth"), IsPropertyDirty("TbCCmbC_MinWidth"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_MinWidth_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_MinWidth_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCCmbC_MaxWidth_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_MaxWidth_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_MaxWidth");
                string newBrokenRules = "";
                
                if (TbCCmbC_MaxWidth == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_MaxWidth", BROKENRULE_TbCCmbC_MaxWidth_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCCmbC_MaxWidth", BROKENRULE_TbCCmbC_MaxWidth_Required);
                }

                if (TbCCmbC_MaxWidth == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_MaxWidth", BROKENRULE_TbCCmbC_MaxWidth_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCCmbC_MaxWidth", BROKENRULE_TbCCmbC_MaxWidth_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_MaxWidth");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCCmbC_MaxWidth", _brokenRuleManager.IsPropertyValid("TbCCmbC_MaxWidth"), IsPropertyDirty("TbCCmbC_MaxWidth"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_MaxWidth_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_MaxWidth_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCCmbC_TextWrap_Validate()
        {
        }

        private void TbCCmbC_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_IsActiveRow");
                string newBrokenRules = "";
                
                if (TbCCmbC_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_IsActiveRow", BROKENRULE_TbCCmbC_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCCmbC_IsActiveRow", BROKENRULE_TbCCmbC_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCCmbC_IsActiveRow", _brokenRuleManager.IsPropertyValid("TbCCmbC_IsActiveRow"), IsPropertyDirty("TbCCmbC_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCCmbC_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_IsDeleted");
                string newBrokenRules = "";
                
                if (TbCCmbC_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_IsDeleted", BROKENRULE_TbCCmbC_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCCmbC_IsDeleted", BROKENRULE_TbCCmbC_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCCmbC_IsDeleted", _brokenRuleManager.IsPropertyValid("TbCCmbC_IsDeleted"), IsPropertyDirty("TbCCmbC_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCCmbC_CreatedUserId_Validate()
        {
        }

        private void TbCCmbC_CreatedDate_Validate()
        {
        }

        private void TbCCmbC_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCCmbC_LastModifiedDate_Validate()
        {
        }

        private void TbCCmbC_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_Stamp");
                string newBrokenRules = "";
                
                if (TbCCmbC_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCCmbC_Stamp", BROKENRULE_TbCCmbC_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCCmbC_Stamp", BROKENRULE_TbCCmbC_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCCmbC_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCCmbC_Stamp", _brokenRuleManager.IsPropertyValid("TbCCmbC_Stamp"), IsPropertyDirty("TbCCmbC_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCCmbC_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


