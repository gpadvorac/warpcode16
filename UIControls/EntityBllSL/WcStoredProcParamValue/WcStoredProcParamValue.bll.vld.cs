using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/26/2017 1:52:34 PM

namespace EntityBll.SL
{
    public partial class WcStoredProcParamValue_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_SpPV_Value = 500;
		public const int STRINGSIZE_SpPV_Notes = 255;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_SpPV_Id_Required = "Id is a required field.";
		private const string BROKENRULE_SpPV_SpPVGrp_Id_Required = "SpPVGrp_Id is a required field.";
		private const string BROKENRULE_SpPV_SpP_Id_Required = "SpP_Id is a required field.";
		private string BROKENRULE_SpPV_Value_TextLength = "Value has a maximum text length of  '" + STRINGSIZE_SpPV_Value + "'.";
		private const string BROKENRULE_SpPV_Value_Required = "Value is a required field.";
		private string BROKENRULE_SpPV_Notes_TextLength = "Notes has a maximum text length of  '" + STRINGSIZE_SpPV_Notes + "'.";
		private const string BROKENRULE_SpPV_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_SpPV_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_SpPV_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPV_Id", BROKENRULE_SpPV_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPV_SpPVGrp_Id", BROKENRULE_SpPV_SpPVGrp_Id_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("SpPV_SpP_Id", BROKENRULE_SpPV_SpP_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPV_Value", BROKENRULE_SpPV_Value_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("SpPV_Value", BROKENRULE_SpPV_Value_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPV_Notes", BROKENRULE_SpPV_Notes_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPV_IsActiveRow", BROKENRULE_SpPV_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPV_IsDeleted", BROKENRULE_SpPV_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("SpPV_Stamp", BROKENRULE_SpPV_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void SpPV_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_Id");
                string newBrokenRules = "";
                
                if (SpPV_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPV_Id", BROKENRULE_SpPV_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPV_Id", BROKENRULE_SpPV_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPV_Id", _brokenRuleManager.IsPropertyValid("SpPV_Id"), IsPropertyDirty("SpPV_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPV_SpPVGrp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_SpPVGrp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_SpPVGrp_Id");
                string newBrokenRules = "";
                
                if (SpPV_SpPVGrp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPV_SpPVGrp_Id", BROKENRULE_SpPV_SpPVGrp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPV_SpPVGrp_Id", BROKENRULE_SpPV_SpPVGrp_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_SpPVGrp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPV_SpPVGrp_Id", _brokenRuleManager.IsPropertyValid("SpPV_SpPVGrp_Id"), IsPropertyDirty("SpPV_SpPVGrp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_SpPVGrp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_SpPVGrp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPV_SpP_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_SpP_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_SpP_Id");
                string newBrokenRules = "";
                
                if (SpPV_SpP_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPV_SpP_Id", BROKENRULE_SpPV_SpP_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPV_SpP_Id", BROKENRULE_SpPV_SpP_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_SpP_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPV_SpP_Id", _brokenRuleManager.IsPropertyValid("SpPV_SpP_Id"), IsPropertyDirty("SpPV_SpP_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_SpP_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_SpP_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPV_SpP_Id_TextField_Validate()
        {
        }

        private void SpPV_Value_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_Value_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_Value");
                string newBrokenRules = "";
                				int len = 0;
                if (SpPV_Value != null)
                {
                    len = SpPV_Value.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPV_Value", BROKENRULE_SpPV_Value_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPV_Value", BROKENRULE_SpPV_Value_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPV_Value", BROKENRULE_SpPV_Value_Required);
                    if (len > STRINGSIZE_SpPV_Value)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("SpPV_Value", BROKENRULE_SpPV_Value_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("SpPV_Value", BROKENRULE_SpPV_Value_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_Value");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPV_Value", _brokenRuleManager.IsPropertyValid("SpPV_Value"), IsPropertyDirty("SpPV_Value"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_Value_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_Value_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPV_Notes_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_Notes_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_Notes");
                string newBrokenRules = "";
                				int len = 0;
                if (SpPV_Notes != null)
                {
                    len = SpPV_Notes.Length;
                }

                if (len > STRINGSIZE_SpPV_Notes)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPV_Notes", BROKENRULE_SpPV_Notes_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPV_Notes", BROKENRULE_SpPV_Notes_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_Notes");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPV_Notes", _brokenRuleManager.IsPropertyValid("SpPV_Notes"), IsPropertyDirty("SpPV_Notes"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_Notes_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_Notes_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPV_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_IsActiveRow");
                string newBrokenRules = "";
                
                if (SpPV_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPV_IsActiveRow", BROKENRULE_SpPV_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPV_IsActiveRow", BROKENRULE_SpPV_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPV_IsActiveRow", _brokenRuleManager.IsPropertyValid("SpPV_IsActiveRow"), IsPropertyDirty("SpPV_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPV_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_IsDeleted");
                string newBrokenRules = "";
                
                if (SpPV_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPV_IsDeleted", BROKENRULE_SpPV_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPV_IsDeleted", BROKENRULE_SpPV_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPV_IsDeleted", _brokenRuleManager.IsPropertyValid("SpPV_IsDeleted"), IsPropertyDirty("SpPV_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPV_CreatedUserId_Validate()
        {
        }

        private void SpPV_CreatedDate_Validate()
        {
        }

        private void SpPV_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SpPV_LastModifiedDate_Validate()
        {
        }

        private void SpPV_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_Stamp");
                string newBrokenRules = "";
                
                if (SpPV_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SpPV_Stamp", BROKENRULE_SpPV_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SpPV_Stamp", BROKENRULE_SpPV_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SpPV_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SpPV_Stamp", _brokenRuleManager.IsPropertyValid("SpPV_Stamp"), IsPropertyDirty("SpPV_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SpPV_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


