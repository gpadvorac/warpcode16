using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/27/2017 6:41:49 PM

namespace EntityBll.SL
{
    public partial class WcPropsTile_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_PrpTl_GridName = 50;
		public const int STRINGSIZE_PrpTl_Header = 75;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_PrpTl_Id_Required = "Id is a required field.";
		private const string BROKENRULE_PrpTl_Sort_ZeroNotAllowed = "Sort:  '0'  (zero) is not allowed.";
		private string BROKENRULE_PrpTl_GridName_TextLength = "Grid Name has a maximum text length of  '" + STRINGSIZE_PrpTl_GridName + "'.";
		private const string BROKENRULE_PrpTl_GridName_Required = "Grid Name is a required field.";
		private string BROKENRULE_PrpTl_Header_TextLength = "Tile Caption Text has a maximum text length of  '" + STRINGSIZE_PrpTl_Header + "'.";
		private const string BROKENRULE_PrpTl_Header_Required = "Tile Caption Text is a required field.";
		private const string BROKENRULE_PrpTl_IsDefaultView_Required = "Is Default View is a required field.";
		private const string BROKENRULE_PrpTl_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_PrpTl_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_PrpTl_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("PrpTl_Id", BROKENRULE_PrpTl_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("PrpTl_Tb_Id", BROKENRULE_PrpTl_Tb_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("PrpTl_Sort", BROKENRULE_PrpTl_Sort_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("PrpTl_GridName", BROKENRULE_PrpTl_GridName_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("PrpTl_GridName", BROKENRULE_PrpTl_GridName_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("PrpTl_Header", BROKENRULE_PrpTl_Header_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("PrpTl_Header", BROKENRULE_PrpTl_Header_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("PrpTl_IsDefaultView", BROKENRULE_PrpTl_IsDefaultView_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("PrpTl_IsActiveRow", BROKENRULE_PrpTl_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("PrpTl_IsDeleted", BROKENRULE_PrpTl_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("PrpTl_Stamp", BROKENRULE_PrpTl_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void PrpTl_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_Id");
                string newBrokenRules = "";
                
                if (PrpTl_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("PrpTl_Id", BROKENRULE_PrpTl_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("PrpTl_Id", BROKENRULE_PrpTl_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("PrpTl_Id", _brokenRuleManager.IsPropertyValid("PrpTl_Id"), IsPropertyDirty("PrpTl_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void PrpTl_Tb_Id_Validate()
        {
        }

        private void PrpTl_Sort_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_Sort_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_Sort");
                string newBrokenRules = "";
                
                if (PrpTl_Sort == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("PrpTl_Sort", BROKENRULE_PrpTl_Sort_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("PrpTl_Sort", BROKENRULE_PrpTl_Sort_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_Sort");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("PrpTl_Sort", _brokenRuleManager.IsPropertyValid("PrpTl_Sort"), IsPropertyDirty("PrpTl_Sort"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_Sort_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_Sort_Validate", IfxTraceCategory.Leave);
            }
        }

        private void PrpTl_GridName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_GridName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_GridName");
                string newBrokenRules = "";
                				int len = 0;
                if (PrpTl_GridName != null)
                {
                    len = PrpTl_GridName.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("PrpTl_GridName", BROKENRULE_PrpTl_GridName_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("PrpTl_GridName", BROKENRULE_PrpTl_GridName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("PrpTl_GridName", BROKENRULE_PrpTl_GridName_Required);
                    if (len > STRINGSIZE_PrpTl_GridName)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("PrpTl_GridName", BROKENRULE_PrpTl_GridName_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("PrpTl_GridName", BROKENRULE_PrpTl_GridName_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_GridName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("PrpTl_GridName", _brokenRuleManager.IsPropertyValid("PrpTl_GridName"), IsPropertyDirty("PrpTl_GridName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_GridName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_GridName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void PrpTl_Header_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_Header_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_Header");
                string newBrokenRules = "";
                				int len = 0;
                if (PrpTl_Header != null)
                {
                    len = PrpTl_Header.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("PrpTl_Header", BROKENRULE_PrpTl_Header_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("PrpTl_Header", BROKENRULE_PrpTl_Header_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("PrpTl_Header", BROKENRULE_PrpTl_Header_Required);
                    if (len > STRINGSIZE_PrpTl_Header)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("PrpTl_Header", BROKENRULE_PrpTl_Header_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("PrpTl_Header", BROKENRULE_PrpTl_Header_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_Header");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("PrpTl_Header", _brokenRuleManager.IsPropertyValid("PrpTl_Header"), IsPropertyDirty("PrpTl_Header"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_Header_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_Header_Validate", IfxTraceCategory.Leave);
            }
        }

        private void PrpTl_IsDefaultView_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_IsDefaultView_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_IsDefaultView");
                string newBrokenRules = "";
                
                if (PrpTl_IsDefaultView == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("PrpTl_IsDefaultView", BROKENRULE_PrpTl_IsDefaultView_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("PrpTl_IsDefaultView", BROKENRULE_PrpTl_IsDefaultView_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_IsDefaultView");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("PrpTl_IsDefaultView", _brokenRuleManager.IsPropertyValid("PrpTl_IsDefaultView"), IsPropertyDirty("PrpTl_IsDefaultView"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_IsDefaultView_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_IsDefaultView_Validate", IfxTraceCategory.Leave);
            }
        }

        private void PrpTl_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_IsActiveRow");
                string newBrokenRules = "";
                
                if (PrpTl_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("PrpTl_IsActiveRow", BROKENRULE_PrpTl_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("PrpTl_IsActiveRow", BROKENRULE_PrpTl_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("PrpTl_IsActiveRow", _brokenRuleManager.IsPropertyValid("PrpTl_IsActiveRow"), IsPropertyDirty("PrpTl_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void PrpTl_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_IsDeleted");
                string newBrokenRules = "";
                
                if (PrpTl_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("PrpTl_IsDeleted", BROKENRULE_PrpTl_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("PrpTl_IsDeleted", BROKENRULE_PrpTl_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("PrpTl_IsDeleted", _brokenRuleManager.IsPropertyValid("PrpTl_IsDeleted"), IsPropertyDirty("PrpTl_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void PrpTl_CreatedUserId_Validate()
        {
        }

        private void PrpTl_CreatedDate_Validate()
        {
        }

        private void PrpTl_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void PrpTl_LastModifiedDate_Validate()
        {
        }

        private void PrpTl_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_Stamp");
                string newBrokenRules = "";
                
                if (PrpTl_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("PrpTl_Stamp", BROKENRULE_PrpTl_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("PrpTl_Stamp", BROKENRULE_PrpTl_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PrpTl_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("PrpTl_Stamp", _brokenRuleManager.IsPropertyValid("PrpTl_Stamp"), IsPropertyDirty("PrpTl_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PrpTl_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


