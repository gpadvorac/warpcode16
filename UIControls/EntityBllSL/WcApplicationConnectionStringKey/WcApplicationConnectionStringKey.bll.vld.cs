using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  11/17/2016 3:17:35 PM

namespace EntityBll.SL
{
    public partial class WcApplicationConnectionStringKey_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_ApCnSK_Name = 100;
		public const int STRINGSIZE_ApCnSK_Server = 50;
		public const int STRINGSIZE_ApCnSK_Database = 50;
		public const int STRINGSIZE_ApCnSK_UserName = 50;
		public const int STRINGSIZE_ApCnSK_Password = 50;
		public const int STRINGSIZE_UserName = 60;
		private string BROKENRULE_ApCnSK_Name_TextLength = "Name has a maximum text length of  '" + STRINGSIZE_ApCnSK_Name + "'.";
		private const string BROKENRULE_ApCnSK_Name_Required = "Name is a required field.";
		private const string BROKENRULE_ApCnSK_IsDefault_Required = "Is Default is a required field.";
		private string BROKENRULE_ApCnSK_Server_TextLength = "Server has a maximum text length of  '" + STRINGSIZE_ApCnSK_Server + "'.";
		private const string BROKENRULE_ApCnSK_Server_Required = "Server is a required field.";
		private string BROKENRULE_ApCnSK_Database_TextLength = "Database has a maximum text length of  '" + STRINGSIZE_ApCnSK_Database + "'.";
		private const string BROKENRULE_ApCnSK_Database_Required = "Database is a required field.";
		private string BROKENRULE_ApCnSK_UserName_TextLength = "User Name has a maximum text length of  '" + STRINGSIZE_ApCnSK_UserName + "'.";
		private const string BROKENRULE_ApCnSK_UserName_Required = "User Name is a required field.";
		private string BROKENRULE_ApCnSK_Password_TextLength = "Password has a maximum text length of  '" + STRINGSIZE_ApCnSK_Password + "'.";
		private const string BROKENRULE_ApCnSK_Password_Required = "Password is a required field.";
		private const string BROKENRULE_ApCnSK_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_ApCnSK_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_ApCnSK_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Id", BROKENRULE_ApCnSK_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Ap_Id", BROKENRULE_ApCnSK_Ap_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Name", BROKENRULE_ApCnSK_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Name", BROKENRULE_ApCnSK_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_IsDefault", BROKENRULE_ApCnSK_IsDefault_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Server", BROKENRULE_ApCnSK_Server_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Server", BROKENRULE_ApCnSK_Server_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Database", BROKENRULE_ApCnSK_Database_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Database", BROKENRULE_ApCnSK_Database_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_UserName", BROKENRULE_ApCnSK_UserName_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_UserName", BROKENRULE_ApCnSK_UserName_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Password", BROKENRULE_ApCnSK_Password_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Password", BROKENRULE_ApCnSK_Password_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_IsActiveRow", BROKENRULE_ApCnSK_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_IsDeleted", BROKENRULE_ApCnSK_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Stamp", BROKENRULE_ApCnSK_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void ApCnSK_Id_Validate()
        {
        }

        private void ApCnSK_Ap_Id_Validate()
        {
        }

        private void ApCnSK_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (ApCnSK_Name != null)
                {
                    len = ApCnSK_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Name", BROKENRULE_ApCnSK_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_Name", BROKENRULE_ApCnSK_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_Name", BROKENRULE_ApCnSK_Name_Required);
                    if (len > STRINGSIZE_ApCnSK_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Name", BROKENRULE_ApCnSK_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_Name", BROKENRULE_ApCnSK_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApCnSK_Name", _brokenRuleManager.IsPropertyValid("ApCnSK_Name"), IsPropertyDirty("ApCnSK_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApCnSK_IsDefault_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_IsDefault_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_IsDefault");
                string newBrokenRules = "";
                
                if (ApCnSK_IsDefault == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_IsDefault", BROKENRULE_ApCnSK_IsDefault_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_IsDefault", BROKENRULE_ApCnSK_IsDefault_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_IsDefault");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApCnSK_IsDefault", _brokenRuleManager.IsPropertyValid("ApCnSK_IsDefault"), IsPropertyDirty("ApCnSK_IsDefault"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_IsDefault_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_IsDefault_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApCnSK_Server_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Server_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_Server");
                string newBrokenRules = "";
                				int len = 0;
                if (ApCnSK_Server != null)
                {
                    len = ApCnSK_Server.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Server", BROKENRULE_ApCnSK_Server_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_Server", BROKENRULE_ApCnSK_Server_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_Server", BROKENRULE_ApCnSK_Server_Required);
                    if (len > STRINGSIZE_ApCnSK_Server)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Server", BROKENRULE_ApCnSK_Server_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_Server", BROKENRULE_ApCnSK_Server_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_Server");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApCnSK_Server", _brokenRuleManager.IsPropertyValid("ApCnSK_Server"), IsPropertyDirty("ApCnSK_Server"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Server_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Server_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApCnSK_Database_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Database_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_Database");
                string newBrokenRules = "";
                				int len = 0;
                if (ApCnSK_Database != null)
                {
                    len = ApCnSK_Database.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Database", BROKENRULE_ApCnSK_Database_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_Database", BROKENRULE_ApCnSK_Database_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_Database", BROKENRULE_ApCnSK_Database_Required);
                    if (len > STRINGSIZE_ApCnSK_Database)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Database", BROKENRULE_ApCnSK_Database_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_Database", BROKENRULE_ApCnSK_Database_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_Database");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApCnSK_Database", _brokenRuleManager.IsPropertyValid("ApCnSK_Database"), IsPropertyDirty("ApCnSK_Database"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Database_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Database_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApCnSK_UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (ApCnSK_UserName != null)
                {
                    len = ApCnSK_UserName.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_UserName", BROKENRULE_ApCnSK_UserName_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_UserName", BROKENRULE_ApCnSK_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_UserName", BROKENRULE_ApCnSK_UserName_Required);
                    if (len > STRINGSIZE_ApCnSK_UserName)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_UserName", BROKENRULE_ApCnSK_UserName_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_UserName", BROKENRULE_ApCnSK_UserName_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApCnSK_UserName", _brokenRuleManager.IsPropertyValid("ApCnSK_UserName"), IsPropertyDirty("ApCnSK_UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApCnSK_Password_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Password_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_Password");
                string newBrokenRules = "";
                				int len = 0;
                if (ApCnSK_Password != null)
                {
                    len = ApCnSK_Password.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Password", BROKENRULE_ApCnSK_Password_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_Password", BROKENRULE_ApCnSK_Password_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_Password", BROKENRULE_ApCnSK_Password_Required);
                    if (len > STRINGSIZE_ApCnSK_Password)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Password", BROKENRULE_ApCnSK_Password_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_Password", BROKENRULE_ApCnSK_Password_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_Password");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApCnSK_Password", _brokenRuleManager.IsPropertyValid("ApCnSK_Password"), IsPropertyDirty("ApCnSK_Password"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Password_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Password_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApCnSK_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_IsActiveRow");
                string newBrokenRules = "";
                
                if (ApCnSK_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_IsActiveRow", BROKENRULE_ApCnSK_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_IsActiveRow", BROKENRULE_ApCnSK_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApCnSK_IsActiveRow", _brokenRuleManager.IsPropertyValid("ApCnSK_IsActiveRow"), IsPropertyDirty("ApCnSK_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApCnSK_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_IsDeleted");
                string newBrokenRules = "";
                
                if (ApCnSK_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_IsDeleted", BROKENRULE_ApCnSK_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_IsDeleted", BROKENRULE_ApCnSK_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApCnSK_IsDeleted", _brokenRuleManager.IsPropertyValid("ApCnSK_IsDeleted"), IsPropertyDirty("ApCnSK_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApCnSK_CreatedUserId_Validate()
        {
        }

        private void ApCnSK_CreatedDate_Validate()
        {
        }

        private void ApCnSK_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApCnSK_LastModifiedDate_Validate()
        {
        }

        private void ApCnSK_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_Stamp");
                string newBrokenRules = "";
                
                if (ApCnSK_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApCnSK_Stamp", BROKENRULE_ApCnSK_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApCnSK_Stamp", BROKENRULE_ApCnSK_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApCnSK_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApCnSK_Stamp", _brokenRuleManager.IsPropertyValid("ApCnSK_Stamp"), IsPropertyDirty("ApCnSK_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApCnSK_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


