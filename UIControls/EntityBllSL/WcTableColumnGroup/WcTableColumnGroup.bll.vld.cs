using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/15/2017 5:08:05 PM

namespace EntityBll.SL
{
    public partial class WcTableColumnGroup_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_TbCGrp_Name = 50;
		public const int STRINGSIZE_TbCGrp_Desc = 200;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_TbCGrp_Id_Required = "Id is a required field.";
		private const string BROKENRULE_TbCGrp_SortOrder_ZeroNotAllowed = "Sort Order:  '0'  (zero) is not allowed.";
		private string BROKENRULE_TbCGrp_Name_TextLength = "Column Group Name has a maximum text length of  '" + STRINGSIZE_TbCGrp_Name + "'.";
		private string BROKENRULE_TbCGrp_Desc_TextLength = "Description has a maximum text length of  '" + STRINGSIZE_TbCGrp_Desc + "'.";
		private const string BROKENRULE_TbCGrp_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_TbCGrp_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_TbCGrp_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_Id", BROKENRULE_TbCGrp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_ApVrsn_Id", BROKENRULE_TbCGrp_ApVrsn_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_SortOrder", BROKENRULE_TbCGrp_SortOrder_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_Name", BROKENRULE_TbCGrp_Name_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_Desc", BROKENRULE_TbCGrp_Desc_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_IsActiveRow", BROKENRULE_TbCGrp_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_IsDeleted", BROKENRULE_TbCGrp_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_Stamp", BROKENRULE_TbCGrp_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void TbCGrp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_Id");
                string newBrokenRules = "";
                
                if (TbCGrp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_Id", BROKENRULE_TbCGrp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCGrp_Id", BROKENRULE_TbCGrp_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCGrp_Id", _brokenRuleManager.IsPropertyValid("TbCGrp_Id"), IsPropertyDirty("TbCGrp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCGrp_ApVrsn_Id_Validate()
        {
        }

        private void TbCGrp_Tb_Id_Validate()
        {
        }

        private void TbCGrp_SortOrder_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_SortOrder_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_SortOrder");
                string newBrokenRules = "";
                
                if (TbCGrp_SortOrder == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_SortOrder", BROKENRULE_TbCGrp_SortOrder_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCGrp_SortOrder", BROKENRULE_TbCGrp_SortOrder_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_SortOrder");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCGrp_SortOrder", _brokenRuleManager.IsPropertyValid("TbCGrp_SortOrder"), IsPropertyDirty("TbCGrp_SortOrder"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_SortOrder_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_SortOrder_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCGrp_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (TbCGrp_Name != null)
                {
                    len = TbCGrp_Name.Length;
                }

                if (len > STRINGSIZE_TbCGrp_Name)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_Name", BROKENRULE_TbCGrp_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCGrp_Name", BROKENRULE_TbCGrp_Name_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCGrp_Name", _brokenRuleManager.IsPropertyValid("TbCGrp_Name"), IsPropertyDirty("TbCGrp_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCGrp_Desc_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_Desc_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_Desc");
                string newBrokenRules = "";
                				int len = 0;
                if (TbCGrp_Desc != null)
                {
                    len = TbCGrp_Desc.Length;
                }

                if (len > STRINGSIZE_TbCGrp_Desc)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_Desc", BROKENRULE_TbCGrp_Desc_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCGrp_Desc", BROKENRULE_TbCGrp_Desc_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_Desc");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCGrp_Desc", _brokenRuleManager.IsPropertyValid("TbCGrp_Desc"), IsPropertyDirty("TbCGrp_Desc"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_Desc_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_Desc_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCGrp_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_IsActiveRow");
                string newBrokenRules = "";
                
                if (TbCGrp_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_IsActiveRow", BROKENRULE_TbCGrp_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCGrp_IsActiveRow", BROKENRULE_TbCGrp_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCGrp_IsActiveRow", _brokenRuleManager.IsPropertyValid("TbCGrp_IsActiveRow"), IsPropertyDirty("TbCGrp_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCGrp_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_IsDeleted");
                string newBrokenRules = "";
                
                if (TbCGrp_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_IsDeleted", BROKENRULE_TbCGrp_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCGrp_IsDeleted", BROKENRULE_TbCGrp_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCGrp_IsDeleted", _brokenRuleManager.IsPropertyValid("TbCGrp_IsDeleted"), IsPropertyDirty("TbCGrp_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCGrp_CreatedUserId_Validate()
        {
        }

        private void TbCGrp_CreatedDate_Validate()
        {
        }

        private void TbCGrp_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbCGrp_LastModifiedDate_Validate()
        {
        }

        private void TbCGrp_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_Stamp");
                string newBrokenRules = "";
                
                if (TbCGrp_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbCGrp_Stamp", BROKENRULE_TbCGrp_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbCGrp_Stamp", BROKENRULE_TbCGrp_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbCGrp_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbCGrp_Stamp", _brokenRuleManager.IsPropertyValid("TbCGrp_Stamp"), IsPropertyDirty("TbCGrp_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbCGrp_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


