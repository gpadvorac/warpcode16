using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections;
using TypeServices;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/8/2018 10:54:55 AM

namespace EntityBll.SL
{
    public partial class WcStoredProc_List : ObservableCollection<WcStoredProc_Bll>, IBusinessObject_List
    {

        #region Initialize Variables

        int _lastAddedIndex = -1;
        #endregion Initialize Variables


        #region Constructors

        public WcStoredProc_List()
        {
        }
  
        //public WcStoredProc_List(bool isPartialLoad)
        //{
        //    _isPartialLoad = isPartialLoad;
        //}


        public WcStoredProc_List(WcStoredProc_ValuesMngr[] list)
        {
            Fill(list);
        }

        #endregion Constructors


        #region Helper Methods

        public void Fill(IBusinessObject[] list)
        {
            //  This assumes that each Category_Lu_ValuesMngr has been fetched from a db or some other reliable source which gives us the following state
            //  Hack, Hack;  we have to add the state here becuase of how its handled (or not handled) in Category_Lu_ValuesMngr due to the problem of loosing this value when passed accross the wire
            EntityState state = new EntityState(false, true, false);
            foreach (WcStoredProc_ValuesMngr obj in list)
            {
                base.Add(new WcStoredProc_Bll(obj, state));
            }
        }

        public void Fill(WcStoredProc_ValuesMngr[] list)
        {
            //  This assumes that each WcStoredProc_ValuesMngr has been fetched from a db or some other reliable source which gives us the following state
            //  Hack, Hack;  we have to add the state here becuase of how its handled (or not handled) in WcStoredProc_ValuesMngr due to the problem of loosing this value when passed accross the wire
            EntityState state = new EntityState(false, true, false);
            foreach (WcStoredProc_ValuesMngr obj in list)
            {
                base.Add(new WcStoredProc_Bll(obj, state));
            }
        }

        public void ReplaceList(WcStoredProc_ValuesMngr[] list)
        {
            base.Clear();
            //  This assumes that each WcStoredProc_ValuesMngr has been fetched from a db or some other reliable source which gives us the following state
            //  Hack, Hack;  we have to add the state here becuase of how its handled (or not handled) in WcStoredProc_ValuesMngr due to the problem of loosing this value when passed accross the wire
            EntityState state = new EntityState(false, true, false);
            foreach (WcStoredProc_ValuesMngr obj in list)
            {
                base.Add(new WcStoredProc_Bll(obj, state));
            }
        }

        public void ReplaceList(IEntity_ValuesMngr[] list)
        {
            base.Clear();
            //  This assumes that each WcStoredProc_ValuesMngr has been fetched from a db or some other reliable source which gives us the following state
            //  Hack, Hack;  we have to add the state here becuase of how its handled (or not handled) in WcStoredProc_ValuesMngr due to the problem of loosing this value when passed accross the wire
            EntityState state = new EntityState(false, true, false);

            foreach (WcStoredProc_ValuesMngr obj in list)
            {
                base.Add(new WcStoredProc_Bll(obj, state));
            }
        }

        public void ReplaceList(List<WcStoredProc_ValuesMngr> list)
        {
            base.Clear();
            //  This assumes that each WcStoredProc_ValuesMngr has been fetched from a db or some other reliable source which gives us the following state
            //  Hack, Hack;  we have to add the state here becuase of how its handled (or not handled) in WcStoredProc_ValuesMngr due to the problem of loosing this value when passed accross the wire
            EntityState state = new EntityState(false, true, false);
            foreach (WcStoredProc_ValuesMngr obj in list)
            {
                base.Add(new WcStoredProc_Bll(obj, state));
            }
        }


        public void ReplaceList(object[]list)
        {
            base.Clear();
            EntityState state = new EntityState(false, true, false);
            for (int i = 0; i < list.GetUpperBound(0) + 1; i++)
            {
                base.Add(new WcStoredProc_Bll(new WcStoredProc_ValuesMngr((object[])list[i], state), state));
            }
        }

        #endregion Helper Methods


        #region List Methods

//        public override void CancelNew(int itemIndex)
//        {
//            base.CancelNew(itemIndex);
//            base.RemoveItem(itemIndex);
//        }

        #endregion List Methods


        #region Properties

        //public bool IsPartialLoad
        //{
        //    get { return _isPartialLoad; }
        //    set { _isPartialLoad = value; }
        //}

        #endregion Properties

        #region BindingList Members

        #region IBindingList Members

        public void AddIndex(PropertyDescriptor property)
        {
            throw new NotImplementedException();
        }

        public IBusinessObject AddNewEntity()
        {
            WcStoredProc_Bll obj = new WcStoredProc_Bll();
            this.Add(obj);
            return obj;
        }
        public WcStoredProc_Bll AddNew()
        {
            WcStoredProc_Bll obj = new WcStoredProc_Bll();
            this.Add(obj);
            return obj;
        }

//        public bool AllowEdit
//        {
//            get
//            {
//                return base.AllowEdit;
//            }
//        }
//
//        public bool AllowNew
//        {
//            get
//            {
//                return base.AllowNew;
//            }
//        }
//
//        public bool AllowRemove
//        {
//            get
//            {
//                return base.AllowRemove;
//            }
//        }

        public void ApplySort(PropertyDescriptor property, ListSortDirection direction)
        {
            throw new NotImplementedException();
        }

        public int Find(PropertyDescriptor property, object key)
        {
            throw new NotImplementedException();
        }

//        public bool IsSorted
//        {
//            get
//            {
//                return base.IsSortedCore;
//            }
//        }

//        public event ListChangedEventHandler ListChanged;

        public void RemoveIndex(PropertyDescriptor property)
        {
            throw new NotImplementedException();
        }

//        public void RemoveSort()
//        {
//            throw new NotImplementedException();
//        }
//
//        public ListSortDirection SortDirection
//        {
//            get
//            {
//                return base.SortDirectionCore;
//            }
//        }
//
//        public PropertyDescriptor SortProperty
//        {
//            get
//            {
//                return base.SortPropertyCore;
//            }
//        }
//
//        public bool SupportsChangeNotification
//        {
//            get
//            {
//                return base.SupportsChangeNotificationCore;
//            }
//        }
//
//        public bool SupportsSearching
//        {
//            get
//            {
//                return base.SupportsSearchingCore;
//            }
//        }
//
//        public bool SupportsSorting
//        {
//            get
//            {
//                return base.SupportsSortingCore;
//            }
//        }

        #endregion IBindingList Members

        #region IList Members

        public int Add(IBusinessObject obj)
        {
            base.Add((WcStoredProc_Bll)obj);
            return base.Count - 1;
        }

        public int Add(WcStoredProc_Bll obj)
        {
            base.Add(obj);
            return base.Count - 1;
        }

        public void Clear()
        {
            base.Clear();
        }

        public bool Contains(IBusinessObject item)
        {
            return base.Contains((WcStoredProc_Bll)item);
        }

        public bool Contains(WcStoredProc_Bll item)
        {
            return base.Contains(item);
        }

        public int IndexOf(object value)
        {
            int itemIndex = -1;
            for (int i = 0; i < Count; i++)
            {
                if (base[i] == value)
                {
                    itemIndex = i;
                    break;
                }
            }
            return itemIndex;
        }

        public void Insert(int index, IBusinessObject item)
        {
            base.Insert(index, (WcStoredProc_Bll)item);
        }

        public void Insert(int index, WcStoredProc_Bll item)
        {
            base.Insert(index, item);
        }

        public bool IsFixedSize
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public void Remove(IBusinessObject item)
        {
            base.Remove((WcStoredProc_Bll)item);
        }

        public void Remove(WcStoredProc_Bll item)
        {
            base.Remove(item);
        }

        public void RemoveAt(int index)
        {
            base.RemoveAt(index);
        }

        public WcStoredProc_Bll this[int index]
        {
            get
            {
                return base[index];
            }
            set
            {
                base[index] = value;
            }
        }

        #endregion IList Members


        #region ICollection Members

        public void CopyTo(IBusinessObject[] array, int index)
        {
            base.CopyTo((WcStoredProc_Bll[])array, index);
        }

        public void CopyTo(WcStoredProc_Bll[] array, int index)
        {
            base.CopyTo(array, index);
        }

        public int Count
        {
            get
            {
                return base.Count;
            }
        }

        public bool IsSynchronized
        {
            get { throw new NotImplementedException(); }
        }

        public object SyncRoot
        {
            get { throw new NotImplementedException(); }
        }

        #endregion ICollection Members


        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return base.GetEnumerator();
        }

        #endregion IEnumerable Members


        #endregion BindingList Members

    }
}


