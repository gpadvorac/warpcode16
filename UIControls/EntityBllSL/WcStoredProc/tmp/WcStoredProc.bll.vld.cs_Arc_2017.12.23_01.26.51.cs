using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/1/2016 11:56:50 AM

namespace EntityBll.SL
{
    public partial class WcStoredProc_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_Sp_Name = 75;
		public const int STRINGSIZE_Sp_TypeName = 100;
		public const int STRINGSIZE_Sp_MethodName = 75;
		public const int STRINGSIZE_Sp_Notes = 255;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_Sp_Id_Required = "Id is a required field.";
		private const string BROKENRULE_Sp_ApVrsn_Id_Required = "ApVrsn_Id is a required field.";
		private string BROKENRULE_Sp_Name_TextLength = "Name has a maximum text length of  '" + STRINGSIZE_Sp_Name + "'.";
		private const string BROKENRULE_Sp_Name_Required = "Name is a required field.";
		private string BROKENRULE_Sp_TypeName_TextLength = "Type Name has a maximum text length of  '" + STRINGSIZE_Sp_TypeName + "'.";
		private string BROKENRULE_Sp_MethodName_TextLength = "Method Name has a maximum text length of  '" + STRINGSIZE_Sp_MethodName + "'.";
		private const string BROKENRULE_Sp_IsBuildDataAccessMethod_Required = "Build Data Access Method is a required field.";
		private const string BROKENRULE_Sp_IsFetchEntity_Required = "Is Fetch Entity is a required field.";
		private const string BROKENRULE_Sp_IsStaticList_Required = "Is Static List is a required field.";
		private const string BROKENRULE_Sp_SpRsTp_Id_ZeroNotAllowed = "Result Type:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_Sp_SpRtTp_Id_ZeroNotAllowed = "Return Type:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_Sp_IsInputParamsAsObjectArray_Required = "Is Input Params As Object Array is a required field.";
		private const string BROKENRULE_Sp_HasNewParams_Required = "Has New Params is a required field.";
		private const string BROKENRULE_Sp_IsParamsValid_Required = "Is Params Valid is a required field.";
		private const string BROKENRULE_Sp_IsParamValueSetValid_Required = "Is Param Value Set Valid is a required field.";
		private const string BROKENRULE_Sp_HasNewColumns_Required = "Has New Columns is a required field.";
		private const string BROKENRULE_Sp_IsColumnsValid_Required = "Is Columns Valid is a required field.";
		private const string BROKENRULE_Sp_IsValid_Required = "Is Valid is a required field.";
		private string BROKENRULE_Sp_Notes_TextLength = "Notes has a maximum text length of  '" + STRINGSIZE_Sp_Notes + "'.";
		private const string BROKENRULE_Sp_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_Sp_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_Sp_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_Id", BROKENRULE_Sp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_ApVrsn_Id", BROKENRULE_Sp_ApVrsn_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_TypeName", BROKENRULE_Sp_TypeName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_MethodName", BROKENRULE_Sp_MethodName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsBuildDataAccessMethod", BROKENRULE_Sp_IsBuildDataAccessMethod_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsFetchEntity", BROKENRULE_Sp_IsFetchEntity_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsStaticList", BROKENRULE_Sp_IsStaticList_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_SpRsTp_Id", BROKENRULE_Sp_SpRsTp_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_SpRtTp_Id", BROKENRULE_Sp_SpRtTp_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsInputParamsAsObjectArray", BROKENRULE_Sp_IsInputParamsAsObjectArray_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_HasNewParams", BROKENRULE_Sp_HasNewParams_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsParamsValid", BROKENRULE_Sp_IsParamsValid_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsParamValueSetValid", BROKENRULE_Sp_IsParamValueSetValid_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_HasNewColumns", BROKENRULE_Sp_HasNewColumns_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsColumnsValid", BROKENRULE_Sp_IsColumnsValid_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsValid", BROKENRULE_Sp_IsValid_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_Notes", BROKENRULE_Sp_Notes_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsActiveRow", BROKENRULE_Sp_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsDeleted", BROKENRULE_Sp_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_Stamp", BROKENRULE_Sp_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void Sp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Id");
                string newBrokenRules = "";
                
                if (Sp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_Id", BROKENRULE_Sp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_Id", BROKENRULE_Sp_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_Id", _brokenRuleManager.IsPropertyValid("Sp_Id"), IsPropertyDirty("Sp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_ApVrsn_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_ApVrsn_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_ApVrsn_Id");
                string newBrokenRules = "";
                
                if (Sp_ApVrsn_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_ApVrsn_Id", BROKENRULE_Sp_ApVrsn_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_ApVrsn_Id", BROKENRULE_Sp_ApVrsn_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_ApVrsn_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_ApVrsn_Id", _brokenRuleManager.IsPropertyValid("Sp_ApVrsn_Id"), IsPropertyDirty("Sp_ApVrsn_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_ApVrsn_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_ApVrsn_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (Sp_Name != null)
                {
                    len = Sp_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_Required);
                    if (len > STRINGSIZE_Sp_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_Name", _brokenRuleManager.IsPropertyValid("Sp_Name"), IsPropertyDirty("Sp_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_TypeName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_TypeName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_TypeName");
                string newBrokenRules = "";
                				int len = 0;
                if (Sp_TypeName != null)
                {
                    len = Sp_TypeName.Length;
                }

                if (len > STRINGSIZE_Sp_TypeName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_TypeName", BROKENRULE_Sp_TypeName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_TypeName", BROKENRULE_Sp_TypeName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_TypeName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_TypeName", _brokenRuleManager.IsPropertyValid("Sp_TypeName"), IsPropertyDirty("Sp_TypeName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_TypeName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_TypeName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_MethodName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_MethodName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_MethodName");
                string newBrokenRules = "";
                				int len = 0;
                if (Sp_MethodName != null)
                {
                    len = Sp_MethodName.Length;
                }

                if (len > STRINGSIZE_Sp_MethodName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_MethodName", BROKENRULE_Sp_MethodName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_MethodName", BROKENRULE_Sp_MethodName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_MethodName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_MethodName", _brokenRuleManager.IsPropertyValid("Sp_MethodName"), IsPropertyDirty("Sp_MethodName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_MethodName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_MethodName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsBuildDataAccessMethod_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsBuildDataAccessMethod_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsBuildDataAccessMethod");
                string newBrokenRules = "";
                
                if (Sp_IsBuildDataAccessMethod == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsBuildDataAccessMethod", BROKENRULE_Sp_IsBuildDataAccessMethod_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsBuildDataAccessMethod", BROKENRULE_Sp_IsBuildDataAccessMethod_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsBuildDataAccessMethod");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsBuildDataAccessMethod", _brokenRuleManager.IsPropertyValid("Sp_IsBuildDataAccessMethod"), IsPropertyDirty("Sp_IsBuildDataAccessMethod"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsBuildDataAccessMethod_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsBuildDataAccessMethod_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_AssocEntity_Id_Validate()
        {
        }

        private void Sp_AssocEntity_Id_TextField_Validate()
        {
        }

        private void Sp_IsFetchEntity_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsFetchEntity_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsFetchEntity");
                string newBrokenRules = "";
                
                if (Sp_IsFetchEntity == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsFetchEntity", BROKENRULE_Sp_IsFetchEntity_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsFetchEntity", BROKENRULE_Sp_IsFetchEntity_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsFetchEntity");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsFetchEntity", _brokenRuleManager.IsPropertyValid("Sp_IsFetchEntity"), IsPropertyDirty("Sp_IsFetchEntity"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsFetchEntity_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsFetchEntity_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsTypeComboItem_Validate()
        {
        }

        private void Sp_IsStaticList_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsStaticList_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsStaticList");
                string newBrokenRules = "";
                
                if (Sp_IsStaticList == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsStaticList", BROKENRULE_Sp_IsStaticList_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsStaticList", BROKENRULE_Sp_IsStaticList_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsStaticList");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsStaticList", _brokenRuleManager.IsPropertyValid("Sp_IsStaticList"), IsPropertyDirty("Sp_IsStaticList"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsStaticList_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsStaticList_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_SpRsTp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_SpRsTp_Id");
                string newBrokenRules = "";
                
                if (Sp_SpRsTp_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_SpRsTp_Id", BROKENRULE_Sp_SpRsTp_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_SpRsTp_Id", BROKENRULE_Sp_SpRsTp_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_SpRsTp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_SpRsTp_Id", _brokenRuleManager.IsPropertyValid("Sp_SpRsTp_Id"), IsPropertyDirty("Sp_SpRsTp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_SpRsTp_Id_TextField_Validate()
        {
        }

        private void Sp_SpRtTp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_SpRtTp_Id");
                string newBrokenRules = "";
                
                if (Sp_SpRtTp_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_SpRtTp_Id", BROKENRULE_Sp_SpRtTp_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_SpRtTp_Id", BROKENRULE_Sp_SpRtTp_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_SpRtTp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_SpRtTp_Id", _brokenRuleManager.IsPropertyValid("Sp_SpRtTp_Id"), IsPropertyDirty("Sp_SpRtTp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_SpRtTp_Id_TextField_Validate()
        {
        }

        private void Sp_IsInputParamsAsObjectArray_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsInputParamsAsObjectArray_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsInputParamsAsObjectArray");
                string newBrokenRules = "";
                
                if (Sp_IsInputParamsAsObjectArray == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsInputParamsAsObjectArray", BROKENRULE_Sp_IsInputParamsAsObjectArray_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsInputParamsAsObjectArray", BROKENRULE_Sp_IsInputParamsAsObjectArray_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsInputParamsAsObjectArray");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsInputParamsAsObjectArray", _brokenRuleManager.IsPropertyValid("Sp_IsInputParamsAsObjectArray"), IsPropertyDirty("Sp_IsInputParamsAsObjectArray"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsInputParamsAsObjectArray_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsInputParamsAsObjectArray_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_HasNewParams_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewParams_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_HasNewParams");
                string newBrokenRules = "";
                
                if (Sp_HasNewParams == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_HasNewParams", BROKENRULE_Sp_HasNewParams_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_HasNewParams", BROKENRULE_Sp_HasNewParams_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_HasNewParams");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_HasNewParams", _brokenRuleManager.IsPropertyValid("Sp_HasNewParams"), IsPropertyDirty("Sp_HasNewParams"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewParams_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewParams_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsParamsValid_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsValid_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsParamsValid");
                string newBrokenRules = "";
                
                if (Sp_IsParamsValid == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsParamsValid", BROKENRULE_Sp_IsParamsValid_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsParamsValid", BROKENRULE_Sp_IsParamsValid_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsParamsValid");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsParamsValid", _brokenRuleManager.IsPropertyValid("Sp_IsParamsValid"), IsPropertyDirty("Sp_IsParamsValid"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsValid_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsValid_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsParamValueSetValid_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamValueSetValid_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsParamValueSetValid");
                string newBrokenRules = "";
                
                if (Sp_IsParamValueSetValid == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsParamValueSetValid", BROKENRULE_Sp_IsParamValueSetValid_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsParamValueSetValid", BROKENRULE_Sp_IsParamValueSetValid_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsParamValueSetValid");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsParamValueSetValid", _brokenRuleManager.IsPropertyValid("Sp_IsParamValueSetValid"), IsPropertyDirty("Sp_IsParamValueSetValid"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamValueSetValid_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamValueSetValid_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_HasNewColumns_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewColumns_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_HasNewColumns");
                string newBrokenRules = "";
                
                if (Sp_HasNewColumns == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_HasNewColumns", BROKENRULE_Sp_HasNewColumns_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_HasNewColumns", BROKENRULE_Sp_HasNewColumns_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_HasNewColumns");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_HasNewColumns", _brokenRuleManager.IsPropertyValid("Sp_HasNewColumns"), IsPropertyDirty("Sp_HasNewColumns"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewColumns_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewColumns_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsColumnsValid_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsColumnsValid_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsColumnsValid");
                string newBrokenRules = "";
                
                if (Sp_IsColumnsValid == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsColumnsValid", BROKENRULE_Sp_IsColumnsValid_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsColumnsValid", BROKENRULE_Sp_IsColumnsValid_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsColumnsValid");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsColumnsValid", _brokenRuleManager.IsPropertyValid("Sp_IsColumnsValid"), IsPropertyDirty("Sp_IsColumnsValid"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsColumnsValid_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsColumnsValid_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsValid_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsValid_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsValid");
                string newBrokenRules = "";
                
                if (Sp_IsValid == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsValid", BROKENRULE_Sp_IsValid_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsValid", BROKENRULE_Sp_IsValid_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsValid");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsValid", _brokenRuleManager.IsPropertyValid("Sp_IsValid"), IsPropertyDirty("Sp_IsValid"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsValid_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsValid_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_Notes_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Notes_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Notes");
                string newBrokenRules = "";
                				int len = 0;
                if (Sp_Notes != null)
                {
                    len = Sp_Notes.Length;
                }

                if (len > STRINGSIZE_Sp_Notes)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_Notes", BROKENRULE_Sp_Notes_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_Notes", BROKENRULE_Sp_Notes_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Notes");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_Notes", _brokenRuleManager.IsPropertyValid("Sp_Notes"), IsPropertyDirty("Sp_Notes"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Notes_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Notes_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsActiveRow");
                string newBrokenRules = "";
                
                if (Sp_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsActiveRow", BROKENRULE_Sp_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsActiveRow", BROKENRULE_Sp_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsActiveRow", _brokenRuleManager.IsPropertyValid("Sp_IsActiveRow"), IsPropertyDirty("Sp_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsDeleted");
                string newBrokenRules = "";
                
                if (Sp_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsDeleted", BROKENRULE_Sp_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsDeleted", BROKENRULE_Sp_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsDeleted", _brokenRuleManager.IsPropertyValid("Sp_IsDeleted"), IsPropertyDirty("Sp_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_CreatedUserId_Validate()
        {
        }

        private void Sp_CreatedDate_Validate()
        {
        }

        private void Sp_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_LastModifiedDate_Validate()
        {
        }

        private void Sp_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Stamp");
                string newBrokenRules = "";
                
                if (Sp_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_Stamp", BROKENRULE_Sp_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_Stamp", BROKENRULE_Sp_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_Stamp", _brokenRuleManager.IsPropertyValid("Sp_Stamp"), IsPropertyDirty("Sp_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


