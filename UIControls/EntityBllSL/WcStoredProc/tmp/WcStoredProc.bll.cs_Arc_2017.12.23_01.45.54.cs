using System;
using System.Collections.Generic;
using TypeServices;
using System.ComponentModel;
using System.Diagnostics;
using EntityWireType;
using Ifx;
using Velocity;
using ProxyWrapper;
using vComboDataTypes;

// Gen Timestamp:  12/23/2017 1:40:05 AM

namespace EntityBll
{


    public partial class WcStoredProc_Bll : BusinessObjectBase, ITraceItem, IBusinessObject, IBusinessObjectV2, INotifyPropertyChanged , IEditableObject
    {


        #region Initialize Variables

        /// <summary>
        /// This is a private instance of TraceItemList which is normaly used in every entity
        /// business type and is used to provide rich information abou the current state and values
        /// in this business object when included in a trace call. To see exactly what data will be
        /// provided in a trace, look at the GetTraceItemsShortList method in the WcStoredProc_Bll code
        /// file.
        /// </summary>
        /// <seealso cref="!:file://D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Optimized_Deep_Tracing_of_Business_Object.html" cat="My Test Category">My Test Caption</seealso>
        TraceItemList _traceItems;
        /// <seealso cref="EntityWireType.WcStoredProc_Values">WcStoredProc_Values Class</seealso>
        /// <summary>
        ///     This is an instance of the wire type (<see cref="EntityWireType.WcStoredProc_Values">WcStoredProc_Values</see>) that’s wrapped by the Values
        ///     Manager (<see cref="EntityWireType.WcStoredProc_ValuesMngr">WcStoredProc_ValuesMngr</see>) and
        ///     used to Plug-n-Play into this business object.
        /// </summary>
        private WcStoredProc_ValuesMngr _data = null;

        /// <summary>See <see cref="OnCrudFailed">OnCrudFailed</see> for information.</summary>
        public event CrudFailedEventHandler CrudFailed;

        public event AsyncSaveCompleteEventHandler AsyncSaveComplete;

        public event AsyncSaveWithResponseCompleteEventHandler AsyncSaveWithResponseComplete;

        public event EntityRowReceivedEventHandler EntityRowReceived;


        /// <summary>
        ///     See <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> for
        ///     information.
        /// </summary>
        public event CurrentEntityStateEventHandler CurrentEntityStateChanged;
        /// <summary>See <see cref="OnBrokenRuleChanged">OnBrokenRuleChanged</see> for information.</summary>
        public event BrokenRuleEventHandler BrokenRuleChanged;
        /// <summary>
        /// 	<para>
        ///         Raised by the This event is currently <see cref="OnPropertyValueChanged">OnPropertyValueChanged</see> method, its currently not
        ///         being used and may be obsolete.
        ///     </para>
        /// </summary>
        public event PropertyValueChangedEventHandler PropertyValueChanged;
        /// <summary>This event is currently not being used and may be obsolete.</summary>
        public event BusinessObjectUpdatedEventHandler BusinessObjectUpdated;
        /// <summary>
        /// 	<para>
        ///         Required by the INotifyPropertyChanged interface, this event is raise by the
        ///         <see cref="Notify">Notify</see> method (also part of the INotifyPropertyChanged
        ///         interface. The Notify method is called in nearly all public data field
        ///         properties. The INotifyPropertyChanged interface is implemented to make
        ///         WcStoredProc_Bll more extendable.
        ///     </para>
        /// 	<para>The <strong>INotifyPropertyChanged</strong> interface is used to notify
        ///     clients, typically binding clients, that a property value has changed. For more
        ///     information, search INotifyPropertyChanged on <strong>MSDN</strong>.</para>
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 	<para>
        ///         Private field for the <see cref="ActiveRestrictedStringProperty">ActiveRestrictedStringProperty</see> property.
        ///         See it’s documentation for details.
        ///     </para>
        /// 	<para></para>
        /// </summary>
        private string _activeRestrictedStringProperty;
        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcStoredProc_Bll";
        /// <summary>
        /// 	<para>
        ///         Used in the validation section of this class in the WcStoredProc.bll.vld.cs code file,
        ///         this event notifies event handlers that a data field’s valid state has changed
        ///         (valid or not valid).<br/>
        ///         For more information about this event, see <see cref="TypeServices.ControlValidStateChangedEventHandler">CurrentEntityStateEventHandler</see>.<br/>
        ///         For more information on how it’s used, see these methods:
        ///     </para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="NewEntityRow">NewEntityRow</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="GetEntityRow">GetEntityRow</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="Save">Save</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcProps.RaiseCurrentEntityStateChanged">ucWcStoredProcProps.RaiseCurrentEntityStateChanged()</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcProps.RaiseCurrentEntityStateChanged">ucWcStoredProcProps.RaiseCurrentEntityStateChanged(EntityStateSwitch
        ///             state)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public event ControlValidStateChangedEventHandler ControlValidStateChanged;
        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcProps.OnRestrictedTextLengthChanged">ucWcStoredProcProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcProps.SetRestrictedStringLengthText">ucWcStoredProcProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public event RestrictedTextLengthChangedEventHandler RestrictedTextLengthChanged;

        UserSecurityContext _context = null;

        private object _parentEditObject = null;
        private ParentEditObjectType _parentType = ParentEditObjectType.None;

        private WcStoredProcService_ProxyWrapper _wcStoredProcProxy = null;



        Guid _StandingFK = Guid.Empty;
        string _parentEntityType = "";

        private WcStoredProcParamService_ProxyWrapper _proxyWcStoredProcParamService;
            
        private WcStoredProcColumnService_ProxyWrapper _proxyWcStoredProcColumnService;
            
        #endregion Initialize Variables

            
        #region Initialize Class

        public WcStoredProc_Bll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Bll()", IfxTraceCategory.Enter);
                _context = UserSecurityContext.GetCurrent();
                InitializeClass();
//                WcStoredProcService_ProxyWrapper _wcStoredProcProxy = new  WcStoredProcService_ProxyWrapper();
//                _wcStoredProcProxy.WcStoredProc_SaveCompleted += new EventHandler<WcStoredProc_SaveCompletedEventArgs>(SaveCompleted);
                _proxyWcStoredProcParamService = new WcStoredProcParamService_ProxyWrapper();
                _proxyWcStoredProcParamService.WcStoredProcParam_GetListByFKCompleted += _proxyWcStoredProcParamService_WcStoredProcParam_GetListByFKCompleted;
                _children_WcStoredProcParam.Add(new WcStoredProcParam_Bll());
            
                _proxyWcStoredProcColumnService = new WcStoredProcColumnService_ProxyWrapper();
                _proxyWcStoredProcColumnService.WcStoredProcColumn_GetListByFKCompleted += _proxyWcStoredProcColumnService_WcStoredProcColumn_GetListByFKCompleted;
                _children_WcStoredProcColumn.Add(new WcStoredProcColumn_Bll());
            
                SetNewEntityRow();

                CustomConstructor();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Bll()", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Bll()", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>
        ///         Passes in the <see cref="EntityWireType.WcStoredProc_ValuesMngr">WcStoredProc_ValuesMngr</see> type and <see cref="TypeServices.EntityState">EntityState</see>. The WcStoredProc_ValuesMngr type is the
        ///         data object used in a ‘Plug-n-Play’ fashion for loading data into WcStoredProc_Bll
        ///         fast and efficiently. EntityState sets WcStoredProc_Bll’s state.
        ///     </para>
        /// 	<para>Used by the following methods call this constructor:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="FromWire">FromWire</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcStoredProc_List.Fill">Fill</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcStoredProc_List.ReplaceList">ReplaceList(WcStoredProc_ValuesMngr[]
        ///             list)</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcStoredProc_List.ReplaceList">ReplaceList(IEntity_ValuesMngr[]
        ///             list)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public WcStoredProc_Bll(WcStoredProc_ValuesMngr valueObject, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Bll(WcStoredProc_ValuesMngr valueObject, EntityState state)", IfxTraceCategory.Enter);
                _context = UserSecurityContext.GetCurrent(); 
                _data = valueObject;
                InitializeClass(state);
               // _data.SetClassStateAfterFetch(state);
//                 WcStoredProcService_ProxyWrapper _wcStoredProcProxy = new  WcStoredProcService_ProxyWrapper();
//                _wcStoredProcProxy.WcStoredProc_SaveCompleted += new EventHandler<WcStoredProc_SaveCompletedEventArgs>(SaveCompleted);
                _proxyWcStoredProcParamService = new WcStoredProcParamService_ProxyWrapper();
                _proxyWcStoredProcParamService.WcStoredProcParam_GetListByFKCompleted += _proxyWcStoredProcParamService_WcStoredProcParam_GetListByFKCompleted;
                _children_WcStoredProcParam.Add(new WcStoredProcParam_Bll());
            
                _proxyWcStoredProcColumnService = new WcStoredProcColumnService_ProxyWrapper();
                _proxyWcStoredProcColumnService.WcStoredProcColumn_GetListByFKCompleted += _proxyWcStoredProcColumnService_WcStoredProcColumn_GetListByFKCompleted;
                _children_WcStoredProcColumn.Add(new WcStoredProcColumn_Bll());
            
                CustomConstructor();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Bll(WcStoredProc_ValuesMngr valueObject, EntityState state)", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Bll(WcStoredProc_ValuesMngr valueObject, EntityState state)", IfxTraceCategory.Leave);
            }
        }


        private void InitializeClass()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Enter);
                InitializeClass(new EntityState(true, true, false));
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Leave);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Leave);
            }
        }



        /// <summary>Called from the constructor, events are wired here.</summary>
        private void InitializeClass(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass(EntityState state)", IfxTraceCategory.Enter);
                if (_data == null)
                {
                    _data = GetDefault();
                    //**  This is a hack.  We should set the state elsewhere, however, when we get this from accross the wire,
                    //      we loose the state and therefore are forced to always call SetClassStateAfterFetch.
                   // _data.SetClassStateAfterFetch(state);
                }
                _data.SetClassStateAfterFetch(state);
                BrokenRuleManagerProperty = new BrokenRuleManager(this);
                _brokenRuleManager.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                //TODO: need to code it for a dynamic creation for an insert
                //TODO: need to move save, assignwithid, update, delete, deactivate, persist (generic, check the state) into this class
                //TODO: need to move the various lists of apps to get into an applicationprovider class for the client side that thunks to the proxywrapper
                //TODO: need to move field level validation into the setters and call the object validation on any persist operation
                //TODO:need to implement the two eh and tracing interfaces on the object
                InitializeClass_Cust();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityState", IfxTraceCategory.Leave);
            }
        }

        private  WcStoredProcService_ProxyWrapper WcStoredProcProxy
        {
            get
            {
                if (_wcStoredProcProxy == null)
                {
                    _wcStoredProcProxy = new  WcStoredProcService_ProxyWrapper();
                    _wcStoredProcProxy.WcStoredProc_SaveCompleted += new EventHandler<WcStoredProc_SaveCompletedEventArgs>(SaveCompleted);
                    _wcStoredProcProxy.WcStoredProc_GetByIdCompleted += new EventHandler<WcStoredProc_GetByIdCompletedEventArgs>(GetEntityRowResponse);

                }

                return _wcStoredProcProxy;
            }
        }

        #endregion  Initialize Class


		#region CRUD Methods



        /// <returns><see cref="EntityWireType.WcStoredProc_ValuesMngr">WcStoredProc_ValuesMngr</see></returns>
        /// <summary>
        ///     When this business type is set to a new state, a new data type also known as a wire
        ///     type (<see cref="EntityWireType.WcStoredProc_Values">WcStoredProc_Values</see>) is created, new
        ///     Id value is created, default values are set and appropriate state setting are made.
        ///     WcStoredProc_Values is wrapped in the <see cref="EntityWireType.WcStoredProc_ValuesMngr">WcStoredProc_ValuesMngr</see> type and returned.
        /// </summary>
        private WcStoredProc_ValuesMngr GetDefault()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                Guid Id = GetNewID();
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Enter);
                return new WcStoredProc_ValuesMngr(new object[] 
                    {
                        Id,    //  Sp_Id
						null,		//  Sp_ApVrsn_Id
						null,		//  Sp_Name
						false,		//  Sp_IsCodeGen
						false,		//  Sp_IsReadyCodeGen
						false,		//  Sp_IsCodeGenComplete
						false,		//  Sp_IsTagForCodeGen
						false,		//  Sp_IsTagForOther
						null,		//  Sp_SprocGroups
						false,		//  Sp_IsBuildDataAccessMethod
						false,		//  Sp_BuildListClass
						false,		//  Sp_IsFetchEntity
						false,		//  Sp_IsStaticList
						true,		//  Sp_IsTypeComboItem
						false,		//  Sp_IsCreateWireType
						null,		//  Sp_WireTypeName
						null,		//  Sp_MethodName
						null,		//  Sp_AssocEntity_Id
						null,		//  Sp_AssocEntity_Id_TextField
						null,		//  Sp_AssocEntityNames
						null,		//  Sp_SpRsTp_Id
						null,		//  Sp_SpRsTp_Id_TextField
						null,		//  Sp_SpRtTp_Id
						null,		//  Sp_SpRtTp_Id_TextField
						false,		//  Sp_IsInputParamsAsObjectArray
						true,		//  Sp_IsParamsAutoRefresh
						false,		//  Sp_HasNewParams
						false,		//  Sp_IsParamsValid
						false,		//  Sp_IsParamValueSetValid
						false,		//  Sp_HasNewColumns
						false,		//  Sp_IsColumnsValid
						false,		//  Sp_IsValid
						null,		//  Sp_Notes
						true,		//  Sp_IsActiveRow
						false,		//  Sp_IsDeleted
						null,		//  Sp_CreatedUserId
						null,		//  Sp_CreatedDate
						null,		//  Sp_UserId
						null,		//  UserName
						null,		//  Sp_LastModifiedDate
						null,		//  Sp_Stamp
                    }, new EntityState(true, true, false)
                );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// Called by any parent object that wants to clear out the existing data and state,
        /// and replace all with new state and data object.
        /// </summary>
        public void NewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Enter);
                _data = GetDefault();
                NewEntityRowCustomCode();
                SetDefaultBrokenRules();
                //RaiseEventCurrentEntityStateChanged(EntityStateSwitch.NewInvalidNotDirty);
                RaiseEventCurrentEntityStateChanged(_data.S.Switch);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// This is called from the parameterless constructor.  It's assumed that a data object will not be passed (which would have
        /// cause the EntityState to be configured properly) and therefore this new entity must be properly configured.
        /// </summary>
        public void SetNewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", IfxTraceCategory.Enter);
                NewEntityRowCustomCode();
                SetDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", IfxTraceCategory.Leave);
            }
        }


        public void GetEntityRow(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", new ValuePair[] {new ValuePair("Id", Id) }, IfxTraceCategory.Enter);

                WcStoredProcProxy.Begin_WcStoredProc_GetById(Id);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", IfxTraceCategory.Leave);
            }
        }

        public void GetEntityRow(long Id)
        {
            throw new NotImplementedException("WcStoredProc_Bll GetEntityRow(long Id) Not Implemented");
        }

        public void GetEntityRow(int Id)
        {
            throw new NotImplementedException("WcStoredProc_Bll GetEntityRow(int Id) Not Implemented");
        }

        public void GetEntityRow(short Id)
        {
            throw new NotImplementedException("WcStoredProc_Bll GetEntityRow(short Id) Not Implemented");
        }

        public void GetEntityRow(byte Id)
        {
            throw new NotImplementedException("WcStoredProc_Bll GetEntityRow(byte Id) Not Implemented");
        }

        public void GetEntityRow(object Id)
        {
            throw new NotImplementedException("WcStoredProc_Bll GetEntityRow(object Id) Not Implemented");
        }

        private void GetEntityRowResponse(object sender, WcStoredProc_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                if (data == null)
                {
                    // Alert UI
                    return;
                }
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array == null)
                {
                    // Alert UI
                    return;
                }
                _brokenRuleManager.ClearAllBrokenRules();
                _data.ReplaceData((object[])array[0], new EntityState(false, true, false));
                RaiseEventEntityRowReceived();
                RefreshFields();
                // Do we still need this next line?
                //RaiseEventCurrentEntityStateChanged(_data.S.Switch);

                //  ToDo:  Later when we have a Foreign Key, do something like this:  DataProps.TbC_Tb_ID = DataProps.standing_FK;
                //              How do we handle the standing FK in the new framework?
 
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", IfxTraceCategory.Leave);
            }
        }



        /// <summary>
        ///     Called by <see cref="GetDefault">GetDefault</see> and creates a new WcStoredProc Id value
        ///     for a new data object (<see cref="EntityWireType.WcStoredProc_ValuesMngr">WcStoredProc_ValuesMngr</see>).
        /// </summary>
        private Guid GetNewID()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", IfxTraceCategory.Enter);
                return Guid.NewGuid();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Un-does the current data edits and restores everything back to the previous saved
        ///     or new <see cref="TypeServices.EntityState">state</see>. This will include calling
        ///     <see cref="TypeServices.BrokenRuleManager.ClearAllBrokenRules">_brokenRuleManager.ClearAllBrokenRules</see>
        ///     to clear any BrokenRules or calling <see cref="SetDefaultBrokenRules">SetDefaultBrokenRules</see> to set any default BrokenRules;
        ///     and raising the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event is raised to
        ///     notify the parent objects that the state has changed.
        /// </summary>
        public void UnDo()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Enter);
                EntityStateSwitch es = _data.S.Switch;
                _data.SetCurrentToOriginal();
                if (_data.S.IsNew() == true)
                {
                    SetDefaultBrokenRules();
                }
                else
                {
                    _brokenRuleManager.ClearAllBrokenRules();
                }
                CheckEntityState(es);

                UnDoCustomCode();
                RefreshFields();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Call the Notify method for all visible fields.  This allows the fields/properties to be refreshed in grids and other controls that are consuming them via databinding.
        /// </summary>
        public void RefreshFields()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", IfxTraceCategory.Enter);
				Notify("SprocInDb");
				Notify("Sp_Name");
				Notify("Sp_IsCodeGen");
				Notify("Sp_IsReadyCodeGen");
				Notify("Sp_IsCodeGenComplete");
				Notify("Sp_IsTagForCodeGen");
				Notify("Sp_IsTagForOther");
				Notify("Sp_SprocGroups");
				Notify("Sp_IsBuildDataAccessMethod");
				Notify("Sp_BuildListClass");
				Notify("Sp_IsFetchEntity");
				Notify("Sp_IsStaticList");
				Notify("Sp_IsTypeComboItem");
				Notify("Sp_IsCreateWireType");
				Notify("Sp_WireTypeName");
				Notify("Sp_MethodName");
				Notify("Sp_AssocEntity_Id");
				Notify("Sp_AssocEntity_Id_TextField");
				Notify("Sp_AssocEntityNames");
				Notify("Sp_SpRsTp_Id");
				Notify("Sp_SpRsTp_Id_TextField");
				Notify("Sp_SpRtTp_Id");
				Notify("Sp_SpRtTp_Id_TextField");
				Notify("Sp_IsInputParamsAsObjectArray");
				Notify("Sp_IsParamsAutoRefresh");
				Notify("Sp_HasNewParams");
				Notify("Sp_IsParamsValid");
				Notify("Sp_IsParamValueSetValid");
				Notify("Sp_HasNewColumns");
				Notify("Sp_IsColumnsValid");
				Notify("Sp_IsValid");
				Notify("Sp_Notes");
				Notify("Sp_IsActiveRow");
				Notify("UserName");
				Notify("Sp_LastModifiedDate");

                RefreshFieldsCustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Called from <see cref="wnConcurrencyManager">wnConcurrencyManager</see> and therefore does not need
        ///     to pass in the Parent and Parent Type parameters.
        ///     Saves the current data to the data store, configures the appropriate state and
        ///     calls <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see> to
        ///     notify the parent object of the state change.
        /// </summary>
        public void Save(UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                _data.C.Sp_UserId_noevents = Credentials.UserId;
                WcStoredProcProxy.Begin_WcStoredProc_Save(_data.C.GetValues(), (int)check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Saves the current data to the data store, configures the appropriate state and
        ///     calls <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see> to
        ///     notify the parent object of the state change.
        /// </summary>
        public void Save(object parentEditObject, ParentEditObjectType parentType,  UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                _parentEditObject = parentEditObject;
                _parentType = parentType;
                _data.C.Sp_UserId_noevents = Credentials.UserId;
                WcStoredProcProxy.Begin_WcStoredProc_Save(_data.C.GetValues(), (int)check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        void SaveCompleted(object sender, WcStoredProc_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", IfxTraceCategory.Enter);
                DataServiceInsertUpdateResponse newData = null;
                if (e.Result == null)
                {
                    // return some message to the calling object
                    newData = new DataServiceInsertUpdateResponse();
                    newData.Result = DataOperationResult.HandledException;
                    //RaiseEventAsyncSaveComplete(newData.Result, null);
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);
                }
                else
                {
                    newData = new DataServiceInsertUpdateResponse();
                    byte[] data = e.Result;
                    object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                    newData.SetFromObjectArray(array);
                }

                // Get the Result status
                if (newData.Result == DataOperationResult.Success)
                {
                    if (newData.ReturnCurrentRowOnInsertUpdate == true)
                    {
                        // If we returned the current row from the data store, then syncronize the wire object with it.
                        _data.C.ReplaceDataFromObjectArray(newData.CurrentValues);
                        RefreshFields();
                    }
                    else
                    {
                        // Otherwise:
                        //  Set the PK value from the data store incase we did an insert.
                        //  In theory the guid was already created by this busness object, but we're setting it here
                        //  just in case some code changed somewhere and it was created on the server.
                        _data.C._a = newData.guidPrimaryKey;
                        //  Set the new TimeStamp value
                        _data.C.Sp_Stamp_noevents = newData.CurrentTimestamp;
                    }
                    _data.S.SetNotNew();
                    _data.S.SetValid();
                    _data.S.SetNotDirty();
                    _data.SetOriginalToCurrent();
                  
                    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);

                    //if (_parentType == ParentEditObjectType.EntitiyPropertiesControl)
                    //{
                    //    // A EntitiyPropertiesControl called the same method, therefore raise the following event
                    //    // allowing all parent conrol to reconfigure thier entity state.
                    //    // If this was called from editing a grid control, then this is not nessesacy since we 
                    //    // dont change the state of surrounding controls.
                    //    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                    //
                    //    // Rasie this event to the props control can call it's SetState method to refresh the UI
                    //    // The Entity List control will be updated via the PropertyChanged event which gets called from the RefreshFields call above.
                    //    if (newData.ReturnCurrentRowOnInsertUpdate == true)
                    //    {
                    //        RaiseEventAsyncSaveWithResponseComplete(newData, null);
                    //    }
                    //
                    //}
                    //else
                    //{
                    //    RaiseEventAsyncSaveComplete(newData.Result, null);
                    //}
                }
                else
                {
                    // Add the current date from the database to the wire obect's concurrent property
                    //if (newData.Result == DataOperationResult.ConcurrencyFailure)
                    //{
                    //    _data.X.ReplaceDataFromObjectArray(newData.CurrentValues);
                    //}
                    // ToDo:  we need to develope logic and code to pass user friendly error message 
                    // instead of null when the save failed.

                    // We had some type of failure, so raise this event regardless of the parent type 
                    // so the UI can notify the user.
                    //RaiseEventAsyncSaveComplete(newData.Result, null);
                    //*** use this instead
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _parentEditObject = null;
                _parentType = ParentEditObjectType.None;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", IfxTraceCategory.Leave);
            }
        }







        /// <summary>
        /// Calls a Delete method on the server to delete this entity from the data
        /// store.
        /// </summary>
        /// <returns>1 = Success, 2 = Failed</returns>
        public int  Entity_Delete()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", IfxTraceCategory.Enter);
//                // add credentials
//                WcStoredProc_ValuesMngr retObject = ProxyWrapper.EntityProxyWrapper.WcStoredProc_Delete(_data);
//                return 1;
//                //  Needs further design.  What do we do when the delete fails or there is an concurrency issue?
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Just a stub and not being used. Consider renaming this to ActivatOrDeactivate so
        /// it can be used either way.
        /// </summary>
        /// <returns>Returns 0 because this method is not currently functional.</returns>
        public int Entity_Deactivate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", IfxTraceCategory.Enter);
                //  This is currently setup to return a wire object, however, we might only need to return an int flag
                //  Needs further design
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", IfxTraceCategory.Leave);
            }
        }

        /// <returns>Returns 0 because this method is not currently functional.</returns>
        /// <summary>Just a stub and not being used.</summary>
        public int Entity_Remove()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", IfxTraceCategory.Enter);
                //  This is currently setup to return a wire object, however, we might only need to return an int flag
                //  Needs further design
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Copies the values from another data object (<see cref="EntityWireType.WcStoredProc_ValuesMngr">WcStoredProc_ValuesMngr</see>) to the one plugged into
        ///     this business object. This is called from the Save method because it returns a
        ///     fresh copy of this entity’s data from the data store incase modifications were made
        ///     in the process of saving.
        /// </summary>
        /// <param name="thisData">
        /// The data object (WcStoredProc_ValuesMngr) plugged into this business object which data
        /// will be copied to.
        /// </param>
        /// <param name="newData">The data object (WcStoredProc_ValuesMngr) which data will be copied from.</param>
        private void SyncValueObjectCurrentProperties(WcStoredProc_ValuesMngr thisData, WcStoredProc_ValuesMngr newData)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", IfxTraceCategory.Enter);
				thisData.C.Sp_Id_noevents = newData.C.Sp_Id_noevents;
				thisData.C.Sp_ApVrsn_Id_noevents = newData.C.Sp_ApVrsn_Id_noevents;
				thisData.C.Sp_Name_noevents = newData.C.Sp_Name_noevents;
				thisData.C.Sp_IsCodeGen_noevents = newData.C.Sp_IsCodeGen_noevents;
				thisData.C.Sp_IsReadyCodeGen_noevents = newData.C.Sp_IsReadyCodeGen_noevents;
				thisData.C.Sp_IsCodeGenComplete_noevents = newData.C.Sp_IsCodeGenComplete_noevents;
				thisData.C.Sp_IsTagForCodeGen_noevents = newData.C.Sp_IsTagForCodeGen_noevents;
				thisData.C.Sp_IsTagForOther_noevents = newData.C.Sp_IsTagForOther_noevents;
				thisData.C.Sp_SprocGroups_noevents = newData.C.Sp_SprocGroups_noevents;
				thisData.C.Sp_IsBuildDataAccessMethod_noevents = newData.C.Sp_IsBuildDataAccessMethod_noevents;
				thisData.C.Sp_BuildListClass_noevents = newData.C.Sp_BuildListClass_noevents;
				thisData.C.Sp_IsFetchEntity_noevents = newData.C.Sp_IsFetchEntity_noevents;
				thisData.C.Sp_IsStaticList_noevents = newData.C.Sp_IsStaticList_noevents;
				thisData.C.Sp_IsTypeComboItem_noevents = newData.C.Sp_IsTypeComboItem_noevents;
				thisData.C.Sp_IsCreateWireType_noevents = newData.C.Sp_IsCreateWireType_noevents;
				thisData.C.Sp_WireTypeName_noevents = newData.C.Sp_WireTypeName_noevents;
				thisData.C.Sp_MethodName_noevents = newData.C.Sp_MethodName_noevents;
				thisData.C.Sp_AssocEntity_Id_noevents = newData.C.Sp_AssocEntity_Id_noevents;
				thisData.C.Sp_AssocEntity_Id_TextField_noevents = newData.C.Sp_AssocEntity_Id_TextField_noevents;
				thisData.C.Sp_AssocEntityNames_noevents = newData.C.Sp_AssocEntityNames_noevents;
				thisData.C.Sp_SpRsTp_Id_noevents = newData.C.Sp_SpRsTp_Id_noevents;
				thisData.C.Sp_SpRsTp_Id_TextField_noevents = newData.C.Sp_SpRsTp_Id_TextField_noevents;
				thisData.C.Sp_SpRtTp_Id_noevents = newData.C.Sp_SpRtTp_Id_noevents;
				thisData.C.Sp_SpRtTp_Id_TextField_noevents = newData.C.Sp_SpRtTp_Id_TextField_noevents;
				thisData.C.Sp_IsInputParamsAsObjectArray_noevents = newData.C.Sp_IsInputParamsAsObjectArray_noevents;
				thisData.C.Sp_IsParamsAutoRefresh_noevents = newData.C.Sp_IsParamsAutoRefresh_noevents;
				thisData.C.Sp_HasNewParams_noevents = newData.C.Sp_HasNewParams_noevents;
				thisData.C.Sp_IsParamsValid_noevents = newData.C.Sp_IsParamsValid_noevents;
				thisData.C.Sp_IsParamValueSetValid_noevents = newData.C.Sp_IsParamValueSetValid_noevents;
				thisData.C.Sp_HasNewColumns_noevents = newData.C.Sp_HasNewColumns_noevents;
				thisData.C.Sp_IsColumnsValid_noevents = newData.C.Sp_IsColumnsValid_noevents;
				thisData.C.Sp_IsValid_noevents = newData.C.Sp_IsValid_noevents;
				thisData.C.Sp_Notes_noevents = newData.C.Sp_Notes_noevents;
				thisData.C.Sp_IsActiveRow_noevents = newData.C.Sp_IsActiveRow_noevents;
				thisData.C.Sp_IsDeleted_noevents = newData.C.Sp_IsDeleted_noevents;
				thisData.C.Sp_CreatedUserId_noevents = newData.C.Sp_CreatedUserId_noevents;
				thisData.C.Sp_CreatedDate_noevents = newData.C.Sp_CreatedDate_noevents;
				thisData.C.Sp_UserId_noevents = newData.C.Sp_UserId_noevents;
				thisData.C.UserName_noevents = newData.C.UserName_noevents;
				thisData.C.Sp_LastModifiedDate_noevents = newData.C.Sp_LastModifiedDate_noevents;
				thisData.C.Sp_Stamp_noevents = newData.C.Sp_Stamp_noevents;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", IfxTraceCategory.Leave);
            }
        }


		#endregion CRUD Methods


		#region General Methods and Properties


		#region Wire Methods

        /// <summary>
        /// Returns a business object implementing the IBusinessObject interface from a wire
        /// object. This assumes the WcStoredProc_ValuesMngr being passed in has been fetched from a
        /// reliable data store or some other reliable source which gives us appropriate data and
        /// state.
        /// </summary>
        public static WcStoredProc_Bll FromWire(WcStoredProc_ValuesMngr valueObject)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", new ValuePair[] {new ValuePair("valueObject", valueObject) }, IfxTraceCategory.Enter);
                //  This assumes that each WcStoredProc_ValuesMngr has been fetched from a db or some other reliable source which gives us the following state
                //  Hack, Hack;  we have to add the state here becuase of how its handled (or not handled) in WcStoredProc_ValuesMngr due to the problem of loosing this value when passed accross the wire
                WcStoredProc_Bll obj = new WcStoredProc_Bll(valueObject, new EntityState(false, true, false));
                return obj;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Returns the current wire object plugged into this business object.</summary>
        public WcStoredProc_ValuesMngr ToWire()
        {
            return _data;
        }

        /// <summary>The wire object property.</summary>
        public WcStoredProc_ValuesMngr Wire
        {
            get { return _data; }
            set
            {
                _data = value;
            }
        }        

		#endregion Wire Methods


        /// <overloads>Get a list of current BrokenRules for this entity.</overloads>
        /// <summary>Retuns the current BrokenRules as list of strings.</summary>
        public List<string> GetBrokenRulesForEntity()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRulesForEntity();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Pass in a valid format as a string and return the current BrokenRules in a
        /// formatted list of strings.
        /// </summary>
        public List<string> GetBrokenRulesForEntity(string format)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", new ValuePair[] {new ValuePair("format", format) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRulesForEntity(format);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Leave);
            }
        }

        // get the current list of broken rules for a property
        /// <summary>Pass in a property name and return a list of its current BrokenRules.</summary>
        public List<vRuleItem> GetBrokenRulesForProperty(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRuleListForProperty(propertyName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Pass in a property name to find out if it’s valid or not.</summary>
        /// <returns>true = valid, false = not valid</returns>
        public bool IsPropertyValid(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.IsPropertyValid(propertyName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", IfxTraceCategory.Leave);
            }
        }

        public bool IsPropertyDirty(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);

                if (propertyName == null)
                {
                    throw new Exception("WcStoredProc_Bll.IsPropertyDirty: Parameter 'propertyName' had no value.");
                }
                else if (propertyName.Trim().Length == 0)
                {
                    throw new Exception("WcStoredProc_Bll.IsPropertyDirty: Parameter 'propertyName' had no value.");
                }

                switch (propertyName)
                {
                    case "Sp_ApVrsn_Id":
                        if (_data.C.Sp_ApVrsn_Id != _data.O.Sp_ApVrsn_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "SprocInDb":
                        return IsPropertyDirtyCustomCode(propertyName);
                    case "Sp_Name":
                        if (_data.C.Sp_Name != _data.O.Sp_Name)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsCodeGen":
                        if (_data.C.Sp_IsCodeGen != _data.O.Sp_IsCodeGen)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsReadyCodeGen":
                        if (_data.C.Sp_IsReadyCodeGen != _data.O.Sp_IsReadyCodeGen)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsCodeGenComplete":
                        if (_data.C.Sp_IsCodeGenComplete != _data.O.Sp_IsCodeGenComplete)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsTagForCodeGen":
                        if (_data.C.Sp_IsTagForCodeGen != _data.O.Sp_IsTagForCodeGen)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsTagForOther":
                        if (_data.C.Sp_IsTagForOther != _data.O.Sp_IsTagForOther)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_SprocGroups":
                        if (_data.C.Sp_SprocGroups != _data.O.Sp_SprocGroups)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsBuildDataAccessMethod":
                        if (_data.C.Sp_IsBuildDataAccessMethod != _data.O.Sp_IsBuildDataAccessMethod)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_BuildListClass":
                        if (_data.C.Sp_BuildListClass != _data.O.Sp_BuildListClass)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsFetchEntity":
                        if (_data.C.Sp_IsFetchEntity != _data.O.Sp_IsFetchEntity)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsStaticList":
                        if (_data.C.Sp_IsStaticList != _data.O.Sp_IsStaticList)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsTypeComboItem":
                        if (_data.C.Sp_IsTypeComboItem != _data.O.Sp_IsTypeComboItem)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsCreateWireType":
                        if (_data.C.Sp_IsCreateWireType != _data.O.Sp_IsCreateWireType)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_WireTypeName":
                        if (_data.C.Sp_WireTypeName != _data.O.Sp_WireTypeName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_MethodName":
                        if (_data.C.Sp_MethodName != _data.O.Sp_MethodName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_AssocEntity_Id":
                        if (_data.C.Sp_AssocEntity_Id != _data.O.Sp_AssocEntity_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_AssocEntityNames":
                        if (_data.C.Sp_AssocEntityNames != _data.O.Sp_AssocEntityNames)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_SpRsTp_Id":
                        if (_data.C.Sp_SpRsTp_Id != _data.O.Sp_SpRsTp_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_SpRtTp_Id":
                        if (_data.C.Sp_SpRtTp_Id != _data.O.Sp_SpRtTp_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsInputParamsAsObjectArray":
                        if (_data.C.Sp_IsInputParamsAsObjectArray != _data.O.Sp_IsInputParamsAsObjectArray)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsParamsAutoRefresh":
                        if (_data.C.Sp_IsParamsAutoRefresh != _data.O.Sp_IsParamsAutoRefresh)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_HasNewParams":
                        if (_data.C.Sp_HasNewParams != _data.O.Sp_HasNewParams)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsParamsValid":
                        if (_data.C.Sp_IsParamsValid != _data.O.Sp_IsParamsValid)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsParamValueSetValid":
                        if (_data.C.Sp_IsParamValueSetValid != _data.O.Sp_IsParamValueSetValid)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_HasNewColumns":
                        if (_data.C.Sp_HasNewColumns != _data.O.Sp_HasNewColumns)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsColumnsValid":
                        if (_data.C.Sp_IsColumnsValid != _data.O.Sp_IsColumnsValid)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsValid":
                        if (_data.C.Sp_IsValid != _data.O.Sp_IsValid)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_Notes":
                        if (_data.C.Sp_Notes != _data.O.Sp_Notes)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsActiveRow":
                        if (_data.C.Sp_IsActiveRow != _data.O.Sp_IsActiveRow)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_IsDeleted":
                        if (_data.C.Sp_IsDeleted != _data.O.Sp_IsDeleted)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Sp_CreatedUserId":
                        if (_data.C.Sp_CreatedUserId != _data.O.Sp_CreatedUserId)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "UserName":
                        if (_data.C.UserName != _data.O.UserName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    default:
                        throw new Exception("WcStoredProc_Bll.IsPropertyDirty found no matching propery name for " + propertyName);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", IfxTraceCategory.Leave);
            }
        }

        public void SetDateFromString(string propName, string value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", new ValuePair[] {new ValuePair("value", value), new ValuePair("propName", propName) }, IfxTraceCategory.Enter);
                DateTime? dt = null;
                if (BLLHelper.IsDate(value) == true)
                {
                    dt = DateTime.Parse(value);
                }
                switch (propName)
                {
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", IfxTraceCategory.Leave);
            }
        }

        public object GetPropertyValueByKey(string key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", new ValuePair[] {new ValuePair("key", key) }, IfxTraceCategory.Enter);
                switch (key)
                {
                    case "SprocInDb":
                        return SprocInDb;

                    case "Sp_Name":
                        return Sp_Name;

                    case "Sp_IsCodeGen":
                        return Sp_IsCodeGen;

                    case "Sp_IsReadyCodeGen":
                        return Sp_IsReadyCodeGen;

                    case "Sp_IsCodeGenComplete":
                        return Sp_IsCodeGenComplete;

                    case "Sp_IsTagForCodeGen":
                        return Sp_IsTagForCodeGen;

                    case "Sp_IsTagForOther":
                        return Sp_IsTagForOther;

                    case "Sp_SprocGroups":
                        return Sp_SprocGroups;

                    case "Sp_IsBuildDataAccessMethod":
                        return Sp_IsBuildDataAccessMethod;

                    case "Sp_BuildListClass":
                        return Sp_BuildListClass;

                    case "Sp_IsFetchEntity":
                        return Sp_IsFetchEntity;

                    case "Sp_IsStaticList":
                        return Sp_IsStaticList;

                    case "Sp_IsTypeComboItem":
                        return Sp_IsTypeComboItem;

                    case "Sp_IsCreateWireType":
                        return Sp_IsCreateWireType;

                    case "Sp_WireTypeName":
                        return Sp_WireTypeName;

                    case "Sp_MethodName":
                        return Sp_MethodName;

                    case "Sp_AssocEntity_Id":
                        return Sp_AssocEntity_Id;

                    case "Sp_AssocEntityNames":
                        return Sp_AssocEntityNames;

                    case "Sp_SpRsTp_Id":
                        return Sp_SpRsTp_Id;

                    case "Sp_SpRtTp_Id":
                        return Sp_SpRtTp_Id;

                    case "Sp_IsInputParamsAsObjectArray":
                        return Sp_IsInputParamsAsObjectArray;

                    case "Sp_IsParamsAutoRefresh":
                        return Sp_IsParamsAutoRefresh;

                    case "Sp_HasNewParams":
                        return Sp_HasNewParams;

                    case "Sp_IsParamsValid":
                        return Sp_IsParamsValid;

                    case "Sp_IsParamValueSetValid":
                        return Sp_IsParamValueSetValid;

                    case "Sp_HasNewColumns":
                        return Sp_HasNewColumns;

                    case "Sp_IsColumnsValid":
                        return Sp_IsColumnsValid;

                    case "Sp_IsValid":
                        return Sp_IsValid;

                    case "Sp_Notes":
                        return Sp_Notes;

                    case "Sp_IsActiveRow":
                        return Sp_IsActiveRow;

                    case "UserName":
                        return UserName;

                    case "Sp_LastModifiedDate":
                        return Sp_LastModifiedDate;

                    default:
                        return GetPropertyValueByKeyCustomCode(key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", IfxTraceCategory.Leave);
            }
        }

        public string GetPropertyFormattedStringValueByKey(string key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", new ValuePair[] {new ValuePair("key", key) }, IfxTraceCategory.Enter);
                switch (key)
                {
                    case "EntityId":
                        return Sp_Id.ToString();

                    case "SprocInDb":
                        return SprocInDb;

                    case "Sp_Name":
                        return Sp_Name;

                    case "Sp_IsCodeGen":
                        return Sp_IsCodeGen.ToString();

                    case "Sp_IsReadyCodeGen":
                        return Sp_IsReadyCodeGen.ToString();

                    case "Sp_IsCodeGenComplete":
                        return Sp_IsCodeGenComplete.ToString();

                    case "Sp_IsTagForCodeGen":
                        return Sp_IsTagForCodeGen.ToString();

                    case "Sp_IsTagForOther":
                        return Sp_IsTagForOther.ToString();

                    case "Sp_SprocGroups":
                        return Sp_SprocGroups;

                    case "Sp_IsBuildDataAccessMethod":
                        return Sp_IsBuildDataAccessMethod.ToString();

                    case "Sp_BuildListClass":
                        return Sp_BuildListClass.ToString();

                    case "Sp_IsFetchEntity":
                        return Sp_IsFetchEntity.ToString();

                    case "Sp_IsStaticList":
                        return Sp_IsStaticList.ToString();

                    case "Sp_IsTypeComboItem":
                        if (Sp_IsTypeComboItem == null)
                        {
                            return null;
                        }
                        else
                        {
                            return Sp_IsTypeComboItem.ToString();
                        }

                    case "Sp_IsCreateWireType":
                        return Sp_IsCreateWireType.ToString();

                    case "Sp_WireTypeName":
                        return Sp_WireTypeName;

                    case "Sp_MethodName":
                        return Sp_MethodName;

                    case "Sp_AssocEntity_Id":
                        return Sp_AssocEntity_Id_TextField;

                    case "Sp_AssocEntityNames":
                        return Sp_AssocEntityNames;

                    case "Sp_SpRsTp_Id":
                        return Sp_SpRsTp_Id_TextField;

                    case "Sp_SpRtTp_Id":
                        return Sp_SpRtTp_Id_TextField;

                    case "Sp_IsInputParamsAsObjectArray":
                        return Sp_IsInputParamsAsObjectArray.ToString();

                    case "Sp_IsParamsAutoRefresh":
                        return Sp_IsParamsAutoRefresh.ToString();

                    case "Sp_HasNewParams":
                        return Sp_HasNewParams.ToString();

                    case "Sp_IsParamsValid":
                        return Sp_IsParamsValid.ToString();

                    case "Sp_IsParamValueSetValid":
                        return Sp_IsParamValueSetValid.ToString();

                    case "Sp_HasNewColumns":
                        return Sp_HasNewColumns.ToString();

                    case "Sp_IsColumnsValid":
                        return Sp_IsColumnsValid.ToString();

                    case "Sp_IsValid":
                        return Sp_IsValid.ToString();

                    case "Sp_Notes":
                        return Sp_Notes;

                    case "Sp_IsActiveRow":
                        return Sp_IsActiveRow.ToString();

                    case "UserName":
                        return UserName;

                    case "Sp_LastModifiedDate":
                        if (Sp_LastModifiedDate == null)
                        {
                            return null;
                        }
                        else
                        {
                            return Sp_LastModifiedDate.ToString();
                        }

                    default:
                        return GetPropertyFormattedStringValueByKeyCustomCode(key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", IfxTraceCategory.Leave);
            }
        }

        
        #region General Properties

        /// <summary>
        ///     Returns a list of current BrokenRules for this entity as a list of <see cref="TypeServices.vRuleItem">vRuleItem</see> types.
        /// </summary>
        [Browsable(false)]
        public List<vRuleItem> GetBrokenRuleListForEntity()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", IfxTraceCategory.Enter);
                return BrokenRuleManagerProperty.GetBrokenRuleListForEntity();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", IfxTraceCategory.Leave);
            }
        }

        #endregion General Properties        

		#endregion General Methods and Properties


		#region Events


        /// <summary>
        ///     Raises the <see cref="CrudFailed">CrudFailed</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnCrudFailed(object sender, CrudFailedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Enter);
            CrudFailedEventHandler handler = CrudFailed;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Leave);
        }

        // THIS IS NOW OBSOLETE
        /// <summary>
        ///     Calls the <see cref="OnAsyncSaveComplete">OnAsyncSaveComplete</see> to raise the <see cref="AsyncSaveComplete">AsyncSaveComplete</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventAsyncSaveComplete(DataOperationResult result, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveComplete", IfxTraceCategory.Enter);
            AsyncSaveCompleteArgs e = new AsyncSaveCompleteArgs(result, failedReasonText);
            OnAsyncSaveComplete(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveComplete", IfxTraceCategory.Leave);
        }

        // THIS IS NOW OBSOLETE
        /// <summary>
        ///     Raises the <see cref="AsyncSaveComplete">AsyncSaveComplete</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnAsyncSaveComplete(object sender, AsyncSaveCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveComplete", IfxTraceCategory.Enter);
            AsyncSaveCompleteEventHandler handler = AsyncSaveComplete;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveComplete", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Calls the <see cref="OnAsyncSaveWithResponseComplete">OnAsyncSaveWithResponseComplete</see> to raise the <see cref="AsyncSaveWithResponseComplete">AsyncSaveWithResponseComplete</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventAsyncSaveWithResponseComplete(DataServiceInsertUpdateResponse response, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
            AsyncSaveWithResponseCompleteArgs e = new AsyncSaveWithResponseCompleteArgs(response, failedReasonText);
            OnAsyncSaveWithResponseComplete(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Raises the <see cref="AsyncSaveWithResponseComplete">AsyncSaveWithResponseComplete</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnAsyncSaveWithResponseComplete(object sender, AsyncSaveWithResponseCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
            AsyncSaveWithResponseCompleteEventHandler handler = AsyncSaveWithResponseComplete;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
        }










        void RaiseEventEntityRowReceived()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventEntityRowReceived", IfxTraceCategory.Enter);
            EntityRowReceivedEventHandler handler = EntityRowReceived;
            if (handler != null)
            {
                handler(this, new EntityRowReceivedArgs());
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventEntityRowReceived", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Calls the <see cref="OnCrudFailed">OnCrudFailed</see> to raise the <see cref="CrudFailed">CrudFailed</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventCrudFailed(int failureCode, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCrudFailed", IfxTraceCategory.Enter);
            CrudFailedArgs e = new CrudFailedArgs(failureCode, failedReasonText);
            OnCrudFailed(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCrudFailed", IfxTraceCategory.Leave);
        }


        /// <summary>
        ///     Calls the <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see>
        ///     method to raise the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event passing in a
        ///     reference of this business object for the ActiveBusinessObject parameter. As it
        ///     bubbles up to an Entity Properties control, that control will pass in a reference
        ///     of itself for the ActivePropertiesControl parameter. As it bubbles up to the Entity
        ///     Manager control, that control will pass in a reference of itself for the
        ///     ActiveEntityControl parameter. It should continue to bubble up to the top level
        ///     control. This notifies all controls along the about which controls are active and
        ///     the current state so they can always be configures accordingly. Now that the top
        ///     level control (perhaps the main application window) has a reference to these 3
        ///     important objects, it can easily communicate with then as the use interacts with
        ///     the application.
        /// </summary>        
        void RaiseEventCurrentEntityStateChanged(EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCurrentEntityStateChanged", new ValuePair[] {new ValuePair("state", state) }, IfxTraceCategory.Enter);
            CurrentEntityStateArgs e = new CurrentEntityStateArgs(state, null, null, this);
            OnCurrentEntityStateChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCurrentEntityStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     Raises the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see>
        ///     event passing in a reference of this business object for the ActiveBusinessObject
        ///     parameter. As it bubbles up to an Entity Properties control, that control will pass
        ///     in a reference of itself for the ActivePropertiesControl parameter. As it bubbles
        ///     up to the Entity Manager control, that control will pass in a reference of itself
        ///     for the ActiveEntityControl parameter. It should continue to bubble up to the top
        ///     level control. This notifies all controls along the about which controls are active
        ///     and the current state so they can always be configures accordingly. Now that the
        ///     top level control (perhaps the main application window) has a reference to these 3
        ///     important objects, it can easily communicate with then as the use interacts with
        ///     the application.
        /// </summary>
        void OnCurrentEntityStateChanged(object sender, CurrentEntityStateArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", new ValuePair[] {new ValuePair("e.State", e.State) }, IfxTraceCategory.Enter);
            CurrentEntityStateEventHandler handler = CurrentEntityStateChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     This method is obsolete and is replaced with <see cref="RaiseEventControlValidStateChanged">RaiseEventControlValidStateChanged</see> which
        ///     is called in the validation section of this class in the partial class
        ///     WcStoredProc.bll.vld.cs code file.
        /// </summary>
        void RaiseEventBrokenRuleChanged(string rule)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventBrokenRuleChanged", new ValuePair[] {new ValuePair("rule", rule) }, IfxTraceCategory.Enter);
            BrokenRuleArgs e = new BrokenRuleArgs(rule);
            OnBrokenRuleChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventBrokenRuleChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>This method is mostly obsolete, but remains because it’s an easy way to be
        ///     notified of the current broken rule.</para>
        /// 	<para>
        ///         This method is hooked to the <see cref="BrokenRuleChanged">BrokenRuleChanged</see> event which is initiated by
        ///         BrokenRuleManager and bubbles up to the business object and then to this
        ///         control (ucWcStoredProcProps) (or in some cases a ucEntityList control when it’s in
        ///         Read/Write mode).
        ///     </para>
        /// 	<para><br/>
        ///     For information on how Broken Rules are currently being managed, see (in the order
        ///     of execution):</para>
        /// 	<list type="bullet">
        /// 		<item>In the business object’s data property – FieldName_Validate();</item>
        /// 		<item>
        ///             In the FieldName_Validate method this line executes:<br/>
        ///             _brokenRuleManager.<see cref="TypeServices.BrokenRuleManager.AddBrokenRuleForProperty">AddBrokenRuleForProperty</see>("FieldName",
        ///             BROKENRULE_ FieldName _SomeRuleName);
        ///         </item>
        /// 		<item>
        /// 			<see cref="TypeServices.BrokenRuleManager.SetEntityValidState">BrokenRuleManager.SetEntityValidState()</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
       private void OnBrokenRuleChanged(object sender, BrokenRuleArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Enter);
            BrokenRuleEventHandler handler = BrokenRuleChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// Notifies an event handler that a property value as change. Currently not being
        /// uses.
        /// </summary>
        private void OnPropertyValueChanged(object sender, PropertyValueChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnPropertyValueChanged", new ValuePair[] {new ValuePair("e.PropertyName", e.PropertyName), new ValuePair("e.IsValid", e.IsValid), new ValuePair("e.BrokenRules", e.BrokenRules) }, IfxTraceCategory.Enter);
            PropertyValueChangedEventHandler handler = PropertyValueChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnPropertyValueChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     Replaces the RaiseEventBrokenRuleChanged method and used in the validation section
        ///     of this class in the partial class WcStoredProc.bll.vld.cs code file. Validation code will
        ///     pass in property name and an isValid flag. This will call the <see cref="OnControlValidStateChanged">OnControlValidStateChanged</see> method which will
        ///     raise the <see cref="ControlValidStateChanged">ControlValidStateChanged</see> event
        ///     up to the parent. The parent will then take the appropriate actions such as setting
        ///     the control’s valid/not valid appearance.
        /// </summary>
        void RaiseEventControlValidStateChanged(string propertyName, bool isValid, bool isDirty)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventControlValidStateChanged", new ValuePair[] {new ValuePair("isValid", isValid) }, IfxTraceCategory.Enter);
            ControlValidStateChangedArgs e = new ControlValidStateChangedArgs(propertyName, isValid, isDirty);
            OnControlValidStateChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventControlValidStateChanged", IfxTraceCategory.Leave);
        }       

        /// <summary>
        /// Raise the ControlValidStateChanged event up to the parent. The parent will then
        /// take the appropriate actions such as setting the control’s valid/not valid
        /// appearance.
        /// </summary>
        private void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", new ValuePair[] {new ValuePair("e.IsValid", e.IsValid) }, IfxTraceCategory.Enter);
            ControlValidStateChangedEventHandler handler = ControlValidStateChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcProps.OnRestrictedTextLengthChanged">ucWcStoredProcProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcProps.SetRestrictedStringLengthText">ucWcStoredProcProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void RaiseEventRestrictedTextLengthChanged()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Enter);
            RestrictedTextLengthChangedArgs e = new RestrictedTextLengthChangedArgs();
            RestrictedTextLengthChangedEventHandler handler = RestrictedTextLengthChanged;
            if (handler != null)
            {
                handler(this, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<param name="oldVal">The original property value.</param>
        /// 	<param name="newVal">The new property value.</param>        
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcProps.OnRestrictedTextLengthChanged">ucWcStoredProcProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcStoredProcProps.SetRestrictedStringLengthText">ucWcStoredProcProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void RaiseEventRestrictedTextLengthChanged(string oldVal, string newVal)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Enter);
            bool lenChanged = false;
            if (oldVal == null && newVal != null)
            {
                if (newVal.Trim().Length > 0)
                {
                    lenChanged = true;
                }
            }
            else if (newVal == null && oldVal != null)
            {
                if (oldVal.Trim().Length > 0)
                {
                    lenChanged = true;
                }
            }
            else if (oldVal != null && newVal != null)
            {
                if (oldVal.Trim().Length != newVal.Trim().Length)
                {
                    lenChanged = true;
                }
            }
            if (lenChanged == true)
            {
                RestrictedTextLengthChangedArgs e = new RestrictedTextLengthChangedArgs();
                RestrictedTextLengthChangedEventHandler handler = RestrictedTextLengthChanged;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Leave);
        }        


		#endregion Events


		#region Flag Props


        /// <summary>
        ///     The name of the (text type – TextBox, TextBlock, etc.) control in the UI that
        ///     currently has the focus. This is used by the <see cref="ActiveRestrictedStringPropertyLength">ActiveRestrictedStringPropertyLength</see>
        ///     method to which returns the remaining available text length for the text control
        ///     with that name. It was decided to persist the name of the UI control in this
        ///     business object property rather than in the UI because it seemed to make sense to
        ///     have all of this logic centralized, and also because it simplifies things.
        /// </summary>
        [Browsable(false)]
        public string ActiveRestrictedStringProperty
        {
            get { return _activeRestrictedStringProperty; }
            set 
            { 
                _activeRestrictedStringProperty = value;
            }
        }

        /// <summary>
        ///     Returns the remaining available text length for the text control named by
        ///     <see cref="ActiveRestrictedStringProperty">ActiveRestrictedStringProperty</see>.
        /// </summary>
        [Browsable(false)]
        public int ActiveRestrictedStringPropertyLength
        {
            get
            {
                switch (_activeRestrictedStringProperty)
                {
                    case "Sp_Name":
                        if (_data.C.Sp_Name == null)
                        {
                            return STRINGSIZE_Sp_Name;
                        }
                        else
                        {
                            return (STRINGSIZE_Sp_Name - _data.C.Sp_Name.Length);
                        }
                        break;
                    
                    case "Sp_SprocGroups":
                        if (_data.C.Sp_SprocGroups == null)
                        {
                            return STRINGSIZE_Sp_SprocGroups;
                        }
                        else
                        {
                            return (STRINGSIZE_Sp_SprocGroups - _data.C.Sp_SprocGroups.Length);
                        }
                        break;
                    
                    case "Sp_WireTypeName":
                        if (_data.C.Sp_WireTypeName == null)
                        {
                            return STRINGSIZE_Sp_WireTypeName;
                        }
                        else
                        {
                            return (STRINGSIZE_Sp_WireTypeName - _data.C.Sp_WireTypeName.Length);
                        }
                        break;
                    
                    case "Sp_MethodName":
                        if (_data.C.Sp_MethodName == null)
                        {
                            return STRINGSIZE_Sp_MethodName;
                        }
                        else
                        {
                            return (STRINGSIZE_Sp_MethodName - _data.C.Sp_MethodName.Length);
                        }
                        break;
                    
                    case "Sp_Notes":
                        if (_data.C.Sp_Notes == null)
                        {
                            return STRINGSIZE_Sp_Notes;
                        }
                        else
                        {
                            return (STRINGSIZE_Sp_Notes - _data.C.Sp_Notes.Length);
                        }
                        break;
                    }
                return 0;
            }
        }

 

		#endregion Flag Props


		#region Data Props


        /// <summary>
        ///     Called by data field properties in their getter block. This method will determine
        ///     if he <see cref="TypeServices.EntityState">state</see> has changed by the getter
        ///     being called. If it has changed, the <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see>
        ///     method will be called to notify the object using this business object.
        /// </summary>
       private void CheckEntityState(EntityStateSwitch es)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", new ValuePair[] {new ValuePair("_data.S.Switch", _data.S.Switch), new ValuePair("es", es) }, IfxTraceCategory.Enter);
                if (es != _data.S.Switch)
                {
                    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// This property does nothing and is used as a stub for a 'Rich Data Grid Column' binding to this entity
        /// </summary>
        public string RichGrid
        {
            get { return ""; }
        }



        public string DataRowName
        {
            get { return Sp_Name; }
        }




        private IBusinessObjectV2 _parentBusinessObject = null;

        public IBusinessObjectV2 ParentBusinessObject
        {
            get { return _parentBusinessObject; }
            set { _parentBusinessObject = value; }
        }

       public Guid? GuidPrimaryKey()
       {
           return _data.C.Sp_Id; 
       }

       public long? LongPrimaryKey()
       {
           throw new NotImplementedException("WcStoredProc_Bll LongPrimaryKey() Not Implemented"); 
       }

       public int? IntPrimaryKey()
       {
           throw new NotImplementedException("WcStoredProc_Bll LongPrimaryKey() Not Implemented"); 
       }

       public short? ShortPrimaryKey()
       {
           throw new NotImplementedException("WcStoredProc_Bll LongPrimaryKey() Not Implemented"); 
       }

       public byte? BytePrimaryKey()
       {
           throw new NotImplementedException("WcStoredProc_Bll LongPrimaryKey() Not Implemented"); 
       }

       public object ObjectPrimaryKey()
       {
           return _data.C.Sp_Id;
       }


        /// <summary>
        /// This is the Standing Foreign Key property. This value remains constant when
        /// calling the new method where all data fields are cleared and set to their ‘new’ default
        /// values. This allows creating new entities for the same parent. When an entity is
        /// fetched from the data store and used as the current entity in this business object, the
        /// Standing Foreign Key value is reset using the value from the fetched entity.
        /// </summary>
        public Guid StandingFK
        {
            get { return _StandingFK; }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    _StandingFK = value;
                    _data.C.Sp_ApVrsn_Id_noevents = _StandingFK;
                    _data.O.Sp_ApVrsn_Id_noevents = _StandingFK;
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", IfxTraceCategory.Leave);
                }
            }
        }

       /// <summary>
       /// 	<para>The Primary Key for WcStoredProc</para>
       /// </summary>
        public Guid Sp_Id
        {
            get
            {
                return _data.C.Sp_Id;
            }
        }

		
        public Guid? Sp_ApVrsn_Id
        {
            get
            {
                return _data.C.Sp_ApVrsn_Id;
            }
        }


        public String SprocInDb
        {
            get
            {
                //return _data.C.SprocInDb;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SprocInDb Setter", IfxTraceCategory.Enter);

                Notify("SprocInDb");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SprocInDb Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SprocInDb Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public String Sp_Name
        {
            get
            {
                return _data.C.Sp_Name;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Name - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_Name == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Sp_Name == null || _data.C.Sp_Name.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Sp_Name == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Sp_Name = null;
                    }
                    else
                    {
                        _data.C.Sp_Name = value.Trim();
                    }
                    //else if ((_data.C.Sp_Name == null || _data.C.Sp_Name.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Sp_Name == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Sp_Name == null || _data.C.Sp_Name.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Sp_Name = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Sp_Name = value;
                    //}
                    CustomPropertySetterCode("Sp_Name");
                    Sp_Name_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Sp_Name, _data.C.Sp_Name);
                        
					Notify("Sp_Name");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Name - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Name - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsCodeGen
        {
            get
            {
                return _data.C.Sp_IsCodeGen;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCodeGen - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsCodeGen == value) { return; }
                    _data.C.Sp_IsCodeGen = value;
                    CustomPropertySetterCode("Sp_IsCodeGen");
                    Sp_IsCodeGen_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsCodeGen");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCodeGen - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCodeGen - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsReadyCodeGen
        {
            get
            {
                return _data.C.Sp_IsReadyCodeGen;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsReadyCodeGen - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsReadyCodeGen == value) { return; }
                    _data.C.Sp_IsReadyCodeGen = value;
                    CustomPropertySetterCode("Sp_IsReadyCodeGen");
                    Sp_IsReadyCodeGen_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsReadyCodeGen");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsReadyCodeGen - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsReadyCodeGen - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsCodeGenComplete
        {
            get
            {
                return _data.C.Sp_IsCodeGenComplete;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCodeGenComplete - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsCodeGenComplete == value) { return; }
                    _data.C.Sp_IsCodeGenComplete = value;
                    CustomPropertySetterCode("Sp_IsCodeGenComplete");
                    Sp_IsCodeGenComplete_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsCodeGenComplete");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCodeGenComplete - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCodeGenComplete - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsTagForCodeGen
        {
            get
            {
                return _data.C.Sp_IsTagForCodeGen;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTagForCodeGen - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsTagForCodeGen == value) { return; }
                    _data.C.Sp_IsTagForCodeGen = value;
                    CustomPropertySetterCode("Sp_IsTagForCodeGen");
                    Sp_IsTagForCodeGen_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsTagForCodeGen");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTagForCodeGen - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTagForCodeGen - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsTagForOther
        {
            get
            {
                return _data.C.Sp_IsTagForOther;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTagForOther - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsTagForOther == value) { return; }
                    _data.C.Sp_IsTagForOther = value;
                    CustomPropertySetterCode("Sp_IsTagForOther");
                    Sp_IsTagForOther_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsTagForOther");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTagForOther - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTagForOther - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Sp_SprocGroups
        {
            get
            {
                return _data.C.Sp_SprocGroups;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SprocGroups - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_SprocGroups == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Sp_SprocGroups == null || _data.C.Sp_SprocGroups.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Sp_SprocGroups == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Sp_SprocGroups = null;
                    }
                    else
                    {
                        _data.C.Sp_SprocGroups = value.Trim();
                    }
                    //else if ((_data.C.Sp_SprocGroups == null || _data.C.Sp_SprocGroups.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Sp_SprocGroups == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Sp_SprocGroups == null || _data.C.Sp_SprocGroups.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Sp_SprocGroups = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Sp_SprocGroups = value;
                    //}
                    CustomPropertySetterCode("Sp_SprocGroups");
                    Sp_SprocGroups_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Sp_SprocGroups, _data.C.Sp_SprocGroups);
                        
					Notify("Sp_SprocGroups");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SprocGroups - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SprocGroups - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsBuildDataAccessMethod
        {
            get
            {
                return _data.C.Sp_IsBuildDataAccessMethod;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsBuildDataAccessMethod - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsBuildDataAccessMethod == value) { return; }
                    _data.C.Sp_IsBuildDataAccessMethod = value;
                    CustomPropertySetterCode("Sp_IsBuildDataAccessMethod");
                    Sp_IsBuildDataAccessMethod_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsBuildDataAccessMethod");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsBuildDataAccessMethod - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsBuildDataAccessMethod - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_BuildListClass
        {
            get
            {
                return _data.C.Sp_BuildListClass;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_BuildListClass - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_BuildListClass == value) { return; }
                    _data.C.Sp_BuildListClass = value;
                    CustomPropertySetterCode("Sp_BuildListClass");
                    Sp_BuildListClass_Validate();
                    CheckEntityState(es);
                    Notify("Sp_BuildListClass");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_BuildListClass - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_BuildListClass - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsFetchEntity
        {
            get
            {
                return _data.C.Sp_IsFetchEntity;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsFetchEntity - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsFetchEntity == value) { return; }
                    _data.C.Sp_IsFetchEntity = value;
                    CustomPropertySetterCode("Sp_IsFetchEntity");
                    Sp_IsFetchEntity_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsFetchEntity");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsFetchEntity - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsFetchEntity - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsStaticList
        {
            get
            {
                return _data.C.Sp_IsStaticList;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsStaticList - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsStaticList == value) { return; }
                    _data.C.Sp_IsStaticList = value;
                    CustomPropertySetterCode("Sp_IsStaticList");
                    Sp_IsStaticList_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsStaticList");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsStaticList - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsStaticList - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean? Sp_IsTypeComboItem
        {
            get
            {
                return _data.C.Sp_IsTypeComboItem;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTypeComboItem - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsTypeComboItem == value) { return; }
                    _data.C.Sp_IsTypeComboItem = value;
                    CustomPropertySetterCode("Sp_IsTypeComboItem");
                    Sp_IsTypeComboItem_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsTypeComboItem");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTypeComboItem - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTypeComboItem - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsCreateWireType
        {
            get
            {
                return _data.C.Sp_IsCreateWireType;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCreateWireType - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsCreateWireType == value) { return; }
                    _data.C.Sp_IsCreateWireType = value;
                    CustomPropertySetterCode("Sp_IsCreateWireType");
                    Sp_IsCreateWireType_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsCreateWireType");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCreateWireType - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCreateWireType - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Sp_WireTypeName
        {
            get
            {
                return _data.C.Sp_WireTypeName;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_WireTypeName - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_WireTypeName == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Sp_WireTypeName == null || _data.C.Sp_WireTypeName.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Sp_WireTypeName == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Sp_WireTypeName = null;
                    }
                    else
                    {
                        _data.C.Sp_WireTypeName = value.Trim();
                    }
                    //else if ((_data.C.Sp_WireTypeName == null || _data.C.Sp_WireTypeName.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Sp_WireTypeName == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Sp_WireTypeName == null || _data.C.Sp_WireTypeName.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Sp_WireTypeName = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Sp_WireTypeName = value;
                    //}
                    CustomPropertySetterCode("Sp_WireTypeName");
                    Sp_WireTypeName_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Sp_WireTypeName, _data.C.Sp_WireTypeName);
                        
					Notify("Sp_WireTypeName");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_WireTypeName - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_WireTypeName - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Sp_MethodName
        {
            get
            {
                return _data.C.Sp_MethodName;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_MethodName - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_MethodName == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Sp_MethodName == null || _data.C.Sp_MethodName.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Sp_MethodName == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Sp_MethodName = null;
                    }
                    else
                    {
                        _data.C.Sp_MethodName = value.Trim();
                    }
                    //else if ((_data.C.Sp_MethodName == null || _data.C.Sp_MethodName.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Sp_MethodName == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Sp_MethodName == null || _data.C.Sp_MethodName.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Sp_MethodName = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Sp_MethodName = value;
                    //}
                    CustomPropertySetterCode("Sp_MethodName");
                    Sp_MethodName_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Sp_MethodName, _data.C.Sp_MethodName);
                        
					Notify("Sp_MethodName");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_MethodName - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_MethodName - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public String Sp_AssocEntity_Id_TextField
        {
            get
            {
                return _data.C.Sp_AssocEntity_Id_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_AssocEntity_Id Setter", IfxTraceCategory.Enter);
                    if (_data.C.Sp_AssocEntity_Id_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.Sp_AssocEntity_Id_TextField == null || _data.C.Sp_AssocEntity_Id_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.Sp_AssocEntity_Id_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.Sp_AssocEntity_Id_TextField = value;
                    }
                    CustomPropertySetterCode("Sp_AssocEntity_Id_TextField");
                    Notify("Sp_AssocEntity_Id_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_AssocEntity_Id Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_AssocEntity_Id Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Guid? Sp_AssocEntity_Id
        {
            get
            {
                return _data.C.Sp_AssocEntity_Id;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_AssocEntity_Id - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_AssocEntity_Id == value) { return; }
                    _data.C.Sp_AssocEntity_Id = value;
                    CustomPropertySetterCode("Sp_AssocEntity_Id");
                    Sp_AssocEntity_Id_Validate();
                    CheckEntityState(es);
                    Notify("Sp_AssocEntity_Id");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty.FindItemById(_data.C.Sp_AssocEntity_Id);
                    if (obj == null)
                    {
                        Sp_AssocEntity_Id_TextField = "";
                    }
                    else
                    {
                        Sp_AssocEntity_Id_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_AssocEntity_Id - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_AssocEntity_Id - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Sp_AssocEntityNames
        {
            get
            {
                return _data.C.Sp_AssocEntityNames;
            }
        }


        public String Sp_SpRsTp_Id_TextField
        {
            get
            {
                return _data.C.Sp_SpRsTp_Id_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id Setter", IfxTraceCategory.Enter);
                    if (_data.C.Sp_SpRsTp_Id_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.Sp_SpRsTp_Id_TextField == null || _data.C.Sp_SpRsTp_Id_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.Sp_SpRsTp_Id_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.Sp_SpRsTp_Id_TextField = value;
                    }
                    CustomPropertySetterCode("Sp_SpRsTp_Id_TextField");
                    Notify("Sp_SpRsTp_Id_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Int32? Sp_SpRsTp_Id
        {
            get
            {
                return _data.C.Sp_SpRsTp_Id;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_SpRsTp_Id == value) { return; }
                    _data.C.Sp_SpRsTp_Id = value;
                    CustomPropertySetterCode("Sp_SpRsTp_Id");
                    Sp_SpRsTp_Id_Validate();
                    CheckEntityState(es);
                    Notify("Sp_SpRsTp_Id");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = CommonClientData_Bll_staticLists.WcStoredProcResultType_ComboItemList_BindingListProperty.FindItemById(_data.C.Sp_SpRsTp_Id);
                    if (obj == null)
                    {
                        Sp_SpRsTp_Id_TextField = "";
                    }
                    else
                    {
                        Sp_SpRsTp_Id_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public String Sp_SpRtTp_Id_TextField
        {
            get
            {
                return _data.C.Sp_SpRtTp_Id_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id Setter", IfxTraceCategory.Enter);
                    if (_data.C.Sp_SpRtTp_Id_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.Sp_SpRtTp_Id_TextField == null || _data.C.Sp_SpRtTp_Id_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.Sp_SpRtTp_Id_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.Sp_SpRtTp_Id_TextField = value;
                    }
                    CustomPropertySetterCode("Sp_SpRtTp_Id_TextField");
                    Notify("Sp_SpRtTp_Id_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Int32? Sp_SpRtTp_Id
        {
            get
            {
                return _data.C.Sp_SpRtTp_Id;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_SpRtTp_Id == value) { return; }
                    _data.C.Sp_SpRtTp_Id = value;
                    CustomPropertySetterCode("Sp_SpRtTp_Id");
                    Sp_SpRtTp_Id_Validate();
                    CheckEntityState(es);
                    Notify("Sp_SpRtTp_Id");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = CommonClientData_Bll_staticLists.WcStoredProcReturnType_ComboItemList_BindingListProperty.FindItemById(_data.C.Sp_SpRtTp_Id);
                    if (obj == null)
                    {
                        Sp_SpRtTp_Id_TextField = "";
                    }
                    else
                    {
                        Sp_SpRtTp_Id_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsInputParamsAsObjectArray
        {
            get
            {
                return _data.C.Sp_IsInputParamsAsObjectArray;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsInputParamsAsObjectArray - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsInputParamsAsObjectArray == value) { return; }
                    _data.C.Sp_IsInputParamsAsObjectArray = value;
                    CustomPropertySetterCode("Sp_IsInputParamsAsObjectArray");
                    Sp_IsInputParamsAsObjectArray_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsInputParamsAsObjectArray");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsInputParamsAsObjectArray - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsInputParamsAsObjectArray - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsParamsAutoRefresh
        {
            get
            {
                return _data.C.Sp_IsParamsAutoRefresh;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsAutoRefresh - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsParamsAutoRefresh == value) { return; }
                    _data.C.Sp_IsParamsAutoRefresh = value;
                    CustomPropertySetterCode("Sp_IsParamsAutoRefresh");
                    Sp_IsParamsAutoRefresh_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsParamsAutoRefresh");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsAutoRefresh - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsAutoRefresh - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_HasNewParams
        {
            get
            {
                return _data.C.Sp_HasNewParams;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewParams - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_HasNewParams == value) { return; }
                    _data.C.Sp_HasNewParams = value;
                    CustomPropertySetterCode("Sp_HasNewParams");
                    Sp_HasNewParams_Validate();
                    CheckEntityState(es);
                    Notify("Sp_HasNewParams");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewParams - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewParams - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsParamsValid
        {
            get
            {
                return _data.C.Sp_IsParamsValid;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsValid - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsParamsValid == value) { return; }
                    _data.C.Sp_IsParamsValid = value;
                    CustomPropertySetterCode("Sp_IsParamsValid");
                    Sp_IsParamsValid_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsParamsValid");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsValid - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsValid - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsParamValueSetValid
        {
            get
            {
                return _data.C.Sp_IsParamValueSetValid;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamValueSetValid - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsParamValueSetValid == value) { return; }
                    _data.C.Sp_IsParamValueSetValid = value;
                    CustomPropertySetterCode("Sp_IsParamValueSetValid");
                    Sp_IsParamValueSetValid_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsParamValueSetValid");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamValueSetValid - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamValueSetValid - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_HasNewColumns
        {
            get
            {
                return _data.C.Sp_HasNewColumns;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewColumns - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_HasNewColumns == value) { return; }
                    _data.C.Sp_HasNewColumns = value;
                    CustomPropertySetterCode("Sp_HasNewColumns");
                    Sp_HasNewColumns_Validate();
                    CheckEntityState(es);
                    Notify("Sp_HasNewColumns");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewColumns - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewColumns - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsColumnsValid
        {
            get
            {
                return _data.C.Sp_IsColumnsValid;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsColumnsValid - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsColumnsValid == value) { return; }
                    _data.C.Sp_IsColumnsValid = value;
                    CustomPropertySetterCode("Sp_IsColumnsValid");
                    Sp_IsColumnsValid_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsColumnsValid");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsColumnsValid - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsColumnsValid - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsValid
        {
            get
            {
                return _data.C.Sp_IsValid;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsValid - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsValid == value) { return; }
                    _data.C.Sp_IsValid = value;
                    CustomPropertySetterCode("Sp_IsValid");
                    Sp_IsValid_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsValid");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsValid - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsValid - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Sp_Notes
        {
            get
            {
                return _data.C.Sp_Notes;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Notes - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_Notes == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Sp_Notes == null || _data.C.Sp_Notes.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Sp_Notes == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Sp_Notes = null;
                    }
                    else
                    {
                        _data.C.Sp_Notes = value.Trim();
                    }
                    //else if ((_data.C.Sp_Notes == null || _data.C.Sp_Notes.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Sp_Notes == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Sp_Notes == null || _data.C.Sp_Notes.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Sp_Notes = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Sp_Notes = value;
                    //}
                    CustomPropertySetterCode("Sp_Notes");
                    Sp_Notes_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Sp_Notes, _data.C.Sp_Notes);
                        
					Notify("Sp_Notes");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Notes - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Notes - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsActiveRow
        {
            get
            {
                return _data.C.Sp_IsActiveRow;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsActiveRow - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Sp_IsActiveRow == value) { return; }
                    _data.C.Sp_IsActiveRow = value;
                    CustomPropertySetterCode("Sp_IsActiveRow");
                    Sp_IsActiveRow_Validate();
                    CheckEntityState(es);
                    Notify("Sp_IsActiveRow");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsActiveRow - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsActiveRow - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Sp_IsDeleted
        {
            get
            {
                return _data.C.Sp_IsDeleted;
            }
        }

		
        public Guid? Sp_CreatedUserId
        {
            get
            {
                return _data.C.Sp_CreatedUserId;
            }
        }


		/// <summary>
        ///  Use this property when binding in Grids so the Notify method won't prevent you from entering complete dates or the year part of a date.  Notify will refresh the UI cutting off anything after the first diget of the month.
        /// </summary>
        public String Sp_CreatedDate_asString
        {
            get
            {
                if (null == _data.C.Sp_CreatedDate)
                {
                    return "";
                }
                else
                {
                    return _data.C.Sp_CreatedDate.ToString();
                }
            }
        }

		
        public DateTime? Sp_CreatedDate
        {
            get
            {
                return _data.C.Sp_CreatedDate;
            }
        }

		
        public Guid? Sp_UserId
        {
            get
            {
                return _data.C.Sp_UserId;
            }
        }


        public String UserName
        {
            get
            {
                return _data.C.UserName;
            }
        }


		/// <summary>
        ///  Use this property when binding in Grids so the Notify method won't prevent you from entering complete dates or the year part of a date.  Notify will refresh the UI cutting off anything after the first diget of the month.
        /// </summary>
        public String Sp_LastModifiedDate_asString
        {
            get
            {
                if (null == _data.C.Sp_LastModifiedDate)
                {
                    return "";
                }
                else
                {
                    return _data.C.Sp_LastModifiedDate.ToString();
                }
            }
        }

		
        public DateTime? Sp_LastModifiedDate
        {
            get
            {
                return _data.C.Sp_LastModifiedDate;
            }
        }


        public Byte[] Sp_Stamp
        {
            get
            {
                return _data.C.Sp_Stamp;
            }
        }



		#endregion Data Props


		#region Concurrency and Data State


        private bool DeterminConcurrencyCheck(UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", IfxTraceCategory.Enter);
                switch (check)
                {
                    case UseConcurrencyCheck.UseDefaultSetting:
                        return true;
                    case UseConcurrencyCheck.UseConcurrencyCheck:
                        return true;
                    case UseConcurrencyCheck.BypassConcurrencyCheck:
                        return false;
                }
                return false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     This is a reference to the current data (<see cref="EntityWireType.WcStoredProc_Values">WcStoredProc_Values</see>) for this business object
        ///     (WcStoredProc_Bll). The Values Manager (<see cref="EntityWireType.WcStoredProc_ValuesMngr">WcStoredProc_ValuesMngr</see>) has three sets of WcStoredProc
        ///     data used for managing state and concurrency. For more information about the other
        ///     two sets of data, <see cref="Original">Original</see> and <see cref="Concurrency">Concurrency</see>, and how they are used, see the overviews of
        ///     <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Concurrency_Overview.html">
        ///     Concurrency</a> and <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Wire_Object_Overview.html">
        ///     Wire Objects</a>.
        /// </summary>
        /// <seealso cref="Original">Original Property</seealso>
        /// <seealso cref="Concurrency">Concurrency Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcStoredProc_Values Current
        {
            get
            {
                return _data.C;
            }
            set
            {
                //Management Code
                _data.C = value;
            }
        }

        /// <summary>
        ///     This is a reference to the original data (<see cref="EntityWireType.WcStoredProc_Values">WcStoredProc_Values</see>) for this business object
        ///     (WcStoredProc_Bll) which was either data for a ‘new-not dirty’ WcStoredProc or data for a WcStoredProc
        ///     retrieved from the data store. The Values Manager (<see cref="EntityWireType.WcStoredProc_ValuesMngr">WcStoredProc_ValuesMngr</see>) has three sets of WcStoredProc
        ///     data used for managing state and concurrency. For more information about the other
        ///     two sets of data, <see cref="Current">Current</see> and <see cref="Concurrency">Concurrency</see>, and how they are used, see the overview of
        ///     Concurrency and Wire Objects.
        /// </summary>
        /// <seealso cref="Current">Current Property</seealso>
        /// <seealso cref="Concurrency">Concurrency Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcStoredProc_Values Original
        {
            get
            {
                return _data.O;
            }
            set
            {
                //Management Code
                _data.O = value;
            }
        }

        /// <summary>*** Need to review before documenting ***</summary>
        /// <seealso cref="Current">Current Property</seealso>
        /// <seealso cref="Original">Original Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcStoredProc_Values Concurrency
        {
            get
            {
                return _data.X;
            }
            set
            {
                //Management Code
                _data.X = value;
            }
        }

        /// <value>
        ///     This class manages the <see cref="TypeServices.EntityState">EntityStateSwitch</see>
        ///     and is used to describe the current <see cref="TypeServices.EntityState">state</see> of this business object such as IsDirty,
        ///     IsValid and IsNew. The combinations of these six possible values describe the
        ///     entity state and are used for logic in configuring settings in the business object
        ///     as well as the UI.
        /// </value>
        /// <seealso cref="EntityWireType.WcStoredProc_ValuesMngr">WcStoredProc_ValuesMngr Class</seealso>
        [Browsable(false)]
        public override EntityState State
        {
            get
            {
                return _data.S;
            }
            set
            {
                //Management Code
                _data.S = value;
            }
        }

        /// <summary>
        ///     This is the combination of IsDirty, IsValid and IsNew and is managed by the
        ///     <see cref="TypeServices.EntityState">EntityState</see> class. The combinations of
        ///     these six possible values describe the entity <see cref="TypeServices.EntityState">state</see> and are used for logic in configuring
        ///     settings in the business object as well as the UI.
        /// </summary>
        /// <seealso cref="EntityWireType.WcStoredProc_ValuesMngr">WcStoredProc_ValuesMngr Class</seealso>
        public EntityStateSwitch StateSwitch
        {
            get
            {
                return _data.S.Switch;
            }
        }




		#endregion Concurrency and Data State

        [Browsable(false)]
        public override object this[string name]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                //Management Code
                throw new NotImplementedException();
            }
        }


		#region ITraceItem Members


        /// <summary>
        /// 	<para>
        ///         Used in IFX (the instrumentation framework) for tracing. This returns the short
        ///         version of the <see cref="ManagementServices.TraceItemList">TraceItemList</see>. The ‘Short Version’
        ///         returns only the entity’s data fields, <see cref="TypeServices.BrokenRuleManager.GetBrokenRuleListForEntity">BrokenRules</see>
        ///         and <see cref="TypeServices.EntityStateSwitch">EntityStateSwitch</see>.
        ///         However, it also calls the <see cref="GetTraceItemsShortListCustom">GetTraceItemsShortListCustom</see> method in the
        ///         WcStoredProc.bll.cust.cs code file so a developer can customize the list by adding
        ///         additional items to it.
        ///     </para>
        /// 	<para>
        ///         Keep in mind that this is the short versions so don’t get carried away adding
        ///         to many things to it. For a robust and extensive list of items to record in the
        ///         trace, use the <see cref="GetTraceItemsLongList">GetTraceItemsLongList</see>
        ///         and <see cref="GetTraceItemsLongListCustom">GetTraceItemsLongListCustom</see>
        ///         methods.
        ///     </para>
        /// </summary>
        public TraceItemList GetTraceItemsShortList()
        {
            if (null == _traceItems)
            {
                _traceItems = new TraceItemList("WcStoredProc_Bll");
            }
            else
            {
                _traceItems.Clear();
            }
            _traceItems.Add("StateSwitch", StateSwitch.ToString(), TraceDataTypes.String);
            _traceItems.Add("Broken Rules", BrokenRuleManagerProperty.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_Id", _data.C.Sp_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Sp_ApVrsn_Id", _data.C.Sp_ApVrsn_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Sp_Name", _data.C.Sp_Name.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_IsCodeGen", _data.C.Sp_IsCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsReadyCodeGen", _data.C.Sp_IsReadyCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsCodeGenComplete", _data.C.Sp_IsCodeGenComplete.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsTagForCodeGen", _data.C.Sp_IsTagForCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsTagForOther", _data.C.Sp_IsTagForOther.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_SprocGroups", _data.C.Sp_SprocGroups.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_IsBuildDataAccessMethod", _data.C.Sp_IsBuildDataAccessMethod.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_BuildListClass", _data.C.Sp_BuildListClass.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsFetchEntity", _data.C.Sp_IsFetchEntity.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsStaticList", _data.C.Sp_IsStaticList.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsTypeComboItem", _data.C.Sp_IsTypeComboItem.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsCreateWireType", _data.C.Sp_IsCreateWireType.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_WireTypeName", _data.C.Sp_WireTypeName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_MethodName", _data.C.Sp_MethodName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_AssocEntity_Id", _data.C.Sp_AssocEntity_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Sp_AssocEntity_Id_TextField", _data.C.Sp_AssocEntity_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_AssocEntityNames", _data.C.Sp_AssocEntityNames.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_SpRsTp_Id", _data.C.Sp_SpRsTp_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("Sp_SpRsTp_Id_TextField", _data.C.Sp_SpRsTp_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_SpRtTp_Id", _data.C.Sp_SpRtTp_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("Sp_SpRtTp_Id_TextField", _data.C.Sp_SpRtTp_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_IsInputParamsAsObjectArray", _data.C.Sp_IsInputParamsAsObjectArray.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsParamsAutoRefresh", _data.C.Sp_IsParamsAutoRefresh.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_HasNewParams", _data.C.Sp_HasNewParams.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsParamsValid", _data.C.Sp_IsParamsValid.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsParamValueSetValid", _data.C.Sp_IsParamValueSetValid.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_HasNewColumns", _data.C.Sp_HasNewColumns.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsColumnsValid", _data.C.Sp_IsColumnsValid.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsValid", _data.C.Sp_IsValid.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_Notes", _data.C.Sp_Notes.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_IsActiveRow", _data.C.Sp_IsActiveRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsDeleted", _data.C.Sp_IsDeleted.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_CreatedUserId", _data.C.Sp_CreatedUserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Sp_CreatedDate", _data.C.Sp_CreatedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("Sp_UserId", _data.C.Sp_UserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("UserName", _data.C.UserName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_LastModifiedDate", _data.C.Sp_LastModifiedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("Sp_Stamp", _data.C.Sp_Stamp.ToString(), TraceDataTypes.ByteArray);
			GetTraceItemsShortListCustom();
            return _traceItems;
        }

        /// <summary>
        /// 	<para>
        ///         Used in IFX (the instrumentation framework) for tracing. By default, this is
        ///         code-genned to return the same information as <see cref="GetTraceItemsShortList">GetTraceItemsShortList</see>, but is intended for
        ///         developers to add additional informaiton that would be helpful in a trace. This
        ///         method also calls the <see cref="GetTraceItemsLongListCustom">GetTraceItemsLongListCustom</see> method in the
        ///         WcStoredProc.bll.cust.cs code file so a developer can customize the list by adding
        ///         additional items to it.
        ///     </para>
        /// 	<para>
        ///         Remember: <see cref="GetTraceItemsShortList">GetTraceItemsShortList</see> is
        ///         intended for a limited list of items (the basic items) to trace and
        ///         <strong>GetTraceItemsLongList</strong> is intended for a robust and extensive
        ///         list of items to record in the trace.
        ///     </para>
        /// </summary>
        public TraceItemList GetTraceItemsLongList()
        {
            if (null == _traceItems)
            {
                _traceItems = new TraceItemList("WcStoredProc_Bll");
            }
            else
            {
                _traceItems.Clear();
            }
            _traceItems.Add("StateSwitch", StateSwitch.ToString(), TraceDataTypes.String);
            _traceItems.Add("Broken Rules", BrokenRuleManagerProperty.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_Id", _data.C.Sp_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Sp_ApVrsn_Id", _data.C.Sp_ApVrsn_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Sp_Name", _data.C.Sp_Name.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_IsCodeGen", _data.C.Sp_IsCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsReadyCodeGen", _data.C.Sp_IsReadyCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsCodeGenComplete", _data.C.Sp_IsCodeGenComplete.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsTagForCodeGen", _data.C.Sp_IsTagForCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsTagForOther", _data.C.Sp_IsTagForOther.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_SprocGroups", _data.C.Sp_SprocGroups.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_IsBuildDataAccessMethod", _data.C.Sp_IsBuildDataAccessMethod.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_BuildListClass", _data.C.Sp_BuildListClass.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsFetchEntity", _data.C.Sp_IsFetchEntity.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsStaticList", _data.C.Sp_IsStaticList.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsTypeComboItem", _data.C.Sp_IsTypeComboItem.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsCreateWireType", _data.C.Sp_IsCreateWireType.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_WireTypeName", _data.C.Sp_WireTypeName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_MethodName", _data.C.Sp_MethodName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_AssocEntity_Id", _data.C.Sp_AssocEntity_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Sp_AssocEntity_Id_TextField", _data.C.Sp_AssocEntity_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_AssocEntityNames", _data.C.Sp_AssocEntityNames.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_SpRsTp_Id", _data.C.Sp_SpRsTp_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("Sp_SpRsTp_Id_TextField", _data.C.Sp_SpRsTp_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_SpRtTp_Id", _data.C.Sp_SpRtTp_Id.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("Sp_SpRtTp_Id_TextField", _data.C.Sp_SpRtTp_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_IsInputParamsAsObjectArray", _data.C.Sp_IsInputParamsAsObjectArray.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsParamsAutoRefresh", _data.C.Sp_IsParamsAutoRefresh.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_HasNewParams", _data.C.Sp_HasNewParams.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsParamsValid", _data.C.Sp_IsParamsValid.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsParamValueSetValid", _data.C.Sp_IsParamValueSetValid.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_HasNewColumns", _data.C.Sp_HasNewColumns.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsColumnsValid", _data.C.Sp_IsColumnsValid.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsValid", _data.C.Sp_IsValid.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_Notes", _data.C.Sp_Notes.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_IsActiveRow", _data.C.Sp_IsActiveRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_IsDeleted", _data.C.Sp_IsDeleted.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Sp_CreatedUserId", _data.C.Sp_CreatedUserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Sp_CreatedDate", _data.C.Sp_CreatedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("Sp_UserId", _data.C.Sp_UserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("UserName", _data.C.UserName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Sp_LastModifiedDate", _data.C.Sp_LastModifiedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("Sp_Stamp", _data.C.Sp_Stamp.ToString(), TraceDataTypes.ByteArray);
			GetTraceItemsLongListCustom();
            return _traceItems;
        }

        public object[] GetTraceData()
        {
            object[] data = new object[40];
            data[0] = new object[] { "StateSwitch", StateSwitch.ToString() };
            data[1] = new object[] { "BrokenRuleManagerProperty", BrokenRuleManagerProperty.ToString() };
            data[2] = new object[] { "Sp_Id", _data.C.Sp_Id.ToString() };
            data[3] = new object[] { "Sp_ApVrsn_Id", _data.C.Sp_ApVrsn_Id.ToString() };
            data[4] = new object[] { "Sp_Name", _data.C.Sp_Name == null ? "<Null>" : _data.C.Sp_Name.ToString() };
            data[5] = new object[] { "Sp_IsCodeGen", _data.C.Sp_IsCodeGen.ToString() };
            data[6] = new object[] { "Sp_IsReadyCodeGen", _data.C.Sp_IsReadyCodeGen.ToString() };
            data[7] = new object[] { "Sp_IsCodeGenComplete", _data.C.Sp_IsCodeGenComplete.ToString() };
            data[8] = new object[] { "Sp_IsTagForCodeGen", _data.C.Sp_IsTagForCodeGen.ToString() };
            data[9] = new object[] { "Sp_IsTagForOther", _data.C.Sp_IsTagForOther.ToString() };
            data[10] = new object[] { "Sp_SprocGroups", _data.C.Sp_SprocGroups == null ? "<Null>" : _data.C.Sp_SprocGroups.ToString() };
            data[11] = new object[] { "Sp_IsBuildDataAccessMethod", _data.C.Sp_IsBuildDataAccessMethod.ToString() };
            data[12] = new object[] { "Sp_BuildListClass", _data.C.Sp_BuildListClass.ToString() };
            data[13] = new object[] { "Sp_IsFetchEntity", _data.C.Sp_IsFetchEntity.ToString() };
            data[14] = new object[] { "Sp_IsStaticList", _data.C.Sp_IsStaticList.ToString() };
            data[15] = new object[] { "Sp_IsTypeComboItem", _data.C.Sp_IsTypeComboItem == null ? "<Null>" : _data.C.Sp_IsTypeComboItem.ToString() };
            data[16] = new object[] { "Sp_IsCreateWireType", _data.C.Sp_IsCreateWireType.ToString() };
            data[17] = new object[] { "Sp_WireTypeName", _data.C.Sp_WireTypeName == null ? "<Null>" : _data.C.Sp_WireTypeName.ToString() };
            data[18] = new object[] { "Sp_MethodName", _data.C.Sp_MethodName == null ? "<Null>" : _data.C.Sp_MethodName.ToString() };
            data[19] = new object[] { "Sp_AssocEntity_Id", _data.C.Sp_AssocEntity_Id == null ? "<Null>" : _data.C.Sp_AssocEntity_Id.ToString() };
            data[20] = new object[] { "Sp_AssocEntity_Id_TextField", _data.C.Sp_AssocEntity_Id_TextField == null ? "<Null>" : _data.C.Sp_AssocEntity_Id_TextField.ToString() };
            data[21] = new object[] { "Sp_AssocEntityNames", _data.C.Sp_AssocEntityNames == null ? "<Null>" : _data.C.Sp_AssocEntityNames.ToString() };
            data[22] = new object[] { "Sp_SpRsTp_Id", _data.C.Sp_SpRsTp_Id == null ? "<Null>" : _data.C.Sp_SpRsTp_Id.ToString() };
            data[23] = new object[] { "Sp_SpRsTp_Id_TextField", _data.C.Sp_SpRsTp_Id_TextField == null ? "<Null>" : _data.C.Sp_SpRsTp_Id_TextField.ToString() };
            data[24] = new object[] { "Sp_SpRtTp_Id", _data.C.Sp_SpRtTp_Id == null ? "<Null>" : _data.C.Sp_SpRtTp_Id.ToString() };
            data[25] = new object[] { "Sp_SpRtTp_Id_TextField", _data.C.Sp_SpRtTp_Id_TextField == null ? "<Null>" : _data.C.Sp_SpRtTp_Id_TextField.ToString() };
            data[26] = new object[] { "Sp_IsInputParamsAsObjectArray", _data.C.Sp_IsInputParamsAsObjectArray.ToString() };
            data[27] = new object[] { "Sp_IsParamsAutoRefresh", _data.C.Sp_IsParamsAutoRefresh.ToString() };
            data[28] = new object[] { "Sp_HasNewParams", _data.C.Sp_HasNewParams.ToString() };
            data[29] = new object[] { "Sp_IsParamsValid", _data.C.Sp_IsParamsValid.ToString() };
            data[30] = new object[] { "Sp_IsParamValueSetValid", _data.C.Sp_IsParamValueSetValid.ToString() };
            data[31] = new object[] { "Sp_HasNewColumns", _data.C.Sp_HasNewColumns.ToString() };
            data[32] = new object[] { "Sp_IsColumnsValid", _data.C.Sp_IsColumnsValid.ToString() };
            data[33] = new object[] { "Sp_IsValid", _data.C.Sp_IsValid.ToString() };
            data[34] = new object[] { "Sp_Notes", _data.C.Sp_Notes == null ? "<Null>" : _data.C.Sp_Notes.ToString() };
            data[35] = new object[] { "Sp_IsActiveRow", _data.C.Sp_IsActiveRow.ToString() };
            data[36] = new object[] { "Sp_IsDeleted", _data.C.Sp_IsDeleted.ToString() };
            data[37] = new object[] { "Sp_CreatedUserId", _data.C.Sp_CreatedUserId == null ? "<Null>" : _data.C.Sp_CreatedUserId.ToString() };
            data[38] = new object[] { "Sp_CreatedDate", _data.C.Sp_CreatedDate == null ? "<Null>" : _data.C.Sp_CreatedDate.ToString() };
            data[39] = new object[] { "Sp_UserId", _data.C.Sp_UserId == null ? "<Null>" : _data.C.Sp_UserId.ToString() };
            data[40] = new object[] { "UserName", _data.C.UserName == null ? "<Null>" : _data.C.UserName.ToString() };
            data[41] = new object[] { "Sp_LastModifiedDate", _data.C.Sp_LastModifiedDate == null ? "<Null>" : _data.C.Sp_LastModifiedDate.ToString() };
            return data;
        }


		#endregion ITraceItem Members


		#region INotifyPropertyChanged Members

        /// <summary>
        /// 	<para>
        ///         This method raises the <see cref="PropertyChanged">PropertyChanged</see> event
        ///         and is required by the INotifyPropertyChanged interface. It’s called in nearly
        ///         all public data field properties.
        ///     </para>
        /// 	<para>The <strong>INotifyPropertyChanged</strong> interface is used to notify
        ///     clients, typically binding clients, that a property value has changed. For more
        ///     information, search INotifyPropertyChanged on <strong>MSDN</strong>.</para>
        /// </summary>
        protected void Notify(string propName)
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }


		#endregion INotifyPropertyChanged Members


		#region INotifyDataErrorInfo Members

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        //public void RaiseErrorsChanged(string propertyName)
        //{
        //    if (ErrorsChanged != null)
        //        ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        //}

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            //throw new NotImplementedException();
            if (_brokenRuleManager.GetBrokenRulesForProperty(propertyName).Count == 0)
            {
                return "";
            }
            else
            {

                return _brokenRuleManager.GetBrokenRulesForProperty(propertyName)[0].ToString();
            }
        }

        public bool HasErrors
        {
            get { return _brokenRuleManager.IsEntityValid(); }
        }


		#endregion INotifyDataErrorInfo Members


        #region List Methods

        #region IEditableObject Members

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="CancelEdit">CancelEdit</see> and <see cref="EndEdit">EndEdit</see>.
        ///     </para>
        /// 	<para>Begins an edit on an object.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void BeginEdit()
        {
//            Guid? traceId = Guid.NewGuid();
//            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BeginEdit", IfxTraceCategory.Enter);
//            Console.WriteLine("BeginEdit");
//            //***  ToDo:
//            // Set the parent type below, then when properties are being edited, raise an event to sync fields in the props control.
//            _parentType = ParentEditObjectType.EntitiyListControl;
//
//            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BeginEdit", IfxTraceCategory.Leave);
        }

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="BeginEdit">BeginEdit</see> and <see cref="EndEdit">EndEdit</see>.
        ///     </para>
        /// 	<para>Discards changes since the last BeginEdit call.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void CancelEdit()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelEdit", IfxTraceCategory.Enter);
            UnDo();
            _parentType = ParentEditObjectType.None;

            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelEdit", IfxTraceCategory.Leave);
        }

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="BeginEdit">BeginEdit</see> and <see cref="CancelEdit">CancelEdit</see>.
        ///     </para>
        /// 	<para>Pushes changes since the last BeginEdit or IBindingList.AddNew call into the
        ///     underlying object.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void EndEdit()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", IfxTraceCategory.Enter);
//                if (_data.S.IsDirty() && _data.S.IsValid())
//                {
//                    // ToDo - This needs more testing for Silverlight
//                    Save(null, ParentEditObjectType.EntitiyListControl, UseConcurrencyCheck.UseDefaultSetting);
//
//                    //DataServiceInsertUpdateResponseClientSide result = Save(UseConcurrencyCheck.UseDefaultSetting);
//                    //if (result.Result != DataOperationResult.Success)
//                    //{
//                    //    UnDo();
//                    //    Debugger.Break();
//                    //}
//                }
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", IfxTraceCategory.Leave);
//            }
        }

        #endregion IEditableObject Members

        #endregion List Methods



        #region Children_WcStoredProcParam


        WcStoredProcParam_List _children_WcStoredProcParam = new WcStoredProcParam_List();


        public WcStoredProcParam_List Children_WcStoredProcParam
        {
            get { return _children_WcStoredProcParam; }
            set { _children_WcStoredProcParam = value; }
        }

        public void Get_Children_WcStoredProcParam()
        {
            _proxyWcStoredProcParamService.Begin_WcStoredProcParam_GetListByFK(Sp_Id);
        }
        

        void _proxyWcStoredProcParamService_WcStoredProcParam_GetListByFKCompleted(object sender, WcStoredProcParam_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcStoredProcParamService_WcStoredProcParam_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array != null)
                {
                    Children_WcStoredProcParam.ReplaceList(array);
                }
                else
                {
                    Children_WcStoredProcParam.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcStoredProcParamService_WcStoredProcParam_GetListByFKCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcStoredProcParamService_WcStoredProcParam_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }


            #endregion Children_WcStoredProcParam

            

        #region Children_WcStoredProcColumn


        WcStoredProcColumn_List _children_WcStoredProcColumn = new WcStoredProcColumn_List();


        public WcStoredProcColumn_List Children_WcStoredProcColumn
        {
            get { return _children_WcStoredProcColumn; }
            set { _children_WcStoredProcColumn = value; }
        }

        public void Get_Children_WcStoredProcColumn()
        {
            _proxyWcStoredProcColumnService.Begin_WcStoredProcColumn_GetListByFK(Sp_Id);
        }
        

        void _proxyWcStoredProcColumnService_WcStoredProcColumn_GetListByFKCompleted(object sender, WcStoredProcColumn_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcStoredProcColumnService_WcStoredProcColumn_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array != null)
                {
                    Children_WcStoredProcColumn.ReplaceList(array);
                }
                else
                {
                    Children_WcStoredProcColumn.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcStoredProcColumnService_WcStoredProcColumn_GetListByFKCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcStoredProcColumnService_WcStoredProcColumn_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }


            #endregion Children_WcStoredProcColumn

            

//        #region IDataErrorInfo Members
//
//        /// <summary>
//        /// 	<para>Part of the <strong>IDataErrorInfo</strong> Interface. Gets an error message
//        ///     indicating what is wrong with this object.</para>
//        /// 	<para>For more information, search <strong>IDataErrorInfo</strong> on
//        ///     <strong>MSDN</strong>.</para>
//        /// </summary>
//        public string Error
//        {
//            get { throw new NotImplementedException(); }
//        }
//
//        /// <summary>
//        /// 	<para>Part of the <strong>IDataErrorInfo</strong> Interface. Gets the error message
//        ///     for the property with the given name.</para>
//        /// 	<para>For more information, search <strong>IDataErrorInfo</strong> on
//        ///     <strong>MSDN</strong>.</para>
//        /// </summary>
//        string IDataErrorInfo.this[string columnName]
//        {
//            get
//            {
//                if (!_brokenRuleManager.IsPropertyValid(columnName))
//                {
//                    //string err = "line 1 First validation msg" + Environment.NewLine + "line 2 Second validation msg";
//                    //return err;
//                    return _brokenRuleManager.GetBrokenRulesForProperty(columnName)[0].ToString();
//                }
//                else
//                {
//                    return null;
//                }
//            }
//        }
//
//        #endregion



    }

}



