using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/8/2018 10:54:55 AM

namespace EntityBll.SL
{
    public partial class WcStoredProc_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_SprocInDb = 128;
		public const int STRINGSIZE_Sp_Name = 128;
		public const int STRINGSIZE_Sp_SprocGroups = 500;
		public const int STRINGSIZE_Sp_WireTypeName = 100;
		public const int STRINGSIZE_Sp_MethodName = 75;
		public const int STRINGSIZE_Sp_AssocEntityNames = 1000;
		public const int STRINGSIZE_Sp_Notes = 255;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_Sp_Id_Required = "Id is a required field.";
		private const string BROKENRULE_Sp_ApVrsn_Id_Required = "ApVrsn_Id is a required field.";
		private const string BROKENRULE_ImportSproc_Required = "Import Stored Procedure is a required field.";
		private const string BROKENRULE_IsImported_Required = "IsImported is a required field.";
		private string BROKENRULE_SprocInDb_TextLength = "Stored Procedure in Database has a maximum text length of  '" + STRINGSIZE_SprocInDb + "'.";
		private string BROKENRULE_Sp_Name_TextLength = "Stored Procedure in WC has a maximum text length of  '" + STRINGSIZE_Sp_Name + "'.";
		private const string BROKENRULE_Sp_Name_Required = "Stored Procedure in WC is a required field.";
		private const string BROKENRULE_Sp_IsCodeGen_Required = "Is CodeGen is a required field.";
		private const string BROKENRULE_Sp_IsReadyCodeGen_Required = "Is Ready For CodeGen is a required field.";
		private const string BROKENRULE_Sp_IsCodeGenComplete_Required = "Is CodeGen Complete is a required field.";
		private const string BROKENRULE_Sp_IsTagForCodeGen_Required = "Tag For CodeGen is a required field.";
		private const string BROKENRULE_Sp_IsTagForOther_Required = "Is Tag For Other is a required field.";
		private string BROKENRULE_Sp_SprocGroups_TextLength = "Sproc Groups has a maximum text length of  '" + STRINGSIZE_Sp_SprocGroups + "'.";
		private const string BROKENRULE_Sp_IsBuildDataAccessMethod_Required = "Build Data Access Method is a required field.";
		private const string BROKENRULE_Sp_BuildListClass_Required = "Build List Class is a required field.";
		private const string BROKENRULE_Sp_IsFetchEntity_Required = "Is Fetch Entity is a required field.";
		private const string BROKENRULE_Sp_IsStaticList_Required = "Is Static List is a required field.";
		private const string BROKENRULE_Sp_IsCreateWireType_Required = "Is Create WireType is a required field.";
		private string BROKENRULE_Sp_WireTypeName_TextLength = "Type Name has a maximum text length of  '" + STRINGSIZE_Sp_WireTypeName + "'.";
		private string BROKENRULE_Sp_MethodName_TextLength = "Method Name has a maximum text length of  '" + STRINGSIZE_Sp_MethodName + "'.";
		private string BROKENRULE_Sp_AssocEntityNames_TextLength = "Associated Entity Names has a maximum text length of  '" + STRINGSIZE_Sp_AssocEntityNames + "'.";
		private const string BROKENRULE_Sp_UseLegacyConnectionCode_Required = "UseLegacyConnectionCode is a required field.";
		private const string BROKENRULE_Sp_SpRsTp_Id_ZeroNotAllowed = "Result Type:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_Sp_SpRtTp_Id_ZeroNotAllowed = "Return Type:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_Sp_IsInputParamsAsObjectArray_Required = "Is Input Params As Object Array is a required field.";
		private const string BROKENRULE_Sp_IsParamsAutoRefresh_Required = "Is Parm Values Auto Refresh is a required field.";
		private const string BROKENRULE_Sp_HasNewParams_Required = "Has New Params is a required field.";
		private const string BROKENRULE_Sp_IsParamsValid_Required = "Is Params Valid is a required field.";
		private const string BROKENRULE_Sp_IsParamValueSetValid_Required = "Is Param Value Set Valid is a required field.";
		private const string BROKENRULE_Sp_HasNewColumns_Required = "Has New Columns is a required field.";
		private const string BROKENRULE_Sp_IsColumnsValid_Required = "Is Columns Valid is a required field.";
		private const string BROKENRULE_Sp_IsValid_Required = "Is Valid is a required field.";
		private string BROKENRULE_Sp_Notes_TextLength = "Notes has a maximum text length of  '" + STRINGSIZE_Sp_Notes + "'.";
		private const string BROKENRULE_Sp_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_Sp_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_Sp_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_Id", BROKENRULE_Sp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_ApVrsn_Id", BROKENRULE_Sp_ApVrsn_Id_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("ImportSproc", BROKENRULE_ImportSproc_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("IsImported", BROKENRULE_IsImported_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("SprocInDb", BROKENRULE_SprocInDb_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsCodeGen", BROKENRULE_Sp_IsCodeGen_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsReadyCodeGen", BROKENRULE_Sp_IsReadyCodeGen_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsCodeGenComplete", BROKENRULE_Sp_IsCodeGenComplete_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsTagForCodeGen", BROKENRULE_Sp_IsTagForCodeGen_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsTagForOther", BROKENRULE_Sp_IsTagForOther_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_SprocGroups", BROKENRULE_Sp_SprocGroups_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsBuildDataAccessMethod", BROKENRULE_Sp_IsBuildDataAccessMethod_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_BuildListClass", BROKENRULE_Sp_BuildListClass_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsFetchEntity", BROKENRULE_Sp_IsFetchEntity_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsStaticList", BROKENRULE_Sp_IsStaticList_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsCreateWireType", BROKENRULE_Sp_IsCreateWireType_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_WireTypeName", BROKENRULE_Sp_WireTypeName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_MethodName", BROKENRULE_Sp_MethodName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_AssocEntityNames", BROKENRULE_Sp_AssocEntityNames_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Sp_UseLegacyConnectionCode", BROKENRULE_Sp_UseLegacyConnectionCode_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_SpRsTp_Id", BROKENRULE_Sp_SpRsTp_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_SpRtTp_Id", BROKENRULE_Sp_SpRtTp_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsInputParamsAsObjectArray", BROKENRULE_Sp_IsInputParamsAsObjectArray_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsParamsAutoRefresh", BROKENRULE_Sp_IsParamsAutoRefresh_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_HasNewParams", BROKENRULE_Sp_HasNewParams_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsParamsValid", BROKENRULE_Sp_IsParamsValid_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsParamValueSetValid", BROKENRULE_Sp_IsParamValueSetValid_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_HasNewColumns", BROKENRULE_Sp_HasNewColumns_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsColumnsValid", BROKENRULE_Sp_IsColumnsValid_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsValid", BROKENRULE_Sp_IsValid_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_Notes", BROKENRULE_Sp_Notes_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsActiveRow", BROKENRULE_Sp_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_IsDeleted", BROKENRULE_Sp_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Sp_Stamp", BROKENRULE_Sp_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void Sp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Id");
                string newBrokenRules = "";
                
                if (Sp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_Id", BROKENRULE_Sp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_Id", BROKENRULE_Sp_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_Id", _brokenRuleManager.IsPropertyValid("Sp_Id"), IsPropertyDirty("Sp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_ApVrsn_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_ApVrsn_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_ApVrsn_Id");
                string newBrokenRules = "";
                
                if (Sp_ApVrsn_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_ApVrsn_Id", BROKENRULE_Sp_ApVrsn_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_ApVrsn_Id", BROKENRULE_Sp_ApVrsn_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_ApVrsn_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_ApVrsn_Id", _brokenRuleManager.IsPropertyValid("Sp_ApVrsn_Id"), IsPropertyDirty("Sp_ApVrsn_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_ApVrsn_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_ApVrsn_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ImportSproc_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSproc_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ImportSproc");
                string newBrokenRules = "";
                
                if (ImportSproc == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ImportSproc", BROKENRULE_ImportSproc_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ImportSproc", BROKENRULE_ImportSproc_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ImportSproc");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ImportSproc", _brokenRuleManager.IsPropertyValid("ImportSproc"), IsPropertyDirty("ImportSproc"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSproc_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSproc_Validate", IfxTraceCategory.Leave);
            }
        }

        private void IsImported_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsImported_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("IsImported");
                string newBrokenRules = "";
                
                if (IsImported == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("IsImported", BROKENRULE_IsImported_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("IsImported", BROKENRULE_IsImported_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("IsImported");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("IsImported", _brokenRuleManager.IsPropertyValid("IsImported"), IsPropertyDirty("IsImported"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsImported_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsImported_Validate", IfxTraceCategory.Leave);
            }
        }

        private void SprocInDb_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SprocInDb_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SprocInDb");
                string newBrokenRules = "";
                				int len = 0;
                if (SprocInDb != null)
                {
                    len = SprocInDb.Length;
                }

                if (len > STRINGSIZE_SprocInDb)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("SprocInDb", BROKENRULE_SprocInDb_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("SprocInDb", BROKENRULE_SprocInDb_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("SprocInDb");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("SprocInDb", _brokenRuleManager.IsPropertyValid("SprocInDb"), IsPropertyDirty("SprocInDb"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SprocInDb_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SprocInDb_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (Sp_Name != null)
                {
                    len = Sp_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_Required);
                    if (len > STRINGSIZE_Sp_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Sp_Name", BROKENRULE_Sp_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_Name", _brokenRuleManager.IsPropertyValid("Sp_Name"), IsPropertyDirty("Sp_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsCodeGen_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCodeGen_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsCodeGen");
                string newBrokenRules = "";
                
                if (Sp_IsCodeGen == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsCodeGen", BROKENRULE_Sp_IsCodeGen_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsCodeGen", BROKENRULE_Sp_IsCodeGen_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsCodeGen");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsCodeGen", _brokenRuleManager.IsPropertyValid("Sp_IsCodeGen"), IsPropertyDirty("Sp_IsCodeGen"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCodeGen_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCodeGen_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsReadyCodeGen_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsReadyCodeGen_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsReadyCodeGen");
                string newBrokenRules = "";
                
                if (Sp_IsReadyCodeGen == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsReadyCodeGen", BROKENRULE_Sp_IsReadyCodeGen_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsReadyCodeGen", BROKENRULE_Sp_IsReadyCodeGen_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsReadyCodeGen");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsReadyCodeGen", _brokenRuleManager.IsPropertyValid("Sp_IsReadyCodeGen"), IsPropertyDirty("Sp_IsReadyCodeGen"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsReadyCodeGen_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsReadyCodeGen_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsCodeGenComplete_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCodeGenComplete_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsCodeGenComplete");
                string newBrokenRules = "";
                
                if (Sp_IsCodeGenComplete == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsCodeGenComplete", BROKENRULE_Sp_IsCodeGenComplete_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsCodeGenComplete", BROKENRULE_Sp_IsCodeGenComplete_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsCodeGenComplete");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsCodeGenComplete", _brokenRuleManager.IsPropertyValid("Sp_IsCodeGenComplete"), IsPropertyDirty("Sp_IsCodeGenComplete"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCodeGenComplete_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCodeGenComplete_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsTagForCodeGen_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTagForCodeGen_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsTagForCodeGen");
                string newBrokenRules = "";
                
                if (Sp_IsTagForCodeGen == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsTagForCodeGen", BROKENRULE_Sp_IsTagForCodeGen_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsTagForCodeGen", BROKENRULE_Sp_IsTagForCodeGen_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsTagForCodeGen");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsTagForCodeGen", _brokenRuleManager.IsPropertyValid("Sp_IsTagForCodeGen"), IsPropertyDirty("Sp_IsTagForCodeGen"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTagForCodeGen_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTagForCodeGen_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsTagForOther_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTagForOther_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsTagForOther");
                string newBrokenRules = "";
                
                if (Sp_IsTagForOther == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsTagForOther", BROKENRULE_Sp_IsTagForOther_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsTagForOther", BROKENRULE_Sp_IsTagForOther_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsTagForOther");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsTagForOther", _brokenRuleManager.IsPropertyValid("Sp_IsTagForOther"), IsPropertyDirty("Sp_IsTagForOther"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTagForOther_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsTagForOther_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_SprocGroups_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SprocGroups_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_SprocGroups");
                string newBrokenRules = "";
                				int len = 0;
                if (Sp_SprocGroups != null)
                {
                    len = Sp_SprocGroups.Length;
                }

                if (len > STRINGSIZE_Sp_SprocGroups)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_SprocGroups", BROKENRULE_Sp_SprocGroups_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_SprocGroups", BROKENRULE_Sp_SprocGroups_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_SprocGroups");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_SprocGroups", _brokenRuleManager.IsPropertyValid("Sp_SprocGroups"), IsPropertyDirty("Sp_SprocGroups"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SprocGroups_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SprocGroups_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsBuildDataAccessMethod_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsBuildDataAccessMethod_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsBuildDataAccessMethod");
                string newBrokenRules = "";
                
                if (Sp_IsBuildDataAccessMethod == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsBuildDataAccessMethod", BROKENRULE_Sp_IsBuildDataAccessMethod_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsBuildDataAccessMethod", BROKENRULE_Sp_IsBuildDataAccessMethod_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsBuildDataAccessMethod");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsBuildDataAccessMethod", _brokenRuleManager.IsPropertyValid("Sp_IsBuildDataAccessMethod"), IsPropertyDirty("Sp_IsBuildDataAccessMethod"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsBuildDataAccessMethod_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsBuildDataAccessMethod_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_BuildListClass_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_BuildListClass_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_BuildListClass");
                string newBrokenRules = "";
                
                if (Sp_BuildListClass == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_BuildListClass", BROKENRULE_Sp_BuildListClass_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_BuildListClass", BROKENRULE_Sp_BuildListClass_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_BuildListClass");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_BuildListClass", _brokenRuleManager.IsPropertyValid("Sp_BuildListClass"), IsPropertyDirty("Sp_BuildListClass"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_BuildListClass_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_BuildListClass_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsFetchEntity_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsFetchEntity_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsFetchEntity");
                string newBrokenRules = "";
                
                if (Sp_IsFetchEntity == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsFetchEntity", BROKENRULE_Sp_IsFetchEntity_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsFetchEntity", BROKENRULE_Sp_IsFetchEntity_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsFetchEntity");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsFetchEntity", _brokenRuleManager.IsPropertyValid("Sp_IsFetchEntity"), IsPropertyDirty("Sp_IsFetchEntity"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsFetchEntity_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsFetchEntity_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsStaticList_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsStaticList_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsStaticList");
                string newBrokenRules = "";
                
                if (Sp_IsStaticList == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsStaticList", BROKENRULE_Sp_IsStaticList_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsStaticList", BROKENRULE_Sp_IsStaticList_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsStaticList");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsStaticList", _brokenRuleManager.IsPropertyValid("Sp_IsStaticList"), IsPropertyDirty("Sp_IsStaticList"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsStaticList_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsStaticList_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsTypeComboItem_Validate()
        {
        }

        private void Sp_IsCreateWireType_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCreateWireType_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsCreateWireType");
                string newBrokenRules = "";
                
                if (Sp_IsCreateWireType == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsCreateWireType", BROKENRULE_Sp_IsCreateWireType_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsCreateWireType", BROKENRULE_Sp_IsCreateWireType_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsCreateWireType");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsCreateWireType", _brokenRuleManager.IsPropertyValid("Sp_IsCreateWireType"), IsPropertyDirty("Sp_IsCreateWireType"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCreateWireType_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsCreateWireType_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_WireTypeName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_WireTypeName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_WireTypeName");
                string newBrokenRules = "";
                				int len = 0;
                if (Sp_WireTypeName != null)
                {
                    len = Sp_WireTypeName.Length;
                }

                if (len > STRINGSIZE_Sp_WireTypeName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_WireTypeName", BROKENRULE_Sp_WireTypeName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_WireTypeName", BROKENRULE_Sp_WireTypeName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_WireTypeName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_WireTypeName", _brokenRuleManager.IsPropertyValid("Sp_WireTypeName"), IsPropertyDirty("Sp_WireTypeName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_WireTypeName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_WireTypeName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_MethodName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_MethodName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_MethodName");
                string newBrokenRules = "";
                				int len = 0;
                if (Sp_MethodName != null)
                {
                    len = Sp_MethodName.Length;
                }

                if (len > STRINGSIZE_Sp_MethodName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_MethodName", BROKENRULE_Sp_MethodName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_MethodName", BROKENRULE_Sp_MethodName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_MethodName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_MethodName", _brokenRuleManager.IsPropertyValid("Sp_MethodName"), IsPropertyDirty("Sp_MethodName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_MethodName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_MethodName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_AssocEntity_Id_Validate()
        {
        }

        private void Sp_AssocEntity_Id_TextField_Validate()
        {
        }

        private void Sp_AssocEntityNames_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_AssocEntityNames_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_AssocEntityNames");
                string newBrokenRules = "";
                				int len = 0;
                if (Sp_AssocEntityNames != null)
                {
                    len = Sp_AssocEntityNames.Length;
                }

                if (len > STRINGSIZE_Sp_AssocEntityNames)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_AssocEntityNames", BROKENRULE_Sp_AssocEntityNames_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_AssocEntityNames", BROKENRULE_Sp_AssocEntityNames_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_AssocEntityNames");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_AssocEntityNames", _brokenRuleManager.IsPropertyValid("Sp_AssocEntityNames"), IsPropertyDirty("Sp_AssocEntityNames"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_AssocEntityNames_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_AssocEntityNames_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_UseLegacyConnectionCode_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_UseLegacyConnectionCode_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_UseLegacyConnectionCode");
                string newBrokenRules = "";
                
                if (Sp_UseLegacyConnectionCode == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_UseLegacyConnectionCode", BROKENRULE_Sp_UseLegacyConnectionCode_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_UseLegacyConnectionCode", BROKENRULE_Sp_UseLegacyConnectionCode_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_UseLegacyConnectionCode");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_UseLegacyConnectionCode", _brokenRuleManager.IsPropertyValid("Sp_UseLegacyConnectionCode"), IsPropertyDirty("Sp_UseLegacyConnectionCode"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_UseLegacyConnectionCode_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_UseLegacyConnectionCode_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_DbCnSK_Id_Validate()
        {
        }

        private void Sp_DbCnSK_Id_TextField_Validate()
        {
        }

        private void Sp_ApDbScm_Id_Validate()
        {
        }

        private void Sp_ApDbScm_Id_TextField_Validate()
        {
        }

        private void Sp_SpRsTp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_SpRsTp_Id");
                string newBrokenRules = "";
                
                if (Sp_SpRsTp_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_SpRsTp_Id", BROKENRULE_Sp_SpRsTp_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_SpRsTp_Id", BROKENRULE_Sp_SpRsTp_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_SpRsTp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_SpRsTp_Id", _brokenRuleManager.IsPropertyValid("Sp_SpRsTp_Id"), IsPropertyDirty("Sp_SpRsTp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_SpRsTp_Id_TextField_Validate()
        {
        }

        private void Sp_SpRtTp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_SpRtTp_Id");
                string newBrokenRules = "";
                
                if (Sp_SpRtTp_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_SpRtTp_Id", BROKENRULE_Sp_SpRtTp_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_SpRtTp_Id", BROKENRULE_Sp_SpRtTp_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_SpRtTp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_SpRtTp_Id", _brokenRuleManager.IsPropertyValid("Sp_SpRtTp_Id"), IsPropertyDirty("Sp_SpRtTp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_SpRtTp_Id_TextField_Validate()
        {
        }

        private void Sp_IsInputParamsAsObjectArray_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsInputParamsAsObjectArray_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsInputParamsAsObjectArray");
                string newBrokenRules = "";
                
                if (Sp_IsInputParamsAsObjectArray == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsInputParamsAsObjectArray", BROKENRULE_Sp_IsInputParamsAsObjectArray_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsInputParamsAsObjectArray", BROKENRULE_Sp_IsInputParamsAsObjectArray_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsInputParamsAsObjectArray");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsInputParamsAsObjectArray", _brokenRuleManager.IsPropertyValid("Sp_IsInputParamsAsObjectArray"), IsPropertyDirty("Sp_IsInputParamsAsObjectArray"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsInputParamsAsObjectArray_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsInputParamsAsObjectArray_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsParamsAutoRefresh_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsAutoRefresh_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsParamsAutoRefresh");
                string newBrokenRules = "";
                
                if (Sp_IsParamsAutoRefresh == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsParamsAutoRefresh", BROKENRULE_Sp_IsParamsAutoRefresh_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsParamsAutoRefresh", BROKENRULE_Sp_IsParamsAutoRefresh_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsParamsAutoRefresh");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsParamsAutoRefresh", _brokenRuleManager.IsPropertyValid("Sp_IsParamsAutoRefresh"), IsPropertyDirty("Sp_IsParamsAutoRefresh"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsAutoRefresh_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsAutoRefresh_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_HasNewParams_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewParams_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_HasNewParams");
                string newBrokenRules = "";
                
                if (Sp_HasNewParams == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_HasNewParams", BROKENRULE_Sp_HasNewParams_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_HasNewParams", BROKENRULE_Sp_HasNewParams_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_HasNewParams");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_HasNewParams", _brokenRuleManager.IsPropertyValid("Sp_HasNewParams"), IsPropertyDirty("Sp_HasNewParams"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewParams_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewParams_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsParamsValid_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsValid_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsParamsValid");
                string newBrokenRules = "";
                
                if (Sp_IsParamsValid == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsParamsValid", BROKENRULE_Sp_IsParamsValid_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsParamsValid", BROKENRULE_Sp_IsParamsValid_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsParamsValid");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsParamsValid", _brokenRuleManager.IsPropertyValid("Sp_IsParamsValid"), IsPropertyDirty("Sp_IsParamsValid"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsValid_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamsValid_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsParamValueSetValid_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamValueSetValid_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsParamValueSetValid");
                string newBrokenRules = "";
                
                if (Sp_IsParamValueSetValid == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsParamValueSetValid", BROKENRULE_Sp_IsParamValueSetValid_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsParamValueSetValid", BROKENRULE_Sp_IsParamValueSetValid_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsParamValueSetValid");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsParamValueSetValid", _brokenRuleManager.IsPropertyValid("Sp_IsParamValueSetValid"), IsPropertyDirty("Sp_IsParamValueSetValid"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamValueSetValid_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsParamValueSetValid_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_HasNewColumns_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewColumns_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_HasNewColumns");
                string newBrokenRules = "";
                
                if (Sp_HasNewColumns == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_HasNewColumns", BROKENRULE_Sp_HasNewColumns_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_HasNewColumns", BROKENRULE_Sp_HasNewColumns_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_HasNewColumns");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_HasNewColumns", _brokenRuleManager.IsPropertyValid("Sp_HasNewColumns"), IsPropertyDirty("Sp_HasNewColumns"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewColumns_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_HasNewColumns_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsColumnsValid_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsColumnsValid_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsColumnsValid");
                string newBrokenRules = "";
                
                if (Sp_IsColumnsValid == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsColumnsValid", BROKENRULE_Sp_IsColumnsValid_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsColumnsValid", BROKENRULE_Sp_IsColumnsValid_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsColumnsValid");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsColumnsValid", _brokenRuleManager.IsPropertyValid("Sp_IsColumnsValid"), IsPropertyDirty("Sp_IsColumnsValid"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsColumnsValid_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsColumnsValid_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsValid_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsValid_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsValid");
                string newBrokenRules = "";
                
                if (Sp_IsValid == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsValid", BROKENRULE_Sp_IsValid_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsValid", BROKENRULE_Sp_IsValid_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsValid");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsValid", _brokenRuleManager.IsPropertyValid("Sp_IsValid"), IsPropertyDirty("Sp_IsValid"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsValid_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsValid_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_Notes_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Notes_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Notes");
                string newBrokenRules = "";
                				int len = 0;
                if (Sp_Notes != null)
                {
                    len = Sp_Notes.Length;
                }

                if (len > STRINGSIZE_Sp_Notes)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_Notes", BROKENRULE_Sp_Notes_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_Notes", BROKENRULE_Sp_Notes_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Notes");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_Notes", _brokenRuleManager.IsPropertyValid("Sp_Notes"), IsPropertyDirty("Sp_Notes"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Notes_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Notes_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsActiveRow");
                string newBrokenRules = "";
                
                if (Sp_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsActiveRow", BROKENRULE_Sp_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsActiveRow", BROKENRULE_Sp_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsActiveRow", _brokenRuleManager.IsPropertyValid("Sp_IsActiveRow"), IsPropertyDirty("Sp_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsDeleted");
                string newBrokenRules = "";
                
                if (Sp_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_IsDeleted", BROKENRULE_Sp_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_IsDeleted", BROKENRULE_Sp_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_IsDeleted", _brokenRuleManager.IsPropertyValid("Sp_IsDeleted"), IsPropertyDirty("Sp_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_CreatedUserId_Validate()
        {
        }

        private void Sp_CreatedDate_Validate()
        {
        }

        private void Sp_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Sp_LastModifiedDate_Validate()
        {
        }

        private void Sp_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Stamp");
                string newBrokenRules = "";
                
                if (Sp_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Sp_Stamp", BROKENRULE_Sp_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Sp_Stamp", BROKENRULE_Sp_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Sp_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Sp_Stamp", _brokenRuleManager.IsPropertyValid("Sp_Stamp"), IsPropertyDirty("Sp_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


