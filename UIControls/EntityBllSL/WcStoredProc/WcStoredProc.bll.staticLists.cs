using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;

// Gen Timestamp:  12/23/2017 1:26:51 AM

namespace EntityBll.SL
{

    public partial class WcStoredProc_Bll_staticLists
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcStoredProc_Bll_staticLists";



        static bool isDataLoaded = false;
        static ProxyWrapper.WcStoredProcService_ProxyWrapper _staticWcStoredProcProxy = null;

        #endregion Initialize Variables

        public static void LoadStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Enter);
//
//                if (_staticWellDtProxy == null)
//                {
//                    _staticWellDtProxy = new ProxyWrapper.WellDtService_ProxyWrapper();
//                }

                LoadStaticLists_Custom();
                isDataLoaded = false;

}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Leave);
            }
        }



        #region Properties

        public static bool IsDataLoaded
        {
            get { return isDataLoaded; }
        }

        #endregion Properties


        #region RefreshLists



        #endregion RefreshLists


    }

    #region Classes for Binding

#endregion Classes for Binding

}

