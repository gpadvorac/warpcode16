using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;


// Gen Timestamp:  12/23/2017 1:26:51 AM

namespace EntityBll.SL
{

    public partial class WcStoredProc_Bll_staticLists
    {

        #region Initialize Variables



        #endregion Initialize Variables




        public static void LoadStaticLists_Custom()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", IfxTraceCategory.Enter);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", IfxTraceCategory.Leave);
            }
        }




    }

}


