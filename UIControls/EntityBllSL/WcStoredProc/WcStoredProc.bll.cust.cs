using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TypeServices;
using System.ComponentModel;
using System.Windows;
using Ifx.SL;
using vCommands;
using vControls;
using vControls.vCustomContextMenu;
using vUICommon;
using Velocity.SL;

// Gen Timestamp:  12/23/2017 1:26:51 AM

namespace EntityBll.SL
{


    public partial class WcStoredProc_Bll : IContextMenuItemsSource //: BusinessObjectBase, ITraceItem, IBusinessObject, IBusinessObjectV2, INotifyPropertyChanged , IEditableObject, IDataErrorInfo
    {




        #region Initialize Custom Variables



        #endregion Initialize Custom Variables


        #region General Methods


        void InitializeClass_Cust()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", IfxTraceCategory.Enter);
//      
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", IfxTraceCategory.Leave);
//            }
        }

        void CustomConstructor()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructor", IfxTraceCategory.Enter);


                WcStoredProcProxy.ExecuteWcStoredProc_importFromSourceDbCompleted += WcStoredProcProxy_ExecuteWcStoredProc_importFromSourceDbCompleted;
                WcStoredProcProxy.ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompleted += WcStoredProcProxy_ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompleted;
                WcStoredProcProxy.RefreshStoredProcedureColumnsCompleted += WcStoredProcProxy_RefreshStoredProcedureColumnsCompleted;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructor", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructor", IfxTraceCategory.Leave);
            }
        }

        public void SetStandingFK(string parentType, Guid fk)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Enter);
                _parentEntityType = parentType;
                _StandingFK = fk;
                SetStandingFK();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Leave);
            }
        }

        public void SetStandingFK()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Enter);
//                //switch (_parentEntityType)
//                //{
//                //    case ParentType.Company:
//                //        _data.C._b = _StandingFK;
//                //        break;
//                //    case ParentType.PersonContact:
//                //        _data.C._c = _StandingFK;
//                //        break;
//                //}      
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Leave);
//            }
        }

        // Alternate FKs
        public void SetOtherParentKeys(string parentType, Guid id)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", IfxTraceCategory.Enter);
//                //_parentEntityType = parentType;
//                //switch (_parentEntityType)
//                //{
//                //    case "ucWellDt":
//                //        _data.C.WlD_Id_noevents = id;
//                //        break;
//                //    case "ucContractDt":
//                //        _data.C.CtD_Id_noevents = id;
//                //        break;
//                //}
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", IfxTraceCategory.Leave);
//            }
        }



        #endregion General Methods


        #region Data Access Methods


        #endregion Data Access Methods


        #region Method Extentions For Custom Code



        private void SetCustomDefaultBrokenRules()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", IfxTraceCategory.Enter);
//
//         
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", IfxTraceCategory.Leave);
//            }
        }




        private void CustomPropertySetterCode(string propName)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", IfxTraceCategory.Enter);
//                switch(propName)
//                {
//                    case "xxxx":
//
//                        break;
//
//                }
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", IfxTraceCategory.Leave);
//            }
        }

        private void UnDoCustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", IfxTraceCategory.Enter);
//   
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", IfxTraceCategory.Leave);
//            }
        }

        private void RefreshFieldsCustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", IfxTraceCategory.Enter);
//   
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", IfxTraceCategory.Leave);
//            }
        }

        private void NewEntityRowCustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", IfxTraceCategory.Enter);
//                //SetStandingFK();  //Unrem this when we have FKs
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", IfxTraceCategory.Leave);
//            }
        }

        private bool IsPropertyDirtyCustomCode(string propertyName)
        {
            ////  This is called from 'IsPropertyDirty' for calculated fields that are not represented in the wire objects.  They will always require custom code here.
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", IfxTraceCategory.Enter);
            //    switch (propertyName)
            //    {
            //        case "xxxx":
            //            //  return something
            //            break;
            //        default:
            //            throw new Exception(_as + "." + _cn + ".IsPropertyDirtyCustomCode:  Missing code and logic for " + propertyName + ".");
            //    }
                return false;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        public string GetPropertyValueByKeyCustomCode(string key)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", IfxTraceCategory.Enter);
            //    //switch (key)
            //    //{
            //    //    case "xxxx":
            //    //        return null;
            //    //        return xxx;
            //    //}
                return null;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        public string GetPropertyFormattedStringValueByKeyCustomCode(string key)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", IfxTraceCategory.Enter);
            //    //switch (key)
            //    //{
            //    //    case "xxxx":
            //    //        return null;
            //    //        return xxx;
            //    //}
                return null;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        #endregion Method Extentions For Custom Code


        #region Code Gen Methods For Modification to be copied to here



        ///// <summary>
        ///// 	<para>The Primary Key for WcStoredProc</para>
        ///// </summary>
        //public Guid? Sp_Id
        //{
        //    get
        //    {
        //        return _data.C.Sp_Id;
        //    }
        //}


        //public void Get_Children_WcStoredProcParam()
        //{
        //    if (Sp_Id != null)
        //    {
        //        _proxyWcStoredProcParamService.Begin_WcStoredProcParam_GetListByFK((Guid)Sp_Id);
        //    }
        //    else
        //    {
        //        // we should clear the list if there is one
        //    }
        //}


        //public void Get_Children_WcStoredProcColumn()
        //{
        //    if (Sp_Id != null)
        //    {
        //        _proxyWcStoredProcColumnService.Begin_WcStoredProcColumn_GetListByFK((Guid)Sp_Id);
        //    }
        //    else
        //    {
        //        // we should clear the list if there is one
        //    }
        //}

        //public void Get_Children_WcStoredProcParamValueGroup()
        //{
        //    if (Sp_Id != null)
        //    {
        //        _proxyWcStoredProcParamValueGroupService.Begin_WcStoredProcParamValueGroup_GetListByFK((Guid)Sp_Id);
        //    }
        //    else
        //    {
        //        // we should clear the list if there is one
        //    }
        //}


        #endregion Code Gen Methods For Modification to be copied to here


        #region Code Gen Properties For Modification to be copied to here


        #endregion Code Gen Properties For Modification to be copied to here


        #region Code Gen Validation Logic For Modification to be copied to here


        #endregion Code Gen Validation Logic For Modification to be copied to here


        #region Properties

        //public string DataRowName
        //{
        //    get { return Sp_Name; }
        //}

        #endregion Properties


        #region Validation Logic


        #endregion Validation Logic


        #region Custom Code


        //// import table from here
        //public void ImportSelectedTable(Guid connStringKeyId)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSelectedTable", IfxTraceCategory.Enter);

        //        if (Tb_Id != null || TableInDatabase == null)
        //        {
        //            // Do something?
        //            return;
        //        }

        //        _wcTableProxy.Begin_ExecuteWcTable_InsertTable_ByConnectionKeyId(connStringKeyId, TableInDatabase, (Guid)Credentials.UserId);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSelectedTable", ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSelectedTable", IfxTraceCategory.Leave);
        //    }
        //}




        #region ContextMenuColumn


        public IEnumerable GetContextMenuItemsSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetContextMenuItemsSource", IfxTraceCategory.Enter);

                var itemModel1 = new MenuItemModel
                {
                    Name = "Import- Get Resultset Entity",
                    Command = new ActionCommand(() => ImportUpdateSproc("GetResultsetEntity")),
                    //ImageSource = "/vImages;component/Images/Velocity/Report16.png"
                };

                var itemModel2 = new MenuItemModel
                {
                    Name = "Import- Get Resultset ComboItemList",
                    Command = new ActionCommand(() => ImportUpdateSproc("GetResultsetComboItemList")),
                    //ImageSource = "/vImages;component/Images/Velocity/Report16.png"
                };

                var itemModel3 = new MenuItemModel
                {
                    Name = "Import- Get Resultset ComboItemList Static",
                    Command = new ActionCommand(() => ImportUpdateSproc("GetResultsetComboItemListStatic")),
                    //ImageSource = "/vImages;component/Images/Velocity/Report16.png"
                };

                var itemModel4 = new MenuItemModel
                {
                    Name = "Import- Get Resultset Binding WireType",
                    Command = new ActionCommand(() => ImportUpdateSproc("GetResultsetBindingWireType")),
                    //ImageSource = "/vImages;component/Images/Velocity/Report16.png"
                };

                var itemModel5 = new MenuItemModel
                {
                    Name = "Import- Execute Params, ObjectArray",
                    Command = new ActionCommand(() => ImportUpdateSproc("ExecuteParams_ObjectArray")),
                    //ImageSource = "/vImages;component/Images/Velocity/Report16.png"
                };

                var itemModel6 = new MenuItemModel
                {
                    Name = "Import- Execute  Params, Parameter",
                    Command = new ActionCommand(() => ImportUpdateSproc("ExecuteParams_Parameter")),
                    //ImageSource = "/vImages;component/Images/Velocity/Report16.png"
                };

                var itemModel7 = new MenuItemModel
                {
                    Name = "Refresh Params",
                    Command = new ActionCommand(() => ImportUpdateSproc("RefreshParams")),
                    //ImageSource = "/vImages;component/Images/Velocity/Report16.png"
                };

                var itemModel8 = new MenuItemModel
                {
                    Name = "Delete-Import Params",
                    Command = new ActionCommand(() => ImportUpdateSproc("DeleteImportParams")),
                    //ImageSource = "/vImages;component/Images/Velocity/Report16.png"
                };

                var itemModel9 = new MenuItemModel
                {
                    Name = "Delete-Re-Import Param Value Group",
                    Command = new ActionCommand(() => ImportUpdateSproc("DeleteReImportParamsAndGroup")),
                    //ImageSource = "/vImages;component/Images/Velocity/Report16.png"
                };

                var itemModel10 = new MenuItemModel
                {
                    Name = "Refresh Column",
                    Command = new ActionCommand(() => ImportUpdateSproc("RefreshColumn")),
                    //ImageSource = "/vImages;component/Images/Velocity/Report16.png"
                };

                var itemModel11 = new MenuItemModel
                {
                    Name = "Delete-Import Columns",
                    Command = new ActionCommand(() => ImportUpdateSproc("DeleteImportColumns")),
                    //ImageSource = "/vImages;component/Images/Velocity/Report16.png"
                };

                var itemModel12 = new MenuItemModel
                {
                    Name = "Refresh Stored Procedure Row",
                    Command = new ActionCommand(() => ImportUpdateSproc("RefreshRow")),
                    //ImageSource = "/vImages;component/Images/Velocity/Report16.png"
                };

                return new ObservableCollection<MenuItemModel> { itemModel1, itemModel2, itemModel3, itemModel4, itemModel5, itemModel6, itemModel7, itemModel8, itemModel9, itemModel10, itemModel11, itemModel12 };
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetContextMenuItemsSource", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetContextMenuItemsSource", IfxTraceCategory.Leave);
            }
        }

        class SprocImportType
        {
            public const int GetResultsetEntity = 1;
            public const int GetResultsetComboItemList = 2;
            public const int GetResultsetComboItemListStatic = 3;
            public const int GetResultsetBindingWireType = 4;
            public const int ExecuteParams_ObjectArray = 5;
            public const int ExecuteParams_Parameter = 6;
        }

        class ParamRefreshType
        {
            public const int Refresh = 1;
            public const int DeleteReImportGroups = 2;
            public const int DeleteReImportGroupsAndParams = 3;
        }

        class ColumnRefreshType
        {
            public const int Refresh = 1;
            public const int DeleteReImport = 2;
        }

        void ImportUpdateSproc(string menuItem)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportUpdateSproc", IfxTraceCategory.Enter);
            
      
                switch (menuItem)
                {
                    case "GetResultsetEntity":
                        WcStoredProcProxy.Begin_ExecuteWcStoredProc_importFromSourceDb((Guid)Sp_ApVrsn_Id, Sp_DbCnSK_Id, SprocInDb, 
                            SprocImportType.GetResultsetEntity, (Guid)Credentials.UserId, Sp_Id);
                        break;
                    case "GetResultsetComboItemList":
                        WcStoredProcProxy.Begin_ExecuteWcStoredProc_importFromSourceDb((Guid)Sp_ApVrsn_Id, Sp_DbCnSK_Id, SprocInDb,
                            SprocImportType.GetResultsetComboItemList, (Guid)Credentials.UserId, Sp_Id);
                        break;
                    case "GetResultsetComboItemListStatic":
                        WcStoredProcProxy.Begin_ExecuteWcStoredProc_importFromSourceDb((Guid)Sp_ApVrsn_Id, Sp_DbCnSK_Id, SprocInDb,
                            SprocImportType.GetResultsetComboItemListStatic, (Guid)Credentials.UserId, Sp_Id);
                        break;
                    case "GetResultsetBindingWireType":
                        WcStoredProcProxy.Begin_ExecuteWcStoredProc_importFromSourceDb((Guid)Sp_ApVrsn_Id, Sp_DbCnSK_Id, SprocInDb,
                            SprocImportType.GetResultsetBindingWireType, (Guid)Credentials.UserId, Sp_Id);
                        break;
                    case "ExecuteParams_ObjectArray":
                        WcStoredProcProxy.Begin_ExecuteWcStoredProc_importFromSourceDb((Guid)Sp_ApVrsn_Id, Sp_DbCnSK_Id, SprocInDb,
                            SprocImportType.ExecuteParams_ObjectArray, (Guid)Credentials.UserId, Sp_Id);
                        break;
                    case "ExecuteParams_Parameter":
                        WcStoredProcProxy.Begin_ExecuteWcStoredProc_importFromSourceDb((Guid)Sp_ApVrsn_Id, Sp_DbCnSK_Id, SprocInDb,
                            SprocImportType.ExecuteParams_Parameter, (Guid)Credentials.UserId, Sp_Id);
                        break;
                    case "RefreshParams":
                        WcStoredProcProxy.Begin_ExecuteWcStoredProc_ParamAndValueGroup_Refresh(Sp_Id, ParamRefreshType.Refresh,
                            (Guid) Credentials.UserId);
                        break;
                    case "DeleteImportParams":
                        WcStoredProcProxy.Begin_ExecuteWcStoredProc_ParamAndValueGroup_Refresh(Sp_Id, ParamRefreshType.DeleteReImportGroups,
                            (Guid)Credentials.UserId);
                        break;
                    case "DeleteReImportParamsAndGroup":
                        WcStoredProcProxy.Begin_ExecuteWcStoredProc_ParamAndValueGroup_Refresh(Sp_Id, ParamRefreshType.DeleteReImportGroupsAndParams,
                            (Guid)Credentials.UserId);
                        break;
                    case "RefreshColumn":
                        WcStoredProcProxy.Begin_RefreshStoredProcedureColumns(Sp_Id, Sp_DbCnSK_Id, (Guid)Credentials.UserId, ColumnRefreshType.Refresh);
                        break;
                    case "DeleteImportColumns":
                        WcStoredProcProxy.Begin_RefreshStoredProcedureColumns(Sp_Id, Sp_DbCnSK_Id, (Guid)Credentials.UserId, ColumnRefreshType.DeleteReImport);
                        break;
                    case "RefreshRow":
                        GetEntityRow(Sp_Id);
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportUpdateSproc", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportUpdateSproc", IfxTraceCategory.Leave);
            }
        }


        public bool IsContextMenuButtonAllowed
        {
            get { return true; }
        }




        private void WcStoredProcProxy_ExecuteWcStoredProc_importFromSourceDbCompleted(object sender, ExecuteWcStoredProc_importFromSourceDbCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcProxy_ExecuteWcStoredProc_importFromSourceDbCompleted", IfxTraceCategory.Enter);
                object[] data = e.Result;
                Guid? sp_Id = data[0] as Guid?;
                Int32? success = data[1] as int?;

                if (success != 1)
                {
                    // The import failed.
                    // Do something.
                    MessageBox.Show("Ooops, there was a problem.", "", MessageBoxButton.OK);
                }
                else
                {
                    GetEntityRow((Guid)Sp_Id);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcProxy_ExecuteWcStoredProc_importFromSourceDbCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcProxy_ExecuteWcStoredProc_importFromSourceDbCompleted", IfxTraceCategory.Leave);
            }
        }

        private void WcStoredProcProxy_ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompleted(object sender, ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcProxy_ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompleted", IfxTraceCategory.Enter);
                object[] data = e.Result;
                Guid? sp_Id = data[0] as Guid?;
                Int32? success = data[1] as int?;

                if (success != 1)
                {
                    // The import failed.
                    // Do something.
                    MessageBox.Show("Ooops, there was a problem.", "", MessageBoxButton.OK);
                }
                else
                {
                    GetEntityRow((Guid)Sp_Id);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcProxy_ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcProxy_ExecuteWcStoredProc_ParamAndValueGroup_RefreshCompleted", IfxTraceCategory.Leave);
            }
        }


        private void WcStoredProcProxy_RefreshStoredProcedureColumnsCompleted(object sender, RefreshStoredProcedureColumnsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcProxy_ExecuteWcStoredProc_importFromSourceDbCompleted", IfxTraceCategory.Enter);
                int? success = e.Result;

                if (success != 1)
                {
                    // The import failed.
                    // Do something.
                    MessageBox.Show("Ooops, there was a problem.", "", MessageBoxButton.OK);
                }
                else
                {
                    GetEntityRow((Guid)Sp_Id);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcProxy_ExecuteWcStoredProc_importFromSourceDbCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcProxy_ExecuteWcStoredProc_importFromSourceDbCompleted", IfxTraceCategory.Leave);
            }
        }


        #endregion ContextMenuColumn








        #endregion Custom Code


        #region ITraceItem Members

        void GetTraceItemsShortListCustom()
        {
            //  Add custom trace items here.
            //_traceItems.Add("xxxxxxxxx", xxxxxxxxx, TraceDataTypes.String);
            //_traceItems.Add("SomeChildObject", new SomeChildObject(), TraceDataTypes.ITraceItem);
        }

        void GetTraceItemsLongListCustom()
        {
            //  Add custom trace items here.
            //_traceItems.Add("xxxxxxxxx", xxxxxxxxx.ToString, TraceDataTypes.String);
            //_traceItems.Add("SomeChildObject", new SomeChildObject(), TraceDataTypes.ITraceItem);
        }

        #endregion ITraceItem Members
    }

}


