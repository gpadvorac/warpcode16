using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/27/2017 7:57:30 PM

namespace EntityBll.SL
{
    public partial class WcGridColumnGroup_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_GrdColGrp_Name = 50;
		public const int STRINGSIZE_GrdColGrp_HeaderText = 50;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_GrdColGrp_Id_Required = "Id is a required field.";
		private const string BROKENRULE_GrdColGrp_Sort_Required = "Sort is a required field.";
		private const string BROKENRULE_GrdColGrp_Sort_ZeroNotAllowed = "Sort:  '0'  (zero) is not allowed.";
		private string BROKENRULE_GrdColGrp_Name_TextLength = "System Name has a maximum text length of  '" + STRINGSIZE_GrdColGrp_Name + "'.";
		private const string BROKENRULE_GrdColGrp_Name_Required = "System Name is a required field.";
		private string BROKENRULE_GrdColGrp_HeaderText_TextLength = "Group Name has a maximum text length of  '" + STRINGSIZE_GrdColGrp_HeaderText + "'.";
		private const string BROKENRULE_GrdColGrp_HeaderText_Required = "Group Name is a required field.";
		private const string BROKENRULE_GrdColGrp_IsDefaultView_Required = "Is Default View is a required field.";
		private const string BROKENRULE_GrdColGrp_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_GrdColGrp_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_GrdColGrp_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_Id", BROKENRULE_GrdColGrp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_Tb_Id", BROKENRULE_GrdColGrp_Tb_Id_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_Sort", BROKENRULE_GrdColGrp_Sort_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_Sort", BROKENRULE_GrdColGrp_Sort_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_Name", BROKENRULE_GrdColGrp_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_Name", BROKENRULE_GrdColGrp_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_HeaderText", BROKENRULE_GrdColGrp_HeaderText_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_HeaderText", BROKENRULE_GrdColGrp_HeaderText_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_IsDefaultView", BROKENRULE_GrdColGrp_IsDefaultView_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_IsActiveRow", BROKENRULE_GrdColGrp_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_IsDeleted", BROKENRULE_GrdColGrp_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_Stamp", BROKENRULE_GrdColGrp_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void GrdColGrp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_Id");
                string newBrokenRules = "";
                
                if (GrdColGrp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_Id", BROKENRULE_GrdColGrp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("GrdColGrp_Id", BROKENRULE_GrdColGrp_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("GrdColGrp_Id", _brokenRuleManager.IsPropertyValid("GrdColGrp_Id"), IsPropertyDirty("GrdColGrp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void GrdColGrp_Tb_Id_Validate()
        {
        }

        private void GrdColGrp_Sort_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_Sort_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_Sort");
                string newBrokenRules = "";
                
                if (GrdColGrp_Sort == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_Sort", BROKENRULE_GrdColGrp_Sort_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("GrdColGrp_Sort", BROKENRULE_GrdColGrp_Sort_Required);
                }

                if (GrdColGrp_Sort == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_Sort", BROKENRULE_GrdColGrp_Sort_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("GrdColGrp_Sort", BROKENRULE_GrdColGrp_Sort_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_Sort");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("GrdColGrp_Sort", _brokenRuleManager.IsPropertyValid("GrdColGrp_Sort"), IsPropertyDirty("GrdColGrp_Sort"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_Sort_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_Sort_Validate", IfxTraceCategory.Leave);
            }
        }

        private void GrdColGrp_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (GrdColGrp_Name != null)
                {
                    len = GrdColGrp_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_Name", BROKENRULE_GrdColGrp_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("GrdColGrp_Name", BROKENRULE_GrdColGrp_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("GrdColGrp_Name", BROKENRULE_GrdColGrp_Name_Required);
                    if (len > STRINGSIZE_GrdColGrp_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_Name", BROKENRULE_GrdColGrp_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("GrdColGrp_Name", BROKENRULE_GrdColGrp_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("GrdColGrp_Name", _brokenRuleManager.IsPropertyValid("GrdColGrp_Name"), IsPropertyDirty("GrdColGrp_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void GrdColGrp_HeaderText_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_HeaderText_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_HeaderText");
                string newBrokenRules = "";
                				int len = 0;
                if (GrdColGrp_HeaderText != null)
                {
                    len = GrdColGrp_HeaderText.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_HeaderText", BROKENRULE_GrdColGrp_HeaderText_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("GrdColGrp_HeaderText", BROKENRULE_GrdColGrp_HeaderText_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("GrdColGrp_HeaderText", BROKENRULE_GrdColGrp_HeaderText_Required);
                    if (len > STRINGSIZE_GrdColGrp_HeaderText)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_HeaderText", BROKENRULE_GrdColGrp_HeaderText_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("GrdColGrp_HeaderText", BROKENRULE_GrdColGrp_HeaderText_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_HeaderText");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("GrdColGrp_HeaderText", _brokenRuleManager.IsPropertyValid("GrdColGrp_HeaderText"), IsPropertyDirty("GrdColGrp_HeaderText"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_HeaderText_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_HeaderText_Validate", IfxTraceCategory.Leave);
            }
        }

        private void GrdColGrp_IsDefaultView_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_IsDefaultView_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_IsDefaultView");
                string newBrokenRules = "";
                
                if (GrdColGrp_IsDefaultView == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_IsDefaultView", BROKENRULE_GrdColGrp_IsDefaultView_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("GrdColGrp_IsDefaultView", BROKENRULE_GrdColGrp_IsDefaultView_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_IsDefaultView");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("GrdColGrp_IsDefaultView", _brokenRuleManager.IsPropertyValid("GrdColGrp_IsDefaultView"), IsPropertyDirty("GrdColGrp_IsDefaultView"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_IsDefaultView_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_IsDefaultView_Validate", IfxTraceCategory.Leave);
            }
        }

        private void GrdColGrp_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_IsActiveRow");
                string newBrokenRules = "";
                
                if (GrdColGrp_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_IsActiveRow", BROKENRULE_GrdColGrp_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("GrdColGrp_IsActiveRow", BROKENRULE_GrdColGrp_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("GrdColGrp_IsActiveRow", _brokenRuleManager.IsPropertyValid("GrdColGrp_IsActiveRow"), IsPropertyDirty("GrdColGrp_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void GrdColGrp_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_IsDeleted");
                string newBrokenRules = "";
                
                if (GrdColGrp_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_IsDeleted", BROKENRULE_GrdColGrp_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("GrdColGrp_IsDeleted", BROKENRULE_GrdColGrp_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("GrdColGrp_IsDeleted", _brokenRuleManager.IsPropertyValid("GrdColGrp_IsDeleted"), IsPropertyDirty("GrdColGrp_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void GrdColGrp_CreatedUserId_Validate()
        {
        }

        private void GrdColGrp_CreatedDate_Validate()
        {
        }

        private void GrdColGrp_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void GrdColGrp_LastModifiedDate_Validate()
        {
        }

        private void GrdColGrp_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_Stamp");
                string newBrokenRules = "";
                
                if (GrdColGrp_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("GrdColGrp_Stamp", BROKENRULE_GrdColGrp_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("GrdColGrp_Stamp", BROKENRULE_GrdColGrp_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("GrdColGrp_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("GrdColGrp_Stamp", _brokenRuleManager.IsPropertyValid("GrdColGrp_Stamp"), IsPropertyDirty("GrdColGrp_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GrdColGrp_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


