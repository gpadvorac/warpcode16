using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/18/2017 10:13:09 PM

namespace EntityBll.SL
{
    public partial class WcTable_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_Tb_Name = 100;
		public const int STRINGSIZE_Tb_EntityRootName = 75;
		public const int STRINGSIZE_Tb_VariableName = 100;
		public const int STRINGSIZE_Tb_ScreenCaption = 100;
		public const int STRINGSIZE_Tb_Description = 2000;
		public const int STRINGSIZE_Tb_DevelopmentNote = 2000;
		public const int STRINGSIZE_Tb_UIAssemblyName = 75;
		public const int STRINGSIZE_Tb_UINamespace = 75;
		public const int STRINGSIZE_Tb_UIAssemblyPath = 250;
		public const int STRINGSIZE_Tb_WebServiceName = 100;
		public const int STRINGSIZE_Tb_WebServiceFolder = 100;
		public const int STRINGSIZE_Tb_DataServiceAssemblyName = 100;
		public const int STRINGSIZE_Tb_DataServicePath = 250;
		public const int STRINGSIZE_Tb_WireTypeAssemblyName = 100;
		public const int STRINGSIZE_Tb_WireTypePath = 250;
		public const int STRINGSIZE_Tb_TableGroups = 500;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_Tb_Id_Required = "Id is a required field.";
		private const string BROKENRULE_Tb_ApVrsn_Id_Required = "ApVrsn_Id is a required field.";
		private const string BROKENRULE_AttachmentCount_ZeroNotAllowed = "Attachments:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_DiscussionCount_ZeroNotAllowed = "Discussions:  '0'  (zero) is not allowed.";
		private string BROKENRULE_Tb_Name_TextLength = "Db Table Name has a maximum text length of  '" + STRINGSIZE_Tb_Name + "'.";
		private const string BROKENRULE_Tb_Name_Required = "Db Table Name is a required field.";
		private string BROKENRULE_Tb_EntityRootName_TextLength = "Entity Root Name has a maximum text length of  '" + STRINGSIZE_Tb_EntityRootName + "'.";
		private const string BROKENRULE_Tb_EntityRootName_Required = "Entity Root Name is a required field.";
		private string BROKENRULE_Tb_VariableName_TextLength = "Variable Name has a maximum text length of  '" + STRINGSIZE_Tb_VariableName + "'.";
		private const string BROKENRULE_Tb_VariableName_Required = "Variable Name is a required field.";
		private string BROKENRULE_Tb_ScreenCaption_TextLength = "Screen Caption has a maximum text length of  '" + STRINGSIZE_Tb_ScreenCaption + "'.";
		private const string BROKENRULE_Tb_ScreenCaption_Required = "Screen Caption is a required field.";
		private string BROKENRULE_Tb_Description_TextLength = "Description has a maximum text length of  '" + STRINGSIZE_Tb_Description + "'.";
		private string BROKENRULE_Tb_DevelopmentNote_TextLength = "Development Notes has a maximum text length of  '" + STRINGSIZE_Tb_DevelopmentNote + "'.";
		private string BROKENRULE_Tb_UIAssemblyName_TextLength = "UI Assembly Name has a maximum text length of  '" + STRINGSIZE_Tb_UIAssemblyName + "'.";
		private string BROKENRULE_Tb_UINamespace_TextLength = "UI Namespace has a maximum text length of  '" + STRINGSIZE_Tb_UINamespace + "'.";
		private string BROKENRULE_Tb_UIAssemblyPath_TextLength = "UI Assembly Path has a maximum text length of  '" + STRINGSIZE_Tb_UIAssemblyPath + "'.";
		private string BROKENRULE_Tb_WebServiceName_TextLength = "Web Service Name has a maximum text length of  '" + STRINGSIZE_Tb_WebServiceName + "'.";
		private string BROKENRULE_Tb_WebServiceFolder_TextLength = "Web Service Folder has a maximum text length of  '" + STRINGSIZE_Tb_WebServiceFolder + "'.";
		private string BROKENRULE_Tb_DataServiceAssemblyName_TextLength = "DataService Assembly Name has a maximum text length of  '" + STRINGSIZE_Tb_DataServiceAssemblyName + "'.";
		private string BROKENRULE_Tb_DataServicePath_TextLength = "Data Service Path has a maximum text length of  '" + STRINGSIZE_Tb_DataServicePath + "'.";
		private string BROKENRULE_Tb_WireTypeAssemblyName_TextLength = "WireType Assembly Name has a maximum text length of  '" + STRINGSIZE_Tb_WireTypeAssemblyName + "'.";
		private string BROKENRULE_Tb_WireTypePath_TextLength = "WireType Path has a maximum text length of  '" + STRINGSIZE_Tb_WireTypePath + "'.";
		private const string BROKENRULE_Tb_UseTilesInPropsScreen_Required = "Use Tiles In Props Screen is a required field.";
		private const string BROKENRULE_Tb_UseGridColumnGroups_Required = "Use Grid Column Groups is a required field.";
		private const string BROKENRULE_Tb_UseGridDataSourceCombo_Required = "Use Grid DataSource Combo is a required field.";
		private const string BROKENRULE_Tb_UseLegacyConnectionCode_Required = "Use Legacy Connection Code is a required field.";
		private const string BROKENRULE_Tb_PkIsIdentity_Required = "Pk Is Identity is a required field.";
		private const string BROKENRULE_Tb_IsVirtual_Required = "Is Virtual Entity is a required field.";
		private const string BROKENRULE_Tb_IsNotEntity_Required = "Is Not An Entity is a required field.";
		private const string BROKENRULE_Tb_IsMany2Many_Required = "Is Many 2 Many is a required field.";
		private const string BROKENRULE_Tb_UseLastModifiedByUserNameInSproc_Required = "Pull UserName In Sproc is a required field.";
		private const string BROKENRULE_Tb_UseUserTimeStamp_Required = "Use User TimeStamp is a required field.";
		private const string BROKENRULE_Tb_UseForAudit_Required = "Use For Audit is a required field.";
		private const string BROKENRULE_Tb_IsInputComplete_Required = "Input Complete is a required field.";
		private const string BROKENRULE_Tb_IsCodeGen_Required = "Is CodeGen is a required field.";
		private const string BROKENRULE_Tb_IsReadyCodeGen_Required = "Is Ready For CodeGen is a required field.";
		private const string BROKENRULE_Tb_IsCodeGenComplete_Required = "Is CodeGen Complete is a required field.";
		private const string BROKENRULE_Tb_IsTagForCodeGen_Required = "Tag For CodeGen is a required field.";
		private const string BROKENRULE_Tb_IsTagForOther_Required = "Is Tag For Other is a required field.";
		private string BROKENRULE_Tb_TableGroups_TextLength = "Table Groups has a maximum text length of  '" + STRINGSIZE_Tb_TableGroups + "'.";
		private const string BROKENRULE_Tb_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_Tb_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_Tb_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_Id", BROKENRULE_Tb_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_ApVrsn_Id", BROKENRULE_Tb_ApVrsn_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("AttachmentFileNames", BROKENRULE_AttachmentFileNames_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("DiscussionTitles", BROKENRULE_DiscussionTitles_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_Description", BROKENRULE_Tb_Description_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_DevelopmentNote", BROKENRULE_Tb_DevelopmentNote_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_UIAssemblyName", BROKENRULE_Tb_UIAssemblyName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_UINamespace", BROKENRULE_Tb_UINamespace_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_UIAssemblyPath", BROKENRULE_Tb_UIAssemblyPath_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_WebServiceName", BROKENRULE_Tb_WebServiceName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_WebServiceFolder", BROKENRULE_Tb_WebServiceFolder_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_DataServiceAssemblyName", BROKENRULE_Tb_DataServiceAssemblyName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_DataServicePath", BROKENRULE_Tb_DataServicePath_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_WireTypeAssemblyName", BROKENRULE_Tb_WireTypeAssemblyName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_WireTypePath", BROKENRULE_Tb_WireTypePath_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_UseTilesInPropsScreen", BROKENRULE_Tb_UseTilesInPropsScreen_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_UseGridColumnGroups", BROKENRULE_Tb_UseGridColumnGroups_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_UseGridDataSourceCombo", BROKENRULE_Tb_UseGridDataSourceCombo_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_UseLegacyConnectionCode", BROKENRULE_Tb_UseLegacyConnectionCode_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_PkIsIdentity", BROKENRULE_Tb_PkIsIdentity_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsVirtual", BROKENRULE_Tb_IsVirtual_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsNotEntity", BROKENRULE_Tb_IsNotEntity_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsMany2Many", BROKENRULE_Tb_IsMany2Many_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_UseLastModifiedByUserNameInSproc", BROKENRULE_Tb_UseLastModifiedByUserNameInSproc_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_UseUserTimeStamp", BROKENRULE_Tb_UseUserTimeStamp_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_UseForAudit", BROKENRULE_Tb_UseForAudit_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsInputComplete", BROKENRULE_Tb_IsInputComplete_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsCodeGen", BROKENRULE_Tb_IsCodeGen_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsReadyCodeGen", BROKENRULE_Tb_IsReadyCodeGen_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsCodeGenComplete", BROKENRULE_Tb_IsCodeGenComplete_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsTagForCodeGen", BROKENRULE_Tb_IsTagForCodeGen_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsTagForOther", BROKENRULE_Tb_IsTagForOther_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_TableGroups", BROKENRULE_Tb_TableGroups_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsActiveRow", BROKENRULE_Tb_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsDeleted", BROKENRULE_Tb_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_Stamp", BROKENRULE_Tb_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void Tb_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Id");
                string newBrokenRules = "";
                
                if (Tb_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_Id", BROKENRULE_Tb_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_Id", BROKENRULE_Tb_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_Id", _brokenRuleManager.IsPropertyValid("Tb_Id"), IsPropertyDirty("Tb_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_ApVrsn_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ApVrsn_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_ApVrsn_Id");
                string newBrokenRules = "";
                
                if (Tb_ApVrsn_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_ApVrsn_Id", BROKENRULE_Tb_ApVrsn_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_ApVrsn_Id", BROKENRULE_Tb_ApVrsn_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_ApVrsn_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_ApVrsn_Id", _brokenRuleManager.IsPropertyValid("Tb_ApVrsn_Id"), IsPropertyDirty("Tb_ApVrsn_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ApVrsn_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ApVrsn_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_auditTable_Id_Validate()
        {
        }

        private void AttachmentCount_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("AttachmentCount");
                string newBrokenRules = "";
                
                if (AttachmentCount == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("AttachmentCount");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("AttachmentCount", _brokenRuleManager.IsPropertyValid("AttachmentCount"), IsPropertyDirty("AttachmentCount"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", IfxTraceCategory.Leave);
            }
        }

        private void AttachmentFileNames_Validate()
        {
        }

        private void DiscussionCount_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DiscussionCount");
                string newBrokenRules = "";
                
                if (DiscussionCount == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DiscussionCount");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DiscussionCount", _brokenRuleManager.IsPropertyValid("DiscussionCount"), IsPropertyDirty("DiscussionCount"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DiscussionTitles_Validate()
        {
        }

        private void Tb_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_Name != null)
                {
                    len = Tb_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_Required);
                    if (len > STRINGSIZE_Tb_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_Name", _brokenRuleManager.IsPropertyValid("Tb_Name"), IsPropertyDirty("Tb_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_EntityRootName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_EntityRootName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_EntityRootName");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_EntityRootName != null)
                {
                    len = Tb_EntityRootName.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_Required);
                    if (len > STRINGSIZE_Tb_EntityRootName)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_EntityRootName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_EntityRootName", _brokenRuleManager.IsPropertyValid("Tb_EntityRootName"), IsPropertyDirty("Tb_EntityRootName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_EntityRootName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_EntityRootName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_VariableName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_VariableName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_VariableName");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_VariableName != null)
                {
                    len = Tb_VariableName.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_Required);
                    if (len > STRINGSIZE_Tb_VariableName)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_VariableName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_VariableName", _brokenRuleManager.IsPropertyValid("Tb_VariableName"), IsPropertyDirty("Tb_VariableName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_VariableName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_VariableName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_ScreenCaption_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ScreenCaption_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_ScreenCaption");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_ScreenCaption != null)
                {
                    len = Tb_ScreenCaption.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_Required);
                    if (len > STRINGSIZE_Tb_ScreenCaption)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_ScreenCaption");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_ScreenCaption", _brokenRuleManager.IsPropertyValid("Tb_ScreenCaption"), IsPropertyDirty("Tb_ScreenCaption"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ScreenCaption_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ScreenCaption_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_Description_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Description_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Description");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_Description != null)
                {
                    len = Tb_Description.Length;
                }

                if (len > STRINGSIZE_Tb_Description)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_Description", BROKENRULE_Tb_Description_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_Description", BROKENRULE_Tb_Description_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Description");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_Description", _brokenRuleManager.IsPropertyValid("Tb_Description"), IsPropertyDirty("Tb_Description"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Description_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Description_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_DevelopmentNote_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DevelopmentNote_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_DevelopmentNote");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_DevelopmentNote != null)
                {
                    len = Tb_DevelopmentNote.Length;
                }

                if (len > STRINGSIZE_Tb_DevelopmentNote)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_DevelopmentNote", BROKENRULE_Tb_DevelopmentNote_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_DevelopmentNote", BROKENRULE_Tb_DevelopmentNote_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_DevelopmentNote");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_DevelopmentNote", _brokenRuleManager.IsPropertyValid("Tb_DevelopmentNote"), IsPropertyDirty("Tb_DevelopmentNote"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DevelopmentNote_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DevelopmentNote_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_UIAssemblyName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UIAssemblyName");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_UIAssemblyName != null)
                {
                    len = Tb_UIAssemblyName.Length;
                }

                if (len > STRINGSIZE_Tb_UIAssemblyName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_UIAssemblyName", BROKENRULE_Tb_UIAssemblyName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_UIAssemblyName", BROKENRULE_Tb_UIAssemblyName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UIAssemblyName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_UIAssemblyName", _brokenRuleManager.IsPropertyValid("Tb_UIAssemblyName"), IsPropertyDirty("Tb_UIAssemblyName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_UINamespace_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UINamespace_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UINamespace");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_UINamespace != null)
                {
                    len = Tb_UINamespace.Length;
                }

                if (len > STRINGSIZE_Tb_UINamespace)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_UINamespace", BROKENRULE_Tb_UINamespace_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_UINamespace", BROKENRULE_Tb_UINamespace_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UINamespace");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_UINamespace", _brokenRuleManager.IsPropertyValid("Tb_UINamespace"), IsPropertyDirty("Tb_UINamespace"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UINamespace_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UINamespace_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_UIAssemblyPath_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyPath_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UIAssemblyPath");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_UIAssemblyPath != null)
                {
                    len = Tb_UIAssemblyPath.Length;
                }

                if (len > STRINGSIZE_Tb_UIAssemblyPath)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_UIAssemblyPath", BROKENRULE_Tb_UIAssemblyPath_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_UIAssemblyPath", BROKENRULE_Tb_UIAssemblyPath_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UIAssemblyPath");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_UIAssemblyPath", _brokenRuleManager.IsPropertyValid("Tb_UIAssemblyPath"), IsPropertyDirty("Tb_UIAssemblyPath"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyPath_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyPath_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_WebServiceName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WebServiceName");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_WebServiceName != null)
                {
                    len = Tb_WebServiceName.Length;
                }

                if (len > STRINGSIZE_Tb_WebServiceName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_WebServiceName", BROKENRULE_Tb_WebServiceName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_WebServiceName", BROKENRULE_Tb_WebServiceName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WebServiceName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_WebServiceName", _brokenRuleManager.IsPropertyValid("Tb_WebServiceName"), IsPropertyDirty("Tb_WebServiceName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_WebServiceFolder_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceFolder_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WebServiceFolder");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_WebServiceFolder != null)
                {
                    len = Tb_WebServiceFolder.Length;
                }

                if (len > STRINGSIZE_Tb_WebServiceFolder)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_WebServiceFolder", BROKENRULE_Tb_WebServiceFolder_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_WebServiceFolder", BROKENRULE_Tb_WebServiceFolder_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WebServiceFolder");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_WebServiceFolder", _brokenRuleManager.IsPropertyValid("Tb_WebServiceFolder"), IsPropertyDirty("Tb_WebServiceFolder"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceFolder_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceFolder_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_DataServiceAssemblyName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServiceAssemblyName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_DataServiceAssemblyName");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_DataServiceAssemblyName != null)
                {
                    len = Tb_DataServiceAssemblyName.Length;
                }

                if (len > STRINGSIZE_Tb_DataServiceAssemblyName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_DataServiceAssemblyName", BROKENRULE_Tb_DataServiceAssemblyName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_DataServiceAssemblyName", BROKENRULE_Tb_DataServiceAssemblyName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_DataServiceAssemblyName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_DataServiceAssemblyName", _brokenRuleManager.IsPropertyValid("Tb_DataServiceAssemblyName"), IsPropertyDirty("Tb_DataServiceAssemblyName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServiceAssemblyName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServiceAssemblyName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_DataServicePath_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServicePath_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_DataServicePath");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_DataServicePath != null)
                {
                    len = Tb_DataServicePath.Length;
                }

                if (len > STRINGSIZE_Tb_DataServicePath)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_DataServicePath", BROKENRULE_Tb_DataServicePath_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_DataServicePath", BROKENRULE_Tb_DataServicePath_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_DataServicePath");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_DataServicePath", _brokenRuleManager.IsPropertyValid("Tb_DataServicePath"), IsPropertyDirty("Tb_DataServicePath"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServicePath_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServicePath_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_WireTypeAssemblyName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypeAssemblyName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WireTypeAssemblyName");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_WireTypeAssemblyName != null)
                {
                    len = Tb_WireTypeAssemblyName.Length;
                }

                if (len > STRINGSIZE_Tb_WireTypeAssemblyName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_WireTypeAssemblyName", BROKENRULE_Tb_WireTypeAssemblyName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_WireTypeAssemblyName", BROKENRULE_Tb_WireTypeAssemblyName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WireTypeAssemblyName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_WireTypeAssemblyName", _brokenRuleManager.IsPropertyValid("Tb_WireTypeAssemblyName"), IsPropertyDirty("Tb_WireTypeAssemblyName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypeAssemblyName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypeAssemblyName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_WireTypePath_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypePath_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WireTypePath");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_WireTypePath != null)
                {
                    len = Tb_WireTypePath.Length;
                }

                if (len > STRINGSIZE_Tb_WireTypePath)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_WireTypePath", BROKENRULE_Tb_WireTypePath_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_WireTypePath", BROKENRULE_Tb_WireTypePath_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WireTypePath");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_WireTypePath", _brokenRuleManager.IsPropertyValid("Tb_WireTypePath"), IsPropertyDirty("Tb_WireTypePath"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypePath_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypePath_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_UseTilesInPropsScreen_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseTilesInPropsScreen_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseTilesInPropsScreen");
                string newBrokenRules = "";
                
                if (Tb_UseTilesInPropsScreen == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_UseTilesInPropsScreen", BROKENRULE_Tb_UseTilesInPropsScreen_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_UseTilesInPropsScreen", BROKENRULE_Tb_UseTilesInPropsScreen_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseTilesInPropsScreen");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_UseTilesInPropsScreen", _brokenRuleManager.IsPropertyValid("Tb_UseTilesInPropsScreen"), IsPropertyDirty("Tb_UseTilesInPropsScreen"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseTilesInPropsScreen_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseTilesInPropsScreen_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_UseGridColumnGroups_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseGridColumnGroups_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseGridColumnGroups");
                string newBrokenRules = "";
                
                if (Tb_UseGridColumnGroups == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_UseGridColumnGroups", BROKENRULE_Tb_UseGridColumnGroups_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_UseGridColumnGroups", BROKENRULE_Tb_UseGridColumnGroups_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseGridColumnGroups");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_UseGridColumnGroups", _brokenRuleManager.IsPropertyValid("Tb_UseGridColumnGroups"), IsPropertyDirty("Tb_UseGridColumnGroups"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseGridColumnGroups_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseGridColumnGroups_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_UseGridDataSourceCombo_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseGridDataSourceCombo_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseGridDataSourceCombo");
                string newBrokenRules = "";
                
                if (Tb_UseGridDataSourceCombo == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_UseGridDataSourceCombo", BROKENRULE_Tb_UseGridDataSourceCombo_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_UseGridDataSourceCombo", BROKENRULE_Tb_UseGridDataSourceCombo_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseGridDataSourceCombo");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_UseGridDataSourceCombo", _brokenRuleManager.IsPropertyValid("Tb_UseGridDataSourceCombo"), IsPropertyDirty("Tb_UseGridDataSourceCombo"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseGridDataSourceCombo_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseGridDataSourceCombo_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_ApCnSK_Id_Validate()
        {
        }

        private void Tb_ApCnSK_Id_TextField_Validate()
        {
        }

        private void Tb_UseLegacyConnectionCode_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseLegacyConnectionCode_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseLegacyConnectionCode");
                string newBrokenRules = "";
                
                if (Tb_UseLegacyConnectionCode == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_UseLegacyConnectionCode", BROKENRULE_Tb_UseLegacyConnectionCode_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_UseLegacyConnectionCode", BROKENRULE_Tb_UseLegacyConnectionCode_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseLegacyConnectionCode");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_UseLegacyConnectionCode", _brokenRuleManager.IsPropertyValid("Tb_UseLegacyConnectionCode"), IsPropertyDirty("Tb_UseLegacyConnectionCode"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseLegacyConnectionCode_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseLegacyConnectionCode_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_PkIsIdentity_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_PkIsIdentity_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_PkIsIdentity");
                string newBrokenRules = "";
                
                if (Tb_PkIsIdentity == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_PkIsIdentity", BROKENRULE_Tb_PkIsIdentity_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_PkIsIdentity", BROKENRULE_Tb_PkIsIdentity_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_PkIsIdentity");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_PkIsIdentity", _brokenRuleManager.IsPropertyValid("Tb_PkIsIdentity"), IsPropertyDirty("Tb_PkIsIdentity"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_PkIsIdentity_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_PkIsIdentity_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_IsVirtual_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsVirtual_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsVirtual");
                string newBrokenRules = "";
                
                if (Tb_IsVirtual == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsVirtual", BROKENRULE_Tb_IsVirtual_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsVirtual", BROKENRULE_Tb_IsVirtual_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsVirtual");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsVirtual", _brokenRuleManager.IsPropertyValid("Tb_IsVirtual"), IsPropertyDirty("Tb_IsVirtual"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsVirtual_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsVirtual_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_IsNotEntity_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsNotEntity_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsNotEntity");
                string newBrokenRules = "";
                
                if (Tb_IsNotEntity == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsNotEntity", BROKENRULE_Tb_IsNotEntity_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsNotEntity", BROKENRULE_Tb_IsNotEntity_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsNotEntity");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsNotEntity", _brokenRuleManager.IsPropertyValid("Tb_IsNotEntity"), IsPropertyDirty("Tb_IsNotEntity"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsNotEntity_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsNotEntity_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_IsMany2Many_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsMany2Many_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsMany2Many");
                string newBrokenRules = "";
                
                if (Tb_IsMany2Many == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsMany2Many", BROKENRULE_Tb_IsMany2Many_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsMany2Many", BROKENRULE_Tb_IsMany2Many_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsMany2Many");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsMany2Many", _brokenRuleManager.IsPropertyValid("Tb_IsMany2Many"), IsPropertyDirty("Tb_IsMany2Many"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsMany2Many_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsMany2Many_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_UseLastModifiedByUserNameInSproc_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseLastModifiedByUserNameInSproc_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseLastModifiedByUserNameInSproc");
                string newBrokenRules = "";
                
                if (Tb_UseLastModifiedByUserNameInSproc == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_UseLastModifiedByUserNameInSproc", BROKENRULE_Tb_UseLastModifiedByUserNameInSproc_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_UseLastModifiedByUserNameInSproc", BROKENRULE_Tb_UseLastModifiedByUserNameInSproc_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseLastModifiedByUserNameInSproc");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_UseLastModifiedByUserNameInSproc", _brokenRuleManager.IsPropertyValid("Tb_UseLastModifiedByUserNameInSproc"), IsPropertyDirty("Tb_UseLastModifiedByUserNameInSproc"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseLastModifiedByUserNameInSproc_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseLastModifiedByUserNameInSproc_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_UseUserTimeStamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseUserTimeStamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseUserTimeStamp");
                string newBrokenRules = "";
                
                if (Tb_UseUserTimeStamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_UseUserTimeStamp", BROKENRULE_Tb_UseUserTimeStamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_UseUserTimeStamp", BROKENRULE_Tb_UseUserTimeStamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseUserTimeStamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_UseUserTimeStamp", _brokenRuleManager.IsPropertyValid("Tb_UseUserTimeStamp"), IsPropertyDirty("Tb_UseUserTimeStamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseUserTimeStamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseUserTimeStamp_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_UseForAudit_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseForAudit_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseForAudit");
                string newBrokenRules = "";
                
                if (Tb_UseForAudit == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_UseForAudit", BROKENRULE_Tb_UseForAudit_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_UseForAudit", BROKENRULE_Tb_UseForAudit_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UseForAudit");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_UseForAudit", _brokenRuleManager.IsPropertyValid("Tb_UseForAudit"), IsPropertyDirty("Tb_UseForAudit"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseForAudit_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseForAudit_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_IsInputComplete_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsInputComplete_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsInputComplete");
                string newBrokenRules = "";
                
                if (Tb_IsInputComplete == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsInputComplete", BROKENRULE_Tb_IsInputComplete_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsInputComplete", BROKENRULE_Tb_IsInputComplete_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsInputComplete");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsInputComplete", _brokenRuleManager.IsPropertyValid("Tb_IsInputComplete"), IsPropertyDirty("Tb_IsInputComplete"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsInputComplete_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsInputComplete_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_IsCodeGen_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGen_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsCodeGen");
                string newBrokenRules = "";
                
                if (Tb_IsCodeGen == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsCodeGen", BROKENRULE_Tb_IsCodeGen_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsCodeGen", BROKENRULE_Tb_IsCodeGen_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsCodeGen");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsCodeGen", _brokenRuleManager.IsPropertyValid("Tb_IsCodeGen"), IsPropertyDirty("Tb_IsCodeGen"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGen_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGen_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_IsReadyCodeGen_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsReadyCodeGen_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsReadyCodeGen");
                string newBrokenRules = "";
                
                if (Tb_IsReadyCodeGen == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsReadyCodeGen", BROKENRULE_Tb_IsReadyCodeGen_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsReadyCodeGen", BROKENRULE_Tb_IsReadyCodeGen_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsReadyCodeGen");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsReadyCodeGen", _brokenRuleManager.IsPropertyValid("Tb_IsReadyCodeGen"), IsPropertyDirty("Tb_IsReadyCodeGen"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsReadyCodeGen_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsReadyCodeGen_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_IsCodeGenComplete_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGenComplete_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsCodeGenComplete");
                string newBrokenRules = "";
                
                if (Tb_IsCodeGenComplete == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsCodeGenComplete", BROKENRULE_Tb_IsCodeGenComplete_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsCodeGenComplete", BROKENRULE_Tb_IsCodeGenComplete_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsCodeGenComplete");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsCodeGenComplete", _brokenRuleManager.IsPropertyValid("Tb_IsCodeGenComplete"), IsPropertyDirty("Tb_IsCodeGenComplete"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGenComplete_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGenComplete_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_IsTagForCodeGen_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsTagForCodeGen_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsTagForCodeGen");
                string newBrokenRules = "";
                
                if (Tb_IsTagForCodeGen == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsTagForCodeGen", BROKENRULE_Tb_IsTagForCodeGen_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsTagForCodeGen", BROKENRULE_Tb_IsTagForCodeGen_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsTagForCodeGen");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsTagForCodeGen", _brokenRuleManager.IsPropertyValid("Tb_IsTagForCodeGen"), IsPropertyDirty("Tb_IsTagForCodeGen"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsTagForCodeGen_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsTagForCodeGen_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_IsTagForOther_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsTagForOther_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsTagForOther");
                string newBrokenRules = "";
                
                if (Tb_IsTagForOther == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsTagForOther", BROKENRULE_Tb_IsTagForOther_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsTagForOther", BROKENRULE_Tb_IsTagForOther_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsTagForOther");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsTagForOther", _brokenRuleManager.IsPropertyValid("Tb_IsTagForOther"), IsPropertyDirty("Tb_IsTagForOther"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsTagForOther_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsTagForOther_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_TableGroups_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_TableGroups_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_TableGroups");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_TableGroups != null)
                {
                    len = Tb_TableGroups.Length;
                }

                if (len > STRINGSIZE_Tb_TableGroups)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_TableGroups", BROKENRULE_Tb_TableGroups_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_TableGroups", BROKENRULE_Tb_TableGroups_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_TableGroups");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_TableGroups", _brokenRuleManager.IsPropertyValid("Tb_TableGroups"), IsPropertyDirty("Tb_TableGroups"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_TableGroups_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_TableGroups_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsActiveRow");
                string newBrokenRules = "";
                
                if (Tb_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsActiveRow", BROKENRULE_Tb_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsActiveRow", BROKENRULE_Tb_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsActiveRow", _brokenRuleManager.IsPropertyValid("Tb_IsActiveRow"), IsPropertyDirty("Tb_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsDeleted");
                string newBrokenRules = "";
                
                if (Tb_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsDeleted", BROKENRULE_Tb_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsDeleted", BROKENRULE_Tb_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsDeleted", _brokenRuleManager.IsPropertyValid("Tb_IsDeleted"), IsPropertyDirty("Tb_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_CreatedUserId_Validate()
        {
        }

        private void Tb_CreatedDate_Validate()
        {
        }

        private void Tb_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_LastModifiedDate_Validate()
        {
        }

        private void Tb_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Stamp");
                string newBrokenRules = "";
                
                if (Tb_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_Stamp", BROKENRULE_Tb_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_Stamp", BROKENRULE_Tb_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_Stamp", _brokenRuleManager.IsPropertyValid("Tb_Stamp"), IsPropertyDirty("Tb_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


