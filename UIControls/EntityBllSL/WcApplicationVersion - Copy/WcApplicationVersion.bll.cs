using System;
using System.Collections.Generic;
using TypeServices;
using System.ComponentModel;
using System.Diagnostics;
using EntityWireTypeSL;
using Ifx.SL;
using Velocity.SL;
using ProxyWrapper;
using vComboDataTypes;

// Gen Timestamp:  12/15/2017 12:37:20 PM

namespace EntityBll.SL
{


    public partial class WcApplicationVersion_Bll : BusinessObjectBase, ITraceItem, IBusinessObject, IBusinessObjectV2, INotifyPropertyChanged , IEditableObject
    {


        #region Initialize Variables

        /// <summary>
        /// This is a private instance of TraceItemList which is normaly used in every entity
        /// business type and is used to provide rich information abou the current state and values
        /// in this business object when included in a trace call. To see exactly what data will be
        /// provided in a trace, look at the GetTraceItemsShortList method in the WcApplicationVersion_Bll code
        /// file.
        /// </summary>
        /// <seealso cref="!:file://D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Optimized_Deep_Tracing_of_Business_Object.html" cat="My Test Category">My Test Caption</seealso>
        TraceItemList _traceItems;
        /// <seealso cref="EntityWireType.WcApplicationVersion_Values">WcApplicationVersion_Values Class</seealso>
        /// <summary>
        ///     This is an instance of the wire type (<see cref="EntityWireType.WcApplicationVersion_Values">WcApplicationVersion_Values</see>) that’s wrapped by the Values
        ///     Manager (<see cref="EntityWireType.WcApplicationVersion_ValuesMngr">WcApplicationVersion_ValuesMngr</see>) and
        ///     used to Plug-n-Play into this business object.
        /// </summary>
        private WcApplicationVersion_ValuesMngr _data = null;

        /// <summary>See <see cref="OnCrudFailed">OnCrudFailed</see> for information.</summary>
        public event CrudFailedEventHandler CrudFailed;

        public event AsyncSaveCompleteEventHandler AsyncSaveComplete;

        public event AsyncSaveWithResponseCompleteEventHandler AsyncSaveWithResponseComplete;

        public event EntityRowReceivedEventHandler EntityRowReceived;


        /// <summary>
        ///     See <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> for
        ///     information.
        /// </summary>
        public event CurrentEntityStateEventHandler CurrentEntityStateChanged;
        /// <summary>See <see cref="OnBrokenRuleChanged">OnBrokenRuleChanged</see> for information.</summary>
        public event BrokenRuleEventHandler BrokenRuleChanged;
        /// <summary>
        /// 	<para>
        ///         Raised by the This event is currently <see cref="OnPropertyValueChanged">OnPropertyValueChanged</see> method, its currently not
        ///         being used and may be obsolete.
        ///     </para>
        /// </summary>
        public event PropertyValueChangedEventHandler PropertyValueChanged;
        /// <summary>This event is currently not being used and may be obsolete.</summary>
        public event BusinessObjectUpdatedEventHandler BusinessObjectUpdated;
        /// <summary>
        /// 	<para>
        ///         Required by the INotifyPropertyChanged interface, this event is raise by the
        ///         <see cref="Notify">Notify</see> method (also part of the INotifyPropertyChanged
        ///         interface. The Notify method is called in nearly all public data field
        ///         properties. The INotifyPropertyChanged interface is implemented to make
        ///         WcApplicationVersion_Bll more extendable.
        ///     </para>
        /// 	<para>The <strong>INotifyPropertyChanged</strong> interface is used to notify
        ///     clients, typically binding clients, that a property value has changed. For more
        ///     information, search INotifyPropertyChanged on <strong>MSDN</strong>.</para>
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 	<para>
        ///         Private field for the <see cref="ActiveRestrictedStringProperty">ActiveRestrictedStringProperty</see> property.
        ///         See it’s documentation for details.
        ///     </para>
        /// 	<para></para>
        /// </summary>
        private string _activeRestrictedStringProperty;
        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcApplicationVersion_Bll";
        /// <summary>
        /// 	<para>
        ///         Used in the validation section of this class in the WcApplicationVersion.bll.vld.cs code file,
        ///         this event notifies event handlers that a data field’s valid state has changed
        ///         (valid or not valid).<br/>
        ///         For more information about this event, see <see cref="TypeServices.ControlValidStateChangedEventHandler">CurrentEntityStateEventHandler</see>.<br/>
        ///         For more information on how it’s used, see these methods:
        ///     </para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="NewEntityRow">NewEntityRow</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="GetEntityRow">GetEntityRow</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="Save">Save</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcApplicationVersionProps.RaiseCurrentEntityStateChanged">ucWcApplicationVersionProps.RaiseCurrentEntityStateChanged()</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcApplicationVersionProps.RaiseCurrentEntityStateChanged">ucWcApplicationVersionProps.RaiseCurrentEntityStateChanged(EntityStateSwitch
        ///             state)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public event ControlValidStateChangedEventHandler ControlValidStateChanged;
        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcApplicationVersionProps.OnRestrictedTextLengthChanged">ucWcApplicationVersionProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcApplicationVersionProps.SetRestrictedStringLengthText">ucWcApplicationVersionProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public event RestrictedTextLengthChangedEventHandler RestrictedTextLengthChanged;

        UserSecurityContext _context = null;

        private object _parentEditObject = null;
        private ParentEditObjectType _parentType = ParentEditObjectType.None;

        private WcApplicationVersionService_ProxyWrapper _wcApplicationVersionProxy = null;



        Guid _StandingFK = Guid.Empty;
        string _parentEntityType = "";

        private WcTableService_ProxyWrapper _proxyWcTableService;
            
        private WcTableGroupService_ProxyWrapper _proxyWcTableGroupService;
            
        private WcTableColumnGroupService_ProxyWrapper _proxyWcTableColumnGroupService;
            
        private WcStoredProcGroupService_ProxyWrapper _proxyWcStoredProcGroupService;
            
        #endregion Initialize Variables

            
        #region Initialize Class

        public WcApplicationVersion_Bll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Bll()", IfxTraceCategory.Enter);
                _context = UserSecurityContext.GetCurrent();
                InitializeClass();
//                WcApplicationVersionService_ProxyWrapper _wcApplicationVersionProxy = new  WcApplicationVersionService_ProxyWrapper();
//                _wcApplicationVersionProxy.WcApplicationVersion_SaveCompleted += new EventHandler<WcApplicationVersion_SaveCompletedEventArgs>(SaveCompleted);
                _proxyWcTableService = new WcTableService_ProxyWrapper();
                _proxyWcTableService.WcTable_GetListByFKCompleted += _proxyWcTableService_WcTable_GetListByFKCompleted;
                _children_WcTable.Add(new WcTable_Bll());
            
                _proxyWcTableGroupService = new WcTableGroupService_ProxyWrapper();
                _proxyWcTableGroupService.WcTableGroup_GetListByFKCompleted += _proxyWcTableGroupService_WcTableGroup_GetListByFKCompleted;
                _children_WcTableGroup.Add(new WcTableGroup_Bll());
            
                _proxyWcTableColumnGroupService = new WcTableColumnGroupService_ProxyWrapper();
                _proxyWcTableColumnGroupService.WcTableColumnGroup_GetListByFKCompleted += _proxyWcTableColumnGroupService_WcTableColumnGroup_GetListByFKCompleted;
                _children_WcTableColumnGroup.Add(new WcTableColumnGroup_Bll());
            
                _proxyWcStoredProcGroupService = new WcStoredProcGroupService_ProxyWrapper();
                _proxyWcStoredProcGroupService.WcStoredProcGroup_GetListByFKCompleted += _proxyWcStoredProcGroupService_WcStoredProcGroup_GetListByFKCompleted;
                _children_WcStoredProcGroup.Add(new WcStoredProcGroup_Bll());
            
                SetNewEntityRow();

                CustomConstructor();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Bll()", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Bll()", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>
        ///         Passes in the <see cref="EntityWireType.WcApplicationVersion_ValuesMngr">WcApplicationVersion_ValuesMngr</see> type and <see cref="TypeServices.EntityState">EntityState</see>. The WcApplicationVersion_ValuesMngr type is the
        ///         data object used in a ‘Plug-n-Play’ fashion for loading data into WcApplicationVersion_Bll
        ///         fast and efficiently. EntityState sets WcApplicationVersion_Bll’s state.
        ///     </para>
        /// 	<para>Used by the following methods call this constructor:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="FromWire">FromWire</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcApplicationVersion_List.Fill">Fill</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcApplicationVersion_List.ReplaceList">ReplaceList(WcApplicationVersion_ValuesMngr[]
        ///             list)</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcApplicationVersion_List.ReplaceList">ReplaceList(IEntity_ValuesMngr[]
        ///             list)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public WcApplicationVersion_Bll(WcApplicationVersion_ValuesMngr valueObject, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Bll(WcApplicationVersion_ValuesMngr valueObject, EntityState state)", IfxTraceCategory.Enter);
                _context = UserSecurityContext.GetCurrent(); 
                _data = valueObject;
                InitializeClass(state);
               // _data.SetClassStateAfterFetch(state);
//                 WcApplicationVersionService_ProxyWrapper _wcApplicationVersionProxy = new  WcApplicationVersionService_ProxyWrapper();
//                _wcApplicationVersionProxy.WcApplicationVersion_SaveCompleted += new EventHandler<WcApplicationVersion_SaveCompletedEventArgs>(SaveCompleted);
                _proxyWcTableService = new WcTableService_ProxyWrapper();
                _proxyWcTableService.WcTable_GetListByFKCompleted += _proxyWcTableService_WcTable_GetListByFKCompleted;
                _children_WcTable.Add(new WcTable_Bll());
            
                _proxyWcTableGroupService = new WcTableGroupService_ProxyWrapper();
                _proxyWcTableGroupService.WcTableGroup_GetListByFKCompleted += _proxyWcTableGroupService_WcTableGroup_GetListByFKCompleted;
                _children_WcTableGroup.Add(new WcTableGroup_Bll());
            
                _proxyWcTableColumnGroupService = new WcTableColumnGroupService_ProxyWrapper();
                _proxyWcTableColumnGroupService.WcTableColumnGroup_GetListByFKCompleted += _proxyWcTableColumnGroupService_WcTableColumnGroup_GetListByFKCompleted;
                _children_WcTableColumnGroup.Add(new WcTableColumnGroup_Bll());
            
                _proxyWcStoredProcGroupService = new WcStoredProcGroupService_ProxyWrapper();
                _proxyWcStoredProcGroupService.WcStoredProcGroup_GetListByFKCompleted += _proxyWcStoredProcGroupService_WcStoredProcGroup_GetListByFKCompleted;
                _children_WcStoredProcGroup.Add(new WcStoredProcGroup_Bll());
            
                CustomConstructor();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Bll(WcApplicationVersion_ValuesMngr valueObject, EntityState state)", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Bll(WcApplicationVersion_ValuesMngr valueObject, EntityState state)", IfxTraceCategory.Leave);
            }
        }


        private void InitializeClass()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Enter);
                InitializeClass(new EntityState(true, true, false));
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Leave);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Leave);
            }
        }



        /// <summary>Called from the constructor, events are wired here.</summary>
        private void InitializeClass(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass(EntityState state)", IfxTraceCategory.Enter);
                if (_data == null)
                {
                    _data = GetDefault();
                    //**  This is a hack.  We should set the state elsewhere, however, when we get this from accross the wire,
                    //      we loose the state and therefore are forced to always call SetClassStateAfterFetch.
                   // _data.SetClassStateAfterFetch(state);
                }
                _data.SetClassStateAfterFetch(state);
                BrokenRuleManagerProperty = new BrokenRuleManager(this);
                _brokenRuleManager.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                //TODO: need to code it for a dynamic creation for an insert
                //TODO: need to move save, assignwithid, update, delete, deactivate, persist (generic, check the state) into this class
                //TODO: need to move the various lists of apps to get into an applicationprovider class for the client side that thunks to the proxywrapper
                //TODO: need to move field level validation into the setters and call the object validation on any persist operation
                //TODO:need to implement the two eh and tracing interfaces on the object
                InitializeClass_Cust();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityState", IfxTraceCategory.Leave);
            }
        }

        private  WcApplicationVersionService_ProxyWrapper WcApplicationVersionProxy
        {
            get
            {
                if (_wcApplicationVersionProxy == null)
                {
                    _wcApplicationVersionProxy = new  WcApplicationVersionService_ProxyWrapper();
                    _wcApplicationVersionProxy.WcApplicationVersion_SaveCompleted += new EventHandler<WcApplicationVersion_SaveCompletedEventArgs>(SaveCompleted);
                    _wcApplicationVersionProxy.WcApplicationVersion_GetByIdCompleted += new EventHandler<WcApplicationVersion_GetByIdCompletedEventArgs>(GetEntityRowResponse);

                }

                return _wcApplicationVersionProxy;
            }
        }

        #endregion  Initialize Class


		#region CRUD Methods



        /// <returns><see cref="EntityWireType.WcApplicationVersion_ValuesMngr">WcApplicationVersion_ValuesMngr</see></returns>
        /// <summary>
        ///     When this business type is set to a new state, a new data type also known as a wire
        ///     type (<see cref="EntityWireType.WcApplicationVersion_Values">WcApplicationVersion_Values</see>) is created, new
        ///     Id value is created, default values are set and appropriate state setting are made.
        ///     WcApplicationVersion_Values is wrapped in the <see cref="EntityWireType.WcApplicationVersion_ValuesMngr">WcApplicationVersion_ValuesMngr</see> type and returned.
        /// </summary>
        private WcApplicationVersion_ValuesMngr GetDefault()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                Guid Id = GetNewID();
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Enter);
                return new WcApplicationVersion_ValuesMngr(new object[] 
                    {
                        Id,    //  ApVrsn_Id
						null,		//  ApVrsn_Ap_Id
						null,		//  AttachmentCount
						null,		//  AttachmentFileNames
						null,		//  DiscussionCount
						null,		//  DiscussionTitles
						null,		//  VersionNo
						(byte)1,		//  ApVrsn_MajorVersion
						(byte)1,		//  ApVrsn_MinorVersion
						(byte)0,		//  ApVrsn_VersionIteration
						null,		//  ApVrsn_Server
						null,		//  ApVrsn_DbName
						null,		//  ApVrsn_SolutionPath
						null,		//  ApVrsn_DefaultUIAssembly
						null,		//  ApVrsn_DefaultUIAssemblyPath
						null,		//  ApVrsn_DefaultWireTypePath
						null,		//  ApVrsn_WebServerURL
						null,		//  ApVrsn_WebsiteCodeFolderPath
						null,		//  ApVrsn_WebserviceCodeFolderPath
						null,		//  ApVrsn_StoredProcCodeFolder
						true,		//  ApVrsn_UseLegacyConnectionCode
						null,		//  ApVrsn_DefaultConnectionCodeId
						null,		//  ApVrsn_DefaultConnectionCodeId_TextField
						false,		//  ApVrsn_IsMulticultural
						null,		//  ApVrsn_Notes
						true,		//  ApVrsn_IsActiveRow
						false,		//  ApVrsn_IsDeleted
						null,		//  ApVrsn_CreatedUserId
						null,		//  ApVrsn_CreatedDate
						null,		//  ApVrsn_UserId
						null,		//  UserName
						null,		//  ApVrsn_LastModifiedDate
						null,		//  ApVrsn_Stamp
                    }, new EntityState(true, true, false)
                );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// Called by any parent object that wants to clear out the existing data and state,
        /// and replace all with new state and data object.
        /// </summary>
        public void NewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Enter);
                _data = GetDefault();
                NewEntityRowCustomCode();
                SetDefaultBrokenRules();
                //RaiseEventCurrentEntityStateChanged(EntityStateSwitch.NewInvalidNotDirty);
                RaiseEventCurrentEntityStateChanged(_data.S.Switch);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// This is called from the parameterless constructor.  It's assumed that a data object will not be passed (which would have
        /// cause the EntityState to be configured properly) and therefore this new entity must be properly configured.
        /// </summary>
        public void SetNewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", IfxTraceCategory.Enter);
                NewEntityRowCustomCode();
                SetDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", IfxTraceCategory.Leave);
            }
        }


        public void GetEntityRow(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", new ValuePair[] {new ValuePair("Id", Id) }, IfxTraceCategory.Enter);

                WcApplicationVersionProxy.Begin_WcApplicationVersion_GetById(Id);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", IfxTraceCategory.Leave);
            }
        }

        public void GetEntityRow(long Id)
        {
            throw new NotImplementedException("WcApplicationVersion_Bll GetEntityRow(long Id) Not Implemented");
        }

        public void GetEntityRow(int Id)
        {
            throw new NotImplementedException("WcApplicationVersion_Bll GetEntityRow(int Id) Not Implemented");
        }

        public void GetEntityRow(short Id)
        {
            throw new NotImplementedException("WcApplicationVersion_Bll GetEntityRow(short Id) Not Implemented");
        }

        public void GetEntityRow(byte Id)
        {
            throw new NotImplementedException("WcApplicationVersion_Bll GetEntityRow(byte Id) Not Implemented");
        }

        public void GetEntityRow(object Id)
        {
            throw new NotImplementedException("WcApplicationVersion_Bll GetEntityRow(object Id) Not Implemented");
        }

        private void GetEntityRowResponse(object sender, WcApplicationVersion_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                if (data == null)
                {
                    // Alert UI
                    return;
                }
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array == null)
                {
                    // Alert UI
                    return;
                }
                _brokenRuleManager.ClearAllBrokenRules();
                _data.ReplaceData((object[])array[0], new EntityState(false, true, false));
                RaiseEventEntityRowReceived();
                RefreshFields();
                // Do we still need this next line?
                //RaiseEventCurrentEntityStateChanged(_data.S.Switch);

                //  ToDo:  Later when we have a Foreign Key, do something like this:  DataProps.TbC_Tb_ID = DataProps.standing_FK;
                //              How do we handle the standing FK in the new framework?
 
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", IfxTraceCategory.Leave);
            }
        }



        /// <summary>
        ///     Called by <see cref="GetDefault">GetDefault</see> and creates a new WcApplicationVersion Id value
        ///     for a new data object (<see cref="EntityWireType.WcApplicationVersion_ValuesMngr">WcApplicationVersion_ValuesMngr</see>).
        /// </summary>
        private Guid GetNewID()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", IfxTraceCategory.Enter);
                return Guid.NewGuid();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Un-does the current data edits and restores everything back to the previous saved
        ///     or new <see cref="TypeServices.EntityState">state</see>. This will include calling
        ///     <see cref="TypeServices.BrokenRuleManager.ClearAllBrokenRules">_brokenRuleManager.ClearAllBrokenRules</see>
        ///     to clear any BrokenRules or calling <see cref="SetDefaultBrokenRules">SetDefaultBrokenRules</see> to set any default BrokenRules;
        ///     and raising the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event is raised to
        ///     notify the parent objects that the state has changed.
        /// </summary>
        public void UnDo()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Enter);
                EntityStateSwitch es = _data.S.Switch;
                _data.SetCurrentToOriginal();
                if (_data.S.IsNew() == true)
                {
                    SetDefaultBrokenRules();
                }
                else
                {
                    _brokenRuleManager.ClearAllBrokenRules();
                }
                CheckEntityState(es);

                UnDoCustomCode();
                RefreshFields();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Call the Notify method for all visible fields.  This allows the fields/properties to be refreshed in grids and other controls that are consuming them via databinding.
        /// </summary>
        public void RefreshFields()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", IfxTraceCategory.Enter);
				Notify("AttachmentCount");
				Notify("DiscussionCount");
				Notify("VersionNo");
				Notify("ApVrsn_MajorVersion");
				Notify("ApVrsn_MinorVersion");
				Notify("ApVrsn_VersionIteration");
				Notify("ApVrsn_Server");
				Notify("ApVrsn_DbName");
				Notify("ApVrsn_SolutionPath");
				Notify("ApVrsn_DefaultUIAssembly");
				Notify("ApVrsn_DefaultUIAssemblyPath");
				Notify("ApVrsn_DefaultWireTypePath");
				Notify("ApVrsn_WebServerURL");
				Notify("ApVrsn_WebsiteCodeFolderPath");
				Notify("ApVrsn_WebserviceCodeFolderPath");
				Notify("ApVrsn_StoredProcCodeFolder");
				Notify("ApVrsn_UseLegacyConnectionCode");
				Notify("ApVrsn_DefaultConnectionCodeId");
				Notify("ApVrsn_DefaultConnectionCodeId_TextField");
				Notify("ApVrsn_IsMulticultural");
				Notify("ApVrsn_Notes");
				Notify("ApVrsn_IsActiveRow");
				Notify("UserName");
				Notify("ApVrsn_LastModifiedDate");

                RefreshFieldsCustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Called from <see cref="wnConcurrencyManager">wnConcurrencyManager</see> and therefore does not need
        ///     to pass in the Parent and Parent Type parameters.
        ///     Saves the current data to the data store, configures the appropriate state and
        ///     calls <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see> to
        ///     notify the parent object of the state change.
        /// </summary>
        public void Save(UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                _data.C.ApVrsn_UserId_noevents = Credentials.UserId;
                WcApplicationVersionProxy.Begin_WcApplicationVersion_Save(_data.C.GetValues(), (int)check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Saves the current data to the data store, configures the appropriate state and
        ///     calls <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see> to
        ///     notify the parent object of the state change.
        /// </summary>
        public void Save(object parentEditObject, ParentEditObjectType parentType,  UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                _parentEditObject = parentEditObject;
                _parentType = parentType;
                _data.C.ApVrsn_UserId_noevents = Credentials.UserId;
                WcApplicationVersionProxy.Begin_WcApplicationVersion_Save(_data.C.GetValues(), (int)check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        void SaveCompleted(object sender, WcApplicationVersion_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", IfxTraceCategory.Enter);
                DataServiceInsertUpdateResponse newData = null;
                if (e.Result == null)
                {
                    // return some message to the calling object
                    newData = new DataServiceInsertUpdateResponse();
                    newData.Result = DataOperationResult.HandledException;
                    //RaiseEventAsyncSaveComplete(newData.Result, null);
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);
                }
                else
                {
                    newData = new DataServiceInsertUpdateResponse();
                    byte[] data = e.Result;
                    object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                    newData.SetFromObjectArray(array);
                }

                // Get the Result status
                if (newData.Result == DataOperationResult.Success)
                {
                    if (newData.ReturnCurrentRowOnInsertUpdate == true)
                    {
                        // If we returned the current row from the data store, then syncronize the wire object with it.
                        _data.C.ReplaceDataFromObjectArray(newData.CurrentValues);
                        RefreshFields();
                    }
                    else
                    {
                        // Otherwise:
                        //  Set the PK value from the data store incase we did an insert.
                        //  In theory the guid was already created by this busness object, but we're setting it here
                        //  just in case some code changed somewhere and it was created on the server.
                        _data.C._a = newData.guidPrimaryKey;
                        //  Set the new TimeStamp value
                        _data.C.ApVrsn_Stamp_noevents = newData.CurrentTimestamp;
                    }
                    _data.S.SetNotNew();
                    _data.S.SetValid();
                    _data.S.SetNotDirty();
                    _data.SetOriginalToCurrent();
                  
                    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);

                    //if (_parentType == ParentEditObjectType.EntitiyPropertiesControl)
                    //{
                    //    // A EntitiyPropertiesControl called the same method, therefore raise the following event
                    //    // allowing all parent conrol to reconfigure thier entity state.
                    //    // If this was called from editing a grid control, then this is not nessesacy since we 
                    //    // dont change the state of surrounding controls.
                    //    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                    //
                    //    // Rasie this event to the props control can call it's SetState method to refresh the UI
                    //    // The Entity List control will be updated via the PropertyChanged event which gets called from the RefreshFields call above.
                    //    if (newData.ReturnCurrentRowOnInsertUpdate == true)
                    //    {
                    //        RaiseEventAsyncSaveWithResponseComplete(newData, null);
                    //    }
                    //
                    //}
                    //else
                    //{
                    //    RaiseEventAsyncSaveComplete(newData.Result, null);
                    //}
                }
                else
                {
                    // Add the current date from the database to the wire obect's concurrent property
                    //if (newData.Result == DataOperationResult.ConcurrencyFailure)
                    //{
                    //    _data.X.ReplaceDataFromObjectArray(newData.CurrentValues);
                    //}
                    // ToDo:  we need to develope logic and code to pass user friendly error message 
                    // instead of null when the save failed.

                    // We had some type of failure, so raise this event regardless of the parent type 
                    // so the UI can notify the user.
                    //RaiseEventAsyncSaveComplete(newData.Result, null);
                    //*** use this instead
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _parentEditObject = null;
                _parentType = ParentEditObjectType.None;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", IfxTraceCategory.Leave);
            }
        }







        /// <summary>
        /// Calls a Delete method on the server to delete this entity from the data
        /// store.
        /// </summary>
        /// <returns>1 = Success, 2 = Failed</returns>
        public int  Entity_Delete()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", IfxTraceCategory.Enter);
//                // add credentials
//                WcApplicationVersion_ValuesMngr retObject = ProxyWrapper.EntityProxyWrapper.WcApplicationVersion_Delete(_data);
//                return 1;
//                //  Needs further design.  What do we do when the delete fails or there is an concurrency issue?
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Just a stub and not being used. Consider renaming this to ActivatOrDeactivate so
        /// it can be used either way.
        /// </summary>
        /// <returns>Returns 0 because this method is not currently functional.</returns>
        public int Entity_Deactivate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", IfxTraceCategory.Enter);
                //  This is currently setup to return a wire object, however, we might only need to return an int flag
                //  Needs further design
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", IfxTraceCategory.Leave);
            }
        }

        /// <returns>Returns 0 because this method is not currently functional.</returns>
        /// <summary>Just a stub and not being used.</summary>
        public int Entity_Remove()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", IfxTraceCategory.Enter);
                //  This is currently setup to return a wire object, however, we might only need to return an int flag
                //  Needs further design
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Copies the values from another data object (<see cref="EntityWireType.WcApplicationVersion_ValuesMngr">WcApplicationVersion_ValuesMngr</see>) to the one plugged into
        ///     this business object. This is called from the Save method because it returns a
        ///     fresh copy of this entity’s data from the data store incase modifications were made
        ///     in the process of saving.
        /// </summary>
        /// <param name="thisData">
        /// The data object (WcApplicationVersion_ValuesMngr) plugged into this business object which data
        /// will be copied to.
        /// </param>
        /// <param name="newData">The data object (WcApplicationVersion_ValuesMngr) which data will be copied from.</param>
        private void SyncValueObjectCurrentProperties(WcApplicationVersion_ValuesMngr thisData, WcApplicationVersion_ValuesMngr newData)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", IfxTraceCategory.Enter);
				thisData.C.ApVrsn_Id_noevents = newData.C.ApVrsn_Id_noevents;
				thisData.C.ApVrsn_Ap_Id_noevents = newData.C.ApVrsn_Ap_Id_noevents;
				thisData.C.AttachmentCount_noevents = newData.C.AttachmentCount_noevents;
				thisData.C.AttachmentFileNames_noevents = newData.C.AttachmentFileNames_noevents;
				thisData.C.DiscussionCount_noevents = newData.C.DiscussionCount_noevents;
				thisData.C.DiscussionTitles_noevents = newData.C.DiscussionTitles_noevents;
				thisData.C.VersionNo_noevents = newData.C.VersionNo_noevents;
				thisData.C.ApVrsn_MajorVersion_noevents = newData.C.ApVrsn_MajorVersion_noevents;
				thisData.C.ApVrsn_MinorVersion_noevents = newData.C.ApVrsn_MinorVersion_noevents;
				thisData.C.ApVrsn_VersionIteration_noevents = newData.C.ApVrsn_VersionIteration_noevents;
				thisData.C.ApVrsn_Server_noevents = newData.C.ApVrsn_Server_noevents;
				thisData.C.ApVrsn_DbName_noevents = newData.C.ApVrsn_DbName_noevents;
				thisData.C.ApVrsn_SolutionPath_noevents = newData.C.ApVrsn_SolutionPath_noevents;
				thisData.C.ApVrsn_DefaultUIAssembly_noevents = newData.C.ApVrsn_DefaultUIAssembly_noevents;
				thisData.C.ApVrsn_DefaultUIAssemblyPath_noevents = newData.C.ApVrsn_DefaultUIAssemblyPath_noevents;
				thisData.C.ApVrsn_DefaultWireTypePath_noevents = newData.C.ApVrsn_DefaultWireTypePath_noevents;
				thisData.C.ApVrsn_WebServerURL_noevents = newData.C.ApVrsn_WebServerURL_noevents;
				thisData.C.ApVrsn_WebsiteCodeFolderPath_noevents = newData.C.ApVrsn_WebsiteCodeFolderPath_noevents;
				thisData.C.ApVrsn_WebserviceCodeFolderPath_noevents = newData.C.ApVrsn_WebserviceCodeFolderPath_noevents;
				thisData.C.ApVrsn_StoredProcCodeFolder_noevents = newData.C.ApVrsn_StoredProcCodeFolder_noevents;
				thisData.C.ApVrsn_UseLegacyConnectionCode_noevents = newData.C.ApVrsn_UseLegacyConnectionCode_noevents;
				thisData.C.ApVrsn_DefaultConnectionCodeId_noevents = newData.C.ApVrsn_DefaultConnectionCodeId_noevents;
				thisData.C.ApVrsn_DefaultConnectionCodeId_TextField_noevents = newData.C.ApVrsn_DefaultConnectionCodeId_TextField_noevents;
				thisData.C.ApVrsn_IsMulticultural_noevents = newData.C.ApVrsn_IsMulticultural_noevents;
				thisData.C.ApVrsn_Notes_noevents = newData.C.ApVrsn_Notes_noevents;
				thisData.C.ApVrsn_IsActiveRow_noevents = newData.C.ApVrsn_IsActiveRow_noevents;
				thisData.C.ApVrsn_IsDeleted_noevents = newData.C.ApVrsn_IsDeleted_noevents;
				thisData.C.ApVrsn_CreatedUserId_noevents = newData.C.ApVrsn_CreatedUserId_noevents;
				thisData.C.ApVrsn_CreatedDate_noevents = newData.C.ApVrsn_CreatedDate_noevents;
				thisData.C.ApVrsn_UserId_noevents = newData.C.ApVrsn_UserId_noevents;
				thisData.C.UserName_noevents = newData.C.UserName_noevents;
				thisData.C.ApVrsn_LastModifiedDate_noevents = newData.C.ApVrsn_LastModifiedDate_noevents;
				thisData.C.ApVrsn_Stamp_noevents = newData.C.ApVrsn_Stamp_noevents;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", IfxTraceCategory.Leave);
            }
        }


		#endregion CRUD Methods


		#region General Methods and Properties


		#region Wire Methods

        /// <summary>
        /// Returns a business object implementing the IBusinessObject interface from a wire
        /// object. This assumes the WcApplicationVersion_ValuesMngr being passed in has been fetched from a
        /// reliable data store or some other reliable source which gives us appropriate data and
        /// state.
        /// </summary>
        public static WcApplicationVersion_Bll FromWire(WcApplicationVersion_ValuesMngr valueObject)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", new ValuePair[] {new ValuePair("valueObject", valueObject) }, IfxTraceCategory.Enter);
                //  This assumes that each WcApplicationVersion_ValuesMngr has been fetched from a db or some other reliable source which gives us the following state
                //  Hack, Hack;  we have to add the state here becuase of how its handled (or not handled) in WcApplicationVersion_ValuesMngr due to the problem of loosing this value when passed accross the wire
                WcApplicationVersion_Bll obj = new WcApplicationVersion_Bll(valueObject, new EntityState(false, true, false));
                return obj;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Returns the current wire object plugged into this business object.</summary>
        public WcApplicationVersion_ValuesMngr ToWire()
        {
            return _data;
        }

        /// <summary>The wire object property.</summary>
        public WcApplicationVersion_ValuesMngr Wire
        {
            get { return _data; }
            set
            {
                _data = value;
            }
        }        

		#endregion Wire Methods


        /// <overloads>Get a list of current BrokenRules for this entity.</overloads>
        /// <summary>Retuns the current BrokenRules as list of strings.</summary>
        public List<string> GetBrokenRulesForEntity()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRulesForEntity();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Pass in a valid format as a string and return the current BrokenRules in a
        /// formatted list of strings.
        /// </summary>
        public List<string> GetBrokenRulesForEntity(string format)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", new ValuePair[] {new ValuePair("format", format) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRulesForEntity(format);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Leave);
            }
        }

        // get the current list of broken rules for a property
        /// <summary>Pass in a property name and return a list of its current BrokenRules.</summary>
        public List<vRuleItem> GetBrokenRulesForProperty(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRuleListForProperty(propertyName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Pass in a property name to find out if it’s valid or not.</summary>
        /// <returns>true = valid, false = not valid</returns>
        public bool IsPropertyValid(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.IsPropertyValid(propertyName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", IfxTraceCategory.Leave);
            }
        }

        public bool IsPropertyDirty(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);

                if (propertyName == null)
                {
                    throw new Exception("WcApplicationVersion_Bll.IsPropertyDirty: Parameter 'propertyName' had no value.");
                }
                else if (propertyName.Trim().Length == 0)
                {
                    throw new Exception("WcApplicationVersion_Bll.IsPropertyDirty: Parameter 'propertyName' had no value.");
                }

                switch (propertyName)
                {
                    case "ApVrsn_Ap_Id":
                        if (_data.C.ApVrsn_Ap_Id != _data.O.ApVrsn_Ap_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "AttachmentCount":
                        if (_data.C.AttachmentCount != _data.O.AttachmentCount)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "AttachmentFileNames":
                        if (_data.C.AttachmentFileNames != _data.O.AttachmentFileNames)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "DiscussionCount":
                        if (_data.C.DiscussionCount != _data.O.DiscussionCount)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "DiscussionTitles":
                        if (_data.C.DiscussionTitles != _data.O.DiscussionTitles)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "VersionNo":
                        if (_data.C.VersionNo != _data.O.VersionNo)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_MajorVersion":
                        if (_data.C.ApVrsn_MajorVersion != _data.O.ApVrsn_MajorVersion)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_MinorVersion":
                        if (_data.C.ApVrsn_MinorVersion != _data.O.ApVrsn_MinorVersion)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_VersionIteration":
                        if (_data.C.ApVrsn_VersionIteration != _data.O.ApVrsn_VersionIteration)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_Server":
                        if (_data.C.ApVrsn_Server != _data.O.ApVrsn_Server)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_DbName":
                        if (_data.C.ApVrsn_DbName != _data.O.ApVrsn_DbName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_SolutionPath":
                        if (_data.C.ApVrsn_SolutionPath != _data.O.ApVrsn_SolutionPath)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_DefaultUIAssembly":
                        if (_data.C.ApVrsn_DefaultUIAssembly != _data.O.ApVrsn_DefaultUIAssembly)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_DefaultUIAssemblyPath":
                        if (_data.C.ApVrsn_DefaultUIAssemblyPath != _data.O.ApVrsn_DefaultUIAssemblyPath)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_DefaultWireTypePath":
                        if (_data.C.ApVrsn_DefaultWireTypePath != _data.O.ApVrsn_DefaultWireTypePath)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_WebServerURL":
                        if (_data.C.ApVrsn_WebServerURL != _data.O.ApVrsn_WebServerURL)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_WebsiteCodeFolderPath":
                        if (_data.C.ApVrsn_WebsiteCodeFolderPath != _data.O.ApVrsn_WebsiteCodeFolderPath)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_WebserviceCodeFolderPath":
                        if (_data.C.ApVrsn_WebserviceCodeFolderPath != _data.O.ApVrsn_WebserviceCodeFolderPath)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_StoredProcCodeFolder":
                        if (_data.C.ApVrsn_StoredProcCodeFolder != _data.O.ApVrsn_StoredProcCodeFolder)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_UseLegacyConnectionCode":
                        if (_data.C.ApVrsn_UseLegacyConnectionCode != _data.O.ApVrsn_UseLegacyConnectionCode)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_DefaultConnectionCodeId":
                        if (_data.C.ApVrsn_DefaultConnectionCodeId != _data.O.ApVrsn_DefaultConnectionCodeId)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_IsMulticultural":
                        if (_data.C.ApVrsn_IsMulticultural != _data.O.ApVrsn_IsMulticultural)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_Notes":
                        if (_data.C.ApVrsn_Notes != _data.O.ApVrsn_Notes)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_IsActiveRow":
                        if (_data.C.ApVrsn_IsActiveRow != _data.O.ApVrsn_IsActiveRow)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_IsDeleted":
                        if (_data.C.ApVrsn_IsDeleted != _data.O.ApVrsn_IsDeleted)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "ApVrsn_CreatedUserId":
                        if (_data.C.ApVrsn_CreatedUserId != _data.O.ApVrsn_CreatedUserId)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "UserName":
                        if (_data.C.UserName != _data.O.UserName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    default:
                        throw new Exception("WcApplicationVersion_Bll.IsPropertyDirty found no matching propery name for " + propertyName);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", IfxTraceCategory.Leave);
            }
        }

        public void SetDateFromString(string propName, string value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", new ValuePair[] {new ValuePair("value", value), new ValuePair("propName", propName) }, IfxTraceCategory.Enter);
                DateTime? dt = null;
                if (BLLHelper.IsDate(value) == true)
                {
                    dt = DateTime.Parse(value);
                }
                switch (propName)
                {
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", IfxTraceCategory.Leave);
            }
        }

        public object GetPropertyValueByKey(string key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", new ValuePair[] {new ValuePair("key", key) }, IfxTraceCategory.Enter);
                switch (key)
                {
                    case "AttachmentCount":
                        return AttachmentCount;

                    case "DiscussionCount":
                        return DiscussionCount;

                    case "VersionNo":
                        return VersionNo;

                    case "ApVrsn_MajorVersion":
                        return ApVrsn_MajorVersion;

                    case "ApVrsn_MinorVersion":
                        return ApVrsn_MinorVersion;

                    case "ApVrsn_VersionIteration":
                        return ApVrsn_VersionIteration;

                    case "ApVrsn_Server":
                        return ApVrsn_Server;

                    case "ApVrsn_DbName":
                        return ApVrsn_DbName;

                    case "ApVrsn_SolutionPath":
                        return ApVrsn_SolutionPath;

                    case "ApVrsn_DefaultUIAssembly":
                        return ApVrsn_DefaultUIAssembly;

                    case "ApVrsn_DefaultUIAssemblyPath":
                        return ApVrsn_DefaultUIAssemblyPath;

                    case "ApVrsn_DefaultWireTypePath":
                        return ApVrsn_DefaultWireTypePath;

                    case "ApVrsn_WebServerURL":
                        return ApVrsn_WebServerURL;

                    case "ApVrsn_WebsiteCodeFolderPath":
                        return ApVrsn_WebsiteCodeFolderPath;

                    case "ApVrsn_WebserviceCodeFolderPath":
                        return ApVrsn_WebserviceCodeFolderPath;

                    case "ApVrsn_StoredProcCodeFolder":
                        return ApVrsn_StoredProcCodeFolder;

                    case "ApVrsn_UseLegacyConnectionCode":
                        return ApVrsn_UseLegacyConnectionCode;

                    case "ApVrsn_DefaultConnectionCodeId":
                        return ApVrsn_DefaultConnectionCodeId;

                    case "ApVrsn_IsMulticultural":
                        return ApVrsn_IsMulticultural;

                    case "ApVrsn_Notes":
                        return ApVrsn_Notes;

                    case "ApVrsn_IsActiveRow":
                        return ApVrsn_IsActiveRow;

                    case "UserName":
                        return UserName;

                    case "ApVrsn_LastModifiedDate":
                        return ApVrsn_LastModifiedDate;

                    default:
                        return GetPropertyValueByKeyCustomCode(key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", IfxTraceCategory.Leave);
            }
        }

        public string GetPropertyFormattedStringValueByKey(string key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", new ValuePair[] {new ValuePair("key", key) }, IfxTraceCategory.Enter);
                switch (key)
                {
                    case "EntityId":
                        return ApVrsn_Id.ToString();

                    case "AttachmentCount":
                        if (AttachmentCount == null)
                        {
                            return null;
                        }
                        else
                        {
                            return AttachmentCount.ToString();
                        }

                    case "DiscussionCount":
                        if (DiscussionCount == null)
                        {
                            return null;
                        }
                        else
                        {
                            return DiscussionCount.ToString();
                        }

                    case "VersionNo":
                        return VersionNo;

                    case "ApVrsn_MajorVersion":
                        return ApVrsn_MajorVersion.ToString();

                    case "ApVrsn_MinorVersion":
                        if (ApVrsn_MinorVersion == null)
                        {
                            return null;
                        }
                        else
                        {
                            return ApVrsn_MinorVersion.ToString();
                        }

                    case "ApVrsn_VersionIteration":
                        if (ApVrsn_VersionIteration == null)
                        {
                            return null;
                        }
                        else
                        {
                            return ApVrsn_VersionIteration.ToString();
                        }

                    case "ApVrsn_Server":
                        return ApVrsn_Server;

                    case "ApVrsn_DbName":
                        return ApVrsn_DbName;

                    case "ApVrsn_SolutionPath":
                        return ApVrsn_SolutionPath;

                    case "ApVrsn_DefaultUIAssembly":
                        return ApVrsn_DefaultUIAssembly;

                    case "ApVrsn_DefaultUIAssemblyPath":
                        return ApVrsn_DefaultUIAssemblyPath;

                    case "ApVrsn_DefaultWireTypePath":
                        return ApVrsn_DefaultWireTypePath;

                    case "ApVrsn_WebServerURL":
                        return ApVrsn_WebServerURL;

                    case "ApVrsn_WebsiteCodeFolderPath":
                        return ApVrsn_WebsiteCodeFolderPath;

                    case "ApVrsn_WebserviceCodeFolderPath":
                        return ApVrsn_WebserviceCodeFolderPath;

                    case "ApVrsn_StoredProcCodeFolder":
                        return ApVrsn_StoredProcCodeFolder;

                    case "ApVrsn_UseLegacyConnectionCode":
                        return ApVrsn_UseLegacyConnectionCode.ToString();

                    case "ApVrsn_DefaultConnectionCodeId":
                        return ApVrsn_DefaultConnectionCodeId_TextField;

                    case "ApVrsn_IsMulticultural":
                        return ApVrsn_IsMulticultural.ToString();

                    case "ApVrsn_Notes":
                        return ApVrsn_Notes;

                    case "ApVrsn_IsActiveRow":
                        return ApVrsn_IsActiveRow.ToString();

                    case "UserName":
                        return UserName;

                    case "ApVrsn_LastModifiedDate":
                        if (ApVrsn_LastModifiedDate == null)
                        {
                            return null;
                        }
                        else
                        {
                            return ApVrsn_LastModifiedDate.ToString();
                        }

                    default:
                        return GetPropertyFormattedStringValueByKeyCustomCode(key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", IfxTraceCategory.Leave);
            }
        }

        
        #region General Properties

        /// <summary>
        ///     Returns a list of current BrokenRules for this entity as a list of <see cref="TypeServices.vRuleItem">vRuleItem</see> types.
        /// </summary>
        [Browsable(false)]
        public List<vRuleItem> GetBrokenRuleListForEntity()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", IfxTraceCategory.Enter);
                return BrokenRuleManagerProperty.GetBrokenRuleListForEntity();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", IfxTraceCategory.Leave);
            }
        }

        #endregion General Properties        

		#endregion General Methods and Properties


		#region Events


        /// <summary>
        ///     Raises the <see cref="CrudFailed">CrudFailed</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnCrudFailed(object sender, CrudFailedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Enter);
            CrudFailedEventHandler handler = CrudFailed;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Leave);
        }

        // THIS IS NOW OBSOLETE
        /// <summary>
        ///     Calls the <see cref="OnAsyncSaveComplete">OnAsyncSaveComplete</see> to raise the <see cref="AsyncSaveComplete">AsyncSaveComplete</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventAsyncSaveComplete(DataOperationResult result, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveComplete", IfxTraceCategory.Enter);
            AsyncSaveCompleteArgs e = new AsyncSaveCompleteArgs(result, failedReasonText);
            OnAsyncSaveComplete(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveComplete", IfxTraceCategory.Leave);
        }

        // THIS IS NOW OBSOLETE
        /// <summary>
        ///     Raises the <see cref="AsyncSaveComplete">AsyncSaveComplete</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnAsyncSaveComplete(object sender, AsyncSaveCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveComplete", IfxTraceCategory.Enter);
            AsyncSaveCompleteEventHandler handler = AsyncSaveComplete;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveComplete", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Calls the <see cref="OnAsyncSaveWithResponseComplete">OnAsyncSaveWithResponseComplete</see> to raise the <see cref="AsyncSaveWithResponseComplete">AsyncSaveWithResponseComplete</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventAsyncSaveWithResponseComplete(DataServiceInsertUpdateResponse response, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
            AsyncSaveWithResponseCompleteArgs e = new AsyncSaveWithResponseCompleteArgs(response, failedReasonText);
            OnAsyncSaveWithResponseComplete(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Raises the <see cref="AsyncSaveWithResponseComplete">AsyncSaveWithResponseComplete</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnAsyncSaveWithResponseComplete(object sender, AsyncSaveWithResponseCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
            AsyncSaveWithResponseCompleteEventHandler handler = AsyncSaveWithResponseComplete;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
        }










        void RaiseEventEntityRowReceived()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventEntityRowReceived", IfxTraceCategory.Enter);
            EntityRowReceivedEventHandler handler = EntityRowReceived;
            if (handler != null)
            {
                handler(this, new EntityRowReceivedArgs());
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventEntityRowReceived", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Calls the <see cref="OnCrudFailed">OnCrudFailed</see> to raise the <see cref="CrudFailed">CrudFailed</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventCrudFailed(int failureCode, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCrudFailed", IfxTraceCategory.Enter);
            CrudFailedArgs e = new CrudFailedArgs(failureCode, failedReasonText);
            OnCrudFailed(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCrudFailed", IfxTraceCategory.Leave);
        }


        /// <summary>
        ///     Calls the <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see>
        ///     method to raise the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event passing in a
        ///     reference of this business object for the ActiveBusinessObject parameter. As it
        ///     bubbles up to an Entity Properties control, that control will pass in a reference
        ///     of itself for the ActivePropertiesControl parameter. As it bubbles up to the Entity
        ///     Manager control, that control will pass in a reference of itself for the
        ///     ActiveEntityControl parameter. It should continue to bubble up to the top level
        ///     control. This notifies all controls along the about which controls are active and
        ///     the current state so they can always be configures accordingly. Now that the top
        ///     level control (perhaps the main application window) has a reference to these 3
        ///     important objects, it can easily communicate with then as the use interacts with
        ///     the application.
        /// </summary>        
        void RaiseEventCurrentEntityStateChanged(EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCurrentEntityStateChanged", new ValuePair[] {new ValuePair("state", state) }, IfxTraceCategory.Enter);
            CurrentEntityStateArgs e = new CurrentEntityStateArgs(state, null, null, this);
            OnCurrentEntityStateChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCurrentEntityStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     Raises the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see>
        ///     event passing in a reference of this business object for the ActiveBusinessObject
        ///     parameter. As it bubbles up to an Entity Properties control, that control will pass
        ///     in a reference of itself for the ActivePropertiesControl parameter. As it bubbles
        ///     up to the Entity Manager control, that control will pass in a reference of itself
        ///     for the ActiveEntityControl parameter. It should continue to bubble up to the top
        ///     level control. This notifies all controls along the about which controls are active
        ///     and the current state so they can always be configures accordingly. Now that the
        ///     top level control (perhaps the main application window) has a reference to these 3
        ///     important objects, it can easily communicate with then as the use interacts with
        ///     the application.
        /// </summary>
        void OnCurrentEntityStateChanged(object sender, CurrentEntityStateArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", new ValuePair[] {new ValuePair("e.State", e.State) }, IfxTraceCategory.Enter);
            CurrentEntityStateEventHandler handler = CurrentEntityStateChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     This method is obsolete and is replaced with <see cref="RaiseEventControlValidStateChanged">RaiseEventControlValidStateChanged</see> which
        ///     is called in the validation section of this class in the partial class
        ///     WcApplicationVersion.bll.vld.cs code file.
        /// </summary>
        void RaiseEventBrokenRuleChanged(string rule)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventBrokenRuleChanged", new ValuePair[] {new ValuePair("rule", rule) }, IfxTraceCategory.Enter);
            BrokenRuleArgs e = new BrokenRuleArgs(rule);
            OnBrokenRuleChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventBrokenRuleChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>This method is mostly obsolete, but remains because it’s an easy way to be
        ///     notified of the current broken rule.</para>
        /// 	<para>
        ///         This method is hooked to the <see cref="BrokenRuleChanged">BrokenRuleChanged</see> event which is initiated by
        ///         BrokenRuleManager and bubbles up to the business object and then to this
        ///         control (ucWcApplicationVersionProps) (or in some cases a ucEntityList control when it’s in
        ///         Read/Write mode).
        ///     </para>
        /// 	<para><br/>
        ///     For information on how Broken Rules are currently being managed, see (in the order
        ///     of execution):</para>
        /// 	<list type="bullet">
        /// 		<item>In the business object’s data property – FieldName_Validate();</item>
        /// 		<item>
        ///             In the FieldName_Validate method this line executes:<br/>
        ///             _brokenRuleManager.<see cref="TypeServices.BrokenRuleManager.AddBrokenRuleForProperty">AddBrokenRuleForProperty</see>("FieldName",
        ///             BROKENRULE_ FieldName _SomeRuleName);
        ///         </item>
        /// 		<item>
        /// 			<see cref="TypeServices.BrokenRuleManager.SetEntityValidState">BrokenRuleManager.SetEntityValidState()</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
       private void OnBrokenRuleChanged(object sender, BrokenRuleArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Enter);
            BrokenRuleEventHandler handler = BrokenRuleChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// Notifies an event handler that a property value as change. Currently not being
        /// uses.
        /// </summary>
        private void OnPropertyValueChanged(object sender, PropertyValueChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnPropertyValueChanged", new ValuePair[] {new ValuePair("e.PropertyName", e.PropertyName), new ValuePair("e.IsValid", e.IsValid), new ValuePair("e.BrokenRules", e.BrokenRules) }, IfxTraceCategory.Enter);
            PropertyValueChangedEventHandler handler = PropertyValueChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnPropertyValueChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     Replaces the RaiseEventBrokenRuleChanged method and used in the validation section
        ///     of this class in the partial class WcApplicationVersion.bll.vld.cs code file. Validation code will
        ///     pass in property name and an isValid flag. This will call the <see cref="OnControlValidStateChanged">OnControlValidStateChanged</see> method which will
        ///     raise the <see cref="ControlValidStateChanged">ControlValidStateChanged</see> event
        ///     up to the parent. The parent will then take the appropriate actions such as setting
        ///     the control’s valid/not valid appearance.
        /// </summary>
        void RaiseEventControlValidStateChanged(string propertyName, bool isValid, bool isDirty)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventControlValidStateChanged", new ValuePair[] {new ValuePair("isValid", isValid) }, IfxTraceCategory.Enter);
            ControlValidStateChangedArgs e = new ControlValidStateChangedArgs(propertyName, isValid, isDirty);
            OnControlValidStateChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventControlValidStateChanged", IfxTraceCategory.Leave);
        }       

        /// <summary>
        /// Raise the ControlValidStateChanged event up to the parent. The parent will then
        /// take the appropriate actions such as setting the control’s valid/not valid
        /// appearance.
        /// </summary>
        private void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", new ValuePair[] {new ValuePair("e.IsValid", e.IsValid) }, IfxTraceCategory.Enter);
            ControlValidStateChangedEventHandler handler = ControlValidStateChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcApplicationVersionProps.OnRestrictedTextLengthChanged">ucWcApplicationVersionProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcApplicationVersionProps.SetRestrictedStringLengthText">ucWcApplicationVersionProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void RaiseEventRestrictedTextLengthChanged()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Enter);
            RestrictedTextLengthChangedArgs e = new RestrictedTextLengthChangedArgs();
            RestrictedTextLengthChangedEventHandler handler = RestrictedTextLengthChanged;
            if (handler != null)
            {
                handler(this, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<param name="oldVal">The original property value.</param>
        /// 	<param name="newVal">The new property value.</param>        
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcApplicationVersionProps.OnRestrictedTextLengthChanged">ucWcApplicationVersionProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcApplicationVersionProps.SetRestrictedStringLengthText">ucWcApplicationVersionProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void RaiseEventRestrictedTextLengthChanged(string oldVal, string newVal)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Enter);
            bool lenChanged = false;
            if (oldVal == null && newVal != null)
            {
                if (newVal.Trim().Length > 0)
                {
                    lenChanged = true;
                }
            }
            else if (newVal == null && oldVal != null)
            {
                if (oldVal.Trim().Length > 0)
                {
                    lenChanged = true;
                }
            }
            else if (oldVal != null && newVal != null)
            {
                if (oldVal.Trim().Length != newVal.Trim().Length)
                {
                    lenChanged = true;
                }
            }
            if (lenChanged == true)
            {
                RestrictedTextLengthChangedArgs e = new RestrictedTextLengthChangedArgs();
                RestrictedTextLengthChangedEventHandler handler = RestrictedTextLengthChanged;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Leave);
        }        


		#endregion Events


		#region Flag Props


        /// <summary>
        ///     The name of the (text type – TextBox, TextBlock, etc.) control in the UI that
        ///     currently has the focus. This is used by the <see cref="ActiveRestrictedStringPropertyLength">ActiveRestrictedStringPropertyLength</see>
        ///     method to which returns the remaining available text length for the text control
        ///     with that name. It was decided to persist the name of the UI control in this
        ///     business object property rather than in the UI because it seemed to make sense to
        ///     have all of this logic centralized, and also because it simplifies things.
        /// </summary>
        [Browsable(false)]
        public string ActiveRestrictedStringProperty
        {
            get { return _activeRestrictedStringProperty; }
            set 
            { 
                _activeRestrictedStringProperty = value;
            }
        }

        /// <summary>
        ///     Returns the remaining available text length for the text control named by
        ///     <see cref="ActiveRestrictedStringProperty">ActiveRestrictedStringProperty</see>.
        /// </summary>
        [Browsable(false)]
        public int ActiveRestrictedStringPropertyLength
        {
            get
            {
                switch (_activeRestrictedStringProperty)
                {
                    case "ApVrsn_Server":
                        if (_data.C.ApVrsn_Server == null)
                        {
                            return STRINGSIZE_ApVrsn_Server;
                        }
                        else
                        {
                            return (STRINGSIZE_ApVrsn_Server - _data.C.ApVrsn_Server.Length);
                        }
                        break;
                    
                    case "ApVrsn_DbName":
                        if (_data.C.ApVrsn_DbName == null)
                        {
                            return STRINGSIZE_ApVrsn_DbName;
                        }
                        else
                        {
                            return (STRINGSIZE_ApVrsn_DbName - _data.C.ApVrsn_DbName.Length);
                        }
                        break;
                    
                    case "ApVrsn_SolutionPath":
                        if (_data.C.ApVrsn_SolutionPath == null)
                        {
                            return STRINGSIZE_ApVrsn_SolutionPath;
                        }
                        else
                        {
                            return (STRINGSIZE_ApVrsn_SolutionPath - _data.C.ApVrsn_SolutionPath.Length);
                        }
                        break;
                    
                    case "ApVrsn_DefaultUIAssembly":
                        if (_data.C.ApVrsn_DefaultUIAssembly == null)
                        {
                            return STRINGSIZE_ApVrsn_DefaultUIAssembly;
                        }
                        else
                        {
                            return (STRINGSIZE_ApVrsn_DefaultUIAssembly - _data.C.ApVrsn_DefaultUIAssembly.Length);
                        }
                        break;
                    
                    case "ApVrsn_DefaultUIAssemblyPath":
                        if (_data.C.ApVrsn_DefaultUIAssemblyPath == null)
                        {
                            return STRINGSIZE_ApVrsn_DefaultUIAssemblyPath;
                        }
                        else
                        {
                            return (STRINGSIZE_ApVrsn_DefaultUIAssemblyPath - _data.C.ApVrsn_DefaultUIAssemblyPath.Length);
                        }
                        break;
                    
                    case "ApVrsn_DefaultWireTypePath":
                        if (_data.C.ApVrsn_DefaultWireTypePath == null)
                        {
                            return STRINGSIZE_ApVrsn_DefaultWireTypePath;
                        }
                        else
                        {
                            return (STRINGSIZE_ApVrsn_DefaultWireTypePath - _data.C.ApVrsn_DefaultWireTypePath.Length);
                        }
                        break;
                    
                    case "ApVrsn_WebServerURL":
                        if (_data.C.ApVrsn_WebServerURL == null)
                        {
                            return STRINGSIZE_ApVrsn_WebServerURL;
                        }
                        else
                        {
                            return (STRINGSIZE_ApVrsn_WebServerURL - _data.C.ApVrsn_WebServerURL.Length);
                        }
                        break;
                    
                    case "ApVrsn_WebsiteCodeFolderPath":
                        if (_data.C.ApVrsn_WebsiteCodeFolderPath == null)
                        {
                            return STRINGSIZE_ApVrsn_WebsiteCodeFolderPath;
                        }
                        else
                        {
                            return (STRINGSIZE_ApVrsn_WebsiteCodeFolderPath - _data.C.ApVrsn_WebsiteCodeFolderPath.Length);
                        }
                        break;
                    
                    case "ApVrsn_WebserviceCodeFolderPath":
                        if (_data.C.ApVrsn_WebserviceCodeFolderPath == null)
                        {
                            return STRINGSIZE_ApVrsn_WebserviceCodeFolderPath;
                        }
                        else
                        {
                            return (STRINGSIZE_ApVrsn_WebserviceCodeFolderPath - _data.C.ApVrsn_WebserviceCodeFolderPath.Length);
                        }
                        break;
                    
                    case "ApVrsn_StoredProcCodeFolder":
                        if (_data.C.ApVrsn_StoredProcCodeFolder == null)
                        {
                            return STRINGSIZE_ApVrsn_StoredProcCodeFolder;
                        }
                        else
                        {
                            return (STRINGSIZE_ApVrsn_StoredProcCodeFolder - _data.C.ApVrsn_StoredProcCodeFolder.Length);
                        }
                        break;
                    
                    case "ApVrsn_Notes":
                        if (_data.C.ApVrsn_Notes == null)
                        {
                            return STRINGSIZE_ApVrsn_Notes;
                        }
                        else
                        {
                            return (STRINGSIZE_ApVrsn_Notes - _data.C.ApVrsn_Notes.Length);
                        }
                        break;
                    }
                return 0;
            }
        }

 

		#endregion Flag Props


		#region Data Props


        /// <summary>
        ///     Called by data field properties in their getter block. This method will determine
        ///     if he <see cref="TypeServices.EntityState">state</see> has changed by the getter
        ///     being called. If it has changed, the <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see>
        ///     method will be called to notify the object using this business object.
        /// </summary>
       private void CheckEntityState(EntityStateSwitch es)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", new ValuePair[] {new ValuePair("_data.S.Switch", _data.S.Switch), new ValuePair("es", es) }, IfxTraceCategory.Enter);
                if (es != _data.S.Switch)
                {
                    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// This property does nothing and is used as a stub for a 'Rich Data Grid Column' binding to this entity
        /// </summary>
        public string RichGrid
        {
            get { return ""; }
        }


        private IBusinessObjectV2 _parentBusinessObject = null;

        public IBusinessObjectV2 ParentBusinessObject
        {
            get { return _parentBusinessObject; }
            set { _parentBusinessObject = value; }
        }

       public Guid? GuidPrimaryKey()
       {
           return _data.C.ApVrsn_Id; 
       }

       public long? LongPrimaryKey()
       {
           throw new NotImplementedException("WcApplicationVersion_Bll LongPrimaryKey() Not Implemented"); 
       }

       public int? IntPrimaryKey()
       {
           throw new NotImplementedException("WcApplicationVersion_Bll LongPrimaryKey() Not Implemented"); 
       }

       public short? ShortPrimaryKey()
       {
           throw new NotImplementedException("WcApplicationVersion_Bll LongPrimaryKey() Not Implemented"); 
       }

       public byte? BytePrimaryKey()
       {
           throw new NotImplementedException("WcApplicationVersion_Bll LongPrimaryKey() Not Implemented"); 
       }

       public object ObjectPrimaryKey()
       {
           return _data.C.ApVrsn_Id;
       }


        /// <summary>
        /// This is the Standing Foreign Key property. This value remains constant when
        /// calling the new method where all data fields are cleared and set to their ‘new’ default
        /// values. This allows creating new entities for the same parent. When an entity is
        /// fetched from the data store and used as the current entity in this business object, the
        /// Standing Foreign Key value is reset using the value from the fetched entity.
        /// </summary>
        public Guid StandingFK
        {
            get { return _StandingFK; }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    _StandingFK = value;
                    _data.C.ApVrsn_Ap_Id_noevents = _StandingFK;
                    _data.O.ApVrsn_Ap_Id_noevents = _StandingFK;
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", IfxTraceCategory.Leave);
                }
            }
        }

       /// <summary>
       /// 	<para>The Primary Key for WcApplicationVersion</para>
       /// </summary>
        public Guid ApVrsn_Id
        {
            get
            {
                return _data.C.ApVrsn_Id;
            }
        }

		
        public Guid? ApVrsn_Ap_Id
        {
            get
            {
                return _data.C.ApVrsn_Ap_Id;
            }
        }

		
        public Int32? AttachmentCount
        {
            get
            {
                return _data.C.AttachmentCount;
            }
        }

		
        public String AttachmentFileNames
        {
            get
            {
                return _data.C.AttachmentFileNames;
            }
        }

		
        public Int32? DiscussionCount
        {
            get
            {
                return _data.C.DiscussionCount;
            }
        }

		
        public String DiscussionTitles
        {
            get
            {
                return _data.C.DiscussionTitles;
            }
        }


        public String VersionNo
        {
            get
            {
                return _data.C.VersionNo;
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="ApVrsn_MajorVersion">ApVrsn_MajorVersion</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the ApVrsn_MajorVersion field. If it’s not numeric, then ApVrsn_MajorVersion is set to
        ///     null.</para>
        /// </summary>
        public String ApVrsn_MajorVersion_asString
        {
            get
            {
                return _data.C.ApVrsn_MajorVersion.ToString();
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MajorVersion - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
					if (BLLHelper.IsNumber(value))
                    {
                        try
                        {
							ApVrsn_MajorVersion = Byte.Parse(value);
                        }
                        catch (FormatException e)
                        {
                            ApVrsn_MajorVersion = 0;
                        }
                    }
                    else
                    {
                        ApVrsn_MajorVersion = 0;
                    }
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MajorVersion - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MajorVersion - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Byte ApVrsn_MajorVersion
        {
            get
            {
                return _data.C.ApVrsn_MajorVersion;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MajorVersion - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_MajorVersion == value) { return; }
                    _data.C.ApVrsn_MajorVersion = value;
                    CustomPropertySetterCode("ApVrsn_MajorVersion");
                    ApVrsn_MajorVersion_Validate();
                    CheckEntityState(es);
                    Notify("ApVrsn_MajorVersion");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MajorVersion - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MajorVersion - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="ApVrsn_MinorVersion">ApVrsn_MinorVersion</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the ApVrsn_MinorVersion field. If it’s not numeric, then ApVrsn_MinorVersion is set to
        ///     null.</para>
        /// </summary>
        public String ApVrsn_MinorVersion_asString
        {
            get
            {
                if (null == _data.C.ApVrsn_MinorVersion)
                {
                    return "";
                }
                else
                {
                    return _data.C.ApVrsn_MinorVersion.ToString();
                }
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MinorVersion - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
					if (BLLHelper.IsNumber(value))
                    {
                        try
                        {
							ApVrsn_MinorVersion = Byte.Parse(value);
                        }
                        catch (FormatException e)
                        {
                            ApVrsn_MinorVersion = null;
                        }
                    }
                    else
                    {
                        ApVrsn_MinorVersion = null;
                    }
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MinorVersion - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MinorVersion - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Byte? ApVrsn_MinorVersion
        {
            get
            {
                return _data.C.ApVrsn_MinorVersion;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MinorVersion - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_MinorVersion == value) { return; }
                    _data.C.ApVrsn_MinorVersion = value;
                    CustomPropertySetterCode("ApVrsn_MinorVersion");
                    ApVrsn_MinorVersion_Validate();
                    CheckEntityState(es);
                    Notify("ApVrsn_MinorVersion");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MinorVersion - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MinorVersion - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="ApVrsn_VersionIteration">ApVrsn_VersionIteration</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the ApVrsn_VersionIteration field. If it’s not numeric, then ApVrsn_VersionIteration is set to
        ///     null.</para>
        /// </summary>
        public String ApVrsn_VersionIteration_asString
        {
            get
            {
                if (null == _data.C.ApVrsn_VersionIteration)
                {
                    return "";
                }
                else
                {
                    return _data.C.ApVrsn_VersionIteration.ToString();
                }
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_VersionIteration - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
					if (BLLHelper.IsNumber(value))
                    {
                        try
                        {
							ApVrsn_VersionIteration = Byte.Parse(value);
                        }
                        catch (FormatException e)
                        {
                            ApVrsn_VersionIteration = null;
                        }
                    }
                    else
                    {
                        ApVrsn_VersionIteration = null;
                    }
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_VersionIteration - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_VersionIteration - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Byte? ApVrsn_VersionIteration
        {
            get
            {
                return _data.C.ApVrsn_VersionIteration;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_VersionIteration - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_VersionIteration == value) { return; }
                    _data.C.ApVrsn_VersionIteration = value;
                    CustomPropertySetterCode("ApVrsn_VersionIteration");
                    ApVrsn_VersionIteration_Validate();
                    CheckEntityState(es);
                    Notify("ApVrsn_VersionIteration");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_VersionIteration - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_VersionIteration - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String ApVrsn_Server
        {
            get
            {
                return _data.C.ApVrsn_Server;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Server - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_Server == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.ApVrsn_Server == null || _data.C.ApVrsn_Server.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.ApVrsn_Server == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.ApVrsn_Server = null;
                    }
                    else
                    {
                        _data.C.ApVrsn_Server = value.Trim();
                    }
                    //else if ((_data.C.ApVrsn_Server == null || _data.C.ApVrsn_Server.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.ApVrsn_Server == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.ApVrsn_Server == null || _data.C.ApVrsn_Server.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.ApVrsn_Server = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.ApVrsn_Server = value;
                    //}
                    CustomPropertySetterCode("ApVrsn_Server");
                    ApVrsn_Server_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.ApVrsn_Server, _data.C.ApVrsn_Server);
                        
					Notify("ApVrsn_Server");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Server - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Server - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String ApVrsn_DbName
        {
            get
            {
                return _data.C.ApVrsn_DbName;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DbName - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_DbName == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.ApVrsn_DbName == null || _data.C.ApVrsn_DbName.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.ApVrsn_DbName == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.ApVrsn_DbName = null;
                    }
                    else
                    {
                        _data.C.ApVrsn_DbName = value.Trim();
                    }
                    //else if ((_data.C.ApVrsn_DbName == null || _data.C.ApVrsn_DbName.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.ApVrsn_DbName == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.ApVrsn_DbName == null || _data.C.ApVrsn_DbName.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.ApVrsn_DbName = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.ApVrsn_DbName = value;
                    //}
                    CustomPropertySetterCode("ApVrsn_DbName");
                    ApVrsn_DbName_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.ApVrsn_DbName, _data.C.ApVrsn_DbName);
                        
					Notify("ApVrsn_DbName");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DbName - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DbName - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String ApVrsn_SolutionPath
        {
            get
            {
                return _data.C.ApVrsn_SolutionPath;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_SolutionPath - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_SolutionPath == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.ApVrsn_SolutionPath == null || _data.C.ApVrsn_SolutionPath.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.ApVrsn_SolutionPath == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.ApVrsn_SolutionPath = null;
                    }
                    else
                    {
                        _data.C.ApVrsn_SolutionPath = value.Trim();
                    }
                    //else if ((_data.C.ApVrsn_SolutionPath == null || _data.C.ApVrsn_SolutionPath.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.ApVrsn_SolutionPath == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.ApVrsn_SolutionPath == null || _data.C.ApVrsn_SolutionPath.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.ApVrsn_SolutionPath = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.ApVrsn_SolutionPath = value;
                    //}
                    CustomPropertySetterCode("ApVrsn_SolutionPath");
                    ApVrsn_SolutionPath_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.ApVrsn_SolutionPath, _data.C.ApVrsn_SolutionPath);
                        
					Notify("ApVrsn_SolutionPath");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_SolutionPath - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_SolutionPath - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String ApVrsn_DefaultUIAssembly
        {
            get
            {
                return _data.C.ApVrsn_DefaultUIAssembly;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultUIAssembly - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_DefaultUIAssembly == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.ApVrsn_DefaultUIAssembly == null || _data.C.ApVrsn_DefaultUIAssembly.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.ApVrsn_DefaultUIAssembly == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.ApVrsn_DefaultUIAssembly = null;
                    }
                    else
                    {
                        _data.C.ApVrsn_DefaultUIAssembly = value.Trim();
                    }
                    //else if ((_data.C.ApVrsn_DefaultUIAssembly == null || _data.C.ApVrsn_DefaultUIAssembly.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.ApVrsn_DefaultUIAssembly == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.ApVrsn_DefaultUIAssembly == null || _data.C.ApVrsn_DefaultUIAssembly.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.ApVrsn_DefaultUIAssembly = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.ApVrsn_DefaultUIAssembly = value;
                    //}
                    CustomPropertySetterCode("ApVrsn_DefaultUIAssembly");
                    ApVrsn_DefaultUIAssembly_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.ApVrsn_DefaultUIAssembly, _data.C.ApVrsn_DefaultUIAssembly);
                        
					Notify("ApVrsn_DefaultUIAssembly");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultUIAssembly - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultUIAssembly - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String ApVrsn_DefaultUIAssemblyPath
        {
            get
            {
                return _data.C.ApVrsn_DefaultUIAssemblyPath;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultUIAssemblyPath - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_DefaultUIAssemblyPath == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.ApVrsn_DefaultUIAssemblyPath == null || _data.C.ApVrsn_DefaultUIAssemblyPath.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.ApVrsn_DefaultUIAssemblyPath == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.ApVrsn_DefaultUIAssemblyPath = null;
                    }
                    else
                    {
                        _data.C.ApVrsn_DefaultUIAssemblyPath = value.Trim();
                    }
                    //else if ((_data.C.ApVrsn_DefaultUIAssemblyPath == null || _data.C.ApVrsn_DefaultUIAssemblyPath.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.ApVrsn_DefaultUIAssemblyPath == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.ApVrsn_DefaultUIAssemblyPath == null || _data.C.ApVrsn_DefaultUIAssemblyPath.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.ApVrsn_DefaultUIAssemblyPath = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.ApVrsn_DefaultUIAssemblyPath = value;
                    //}
                    CustomPropertySetterCode("ApVrsn_DefaultUIAssemblyPath");
                    ApVrsn_DefaultUIAssemblyPath_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.ApVrsn_DefaultUIAssemblyPath, _data.C.ApVrsn_DefaultUIAssemblyPath);
                        
					Notify("ApVrsn_DefaultUIAssemblyPath");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultUIAssemblyPath - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultUIAssemblyPath - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String ApVrsn_DefaultWireTypePath
        {
            get
            {
                return _data.C.ApVrsn_DefaultWireTypePath;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultWireTypePath - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_DefaultWireTypePath == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.ApVrsn_DefaultWireTypePath == null || _data.C.ApVrsn_DefaultWireTypePath.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.ApVrsn_DefaultWireTypePath == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.ApVrsn_DefaultWireTypePath = null;
                    }
                    else
                    {
                        _data.C.ApVrsn_DefaultWireTypePath = value.Trim();
                    }
                    //else if ((_data.C.ApVrsn_DefaultWireTypePath == null || _data.C.ApVrsn_DefaultWireTypePath.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.ApVrsn_DefaultWireTypePath == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.ApVrsn_DefaultWireTypePath == null || _data.C.ApVrsn_DefaultWireTypePath.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.ApVrsn_DefaultWireTypePath = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.ApVrsn_DefaultWireTypePath = value;
                    //}
                    CustomPropertySetterCode("ApVrsn_DefaultWireTypePath");
                    ApVrsn_DefaultWireTypePath_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.ApVrsn_DefaultWireTypePath, _data.C.ApVrsn_DefaultWireTypePath);
                        
					Notify("ApVrsn_DefaultWireTypePath");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultWireTypePath - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultWireTypePath - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String ApVrsn_WebServerURL
        {
            get
            {
                return _data.C.ApVrsn_WebServerURL;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebServerURL - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_WebServerURL == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.ApVrsn_WebServerURL == null || _data.C.ApVrsn_WebServerURL.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.ApVrsn_WebServerURL == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.ApVrsn_WebServerURL = null;
                    }
                    else
                    {
                        _data.C.ApVrsn_WebServerURL = value.Trim();
                    }
                    //else if ((_data.C.ApVrsn_WebServerURL == null || _data.C.ApVrsn_WebServerURL.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.ApVrsn_WebServerURL == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.ApVrsn_WebServerURL == null || _data.C.ApVrsn_WebServerURL.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.ApVrsn_WebServerURL = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.ApVrsn_WebServerURL = value;
                    //}
                    CustomPropertySetterCode("ApVrsn_WebServerURL");
                    ApVrsn_WebServerURL_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.ApVrsn_WebServerURL, _data.C.ApVrsn_WebServerURL);
                        
					Notify("ApVrsn_WebServerURL");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebServerURL - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebServerURL - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String ApVrsn_WebsiteCodeFolderPath
        {
            get
            {
                return _data.C.ApVrsn_WebsiteCodeFolderPath;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebsiteCodeFolderPath - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_WebsiteCodeFolderPath == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.ApVrsn_WebsiteCodeFolderPath == null || _data.C.ApVrsn_WebsiteCodeFolderPath.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.ApVrsn_WebsiteCodeFolderPath == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.ApVrsn_WebsiteCodeFolderPath = null;
                    }
                    else
                    {
                        _data.C.ApVrsn_WebsiteCodeFolderPath = value.Trim();
                    }
                    //else if ((_data.C.ApVrsn_WebsiteCodeFolderPath == null || _data.C.ApVrsn_WebsiteCodeFolderPath.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.ApVrsn_WebsiteCodeFolderPath == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.ApVrsn_WebsiteCodeFolderPath == null || _data.C.ApVrsn_WebsiteCodeFolderPath.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.ApVrsn_WebsiteCodeFolderPath = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.ApVrsn_WebsiteCodeFolderPath = value;
                    //}
                    CustomPropertySetterCode("ApVrsn_WebsiteCodeFolderPath");
                    ApVrsn_WebsiteCodeFolderPath_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.ApVrsn_WebsiteCodeFolderPath, _data.C.ApVrsn_WebsiteCodeFolderPath);
                        
					Notify("ApVrsn_WebsiteCodeFolderPath");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebsiteCodeFolderPath - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebsiteCodeFolderPath - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String ApVrsn_WebserviceCodeFolderPath
        {
            get
            {
                return _data.C.ApVrsn_WebserviceCodeFolderPath;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebserviceCodeFolderPath - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_WebserviceCodeFolderPath == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.ApVrsn_WebserviceCodeFolderPath == null || _data.C.ApVrsn_WebserviceCodeFolderPath.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.ApVrsn_WebserviceCodeFolderPath == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.ApVrsn_WebserviceCodeFolderPath = null;
                    }
                    else
                    {
                        _data.C.ApVrsn_WebserviceCodeFolderPath = value.Trim();
                    }
                    //else if ((_data.C.ApVrsn_WebserviceCodeFolderPath == null || _data.C.ApVrsn_WebserviceCodeFolderPath.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.ApVrsn_WebserviceCodeFolderPath == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.ApVrsn_WebserviceCodeFolderPath == null || _data.C.ApVrsn_WebserviceCodeFolderPath.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.ApVrsn_WebserviceCodeFolderPath = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.ApVrsn_WebserviceCodeFolderPath = value;
                    //}
                    CustomPropertySetterCode("ApVrsn_WebserviceCodeFolderPath");
                    ApVrsn_WebserviceCodeFolderPath_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.ApVrsn_WebserviceCodeFolderPath, _data.C.ApVrsn_WebserviceCodeFolderPath);
                        
					Notify("ApVrsn_WebserviceCodeFolderPath");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebserviceCodeFolderPath - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebserviceCodeFolderPath - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String ApVrsn_StoredProcCodeFolder
        {
            get
            {
                return _data.C.ApVrsn_StoredProcCodeFolder;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_StoredProcCodeFolder - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_StoredProcCodeFolder == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.ApVrsn_StoredProcCodeFolder == null || _data.C.ApVrsn_StoredProcCodeFolder.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.ApVrsn_StoredProcCodeFolder == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.ApVrsn_StoredProcCodeFolder = null;
                    }
                    else
                    {
                        _data.C.ApVrsn_StoredProcCodeFolder = value.Trim();
                    }
                    //else if ((_data.C.ApVrsn_StoredProcCodeFolder == null || _data.C.ApVrsn_StoredProcCodeFolder.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.ApVrsn_StoredProcCodeFolder == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.ApVrsn_StoredProcCodeFolder == null || _data.C.ApVrsn_StoredProcCodeFolder.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.ApVrsn_StoredProcCodeFolder = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.ApVrsn_StoredProcCodeFolder = value;
                    //}
                    CustomPropertySetterCode("ApVrsn_StoredProcCodeFolder");
                    ApVrsn_StoredProcCodeFolder_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.ApVrsn_StoredProcCodeFolder, _data.C.ApVrsn_StoredProcCodeFolder);
                        
					Notify("ApVrsn_StoredProcCodeFolder");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_StoredProcCodeFolder - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_StoredProcCodeFolder - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean ApVrsn_UseLegacyConnectionCode
        {
            get
            {
                return _data.C.ApVrsn_UseLegacyConnectionCode;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_UseLegacyConnectionCode - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_UseLegacyConnectionCode == value) { return; }
                    _data.C.ApVrsn_UseLegacyConnectionCode = value;
                    CustomPropertySetterCode("ApVrsn_UseLegacyConnectionCode");
                    ApVrsn_UseLegacyConnectionCode_Validate();
                    CheckEntityState(es);
                    Notify("ApVrsn_UseLegacyConnectionCode");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_UseLegacyConnectionCode - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_UseLegacyConnectionCode - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public String ApVrsn_DefaultConnectionCodeId_TextField
        {
            get
            {
                return _data.C.ApVrsn_DefaultConnectionCodeId_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultConnectionCodeId Setter", IfxTraceCategory.Enter);
                    if (_data.C.ApVrsn_DefaultConnectionCodeId_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.ApVrsn_DefaultConnectionCodeId_TextField == null || _data.C.ApVrsn_DefaultConnectionCodeId_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.ApVrsn_DefaultConnectionCodeId_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.ApVrsn_DefaultConnectionCodeId_TextField = value;
                    }
                    CustomPropertySetterCode("ApVrsn_DefaultConnectionCodeId_TextField");
                    Notify("ApVrsn_DefaultConnectionCodeId_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultConnectionCodeId Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultConnectionCodeId Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Guid? ApVrsn_DefaultConnectionCodeId
        {
            get
            {
                return _data.C.ApVrsn_DefaultConnectionCodeId;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultConnectionCodeId - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_DefaultConnectionCodeId == value) { return; }
                    _data.C.ApVrsn_DefaultConnectionCodeId = value;
                    CustomPropertySetterCode("ApVrsn_DefaultConnectionCodeId");
                    ApVrsn_DefaultConnectionCodeId_Validate();
                    CheckEntityState(es);
                    Notify("ApVrsn_DefaultConnectionCodeId");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = WcApplication_Bll_staticLists.WcApplicationConnectionStringKey_ComboItemList_BindingListProperty.FindItemById(_data.C.ApVrsn_DefaultConnectionCodeId);
                    if (obj == null)
                    {
                        ApVrsn_DefaultConnectionCodeId_TextField = "";
                    }
                    else
                    {
                        ApVrsn_DefaultConnectionCodeId_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultConnectionCodeId - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultConnectionCodeId - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean ApVrsn_IsMulticultural
        {
            get
            {
                return _data.C.ApVrsn_IsMulticultural;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsMulticultural - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_IsMulticultural == value) { return; }
                    _data.C.ApVrsn_IsMulticultural = value;
                    CustomPropertySetterCode("ApVrsn_IsMulticultural");
                    ApVrsn_IsMulticultural_Validate();
                    CheckEntityState(es);
                    Notify("ApVrsn_IsMulticultural");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsMulticultural - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsMulticultural - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String ApVrsn_Notes
        {
            get
            {
                return _data.C.ApVrsn_Notes;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Notes - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_Notes == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.ApVrsn_Notes == null || _data.C.ApVrsn_Notes.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.ApVrsn_Notes == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.ApVrsn_Notes = null;
                    }
                    else
                    {
                        _data.C.ApVrsn_Notes = value.Trim();
                    }
                    //else if ((_data.C.ApVrsn_Notes == null || _data.C.ApVrsn_Notes.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.ApVrsn_Notes == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.ApVrsn_Notes == null || _data.C.ApVrsn_Notes.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.ApVrsn_Notes = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.ApVrsn_Notes = value;
                    //}
                    CustomPropertySetterCode("ApVrsn_Notes");
                    ApVrsn_Notes_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.ApVrsn_Notes, _data.C.ApVrsn_Notes);
                        
					Notify("ApVrsn_Notes");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Notes - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Notes - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean ApVrsn_IsActiveRow
        {
            get
            {
                return _data.C.ApVrsn_IsActiveRow;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsActiveRow - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.ApVrsn_IsActiveRow == value) { return; }
                    _data.C.ApVrsn_IsActiveRow = value;
                    CustomPropertySetterCode("ApVrsn_IsActiveRow");
                    ApVrsn_IsActiveRow_Validate();
                    CheckEntityState(es);
                    Notify("ApVrsn_IsActiveRow");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsActiveRow - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsActiveRow - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean ApVrsn_IsDeleted
        {
            get
            {
                return _data.C.ApVrsn_IsDeleted;
            }
        }

		
        public Guid? ApVrsn_CreatedUserId
        {
            get
            {
                return _data.C.ApVrsn_CreatedUserId;
            }
        }


		/// <summary>
        ///  Use this property when binding in Grids so the Notify method won't prevent you from entering complete dates or the year part of a date.  Notify will refresh the UI cutting off anything after the first diget of the month.
        /// </summary>
        public String ApVrsn_CreatedDate_asString
        {
            get
            {
                if (null == _data.C.ApVrsn_CreatedDate)
                {
                    return "";
                }
                else
                {
                    return _data.C.ApVrsn_CreatedDate.ToString();
                }
            }
        }

		
        public DateTime? ApVrsn_CreatedDate
        {
            get
            {
                return _data.C.ApVrsn_CreatedDate;
            }
        }

		
        public Guid? ApVrsn_UserId
        {
            get
            {
                return _data.C.ApVrsn_UserId;
            }
        }


        public String UserName
        {
            get
            {
                return _data.C.UserName;
            }
        }


		/// <summary>
        ///  Use this property when binding in Grids so the Notify method won't prevent you from entering complete dates or the year part of a date.  Notify will refresh the UI cutting off anything after the first diget of the month.
        /// </summary>
        public String ApVrsn_LastModifiedDate_asString
        {
            get
            {
                if (null == _data.C.ApVrsn_LastModifiedDate)
                {
                    return "";
                }
                else
                {
                    return _data.C.ApVrsn_LastModifiedDate.ToString();
                }
            }
        }

		
        public DateTime? ApVrsn_LastModifiedDate
        {
            get
            {
                return _data.C.ApVrsn_LastModifiedDate;
            }
        }


        public Byte[] ApVrsn_Stamp
        {
            get
            {
                return _data.C.ApVrsn_Stamp;
            }
        }



		#endregion Data Props


		#region Concurrency and Data State


        private bool DeterminConcurrencyCheck(UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", IfxTraceCategory.Enter);
                switch (check)
                {
                    case UseConcurrencyCheck.UseDefaultSetting:
                        return true;
                    case UseConcurrencyCheck.UseConcurrencyCheck:
                        return true;
                    case UseConcurrencyCheck.BypassConcurrencyCheck:
                        return false;
                }
                return false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     This is a reference to the current data (<see cref="EntityWireType.WcApplicationVersion_Values">WcApplicationVersion_Values</see>) for this business object
        ///     (WcApplicationVersion_Bll). The Values Manager (<see cref="EntityWireType.WcApplicationVersion_ValuesMngr">WcApplicationVersion_ValuesMngr</see>) has three sets of WcApplicationVersion
        ///     data used for managing state and concurrency. For more information about the other
        ///     two sets of data, <see cref="Original">Original</see> and <see cref="Concurrency">Concurrency</see>, and how they are used, see the overviews of
        ///     <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Concurrency_Overview.html">
        ///     Concurrency</a> and <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Wire_Object_Overview.html">
        ///     Wire Objects</a>.
        /// </summary>
        /// <seealso cref="Original">Original Property</seealso>
        /// <seealso cref="Concurrency">Concurrency Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcApplicationVersion_Values Current
        {
            get
            {
                return _data.C;
            }
            set
            {
                //Management Code
                _data.C = value;
            }
        }

        /// <summary>
        ///     This is a reference to the original data (<see cref="EntityWireType.WcApplicationVersion_Values">WcApplicationVersion_Values</see>) for this business object
        ///     (WcApplicationVersion_Bll) which was either data for a ‘new-not dirty’ WcApplicationVersion or data for a WcApplicationVersion
        ///     retrieved from the data store. The Values Manager (<see cref="EntityWireType.WcApplicationVersion_ValuesMngr">WcApplicationVersion_ValuesMngr</see>) has three sets of WcApplicationVersion
        ///     data used for managing state and concurrency. For more information about the other
        ///     two sets of data, <see cref="Current">Current</see> and <see cref="Concurrency">Concurrency</see>, and how they are used, see the overview of
        ///     Concurrency and Wire Objects.
        /// </summary>
        /// <seealso cref="Current">Current Property</seealso>
        /// <seealso cref="Concurrency">Concurrency Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcApplicationVersion_Values Original
        {
            get
            {
                return _data.O;
            }
            set
            {
                //Management Code
                _data.O = value;
            }
        }

        /// <summary>*** Need to review before documenting ***</summary>
        /// <seealso cref="Current">Current Property</seealso>
        /// <seealso cref="Original">Original Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcApplicationVersion_Values Concurrency
        {
            get
            {
                return _data.X;
            }
            set
            {
                //Management Code
                _data.X = value;
            }
        }

        /// <value>
        ///     This class manages the <see cref="TypeServices.EntityState">EntityStateSwitch</see>
        ///     and is used to describe the current <see cref="TypeServices.EntityState">state</see> of this business object such as IsDirty,
        ///     IsValid and IsNew. The combinations of these six possible values describe the
        ///     entity state and are used for logic in configuring settings in the business object
        ///     as well as the UI.
        /// </value>
        /// <seealso cref="EntityWireType.WcApplicationVersion_ValuesMngr">WcApplicationVersion_ValuesMngr Class</seealso>
        [Browsable(false)]
        public override EntityState State
        {
            get
            {
                return _data.S;
            }
            set
            {
                //Management Code
                _data.S = value;
            }
        }

        /// <summary>
        ///     This is the combination of IsDirty, IsValid and IsNew and is managed by the
        ///     <see cref="TypeServices.EntityState">EntityState</see> class. The combinations of
        ///     these six possible values describe the entity <see cref="TypeServices.EntityState">state</see> and are used for logic in configuring
        ///     settings in the business object as well as the UI.
        /// </summary>
        /// <seealso cref="EntityWireType.WcApplicationVersion_ValuesMngr">WcApplicationVersion_ValuesMngr Class</seealso>
        public EntityStateSwitch StateSwitch
        {
            get
            {
                return _data.S.Switch;
            }
        }




		#endregion Concurrency and Data State

        [Browsable(false)]
        public override object this[string name]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                //Management Code
                throw new NotImplementedException();
            }
        }


		#region ITraceItem Members


        /// <summary>
        /// 	<para>
        ///         Used in IFX (the instrumentation framework) for tracing. This returns the short
        ///         version of the <see cref="ManagementServices.TraceItemList">TraceItemList</see>. The ‘Short Version’
        ///         returns only the entity’s data fields, <see cref="TypeServices.BrokenRuleManager.GetBrokenRuleListForEntity">BrokenRules</see>
        ///         and <see cref="TypeServices.EntityStateSwitch">EntityStateSwitch</see>.
        ///         However, it also calls the <see cref="GetTraceItemsShortListCustom">GetTraceItemsShortListCustom</see> method in the
        ///         WcApplicationVersion.bll.cust.cs code file so a developer can customize the list by adding
        ///         additional items to it.
        ///     </para>
        /// 	<para>
        ///         Keep in mind that this is the short versions so don’t get carried away adding
        ///         to many things to it. For a robust and extensive list of items to record in the
        ///         trace, use the <see cref="GetTraceItemsLongList">GetTraceItemsLongList</see>
        ///         and <see cref="GetTraceItemsLongListCustom">GetTraceItemsLongListCustom</see>
        ///         methods.
        ///     </para>
        /// </summary>
        public TraceItemList GetTraceItemsShortList()
        {
            if (null == _traceItems)
            {
                _traceItems = new TraceItemList("WcApplicationVersion_Bll");
            }
            else
            {
                _traceItems.Clear();
            }
            _traceItems.Add("StateSwitch", StateSwitch.ToString(), TraceDataTypes.String);
            _traceItems.Add("Broken Rules", BrokenRuleManagerProperty.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_Id", _data.C.ApVrsn_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("ApVrsn_Ap_Id", _data.C.ApVrsn_Ap_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("AttachmentCount", _data.C.AttachmentCount.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("AttachmentFileNames", _data.C.AttachmentFileNames.ToString(), TraceDataTypes.String);
			_traceItems.Add("DiscussionCount", _data.C.DiscussionCount.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("DiscussionTitles", _data.C.DiscussionTitles.ToString(), TraceDataTypes.String);
			_traceItems.Add("VersionNo", _data.C.VersionNo.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_MajorVersion", _data.C.ApVrsn_MajorVersion.ToString(), TraceDataTypes.Byte);
			_traceItems.Add("ApVrsn_MinorVersion", _data.C.ApVrsn_MinorVersion.ToString(), TraceDataTypes.Byte);
			_traceItems.Add("ApVrsn_VersionIteration", _data.C.ApVrsn_VersionIteration.ToString(), TraceDataTypes.Byte);
			_traceItems.Add("ApVrsn_Server", _data.C.ApVrsn_Server.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_DbName", _data.C.ApVrsn_DbName.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_SolutionPath", _data.C.ApVrsn_SolutionPath.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_DefaultUIAssembly", _data.C.ApVrsn_DefaultUIAssembly.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_DefaultUIAssemblyPath", _data.C.ApVrsn_DefaultUIAssemblyPath.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_DefaultWireTypePath", _data.C.ApVrsn_DefaultWireTypePath.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_WebServerURL", _data.C.ApVrsn_WebServerURL.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_WebsiteCodeFolderPath", _data.C.ApVrsn_WebsiteCodeFolderPath.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_WebserviceCodeFolderPath", _data.C.ApVrsn_WebserviceCodeFolderPath.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_StoredProcCodeFolder", _data.C.ApVrsn_StoredProcCodeFolder.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_UseLegacyConnectionCode", _data.C.ApVrsn_UseLegacyConnectionCode.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("ApVrsn_DefaultConnectionCodeId", _data.C.ApVrsn_DefaultConnectionCodeId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("ApVrsn_DefaultConnectionCodeId_TextField", _data.C.ApVrsn_DefaultConnectionCodeId_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_IsMulticultural", _data.C.ApVrsn_IsMulticultural.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("ApVrsn_Notes", _data.C.ApVrsn_Notes.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_IsActiveRow", _data.C.ApVrsn_IsActiveRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("ApVrsn_IsDeleted", _data.C.ApVrsn_IsDeleted.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("ApVrsn_CreatedUserId", _data.C.ApVrsn_CreatedUserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("ApVrsn_CreatedDate", _data.C.ApVrsn_CreatedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("ApVrsn_UserId", _data.C.ApVrsn_UserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("UserName", _data.C.UserName.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_LastModifiedDate", _data.C.ApVrsn_LastModifiedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("ApVrsn_Stamp", _data.C.ApVrsn_Stamp.ToString(), TraceDataTypes.ByteArray);
			GetTraceItemsShortListCustom();
            return _traceItems;
        }

        /// <summary>
        /// 	<para>
        ///         Used in IFX (the instrumentation framework) for tracing. By default, this is
        ///         code-genned to return the same information as <see cref="GetTraceItemsShortList">GetTraceItemsShortList</see>, but is intended for
        ///         developers to add additional informaiton that would be helpful in a trace. This
        ///         method also calls the <see cref="GetTraceItemsLongListCustom">GetTraceItemsLongListCustom</see> method in the
        ///         WcApplicationVersion.bll.cust.cs code file so a developer can customize the list by adding
        ///         additional items to it.
        ///     </para>
        /// 	<para>
        ///         Remember: <see cref="GetTraceItemsShortList">GetTraceItemsShortList</see> is
        ///         intended for a limited list of items (the basic items) to trace and
        ///         <strong>GetTraceItemsLongList</strong> is intended for a robust and extensive
        ///         list of items to record in the trace.
        ///     </para>
        /// </summary>
        public TraceItemList GetTraceItemsLongList()
        {
            if (null == _traceItems)
            {
                _traceItems = new TraceItemList("WcApplicationVersion_Bll");
            }
            else
            {
                _traceItems.Clear();
            }
            _traceItems.Add("StateSwitch", StateSwitch.ToString(), TraceDataTypes.String);
            _traceItems.Add("Broken Rules", BrokenRuleManagerProperty.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_Id", _data.C.ApVrsn_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("ApVrsn_Ap_Id", _data.C.ApVrsn_Ap_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("AttachmentCount", _data.C.AttachmentCount.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("AttachmentFileNames", _data.C.AttachmentFileNames.ToString(), TraceDataTypes.String);
			_traceItems.Add("DiscussionCount", _data.C.DiscussionCount.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("DiscussionTitles", _data.C.DiscussionTitles.ToString(), TraceDataTypes.String);
			_traceItems.Add("VersionNo", _data.C.VersionNo.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_MajorVersion", _data.C.ApVrsn_MajorVersion.ToString(), TraceDataTypes.Byte);
			_traceItems.Add("ApVrsn_MinorVersion", _data.C.ApVrsn_MinorVersion.ToString(), TraceDataTypes.Byte);
			_traceItems.Add("ApVrsn_VersionIteration", _data.C.ApVrsn_VersionIteration.ToString(), TraceDataTypes.Byte);
			_traceItems.Add("ApVrsn_Server", _data.C.ApVrsn_Server.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_DbName", _data.C.ApVrsn_DbName.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_SolutionPath", _data.C.ApVrsn_SolutionPath.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_DefaultUIAssembly", _data.C.ApVrsn_DefaultUIAssembly.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_DefaultUIAssemblyPath", _data.C.ApVrsn_DefaultUIAssemblyPath.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_DefaultWireTypePath", _data.C.ApVrsn_DefaultWireTypePath.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_WebServerURL", _data.C.ApVrsn_WebServerURL.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_WebsiteCodeFolderPath", _data.C.ApVrsn_WebsiteCodeFolderPath.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_WebserviceCodeFolderPath", _data.C.ApVrsn_WebserviceCodeFolderPath.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_StoredProcCodeFolder", _data.C.ApVrsn_StoredProcCodeFolder.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_UseLegacyConnectionCode", _data.C.ApVrsn_UseLegacyConnectionCode.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("ApVrsn_DefaultConnectionCodeId", _data.C.ApVrsn_DefaultConnectionCodeId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("ApVrsn_DefaultConnectionCodeId_TextField", _data.C.ApVrsn_DefaultConnectionCodeId_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_IsMulticultural", _data.C.ApVrsn_IsMulticultural.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("ApVrsn_Notes", _data.C.ApVrsn_Notes.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_IsActiveRow", _data.C.ApVrsn_IsActiveRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("ApVrsn_IsDeleted", _data.C.ApVrsn_IsDeleted.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("ApVrsn_CreatedUserId", _data.C.ApVrsn_CreatedUserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("ApVrsn_CreatedDate", _data.C.ApVrsn_CreatedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("ApVrsn_UserId", _data.C.ApVrsn_UserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("UserName", _data.C.UserName.ToString(), TraceDataTypes.String);
			_traceItems.Add("ApVrsn_LastModifiedDate", _data.C.ApVrsn_LastModifiedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("ApVrsn_Stamp", _data.C.ApVrsn_Stamp.ToString(), TraceDataTypes.ByteArray);
			GetTraceItemsLongListCustom();
            return _traceItems;
        }

        public object[] GetTraceData()
        {
            object[] data = new object[32];
            data[0] = new object[] { "StateSwitch", StateSwitch.ToString() };
            data[1] = new object[] { "BrokenRuleManagerProperty", BrokenRuleManagerProperty.ToString() };
            data[2] = new object[] { "ApVrsn_Id", _data.C.ApVrsn_Id.ToString() };
            data[3] = new object[] { "ApVrsn_Ap_Id", _data.C.ApVrsn_Ap_Id.ToString() };
            data[4] = new object[] { "AttachmentCount", _data.C.AttachmentCount == null ? "<Null>" : _data.C.AttachmentCount.ToString() };
            data[5] = new object[] { "AttachmentFileNames", _data.C.AttachmentFileNames == null ? "<Null>" : _data.C.AttachmentFileNames.ToString() };
            data[6] = new object[] { "DiscussionCount", _data.C.DiscussionCount == null ? "<Null>" : _data.C.DiscussionCount.ToString() };
            data[7] = new object[] { "DiscussionTitles", _data.C.DiscussionTitles == null ? "<Null>" : _data.C.DiscussionTitles.ToString() };
            data[8] = new object[] { "VersionNo", _data.C.VersionNo == null ? "<Null>" : _data.C.VersionNo.ToString() };
            data[9] = new object[] { "ApVrsn_MajorVersion", _data.C.ApVrsn_MajorVersion.ToString() };
            data[10] = new object[] { "ApVrsn_MinorVersion", _data.C.ApVrsn_MinorVersion == null ? "<Null>" : _data.C.ApVrsn_MinorVersion.ToString() };
            data[11] = new object[] { "ApVrsn_VersionIteration", _data.C.ApVrsn_VersionIteration == null ? "<Null>" : _data.C.ApVrsn_VersionIteration.ToString() };
            data[12] = new object[] { "ApVrsn_Server", _data.C.ApVrsn_Server == null ? "<Null>" : _data.C.ApVrsn_Server.ToString() };
            data[13] = new object[] { "ApVrsn_DbName", _data.C.ApVrsn_DbName == null ? "<Null>" : _data.C.ApVrsn_DbName.ToString() };
            data[14] = new object[] { "ApVrsn_SolutionPath", _data.C.ApVrsn_SolutionPath == null ? "<Null>" : _data.C.ApVrsn_SolutionPath.ToString() };
            data[15] = new object[] { "ApVrsn_DefaultUIAssembly", _data.C.ApVrsn_DefaultUIAssembly == null ? "<Null>" : _data.C.ApVrsn_DefaultUIAssembly.ToString() };
            data[16] = new object[] { "ApVrsn_DefaultUIAssemblyPath", _data.C.ApVrsn_DefaultUIAssemblyPath == null ? "<Null>" : _data.C.ApVrsn_DefaultUIAssemblyPath.ToString() };
            data[17] = new object[] { "ApVrsn_DefaultWireTypePath", _data.C.ApVrsn_DefaultWireTypePath == null ? "<Null>" : _data.C.ApVrsn_DefaultWireTypePath.ToString() };
            data[18] = new object[] { "ApVrsn_WebServerURL", _data.C.ApVrsn_WebServerURL == null ? "<Null>" : _data.C.ApVrsn_WebServerURL.ToString() };
            data[19] = new object[] { "ApVrsn_WebsiteCodeFolderPath", _data.C.ApVrsn_WebsiteCodeFolderPath == null ? "<Null>" : _data.C.ApVrsn_WebsiteCodeFolderPath.ToString() };
            data[20] = new object[] { "ApVrsn_WebserviceCodeFolderPath", _data.C.ApVrsn_WebserviceCodeFolderPath == null ? "<Null>" : _data.C.ApVrsn_WebserviceCodeFolderPath.ToString() };
            data[21] = new object[] { "ApVrsn_StoredProcCodeFolder", _data.C.ApVrsn_StoredProcCodeFolder == null ? "<Null>" : _data.C.ApVrsn_StoredProcCodeFolder.ToString() };
            data[22] = new object[] { "ApVrsn_UseLegacyConnectionCode", _data.C.ApVrsn_UseLegacyConnectionCode.ToString() };
            data[23] = new object[] { "ApVrsn_DefaultConnectionCodeId", _data.C.ApVrsn_DefaultConnectionCodeId == null ? "<Null>" : _data.C.ApVrsn_DefaultConnectionCodeId.ToString() };
            data[24] = new object[] { "ApVrsn_DefaultConnectionCodeId_TextField", _data.C.ApVrsn_DefaultConnectionCodeId_TextField == null ? "<Null>" : _data.C.ApVrsn_DefaultConnectionCodeId_TextField.ToString() };
            data[25] = new object[] { "ApVrsn_IsMulticultural", _data.C.ApVrsn_IsMulticultural.ToString() };
            data[26] = new object[] { "ApVrsn_Notes", _data.C.ApVrsn_Notes == null ? "<Null>" : _data.C.ApVrsn_Notes.ToString() };
            data[27] = new object[] { "ApVrsn_IsActiveRow", _data.C.ApVrsn_IsActiveRow.ToString() };
            data[28] = new object[] { "ApVrsn_IsDeleted", _data.C.ApVrsn_IsDeleted.ToString() };
            data[29] = new object[] { "ApVrsn_CreatedUserId", _data.C.ApVrsn_CreatedUserId == null ? "<Null>" : _data.C.ApVrsn_CreatedUserId.ToString() };
            data[30] = new object[] { "ApVrsn_CreatedDate", _data.C.ApVrsn_CreatedDate == null ? "<Null>" : _data.C.ApVrsn_CreatedDate.ToString() };
            data[31] = new object[] { "ApVrsn_UserId", _data.C.ApVrsn_UserId == null ? "<Null>" : _data.C.ApVrsn_UserId.ToString() };
            data[32] = new object[] { "UserName", _data.C.UserName == null ? "<Null>" : _data.C.UserName.ToString() };
            data[33] = new object[] { "ApVrsn_LastModifiedDate", _data.C.ApVrsn_LastModifiedDate == null ? "<Null>" : _data.C.ApVrsn_LastModifiedDate.ToString() };
            return data;
        }


		#endregion ITraceItem Members


		#region INotifyPropertyChanged Members

        /// <summary>
        /// 	<para>
        ///         This method raises the <see cref="PropertyChanged">PropertyChanged</see> event
        ///         and is required by the INotifyPropertyChanged interface. It’s called in nearly
        ///         all public data field properties.
        ///     </para>
        /// 	<para>The <strong>INotifyPropertyChanged</strong> interface is used to notify
        ///     clients, typically binding clients, that a property value has changed. For more
        ///     information, search INotifyPropertyChanged on <strong>MSDN</strong>.</para>
        /// </summary>
        protected void Notify(string propName)
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }


		#endregion INotifyPropertyChanged Members


		#region INotifyDataErrorInfo Members

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        //public void RaiseErrorsChanged(string propertyName)
        //{
        //    if (ErrorsChanged != null)
        //        ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        //}

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            //throw new NotImplementedException();
            if (_brokenRuleManager.GetBrokenRulesForProperty(propertyName).Count == 0)
            {
                return "";
            }
            else
            {

                return _brokenRuleManager.GetBrokenRulesForProperty(propertyName)[0].ToString();
            }
        }

        public bool HasErrors
        {
            get { return _brokenRuleManager.IsEntityValid(); }
        }


		#endregion INotifyDataErrorInfo Members


        #region List Methods

        #region IEditableObject Members

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="CancelEdit">CancelEdit</see> and <see cref="EndEdit">EndEdit</see>.
        ///     </para>
        /// 	<para>Begins an edit on an object.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void BeginEdit()
        {
//            Guid? traceId = Guid.NewGuid();
//            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BeginEdit", IfxTraceCategory.Enter);
//            Console.WriteLine("BeginEdit");
//            //***  ToDo:
//            // Set the parent type below, then when properties are being edited, raise an event to sync fields in the props control.
//            _parentType = ParentEditObjectType.EntitiyListControl;
//
//            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BeginEdit", IfxTraceCategory.Leave);
        }

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="BeginEdit">BeginEdit</see> and <see cref="EndEdit">EndEdit</see>.
        ///     </para>
        /// 	<para>Discards changes since the last BeginEdit call.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void CancelEdit()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelEdit", IfxTraceCategory.Enter);
            UnDo();
            _parentType = ParentEditObjectType.None;

            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelEdit", IfxTraceCategory.Leave);
        }

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="BeginEdit">BeginEdit</see> and <see cref="CancelEdit">CancelEdit</see>.
        ///     </para>
        /// 	<para>Pushes changes since the last BeginEdit or IBindingList.AddNew call into the
        ///     underlying object.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void EndEdit()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", IfxTraceCategory.Enter);
//                if (_data.S.IsDirty() && _data.S.IsValid())
//                {
//                    // ToDo - This needs more testing for Silverlight
//                    Save(null, ParentEditObjectType.EntitiyListControl, UseConcurrencyCheck.UseDefaultSetting);
//
//                    //DataServiceInsertUpdateResponseClientSide result = Save(UseConcurrencyCheck.UseDefaultSetting);
//                    //if (result.Result != DataOperationResult.Success)
//                    //{
//                    //    UnDo();
//                    //    Debugger.Break();
//                    //}
//                }
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", IfxTraceCategory.Leave);
//            }
        }

        #endregion IEditableObject Members

        #endregion List Methods



        #region Children_WcTable


        WcTable_List _children_WcTable = new WcTable_List();


        public WcTable_List Children_WcTable
        {
            get { return _children_WcTable; }
            set { _children_WcTable = value; }
        }

        public void Get_Children_WcTable()
        {
            _proxyWcTableService.Begin_WcTable_GetListByFK(ApVrsn_Id);
        }
        

        void _proxyWcTableService_WcTable_GetListByFKCompleted(object sender, WcTable_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableService_WcTable_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array != null)
                {
                    Children_WcTable.ReplaceList(array);
                }
                else
                {
                    Children_WcTable.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableService_WcTable_GetListByFKCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableService_WcTable_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }


            #endregion Children_WcTable

            

        #region Children_WcTableGroup


        WcTableGroup_List _children_WcTableGroup = new WcTableGroup_List();


        public WcTableGroup_List Children_WcTableGroup
        {
            get { return _children_WcTableGroup; }
            set { _children_WcTableGroup = value; }
        }

        public void Get_Children_WcTableGroup()
        {
            _proxyWcTableGroupService.Begin_WcTableGroup_GetListByFK(ApVrsn_Id);
        }
        

        void _proxyWcTableGroupService_WcTableGroup_GetListByFKCompleted(object sender, WcTableGroup_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableGroupService_WcTableGroup_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array != null)
                {
                    Children_WcTableGroup.ReplaceList(array);
                }
                else
                {
                    Children_WcTableGroup.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableGroupService_WcTableGroup_GetListByFKCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableGroupService_WcTableGroup_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }


            #endregion Children_WcTableGroup

            

        #region Children_WcTableColumnGroup


        WcTableColumnGroup_List _children_WcTableColumnGroup = new WcTableColumnGroup_List();


        public WcTableColumnGroup_List Children_WcTableColumnGroup
        {
            get { return _children_WcTableColumnGroup; }
            set { _children_WcTableColumnGroup = value; }
        }

        public void Get_Children_WcTableColumnGroup()
        {
            _proxyWcTableColumnGroupService.Begin_WcTableColumnGroup_GetListByFK(ApVrsn_Id);
        }
        

        void _proxyWcTableColumnGroupService_WcTableColumnGroup_GetListByFKCompleted(object sender, WcTableColumnGroup_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableColumnGroupService_WcTableColumnGroup_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array != null)
                {
                    Children_WcTableColumnGroup.ReplaceList(array);
                }
                else
                {
                    Children_WcTableColumnGroup.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableColumnGroupService_WcTableColumnGroup_GetListByFKCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableColumnGroupService_WcTableColumnGroup_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }


            #endregion Children_WcTableColumnGroup

            

        #region Children_WcStoredProcGroup


        WcStoredProcGroup_List _children_WcStoredProcGroup = new WcStoredProcGroup_List();


        public WcStoredProcGroup_List Children_WcStoredProcGroup
        {
            get { return _children_WcStoredProcGroup; }
            set { _children_WcStoredProcGroup = value; }
        }

        public void Get_Children_WcStoredProcGroup()
        {
            _proxyWcStoredProcGroupService.Begin_WcStoredProcGroup_GetListByFK(ApVrsn_Id);
        }
        

        void _proxyWcStoredProcGroupService_WcStoredProcGroup_GetListByFKCompleted(object sender, WcStoredProcGroup_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcStoredProcGroupService_WcStoredProcGroup_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array != null)
                {
                    Children_WcStoredProcGroup.ReplaceList(array);
                }
                else
                {
                    Children_WcStoredProcGroup.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcStoredProcGroupService_WcStoredProcGroup_GetListByFKCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcStoredProcGroupService_WcStoredProcGroup_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }


            #endregion Children_WcStoredProcGroup

            

//        #region IDataErrorInfo Members
//
//        /// <summary>
//        /// 	<para>Part of the <strong>IDataErrorInfo</strong> Interface. Gets an error message
//        ///     indicating what is wrong with this object.</para>
//        /// 	<para>For more information, search <strong>IDataErrorInfo</strong> on
//        ///     <strong>MSDN</strong>.</para>
//        /// </summary>
//        public string Error
//        {
//            get { throw new NotImplementedException(); }
//        }
//
//        /// <summary>
//        /// 	<para>Part of the <strong>IDataErrorInfo</strong> Interface. Gets the error message
//        ///     for the property with the given name.</para>
//        /// 	<para>For more information, search <strong>IDataErrorInfo</strong> on
//        ///     <strong>MSDN</strong>.</para>
//        /// </summary>
//        string IDataErrorInfo.this[string columnName]
//        {
//            get
//            {
//                if (!_brokenRuleManager.IsPropertyValid(columnName))
//                {
//                    //string err = "line 1 First validation msg" + Environment.NewLine + "line 2 Second validation msg";
//                    //return err;
//                    return _brokenRuleManager.GetBrokenRulesForProperty(columnName)[0].ToString();
//                }
//                else
//                {
//                    return null;
//                }
//            }
//        }
//
//        #endregion



    }

}



