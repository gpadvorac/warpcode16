using System;
using System.Collections.Generic;
using TypeServices;
using System.ComponentModel;
using System.Windows;
using EntityWireTypeSL;
using Ifx.SL;
using vUICommon;
using Velocity.SL;

// Gen Timestamp:  9/1/2017 9:49:05 PM

namespace EntityBll.SL
{


    public partial class WcTable_Bll //: BusinessObjectBase, ITraceItem, IBusinessObject, IBusinessObjectV2, INotifyPropertyChanged , IEditableObject, IDataErrorInfo
    {




        #region Initialize Custom Variables



        #endregion Initialize Custom Variables


        #region General Methods


        void InitializeClass_Cust()
        {
            //            Guid? traceId = Guid.NewGuid();
            //            try
            //            {
            //                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", IfxTraceCategory.Enter);
            //      
            //            }
            //            catch (Exception ex)
            //            {
            //                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", ex);
            // throw IfxWrapperException.GetError(ex);
            //            }
            //            finally
            //            {
            //                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass_Cust", IfxTraceCategory.Leave);
            //            }
        }

        void CustomConstructor()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructor", IfxTraceCategory.Enter);

                WcTableProxy.ExecuteWcTable_InsertTable_ByConnectionKeyIdCompleted += _wcTableProxy_ExecuteWcTable_InsertTable_ByConnectionKeyIdCompleted;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructor", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructor", IfxTraceCategory.Leave);
            }
        }


        public void SetStandingFK(string parentType, Guid fk)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Enter);
                _parentEntityType = parentType;
                _StandingFK = fk;
                SetStandingFK();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", ex);
                // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Leave);
            }
        }

        public void SetStandingFK()
        {
            //            Guid? traceId = Guid.NewGuid();
            //            try
            //            {
            //                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Enter);
            //                //switch (_parentEntityType)
            //                //{
            //                //    case ParentType.Company:
            //                //        _data.C._b = _StandingFK;
            //                //        break;
            //                //    case ParentType.PersonContact:
            //                //        _data.C._c = _StandingFK;
            //                //        break;
            //                //}      
            //            }
            //            catch (Exception ex)
            //            {
            //                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", ex);
            // throw IfxWrapperException.GetError(ex);
            //            }
            //            finally
            //            {
            //                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStandingFK", IfxTraceCategory.Leave);
            //            }
        }

        // Alternate FKs
        public void SetOtherParentKeys(string parentType, Guid id)
        {
            //            Guid? traceId = Guid.NewGuid();
            //            try
            //            {
            //                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", IfxTraceCategory.Enter);
            //                //_parentEntityType = parentType;
            //                //switch (_parentEntityType)
            //                //{
            //                //    case "ucWellDt":
            //                //        _data.C.WlD_Id_noevents = id;
            //                //        break;
            //                //    case "ucContractDt":
            //                //        _data.C.CtD_Id_noevents = id;
            //                //        break;
            //                //}
            //            }
            //            catch (Exception ex)
            //            {
            //                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", ex);
            // throw IfxWrapperException.GetError(ex);
            //            }
            //            finally
            //            {
            //                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetOtherParentKeys", IfxTraceCategory.Leave);
            //            }
        }



        #endregion General Methods


        #region Data Access Methods


        #endregion Data Access Methods


        #region Method Extentions For Custom Code



        private void SetCustomDefaultBrokenRules()
        {
            //            Guid? traceId = Guid.NewGuid();
            //            try
            //            {
            //                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", IfxTraceCategory.Enter);
            //
            //         
            //            }
            //            catch (Exception ex)
            //            {
            //                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", ex);
            // throw IfxWrapperException.GetError(ex);
            //            }
            //            finally
            //            {
            //                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCustomDefaultBrokenRules", IfxTraceCategory.Leave);
            //            }
        }




        private void CustomPropertySetterCode(string propName)
        {
            //            Guid? traceId = Guid.NewGuid();
            //            try
            //            {
            //                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", IfxTraceCategory.Enter);
            //                switch(propName)
            //                {
            //                    case "xxxx":
            //
            //                        break;
            //
            //                }
            //            }
            //            catch (Exception ex)
            //            {
            //                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", ex);
            // throw IfxWrapperException.GetError(ex);
            //            }
            //            finally
            //            {
            //                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomPropertySetterCode", IfxTraceCategory.Leave);
            //            }
        }

        private void UnDoCustomCode()
        {
            //            Guid? traceId = Guid.NewGuid();
            //            try
            //            {
            //                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", IfxTraceCategory.Enter);
            //   
            //
            //            }
            //            catch (Exception ex)
            //            {
            //                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", ex);
            // throw IfxWrapperException.GetError(ex);
            //            }
            //            finally
            //            {
            //                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDoCustomCode", IfxTraceCategory.Leave);
            //            }
        }

        private void RefreshFieldsCustomCode()
        {
            //            Guid? traceId = Guid.NewGuid();
            //            try
            //            {
            //                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", IfxTraceCategory.Enter);
            //   
            //
            //            }
            //            catch (Exception ex)
            //            {
            //                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", ex);
            // throw IfxWrapperException.GetError(ex);
            //            }
            //            finally
            //            {
            //                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFieldsCustomCode", IfxTraceCategory.Leave);
            //            }
        }

        private void NewEntityRowCustomCode()
        {
            //            Guid? traceId = Guid.NewGuid();
            //            try
            //            {
            //                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", IfxTraceCategory.Enter);
            //                //SetStandingFK();  //Unrem this when we have FKs
            //
            //            }
            //            catch (Exception ex)
            //            {
            //                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", ex);
            // throw IfxWrapperException.GetError(ex);
            //            }
            //            finally
            //            {
            //                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRowCustomCode", IfxTraceCategory.Leave);
            //            }
        }

        private bool IsPropertyDirtyCustomCode(string propertyName)
        {
            ////  This is called from 'IsPropertyDirty' for calculated fields that are not represented in the wire objects.  They will always require custom code here.
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", IfxTraceCategory.Enter);
            //    switch (propertyName)
            //    {
            //        case "xxxx":
            //            //  return something
            //            break;
            //        default:
            //            throw new Exception(_as + "." + _cn + ".IsPropertyDirtyCustomCode:  Missing code and logic for " + propertyName + ".");
            //    }
            return false;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", ex);
            // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirtyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        public string GetPropertyValueByKeyCustomCode(string key)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", IfxTraceCategory.Enter);
            //    //switch (key)
            //    //{
            //    //    case "xxxx":
            //    //        return null;
            //    //        return xxx;
            //    //}
            return null;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", ex);
            // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKeyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        public string GetPropertyFormattedStringValueByKeyCustomCode(string key)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", IfxTraceCategory.Enter);
            //    //switch (key)
            //    //{
            //    //    case "xxxx":
            //    //        return null;
            //    //        return xxx;
            //    //}
            return null;
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", ex);
            // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKeyCustomCode", IfxTraceCategory.Leave);
            //}
        }

        #endregion Method Extentions For Custom Code


        #region Code Gen Methods For Modification to be copied to here





        public void Get_Children_WcTableColumnGroup()
        {
            _proxyWcTableColumnGroupService.Begin_GetWcTableColumnGroup_lstByParentId(null, Tb_Id);
        }


        public void Get_Children_WcTableChild()
        {
            if (IsImported==true)
            {
                _proxyWcTableChildService.Begin_WcTableChild_GetListByFK((Guid)Tb_Id);
            }
        }


        public void Get_Children_WcTableSortBy()
        {
            if (IsImported == true)
            {
                _proxyWcTableSortByService.Begin_WcTableSortBy_GetListByFK((Guid)Tb_Id);
            }
        }


        public void Get_Children_WcTableColumn()
        {
            if (IsImported == true)
            {
                _proxyWcTableColumnService.Begin_WcTableColumn_GetListByFK((Guid)Tb_Id);
            }
        }





        public string DataRowName
        {
            get
            {
                if (Tb_Name == null)
                {
                    return TableInDatabase;
                }
                else
                {
                    return Tb_Name;
                }
            }
        }

        #endregion Code Gen Methods For Modification to be copied to here


        #region Code Gen Properties For Modification to be copied to here


        #endregion Code Gen Properties For Modification to be copied to here


        #region Code Gen Validation Logic For Modification to be copied to here


        #endregion Code Gen Validation Logic For Modification to be copied to here


        #region Properties

        private WcTable_ConnectionInfo_Binding _connectionInfo = null;

        public WcTable_ConnectionInfo_Binding ConnectionInfo
        {
            get { return _connectionInfo; }
            set { _connectionInfo = value; }
        }

        #endregion Properties


        #region Validation Logic


        #endregion Validation Logic


        #region Custom Code

        public object GetComboItemsSourceFromDatabase(string columnKey)
        {
            return null;
        }

        // import table from here
        public void ImportSelectedTable(Guid connStringKeyId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSelectedTable", IfxTraceCategory.Enter);

                if (TableInDatabase == null)
                {
                    // Do something?
                    MessageBox.Show("Missing source table name.", "Data Error", MessageBoxButton.OK);
                    return;
                }
                
                _wcTableProxy.Begin_ExecuteWcTable_InsertTable_ByConnectionKeyId(connStringKeyId, TableInDatabase, (Guid)Credentials.UserId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSelectedTable", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSelectedTable", IfxTraceCategory.Leave);
            }
        }



        private void _wcTableProxy_ExecuteWcTable_InsertTable_ByConnectionKeyIdCompleted(object sender, ExecuteWcTable_InsertTable_ByConnectionKeyIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcTableProxy_GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyIdCompleted", IfxTraceCategory.Enter);
                object[] data = e.Result;
                Guid? tb_Id = data[0] as Guid?;
                Int32? success = data[1] as int?;

                if (success != 1)
                {
                    // The import failed.
                    // Do something.

                }
                else
                {
                    GetEntityRow((Guid)tb_Id);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcTableProxy_GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyIdCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcTableProxy_GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyIdCompleted", IfxTraceCategory.Leave);
            }
        }




        /// <summary>
        /// Loop thru a master list of table columns and add only those that belong to this table.
        /// </summary>
        public void Children_WcTableColumn_LoadFromMasterList(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Children_WcTableColumn_LoadFromMasterList", IfxTraceCategory.Enter);
                _children_WcTableColumn.Clear();
                if (data == null || data.Length == 0)
                {
                    return;
                }
                List<WcTableColumn_ValuesMngr> list = new List<WcTableColumn_ValuesMngr>();
                EntityState state = new EntityState(false, true, false);
                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                {
                    list.Add(new WcTableColumn_ValuesMngr((object[])data[i], state));
                }
                foreach (WcTableColumn_ValuesMngr obj in list)
                {
                    if (obj.C.TbC_Tb_Id == Tb_Id)
                    {
                        _children_WcTableColumn.Add(new WcTableColumn_Bll(obj, state));
                    }
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Children_WcTableColumn_LoadFromMasterList", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Children_WcTableColumn_LoadFromMasterList", IfxTraceCategory.Leave);
            }
        }





        List<WcCodeGen_TableColumn_Binding> _childTableColumns = new List<WcCodeGen_TableColumn_Binding>();

        public List<WcCodeGen_TableColumn_Binding> ChildTableColumns
        {
            get { return _childTableColumns; }
            set { _childTableColumns = value; }
        }

        public void LoadChildTableColumns(object[] list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildTableColumns", IfxTraceCategory.Enter);
                _childTableColumns.Clear();
                for (int i = 0; i < list.GetUpperBound(0) + 1; i++)
                {
                    _childTableColumns.Add(new WcCodeGen_TableColumn_Binding((object[])list[i]));
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildTableColumns", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildTableColumns", IfxTraceCategory.Leave);
            }
        }

        public void LoadChildTableColumnsFromMasterList(List<WcCodeGen_TableColumn_Binding> list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildTableColumnsFromMasterList", IfxTraceCategory.Enter);
                _childTableColumns.Clear();
                foreach (WcCodeGen_TableColumn_Binding obj in list)
                {
                    if (obj.TbC_Tb_Id == Tb_Id)
                    {
                        _childTableColumns.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildTableColumnsFromMasterList", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadChildTableColumnsFromMasterList", IfxTraceCategory.Leave);
            }
        }











        ///// <summary>
        ///// Loop thru a master list of table columns and add only those that belong to this table.
        ///// </summary>
        //public void Children_WcTableSortBy_LoadFromMasterList(object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Children_WcTableSortBy_LoadFromMasterList", IfxTraceCategory.Enter);
        //        _children_WcTableSortBy.Clear();
        //        if (data == null || data.Length == 0)
        //        {
        //            return;
        //        }
        //        List<WcTableSortBy_ValuesMngr> list = new List<WcTableSortBy_ValuesMngr>();
        //        EntityState state = new EntityState(false, true, false);
        //        for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
        //        {
        //            list.Add(new WcTableSortBy_ValuesMngr((object[])data[i], state));
        //        }
        //        foreach (WcTableSortBy_ValuesMngr obj in list)
        //        {
        //            if (obj.C.TbSb_Tb_Id == Tb_Id)
        //            {
        //                _children_WcTableSortBy.Add(new WcTableSortBy_Bll(obj, state));
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Children_WcTableSortBy_LoadFromMasterList", ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Children_WcTableSortBy_LoadFromMasterList", IfxTraceCategory.Leave);
        //    }
        //}


        public void LoadConnectionInfoFromMasterList(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadConnectionInfoFromMasterList", IfxTraceCategory.Enter);

                _connectionInfo = null;
                if (data == null || data.Length == 0)
                {
                    return;
                }
                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                {
                    Guid tb_Id = (Guid) ((object[]) (object[]) data[i])[0];
                    if (tb_Id == Tb_Id)
                    {
                        _connectionInfo = new WcTable_ConnectionInfo_Binding((object[]) data[i]);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadConnectionInfoFromMasterList", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadConnectionInfoFromMasterList", IfxTraceCategory.Leave);
            }
        }



        #endregion Custom Code


        #region ITraceItem Members

        void GetTraceItemsShortListCustom()
        {
            //  Add custom trace items here.
            //_traceItems.Add("xxxxxxxxx", xxxxxxxxx, TraceDataTypes.String);
            //_traceItems.Add("SomeChildObject", new SomeChildObject(), TraceDataTypes.ITraceItem);
        }

        void GetTraceItemsLongListCustom()
        {
            //  Add custom trace items here.
            //_traceItems.Add("xxxxxxxxx", xxxxxxxxx.ToString, TraceDataTypes.String);
            //_traceItems.Add("SomeChildObject", new SomeChildObject(), TraceDataTypes.ITraceItem);
        }

        #endregion ITraceItem Members
    }

}


