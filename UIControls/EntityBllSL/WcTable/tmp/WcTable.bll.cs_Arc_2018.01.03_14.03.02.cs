using System;
using System.Collections.Generic;
using TypeServices;
using System.ComponentModel;
using System.Diagnostics;
using EntityWireTypeSL;
using Ifx.SL;
using Velocity.SL;
using ProxyWrapper;
using vComboDataTypes;

// Gen Timestamp:  1/2/2018 8:01:05 PM

namespace EntityBll.SL
{


    public partial class WcTable_Bll : BusinessObjectBase, ITraceItem, IBusinessObject, IBusinessObjectV2, INotifyPropertyChanged , IEditableObject
    {


        #region Initialize Variables

        /// <summary>
        /// This is a private instance of TraceItemList which is normaly used in every entity
        /// business type and is used to provide rich information abou the current state and values
        /// in this business object when included in a trace call. To see exactly what data will be
        /// provided in a trace, look at the GetTraceItemsShortList method in the WcTable_Bll code
        /// file.
        /// </summary>
        /// <seealso cref="!:file://D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Optimized_Deep_Tracing_of_Business_Object.html" cat="My Test Category">My Test Caption</seealso>
        TraceItemList _traceItems;
        /// <seealso cref="EntityWireType.WcTable_Values">WcTable_Values Class</seealso>
        /// <summary>
        ///     This is an instance of the wire type (<see cref="EntityWireType.WcTable_Values">WcTable_Values</see>) that’s wrapped by the Values
        ///     Manager (<see cref="EntityWireType.WcTable_ValuesMngr">WcTable_ValuesMngr</see>) and
        ///     used to Plug-n-Play into this business object.
        /// </summary>
        private WcTable_ValuesMngr _data = null;

        /// <summary>See <see cref="OnCrudFailed">OnCrudFailed</see> for information.</summary>
        public event CrudFailedEventHandler CrudFailed;

        public event AsyncSaveCompleteEventHandler AsyncSaveComplete;

        public event AsyncSaveWithResponseCompleteEventHandler AsyncSaveWithResponseComplete;

        public event EntityRowReceivedEventHandler EntityRowReceived;


        /// <summary>
        ///     See <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> for
        ///     information.
        /// </summary>
        public event CurrentEntityStateEventHandler CurrentEntityStateChanged;
        /// <summary>See <see cref="OnBrokenRuleChanged">OnBrokenRuleChanged</see> for information.</summary>
        public event BrokenRuleEventHandler BrokenRuleChanged;
        /// <summary>
        /// 	<para>
        ///         Raised by the This event is currently <see cref="OnPropertyValueChanged">OnPropertyValueChanged</see> method, its currently not
        ///         being used and may be obsolete.
        ///     </para>
        /// </summary>
        public event PropertyValueChangedEventHandler PropertyValueChanged;
        /// <summary>This event is currently not being used and may be obsolete.</summary>
        public event BusinessObjectUpdatedEventHandler BusinessObjectUpdated;
        /// <summary>
        /// 	<para>
        ///         Required by the INotifyPropertyChanged interface, this event is raise by the
        ///         <see cref="Notify">Notify</see> method (also part of the INotifyPropertyChanged
        ///         interface. The Notify method is called in nearly all public data field
        ///         properties. The INotifyPropertyChanged interface is implemented to make
        ///         WcTable_Bll more extendable.
        ///     </para>
        /// 	<para>The <strong>INotifyPropertyChanged</strong> interface is used to notify
        ///     clients, typically binding clients, that a property value has changed. For more
        ///     information, search INotifyPropertyChanged on <strong>MSDN</strong>.</para>
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 	<para>
        ///         Private field for the <see cref="ActiveRestrictedStringProperty">ActiveRestrictedStringProperty</see> property.
        ///         See it’s documentation for details.
        ///     </para>
        /// 	<para></para>
        /// </summary>
        private string _activeRestrictedStringProperty;
        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcTable_Bll";
        /// <summary>
        /// 	<para>
        ///         Used in the validation section of this class in the WcTable.bll.vld.cs code file,
        ///         this event notifies event handlers that a data field’s valid state has changed
        ///         (valid or not valid).<br/>
        ///         For more information about this event, see <see cref="TypeServices.ControlValidStateChangedEventHandler">CurrentEntityStateEventHandler</see>.<br/>
        ///         For more information on how it’s used, see these methods:
        ///     </para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="NewEntityRow">NewEntityRow</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="GetEntityRow">GetEntityRow</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="Save">Save</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableProps.RaiseCurrentEntityStateChanged">ucWcTableProps.RaiseCurrentEntityStateChanged()</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableProps.RaiseCurrentEntityStateChanged">ucWcTableProps.RaiseCurrentEntityStateChanged(EntityStateSwitch
        ///             state)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public event ControlValidStateChangedEventHandler ControlValidStateChanged;
        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableProps.OnRestrictedTextLengthChanged">ucWcTableProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableProps.SetRestrictedStringLengthText">ucWcTableProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public event RestrictedTextLengthChangedEventHandler RestrictedTextLengthChanged;

        UserSecurityContext _context = null;

        private object _parentEditObject = null;
        private ParentEditObjectType _parentType = ParentEditObjectType.None;

        private WcTableService_ProxyWrapper _wcTableProxy = null;



        Guid _StandingFK = Guid.Empty;
        string _parentEntityType = "";

        private WcTableColumnService_ProxyWrapper _proxyWcTableColumnService;
            
        private WcTableChildService_ProxyWrapper _proxyWcTableChildService;
            
        private WcTableSortByService_ProxyWrapper _proxyWcTableSortByService;
            
        private WcTableColumnGroupService_ProxyWrapper _proxyWcTableColumnGroupService;
            
        #endregion Initialize Variables

            
        #region Initialize Class

        public WcTable_Bll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Bll()", IfxTraceCategory.Enter);
                _context = UserSecurityContext.GetCurrent();
                InitializeClass();
//                WcTableService_ProxyWrapper _wcTableProxy = new  WcTableService_ProxyWrapper();
//                _wcTableProxy.WcTable_SaveCompleted += new EventHandler<WcTable_SaveCompletedEventArgs>(SaveCompleted);
                _proxyWcTableColumnService = new WcTableColumnService_ProxyWrapper();
                _proxyWcTableColumnService.WcTableColumn_GetListByFKCompleted += _proxyWcTableColumnService_WcTableColumn_GetListByFKCompleted;
                _children_WcTableColumn.Add(new WcTableColumn_Bll());
            
                _proxyWcTableChildService = new WcTableChildService_ProxyWrapper();
                _proxyWcTableChildService.WcTableChild_GetListByFKCompleted += _proxyWcTableChildService_WcTableChild_GetListByFKCompleted;
                _children_WcTableChild.Add(new WcTableChild_Bll());
            
                _proxyWcTableSortByService = new WcTableSortByService_ProxyWrapper();
                _proxyWcTableSortByService.WcTableSortBy_GetListByFKCompleted += _proxyWcTableSortByService_WcTableSortBy_GetListByFKCompleted;
                _children_WcTableSortBy.Add(new WcTableSortBy_Bll());
            
                _proxyWcTableColumnGroupService = new WcTableColumnGroupService_ProxyWrapper();
                _proxyWcTableColumnGroupService.WcTableColumnGroup_GetListByFKCompleted += _proxyWcTableColumnGroupService_WcTableColumnGroup_GetListByFKCompleted;
                _children_WcTableColumnGroup.Add(new WcTableColumnGroup_Bll());
            
                SetNewEntityRow();

                CustomConstructor();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Bll()", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Bll()", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>
        ///         Passes in the <see cref="EntityWireType.WcTable_ValuesMngr">WcTable_ValuesMngr</see> type and <see cref="TypeServices.EntityState">EntityState</see>. The WcTable_ValuesMngr type is the
        ///         data object used in a ‘Plug-n-Play’ fashion for loading data into WcTable_Bll
        ///         fast and efficiently. EntityState sets WcTable_Bll’s state.
        ///     </para>
        /// 	<para>Used by the following methods call this constructor:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="FromWire">FromWire</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcTable_List.Fill">Fill</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcTable_List.ReplaceList">ReplaceList(WcTable_ValuesMngr[]
        ///             list)</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcTable_List.ReplaceList">ReplaceList(IEntity_ValuesMngr[]
        ///             list)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public WcTable_Bll(WcTable_ValuesMngr valueObject, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Bll(WcTable_ValuesMngr valueObject, EntityState state)", IfxTraceCategory.Enter);
                _context = UserSecurityContext.GetCurrent(); 
                _data = valueObject;
                InitializeClass(state);
               // _data.SetClassStateAfterFetch(state);
//                 WcTableService_ProxyWrapper _wcTableProxy = new  WcTableService_ProxyWrapper();
//                _wcTableProxy.WcTable_SaveCompleted += new EventHandler<WcTable_SaveCompletedEventArgs>(SaveCompleted);
                _proxyWcTableColumnService = new WcTableColumnService_ProxyWrapper();
                _proxyWcTableColumnService.WcTableColumn_GetListByFKCompleted += _proxyWcTableColumnService_WcTableColumn_GetListByFKCompleted;
                _children_WcTableColumn.Add(new WcTableColumn_Bll());
            
                _proxyWcTableChildService = new WcTableChildService_ProxyWrapper();
                _proxyWcTableChildService.WcTableChild_GetListByFKCompleted += _proxyWcTableChildService_WcTableChild_GetListByFKCompleted;
                _children_WcTableChild.Add(new WcTableChild_Bll());
            
                _proxyWcTableSortByService = new WcTableSortByService_ProxyWrapper();
                _proxyWcTableSortByService.WcTableSortBy_GetListByFKCompleted += _proxyWcTableSortByService_WcTableSortBy_GetListByFKCompleted;
                _children_WcTableSortBy.Add(new WcTableSortBy_Bll());
            
                _proxyWcTableColumnGroupService = new WcTableColumnGroupService_ProxyWrapper();
                _proxyWcTableColumnGroupService.WcTableColumnGroup_GetListByFKCompleted += _proxyWcTableColumnGroupService_WcTableColumnGroup_GetListByFKCompleted;
                _children_WcTableColumnGroup.Add(new WcTableColumnGroup_Bll());
            
                CustomConstructor();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Bll(WcTable_ValuesMngr valueObject, EntityState state)", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Bll(WcTable_ValuesMngr valueObject, EntityState state)", IfxTraceCategory.Leave);
            }
        }


        private void InitializeClass()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Enter);
                InitializeClass(new EntityState(true, true, false));
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Leave);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Leave);
            }
        }



        /// <summary>Called from the constructor, events are wired here.</summary>
        private void InitializeClass(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass(EntityState state)", IfxTraceCategory.Enter);
                if (_data == null)
                {
                    _data = GetDefault();
                    //**  This is a hack.  We should set the state elsewhere, however, when we get this from accross the wire,
                    //      we loose the state and therefore are forced to always call SetClassStateAfterFetch.
                   // _data.SetClassStateAfterFetch(state);
                }
                _data.SetClassStateAfterFetch(state);
                BrokenRuleManagerProperty = new BrokenRuleManager(this);
                _brokenRuleManager.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                //TODO: need to code it for a dynamic creation for an insert
                //TODO: need to move save, assignwithid, update, delete, deactivate, persist (generic, check the state) into this class
                //TODO: need to move the various lists of apps to get into an applicationprovider class for the client side that thunks to the proxywrapper
                //TODO: need to move field level validation into the setters and call the object validation on any persist operation
                //TODO:need to implement the two eh and tracing interfaces on the object
                InitializeClass_Cust();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityState", IfxTraceCategory.Leave);
            }
        }

        private  WcTableService_ProxyWrapper WcTableProxy
        {
            get
            {
                if (_wcTableProxy == null)
                {
                    _wcTableProxy = new  WcTableService_ProxyWrapper();
                    _wcTableProxy.WcTable_SaveCompleted += new EventHandler<WcTable_SaveCompletedEventArgs>(SaveCompleted);
                    _wcTableProxy.WcTable_GetByIdCompleted += new EventHandler<WcTable_GetByIdCompletedEventArgs>(GetEntityRowResponse);

                }

                return _wcTableProxy;
            }
        }

        #endregion  Initialize Class


		#region CRUD Methods



        /// <returns><see cref="EntityWireType.WcTable_ValuesMngr">WcTable_ValuesMngr</see></returns>
        /// <summary>
        ///     When this business type is set to a new state, a new data type also known as a wire
        ///     type (<see cref="EntityWireType.WcTable_Values">WcTable_Values</see>) is created, new
        ///     Id value is created, default values are set and appropriate state setting are made.
        ///     WcTable_Values is wrapped in the <see cref="EntityWireType.WcTable_ValuesMngr">WcTable_ValuesMngr</see> type and returned.
        /// </summary>
        private WcTable_ValuesMngr GetDefault()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                Guid Id = GetNewID();
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Enter);
                return new WcTable_ValuesMngr(new object[] 
                    {
                        Id,    //  Tb_Id
						null,		//  Tb_ApVrsn_Id
						null,		//  Tb_auditTable_Id
						null,		//  AttachmentCount
						null,		//  AttachmentFileNames
						null,		//  DiscussionCount
						null,		//  DiscussionTitles
						null,		//  Tb_Name
						null,		//  Tb_EntityRootName
						null,		//  Tb_VariableName
						null,		//  Tb_ScreenCaption
						null,		//  Tb_Description
						null,		//  Tb_DevelopmentNote
						null,		//  Tb_UIAssemblyName
						null,		//  Tb_UINamespace
						null,		//  Tb_UIAssemblyPath
						null,		//  Tb_ProxyAssemblyName
						null,		//  Tb_ProxyAssemblyPath
						null,		//  Tb_WebServiceName
						null,		//  Tb_WebServiceFolder
						null,		//  Tb_DataServiceAssemblyName
						null,		//  Tb_DataServicePath
						null,		//  Tb_WireTypeAssemblyName
						null,		//  Tb_WireTypePath
						false,		//  Tb_UseTilesInPropsScreen
						false,		//  Tb_UseGridColumnGroups
						false,		//  Tb_UseGridDataSourceCombo
						null,		//  Tb_ApCnSK_Id
						null,		//  Tb_ApCnSK_Id_TextField
						true,		//  Tb_UseLegacyConnectionCode
						false,		//  Tb_PkIsIdentity
						false,		//  Tb_IsVirtual
						null,		//  Tb_IsScreenPlaceHolder
						false,		//  Tb_IsNotEntity
						false,		//  Tb_IsMany2Many
						true,		//  Tb_UseLastModifiedByUserNameInSproc
						true,		//  Tb_UseUserTimeStamp
						false,		//  Tb_UseForAudit
						null,		//  Tb_IsAllowDelete
						true,		//  Tb_CnfgGdMnu_MenuRow_IsVisible
						true,		//  Tb_CnfgGdMnu_GridTools_IsVisible
						true,		//  Tb_CnfgGdMnu_SplitScreen_IsVisible
						true,		//  Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
						232,		//  Tb_CnfgGdMnu_NavColumnWidth
						false,		//  Tb_CnfgGdMnu_IsReadOnly
						true,		//  Tb_CnfgGdMnu_IsAllowNewRow
						true,		//  Tb_CnfgGdMnu_ExcelExport_IsVisible
						true,		//  Tb_CnfgGdMnu_ColumnChooser_IsVisible
						true,		//  Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
						true,		//  Tb_CnfgGdMnu_RefreshGrid_IsVisible
						true,		//  Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
						false,		//  Tb_IsInputComplete
						true,		//  Tb_IsCodeGen
						false,		//  Tb_IsReadyCodeGen
						false,		//  Tb_IsCodeGenComplete
						false,		//  Tb_IsTagForCodeGen
						false,		//  Tb_IsTagForOther
						null,		//  Tb_TableGroups
						true,		//  Tb_IsActiveRow
						false,		//  Tb_IsDeleted
						null,		//  Tb_CreatedUserId
						null,		//  Tb_CreatedDate
						null,		//  Tb_UserId
						null,		//  UserName
						null,		//  Tb_LastModifiedDate
						null,		//  Tb_Stamp
                    }, new EntityState(true, true, false)
                );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// Called by any parent object that wants to clear out the existing data and state,
        /// and replace all with new state and data object.
        /// </summary>
        public void NewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Enter);
                _data = GetDefault();
                NewEntityRowCustomCode();
                SetDefaultBrokenRules();
                //RaiseEventCurrentEntityStateChanged(EntityStateSwitch.NewInvalidNotDirty);
                RaiseEventCurrentEntityStateChanged(_data.S.Switch);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// This is called from the parameterless constructor.  It's assumed that a data object will not be passed (which would have
        /// cause the EntityState to be configured properly) and therefore this new entity must be properly configured.
        /// </summary>
        public void SetNewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", IfxTraceCategory.Enter);
                NewEntityRowCustomCode();
                SetDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", IfxTraceCategory.Leave);
            }
        }


        public void GetEntityRow(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", new ValuePair[] {new ValuePair("Id", Id) }, IfxTraceCategory.Enter);

                WcTableProxy.Begin_WcTable_GetById(Id);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", IfxTraceCategory.Leave);
            }
        }

        public void GetEntityRow(long Id)
        {
            throw new NotImplementedException("WcTable_Bll GetEntityRow(long Id) Not Implemented");
        }

        public void GetEntityRow(int Id)
        {
            throw new NotImplementedException("WcTable_Bll GetEntityRow(int Id) Not Implemented");
        }

        public void GetEntityRow(short Id)
        {
            throw new NotImplementedException("WcTable_Bll GetEntityRow(short Id) Not Implemented");
        }

        public void GetEntityRow(byte Id)
        {
            throw new NotImplementedException("WcTable_Bll GetEntityRow(byte Id) Not Implemented");
        }

        public void GetEntityRow(object Id)
        {
            throw new NotImplementedException("WcTable_Bll GetEntityRow(object Id) Not Implemented");
        }

        private void GetEntityRowResponse(object sender, WcTable_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                if (data == null)
                {
                    // Alert UI
                    return;
                }
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array == null)
                {
                    // Alert UI
                    return;
                }
                _brokenRuleManager.ClearAllBrokenRules();
                _data.ReplaceData((object[])array[0], new EntityState(false, true, false));
                RaiseEventEntityRowReceived();
                RefreshFields();
                // Do we still need this next line?
                //RaiseEventCurrentEntityStateChanged(_data.S.Switch);

                //  ToDo:  Later when we have a Foreign Key, do something like this:  DataProps.TbC_Tb_ID = DataProps.standing_FK;
                //              How do we handle the standing FK in the new framework?
 
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", IfxTraceCategory.Leave);
            }
        }



        /// <summary>
        ///     Called by <see cref="GetDefault">GetDefault</see> and creates a new WcTable Id value
        ///     for a new data object (<see cref="EntityWireType.WcTable_ValuesMngr">WcTable_ValuesMngr</see>).
        /// </summary>
        private Guid GetNewID()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", IfxTraceCategory.Enter);
                return Guid.NewGuid();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Un-does the current data edits and restores everything back to the previous saved
        ///     or new <see cref="TypeServices.EntityState">state</see>. This will include calling
        ///     <see cref="TypeServices.BrokenRuleManager.ClearAllBrokenRules">_brokenRuleManager.ClearAllBrokenRules</see>
        ///     to clear any BrokenRules or calling <see cref="SetDefaultBrokenRules">SetDefaultBrokenRules</see> to set any default BrokenRules;
        ///     and raising the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event is raised to
        ///     notify the parent objects that the state has changed.
        /// </summary>
        public void UnDo()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Enter);
                EntityStateSwitch es = _data.S.Switch;
                _data.SetCurrentToOriginal();
                if (_data.S.IsNew() == true)
                {
                    SetDefaultBrokenRules();
                }
                else
                {
                    _brokenRuleManager.ClearAllBrokenRules();
                }
                CheckEntityState(es);

                UnDoCustomCode();
                RefreshFields();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Call the Notify method for all visible fields.  This allows the fields/properties to be refreshed in grids and other controls that are consuming them via databinding.
        /// </summary>
        public void RefreshFields()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", IfxTraceCategory.Enter);
				Notify("AttachmentCount");
				Notify("DiscussionCount");
				Notify("Tb_Name");
				Notify("Tb_EntityRootName");
				Notify("Tb_VariableName");
				Notify("Tb_ScreenCaption");
				Notify("Tb_Description");
				Notify("Tb_DevelopmentNote");
				Notify("Tb_UIAssemblyName");
				Notify("Tb_UINamespace");
				Notify("Tb_UIAssemblyPath");
				Notify("Tb_ProxyAssemblyName");
				Notify("Tb_ProxyAssemblyPath");
				Notify("Tb_WebServiceName");
				Notify("Tb_WebServiceFolder");
				Notify("Tb_DataServiceAssemblyName");
				Notify("Tb_DataServicePath");
				Notify("Tb_WireTypeAssemblyName");
				Notify("Tb_WireTypePath");
				Notify("Tb_UseTilesInPropsScreen");
				Notify("Tb_UseGridColumnGroups");
				Notify("Tb_UseGridDataSourceCombo");
				Notify("Tb_ApCnSK_Id");
				Notify("Tb_ApCnSK_Id_TextField");
				Notify("Tb_UseLegacyConnectionCode");
				Notify("Tb_PkIsIdentity");
				Notify("Tb_IsVirtual");
				Notify("Tb_IsScreenPlaceHolder");
				Notify("Tb_IsNotEntity");
				Notify("Tb_IsMany2Many");
				Notify("Tb_UseLastModifiedByUserNameInSproc");
				Notify("Tb_UseUserTimeStamp");
				Notify("Tb_UseForAudit");
				Notify("Tb_IsAllowDelete");
				Notify("Tb_CnfgGdMnu_MenuRow_IsVisible");
				Notify("Tb_CnfgGdMnu_GridTools_IsVisible");
				Notify("Tb_CnfgGdMnu_SplitScreen_IsVisible");
				Notify("Tb_CnfgGdMnu_SplitScreen_IsSplit_Default");
				Notify("Tb_CnfgGdMnu_NavColumnWidth");
				Notify("Tb_CnfgGdMnu_IsReadOnly");
				Notify("Tb_CnfgGdMnu_IsAllowNewRow");
				Notify("Tb_CnfgGdMnu_ExcelExport_IsVisible");
				Notify("Tb_CnfgGdMnu_ColumnChooser_IsVisible");
				Notify("Tb_CnfgGdMnu_ShowHideColBtn_IsVisible");
				Notify("Tb_CnfgGdMnu_RefreshGrid_IsVisible");
				Notify("Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible");
				Notify("Tb_IsInputComplete");
				Notify("Tb_IsCodeGen");
				Notify("Tb_IsReadyCodeGen");
				Notify("Tb_IsCodeGenComplete");
				Notify("Tb_IsTagForCodeGen");
				Notify("Tb_IsTagForOther");
				Notify("Tb_TableGroups");
				Notify("Tb_IsActiveRow");
				Notify("UserName");
				Notify("Tb_LastModifiedDate");

                RefreshFieldsCustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Called from <see cref="wnConcurrencyManager">wnConcurrencyManager</see> and therefore does not need
        ///     to pass in the Parent and Parent Type parameters.
        ///     Saves the current data to the data store, configures the appropriate state and
        ///     calls <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see> to
        ///     notify the parent object of the state change.
        /// </summary>
        public void Save(UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                _data.C.Tb_UserId_noevents = Credentials.UserId;
                WcTableProxy.Begin_WcTable_Save(_data.C.GetValues(), (int)check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Saves the current data to the data store, configures the appropriate state and
        ///     calls <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see> to
        ///     notify the parent object of the state change.
        /// </summary>
        public void Save(object parentEditObject, ParentEditObjectType parentType,  UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                _parentEditObject = parentEditObject;
                _parentType = parentType;
                _data.C.Tb_UserId_noevents = Credentials.UserId;
                WcTableProxy.Begin_WcTable_Save(_data.C.GetValues(), (int)check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        void SaveCompleted(object sender, WcTable_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", IfxTraceCategory.Enter);
                DataServiceInsertUpdateResponse newData = null;
                if (e.Result == null)
                {
                    // return some message to the calling object
                    newData = new DataServiceInsertUpdateResponse();
                    newData.Result = DataOperationResult.HandledException;
                    //RaiseEventAsyncSaveComplete(newData.Result, null);
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);
                }
                else
                {
                    newData = new DataServiceInsertUpdateResponse();
                    byte[] data = e.Result;
                    object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                    newData.SetFromObjectArray(array);
                }

                // Get the Result status
                if (newData.Result == DataOperationResult.Success)
                {
                    if (newData.ReturnCurrentRowOnInsertUpdate == true)
                    {
                        // If we returned the current row from the data store, then syncronize the wire object with it.
                        _data.C.ReplaceDataFromObjectArray(newData.CurrentValues);
                        RefreshFields();
                    }
                    else
                    {
                        // Otherwise:
                        //  Set the PK value from the data store incase we did an insert.
                        //  In theory the guid was already created by this busness object, but we're setting it here
                        //  just in case some code changed somewhere and it was created on the server.
                        _data.C._a = newData.guidPrimaryKey;
                        //  Set the new TimeStamp value
                        _data.C.Tb_Stamp_noevents = newData.CurrentTimestamp;
                    }
                    _data.S.SetNotNew();
                    _data.S.SetValid();
                    _data.S.SetNotDirty();
                    _data.SetOriginalToCurrent();
                  
                    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);

                    //if (_parentType == ParentEditObjectType.EntitiyPropertiesControl)
                    //{
                    //    // A EntitiyPropertiesControl called the same method, therefore raise the following event
                    //    // allowing all parent conrol to reconfigure thier entity state.
                    //    // If this was called from editing a grid control, then this is not nessesacy since we 
                    //    // dont change the state of surrounding controls.
                    //    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                    //
                    //    // Rasie this event to the props control can call it's SetState method to refresh the UI
                    //    // The Entity List control will be updated via the PropertyChanged event which gets called from the RefreshFields call above.
                    //    if (newData.ReturnCurrentRowOnInsertUpdate == true)
                    //    {
                    //        RaiseEventAsyncSaveWithResponseComplete(newData, null);
                    //    }
                    //
                    //}
                    //else
                    //{
                    //    RaiseEventAsyncSaveComplete(newData.Result, null);
                    //}
                }
                else
                {
                    // Add the current date from the database to the wire obect's concurrent property
                    //if (newData.Result == DataOperationResult.ConcurrencyFailure)
                    //{
                    //    _data.X.ReplaceDataFromObjectArray(newData.CurrentValues);
                    //}
                    // ToDo:  we need to develope logic and code to pass user friendly error message 
                    // instead of null when the save failed.

                    // We had some type of failure, so raise this event regardless of the parent type 
                    // so the UI can notify the user.
                    //RaiseEventAsyncSaveComplete(newData.Result, null);
                    //*** use this instead
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _parentEditObject = null;
                _parentType = ParentEditObjectType.None;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", IfxTraceCategory.Leave);
            }
        }







        /// <summary>
        /// Calls a Delete method on the server to delete this entity from the data
        /// store.
        /// </summary>
        /// <returns>1 = Success, 2 = Failed</returns>
        public int  Entity_Delete()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", IfxTraceCategory.Enter);
//                // add credentials
//                WcTable_ValuesMngr retObject = ProxyWrapper.EntityProxyWrapper.WcTable_Delete(_data);
//                return 1;
//                //  Needs further design.  What do we do when the delete fails or there is an concurrency issue?
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Just a stub and not being used. Consider renaming this to ActivatOrDeactivate so
        /// it can be used either way.
        /// </summary>
        /// <returns>Returns 0 because this method is not currently functional.</returns>
        public int Entity_Deactivate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", IfxTraceCategory.Enter);
                //  This is currently setup to return a wire object, however, we might only need to return an int flag
                //  Needs further design
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", IfxTraceCategory.Leave);
            }
        }

        /// <returns>Returns 0 because this method is not currently functional.</returns>
        /// <summary>Just a stub and not being used.</summary>
        public int Entity_Remove()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", IfxTraceCategory.Enter);
                //  This is currently setup to return a wire object, however, we might only need to return an int flag
                //  Needs further design
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Copies the values from another data object (<see cref="EntityWireType.WcTable_ValuesMngr">WcTable_ValuesMngr</see>) to the one plugged into
        ///     this business object. This is called from the Save method because it returns a
        ///     fresh copy of this entity’s data from the data store incase modifications were made
        ///     in the process of saving.
        /// </summary>
        /// <param name="thisData">
        /// The data object (WcTable_ValuesMngr) plugged into this business object which data
        /// will be copied to.
        /// </param>
        /// <param name="newData">The data object (WcTable_ValuesMngr) which data will be copied from.</param>
        private void SyncValueObjectCurrentProperties(WcTable_ValuesMngr thisData, WcTable_ValuesMngr newData)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", IfxTraceCategory.Enter);
				thisData.C.Tb_Id_noevents = newData.C.Tb_Id_noevents;
				thisData.C.Tb_ApVrsn_Id_noevents = newData.C.Tb_ApVrsn_Id_noevents;
				thisData.C.Tb_auditTable_Id_noevents = newData.C.Tb_auditTable_Id_noevents;
				thisData.C.AttachmentCount_noevents = newData.C.AttachmentCount_noevents;
				thisData.C.AttachmentFileNames_noevents = newData.C.AttachmentFileNames_noevents;
				thisData.C.DiscussionCount_noevents = newData.C.DiscussionCount_noevents;
				thisData.C.DiscussionTitles_noevents = newData.C.DiscussionTitles_noevents;
				thisData.C.Tb_Name_noevents = newData.C.Tb_Name_noevents;
				thisData.C.Tb_EntityRootName_noevents = newData.C.Tb_EntityRootName_noevents;
				thisData.C.Tb_VariableName_noevents = newData.C.Tb_VariableName_noevents;
				thisData.C.Tb_ScreenCaption_noevents = newData.C.Tb_ScreenCaption_noevents;
				thisData.C.Tb_Description_noevents = newData.C.Tb_Description_noevents;
				thisData.C.Tb_DevelopmentNote_noevents = newData.C.Tb_DevelopmentNote_noevents;
				thisData.C.Tb_UIAssemblyName_noevents = newData.C.Tb_UIAssemblyName_noevents;
				thisData.C.Tb_UINamespace_noevents = newData.C.Tb_UINamespace_noevents;
				thisData.C.Tb_UIAssemblyPath_noevents = newData.C.Tb_UIAssemblyPath_noevents;
				thisData.C.Tb_ProxyAssemblyName_noevents = newData.C.Tb_ProxyAssemblyName_noevents;
				thisData.C.Tb_ProxyAssemblyPath_noevents = newData.C.Tb_ProxyAssemblyPath_noevents;
				thisData.C.Tb_WebServiceName_noevents = newData.C.Tb_WebServiceName_noevents;
				thisData.C.Tb_WebServiceFolder_noevents = newData.C.Tb_WebServiceFolder_noevents;
				thisData.C.Tb_DataServiceAssemblyName_noevents = newData.C.Tb_DataServiceAssemblyName_noevents;
				thisData.C.Tb_DataServicePath_noevents = newData.C.Tb_DataServicePath_noevents;
				thisData.C.Tb_WireTypeAssemblyName_noevents = newData.C.Tb_WireTypeAssemblyName_noevents;
				thisData.C.Tb_WireTypePath_noevents = newData.C.Tb_WireTypePath_noevents;
				thisData.C.Tb_UseTilesInPropsScreen_noevents = newData.C.Tb_UseTilesInPropsScreen_noevents;
				thisData.C.Tb_UseGridColumnGroups_noevents = newData.C.Tb_UseGridColumnGroups_noevents;
				thisData.C.Tb_UseGridDataSourceCombo_noevents = newData.C.Tb_UseGridDataSourceCombo_noevents;
				thisData.C.Tb_ApCnSK_Id_noevents = newData.C.Tb_ApCnSK_Id_noevents;
				thisData.C.Tb_ApCnSK_Id_TextField_noevents = newData.C.Tb_ApCnSK_Id_TextField_noevents;
				thisData.C.Tb_UseLegacyConnectionCode_noevents = newData.C.Tb_UseLegacyConnectionCode_noevents;
				thisData.C.Tb_PkIsIdentity_noevents = newData.C.Tb_PkIsIdentity_noevents;
				thisData.C.Tb_IsVirtual_noevents = newData.C.Tb_IsVirtual_noevents;
				thisData.C.Tb_IsScreenPlaceHolder_noevents = newData.C.Tb_IsScreenPlaceHolder_noevents;
				thisData.C.Tb_IsNotEntity_noevents = newData.C.Tb_IsNotEntity_noevents;
				thisData.C.Tb_IsMany2Many_noevents = newData.C.Tb_IsMany2Many_noevents;
				thisData.C.Tb_UseLastModifiedByUserNameInSproc_noevents = newData.C.Tb_UseLastModifiedByUserNameInSproc_noevents;
				thisData.C.Tb_UseUserTimeStamp_noevents = newData.C.Tb_UseUserTimeStamp_noevents;
				thisData.C.Tb_UseForAudit_noevents = newData.C.Tb_UseForAudit_noevents;
				thisData.C.Tb_IsAllowDelete_noevents = newData.C.Tb_IsAllowDelete_noevents;
				thisData.C.Tb_CnfgGdMnu_MenuRow_IsVisible_noevents = newData.C.Tb_CnfgGdMnu_MenuRow_IsVisible_noevents;
				thisData.C.Tb_CnfgGdMnu_GridTools_IsVisible_noevents = newData.C.Tb_CnfgGdMnu_GridTools_IsVisible_noevents;
				thisData.C.Tb_CnfgGdMnu_SplitScreen_IsVisible_noevents = newData.C.Tb_CnfgGdMnu_SplitScreen_IsVisible_noevents;
				thisData.C.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default_noevents = newData.C.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default_noevents;
				thisData.C.Tb_CnfgGdMnu_NavColumnWidth_noevents = newData.C.Tb_CnfgGdMnu_NavColumnWidth_noevents;
				thisData.C.Tb_CnfgGdMnu_IsReadOnly_noevents = newData.C.Tb_CnfgGdMnu_IsReadOnly_noevents;
				thisData.C.Tb_CnfgGdMnu_IsAllowNewRow_noevents = newData.C.Tb_CnfgGdMnu_IsAllowNewRow_noevents;
				thisData.C.Tb_CnfgGdMnu_ExcelExport_IsVisible_noevents = newData.C.Tb_CnfgGdMnu_ExcelExport_IsVisible_noevents;
				thisData.C.Tb_CnfgGdMnu_ColumnChooser_IsVisible_noevents = newData.C.Tb_CnfgGdMnu_ColumnChooser_IsVisible_noevents;
				thisData.C.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible_noevents = newData.C.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible_noevents;
				thisData.C.Tb_CnfgGdMnu_RefreshGrid_IsVisible_noevents = newData.C.Tb_CnfgGdMnu_RefreshGrid_IsVisible_noevents;
				thisData.C.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible_noevents = newData.C.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible_noevents;
				thisData.C.Tb_IsInputComplete_noevents = newData.C.Tb_IsInputComplete_noevents;
				thisData.C.Tb_IsCodeGen_noevents = newData.C.Tb_IsCodeGen_noevents;
				thisData.C.Tb_IsReadyCodeGen_noevents = newData.C.Tb_IsReadyCodeGen_noevents;
				thisData.C.Tb_IsCodeGenComplete_noevents = newData.C.Tb_IsCodeGenComplete_noevents;
				thisData.C.Tb_IsTagForCodeGen_noevents = newData.C.Tb_IsTagForCodeGen_noevents;
				thisData.C.Tb_IsTagForOther_noevents = newData.C.Tb_IsTagForOther_noevents;
				thisData.C.Tb_TableGroups_noevents = newData.C.Tb_TableGroups_noevents;
				thisData.C.Tb_IsActiveRow_noevents = newData.C.Tb_IsActiveRow_noevents;
				thisData.C.Tb_IsDeleted_noevents = newData.C.Tb_IsDeleted_noevents;
				thisData.C.Tb_CreatedUserId_noevents = newData.C.Tb_CreatedUserId_noevents;
				thisData.C.Tb_CreatedDate_noevents = newData.C.Tb_CreatedDate_noevents;
				thisData.C.Tb_UserId_noevents = newData.C.Tb_UserId_noevents;
				thisData.C.UserName_noevents = newData.C.UserName_noevents;
				thisData.C.Tb_LastModifiedDate_noevents = newData.C.Tb_LastModifiedDate_noevents;
				thisData.C.Tb_Stamp_noevents = newData.C.Tb_Stamp_noevents;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", IfxTraceCategory.Leave);
            }
        }


		#endregion CRUD Methods


		#region General Methods and Properties


		#region Wire Methods

        /// <summary>
        /// Returns a business object implementing the IBusinessObject interface from a wire
        /// object. This assumes the WcTable_ValuesMngr being passed in has been fetched from a
        /// reliable data store or some other reliable source which gives us appropriate data and
        /// state.
        /// </summary>
        public static WcTable_Bll FromWire(WcTable_ValuesMngr valueObject)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", new ValuePair[] {new ValuePair("valueObject", valueObject) }, IfxTraceCategory.Enter);
                //  This assumes that each WcTable_ValuesMngr has been fetched from a db or some other reliable source which gives us the following state
                //  Hack, Hack;  we have to add the state here becuase of how its handled (or not handled) in WcTable_ValuesMngr due to the problem of loosing this value when passed accross the wire
                WcTable_Bll obj = new WcTable_Bll(valueObject, new EntityState(false, true, false));
                return obj;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Returns the current wire object plugged into this business object.</summary>
        public WcTable_ValuesMngr ToWire()
        {
            return _data;
        }

        /// <summary>The wire object property.</summary>
        public WcTable_ValuesMngr Wire
        {
            get { return _data; }
            set
            {
                _data = value;
            }
        }        

		#endregion Wire Methods


        /// <overloads>Get a list of current BrokenRules for this entity.</overloads>
        /// <summary>Retuns the current BrokenRules as list of strings.</summary>
        public List<string> GetBrokenRulesForEntity()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRulesForEntity();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Pass in a valid format as a string and return the current BrokenRules in a
        /// formatted list of strings.
        /// </summary>
        public List<string> GetBrokenRulesForEntity(string format)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", new ValuePair[] {new ValuePair("format", format) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRulesForEntity(format);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Leave);
            }
        }

        // get the current list of broken rules for a property
        /// <summary>Pass in a property name and return a list of its current BrokenRules.</summary>
        public List<vRuleItem> GetBrokenRulesForProperty(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRuleListForProperty(propertyName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Pass in a property name to find out if it’s valid or not.</summary>
        /// <returns>true = valid, false = not valid</returns>
        public bool IsPropertyValid(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.IsPropertyValid(propertyName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", IfxTraceCategory.Leave);
            }
        }

        public bool IsPropertyDirty(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);

                if (propertyName == null)
                {
                    throw new Exception("WcTable_Bll.IsPropertyDirty: Parameter 'propertyName' had no value.");
                }
                else if (propertyName.Trim().Length == 0)
                {
                    throw new Exception("WcTable_Bll.IsPropertyDirty: Parameter 'propertyName' had no value.");
                }

                switch (propertyName)
                {
                    case "Tb_ApVrsn_Id":
                        if (_data.C.Tb_ApVrsn_Id != _data.O.Tb_ApVrsn_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_auditTable_Id":
                        if (_data.C.Tb_auditTable_Id != _data.O.Tb_auditTable_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "AttachmentCount":
                        if (_data.C.AttachmentCount != _data.O.AttachmentCount)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "AttachmentFileNames":
                        if (_data.C.AttachmentFileNames != _data.O.AttachmentFileNames)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "DiscussionCount":
                        if (_data.C.DiscussionCount != _data.O.DiscussionCount)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "DiscussionTitles":
                        if (_data.C.DiscussionTitles != _data.O.DiscussionTitles)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_Name":
                        if (_data.C.Tb_Name != _data.O.Tb_Name)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_EntityRootName":
                        if (_data.C.Tb_EntityRootName != _data.O.Tb_EntityRootName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_VariableName":
                        if (_data.C.Tb_VariableName != _data.O.Tb_VariableName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_ScreenCaption":
                        if (_data.C.Tb_ScreenCaption != _data.O.Tb_ScreenCaption)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_Description":
                        if (_data.C.Tb_Description != _data.O.Tb_Description)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_DevelopmentNote":
                        if (_data.C.Tb_DevelopmentNote != _data.O.Tb_DevelopmentNote)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_UIAssemblyName":
                        if (_data.C.Tb_UIAssemblyName != _data.O.Tb_UIAssemblyName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_UINamespace":
                        if (_data.C.Tb_UINamespace != _data.O.Tb_UINamespace)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_UIAssemblyPath":
                        if (_data.C.Tb_UIAssemblyPath != _data.O.Tb_UIAssemblyPath)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_ProxyAssemblyName":
                        if (_data.C.Tb_ProxyAssemblyName != _data.O.Tb_ProxyAssemblyName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_ProxyAssemblyPath":
                        if (_data.C.Tb_ProxyAssemblyPath != _data.O.Tb_ProxyAssemblyPath)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_WebServiceName":
                        if (_data.C.Tb_WebServiceName != _data.O.Tb_WebServiceName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_WebServiceFolder":
                        if (_data.C.Tb_WebServiceFolder != _data.O.Tb_WebServiceFolder)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_DataServiceAssemblyName":
                        if (_data.C.Tb_DataServiceAssemblyName != _data.O.Tb_DataServiceAssemblyName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_DataServicePath":
                        if (_data.C.Tb_DataServicePath != _data.O.Tb_DataServicePath)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_WireTypeAssemblyName":
                        if (_data.C.Tb_WireTypeAssemblyName != _data.O.Tb_WireTypeAssemblyName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_WireTypePath":
                        if (_data.C.Tb_WireTypePath != _data.O.Tb_WireTypePath)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_UseTilesInPropsScreen":
                        if (_data.C.Tb_UseTilesInPropsScreen != _data.O.Tb_UseTilesInPropsScreen)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_UseGridColumnGroups":
                        if (_data.C.Tb_UseGridColumnGroups != _data.O.Tb_UseGridColumnGroups)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_UseGridDataSourceCombo":
                        if (_data.C.Tb_UseGridDataSourceCombo != _data.O.Tb_UseGridDataSourceCombo)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_ApCnSK_Id":
                        if (_data.C.Tb_ApCnSK_Id != _data.O.Tb_ApCnSK_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_UseLegacyConnectionCode":
                        if (_data.C.Tb_UseLegacyConnectionCode != _data.O.Tb_UseLegacyConnectionCode)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_PkIsIdentity":
                        if (_data.C.Tb_PkIsIdentity != _data.O.Tb_PkIsIdentity)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_IsVirtual":
                        if (_data.C.Tb_IsVirtual != _data.O.Tb_IsVirtual)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_IsScreenPlaceHolder":
                        if (_data.C.Tb_IsScreenPlaceHolder != _data.O.Tb_IsScreenPlaceHolder)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_IsNotEntity":
                        if (_data.C.Tb_IsNotEntity != _data.O.Tb_IsNotEntity)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_IsMany2Many":
                        if (_data.C.Tb_IsMany2Many != _data.O.Tb_IsMany2Many)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_UseLastModifiedByUserNameInSproc":
                        if (_data.C.Tb_UseLastModifiedByUserNameInSproc != _data.O.Tb_UseLastModifiedByUserNameInSproc)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_UseUserTimeStamp":
                        if (_data.C.Tb_UseUserTimeStamp != _data.O.Tb_UseUserTimeStamp)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_UseForAudit":
                        if (_data.C.Tb_UseForAudit != _data.O.Tb_UseForAudit)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_IsAllowDelete":
                        if (_data.C.Tb_IsAllowDelete != _data.O.Tb_IsAllowDelete)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_CnfgGdMnu_MenuRow_IsVisible":
                        if (_data.C.Tb_CnfgGdMnu_MenuRow_IsVisible != _data.O.Tb_CnfgGdMnu_MenuRow_IsVisible)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_CnfgGdMnu_GridTools_IsVisible":
                        if (_data.C.Tb_CnfgGdMnu_GridTools_IsVisible != _data.O.Tb_CnfgGdMnu_GridTools_IsVisible)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_CnfgGdMnu_SplitScreen_IsVisible":
                        if (_data.C.Tb_CnfgGdMnu_SplitScreen_IsVisible != _data.O.Tb_CnfgGdMnu_SplitScreen_IsVisible)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_CnfgGdMnu_SplitScreen_IsSplit_Default":
                        if (_data.C.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default != _data.O.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_CnfgGdMnu_NavColumnWidth":
                        if (_data.C.Tb_CnfgGdMnu_NavColumnWidth != _data.O.Tb_CnfgGdMnu_NavColumnWidth)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_CnfgGdMnu_IsReadOnly":
                        if (_data.C.Tb_CnfgGdMnu_IsReadOnly != _data.O.Tb_CnfgGdMnu_IsReadOnly)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_CnfgGdMnu_IsAllowNewRow":
                        if (_data.C.Tb_CnfgGdMnu_IsAllowNewRow != _data.O.Tb_CnfgGdMnu_IsAllowNewRow)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_CnfgGdMnu_ExcelExport_IsVisible":
                        if (_data.C.Tb_CnfgGdMnu_ExcelExport_IsVisible != _data.O.Tb_CnfgGdMnu_ExcelExport_IsVisible)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_CnfgGdMnu_ColumnChooser_IsVisible":
                        if (_data.C.Tb_CnfgGdMnu_ColumnChooser_IsVisible != _data.O.Tb_CnfgGdMnu_ColumnChooser_IsVisible)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_CnfgGdMnu_ShowHideColBtn_IsVisible":
                        if (_data.C.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible != _data.O.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_CnfgGdMnu_RefreshGrid_IsVisible":
                        if (_data.C.Tb_CnfgGdMnu_RefreshGrid_IsVisible != _data.O.Tb_CnfgGdMnu_RefreshGrid_IsVisible)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible":
                        if (_data.C.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible != _data.O.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_IsInputComplete":
                        if (_data.C.Tb_IsInputComplete != _data.O.Tb_IsInputComplete)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_IsCodeGen":
                        if (_data.C.Tb_IsCodeGen != _data.O.Tb_IsCodeGen)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_IsReadyCodeGen":
                        if (_data.C.Tb_IsReadyCodeGen != _data.O.Tb_IsReadyCodeGen)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_IsCodeGenComplete":
                        if (_data.C.Tb_IsCodeGenComplete != _data.O.Tb_IsCodeGenComplete)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_IsTagForCodeGen":
                        if (_data.C.Tb_IsTagForCodeGen != _data.O.Tb_IsTagForCodeGen)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_IsTagForOther":
                        if (_data.C.Tb_IsTagForOther != _data.O.Tb_IsTagForOther)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_TableGroups":
                        if (_data.C.Tb_TableGroups != _data.O.Tb_TableGroups)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_IsActiveRow":
                        if (_data.C.Tb_IsActiveRow != _data.O.Tb_IsActiveRow)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_IsDeleted":
                        if (_data.C.Tb_IsDeleted != _data.O.Tb_IsDeleted)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tb_CreatedUserId":
                        if (_data.C.Tb_CreatedUserId != _data.O.Tb_CreatedUserId)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "UserName":
                        if (_data.C.UserName != _data.O.UserName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    default:
                        throw new Exception("WcTable_Bll.IsPropertyDirty found no matching propery name for " + propertyName);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", IfxTraceCategory.Leave);
            }
        }

        public void SetDateFromString(string propName, string value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", new ValuePair[] {new ValuePair("value", value), new ValuePair("propName", propName) }, IfxTraceCategory.Enter);
                DateTime? dt = null;
                if (BLLHelper.IsDate(value) == true)
                {
                    dt = DateTime.Parse(value);
                }
                switch (propName)
                {
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", IfxTraceCategory.Leave);
            }
        }

        public object GetPropertyValueByKey(string key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", new ValuePair[] {new ValuePair("key", key) }, IfxTraceCategory.Enter);
                switch (key)
                {
                    case "AttachmentCount":
                        return AttachmentCount;

                    case "DiscussionCount":
                        return DiscussionCount;

                    case "Tb_Name":
                        return Tb_Name;

                    case "Tb_EntityRootName":
                        return Tb_EntityRootName;

                    case "Tb_VariableName":
                        return Tb_VariableName;

                    case "Tb_ScreenCaption":
                        return Tb_ScreenCaption;

                    case "Tb_Description":
                        return Tb_Description;

                    case "Tb_DevelopmentNote":
                        return Tb_DevelopmentNote;

                    case "Tb_UIAssemblyName":
                        return Tb_UIAssemblyName;

                    case "Tb_UINamespace":
                        return Tb_UINamespace;

                    case "Tb_UIAssemblyPath":
                        return Tb_UIAssemblyPath;

                    case "Tb_ProxyAssemblyName":
                        return Tb_ProxyAssemblyName;

                    case "Tb_ProxyAssemblyPath":
                        return Tb_ProxyAssemblyPath;

                    case "Tb_WebServiceName":
                        return Tb_WebServiceName;

                    case "Tb_WebServiceFolder":
                        return Tb_WebServiceFolder;

                    case "Tb_DataServiceAssemblyName":
                        return Tb_DataServiceAssemblyName;

                    case "Tb_DataServicePath":
                        return Tb_DataServicePath;

                    case "Tb_WireTypeAssemblyName":
                        return Tb_WireTypeAssemblyName;

                    case "Tb_WireTypePath":
                        return Tb_WireTypePath;

                    case "Tb_UseTilesInPropsScreen":
                        return Tb_UseTilesInPropsScreen;

                    case "Tb_UseGridColumnGroups":
                        return Tb_UseGridColumnGroups;

                    case "Tb_UseGridDataSourceCombo":
                        return Tb_UseGridDataSourceCombo;

                    case "Tb_ApCnSK_Id":
                        return Tb_ApCnSK_Id;

                    case "Tb_UseLegacyConnectionCode":
                        return Tb_UseLegacyConnectionCode;

                    case "Tb_PkIsIdentity":
                        return Tb_PkIsIdentity;

                    case "Tb_IsVirtual":
                        return Tb_IsVirtual;

                    case "Tb_IsScreenPlaceHolder":
                        return Tb_IsScreenPlaceHolder;

                    case "Tb_IsNotEntity":
                        return Tb_IsNotEntity;

                    case "Tb_IsMany2Many":
                        return Tb_IsMany2Many;

                    case "Tb_UseLastModifiedByUserNameInSproc":
                        return Tb_UseLastModifiedByUserNameInSproc;

                    case "Tb_UseUserTimeStamp":
                        return Tb_UseUserTimeStamp;

                    case "Tb_UseForAudit":
                        return Tb_UseForAudit;

                    case "Tb_IsAllowDelete":
                        return Tb_IsAllowDelete;

                    case "Tb_CnfgGdMnu_MenuRow_IsVisible":
                        return Tb_CnfgGdMnu_MenuRow_IsVisible;

                    case "Tb_CnfgGdMnu_GridTools_IsVisible":
                        return Tb_CnfgGdMnu_GridTools_IsVisible;

                    case "Tb_CnfgGdMnu_SplitScreen_IsVisible":
                        return Tb_CnfgGdMnu_SplitScreen_IsVisible;

                    case "Tb_CnfgGdMnu_SplitScreen_IsSplit_Default":
                        return Tb_CnfgGdMnu_SplitScreen_IsSplit_Default;

                    case "Tb_CnfgGdMnu_NavColumnWidth":
                        return Tb_CnfgGdMnu_NavColumnWidth;

                    case "Tb_CnfgGdMnu_IsReadOnly":
                        return Tb_CnfgGdMnu_IsReadOnly;

                    case "Tb_CnfgGdMnu_IsAllowNewRow":
                        return Tb_CnfgGdMnu_IsAllowNewRow;

                    case "Tb_CnfgGdMnu_ExcelExport_IsVisible":
                        return Tb_CnfgGdMnu_ExcelExport_IsVisible;

                    case "Tb_CnfgGdMnu_ColumnChooser_IsVisible":
                        return Tb_CnfgGdMnu_ColumnChooser_IsVisible;

                    case "Tb_CnfgGdMnu_ShowHideColBtn_IsVisible":
                        return Tb_CnfgGdMnu_ShowHideColBtn_IsVisible;

                    case "Tb_CnfgGdMnu_RefreshGrid_IsVisible":
                        return Tb_CnfgGdMnu_RefreshGrid_IsVisible;

                    case "Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible":
                        return Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible;

                    case "Tb_IsInputComplete":
                        return Tb_IsInputComplete;

                    case "Tb_IsCodeGen":
                        return Tb_IsCodeGen;

                    case "Tb_IsReadyCodeGen":
                        return Tb_IsReadyCodeGen;

                    case "Tb_IsCodeGenComplete":
                        return Tb_IsCodeGenComplete;

                    case "Tb_IsTagForCodeGen":
                        return Tb_IsTagForCodeGen;

                    case "Tb_IsTagForOther":
                        return Tb_IsTagForOther;

                    case "Tb_TableGroups":
                        return Tb_TableGroups;

                    case "Tb_IsActiveRow":
                        return Tb_IsActiveRow;

                    case "UserName":
                        return UserName;

                    case "Tb_LastModifiedDate":
                        return Tb_LastModifiedDate;

                    default:
                        return GetPropertyValueByKeyCustomCode(key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", IfxTraceCategory.Leave);
            }
        }

        public string GetPropertyFormattedStringValueByKey(string key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", new ValuePair[] {new ValuePair("key", key) }, IfxTraceCategory.Enter);
                switch (key)
                {
                    case "EntityId":
                        return Tb_Id.ToString();

                    case "AttachmentCount":
                        if (AttachmentCount == null)
                        {
                            return null;
                        }
                        else
                        {
                            return AttachmentCount.ToString();
                        }

                    case "DiscussionCount":
                        if (DiscussionCount == null)
                        {
                            return null;
                        }
                        else
                        {
                            return DiscussionCount.ToString();
                        }

                    case "Tb_Name":
                        return Tb_Name;

                    case "Tb_EntityRootName":
                        return Tb_EntityRootName;

                    case "Tb_VariableName":
                        return Tb_VariableName;

                    case "Tb_ScreenCaption":
                        return Tb_ScreenCaption;

                    case "Tb_Description":
                        return Tb_Description;

                    case "Tb_DevelopmentNote":
                        return Tb_DevelopmentNote;

                    case "Tb_UIAssemblyName":
                        return Tb_UIAssemblyName;

                    case "Tb_UINamespace":
                        return Tb_UINamespace;

                    case "Tb_UIAssemblyPath":
                        return Tb_UIAssemblyPath;

                    case "Tb_ProxyAssemblyName":
                        return Tb_ProxyAssemblyName;

                    case "Tb_ProxyAssemblyPath":
                        return Tb_ProxyAssemblyPath;

                    case "Tb_WebServiceName":
                        return Tb_WebServiceName;

                    case "Tb_WebServiceFolder":
                        return Tb_WebServiceFolder;

                    case "Tb_DataServiceAssemblyName":
                        return Tb_DataServiceAssemblyName;

                    case "Tb_DataServicePath":
                        return Tb_DataServicePath;

                    case "Tb_WireTypeAssemblyName":
                        return Tb_WireTypeAssemblyName;

                    case "Tb_WireTypePath":
                        return Tb_WireTypePath;

                    case "Tb_UseTilesInPropsScreen":
                        return Tb_UseTilesInPropsScreen.ToString();

                    case "Tb_UseGridColumnGroups":
                        return Tb_UseGridColumnGroups.ToString();

                    case "Tb_UseGridDataSourceCombo":
                        return Tb_UseGridDataSourceCombo.ToString();

                    case "Tb_ApCnSK_Id":
                        return Tb_ApCnSK_Id_TextField;

                    case "Tb_UseLegacyConnectionCode":
                        return Tb_UseLegacyConnectionCode.ToString();

                    case "Tb_PkIsIdentity":
                        return Tb_PkIsIdentity.ToString();

                    case "Tb_IsVirtual":
                        return Tb_IsVirtual.ToString();

                    case "Tb_IsScreenPlaceHolder":
                        return Tb_IsScreenPlaceHolder.ToString();

                    case "Tb_IsNotEntity":
                        return Tb_IsNotEntity.ToString();

                    case "Tb_IsMany2Many":
                        return Tb_IsMany2Many.ToString();

                    case "Tb_UseLastModifiedByUserNameInSproc":
                        return Tb_UseLastModifiedByUserNameInSproc.ToString();

                    case "Tb_UseUserTimeStamp":
                        return Tb_UseUserTimeStamp.ToString();

                    case "Tb_UseForAudit":
                        return Tb_UseForAudit.ToString();

                    case "Tb_IsAllowDelete":
                        return Tb_IsAllowDelete.ToString();

                    case "Tb_CnfgGdMnu_MenuRow_IsVisible":
                        return Tb_CnfgGdMnu_MenuRow_IsVisible.ToString();

                    case "Tb_CnfgGdMnu_GridTools_IsVisible":
                        return Tb_CnfgGdMnu_GridTools_IsVisible.ToString();

                    case "Tb_CnfgGdMnu_SplitScreen_IsVisible":
                        return Tb_CnfgGdMnu_SplitScreen_IsVisible.ToString();

                    case "Tb_CnfgGdMnu_SplitScreen_IsSplit_Default":
                        return Tb_CnfgGdMnu_SplitScreen_IsSplit_Default.ToString();

                    case "Tb_CnfgGdMnu_NavColumnWidth":
                        return Tb_CnfgGdMnu_NavColumnWidth.ToString();

                    case "Tb_CnfgGdMnu_IsReadOnly":
                        return Tb_CnfgGdMnu_IsReadOnly.ToString();

                    case "Tb_CnfgGdMnu_IsAllowNewRow":
                        return Tb_CnfgGdMnu_IsAllowNewRow.ToString();

                    case "Tb_CnfgGdMnu_ExcelExport_IsVisible":
                        return Tb_CnfgGdMnu_ExcelExport_IsVisible.ToString();

                    case "Tb_CnfgGdMnu_ColumnChooser_IsVisible":
                        return Tb_CnfgGdMnu_ColumnChooser_IsVisible.ToString();

                    case "Tb_CnfgGdMnu_ShowHideColBtn_IsVisible":
                        return Tb_CnfgGdMnu_ShowHideColBtn_IsVisible.ToString();

                    case "Tb_CnfgGdMnu_RefreshGrid_IsVisible":
                        return Tb_CnfgGdMnu_RefreshGrid_IsVisible.ToString();

                    case "Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible":
                        return Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible.ToString();

                    case "Tb_IsInputComplete":
                        return Tb_IsInputComplete.ToString();

                    case "Tb_IsCodeGen":
                        return Tb_IsCodeGen.ToString();

                    case "Tb_IsReadyCodeGen":
                        return Tb_IsReadyCodeGen.ToString();

                    case "Tb_IsCodeGenComplete":
                        return Tb_IsCodeGenComplete.ToString();

                    case "Tb_IsTagForCodeGen":
                        return Tb_IsTagForCodeGen.ToString();

                    case "Tb_IsTagForOther":
                        return Tb_IsTagForOther.ToString();

                    case "Tb_TableGroups":
                        return Tb_TableGroups;

                    case "Tb_IsActiveRow":
                        return Tb_IsActiveRow.ToString();

                    case "UserName":
                        return UserName;

                    case "Tb_LastModifiedDate":
                        if (Tb_LastModifiedDate == null)
                        {
                            return null;
                        }
                        else
                        {
                            return Tb_LastModifiedDate.ToString();
                        }

                    default:
                        return GetPropertyFormattedStringValueByKeyCustomCode(key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", IfxTraceCategory.Leave);
            }
        }

        
        #region General Properties

        /// <summary>
        ///     Returns a list of current BrokenRules for this entity as a list of <see cref="TypeServices.vRuleItem">vRuleItem</see> types.
        /// </summary>
        [Browsable(false)]
        public List<vRuleItem> GetBrokenRuleListForEntity()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", IfxTraceCategory.Enter);
                return BrokenRuleManagerProperty.GetBrokenRuleListForEntity();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", IfxTraceCategory.Leave);
            }
        }

        #endregion General Properties        

		#endregion General Methods and Properties


		#region Events


        /// <summary>
        ///     Raises the <see cref="CrudFailed">CrudFailed</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnCrudFailed(object sender, CrudFailedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Enter);
            CrudFailedEventHandler handler = CrudFailed;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Leave);
        }

        // THIS IS NOW OBSOLETE
        /// <summary>
        ///     Calls the <see cref="OnAsyncSaveComplete">OnAsyncSaveComplete</see> to raise the <see cref="AsyncSaveComplete">AsyncSaveComplete</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventAsyncSaveComplete(DataOperationResult result, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveComplete", IfxTraceCategory.Enter);
            AsyncSaveCompleteArgs e = new AsyncSaveCompleteArgs(result, failedReasonText);
            OnAsyncSaveComplete(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveComplete", IfxTraceCategory.Leave);
        }

        // THIS IS NOW OBSOLETE
        /// <summary>
        ///     Raises the <see cref="AsyncSaveComplete">AsyncSaveComplete</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnAsyncSaveComplete(object sender, AsyncSaveCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveComplete", IfxTraceCategory.Enter);
            AsyncSaveCompleteEventHandler handler = AsyncSaveComplete;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveComplete", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Calls the <see cref="OnAsyncSaveWithResponseComplete">OnAsyncSaveWithResponseComplete</see> to raise the <see cref="AsyncSaveWithResponseComplete">AsyncSaveWithResponseComplete</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventAsyncSaveWithResponseComplete(DataServiceInsertUpdateResponse response, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
            AsyncSaveWithResponseCompleteArgs e = new AsyncSaveWithResponseCompleteArgs(response, failedReasonText);
            OnAsyncSaveWithResponseComplete(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Raises the <see cref="AsyncSaveWithResponseComplete">AsyncSaveWithResponseComplete</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnAsyncSaveWithResponseComplete(object sender, AsyncSaveWithResponseCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
            AsyncSaveWithResponseCompleteEventHandler handler = AsyncSaveWithResponseComplete;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
        }










        void RaiseEventEntityRowReceived()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventEntityRowReceived", IfxTraceCategory.Enter);
            EntityRowReceivedEventHandler handler = EntityRowReceived;
            if (handler != null)
            {
                handler(this, new EntityRowReceivedArgs());
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventEntityRowReceived", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Calls the <see cref="OnCrudFailed">OnCrudFailed</see> to raise the <see cref="CrudFailed">CrudFailed</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventCrudFailed(int failureCode, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCrudFailed", IfxTraceCategory.Enter);
            CrudFailedArgs e = new CrudFailedArgs(failureCode, failedReasonText);
            OnCrudFailed(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCrudFailed", IfxTraceCategory.Leave);
        }


        /// <summary>
        ///     Calls the <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see>
        ///     method to raise the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event passing in a
        ///     reference of this business object for the ActiveBusinessObject parameter. As it
        ///     bubbles up to an Entity Properties control, that control will pass in a reference
        ///     of itself for the ActivePropertiesControl parameter. As it bubbles up to the Entity
        ///     Manager control, that control will pass in a reference of itself for the
        ///     ActiveEntityControl parameter. It should continue to bubble up to the top level
        ///     control. This notifies all controls along the about which controls are active and
        ///     the current state so they can always be configures accordingly. Now that the top
        ///     level control (perhaps the main application window) has a reference to these 3
        ///     important objects, it can easily communicate with then as the use interacts with
        ///     the application.
        /// </summary>        
        void RaiseEventCurrentEntityStateChanged(EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCurrentEntityStateChanged", new ValuePair[] {new ValuePair("state", state) }, IfxTraceCategory.Enter);
            CurrentEntityStateArgs e = new CurrentEntityStateArgs(state, null, null, this);
            OnCurrentEntityStateChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCurrentEntityStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     Raises the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see>
        ///     event passing in a reference of this business object for the ActiveBusinessObject
        ///     parameter. As it bubbles up to an Entity Properties control, that control will pass
        ///     in a reference of itself for the ActivePropertiesControl parameter. As it bubbles
        ///     up to the Entity Manager control, that control will pass in a reference of itself
        ///     for the ActiveEntityControl parameter. It should continue to bubble up to the top
        ///     level control. This notifies all controls along the about which controls are active
        ///     and the current state so they can always be configures accordingly. Now that the
        ///     top level control (perhaps the main application window) has a reference to these 3
        ///     important objects, it can easily communicate with then as the use interacts with
        ///     the application.
        /// </summary>
        void OnCurrentEntityStateChanged(object sender, CurrentEntityStateArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", new ValuePair[] {new ValuePair("e.State", e.State) }, IfxTraceCategory.Enter);
            CurrentEntityStateEventHandler handler = CurrentEntityStateChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     This method is obsolete and is replaced with <see cref="RaiseEventControlValidStateChanged">RaiseEventControlValidStateChanged</see> which
        ///     is called in the validation section of this class in the partial class
        ///     WcTable.bll.vld.cs code file.
        /// </summary>
        void RaiseEventBrokenRuleChanged(string rule)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventBrokenRuleChanged", new ValuePair[] {new ValuePair("rule", rule) }, IfxTraceCategory.Enter);
            BrokenRuleArgs e = new BrokenRuleArgs(rule);
            OnBrokenRuleChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventBrokenRuleChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>This method is mostly obsolete, but remains because it’s an easy way to be
        ///     notified of the current broken rule.</para>
        /// 	<para>
        ///         This method is hooked to the <see cref="BrokenRuleChanged">BrokenRuleChanged</see> event which is initiated by
        ///         BrokenRuleManager and bubbles up to the business object and then to this
        ///         control (ucWcTableProps) (or in some cases a ucEntityList control when it’s in
        ///         Read/Write mode).
        ///     </para>
        /// 	<para><br/>
        ///     For information on how Broken Rules are currently being managed, see (in the order
        ///     of execution):</para>
        /// 	<list type="bullet">
        /// 		<item>In the business object’s data property – FieldName_Validate();</item>
        /// 		<item>
        ///             In the FieldName_Validate method this line executes:<br/>
        ///             _brokenRuleManager.<see cref="TypeServices.BrokenRuleManager.AddBrokenRuleForProperty">AddBrokenRuleForProperty</see>("FieldName",
        ///             BROKENRULE_ FieldName _SomeRuleName);
        ///         </item>
        /// 		<item>
        /// 			<see cref="TypeServices.BrokenRuleManager.SetEntityValidState">BrokenRuleManager.SetEntityValidState()</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
       private void OnBrokenRuleChanged(object sender, BrokenRuleArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Enter);
            BrokenRuleEventHandler handler = BrokenRuleChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// Notifies an event handler that a property value as change. Currently not being
        /// uses.
        /// </summary>
        private void OnPropertyValueChanged(object sender, PropertyValueChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnPropertyValueChanged", new ValuePair[] {new ValuePair("e.PropertyName", e.PropertyName), new ValuePair("e.IsValid", e.IsValid), new ValuePair("e.BrokenRules", e.BrokenRules) }, IfxTraceCategory.Enter);
            PropertyValueChangedEventHandler handler = PropertyValueChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnPropertyValueChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     Replaces the RaiseEventBrokenRuleChanged method and used in the validation section
        ///     of this class in the partial class WcTable.bll.vld.cs code file. Validation code will
        ///     pass in property name and an isValid flag. This will call the <see cref="OnControlValidStateChanged">OnControlValidStateChanged</see> method which will
        ///     raise the <see cref="ControlValidStateChanged">ControlValidStateChanged</see> event
        ///     up to the parent. The parent will then take the appropriate actions such as setting
        ///     the control’s valid/not valid appearance.
        /// </summary>
        void RaiseEventControlValidStateChanged(string propertyName, bool isValid, bool isDirty)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventControlValidStateChanged", new ValuePair[] {new ValuePair("isValid", isValid) }, IfxTraceCategory.Enter);
            ControlValidStateChangedArgs e = new ControlValidStateChangedArgs(propertyName, isValid, isDirty);
            OnControlValidStateChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventControlValidStateChanged", IfxTraceCategory.Leave);
        }       

        /// <summary>
        /// Raise the ControlValidStateChanged event up to the parent. The parent will then
        /// take the appropriate actions such as setting the control’s valid/not valid
        /// appearance.
        /// </summary>
        private void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", new ValuePair[] {new ValuePair("e.IsValid", e.IsValid) }, IfxTraceCategory.Enter);
            ControlValidStateChangedEventHandler handler = ControlValidStateChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableProps.OnRestrictedTextLengthChanged">ucWcTableProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableProps.SetRestrictedStringLengthText">ucWcTableProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void RaiseEventRestrictedTextLengthChanged()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Enter);
            RestrictedTextLengthChangedArgs e = new RestrictedTextLengthChangedArgs();
            RestrictedTextLengthChangedEventHandler handler = RestrictedTextLengthChanged;
            if (handler != null)
            {
                handler(this, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<param name="oldVal">The original property value.</param>
        /// 	<param name="newVal">The new property value.</param>        
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableProps.OnRestrictedTextLengthChanged">ucWcTableProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcTableProps.SetRestrictedStringLengthText">ucWcTableProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void RaiseEventRestrictedTextLengthChanged(string oldVal, string newVal)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Enter);
            bool lenChanged = false;
            if (oldVal == null && newVal != null)
            {
                if (newVal.Trim().Length > 0)
                {
                    lenChanged = true;
                }
            }
            else if (newVal == null && oldVal != null)
            {
                if (oldVal.Trim().Length > 0)
                {
                    lenChanged = true;
                }
            }
            else if (oldVal != null && newVal != null)
            {
                if (oldVal.Trim().Length != newVal.Trim().Length)
                {
                    lenChanged = true;
                }
            }
            if (lenChanged == true)
            {
                RestrictedTextLengthChangedArgs e = new RestrictedTextLengthChangedArgs();
                RestrictedTextLengthChangedEventHandler handler = RestrictedTextLengthChanged;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Leave);
        }        


		#endregion Events


		#region Flag Props


        /// <summary>
        ///     The name of the (text type – TextBox, TextBlock, etc.) control in the UI that
        ///     currently has the focus. This is used by the <see cref="ActiveRestrictedStringPropertyLength">ActiveRestrictedStringPropertyLength</see>
        ///     method to which returns the remaining available text length for the text control
        ///     with that name. It was decided to persist the name of the UI control in this
        ///     business object property rather than in the UI because it seemed to make sense to
        ///     have all of this logic centralized, and also because it simplifies things.
        /// </summary>
        [Browsable(false)]
        public string ActiveRestrictedStringProperty
        {
            get { return _activeRestrictedStringProperty; }
            set 
            { 
                _activeRestrictedStringProperty = value;
            }
        }

        /// <summary>
        ///     Returns the remaining available text length for the text control named by
        ///     <see cref="ActiveRestrictedStringProperty">ActiveRestrictedStringProperty</see>.
        /// </summary>
        [Browsable(false)]
        public int ActiveRestrictedStringPropertyLength
        {
            get
            {
                switch (_activeRestrictedStringProperty)
                {
                    case "Tb_Name":
                        if (_data.C.Tb_Name == null)
                        {
                            return STRINGSIZE_Tb_Name;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_Name - _data.C.Tb_Name.Length);
                        }
                        break;
                    
                    case "Tb_EntityRootName":
                        if (_data.C.Tb_EntityRootName == null)
                        {
                            return STRINGSIZE_Tb_EntityRootName;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_EntityRootName - _data.C.Tb_EntityRootName.Length);
                        }
                        break;
                    
                    case "Tb_VariableName":
                        if (_data.C.Tb_VariableName == null)
                        {
                            return STRINGSIZE_Tb_VariableName;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_VariableName - _data.C.Tb_VariableName.Length);
                        }
                        break;
                    
                    case "Tb_ScreenCaption":
                        if (_data.C.Tb_ScreenCaption == null)
                        {
                            return STRINGSIZE_Tb_ScreenCaption;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_ScreenCaption - _data.C.Tb_ScreenCaption.Length);
                        }
                        break;
                    
                    case "Tb_Description":
                        if (_data.C.Tb_Description == null)
                        {
                            return STRINGSIZE_Tb_Description;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_Description - _data.C.Tb_Description.Length);
                        }
                        break;
                    
                    case "Tb_DevelopmentNote":
                        if (_data.C.Tb_DevelopmentNote == null)
                        {
                            return STRINGSIZE_Tb_DevelopmentNote;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_DevelopmentNote - _data.C.Tb_DevelopmentNote.Length);
                        }
                        break;
                    
                    case "Tb_UIAssemblyName":
                        if (_data.C.Tb_UIAssemblyName == null)
                        {
                            return STRINGSIZE_Tb_UIAssemblyName;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_UIAssemblyName - _data.C.Tb_UIAssemblyName.Length);
                        }
                        break;
                    
                    case "Tb_UINamespace":
                        if (_data.C.Tb_UINamespace == null)
                        {
                            return STRINGSIZE_Tb_UINamespace;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_UINamespace - _data.C.Tb_UINamespace.Length);
                        }
                        break;
                    
                    case "Tb_UIAssemblyPath":
                        if (_data.C.Tb_UIAssemblyPath == null)
                        {
                            return STRINGSIZE_Tb_UIAssemblyPath;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_UIAssemblyPath - _data.C.Tb_UIAssemblyPath.Length);
                        }
                        break;
                    
                    case "Tb_ProxyAssemblyName":
                        if (_data.C.Tb_ProxyAssemblyName == null)
                        {
                            return STRINGSIZE_Tb_ProxyAssemblyName;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_ProxyAssemblyName - _data.C.Tb_ProxyAssemblyName.Length);
                        }
                        break;
                    
                    case "Tb_ProxyAssemblyPath":
                        if (_data.C.Tb_ProxyAssemblyPath == null)
                        {
                            return STRINGSIZE_Tb_ProxyAssemblyPath;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_ProxyAssemblyPath - _data.C.Tb_ProxyAssemblyPath.Length);
                        }
                        break;
                    
                    case "Tb_WebServiceName":
                        if (_data.C.Tb_WebServiceName == null)
                        {
                            return STRINGSIZE_Tb_WebServiceName;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_WebServiceName - _data.C.Tb_WebServiceName.Length);
                        }
                        break;
                    
                    case "Tb_WebServiceFolder":
                        if (_data.C.Tb_WebServiceFolder == null)
                        {
                            return STRINGSIZE_Tb_WebServiceFolder;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_WebServiceFolder - _data.C.Tb_WebServiceFolder.Length);
                        }
                        break;
                    
                    case "Tb_DataServiceAssemblyName":
                        if (_data.C.Tb_DataServiceAssemblyName == null)
                        {
                            return STRINGSIZE_Tb_DataServiceAssemblyName;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_DataServiceAssemblyName - _data.C.Tb_DataServiceAssemblyName.Length);
                        }
                        break;
                    
                    case "Tb_DataServicePath":
                        if (_data.C.Tb_DataServicePath == null)
                        {
                            return STRINGSIZE_Tb_DataServicePath;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_DataServicePath - _data.C.Tb_DataServicePath.Length);
                        }
                        break;
                    
                    case "Tb_WireTypeAssemblyName":
                        if (_data.C.Tb_WireTypeAssemblyName == null)
                        {
                            return STRINGSIZE_Tb_WireTypeAssemblyName;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_WireTypeAssemblyName - _data.C.Tb_WireTypeAssemblyName.Length);
                        }
                        break;
                    
                    case "Tb_WireTypePath":
                        if (_data.C.Tb_WireTypePath == null)
                        {
                            return STRINGSIZE_Tb_WireTypePath;
                        }
                        else
                        {
                            return (STRINGSIZE_Tb_WireTypePath - _data.C.Tb_WireTypePath.Length);
                        }
                        break;
                    }
                return 0;
            }
        }

 

		#endregion Flag Props


		#region Data Props


        /// <summary>
        ///     Called by data field properties in their getter block. This method will determine
        ///     if he <see cref="TypeServices.EntityState">state</see> has changed by the getter
        ///     being called. If it has changed, the <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see>
        ///     method will be called to notify the object using this business object.
        /// </summary>
       private void CheckEntityState(EntityStateSwitch es)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", new ValuePair[] {new ValuePair("_data.S.Switch", _data.S.Switch), new ValuePair("es", es) }, IfxTraceCategory.Enter);
                if (es != _data.S.Switch)
                {
                    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// This property does nothing and is used as a stub for a 'Rich Data Grid Column' binding to this entity
        /// </summary>
        public string RichGrid
        {
            get { return ""; }
        }



        public string DataRowName
        {
            get { return Tb_Name; }
        }




        private IBusinessObjectV2 _parentBusinessObject = null;

        public IBusinessObjectV2 ParentBusinessObject
        {
            get { return _parentBusinessObject; }
            set { _parentBusinessObject = value; }
        }

       public Guid? GuidPrimaryKey()
       {
           return _data.C.Tb_Id; 
       }

       public long? LongPrimaryKey()
       {
           throw new NotImplementedException("WcTable_Bll LongPrimaryKey() Not Implemented"); 
       }

       public int? IntPrimaryKey()
       {
           throw new NotImplementedException("WcTable_Bll LongPrimaryKey() Not Implemented"); 
       }

       public short? ShortPrimaryKey()
       {
           throw new NotImplementedException("WcTable_Bll LongPrimaryKey() Not Implemented"); 
       }

       public byte? BytePrimaryKey()
       {
           throw new NotImplementedException("WcTable_Bll LongPrimaryKey() Not Implemented"); 
       }

       public object ObjectPrimaryKey()
       {
           return _data.C.Tb_Id;
       }


        /// <summary>
        /// This is the Standing Foreign Key property. This value remains constant when
        /// calling the new method where all data fields are cleared and set to their ‘new’ default
        /// values. This allows creating new entities for the same parent. When an entity is
        /// fetched from the data store and used as the current entity in this business object, the
        /// Standing Foreign Key value is reset using the value from the fetched entity.
        /// </summary>
        public Guid StandingFK
        {
            get { return _StandingFK; }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    _StandingFK = value;
                    _data.C.Tb_ApVrsn_Id_noevents = _StandingFK;
                    _data.O.Tb_ApVrsn_Id_noevents = _StandingFK;
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", IfxTraceCategory.Leave);
                }
            }
        }

       /// <summary>
       /// 	<para>The Primary Key for WcTable</para>
       /// </summary>
        public Guid Tb_Id
        {
            get
            {
                return _data.C.Tb_Id;
            }
        }

		
        public Guid? Tb_ApVrsn_Id
        {
            get
            {
                return _data.C.Tb_ApVrsn_Id;
            }
        }

		
        public Guid? Tb_auditTable_Id
        {
            get
            {
                return _data.C.Tb_auditTable_Id;
            }
        }

		
        public Int32? AttachmentCount
        {
            get
            {
                return _data.C.AttachmentCount;
            }
        }

		
        public String AttachmentFileNames
        {
            get
            {
                return _data.C.AttachmentFileNames;
            }
        }

		
        public Int32? DiscussionCount
        {
            get
            {
                return _data.C.DiscussionCount;
            }
        }

		
        public String DiscussionTitles
        {
            get
            {
                return _data.C.DiscussionTitles;
            }
        }

		
        public String Tb_Name
        {
            get
            {
                return _data.C.Tb_Name;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Name - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_Name == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_Name == null || _data.C.Tb_Name.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_Name == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_Name = null;
                    }
                    else
                    {
                        _data.C.Tb_Name = value.Trim();
                    }
                    //else if ((_data.C.Tb_Name == null || _data.C.Tb_Name.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_Name == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_Name == null || _data.C.Tb_Name.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_Name = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_Name = value;
                    //}
                    CustomPropertySetterCode("Tb_Name");
                    Tb_Name_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_Name, _data.C.Tb_Name);
                        
					Notify("Tb_Name");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Name - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Name - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_EntityRootName
        {
            get
            {
                return _data.C.Tb_EntityRootName;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_EntityRootName - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_EntityRootName == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_EntityRootName == null || _data.C.Tb_EntityRootName.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_EntityRootName == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_EntityRootName = null;
                    }
                    else
                    {
                        _data.C.Tb_EntityRootName = value.Trim();
                    }
                    //else if ((_data.C.Tb_EntityRootName == null || _data.C.Tb_EntityRootName.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_EntityRootName == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_EntityRootName == null || _data.C.Tb_EntityRootName.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_EntityRootName = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_EntityRootName = value;
                    //}
                    CustomPropertySetterCode("Tb_EntityRootName");
                    Tb_EntityRootName_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_EntityRootName, _data.C.Tb_EntityRootName);
                        
					Notify("Tb_EntityRootName");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_EntityRootName - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_EntityRootName - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_VariableName
        {
            get
            {
                return _data.C.Tb_VariableName;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_VariableName - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_VariableName == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_VariableName == null || _data.C.Tb_VariableName.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_VariableName == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_VariableName = null;
                    }
                    else
                    {
                        _data.C.Tb_VariableName = value.Trim();
                    }
                    //else if ((_data.C.Tb_VariableName == null || _data.C.Tb_VariableName.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_VariableName == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_VariableName == null || _data.C.Tb_VariableName.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_VariableName = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_VariableName = value;
                    //}
                    CustomPropertySetterCode("Tb_VariableName");
                    Tb_VariableName_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_VariableName, _data.C.Tb_VariableName);
                        
					Notify("Tb_VariableName");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_VariableName - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_VariableName - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_ScreenCaption
        {
            get
            {
                return _data.C.Tb_ScreenCaption;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ScreenCaption - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_ScreenCaption == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_ScreenCaption == null || _data.C.Tb_ScreenCaption.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_ScreenCaption == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_ScreenCaption = null;
                    }
                    else
                    {
                        _data.C.Tb_ScreenCaption = value.Trim();
                    }
                    //else if ((_data.C.Tb_ScreenCaption == null || _data.C.Tb_ScreenCaption.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_ScreenCaption == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_ScreenCaption == null || _data.C.Tb_ScreenCaption.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_ScreenCaption = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_ScreenCaption = value;
                    //}
                    CustomPropertySetterCode("Tb_ScreenCaption");
                    Tb_ScreenCaption_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_ScreenCaption, _data.C.Tb_ScreenCaption);
                        
					Notify("Tb_ScreenCaption");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ScreenCaption - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ScreenCaption - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_Description
        {
            get
            {
                return _data.C.Tb_Description;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Description - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_Description == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_Description == null || _data.C.Tb_Description.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_Description == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_Description = null;
                    }
                    else
                    {
                        _data.C.Tb_Description = value.Trim();
                    }
                    //else if ((_data.C.Tb_Description == null || _data.C.Tb_Description.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_Description == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_Description == null || _data.C.Tb_Description.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_Description = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_Description = value;
                    //}
                    CustomPropertySetterCode("Tb_Description");
                    Tb_Description_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_Description, _data.C.Tb_Description);
                        
					Notify("Tb_Description");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Description - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Description - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_DevelopmentNote
        {
            get
            {
                return _data.C.Tb_DevelopmentNote;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DevelopmentNote - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_DevelopmentNote == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_DevelopmentNote == null || _data.C.Tb_DevelopmentNote.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_DevelopmentNote == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_DevelopmentNote = null;
                    }
                    else
                    {
                        _data.C.Tb_DevelopmentNote = value.Trim();
                    }
                    //else if ((_data.C.Tb_DevelopmentNote == null || _data.C.Tb_DevelopmentNote.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_DevelopmentNote == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_DevelopmentNote == null || _data.C.Tb_DevelopmentNote.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_DevelopmentNote = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_DevelopmentNote = value;
                    //}
                    CustomPropertySetterCode("Tb_DevelopmentNote");
                    Tb_DevelopmentNote_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_DevelopmentNote, _data.C.Tb_DevelopmentNote);
                        
					Notify("Tb_DevelopmentNote");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DevelopmentNote - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DevelopmentNote - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_UIAssemblyName
        {
            get
            {
                return _data.C.Tb_UIAssemblyName;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyName - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_UIAssemblyName == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_UIAssemblyName == null || _data.C.Tb_UIAssemblyName.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_UIAssemblyName == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_UIAssemblyName = null;
                    }
                    else
                    {
                        _data.C.Tb_UIAssemblyName = value.Trim();
                    }
                    //else if ((_data.C.Tb_UIAssemblyName == null || _data.C.Tb_UIAssemblyName.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_UIAssemblyName == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_UIAssemblyName == null || _data.C.Tb_UIAssemblyName.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_UIAssemblyName = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_UIAssemblyName = value;
                    //}
                    CustomPropertySetterCode("Tb_UIAssemblyName");
                    Tb_UIAssemblyName_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_UIAssemblyName, _data.C.Tb_UIAssemblyName);
                        
					Notify("Tb_UIAssemblyName");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyName - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyName - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_UINamespace
        {
            get
            {
                return _data.C.Tb_UINamespace;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UINamespace - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_UINamespace == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_UINamespace == null || _data.C.Tb_UINamespace.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_UINamespace == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_UINamespace = null;
                    }
                    else
                    {
                        _data.C.Tb_UINamespace = value.Trim();
                    }
                    //else if ((_data.C.Tb_UINamespace == null || _data.C.Tb_UINamespace.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_UINamespace == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_UINamespace == null || _data.C.Tb_UINamespace.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_UINamespace = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_UINamespace = value;
                    //}
                    CustomPropertySetterCode("Tb_UINamespace");
                    Tb_UINamespace_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_UINamespace, _data.C.Tb_UINamespace);
                        
					Notify("Tb_UINamespace");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UINamespace - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UINamespace - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_UIAssemblyPath
        {
            get
            {
                return _data.C.Tb_UIAssemblyPath;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyPath - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_UIAssemblyPath == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_UIAssemblyPath == null || _data.C.Tb_UIAssemblyPath.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_UIAssemblyPath == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_UIAssemblyPath = null;
                    }
                    else
                    {
                        _data.C.Tb_UIAssemblyPath = value.Trim();
                    }
                    //else if ((_data.C.Tb_UIAssemblyPath == null || _data.C.Tb_UIAssemblyPath.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_UIAssemblyPath == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_UIAssemblyPath == null || _data.C.Tb_UIAssemblyPath.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_UIAssemblyPath = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_UIAssemblyPath = value;
                    //}
                    CustomPropertySetterCode("Tb_UIAssemblyPath");
                    Tb_UIAssemblyPath_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_UIAssemblyPath, _data.C.Tb_UIAssemblyPath);
                        
					Notify("Tb_UIAssemblyPath");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyPath - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyPath - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_ProxyAssemblyName
        {
            get
            {
                return _data.C.Tb_ProxyAssemblyName;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ProxyAssemblyName - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_ProxyAssemblyName == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_ProxyAssemblyName == null || _data.C.Tb_ProxyAssemblyName.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_ProxyAssemblyName == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_ProxyAssemblyName = null;
                    }
                    else
                    {
                        _data.C.Tb_ProxyAssemblyName = value.Trim();
                    }
                    //else if ((_data.C.Tb_ProxyAssemblyName == null || _data.C.Tb_ProxyAssemblyName.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_ProxyAssemblyName == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_ProxyAssemblyName == null || _data.C.Tb_ProxyAssemblyName.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_ProxyAssemblyName = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_ProxyAssemblyName = value;
                    //}
                    CustomPropertySetterCode("Tb_ProxyAssemblyName");
                    Tb_ProxyAssemblyName_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_ProxyAssemblyName, _data.C.Tb_ProxyAssemblyName);
                        
					Notify("Tb_ProxyAssemblyName");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ProxyAssemblyName - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ProxyAssemblyName - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_ProxyAssemblyPath
        {
            get
            {
                return _data.C.Tb_ProxyAssemblyPath;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ProxyAssemblyPath - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_ProxyAssemblyPath == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_ProxyAssemblyPath == null || _data.C.Tb_ProxyAssemblyPath.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_ProxyAssemblyPath == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_ProxyAssemblyPath = null;
                    }
                    else
                    {
                        _data.C.Tb_ProxyAssemblyPath = value.Trim();
                    }
                    //else if ((_data.C.Tb_ProxyAssemblyPath == null || _data.C.Tb_ProxyAssemblyPath.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_ProxyAssemblyPath == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_ProxyAssemblyPath == null || _data.C.Tb_ProxyAssemblyPath.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_ProxyAssemblyPath = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_ProxyAssemblyPath = value;
                    //}
                    CustomPropertySetterCode("Tb_ProxyAssemblyPath");
                    Tb_ProxyAssemblyPath_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_ProxyAssemblyPath, _data.C.Tb_ProxyAssemblyPath);
                        
					Notify("Tb_ProxyAssemblyPath");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ProxyAssemblyPath - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ProxyAssemblyPath - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_WebServiceName
        {
            get
            {
                return _data.C.Tb_WebServiceName;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceName - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_WebServiceName == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_WebServiceName == null || _data.C.Tb_WebServiceName.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_WebServiceName == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_WebServiceName = null;
                    }
                    else
                    {
                        _data.C.Tb_WebServiceName = value.Trim();
                    }
                    //else if ((_data.C.Tb_WebServiceName == null || _data.C.Tb_WebServiceName.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_WebServiceName == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_WebServiceName == null || _data.C.Tb_WebServiceName.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_WebServiceName = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_WebServiceName = value;
                    //}
                    CustomPropertySetterCode("Tb_WebServiceName");
                    Tb_WebServiceName_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_WebServiceName, _data.C.Tb_WebServiceName);
                        
					Notify("Tb_WebServiceName");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceName - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceName - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_WebServiceFolder
        {
            get
            {
                return _data.C.Tb_WebServiceFolder;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceFolder - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_WebServiceFolder == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_WebServiceFolder == null || _data.C.Tb_WebServiceFolder.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_WebServiceFolder == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_WebServiceFolder = null;
                    }
                    else
                    {
                        _data.C.Tb_WebServiceFolder = value.Trim();
                    }
                    //else if ((_data.C.Tb_WebServiceFolder == null || _data.C.Tb_WebServiceFolder.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_WebServiceFolder == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_WebServiceFolder == null || _data.C.Tb_WebServiceFolder.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_WebServiceFolder = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_WebServiceFolder = value;
                    //}
                    CustomPropertySetterCode("Tb_WebServiceFolder");
                    Tb_WebServiceFolder_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_WebServiceFolder, _data.C.Tb_WebServiceFolder);
                        
					Notify("Tb_WebServiceFolder");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceFolder - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceFolder - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_DataServiceAssemblyName
        {
            get
            {
                return _data.C.Tb_DataServiceAssemblyName;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServiceAssemblyName - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_DataServiceAssemblyName == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_DataServiceAssemblyName == null || _data.C.Tb_DataServiceAssemblyName.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_DataServiceAssemblyName == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_DataServiceAssemblyName = null;
                    }
                    else
                    {
                        _data.C.Tb_DataServiceAssemblyName = value.Trim();
                    }
                    //else if ((_data.C.Tb_DataServiceAssemblyName == null || _data.C.Tb_DataServiceAssemblyName.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_DataServiceAssemblyName == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_DataServiceAssemblyName == null || _data.C.Tb_DataServiceAssemblyName.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_DataServiceAssemblyName = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_DataServiceAssemblyName = value;
                    //}
                    CustomPropertySetterCode("Tb_DataServiceAssemblyName");
                    Tb_DataServiceAssemblyName_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_DataServiceAssemblyName, _data.C.Tb_DataServiceAssemblyName);
                        
					Notify("Tb_DataServiceAssemblyName");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServiceAssemblyName - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServiceAssemblyName - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_DataServicePath
        {
            get
            {
                return _data.C.Tb_DataServicePath;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServicePath - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_DataServicePath == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_DataServicePath == null || _data.C.Tb_DataServicePath.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_DataServicePath == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_DataServicePath = null;
                    }
                    else
                    {
                        _data.C.Tb_DataServicePath = value.Trim();
                    }
                    //else if ((_data.C.Tb_DataServicePath == null || _data.C.Tb_DataServicePath.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_DataServicePath == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_DataServicePath == null || _data.C.Tb_DataServicePath.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_DataServicePath = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_DataServicePath = value;
                    //}
                    CustomPropertySetterCode("Tb_DataServicePath");
                    Tb_DataServicePath_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_DataServicePath, _data.C.Tb_DataServicePath);
                        
					Notify("Tb_DataServicePath");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServicePath - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServicePath - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_WireTypeAssemblyName
        {
            get
            {
                return _data.C.Tb_WireTypeAssemblyName;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypeAssemblyName - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_WireTypeAssemblyName == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_WireTypeAssemblyName == null || _data.C.Tb_WireTypeAssemblyName.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_WireTypeAssemblyName == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_WireTypeAssemblyName = null;
                    }
                    else
                    {
                        _data.C.Tb_WireTypeAssemblyName = value.Trim();
                    }
                    //else if ((_data.C.Tb_WireTypeAssemblyName == null || _data.C.Tb_WireTypeAssemblyName.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_WireTypeAssemblyName == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_WireTypeAssemblyName == null || _data.C.Tb_WireTypeAssemblyName.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_WireTypeAssemblyName = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_WireTypeAssemblyName = value;
                    //}
                    CustomPropertySetterCode("Tb_WireTypeAssemblyName");
                    Tb_WireTypeAssemblyName_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_WireTypeAssemblyName, _data.C.Tb_WireTypeAssemblyName);
                        
					Notify("Tb_WireTypeAssemblyName");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypeAssemblyName - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypeAssemblyName - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_WireTypePath
        {
            get
            {
                return _data.C.Tb_WireTypePath;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypePath - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_WireTypePath == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tb_WireTypePath == null || _data.C.Tb_WireTypePath.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tb_WireTypePath == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tb_WireTypePath = null;
                    }
                    else
                    {
                        _data.C.Tb_WireTypePath = value.Trim();
                    }
                    //else if ((_data.C.Tb_WireTypePath == null || _data.C.Tb_WireTypePath.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tb_WireTypePath == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tb_WireTypePath == null || _data.C.Tb_WireTypePath.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tb_WireTypePath = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tb_WireTypePath = value;
                    //}
                    CustomPropertySetterCode("Tb_WireTypePath");
                    Tb_WireTypePath_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tb_WireTypePath, _data.C.Tb_WireTypePath);
                        
					Notify("Tb_WireTypePath");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypePath - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypePath - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_UseTilesInPropsScreen
        {
            get
            {
                return _data.C.Tb_UseTilesInPropsScreen;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseTilesInPropsScreen - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_UseTilesInPropsScreen == value) { return; }
                    _data.C.Tb_UseTilesInPropsScreen = value;
                    CustomPropertySetterCode("Tb_UseTilesInPropsScreen");
                    Tb_UseTilesInPropsScreen_Validate();
                    CheckEntityState(es);
                    Notify("Tb_UseTilesInPropsScreen");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseTilesInPropsScreen - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseTilesInPropsScreen - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_UseGridColumnGroups
        {
            get
            {
                return _data.C.Tb_UseGridColumnGroups;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseGridColumnGroups - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_UseGridColumnGroups == value) { return; }
                    _data.C.Tb_UseGridColumnGroups = value;
                    CustomPropertySetterCode("Tb_UseGridColumnGroups");
                    Tb_UseGridColumnGroups_Validate();
                    CheckEntityState(es);
                    Notify("Tb_UseGridColumnGroups");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseGridColumnGroups - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseGridColumnGroups - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_UseGridDataSourceCombo
        {
            get
            {
                return _data.C.Tb_UseGridDataSourceCombo;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseGridDataSourceCombo - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_UseGridDataSourceCombo == value) { return; }
                    _data.C.Tb_UseGridDataSourceCombo = value;
                    CustomPropertySetterCode("Tb_UseGridDataSourceCombo");
                    Tb_UseGridDataSourceCombo_Validate();
                    CheckEntityState(es);
                    Notify("Tb_UseGridDataSourceCombo");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseGridDataSourceCombo - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseGridDataSourceCombo - Setter", IfxTraceCategory.Leave);
                }
            }
        }


        public String Tb_ApCnSK_Id_TextField
        {
            get
            {
                return _data.C.Tb_ApCnSK_Id_TextField;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ApCnSK_Id Setter", IfxTraceCategory.Enter);
                    if (_data.C.Tb_ApCnSK_Id_TextField == value) { return; }
                    // If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    if ((_data.C.Tb_ApCnSK_Id_TextField == null || _data.C.Tb_ApCnSK_Id_TextField.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    {
                        return;
                    }
                    if (value != null)
                    {
                        _data.C.Tb_ApCnSK_Id_TextField = value.Trim();
                    }
                    else
                    {
                        _data.C.Tb_ApCnSK_Id_TextField = value;
                    }
                    CustomPropertySetterCode("Tb_ApCnSK_Id_TextField");
                    Notify("Tb_ApCnSK_Id_TextField");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ApCnSK_Id Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ApCnSK_Id Setter", IfxTraceCategory.Leave);
                }
            }
        }
		
        public Guid? Tb_ApCnSK_Id
        {
            get
            {
                return _data.C.Tb_ApCnSK_Id;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ApCnSK_Id - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_ApCnSK_Id == value) { return; }
                    _data.C.Tb_ApCnSK_Id = value;
                    CustomPropertySetterCode("Tb_ApCnSK_Id");
                    Tb_ApCnSK_Id_Validate();
                    CheckEntityState(es);
                    Notify("Tb_ApCnSK_Id");

                    // The UI's combo for this prop makes use of an alternate TextField property, so set it's text now.
                    ComboItem obj = WcApplication_Bll_staticLists.WcApplicationConnectionStringKey_ComboItemList_BindingListProperty.FindItemById(_data.C.Tb_ApCnSK_Id);
                    if (obj == null)
                    {
                        Tb_ApCnSK_Id_TextField = "";
                    }
                    else
                    {
                        Tb_ApCnSK_Id_TextField = obj.ItemName;
                    }

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ApCnSK_Id - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ApCnSK_Id - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_UseLegacyConnectionCode
        {
            get
            {
                return _data.C.Tb_UseLegacyConnectionCode;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseLegacyConnectionCode - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_UseLegacyConnectionCode == value) { return; }
                    _data.C.Tb_UseLegacyConnectionCode = value;
                    CustomPropertySetterCode("Tb_UseLegacyConnectionCode");
                    Tb_UseLegacyConnectionCode_Validate();
                    CheckEntityState(es);
                    Notify("Tb_UseLegacyConnectionCode");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseLegacyConnectionCode - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseLegacyConnectionCode - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_PkIsIdentity
        {
            get
            {
                return _data.C.Tb_PkIsIdentity;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_PkIsIdentity - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_PkIsIdentity == value) { return; }
                    _data.C.Tb_PkIsIdentity = value;
                    CustomPropertySetterCode("Tb_PkIsIdentity");
                    Tb_PkIsIdentity_Validate();
                    CheckEntityState(es);
                    Notify("Tb_PkIsIdentity");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_PkIsIdentity - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_PkIsIdentity - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_IsVirtual
        {
            get
            {
                return _data.C.Tb_IsVirtual;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsVirtual - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_IsVirtual == value) { return; }
                    _data.C.Tb_IsVirtual = value;
                    CustomPropertySetterCode("Tb_IsVirtual");
                    Tb_IsVirtual_Validate();
                    CheckEntityState(es);
                    Notify("Tb_IsVirtual");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsVirtual - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsVirtual - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean? Tb_IsScreenPlaceHolder
        {
            get
            {
                return _data.C.Tb_IsScreenPlaceHolder;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsScreenPlaceHolder - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_IsScreenPlaceHolder == value) { return; }
                    _data.C.Tb_IsScreenPlaceHolder = value;
                    CustomPropertySetterCode("Tb_IsScreenPlaceHolder");
                    Tb_IsScreenPlaceHolder_Validate();
                    CheckEntityState(es);
                    Notify("Tb_IsScreenPlaceHolder");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsScreenPlaceHolder - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsScreenPlaceHolder - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_IsNotEntity
        {
            get
            {
                return _data.C.Tb_IsNotEntity;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsNotEntity - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_IsNotEntity == value) { return; }
                    _data.C.Tb_IsNotEntity = value;
                    CustomPropertySetterCode("Tb_IsNotEntity");
                    Tb_IsNotEntity_Validate();
                    CheckEntityState(es);
                    Notify("Tb_IsNotEntity");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsNotEntity - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsNotEntity - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_IsMany2Many
        {
            get
            {
                return _data.C.Tb_IsMany2Many;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsMany2Many - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_IsMany2Many == value) { return; }
                    _data.C.Tb_IsMany2Many = value;
                    CustomPropertySetterCode("Tb_IsMany2Many");
                    Tb_IsMany2Many_Validate();
                    CheckEntityState(es);
                    Notify("Tb_IsMany2Many");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsMany2Many - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsMany2Many - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_UseLastModifiedByUserNameInSproc
        {
            get
            {
                return _data.C.Tb_UseLastModifiedByUserNameInSproc;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseLastModifiedByUserNameInSproc - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_UseLastModifiedByUserNameInSproc == value) { return; }
                    _data.C.Tb_UseLastModifiedByUserNameInSproc = value;
                    CustomPropertySetterCode("Tb_UseLastModifiedByUserNameInSproc");
                    Tb_UseLastModifiedByUserNameInSproc_Validate();
                    CheckEntityState(es);
                    Notify("Tb_UseLastModifiedByUserNameInSproc");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseLastModifiedByUserNameInSproc - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseLastModifiedByUserNameInSproc - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_UseUserTimeStamp
        {
            get
            {
                return _data.C.Tb_UseUserTimeStamp;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseUserTimeStamp - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_UseUserTimeStamp == value) { return; }
                    _data.C.Tb_UseUserTimeStamp = value;
                    CustomPropertySetterCode("Tb_UseUserTimeStamp");
                    Tb_UseUserTimeStamp_Validate();
                    CheckEntityState(es);
                    Notify("Tb_UseUserTimeStamp");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseUserTimeStamp - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseUserTimeStamp - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_UseForAudit
        {
            get
            {
                return _data.C.Tb_UseForAudit;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseForAudit - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_UseForAudit == value) { return; }
                    _data.C.Tb_UseForAudit = value;
                    CustomPropertySetterCode("Tb_UseForAudit");
                    Tb_UseForAudit_Validate();
                    CheckEntityState(es);
                    Notify("Tb_UseForAudit");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseForAudit - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UseForAudit - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean? Tb_IsAllowDelete
        {
            get
            {
                return _data.C.Tb_IsAllowDelete;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsAllowDelete - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_IsAllowDelete == value) { return; }
                    _data.C.Tb_IsAllowDelete = value;
                    CustomPropertySetterCode("Tb_IsAllowDelete");
                    Tb_IsAllowDelete_Validate();
                    CheckEntityState(es);
                    Notify("Tb_IsAllowDelete");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsAllowDelete - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsAllowDelete - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_CnfgGdMnu_MenuRow_IsVisible
        {
            get
            {
                return _data.C.Tb_CnfgGdMnu_MenuRow_IsVisible;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_MenuRow_IsVisible - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_CnfgGdMnu_MenuRow_IsVisible == value) { return; }
                    _data.C.Tb_CnfgGdMnu_MenuRow_IsVisible = value;
                    CustomPropertySetterCode("Tb_CnfgGdMnu_MenuRow_IsVisible");
                    Tb_CnfgGdMnu_MenuRow_IsVisible_Validate();
                    CheckEntityState(es);
                    Notify("Tb_CnfgGdMnu_MenuRow_IsVisible");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_MenuRow_IsVisible - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_MenuRow_IsVisible - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_CnfgGdMnu_GridTools_IsVisible
        {
            get
            {
                return _data.C.Tb_CnfgGdMnu_GridTools_IsVisible;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_GridTools_IsVisible - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_CnfgGdMnu_GridTools_IsVisible == value) { return; }
                    _data.C.Tb_CnfgGdMnu_GridTools_IsVisible = value;
                    CustomPropertySetterCode("Tb_CnfgGdMnu_GridTools_IsVisible");
                    Tb_CnfgGdMnu_GridTools_IsVisible_Validate();
                    CheckEntityState(es);
                    Notify("Tb_CnfgGdMnu_GridTools_IsVisible");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_GridTools_IsVisible - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_GridTools_IsVisible - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_CnfgGdMnu_SplitScreen_IsVisible
        {
            get
            {
                return _data.C.Tb_CnfgGdMnu_SplitScreen_IsVisible;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_SplitScreen_IsVisible - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_CnfgGdMnu_SplitScreen_IsVisible == value) { return; }
                    _data.C.Tb_CnfgGdMnu_SplitScreen_IsVisible = value;
                    CustomPropertySetterCode("Tb_CnfgGdMnu_SplitScreen_IsVisible");
                    Tb_CnfgGdMnu_SplitScreen_IsVisible_Validate();
                    CheckEntityState(es);
                    Notify("Tb_CnfgGdMnu_SplitScreen_IsVisible");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_SplitScreen_IsVisible - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_SplitScreen_IsVisible - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
        {
            get
            {
                return _data.C.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_SplitScreen_IsSplit_Default - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default == value) { return; }
                    _data.C.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default = value;
                    CustomPropertySetterCode("Tb_CnfgGdMnu_SplitScreen_IsSplit_Default");
                    Tb_CnfgGdMnu_SplitScreen_IsSplit_Default_Validate();
                    CheckEntityState(es);
                    Notify("Tb_CnfgGdMnu_SplitScreen_IsSplit_Default");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_SplitScreen_IsSplit_Default - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_SplitScreen_IsSplit_Default - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="Tb_CnfgGdMnu_NavColumnWidth">Tb_CnfgGdMnu_NavColumnWidth</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the Tb_CnfgGdMnu_NavColumnWidth field. If it’s not numeric, then Tb_CnfgGdMnu_NavColumnWidth is set to
        ///     null.</para>
        /// </summary>
        public String Tb_CnfgGdMnu_NavColumnWidth_asString
        {
            get
            {
                return _data.C.Tb_CnfgGdMnu_NavColumnWidth.ToString();
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_NavColumnWidth - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
					if (BLLHelper.IsNumber(value))
                    {
                        try
                        {
							Double newValNotNull;
                            Double.TryParse(Convert.ToString(value), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.CurrentInfo, out newValNotNull);
                            Tb_CnfgGdMnu_NavColumnWidth = (Int32)newValNotNull;
                        }
                        catch (FormatException e)
                        {
                            Tb_CnfgGdMnu_NavColumnWidth = 0;
                        }
                    }
                    else
                    {
                        Tb_CnfgGdMnu_NavColumnWidth = 0;
                    }
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_NavColumnWidth - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_NavColumnWidth - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Int32 Tb_CnfgGdMnu_NavColumnWidth
        {
            get
            {
                return _data.C.Tb_CnfgGdMnu_NavColumnWidth;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_NavColumnWidth - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_CnfgGdMnu_NavColumnWidth == value) { return; }
                    _data.C.Tb_CnfgGdMnu_NavColumnWidth = value;
                    CustomPropertySetterCode("Tb_CnfgGdMnu_NavColumnWidth");
                    Tb_CnfgGdMnu_NavColumnWidth_Validate();
                    CheckEntityState(es);
                    Notify("Tb_CnfgGdMnu_NavColumnWidth");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_NavColumnWidth - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_NavColumnWidth - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_CnfgGdMnu_IsReadOnly
        {
            get
            {
                return _data.C.Tb_CnfgGdMnu_IsReadOnly;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_IsReadOnly - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_CnfgGdMnu_IsReadOnly == value) { return; }
                    _data.C.Tb_CnfgGdMnu_IsReadOnly = value;
                    CustomPropertySetterCode("Tb_CnfgGdMnu_IsReadOnly");
                    Tb_CnfgGdMnu_IsReadOnly_Validate();
                    CheckEntityState(es);
                    Notify("Tb_CnfgGdMnu_IsReadOnly");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_IsReadOnly - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_IsReadOnly - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_CnfgGdMnu_IsAllowNewRow
        {
            get
            {
                return _data.C.Tb_CnfgGdMnu_IsAllowNewRow;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_IsAllowNewRow - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_CnfgGdMnu_IsAllowNewRow == value) { return; }
                    _data.C.Tb_CnfgGdMnu_IsAllowNewRow = value;
                    CustomPropertySetterCode("Tb_CnfgGdMnu_IsAllowNewRow");
                    Tb_CnfgGdMnu_IsAllowNewRow_Validate();
                    CheckEntityState(es);
                    Notify("Tb_CnfgGdMnu_IsAllowNewRow");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_IsAllowNewRow - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_IsAllowNewRow - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_CnfgGdMnu_ExcelExport_IsVisible
        {
            get
            {
                return _data.C.Tb_CnfgGdMnu_ExcelExport_IsVisible;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_ExcelExport_IsVisible - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_CnfgGdMnu_ExcelExport_IsVisible == value) { return; }
                    _data.C.Tb_CnfgGdMnu_ExcelExport_IsVisible = value;
                    CustomPropertySetterCode("Tb_CnfgGdMnu_ExcelExport_IsVisible");
                    Tb_CnfgGdMnu_ExcelExport_IsVisible_Validate();
                    CheckEntityState(es);
                    Notify("Tb_CnfgGdMnu_ExcelExport_IsVisible");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_ExcelExport_IsVisible - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_ExcelExport_IsVisible - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_CnfgGdMnu_ColumnChooser_IsVisible
        {
            get
            {
                return _data.C.Tb_CnfgGdMnu_ColumnChooser_IsVisible;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_ColumnChooser_IsVisible - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_CnfgGdMnu_ColumnChooser_IsVisible == value) { return; }
                    _data.C.Tb_CnfgGdMnu_ColumnChooser_IsVisible = value;
                    CustomPropertySetterCode("Tb_CnfgGdMnu_ColumnChooser_IsVisible");
                    Tb_CnfgGdMnu_ColumnChooser_IsVisible_Validate();
                    CheckEntityState(es);
                    Notify("Tb_CnfgGdMnu_ColumnChooser_IsVisible");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_ColumnChooser_IsVisible - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_ColumnChooser_IsVisible - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
        {
            get
            {
                return _data.C.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_ShowHideColBtn_IsVisible - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible == value) { return; }
                    _data.C.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible = value;
                    CustomPropertySetterCode("Tb_CnfgGdMnu_ShowHideColBtn_IsVisible");
                    Tb_CnfgGdMnu_ShowHideColBtn_IsVisible_Validate();
                    CheckEntityState(es);
                    Notify("Tb_CnfgGdMnu_ShowHideColBtn_IsVisible");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_ShowHideColBtn_IsVisible - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_ShowHideColBtn_IsVisible - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_CnfgGdMnu_RefreshGrid_IsVisible
        {
            get
            {
                return _data.C.Tb_CnfgGdMnu_RefreshGrid_IsVisible;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_RefreshGrid_IsVisible - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_CnfgGdMnu_RefreshGrid_IsVisible == value) { return; }
                    _data.C.Tb_CnfgGdMnu_RefreshGrid_IsVisible = value;
                    CustomPropertySetterCode("Tb_CnfgGdMnu_RefreshGrid_IsVisible");
                    Tb_CnfgGdMnu_RefreshGrid_IsVisible_Validate();
                    CheckEntityState(es);
                    Notify("Tb_CnfgGdMnu_RefreshGrid_IsVisible");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_RefreshGrid_IsVisible - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_RefreshGrid_IsVisible - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
        {
            get
            {
                return _data.C.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible == value) { return; }
                    _data.C.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible = value;
                    CustomPropertySetterCode("Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible");
                    Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible_Validate();
                    CheckEntityState(es);
                    Notify("Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_IsInputComplete
        {
            get
            {
                return _data.C.Tb_IsInputComplete;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsInputComplete - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_IsInputComplete == value) { return; }
                    _data.C.Tb_IsInputComplete = value;
                    CustomPropertySetterCode("Tb_IsInputComplete");
                    Tb_IsInputComplete_Validate();
                    CheckEntityState(es);
                    Notify("Tb_IsInputComplete");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsInputComplete - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsInputComplete - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_IsCodeGen
        {
            get
            {
                return _data.C.Tb_IsCodeGen;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGen - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_IsCodeGen == value) { return; }
                    _data.C.Tb_IsCodeGen = value;
                    CustomPropertySetterCode("Tb_IsCodeGen");
                    Tb_IsCodeGen_Validate();
                    CheckEntityState(es);
                    Notify("Tb_IsCodeGen");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGen - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGen - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_IsReadyCodeGen
        {
            get
            {
                return _data.C.Tb_IsReadyCodeGen;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsReadyCodeGen - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_IsReadyCodeGen == value) { return; }
                    _data.C.Tb_IsReadyCodeGen = value;
                    CustomPropertySetterCode("Tb_IsReadyCodeGen");
                    Tb_IsReadyCodeGen_Validate();
                    CheckEntityState(es);
                    Notify("Tb_IsReadyCodeGen");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsReadyCodeGen - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsReadyCodeGen - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_IsCodeGenComplete
        {
            get
            {
                return _data.C.Tb_IsCodeGenComplete;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGenComplete - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_IsCodeGenComplete == value) { return; }
                    _data.C.Tb_IsCodeGenComplete = value;
                    CustomPropertySetterCode("Tb_IsCodeGenComplete");
                    Tb_IsCodeGenComplete_Validate();
                    CheckEntityState(es);
                    Notify("Tb_IsCodeGenComplete");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGenComplete - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGenComplete - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_IsTagForCodeGen
        {
            get
            {
                return _data.C.Tb_IsTagForCodeGen;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsTagForCodeGen - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_IsTagForCodeGen == value) { return; }
                    _data.C.Tb_IsTagForCodeGen = value;
                    CustomPropertySetterCode("Tb_IsTagForCodeGen");
                    Tb_IsTagForCodeGen_Validate();
                    CheckEntityState(es);
                    Notify("Tb_IsTagForCodeGen");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsTagForCodeGen - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsTagForCodeGen - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_IsTagForOther
        {
            get
            {
                return _data.C.Tb_IsTagForOther;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsTagForOther - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_IsTagForOther == value) { return; }
                    _data.C.Tb_IsTagForOther = value;
                    CustomPropertySetterCode("Tb_IsTagForOther");
                    Tb_IsTagForOther_Validate();
                    CheckEntityState(es);
                    Notify("Tb_IsTagForOther");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsTagForOther - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsTagForOther - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tb_TableGroups
        {
            get
            {
                return _data.C.Tb_TableGroups;
            }
        }

		
        public Boolean Tb_IsActiveRow
        {
            get
            {
                return _data.C.Tb_IsActiveRow;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsActiveRow - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tb_IsActiveRow == value) { return; }
                    _data.C.Tb_IsActiveRow = value;
                    CustomPropertySetterCode("Tb_IsActiveRow");
                    Tb_IsActiveRow_Validate();
                    CheckEntityState(es);
                    Notify("Tb_IsActiveRow");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsActiveRow - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsActiveRow - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tb_IsDeleted
        {
            get
            {
                return _data.C.Tb_IsDeleted;
            }
        }

		
        public Guid? Tb_CreatedUserId
        {
            get
            {
                return _data.C.Tb_CreatedUserId;
            }
        }


		/// <summary>
        ///  Use this property when binding in Grids so the Notify method won't prevent you from entering complete dates or the year part of a date.  Notify will refresh the UI cutting off anything after the first diget of the month.
        /// </summary>
        public String Tb_CreatedDate_asString
        {
            get
            {
                if (null == _data.C.Tb_CreatedDate)
                {
                    return "";
                }
                else
                {
                    return _data.C.Tb_CreatedDate.ToString();
                }
            }
        }

		
        public DateTime? Tb_CreatedDate
        {
            get
            {
                return _data.C.Tb_CreatedDate;
            }
        }

		
        public Guid? Tb_UserId
        {
            get
            {
                return _data.C.Tb_UserId;
            }
        }


        public String UserName
        {
            get
            {
                return _data.C.UserName;
            }
        }


		/// <summary>
        ///  Use this property when binding in Grids so the Notify method won't prevent you from entering complete dates or the year part of a date.  Notify will refresh the UI cutting off anything after the first diget of the month.
        /// </summary>
        public String Tb_LastModifiedDate_asString
        {
            get
            {
                if (null == _data.C.Tb_LastModifiedDate)
                {
                    return "";
                }
                else
                {
                    return _data.C.Tb_LastModifiedDate.ToString();
                }
            }
        }

		
        public DateTime? Tb_LastModifiedDate
        {
            get
            {
                return _data.C.Tb_LastModifiedDate;
            }
        }


        public Byte[] Tb_Stamp
        {
            get
            {
                return _data.C.Tb_Stamp;
            }
        }



		#endregion Data Props


		#region Concurrency and Data State


        private bool DeterminConcurrencyCheck(UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", IfxTraceCategory.Enter);
                switch (check)
                {
                    case UseConcurrencyCheck.UseDefaultSetting:
                        return true;
                    case UseConcurrencyCheck.UseConcurrencyCheck:
                        return true;
                    case UseConcurrencyCheck.BypassConcurrencyCheck:
                        return false;
                }
                return false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     This is a reference to the current data (<see cref="EntityWireType.WcTable_Values">WcTable_Values</see>) for this business object
        ///     (WcTable_Bll). The Values Manager (<see cref="EntityWireType.WcTable_ValuesMngr">WcTable_ValuesMngr</see>) has three sets of WcTable
        ///     data used for managing state and concurrency. For more information about the other
        ///     two sets of data, <see cref="Original">Original</see> and <see cref="Concurrency">Concurrency</see>, and how they are used, see the overviews of
        ///     <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Concurrency_Overview.html">
        ///     Concurrency</a> and <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Wire_Object_Overview.html">
        ///     Wire Objects</a>.
        /// </summary>
        /// <seealso cref="Original">Original Property</seealso>
        /// <seealso cref="Concurrency">Concurrency Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcTable_Values Current
        {
            get
            {
                return _data.C;
            }
            set
            {
                //Management Code
                _data.C = value;
            }
        }

        /// <summary>
        ///     This is a reference to the original data (<see cref="EntityWireType.WcTable_Values">WcTable_Values</see>) for this business object
        ///     (WcTable_Bll) which was either data for a ‘new-not dirty’ WcTable or data for a WcTable
        ///     retrieved from the data store. The Values Manager (<see cref="EntityWireType.WcTable_ValuesMngr">WcTable_ValuesMngr</see>) has three sets of WcTable
        ///     data used for managing state and concurrency. For more information about the other
        ///     two sets of data, <see cref="Current">Current</see> and <see cref="Concurrency">Concurrency</see>, and how they are used, see the overview of
        ///     Concurrency and Wire Objects.
        /// </summary>
        /// <seealso cref="Current">Current Property</seealso>
        /// <seealso cref="Concurrency">Concurrency Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcTable_Values Original
        {
            get
            {
                return _data.O;
            }
            set
            {
                //Management Code
                _data.O = value;
            }
        }

        /// <summary>*** Need to review before documenting ***</summary>
        /// <seealso cref="Current">Current Property</seealso>
        /// <seealso cref="Original">Original Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcTable_Values Concurrency
        {
            get
            {
                return _data.X;
            }
            set
            {
                //Management Code
                _data.X = value;
            }
        }

        /// <value>
        ///     This class manages the <see cref="TypeServices.EntityState">EntityStateSwitch</see>
        ///     and is used to describe the current <see cref="TypeServices.EntityState">state</see> of this business object such as IsDirty,
        ///     IsValid and IsNew. The combinations of these six possible values describe the
        ///     entity state and are used for logic in configuring settings in the business object
        ///     as well as the UI.
        /// </value>
        /// <seealso cref="EntityWireType.WcTable_ValuesMngr">WcTable_ValuesMngr Class</seealso>
        [Browsable(false)]
        public override EntityState State
        {
            get
            {
                return _data.S;
            }
            set
            {
                //Management Code
                _data.S = value;
            }
        }

        /// <summary>
        ///     This is the combination of IsDirty, IsValid and IsNew and is managed by the
        ///     <see cref="TypeServices.EntityState">EntityState</see> class. The combinations of
        ///     these six possible values describe the entity <see cref="TypeServices.EntityState">state</see> and are used for logic in configuring
        ///     settings in the business object as well as the UI.
        /// </summary>
        /// <seealso cref="EntityWireType.WcTable_ValuesMngr">WcTable_ValuesMngr Class</seealso>
        public EntityStateSwitch StateSwitch
        {
            get
            {
                return _data.S.Switch;
            }
        }




		#endregion Concurrency and Data State

        [Browsable(false)]
        public override object this[string name]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                //Management Code
                throw new NotImplementedException();
            }
        }


		#region ITraceItem Members


        /// <summary>
        /// 	<para>
        ///         Used in IFX (the instrumentation framework) for tracing. This returns the short
        ///         version of the <see cref="ManagementServices.TraceItemList">TraceItemList</see>. The ‘Short Version’
        ///         returns only the entity’s data fields, <see cref="TypeServices.BrokenRuleManager.GetBrokenRuleListForEntity">BrokenRules</see>
        ///         and <see cref="TypeServices.EntityStateSwitch">EntityStateSwitch</see>.
        ///         However, it also calls the <see cref="GetTraceItemsShortListCustom">GetTraceItemsShortListCustom</see> method in the
        ///         WcTable.bll.cust.cs code file so a developer can customize the list by adding
        ///         additional items to it.
        ///     </para>
        /// 	<para>
        ///         Keep in mind that this is the short versions so don’t get carried away adding
        ///         to many things to it. For a robust and extensive list of items to record in the
        ///         trace, use the <see cref="GetTraceItemsLongList">GetTraceItemsLongList</see>
        ///         and <see cref="GetTraceItemsLongListCustom">GetTraceItemsLongListCustom</see>
        ///         methods.
        ///     </para>
        /// </summary>
        public TraceItemList GetTraceItemsShortList()
        {
            if (null == _traceItems)
            {
                _traceItems = new TraceItemList("WcTable_Bll");
            }
            else
            {
                _traceItems.Clear();
            }
            _traceItems.Add("StateSwitch", StateSwitch.ToString(), TraceDataTypes.String);
            _traceItems.Add("Broken Rules", BrokenRuleManagerProperty.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_Id", _data.C.Tb_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tb_ApVrsn_Id", _data.C.Tb_ApVrsn_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tb_auditTable_Id", _data.C.Tb_auditTable_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("AttachmentCount", _data.C.AttachmentCount.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("AttachmentFileNames", _data.C.AttachmentFileNames.ToString(), TraceDataTypes.String);
			_traceItems.Add("DiscussionCount", _data.C.DiscussionCount.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("DiscussionTitles", _data.C.DiscussionTitles.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_Name", _data.C.Tb_Name.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_EntityRootName", _data.C.Tb_EntityRootName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_VariableName", _data.C.Tb_VariableName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_ScreenCaption", _data.C.Tb_ScreenCaption.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_Description", _data.C.Tb_Description.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_DevelopmentNote", _data.C.Tb_DevelopmentNote.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_UIAssemblyName", _data.C.Tb_UIAssemblyName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_UINamespace", _data.C.Tb_UINamespace.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_UIAssemblyPath", _data.C.Tb_UIAssemblyPath.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_ProxyAssemblyName", _data.C.Tb_ProxyAssemblyName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_ProxyAssemblyPath", _data.C.Tb_ProxyAssemblyPath.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_WebServiceName", _data.C.Tb_WebServiceName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_WebServiceFolder", _data.C.Tb_WebServiceFolder.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_DataServiceAssemblyName", _data.C.Tb_DataServiceAssemblyName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_DataServicePath", _data.C.Tb_DataServicePath.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_WireTypeAssemblyName", _data.C.Tb_WireTypeAssemblyName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_WireTypePath", _data.C.Tb_WireTypePath.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_UseTilesInPropsScreen", _data.C.Tb_UseTilesInPropsScreen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_UseGridColumnGroups", _data.C.Tb_UseGridColumnGroups.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_UseGridDataSourceCombo", _data.C.Tb_UseGridDataSourceCombo.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_ApCnSK_Id", _data.C.Tb_ApCnSK_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tb_ApCnSK_Id_TextField", _data.C.Tb_ApCnSK_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_UseLegacyConnectionCode", _data.C.Tb_UseLegacyConnectionCode.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_PkIsIdentity", _data.C.Tb_PkIsIdentity.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsVirtual", _data.C.Tb_IsVirtual.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsScreenPlaceHolder", _data.C.Tb_IsScreenPlaceHolder.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsNotEntity", _data.C.Tb_IsNotEntity.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsMany2Many", _data.C.Tb_IsMany2Many.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_UseLastModifiedByUserNameInSproc", _data.C.Tb_UseLastModifiedByUserNameInSproc.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_UseUserTimeStamp", _data.C.Tb_UseUserTimeStamp.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_UseForAudit", _data.C.Tb_UseForAudit.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsAllowDelete", _data.C.Tb_IsAllowDelete.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_MenuRow_IsVisible", _data.C.Tb_CnfgGdMnu_MenuRow_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_GridTools_IsVisible", _data.C.Tb_CnfgGdMnu_GridTools_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_SplitScreen_IsVisible", _data.C.Tb_CnfgGdMnu_SplitScreen_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_SplitScreen_IsSplit_Default", _data.C.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_NavColumnWidth", _data.C.Tb_CnfgGdMnu_NavColumnWidth.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("Tb_CnfgGdMnu_IsReadOnly", _data.C.Tb_CnfgGdMnu_IsReadOnly.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_IsAllowNewRow", _data.C.Tb_CnfgGdMnu_IsAllowNewRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_ExcelExport_IsVisible", _data.C.Tb_CnfgGdMnu_ExcelExport_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_ColumnChooser_IsVisible", _data.C.Tb_CnfgGdMnu_ColumnChooser_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_ShowHideColBtn_IsVisible", _data.C.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_RefreshGrid_IsVisible", _data.C.Tb_CnfgGdMnu_RefreshGrid_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible", _data.C.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsInputComplete", _data.C.Tb_IsInputComplete.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsCodeGen", _data.C.Tb_IsCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsReadyCodeGen", _data.C.Tb_IsReadyCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsCodeGenComplete", _data.C.Tb_IsCodeGenComplete.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsTagForCodeGen", _data.C.Tb_IsTagForCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsTagForOther", _data.C.Tb_IsTagForOther.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_TableGroups", _data.C.Tb_TableGroups.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_IsActiveRow", _data.C.Tb_IsActiveRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsDeleted", _data.C.Tb_IsDeleted.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CreatedUserId", _data.C.Tb_CreatedUserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tb_CreatedDate", _data.C.Tb_CreatedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("Tb_UserId", _data.C.Tb_UserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("UserName", _data.C.UserName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_LastModifiedDate", _data.C.Tb_LastModifiedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("Tb_Stamp", _data.C.Tb_Stamp.ToString(), TraceDataTypes.ByteArray);
			GetTraceItemsShortListCustom();
            return _traceItems;
        }

        /// <summary>
        /// 	<para>
        ///         Used in IFX (the instrumentation framework) for tracing. By default, this is
        ///         code-genned to return the same information as <see cref="GetTraceItemsShortList">GetTraceItemsShortList</see>, but is intended for
        ///         developers to add additional informaiton that would be helpful in a trace. This
        ///         method also calls the <see cref="GetTraceItemsLongListCustom">GetTraceItemsLongListCustom</see> method in the
        ///         WcTable.bll.cust.cs code file so a developer can customize the list by adding
        ///         additional items to it.
        ///     </para>
        /// 	<para>
        ///         Remember: <see cref="GetTraceItemsShortList">GetTraceItemsShortList</see> is
        ///         intended for a limited list of items (the basic items) to trace and
        ///         <strong>GetTraceItemsLongList</strong> is intended for a robust and extensive
        ///         list of items to record in the trace.
        ///     </para>
        /// </summary>
        public TraceItemList GetTraceItemsLongList()
        {
            if (null == _traceItems)
            {
                _traceItems = new TraceItemList("WcTable_Bll");
            }
            else
            {
                _traceItems.Clear();
            }
            _traceItems.Add("StateSwitch", StateSwitch.ToString(), TraceDataTypes.String);
            _traceItems.Add("Broken Rules", BrokenRuleManagerProperty.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_Id", _data.C.Tb_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tb_ApVrsn_Id", _data.C.Tb_ApVrsn_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tb_auditTable_Id", _data.C.Tb_auditTable_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("AttachmentCount", _data.C.AttachmentCount.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("AttachmentFileNames", _data.C.AttachmentFileNames.ToString(), TraceDataTypes.String);
			_traceItems.Add("DiscussionCount", _data.C.DiscussionCount.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("DiscussionTitles", _data.C.DiscussionTitles.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_Name", _data.C.Tb_Name.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_EntityRootName", _data.C.Tb_EntityRootName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_VariableName", _data.C.Tb_VariableName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_ScreenCaption", _data.C.Tb_ScreenCaption.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_Description", _data.C.Tb_Description.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_DevelopmentNote", _data.C.Tb_DevelopmentNote.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_UIAssemblyName", _data.C.Tb_UIAssemblyName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_UINamespace", _data.C.Tb_UINamespace.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_UIAssemblyPath", _data.C.Tb_UIAssemblyPath.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_ProxyAssemblyName", _data.C.Tb_ProxyAssemblyName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_ProxyAssemblyPath", _data.C.Tb_ProxyAssemblyPath.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_WebServiceName", _data.C.Tb_WebServiceName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_WebServiceFolder", _data.C.Tb_WebServiceFolder.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_DataServiceAssemblyName", _data.C.Tb_DataServiceAssemblyName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_DataServicePath", _data.C.Tb_DataServicePath.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_WireTypeAssemblyName", _data.C.Tb_WireTypeAssemblyName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_WireTypePath", _data.C.Tb_WireTypePath.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_UseTilesInPropsScreen", _data.C.Tb_UseTilesInPropsScreen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_UseGridColumnGroups", _data.C.Tb_UseGridColumnGroups.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_UseGridDataSourceCombo", _data.C.Tb_UseGridDataSourceCombo.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_ApCnSK_Id", _data.C.Tb_ApCnSK_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tb_ApCnSK_Id_TextField", _data.C.Tb_ApCnSK_Id_TextField.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_UseLegacyConnectionCode", _data.C.Tb_UseLegacyConnectionCode.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_PkIsIdentity", _data.C.Tb_PkIsIdentity.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsVirtual", _data.C.Tb_IsVirtual.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsScreenPlaceHolder", _data.C.Tb_IsScreenPlaceHolder.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsNotEntity", _data.C.Tb_IsNotEntity.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsMany2Many", _data.C.Tb_IsMany2Many.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_UseLastModifiedByUserNameInSproc", _data.C.Tb_UseLastModifiedByUserNameInSproc.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_UseUserTimeStamp", _data.C.Tb_UseUserTimeStamp.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_UseForAudit", _data.C.Tb_UseForAudit.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsAllowDelete", _data.C.Tb_IsAllowDelete.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_MenuRow_IsVisible", _data.C.Tb_CnfgGdMnu_MenuRow_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_GridTools_IsVisible", _data.C.Tb_CnfgGdMnu_GridTools_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_SplitScreen_IsVisible", _data.C.Tb_CnfgGdMnu_SplitScreen_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_SplitScreen_IsSplit_Default", _data.C.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_NavColumnWidth", _data.C.Tb_CnfgGdMnu_NavColumnWidth.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("Tb_CnfgGdMnu_IsReadOnly", _data.C.Tb_CnfgGdMnu_IsReadOnly.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_IsAllowNewRow", _data.C.Tb_CnfgGdMnu_IsAllowNewRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_ExcelExport_IsVisible", _data.C.Tb_CnfgGdMnu_ExcelExport_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_ColumnChooser_IsVisible", _data.C.Tb_CnfgGdMnu_ColumnChooser_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_ShowHideColBtn_IsVisible", _data.C.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_RefreshGrid_IsVisible", _data.C.Tb_CnfgGdMnu_RefreshGrid_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible", _data.C.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsInputComplete", _data.C.Tb_IsInputComplete.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsCodeGen", _data.C.Tb_IsCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsReadyCodeGen", _data.C.Tb_IsReadyCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsCodeGenComplete", _data.C.Tb_IsCodeGenComplete.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsTagForCodeGen", _data.C.Tb_IsTagForCodeGen.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsTagForOther", _data.C.Tb_IsTagForOther.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_TableGroups", _data.C.Tb_TableGroups.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_IsActiveRow", _data.C.Tb_IsActiveRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_IsDeleted", _data.C.Tb_IsDeleted.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tb_CreatedUserId", _data.C.Tb_CreatedUserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tb_CreatedDate", _data.C.Tb_CreatedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("Tb_UserId", _data.C.Tb_UserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("UserName", _data.C.UserName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tb_LastModifiedDate", _data.C.Tb_LastModifiedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("Tb_Stamp", _data.C.Tb_Stamp.ToString(), TraceDataTypes.ByteArray);
			GetTraceItemsLongListCustom();
            return _traceItems;
        }

        public object[] GetTraceData()
        {
            object[] data = new object[65];
            data[0] = new object[] { "StateSwitch", StateSwitch.ToString() };
            data[1] = new object[] { "BrokenRuleManagerProperty", BrokenRuleManagerProperty.ToString() };
            data[2] = new object[] { "Tb_Id", _data.C.Tb_Id.ToString() };
            data[3] = new object[] { "Tb_ApVrsn_Id", _data.C.Tb_ApVrsn_Id.ToString() };
            data[4] = new object[] { "Tb_auditTable_Id", _data.C.Tb_auditTable_Id == null ? "<Null>" : _data.C.Tb_auditTable_Id.ToString() };
            data[5] = new object[] { "AttachmentCount", _data.C.AttachmentCount == null ? "<Null>" : _data.C.AttachmentCount.ToString() };
            data[6] = new object[] { "AttachmentFileNames", _data.C.AttachmentFileNames == null ? "<Null>" : _data.C.AttachmentFileNames.ToString() };
            data[7] = new object[] { "DiscussionCount", _data.C.DiscussionCount == null ? "<Null>" : _data.C.DiscussionCount.ToString() };
            data[8] = new object[] { "DiscussionTitles", _data.C.DiscussionTitles == null ? "<Null>" : _data.C.DiscussionTitles.ToString() };
            data[9] = new object[] { "Tb_Name", _data.C.Tb_Name == null ? "<Null>" : _data.C.Tb_Name.ToString() };
            data[10] = new object[] { "Tb_EntityRootName", _data.C.Tb_EntityRootName == null ? "<Null>" : _data.C.Tb_EntityRootName.ToString() };
            data[11] = new object[] { "Tb_VariableName", _data.C.Tb_VariableName == null ? "<Null>" : _data.C.Tb_VariableName.ToString() };
            data[12] = new object[] { "Tb_ScreenCaption", _data.C.Tb_ScreenCaption == null ? "<Null>" : _data.C.Tb_ScreenCaption.ToString() };
            data[13] = new object[] { "Tb_Description", _data.C.Tb_Description == null ? "<Null>" : _data.C.Tb_Description.ToString() };
            data[14] = new object[] { "Tb_DevelopmentNote", _data.C.Tb_DevelopmentNote == null ? "<Null>" : _data.C.Tb_DevelopmentNote.ToString() };
            data[15] = new object[] { "Tb_UIAssemblyName", _data.C.Tb_UIAssemblyName == null ? "<Null>" : _data.C.Tb_UIAssemblyName.ToString() };
            data[16] = new object[] { "Tb_UINamespace", _data.C.Tb_UINamespace == null ? "<Null>" : _data.C.Tb_UINamespace.ToString() };
            data[17] = new object[] { "Tb_UIAssemblyPath", _data.C.Tb_UIAssemblyPath == null ? "<Null>" : _data.C.Tb_UIAssemblyPath.ToString() };
            data[18] = new object[] { "Tb_ProxyAssemblyName", _data.C.Tb_ProxyAssemblyName == null ? "<Null>" : _data.C.Tb_ProxyAssemblyName.ToString() };
            data[19] = new object[] { "Tb_ProxyAssemblyPath", _data.C.Tb_ProxyAssemblyPath == null ? "<Null>" : _data.C.Tb_ProxyAssemblyPath.ToString() };
            data[20] = new object[] { "Tb_WebServiceName", _data.C.Tb_WebServiceName == null ? "<Null>" : _data.C.Tb_WebServiceName.ToString() };
            data[21] = new object[] { "Tb_WebServiceFolder", _data.C.Tb_WebServiceFolder == null ? "<Null>" : _data.C.Tb_WebServiceFolder.ToString() };
            data[22] = new object[] { "Tb_DataServiceAssemblyName", _data.C.Tb_DataServiceAssemblyName == null ? "<Null>" : _data.C.Tb_DataServiceAssemblyName.ToString() };
            data[23] = new object[] { "Tb_DataServicePath", _data.C.Tb_DataServicePath == null ? "<Null>" : _data.C.Tb_DataServicePath.ToString() };
            data[24] = new object[] { "Tb_WireTypeAssemblyName", _data.C.Tb_WireTypeAssemblyName == null ? "<Null>" : _data.C.Tb_WireTypeAssemblyName.ToString() };
            data[25] = new object[] { "Tb_WireTypePath", _data.C.Tb_WireTypePath == null ? "<Null>" : _data.C.Tb_WireTypePath.ToString() };
            data[26] = new object[] { "Tb_UseTilesInPropsScreen", _data.C.Tb_UseTilesInPropsScreen.ToString() };
            data[27] = new object[] { "Tb_UseGridColumnGroups", _data.C.Tb_UseGridColumnGroups.ToString() };
            data[28] = new object[] { "Tb_UseGridDataSourceCombo", _data.C.Tb_UseGridDataSourceCombo.ToString() };
            data[29] = new object[] { "Tb_ApCnSK_Id", _data.C.Tb_ApCnSK_Id == null ? "<Null>" : _data.C.Tb_ApCnSK_Id.ToString() };
            data[30] = new object[] { "Tb_ApCnSK_Id_TextField", _data.C.Tb_ApCnSK_Id_TextField == null ? "<Null>" : _data.C.Tb_ApCnSK_Id_TextField.ToString() };
            data[31] = new object[] { "Tb_UseLegacyConnectionCode", _data.C.Tb_UseLegacyConnectionCode.ToString() };
            data[32] = new object[] { "Tb_PkIsIdentity", _data.C.Tb_PkIsIdentity.ToString() };
            data[33] = new object[] { "Tb_IsVirtual", _data.C.Tb_IsVirtual.ToString() };
            data[34] = new object[] { "Tb_IsScreenPlaceHolder", _data.C.Tb_IsScreenPlaceHolder.ToString() };
            data[35] = new object[] { "Tb_IsNotEntity", _data.C.Tb_IsNotEntity.ToString() };
            data[36] = new object[] { "Tb_IsMany2Many", _data.C.Tb_IsMany2Many.ToString() };
            data[37] = new object[] { "Tb_UseLastModifiedByUserNameInSproc", _data.C.Tb_UseLastModifiedByUserNameInSproc.ToString() };
            data[38] = new object[] { "Tb_UseUserTimeStamp", _data.C.Tb_UseUserTimeStamp.ToString() };
            data[39] = new object[] { "Tb_UseForAudit", _data.C.Tb_UseForAudit.ToString() };
            data[40] = new object[] { "Tb_IsAllowDelete", _data.C.Tb_IsAllowDelete.ToString() };
            data[41] = new object[] { "Tb_CnfgGdMnu_MenuRow_IsVisible", _data.C.Tb_CnfgGdMnu_MenuRow_IsVisible.ToString() };
            data[42] = new object[] { "Tb_CnfgGdMnu_GridTools_IsVisible", _data.C.Tb_CnfgGdMnu_GridTools_IsVisible.ToString() };
            data[43] = new object[] { "Tb_CnfgGdMnu_SplitScreen_IsVisible", _data.C.Tb_CnfgGdMnu_SplitScreen_IsVisible.ToString() };
            data[44] = new object[] { "Tb_CnfgGdMnu_SplitScreen_IsSplit_Default", _data.C.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default.ToString() };
            data[45] = new object[] { "Tb_CnfgGdMnu_NavColumnWidth", _data.C.Tb_CnfgGdMnu_NavColumnWidth.ToString() };
            data[46] = new object[] { "Tb_CnfgGdMnu_IsReadOnly", _data.C.Tb_CnfgGdMnu_IsReadOnly.ToString() };
            data[47] = new object[] { "Tb_CnfgGdMnu_IsAllowNewRow", _data.C.Tb_CnfgGdMnu_IsAllowNewRow.ToString() };
            data[48] = new object[] { "Tb_CnfgGdMnu_ExcelExport_IsVisible", _data.C.Tb_CnfgGdMnu_ExcelExport_IsVisible.ToString() };
            data[49] = new object[] { "Tb_CnfgGdMnu_ColumnChooser_IsVisible", _data.C.Tb_CnfgGdMnu_ColumnChooser_IsVisible.ToString() };
            data[50] = new object[] { "Tb_CnfgGdMnu_ShowHideColBtn_IsVisible", _data.C.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible.ToString() };
            data[51] = new object[] { "Tb_CnfgGdMnu_RefreshGrid_IsVisible", _data.C.Tb_CnfgGdMnu_RefreshGrid_IsVisible.ToString() };
            data[52] = new object[] { "Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible", _data.C.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible.ToString() };
            data[53] = new object[] { "Tb_IsInputComplete", _data.C.Tb_IsInputComplete.ToString() };
            data[54] = new object[] { "Tb_IsCodeGen", _data.C.Tb_IsCodeGen.ToString() };
            data[55] = new object[] { "Tb_IsReadyCodeGen", _data.C.Tb_IsReadyCodeGen.ToString() };
            data[56] = new object[] { "Tb_IsCodeGenComplete", _data.C.Tb_IsCodeGenComplete.ToString() };
            data[57] = new object[] { "Tb_IsTagForCodeGen", _data.C.Tb_IsTagForCodeGen.ToString() };
            data[58] = new object[] { "Tb_IsTagForOther", _data.C.Tb_IsTagForOther.ToString() };
            data[59] = new object[] { "Tb_TableGroups", _data.C.Tb_TableGroups == null ? "<Null>" : _data.C.Tb_TableGroups.ToString() };
            data[60] = new object[] { "Tb_IsActiveRow", _data.C.Tb_IsActiveRow.ToString() };
            data[61] = new object[] { "Tb_IsDeleted", _data.C.Tb_IsDeleted.ToString() };
            data[62] = new object[] { "Tb_CreatedUserId", _data.C.Tb_CreatedUserId == null ? "<Null>" : _data.C.Tb_CreatedUserId.ToString() };
            data[63] = new object[] { "Tb_CreatedDate", _data.C.Tb_CreatedDate == null ? "<Null>" : _data.C.Tb_CreatedDate.ToString() };
            data[64] = new object[] { "Tb_UserId", _data.C.Tb_UserId == null ? "<Null>" : _data.C.Tb_UserId.ToString() };
            data[65] = new object[] { "UserName", _data.C.UserName == null ? "<Null>" : _data.C.UserName.ToString() };
            data[66] = new object[] { "Tb_LastModifiedDate", _data.C.Tb_LastModifiedDate == null ? "<Null>" : _data.C.Tb_LastModifiedDate.ToString() };
            return data;
        }


		#endregion ITraceItem Members


		#region INotifyPropertyChanged Members

        /// <summary>
        /// 	<para>
        ///         This method raises the <see cref="PropertyChanged">PropertyChanged</see> event
        ///         and is required by the INotifyPropertyChanged interface. It’s called in nearly
        ///         all public data field properties.
        ///     </para>
        /// 	<para>The <strong>INotifyPropertyChanged</strong> interface is used to notify
        ///     clients, typically binding clients, that a property value has changed. For more
        ///     information, search INotifyPropertyChanged on <strong>MSDN</strong>.</para>
        /// </summary>
        protected void Notify(string propName)
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }


		#endregion INotifyPropertyChanged Members


		#region INotifyDataErrorInfo Members

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        //public void RaiseErrorsChanged(string propertyName)
        //{
        //    if (ErrorsChanged != null)
        //        ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        //}

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            //throw new NotImplementedException();
            if (_brokenRuleManager.GetBrokenRulesForProperty(propertyName).Count == 0)
            {
                return "";
            }
            else
            {

                return _brokenRuleManager.GetBrokenRulesForProperty(propertyName)[0].ToString();
            }
        }

        public bool HasErrors
        {
            get { return _brokenRuleManager.IsEntityValid(); }
        }


		#endregion INotifyDataErrorInfo Members


        #region List Methods

        #region IEditableObject Members

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="CancelEdit">CancelEdit</see> and <see cref="EndEdit">EndEdit</see>.
        ///     </para>
        /// 	<para>Begins an edit on an object.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void BeginEdit()
        {
//            Guid? traceId = Guid.NewGuid();
//            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BeginEdit", IfxTraceCategory.Enter);
//            Console.WriteLine("BeginEdit");
//            //***  ToDo:
//            // Set the parent type below, then when properties are being edited, raise an event to sync fields in the props control.
//            _parentType = ParentEditObjectType.EntitiyListControl;
//
//            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BeginEdit", IfxTraceCategory.Leave);
        }

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="BeginEdit">BeginEdit</see> and <see cref="EndEdit">EndEdit</see>.
        ///     </para>
        /// 	<para>Discards changes since the last BeginEdit call.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void CancelEdit()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelEdit", IfxTraceCategory.Enter);
            UnDo();
            _parentType = ParentEditObjectType.None;

            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelEdit", IfxTraceCategory.Leave);
        }

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="BeginEdit">BeginEdit</see> and <see cref="CancelEdit">CancelEdit</see>.
        ///     </para>
        /// 	<para>Pushes changes since the last BeginEdit or IBindingList.AddNew call into the
        ///     underlying object.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void EndEdit()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", IfxTraceCategory.Enter);
//                if (_data.S.IsDirty() && _data.S.IsValid())
//                {
//                    // ToDo - This needs more testing for Silverlight
//                    Save(null, ParentEditObjectType.EntitiyListControl, UseConcurrencyCheck.UseDefaultSetting);
//
//                    //DataServiceInsertUpdateResponseClientSide result = Save(UseConcurrencyCheck.UseDefaultSetting);
//                    //if (result.Result != DataOperationResult.Success)
//                    //{
//                    //    UnDo();
//                    //    Debugger.Break();
//                    //}
//                }
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", IfxTraceCategory.Leave);
//            }
        }

        #endregion IEditableObject Members

        #endregion List Methods



        #region Children_WcTableColumn


        WcTableColumn_List _children_WcTableColumn = new WcTableColumn_List();


        public WcTableColumn_List Children_WcTableColumn
        {
            get { return _children_WcTableColumn; }
            set { _children_WcTableColumn = value; }
        }

        public void Get_Children_WcTableColumn()
        {
            _proxyWcTableColumnService.Begin_WcTableColumn_GetListByFK(Tb_Id);
        }
        

        void _proxyWcTableColumnService_WcTableColumn_GetListByFKCompleted(object sender, WcTableColumn_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableColumnService_WcTableColumn_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array != null)
                {
                    Children_WcTableColumn.ReplaceList(array);
                }
                else
                {
                    Children_WcTableColumn.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableColumnService_WcTableColumn_GetListByFKCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableColumnService_WcTableColumn_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }


            #endregion Children_WcTableColumn

            

        #region Children_WcTableChild


        WcTableChild_List _children_WcTableChild = new WcTableChild_List();


        public WcTableChild_List Children_WcTableChild
        {
            get { return _children_WcTableChild; }
            set { _children_WcTableChild = value; }
        }

        public void Get_Children_WcTableChild()
        {
            _proxyWcTableChildService.Begin_WcTableChild_GetListByFK(Tb_Id);
        }
        

        void _proxyWcTableChildService_WcTableChild_GetListByFKCompleted(object sender, WcTableChild_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableChildService_WcTableChild_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array != null)
                {
                    Children_WcTableChild.ReplaceList(array);
                }
                else
                {
                    Children_WcTableChild.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableChildService_WcTableChild_GetListByFKCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableChildService_WcTableChild_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }


            #endregion Children_WcTableChild

            

        #region Children_WcTableSortBy


        WcTableSortBy_List _children_WcTableSortBy = new WcTableSortBy_List();


        public WcTableSortBy_List Children_WcTableSortBy
        {
            get { return _children_WcTableSortBy; }
            set { _children_WcTableSortBy = value; }
        }

        public void Get_Children_WcTableSortBy()
        {
            _proxyWcTableSortByService.Begin_WcTableSortBy_GetListByFK(Tb_Id);
        }
        

        void _proxyWcTableSortByService_WcTableSortBy_GetListByFKCompleted(object sender, WcTableSortBy_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableSortByService_WcTableSortBy_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array != null)
                {
                    Children_WcTableSortBy.ReplaceList(array);
                }
                else
                {
                    Children_WcTableSortBy.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableSortByService_WcTableSortBy_GetListByFKCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableSortByService_WcTableSortBy_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }


            #endregion Children_WcTableSortBy

            

        #region Children_WcTableColumnGroup


        WcTableColumnGroup_List _children_WcTableColumnGroup = new WcTableColumnGroup_List();


        public WcTableColumnGroup_List Children_WcTableColumnGroup
        {
            get { return _children_WcTableColumnGroup; }
            set { _children_WcTableColumnGroup = value; }
        }

        //public void Get_Children_WcTableColumnGroup()
        //{
        //    _proxyWcTableColumnGroupService.Begin_WcTableColumnGroup_GetListByFK(Tb_Id);
        //}
        

        void _proxyWcTableColumnGroupService_WcTableColumnGroup_GetListByFKCompleted(object sender, WcTableColumnGroup_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableColumnGroupService_WcTableColumnGroup_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array != null)
                {
                    Children_WcTableColumnGroup.ReplaceList(array);
                }
                else
                {
                    Children_WcTableColumnGroup.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableColumnGroupService_WcTableColumnGroup_GetListByFKCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableColumnGroupService_WcTableColumnGroup_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }


            #endregion Children_WcTableColumnGroup

            

//        #region IDataErrorInfo Members
//
//        /// <summary>
//        /// 	<para>Part of the <strong>IDataErrorInfo</strong> Interface. Gets an error message
//        ///     indicating what is wrong with this object.</para>
//        /// 	<para>For more information, search <strong>IDataErrorInfo</strong> on
//        ///     <strong>MSDN</strong>.</para>
//        /// </summary>
//        public string Error
//        {
//            get { throw new NotImplementedException(); }
//        }
//
//        /// <summary>
//        /// 	<para>Part of the <strong>IDataErrorInfo</strong> Interface. Gets the error message
//        ///     for the property with the given name.</para>
//        /// 	<para>For more information, search <strong>IDataErrorInfo</strong> on
//        ///     <strong>MSDN</strong>.</para>
//        /// </summary>
//        string IDataErrorInfo.this[string columnName]
//        {
//            get
//            {
//                if (!_brokenRuleManager.IsPropertyValid(columnName))
//                {
//                    //string err = "line 1 First validation msg" + Environment.NewLine + "line 2 Second validation msg";
//                    //return err;
//                    return _brokenRuleManager.GetBrokenRulesForProperty(columnName)[0].ToString();
//                }
//                else
//                {
//                    return null;
//                }
//            }
//        }
//
//        #endregion



    }

}



