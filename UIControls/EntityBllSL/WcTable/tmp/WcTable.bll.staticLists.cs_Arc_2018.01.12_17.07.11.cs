using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;

// Gen Timestamp:  1/11/2018 6:38:10 PM

namespace EntityBll.SL
{

    public partial class WcTable_Bll_staticLists
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcTable_Bll_staticLists";




        static bool isDataLoaded = false;
        static ProxyWrapper.WcTableService_ProxyWrapper _staticWcTableProxy = null;

        #endregion Initialize Variables


        #region Load Data

        public static void InitializeProxyWrapper()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Enter);

                if (_staticWcTableProxy == null)
                {
                    _staticWcTableProxy = new ProxyWrapper.WcTableService_ProxyWrapper();
                    _staticWcTableProxy.GetWcTable_ReadOnlyStaticListsCompleted += new EventHandler<GetWcTable_ReadOnlyStaticListsCompletedEventArgs>(GetWcTable_ReadOnlyStaticListsCompleted);
                    _staticWcTableProxy.GetWcTableColumn_lstByTable_ComboItemListCompleted += new EventHandler<GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs>(GetWcTableColumn_lstByTable_ComboItemListCompleted);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Leave);
            }
        }

        public static void LoadStaticLists(Guid Tb_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Enter);

                if (_staticWcTableProxy == null)
                {
                    InitializeProxyWrapper();
                }

                _staticWcTableProxy.Begin_GetWcTable_ReadOnlyStaticLists(Tb_Id );

                LoadStaticLists_Custom();
                isDataLoaded = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Leave);
            }
        }

        static void GetWcTable_ReadOnlyStaticListsCompleted(object sender, GetWcTable_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTable_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                if (data != null)
                {


                    // WcTableColumn_lstByTable_ComboItemList
                    _wcTableColumn_lstByTable_ComboItemList_BindingList.IsRefreshingData = true;
                    _wcTableColumn_lstByTable_ComboItemList_BindingList.CachedList.Clear();
                    _wcTableColumn_lstByTable_ComboItemList_BindingList.Clear();
                    _wcTableColumn_lstByTable_ComboItemList_BindingList.ReplaceList((object[])data[0]);

                    _wcTableColumn_lstByTable_ComboItemList_BindingList.IsRefreshingData = false;
                    isDataLoaded = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTable_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTable_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Load Data


        #region Properties

        public static bool IsDataLoaded
        {
            get { return isDataLoaded; }
        }

                    // WcTableColumn_lstByTable_ComboItemList
        public static ComboItemList WcTableColumn_lstByTable_ComboItemList_BindingListProperty
        {
            get
            {
                return _wcTableColumn_lstByTable_ComboItemList_BindingList;
            }
            set
            {
                _wcTableColumn_lstByTable_ComboItemList_BindingList = value;
            }
        }

        #endregion Properties


        #region RefreshLists



        #region WcTableColumn_lstByTable_ComboItemList

        public static void Refresh_WcTableColumn_lstByTable_ComboItemList_BindingList(Guid Tb_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcTableColumn_lstByTable_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticWcTableProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticWcTableProxy.Begin_GetWcTableColumn_lstByTable_ComboItemList(Tb_Id );

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcTableColumn_lstByTable_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcTableColumn_lstByTable_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetWcTableColumn_lstByTable_ComboItemListCompleted(object sender, GetWcTableColumn_lstByTable_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByTable_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _wcTableColumn_lstByTable_ComboItemList_BindingList.IsRefreshingData = true;
                _wcTableColumn_lstByTable_ComboItemList_BindingList.ReplaceList(data);
                _wcTableColumn_lstByTable_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByTable_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByTable_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_lstByTable_ComboItemList



        #endregion RefreshLists


    }

    #region Classes for Binding

#endregion Classes for Binding

}

