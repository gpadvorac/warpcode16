using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/10/2015 7:19:54 PM

namespace EntityBll.SL
{
    public partial class TaskCategory_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_TkCt_Name = 100;
		public const int STRINGSIZE_TkCt_Desc = 300;
		public const int STRINGSIZE_UserName = 256;
		private const string BROKENRULE_TkCt_Id_Required = "Id is a required field.";
		private string BROKENRULE_TkCt_Name_TextLength = "Name has a maximum text length of  '" + STRINGSIZE_TkCt_Name + "'.";
		private const string BROKENRULE_TkCt_Name_Required = "Name is a required field.";
		private string BROKENRULE_TkCt_Desc_TextLength = "Description has a maximum text length of  '" + STRINGSIZE_TkCt_Desc + "'.";
		private const string BROKENRULE_TkCt_IsActiveRow_Required = "Active record is a required field.";
		private string BROKENRULE_UserName_TextLength = "User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_TkCt_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("TkCt_Id", BROKENRULE_TkCt_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TkCt_Name", BROKENRULE_TkCt_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("TkCt_Name", BROKENRULE_TkCt_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TkCt_Desc", BROKENRULE_TkCt_Desc_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TkCt_IsActiveRow", BROKENRULE_TkCt_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TkCt_Stamp", BROKENRULE_TkCt_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void TkCt_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TkCt_Id");
                string newBrokenRules = "";
                
                if (TkCt_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TkCt_Id", BROKENRULE_TkCt_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TkCt_Id", BROKENRULE_TkCt_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TkCt_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TkCt_Id", _brokenRuleManager.IsPropertyValid("TkCt_Id"), IsPropertyDirty("TkCt_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TkCt_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TkCt_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (TkCt_Name != null)
                {
                    len = TkCt_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TkCt_Name", BROKENRULE_TkCt_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("TkCt_Name", BROKENRULE_TkCt_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TkCt_Name", BROKENRULE_TkCt_Name_Required);

                    if (len > STRINGSIZE_TkCt_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("TkCt_Name", BROKENRULE_TkCt_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("TkCt_Name", BROKENRULE_TkCt_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TkCt_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TkCt_Name", _brokenRuleManager.IsPropertyValid("TkCt_Name"), IsPropertyDirty("TkCt_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TkCt_Desc_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_Desc_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TkCt_Desc");
                string newBrokenRules = "";
                				int len = 0;
                if (TkCt_Desc != null)
                {
                    len = TkCt_Desc.Length;
                }

                if (len > STRINGSIZE_TkCt_Desc)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TkCt_Desc", BROKENRULE_TkCt_Desc_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TkCt_Desc", BROKENRULE_TkCt_Desc_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TkCt_Desc");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TkCt_Desc", _brokenRuleManager.IsPropertyValid("TkCt_Desc"), IsPropertyDirty("TkCt_Desc"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_Desc_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_Desc_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TkCt_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TkCt_IsActiveRow");
                string newBrokenRules = "";
                
                if (TkCt_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TkCt_IsActiveRow", BROKENRULE_TkCt_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TkCt_IsActiveRow", BROKENRULE_TkCt_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TkCt_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TkCt_IsActiveRow", _brokenRuleManager.IsPropertyValid("TkCt_IsActiveRow"), IsPropertyDirty("TkCt_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TkCt_CreatedUserId_Validate()
        {
        }

        private void TkCt_CreatedDate_Validate()
        {
        }

        private void TkCt_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TkCt_LastModifiedDate_Validate()
        {
        }

        private void TkCt_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TkCt_Stamp");
                string newBrokenRules = "";
                
                if (TkCt_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TkCt_Stamp", BROKENRULE_TkCt_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TkCt_Stamp", BROKENRULE_TkCt_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TkCt_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TkCt_Stamp", _brokenRuleManager.IsPropertyValid("TkCt_Stamp"), IsPropertyDirty("TkCt_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TkCt_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


