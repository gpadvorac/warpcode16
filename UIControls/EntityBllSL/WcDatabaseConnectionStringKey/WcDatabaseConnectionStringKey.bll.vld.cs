using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/3/2018 10:46:34 AM

namespace EntityBll.SL
{
    public partial class WcDatabaseConnectionStringKey_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_DbCnSK_Name = 100;
		public const int STRINGSIZE_DbCnSK_Server = 50;
		public const int STRINGSIZE_DbCnSK_Database = 50;
		public const int STRINGSIZE_DbCnSK_UserName = 50;
		public const int STRINGSIZE_DbCnSK_Password = 50;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_DbCnSK_Id_Required = "Id is a required field.";
		private const string BROKENRULE_DbCnSK_ApVrsn_Id_Required = "ApVrsn_Id is a required field.";
		private string BROKENRULE_DbCnSK_Name_TextLength = "Key Name has a maximum text length of  '" + STRINGSIZE_DbCnSK_Name + "'.";
		private const string BROKENRULE_DbCnSK_Name_Required = "Key Name is a required field.";
		private const string BROKENRULE_DbCnSK_IsDefault_Required = "Is Default is a required field.";
		private string BROKENRULE_DbCnSK_Server_TextLength = "Server has a maximum text length of  '" + STRINGSIZE_DbCnSK_Server + "'.";
		private string BROKENRULE_DbCnSK_Database_TextLength = "Database has a maximum text length of  '" + STRINGSIZE_DbCnSK_Database + "'.";
		private string BROKENRULE_DbCnSK_UserName_TextLength = "UserName has a maximum text length of  '" + STRINGSIZE_DbCnSK_UserName + "'.";
		private string BROKENRULE_DbCnSK_Password_TextLength = "Password has a maximum text length of  '" + STRINGSIZE_DbCnSK_Password + "'.";
		private const string BROKENRULE_DbCnSK_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_DbCnSK_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_DbCnSK_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Id", BROKENRULE_DbCnSK_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_ApVrsn_Id", BROKENRULE_DbCnSK_ApVrsn_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Name", BROKENRULE_DbCnSK_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Name", BROKENRULE_DbCnSK_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_IsDefault", BROKENRULE_DbCnSK_IsDefault_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Server", BROKENRULE_DbCnSK_Server_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Database", BROKENRULE_DbCnSK_Database_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_UserName", BROKENRULE_DbCnSK_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Password", BROKENRULE_DbCnSK_Password_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_IsActiveRow", BROKENRULE_DbCnSK_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_IsDeleted", BROKENRULE_DbCnSK_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Stamp", BROKENRULE_DbCnSK_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void DbCnSK_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_Id");
                string newBrokenRules = "";
                
                if (DbCnSK_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Id", BROKENRULE_DbCnSK_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DbCnSK_Id", BROKENRULE_DbCnSK_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DbCnSK_Id", _brokenRuleManager.IsPropertyValid("DbCnSK_Id"), IsPropertyDirty("DbCnSK_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DbCnSK_ApVrsn_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_ApVrsn_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_ApVrsn_Id");
                string newBrokenRules = "";
                
                if (DbCnSK_ApVrsn_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_ApVrsn_Id", BROKENRULE_DbCnSK_ApVrsn_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DbCnSK_ApVrsn_Id", BROKENRULE_DbCnSK_ApVrsn_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_ApVrsn_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DbCnSK_ApVrsn_Id", _brokenRuleManager.IsPropertyValid("DbCnSK_ApVrsn_Id"), IsPropertyDirty("DbCnSK_ApVrsn_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_ApVrsn_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_ApVrsn_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DbCnSK_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (DbCnSK_Name != null)
                {
                    len = DbCnSK_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Name", BROKENRULE_DbCnSK_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("DbCnSK_Name", BROKENRULE_DbCnSK_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DbCnSK_Name", BROKENRULE_DbCnSK_Name_Required);
                    if (len > STRINGSIZE_DbCnSK_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Name", BROKENRULE_DbCnSK_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("DbCnSK_Name", BROKENRULE_DbCnSK_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DbCnSK_Name", _brokenRuleManager.IsPropertyValid("DbCnSK_Name"), IsPropertyDirty("DbCnSK_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DbCnSK_IsDefault_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_IsDefault_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_IsDefault");
                string newBrokenRules = "";
                
                if (DbCnSK_IsDefault == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_IsDefault", BROKENRULE_DbCnSK_IsDefault_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DbCnSK_IsDefault", BROKENRULE_DbCnSK_IsDefault_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_IsDefault");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DbCnSK_IsDefault", _brokenRuleManager.IsPropertyValid("DbCnSK_IsDefault"), IsPropertyDirty("DbCnSK_IsDefault"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_IsDefault_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_IsDefault_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DbCnSK_Server_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Server_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_Server");
                string newBrokenRules = "";
                				int len = 0;
                if (DbCnSK_Server != null)
                {
                    len = DbCnSK_Server.Length;
                }

                if (len > STRINGSIZE_DbCnSK_Server)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Server", BROKENRULE_DbCnSK_Server_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DbCnSK_Server", BROKENRULE_DbCnSK_Server_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_Server");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DbCnSK_Server", _brokenRuleManager.IsPropertyValid("DbCnSK_Server"), IsPropertyDirty("DbCnSK_Server"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Server_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Server_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DbCnSK_Database_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Database_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_Database");
                string newBrokenRules = "";
                				int len = 0;
                if (DbCnSK_Database != null)
                {
                    len = DbCnSK_Database.Length;
                }

                if (len > STRINGSIZE_DbCnSK_Database)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Database", BROKENRULE_DbCnSK_Database_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DbCnSK_Database", BROKENRULE_DbCnSK_Database_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_Database");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DbCnSK_Database", _brokenRuleManager.IsPropertyValid("DbCnSK_Database"), IsPropertyDirty("DbCnSK_Database"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Database_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Database_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DbCnSK_UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (DbCnSK_UserName != null)
                {
                    len = DbCnSK_UserName.Length;
                }

                if (len > STRINGSIZE_DbCnSK_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_UserName", BROKENRULE_DbCnSK_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DbCnSK_UserName", BROKENRULE_DbCnSK_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DbCnSK_UserName", _brokenRuleManager.IsPropertyValid("DbCnSK_UserName"), IsPropertyDirty("DbCnSK_UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DbCnSK_Password_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Password_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_Password");
                string newBrokenRules = "";
                				int len = 0;
                if (DbCnSK_Password != null)
                {
                    len = DbCnSK_Password.Length;
                }

                if (len > STRINGSIZE_DbCnSK_Password)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Password", BROKENRULE_DbCnSK_Password_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DbCnSK_Password", BROKENRULE_DbCnSK_Password_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_Password");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DbCnSK_Password", _brokenRuleManager.IsPropertyValid("DbCnSK_Password"), IsPropertyDirty("DbCnSK_Password"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Password_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Password_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DbCnSK_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_IsActiveRow");
                string newBrokenRules = "";
                
                if (DbCnSK_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_IsActiveRow", BROKENRULE_DbCnSK_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DbCnSK_IsActiveRow", BROKENRULE_DbCnSK_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DbCnSK_IsActiveRow", _brokenRuleManager.IsPropertyValid("DbCnSK_IsActiveRow"), IsPropertyDirty("DbCnSK_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DbCnSK_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_IsDeleted");
                string newBrokenRules = "";
                
                if (DbCnSK_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_IsDeleted", BROKENRULE_DbCnSK_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DbCnSK_IsDeleted", BROKENRULE_DbCnSK_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DbCnSK_IsDeleted", _brokenRuleManager.IsPropertyValid("DbCnSK_IsDeleted"), IsPropertyDirty("DbCnSK_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DbCnSK_CreatedUserId_Validate()
        {
        }

        private void DbCnSK_CreatedDate_Validate()
        {
        }

        private void DbCnSK_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DbCnSK_LastModifiedDate_Validate()
        {
        }

        private void DbCnSK_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_Stamp");
                string newBrokenRules = "";
                
                if (DbCnSK_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DbCnSK_Stamp", BROKENRULE_DbCnSK_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DbCnSK_Stamp", BROKENRULE_DbCnSK_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DbCnSK_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DbCnSK_Stamp", _brokenRuleManager.IsPropertyValid("DbCnSK_Stamp"), IsPropertyDirty("DbCnSK_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DbCnSK_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


