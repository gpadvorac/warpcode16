using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/15/2017 3:10:17 PM

namespace EntityBll.SL
{
    public partial class WcApplicationVersion_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_VersionNo = 16;
		public const int STRINGSIZE_ApVrsn_Server = 25;
		public const int STRINGSIZE_ApVrsn_DbName = 75;
		public const int STRINGSIZE_ApVrsn_SolutionPath = 255;
		public const int STRINGSIZE_ApVrsn_DefaultUIAssembly = 100;
		public const int STRINGSIZE_ApVrsn_DefaultUIAssemblyPath = 255;
		public const int STRINGSIZE_ApVrsn_DefaultWireTypePath = 255;
		public const int STRINGSIZE_ApVrsn_WebServerURL = 100;
		public const int STRINGSIZE_ApVrsn_WebsiteCodeFolderPath = 255;
		public const int STRINGSIZE_ApVrsn_WebserviceCodeFolderPath = 255;
		public const int STRINGSIZE_ApVrsn_StoredProcCodeFolder = 255;
		public const int STRINGSIZE_ApVrsn_Notes = 500;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_ApVrsn_Id_Required = "Id is a required field.";
		private const string BROKENRULE_ApVrsn_Ap_Id_Required = "Ap_Id is a required field.";
		private const string BROKENRULE_AttachmentCount_ZeroNotAllowed = "Attachments:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_DiscussionCount_ZeroNotAllowed = "Discussions:  '0'  (zero) is not allowed.";
		private string BROKENRULE_VersionNo_TextLength = "Vrsn# has a maximum text length of  '" + STRINGSIZE_VersionNo + "'.";
		private const string BROKENRULE_ApVrsn_MajorVersion_Required = "Major Version is a required field.";
		private const string BROKENRULE_ApVrsn_MajorVersion_ZeroNotAllowed = "Major Version:  '0'  (zero) is not allowed.";
		private string BROKENRULE_ApVrsn_Server_TextLength = "Server has a maximum text length of  '" + STRINGSIZE_ApVrsn_Server + "'.";
		private string BROKENRULE_ApVrsn_DbName_TextLength = "Database Name has a maximum text length of  '" + STRINGSIZE_ApVrsn_DbName + "'.";
		private const string BROKENRULE_ApVrsn_DbName_Required = "Database Name is a required field.";
		private string BROKENRULE_ApVrsn_SolutionPath_TextLength = "Solution Path has a maximum text length of  '" + STRINGSIZE_ApVrsn_SolutionPath + "'.";
		private string BROKENRULE_ApVrsn_DefaultUIAssembly_TextLength = "Default UI Assembly has a maximum text length of  '" + STRINGSIZE_ApVrsn_DefaultUIAssembly + "'.";
		private const string BROKENRULE_ApVrsn_DefaultUIAssembly_Required = "Default UI Assembly is a required field.";
		private string BROKENRULE_ApVrsn_DefaultUIAssemblyPath_TextLength = "Default UI Assembly Path has a maximum text length of  '" + STRINGSIZE_ApVrsn_DefaultUIAssemblyPath + "'.";
		private const string BROKENRULE_ApVrsn_DefaultUIAssemblyPath_Required = "Default UI Assembly Path is a required field.";
		private string BROKENRULE_ApVrsn_DefaultWireTypePath_TextLength = "Default Wire Type Path has a maximum text length of  '" + STRINGSIZE_ApVrsn_DefaultWireTypePath + "'.";
		private const string BROKENRULE_ApVrsn_DefaultWireTypePath_Required = "Default Wire Type Path is a required field.";
		private string BROKENRULE_ApVrsn_WebServerURL_TextLength = "Web Server URL has a maximum text length of  '" + STRINGSIZE_ApVrsn_WebServerURL + "'.";
		private const string BROKENRULE_ApVrsn_WebServerURL_Required = "Web Server URL is a required field.";
		private string BROKENRULE_ApVrsn_WebsiteCodeFolderPath_TextLength = "Website Code Folder Path has a maximum text length of  '" + STRINGSIZE_ApVrsn_WebsiteCodeFolderPath + "'.";
		private const string BROKENRULE_ApVrsn_WebsiteCodeFolderPath_Required = "Website Code Folder Path is a required field.";
		private string BROKENRULE_ApVrsn_WebserviceCodeFolderPath_TextLength = "Default W.S. Code Fldr Path has a maximum text length of  '" + STRINGSIZE_ApVrsn_WebserviceCodeFolderPath + "'.";
		private const string BROKENRULE_ApVrsn_WebserviceCodeFolderPath_Required = "Default W.S. Code Fldr Path is a required field.";
		private string BROKENRULE_ApVrsn_StoredProcCodeFolder_TextLength = "Stored Proc. Code Folder has a maximum text length of  '" + STRINGSIZE_ApVrsn_StoredProcCodeFolder + "'.";
		private const string BROKENRULE_ApVrsn_StoredProcCodeFolder_Required = "Stored Proc. Code Folder is a required field.";
		private const string BROKENRULE_ApVrsn_UseLegacyConnectionCode_Required = "Use Legacy Connection Code is a required field.";
		private const string BROKENRULE_ApVrsn_DefaultConnectionCodeId_Required = "Default Db Connection Key is a required field.";
		private const string BROKENRULE_ApVrsn_IsMulticultural_Required = "Is Multicultural is a required field.";
		private string BROKENRULE_ApVrsn_Notes_TextLength = "Notes has a maximum text length of  '" + STRINGSIZE_ApVrsn_Notes + "'.";
		private const string BROKENRULE_ApVrsn_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_ApVrsn_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_ApVrsn_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_Id", BROKENRULE_ApVrsn_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_Ap_Id", BROKENRULE_ApVrsn_Ap_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("AttachmentFileNames", BROKENRULE_AttachmentFileNames_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("DiscussionTitles", BROKENRULE_DiscussionTitles_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("VersionNo", BROKENRULE_VersionNo_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_MajorVersion", BROKENRULE_ApVrsn_MajorVersion_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_MajorVersion", BROKENRULE_ApVrsn_MajorVersion_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_Server", BROKENRULE_ApVrsn_Server_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DbName", BROKENRULE_ApVrsn_DbName_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DbName", BROKENRULE_ApVrsn_DbName_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_SolutionPath", BROKENRULE_ApVrsn_SolutionPath_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultUIAssembly", BROKENRULE_ApVrsn_DefaultUIAssembly_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultUIAssembly", BROKENRULE_ApVrsn_DefaultUIAssembly_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultUIAssemblyPath", BROKENRULE_ApVrsn_DefaultUIAssemblyPath_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultUIAssemblyPath", BROKENRULE_ApVrsn_DefaultUIAssemblyPath_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultWireTypePath", BROKENRULE_ApVrsn_DefaultWireTypePath_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultWireTypePath", BROKENRULE_ApVrsn_DefaultWireTypePath_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_WebServerURL", BROKENRULE_ApVrsn_WebServerURL_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_WebServerURL", BROKENRULE_ApVrsn_WebServerURL_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_WebsiteCodeFolderPath", BROKENRULE_ApVrsn_WebsiteCodeFolderPath_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_WebsiteCodeFolderPath", BROKENRULE_ApVrsn_WebsiteCodeFolderPath_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_WebserviceCodeFolderPath", BROKENRULE_ApVrsn_WebserviceCodeFolderPath_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_WebserviceCodeFolderPath", BROKENRULE_ApVrsn_WebserviceCodeFolderPath_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_StoredProcCodeFolder", BROKENRULE_ApVrsn_StoredProcCodeFolder_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_StoredProcCodeFolder", BROKENRULE_ApVrsn_StoredProcCodeFolder_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_UseLegacyConnectionCode", BROKENRULE_ApVrsn_UseLegacyConnectionCode_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultConnectionCodeId", BROKENRULE_ApVrsn_DefaultConnectionCodeId_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_IsMulticultural", BROKENRULE_ApVrsn_IsMulticultural_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_Notes", BROKENRULE_ApVrsn_Notes_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_IsActiveRow", BROKENRULE_ApVrsn_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_IsDeleted", BROKENRULE_ApVrsn_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_Stamp", BROKENRULE_ApVrsn_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void ApVrsn_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_Id");
                string newBrokenRules = "";
                
                if (ApVrsn_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_Id", BROKENRULE_ApVrsn_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_Id", BROKENRULE_ApVrsn_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_Id", _brokenRuleManager.IsPropertyValid("ApVrsn_Id"), IsPropertyDirty("ApVrsn_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_Ap_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Ap_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_Ap_Id");
                string newBrokenRules = "";
                
                if (ApVrsn_Ap_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_Ap_Id", BROKENRULE_ApVrsn_Ap_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_Ap_Id", BROKENRULE_ApVrsn_Ap_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_Ap_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_Ap_Id", _brokenRuleManager.IsPropertyValid("ApVrsn_Ap_Id"), IsPropertyDirty("ApVrsn_Ap_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Ap_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Ap_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void AttachmentCount_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("AttachmentCount");
                string newBrokenRules = "";
                
                if (AttachmentCount == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("AttachmentCount");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("AttachmentCount", _brokenRuleManager.IsPropertyValid("AttachmentCount"), IsPropertyDirty("AttachmentCount"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", IfxTraceCategory.Leave);
            }
        }

        private void AttachmentFileNames_Validate()
        {
        }

        private void DiscussionCount_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DiscussionCount");
                string newBrokenRules = "";
                
                if (DiscussionCount == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DiscussionCount");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DiscussionCount", _brokenRuleManager.IsPropertyValid("DiscussionCount"), IsPropertyDirty("DiscussionCount"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DiscussionTitles_Validate()
        {
        }

        private void VersionNo_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "VersionNo_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("VersionNo");
                string newBrokenRules = "";
                				int len = 0;
                if (VersionNo != null)
                {
                    len = VersionNo.Length;
                }

                if (len > STRINGSIZE_VersionNo)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("VersionNo", BROKENRULE_VersionNo_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("VersionNo", BROKENRULE_VersionNo_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("VersionNo");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("VersionNo", _brokenRuleManager.IsPropertyValid("VersionNo"), IsPropertyDirty("VersionNo"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "VersionNo_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "VersionNo_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_MajorVersion_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MajorVersion_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_MajorVersion");
                string newBrokenRules = "";
                
                if (ApVrsn_MajorVersion == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_MajorVersion", BROKENRULE_ApVrsn_MajorVersion_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_MajorVersion", BROKENRULE_ApVrsn_MajorVersion_Required);
                }

                if (ApVrsn_MajorVersion == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_MajorVersion", BROKENRULE_ApVrsn_MajorVersion_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_MajorVersion", BROKENRULE_ApVrsn_MajorVersion_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_MajorVersion");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_MajorVersion", _brokenRuleManager.IsPropertyValid("ApVrsn_MajorVersion"), IsPropertyDirty("ApVrsn_MajorVersion"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MajorVersion_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_MajorVersion_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_MinorVersion_Validate()
        {
        }

        private void ApVrsn_VersionIteration_Validate()
        {
        }

        private void ApVrsn_Server_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Server_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_Server");
                string newBrokenRules = "";
                				int len = 0;
                if (ApVrsn_Server != null)
                {
                    len = ApVrsn_Server.Length;
                }

                if (len > STRINGSIZE_ApVrsn_Server)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_Server", BROKENRULE_ApVrsn_Server_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_Server", BROKENRULE_ApVrsn_Server_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_Server");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_Server", _brokenRuleManager.IsPropertyValid("ApVrsn_Server"), IsPropertyDirty("ApVrsn_Server"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Server_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Server_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_DbName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DbName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_DbName");
                string newBrokenRules = "";
                				int len = 0;
                if (ApVrsn_DbName != null)
                {
                    len = ApVrsn_DbName.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DbName", BROKENRULE_ApVrsn_DbName_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_DbName", BROKENRULE_ApVrsn_DbName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_DbName", BROKENRULE_ApVrsn_DbName_Required);
                    if (len > STRINGSIZE_ApVrsn_DbName)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DbName", BROKENRULE_ApVrsn_DbName_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_DbName", BROKENRULE_ApVrsn_DbName_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_DbName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_DbName", _brokenRuleManager.IsPropertyValid("ApVrsn_DbName"), IsPropertyDirty("ApVrsn_DbName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DbName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DbName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_SolutionPath_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_SolutionPath_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_SolutionPath");
                string newBrokenRules = "";
                				int len = 0;
                if (ApVrsn_SolutionPath != null)
                {
                    len = ApVrsn_SolutionPath.Length;
                }

                if (len > STRINGSIZE_ApVrsn_SolutionPath)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_SolutionPath", BROKENRULE_ApVrsn_SolutionPath_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_SolutionPath", BROKENRULE_ApVrsn_SolutionPath_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_SolutionPath");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_SolutionPath", _brokenRuleManager.IsPropertyValid("ApVrsn_SolutionPath"), IsPropertyDirty("ApVrsn_SolutionPath"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_SolutionPath_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_SolutionPath_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_DefaultUIAssembly_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultUIAssembly_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_DefaultUIAssembly");
                string newBrokenRules = "";
                				int len = 0;
                if (ApVrsn_DefaultUIAssembly != null)
                {
                    len = ApVrsn_DefaultUIAssembly.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultUIAssembly", BROKENRULE_ApVrsn_DefaultUIAssembly_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_DefaultUIAssembly", BROKENRULE_ApVrsn_DefaultUIAssembly_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_DefaultUIAssembly", BROKENRULE_ApVrsn_DefaultUIAssembly_Required);
                    if (len > STRINGSIZE_ApVrsn_DefaultUIAssembly)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultUIAssembly", BROKENRULE_ApVrsn_DefaultUIAssembly_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_DefaultUIAssembly", BROKENRULE_ApVrsn_DefaultUIAssembly_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_DefaultUIAssembly");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_DefaultUIAssembly", _brokenRuleManager.IsPropertyValid("ApVrsn_DefaultUIAssembly"), IsPropertyDirty("ApVrsn_DefaultUIAssembly"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultUIAssembly_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultUIAssembly_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_DefaultUIAssemblyPath_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultUIAssemblyPath_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_DefaultUIAssemblyPath");
                string newBrokenRules = "";
                				int len = 0;
                if (ApVrsn_DefaultUIAssemblyPath != null)
                {
                    len = ApVrsn_DefaultUIAssemblyPath.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultUIAssemblyPath", BROKENRULE_ApVrsn_DefaultUIAssemblyPath_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_DefaultUIAssemblyPath", BROKENRULE_ApVrsn_DefaultUIAssemblyPath_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_DefaultUIAssemblyPath", BROKENRULE_ApVrsn_DefaultUIAssemblyPath_Required);
                    if (len > STRINGSIZE_ApVrsn_DefaultUIAssemblyPath)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultUIAssemblyPath", BROKENRULE_ApVrsn_DefaultUIAssemblyPath_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_DefaultUIAssemblyPath", BROKENRULE_ApVrsn_DefaultUIAssemblyPath_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_DefaultUIAssemblyPath");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_DefaultUIAssemblyPath", _brokenRuleManager.IsPropertyValid("ApVrsn_DefaultUIAssemblyPath"), IsPropertyDirty("ApVrsn_DefaultUIAssemblyPath"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultUIAssemblyPath_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultUIAssemblyPath_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_DefaultWireTypePath_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultWireTypePath_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_DefaultWireTypePath");
                string newBrokenRules = "";
                				int len = 0;
                if (ApVrsn_DefaultWireTypePath != null)
                {
                    len = ApVrsn_DefaultWireTypePath.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultWireTypePath", BROKENRULE_ApVrsn_DefaultWireTypePath_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_DefaultWireTypePath", BROKENRULE_ApVrsn_DefaultWireTypePath_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_DefaultWireTypePath", BROKENRULE_ApVrsn_DefaultWireTypePath_Required);
                    if (len > STRINGSIZE_ApVrsn_DefaultWireTypePath)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultWireTypePath", BROKENRULE_ApVrsn_DefaultWireTypePath_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_DefaultWireTypePath", BROKENRULE_ApVrsn_DefaultWireTypePath_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_DefaultWireTypePath");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_DefaultWireTypePath", _brokenRuleManager.IsPropertyValid("ApVrsn_DefaultWireTypePath"), IsPropertyDirty("ApVrsn_DefaultWireTypePath"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultWireTypePath_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultWireTypePath_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_WebServerURL_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebServerURL_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_WebServerURL");
                string newBrokenRules = "";
                				int len = 0;
                if (ApVrsn_WebServerURL != null)
                {
                    len = ApVrsn_WebServerURL.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_WebServerURL", BROKENRULE_ApVrsn_WebServerURL_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_WebServerURL", BROKENRULE_ApVrsn_WebServerURL_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_WebServerURL", BROKENRULE_ApVrsn_WebServerURL_Required);
                    if (len > STRINGSIZE_ApVrsn_WebServerURL)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_WebServerURL", BROKENRULE_ApVrsn_WebServerURL_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_WebServerURL", BROKENRULE_ApVrsn_WebServerURL_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_WebServerURL");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_WebServerURL", _brokenRuleManager.IsPropertyValid("ApVrsn_WebServerURL"), IsPropertyDirty("ApVrsn_WebServerURL"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebServerURL_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebServerURL_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_WebsiteCodeFolderPath_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebsiteCodeFolderPath_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_WebsiteCodeFolderPath");
                string newBrokenRules = "";
                				int len = 0;
                if (ApVrsn_WebsiteCodeFolderPath != null)
                {
                    len = ApVrsn_WebsiteCodeFolderPath.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_WebsiteCodeFolderPath", BROKENRULE_ApVrsn_WebsiteCodeFolderPath_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_WebsiteCodeFolderPath", BROKENRULE_ApVrsn_WebsiteCodeFolderPath_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_WebsiteCodeFolderPath", BROKENRULE_ApVrsn_WebsiteCodeFolderPath_Required);
                    if (len > STRINGSIZE_ApVrsn_WebsiteCodeFolderPath)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_WebsiteCodeFolderPath", BROKENRULE_ApVrsn_WebsiteCodeFolderPath_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_WebsiteCodeFolderPath", BROKENRULE_ApVrsn_WebsiteCodeFolderPath_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_WebsiteCodeFolderPath");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_WebsiteCodeFolderPath", _brokenRuleManager.IsPropertyValid("ApVrsn_WebsiteCodeFolderPath"), IsPropertyDirty("ApVrsn_WebsiteCodeFolderPath"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebsiteCodeFolderPath_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebsiteCodeFolderPath_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_WebserviceCodeFolderPath_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebserviceCodeFolderPath_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_WebserviceCodeFolderPath");
                string newBrokenRules = "";
                				int len = 0;
                if (ApVrsn_WebserviceCodeFolderPath != null)
                {
                    len = ApVrsn_WebserviceCodeFolderPath.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_WebserviceCodeFolderPath", BROKENRULE_ApVrsn_WebserviceCodeFolderPath_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_WebserviceCodeFolderPath", BROKENRULE_ApVrsn_WebserviceCodeFolderPath_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_WebserviceCodeFolderPath", BROKENRULE_ApVrsn_WebserviceCodeFolderPath_Required);
                    if (len > STRINGSIZE_ApVrsn_WebserviceCodeFolderPath)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_WebserviceCodeFolderPath", BROKENRULE_ApVrsn_WebserviceCodeFolderPath_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_WebserviceCodeFolderPath", BROKENRULE_ApVrsn_WebserviceCodeFolderPath_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_WebserviceCodeFolderPath");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_WebserviceCodeFolderPath", _brokenRuleManager.IsPropertyValid("ApVrsn_WebserviceCodeFolderPath"), IsPropertyDirty("ApVrsn_WebserviceCodeFolderPath"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebserviceCodeFolderPath_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_WebserviceCodeFolderPath_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_StoredProcCodeFolder_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_StoredProcCodeFolder_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_StoredProcCodeFolder");
                string newBrokenRules = "";
                				int len = 0;
                if (ApVrsn_StoredProcCodeFolder != null)
                {
                    len = ApVrsn_StoredProcCodeFolder.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_StoredProcCodeFolder", BROKENRULE_ApVrsn_StoredProcCodeFolder_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_StoredProcCodeFolder", BROKENRULE_ApVrsn_StoredProcCodeFolder_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_StoredProcCodeFolder", BROKENRULE_ApVrsn_StoredProcCodeFolder_Required);
                    if (len > STRINGSIZE_ApVrsn_StoredProcCodeFolder)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_StoredProcCodeFolder", BROKENRULE_ApVrsn_StoredProcCodeFolder_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_StoredProcCodeFolder", BROKENRULE_ApVrsn_StoredProcCodeFolder_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_StoredProcCodeFolder");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_StoredProcCodeFolder", _brokenRuleManager.IsPropertyValid("ApVrsn_StoredProcCodeFolder"), IsPropertyDirty("ApVrsn_StoredProcCodeFolder"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_StoredProcCodeFolder_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_StoredProcCodeFolder_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_UseLegacyConnectionCode_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_UseLegacyConnectionCode_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_UseLegacyConnectionCode");
                string newBrokenRules = "";
                
                if (ApVrsn_UseLegacyConnectionCode == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_UseLegacyConnectionCode", BROKENRULE_ApVrsn_UseLegacyConnectionCode_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_UseLegacyConnectionCode", BROKENRULE_ApVrsn_UseLegacyConnectionCode_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_UseLegacyConnectionCode");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_UseLegacyConnectionCode", _brokenRuleManager.IsPropertyValid("ApVrsn_UseLegacyConnectionCode"), IsPropertyDirty("ApVrsn_UseLegacyConnectionCode"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_UseLegacyConnectionCode_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_UseLegacyConnectionCode_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_DefaultConnectionCodeId_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultConnectionCodeId_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_DefaultConnectionCodeId");
                string newBrokenRules = "";
                
                if (ApVrsn_DefaultConnectionCodeId == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_DefaultConnectionCodeId", BROKENRULE_ApVrsn_DefaultConnectionCodeId_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_DefaultConnectionCodeId", BROKENRULE_ApVrsn_DefaultConnectionCodeId_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_DefaultConnectionCodeId");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_DefaultConnectionCodeId", _brokenRuleManager.IsPropertyValid("ApVrsn_DefaultConnectionCodeId"), IsPropertyDirty("ApVrsn_DefaultConnectionCodeId"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultConnectionCodeId_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_DefaultConnectionCodeId_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_DefaultConnectionCodeId_TextField_Validate()
        {
        }

        private void ApVrsn_IsMulticultural_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsMulticultural_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_IsMulticultural");
                string newBrokenRules = "";
                
                if (ApVrsn_IsMulticultural == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_IsMulticultural", BROKENRULE_ApVrsn_IsMulticultural_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_IsMulticultural", BROKENRULE_ApVrsn_IsMulticultural_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_IsMulticultural");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_IsMulticultural", _brokenRuleManager.IsPropertyValid("ApVrsn_IsMulticultural"), IsPropertyDirty("ApVrsn_IsMulticultural"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsMulticultural_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsMulticultural_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_Notes_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Notes_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_Notes");
                string newBrokenRules = "";
                				int len = 0;
                if (ApVrsn_Notes != null)
                {
                    len = ApVrsn_Notes.Length;
                }

                if (len > STRINGSIZE_ApVrsn_Notes)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_Notes", BROKENRULE_ApVrsn_Notes_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_Notes", BROKENRULE_ApVrsn_Notes_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_Notes");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_Notes", _brokenRuleManager.IsPropertyValid("ApVrsn_Notes"), IsPropertyDirty("ApVrsn_Notes"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Notes_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Notes_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_IsActiveRow");
                string newBrokenRules = "";
                
                if (ApVrsn_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_IsActiveRow", BROKENRULE_ApVrsn_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_IsActiveRow", BROKENRULE_ApVrsn_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_IsActiveRow", _brokenRuleManager.IsPropertyValid("ApVrsn_IsActiveRow"), IsPropertyDirty("ApVrsn_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_IsDeleted");
                string newBrokenRules = "";
                
                if (ApVrsn_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_IsDeleted", BROKENRULE_ApVrsn_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_IsDeleted", BROKENRULE_ApVrsn_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_IsDeleted", _brokenRuleManager.IsPropertyValid("ApVrsn_IsDeleted"), IsPropertyDirty("ApVrsn_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_CreatedUserId_Validate()
        {
        }

        private void ApVrsn_CreatedDate_Validate()
        {
        }

        private void ApVrsn_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void ApVrsn_LastModifiedDate_Validate()
        {
        }

        private void ApVrsn_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_Stamp");
                string newBrokenRules = "";
                
                if (ApVrsn_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("ApVrsn_Stamp", BROKENRULE_ApVrsn_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("ApVrsn_Stamp", BROKENRULE_ApVrsn_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("ApVrsn_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("ApVrsn_Stamp", _brokenRuleManager.IsPropertyValid("ApVrsn_Stamp"), IsPropertyDirty("ApVrsn_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ApVrsn_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


