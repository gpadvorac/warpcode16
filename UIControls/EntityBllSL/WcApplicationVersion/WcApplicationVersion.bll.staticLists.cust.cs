using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;


// Gen Timestamp:  11/17/2016 12:24:49 PM

namespace EntityBll.SL
{

    public partial class WcApplicationVersion_Bll_staticLists
    {

        #region Initialize Variables

        private static ComboItemList _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);
        private static bool _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList_HasItems = true;


        #endregion Initialize Variables




        public static void LoadStaticLists_Custom()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", IfxTraceCategory.Enter);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", IfxTraceCategory.Leave);
            }
        }




    }

}


