using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  6/18/2016 5:33:08 PM

namespace EntityBll.SL
{
    public partial class Task_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_CategoryDescription = 300;
		public const int STRINGSIZE_Tk_Name = 200;
		public const int STRINGSIZE_Tk_Desc = 2000;
		public const int STRINGSIZE_Tk_Results = 1000;
		public const int STRINGSIZE_Tk_Remarks = 1000;
		public const int STRINGSIZE_Tk_Assignees = 1000;
		public const int STRINGSIZE_UserName = 256;
		private const string BROKENRULE_Tk_Id_Required = "Id is a required field.";
		private const string BROKENRULE_Tk_Prj_Id_Required = "Prj_Id is a required field.";
		private const string BROKENRULE_AttachmentCount_ZeroNotAllowed = "Attachments:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_DiscussionCount_ZeroNotAllowed = "Discussions:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_Tk_TkTp_Id_Required = "Type is a required field.";
		private const string BROKENRULE_Tk_TkTp_Id_ZeroNotAllowed = "Type:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_Tk_TkTp_Id_NullNotAllowed = "Type 'Null' is not allowed.";
		private string BROKENRULE_CategoryDescription_TextLength = "Category Description has a maximum text length of  '" + STRINGSIZE_CategoryDescription + "'.";
		private const string BROKENRULE_Tk_Number_ZeroNotAllowed = "Number:  '0'  (zero) is not allowed.";
		private string BROKENRULE_Tk_Name_TextLength = "Name has a maximum text length of  '" + STRINGSIZE_Tk_Name + "'.";
		private const string BROKENRULE_Tk_Name_Required = "Name is a required field.";
		private string BROKENRULE_Tk_Desc_TextLength = "Description has a maximum text length of  '" + STRINGSIZE_Tk_Desc + "'.";
		private const string BROKENRULE_Tk_EstimatedTime_ZeroNotAllowed = "Estimated Time:  '0'  (zero) is not allowed.";
		private string BROKENRULE_Tk_EstimatedTime_Precision = "Estimated Time - has a maximum of ‘6’ digits allowed to the left of the decimal point.  You entered have more than 6.";
		private string BROKENRULE_Tk_EstimatedTime_Scale = "Estimated Time - has a maximum of ‘2’ digits allowed to the right of the decimal point.  You entered have more than 2.";
		private const string BROKENRULE_Tk_ActualTime_ZeroNotAllowed = "Actual Time:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_Tk_PercentComplete_ZeroNotAllowed = "Percent Complete:  '0'  (zero) is not allowed.";
		private string BROKENRULE_Tk_PercentComplete_Precision = "Percent Complete - has a maximum of ‘1’ digits allowed to the left of the decimal point.  You entered have more than 1.";
		private string BROKENRULE_Tk_PercentComplete_Scale = "Percent Complete - has a maximum of ‘3’ digits allowed to the right of the decimal point.  You entered have more than 3.";
		private const string BROKENRULE_Tk_TkSt_Id_ZeroNotAllowed = "Status:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_Tk_TkPr_Id_ZeroNotAllowed = "Priority:  '0'  (zero) is not allowed.";
		private string BROKENRULE_Tk_Results_TextLength = "Results has a maximum text length of  '" + STRINGSIZE_Tk_Results + "'.";
		private string BROKENRULE_Tk_Remarks_TextLength = "Remarks has a maximum text length of  '" + STRINGSIZE_Tk_Remarks + "'.";
		private string BROKENRULE_Tk_Assignees_TextLength = "Assignees has a maximum text length of  '" + STRINGSIZE_Tk_Assignees + "'.";
		private const string BROKENRULE_Tk_IsPrivate_Required = "Private is a required field.";
		private const string BROKENRULE_Tk_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_Tk_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_Tk_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_Id", BROKENRULE_Tk_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_Prj_Id", BROKENRULE_Tk_Prj_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("AttachmentFileNames", BROKENRULE_AttachmentFileNames_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("DiscussionTitles", BROKENRULE_DiscussionTitles_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_TkTp_Id", BROKENRULE_Tk_TkTp_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_TkTp_Id", BROKENRULE_Tk_TkTp_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_TkTp_Id", BROKENRULE_Tk_TkTp_Id_NullNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("CategoryDescription", BROKENRULE_CategoryDescription_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_Number", BROKENRULE_Tk_Number_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_Name", BROKENRULE_Tk_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Tk_Name", BROKENRULE_Tk_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_Desc", BROKENRULE_Tk_Desc_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_EstimatedTime", BROKENRULE_Tk_EstimatedTime_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_ActualTime", BROKENRULE_Tk_ActualTime_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_PercentComplete", BROKENRULE_Tk_PercentComplete_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_TkSt_Id", BROKENRULE_Tk_TkSt_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_TkPr_Id", BROKENRULE_Tk_TkPr_Id_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_Results", BROKENRULE_Tk_Results_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_Remarks", BROKENRULE_Tk_Remarks_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_Assignees", BROKENRULE_Tk_Assignees_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_IsPrivate", BROKENRULE_Tk_IsPrivate_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_IsActiveRow", BROKENRULE_Tk_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_IsDeleted", BROKENRULE_Tk_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tk_Stamp", BROKENRULE_Tk_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void Tk_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Id");
                string newBrokenRules = "";
                
                if (Tk_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_Id", BROKENRULE_Tk_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_Id", BROKENRULE_Tk_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_Id", _brokenRuleManager.IsPropertyValid("Tk_Id"), IsPropertyDirty("Tk_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_Prj_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Prj_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Prj_Id");
                string newBrokenRules = "";
                
                if (Tk_Prj_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_Prj_Id", BROKENRULE_Tk_Prj_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_Prj_Id", BROKENRULE_Tk_Prj_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Prj_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_Prj_Id", _brokenRuleManager.IsPropertyValid("Tk_Prj_Id"), IsPropertyDirty("Tk_Prj_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Prj_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Prj_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_Tk_Id_Validate()
        {
        }

        private void AttachmentCount_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("AttachmentCount");
                string newBrokenRules = "";
                
                if (AttachmentCount == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("AttachmentCount");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("AttachmentCount", _brokenRuleManager.IsPropertyValid("AttachmentCount"), IsPropertyDirty("AttachmentCount"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", IfxTraceCategory.Leave);
            }
        }

        private void AttachmentFileNames_Validate()
        {
        }

        private void DiscussionCount_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DiscussionCount");
                string newBrokenRules = "";
                
                if (DiscussionCount == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DiscussionCount");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DiscussionCount", _brokenRuleManager.IsPropertyValid("DiscussionCount"), IsPropertyDirty("DiscussionCount"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DiscussionTitles_Validate()
        {
        }

        private void Tk_TkTp_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_TkTp_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_TkTp_Id");
                string newBrokenRules = "";
                
                if (Tk_TkTp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_TkTp_Id", BROKENRULE_Tk_TkTp_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_TkTp_Id", BROKENRULE_Tk_TkTp_Id_Required);
                }

                if (Tk_TkTp_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_TkTp_Id", BROKENRULE_Tk_TkTp_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_TkTp_Id", BROKENRULE_Tk_TkTp_Id_ZeroNotAllowed);
                }


                if (Tk_TkTp_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_TkTp_Id", BROKENRULE_Tk_TkTp_Id_NullNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_TkTp_Id", BROKENRULE_Tk_TkTp_Id_NullNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_TkTp_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_TkTp_Id", _brokenRuleManager.IsPropertyValid("Tk_TkTp_Id"), IsPropertyDirty("Tk_TkTp_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_TkTp_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_TkTp_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_TkTp_Id_TextField_Validate()
        {
        }

        private void Tk_TkCt_Id_Validate()
        {
        }

        private void Tk_TkCt_Id_TextField_Validate()
        {
        }

        private void CategoryDescription_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CategoryDescription_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("CategoryDescription");
                string newBrokenRules = "";
                				int len = 0;
                if (CategoryDescription != null)
                {
                    len = CategoryDescription.Length;
                }

                if (len > STRINGSIZE_CategoryDescription)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("CategoryDescription", BROKENRULE_CategoryDescription_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("CategoryDescription", BROKENRULE_CategoryDescription_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("CategoryDescription");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("CategoryDescription", _brokenRuleManager.IsPropertyValid("CategoryDescription"), IsPropertyDirty("CategoryDescription"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CategoryDescription_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CategoryDescription_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_Number_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Number_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Number");
                string newBrokenRules = "";
                
                if (Tk_Number == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_Number", BROKENRULE_Tk_Number_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_Number", BROKENRULE_Tk_Number_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Number");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_Number", _brokenRuleManager.IsPropertyValid("Tk_Number"), IsPropertyDirty("Tk_Number"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Number_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Number_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (Tk_Name != null)
                {
                    len = Tk_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_Name", BROKENRULE_Tk_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_Name", BROKENRULE_Tk_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_Name", BROKENRULE_Tk_Name_Required);

                    if (len > STRINGSIZE_Tk_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Tk_Name", BROKENRULE_Tk_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Tk_Name", BROKENRULE_Tk_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_Name", _brokenRuleManager.IsPropertyValid("Tk_Name"), IsPropertyDirty("Tk_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_Desc_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Desc_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Desc");
                string newBrokenRules = "";
                				int len = 0;
                if (Tk_Desc != null)
                {
                    len = Tk_Desc.Length;
                }

                if (len > STRINGSIZE_Tk_Desc)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_Desc", BROKENRULE_Tk_Desc_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_Desc", BROKENRULE_Tk_Desc_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Desc");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_Desc", _brokenRuleManager.IsPropertyValid("Tk_Desc"), IsPropertyDirty("Tk_Desc"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Desc_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Desc_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_EstimatedTime_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_EstimatedTime_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_EstimatedTime");
                string newBrokenRules = "";
                
                if (Tk_EstimatedTime == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_EstimatedTime", BROKENRULE_Tk_EstimatedTime_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_EstimatedTime", BROKENRULE_Tk_EstimatedTime_ZeroNotAllowed);
                }


                if (Tk_EstimatedTime == null)
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_EstimatedTime", BROKENRULE_Tk_EstimatedTime_Precision);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_EstimatedTime", BROKENRULE_Tk_EstimatedTime_Scale);
                }
                else
                {
                    string[] dString = ((decimal)Tk_EstimatedTime).ToString("###############.##############", CultureInfo.InvariantCulture).Split('.');

                    if (dString[0].Length > (6))
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Tk_EstimatedTime", BROKENRULE_Tk_EstimatedTime_Precision);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Tk_EstimatedTime", BROKENRULE_Tk_EstimatedTime_Precision);
                    }

                    if (dString.Length == 1)
                    {
                        // No decimal values
                            _brokenRuleManager.ClearBrokenRuleForProperty("Tk_EstimatedTime", BROKENRULE_Tk_EstimatedTime_Scale);
                    }
                    else
                    {
                        if (dString[1].Length > 2)
                        {
                            _brokenRuleManager.AddBrokenRuleForProperty("Tk_EstimatedTime", BROKENRULE_Tk_EstimatedTime_Scale);
                        }
                        else
                        {
                            _brokenRuleManager.ClearBrokenRuleForProperty("Tk_EstimatedTime", BROKENRULE_Tk_EstimatedTime_Scale);
                        }
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_EstimatedTime");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_EstimatedTime", _brokenRuleManager.IsPropertyValid("Tk_EstimatedTime"), IsPropertyDirty("Tk_EstimatedTime"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_EstimatedTime_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_EstimatedTime_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_ActualTime_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_ActualTime_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_ActualTime");
                string newBrokenRules = "";
                
                if (Tk_ActualTime == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_ActualTime", BROKENRULE_Tk_ActualTime_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_ActualTime", BROKENRULE_Tk_ActualTime_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_ActualTime");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_ActualTime", _brokenRuleManager.IsPropertyValid("Tk_ActualTime"), IsPropertyDirty("Tk_ActualTime"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_ActualTime_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_ActualTime_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_PercentComplete_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_PercentComplete_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_PercentComplete");
                string newBrokenRules = "";
                
                if (Tk_PercentComplete == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_PercentComplete", BROKENRULE_Tk_PercentComplete_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_PercentComplete", BROKENRULE_Tk_PercentComplete_ZeroNotAllowed);
                }


                if (Tk_PercentComplete == null)
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_PercentComplete", BROKENRULE_Tk_PercentComplete_Precision);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_PercentComplete", BROKENRULE_Tk_PercentComplete_Scale);
                }
                else
                {
                    string[] dString = ((decimal)Tk_PercentComplete).ToString("###############.##############", CultureInfo.InvariantCulture).Split('.');

                    if (dString[0].Length > (1))
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Tk_PercentComplete", BROKENRULE_Tk_PercentComplete_Precision);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Tk_PercentComplete", BROKENRULE_Tk_PercentComplete_Precision);
                    }

                    if (dString.Length == 1)
                    {
                        // No decimal values
                            _brokenRuleManager.ClearBrokenRuleForProperty("Tk_PercentComplete", BROKENRULE_Tk_PercentComplete_Scale);
                    }
                    else
                    {
                        if (dString[1].Length > 3)
                        {
                            _brokenRuleManager.AddBrokenRuleForProperty("Tk_PercentComplete", BROKENRULE_Tk_PercentComplete_Scale);
                        }
                        else
                        {
                            _brokenRuleManager.ClearBrokenRuleForProperty("Tk_PercentComplete", BROKENRULE_Tk_PercentComplete_Scale);
                        }
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_PercentComplete");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_PercentComplete", _brokenRuleManager.IsPropertyValid("Tk_PercentComplete"), IsPropertyDirty("Tk_PercentComplete"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_PercentComplete_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_PercentComplete_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_StartDate_Validate()
        {
        }

        private void Tk_Deadline_Validate()
        {
        }

        private void Tk_TkSt_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_TkSt_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_TkSt_Id");
                string newBrokenRules = "";
                
                if (Tk_TkSt_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_TkSt_Id", BROKENRULE_Tk_TkSt_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_TkSt_Id", BROKENRULE_Tk_TkSt_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_TkSt_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_TkSt_Id", _brokenRuleManager.IsPropertyValid("Tk_TkSt_Id"), IsPropertyDirty("Tk_TkSt_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_TkSt_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_TkSt_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_TkSt_Id_TextField_Validate()
        {
        }

        private void Tk_TkPr_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_TkPr_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_TkPr_Id");
                string newBrokenRules = "";
                
                if (Tk_TkPr_Id == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_TkPr_Id", BROKENRULE_Tk_TkPr_Id_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_TkPr_Id", BROKENRULE_Tk_TkPr_Id_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_TkPr_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_TkPr_Id", _brokenRuleManager.IsPropertyValid("Tk_TkPr_Id"), IsPropertyDirty("Tk_TkPr_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_TkPr_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_TkPr_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_TkPr_Id_TextField_Validate()
        {
        }

        private void Tk_Results_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Results_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Results");
                string newBrokenRules = "";
                				int len = 0;
                if (Tk_Results != null)
                {
                    len = Tk_Results.Length;
                }

                if (len > STRINGSIZE_Tk_Results)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_Results", BROKENRULE_Tk_Results_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_Results", BROKENRULE_Tk_Results_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Results");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_Results", _brokenRuleManager.IsPropertyValid("Tk_Results"), IsPropertyDirty("Tk_Results"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Results_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Results_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_Remarks_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Remarks_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Remarks");
                string newBrokenRules = "";
                				int len = 0;
                if (Tk_Remarks != null)
                {
                    len = Tk_Remarks.Length;
                }

                if (len > STRINGSIZE_Tk_Remarks)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_Remarks", BROKENRULE_Tk_Remarks_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_Remarks", BROKENRULE_Tk_Remarks_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Remarks");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_Remarks", _brokenRuleManager.IsPropertyValid("Tk_Remarks"), IsPropertyDirty("Tk_Remarks"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Remarks_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Remarks_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_Assignees_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Assignees_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Assignees");
                string newBrokenRules = "";
                				int len = 0;
                if (Tk_Assignees != null)
                {
                    len = Tk_Assignees.Length;
                }

                if (len > STRINGSIZE_Tk_Assignees)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_Assignees", BROKENRULE_Tk_Assignees_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_Assignees", BROKENRULE_Tk_Assignees_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Assignees");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_Assignees", _brokenRuleManager.IsPropertyValid("Tk_Assignees"), IsPropertyDirty("Tk_Assignees"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Assignees_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Assignees_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_IsPrivate_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_IsPrivate_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_IsPrivate");
                string newBrokenRules = "";
                
                if (Tk_IsPrivate == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_IsPrivate", BROKENRULE_Tk_IsPrivate_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_IsPrivate", BROKENRULE_Tk_IsPrivate_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_IsPrivate");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_IsPrivate", _brokenRuleManager.IsPropertyValid("Tk_IsPrivate"), IsPropertyDirty("Tk_IsPrivate"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_IsPrivate_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_IsPrivate_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_IsActiveRow");
                string newBrokenRules = "";
                
                if (Tk_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_IsActiveRow", BROKENRULE_Tk_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_IsActiveRow", BROKENRULE_Tk_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_IsActiveRow", _brokenRuleManager.IsPropertyValid("Tk_IsActiveRow"), IsPropertyDirty("Tk_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_IsDeleted");
                string newBrokenRules = "";
                
                if (Tk_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_IsDeleted", BROKENRULE_Tk_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_IsDeleted", BROKENRULE_Tk_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_IsDeleted", _brokenRuleManager.IsPropertyValid("Tk_IsDeleted"), IsPropertyDirty("Tk_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_CreatedUserId_Validate()
        {
        }

        private void Tk_CreatedDate_Validate()
        {
        }

        private void Tk_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tk_LastModifiedDate_Validate()
        {
        }

        private void Tk_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Stamp");
                string newBrokenRules = "";
                
                if (Tk_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tk_Stamp", BROKENRULE_Tk_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tk_Stamp", BROKENRULE_Tk_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tk_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tk_Stamp", _brokenRuleManager.IsPropertyValid("Tk_Stamp"), IsPropertyDirty("Tk_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tk_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Ct_Id_Validate()
        {
        }

        private void CtD_Id_Validate()
        {
        }

        private void Df_Id_Validate()
        {
        }

        private void Ls_Id_Validate()
        {
        }

        private void LsTr_Id_Validate()
        {
        }

        private void Ob_Id_Validate()
        {
        }

        private void Pp_Id_Validate()
        {
        }

        private void Wl_Id_Validate()
        {
        }

        private void RCt_Id_Validate()
        {
        }


		#endregion Validation Logic


    }

}


