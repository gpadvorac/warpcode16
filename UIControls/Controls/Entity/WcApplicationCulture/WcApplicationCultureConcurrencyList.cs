using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class WcApplicationCultureConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcApplicationCultureConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public WcApplicationCultureConcurrencyList(WcApplicationCulture_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcApplicationCultureConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Sort Order", data.C.ApCltr_Sort, data.X.ApCltr_Sort));
                _concurrencyList.Add(new ConcurrencyItem("Culture", data.C.ApCltr_Cltr_Id, data.X.ApCltr_Cltr_Id));
                _concurrencyList.Add(new ConcurrencyItem("Comment", data.C.ApCltr_Comment, data.X.ApCltr_Comment));
                _concurrencyList.Add(new ConcurrencyItem("Is Default", data.C.ApCltr_IsDefault, data.X.ApCltr_IsDefault));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.ApCltr_IsActiveRow, data.X.ApCltr_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



