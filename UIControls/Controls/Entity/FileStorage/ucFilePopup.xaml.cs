﻿using ApplicationTypeServices;
using EntityBll.SL;
using EntityWireTypeSL;
using Ifx.SL;
using Infragistics.Controls.Grids;
using Infragistics.Controls.Interactions;
using ProxyWrapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using TypeServices;
using vControls;
using Velocity.SL;
using vUICommon;

namespace UIControls
{
    public partial class ucFilePopup : UserControl
    {




        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "ucFilePopup";


        public event ClosePopUpEventHandler ClosePopup;

        XamDialogWindow _parentWindow = null;

        public enum ParentType
        {
            Customer,
            Order,
            Task,
            DiscussionComment
        }

        /// <summary>
        ///     A string value naming the current parent's type. It’s possible for an Entity
        ///     Manager to have many different parent types while others have only one or none at
        ///     all. For example, an <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/The_Entity_Manager.html">Entity Manager</a> for an Address entity could have a parent type
        ///     of Customer, Company, Person, and so on. There may be situations where different
        ///     logic or filters need to be applied depending on the type of parent. This helps
        ///     make the <see cref="TypeServices.IEntityControl">IEntityControl</see> interface
        ///     more extendable and reusable.
        /// </summary>
        string _parentType;
        /// <summary>
        ///     Guid Id value of the object currently acting as the parent. Used to filter the
        ///     navigation list (<see cref="ucLeaseList">ucLeaseList</see>) and also to set the
        ///     StandingFK value. Both int and Guid data types are used as input parameters so the
        ///     <see cref="TypeServices.IEntityControl">IEntityControl</see> interface can
        ///     accommodate both data types making this more extendable and reusable.
        /// </summary>
        Guid? _guidParentId;
        /// <summary>
        ///     int Id value of the object currently acting as the parent. Used to filter the
        ///     navigation list (<see cref="ucLeaseList">ucLeaseList</see>) and also to set the
        ///     StandingFK value. Both int and Guid data types are used as input parameters so the
        ///     <see cref="TypeServices.IEntityControl">IEntityControl</see> interface can
        ///     accommodate both data types making this more extendable and reusable.
        /// </summary>
        Int32? _intParentId;
        /// <summary>
        ///     Guid Id value for the current Lease. This can be used in <see cref="ucLeaseList.FindRowById">ucLeaseList.FindRowById</see> to select this instance of
        ///     <see cref="EntityBll.Lease_Bll">Lease_ Bll</see> in the navigation list and/or can
        ///     also be used to populate <see cref="ucLeaseProps">ucProps</see>. ucProps will
        ///     either use the instance of <see cref="EntityBll.Lease_Bll">Lease_Bll</see> selected
        ///     in the navigation list (<see cref="ucLeaseList">ucLeaseList</see>), or can be used
        ///     to fetch the data from the database. Both int and Guid data types are used as input
        ///     parameters so the <see cref="TypeServices.IEntityControl">IEntityControl</see>
        ///     interface can accommodate both types making this more extendable and reusable.
        /// </summary>
        Guid? _giudCurrentId;
        /// <summary>
        ///     int Id value for the current Lease. This can be used in <see cref="ucLeaseList.FindRowById">ucLeaseList.FindRowById</see> to select this instance of
        ///     <see cref="EntityBll.Lease_Bll">Lease_ Bll</see> in the navigation list and/or can
        ///     also be used to populate <see cref="ucLeaseProps">ucProps</see>. ucProps will
        ///     either use the instance of <see cref="EntityBll.Lease_Bll">Lease_Bll</see> selected
        ///     in the navigation list (<see cref="ucLeaseList">ucLeaseList</see>), or can be used
        ///     to fetch the data from the database. Both int and Guid data types are used as input
        ///     parameters so the <see cref="TypeServices.IEntityControl">IEntityControl</see>
        ///     interface can accommodate both types making this more extendable and reusable.
        /// </summary>
        Int32? _intCurrentId;
        Guid? _parentId;

        bool _FLG_IsLoaded = false;

        List<FileStorage_Values> _files = new List<FileStorage_Values>();
        //FileStorage_List _list = new FileStorage_List();

        FileStorageService_ProxyWrapper _proxy = null;
        SaveFileDialog _saveDialog = null;
        //private OpenFileDialog _openFileDialog;

        #endregion Initialize Variables

        #region Constructors

        public ucFilePopup()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFilePopup", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();
                _proxy = new FileStorageService_ProxyWrapper();
                _proxy.GetFileStorage_lstByLinkedRecordCompleted += new EventHandler<GetFileStorage_lstByLinkedRecordCompletedEventArgs>(GetFileStorage_lstByLinkedRecordCompleted);
                _proxy.SaveFileCompleted += new EventHandler<SaveFileCompletedEventArgs>(SaveFileCompleted);
                _proxy.GetFileCompleted += new EventHandler<GetFileCompletedEventArgs>(GetFileCompleted);
                //_proxy.GetFilePathFromTempDirCompleted += GetFilePathFromTempDirCompleted;
                _proxy.FileStorage_DeleteTempFileCompleted += FileStorage_DeleteTempFileCompleted;
                //_proxy.GetFileNameCompleted += GetFileNameCompleted;
                _proxy.FileStorage_DeleteCompleted += new EventHandler<FileStorage_DeleteCompletedEventArgs>(FileStorage_DeleteCompleted);


                InitializeGrids();
                _saveDialog = new SaveFileDialog();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFilePopup", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFilePopup", IfxTraceCategory.Leave);
            }
        }



        #endregion Constructors




        #region Methods


        public void SetStateFromParent(object ancestorId, string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, IBusinessObject currentBusinessObject, object[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Enter);

                _intParentId = intParentId;
                _guidParentId = guidParentId;
                _intCurrentId = intId;
                _giudCurrentId = guidId;
                _parentType = parentType;

                if (_guidParentId == null)
                {
                    MessageBox.Show("Missing record ID.  This should not be happening and please contact support.", "Missing data", MessageBoxButton.OK);
                    return;
                }
                ConfigureUI();
                _proxy.Begin_GetFileStorage_lstByLinkedRecord((Guid)_guidParentId, _parentType);
                //_proxy.Begin_GetFileStorage_lstByLinkedXRefRecords((Guid)_guidParentId, _parentType);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Leave);
            }
        }

        void ConfigureUI()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureUI", IfxTraceCategory.Enter);

                if (_parentType == "Project")
                {
                    LayoutRoot.ColumnDefinitions[1].Width = new System.Windows.GridLength(0);
                    LayoutRoot.ColumnDefinitions[2].Width = new System.Windows.GridLength(0);
                    //txbCurrentItem.Text = "Files for the selected project.";
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureUI", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureUI", IfxTraceCategory.Leave);
            }
        }

        void UploadFile()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UploadFile", IfxTraceCategory.Enter);

                if (_guidParentId == null)
                {
                    MessageBox.Show("Missing a Record ID.  If this continues, please contact support.", "Missing data", MessageBoxButton.OK);
                    return;
                }
                OpenFileDialog opDlg = new OpenFileDialog();
                //opDlg.InitialDirectory = @"D:\Apps\SampleCode\2012-08-22 FileManager\2012-08-22 FileManager\Files";
                if ((bool)opDlg.ShowDialog())
                {
                    //open the file as a stream
                    Stream stream = opDlg.File.OpenRead();
                    //put the stream in the BinaryReader
                    BinaryReader binary = new BinaryReader(stream);
                    //Read bytes from the BinaryReader and put them into a byte array.
                    Byte[] imgB = binary.ReadBytes((int)stream.Length);
                    string filelen = imgB.Length.ToString();

                    // See if the file already exists searching for it in the grid
                    FileStorage_Values fileRow = GetFileInfoFrom_XdgFileList(opDlg.File.ToString());
                    if (fileRow != null)
                    {
                        MessageBoxResult result = MessageBox.Show("The file:  " + opDlg.File.ToString() + "  already exists.  Do you want to overwrite it?", "Overwrite File", MessageBoxButton.OKCancel);
                        if (result == MessageBoxResult.Cancel)
                        {
                            return;
                        }

                        fileRow.FS_FileSize_noevents = opDlg.File.Length;
                        fileRow.IsNew_noevents = false;
                        fileRow.FileData_noevents = imgB;

                    }
                    else
                    {
                        fileRow = FileStorage_Values.GetDefault();

                        fileRow.FS_FileName_noevents = opDlg.File.ToString();
                        //Note:   _parentType  is a string and not an enum because its passed in from SetStateFromParent which is normaly codegenned and part of an interface - in which case the enum cant be defined ahead of time.
                        fileRow.FS_ParentType_noevents = _parentType;
                        switch (_parentType)
                        {
                            case "Project":
                                fileRow.FS_Pj_Id_noevents = (Guid)_guidParentId;
                                break;
                            case "Lease":
                                fileRow.FS_Ls_Id_noevents = (Guid)_guidParentId;
                                break;
                            case "Contract":
                                fileRow.FS_Ct_Id_noevents = (Guid)_guidParentId;
                                break;
                            case "Well":
                                fileRow.FS_Wl_Id_noevents = (Guid)_guidParentId;
                                break;
                            case "Obligation":
                                fileRow.FS_Ob_Id_noevents = (Guid)_guidParentId;
                                break;
                            case "Defect":
                                fileRow.FS_Df_Id_noevents = (Guid)_guidParentId;
                                break;
                            case "Task":
                                fileRow.FS_Tk_Id_noevents = (Guid)_guidParentId;
                                break;
                        }
                        fileRow.FS_FileSize_noevents = opDlg.File.Length;
                        fileRow.IsNew_noevents = true;
                        fileRow.FileData_noevents = imgB;

                    }


                    byte[] data = Serialization.SilverlightSerializer.Serialize(fileRow.GetValues());
                    _proxy.Begin_SaveFile(data);


                    //if (imgB != null)
                    //{
                    //    FileWireObject obj = new FileWireObject(opDlg.File.ToString(), _parentType, (Guid)_guidParentId, imgB, (Guid)Credentials.UserId, opDlg.File.Length);
                    //    byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                    //    _proxy.Begin_SaveFile(data);
                    //}
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UploadFile", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UploadFile", IfxTraceCategory.Leave);
            }
        }

        //private FileStorage_Values GetDefault()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        Guid Id = Guid.NewGuid();
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Enter);
        //        return new FileStorage_Values(new object[] 
        //            {
        //                Id,    //  FS_Id
        //                null,		//  FS_Cust_Id
        //                null,		//  FS_Or_Id
        //                null,		//  FS_Tk_Id
        //                null,		//  FS_DscCm_Id
        //                null,		//  FS_ParentType
        //                null,		//  FS_FileName
        //                null,		//  FS_FilePath
        //                null,		//  FS_FileSize
        //                null,		//  FS_XrefIdentifier
        //                false,		//  IsNew
        //                null,		//  FileData
        //                Credentials.UserId,		//  FS_UserId
        //                null,		//  UserName
        //                null,		//  FS_CreatedDate
        //                null,		//  FS_LastModifiedDate
        //                null,		//  FS_Stamp
        //            }, null
        //        );
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Leave);
        //    }
        //}


        #region Grids


        FileStorage_Values GetFileInfoFrom_XdgFileList(string fileName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileFoundInGrid", IfxTraceCategory.Enter);

                foreach (Row rw in XdgFileList.Rows)
                {
                    if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        if (((FileStorage_Values)rw.Data).FS_FileName == fileName)
                        {
                            return (FileStorage_Values)rw.Data;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileFoundInGrid", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileFoundInGrid", IfxTraceCategory.Leave);
            }
        }

        bool FileFoundInGrid(string fileName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileFoundInGrid", IfxTraceCategory.Enter);

                foreach (Row rw in XdgFileList.Rows)
                {
                    if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        if (((FileStorage_Values)rw.Data).FS_FileName == fileName)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileFoundInGrid", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileFoundInGrid", IfxTraceCategory.Leave);
            }
        }

        void InitializeGrids()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeGrids", IfxTraceCategory.Enter);

                XdgFileList.RowSelectorSettings.EnableRowNumbering = true;
                XdgFileList.IsAlternateRowsEnabled = true;
                XdgFileList.SelectionSettings.CellSelection = SelectionType.None;
                XdgFileList.SelectionSettings.RowSelection = SelectionType.Single;
                XdgFileList.SelectionSettings.CellClickAction = CellSelectionAction.SelectRow;
                XdgFileList.RowSelectorSettings.EnableRowNumbering = true;
                XdgFileList.IsAlternateRowsEnabled = true;
                XdgFileList.RowHover = RowHoverType.Row;
                XdgFileList.HeaderVisibility = System.Windows.Visibility.Collapsed;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeGrids", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeGrids", IfxTraceCategory.Leave);
            }
        }


        #endregion Grids


        #endregion Methods

        public XamDialogWindow ParentWindow
        {
            get { return _parentWindow; }
            set { _parentWindow = value; }
        }


        #region Button Events

        private void btnOpenFile_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenFile_Click", IfxTraceCategory.Enter);

                var fileStorageValues = e.Data as FileStorage_Values;
                if (fileStorageValues != null)
                {
                    FilesHelper.OpenFileInBrowser(fileStorageValues);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenFile_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenFile_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnDownloadFile_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDownloadFile_Click", IfxTraceCategory.Enter);

                var fileStorageValues = e.Data as FileStorage_Values;
                if (fileStorageValues != null)
                {
                    FilesHelper.DownloadFile(fileStorageValues);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDownloadFile_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDownloadFile_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnDeleteFile_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDeleteFile_Click", IfxTraceCategory.Enter);

                var fileStorageValues = e.Data as FileStorage_Values;
                if (fileStorageValues != null)
                {
                    _proxy.Begin_FileStorage_Delete(fileStorageValues.GetValues());
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDeleteFile_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDeleteFile_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnUploadFile_Click", IfxTraceCategory.Enter);

                UploadFile();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnUploadFile_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnUploadFile_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnClose_Click", IfxTraceCategory.Enter);

                ClosePopUpEventHandler handler = ClosePopup;
                if (handler != null)
                {
                    handler(this, new ClosePopUpArgs());
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnClose_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnClose_Click", IfxTraceCategory.Leave);
            }
        }

        #endregion Button Events






        #region Web Service Returns
        
        void GetFileStorage_lstByLinkedRecordCompleted(object sender, GetFileStorage_lstByLinkedRecordCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedRecordCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                XdgFileList.ItemsSource = null;
                if (data == null) { return; }

                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array == null) { return; }
           

                _files.Clear();
                for (int i = 0; i < array.GetUpperBound(0) + 1; i++)
                {
                    FileStorage_Values file = new FileStorage_Values((object[])array[i], null);
                    _files.Add(file);
                }
                XdgFileList.ItemsSource = _files;
            }   
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedRecordCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedRecordCompleted", IfxTraceCategory.Leave);
            }
        }
        
        void SaveFileCompleted(object sender, SaveFileCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFileCompleted", IfxTraceCategory.Enter);
                //byte[] data = e.Result;
                //object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                object[] data = e.Result;
                if (data == null)
                {
                    MessageBox.Show("The upload operation may have failed.  If this continues, please contact support.", "Error", MessageBoxButton.OK);
                    return;
                }
                int? success = data[0] as int?;
                if (success == null || success == 0)
                {
                    MessageBox.Show("The upload operation may have failed.  If this continues, please contact support.", "Error", MessageBoxButton.OK);
                    return;
                }
                _proxy.Begin_GetFileStorage_lstByLinkedRecord((Guid)_guidParentId, _parentType);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFileCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFileCompleted", IfxTraceCategory.Leave);
            }
        }
        
        void GetFileCompleted(object sender, GetFileCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                string msg = "";
                if (array == null)
                {
                    msg = "The server failed to return the file.  If this problem continues, please contact support.";
                    IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", new Exception(msg + ";  e.Result returned no data."));
                    MessageBox.Show(msg, "Download Problem", MessageBoxButton.OK);
                    return;
                }
                FileStorage_Values obj = new FileStorage_Values(array, null);
                if (obj.FileData == null)
                {
                    msg = "The file failed to download from the server.  If this problem continues, please contact support.";
                    IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", new Exception(msg + ";  FileStorage_Values.FileData was null."));
                    MessageBox.Show(msg, "Download Problem", MessageBoxButton.OK);
                    return;
                }


                byte[] fileBytes = obj.FileData;

                using (Stream fs = (Stream)this._saveDialog.OpenFile())
                {
                    fs.Write(fileBytes, 0, fileBytes.Length);
                    fs.Close();
                    //this.tblError.Text = "File successfully saved!";
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", IfxTraceCategory.Leave);
            }
        }
        
        //void GetFilePathFromTempDirCompleted(object sender, GetFilePathFromTempDirCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilePathFromTempDirCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;

        //        if (data == null)
        //        {
        //            MessageBox.Show("File not found on server.", "", MessageBoxButton.OK);
        //            return;
        //        }



        //        string[] returnStirngs = Serialization.SilverlightSerializer.Deserialize(data) as string[];
        //        string msg = "";
        //        if (data == null || returnStirngs[0] == null || returnStirngs[1] == null || returnStirngs[2] == null || returnStirngs[3] == null)
        //        {
        //            msg = "The server failed to return information.  If this problem continues, please contact support.";
        //            IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", new Exception(msg + ";  e.Result returned no data."));
        //            MessageBox.Show(msg, "Download Problem", MessageBoxButton.OK);
        //            return;
        //        }

        //        OpenFileInBrowser(returnStirngs[0], returnStirngs[1], returnStirngs[2], returnStirngs[3]);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilePathFromTempDirCompleted", ex);
        //        IfxWrapperException.GetError(ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilePathFromTempDirCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        void FileStorage_DeleteTempFileCompleted(object sender, FileStorage_DeleteTempFileCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteTempFileCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                int? success = array[0] as int?;
                if (success == 0)
                {
                    MessageBox.Show("There was a problem deleting the file on the server.  If this continues, please contact support.", "Error", MessageBoxButton.OK);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteTempFileCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteTempFileCompleted", IfxTraceCategory.Leave);
            }
        }
        
        void FileStorage_DeleteCompleted(object sender, FileStorage_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                int? success = array[0] as int?;
                if (success == 0)
                {
                    MessageBox.Show("There was a problem deleting the file on the server.  If this continues, please contact support.", "Error", MessageBoxButton.OK);

                }
                _proxy.Begin_GetFileStorage_lstByLinkedRecord((Guid)_guidParentId, _parentType);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }




        #endregion Web Service Returns


    }
}
