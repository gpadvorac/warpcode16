﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Ifx.SL;
using System.ComponentModel;
using System.IO;
using TypeServices;
using ProxyWrapper;
using vUICommon;
using EntityWireTypeSL;
using ApplicationTypeServices;
using WireTypes;
using Velocity.SL;
using Infragistics.Controls.Grids;
using System.Windows.Browser;
using Infragistics.Controls.Interactions;

namespace UIControls
{
    public partial class ucFiles : UserControl
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "ucFiles";

        /// <summary>
        ///     A string value naming the current parent's type. It’s possible for an Entity
        ///     Manager to have many different parent types while others have only one or none at
        ///     all. For example, an <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/The_Entity_Manager.html">Entity Manager</a> for an Address entity could have a parent type
        ///     of Customer, Company, Person, and so on. There may be situations where different
        ///     logic or filters need to be applied depending on the type of parent. This helps
        ///     make the <see cref="TypeServices.IEntityControl">IEntityControl</see> interface
        ///     more extendable and reusable.
        /// </summary>
        string _parentType;
        /// <summary>
        ///     Guid Id value of the object currently acting as the parent. Used to filter the
        ///     navigation list (<see cref="ucLeaseList">ucLeaseList</see>) and also to set the
        ///     StandingFK value. Both int and Guid data types are used as input parameters so the
        ///     <see cref="TypeServices.IEntityControl">IEntityControl</see> interface can
        ///     accommodate both data types making this more extendable and reusable.
        /// </summary>
        Guid? _guidParentId;
        /// <summary>
        ///     int Id value of the object currently acting as the parent. Used to filter the
        ///     navigation list (<see cref="ucLeaseList">ucLeaseList</see>) and also to set the
        ///     StandingFK value. Both int and Guid data types are used as input parameters so the
        ///     <see cref="TypeServices.IEntityControl">IEntityControl</see> interface can
        ///     accommodate both data types making this more extendable and reusable.
        /// </summary>
        Int32? _intParentId;
        /// <summary>
        ///     Guid Id value for the current Lease. This can be used in <see cref="ucLeaseList.FindRowById">ucLeaseList.FindRowById</see> to select this instance of
        ///     <see cref="EntityBll.Lease_Bll">Lease_ Bll</see> in the navigation list and/or can
        ///     also be used to populate <see cref="ucLeaseProps">ucProps</see>. ucProps will
        ///     either use the instance of <see cref="EntityBll.Lease_Bll">Lease_Bll</see> selected
        ///     in the navigation list (<see cref="ucLeaseList">ucLeaseList</see>), or can be used
        ///     to fetch the data from the database. Both int and Guid data types are used as input
        ///     parameters so the <see cref="TypeServices.IEntityControl">IEntityControl</see>
        ///     interface can accommodate both types making this more extendable and reusable.
        /// </summary>
        Guid? _giudCurrentId;
        /// <summary>
        ///     int Id value for the current Lease. This can be used in <see cref="ucLeaseList.FindRowById">ucLeaseList.FindRowById</see> to select this instance of
        ///     <see cref="EntityBll.Lease_Bll">Lease_ Bll</see> in the navigation list and/or can
        ///     also be used to populate <see cref="ucLeaseProps">ucProps</see>. ucProps will
        ///     either use the instance of <see cref="EntityBll.Lease_Bll">Lease_Bll</see> selected
        ///     in the navigation list (<see cref="ucLeaseList">ucLeaseList</see>), or can be used
        ///     to fetch the data from the database. Both int and Guid data types are used as input
        ///     parameters so the <see cref="TypeServices.IEntityControl">IEntityControl</see>
        ///     interface can accommodate both types making this more extendable and reusable.
        /// </summary>
        Int32? _intCurrentId;
        Guid? _parentId;

        bool _FLG_IsLoaded = false;

        XRexGroups _groups = new XRexGroups();
        List<FileStorage_Values> _files = new List<FileStorage_Values>();

        FileStorageService_ProxyWrapper _proxy = null;
        SaveFileDialog _saveDialog = null;
        //private OpenFileDialog _openFileDialog;



        #endregion Initialize Variables


        #region Constructors

        public ucFiles()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFiles", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();
                _proxy = new FileStorageService_ProxyWrapper();
                _proxy.GetFileStorage_lstByLinkedRecordCompleted += new EventHandler<GetFileStorage_lstByLinkedRecordCompletedEventArgs>(GetFileStorage_lstByLinkedRecordCompleted);
                _proxy.GetFileStorage_lstByLinkedXRefRecordsCompleted += new EventHandler<GetFileStorage_lstByLinkedXRefRecordsCompletedEventArgs>(GetFileStorage_lstByLinkedXRefRecordsCompleted);
                _proxy.SaveFileCompleted += new EventHandler<SaveFileCompletedEventArgs>(SaveFileCompleted);
                _proxy.GetFileCompleted += new EventHandler<GetFileCompletedEventArgs>(GetFileCompleted);
                _proxy.GetFilePathFromTempDirCompleted += GetFilePathFromTempDirCompleted;
                _proxy.FileStorage_DeleteTempFileCompleted += FileStorage_DeleteTempFileCompleted;
                //_proxy.GetFileNameCompleted += GetFileNameCompleted;
                _proxy.FileStorage_DeleteCompleted += new EventHandler<FileStorage_DeleteCompletedEventArgs>(FileStorage_DeleteCompleted);
                InitializeGrids();
                 _saveDialog = new SaveFileDialog();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFiles", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucFiles", IfxTraceCategory.Leave);
            }
        }

   


        #endregion Constructors


        #region Properties


        public XRexGroups Groups
        {
            get { return _groups; }
            set { _groups = value; }
        }


        #endregion Properties


        #region Methods


        public void SetStateFromParent(object ancestorId, string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, IBusinessObject currentBusinessObject, object[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Enter);

                _intParentId = intParentId;
                _guidParentId = guidParentId;
                _intCurrentId = intId;
                _giudCurrentId = guidId;
                _parentType = parentType;

                if (_guidParentId == null)
                {
                    MessageBox.Show("Missing record ID.  This should not be happening and please contact support.", "Missing data", MessageBoxButton.OK);
                    return;
                }
                ConfigureUI();
                _proxy.Begin_GetFileStorage_lstByLinkedRecord((Guid)_guidParentId, _parentType);
                _proxy.Begin_GetFileStorage_lstByLinkedXRefRecords((Guid)_guidParentId, _parentType);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Leave);
            }
        }

        void ConfigureUI()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureUI", IfxTraceCategory.Enter);

                if (_parentType == "Project")
                {
                    LayoutRoot.ColumnDefinitions[1].Width = new System.Windows.GridLength(0);
                    LayoutRoot.ColumnDefinitions[2].Width = new System.Windows.GridLength(0);
                    txbCurrentItem.Text = "Files for the selected project.";
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureUI", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureUI", IfxTraceCategory.Leave);
            }
        }
        
        void UploadFile()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UploadFile", IfxTraceCategory.Enter);

                if (_guidParentId == null)
                {
                    MessageBox.Show("Missing a Record ID.  If this continues, please contact support.", "Missing data", MessageBoxButton.OK);
                    return;
                }
                OpenFileDialog opDlg = new OpenFileDialog();
                //opDlg.InitialDirectory = @"D:\Apps\SampleCode\2012-08-22 FileManager\2012-08-22 FileManager\Files";
                if ((bool)opDlg.ShowDialog())
                {
                    //open the file as a stream
                    Stream stream = opDlg.File.OpenRead();
                    //put the stream in the BinaryReader
                    BinaryReader binary = new BinaryReader(stream);
                    //Read bytes from the BinaryReader and put them into a byte array.
                    Byte[] imgB = binary.ReadBytes((int)stream.Length);
                    string filelen = imgB.Length.ToString();

                    // See if the file already exists searching for it in the grid
                    FileStorage_Values fileRow = GetFileInfoFrom_xgdCurrentRow(opDlg.File.ToString());
                    if (fileRow != null)
                    {
                        MessageBoxResult result = MessageBox.Show("The file:  " + opDlg.File.ToString() + "  already exists.  Do you want to overwrite it?", "Overwrite File", MessageBoxButton.OKCancel);
                        if (result == MessageBoxResult.Cancel)
                        {
                            return;
                        }

                        fileRow.FS_FileSize_noevents = opDlg.File.Length;
                        fileRow.IsNew_noevents = false;
                        fileRow.FileData_noevents = imgB;

                    }
                    else
                    {
                        fileRow = FileStorage_Values.GetDefault();

                        fileRow.FS_FileName_noevents = opDlg.File.ToString();
                        fileRow.FS_ParentType_noevents = _parentType;
                        fileRow.FS_ParentId_Guid_noevents = _guidParentId;
                        fileRow.FS_ParentId_Object_noevents = _guidParentId;
                        //switch (_parentType)
                        //{
                        //    case "Project":
                        //        fileRow.FS_Pj_Id_noevents = (Guid)_guidParentId;
                        //        break;
                        //    case "Lease":
                        //        fileRow.FS_Ls_Id_noevents = (Guid)_guidParentId;
                        //        break;
                        //    case "Contract":
                        //        fileRow.FS_Ct_Id_noevents = (Guid)_guidParentId;
                        //        break;
                        //    case "Well":
                        //        fileRow.FS_Wl_Id_noevents = (Guid)_guidParentId;
                        //        break;
                        //    case "Obligation":
                        //        fileRow.FS_Ob_Id_noevents = (Guid)_guidParentId;
                        //        break;
                        //    case "Defect":
                        //        fileRow.FS_Df_Id_noevents = (Guid)_guidParentId;
                        //        break;
                        //    case "Task":
                        //        fileRow.FS_Tk_Id_noevents = (Guid)_guidParentId;
                        //        break;
                        //}
                        fileRow.FS_FileSize_noevents = opDlg.File.Length;
                        fileRow.IsNew_noevents = true;
                        fileRow.FileData_noevents = imgB;

                    }


                    byte[] data = Serialization.SilverlightSerializer.Serialize(fileRow.GetValues());
                    _proxy.Begin_SaveFile(data);


                    //if (imgB != null)
                    //{
                    //    FileWireObject obj = new FileWireObject(opDlg.File.ToString(), _parentType, (Guid)_guidParentId, imgB, (Guid)Credentials.UserId, opDlg.File.Length);
                    //    byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                    //    _proxy.Begin_SaveFile(data);
                    //}
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UploadFile", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UploadFile", IfxTraceCategory.Leave);
            }
        }

        //private FileStorage_Values GetDefault()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        Guid Id = Guid.NewGuid();
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Enter);
        //        return new FileStorage_Values(new object[] 
        //            {
        //                Id,    //  FS_Id
        //                null,		//  FS_Pj_Id
        //                null,		//  FS_Ls_Id
        //                null,		//  FS_Ct_Id
        //                null,		//  FS_Wl_Id
        //                null,		//  FS_Ob_Id
        //                null,		//  FS_Df_Id
        //                null,		//  FS_Tk_Id
        //                null,		//  FS_ParentType
        //                null,		//  FS_FileName
        //                null,		//  FS_FilePath
        //                null,		//  FS_FileSize
        //                null,		//  FS_XrefIdentifier
        //                false,		//  IsNew
        //                null,		//  FileData
        //                Credentials.UserId,		//  FS_UserId
        //                null,		//  UserName
        //                null,		//  FS_CreatedDate
        //                null,		//  FS_LastModifiedDate
        //                null,		//  FS_Stamp
        //            }, null
        //        );
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Leave);
        //    }
        //}


        #region Grids
        

        void LoadGroups(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateListOfGoups", IfxTraceCategory.Enter);

                //List<XRexGroup> groups = new List<XRexGroup>();
                List<FileStorage_Values> files = new List<FileStorage_Values>();
                _groups.Clear();
                //Project
                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                {
                    FileStorage_Values file = new FileStorage_Values((object[])data[i], null);
                    if (file.FS_ParentType == FileStorageParentType.Project.ToString())
                    {
                        files.Add(file);
                    }
                }
                if (files.Count > 0)
                {
                    _groups.Add(new XRexGroup(FileStorageParentType.Project.ToString(), files));

                }

                //Lease
                files = new List<FileStorage_Values>();
                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                {
                    FileStorage_Values file = new FileStorage_Values((object[])data[i], null);
                    if (file.FS_ParentType == FileStorageParentType.Lease.ToString())
                    {
                        files.Add(file);
                    }
                }
                if (files.Count > 0)
                {
                    //_groups.Add(new XRexGroup(FileStorageParentType.Lease.ToString(), files));
                    _groups.Add(new XRexGroup("Agreements", files));
                }

                //Contract
                files = new List<FileStorage_Values>();
                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                {
                    FileStorage_Values file = new FileStorage_Values((object[])data[i], null);
                    if (file.FS_ParentType == FileStorageParentType.Contract.ToString())
                    {
                        files.Add(file);
                    }
                }
                if (files.Count > 0)
                {
                    //_groups.Add(new XRexGroup(FileStorageParentType.Contract.ToString(), files));
                    _groups.Add(new XRexGroup("Sections", files));
                }

                //Well
                files = new List<FileStorage_Values>();
                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                {
                    FileStorage_Values file = new FileStorage_Values((object[])data[i], null);
                    if (file.FS_ParentType == FileStorageParentType.Well.ToString())
                    {
                        files.Add(file);
                    }
                }
                if (files.Count > 0)
                {
                    //_groups.Add(new XRexGroup(FileStorageParentType.Well.ToString(), files));
                    _groups.Add(new XRexGroup("Pipelines", files));
                }

                //Obligation
                files = new List<FileStorage_Values>();
                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                {
                    FileStorage_Values file = new FileStorage_Values((object[])data[i], null);
                    if (file.FS_ParentType == FileStorageParentType.Obligation.ToString())
                    {
                        files.Add(file);
                    }
                }
                if (files.Count > 0)
                {
                    //_groups.Add(new XRexGroup(FileStorageParentType.Obligation.ToString(), files));
                    _groups.Add(new XRexGroup("Obligations/Provisions", files));
                }

                //Defect
                files = new List<FileStorage_Values>();
                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                {
                    FileStorage_Values file = new FileStorage_Values((object[])data[i], null);
                    if (file.FS_ParentType == FileStorageParentType.Defect.ToString())
                    {
                        files.Add(file);
                    }
                }
                if (files.Count > 0)
                {
                    //_groups.Add(new XRexGroup(FileStorageParentType.Defect.ToString(), files));
                    _groups.Add(new XRexGroup("Issues", files));
                }

                //SurveyMap
                files = new List<FileStorage_Values>();
                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                {
                    FileStorage_Values file = new FileStorage_Values((object[])data[i], null);
                    if (file.FS_ParentType == FileStorageParentType.SurveyMap.ToString())
                    {
                        files.Add(file);
                    }
                }
                if (files.Count > 0)
                {
                    //_groups.Add(new XRexGroup(FileStorageParentType.SurveyMap.ToString(), files));
                    _groups.Add(new XRexGroup("Surveys/Maps", files));
                }

                //RelatedContract
                files = new List<FileStorage_Values>();
                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                {
                    FileStorage_Values file = new FileStorage_Values((object[])data[i], null);
                    if (file.FS_ParentType == FileStorageParentType.RelatedContract.ToString())
                    {
                        files.Add(file);
                    }
                }
                if (files.Count > 0)
                {
                    //_groups.Add(new XRexGroup(FileStorageParentType.RelatedContract.ToString(), files));
                    _groups.Add(new XRexGroup("Related Contracts", files));
                }

                //Task
                files = new List<FileStorage_Values>();
                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                {
                    FileStorage_Values file = new FileStorage_Values((object[])data[i], null);
                    if (file.FS_ParentType == FileStorageParentType.Task.ToString())
                    {
                        files.Add(file);
                    }
                }
                if (files.Count > 0)
                {
                    //_groups.Add(new XRexGroup(FileStorageParentType.Task.ToString(), files));
                    _groups.Add(new XRexGroup("Tasks", files));
                }

                //DiscussionComment
                files = new List<FileStorage_Values>();
                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                {
                    FileStorage_Values file = new FileStorage_Values((object[])data[i], null);
                    if (file.FS_ParentType == FileStorageParentType.DiscussionComment.ToString())
                    {
                        files.Add(file);
                    }
                }
                if (files.Count > 0)
                {
                    //_groups.Add(new XRexGroup(FileStorageParentType.DiscussionComment.ToString(), files));
                    _groups.Add(new XRexGroup("Discussion Comments", files));
                }


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateListOfGoups", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateListOfGoups", IfxTraceCategory.Leave);
            }
        }

        FileStorage_Values GetFileInfoFrom_xgdCurrentRow(string fileName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileFoundInGrid", IfxTraceCategory.Enter);

                foreach (Row rw in xgdCurrentRow.Rows)
                {
                    if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        if (((FileStorage_Values)rw.Data).FS_FileName == fileName)
                        {
                            return (FileStorage_Values)rw.Data;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileFoundInGrid", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileFoundInGrid", IfxTraceCategory.Leave);
            }
        }

        bool FileFoundInGrid(string fileName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileFoundInGrid", IfxTraceCategory.Enter);

                foreach (Row rw in xgdCurrentRow.Rows)
                {
                    if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        if (((FileStorage_Values)rw.Data).FS_FileName == fileName)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileFoundInGrid", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileFoundInGrid", IfxTraceCategory.Leave);
            }
        }
        
        void InitializeGrids()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeGrids", IfxTraceCategory.Enter);
                
                xgdCurrentRow.RowSelectorSettings.EnableRowNumbering = true;
                xgdCurrentRow.IsAlternateRowsEnabled = true;
                xgdCurrentRow.SelectionSettings.CellSelection = SelectionType.None;
                xgdCurrentRow.SelectionSettings.RowSelection = SelectionType.Single;
                xgdCurrentRow.SelectionSettings.CellClickAction = CellSelectionAction.SelectRow;
                xgdCurrentRow.RowSelectorSettings.EnableRowNumbering = true;
                xgdCurrentRow.IsAlternateRowsEnabled = true;


                xgdXRefRows.RowSelectorSettings.EnableRowNumbering = true;
                xgdXRefRows.IsAlternateRowsEnabled = true;
                xgdXRefRows.SelectionSettings.CellSelection = SelectionType.None;
                xgdXRefRows.SelectionSettings.RowSelection = SelectionType.Single;
                xgdXRefRows.SelectionSettings.CellClickAction = CellSelectionAction.SelectRow;
                xgdXRefRows.RowSelectorSettings.EnableRowNumbering = true;
                xgdXRefRows.IsAlternateRowsEnabled = true;
                
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeGrids", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeGrids", IfxTraceCategory.Leave);
            }
        }
        

        #endregion Grids


        #endregion Methods


        #region Events


        private void btnUploadFile_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnUploadFile_Click", IfxTraceCategory.Enter);

                UploadFile();
            
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnUploadFile_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnUploadFile_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnDownoadFile_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDownoadFile_Click", IfxTraceCategory.Enter);

                FileStorage_Values row = null;
                foreach (Row rw in xgdCurrentRow.Rows)
                {
                    if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        if (rw.IsSelected)
                        {
                            row = rw.Data as FileStorage_Values;
                            break;
                        }
                    }
                }
                if (row == null)
                {
                    MessageBox.Show("Please select a file from the grid first.", "Missing Selection", MessageBoxButton.OK);
                    return;
                }

                _saveDialog.DefaultFileName = row.FS_FileName;
                var result = _saveDialog.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    _proxy.Begin_GetFile(row.GetValues());
                }


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDownoadFile_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDownoadFile_Click", IfxTraceCategory.Leave);
            }
        }



        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenFile_Click", IfxTraceCategory.Enter);

                FileStorage_Values row = null;
                foreach (Row rw in xgdCurrentRow.Rows)
                {
                    if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        if (rw.IsSelected)
                        {
                            row = rw.Data as FileStorage_Values;
                            break;
                        }
                    }
                }
                if (row == null)
                {
                    MessageBox.Show("Please select a file from the grid first.", "Missing Selection", MessageBoxButton.OK);
                    return;
                }

                //_proxy.Begin_GetFileName(row.GetValues());
                _proxy.Begin_GetFilePathFromTempDir(row.GetValues());

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenFile_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenFile_Click", IfxTraceCategory.Leave);
            }
        }




        private void btnDownoadXRefFile_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDownoadXRefFile_Click", IfxTraceCategory.Enter);

                FileStorage_Values row = null;
                foreach (Row gpRow in xgdXRefRows.Rows)
                {
                    if (gpRow.RowType == Infragistics.Controls.Grids.RowType.DataRow && gpRow.ChildBands.Count > 0)
                    {
                        XRexGroup gp = gpRow.Data as XRexGroup;
                        if (gp != null)
                        {
                            foreach (Row rw in gpRow.ChildBands[0].Rows)
                            {
                                if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                                {
                                    if (rw.IsSelected)
                                    {
                                        row = rw.Data as FileStorage_Values;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (row != null) { break; }
                }
                if (row == null)
                {
                    MessageBox.Show("Please select a file from the grid first.", "Missing Selection", MessageBoxButton.OK);
                    return;
                }
                _saveDialog.DefaultFileName = row.FS_FileName;
                var result = _saveDialog.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    _proxy.Begin_GetFile(row.GetValues());
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDownoadXRefFile_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDownoadXRefFile_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnOpenXRefFile_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenXRefFile_Click", IfxTraceCategory.Enter);

                FileStorage_Values row = null;
                foreach (Row rw in xgdXRefRows.SelectionSettings.SelectedRows)
                {
                    if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        if (rw.IsSelected)
                        {
                            row = rw.Data as FileStorage_Values;
                            break;
                        }
                    }
                }
                if (row == null)
                {
                    MessageBox.Show("Please select a file from the grid first.", "Missing Selection", MessageBoxButton.OK);
                    return;
                }

                _proxy.Begin_GetFilePathFromTempDir(row.GetValues());

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenXRefFile_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenXRefFile_Click", IfxTraceCategory.Leave);
            }
        }



        private void btnDeleteFile_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDeleteFile_Click", IfxTraceCategory.Enter);


                if (MessageBox.Show("Are you sure you want to delete this file?  You will not be able to recover the file if you continue.", "Delete Warning", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                {
                    return;
                }

                FileStorage_Values row = null;
                foreach (Row rw in xgdCurrentRow.Rows)
                {
                    if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        if (rw.IsSelected)
                        {
                            row = rw.Data as FileStorage_Values;
                            break;
                        }
                    }
                }
                if (row == null)
                {
                    MessageBox.Show("Please select a file from the grid first.", "Missing Selection", MessageBoxButton.OK);
                    return;
                }
                _proxy.Begin_FileStorage_Delete(row.GetValues());
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDeleteFile_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDeleteFile_Click", IfxTraceCategory.Leave);
            }
        }



        #endregion Events


        #region Web Service Returns

        void GetFileStorage_lstByLinkedRecordCompleted(object sender, GetFileStorage_lstByLinkedRecordCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedRecordCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                xgdCurrentRow.ItemsSource = null;
                if (array == null)
                {
                    return;
                }

                _files.Clear();
                for (int i = 0; i < array.GetUpperBound(0) + 1; i++)
                {
                    FileStorage_Values file = new FileStorage_Values((object[])array[i], null);
                    _files.Add(file);
                }
                xgdCurrentRow.ItemsSource = _files;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedRecordCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedRecordCompleted", IfxTraceCategory.Leave);
            }
        }


        void GetFileStorage_lstByLinkedXRefRecordsCompleted(object sender, GetFileStorage_lstByLinkedXRefRecordsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedXRefRecordsCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];



                if (array == null)
                {
                    xgdXRefRows.ItemsSource = null;
                    return;
                }
                LoadGroups(array);
                xgdXRefRows.ItemsSource = null;
                xgdXRefRows.ItemsSource = _groups;




            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedXRefRecordsCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedXRefRecordsCompleted", IfxTraceCategory.Leave);
            }
        }
        

        void SaveFileCompleted(object sender, SaveFileCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFileCompleted", IfxTraceCategory.Enter);
                //byte[] data = e.Result;
                //object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                object[] data = e.Result;
                if (data == null)
                {
                    MessageBox.Show("The upload operation may have failed.  If this continues, please contact support.", "Error", MessageBoxButton.OK);
                    return;
                }
                int? success = data[0] as int?;
                if (success == null || success == 0)
                {
                    MessageBox.Show("The upload operation may have failed.  If this continues, please contact support.", "Error", MessageBoxButton.OK);
                    return;
                }
                _proxy.Begin_GetFileStorage_lstByLinkedRecord((Guid)_guidParentId, _parentType);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFileCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFileCompleted", IfxTraceCategory.Leave);
            }
        }

        
        void GetFileCompleted(object sender, GetFileCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                string msg = "";
                if (array == null)
                {
                    msg = "The server failed to return the file.  If this problem continues, please contact support.";
                     IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", new  Exception(msg + ";  e.Result returned no data."));
                     MessageBox.Show(msg, "Download Problem", MessageBoxButton.OK);
                     return;
                }
                FileStorage_Values obj = new FileStorage_Values(array, null);
                if (obj.FileData == null)
                {
                    msg = "The file failed to download from the server.  If this problem continues, please contact support.";
                    IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", new Exception(msg + ";  FileStorage_Values.FileData was null."));
                    MessageBox.Show(msg, "Download Problem", MessageBoxButton.OK);
                    return;
                }


                byte[] fileBytes = obj.FileData;

                using (Stream fs = (Stream)this._saveDialog.OpenFile())
                {
                    fs.Write(fileBytes, 0, fileBytes.Length);
                    fs.Close();
                    //this.tblError.Text = "File successfully saved!";
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", IfxTraceCategory.Leave);
            }
        }



        //void GetFileNameCompleted(object sender, GetFileNameCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileNameCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;



        //        string[] returnStirngs = Serialization.SilverlightSerializer.Deserialize(data) as string[];
        //        string msg = "";
        //        if (data == null || returnStirngs[0] == null || returnStirngs[1] == null)
        //        {
        //            msg = "The server failed to return information.  If this problem continues, please contact support.";
        //            IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", new Exception(msg + ";  e.Result returned no data."));
        //            MessageBox.Show(msg, "Download Problem", MessageBoxButton.OK);
        //            return;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileNameCompleted", ex);
        //        IfxWrapperException.GetError(ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileNameCompleted", IfxTraceCategory.Leave);
        //    }
        //}




        void GetFilePathFromTempDirCompleted(object sender, GetFilePathFromTempDirCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilePathFromTempDirCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                string msg = "";
                if (data == null )
                {
                    msg = "The server failed to return information.  If this problem continues, please contact support.";
                    IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", new Exception(msg + ";  e.Result returned no data."));
                    MessageBox.Show(msg, "Download Problem", MessageBoxButton.OK);
                    return;
                }

                string[] returnStirngs = Serialization.SilverlightSerializer.Deserialize(data) as string[];
                
                if (returnStirngs==null || returnStirngs[0] == null || returnStirngs[1] == null || returnStirngs[2] == null || returnStirngs[3] == null)
                {
                    msg = "The server failed to return information.  If this problem continues, please contact support.";
                    IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileCompleted", new Exception(msg + ";  e.Result returned no data."));
                    MessageBox.Show(msg, "Download Problem", MessageBoxButton.OK);
                    return;
                }

                OpenFileInBrowser(returnStirngs[0], returnStirngs[1], returnStirngs[2], returnStirngs[3]);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilePathFromTempDirCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilePathFromTempDirCompleted", IfxTraceCategory.Leave);
            }
        }

        void FileStorage_DeleteTempFileCompleted(object sender, FileStorage_DeleteTempFileCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteTempFileCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                int? success = array[0] as int?;
                if (success == 0)
                {
                    MessageBox.Show("There was a problem deleting the file on the server.  If this continues, please contact support.", "Error", MessageBoxButton.OK);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteTempFileCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteTempFileCompleted", IfxTraceCategory.Leave);
            }
        }


        void FileStorage_DeleteCompleted(object sender, FileStorage_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
           
                int? success = array[0] as int?;
                if (success == 0)
                {
                    MessageBox.Show("There was a problem deleting the file on the server.  If this continues, please contact support.", "Error", MessageBoxButton.OK);
                  
                }
                _proxy.Begin_GetFileStorage_lstByLinkedRecord((Guid)_guidParentId, _parentType);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Web Service Returns


        XamDialogWindow _xdw = null;
        XamHtmlViewer xhv = null;
        //bool _loadInBrowser = true;
        void OpenFileInBrowser(string fileUrl, string path, string fileName, string flgOpenInBrowser)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OpenFileInBrowser", IfxTraceCategory.Enter);

                //string fileExt = GetFileExtention(fileName);
                //if (fileExt == null)
                //{
                //    MessageBox.Show("Missing file name or extention.  Try downloading and saving the file first.", "Missing Information", MessageBoxButton.OK);
                //    return;
                //}
               

                //string fileExtention = GetFileExtention(fileName);
                //_loadInBrowser = WillLoadInBrowser(fileExtention);


                if (flgOpenInBrowser == "false")
                {
                    // This file type needs to be opened via the html control thats already open and in a visible state in the screen.
                    // Otherwise, we would need to embed the html control in a popup window like below.  In that case after the file loads, we 
                    //   would not be able to close the window and would be stuck with an empty window (because the file ended up opening outside the window).
                    xhvOpenFile.SourceUri = new Uri(fileUrl);
                }
                else
                {
                    // this file type will not jump out of the browser such as excel, so we need to load it in an html control embeded in a popup window.
                    xhv = new XamHtmlViewer();
                    xhv.SourceUri = new Uri(fileUrl);

                    _xdw = new XamDialogWindow();
                    _xdw.Content = xhv;
                    ApplicationLevelVariables.MainGrid.Children.Add(_xdw);
                    Grid.SetColumnSpan(_xdw, 3);
                    Grid.SetRowSpan(_xdw, 2);
                    _xdw.Header = fileName;
                    _xdw.HeaderIconVisibility = System.Windows.Visibility.Collapsed;
                    _xdw.Height = 450;
                    _xdw.Width = 400;
                }

                object[] obj = new object[1];
                obj[0] = path;
                _proxy.Begin_FileStorage_DeleteTempFile(obj);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OpenFileInBrowser", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OpenFileInBrowser", IfxTraceCategory.Leave);
            }
        }


      
        //string GetFileExtention(string fileName)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileExtention", IfxTraceCategory.Enter);

        //        int pos = fileName.LastIndexOf(".");
        //        if (pos < 1)
        //        {
        //            return null;
        //        }

        //        pos = pos + 1;
        //        if (pos == fileName.Length)
        //        {
        //            return null;
        //        }

        //        string fileExtention = fileName.Substring(pos, fileName.Length - pos);
        //        return fileExtention;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileExtention", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileExtention", IfxTraceCategory.Leave);
        //    }
        //}



        //private bool WillLoadInBrowser(string fileExtension)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileExtention", IfxTraceCategory.Enter);
        //        switch (fileExtension)
        //        {
        //            case "htm":
        //                return true;
        //            case "html":
        //                return true;
        //            case "asp":
        //                return true;
        //            case "aspx":
        //                return true;

        //            case "log":
        //                return true;
        //            case "txt":
        //                return true;
        //            case "doc":
        //                return false;
        //            case "rtf":
        //                return false;
        //            case "docx":
        //                return false;
        //            case "docm":
        //                return false;
        //            case "dotx":
        //                return false;
        //            case "dotm":
        //                return false;

        //            case "zip":
        //                return false;

        //            case "xls":
        //                return false;
        //            case "xlsx":
        //                return false;
        //            case "xlsm":
        //                return false;
        //            case "xlsb":
        //                return false;
        //            case "mhtml":
        //                return false;
        //            case "sltx":
        //                return false;
        //            case "xltm":
        //                return false;
        //            case "xlt":
        //                return false;
        //            case "csv":
        //                return false;

        //            case "tiff":
        //                return true;
        //            case "tif":
        //                return true;
        //            case "gif":
        //                return true;
        //            case "jpg":
        //                return true;
        //            case "jpeg":
        //                return true;
        //            case "bmp":
        //                return true;
        //            case "png":
        //                return true;
        //            case "cur":
        //                return true;
        //            case "ico":
        //                return true;
        //            case "jpe":
        //                return true;
                        
        //            case "asf":
        //                return false;
        //            case "avi":
        //                return false;
        //            case "wav":
        //                return false;
        //            case "mp3":
        //                return false;
        //            case "mp4":
        //                return false;
        //            case "wmv":
        //                return false;
        //            case "mpg":
        //                return false;
        //            case "mpeg":
        //                return false;
                        
        //            case "pdf":
        //                return true;
        //            case "fdf":
        //                return true;
        //            case "dwg":
        //                return false;
        //            case "msg":
        //                return false;
        //            case "xml":
        //                return true;
        //            case "sdxl":
        //                return true;
        //            case "xdp":
        //                return false;

        //            case "vsdx":
        //                return false;
        //            case "vsdm":
        //                return false;
        //            case "vsd":
        //                return false;
        //            case "vwd":
        //                return false;
        //            case "svg":
        //                return false;
        //            case "svgz":
        //                return false;
        //            case "dxf":
        //                return false;


        //            case "xps":
        //                return true;
        //            case "oxps":
        //                return true;


        //            case "pptx":
        //                return false;
        //            case "pptm":
        //                return false;
        //            case "ppt":
        //                return false;
        //            case "ppsx":
        //                return false;
        //            case "ppsm":
        //                return false;
        //            case "pps":
        //                return false;

        //            case "pub":
        //                return false;

        //            default:
        //                return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileExtention", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileExtention", IfxTraceCategory.Leave);
        //    }
        //}










    }
}
