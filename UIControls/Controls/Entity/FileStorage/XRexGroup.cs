﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Ifx.SL;
using System.Collections.Generic;
using EntityWireTypeSL;
using TypeServices;

namespace UIControls
{


    public class XRexGroups : List<XRexGroup>
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "XRexGroups";


        #endregion Initialize Variables



        public XRexGroups()
        {

        }

        public void ReplaceItems(List<XRexGroup> files)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceItems", IfxTraceCategory.Enter);

                base.Clear();
                foreach (var item in files)
                {
                    base.Add(item);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceItems", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReplaceItems", IfxTraceCategory.Leave);
            }
        }


    }



    public class XRexGroup
    {

        #region Initialize Variables

       
        private static string _as = "UIControls";
        private static string _cn = "XRexGroup";

        string _groupName;


        List<FileStorage_Values> _xRefFiles = new List<FileStorage_Values>();
 


        #endregion Initialize Variables



        #region Constructors

        public XRexGroup(string groupName, List<FileStorage_Values> files)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XRexGroup", IfxTraceCategory.Enter);

                _groupName = groupName;
                _xRefFiles.Clear();
                foreach (var item in files)
                {
                    _xRefFiles.Add(item);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XRexGroup", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XRexGroup", IfxTraceCategory.Leave);
            }
        }

  

        #endregion Constructors



        #region Properties

        public string GroupName
        {
            get { return _groupName; }
            set { _groupName = value; }
        }

        public List<FileStorage_Values> XRefFiles
        {
            get { return _xRefFiles; }
            set { _xRefFiles = value; }
        }



        #endregion Properties


        #region Methods




        #endregion Methods


    }
}
