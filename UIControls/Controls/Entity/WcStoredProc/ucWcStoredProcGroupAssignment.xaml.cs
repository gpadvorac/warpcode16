﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using EntityBll.SL;
using EntityWireTypeSL;
using Ifx.SL;
using Infragistics.Controls.Grids;
using ProxyWrapper;
using vControls;
using vUICommon;
using Velocity.SL;
namespace UIControls
{
    public partial class ucWcStoredProcGroupAssignment : UserControl
    {


        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "ucWcStoredProcGroupAssignment";

        ObservableCollection<wcTableGroup_Assignments_Binding> _list = new ObservableCollection<wcTableGroup_Assignments_Binding>();

        Guid? _parentId = null;
        string _parentType = "";


        Guid? _sp_Id = null;
        Guid? _apVrsn_Id = null;

        WcStoredProcService_ProxyWrapper _proxy = null;

        public event GroupAssignmentUpdatedEventHandler XRefUpdated;

        #endregion Initialize Variables



        public ucWcStoredProcGroupAssignment()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucWcStoredProcGroupAssignment Constructor", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();

                _proxy = new WcStoredProcService_ProxyWrapper();
                _proxy.GetWcStoredProcGroup_AssignmentsCompleted += _proxy_GetWcStoredProcGroup_AssignmentsCompleted;
                _proxy.ExecutewcStoredProcGroup_AssignSprocsCompleted += _proxy_ExecutewcStoredProcGroup_AssignSprocsCompleted;

                XgdAssignments.ItemsSource = _list;

                XgdAssignments.EditingSettings.IsMouseActionEditingEnabled = MouseEditingAction.SingleClick;
                XgdAssignments.RowHover = RowHoverType.Row;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucWcStoredProcGroupAssignment Constructor", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucWcStoredProcGroupAssignment Constructor", IfxTraceCategory.Leave);
            }
        }



        public void SetStateFromParent(Guid? apVrsn_Id, Guid? sp_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent", IfxTraceCategory.Enter);

                _apVrsn_Id = apVrsn_Id;
                _sp_Id = sp_Id;


                if (_apVrsn_Id == null)
                {
                    ClearGrid();
                    //MessageBox.Show("Missing App Version Id", "Missing Data", MessageBoxButton.OK);
                    return;
                }
                else if (_sp_Id == null)
                {
                    ClearGrid();
                    //MessageBox.Show("Missing Sproc Id", "Missing Data", MessageBoxButton.OK);
                    return;
                }

                _proxy.Begin_GetWcStoredProcGroup_Assignments((Guid)_apVrsn_Id, (Guid)_sp_Id);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent", IfxTraceCategory.Leave);
            }
        }

        public void ClearGrid()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearGrid", IfxTraceCategory.Enter);
                System.Diagnostics.Debug.WriteLine("ClearGrid");

                XgdAssignments.ItemsSource = null;
                _list.Clear();
                XgdAssignments.ItemsSource = _list;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearGrid", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearGrid", IfxTraceCategory.Leave);
            }
        }



        #region Events

        //Guid? _activeCell_Id = null;
        //bool? _activeCellValue = null;



        private void vCheckColumn_CheckedChanged(object sender, vCheckColumnCheckedChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Enter);

                Console.WriteLine(sender.ToString());
                wcTableGroup_Assignments_Binding data = e.Data as wcTableGroup_Assignments_Binding;
                if (data == null)
                {
                    //  alert user
                }
                else
                {
                    // update database
                    //  NOTE:  We will do the OPOSITE of what the checked value suggest.  the value of the data object has not been set by the ckeckbox yet
                    //  so its current value at this moment will be the oposite of what the checkbox is.  if the user checked it, this will be false - and we need to make the assignment.
                    //if (data.XRef == false)

                    Guid grpId = data.TbGrp_Id;

                    if (((vCheckBox)sender).IsChecked == true)
                    {
                        //Boolean Insert, TbCGrp_Id, TbC_Id, CreatedUserId
                        _proxy.Begin_ExecutewcStoredProcGroup_AssignSprocs(true, grpId, (Guid)_sp_Id, (Guid)Credentials.UserId);
                    }
                    else
                    {
                        _proxy.Begin_ExecutewcStoredProcGroup_AssignSprocs(false, grpId, (Guid)_sp_Id, (Guid)Credentials.UserId);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Leave);
            }
        }



        private void RaiseEvent_XRefUpdated()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEvent_XRefUpdated", IfxTraceCategory.Enter);

                if (XRefUpdated != null)
                {
                    GroupAssignmentUpdatedEventHandler handler = XRefUpdated;
                    GroupAssignmentUpdatedArgs args = new GroupAssignmentUpdatedArgs();
                    handler(this, args);
                }


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEvent_XRefUpdated", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEvent_XRefUpdated", IfxTraceCategory.Leave);
            }
        }



        #endregion Events


        #region Fetch Data


        void XgdAssignments_RowSource(object[] data)
        {
            // This method is usually a followup from calling SetStateFromParent and after the web service has returned.
            // Its also ALWAYS called by the web service reply when returning a new list for the grid.
            // Now reset the list and see to it that any other screens as reset accordingly.
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XgdAssignments_RowSource", IfxTraceCategory.Enter);
                System.Diagnostics.Debug.WriteLine("XgdAssignments_RowSource");

                XgdAssignments.ItemsSource = null;
                _list = null;

                _list = new ObservableCollection<wcTableGroup_Assignments_Binding>();


                if (data != null)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        _list.Add(new wcTableGroup_Assignments_Binding((object[])data[i]));
                    }

                }

                XgdAssignments.ItemsSource = _list;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XgdAssignments_RowSource", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                XgdAssignments.IsEnabled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XgdAssignments_RowSource", IfxTraceCategory.Leave);
            }
        }


        private void _proxy_GetWcStoredProcGroup_AssignmentsCompleted(object sender, GetWcStoredProcGroup_AssignmentsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetWcStoredProcGroup_AssignmentsCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                XgdAssignments_RowSource(array);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetWcStoredProcGroup_AssignmentsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetWcStoredProcGroup_AssignmentsCompleted", IfxTraceCategory.Leave);
            }
        }

        private void _proxy_ExecutewcStoredProcGroup_AssignSprocsCompleted(object sender, ExecutewcStoredProcGroup_AssignSprocsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_ExecutewcStoredProcGroup_AssignSprocsCompleted", IfxTraceCategory.Enter);
                object[] data = e.Result;
                if (data == null)
                {
                    MessageBox.Show("XRefs may not have been removed.  Please check the results, and if this continues, contact support.", "Possible Error", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    int? success = data[0] as int?;
                    if (success == 1)
                    {
                        RaiseEvent_XRefUpdated();
                    }
                    else
                    {
                        MessageBox.Show("XRefs may not have been removed.  Please check the results, and if this continues, contact support.", "Possible Error", MessageBoxButton.OK);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_ExecutewcStoredProcGroup_AssignSprocsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_ExecutewcStoredProcGroup_AssignSprocsCompleted", IfxTraceCategory.Leave);
            }
        }









        #endregion Fetch Data






    }
}
