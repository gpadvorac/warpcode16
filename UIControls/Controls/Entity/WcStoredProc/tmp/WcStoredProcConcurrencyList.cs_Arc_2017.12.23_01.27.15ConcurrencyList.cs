using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class WcStoredProcConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcStoredProcConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public WcStoredProcConcurrencyList(WcStoredProc_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcStoredProcConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Name", data.C.Sp_Name, data.X.Sp_Name));
                _concurrencyList.Add(new ConcurrencyItem("Type Name", data.C.Sp_TypeName, data.X.Sp_TypeName));
                _concurrencyList.Add(new ConcurrencyItem("Method Name", data.C.Sp_MethodName, data.X.Sp_MethodName));
                _concurrencyList.Add(new ConcurrencyItem("Build Data Access Method", data.C.Sp_IsBuildDataAccessMethod, data.X.Sp_IsBuildDataAccessMethod));
                _concurrencyList.Add(new ConcurrencyItem("Associated Entity", data.C.Sp_AssocEntity_Id, data.X.Sp_AssocEntity_Id));
                _concurrencyList.Add(new ConcurrencyItem("Is Fetch Entity", data.C.Sp_IsFetchEntity, data.X.Sp_IsFetchEntity));
                _concurrencyList.Add(new ConcurrencyItem("Is ComboItem List", data.C.Sp_IsTypeComboItem, data.X.Sp_IsTypeComboItem));
                _concurrencyList.Add(new ConcurrencyItem("Is Static List", data.C.Sp_IsStaticList, data.X.Sp_IsStaticList));
                _concurrencyList.Add(new ConcurrencyItem("Result Type", data.C.Sp_SpRsTp_Id, data.X.Sp_SpRsTp_Id));
                _concurrencyList.Add(new ConcurrencyItem("Return Type", data.C.Sp_SpRtTp_Id, data.X.Sp_SpRtTp_Id));
                _concurrencyList.Add(new ConcurrencyItem("Is Input Params As Object Array", data.C.Sp_IsInputParamsAsObjectArray, data.X.Sp_IsInputParamsAsObjectArray));
                _concurrencyList.Add(new ConcurrencyItem("Has New Params", data.C.Sp_HasNewParams, data.X.Sp_HasNewParams));
                _concurrencyList.Add(new ConcurrencyItem("Is Params Valid", data.C.Sp_IsParamsValid, data.X.Sp_IsParamsValid));
                _concurrencyList.Add(new ConcurrencyItem("Is Param Value Set Valid", data.C.Sp_IsParamValueSetValid, data.X.Sp_IsParamValueSetValid));
                _concurrencyList.Add(new ConcurrencyItem("Has New Columns", data.C.Sp_HasNewColumns, data.X.Sp_HasNewColumns));
                _concurrencyList.Add(new ConcurrencyItem("Is Columns Valid", data.C.Sp_IsColumnsValid, data.X.Sp_IsColumnsValid));
                _concurrencyList.Add(new ConcurrencyItem("Is Valid", data.C.Sp_IsValid, data.X.Sp_IsValid));
                _concurrencyList.Add(new ConcurrencyItem("Notes", data.C.Sp_Notes, data.X.Sp_Notes));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.Sp_IsActiveRow, data.X.Sp_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



