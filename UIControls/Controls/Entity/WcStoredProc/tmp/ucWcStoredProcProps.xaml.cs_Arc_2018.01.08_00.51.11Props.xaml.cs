using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Infragistics.Controls.Editors;
using System.ComponentModel;
using TypeServices;
using EntityWireTypeSL;
using EntityBll.SL;
using Ifx.SL;
using vUICommon;
using vUICommon.Controls;
using vControls;
using vComboDataTypes;
using vTooltipProvider;
using Velocity.SL;
using Infragistics.Controls.Layouts;
using ProxyWrapper;
using UIControls.Globalization.WcStoredProc;

// Gen Timestamp:  12/23/2017 2:11:20 AM

namespace UIControls
{


    /// <summary>
    /// 	<para><strong>About this Entity:</strong></para>
    /// 	<para>***General description of the entity from WC***</para>
    /// 	<para></para><br/>
    /// 	<para><strong>About this Control:</strong></para>
    /// 	<para>
    ///     Used as the entity’s (WcStoredProc’s) data entry screen. This control can be embedded
    ///     nearly anywhere, but typically is used in the <a href="D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\The_Entity_Manager.html">
    ///     Entity Manager</a> (<see cref="ucWcStoredProc">ucWcStoredProc</see>) and known as <a href="D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntityProps.html">ucProps</a>.
    ///     ucProps has no UI features for CRUD operations such as buttons or menus and depends
    ///     on either bubbling events up to the top level container where these object usually
    ///     reside, or on user initiated events starting from the top level control and using
    ///     it’s reference to the ‘<see cref="ucWcStoredProc.GetIsActiveEntityControl">Active Entity
    ///     Control’</see> or Active Properties Control’ where the CRUD operation will be
    ///     called to ucProps and then to the business object (<see cref="EntityBll.WcStoredProc_Bll">WcStoredProc_Bll</see>).</para>
    /// </summary>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Entity_Design_Pattern_In_The_UI_Layer.html" cat="Framework and Design Pattern">Entity Design Pattern In The UI Layer</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntity.html" cat="Framework and Design Pattern">ucEntity</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntityList.html" cat="Framework and Design Pattern">ucEntityList</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntityProps.html" cat="Framework and Design Pattern">ucEntityProps</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Tracing_Overview.html" cat="Framework and Design Pattern">Tracing Overview</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Exception_Handling_Overview.html" cat="Framework and Design Pattern">Exception Handling Overview</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Entity_Design_Pattern_In_The_Business_Logic_Layer.html" cat="Framework and Design Pattern In The Business Logic Layer">Business Objects</seealso>
    public partial class ucWcStoredProcProps : UserControl, IEntitiyPropertiesControl
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "ucWcStoredProcProps";

        WcStoredProc_Bll objB;

        /// <summary>
        /// A Boolean flag initialize to true and set to false when the control has loaded.
        /// Sometimes when setting default values in controls their value changed events or
        /// selected index changed fire causing a write action to a business object or some other
        /// action that’s not appropriate when the control is being loaded. When this flag is true,
        /// these events can return without executing any additional code.
        /// </summary>
        bool FLG_FORM_IS_LOADING;
        /// <summary>
        /// A Boolean flag initialize to true when a business object’s data begins to load
        /// into this control, and is set to false when the data load has completed. Usually when
        /// setting values in controls their value changed events or selected index changed fire
        /// causing a write action to a business object or some other action that’s not appropriate
        /// when the data is being loaded. When this flag is true, these events can return without
        /// executing any additional code.
        /// </summary>
        bool FLG_LOADING_REC;
        /// <summary>
        /// A Boolean flag initialize to true when a single data control’s data is being
        /// loaded and you don’t want this control to write to the business object. When this flag
        /// is true, these events can return without executing any additional code.
        /// </summary>
        bool FLG_UPDATING_FIELDVALUE;
        /// <summary>See <see cref="OnCrudFailed">OnCrudFailed</see> for information.</summary>
        public event CrudFailedEventHandler CrudFailed;
        /// <summary>
        ///     See <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> for
        ///     information.
        /// </summary>
        public event CurrentEntityStateEventHandler CurrentEntityStateChanged;
        //public event PropertiesControlActivatedEventHandler PropertiesControlActivated;
        /// <summary>See <see cref="OnBrokenRuleChanged">OnBrokenRuleChanged</see> for information.</summary>
        public event BrokenRuleEventHandler BrokenRuleChanged;
        /// <summary>
        /// Used in situations where this properties control is used in conjunction with a
        /// list control (ucWcStoredProcList). If this properties control has a list data field such as a
        /// ComboBox and users are allowed to edit the ComboBox list, then the corresponding column
        /// in ucEntityList must be updated also. Often this corresponding column in ucEntityList
        /// uses an embedded ComboBox to display the text for the Id bound in that column. If this
        /// properties control’s ComboBox’s list has been changed, then the corresponding
        /// ComboBox’s list in ucWcStoredProcList must be updated also.
        /// </summary>
        public event ListColumnListMustUpdateEventHandler OnListColumnListMustUpdate;
        /// <summary>
        ///     The data field for the property <see cref="IsActivePropertiesControl">IsActivePropertiesControl</see>. Refer to its
        ///     documentation on how its used.
        /// </summary>
        bool _isActivePropertiesControl = false;
        /// <summary>
        ///     This properties control uses a Business Object (<see cref="EntityBll.WcStoredProc_Bll">WcStoredProc_Bll</see>) for its data source. There are times when
        ///     there is no business object such as when this control first loads. There is code
        ///     that expects a business object to be in place and can use this flag to make
        ///     corrections when no business object is found.
        /// </summary>
        bool _hasBusinessObject = false;

        bool _hasBusinessObjectEventsAttached = false;

        /// <summary>
        /// A hard coded value used in positioning the TextLengthCounter on the Canvas. To
        /// position the TextLengthCounter at the bottom of the active text control, this formula
        /// is used:<br/>
        /// TextLengthCounter.Top = TextControl.Top +TextControl.Height +
        /// _textLengthCounterTopOffset<br/>
        /// When the control container is a Grid rather than a Canvas, positioning the
        /// TextLengthCounter is much more complicated and is performed by the
        /// PositionTextLengthCounter.PositionTextLenghtLabel_InGrid method.
        /// </summary>
        int _textLengthCounterTopOffset = -7;
        /// <summary>
        /// A hard coded value used in positioning the TextLengthCounter on the Canvas. To
        /// position the TextLengthCounter so it’s Right Aligned with the active text control, this
        /// formula is used:<br/>
        /// TextLengthCounter.Top = TextControl.Left +TextControl.Width +
        /// _textLengthCounterLeftOffset<br/>
        /// When the control container is a Grid rather than a Canvas, positioning the
        /// TextLengthCounter is much more complicated and is performed by the
        /// PositionTextLengthCounter.PositionTextLenghtLabel_InGrid method.
        /// </summary>
        int _textLengthCounterLeftOffset = -36;
        /// <summary>
        /// 	<para>A Brush value used to set a data control’s border when the control is in a
        ///     valid state.</para>
        /// 	<para>
        ///         This value is set dynamically one time when ucWcStoredProcProps first loads from the
        ///         <see cref="ControlBrushes.DataControlValidColor">ControlBrushes.DataControlValidColor</see>
        ///         method. This function has been refactored to the <see cref="ControlBrushes">ControlBurshes</see> class so the hard coded value could be
        ///         alternatively be retrieved from a config file or data store.
        ///     </para>
        /// </summary>
        Brush brshDataControlValid;
        /// <summary>
        /// 	<para>A Brush value used to set a data control’s border when the control is in a
        ///     NON valid state (has one or more broken rules) and in a Dirty state.</para>
        /// 	<para><br/>
        ///     The two states ‘New – Not Dirty’ and ‘New – Dirty’ have a different appearance.
        ///     When a Properties screen first loads, some data controls may be Not-Valid by
        ///     default such as cases where the field is required and has not default value. In
        ///     this case, rather than giving it the Not-Valid appearance using a bright red
        ///     boarder which would be rather annoying and in-the-face of the user, a softer darker
        ///     red color is used to let user know this field is Not-Valid while not being too
        ///     abrasive or harsh on the eye (and emotions of the user).</para>
        /// 	<para>
        /// 		<br/>
        ///         This value is set dynamically one time when ucWcStoredProcProps first loads from the
        ///         <see cref="ControlBrushes.DataControlNotValidColor">ControlBrushes.DataControlNotValidColor</see>
        ///         method. This function has been refactored to the <see cref="ControlBrushes">ControlBurshes</see> class so the hard coded value could be
        ///         alternatively be retrieved from a config file or data store.
        ///     </para>
        /// </summary>
        Brush brshDataControlNotValid;
        /// <summary>
        /// 	<para>A Brush value used to set a data control’s border when the control is in a
        ///     NON valid state (has one or more broken rules) and in a New – Not Dirty
        ///     state.</para>
        /// 	<para><br/>
        ///     The two states ‘New – Not Dirty’ and ‘New – Dirty’ have a different appearance.
        ///     When a Properties screen first loads, some data controls may be Not-Valid by
        ///     default such as cases where the field is required and has not default value. In
        ///     this case, rather than giving it the Not-Valid appearance using a bright red
        ///     boarder which would be rather annoying and in-the-face of the user, a softer darker
        ///     red color is used to let user know this field is Not-Valid while not being too
        ///     abrasive or harsh on the eye (and emotions of the user).</para>
        /// 	<para>
        /// 		<br/>
        ///         This value is set dynamically one time when ucWcStoredProcProps first loads from the
        ///         <see cref="ControlBrushes.DataControlNotValidNewRecColor">ControlBrushes.DataControlNotValidNewRecColor</see>
        ///         method. This function has been refactored to the <see cref="ControlBrushes">ControlBurshes</see> class so the hard coded value could be
        ///         alternatively be retrieved from a config file or data store.
        ///     </para>
        /// </summary>
        Brush brshDataControlNotValidNewRec;

        /// <summary>
        ///  The class that manages the AdornerLabel class (the 'Text Length Label Adorner).  The AdornerLabel is used to 
        ///  show the remaining number of charectors available in text controls - mainly TextBoxes.
        /// </summary>
        AdornerLabelManager _lblAdrnMngr;

        bool _secuitySettingIsReadOnly = false;

        private DataControlEventManager _dataControlEventManager = new DataControlEventManager();

        private WcStoredProcService_ProxyWrapper _wcStoredProcProxy = null;
        private CommonClientDataService_ProxyWrapper _CommonClientDataServiceProxy = null;
        private WcTableService_ProxyWrapper _WcTableServiceProxy = null;

        private ComboItemList _Sp_SpRsTp_Id_ComboItemList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private ComboItemList _Sp_SpRtTp_Id_ComboItemList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private ComboItemList _Sp_AssocEntity_Id_ComboItemList = new ComboItemList(ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);

        #endregion Initialize Variables


        #region Constructors

        /// <summary>
        /// Various events are wired up here and a call is made to the CustomConstructorCode
        /// method in the ucWcStoredProcProps.xaml.cust.cs partial class.
        /// </summary>
        public ucWcStoredProcProps()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcStoredProcProps", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();
                this.IsEnabled = false;
                this.Name = "WcStoredProcProps";
                _lblAdrnMngr = new AdornerLabelManager();
                _CommonClientDataServiceProxy = new CommonClientDataService_ProxyWrapper();
                _WcTableServiceProxy = new WcTableService_ProxyWrapper();

                InitializeProxyWrapper();
                CustomConstructorCode();
                FormatFields();
                ReadOnlyAssignments();

                ConfigureXamTileView();
                this.Loaded += new RoutedEventHandler(ucWcStoredProcProps_Loaded);

                _userContext.SecurityArtifactsRetrieved += new SecurityArtifactsRetrievedEventHandler(UserSecurityContext_SecurityArtifactsRetrieved);
                SetSecurityState();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcStoredProcProps", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_FORM_IS_LOADING = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcStoredProcProps", IfxTraceCategory.Leave);
            }
        }

        void ucWcStoredProcProps_Loaded(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcProps_Loaded", IfxTraceCategory.Enter);
                CreateBusinessRuleTooltips();
                SetControlEvents();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcProps_Loaded", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcProps_Loaded", IfxTraceCategory.Leave);
            }
        }


        #endregion Constructors


        #region Load this

        void InitializeProxyWrapper()
        {
            if (_wcStoredProcProxy == null)
            {
                _wcStoredProcProxy = new ProxyWrapper.WcStoredProcService_ProxyWrapper();
                //_wcStoredProcProxy.GetWcTable_ComboItemListCompleted += new EventHandler<GetWcTable_ComboItemListCompletedEventArgs>(WcStoredProcProxy_GetWcTable_ComboItemListCompleted);
                //_wcStoredProcProxy.GetWcStoredProcResultType_ComboItemListCompleted += new EventHandler<GetWcStoredProcResultType_ComboItemListCompletedEventArgs>(WcStoredProcProxy_GetWcStoredProcResultType_ComboItemListCompleted);
                //_wcStoredProcProxy.GetWcStoredProcReturnType_ComboItemListCompleted += new EventHandler<GetWcStoredProcReturnType_ComboItemListCompletedEventArgs>(WcStoredProcProxy_GetWcStoredProcReturnType_ComboItemListCompleted);
            }
        }

        /// <summary>
        /// This can be called internally from the constructor or optionally – externally
        /// after this control has loaded. Being called externally after all the other
        /// initialization code has executed allows the certain things to be preconfigured prior to
        /// running additional code such as loading data that may be dependent on these
        /// configurations being in place.
        /// </summary>
        public void LoadControl()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadControl", IfxTraceCategory.Enter);
                FLG_FORM_IS_LOADING = true;
                //                brshDataControlValid = ControlBrushes.DataControlValidColor;
                //                brshDataControlNotValid = ControlBrushes.DataControlNotValidColor;
                //                brshDataControlNotValidNewRec = ControlBrushes.DataControlNotValidNewRecColor;
                if (objB == null)
                {
                    objB = new WcStoredProc_Bll();
                    _hasBusinessObject = true;
                    objB.NewEntityRow();
                }

                AddBusinessObjectEvents();
                Sp_AssocEntity_Id_DataSource();
                Sp_SpRsTp_Id_DataSource();
                Sp_SpRtTp_Id_DataSource();

                CustomLoadMethods();
                AssignTextLengthLabels();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadControl", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_FORM_IS_LOADING = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadControl", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>When a business object (Entity_Bll) is being loaded to support this control,
        ///     all events associated with the outgoing business object must be removed and then
        ///     reattached to the new business object such as:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="EntityBll.WcStoredProc_Bll.CurrentEntityStateChanged">CurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="EntityBll.WcStoredProc_Bll.BrokenRuleChanged">BrokenRuleChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="EntityBll.WcStoredProc_Bll.ControlValidStateChanged">ControlValidStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="EntityBll.WcStoredProc_Bll.RestrictedTextLengthChanged">RestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="CrudFailed">CrudFailed</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public void AddBusinessObjectEvents()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AddBusinessObjectEvents", IfxTraceCategory.Enter);
                if (objB == null) { return; }
                RemoveBusnessObjectEvents();
                objB.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                objB.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                objB.ControlValidStateChanged += new ControlValidStateChangedEventHandler(OnControlValidStateChanged);
                objB.CrudFailed += new CrudFailedEventHandler(OnCrudFailed);
                objB.EntityRowReceived += new EntityRowReceivedEventHandler(OnEntityRowReceived);
                objB.AsyncSaveWithResponseComplete += new AsyncSaveWithResponseCompleteEventHandler(objB_AsyncSaveWithResponseComplete);
                AddBusinessObjectEvents_CustomCode();
                _hasBusinessObjectEventsAttached = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AddBusinessObjectEvents", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_FORM_IS_LOADING = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AddBusinessObjectEvents", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>When a business object (Entity_Bll) is being unloaded prior to loading next
        ///     business object to support this control, all events associated with the business
        ///     object must be removed such as:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="EntityBll.WcStoredProc_Bll.CurrentEntityStateChanged">CurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="EntityBll.WcStoredProc_Bll.BrokenRuleChanged">BrokenRuleChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="EntityBll.WcStoredProc_Bll.ControlValidStateChanged">ControlValidStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="EntityBll.WcStoredProc_Bll.RestrictedTextLengthChanged">RestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="CrudFailed">CrudFailed</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public void RemoveBusnessObjectEvents()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RemoveBusnessObjectEvents", IfxTraceCategory.Enter);
                if (objB == null) { return; }
                objB.CurrentEntityStateChanged -= new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                objB.BrokenRuleChanged -= new BrokenRuleEventHandler(OnBrokenRuleChanged);
                objB.ControlValidStateChanged -= new ControlValidStateChangedEventHandler(OnControlValidStateChanged);
                objB.CrudFailed -= new CrudFailedEventHandler(OnCrudFailed);
                objB.EntityRowReceived -= new EntityRowReceivedEventHandler(OnEntityRowReceived);
                objB.AsyncSaveWithResponseComplete -= new AsyncSaveWithResponseCompleteEventHandler(objB_AsyncSaveWithResponseComplete);
                RemoveBusnessObjectEvents_CustomCode();
                _hasBusinessObjectEventsAttached = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RemoveBusnessObjectEvents", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RemoveBusnessObjectEvents", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Wires up all standard events for the data controls. For example, a reference
        ///     to each TextBox is passed onto the SetTextBoxEvents method where its events are
        ///     attached. This is a list of methods that may be called from this method:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="SetTextBoxEvents">SetTextBoxEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="SetTextBoxForStringsEvents">SetTextBoxForStringsEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="SetCheckBoxEvents">SetCheckBoxEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="SetRadioButtonEvents">SetRadioButtonEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="SetXamDateTimeEditorEvents">SetXamDateTimeEditorEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="SetComboBoxEvents">SetComboBoxEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="SetXamComboEditorEvents">SetXamComboEditorEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="AttachToolTip">AttachToolTip</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void SetControlEvents()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlEvents", IfxTraceCategory.Enter);
                //TEXT BOXES
                SetTextBoxEvents(Sp_Name);
                SetTextBoxEvents(Sp_SprocGroups);
                SetTextBoxEvents(Sp_WireTypeName);
                SetTextBoxEvents(Sp_MethodName);
                SetTextBoxEvents(Sp_AssocEntityNames);
                SetTextBoxEvents(Sp_Notes);

                //DATETIME CONTROLS   DatePicker

                //TIME CONTROLS   TimePicker

                //COMBO BOXES

                //vXamComboEditor
                SetVXamComboEditorEvents(Sp_AssocEntity_Id);
                SetVXamComboEditorEvents(Sp_SpRsTp_Id);
                SetVXamComboEditorEvents(Sp_SpRtTp_Id);

                //vXamMultiColumnComboEditor

                //CHECK BOXES
                SetCheckBoxEvents(Sp_IsCodeGen);
                SetCheckBoxEvents(Sp_IsReadyCodeGen);
                SetCheckBoxEvents(Sp_IsCodeGenComplete);
                SetCheckBoxEvents(Sp_IsTagForCodeGen);
                SetCheckBoxEvents(Sp_IsTagForOther);
                SetCheckBoxEvents(Sp_IsBuildDataAccessMethod);
                SetCheckBoxEvents(Sp_BuildListClass);
                SetCheckBoxEvents(Sp_IsFetchEntity);
                SetCheckBoxEvents(Sp_IsStaticList);
                SetCheckBoxEvents(Sp_IsTypeComboItem);
                SetCheckBoxEvents(Sp_IsCreateWireType);
                SetCheckBoxEvents(Sp_IsInputParamsAsObjectArray);
                SetCheckBoxEvents(Sp_IsParamsAutoRefresh);
                SetCheckBoxEvents(Sp_HasNewParams);
                SetCheckBoxEvents(Sp_IsParamsValid);
                SetCheckBoxEvents(Sp_IsParamValueSetValid);
                SetCheckBoxEvents(Sp_HasNewColumns);
                SetCheckBoxEvents(Sp_IsColumnsValid);
                SetCheckBoxEvents(Sp_IsValid);
                SetCheckBoxEvents(Sp_IsActiveRow);

                //RADIO BUTTONS

                //vRadioButtonGroup CONTROLS

                // ColorPicker

                //TOOLTIPS FOR BROKEN RULES
                AttachToolTip(Sp_Name);
                AttachToolTip(Sp_IsCodeGen);
                AttachToolTip(Sp_IsReadyCodeGen);
                AttachToolTip(Sp_IsCodeGenComplete);
                AttachToolTip(Sp_IsTagForCodeGen);
                AttachToolTip(Sp_IsTagForOther);
                AttachToolTip(Sp_SprocGroups);
                AttachToolTip(Sp_IsBuildDataAccessMethod);
                AttachToolTip(Sp_BuildListClass);
                AttachToolTip(Sp_IsFetchEntity);
                AttachToolTip(Sp_IsStaticList);
                AttachToolTip(Sp_IsCreateWireType);
                AttachToolTip(Sp_WireTypeName);
                AttachToolTip(Sp_MethodName);
                AttachToolTip(Sp_AssocEntityNames);
                AttachToolTip(Sp_SpRsTp_Id);
                AttachToolTip(Sp_SpRtTp_Id);
                AttachToolTip(Sp_IsInputParamsAsObjectArray);
                AttachToolTip(Sp_IsParamsAutoRefresh);
                AttachToolTip(Sp_HasNewParams);
                AttachToolTip(Sp_IsParamsValid);
                AttachToolTip(Sp_IsParamValueSetValid);
                AttachToolTip(Sp_HasNewColumns);
                AttachToolTip(Sp_IsColumnsValid);
                AttachToolTip(Sp_IsValid);
                AttachToolTip(Sp_Notes);
                AttachToolTip(Sp_IsActiveRow);

                SetControlEvents_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlEvents", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlEvents", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>
        ///         This sets the state of the UI by loading the data of a newly loaded business
        ///         object (<see cref="EntityBll.WcStoredProc_Bll">WcStoredProc_Bll</see>) into all of the data
        ///         fields, refreshing any lists which need to be updated with the new data and any
        ///         other configurations that need to be made after loading a business object.
        ///         There are 2 types of business object <see cref="TypeServices.EntityState">states</see> that affect the UI at this point.
        ///     </para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>Existing-Saved (Valid)</item>
        /// 		<item>New-NotDirty (Valid or UnValid)</item>
        /// 	</list>
        /// 	<para>
        ///         This method is called when loading a business object into this control, by
        ///         executing the <see cref="UnDo">UnDo</see> method where the UI’s state is
        ///         returned to its original state – one of the 2 bullets listed above or by
        ///         executing the <see cref="NewEntityRow">NewEntityRow</see> method (which also executes after
        ///         a delete action has been performed if deletes are allowed).
        ///     </para>
        /// </summary>
        void SetState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetState", IfxTraceCategory.Enter);
                FLG_LOADING_REC = true;
                // Remove all TextBox TextChanged Events so they don't fire when we set thier new values.  The TextChanged Event is unpredictable in SL so we need to remove it here.
                DataControlDataChangedEvents_RemoveAll();
                if (objB.Sp_Name == null)
                {
                    Sp_Name.Text = "";
                }
                else
                {
                    Sp_Name.Text = objB.Sp_Name;
                }
                Sp_IsCodeGen.IsChecked = objB.Sp_IsCodeGen;
                Sp_IsReadyCodeGen.IsChecked = objB.Sp_IsReadyCodeGen;
                Sp_IsCodeGenComplete.IsChecked = objB.Sp_IsCodeGenComplete;
                Sp_IsTagForCodeGen.IsChecked = objB.Sp_IsTagForCodeGen;
                Sp_IsTagForOther.IsChecked = objB.Sp_IsTagForOther;
                if (objB.Sp_SprocGroups == null)
                {
                    Sp_SprocGroups.Text = "";
                }
                else
                {
                    Sp_SprocGroups.Text = objB.Sp_SprocGroups;
                }
                Sp_IsBuildDataAccessMethod.IsChecked = objB.Sp_IsBuildDataAccessMethod;
                Sp_BuildListClass.IsChecked = objB.Sp_BuildListClass;
                Sp_IsFetchEntity.IsChecked = objB.Sp_IsFetchEntity;
                Sp_IsStaticList.IsChecked = objB.Sp_IsStaticList;
                Sp_IsTypeComboItem.IsChecked = objB.Sp_IsTypeComboItem;
                Sp_IsCreateWireType.IsChecked = objB.Sp_IsCreateWireType;
                if (objB.Sp_WireTypeName == null)
                {
                    Sp_WireTypeName.Text = "";
                }
                else
                {
                    Sp_WireTypeName.Text = objB.Sp_WireTypeName;
                }
                if (objB.Sp_MethodName == null)
                {
                    Sp_MethodName.Text = "";
                }
                else
                {
                    Sp_MethodName.Text = objB.Sp_MethodName;
                }

                Set_ComboEditorItem_SelectedItem_ComboItemType(Sp_AssocEntity_Id, objB.Sp_AssocEntity_Id);
                if (objB.Sp_AssocEntityNames == null)
                {
                    Sp_AssocEntityNames.Text = "";
                }
                else
                {
                    Sp_AssocEntityNames.Text = objB.Sp_AssocEntityNames;
                }

                Set_ComboEditorItem_SelectedItem_ComboItemType(Sp_SpRsTp_Id, objB.Sp_SpRsTp_Id);

                Set_ComboEditorItem_SelectedItem_ComboItemType(Sp_SpRtTp_Id, objB.Sp_SpRtTp_Id);
                Sp_IsInputParamsAsObjectArray.IsChecked = objB.Sp_IsInputParamsAsObjectArray;
                Sp_IsParamsAutoRefresh.IsChecked = objB.Sp_IsParamsAutoRefresh;
                Sp_HasNewParams.IsChecked = objB.Sp_HasNewParams;
                Sp_IsParamsValid.IsChecked = objB.Sp_IsParamsValid;
                Sp_IsParamValueSetValid.IsChecked = objB.Sp_IsParamValueSetValid;
                Sp_HasNewColumns.IsChecked = objB.Sp_HasNewColumns;
                Sp_IsColumnsValid.IsChecked = objB.Sp_IsColumnsValid;
                Sp_IsValid.IsChecked = objB.Sp_IsValid;
                if (objB.Sp_Notes == null)
                {
                    Sp_Notes.Text = "";
                }
                else
                {
                    Sp_Notes.Text = objB.Sp_Notes;
                }
                Sp_IsActiveRow.IsChecked = objB.Sp_IsActiveRow;

                SetState_CustomCode();
                //  Reset all TextBox TextChanged Events now.
                DataControlDataChangedEvents_SetAll();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetState", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_LOADING_REC = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetState", IfxTraceCategory.Leave);
            }
        }

        void Set_ComboEditorItem_SelectedItem_ComboItemType(XamComboEditor cmb, object value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_ComboItemType", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }

                ComboItemList itemsSource = (ComboItemList)cmb.ItemsSource;
                if (itemsSource == null) { return; }

                switch (itemsSource.Id_DataType)
                {
                    case ComboItemList.DataType.GuidType:
                        foreach (ComboEditorItem obj in cmb.Items)
                        {
                            if ((Guid)((ComboItem)obj.Data).Id == (Guid)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.LongIntegerType:
                        foreach (ComboEditorItem obj in cmb.Items)
                        {
                            if ((long)((ComboItem)obj.Data).Id == (long)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.IntegerType:
                        foreach (ComboEditorItem obj in cmb.Items)
                        {
                            if ((int)((ComboItem)obj.Data).Id == (int)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.ShortType:
                        foreach (ComboEditorItem obj in cmb.Items)
                        {
                            if ((short)((ComboItem)obj.Data).Id == (short)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.ByteType:
                        foreach (ComboEditorItem obj in cmb.Items)
                        {
                            if ((byte)((ComboItem)obj.Data).Id == (byte)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.StringType:
                        foreach (ComboEditorItem obj in cmb.Items)
                        {
                            if ((string)((ComboItem)obj.Data).Id == (string)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.NA:
                        throw new Exception("UIControls.ucWcStoredProcProps.Set_ComboEditorItem_SelectedItem_ComboItemType():  DataType not found in switch statement.");
                }
                // Items was not found in the list so make sure nothing is selected.
                cmb.SelectedIndex = -1;
                return;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_ComboItemType", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_ComboItemType", IfxTraceCategory.Leave);
            }
        }

        void Set_ComboEditorItem_SelectedItem_Int(XamComboEditor cmb, int? value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_Int", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }
                foreach (ComboEditorItem obj in cmb.Items)
                {
                    if (((IWireTypeBinding)obj.Data).Get_Int_Id() == value)
                    {
                        obj.IsSelected = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_Int", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_Int", IfxTraceCategory.Leave);
            }
        }

        void Set_ComboEditorItem_SelectedItem_Guid(XamComboEditor cmb, Guid? value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_Guid", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }
                foreach (ComboEditorItem obj in cmb.Items)
                {
                    if (((IWireTypeBinding)obj.Data).Get_Guid_Id() == value)
                    {
                        obj.IsSelected = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_Guid", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_Guid", IfxTraceCategory.Leave);
            }
        }

        void Set_ComboEditorItem_SelectedItem_String(XamComboEditor cmb, string value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_String", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }
                foreach (ComboEditorItem obj in cmb.Items)
                {
                    if (((IWireTypeBinding)obj.Data).Get_String_Id() == value)
                    {
                        obj.IsSelected = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_String", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_String", IfxTraceCategory.Leave);
            }

        }

        void Set_MultiColumnComboEditorItem_SelectedItem_ComboItemType(XamMultiColumnComboEditor cmb, object value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_ComboItemType", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }

                ComboItemList itemsSource = (ComboItemList)cmb.ItemsSource;

                switch (itemsSource.Id_DataType)
                {
                    case ComboItemList.DataType.GuidType:
                        foreach (ComboRow obj in cmb.Items)
                        {
                            if ((Guid)((ComboItem)obj.Data).Id == (Guid)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.LongIntegerType:
                        foreach (ComboRow obj in cmb.Items)
                        {
                            if ((int)((ComboItem)obj.Data).Id == (long)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.IntegerType:
                        foreach (ComboRow obj in cmb.Items)
                        {
                            if ((int)((ComboItem)obj.Data).Id == (int)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.ShortType:
                        foreach (ComboRow obj in cmb.Items)
                        {
                            if ((int)((ComboItem)obj.Data).Id == (short)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.ByteType:
                        foreach (ComboRow obj in cmb.Items)
                        {
                            if ((int)((ComboItem)obj.Data).Id == (byte)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.StringType:
                        foreach (ComboRow obj in cmb.Items)
                        {
                            if ((string)((ComboItem)obj.Data).Id == (string)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.NA:
                        throw new Exception("UIControls.ucWcStoredProcProps.Set_MultiColumnComboEditorItem_SelectedItem_ComboItemType():  DataType not found in switch statement.");
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_ComboItemType", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_ComboItemType", IfxTraceCategory.Leave);
            }
        }

        void Set_MultiColumnComboEditorItem_SelectedItem_Int(XamMultiColumnComboEditor cmb, int? value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_Int", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }
                foreach (ComboRow obj in cmb.Items)
                {
                    if (((IWireTypeBinding)obj.Data).Get_Int_Id() == value)
                    {
                        obj.IsSelected = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_Int", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_Int", IfxTraceCategory.Leave);
            }
        }

        void Set_MultiColumnComboEditorItem_SelectedItem_Guid(XamMultiColumnComboEditor cmb, Guid? value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_Guid", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }
                foreach (ComboRow obj in cmb.Items)
                {
                    if (((IWireTypeBinding)obj.Data).Get_Guid_Id() == value)
                    {
                        obj.IsSelected = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_Guid", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_Guid", IfxTraceCategory.Leave);
            }
        }

        void Set_MultiColumnComboEditorItem_SelectedItem_String(XamMultiColumnComboEditor cmb, string value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_String", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }
                foreach (ComboRow obj in cmb.Items)
                {
                    if (((IWireTypeBinding)obj.Data).Get_String_Id() == value)
                    {
                        obj.IsSelected = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_String", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_String", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Passes a reference of each control that has one or more validation rules into the
        ///     <see cref="SetControlDefalutValidAppearance">SetControlDefalutValidAppearance</see>
        ///     method where the default valid appearance is set. The return value of the business
        ///     object’s <see cref="EntityBll.WcStoredProc_Bll.IsPropertyValid">IsPropertyValid</see>
        ///     function is also passed into this method.
        /// </summary>
        void SetControlsDefaultValidState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlsDefaultValidState", IfxTraceCategory.Enter);

                #region Fields with validation

                SetControlDefaultValidAppearance(Sp_Name, objB.IsPropertyValid("Sp_Name"));
                SetControlDefaultValidAppearance(Sp_IsCodeGen, objB.IsPropertyValid("Sp_IsCodeGen"));
                SetControlDefaultValidAppearance(Sp_IsReadyCodeGen, objB.IsPropertyValid("Sp_IsReadyCodeGen"));
                SetControlDefaultValidAppearance(Sp_IsCodeGenComplete, objB.IsPropertyValid("Sp_IsCodeGenComplete"));
                SetControlDefaultValidAppearance(Sp_IsTagForCodeGen, objB.IsPropertyValid("Sp_IsTagForCodeGen"));
                SetControlDefaultValidAppearance(Sp_IsTagForOther, objB.IsPropertyValid("Sp_IsTagForOther"));
                SetControlDefaultValidAppearance(Sp_SprocGroups, objB.IsPropertyValid("Sp_SprocGroups"));
                SetControlDefaultValidAppearance(Sp_IsBuildDataAccessMethod, objB.IsPropertyValid("Sp_IsBuildDataAccessMethod"));
                SetControlDefaultValidAppearance(Sp_BuildListClass, objB.IsPropertyValid("Sp_BuildListClass"));
                SetControlDefaultValidAppearance(Sp_IsFetchEntity, objB.IsPropertyValid("Sp_IsFetchEntity"));
                SetControlDefaultValidAppearance(Sp_IsStaticList, objB.IsPropertyValid("Sp_IsStaticList"));
                SetControlDefaultValidAppearance(Sp_IsCreateWireType, objB.IsPropertyValid("Sp_IsCreateWireType"));
                SetControlDefaultValidAppearance(Sp_WireTypeName, objB.IsPropertyValid("Sp_WireTypeName"));
                SetControlDefaultValidAppearance(Sp_MethodName, objB.IsPropertyValid("Sp_MethodName"));
                SetControlDefaultValidAppearance(Sp_AssocEntityNames, objB.IsPropertyValid("Sp_AssocEntityNames"));
                SetControlDefaultValidAppearance(Sp_SpRsTp_Id, objB.IsPropertyValid("Sp_SpRsTp_Id"));
                SetControlDefaultValidAppearance(Sp_SpRtTp_Id, objB.IsPropertyValid("Sp_SpRtTp_Id"));
                SetControlDefaultValidAppearance(Sp_IsInputParamsAsObjectArray, objB.IsPropertyValid("Sp_IsInputParamsAsObjectArray"));
                SetControlDefaultValidAppearance(Sp_IsParamsAutoRefresh, objB.IsPropertyValid("Sp_IsParamsAutoRefresh"));
                SetControlDefaultValidAppearance(Sp_HasNewParams, objB.IsPropertyValid("Sp_HasNewParams"));
                SetControlDefaultValidAppearance(Sp_IsParamsValid, objB.IsPropertyValid("Sp_IsParamsValid"));
                SetControlDefaultValidAppearance(Sp_IsParamValueSetValid, objB.IsPropertyValid("Sp_IsParamValueSetValid"));
                SetControlDefaultValidAppearance(Sp_HasNewColumns, objB.IsPropertyValid("Sp_HasNewColumns"));
                SetControlDefaultValidAppearance(Sp_IsColumnsValid, objB.IsPropertyValid("Sp_IsColumnsValid"));
                SetControlDefaultValidAppearance(Sp_IsValid, objB.IsPropertyValid("Sp_IsValid"));
                SetControlDefaultValidAppearance(Sp_Notes, objB.IsPropertyValid("Sp_Notes"));
                SetControlDefaultValidAppearance(Sp_IsActiveRow, objB.IsPropertyValid("Sp_IsActiveRow"));

                #endregion Fields with validation

                #region Fields with no validation
                //These are listed for reference so can see what doesn’t have validation
                //And also so you can move a line from here to above if it needs validation later on.

                //SetControlDefaultValidAppearance(Sp_IsTypeComboItem, true);
                //SetControlDefaultValidAppearance(Sp_AssocEntity_Id, true);


                #endregion Fields with no validation

                SetControlsDefaultValidState_Custom();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlsDefaultValidState", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlsDefaultValidState", IfxTraceCategory.Leave);
            }
        }

        #endregion Load this


        #region Load List Controls

        #region Sp_AssocEntity_Id XamComboEditor

        /// <summary>
        /// Sets the data source for this list control. Usually a call to the data store is
        /// made and lightweight list of WireType objects is returned for this data source.
        /// </summary>
        void Sp_AssocEntity_Id_DataSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_AssocEntity_Id_DataSource", IfxTraceCategory.Enter);
                Sp_AssocEntity_Id.DisplayMemberPath = "ItemName";
                Sp_AssocEntity_Id.ItemsSource = WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty;
                WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty.DataSourceUpdated -= new StaticComboItemListUpdatedEventHandler(WcTable_ComboItemList_BindingListProperty_DataSourceUpdated);
                WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTable_ComboItemList_BindingListProperty_DataSourceUpdated);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_AssocEntity_Id_DataSource", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_AssocEntity_Id_DataSource", IfxTraceCategory.Leave);
            }
        }

        #endregion Sp_AssocEntity_Id XamComboEditor

        #region Sp_SpRsTp_Id XamComboEditor

        /// <summary>
        /// Sets the data source for this list control. Usually a call to the data store is
        /// made and lightweight list of WireType objects is returned for this data source.
        /// </summary>
        void Sp_SpRsTp_Id_DataSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id_DataSource", IfxTraceCategory.Enter);
                Sp_SpRsTp_Id.DisplayMemberPath = "ItemName";
                Sp_SpRsTp_Id.ItemsSource = CommonClientData_Bll_staticLists.WcStoredProcResultType_ComboItemList_BindingListProperty;
                CommonClientData_Bll_staticLists.WcStoredProcResultType_ComboItemList_BindingListProperty.DataSourceUpdated -= new StaticComboItemListUpdatedEventHandler(WcStoredProcResultType_ComboItemList_BindingListProperty_DataSourceUpdated);
                CommonClientData_Bll_staticLists.WcStoredProcResultType_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcStoredProcResultType_ComboItemList_BindingListProperty_DataSourceUpdated);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id_DataSource", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRsTp_Id_DataSource", IfxTraceCategory.Leave);
            }
        }

        #endregion Sp_SpRsTp_Id XamComboEditor

        #region Sp_SpRtTp_Id XamComboEditor

        /// <summary>
        /// Sets the data source for this list control. Usually a call to the data store is
        /// made and lightweight list of WireType objects is returned for this data source.
        /// </summary>
        void Sp_SpRtTp_Id_DataSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id_DataSource", IfxTraceCategory.Enter);
                Sp_SpRtTp_Id.DisplayMemberPath = "ItemName";
                Sp_SpRtTp_Id.ItemsSource = CommonClientData_Bll_staticLists.WcStoredProcReturnType_ComboItemList_BindingListProperty;
                CommonClientData_Bll_staticLists.WcStoredProcReturnType_ComboItemList_BindingListProperty.DataSourceUpdated -= new StaticComboItemListUpdatedEventHandler(WcStoredProcReturnType_ComboItemList_BindingListProperty_DataSourceUpdated);
                CommonClientData_Bll_staticLists.WcStoredProcReturnType_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcStoredProcReturnType_ComboItemList_BindingListProperty_DataSourceUpdated);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id_DataSource", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Sp_SpRtTp_Id_DataSource", IfxTraceCategory.Leave);
            }
        }

        #endregion Sp_SpRtTp_Id XamComboEditor



        void WcTable_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);
                Sp_AssocEntity_Id.vXamComboEditorSelectionChanged -= new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                if (objB != null)
                {
                    Set_ComboEditorItem_SelectedItem_ComboItemType(Sp_AssocEntity_Id, objB.Sp_AssocEntity_Id);
                }
                Sp_AssocEntity_Id.vXamComboEditorSelectionChanged += new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        void WcStoredProcResultType_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcResultType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);
                Sp_SpRsTp_Id.vXamComboEditorSelectionChanged -= new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                if (objB != null)
                {
                    Set_ComboEditorItem_SelectedItem_ComboItemType(Sp_SpRsTp_Id, objB.Sp_SpRsTp_Id);
                }
                Sp_SpRsTp_Id.vXamComboEditorSelectionChanged += new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcResultType_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcResultType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        void WcStoredProcReturnType_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcReturnType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);
                Sp_SpRtTp_Id.vXamComboEditorSelectionChanged -= new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                if (objB != null)
                {
                    Set_ComboEditorItem_SelectedItem_ComboItemType(Sp_SpRtTp_Id, objB.Sp_SpRtTp_Id);
                }
                Sp_SpRtTp_Id.vXamComboEditorSelectionChanged += new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcReturnType_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcReturnType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        #endregion Load List Controls


        #region Events

        #region ProxyWrapperEvents


        #endregion ProxyWrapperEvents

        #region Control Events


        /// <summary>
        /// 	<para>Part of the standard control events in the Control Events region.<br/>
        ///     If the _isActivePropertiesControl variable is false, then this control
        ///     (ucWcStoredProcProps) was not the current ‘Active Properties Control’. However, now that
        ///     this field has the focus means that ucWcStoredProcProps is now the ‘Active Properties
        ///     Control’. Therefore:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<div class="xmldocbulletlist">
        /// 				<see cref="_isActivePropertiesControl">_isActivePropertiesControl</see>
        ///                 will be set to true (so this code is not executed again until
        ///                 ucWcStoredProcProps has returned to Not Active).
        ///             </div>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="RaiseCurrentEntityStateChanged">RaiseCurrentEntityStateChanged</see> will
        ///             be called. Refer to its documentation for information on what it does and
        ///             why it’s called here.
        ///         </item>
        /// 	</list>
        /// </summary>
        void OnDataControlGotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDataControlGotFocus", IfxTraceCategory.Enter);
                if (_isActivePropertiesControl == false)
                {
                    _isActivePropertiesControl = true;
                    RaiseCurrentEntityStateChanged();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDataControlGotFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDataControlGotFocus", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every TextBox so the ‘TextChanged’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="TextBoxTextChanged">TextBoxTextChanged</see> located in the
        ///         ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no TextBox controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                if (sender.GetType().Name != "vTextBox") { return; }
                vTextBox bx = (vTextBox)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTextChanged", new ValuePair[] { new ValuePair("bx.Name", bx.Name) }, IfxTraceCategory.Enter);
                switch (bx.Name)
                {
                    case "Sp_Name":
                        objB.Sp_Name = bx.Text;
                        break;

                    case "Sp_SprocGroups":
                        objB.Sp_SprocGroups = bx.Text;
                        break;

                    case "Sp_WireTypeName":
                        objB.Sp_WireTypeName = bx.Text;
                        break;

                    case "Sp_MethodName":
                        objB.Sp_MethodName = bx.Text;
                        break;

                    case "Sp_Notes":
                        objB.Sp_Notes = bx.Text;
                        break;

                }
                OnTextBoxTextChanged_Custom(bx);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTextChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTextChanged", IfxTraceCategory.Leave);
            }
        }

        void TextControlGotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                Control ctl = (Control)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TextControlGotFocus", new ValuePair[] { new ValuePair("ctl.Name", ctl.Name) }, IfxTraceCategory.Enter);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TextControlGotFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TextControlGotFocus", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every TextBox so the ‘TextChanged’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="TextBoxLostFocus">TextBoxLostFocus</see> located in the
        ///         ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no TextBox controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnTexBoxLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                FLG_UPDATING_FIELDVALUE = true;
                TextBox bx = sender as TextBox;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTexBoxLostFocus", new ValuePair[] { new ValuePair("bx.Name", bx.Name) }, IfxTraceCategory.Enter);
                if (bx == null) { return; }
                switch (bx.Name)
                {
                    case "Sp_Name":
                        if (objB.Sp_Name == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.Sp_Name;
                        }
                        break;

                    case "Sp_SprocGroups":
                        if (objB.Sp_SprocGroups == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.Sp_SprocGroups;
                        }
                        break;

                    case "Sp_WireTypeName":
                        if (objB.Sp_WireTypeName == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.Sp_WireTypeName;
                        }
                        break;

                    case "Sp_MethodName":
                        if (objB.Sp_MethodName == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.Sp_MethodName;
                        }
                        break;

                    case "Sp_AssocEntityNames":
                        if (objB.Sp_AssocEntityNames == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.Sp_AssocEntityNames;
                        }
                        break;

                    case "Sp_Notes":
                        if (objB.Sp_Notes == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.Sp_Notes;
                        }
                        break;

                }
                OnTextBoxLostFocus_Custom(bx);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTexBoxLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTexBoxLostFocus", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every DatePicker so the ‘SelectionChanged’ event handling
        ///     is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnDatePicker_TextChanged">OnDatePicker_TextChanged(ctl)</see>
        ///         located in the ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no DatePicker controls in
        ///     case some are added at a later date.</para>
        /// </summary>
        void OnDatePicker_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePicker_TextChanged", IfxTraceCategory.Enter);
                if (ctl == null) { return; }
                switch (ctl.Name)
                {
                }
                OnDatePicker_TextChanged_Custom(ctl);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePicker_TextChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePicker_TextChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every DatePicker so the ‘LostFocus’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnDatePickerLostFocus">OnDatePickerLostFocus(ctl)</see>
        ///         located in the ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no DatePicker controls in
        ///     case some are added at a later date.</para>
        /// </summary>
        void OnDatePickerLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                FLG_UPDATING_FIELDVALUE = true;
                DatePicker ctl = (DatePicker)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePickerLostFocus", IfxTraceCategory.Enter);
                switch (ctl.Name)
                {

                }
                OnDatePickerLostFocus_Custom(ctl);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePickerLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePickerLostFocus", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every TimePicker so the ‘ValueChanging’ event handling
        ///     is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnTimePicker_ValueChanged">OnTimePicker_ValueChanged(ctl)</see>
        ///         located in the ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no TimePicker controls in
        ///     case some are added at a later date.</para>
        /// </summary>
        void OnTimePicker_ValueChanged(object sender, RoutedPropertyChangedEventArgs<DateTime?> e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                TimePicker tp = sender as TimePicker;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePicker_ValueChanged", IfxTraceCategory.Enter);
                if (tp == null) { return; }
                switch (tp.Name)
                {
                }
                OnTimePicker_ValueChanged_Custom(tp);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePicker_ValueChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePicker_ValueChanged", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// 	<para>Is wired up to every TimePicker so the ‘LostFocus’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnTimePickerLostFocus">OnTimePickerLostFocus(ctl)</see>
        ///         located in the ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no TimePicker controls in
        ///     case some are added at a later Time.</para>
        /// </summary>        void OnTimePickerLostFocus(object sender, RoutedEventArgs e)
        void OnTimePickerLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                TimePicker tp = sender as TimePicker;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePickerLostFocus", IfxTraceCategory.Enter);
                if (tp == null) { return; }
                switch (tp.Name)
                {
                }
                OnTimePickerLostFocus_Custom(tp);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePickerLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePickerLostFocus", IfxTraceCategory.Leave);
            }
        }



        /// <summary>
        /// 	<para>Is wired up to every XamComboEditor so the ‘OnXamComboEditorSelectionChanged’
        ///     event handling is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnXamComboSelectedItemChanged_Custom">OnXamComboSelectedItemChanged_Custom(cmb)</see>
        ///         located in the ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no XamComboEditor controls in case
        ///     some are added at a later date.</para>
        /// </summary>
        void OnXamComboEditorSelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            // Used for the Infragistics XamComboEditor.  There may not be any here, but this code is inplace incase we add one later
            try
            {
                if (FLG_FORM_IS_LOADING == true) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                XamComboEditor cmb = (XamComboEditor)sender;
                ComboItemList data = cmb.ItemsSource as ComboItemList;
                if (data != null && data.IsRefreshingData == true) { return; } // The combo's list is being updated to don't do anything.
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboEditorSelectionChanged", new ValuePair[] { new ValuePair("cmb.Name", cmb.Name) }, IfxTraceCategory.Enter);
                switch (cmb.Name)
                {
                    case "xxx":

                        break;
                }

                OnXamComboSelectedItemChanged_Custom(cmb);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboEditorSelectionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboEditorSelectionChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every XamComboEditor so the ‘OnVXamComboEditorSelectionChanged’
        ///     event handling is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnVXamComboSelectedItemChanged_Custom">OnVXamComboSelectedItemChanged_Custom(cmb)</see>
        ///         located in the ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no XamComboEditor controls in case
        ///     some are added at a later date.</para>
        /// </summary>
        void OnVXamComboEditorSelectionChanged(object sender, vXamComboEditorSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            // Used for the Infragistics XamComboEditor.  There may not be any here, but this code is inplace incase we add one later
            try
            {
                if (FLG_FORM_IS_LOADING == true) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                if (objB == null) { return; }  // For some reason this event is fired before it's time as there is no underling biz object yet, and we get an error when referencing objB.
                vXamComboEditor cmb = (vXamComboEditor)sender;
                ComboItemList data = cmb.ItemsSource as ComboItemList;
                if (data != null && data.IsRefreshingData == true)
                {
                    // remove the event
                    cmb.vXamComboEditorSelectionChanged -= new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                    // re-set the selected index which we lost when updateing the ItemsSource
                    cmb.SelectedIndex = e.OldIndex;
                    // add the event back on
                    cmb.vXamComboEditorSelectionChanged += new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                    return;  // The combo's list is being updated to don't do anything.
                }
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamComboEditorSelectionChanged", new ValuePair[] { new ValuePair("cmb.Name", cmb.Name) }, IfxTraceCategory.Enter);
                switch (cmb.Name)
                {

                    case "Sp_AssocEntity_Id":
                        if (Sp_AssocEntity_Id.SelectedItem == null)
                        {
                            objB.Sp_AssocEntity_Id = null;
                        }
                        else
                        {
                            objB.Sp_AssocEntity_Id = (Guid)((ComboItem)Sp_AssocEntity_Id.SelectedItem).Id;
                        }
                        break;


                    case "Sp_SpRsTp_Id":
                        if (Sp_SpRsTp_Id.SelectedItem == null)
                        {
                            objB.Sp_SpRsTp_Id = null;
                        }
                        else
                        {
                            objB.Sp_SpRsTp_Id = (Int32)((ComboItem)Sp_SpRsTp_Id.SelectedItem).Id;
                        }
                        break;


                    case "Sp_SpRtTp_Id":
                        if (Sp_SpRtTp_Id.SelectedItem == null)
                        {
                            objB.Sp_SpRtTp_Id = null;
                        }
                        else
                        {
                            objB.Sp_SpRtTp_Id = (Int32)((ComboItem)Sp_SpRtTp_Id.SelectedItem).Id;
                        }
                        break;

                }
                OnVXamComboSelectedItemChanged_Custom(cmb);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamComboEditorSelectionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamComboEditorSelectionChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every XamComboEditor so the ‘OnXamComboEditorLostFocus’ event
        ///     handling is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnXamComboLostFocus_Custom">OnXamComboLostFocus_Custom(cmb)</see> located in the
        ///         ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no XamComboEditor controls in case
        ///     some are added at a later date.<br/></para>
        /// </summary>
        void OnXamComboEditorLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                FLG_UPDATING_FIELDVALUE = true;
                XamComboEditor cmb = (XamComboEditor)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboEditorLostFocus", new ValuePair[] { new ValuePair("cmb.Name", cmb.Name) }, IfxTraceCategory.Enter);
                switch (cmb.Name)
                {
                    case "Sp_AssocEntity_Id":
                        Set_ComboEditorItem_SelectedItem_ComboItemType(Sp_AssocEntity_Id, objB.Sp_AssocEntity_Id);
                        break;

                    case "Sp_SpRsTp_Id":
                        Set_ComboEditorItem_SelectedItem_ComboItemType(Sp_SpRsTp_Id, objB.Sp_SpRsTp_Id);
                        break;

                    case "Sp_SpRtTp_Id":
                        Set_ComboEditorItem_SelectedItem_ComboItemType(Sp_SpRtTp_Id, objB.Sp_SpRtTp_Id);
                        break;

                }
                OnXamComboLostFocus_Custom(cmb);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboEditorLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();

            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboEditorLostFocus", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every XamMultiColumnComboEditor so the ‘OnXamMultiColumnComboEditorSelectionChanged’
        ///     event handling is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnXamComboSelectedItemChanged_Custom">OnXamComboSelectedItemChanged_Custom(cmb)</see>
        ///         located in the ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no XamMultiColumnComboEditor controls in case
        ///     some are added at a later date.</para>
        /// </summary>
        void OnXamMultiColumnComboEditorSelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            // Used for the Infragistics XamMultiColumnComboEditor.  There may not be any here, but this code is inplace incase we add one later
            try
            {
                if (FLG_FORM_IS_LOADING == true) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                XamMultiColumnComboEditor cmb = (XamMultiColumnComboEditor)sender;
                ComboItemList data = cmb.ItemsSource as ComboItemList;
                if (data != null && data.IsRefreshingData == true) { return; } // The combo's list is being updated to don't do anything.
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboEditorSelectionChanged", new ValuePair[] { new ValuePair("cmb.Name", cmb.Name) }, IfxTraceCategory.Enter);
                switch (cmb.Name)
                {
                    case "xxx":

                        break;
                }

                OnXamMultiColumnComboSelectedItemChanged_Custom(cmb);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboEditorSelectionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboEditorSelectionChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every vXamMultiColumnComboEditor so the ‘OnVXamMultiColumnComboEditorSelectionChanged’
        ///     event handling is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnXamComboSelectedItemChanged_Custom">OnXamComboSelectedItemChanged_Custom(cmb)</see>
        ///         located in the ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no vXamMultiColumnComboEditor controls in case
        ///     some are added at a later date.</para>
        /// </summary>
        void OnVXamMultiColumnComboEditorSelectionChanged(object sender, vXamMultiColumnComboEditorSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            // Used for the Infragistics vXamMultiColumnComboEditor.  There may not be any here, but this code is inplace incase we add one later
            try
            {
                if (FLG_FORM_IS_LOADING == true) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                vXamMultiColumnComboEditor cmb = (vXamMultiColumnComboEditor)sender;
                ComboItemList data = cmb.ItemsSource as ComboItemList;
                if (data != null && data.IsRefreshingData == true)
                {
                    // remove the event
                    cmb.vXamMultiColumnComboEditorSelectionChanged -= new vXamMultiColumnComboEditorSelectionChangedEventHandler(OnVXamMultiColumnComboEditorSelectionChanged);
                    // re-set the selected index which we lost when updateing the ItemsSource
                    cmb.SelectedIndex = e.OldIndex;
                    // add the event back on
                    cmb.vXamMultiColumnComboEditorSelectionChanged += new vXamMultiColumnComboEditorSelectionChangedEventHandler(OnVXamMultiColumnComboEditorSelectionChanged);
                    return;  // The combo's list is being updated to don't do anything.
                }
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamMultiColumnComboEditorSelectionChanged", new ValuePair[] { new ValuePair("cmb.Name", cmb.Name) }, IfxTraceCategory.Enter);
                switch (cmb.Name)
                {
                    case "xxxxxxxxxxxx":
                        //objB.xxxxxxxxxxxx = xxxxxxxxxxxx_item.xxxxxxxxxxxxColumnName;
                        break;
                }
                OnVXamMultiColumnComboSelectedItemChanged_Custom(cmb);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamMultiColumnComboEditorSelectionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamMultiColumnComboEditorSelectionChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every XamMultiColumnComboEditor so the ‘OnXamMultiColumnComboEditorLostFocus’ event
        ///     handling is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnXamComboLostFocus_Custom">OnXamComboLostFocus_Custom(cmb)</see> located in the
        ///         ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no XamMultiColumnComboEditor controls in case
        ///     some are added at a later date.<br/></para>
        /// </summary>
        void OnXamMultiColumnComboEditorLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                FLG_UPDATING_FIELDVALUE = true;
                XamMultiColumnComboEditor cmb = (XamMultiColumnComboEditor)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboEditorLostFocus", new ValuePair[] { new ValuePair("cmb.Name", cmb.Name) }, IfxTraceCategory.Enter);
                switch (cmb.Name)
                {
                }
                OnXamMultiColumnComboLostFocus_Custom(cmb);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboEditorLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();

            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboEditorLostFocus", IfxTraceCategory.Leave);
            }
        }

        private void XamColorPicker_SelectedColorChanged(object sender, SelectedColorChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (FLG_FORM_IS_LOADING == true) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                if (sender.GetType().Name != "XamColorPicker") { return; }
                XamColorPicker cp = sender as XamColorPicker;
                if (cp == null) { return; }
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Enter);

                switch (cp.Name)
                {
                }
                XamColorPicker_SelectedColorChanged_custom(cp, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Leave);
            }
        }



        /// <summary>
        /// 	<para>Is wired up to every CheckBox so the ‘Click’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="CheckBoxClick">CheckBoxClick(chk)</see> located in the
        ///         ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no CheckBox controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnCheckBoxClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                if (sender.GetType().Name != "vCheckBox") { return; }
                vCheckBox chk = (vCheckBox)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxClick", new ValuePair[] { new ValuePair("chk.Name", chk.Name) }, IfxTraceCategory.Enter);
                switch (chk.Name)
                {
                    case "Sp_IsCodeGen":
                        objB.Sp_IsCodeGen = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsReadyCodeGen":
                        objB.Sp_IsReadyCodeGen = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsCodeGenComplete":
                        objB.Sp_IsCodeGenComplete = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsTagForCodeGen":
                        objB.Sp_IsTagForCodeGen = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsTagForOther":
                        objB.Sp_IsTagForOther = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsBuildDataAccessMethod":
                        objB.Sp_IsBuildDataAccessMethod = (bool)chk.IsChecked;
                        break;

                    case "Sp_BuildListClass":
                        objB.Sp_BuildListClass = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsFetchEntity":
                        objB.Sp_IsFetchEntity = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsStaticList":
                        objB.Sp_IsStaticList = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsTypeComboItem":
                        objB.Sp_IsTypeComboItem = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsCreateWireType":
                        objB.Sp_IsCreateWireType = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsInputParamsAsObjectArray":
                        objB.Sp_IsInputParamsAsObjectArray = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsParamsAutoRefresh":
                        objB.Sp_IsParamsAutoRefresh = (bool)chk.IsChecked;
                        break;

                    case "Sp_HasNewParams":
                        objB.Sp_HasNewParams = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsParamsValid":
                        objB.Sp_IsParamsValid = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsParamValueSetValid":
                        objB.Sp_IsParamValueSetValid = (bool)chk.IsChecked;
                        break;

                    case "Sp_HasNewColumns":
                        objB.Sp_HasNewColumns = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsColumnsValid":
                        objB.Sp_IsColumnsValid = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsValid":
                        objB.Sp_IsValid = (bool)chk.IsChecked;
                        break;

                    case "Sp_IsActiveRow":
                        objB.Sp_IsActiveRow = (bool)chk.IsChecked;
                        break;


                }
                OnCheckBoxClick_Custom(chk);
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();

            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxClick", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every CheckBox so the ‘LostFocus’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="CheckBoxLostFocus">CheckBoxLostFocus(chk)</see> located in the
        ///         ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no CheckBox controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnCheckBoxLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                FLG_UPDATING_FIELDVALUE = true;
                if (sender.GetType().Name != "vCheckBox") { return; }
                vCheckBox chk = (vCheckBox)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxLostFocus", IfxTraceCategory.Enter);
                switch (chk.Name)
                {
                    case "Sp_IsCodeGen":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsCodeGen);
                        break;

                    case "Sp_IsReadyCodeGen":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsReadyCodeGen);
                        break;

                    case "Sp_IsCodeGenComplete":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsCodeGenComplete);
                        break;

                    case "Sp_IsTagForCodeGen":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsTagForCodeGen);
                        break;

                    case "Sp_IsTagForOther":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsTagForOther);
                        break;

                    case "Sp_IsBuildDataAccessMethod":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsBuildDataAccessMethod);
                        break;

                    case "Sp_BuildListClass":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_BuildListClass);
                        break;

                    case "Sp_IsFetchEntity":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsFetchEntity);
                        break;

                    case "Sp_IsStaticList":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsStaticList);
                        break;

                    case "Sp_IsTypeComboItem":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsTypeComboItem);
                        break;

                    case "Sp_IsCreateWireType":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsCreateWireType);
                        break;

                    case "Sp_IsInputParamsAsObjectArray":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsInputParamsAsObjectArray);
                        break;

                    case "Sp_IsParamsAutoRefresh":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsParamsAutoRefresh);
                        break;

                    case "Sp_HasNewParams":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_HasNewParams);
                        break;

                    case "Sp_IsParamsValid":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsParamsValid);
                        break;

                    case "Sp_IsParamValueSetValid":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsParamValueSetValid);
                        break;

                    case "Sp_HasNewColumns":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_HasNewColumns);
                        break;

                    case "Sp_IsColumnsValid":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsColumnsValid);
                        break;

                    case "Sp_IsValid":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsValid);
                        break;

                    case "Sp_IsActiveRow":
                        chk.IsChecked = Convert.ToBoolean(objB.Sp_IsActiveRow);
                        break;


                }
                OnCheckBoxLostFocus_Custom(chk);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxLostFocus", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every RadioButton so the ‘Click’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="RadioButtonClick">RadioButtonClick(rdb)</see> located in the
        ///         ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no RadioButton controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnRadioButtonClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                if (sender.GetType().Name != "RadioButton") { return; }
                RadioButton rdb = (RadioButton)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonClick", new ValuePair[] { new ValuePair("rdb.Name", rdb.Name) }, IfxTraceCategory.Enter);
                switch (rdb.Name)
                {

                }
                OnRadioButtonClick_Custom(rdb);
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonClick", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every RadioButton so the ‘LostFocus’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnRadioButtonLostFocus_Custom">OnRadioButtonLostFocus_Custom(rdb)</see> located in the
        ///         ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no RadioButton controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnRadioButtonLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                FLG_UPDATING_FIELDVALUE = true;
                RadioButton rdb = (RadioButton)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonLostFocus", IfxTraceCategory.Enter);
                if (sender.GetType().Name != "RadioButton") { return; }
                switch (rdb.Name)
                {

                }
                OnRadioButtonLostFocus_Custom(rdb);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonLostFocus", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every vRadioButtonGroup so the ‘Checked’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="vRadioButtonGroupClick">RadioButtonItemChecked(rbg)</see> located in the
        ///         ucWcStoredProcProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no vRadioButtonGroup controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnvRadioButtonGroupItemChecked(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                if (sender.GetType().Name != "RadioButton") { return; }
                vRadioButtonGroup rbg = sender as vRadioButtonGroup;
                if (rbg == null) { return; }
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnvRadioButtonGroupItemChecked", new ValuePair[] { new ValuePair("rbg.Name", rbg.Name) }, IfxTraceCategory.Enter);
                switch (rbg.Name)
                {

                }
                OnvRadioButtonGroupItemChecked_Custom(rbg);
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnvRadioButtonGroupItemChecked", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnvRadioButtonGroupItemChecked", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Call this method to disable all read only controls. This is a stub to be used by
        /// the security layer.
        /// </summary>
        void DisableReadonlycontrols()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DisableReadonlycontrols", IfxTraceCategory.Enter);
                Sp_Name.IsEnabled = false;
                Sp_IsCodeGen.IsEnabled = false;
                Sp_IsReadyCodeGen.IsEnabled = false;
                Sp_IsCodeGenComplete.IsEnabled = false;
                Sp_IsTagForCodeGen.IsEnabled = false;
                Sp_IsTagForOther.IsEnabled = false;
                Sp_SprocGroups.IsEnabled = false;
                Sp_IsBuildDataAccessMethod.IsEnabled = false;
                Sp_BuildListClass.IsEnabled = false;
                Sp_IsFetchEntity.IsEnabled = false;
                Sp_IsStaticList.IsEnabled = false;
                Sp_IsTypeComboItem.IsEnabled = false;
                Sp_IsCreateWireType.IsEnabled = false;
                Sp_WireTypeName.IsEnabled = false;
                Sp_MethodName.IsEnabled = false;
                Sp_AssocEntity_Id.IsEnabled = false;
                Sp_AssocEntityNames.IsEnabled = false;
                Sp_SpRsTp_Id.IsEnabled = false;
                Sp_SpRtTp_Id.IsEnabled = false;
                Sp_IsInputParamsAsObjectArray.IsEnabled = false;
                Sp_IsParamsAutoRefresh.IsEnabled = false;
                Sp_HasNewParams.IsEnabled = false;
                Sp_IsParamsValid.IsEnabled = false;
                Sp_IsParamValueSetValid.IsEnabled = false;
                Sp_HasNewColumns.IsEnabled = false;
                Sp_IsColumnsValid.IsEnabled = false;
                Sp_IsValid.IsEnabled = false;
                Sp_Notes.IsEnabled = false;
                Sp_IsActiveRow.IsEnabled = false;

                DisableReadonlycontrols_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DisableReadonlycontrols", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DisableReadonlycontrols", IfxTraceCategory.Leave);
            }
        }

        #endregion Control Events


        #region Form Events  (Mostly State Related)

        /// <summary>
        ///     This method has no other option than to pass in ‘this’ control’s <see cref="CurrentBusinessObject">CurrentBusinessObject</see> as the input parameter for the
        ///     <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event.
        /// </summary>
        /// <overloads>
        /// 	<para>These overloads raise the CurrentEntityStateChanged event passing in
        ///     the:</para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>ActiveEntityControl</item>
        /// 		<item>ActivePropertiesControl</item>
        /// 		<item>CurrentBusinessObject</item>
        /// 	</list>
        /// 	<para>and bubbles up to the top level control. This notifies all controls along the
        ///     about which controls are active and the current state so they can always be
        ///     configures accordingly. Now that the top level control (perhaps the main
        ///     application window) has a reference to these 3 important objects, it can easily
        ///     communicate with then as the use interacts with the application.</para>
        /// 	<para></para>
        /// </overloads>
        public void RaiseCurrentEntityStateChanged()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseCurrentEntityStateChanged", IfxTraceCategory.Enter);
                RaiseCurrentEntityStateChanged(CurrentBusinessObject.StateSwitch);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseCurrentEntityStateChanged", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseCurrentEntityStateChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// This method passes null for the activeEntityControl parameter. This way, when the
        /// event bubbles up to the parent (an <a href="D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\The_Entity_Manager.html">Entity Manager</a> control), when the parent sees this
        /// parameter is null, it will pass in a reference to itself before raising the event up to
        /// the next parent.
        /// </summary>
        /// <param name="state">
        /// As this event bubbles up through the various <a href="D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\The_Entity_Manager.html">Entity Managers</a>, they can read this
        /// state and configure themselves accordingly. When this event reaches the top level
        /// control, the application, including all menus, will configure itself according to the
        /// current state.
        /// </param>
        public void RaiseCurrentEntityStateChanged(EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseCurrentEntityStateChanged(EntityStateSwitch state)", IfxTraceCategory.Enter);
                OnCurrentEntityStateChanged(this, new CurrentEntityStateArgs(state, null, this, CurrentBusinessObject));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseCurrentEntityStateChanged(EntityStateSwitch state)", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseCurrentEntityStateChanged(EntityStateSwitch state)", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     This event is usually initiated by the by the business object when the <see cref="OnCurrentEntityStateChanged">StateSwitch</see> changes, or when some controls get
        ///     the focus. This event will bubble up through the various <a href="D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\The_Entity_Manager.html">Entity Managers</a> so they
        ///     can read this <see cref="TypeServices.EntityState">state</see> and configure
        ///     themselves accordingly. When this event reaches the top level control, the
        ///     application, including all menus, will configure itself according to the current
        ///     state. The Args are very important as they contain a reference to the Active Entity
        ///     Manager and Active Properties control. See <see cref="TypeServices.CurrentEntityStateArgs">CurrentEntityStateArgs</see> to learn more
        ///     about how they help manage the overall application state.
        /// </summary>
        /// <param name="e">See <see cref="TypeServices.CurrentEntityStateArgs">CurrentEntityStateArgs</see></param>
        void OnCurrentEntityStateChanged(object sender, CurrentEntityStateArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Enter);
                ConfigureToCurrentEntityState(e.State);
                CurrentEntityStateEventHandler handler = CurrentEntityStateChanged;
                CurrentEntityStateArgs args;
                if (e.ActivePropertiesControl == null)
                {
                    args = new CurrentEntityStateArgs(e.State, e.ActiveEntityControl, this, e.ActiveBusinessObject);
                }
                else
                {
                    args = e;
                }
                if (handler != null)
                {
                    handler(this, args);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>This method is mostly obsolete, but remains because it’s an easy way to be
        ///     notified of the current broken rule.</para>
        /// 	<para>
        ///         This method is hooked to the <see cref="BrokenRuleChanged">BrokenRuleChanged</see> event which is initiated by
        ///         BrokenRuleManager and bubbles up to the business object and then to this
        ///         control (ucWcStoredProcProps) (or in some cases a ucEntityList control when it’s in
        ///         Read/Write mode).
        ///     </para>
        /// 	<para><br/>
        ///     For information on how Broken Rules are currently being managed, see (in the order
        ///     of execution):</para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>In the business object’s data property – FieldName_Validate();</item>
        /// 		<item>
        ///             In the FieldName_Validate method this line executes:<br/>
        ///             _brokenRuleManager.<see cref="TypeServices.BrokenRuleManager.AddBrokenRuleForProperty">AddBrokenRuleForProperty</see>("FieldName",
        ///             BROKENRULE_ FieldName _SomeRuleName);
        ///         </item>
        /// 		<item>
        /// 			<see cref="TypeServices.BrokenRuleManager.SetEntityValidState">BrokenRuleManager.SetEntityValidState()</see>
        /// 		</item>
        /// 	</list>
        /// 	<para><br/>
        ///     And for the Broken Rule Tooltips, see:</para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>
        /// 			<see cref="tt_Loaded">tt_Loaded</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="LoadToolTipControl">LoadToolTipControl(string fieldName)</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="GetListOfValidationRuleMessagesFromBusinessObject">GetListOfValidationRuleMessagesFromBusinessObject(string
        ///             fieldName)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void OnBrokenRuleChanged(object sender, BrokenRuleArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Enter);
                SetBrokenRuleText(e.Rule);
                BrokenRuleEventHandler handler = BrokenRuleChanged;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     This method is hooked to the business object’s <see cref="EntityBll.WcStoredProc_Bll.ControlValidStateChanged">ControlValidStateChanged</see> event
        ///     which is raised by the <see cref="EntityBll.WcStoredProc_Bll.RaiseEventControlValidStateChanged">RaiseEventControlValidStateChanged</see>
        ///     method which is called by called by the business object’s FieldName_Validate
        ///     method.
        /// </summary>
        private void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", IfxTraceCategory.Enter);
                switch (e.PropertyName)
                {
                    case "Sp_Name":
                        SetControlValidAppearance(Sp_Name, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsCodeGen":
                        SetControlValidAppearance(Sp_IsCodeGen, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsReadyCodeGen":
                        SetControlValidAppearance(Sp_IsReadyCodeGen, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsCodeGenComplete":
                        SetControlValidAppearance(Sp_IsCodeGenComplete, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsTagForCodeGen":
                        SetControlValidAppearance(Sp_IsTagForCodeGen, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsTagForOther":
                        SetControlValidAppearance(Sp_IsTagForOther, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_SprocGroups":
                        SetControlValidAppearance(Sp_SprocGroups, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsBuildDataAccessMethod":
                        SetControlValidAppearance(Sp_IsBuildDataAccessMethod, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_BuildListClass":
                        SetControlValidAppearance(Sp_BuildListClass, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsFetchEntity":
                        SetControlValidAppearance(Sp_IsFetchEntity, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsStaticList":
                        SetControlValidAppearance(Sp_IsStaticList, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsCreateWireType":
                        SetControlValidAppearance(Sp_IsCreateWireType, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_WireTypeName":
                        SetControlValidAppearance(Sp_WireTypeName, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_MethodName":
                        SetControlValidAppearance(Sp_MethodName, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_AssocEntityNames":
                        SetControlValidAppearance(Sp_AssocEntityNames, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_SpRsTp_Id":
                        SetControlValidAppearance(Sp_SpRsTp_Id, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_SpRtTp_Id":
                        SetControlValidAppearance(Sp_SpRtTp_Id, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsInputParamsAsObjectArray":
                        SetControlValidAppearance(Sp_IsInputParamsAsObjectArray, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsParamsAutoRefresh":
                        SetControlValidAppearance(Sp_IsParamsAutoRefresh, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_HasNewParams":
                        SetControlValidAppearance(Sp_HasNewParams, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsParamsValid":
                        SetControlValidAppearance(Sp_IsParamsValid, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsParamValueSetValid":
                        SetControlValidAppearance(Sp_IsParamValueSetValid, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_HasNewColumns":
                        SetControlValidAppearance(Sp_HasNewColumns, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsColumnsValid":
                        SetControlValidAppearance(Sp_IsColumnsValid, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsValid":
                        SetControlValidAppearance(Sp_IsValid, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_Notes":
                        SetControlValidAppearance(Sp_Notes, e.IsValid, e.IsDirty);
                        break;
                    case "Sp_IsActiveRow":
                        SetControlValidAppearance(Sp_IsActiveRow, e.IsValid, e.IsDirty);
                        break;
                }
                OnControlValidStateChanged_Custom(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     This method is hooked to the business object’s <see cref="EntityBll.WellSLTest_Bll.CrudFailed">CrudFailed</see> event and will continue to bubble
        ///     up to the top level control. The CrudFailed event is raised when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnCrudFailed(object sender, CrudFailedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Enter);
                CrudFailedEventHandler handler = CrudFailed;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Leave);
            }
        }

        #endregion Form Events  (Mostly State Related)

        #region Button Events

        //        void OnbtnCloseClick(object sender, RoutedEventArgs e)
        //        {
        //            CloseMe();
        //        }
        //
        //        void OnbtnNewClick(object sender, RoutedEventArgs e)
        //        {
        //            NewEntityRow();
        //        }
        //
        //        void OnbtnOpenClick(object sender, RoutedEventArgs e)
        //        {
        //            //Open();
        //        }
        //
        //        void OnbtnSaveClick(object sender, RoutedEventArgs e)
        //        {
        //            Save();
        //        }
        //
        //        void OnbtnUnDoClick(object sender, RoutedEventArgs e)
        //        {
        //            UnDo();
        //        }

        #endregion Button Events

        #endregion Events


        #region General Methods and Properties


        #region General Methods


        #region Data Related

        /// <summary>
        ///     Obsolete. This was used to load the initial data. Call <see cref="GetEntityRow">GetEntityRow</see> instead.
        /// </summary>
        void LoadData(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadData", IfxTraceCategory.Enter);
                if (Id == null)
                {
                    NewEntityRow();
                }
                else
                {
                    GetEntityRow(Id);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadData", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadData", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Can be called by a parent object to pass in the parent’s Id which will be based
        /// to he business object’s StandingFK property. For information on the StandingFK
        /// property, look up the documentation on a business object which is in a child
        /// relationship to another entity.
        /// </summary>
        public void SetParent(long Id)
        {
            throw new NotImplementedException();
        }

        public void SetParent(int Id)
        {
            throw new NotImplementedException();
        }

        public void SetParent(short Id)
        {
            throw new NotImplementedException();
        }

        public void SetParent(byte Id)
        {
            throw new NotImplementedException();
        }

        public void SetParent(Guid Id)
        {
            throw new NotImplementedException();
        }




        /// <summary>
        ///     Acts as a surrogate for <see cref="SetBusinessObject">SetBusinessObject</see> by
        ///     receiving the business object as <see cref="TypeServices.IBusinessObject">IBusinessObject</see> and then passed it to
        ///     SetBusinessObject. This allows the SetBusinessObject functionality to be available
        ///     in the <see cref="TypeServices.IEntitiyPropertiesControl">IEntitiyPropertiesControl</see> interface
        ///     making this very extendable.
        /// </summary>
        public void SetIBusinessObject(IBusinessObject obj)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetIBusinessObject(IBusinessObject obj)", IfxTraceCategory.Enter);
                SetBusinessObject((WcStoredProc_Bll)obj);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetIBusinessObject(IBusinessObject obj)", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetIBusinessObject(IBusinessObject obj)", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Typically receives a business object (<see cref="EntityBll.WcStoredProc_Bll">WcStoredProc_Bll</see>) from the parent control (typically
        ///     <see cref="ucWcStoredProc">ucWcStoredProc</see>) and performs all of the loading functions to
        ///     make it the current business object; and refreshes all the data fields with its
        ///     data and sets the correct <see cref="TypeServices.EntityState">EntityState</see>.
        /// </summary>
        public void SetBusinessObject(WcStoredProc_Bll obj)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetIBusinessObject", IfxTraceCategory.Enter);
                EntityStateSwitch state;
                if (obj == null)
                {
                    //SetControlsDefaultValidState()  should we call NewEntityRow first?
                    RemoveBusnessObjectEvents();
                    ClearDataFromUI();
                    state = EntityStateSwitch.None;
                    objB = null;
                    _hasBusinessObject = false;
                }
                else
                {
                    if (objB != null)
                    {
                        RemoveBusnessObjectEvents();
                        objB = null;
                    }
                    objB = obj;
                    _hasBusinessObject = true;
                    AddBusinessObjectEvents();
                    SetState();
                    SetControlsDefaultValidState();
                    state = objB.StateSwitch;
                }
                SetEnabledState(objB != null);
                RaiseCurrentEntityStateChanged(state);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetBusinessObject", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetBusinessObject", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Acts as a surrogate for <see cref="GetBusinessObject">GetBusinessObject</see> by
        ///     returning the current business object (<see cref="EntityBll.WcStoredProc_Bll">WcStoredProc_Bll</see>) as <see cref="TypeServices.IBusinessObject">IBusinessObject</see>. This method is part of the
        ///     <see cref="TypeServices.IEntitiyPropertiesControl">IEntitiyPropertiesControl</see>
        ///     interface making this control very extendable.
        /// </summary>
        public IBusinessObject GetIBusinessObject()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIBusinessObject", IfxTraceCategory.Enter);
                return (IBusinessObject)objB;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIBusinessObject", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIBusinessObject", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Returns the current business object (<see cref="EntityBll.WcStoredProc_Bll">WcStoredProc_Bll</see>).
        /// </summary>
        public WcStoredProc_Bll GetBusinessObject()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBusinessObject", IfxTraceCategory.Enter);
                return objB;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBusinessObject", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBusinessObject", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Clears all data fields. Is called from the <see cref="SetBusinessObject">SetBusinessObject</see> method when a null business object
        ///     (<see cref="EntityBll.WcStoredProc_Bll">WcStoredProc_Bll</see>) is passed in and therefore making
        ///     the WcStoredProc_Bll default values not available.
        /// </summary>
        private void ClearDataFromUI()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearDataFromUI", IfxTraceCategory.Enter);
                FLG_LOADING_REC = true;
                // Remove all TextBox TextChanged Events so they don't fire when we set thier new values.  The TextChanged Event is unpredictable in SL so we need to remove it here.
                DataControlDataChangedEvents_RemoveAll();
                Sp_Name.Text = "";
                Sp_IsCodeGen.IsChecked = false;
                Sp_IsReadyCodeGen.IsChecked = false;
                Sp_IsCodeGenComplete.IsChecked = false;
                Sp_IsTagForCodeGen.IsChecked = false;
                Sp_IsTagForOther.IsChecked = false;
                Sp_SprocGroups.Text = "";
                Sp_IsBuildDataAccessMethod.IsChecked = false;
                Sp_BuildListClass.IsChecked = false;
                Sp_IsFetchEntity.IsChecked = false;
                Sp_IsStaticList.IsChecked = false;
                Sp_IsTypeComboItem.IsChecked = true;
                Sp_IsCreateWireType.IsChecked = false;
                Sp_WireTypeName.Text = "";
                Sp_MethodName.Text = "";
                Sp_AssocEntity_Id.SelectedIndex = -1;
                Sp_AssocEntityNames.Text = "";
                Sp_SpRsTp_Id.SelectedIndex = -1;
                Sp_SpRtTp_Id.SelectedIndex = -1;
                Sp_IsInputParamsAsObjectArray.IsChecked = false;
                Sp_IsParamsAutoRefresh.IsChecked = true;
                Sp_HasNewParams.IsChecked = false;
                Sp_IsParamsValid.IsChecked = false;
                Sp_IsParamValueSetValid.IsChecked = false;
                Sp_HasNewColumns.IsChecked = false;
                Sp_IsColumnsValid.IsChecked = false;
                Sp_IsValid.IsChecked = false;
                Sp_Notes.Text = "";
                Sp_IsActiveRow.IsChecked = true;
                this.ClearDataFromUI_CustomCode();
                //  Reset all TextBox TextChanged Events now.
                DataControlDataChangedEvents_SetAll();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearDataFromUI", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_LOADING_REC = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearDataFromUI", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     A stub for the <see cref="TypeServices.IEntitiyPropertiesControl">IEntitiyPropertiesControl</see> interface.
        /// </summary>
        public void GetEntityRow(Int64? Id)
        {
            throw new NotImplementedException();
        }
        public void GetEntityRow(Int32? Id)
        {
            throw new NotImplementedException();
        }
        public void GetEntityRow(Int16? Id)
        {
            throw new NotImplementedException();
        }
        public void GetEntityRow(Byte? Id)
        {
            throw new NotImplementedException();
        }

        public void GetEntityRow(Guid? Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", new ValuePair[] { new ValuePair("Id", Id) }, IfxTraceCategory.Enter);
                if (Id == null)
                {
                    NewEntityRow();
                }
                else
                {
                    if (objB == null)
                    {
                        LoadControl();
                    }
                    objB.GetEntityRow((Guid)Id);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", IfxTraceCategory.Leave);
            }
        }


        void OnEntityRowReceived(object sender, EntityRowReceivedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnEntityRowReceived", IfxTraceCategory.Enter);
                SetState();
                SetControlsDefaultValidState();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnEntityRowReceived", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnEntityRowReceived", IfxTraceCategory.Leave);
            }
        }


        public void NewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Enter);
                objB.NewEntityRow();
                SetState();
                SetControlsDefaultValidState();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Leave);
            }
        }

        wnConcurrencyManager wn;

        /// <summary>
        ///     Calls the <see cref="EntityBll.WcStoredProc_Bll.Save">Save</see> method on <see cref="EntityBll.WcStoredProc_Bll">WcStoredProc_Bll</see>.
        /// </summary>
        public int Save()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                objB.Save(this, ParentEditObjectType.EntitiyPropertiesControl, UseConcurrencyCheck.UseDefaultSetting);
                //                DataServiceInsertUpdateResponseClientSide result = objB.Save(UseConcurrencyCheck.UseDefaultSetting);
                //                if (result.Result == DataOperationResult.ConcurrencyFailure)
                //                {
                //                    wn = new wnConcurrencyManager(new WcStoredProcConcurrencyList(objB.Wire), objB);
                //                    wn.Show();
                //                    return 1;
                //                }
                //                else if (result.Result == DataOperationResult.Success)
                //                {
                //                    // do nothing.
                //
                //                    if (result.ReturnCurrentRowOnInsertUpdate == true)
                //                    {
                //                        SetState();
                //                    }
                //
                return 1;
                //                }
                //                else
                //                {
                //                    string msg = "An error occured:  " + result.Result.ToString() + Environment.NewLine + "If you continue to get this error, please contact support.";  // result.Exception.Message;
                //                    MessageBox.Show(msg, "Save Operation Error", MessageBoxButton.OK);
                //                    return -1;
                //                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
                return -1;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }


        void objB_AsyncSaveWithResponseComplete(object sender, AsyncSaveWithResponseCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "objB_AsyncSaveWithResponseComplete", IfxTraceCategory.Enter);

                if (e.Response.Result == DataOperationResult.ConcurrencyFailure)
                {
                    wn = new wnConcurrencyManager(new WcStoredProcConcurrencyList(objB.Wire), objB);
                    wn.Show();
                    //return 1;
                }
                else if (e.Response.Result == DataOperationResult.Success && e.Response.ReturnCurrentRowOnInsertUpdate == true)
                {
                    SetState();
                }
                else if (e.Response.Result == DataOperationResult.Success)
                {
                    // do nothing
                }
                else
                {
                    string msg = "An error occured:  " + e.Response.Result.ToString() + Environment.NewLine + "If you continue to get this error, please contact support.";  // result.Exception.Message;
                    MessageBox.Show(msg, "Save Operation Error", MessageBoxButton.OK);
                    //return -1;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "objB_AsyncSaveWithResponseComplete", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
                //return -1;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "objB_AsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Calls the <see cref="EntityBll.WcStoredProc_Bll.UnDo">UnDo</see> method on <see cref="EntityBll.WcStoredProc_Bll">WcStoredProc_Bll</see> and sets this control back to the previous
        ///     Non-Dirty <see cref="TypeServices.EntityState">state</see>.
        /// </summary>
        public void UnDo()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Enter);
                objB.UnDo();
                SetState();
                SetControlsDefaultValidState();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Not being used. A stub for future use.</summary>
        void CloseMe()
        {
            ////this.Close();
        }

        #endregion Data Related


        #region State Related

        /// <summary>
        ///     Called from <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> as it bubble up
        ///     from the business object. It Configures UI elements according to the current
        ///     <see cref="TypeServices.EntityState">state</see> of the entity. For example: if the
        ///     state is dirty and not valid, the Save button should be disabled and the UnDo
        ///     button should be enabled. Typically when the state is dirty, most areas of the UI
        ///     such as the navigation control (<see cref="ucWcStoredProcList">ucWcStoredProcList</see>) and
        ///     child entity controls are disabled except for this screen (ucWcStoredProcProps). This
        ///     prevents the user from navigating away from data entry area until finishing the job
        ///     – Saving or UnDoing the transaction helps prevent confusion and helps assure data
        ///     integrity.
        /// </summary>
        void ConfigureToCurrentEntityState(EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState", IfxTraceCategory.Enter);
            //    switch (state)
            //    {
            //        case EntityStateSwitch.NewInvalidNotDirty:
            //            btnNew.IsEnabled = false;
            //            btnSave.IsEnabled = false;
            //            btnUnDo.IsEnabled = false;
            //            btnDelete.IsEnabled = false;
            //            break;
            //        case EntityStateSwitch.NewValidNotDirty:
            //            //case DataState.New_NotDirty:
            //            btnNew.IsEnabled = false;
            //            btnSave.IsEnabled = false;
            //            btnUnDo.IsEnabled = false;
            //            btnDelete.IsEnabled = false;
            //            break;
            //        case EntityStateSwitch.NewValidDirty:
            //            //case DataState.New_Dirty_Valid:
            //            btnNew.IsEnabled = false;
            //            btnSave.IsEnabled = true;
            //            btnUnDo.IsEnabled = true;
            //            btnDelete.IsEnabled = true;
            //            break;
            //        case EntityStateSwitch.NewInvalidDirty:
            //            //case DataState.New_Dirty_NotValid:
            //            btnNew.IsEnabled = false;
            //            btnSave.IsEnabled = false;
            //            btnUnDo.IsEnabled = true;
            //            btnDelete.IsEnabled = true;
            //            break;
            //        case EntityStateSwitch.ExistingInvalidDirty:
            //            //case DataState.Existing_Dirty_NotValid:
            //            btnNew.IsEnabled = false;
            //            btnSave.IsEnabled = false;
            //            btnUnDo.IsEnabled = true;
            //            btnDelete.IsEnabled = true;
            //            break;
            //        case EntityStateSwitch.ExistingValidDirty:
            //            //case DataState.Existing_Dirty_Valid:
            //            btnNew.IsEnabled = false;
            //            btnSave.IsEnabled = true;
            //            btnUnDo.IsEnabled = true;
            //            btnDelete.IsEnabled = true;
            //            break;
            //        case EntityStateSwitch.ExistingValidNotDirty:
            //            //case DataState.Existing_Saved:
            //            btnNew.IsEnabled = true;
            //            btnSave.IsEnabled = false;
            //            btnUnDo.IsEnabled = false;
            //            btnDelete.IsEnabled = false;
            //            break;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState", ex);
            // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState", IfxTraceCategory.Leave);
            //}
        }

        void SetControlValidAppearance(IvControlsValidation ctl, bool isValid, bool isDirty)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlValidAppearance", IfxTraceCategory.Enter);
            if (isValid)
            {
                ctl.ValidStateAppearance = ValidationState.Valid;
            }
            else
            {
                if (isDirty)
                {
                    ctl.ValidStateAppearance = ValidationState.NotValidIsDirty;
                }
                else
                {
                    ctl.ValidStateAppearance = ValidationState.NotValidNotDirty;
                }
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlValidAppearance", IfxTraceCategory.Leave);
        }


        void SetControlDefaultValidAppearance(IvControlsValidation ctl, bool isValid)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlDefaultValidAppearance", IfxTraceCategory.Enter);
            if (isValid == true)
            {
                ctl.ValidStateAppearance = ValidationState.Valid;
            }
            else
            {
                ctl.ValidStateAppearance = ValidationState.NotValidNotDirty;
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlDefaultValidAppearance", IfxTraceCategory.Leave);
        }


        /// <summary>
        /// Sets the restricted length value and Valid or Not Valid appearance of the Text
        /// Length label (lblTextLength).
        /// </summary>
        void SetRestrictedStringLengthText()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetRestrictedStringLengthText", IfxTraceCategory.Enter);
                _lblAdrnMngr.LblTextLength.Content = objB.ActiveRestrictedStringPropertyLength;
                if (objB.ActiveRestrictedStringPropertyLength < 0)
                {
                    _lblAdrnMngr.SetTextLengthNotValidAppearance();
                }
                else
                {
                    _lblAdrnMngr.SetTextLengthValidAppearance();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetRestrictedStringLengthText", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetRestrictedStringLengthText", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Obsolete. This used to set the Broken Rule text in the status bar at the bottom of
        ///     the screen, but now a different mechanism using tooltips is used to notify about
        ///     broken rules.
        ///     <para><br/>
        ///     And for the Broken Rule Tooltips, see:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="tt_Loaded">tt_Loaded</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="LoadToolTipControl">LoadToolTipControl(string fieldName)</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="GetListOfValidationRuleMessagesFromBusinessObject">GetListOfValidationRuleMessagesFromBusinessObject(string
        ///             fieldName)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void SetBrokenRuleText(string rule)
        {
            // Set broken rule here
        }


        #endregion  State Related


        #region General

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a TextBox
        ///     and wires up all of the standard TextBox events to their respective
        ///     handlers:</para>
        /// 	<para class="xmldocbulletlist"></para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>
        ///             TextChanged event to the handler <see cref="OnTextChanged">OnTextChanged</see>
        /// 		</item>
        /// 		<item>
        ///             LostFocus event to the handler <see cref="OnTexBoxLostFocus">OnTexBoxLostFocus</see>
        /// 		</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>This way all event handling is centralized, code is reduced and maintenance
        ///     is improved. All of these methods are auto-generated so even if there are no
        ///     TexBoxes, this code will still be here. Therefore, if TextBoxes are added in the
        ///     future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetTextBoxEvents(TextBox ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new TextChangedEventHandler(OnTextChanged));
            ctl.LostFocus += new RoutedEventHandler(OnTexBoxLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a CheckBox
        ///     and wires up all of the standard CheckBox events to their respective
        ///     handlers:</para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>
        ///             Click event to the handler <see cref="OnCheckBoxClick">OnCheckBoxClick</see>
        /// 		</item>
        /// 		<item>LostFocus event to the handler
        ///         %OnCheckBoxLostFocus:OnCheckBoxLostFocus</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>This way all event handling is centralized, code is reduced and maintenance
        ///     is improved. All of these methods are auto-generated so even if there are no
        ///     CheckBoxs, this code will still be here. Therefore, if CheckBoxs are added in the
        ///     future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetCheckBoxEvents(vCheckBox ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new RoutedEventHandler(OnCheckBoxClick));
            ctl.LostFocus += new RoutedEventHandler(OnCheckBoxLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a RadioButton
        ///     and wires up all of the standard RadioButton events to their respective
        ///     handlers:</para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>
        ///             Click event to the handler <see cref="OnRadioButtonClick">OnRadioButtonClick</see>
        /// 		</item>
        /// 		<item>LostFocus event to the handler
        ///         %OnRadioButtonLostFocus:OnRadioButtonLostFocus</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>This way all event handling is centralized, code is reduced and maintenance
        ///     is improved. All of these methods are auto-generated so even if there are no
        ///     RadioButtons, this code will still be here. Therefore, if RadioButtons are added in the
        ///     future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetRadioButtonEvents(RadioButton ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new RoutedEventHandler(OnRadioButtonClick));
            ctl.LostFocus += new RoutedEventHandler(OnRadioButtonLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a vRadioButtonGroup
        ///     and wires up all of the standard vRadioButtonGroup events to their respective
        ///     handlers:</para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>
        ///             Click event to the handler <see cref="OnvRadioButtonGroupClick">OnvRadioButtonGroupClick</see>
        /// 		</item>
        /// 		<item>LostFocus event to the handler
        ///         %OnvRadioButtonGroupLostFocus:OnvRadioButtonGroupLostFocus</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>This way all event handling is centralized, code is reduced and maintenance
        ///     is improved. All of these methods are auto-generated so even if there are no
        ///     vRadioButtonGroups, this code will still be here. Therefore, if vRadioButtonGroups are added in the
        ///     future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetvRadioButtonGroupEvents(vRadioButtonGroup ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new EventHandler(OnvRadioButtonGroupItemChecked));
        }

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a
        ///     XamComboEditor and wires up all of the standard DatePicker events to their
        ///     respective handlers:</para>
        /// 	<list type="bullet">
        /// 		<item>
        ///             SelectedItemChanged event to the handler <see cref="OnDatePicker_TextChanged">OnDatePicker_TextChanged</see>
        /// 		</item>
        /// 		<item>
        ///             LostFocus event to the handler <see cref="OnDatePickerLostFocus">OnDatePickerLostFocus</see>
        /// 		</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>
        ///     This way all event handling is centralized, code is reduced and maintenance is
        ///     improved. All of these methods are auto-generated so even if there are no
        ///     DatePicker, this code will still be here. Therefore, if XamComboEditors are
        ///     added in the future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetDatePickerEvents(DatePicker ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new TextChangedEventHandler(OnDatePicker_TextChanged));
            ctl.LostFocus += new RoutedEventHandler(OnDatePickerLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }


        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a
        ///     XamComboEditor and wires up all of the standard TimePicker events to their
        ///     respective handlers:</para>
        /// 	<list type="bullet">
        /// 		<item>
        ///             SelectedItemChanged event to the handler <see cref="OnTimePicker_ValueChanged">OnTimePicker_ValueChanged</see>
        /// 		</item>
        /// 		<item>
        ///             LostFocus event to the handler <see cref="OnTimePickerLostFocus">OnTimePickerLostFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>
        ///     This way all event handling is centralized, code is reduced and maintenance is
        ///     improved. All of these methods are auto-generated so even if there are no
        ///     TimePicker, this code will still be here. Therefore, if xxx are
        ///     added in the future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetTimePickerEvents(TimePicker ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new RoutedPropertyChangedEventHandler<DateTime?>(OnTimePicker_ValueChanged));
            ctl.LostFocus += new RoutedEventHandler(OnTimePickerLostFocus);
            //ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a
        ///     XamComboEditor and wires up all of the standard XamComboEditor events to their
        ///     respective handlers:</para>
        /// 	<list type="bullet">
        /// 		<item>
        ///             SelectedItemChanged event to the handler <see cref="OnXamComboEditorSelectionChanged">OnXamComboEditorSelectionChanged</see>
        /// 		</item>
        /// 		<item>
        ///             LostFocus event to the handler <see cref="OnXamComboEditorLostFocus">OnXamComboEditorLostFocus</see>
        /// 		</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>
        ///     This way all event handling is centralized, code is reduced and maintenance is
        ///     improved. All of these methods are auto-generated so even if there are no
        ///     XamComboEditors, this code will still be here. Therefore, if XamComboEditors are
        ///     added in the future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetXamComboEditorEvents(XamComboEditor ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new EventHandler(OnXamComboEditorSelectionChanged));
            ctl.LostFocus += new RoutedEventHandler(OnXamComboEditorLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        void SetVXamComboEditorEvents(vXamComboEditor ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
            ctl.LostFocus += new RoutedEventHandler(OnXamComboEditorLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a
        ///     XamMultiColumnComboEditor and wires up all of the standard XamMultiColumnComboEditor events to their
        ///     respective handlers:</para>
        /// 	<list type="bullet">
        /// 		<item>
        ///             SelectedItemChanged event to the handler <see cref="OnXamMultiColumnComboEditorSelectionChanged">OnXamMultiColumnComboEditorSelectionChanged</see>
        /// 		</item>
        /// 		<item>
        ///             LostFocus event to the handler <see cref="OnXamMultiColumnComboEditorLostFocus">OnXamMultiColumnComboEditorLostFocus</see>
        /// 		</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>
        ///     This way all event handling is centralized, code is reduced and maintenance is
        ///     improved. All of these methods are auto-generated so even if there are no
        ///     XamMultiColumnComboEditors, this code will still be here. Therefore, if XamMultiColumnComboEditors are
        ///     added in the future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetXamMultiColumnComboEditorEvents(XamMultiColumnComboEditor ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new EventHandler(OnXamMultiColumnComboEditorSelectionChanged));
            ctl.LostFocus += new RoutedEventHandler(OnXamMultiColumnComboEditorLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        void SetVXamMultiColumnComboEditorEvents(vXamMultiColumnComboEditor ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new vXamMultiColumnComboEditorSelectionChangedEventHandler(OnVXamMultiColumnComboEditorSelectionChanged));
            ctl.LostFocus += new RoutedEventHandler(OnXamMultiColumnComboEditorLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        void SetTXamColorPickerEvents(XamColorPicker ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new EventHandler<SelectedColorChangedEventArgs>(XamColorPicker_SelectedColorChanged));
            //ctl.LostFocus += new RoutedEventHandler(OnTexBoxLostFocus);
            //ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        void DataControlDataChangedEvents_SetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DataControlDataChangedEvents_SetAll", IfxTraceCategory.Enter);

                //TextBox
                _dataControlEventManager.AddHandler(Sp_Name, new TextChangedEventHandler(OnTextChanged));
                _dataControlEventManager.AddHandler(Sp_SprocGroups, new TextChangedEventHandler(OnTextChanged));
                _dataControlEventManager.AddHandler(Sp_WireTypeName, new TextChangedEventHandler(OnTextChanged));
                _dataControlEventManager.AddHandler(Sp_MethodName, new TextChangedEventHandler(OnTextChanged));
                _dataControlEventManager.AddHandler(Sp_AssocEntityNames, new TextChangedEventHandler(OnTextChanged));
                _dataControlEventManager.AddHandler(Sp_Notes, new TextChangedEventHandler(OnTextChanged));

                //XamComboEditor

                //vXamComboEditor
                _dataControlEventManager.AddHandler(Sp_AssocEntity_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
                _dataControlEventManager.AddHandler(Sp_SpRsTp_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
                _dataControlEventManager.AddHandler(Sp_SpRtTp_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));

                //CheckBox
                _dataControlEventManager.AddHandler(Sp_IsCodeGen, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsReadyCodeGen, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsCodeGenComplete, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsTagForCodeGen, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsTagForOther, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsBuildDataAccessMethod, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_BuildListClass, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsFetchEntity, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsStaticList, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsTypeComboItem, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsCreateWireType, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsInputParamsAsObjectArray, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsParamsAutoRefresh, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_HasNewParams, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsParamsValid, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsParamValueSetValid, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_HasNewColumns, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsColumnsValid, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsValid, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.AddHandler(Sp_IsActiveRow, new RoutedEventHandler(OnCheckBoxClick));

                DataControlDataChangedEvents_SetAll_Custom();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DataControlDataChangedEvents_SetAll", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DataControlDataChangedEvents_SetAll", IfxTraceCategory.Leave);
            }
        }

        void DataControlDataChangedEvents_RemoveAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DataControlDataChangedEvents_RemoveAll", IfxTraceCategory.Enter);

                //TextBox
                _dataControlEventManager.RemoveHandler(Sp_Name, new TextChangedEventHandler(OnTextChanged));
                _dataControlEventManager.RemoveHandler(Sp_SprocGroups, new TextChangedEventHandler(OnTextChanged));
                _dataControlEventManager.RemoveHandler(Sp_WireTypeName, new TextChangedEventHandler(OnTextChanged));
                _dataControlEventManager.RemoveHandler(Sp_MethodName, new TextChangedEventHandler(OnTextChanged));
                _dataControlEventManager.RemoveHandler(Sp_AssocEntityNames, new TextChangedEventHandler(OnTextChanged));
                _dataControlEventManager.RemoveHandler(Sp_Notes, new TextChangedEventHandler(OnTextChanged));

                //XamComboEditor

                // vXamComboEditor
                _dataControlEventManager.RemoveHandler(Sp_AssocEntity_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
                _dataControlEventManager.RemoveHandler(Sp_SpRsTp_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
                _dataControlEventManager.RemoveHandler(Sp_SpRtTp_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));

                //CheckBox
                _dataControlEventManager.RemoveHandler(Sp_IsCodeGen, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsReadyCodeGen, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsCodeGenComplete, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsTagForCodeGen, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsTagForOther, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsBuildDataAccessMethod, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_BuildListClass, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsFetchEntity, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsStaticList, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsTypeComboItem, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsCreateWireType, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsInputParamsAsObjectArray, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsParamsAutoRefresh, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_HasNewParams, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsParamsValid, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsParamValueSetValid, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_HasNewColumns, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsColumnsValid, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsValid, new RoutedEventHandler(OnCheckBoxClick));
                _dataControlEventManager.RemoveHandler(Sp_IsActiveRow, new RoutedEventHandler(OnCheckBoxClick));

                DataControlDataChangedEvents_RemoveAll_Custom();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DataControlDataChangedEvents_RemoveAll", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DataControlDataChangedEvents_RemoveAll", IfxTraceCategory.Leave);
            }
        }

        #endregion General


        #endregion General Methods


        #region General Properties, Getters and Setters

        /// <returns>List&lt;ValidationRuleMessage&gt;</returns>
        /// <summary>
        ///     Returns a list of all the BrokenRules for WcStoredProc from the <see cref="EntityBll.WcStoredProc_Bll.GetBrokenRulesForEntity">GetBrokenRuleListForEntity</see>
        ///     method. This could be used to present a full list of all BrokenRules rather than
        ///     just showing a subset in the BrokenRules ToolTip.
        /// </summary>
        /// <seealso cref="tt_Loaded">tt_Loaded Method</seealso>
        /// <seealso cref="TypeServices.BrokenRuleManager.GetBrokenRulesForEntity">GetBrokenRulesForEntity Method (TypeServices.BrokenRuleManager)</seealso>
        public List<vRuleItem> GetBrokenRuleListForEntity()
        {
            return objB.GetBrokenRuleListForEntity();
        }

        /// <summary>
        /// 	<para>
        ///         Gets or Sets the current <see cref="EntityBll.WcStoredProc_Bll">Enty_Bll</see>
        ///         supporting this control.
        ///     </para>
        /// 	<para>
        ///         Important: You avoid using this setter because - if your intent is to set a
        ///         different instance of WcStoredProc_Bll as this control’s current business object, many
        ///         other actions must be performed for this control to work properly. Use the
        ///         <see cref="SetBusinessObject">SetBusinessObject</see> method instead.
        ///     </para>
        /// 	<para>
        ///         This is a surrogate for the $CurrentBusinessObject:CurrentBusinessObject%
        ///         property for compatibility with the <see cref="IEntitiyPropertiesControl">IEntitiyPropertiesControl</see> interface making
        ///         this control more extendable.
        ///     </para>
        /// </summary>
        /// <value>WcStoredProc_Bll</value>
        public IBusinessObject IEntityControlCurrentBusinessObject
        {
            get { return objB; }
            set { SetBusinessObject((WcStoredProc_Bll)value); }
        }

        /// <summary>
        /// 	<para>
        ///         Gets or Sets the current <see cref="EntityBll.WcStoredProc_Bll">Enty_Bll</see>
        ///         supporting this control.
        ///     </para>
        /// 	<para>
        ///         Important: You avoid using this setter because - if your intent is to set a
        ///         different instance of WcStoredProc_Bll as this control’s current business object, many
        ///         other actions must be performed for this control to work properly. Use the
        ///         <see cref="SetBusinessObject">SetBusinessObject</see> method instead.
        ///     </para>
        /// 	<para>
        /// 		<see cref="IEntityControlCurrentBusinessObject">IEntityControlCurrentBusinessObject</see>
        ///         is a surrogate for this property for compatibility with the <see cref="IEntitiyPropertiesControl">IEntitiyPropertiesControl</see> interface making
        ///         this control more extendable.
        ///     </para>
        /// </summary>
        /// <value>WcStoredProc_Bll</value>
        public WcStoredProc_Bll CurrentBusinessObject
        {
            get { return objB; }
            set { SetBusinessObject(value); }
        }

        /// <summary>
        ///     Sets the value of IsActivePropertiesControl. Refer to the <see cref=" IsActivePropertiesControl">IsActivePropertiesControl</see> documentation for its
        ///     usage.
        /// </summary>
        /// <seealso cref="IsActivePropertiesControl">IsActivePropertiesControl Property</seealso>
        public void SetIsActivePropertiesControl(bool value)
        {
            _isActivePropertiesControl = value;
        }

        /// <summary>
        ///     Gets the value of IsActivePropertiesControl. Refer to the <see cref=" IsActivePropertiesControl">IsActivePropertiesControl</see> documentation for its
        ///     usage.
        /// </summary>
        /// <seealso cref="IsActivePropertiesControl">IsActivePropertiesControl Property</seealso>
        public bool GetIsActivePropertiesControl()
        {
            return _isActivePropertiesControl;
        }

        /// <summary>
        /// A flag telling us if this is the active properties control. A complex screen can
        /// have many entity controls each with its own properties control (ucProps) and additional
        /// nested entity controls. Only one entity control can be active at a time. When ucProps
        /// becomes the active properties control, it raises an event that tells its entity control
        /// (or parent control) that it’s active. At this point the entity control becomes the
        /// active entity control. When a user clicks or tabs into a, EntityList, EntityProps, or
        /// any other child control of an entity control, this flag is set to true. As code bubbles
        /// up or tunnels down through the many layers of WPF elements, its often important to know
        /// when its entering the active entity control.
        /// </summary>
        /// <seealso cref="RaiseCurrentEntityStateChanged">RaiseCurrentEntityStateChanged Method</seealso>
        public bool IsActivePropertiesControl
        {
            get { return _isActivePropertiesControl; }
            set
            {
                _isActivePropertiesControl = value;
            }
        }

        /// <seealso cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged Method</seealso>
        /// <seealso cref="OnControlValidStateChanged">OnControlValidStateChanged Method</seealso>
        /// <seealso cref="TypeServices.EntityState">EntityState Class</seealso>
        /// <seealso cref="TypeServices.EntityStateSwitch">EntityStateSwitch Enumeration</seealso>
        /// <summary>
        ///     Gets the current <see cref="TypeServices.EntityStateSwitch">EntityStateSwitch</see>
        ///     from the <see cref="EntityBll.WcStoredProc_Bll.StateSwitch">WcStoredProc_Bll.StateSwitch</see>
        ///     property.
        /// </summary>
        public EntityStateSwitch GetEntityStateSwitch()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityStateSwitch", IfxTraceCategory.Enter);
                return objB.StateSwitch;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityStateSwitch", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityStateSwitch", IfxTraceCategory.Leave);
            }
        }

        /// <seealso cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged Method</seealso>
        /// <seealso cref="OnControlValidStateChanged">OnControlValidStateChanged Method</seealso>
        /// <seealso cref="TypeServices.EntityState">EntityState Class</seealso>
        /// <seealso cref="TypeServices.EntityStateSwitch">EntityStateSwitch Enumeration</seealso>
        /// <summary>
        ///     Gets the current <see cref="TypeServices.EntityState">EntityState</see> from the
        ///     <see cref="EntityBll.WcStoredProc_Bll.State">WcStoredProc_Bll.State</see> property.
        /// </summary>
        public EntityState GetEntityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityState", IfxTraceCategory.Enter);
                return objB.State;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityState", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityState", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Returns true if this control has a business object (bound to an instance of
        ///     <see cref="EntityBll.WcStoredProc_Bll">WcStoredProc_Bll</see> – note, this control doesn’t
        ///     actually bind to WcStoredProc_Bll, but rather emulates binding through custom code for
        ///     greater control of its behavior.)
        /// </summary>
        public bool HasBusinessObject()
        {
            return _hasBusinessObject;
        }



        //        public void SetParentContainerType(bool isGrid)
        //        {
        //            _parentIsGrid = isGrid;
        //        }

        #endregion General Properties, Getters and Setters


        #region  Edit Combo Dropdown List Code

        #region Support Methods

        /// <summary>
        /// 	<para>
        ///         Called from the SomeComboBox_EditDropDownList_AcceptCancelChanges
        ///         event. Normaly this raised event will cause the control hosting a navigation to
        ///         update a list used to popate a list control such as a combox being used as an
        ///         editor control in a grid cell.
        ///     </para>
        /// </summary>
        /// <param name="columnName">Name of the property window/control's data field's list control ( i.e. ComboBox) who's list was just updated.  This name will be used to identify the column in a grid to update.</param>
        public void RaiseListColumnListMustUpdate(string columnName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseListColumnListMustUpdate", IfxTraceCategory.Enter);
                ListColumnListMustUpdateEventHandler handler = OnListColumnListMustUpdate;
                if (handler != null)
                {
                    handler(this, new ListColumnListMustUpdateArgs(columnName));
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseListColumnListMustUpdate", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseListColumnListMustUpdate", IfxTraceCategory.Leave);
            }
        }

        #endregion Support Methods

        #endregion  Edit Combo Dropdown List Code



        #endregion General Methods and Properties


        #region ToolTip Stuff

        #region Validation Tooltips

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a Control
        ///     and wires up all of the Tooltip events used by the BrokenRules Tooltip. This
        ///     ToolTip will show a list of one or more BrokenRules for each control. All controls
        ///     that have validation are passed into this method for wiring.</para>
        /// 	<para>This way all event handling for ToolTips is centralized, code is reduced and
        ///     maintenance is improved. These ToolTip methods are auto-generated so even if
        ///     currently there are no controls with validation, this code will still be here.
        ///     Therefore, if control validation is added in the future, all of the supporting code
        ///     will already be in place.</para>
        /// </summary>
        void AttachToolTip(Control ctl)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachToolTip", IfxTraceCategory.Enter);
                vToolTip tt = vToolTipHelper.GetTooltipInstanceForValidationRuleToolTip();
                ControlTemplate ct = (ControlTemplate)Application.Current.Resources["ToolTipTemplate"];
                tt.Template = ct;
                tt.Content = new TooltipBrokenRuleContent();
                // These lines before and after need to be set in this order or things may not work as expected.
                tt.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
                tt.PlacementTarget = ctl;

                tt.Loaded += new RoutedEventHandler(tt_Loaded);
                vToolTipService.SetToolTip(ctl, tt);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachToolTip", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachToolTip", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     The ToolTip Loaded event is wired to this method (in the <see cref="AttachToolTip">AttachToolTip</see> method) for every data control that has
        ///     validation. When the mouse passes over the control; this method is called,
        ///     configures the tooltip dynamically with the control’s BrokenRules and displays the
        ///     tooltip.
        /// </summary>
        /// <seealso cref="LoadToolTipControl">LoadToolTipControl Method</seealso>
        /// <seealso cref="GetListOfValidationRuleMessagesFromBusinessObject">GetListOfValidationRuleMessagesFromBusinessObject Method</seealso>
        void tt_Loaded(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "tt_Loaded", IfxTraceCategory.Enter);
                Control ctl = (Control)sender;
                if (objB.IsPropertyValid(((Control)((ToolTip)sender).PlacementTarget).Name))
                {
                    ctl.Visibility = Visibility.Collapsed;
                    return;
                }
                else
                {
                    ctl.Visibility = Visibility.Visible;
                }
                vToolTip tt = (vToolTip)sender;
                TooltipBrokenRuleContent uc = tt.Content as TooltipBrokenRuleContent;
                if (uc != null)
                {
                    List<vRuleItem> rules = objB.GetBrokenRulesForProperty(((Control)tt.PlacementTarget).Name);
                    uc.ItemsSource = rules;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "tt_Loaded", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "tt_Loaded", IfxTraceCategory.Leave);
            }
        }

        #endregion Validation Tooltips

        #region Business Rule Tooltips


        void CreateBusinessRuleTooltips()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateBusinessRuleTooltips", IfxTraceCategory.Enter);

                // lblSp_Name
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_Name_1}, StringsWcStoredProcProps.Sp_Name_Vbs), lblSp_Name);

                // lblSp_IsCodeGen
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsCodeGen_1}, StringsWcStoredProcProps.Sp_IsCodeGen_Vbs), lblSp_IsCodeGen);

                // lblSp_IsReadyCodeGen
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsReadyCodeGen_1}, StringsWcStoredProcProps.Sp_IsReadyCodeGen_Vbs), lblSp_IsReadyCodeGen);

                // lblSp_IsCodeGenComplete
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsCodeGenComplete_1,
                    StringsWcStoredProcPropsTooltips.Sp_IsCodeGenComplete_2}, StringsWcStoredProcProps.Sp_IsCodeGenComplete_Vbs), lblSp_IsCodeGenComplete);

                // lblSp_IsTagForCodeGen
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsTagForCodeGen_1}, StringsWcStoredProcProps.Sp_IsTagForCodeGen_Vbs), lblSp_IsTagForCodeGen);

                // lblSp_IsTagForOther
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsTagForOther_1}, StringsWcStoredProcProps.Sp_IsTagForOther_Vbs), lblSp_IsTagForOther);

                // lblSp_IsBuildDataAccessMethod
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsBuildDataAccessMethod_1,
                    StringsWcStoredProcPropsTooltips.Sp_IsBuildDataAccessMethod_2}, StringsWcStoredProcProps.Sp_IsBuildDataAccessMethod_Vbs), lblSp_IsBuildDataAccessMethod);

                // lblSp_BuildListClass
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_BuildListClass_1}, StringsWcStoredProcProps.Sp_BuildListClass_Vbs), lblSp_BuildListClass);

                // lblSp_IsFetchEntity
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsFetchEntity_1,
                    StringsWcStoredProcPropsTooltips.Sp_IsFetchEntity_2}, StringsWcStoredProcProps.Sp_IsFetchEntity_Vbs), lblSp_IsFetchEntity);

                // lblSp_IsStaticList
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsStaticList_1}, StringsWcStoredProcProps.Sp_IsStaticList_Vbs), lblSp_IsStaticList);

                // lblSp_IsTypeComboItem
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsTypeComboItem_1,
                    StringsWcStoredProcPropsTooltips.Sp_IsTypeComboItem_2}, StringsWcStoredProcProps.Sp_IsTypeComboItem_Vbs), lblSp_IsTypeComboItem);

                // lblSp_WireTypeName
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_WireTypeName_1,
                    StringsWcStoredProcPropsTooltips.Sp_WireTypeName_2}, StringsWcStoredProcProps.Sp_WireTypeName_Vbs), lblSp_WireTypeName);

                // lblSp_MethodName
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_MethodName_1}, StringsWcStoredProcProps.Sp_MethodName_Vbs), lblSp_MethodName);

                // lblSp_AssocEntity_Id
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_AssocEntity_Id_1,
                    StringsWcStoredProcPropsTooltips.Sp_AssocEntity_Id_2}, StringsWcStoredProcProps.Sp_AssocEntity_Id_Vbs), lblSp_AssocEntity_Id);

                // lblSp_AssocEntityNames
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_AssocEntityNames_1,
                    StringsWcStoredProcPropsTooltips.Sp_AssocEntityNames_2}, StringsWcStoredProcProps.Sp_AssocEntityNames_Vbs), lblSp_AssocEntityNames);

                // lblSp_SpRsTp_Id
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_SpRsTp_Id_1,
                    StringsWcStoredProcPropsTooltips.Sp_SpRsTp_Id_2}, StringsWcStoredProcProps.Sp_SpRsTp_Id_Vbs), lblSp_SpRsTp_Id);

                // lblSp_SpRtTp_Id
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_SpRtTp_Id_1,
                    StringsWcStoredProcPropsTooltips.Sp_SpRtTp_Id_2}, StringsWcStoredProcProps.Sp_SpRtTp_Id_Vbs), lblSp_SpRtTp_Id);

                // lblSp_IsInputParamsAsObjectArray
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsInputParamsAsObjectArray_1,
                    StringsWcStoredProcPropsTooltips.Sp_IsInputParamsAsObjectArray_2}, StringsWcStoredProcProps.Sp_IsInputParamsAsObjectArray_Vbs), lblSp_IsInputParamsAsObjectArray);

                // lblSp_IsParamsAutoRefresh
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsParamsAutoRefresh_1,
                    StringsWcStoredProcPropsTooltips.Sp_IsParamsAutoRefresh_2}, StringsWcStoredProcProps.Sp_IsParamsAutoRefresh_Vbs), lblSp_IsParamsAutoRefresh);

                // lblSp_HasNewParams
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_HasNewParams_1,
                    StringsWcStoredProcPropsTooltips.Sp_HasNewParams_2}, StringsWcStoredProcProps.Sp_HasNewParams_Vbs), lblSp_HasNewParams);

                // lblSp_IsParamsValid
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsParamsValid_1}, StringsWcStoredProcProps.Sp_IsParamsValid_Vbs), lblSp_IsParamsValid);

                // lblSp_IsParamValueSetValid
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsParamValueSetValid_1,
                    StringsWcStoredProcPropsTooltips.Sp_IsParamValueSetValid_2}, StringsWcStoredProcProps.Sp_IsParamValueSetValid_Vbs), lblSp_IsParamValueSetValid);

                // lblSp_HasNewColumns
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_HasNewColumns_1,
                    StringsWcStoredProcPropsTooltips.Sp_HasNewColumns_2}, StringsWcStoredProcProps.Sp_HasNewColumns_Vbs), lblSp_HasNewColumns);

                // lblSp_IsColumnsValid
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsColumnsValid_1,
                    StringsWcStoredProcPropsTooltips.Sp_IsColumnsValid_2}, StringsWcStoredProcProps.Sp_IsColumnsValid_Vbs), lblSp_IsColumnsValid);

                // lblSp_IsValid
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsValid_1,
                    StringsWcStoredProcPropsTooltips.Sp_IsValid_2}, StringsWcStoredProcProps.Sp_IsValid_Vbs), lblSp_IsValid);

                // lblSp_IsActiveRow
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcStoredProcPropsTooltips.Sp_IsActiveRow_1,
                    StringsWcStoredProcPropsTooltips.Sp_IsActiveRow_2}, StringsWcStoredProcProps.Sp_IsActiveRow_Vbs), lblSp_IsActiveRow);
                DefineCustomToolTips();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateBusinessRuleTooltips", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateBusinessRuleTooltips", IfxTraceCategory.Leave);
            }
        }


        #endregion Business Rule Tooltips

        #endregion ToolTip Stuff


        #region Format Fields and ReadOnly Assignments

        void FormatFields()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FormatFields", IfxTraceCategory.Enter);
                FormatFields_Custom();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FormatFields", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FormatFields", IfxTraceCategory.Leave);
            }
        }



        void ReadOnlyAssignments()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReadOnlyAssignments", IfxTraceCategory.Enter);
                Sp_AssocEntityNames.IsReadOnly = true;
                ReadOnlyAssignments_Custom();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReadOnlyAssignments", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReadOnlyAssignments", IfxTraceCategory.Leave);
            }
        }



        #endregion Format Fields and ReadOnly Assignments


        #region Assign Text Length Labels


        void AssignTextLengthLabels()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Enter);
                Sp_Name.SetTextLengthControlForGrid(WcStoredProc_Bll.STRINGSIZE_Sp_Name);
                Sp_SprocGroups.SetTextLengthControlForGrid(WcStoredProc_Bll.STRINGSIZE_Sp_SprocGroups);
                Sp_WireTypeName.SetTextLengthControlForGrid(WcStoredProc_Bll.STRINGSIZE_Sp_WireTypeName);
                Sp_MethodName.SetTextLengthControlForGrid(WcStoredProc_Bll.STRINGSIZE_Sp_MethodName);
                Sp_Notes.SetTextLengthControlForGrid(WcStoredProc_Bll.STRINGSIZE_Sp_Notes);
                AssignTextLengthLabels_Custom();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Leave);
            }
        }


        #endregion Assign Text Length Labels



        #region Tile Operations

        XamTileManager _xtv = null;
        public void ConfigureXamTileView()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureXamTileView", IfxTraceCategory.Enter);
                //  For design purposes, we have each child grid layed out in a rows of a parent grid.
                //  At runtime - remove the child grids from the LayoutRoot grid, remove the LayoutRoot grid rows
                //    then create XamTileView objects and add the child grids to them.

                _xtv = new XamTileManager();
                ConfigureXamTileView_CustomSettings();
                LayoutRoot.Children.Remove(gdBase);
                LayoutRoot.Children.Remove(gdInputOutput);
                LayoutRoot.Children.Remove(gdWorkflow);
                LayoutRoot.Children.Remove(dbNotesStatus);
                XamTileView_AddItem(gdBase, "Base Info");
                XamTileView_AddItem(gdInputOutput, "Input - Output");
                XamTileView_AddItem(gdWorkflow, "Workflow");
                XamTileView_AddItem(dbNotesStatus, "Notes Status");
                LayoutRoot.RowDefinitions.Remove(LayoutRoot.RowDefinitions[4]);
                LayoutRoot.RowDefinitions.Remove(LayoutRoot.RowDefinitions[3]);
                LayoutRoot.RowDefinitions.Remove(LayoutRoot.RowDefinitions[2]);
                LayoutRoot.RowDefinitions.Remove(LayoutRoot.RowDefinitions[1]);

                TileConstraints tc = new TileConstraints();
                _xtv.NormalModeSettings.TileConstraints = tc;
                LayoutRoot.Children.Add(_xtv);
                Grid.SetRow(_xtv, 1);
                ((XamTile)_xtv.Items[0]).IsMaximized = true;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureXamTileView", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_FORM_IS_LOADING = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureXamTileView", IfxTraceCategory.Leave);
            }
        }

        void XamTileView_AddItem(Grid gd, string header)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamTileView_AddItem", IfxTraceCategory.Enter);

                XamTile xt = new XamTile();
                xt.Header = header;
                Viewbox vb = new Viewbox();
                vb.Child = gd;
                vb.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                xt.Content = vb;
                _xtv.Items.Add(xt);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamTileView_AddItem", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_FORM_IS_LOADING = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamTileView_AddItem", IfxTraceCategory.Leave);
            }
        }

        #endregion Tile Operations



        public bool SecuitySettingIsReadOnly
        {
            get { return _secuitySettingIsReadOnly; }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SecuitySettingIsReadOnly - Setter", IfxTraceCategory.Enter);
                    _secuitySettingIsReadOnly = value;
                    if (_secuitySettingIsReadOnly == true)
                    {
                        gdBase_ReadOnly1.Visibility = System.Windows.Visibility.Visible;
                        gdInputOutput_ReadOnly1.Visibility = System.Windows.Visibility.Visible;
                        gdWorkflow_ReadOnly1.Visibility = System.Windows.Visibility.Visible;
                        dbNotesStatus_ReadOnly1.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        gdBase_ReadOnly1.Visibility = System.Windows.Visibility.Collapsed;
                        gdInputOutput_ReadOnly1.Visibility = System.Windows.Visibility.Collapsed;
                        gdWorkflow_ReadOnly1.Visibility = System.Windows.Visibility.Collapsed;
                        dbNotesStatus_ReadOnly1.Visibility = System.Windows.Visibility.Collapsed;
                    }
                    ReadOnlySettings_Custom();
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SecuitySettingIsReadOnly - Setter", ex);
                    throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SecuitySettingIsReadOnly - Setter", IfxTraceCategory.Leave);
                }

            }
        }





        //        #region CRUD Buttons
        //
        //        private void btnNew_Click(object sender, RoutedEventArgs e)
        //        {
        //            NewEntityRow();
        //        }
        //
        //        private void btnUnDo_Click(object sender, RoutedEventArgs e)
        //        {
        //            UnDo();
        //        }
        //
        //        private void btnSave_Click(object sender, RoutedEventArgs e)
        //        {
        //            Save();
        //        }
        //
        //        private void btnDelete_Click(object sender, RoutedEventArgs e)
        //        {
        //            // No code for this yet.
        //            //MessageBox.Show("btnDelete_Click");
        //        }
        //
        //        #endregion CRUD Buttons
    }
}

