using System;
using System.Windows;
using System.Windows.Controls;
using EntityBll.SL;
using System.ComponentModel;
using Infragistics.Controls.Grids;
using TypeServices;
using Ifx.SL;
using Infragistics.Controls.Menus;
using System.Collections.ObjectModel;
using Infragistics.Controls.Editors;
using System.Collections.Generic;
using EntityWireTypeSL;
using System.Linq;
using vControls;
using Velocity.SL;
using vUICommon;
using vUICommon.Controls;
using vXamGridExcel;
using vTooltipProvider;
using Infragistics.Controls.Grids.Primitives;
using ApplicationTypeServices;

using vGridColumnGroupManager;

namespace UIControls
{

    /// <summary>
    /// 	<para><strong>About this Entity:</strong></para>
    /// 	<para>***General description of the entity from WC***</para>
    /// 	<para></para><br/>
    /// 	<para><strong>About this Control:</strong></para>
    /// 	<para>
    ///         Every entity can have a specialized list control automatically generated using
    ///         the framework’s <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_ucEntityList.html">
    ///         code-gen</a> tool. The list control wraps a grid control and adds extra
    ///         functionality that’s typically used such as communicating with a parent control
    ///         and synchronizing with a properties control (ucProps). ucEntyList design allows
    ///         it to be configured for Read/Write, or Read Only. It’s typically used as the
    ///         navigation control in an <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/The_Entity_Manager.html">
    ///         Entity Manager</a> control which controls the active data in a Properties
    ///         Control (<a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/ucEntityProps.html">ucProps</a>),
    ///         but can also be used as a stand along list. The grid in this control is
    ///         normally bound to the <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Entity_List_Type.html">
    ///         entity’s custom list</a> class (<see cref="EntityBll.WcStoredProc_List">WcStoredProc_List</see>) from the business logic layer which is
    ///         optimized and designed to integrate with this control.
    ///     </para>
    /// 	<para></para>
    /// </summary>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Entity_List_Type.html" cat="Framework and Design Pattern">Entity List Type</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Entity_Design_Pattern_In_The_UI_Layer.html" cat="Framework and Design Pattern">Entity Design Pattern In The UI Layer</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntity.html" cat="Framework and Design Pattern">ucEntity</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntityList.html" cat="Framework and Design Pattern">ucEntityList</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntityProps.html" cat="Framework and Design Pattern">ucEntityProps</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Tracing_Overview.html" cat="Framework and Design Pattern">Tracing Overview</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Exception_Handling_Overview.html" cat="Framework and Design Pattern">Exception Handling Overview</seealso>
    public partial class ucWcStoredProcList : UserControl, ICustomListControl
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "ucWcStoredProcList";

        string _parentType;

        Guid? _guidParentId;
        Int64? _longParentId;
        Int32? _intParentId;
        Int16? _shortParentId;
        Byte? _byteParentId;
        object _oParentId;

        Guid? _guidCurrentId; 
        Int64? _longCurrentId;
        Int32? _intCurrentId;
        Int16? _shortCurrentId;
        Byte? _byteCurrentId;


        /// <summary>
        ///     In cases when an entity is used in a dropdown list and the user wants to edit an
        ///     item in the list, or add a new item; the text the user entered could be passed into
        ///     this parameter and preloaded into the data entry part of the screen. Is passed in
        ///     via <see cref="SetStateFromParent">SetStateFromParent</see>.
        /// </summary>
        string _newText = "";
        /// <summary>
        ///     Currenly not being used, but is a stuf for future use. See <see cref="TypeServices.EntityState">EntityState</see> for more information.
        /// </summary>
        EntityState _currentEntityState;  // Not being used but is a stuf for future use. 
        /// <summary>
        ///     This event is raised from <see cref="RaiseNavigationListSelectedItemChanged">RaiseNavigationListSelectedItemChanged</see>
        ///     and notifies any object consuming this control that a different item in the list
        ///     has been changed. The <see cref="EntityBll.WcStoredProc_Bll">WcStoredProc_Bll</see> object bound
        ///     to the selected item is passed in the event args. Typicaly this will be passed up
        ///     to <see cref="ucWcStoredProc">ucWcStoredProc</see> and then down to <see cref="ucWcStoredProcProps">ucProps</see>; and will be made the current business object in
        ///     ucProps.
        /// </summary>
        public event NavigationListSelectedItemChangedEventHandler NavigationListSelectedItemChanged;

        /// <summary>
        ///     See <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> for
        ///     information.
        /// </summary>
        public event CurrentEntityStateEventHandler CurrentEntityStateChanged;


        public event SplitScreenModeChangedEventHandler SplitScreenModeChanged;

        public event CollapseExpandAllParentNavGridsEventHandler ChangeParentNavGridExpansion;

        /// <summary>
        ///     The default width for this control. See <see cref="GetListWidth">GetListWidth</see>.
        /// </summary>
        private int _listWidth = 300;
        /// <value>
        ///     The default height for this control. See <see cref="GetListHeight">GetListHeight</see>.
        /// </value>
        private int _listHeight = 600;
        private Guid? _lastUpdatedId = null;



        AddNewRowLocation _gridAllowAddRowMode = AddNewRowLocation.Top;

        bool _isSplitSreenMode = false;
        bool _allowSplitSreenMode = true;
        bool _isGridReadOnly = false;  // Do not allow edits even for full grids (not split screen)
        bool _isRichGridVisible = false;

        bool _displyHideColumnButton = false;

        GroupByAreaLocation _allowGrouping = GroupByAreaLocation.Hidden;

        SummaryRowLocation _allowColumnSummary = SummaryRowLocation.None;

        FilterUIType _FilterType = FilterUIType.None;

        FixedColumnType _allowFixedColumns = FixedColumnType.Disabled;

        FixedIndicatorDirection _fixedColumnIndicatorDirection = FixedIndicatorDirection.Left;

        FixedDropAreaLocation _fixedColumnDropAreaLocation = FixedDropAreaLocation.Both;

        private bool _hasSelectedRows = false;
        private bool _hasSelectedCells = false;

        bool _isRowInEditMode = false;
        Infragistics.Controls.Grids.Row _activeRow = null;

        ColumnGroupManager _colGrpMngr = null;
        Guid _gridId = new Guid("9eae48e6-2b86-4e4b-b31a-3e89119af2b5");


        #endregion Initialize Variables


        #region Constructor

        /// <summary>
        ///     This control is the list control for the list class (<see cref="EntityBll.WcStoredProc_List">WcStoredProc_List</see>). This control can be used as the
        ///     navigation list in an <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/The_Entity_Manager.html">Entity Manager</a> (<see cref="ucWcStoredProc">ucWcStoredProc</see>) and is
        ///     sometimes configured in Read/Write mode. In this case <see cref="ucWcStoredProcProps">ucProps</see> may not be needed to edit the WcStoredProc.
        /// </summary>
        public ucWcStoredProcList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcStoredProcList", IfxTraceCategory.Enter);
                InitializeComponent();
                Initialize_navList();  
                navList.ItemsSource = new WcStoredProc_List();
                Set_vXamComboColumn_ItemSources();
                MenuConfiguration();
                _userContext.SecurityArtifactsRetrieved += new SecurityArtifactsRetrievedEventHandler(UserSecurityContext_SecurityArtifactsRetrieved);
                SetSecurityState();
                AssignTextLengthLabels();
                ConfigureColumnHeaderTooltips();
                CustomConstructionCode();
                if (GridColumnGroupHelper.GetGridColumnGroupTypePreference(_gridId) == 2)
                {
                    rdbCustomColumnGroups.IsChecked = true;
                }
                else
                {
                    rdbSystemColumnGroups.IsChecked = true;
                }
                // Call these 2 lines after setting the radio buttons above to the is checked event doesnt call the following ws twice.
                _colGrpMngr = new ColumnGroupManager();
                _colGrpMngr.Initialize_cmbColumnGroups(navList, cmbColumnGroups, GridId, Credentials.UserId);


                navList.RowExpansionChanged += navList_RowExpansionChanged;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcStoredProcList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcStoredProcList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructor


        #region Menu Configuration
        

        void MenuConfiguration()
        {

            if (_mnuClearFilters_AllowVisibility == false)  
            {
                mnuClearFilters.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (_mnuGridTools_AllowVisibility == false)
            {
                mnuGridTools.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (_mnuRichGrid_AllowVisibility == false)
            {
				//mnuRichGrid.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (_mnuSplitScreen_AllowVisibility == false)
            {
                mnuSplitScreen.Visibility = System.Windows.Visibility.Collapsed;
            }

            //if (_isAllowNewRow == false)
            //{
            //    mnuNewRowOnTop.Visibility = System.Windows.Visibility.Collapsed;
            //}

            if (_mnuExcelExport_AllowVisibility == false)
            {
                mnuExcelExport.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (_gridDataSourceCombo_AllowVisibility == false)
            {
                //lblGridData.Visibility = System.Windows.Visibility.Collapsed;
                //cmbGridDataSource.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (_menuGridRow_AllowVisibility == false)
            {
                GridMenuRow.Height = new GridLength(0);
                //LayoutRoot.RowDefinitions[0].Height = new System.Windows.GridLength(0);
            }

        }

        #endregion Menu Configuration


        #region Load

        // Need comments here.
        public WcStoredProc_List NavList_ItemSource
        {
            get { return (WcStoredProc_List)navList.ItemsSource; }
            set {navList.ItemsSource=value;}
        }
   
        public void SetStateFromParent(int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 1
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 1", IfxTraceCategory.Enter);
                SetStateFromParent("", intParentId, guidParentId, intId, guidId, null, null, "");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 1", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 1", IfxTraceCategory.Leave);
            }
        }


        public void SetStateFromParent(string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 2
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 2", IfxTraceCategory.Enter);
                SetStateFromParent(parentType, intParentId, guidParentId, intId, guidId, null, null, "");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 2", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 2", IfxTraceCategory.Leave);
            }
        }


        public void SetStateFromParent(string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 3
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 3", IfxTraceCategory.Enter);
                SetStateFromParent(parentType, intParentId, guidParentId, intId, guidId, null, null, newText);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 3", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 3", IfxTraceCategory.Leave);
            }
        }


        public void SetStateFromParent(int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, IEntity_ValuesMngr[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 4
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 4", IfxTraceCategory.Enter);
                SetStateFromParent("", intParentId, guidParentId, intId, guidId, null, list, newText);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 4", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 4", IfxTraceCategory.Leave);
            }
        }


        public void SetStateFromParent(string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, IEntity_ValuesMngr[] list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 5
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 5", IfxTraceCategory.Enter);
                SetStateFromParent(parentType, intParentId, guidParentId, intId, guidId, null, list, "");

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 5", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 5", IfxTraceCategory.Leave);
            }
        }


        public void SetStateFromParent(string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, IEntity_ValuesMngr[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 6
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 6", IfxTraceCategory.Enter);
                SetStateFromParent(parentType, intParentId, guidParentId, intId, guidId, null, list, newText);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 6", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 6", IfxTraceCategory.Leave);
            }
        }


        public void SetStateFromParent(string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, IBusinessObject currentBusinessObject, IEntity_ValuesMngr[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 7
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Enter);
                SetStateFromParent(null, parentType, intParentId, guidParentId, null, intId, guidId, null, null, list, newText);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Leave);
            }
        }

        #endregion Load


        #region Editors

        //  combo rowsources, format controls, etc.


        #endregion Editors


        #region Grid


        void Initialize_navList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Initialize_navList", IfxTraceCategory.Enter);
                //navList.SummaryRowSettings.AllowSummaryRow = SummaryRowLocation.Top;
                navList.SummaryRowSettings.SummaryScope = SummaryScope.ChildBand;
                navList.SummaryRowSettings.SummaryExecution = Infragistics.SummaryExecution.AfterFilteringBeforePaging;

                navList.ColumnChooserSettings.AllowHiddenColumnIndicator = true;
                navList.FixedColumnSettings.FixedDropAreaLocation = FixedDropAreaLocation.Both;

                navList.EditingSettings.IsOnCellActiveEditingEnabled = false;
                navList.EditingSettings.IsEnterKeyEditingEnabled = true;
                navList.EditingSettings.IsMouseActionEditingEnabled = MouseEditingAction.SingleClick;

                navList.AddNewRowSettings.AllowAddNewRow = _gridAllowAddRowMode;
                navList.RowSelectorSettings.Visibility = Visibility.Visible;
                navList.RowSelectorSettings.EnableRowNumbering = true;
                navList.IsAlternateRowsEnabled = true;

                navList.DeleteKeyAction = DeleteKeyAction.None;

                // SET EVENTS

                // Grid Cell Events
                //navList.CellEnteringEditMode += new EventHandler<BeginEditingCellEventArgs>(navList_CellEnteringEditMode);
                navList.CellEnteredEditMode += new EventHandler<EditingCellEventArgs>(navList_CellEnteredEditMode);
                
                
//                navList.CellExitingEditMode += new EventHandler<ExitEditingCellEventArgs>(navList_CellExitingEditMode);
//                navList.CellExitedEditMode += new EventHandler<CellExitedEditingEventArgs>(navList_CellExitedEditMode); 
                
                navList.CellClicked += new EventHandler<CellClickedEventArgs>(navList_CellClicked);
                navList.SelectedCellsCollectionChanged += new EventHandler<SelectionCollectionChangedEventArgs<SelectedCellsCollection>>(navList_SelectedCellsCollectionChanged);  

                // Grid Row Events
                navList.RowEnteringEditMode += new EventHandler<BeginEditingRowEventArgs>(navList_RowEnteringEditMode);
                navList.RowEnteredEditMode += new EventHandler<EditingRowEventArgs>(navList_RowEnteredEditMode);
                navList.RowExitingEditMode += new EventHandler<ExitEditingRowEventArgs>(navList_RowExitingEditMode);
                navList.RowDeleting += new EventHandler<CancellableRowEventArgs>(navList_RowDeleting);

                //navList.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(navList_PropertyChanged);
                //navList.BindingValidationError += new EventHandler<ValidationErrorEventArgs>(navList_BindingValidationError);

                // SAVE This one
                navList.SelectedRowsCollectionChanged += new EventHandler<SelectionCollectionChangedEventArgs<SelectedRowsCollection>>(navList_SelectedRowsCollectionChanged);

                navList_GridReadOnlyMode();

                // Clipboard
                navList.SelectionSettings.CellSelection = SelectionType.Multiple;
                navList.SelectionSettings.RowSelection = SelectionType.Multiple;
                navList.ClipboardCopying += new EventHandler<ClipboardCopyingEventArgs>(navList_ClipboardCopying);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Initialize_navList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Initialize_navList", IfxTraceCategory.Leave);
            }
        }

        #endregion Grid


        #region Grid Menu Interaction


        public void navList_AllowAddRowMode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_AllowAddRowMode", IfxTraceCategory.Enter);
                if (IsGridReadOnly)
                {
                    navList.AddNewRowSettings.AllowAddNewRow = AddNewRowLocation.None;
                    //mnuNewRowOnTop.Visibility = Visibility.Collapsed;
                }
                else
                {
                    if (_isAllowNewRow)
                    {
                        navList.AddNewRowSettings.AllowAddNewRow = _gridAllowAddRowMode;
                        //    mnuNewRowOnTop.Visibility = Visibility.Visible;
                        //    switch (_gridAllowAddRowMode)
                        //    {
                        //        case AddNewRowLocation.Top:
                        //            mnuNewRowOnTop.Icon = vVelocityImageProvider.LoadImage(vVelocityImageProvider.GetMenuVelocityImagePath(vVelocityImageProvider.MenuImage.NewRowOnTop16));
                        //            break;
                        //        case AddNewRowLocation.Bottom:
                        //            mnuNewRowOnTop.Icon = vVelocityImageProvider.LoadImage(vVelocityImageProvider.GetMenuVelocityImagePath(vVelocityImageProvider.MenuImage.NewRowOnBottom16));
                        //            break;
                        //    }
                    }
                    else  //  added 2012-07-03.  when hiding the new row menu, we probably want to hide the new row also.
                    {
                        navList.AddNewRowSettings.AllowAddNewRow = AddNewRowLocation.None;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_AllowAddRowMode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_AllowAddRowMode", IfxTraceCategory.Leave);
            }
        }

        public void navList_GridReadOnlyMode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_GridReadOnlyMode", IfxTraceCategory.Enter);

                if (IsGridReadOnly)
                {
                    navList.EditingSettings.AllowEditing = EditingType.None;
                }
                else
                {
                    navList.EditingSettings.AllowEditing = EditingType.Row;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_GridReadOnlyMode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_GridReadOnlyMode", IfxTraceCategory.Leave);
            }
        }

        public void InitializeSplitScreenAndReadOnlyModes(bool isSplit, bool allowSplit, bool isReadOnly, bool isAllowNewRow)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeSplitScreenAndReadOnlyModes", IfxTraceCategory.Enter);

                _isSplitSreenMode = isSplit;
                _allowSplitSreenMode = allowSplit;
                _isGridReadOnly = isReadOnly;
                _isAllowNewRow = isAllowNewRow;
                navList_SplitScreenMode();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeSplitScreenAndReadOnlyModes", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeSplitScreenAndReadOnlyModes", IfxTraceCategory.Leave);
            }
        }

        public void navList_SplitScreenMode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SplitScreenMode", IfxTraceCategory.Enter);

                if (_allowSplitSreenMode != true || _mnuSplitScreen_AllowVisibility == false)
                {
                    mnuSplitScreen.Visibility = System.Windows.Visibility.Collapsed;
                }
                else
                {
                    mnuSplitScreen.Visibility = System.Windows.Visibility.Visible;
                    if (_isSplitSreenMode)
                    {
                        mnuSplitScreen.Icon = vVelocityImageProvider.LoadImage(vVelocityImageProvider.GetMenuVelocityImagePath(vVelocityImageProvider.MenuImage.FullGrid16));
                    }
                    else
                    {
                        mnuSplitScreen.Icon = vVelocityImageProvider.LoadImage(vVelocityImageProvider.GetMenuVelocityImagePath(vVelocityImageProvider.MenuImage.SplitScreen16));
                    }
                }

                navList_AllowAddRowMode();
                navList_GridReadOnlyMode();
                RaiseSplitScreenModeChanged();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SplitScreenMode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SplitScreenMode", IfxTraceCategory.Leave);
            }
        }

        void RaiseSplitScreenModeChanged()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseSplitScreenModeChanged", IfxTraceCategory.Enter);
                SplitScreenModeChangedArgs e = new SplitScreenModeChangedArgs(IsSplitSreenMode, IsGridReadOnly);
                SplitScreenModeChangedEventHandler handler = SplitScreenModeChanged;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseSplitScreenModeChanged", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseSplitScreenModeChanged", IfxTraceCategory.Leave);
            }
        }

        public void navList_ShowHideRichGrid()
        {/*
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_ShowHideRichGrid", IfxTraceCategory.Enter);

                if (_isRichGridVisible == false)
                {
                    navList.Columns.DataColumns["RichGrid"].Visibility = Visibility.Visible;
                    mnuRichGrid.Icon = vVelocityImageProvider.LoadImage(vVelocityImageProvider.GetMenuVelocityImagePath(vVelocityImageProvider.MenuImage.RichGridOff21x15));
                    navList.AddNewRowSettings.AllowAddNewRow = AddNewRowLocation.None;
                    //mnuNewRowOnTop.Visibility = Visibility.Collapsed;
                }
                else
                {
                    navList.Columns.DataColumns["RichGrid"].Visibility = Visibility.Collapsed;
                    mnuRichGrid.Icon = vVelocityImageProvider.LoadImage(vVelocityImageProvider.GetMenuVelocityImagePath(vVelocityImageProvider.MenuImage.RichGrid21x15));
                    navList_AllowAddRowMode();
                }
                _isRichGridVisible = !_isRichGridVisible;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_ShowHideRichGrid", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_ShowHideRichGrid", IfxTraceCategory.Leave);
            } */
        }

        public void navList_DisplyHideColumnButton()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_DisplyHideColumnButton", IfxTraceCategory.Enter);
                if (_displyHideColumnButton == true)
                {
                    navList.ColumnChooserSettings.AllowHideColumnIcon = false;
                    mnuDisplyHideColumnButton.Header = "Hide Column Button  -  Add";
                }
                else
                {
                    navList.ColumnChooserSettings.AllowHideColumnIcon = true;
                    mnuDisplyHideColumnButton.Header = "Hide Column Button  -  Remove";
                }
                _displyHideColumnButton = !_displyHideColumnButton;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_DisplyHideColumnButton", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_DisplyHideColumnButton", IfxTraceCategory.Leave);
            }
        }

        public void navList_ColumnGroupingMode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_ColumnGroupingMode", IfxTraceCategory.Enter);
                if (_allowGrouping == GroupByAreaLocation.Hidden)
                {
                    _allowGrouping = GroupByAreaLocation.Top;
                    mnuColumnGrouping.Header = "Column Grouping  -  Remove";
                }
                else
                {
                    _allowGrouping = GroupByAreaLocation.Hidden;
                    mnuColumnGrouping.Header = "Column Grouping  -  Add";
                }
                navList.GroupBySettings.AllowGroupByArea = _allowGrouping;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_ColumnGroupingMode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_ColumnGroupingMode", IfxTraceCategory.Leave);
            }
        }

        public void navList_ColumnSummaryMode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_ColumnSummaryMode", IfxTraceCategory.Enter);
                if (_allowColumnSummary == SummaryRowLocation.None)
                {
                    _allowColumnSummary = SummaryRowLocation.Top;
                    mnuColumnSummary.Header = "Column Summary  -  Hide";
                }
                else
                {
                    _allowColumnSummary = SummaryRowLocation.None;
                    mnuColumnSummary.Header = "Column Summary  -  Show";
                }
                navList.SummaryRowSettings.AllowSummaryRow = _allowColumnSummary;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_ColumnSummaryMode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_ColumnSummaryMode", IfxTraceCategory.Leave);
            }
        }

        public void navList_FilterMode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FilterMode", IfxTraceCategory.Enter);
                navList.FilteringSettings.AllowFiltering = _FilterType;
                switch (_FilterType)
                {
                    case FilterUIType.None:
                        //mnuFilterNone.IsCheckable = true;
                        //mnuFilterMenu.IsCheckable = false;
                        //mnuFilterTop.IsCheckable = false;
                        //mnuFilterBottom.IsCheckable = false;
                        mnuFilterNone.IsChecked = true;
                        mnuFilterMenu.IsChecked = false;
                        mnuFilterTop.IsChecked = false;
                        mnuFilterBottom.IsChecked = false;
                        break;
                    case FilterUIType.FilterMenu:
                        //mnuFilterNone.IsCheckable = false;
                        //mnuFilterMenu.IsCheckable = true;
                        //mnuFilterTop.IsCheckable = false;
                        //mnuFilterBottom.IsCheckable = false;
                        mnuFilterNone.IsChecked = false;
                        mnuFilterMenu.IsChecked = true;
                        mnuFilterTop.IsChecked = false;
                        mnuFilterBottom.IsChecked = false;
                        break;
                    case FilterUIType.FilterRowTop:
                        //mnuFilterNone.IsCheckable = false;
                        //mnuFilterMenu.IsCheckable = false;
                        //mnuFilterTop.IsCheckable = true;
                        //mnuFilterBottom.IsCheckable = false;
                        mnuFilterNone.IsChecked = false;
                        mnuFilterMenu.IsChecked = false;
                        mnuFilterTop.IsChecked = true;
                        mnuFilterBottom.IsChecked = false;
                        break;
                    case FilterUIType.FilterRowBottom:
                        //mnuFilterNone.IsCheckable = false;
                        //mnuFilterMenu.IsCheckable = false;
                        //mnuFilterTop.IsCheckable = false;
                        //mnuFilterBottom.IsCheckable = true;
                        mnuFilterNone.IsChecked = false;
                        mnuFilterMenu.IsChecked = false;
                        mnuFilterTop.IsChecked = false;
                        mnuFilterBottom.IsChecked = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FilterMode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FilterMode", IfxTraceCategory.Leave);
            }
        }

        public void navList_FixedColumnMode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FixedColumnMode", IfxTraceCategory.Enter);
                if (mnuFixedColumn_UseDropArea.IsChecked == false && mnuFixedColumn_UsePin.IsChecked == false)
                {
                    _allowFixedColumns = FixedColumnType.Disabled;
                    mnuFixedColumn_Indicator_Left.IsEnabled = false;
                    mnuFixedColumn_Indicator_Right.IsEnabled = false;

                    mnuFixedColumn_DropArea_Left.IsEnabled = false;
                    mnuFixedColumn_DropArea_Right.IsEnabled = false;
                    mnuFixedColumn_DropArea_Both.IsEnabled = false;
                }
                else if (mnuFixedColumn_UseDropArea.IsChecked == true && mnuFixedColumn_UsePin.IsChecked == false)
                {
                    _allowFixedColumns = FixedColumnType.DropArea;
                    mnuFixedColumn_Indicator_Left.IsEnabled = false;
                    mnuFixedColumn_Indicator_Right.IsEnabled = false;

                    mnuFixedColumn_DropArea_Left.IsEnabled = true;
                    mnuFixedColumn_DropArea_Right.IsEnabled = true;
                    mnuFixedColumn_DropArea_Both.IsEnabled = true;
                }
                else if (mnuFixedColumn_UseDropArea.IsChecked == false && mnuFixedColumn_UsePin.IsChecked == true)
                {
                    _allowFixedColumns = FixedColumnType.Indicator;
                    mnuFixedColumn_Indicator_Left.IsEnabled = true;
                    mnuFixedColumn_Indicator_Right.IsEnabled = true;

                    mnuFixedColumn_DropArea_Left.IsEnabled = false;
                    mnuFixedColumn_DropArea_Right.IsEnabled = false;
                    mnuFixedColumn_DropArea_Both.IsEnabled = false;
                }
                else if (mnuFixedColumn_UseDropArea.IsChecked == true && mnuFixedColumn_UsePin.IsChecked == true)
                {
                    _allowFixedColumns = FixedColumnType.Both;
                    mnuFixedColumn_Indicator_Left.IsEnabled = true;
                    mnuFixedColumn_Indicator_Right.IsEnabled = true;

                    mnuFixedColumn_DropArea_Left.IsEnabled = true;
                    mnuFixedColumn_DropArea_Right.IsEnabled = true;
                    mnuFixedColumn_DropArea_Both.IsEnabled = true;
                }
                
                //switch (_allowFixedColumns)
                //{
                //    case FixedColumnType.Disabled:
                //        //mnuFixedColumn_UsePin.IsChecked = true;
                //        //mnuFixedColumn_Indicator.IsChecked = false;
                //        mnuFixedColumn_Indicator_Left.IsEnabled = false;
                //        mnuFixedColumn_Indicator_Right.IsEnabled = false;
                //        //mnuFixedColumn_UsePin.IsCheckable = true;
                //        //mnuFixedColumn_Indicator.IsCheckable = false;
                //        //mnuFixedColumn_UseDropArea.IsCheckable = false;
                //        //mnuFixedColumn_Both.IsCheckable = false;
                //        //mnuFixedColumn_IndicatorDirection.IsCheckable = false;
                //        //mnuFixedColumn_AreaLocation.IsCheckable = false;
                //        break;
                //    case FixedColumnType.Indicator:
                //        //mnuFixedColumn_UsePin.IsCheckable = false;
                //        //mnuFixedColumn_Indicator.IsCheckable = true;
                //        //mnuFixedColumn_UseDropArea.IsCheckable = false;
                //        //mnuFixedColumn_Both.IsCheckable = false;
                //        //mnuFixedColumn_IndicatorDirection.IsCheckable = true;
                //        //mnuFixedColumn_AreaLocation.IsCheckable = false;
                //        mnuFixedColumn_UsePin.IsChecked = false;
                //        //mnuFixedColumn_Indicator.IsChecked = true;
                //        mnuFixedColumn_Indicator_Left.IsEnabled = true;
                //        mnuFixedColumn_Indicator_Right.IsEnabled = true;

                //        break;
                //    case FixedColumnType.DropArea:
                //        //mnuFixedColumn_UsePin.IsCheckable = false;
                //        //mnuFixedColumn_Indicator.IsCheckable = false;
                //        //mnuFixedColumn_UseDropArea.IsCheckable = true;
                //        //mnuFixedColumn_Both.IsCheckable = false;
                //        //mnuFixedColumn_IndicatorDirection.IsCheckable = false;
                //        //mnuFixedColumn_AreaLocation.IsCheckable = true;
                //        //mnuFixedColumn_UsePin.IsChecked = false;
                //        //mnuFixedColumn_IndicatorDirection.IsEnabled = false;
                //        //mnuFixedColumn_AreaLocation.IsEnabled = true;

                //        break;
                //    case FixedColumnType.Both:
                //        //mnuFixedColumn_UsePin.IsCheckable = false;
                //        //mnuFixedColumn_Indicator.IsCheckable = false;
                //        //mnuFixedColumn_UseDropArea.IsCheckable = false;
                //        //mnuFixedColumn_Both.IsCheckable = true;
                //        //mnuFixedColumn_IndicatorDirection.IsCheckable = true;
                //        //mnuFixedColumn_AreaLocation.IsCheckable = true;
                //        //_allowFixedColumns = FixedColumnType.Both;
                //        //mnuFixedColumn_IndicatorDirection.IsEnabled = true;
                //        //mnuFixedColumn_AreaLocation.IsEnabled = true;

                //        break;
                //}


                navList.FixedColumnSettings.AllowFixedColumns = _allowFixedColumns;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FixedColumnMode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FixedColumnMode", IfxTraceCategory.Leave);
            }
        }

        public void navList_FixedColumnIndicatorDirection()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FixedColumnIndicatorDirection", IfxTraceCategory.Enter);
                switch (_fixedColumnIndicatorDirection)
                {
                    case FixedIndicatorDirection.Left:
                        //mnuFixedColumn_Indicator_Left.IsChecked = true;
                        mnuFixedColumn_Indicator_Right.IsChecked = false;
                        break;
                    case FixedIndicatorDirection.Right:
                        mnuFixedColumn_Indicator_Left.IsChecked = false;
                        //mnuFixedColumn_Indicator_Right.IsChecked = true;
                        break;
                }
                navList.FixedColumnSettings.FixedIndicatorDirection = _fixedColumnIndicatorDirection;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FixedColumnIndicatorDirection", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FixedColumnIndicatorDirection", IfxTraceCategory.Leave);
            }
        }

        public void navList_FixedColumnDropAreaLocation()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FixedColumnDropAreaLocation", IfxTraceCategory.Enter);
                switch (_fixedColumnDropAreaLocation)
                {
                    case FixedDropAreaLocation.Left:
                        _fixedColumnDropAreaLocation = FixedDropAreaLocation.Left;
                        //mnuFixedColumn_DropArea_Left.IsChecked = true;
                        mnuFixedColumn_DropArea_Right.IsChecked = false;
                        mnuFixedColumn_DropArea_Both.IsChecked = false;
                        break;
                    case FixedDropAreaLocation.Right:
                        _fixedColumnDropAreaLocation = FixedDropAreaLocation.Right;
                        mnuFixedColumn_DropArea_Left.IsChecked = false;
                        //mnuFixedColumn_DropArea_Right.IsChecked = true;
                        mnuFixedColumn_DropArea_Both.IsChecked = false;
                        break;
                    case FixedDropAreaLocation.Both:
                        _fixedColumnDropAreaLocation = FixedDropAreaLocation.Both;
                        mnuFixedColumn_DropArea_Left.IsChecked = false;
                        mnuFixedColumn_DropArea_Right.IsChecked = false;
                        //mnuFixedColumn_DropArea_Both.IsChecked = true;
                        break;
                }
                navList.FixedColumnSettings.FixedDropAreaLocation = _fixedColumnDropAreaLocation;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FixedColumnDropAreaLocation", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FixedColumnDropAreaLocation", IfxTraceCategory.Leave);
            }
        }

        #endregion Grid Menu Interaction


        #region Events



        #region GridCell Events

        void navList_CellClicked(object sender, CellClickedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Enter);

                if (e.Cell.Row.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                {
                    //WcStoredProc_Bll obj = e.Cell.Row.Data as WcStoredProc_Bll;
                    ((Row)e.Cell.Row).IsSelected = true;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Leave);
            }
        }

        void navList_SelectedCellsCollectionChanged(object sender, SelectionCollectionChangedEventArgs<SelectedCellsCollection> e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedCellsCollectionChanged", IfxTraceCategory.Enter);
                this._hasSelectedCells = e.NewSelectedItems.Any();
                this.SetClipboardSetting();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedCellsCollectionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedCellsCollectionChanged", IfxTraceCategory.Leave);
            }
        }

        void navList_CellEnteredEditMode(object sender, EditingCellEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellEnteredEditMode", IfxTraceCategory.Enter);
                WcStoredProc_Bll obj = e.Cell.Row.Data as WcStoredProc_Bll;
                if (obj == null) { return; }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellEnteredEditMode", ex);
				   // throw IfxWrapperException.GetError(ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellEnteredEditMode", IfxTraceCategory.Leave);
            }
        }

        #endregion GridCell Events


        #region GridRow Events

        void navList_RowEnteringEditMode(object sender, BeginEditingRowEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteringEditMode", IfxTraceCategory.Enter);

                e.Row.IsSelected = true;
                if (e.Row.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = e.Row.Data as WcStoredProc_Bll;
                    _guidCurrentId = obj.Sp_Id;
                    HookGridCellBusnessObjectEvents(obj, true);
                }
                else if (e.Row.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = e.Row.Data as WcStoredProcParam_Bll;
                    _guidCurrentId = obj.SpP_ID;
                    HookGridCellBusnessObjectEvents(obj, true);
                }
                else if (e.Row.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = e.Row.Data as WcStoredProcParamValueGroup_Bll;
                    _guidCurrentId = obj.SpPVGrp_Id;
                    HookGridCellBusnessObjectEvents(obj, true);
                }
                else if (e.Row.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = e.Row.Data as WcStoredProcColumn_Bll;
                    _guidCurrentId = obj.SpCl_Id;
                    HookGridCellBusnessObjectEvents(obj, true);
                }
                _activeRow = e.Row;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteringEditMode", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteringEditMode", IfxTraceCategory.Leave);
            }
        }

        void navList_RowEnteredEditMode(object sender, EditingRowEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode", IfxTraceCategory.Enter);
                if (e.Row.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = e.Row.Data as WcStoredProc_Bll;
                    _isRowInEditMode = true;
                    if (obj.State.IsNew() == true)
                    {
                        obj.StandingFK = (Guid)GuidParentId;
                        SetNewRowValidation();
                    }
                }
                else if (e.Row.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = e.Row.Data as WcStoredProcParam_Bll;
                    WcStoredProc_Bll parent = e.Row.ParentRow.Data as WcStoredProc_Bll;
                    _isRowInEditMode = true;
                    if (obj.State.IsNew() == true)
                    {
                        obj.StandingFK = parent.Sp_Id;
                        SetNewRowValidation();
                    }
                }
                else if (e.Row.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = e.Row.Data as WcStoredProcParamValueGroup_Bll;
                    WcStoredProc_Bll parent = e.Row.ParentRow.Data as WcStoredProc_Bll;
                    _isRowInEditMode = true;
                    if (obj.State.IsNew() == true)
                    {
                        obj.StandingFK = parent.Sp_Id;
                        SetNewRowValidation();
                    }
                }
                else if (e.Row.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = e.Row.Data as WcStoredProcColumn_Bll;
                    WcStoredProc_Bll parent = e.Row.ParentRow.Data as WcStoredProc_Bll;
                    _isRowInEditMode = true;
                    if (obj.State.IsNew() == true)
                    {
                        obj.StandingFK = parent.Sp_Id;
                        SetNewRowValidation();
                    }
                }
                navList_RowEnteredEditMode_Custom(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///  Use this to determin if the Save operation was inserting a new row, or updating an existing one.  
        ///  This is used in the RowDataObject_AsyncSaveWithResponseComplete event, and if   '_isInsertingNewRow = true'  then code will place the new row at the top of the list.
        ///  Otherwise, the row being edited will not be moved to the top of the list and it's position will remain the same
        /// </summary>
        bool _isInsertingNewRow = false;
        List<IBusinessObject> _rowsCallingSave = new List<IBusinessObject>();
        void navList_RowExitingEditMode(object sender, ExitEditingRowEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowExitingEditMode", IfxTraceCategory.Enter);
                if (e.Row.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = e.Row.Data as WcStoredProc_Bll;
                    obj.AsyncSaveComplete += new AsyncSaveCompleteEventHandler(RowDataObject_AsyncSaveComplete);
                    _isInsertingNewRow = false;  //  Clear this before we start  
                    if (e.EditingCanceled == true)
                    {
                        obj.UnDo();
                        if (obj.State.IsNew() == true)
                        {
                            // UnDo Raised CurrentEntityStateChanged, but the State that bubbled up was 'NewValidNotDirty'
                            //    which will not be correct since this instance of the business object will go away and the true state will be 'None'
                            CurrentEntityStateArgs args = new CurrentEntityStateArgs(EntityStateSwitch.None, null, null, obj);
                            OnCurrentEntityStateChanged(this, args);
                        }
                        ClearGridRowBrokenRuleToolTips(e.Row);
                        HookGridCellBusnessObjectEvents(obj, false);
                    }
                    else if (obj.State.IsValid() == false)
                    {
                        e.Cancel = true;
                        return;
                    }
                    else if (obj.State.IsDirty() == true)
                    {
                        if (obj.State.IsNew() == true)
                        {
                            //This is used in the RowDataObject_AsyncSaveWithResponseComplete event
                            _isInsertingNewRow = true;
                        }
                        obj.AsyncSaveWithResponseComplete += new AsyncSaveWithResponseCompleteEventHandler(RowDataObject_AsyncSaveWithResponseComplete);
                        _rowsCallingSave.Add(obj);
                        obj.Save(UseConcurrencyCheck.UseDefaultSetting);
                    }
                    else
                    {
                        // do we need to raise the ConfigureToEntityState event here?
                        HookGridCellBusnessObjectEvents(obj, false);
                    }
                    _guidCurrentId = null;
                    _isRowInEditMode = false;
                    _activeRow = null;
                }
                else if (e.Row.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = e.Row.Data as WcStoredProcParam_Bll;
                    obj.AsyncSaveComplete += new AsyncSaveCompleteEventHandler(RowDataObject_AsyncSaveComplete);

                    _isInsertingNewRow = false; //  Clear this before we start  
                    if (e.EditingCanceled == true)
                    {
                        obj.UnDo();
                        if (obj.State.IsNew() == true)
                        {
                            // UnDo Raised CurrentEntityStateChanged, but the State that bubbled up was 'NewValidNotDirty'
                            //    which will not be correct since this instance of the business object will go away and the true state will be 'None'
                            CurrentEntityStateArgs args = new CurrentEntityStateArgs(EntityStateSwitch.None, null,
                                null, obj);
                            OnCurrentEntityStateChanged(this, args);
                        }

                        ClearGridRowBrokenRuleToolTips(e.Row);
                        HookGridCellBusnessObjectEvents(obj, false);
                    }
                    else if (obj.State.IsValid() == false)
                    {
                        e.Cancel = true;
                        return;
                    }
                    else if (obj.State.IsDirty() == true)
                    {
                        if (obj.State.IsNew() == true)
                        {
                            //This is used in the RowDataObject_AsyncSaveWithResponseComplete event
                            _isInsertingNewRow = true;
                        }
                        obj.AsyncSaveWithResponseComplete +=
                            new AsyncSaveWithResponseCompleteEventHandler(
                                RowDataObject_AsyncSaveWithResponseComplete);
                        _rowsCallingSave.Add(obj);
                        obj.Save(UseConcurrencyCheck.UseDefaultSetting);
                    }
                    else
                    {
                        // do we need to raise the ConfigureToEntityState event here?
                        HookGridCellBusnessObjectEvents(obj, false);
                    }
                    _guidCurrentId = null;
                    _isRowInEditMode = false;
                    _activeRow = null;
                }
                else if (e.Row.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = e.Row.Data as WcStoredProcParamValueGroup_Bll;
                    obj.AsyncSaveComplete += new AsyncSaveCompleteEventHandler(RowDataObject_AsyncSaveComplete);

                    _isInsertingNewRow = false; //  Clear this before we start  
                    if (e.EditingCanceled == true)
                    {
                        obj.UnDo();
                        if (obj.State.IsNew() == true)
                        {
                            // UnDo Raised CurrentEntityStateChanged, but the State that bubbled up was 'NewValidNotDirty'
                            //    which will not be correct since this instance of the business object will go away and the true state will be 'None'
                            CurrentEntityStateArgs args = new CurrentEntityStateArgs(EntityStateSwitch.None, null,
                                null, obj);
                            OnCurrentEntityStateChanged(this, args);
                        }

                        ClearGridRowBrokenRuleToolTips(e.Row);
                        HookGridCellBusnessObjectEvents(obj, false);
                    }
                    else if (obj.State.IsValid() == false)
                    {
                        e.Cancel = true;
                        return;
                    }
                    else if (obj.State.IsDirty() == true)
                    {
                        if (obj.State.IsNew() == true)
                        {
                            //This is used in the RowDataObject_AsyncSaveWithResponseComplete event
                            _isInsertingNewRow = true;
                        }
                        obj.AsyncSaveWithResponseComplete +=
                            new AsyncSaveWithResponseCompleteEventHandler(
                                RowDataObject_AsyncSaveWithResponseComplete);
                        _rowsCallingSave.Add(obj);
                        obj.Save(UseConcurrencyCheck.UseDefaultSetting);
                    }
                    else
                    {
                        // do we need to raise the ConfigureToEntityState event here?
                        HookGridCellBusnessObjectEvents(obj, false);
                    }
                    _guidCurrentId = null;
                    _isRowInEditMode = false;
                    _activeRow = null;
                }
                else if (e.Row.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = e.Row.Data as WcStoredProcColumn_Bll;
                    obj.AsyncSaveComplete += new AsyncSaveCompleteEventHandler(RowDataObject_AsyncSaveComplete);

                    _isInsertingNewRow = false; //  Clear this before we start  
                    if (e.EditingCanceled == true)
                    {
                        obj.UnDo();
                        if (obj.State.IsNew() == true)
                        {
                            // UnDo Raised CurrentEntityStateChanged, but the State that bubbled up was 'NewValidNotDirty'
                            //    which will not be correct since this instance of the business object will go away and the true state will be 'None'
                            CurrentEntityStateArgs args = new CurrentEntityStateArgs(EntityStateSwitch.None, null,
                                null, obj);
                            OnCurrentEntityStateChanged(this, args);
                        }

                        ClearGridRowBrokenRuleToolTips(e.Row);
                        HookGridCellBusnessObjectEvents(obj, false);
                    }
                    else if (obj.State.IsValid() == false)
                    {
                        e.Cancel = true;
                        return;
                    }
                    else if (obj.State.IsDirty() == true)
                    {
                        if (obj.State.IsNew() == true)
                        {
                            //This is used in the RowDataObject_AsyncSaveWithResponseComplete event
                            _isInsertingNewRow = true;
                        }
                        obj.AsyncSaveWithResponseComplete +=
                            new AsyncSaveWithResponseCompleteEventHandler(
                                RowDataObject_AsyncSaveWithResponseComplete);
                        _rowsCallingSave.Add(obj);
                        obj.Save(UseConcurrencyCheck.UseDefaultSetting);
                    }
                    else
                    {
                        // do we need to raise the ConfigureToEntityState event here?
                        HookGridCellBusnessObjectEvents(obj, false);
                    }
                    _guidCurrentId = null;
                    _isRowInEditMode = false;
                    _activeRow = null;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowExitingEditMode", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowExitingEditMode", IfxTraceCategory.Leave);
            }
        }

        void ClearGridRowBrokenRuleToolTips(Row row)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearGridRowBrokenRuleToolTips", IfxTraceCategory.Enter);
                if (row == null) { return; }
                foreach (var item in row.Cells)
                {
                    if (item != null && item.Control != null && item.Control.Content != null)
                    {
                        IvControlsValidation ctl = item.Control.Content as IvControlsValidation;
                        if (ctl != null)
                        {
                            vToolTipService.SetToolTip((Control)ctl, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearGridRowBrokenRuleToolTips", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearGridRowBrokenRuleToolTips", IfxTraceCategory.Leave);
            }
        }

        void RowDataObject_AsyncSaveWithResponseComplete(object sender, AsyncSaveWithResponseCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RowDataObject_AsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
                if (sender is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = sender as WcStoredProc_Bll;
                        obj.AsyncSaveWithResponseComplete -= new AsyncSaveWithResponseCompleteEventHandler(RowDataObject_AsyncSaveWithResponseComplete);
                        _rowsCallingSave.Remove(obj);
                }
                else if (sender is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = sender as WcStoredProcParam_Bll;
                    obj.AsyncSaveWithResponseComplete -= new AsyncSaveWithResponseCompleteEventHandler(RowDataObject_AsyncSaveWithResponseComplete);
                    _rowsCallingSave.Remove(obj);
                }
                else if (sender is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = sender as WcStoredProcParamValueGroup_Bll;
                    obj.AsyncSaveWithResponseComplete -= new AsyncSaveWithResponseCompleteEventHandler(RowDataObject_AsyncSaveWithResponseComplete);
                    _rowsCallingSave.Remove(obj);
                }
                else if (sender is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = sender as WcStoredProcColumn_Bll;
                    obj.AsyncSaveWithResponseComplete -= new AsyncSaveWithResponseCompleteEventHandler(RowDataObject_AsyncSaveWithResponseComplete);
                    _rowsCallingSave.Remove(obj);
                }

                if (e.Response.Result != DataOperationResult.Success)
                {
                    //** We need a more rebust action here such as selecting the row and highlighting it or something.
                     if (e.Response.Result  == DataOperationResult.ConcurrencyFailure)
                    {
                        string str1 = "Another user has changed this record since you downloaded it." + Environment.NewLine;
                        string str2 = "1)    Your changes will be lost." + Environment.NewLine;
                        string str3 = "2)    If you have any data that’s difficult to recreate such as long comments, " + Environment.NewLine;
                        string str4 = "       go back to the same row and copy those fields" + Environment.NewLine;
                        string str5 = "       to another program such as Notepad." + Environment.NewLine;
                        string str6 = "3)    Now please refresh the grid and try again.";

                        MessageBox.Show(str1 + str2 + str3 + str4 + str5 + str6, "Data Concurrency Problem", MessageBoxButton.OK);
                    }
                     else if (e.FailedReasonText == null)
                    {
                        MessageBox.Show("There was a problem saving your data.  Please refresh the grid to see if your changes were saved.  If this continues, please contact support.", "Save Error", MessageBoxButton.OK);
                    }
                    else
                    {
                        MessageBox.Show(e.FailedReasonText, "", MessageBoxButton.OK);
                    }
                    // need to refresh the parent's current entity state to OK.  right now everything is locked up.
                    CurrentEntityStateArgs args = new CurrentEntityStateArgs(EntityStateSwitch.None, null, null, (IBusinessObject)sender);
                    OnCurrentEntityStateChanged(this, args);
                }
                else
                {
                    if (_isInsertingNewRow == true)
                    {
                        if (sender is WcStoredProc_Bll)
                        {
                            WcStoredProc_Bll obj = sender as WcStoredProc_Bll;
                            // Move item to the top of the list.
                            ((WcStoredProc_List)navList.ItemsSource).Remove(obj);
                            ((WcStoredProc_List)navList.ItemsSource).Insert(0, obj);
                            _isInsertingNewRow = false;
                        }

                        else if (sender is WcStoredProcParam_Bll)
                        {
                            // this may not be possible for child grids.
                            //WcStoredProcParam_Bll obj = sender as WcStoredProcParam_Bll;
                            //// Move item to the top of the list.
                            //((WcStoredProcParam_List)navList.ItemsSource).Remove(obj);
                            //((WcStoredProcParam_List)navList.ItemsSource).Insert(0, obj);
                            _isInsertingNewRow = false;
                        }

                        else if (sender is WcStoredProcParamValueGroup_Bll)
                        {
                            // this may not be possible for child grids.
                            //WcStoredProcParamValueGroup_Bll obj = sender as WcStoredProcParamValueGroup_Bll;
                            //// Move item to the top of the list.
                            //((WcStoredProcParamValueGroup_List)navList.ItemsSource).Remove(obj);
                            //((WcStoredProcParamValueGroup_List)navList.ItemsSource).Insert(0, obj);
                            _isInsertingNewRow = false;
                        }

                        else if (sender is WcStoredProcColumn_Bll)
                        {
                            // this may not be possible for child grids.
                            //WcStoredProcColumn_Bll obj = sender as WcStoredProcColumn_Bll;
                            //// Move item to the top of the list.
                            //((WcStoredProcColumn_List)navList.ItemsSource).Remove(obj);
                            //((WcStoredProcColumn_List)navList.ItemsSource).Insert(0, obj);
                            _isInsertingNewRow = false;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RowDataObject_AsyncSaveWithResponseComplete", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RowDataObject_AsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
            }
        }

        void RowDataObject_AsyncSaveComplete(object sender, AsyncSaveCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RowDataObject_AsyncSaveComplete", IfxTraceCategory.Enter);

                if (sender is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = sender as WcStoredProc_Bll;
                    obj.AsyncSaveComplete -= new AsyncSaveCompleteEventHandler(RowDataObject_AsyncSaveComplete);
                    _rowsCallingSave.Remove(obj);
                }
                if (sender is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = sender as WcStoredProcParam_Bll;
                    obj.AsyncSaveComplete -= new AsyncSaveCompleteEventHandler(RowDataObject_AsyncSaveComplete);
                    _rowsCallingSave.Remove(obj);
                }

                if (sender is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = sender as WcStoredProcParamValueGroup_Bll;
                    obj.AsyncSaveComplete -= new AsyncSaveCompleteEventHandler(RowDataObject_AsyncSaveComplete);
                    _rowsCallingSave.Remove(obj);
                }

                if (sender is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = sender as WcStoredProcColumn_Bll;
                    obj.AsyncSaveComplete -= new AsyncSaveCompleteEventHandler(RowDataObject_AsyncSaveComplete);
                    _rowsCallingSave.Remove(obj);
                }

                if (e.Result != DataOperationResult.Success)
                {
                    //** need a more robust msg here.
                    if (e.FailedReasonText == null)
                    {
                        MessageBox.Show("There was a problem in saving this row.  If this continues, please contact support.", "", MessageBoxButton.OK);
                    }
                    else
                    {
                        MessageBox.Show(e.FailedReasonText, "", MessageBoxButton.OK);
                    }
                }            
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RowDataObject_AsyncSaveComplete", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RowDataObject_AsyncSaveComplete", IfxTraceCategory.Leave);
            }
        }

        void navList_RowDeleting(object sender, CancellableRowEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowDeleting", IfxTraceCategory.Enter);

                // Need custom code here such as a delete warning if child data will be deleted.
                // See the save method  navList_RowExitingEditMode  for an example of wireing up a callback if the delete failes.
            
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowDeleting", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowDeleting", IfxTraceCategory.Leave);
            }
        }

        void navList_SelectedRowsCollectionChanged(object sender, SelectionCollectionChangedEventArgs<SelectedRowsCollection> e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged", IfxTraceCategory.Enter);

                _hasSelectedRows = e.NewSelectedItems.Any();
                SetClipboardSetting();

                if (e.NewSelectedItems.Count == 1)
                {
                    if (e.NewSelectedItems[0].Data is WcStoredProc_Bll)
                    {
                        WcStoredProc_Bll obj = e.NewSelectedItems[0].Data as WcStoredProc_Bll;
                        _guidCurrentId = obj.Sp_Id;
                        RaiseNavigationListSelectedItemChanged(e.NewSelectedItems[0].Data);
                    }
                    else if (e.NewSelectedItems[0].Data is WcStoredProcParam_Bll)
                    {
                        WcStoredProcParam_Bll obj = e.NewSelectedItems[0].Data as WcStoredProcParam_Bll;
                        obj.ParentBusinessObject = e.NewSelectedItems[0].ParentRow.Data as WcStoredProc_Bll;
                        _guidParentId = obj.SpP_ID;
                        RaiseNavigationListSelectedItemChanged(e.NewSelectedItems[0].Data);
                    }
                    else if (e.NewSelectedItems[0].Data is WcStoredProcParamValueGroup_Bll)
                    {
                        WcStoredProcParamValueGroup_Bll obj = e.NewSelectedItems[0].Data as WcStoredProcParamValueGroup_Bll;
                        obj.ParentBusinessObject = e.NewSelectedItems[0].ParentRow.Data as WcStoredProc_Bll;
                        _guidParentId = obj.SpPVGrp_Id;
                        RaiseNavigationListSelectedItemChanged(e.NewSelectedItems[0].Data);
                    }
                    else if (e.NewSelectedItems[0].Data is WcStoredProcColumn_Bll)
                    {
                        WcStoredProcColumn_Bll obj = e.NewSelectedItems[0].Data as WcStoredProcColumn_Bll;
                        obj.ParentBusinessObject = e.NewSelectedItems[0].ParentRow.Data as WcStoredProc_Bll;
                        _guidParentId = obj.SpCl_Id;
                        RaiseNavigationListSelectedItemChanged(e.NewSelectedItems[0].Data);
                    }
                }
                else if (e.PreviouslySelectedItems.Count > 0)
                {
                    // clear ucProps from data
                }
                navList_SelectedRowsCollectionChanged_Custom(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Call this method to activate a new record (row) in the list or grid.</summary>
        public void ActivateNewRecord()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ActivateNewRecord", IfxTraceCategory.Enter);
                if (navList.Rows.Count > 0)
                {
                    navList.SelectionSettings.SelectedRows.Clear();
                    navList.SelectionSettings.SelectedRows.Add(navList.Rows[navList.Rows.Count - 1]);
                    navList.Rows[navList.Rows.Count - 1].IsSelected = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ActivateNewRecord", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ActivateNewRecord", IfxTraceCategory.Leave);
            }
        }

        void RichGrid_EditCanceled(object sender, EditCanceledArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RichGrid_EditCanceled", IfxTraceCategory.Enter);
                CancelGridRowEdit();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RichGrid_EditCanceled", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RichGrid_EditCanceled", IfxTraceCategory.Leave);
            }
        }

        #endregion GridRow Events


        #region GridCell Editor Events

        //  All code for these events in the ...ColumnCode.cs code file of this class

        #endregion GridCell Editor Events


        #region Grid Other Events and Event Support Methods

        void CancelGridRowEdit()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelGridRowEdit", IfxTraceCategory.Enter);
                if (_activeRow.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = _activeRow.Data as WcStoredProc_Bll;
                    obj.UnDo();
                    navList.ExitEditMode(true);
                }
                else if (_activeRow.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = _activeRow.Data as WcStoredProcParam_Bll;
                    obj.UnDo();
                    navList.ExitEditMode(true);
                }
                else if (_activeRow.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = _activeRow.Data as WcStoredProcParamValueGroup_Bll;
                    obj.UnDo();
                    navList.ExitEditMode(true);
                }
                else if (_activeRow.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = _activeRow.Data as WcStoredProcColumn_Bll;
                    obj.UnDo();
                    navList.ExitEditMode(true);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelGridRowEdit", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelGridRowEdit", IfxTraceCategory.Leave);
            }
        }

        void RaiseNavigationListSelectedItemChanged(object item)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseNavigationListSelectedItemChanged", IfxTraceCategory.Enter);
                NavigationListSelectedItemChangedArgs e = new NavigationListSelectedItemChangedArgs(item);
                NavigationListSelectedItemChangedEventHandler handler = NavigationListSelectedItemChanged;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseNavigationListSelectedItemChanged", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseNavigationListSelectedItemChanged", IfxTraceCategory.Leave);
            }
        }

        void HookGridCellBusnessObjectEvents(IBusinessObject obj, bool IsAdd)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "HookGridCellBusnessObjectEvents", IfxTraceCategory.Enter);
                if (IsAdd == true)
                {
                    obj.ControlValidStateChanged += new ControlValidStateChangedEventHandler(OnControlValidStateChanged);
                    obj.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                }
                else
                {
                    obj.ControlValidStateChanged -= new ControlValidStateChangedEventHandler(OnControlValidStateChanged);
                    obj.CurrentEntityStateChanged -= new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "HookGridCellBusnessObjectEvents", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "HookGridCellBusnessObjectEvents", IfxTraceCategory.Leave);
            }
        }



        private void SetClipboardSetting()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClipboardSetting", IfxTraceCategory.Enter);
                if (this._hasSelectedCells)
                {
                    navList.ClipboardSettings.CopyType = GridClipboardCopyType.SelectedCells;
                }

                // In this case, cell selection has greater priority than row selection
                if (!this._hasSelectedCells && this._hasSelectedRows)
                {
                    navList.ClipboardSettings.CopyType = GridClipboardCopyType.SelectedRows;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClipboardSetting", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetClipboardSetting", IfxTraceCategory.Leave);
            }
        }

        void navList_ClipboardCopying(object sender, ClipboardCopyingEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_ClipboardCopying", IfxTraceCategory.Enter);
                if (this._hasSelectedCells && this._hasSelectedRows)
                {
                    MessageBox.Show("The command cannot be used on multiple selections.");
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_ClipboardCopying", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_ClipboardCopying", IfxTraceCategory.Leave);
            }
        }

        void OnCurrentEntityStateChanged(object sender, CurrentEntityStateArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Enter);
                ConfigureToCurrentEntityState(e.State);
                CurrentEntityStateEventHandler handler = CurrentEntityStateChanged;
                CurrentEntityStateArgs args;
                if (e.ActivePropertiesControl == null)
                {
                    args = new CurrentEntityStateArgs(e.State, e.ActiveEntityControl, this, null, e.ActiveBusinessObject);
                }
                else
                {
                    args = e;
                }
                if (handler != null)
                {
                    handler(this, args);
                }

                if (e.ActiveBusinessObject is WcStoredProc_Bll)
                {
                    if (((WcStoredProc_Bll)e.ActiveBusinessObject).Sp_Id != _guidCurrentId)
                    {
                        // This means that the grid row is no longer in edit mode and or the user has navigated away
                        //    therefore, we need to unhook the events.
                        //    we cant do this until after the wcf save call has completed which is probably what just happened.
                        HookGridCellBusnessObjectEvents((WcStoredProc_Bll)e.ActiveBusinessObject, false);
                    }
                }
                else if (e.ActiveBusinessObject is WcStoredProcParam_Bll)
                {
                    if (((WcStoredProcParam_Bll)e.ActiveBusinessObject).SpP_ID != _guidCurrentId)
                    {
                        // This means that the grid row is no longer in edit mode and or the user has navigated away
                        //    therefore, we need to unhook the events.
                        //    we cant do this until after the wcf save call has completed which is probably what just happened.
                        HookGridCellBusnessObjectEvents((WcStoredProcParam_Bll)e.ActiveBusinessObject, false);
                    }
                }
                else if (e.ActiveBusinessObject is WcStoredProcParamValueGroup_Bll)
                {
                    if (((WcStoredProcParamValueGroup_Bll)e.ActiveBusinessObject).SpPVGrp_Id != _guidCurrentId)
                    {
                        // This means that the grid row is no longer in edit mode and or the user has navigated away
                        //    therefore, we need to unhook the events.
                        //    we cant do this until after the wcf save call has completed which is probably what just happened.
                        HookGridCellBusnessObjectEvents((WcStoredProcParamValueGroup_Bll)e.ActiveBusinessObject, false);
                    }
                }
                else if (e.ActiveBusinessObject is WcStoredProcColumn_Bll)
                {
                    if (((WcStoredProcColumn_Bll)e.ActiveBusinessObject).SpCl_Id != _guidCurrentId)
                    {
                        // This means that the grid row is no longer in edit mode and or the user has navigated away
                        //    therefore, we need to unhook the events.
                        //    we cant do this until after the wcf save call has completed which is probably what just happened.
                        HookGridCellBusnessObjectEvents((WcStoredProcColumn_Bll)e.ActiveBusinessObject, false);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Leave);
            }
        }

        #endregion Grid Other Events and Event Support Methods




        #region Column Group Events
        
        private void rdbSystemColumnGroups_Checked(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "rdbSystemColumnGroups_Checked", IfxTraceCategory.Enter);

                if (_colGrpMngr != null)
                {
                    _colGrpMngr.LoadComboGroupsAndColumnLists(ColumnGroupManager.CurrentListType.System);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "rdbSystemColumnGroups_Checked", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "rdbSystemColumnGroups_Checked", IfxTraceCategory.Leave);
            }
        }

        private void rdbCustomColumnGroups_Checked(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "rdbCustomColumnGroups_Checked", IfxTraceCategory.Enter);

                if (_colGrpMngr != null)
                {
                    _colGrpMngr.LoadComboGroupsAndColumnLists(ColumnGroupManager.CurrentListType.UserDefined);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "rdbCustomColumnGroups_Checked", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "rdbCustomColumnGroups_Checked", IfxTraceCategory.Leave);
            }
        }
        
        private void btnEditColumnGroups_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnEditColumnGroups_Click", IfxTraceCategory.Enter);

                vGridColumnGroupManagerService grdColSrvc = new vGridColumnGroupManagerService(navList, GridId, (int)2, Credentials.UserId);
                grdColSrvc.vGridColumnGroupManagerServiceClosing += grdColSrvc_vGridColumnGroupManagerServiceClosing;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnEditColumnGroups_Click", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnEditColumnGroups_Click", IfxTraceCategory.Leave);
            }
        }

        void grdColSrvc_vGridColumnGroupManagerServiceClosing(object sender, vGridColumnGroupManagerServiceClosingArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "grdColSrvc_vGridColumnGroupManagerServiceClosing", IfxTraceCategory.Enter);


                if (rdbSystemColumnGroups.IsChecked == true)
                {
                    if (_colGrpMngr != null)
                    {
                        _colGrpMngr.LoadComboGroupsAndColumnLists(ColumnGroupManager.CurrentListType.System);
                    }
                }
                else
                {
                    if (_colGrpMngr != null)
                    {
                        _colGrpMngr.LoadComboGroupsAndColumnLists(ColumnGroupManager.CurrentListType.UserDefined);
                    }
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "grdColSrvc_vGridColumnGroupManagerServiceClosing", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "grdColSrvc_vGridColumnGroupManagerServiceClosing", IfxTraceCategory.Leave);
            }
        }

        #endregion Column Group Events





        #region Auto Expand All Rows


        private bool AutoExpanding = false;

        void navList_RowExpansionChanged(object sender, RowExpansionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowExpansionChanged", IfxTraceCategory.Enter);
                if (!AutoExpanding)
                    {
                        // expand child rows only if parent row is expanded
                        if (e.Row.IsExpanded)
                        {
                        if (e.Row.Data is WcStoredProc_Bll)
                        {
                            WcStoredProc_Bll obj = e.Row.Data as WcStoredProc_Bll;
                            if (obj != null)
                            {
                                obj.Get_Children_WcStoredProcParam();
                                obj.Get_Children_WcStoredProcParamValueGroup();
                                obj.Get_Children_WcStoredProcColumn();
                            }
                        }

                            AutoExpanding = true;

                            if (e.Row.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                            {
                                AutoExpandAllChildRows((Row)e.Row);
                            }

                            if (e.Row.RowType == Infragistics.Controls.Grids.RowType.ColumnLayoutHeaderRow)
                            {
                                AutoExpandAllChildBands((ChildBand)e.Row);
                            }
                            AutoExpanding = false;
                        }
                    }
                }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowExpansionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowExpansionChanged", IfxTraceCategory.Leave);
            }
        }


        void ExpandAllTopLevelRows()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExpandAllTopLevelRows", IfxTraceCategory.Enter);

                foreach (var rw in navList.Rows)
                {
                    if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        if (rw.HasChildren)
                        {
                            rw.IsExpanded = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExpandAllTopLevelRows Overload 5", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExpandAllTopLevelRows Overload 5", IfxTraceCategory.Leave);
            }

        }

        private void xamGrid_RowExpansionChanged(object sender, RowExpansionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "xamGrid_RowExpansionChanged", IfxTraceCategory.Enter);
                if (!AutoExpanding)
                {
                    // expand child rows only if parent row is expanded
                    if (e.Row.IsExpanded)
                    {
                        AutoExpanding = true;

                        if (e.Row.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                            AutoExpandAllChildRows((Row)e.Row);

                        if (e.Row.RowType == Infragistics.Controls.Grids.RowType.ColumnLayoutHeaderRow)
                            AutoExpandAllChildBands((ChildBand)e.Row);

                        AutoExpanding = false;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "xamGrid_RowExpansionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "xamGrid_RowExpansionChanged", IfxTraceCategory.Leave);
            }
        }

        private void AutoExpandAllChildRows(Row parentRow)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AutoExpandAllChildRows", IfxTraceCategory.Enter);

                foreach (ChildBand cb in parentRow.ChildBands)
                {
                    cb.IsExpanded = true;
                    if (cb.HasChildren)
                    {
                        AutoExpandAllChildBands(cb);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AutoExpandAllChildRows", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AutoExpandAllChildRows", IfxTraceCategory.Leave);
            }
        }

        private void AutoExpandAllChildBands(ChildBand parentBand)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AutoExpandAllChildBands", IfxTraceCategory.Enter);
                foreach (Row r in parentBand.Rows)
                {
                    r.IsExpanded = true;
                    if (r.HasChildren)
                    {
                        AutoExpandAllChildRows(r);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AutoExpandAllChildBands", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AutoExpandAllChildBands", IfxTraceCategory.Leave);
            }
        }

        #endregion Auto Expand All Rows



        #endregion Events


        #region Properties

        public string ParentType
        {
            get { return _parentType; }
            set { _parentType = value; }
        }

        public Guid? GuidParentId
        {
            get { return _guidParentId; }
            set { _guidParentId = value; }
        }

        public Int64? LongParentId
        {
            get { return _longParentId; }
            set { _longParentId = value; }
        }

        public Int32? IntParentId
        {
            get { return _intParentId; }
            set { _intParentId = value; }
        }

        public Int16? ShortParentId
        {
            get { return _shortParentId; }
            set { _shortParentId = value; }
        }

        public Byte? ByteParentId
        {
            get { return _byteParentId; }
            set { _byteParentId = value; }
        }

        public object ObjectParentId
        {
            get { return _oParentId; }
            set { _oParentId = value; }
        }

        
        public Guid? GiudCurrentId
        {
            get { return _guidCurrentId; }
            set { _guidCurrentId = value; }
        }

        public Int64? LongCurrentId
        {
            get { return _longCurrentId; }
            set { _longCurrentId = value; }
        }

        public Int32? IntCurrentId
        {
            get { return _intCurrentId; }
            set { _intCurrentId = value; }
        }

        public Int16? ShortCurrentId
        {
            get { return _shortCurrentId; }
            set { _shortCurrentId = value; }
        }

        public Byte? ByteCurrentId
        {
            get { return _byteCurrentId; }
            set { _byteCurrentId = value; }
        }

        public bool IsSplitSreenMode
        {
            get { return _isSplitSreenMode; }
            set
            {
                if (_isSplitSreenMode != value)
                {
                    _isSplitSreenMode = value;
                }
                else
                {
                    _isSplitSreenMode = value;
                }
            }
        }

        public bool IsGridReadOnly
        {
            get
            {
                if (_secuitySettingIsReadOnly || _isSplitSreenMode || _isGridReadOnly)
                //if (_secuitySettingIsReadOnly || _isGridReadOnly)   // put this back into use 2012-10-08.  this allows us to be in split screen mode and still be able to add new rows - for  cases when split screen mode is used to use a child grid in the right side such as category / sub category
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        public Guid GridId
        {
            get { return _gridId; }
            //set { _gridId = value; }
        }



        #endregion Properties


        #region Methods

        void ConfigureToCurrentEntityState(EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState", IfxTraceCategory.Enter);
                switch (state)
                {
                    case EntityStateSwitch.None:
                        EnableGridToolBar(true);
                        break;
                    case EntityStateSwitch.NewInvalidNotDirty:
                        EnableGridToolBar(true);
                        break;
                    case EntityStateSwitch.NewValidNotDirty:
                        EnableGridToolBar(true);
                        break;
                    case EntityStateSwitch.NewValidDirty:
                        EnableGridToolBar(false);
                        break;
                    case EntityStateSwitch.NewInvalidDirty:
                        EnableGridToolBar(false);
                        break;
                    case EntityStateSwitch.ExistingInvalidDirty:
                        EnableGridToolBar(false);
                        break;
                    case EntityStateSwitch.ExistingValidDirty:
                        EnableGridToolBar(false);
                        break;
                    case EntityStateSwitch.ExistingValidNotDirty:
                        EnableGridToolBar(true);
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState", IfxTraceCategory.Leave);
            }
        }

        public void EnableGridToolBar(bool isEnabled)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EnableGridToolBar", IfxTraceCategory.Enter);

                xamGridMenu.IsEnabled = isEnabled;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EnableGridToolBar", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EnableGridToolBar", IfxTraceCategory.Leave);
            }
        }

        void SetGridCellValidationAppeance(IvControlsValidation ctl , IBusinessObject obj, string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetGridCellValidationAppeance", IfxTraceCategory.Enter);
                if (_activeRow == null) { return; }
                XamGridCellValidationHelper.SetXamGridEditControlValidStateAppearance(ctl, obj, propertyName);
                SetBrokenRuleToolTip((Control)ctl, obj, propertyName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetGridCellValidationAppeance", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetGridCellValidationAppeance", IfxTraceCategory.Leave);
            }
        }

        void SetBrokenRuleToolTip(Control ctl, IBusinessObject obj)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetBrokenRuleToolTip", IfxTraceCategory.Enter);
                SetBrokenRuleToolTip(ctl, obj, ctl.Name);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetBrokenRuleToolTip", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetBrokenRuleToolTip", IfxTraceCategory.Leave);
            }
        }

        void SetBrokenRuleToolTip(Control ctl, IBusinessObject obj, string controName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetBrokenRuleToolTip", IfxTraceCategory.Enter);
                List<vRuleItem> rules = obj.GetBrokenRulesForProperty(controName);
                if (rules == null)
                {
                    vToolTipService.SetToolTip(ctl, null);
                }
                else
                {
                    vToolTip tt = vToolTipHelper.GetTooltipInstanceForValidationRuleToolTip();
                    TooltipBrokenRuleContent uc = new TooltipBrokenRuleContent(rules);
                    tt.Content = uc;
                    // These lines must go here in this order for the tooltip to look right
                    tt.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
                    tt.PlacementTarget = ctl;
                    vToolTipService.SetToolTip(ctl, tt);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetBrokenRuleToolTip", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetBrokenRuleToolTip", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Used to update a list in a list column. In other words, a grid or a list may use
        /// a list control such as a ComboBox in a grid cell. When the ComboBox list has been
        /// edited from an outside source, this method is called so the ComboBox’s data source can
        /// be refreshed.
        /// </summary>
        /// <param name="columnName">
        /// Name of the column which the ComboBox is bound and tells us which ComboBox to
        /// update.
        /// </param>
        public void UpdateListColumnList(string columnName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UpdateListColumnList", IfxTraceCategory.Enter);
                
                // need code here to update the xamcombo list
                //navList_RowSource();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UpdateListColumnList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UpdateListColumnList", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Adds text to the ‘new record template’. When a user wants to add or edit an
        ///     item in a ComboBox dropdown list, this control might be used as the add/edit screen
        ///     for this function. If the user types new text into the ComboBox and then opens a
        ///     screen containing this control, that new text can be placed into this ‘new record
        ///     template’ as a head start in creating the new item.</para>
        /// 	<para>However, here are issues with this process; therefore, this method is
        ///     currently not being implemented. When the user enters new text into the ComboBox,
        ///     the ComboBox will most likely have a selected Id value which will also be passed
        ///     into this control. Therefore, we have no way of knowing if the user is entering a
        ///     new item, or editing an existing one. This needs to be worked out.</para>
        /// </summary>
        void AddTextToAddRecordTemplate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AddTextToAddRecordTemplate", IfxTraceCategory.Enter);
                //DataRecord templateAddRecord = this.navList.RecordManager.CurrentAddRecord;
                //templateAddRecord.Cells[1].IsActive = true;
                //this.navList.ExecuteCommand(DataPresenterCommands.StartEditMode);
                //templateAddRecord.Cells[1].Value = _newText;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AddTextToAddRecordTemplate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AddTextToAddRecordTemplate", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// This can be useful for conditions when there is already a new row in the list,
        /// but a different row is the current active row. When the user clicks on the ‘New’ button
        /// to create a new WcStoredProc, rather than creating a 2nd ‘new row’, this method is called
        /// first. If there’s already a new row in the list, it’s made to be the active row;
        /// otherwise, a new row will be created.
        /// </summary>
        public bool FindNewRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FindNewRow", IfxTraceCategory.Enter);
                foreach (var item in navList.Rows)
                {
                    WcStoredProc_Bll obj = item.Data as WcStoredProc_Bll;
                    if (obj != null)
                    {
                        if (obj.State.IsNew() == true)
                        {
                            item.IsSelected = true;

                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FindNewRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FindNewRow", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// This can be useful for conditions when there is already a new row in the list,
        /// but a different row is the current active row. When the user clicks on the ‘New’ button
        /// to create a new WcStoredProc, rather than creating a 2nd ‘new row’, this method is called
        /// first. If there’s already a new row in the list, it’s made to be the active row;
        /// otherwise, a new row will be created.
        /// </summary>
        public void FindRowById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FindRowById", IfxTraceCategory.Enter);           
                //foreach (var item in navList.Rows)
                //{
                //    WcStoredProc_Bll obj = item.Data as WcStoredProc_Bll;
                //    if (obj.Sp_Id == id)
                //    {
                //        item.IsSelected = true;
                //    }
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FindRowById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FindRowById", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Used to update a list in a list column. In other words, a grid or a list may use
        /// a list control such as a ComboBox in a grid cell. When the ComboBox list has been
        /// edited from an outside source, this method is called so the ComboBox’s data source can
        /// be refreshed.
        /// </summary>
        /// <param name="columnName">
        /// Name of the column which the ComboBox is bound and tells us which ComboBox to
        /// update.
        /// </param>
        public void FindRowById(int id)
        { }

        public void SelectRowById(Int64 id)
        {
            throw new NotImplementedException();
        }

        public void SelectRowById(Int32 id)
        {
            throw new NotImplementedException();
        }

        public void SelectRowById(Int16 id)
        {
            throw new NotImplementedException();
        }

        public void SelectRowById(Byte id)
        {
            throw new NotImplementedException();
        }


        public void SelectRowById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SelectRowById", IfxTraceCategory.Enter);
                foreach(Row rw in navList.Rows)
                {

                    if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        if (((WcStoredProc_Bll)rw.Data).Sp_Id == id)
                        {
                            //rw.IsActive = true;
                            rw.IsSelected = true;
                            _guidCurrentId = id;
                            //RaiseNavigationListSelectedItemChanged((WcStoredProc_Bll)rw.Data);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SelectRowById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SelectRowById", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Provides a default width for this control and can be used by controls that that
        /// consume controls which use the ICustomListControl interface. This way the parent
        /// control doesn’t need to have specific knowledge of this type, but can format it’s
        /// layout to the appropriate size using this width value.
        /// </summary>
        public int GetListWidth()
        {
            return _listWidth;
        }

        /// <summary>
        /// Provides a default height for this control and can be used by controls that that
        /// consume controls which use the ICustomListControl interface. This way the parent
        /// control doesn’t need to have specific knowledge of this type, but can format it’s
        /// layout to the appropriate size using this height value.
        /// </summary>
        public int GetListHeight()
        {
            return _listHeight;
        }

        /// <summary>
        /// Get the Id of the row that was last updated. This method is used when the primary
        /// key is an int data type. There is also a Guid version of this method allowing the
        /// interface to be extendable to both data types.
        /// </summary>
        public int? GetLastUpdatedId_Int()
        {
            return null;
        }

        /// <summary>
        /// 	<para>Get the Id of the row that was last updated. This method is used when the
        ///     primary key is an Guid data type. There is also a int version of this method
        ///     allowing the interface to be extendable to both data types.</para>
        /// </summary>
        public Guid? GetLastUpdatedId_Guid()
        {
            return null;
        }

        public void CancelAddEdit()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelAddEdit", IfxTraceCategory.Enter);
                //NavList_ItemSource.CancelNew(NavList_ItemSource.Count - 1);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelAddEdit", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelAddEdit", IfxTraceCategory.Leave);
            }
        }


        public void CancelNewRow(WcStoredProc_Bll item)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelNewRow", IfxTraceCategory.Enter);
                // this is used when the new row is being managed by ucProps and using the esc key is not an option.
                NavList_ItemSource.Remove(item);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelNewRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelNewRow", IfxTraceCategory.Leave);
            }
        }

        void ExportToExcel()
        {
            ExcelExportHelper.ExportGridToExcel(navList);
        }



        public void ClearFilterCells(XamGrid grid,  RowCollection rows)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearFilterCells", IfxTraceCategory.Enter);

                grid.FilteringSettings.RowFiltersCollection.Clear();
                if (rows.Count == 0) { return; }
            
                if (rows != null)
                {
                    if (rows[0].RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        foreach (Cell cell in ((RowsManager)rows[0].Manager).FilterRowBottom.Cells)
                        {
                            (cell as FilterRowCell).FilterCellValue = null;
                        }

                        foreach (Cell cell in ((RowsManager)rows[0].Manager).FilterRowTop.Cells)
                        {
                            (cell as FilterRowCell).FilterCellValue = null;
                        }

                        foreach (Row dr in rows)
                        {
                            if (dr.HasChildren)
                            {
                                foreach (ChildBand childBand in dr.ChildBands)
                                {
                                    childBand.RowFiltersCollection.Clear();
                                }
                                ClearFilterCells(grid, dr.ChildBands[0].Rows);
                            }
                        }
                    }
                    else
                    {
                        if (rows[0].GetType() == typeof(GroupByRow))
                        {
                            foreach (GroupByRow gbr in rows)
                            {
                                gbr.RowFiltersCollection.Clear();
                                ClearFilterCells(grid, gbr.Rows);

                                foreach (Cell cell in ((RowsManager)gbr.Rows[0].Manager).FilterRowTop.Cells)
                                {
                                    (cell as FilterRowCell).FilterCellValue = null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearFilterCells", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearFilterCells", IfxTraceCategory.Leave);
            }
        }




        #endregion Methods


        #region Fetch Data

        void NavListRefreshFromObjectArray(object[] data)
        {
            // This method is usually a followup from calling SetStateFromParent and after the web service has returned.
            // Its also ALWAYS called by the web service reply when returning a new list for the grid.
            // Now reset the list and see to it that any other screens as reset accordingly.
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", IfxTraceCategory.Enter);
                if (data != null)
                {
                    NavList_ItemSource = null;
                    NavList_ItemSource = new WcStoredProc_List();
                    // Sometimes this is still null because it hasn't loaded in the UI yet
                    if (NavList_ItemSource != null)
                    {
                        NavList_ItemSource.ReplaceList(data);
                    }
                }
                else
                {
                    if (NavList_ItemSource != null)
                    {
                        NavList_ItemSource.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", IfxTraceCategory.Leave);
            }
        }


        void WcStoredProc_GetByIdCompleted(object sender, WcStoredProc_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetByIdCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                NavListRefreshFromObjectArray(array);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetByIdCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        void WcStoredProc_GetListByFKCompleted(object sender, WcStoredProc_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                NavListRefreshFromObjectArray(array);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetListByFKCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        void WcStoredProc_GetAllCompleted(object sender, WcStoredProc_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetAllCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                NavListRefreshFromObjectArray(array);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetAllCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }


        #endregion Fetch Data


        #region Menus


        private void XamMenuItem_Click(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click", IfxTraceCategory.Enter);

                XamMenuItem mnu = (XamMenuItem)sender;
                switch (mnu.Name)
                {
                    case "mnuSplitScreen":
                        _isSplitSreenMode = !_isSplitSreenMode;
                        navList_SplitScreenMode();
                        break;
                    //case "mnuNewRowOnTop":
                        //if (_gridAllowAddRowMode == AddNewRowLocation.Top)
                        //{
                        //    _gridAllowAddRowMode = AddNewRowLocation.Bottom;
                        //}
                        //else
                        //{
                        //    _gridAllowAddRowMode = AddNewRowLocation.Top;
                        //}
                        //navList_AllowAddRowMode();
                        //break;
                    case "mnuColumnChooser":
                        navList.ShowColumnChooser();
                        break;
                    case "mnuDisplyHideColumnButton":
                        navList_DisplyHideColumnButton();
                        break;
                    case "mnuColumnGrouping":
                        navList_ColumnGroupingMode();
                        break;
                    case "mnuColumnSummary":
                        navList_ColumnSummaryMode();
                        break;
                    case "mnuFilterNone":
                        _FilterType = FilterUIType.None;
                        navList_FilterMode();
                        break;
                    case "mnuFilterMenu":
                        _FilterType = FilterUIType.FilterMenu;
                        navList_FilterMode();
                        break;
                    case "mnuFilterTop":
                        _FilterType = FilterUIType.FilterRowTop;
                        navList_FilterMode();
                        break;
                    case "mnuFilterBottom":
                        _FilterType = FilterUIType.FilterRowBottom;
                        navList_FilterMode();
                        break;
                    case "mnuFixedColumn_UsePin":
                        //_allowFixedColumns = FixedColumnType.Disabled;
                        navList_FixedColumnMode();
                        break;
                    //case "mnuFixedColumn_Indicator":
                    //    _allowFixedColumns = FixedColumnType.Indicator;
                    //    navList_FixedColumnMode();
                    //    break;
                    case "mnuFixedColumn_UseDropArea":
                        //_allowFixedColumns = FixedColumnType.DropArea;
                        navList_FixedColumnMode();
                        break;
                    //case "mnuFixedColumn_Both":
                    //    _allowFixedColumns = FixedColumnType.Both;
                    //    navList_FixedColumnMode();
                    //    break;
                    case "mnuFixedColumn_Indicator_Left":
                        _fixedColumnIndicatorDirection = FixedIndicatorDirection.Left;
                        navList_FixedColumnIndicatorDirection();
                        break;
                    case "mnuFixedColumn_Indicator_Right":
                        _fixedColumnIndicatorDirection = FixedIndicatorDirection.Right;
                        navList_FixedColumnIndicatorDirection();
                        break;
                    case "mnuFixedColumn_DropArea_Left":
                        _fixedColumnDropAreaLocation = FixedDropAreaLocation.Left;
                        navList_FixedColumnDropAreaLocation();
                        break;
                    case "mnuFixedColumn_DropArea_Right":
                        _fixedColumnDropAreaLocation = FixedDropAreaLocation.Right;
                        navList_FixedColumnDropAreaLocation();
                        break;
                    case "mnuFixedColumn_DropArea_Both":
                        _fixedColumnDropAreaLocation = FixedDropAreaLocation.Both;
                        navList_FixedColumnDropAreaLocation();
                        break;
                    case "mnuRichGrid":
                        navList_ShowHideRichGrid();
                        break;
                    case "mnuExcelExport":
                        ExportToExcel();
                        break;
                    case "mnuClearFilters":
                        ClearFilterCells(navList, navList.Rows);
                        break;
                    case "mnuCollapsAllParentNavGrids":
                        ToggleParentEntityExpantion();
                        break;
                }
                XamMenuItem_Click_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click", IfxTraceCategory.Leave);
            }
        }


        #region Menu Support Methods

        #endregion Menu Support Methods

        #endregion Menus


        #region Collapse - Expand Parents

        private bool _CollapsAllParents = false;

        void ToggleParentEntityExpantion()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcToggleParentEntityExpantion", IfxTraceCategory.Enter);

                if (_CollapsAllParents == false)
                {
                    Raise_ParentNavGridExpansionChanged(CollapseExpandParentEntityOptions.Collapse);
                    mnuCollapsAllParentNavGrids.Icon = vVelocityImageProvider.LoadImage(vVelocityImageProvider.GetMenuVelocityImagePath(vVelocityImageProvider.MenuImage.ExpandRight16));

                    _CollapsAllParents = true;
                }
                else
                {
                    Raise_ParentNavGridExpansionChanged(CollapseExpandParentEntityOptions.Expand);
                    mnuCollapsAllParentNavGrids.Icon = vVelocityImageProvider.LoadImage(vVelocityImageProvider.GetMenuVelocityImagePath(vVelocityImageProvider.MenuImage.ExpandLeft16));
                    _CollapsAllParents = false;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcToggleParentEntityExpantion", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcToggleParentEntityExpantion", IfxTraceCategory.Leave);
            }
        }

        public void Reset_mnuCollapsAllParentNavGrids()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcReset_mnuCollapsAllParentNavGrids", IfxTraceCategory.Enter);

                mnuCollapsAllParentNavGrids.Icon = vVelocityImageProvider.LoadImage(vVelocityImageProvider.GetMenuVelocityImagePath(vVelocityImageProvider.MenuImage.ExpandLeft16));
                _CollapsAllParents = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcReset_mnuCollapsAllParentNavGrids", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcReset_mnuCollapsAllParentNavGrids", IfxTraceCategory.Leave);
            }
        }

        void Raise_ParentNavGridExpansionChanged(CollapseExpandParentEntityOptions option)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcRaise_ParentNavGridExpansionChanged", IfxTraceCategory.Enter);

                CollapseExpandAllParentNavGridsEventHandler handler = ChangeParentNavGridExpansion;
                if (handler != null)
                {
                    handler(this, new CollapseExpandAllParentNavGridsArgs(option));
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcRaise_ParentNavGridExpansionChanged", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcRaise_ParentNavGridExpansionChanged", IfxTraceCategory.Leave);
            }
        }

#endregion Collapse - Expand Parents

    }
}


