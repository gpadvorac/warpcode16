using System;
using System.Windows;
using Ifx.SL;
using EntityBll.SL;
using TypeServices;
using Infragistics.Controls.Grids;
using vUICommon;
using Velocity.SL;
using vControls;
using Infragistics.Controls.Editors;
using System.Windows.Controls;
using ApplicationTypeServices;
using Infragistics.Controls.Menus;
using ProxyWrapper;
using vComboDataTypes;
using vDialogControl;
using vFileStorageDialog;

// Gen Timestamp:  12/23/2017 1:27:08 AM

namespace UIControls
{
    public partial class ucWcStoredProcList
    {

        #region Initialize Variables

        bool _mnuChangeFilterType_AllowVisibility = true;
        bool _mnuClearFilters_AllowVisibility = true;  
        bool _mnuGridTools_AllowVisibility = true;
        bool _mnuRichGrid_AllowVisibility = true;
        bool _mnuSplitScreen_AllowVisibility = true;
        bool _isAllowNewRow = true;
        bool _mnuExcelExport_AllowVisibility = true;
        bool _gridDataSourceCombo_AllowVisibility = true;
        bool _menuGridRow_AllowVisibility = true;  // the area where the grid menu is located


        //ProxyWrapper.WcStoredProcService_ProxyWrapper _wcStoredProcProxy = null;

        #endregion Initialize Variables


        #region Constructor

        void CustomConstructionCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", IfxTraceCategory.Enter);

                _wcStoredProcProxy = new ProxyWrapper.WcStoredProcService_ProxyWrapper();
                _wcStoredProcProxy.WcStoredProc_GetByIdCompleted += WcStoredProc_GetByIdCompleted;
                _wcStoredProcProxy.WcStoredProc_GetListByFKCompleted += WcStoredProc_GetListByFKCompleted;
                _wcStoredProcProxy.WcStoredProc_GetAllCompleted += WcStoredProc_GetAllCompleted;

                _wcStoredProcProxy.GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted += _wcStoredProcProxy_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted;
                InitializeViewLogItem();

                cmbConnString_DataSource();
                cmbConnString.vXamComboEditorSelectionChanged += CmbConnString_vXamComboEditorSelectionChanged;


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructor


        #region Security

        UserSecurityContext _userContext = new UserSecurityContext();
        //***  Add the ACTUAL Guid below, and then delete this comment
        private Guid _ancestorSecurityId = Guid.NewGuid();
        ControlCache cCache;
        EntityCache eCache = null;
        bool defaultEntityOperationPermission = true;
        bool _secuitySettingIsReadOnly = false;
        //***  Add the ACTUAL Guid below, and then delete this comment
        Guid _controlId = Guid.NewGuid();
        bool _isDeleteActionAllowed = false;
        Guid _deleteActionId = Guid.NewGuid();  // Hard code the actual guid from the security tool


        public void SetSecurityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Enter);

                _userContext.LoadArtifactPermissions(_ancestorSecurityId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Leave);
            }
        }

        void UserSecurityContext_SecurityArtifactsRetrieved(object sender, SecurityArtifactsRetrievedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Enter);
                SecurityCache.AddControlCacheForAncestor(e.ArtifactAncestorData);
                cCache = SecurityCache.GetControlGroupById(e.ArtifactAncestorData.Id);

                UiControlStateSetting setting = cCache.GetControlById(_controlId).Setting;
                if (setting == UiControlStateSetting.Enable)
                {
                    _secuitySettingIsReadOnly = false;
                }
                else
                {
                    _secuitySettingIsReadOnly = true;
                    navList_SplitScreenMode();
                }

                //_isDeleteActionAllowed = SecurityCache.IsEnabled(_deleteActionId);
                //if (_isDeleteActionAllowed)
                //{
                //    navList.DeleteKeyAction = DeleteKeyAction.DeleteSelectedRows;
                //}

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Leave);
            }
        }

        #endregion Security

        #region Method Extentions For Custom Code


        public void Set_vXamComboColumn_ItemSourcesWithParams()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", IfxTraceCategory.Enter);
                
                //_leaseProxy.Begin_GetLeaseAltNumberSource_ComboItemList((Guid)_prj_Id);

//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", IfxTraceCategory.Leave);
//            }
        }

        #region Combo ItemsSource for Non-Static Lists




        #endregion region Combo ItemsSource for Non-Static Lists



        void vXamComboColumn_SelectionChanged_CustomCode(object sender, vXamComboColumnSelectionChangedEventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void vXamMultiColumnComboColumn_SelectionChanged_CustomCode(object sender, vXamMultiColumnComboColumnSelectionChangedEventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Leave);
//            }
            }


        void vDatePickerColumn_TextChanged_CustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void XamColorPicker_SelectedColorChanged_CustomCode(XamColorPicker ctl, SelectedColorChangedEventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void vCheckColumn_CheckedChanged_CustomCode(vCheckBox ctl, IBusinessObject obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void vTextColumn_TextChanged_CustomCode(TextBox ctl, IBusinessObject obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void vDecimalColumn_TextChanged_CustomCode(TextBox ctl, IBusinessObject obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void navList_RowEnteredEditMode_Custom(object sender, EditingRowEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", IfxTraceCategory.Enter);
                WcStoredProc_Bll obj = e.Row.Data as WcStoredProc_Bll;
                if (obj == null) { return; }

                        // For multi parets
////                if (_guidParentId == null)
////                {
////                    throw new Exception(_as + "." + _cn + ".navList_RowEnteredEditMode:  _guidParentId  was null.  _guidParentId must be used for the FK.");
////                }
//                //obj.StandingFK = (Guid)_prj_Id;    // added this line
//
//                //switch (_parentType)
//                //{
//                //    case "ucWell":
//                //        // this might not ever get his.
//                //        break;
//                //    case "ucWellDt":
//                //        obj.Current.WlD_Id_noevents = _guidParentId;
//                //        break;
//                //    case "ucContractDt":
//                //        obj.Current.CtD_Id_noevents = _guidParentId;
//                //        break;
//                //}

                //  Or For a Fixed Single Parent
                _isRowInEditMode = true;
                if (obj.State.IsNew() == true)
                {
                    SetNewRowValidation();
                }
                obj.Current.Sp_ApVrsn_Id_noevents = _guidParentId;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", IfxTraceCategory.Leave);
            }
        }


        void navList_SelectedRowsCollectionChanged_Custom(object sender, SelectionCollectionChangedEventArgs<SelectedRowsCollection> e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", IfxTraceCategory.Leave);
//            }
        }




        void ConfigureColumnHeaderTooltips_CustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void XamMenuItem_Click_CustomCode(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click_CustomCode", IfxTraceCategory.Enter);

                XamMenuItem mnu = (XamMenuItem)sender;
                switch (mnu.Name)
                {
                    case "mnuShowDbTables":
                        ShowDatabaseTables();
                        break;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click_CustomCode", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click_CustomCode", IfxTraceCategory.Leave);
            }
        }



        #endregion Method Extentions For Custom Code


        #region CodeGen Methods for modification


        public void SetStateFromParent(object ancestorId, string parentType, int? intParentId, Guid? guidParentId, object objParentId, int? intCurrentId, Guid? guidCurrentId, object objCurrentId, IBusinessObject currentBusinessObject, IEntity_ValuesMngr[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 7
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Enter);
                _intParentId = intParentId;
                _guidParentId = guidParentId;
                //_guidParentId = objParentId as Guid?;
                
                _intCurrentId = intCurrentId;
                _guidCurrentId = guidCurrentId;
                //_guidCurrentId = objCurrentId as Guid?;

                _parentType = parentType;
                _newText = newText;
                if (null != list)
                {
                    NavList_ItemSource.ReplaceList(list);
                }    
                else
                {
                    if (_guidParentId == null)
                    {
                        NavListRefreshFromObjectArray(null);
                        navList.IsEnabled = false;
                    }
                    else
                    {
                        //  Add code here for multiple parent types
                        navList.IsEnabled = true;
                        _wcStoredProcProxy.Begin_WcStoredProc_GetListByFK((Guid)_guidParentId);
                    }
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Leave);
            }
        }



        void navList_RowEnteredEditMode(object sender, EditingRowEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode", IfxTraceCategory.Enter);
                if (e.Row.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = e.Row.Data as WcStoredProc_Bll;
                    _isRowInEditMode = true;
                    if (obj.State.IsNew() == true)
                    {
                        obj.StandingFK = (Guid)GuidParentId;
                        SetNewRowValidation();
                    }
                }
                else if (e.Row.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = e.Row.Data as WcStoredProcParam_Bll;
                    WcStoredProc_Bll parent = e.Row.ParentRow.Data as WcStoredProc_Bll;
                    _isRowInEditMode = true;
                    if (obj.State.IsNew() == true)
                    {
                        if (parent.Sp_Id != null)
                        {
                            obj.StandingFK = (Guid)parent.Sp_Id;
                        }
                        SetNewRowValidation();
                    }
                }
                else if (e.Row.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = e.Row.Data as WcStoredProcColumn_Bll;
                    WcStoredProc_Bll parent = e.Row.ParentRow.Data as WcStoredProc_Bll;
                    _isRowInEditMode = true;
                    if (obj.State.IsNew() == true)
                    {
                        if (parent.Sp_Id != null)
                        {
                            obj.StandingFK = (Guid)parent.Sp_Id;
                        }
                        SetNewRowValidation();
                    }
                }
                else if (e.Row.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = e.Row.Data as WcStoredProcParamValueGroup_Bll;
                    WcStoredProc_Bll parent = e.Row.ParentRow.Data as WcStoredProc_Bll;
                    _isRowInEditMode = true;
                    if (obj.State.IsNew() == true)
                    {
                        if (parent.Sp_Id != null)
                        {
                            obj.StandingFK = (Guid)parent.Sp_Id;
                        }
                        SetNewRowValidation();
                    }
                }
                navList_RowEnteredEditMode_Custom(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode", IfxTraceCategory.Leave);
            }
        }




        //void navList_RowDeleting(object sender, CancellableRowEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowDeleting", IfxTraceCategory.Enter);

        //        // Need custom code here such as a delete warning if child data will be deleted.
        //        // See the save method  navList_RowExitingEditMode  for an example of wireing up a callback if the delete failes.
        //        if (ApplicationLevelVariables.IsDeleteDataAllowed == false)
        //        {
        //            e.Cancel = true;
        //            return;
        //        }
        //        if (e.Row.Data is WcStoredProc_Bll)
        //        {
        //            WcStoredProc_Bll obj = e.Row.Data as WcStoredProc_Bll;
        //            string msg = "Are you sure you want to DELETE this record from the database and all data under it ? ";
        //            MessageBoxResult result = MessageBox.Show(msg, "Delete Warning", MessageBoxButton.OK);
        //            if (result == MessageBoxResult.OK)
        //            {
        //                if (obj.Sp_Id != null)
        //                {
        //                    _wcStoredProcProxy.Begin_WcStoredProc_SetIsDeleted((Guid)obj.Sp_Id, true);
        //                }
        //            }
        //        }
        //        else if (e.Row.Data is WcStoredProcParam_Bll)
        //        {
        //            WcStoredProcParam_Bll obj = e.Row.Data as WcStoredProcParam_Bll;
        //            string msg = "Are you sure you want to DELETE this record from the database and all data under it ? ";
        //            MessageBoxResult result = MessageBox.Show(msg, "Delete Warning", MessageBoxButton.OK);
        //            if (result == MessageBoxResult.OK)
        //            {
        //                _WcStoredProcParamProxy.Begin_WcStoredProcParam_SetIsDeleted(obj.SpP_Id, true);
        //            }
        //        }
        //        else if (e.Row.Data is WcStoredProcColumn_Bll)
        //        {
        //            WcStoredProcColumn_Bll obj = e.Row.Data as WcStoredProcColumn_Bll;
        //            string msg = "Are you sure you want to DELETE this record from the database and all data under it ? ";
        //            MessageBoxResult result = MessageBox.Show(msg, "Delete Warning", MessageBoxButton.OK);
        //            if (result == MessageBoxResult.OK)
        //            {
        //                _WcStoredProcColumnProxy.Begin_WcStoredProcColumn_SetIsDeleted(obj.SpCl_Id, true);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowDeleting", ex);
        //        IfxWrapperException.GetError(ex, (Guid)traceId);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowDeleting", IfxTraceCategory.Leave);
        //    }
        //}


        void navList_RowDeleting(object sender, CancellableRowEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowDeleting", IfxTraceCategory.Enter);

                // Need custom code here such as a delete warning if child data will be deleted.
                // See the save method  navList_RowExitingEditMode  for an example of wireing up a callback if the delete failes.
                if (ApplicationLevelVariables.IsDeleteDataAllowed == false)
                {
                    e.Cancel = true;
                    return;
                }
                if (e.Row.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = e.Row.Data as WcStoredProc_Bll;
                    string msg = "Are you sure you want to DELETE this record from the database and all data under it ? ";
                    MessageBoxResult result = MessageBox.Show(msg, "Delete Warning", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {
                        // modified
                        if (obj.Sp_Id != null)
                        {
                            _wcStoredProcProxy.Begin_WcStoredProc_SetIsDeleted((Guid)obj.Sp_Id, true);
                        }
                    }
                }
                else if (e.Row.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = e.Row.Data as WcStoredProcParam_Bll;
                    string msg = "Are you sure you want to DELETE this record from the database and all data under it ? ";
                    MessageBoxResult result = MessageBox.Show(msg, "Delete Warning", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {
                        _WcStoredProcParamProxy.Begin_WcStoredProcParam_SetIsDeleted(obj.SpP_Id, true);
                    }
                }
                else if (e.Row.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = e.Row.Data as WcStoredProcColumn_Bll;
                    string msg = "Are you sure you want to DELETE this record from the database and all data under it ? ";
                    MessageBoxResult result = MessageBox.Show(msg, "Delete Warning", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {
                        _WcStoredProcColumnProxy.Begin_WcStoredProcColumn_SetIsDeleted(obj.SpCl_Id, true);
                    }
                }
                else if (e.Row.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = e.Row.Data as WcStoredProcParamValueGroup_Bll;
                    string msg = "Are you sure you want to DELETE this record from the database and all data under it ? ";
                    MessageBoxResult result = MessageBox.Show(msg, "Delete Warning", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {
                        _WcStoredProcParamValueGroupProxy.Begin_WcStoredProcParamValueGroup_SetIsDeleted(obj.SpPVGrp_Id, true);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowDeleting", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowDeleting", IfxTraceCategory.Leave);
            }
        }












        #endregion CodeGen Methods for modification


        #region Custom Code



        void ShowDatabaseTables()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ShowDatabaseTables", IfxTraceCategory.Enter);

                navList.IsEnabled = true;
                if (mnuShowDbTables.IsChecked == true)
                {
                    LoadGridFromConnKeyId();
                }
                else
                {
                    navList.Columns["ImportSproc"].Visibility = Visibility.Collapsed;
                    navList.Columns["SprocInDb"].Visibility = Visibility.Collapsed;

                    _wcStoredProcProxy.Begin_WcStoredProc_GetListByFK((Guid)_guidParentId);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ShowDatabaseTables", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ShowDatabaseTables", IfxTraceCategory.Leave);
            }
        }


        #region cmbConnString XamComboEditor

        /// <summary>
        /// Sets the data source for this list control. Usually a call to the data store is
        /// made and lightweight list of WireType objects is returned for this data source.
        /// </summary>
        void cmbConnString_DataSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "cmbConnString_DataSource", IfxTraceCategory.Enter);
                cmbConnString.DisplayMemberPath = "ItemName";
                cmbConnString.ItemsSource = WcApplicationVersion_Bll_staticLists.WcDatabaseConnectionStringKey_ComboItemList_BindingListProperty;
                WcApplicationVersion_Bll_staticLists.WcDatabaseConnectionStringKey_ComboItemList_BindingListProperty.DataSourceUpdated -= cmbConnString_ComboItemList_BindingListProperty_DataSourceUpdated;
                WcApplicationVersion_Bll_staticLists.WcDatabaseConnectionStringKey_ComboItemList_BindingListProperty.DataSourceUpdated += cmbConnString_ComboItemList_BindingListProperty_DataSourceUpdated;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "cmbConnString_DataSource", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "cmbConnString_DataSource", IfxTraceCategory.Leave);
            }
        }

        void cmbConnString_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "cmbConnString_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);
                cmbConnString.vXamComboEditorSelectionChanged -= CmbConnString_vXamComboEditorSelectionChanged;

                foreach (ComboEditorItem obj in cmbConnString.Items)
                {
                    if ((string)((ComboItem)obj.Data).Desc == "True")
                    {
                        obj.IsSelected = true;
                        return;
                    }
                }
                cmbConnString.vXamComboEditorSelectionChanged += CmbConnString_vXamComboEditorSelectionChanged;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "cmbConnString_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "cmbConnString_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        private Guid? _connStringKeyId = null;
        private void CmbConnString_vXamComboEditorSelectionChanged(object sender, vXamComboEditorSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CmbConnString_vXamComboEditorSelectionChanged", IfxTraceCategory.Enter);

                if (mnuShowDbTables.IsChecked == true)
                {
                    LoadGridFromConnKeyId();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CmbConnString_vXamComboEditorSelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CmbConnString_vXamComboEditorSelectionChanged", IfxTraceCategory.Leave);
            }
        }

        public void LoadGridFromConnKeyId()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter)
                    IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGridFromConnKeyId", IfxTraceCategory.Enter);
                SetViewMode(ViewMode.ShowDbTables);

                _wcStoredProcProxy.Begin_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyId((Guid)_guidParentId, _connStringKeyId);


                //if (cmbConnString.SelectedItem == null)
                //{
                //    // do something
                //    _connStringKeyId = null;
                //    // clear the grid
                //}
                //else
                //{
                //    _connStringKeyId = (Guid)((ComboItem)cmbConnString.SelectedItem).Id;
                //    _wcStoredProcProxy.Begin_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyId((Guid)_guidParentId, (Guid)_connStringKeyId);
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGridFromConnKeyId", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave)
                    IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGridFromConnKeyId", IfxTraceCategory.Leave);
            }
        }

        #endregion cmbConnString XamComboEditor


        public enum ViewMode
        {
            Normal,
            ShowDbTables
        }

        ViewMode _currentViewMode = ViewMode.Normal;

        public ViewMode CurrentViewMode
        {
            get { return _currentViewMode; }
            set { _currentViewMode = value; }
        }

        public void SetViewMode(ViewMode mode)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetViewMode", IfxTraceCategory.Enter);
                _currentViewMode = mode;
                switch (mode)
                {
                    case ViewMode.Normal:
                        mnuShowDbTables.IsChecked = false;
                        navList.Columns["ImportSproc"].Visibility = Visibility.Collapsed;
                        navList.Columns["SprocInDb"].Visibility = Visibility.Collapsed;
                        break;
                    case ViewMode.ShowDbTables:
                        mnuShowDbTables.IsChecked = true;
                        navList.Columns["ImportSproc"].Visibility = Visibility.Visible;
                        navList.Columns["SprocInDb"].Visibility = Visibility.Visible;
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetViewMode", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetViewMode", IfxTraceCategory.Leave);
            }
        }


        //private void btnImportSproc_Click(object sender, ButtonColumnClickEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnImportSproc_Click", IfxTraceCategory.Enter);

        //        var tbl = (WcTable_Bll)e.Data;


        //        //if (tbl.Tb_Id != null || tbl.SprocInDb == null)
        //        //{
        //        //    return;
        //        //}

        //        if (_connStringKeyId == null)
        //        {
        //            MessageBox.Show("Select a connection key first.", "Missing Connectin Key", MessageBoxButton.OK);
        //            return;
        //        }
        //        tbl.ImportSelectedTable((Guid)_connStringKeyId);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnImportSproc_Click", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnImportSproc_Click", IfxTraceCategory.Leave);
        //    }
        //}





        #endregion Custom Code


        #region Fetch Data


        private void _wcStoredProcProxy_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted(object sender, GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcStoredProcProxy_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                NavListRefreshFromObjectArray(array);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcStoredProcProxy_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcStoredProcProxy_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted", IfxTraceCategory.Leave);
            }
        }


        #endregion Fetch Data



    }
}
