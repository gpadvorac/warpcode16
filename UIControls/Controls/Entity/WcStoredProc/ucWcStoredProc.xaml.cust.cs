using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using EntityBll.SL;
using Ifx.SL;
using IfxUserViewLogSL;
using TypeServices;
using vUICommon;

// Gen Timestamp:  12/23/2017 1:27:00 AM

namespace UIControls
{
    public partial class ucWcStoredProc
    {



        #region Initialize Variables

        private ucWcStoredProcGroupAssignment _ucSpGrpAssign = null;

        #endregion Initialize Variables




        void CustomConstructionCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", IfxTraceCategory.Enter);

                _wcStoredProcProxy.GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted += _wcStoredProcProxy_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted;
                // GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyId


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", IfxTraceCategory.Leave);
            }
        }



        #region Copy Code Gen Methods from the Codebehind class here



        public void SetStateFromParent(object ancestorId, string parentType, int? intParentId, Guid? guidParentId, object objParentId, int? intCurrentId, Guid? guidCurrentId, object objCurrentId, IBusinessObject currentBusinessObject, object[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 9
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Enter);
                _intParentId = intParentId;
                if (guidParentId == null)
                {
                    _guidParentId = objParentId as Guid?;
                }
                else
                {
                    _guidParentId = guidParentId;
                }
                
                _intCurrentId = intCurrentId;
                _guidCurrentId = guidCurrentId;
                //_guidCurrentId = objCurrentId as Guid?;
                //_oCurrentId = objCurrentId;

                _parentType = parentType;
                _currentBusinessObject = (WcStoredProc_Bll)currentBusinessObject;  

                ucNav.ParentType = _parentType;
                ucNav.GuidParentId = _guidParentId;  
                ucNav.ObjectParentId = objParentId;

                if (_FLG_IsLoaded == false)
                {
                    InitializeControl();
                }
                if (list == null)
                {
                    //  **********************************      N E E D   C U S T O M   C O D E    H E R E  ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! 
           
                    if (_guidParentId == null)
                    {
                        //ucNav.NavList_ItemSource.Clear();
                        NavListRefreshFromObjectArray(null);
                        ucNav.IsEnabled = false;
                    }
                    else
                    {
                        ucNav.IsEnabled = true;

                        LoadNavlistData();
                        //_wcStoredProcProxy.Begin_WcStoredProc_GetListByFK((Guid)_guidParentId);
                        //ucNav.navList.Cursor = Cursors.Wait;
                    }
                }
                else
                {
                    NavListRefreshFromObjectArray(list);
                }

                // Use    intParentId     if the parent is an Integer.  we might need to be using the Ancestor Id instead.
                if (guidParentId != null)
                {
                    ucNav.Set_vXamComboColumn_ItemSourcesWithParams();
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Leave);
            }
        }

        
        public void LoadNavlistData()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadNavlistData", IfxTraceCategory.Enter);

                _currentBusinessObject = null;
                ucNav.IsEnabled = true;
                //_wcStoredProcProxy.Begin_WcStoredProc_GetListByFK((Guid)_guidParentId);

                if (ucNav.CurrentViewMode == ucWcStoredProcList.ViewMode.Normal)
                {
                    _wcStoredProcProxy.Begin_WcStoredProc_GetListByFK((Guid)_guidParentId);
                    ucNav.navList.Cursor = Cursors.Wait;
                }
                else
                {
                    ucNav.LoadGridFromConnKeyId();
                    //_wcStoredProcProxy.Begin_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyId((Guid)_guidParentId, null);
                }



                //switch (_parentType)
                //{
                //    case "someParent":
                //        //do something
                //        break;
                //    default:
                //        throw new Exception("Missing Parent Type");
                //}

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadNavlistData", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadNavlistData", IfxTraceCategory.Leave);
            }
        }



        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDelete_Click", IfxTraceCategory.Enter);
                if (_currentBusinessObject != null)
                {
                    // call ws and set IsDeleted to true
                    string msg = "Are you sure you want to DELETE this record from the database and all data under it?";
                    MessageBoxResult result = MessageBox.Show(msg, "Delete Warning", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {
                        if (_currentBusinessObject.Sp_Id != null)
                        {
                            _wcStoredProcProxy.Begin_WcStoredProc_SetIsDeleted((Guid)_currentBusinessObject.Sp_Id, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDelete_Click", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDelete_Click", IfxTraceCategory.Leave);
            }
        }





        /// <summary>
        ///     An event that bubbles up from <see cref="ucWcStoredProcList">ucWcStoredProcList</see> when the
        ///     selected index changes.
        /// </summary>
        void ucNav_NavigationListSelectedItemChanged(object sender, NavigationListSelectedItemChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNav_NavigationListSelectedItemChanged", IfxTraceCategory.Enter);

                if (e.SelectedItem is WcStoredProc_Bll)
                {
                    _currentBusinessObject = (WcStoredProc_Bll)e.SelectedItem;
                    _currentChildBusinessObject = null;
                    //_oCurrentId = _currentBusinessObject.Sp_Id;
                }
                else if (e.SelectedItem is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = (WcStoredProcParam_Bll)e.SelectedItem;
                    _currentChildBusinessObject = obj;
                    _currentBusinessObject = obj.ParentBusinessObject as WcStoredProc_Bll;
                }


                else if (e.SelectedItem is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = (WcStoredProcColumn_Bll)e.SelectedItem;
                    _currentChildBusinessObject = obj;
                    _currentBusinessObject = obj.ParentBusinessObject as WcStoredProc_Bll;
                }


                else if (e.SelectedItem is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = (WcStoredProcParamValueGroup_Bll)e.SelectedItem;
                    _currentChildBusinessObject = obj;
                    _currentBusinessObject = obj.ParentBusinessObject as WcStoredProc_Bll;
                }


                if (_currentBusinessObject != null)
                {
                    _viewLogItemForPublish.ItemClickedDataName = _currentBusinessObject.DataRowName;
                    //  revized 
                    if (_currentBusinessObject.ObjectPrimaryKey() == null)
                    {
                        _viewLogItemForPublish.ItemClickedDataId = null;
                    }
                    else
                    {
                        _viewLogItemForPublish.ItemClickedDataId = _currentBusinessObject.ObjectPrimaryKey().ToString();
                    }
                }
                else
                {
                    _viewLogItemForPublish.ItemClickedDataName = "";
                    _viewLogItemForPublish.ItemClickedDataId = "";
                }

                SyncControlsWithCurrentBusinessObject();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNav_NavigationListSelectedItemChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNav_NavigationListSelectedItemChanged", IfxTraceCategory.Leave);
            }
        }



        #endregion Copy Code Gen Methods from the Codebehind class here

        #region Insert Custom Code Here



        private void _wcStoredProcProxy_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted(object sender, GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcStoredProcProxy_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                NavListRefreshFromObjectArray(array);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcStoredProcProxy_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcStoredProcProxy_GetWcStoredProc_lstNamesFromAppAndDB_ByApVrsnOrCnnKeyIdCompleted", IfxTraceCategory.Leave);
                ucNav.navList.Cursor = Cursors.Arrow;
            }
        }




        void ucSpGrpAssign_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucSpGrpAssign_Load", IfxTraceCategory.Enter);
                if (_ucSpGrpAssign == null)
                {
                    _ucSpGrpAssign = new ucWcStoredProcGroupAssignment();
                    //_ucTblColGrpAssign.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    //_ucTblColGrpAssign.ChangeParentNavGridExpansion += ChangeParentNavGridExpansionFromNestedObject;
                    tbi_ucSpGrpAssign.Content = _ucSpGrpAssign;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucSpGrpAssign_Load", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucSpGrpAssign_Load", IfxTraceCategory.Leave);
            }
        }





        #endregion Insert Custom Code Here

        #region  Combo DataSources and Events

        #endregion  Combo DataSources and Events

        #region Method Extentions For Custom Code

        void ConfigureToCurrentEntityState_CustomCode(EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState_CustomCode", IfxTraceCategory.Enter);
                switch (state)
                {
                    case EntityStateSwitch.None:
                        if (_currentBusinessObject == null)
                        {
                            tbi_ucSpGrpAssign.IsEnabled = false;
                        }
                        else
                        {
                            tbi_ucSpGrpAssign.IsEnabled = true;
                        }
                        break;
                    case EntityStateSwitch.NewInvalidNotDirty:
                        tbi_ucSpGrpAssign.IsEnabled = false;
                        break;
                    case EntityStateSwitch.NewValidNotDirty:
                        tbi_ucSpGrpAssign.IsEnabled = false;
                        break;
                    case EntityStateSwitch.NewValidDirty:
                        tbi_ucSpGrpAssign.IsEnabled = false;
                        break;
                    case EntityStateSwitch.NewInvalidDirty:
                        tbi_ucSpGrpAssign.IsEnabled = false;
                        break;
                    case EntityStateSwitch.ExistingInvalidDirty:
                        tbi_ucSpGrpAssign.IsEnabled = false;
                        break;
                    case EntityStateSwitch.ExistingValidDirty:
                        tbi_ucSpGrpAssign.IsEnabled = false;
                        break;
                    case EntityStateSwitch.ExistingValidNotDirty:
                        tbi_ucSpGrpAssign.IsEnabled = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState_CustomCode", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState_CustomCode", IfxTraceCategory.Leave);
            }
        }

        void SyncControlsWithCurrentBusinessObject_CustomCode1()
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", IfxTraceCategory.Enter);


            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    _FLG_IsLoaded = true;
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", IfxTraceCategory.Leave);
            //}
        }

        void SyncControlsWithCurrentBusinessObject_CustomCode2(string selectedTab, object currentId)
            {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", IfxTraceCategory.Enter);
                switch (selectedTab)
                {
                    case "tbi_ucSpGrpAssign":
                        if (_ucSpGrpAssign == null) { ucSpGrpAssign_Load(); }
                        if (_currentBusinessObject != null)
                        {
                            _ucSpGrpAssign.SetStateFromParent(_currentBusinessObject.Sp_ApVrsn_Id, _currentBusinessObject.Sp_Id);
                        }
                        break;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", IfxTraceCategory.Leave);
            }
        }

        #endregion Method Extentions For Custom Code



        #region Standard CodeGen Methods Subject to Customization



        #endregion Standard CodeGen Methods Subject to Customization



    }
    }
