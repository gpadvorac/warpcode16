using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class WcStoredProcColumnConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcStoredProcColumnConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public WcStoredProcColumnConcurrencyList(WcStoredProcColumn_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcStoredProcColumnConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Name", data.C.SpCl_Name, data.X.SpCl_Name));
                _concurrencyList.Add(new ConcurrencyItem("Column Index", data.C.SpCl_ColIndex, data.X.SpCl_ColIndex));
                _concurrencyList.Add(new ConcurrencyItem("Column  Letter", data.C.SpCl_ColLetter, data.X.SpCl_ColLetter));
                _concurrencyList.Add(new ConcurrencyItem("DotNet Type", data.C.SpCl_DtNt_Id, data.X.SpCl_DtNt_Id));
                _concurrencyList.Add(new ConcurrencyItem("SQL Type", data.C.SpCl_DtSql_Id, data.X.SpCl_DtSql_Id));
                _concurrencyList.Add(new ConcurrencyItem("Nullable", data.C.SpCl_IsNullable, data.X.SpCl_IsNullable));
                _concurrencyList.Add(new ConcurrencyItem("Visible", data.C.SpCl_IsVisible, data.X.SpCl_IsVisible));
                _concurrencyList.Add(new ConcurrencyItem("Browsable", data.C.SpCl_Browsable, data.X.SpCl_Browsable));
                _concurrencyList.Add(new ConcurrencyItem("Notes", data.C.SpCl_Notes, data.X.SpCl_Notes));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.SpCl_IsActiveRow, data.X.SpCl_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



