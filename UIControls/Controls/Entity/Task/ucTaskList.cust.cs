using System;
using Ifx.SL;
using EntityBll.SL;
using TypeServices;
using Infragistics.Controls.Grids;
using vUICommon;
using Velocity.SL;
using vControls;
using Infragistics.Controls.Editors;
using System.Windows.Controls;
using System.Windows;
using Infragistics.Controls.Menus;
using ProxyWrapper;
using vDialogControl;
using Infragistics.Controls.Interactions;
using ApplicationTypeServices;
using vFileStorageDialog;

// Gen Timestamp:  1/4/2015 12:10:49 AM

namespace UIControls
{
    public partial class ucTaskList
    {

        #region Initialize Variables



        ProxyWrapper.TaskService_ProxyWrapper _Proxy = null;

        Guid? _prj_Id = null;

        ucTask _ucTk = null;
        private DiscussionSupportService_ProxyWrapper _discussionProxy;
        private Task_Bll _task = null;


        #endregion Initialize Variables


        #region Constructor

        void CustomConstructionCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", IfxTraceCategory.Enter);

                _Proxy = new ProxyWrapper.TaskService_ProxyWrapper();
                _Proxy.Task_GetListByFKCompleted += new EventHandler<Task_GetListByFKCompletedEventArgs>(Task_GetListByFKCompleted);
                _Proxy.Task_GetAllCompleted += new EventHandler<Task_GetAllCompletedEventArgs>(Task_GetAllCompleted);

                _discussionProxy = new DiscussionSupportService_ProxyWrapper();
                _discussionProxy.GetDiscussion_lstBy_TaskCompleted += _discussionProxy_GetDiscussion_lstBy_TaskCompleted;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructor


        #region Security

        UserSecurityContext _userContext = new UserSecurityContext();
        //***  Add the ACTUAL Guid below, and then delete this comment
        private Guid _ancestorSecurityId = Guid.NewGuid();
        ControlCache cCache;
        EntityCache eCache = null;
        bool defaultEntityOperationPermission = true;
        bool _secuitySettingIsReadOnly = false;
        //***  Add the ACTUAL Guid below, and then delete this comment
        Guid _controlId = Guid.NewGuid();
        bool _isDeleteActionAllowed = false;
        Guid _deleteActionId = Guid.NewGuid();  // Hard code the actual guid from the security tool


        public void SetSecurityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Enter);

                _userContext.LoadArtifactPermissions(_ancestorSecurityId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Leave);
            }
        }

        void UserSecurityContext_SecurityArtifactsRetrieved(object sender, SecurityArtifactsRetrievedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Enter);
                SecurityCache.AddControlCacheForAncestor(e.ArtifactAncestorData);
                cCache = SecurityCache.GetControlGroupById(e.ArtifactAncestorData.Id);

                UiControlStateSetting setting = cCache.GetControlById(_controlId).Setting;
                if (setting == UiControlStateSetting.Enable)
                {
                    _secuitySettingIsReadOnly = false;
                }
                else
                {
                    _secuitySettingIsReadOnly = true;
                    navList_SplitScreenMode();
                }

                //_isDeleteActionAllowed = SecurityCache.IsEnabled(_deleteActionId);
                //if (_isDeleteActionAllowed)
                //{
                //    navList.DeleteKeyAction = DeleteKeyAction.DeleteSelectedRows;
                //}

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Leave);
            }
        }

        #endregion Security

        #region Method Extentions For Custom Code


        public void Set_vXamComboColumn_ItemSourcesWithParams()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", IfxTraceCategory.Enter);
                
                //_leaseProxy.Begin_GetLeaseAltNumberSource_ComboItemList((Guid)_prj_Id);

//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", IfxTraceCategory.Leave);
//            }
        }

        #region Combo ItemsSource for Non-Static Lists




        #endregion region Combo ItemsSource for Non-Static Lists



        void vXamComboColumn_SelectionChanged_CustomCode(object sender, vXamComboColumnSelectionChangedEventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void vXamMultiColumnComboColumn_SelectionChanged_CustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void vDatePickerColumn_TextChanged_CustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void XamColorPicker_SelectedColorChanged_CustomCode(XamColorPicker ctl, SelectedColorChangedEventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void vCheckColumn_CheckedChanged_CustomCode(vCheckBox ctl, IBusinessObject obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void vTextColumn_TextChanged_CustomCode(TextBox ctl, Task_Bll obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void vDecimalColumn_TextChanged_CustomCode(TextBox ctl, Task_Bll obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void navList_RowEnteredEditMode_Custom(object sender, EditingRowEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", IfxTraceCategory.Enter);
                Task_Bll obj = e.Row.Data as Task_Bll;
                if (obj == null) { return; }

                obj.StandingFK = (Guid)_prj_Id;    // added this line


                switch (_parentType)
                {
                    //case "ucProject":
                    //    _data.C.WlD_Id_noevents = id;
                    //    break;
                    case "ucDefect":
                        obj.Current.Df_Id_noevents = _guidParentId;
                        break;
                    case "ucContract":
                        obj.Current.Ct_Id_noevents = _guidParentId;
                        break;
                    case "ucContractDt":
                        obj.Current.CtD_Id_noevents = _guidParentId;
                        break;
                    case "ucRelatedContract":
                        obj.Current.RCt_Id_noevents = _guidParentId;
                        break;
                }




                ////  Or For a Fixed Single Parent
                //_isRowInEditMode = true;
                //if (obj.State.IsNew() == true)
                //{
                //    SetNewRowValidation();
                //}
                //obj.Current.Tk_Prj_Id_noevents = _guidParentId;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", IfxTraceCategory.Leave);
            }
        }


        void navList_SelectedRowsCollectionChanged_Custom(object sender, SelectionCollectionChangedEventArgs<SelectedRowsCollection> e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", ex);
				   // throw IfxWrapperException.GetError(ex, (Guid)traceId);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", IfxTraceCategory.Leave);
//            }
        }

        #endregion Method Extentions For Custom Code


        #region CodeGen Methods for modification


        #region Menu Configuration



        bool _mnuClearFilters_AllowVisibility = true;
        bool _mnuGridTools_AllowVisibility = true;
        bool _mnuRichGrid_AllowVisibility = true;
        bool _mnuSplitScreen_AllowVisibility = true;
        bool _isAllowNewRow = true;
        bool _mnuExcelExport_AllowVisibility = true;
        bool _gridDataSourceCombo_AllowVisibility = true;
        bool _menuGridRow_AllowVisibility = true;  // the area where the grid menu is located

        void MenuConfiguration()
        {

            if (_mnuClearFilters_AllowVisibility == false)
            {
                mnuClearFilters.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (_mnuGridTools_AllowVisibility == false)
            {
                mnuGridTools.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (_mnuRichGrid_AllowVisibility == false)
            {
                //mnuRichGrid.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (_mnuSplitScreen_AllowVisibility == false)
            {
                mnuSplitScreen.Visibility = System.Windows.Visibility.Collapsed;
            }

            //if (_isAllowNewRow == false)
            //{
            //    mnuNewRowOnTop.Visibility = System.Windows.Visibility.Collapsed;
            //}

            if (_mnuExcelExport_AllowVisibility == false)
            {
                mnuExcelExport.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (_gridDataSourceCombo_AllowVisibility == false)
            {
                //lblGridData.Visibility = System.Windows.Visibility.Collapsed;
                //cmbGridDataSource.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (_menuGridRow_AllowVisibility == false)
            {
                GridMenuRow.Height = new GridLength(0);
                //LayoutRoot.RowDefinitions[0].Height = new System.Windows.GridLength(0);
            }

        }

        #endregion Menu Configuration



        /// <overloads>
        ///     This is a standard method in most entity related controls where state and related
        ///     Id values are passed in from the parent control. Often an <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/The_Entity_Manager.html">
        ///     Entity Manager</a> control is a child to another entity and needs the parent Id to
        ///     filter its navigation list. Sometimes the current Id value is also known and used
        ///     to select the corresponding row in ucEntityList. Selecting this row starts a series
        ///     of events which end up populating <see cref="ucTaskProps">ucProps</see> with the
        ///     correct data. Since the parent control sometime has information and logic available
        ///     which it can use to create a filtered list, it has the option to preload a list of
        ///     <see cref="EntityBll.Task_Bll">
        /// 		<see cref="EntityBll.Task_Bll">Task_Bll</see>
        /// 	</see> objects and pass into this method which will then be passed into this class
        ///     via this method.
        /// </overloads>
        /// <param name="parentType">
        ///     A string value naming the current parent's type. It’s possible for an Entity
        ///     Manager to have many different parent types while others have only one or none at
        ///     all. For example, an <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/The_Entity_Manager.html">Entity Manager</a> for an Address entity could have a parent type
        ///     of Customer, Company, Person, and so on. There may be situations where different
        ///     logic or filters need to be applied depending on the type of parent. This helps
        ///     make the <see cref="TypeServices.IEntityControl">IEntityControl</see> interface
        ///     more extendable and reusable.
        /// </param>
        /// <param name="intParentId">
        ///     int Id value of the object currently acting as the parent. Used to filter the
        ///     navigation list (<see cref="ucTaskList">ucTaskList</see>) and also to set the
        ///     StandingFK value. Both int and Guid data types are used as input parameters so the
        ///     <see cref="TypeServices.IEntityControl">IEntityControl</see> interface can
        ///     accommodate both data types making this more extendable and reusable.
        /// </param>
        /// <param name="guidParentId">
        ///     Guid Id value of the object currently acting as the parent. Used to filter the
        ///     navigation list (<see cref="ucTaskList">ucTaskList</see>) and also to set the
        ///     StandingFK value. Both int and Guid data types are used as input parameters so the
        ///     <see cref="TypeServices.IEntityControl">IEntityControl</see> interface can
        ///     accommodate both data types making this more extendable and reusable.
        /// </param>
        /// <param name="intId">
        ///     int Id value for the current Task. This can be used in <see cref="ucTaskList.FindRowById">ucTaskList.FindRowById</see> to select this instance of
        ///     <see cref="EntityBll.Task_Bll">Task_ Bll</see> in the navigation list and/or can
        ///     also be used to populate <see cref="ucTaskProps">ucProps</see>. ucProps will
        ///     either use the instance of <see cref="EntityBll.Task_Bll">Task_Bll</see> selected
        ///     in the navigation list (<see cref="ucTaskList">ucTaskList</see>), or can be used
        ///     to fetch the data from the database. Both int and Guid data types are used as input
        ///     parameters so the <see cref="TypeServices.IEntityControl">IEntityControl</see>
        ///     interface can accommodate both types making this more extendable and reusable.
        /// </param>
        /// <param name="guidId">
        ///     Guid Id value for the current Task. This can be used in <see cref="ucTaskList.FindRowById">ucTaskList.FindRowById</see> to select this instance of
        ///     <see cref="EntityBll.Task_Bll">Task_ Bll</see> in the navigation list and/or can
        ///     also be used to populate <see cref="ucTaskProps">ucProps</see>. ucProps will
        ///     either use the instance of <see cref="EntityBll.Task_Bll">Task_Bll</see> selected
        ///     in the navigation list (<see cref="ucTaskList">ucTaskList</see>), or can be used
        ///     to fetch the data from the database. Both int and Guid data types are used as input
        ///     parameters so the <see cref="TypeServices.IEntityControl">IEntityControl</see>
        ///     interface can accommodate both types making this more extendable and reusable.
        /// </param>
        /// <param name="currentBusinessObject">
        ///     In instance of Task_Bll can be passed in and bound to ucProps and/or added to the
        ///     list in <see cref="ucTaskList">ucTaskList</see>.
        /// </param>
        /// <param name="list">
        ///     A list of <see cref="EntityWireType.Task_ValuesMngr">Task_ValuesMngr</see>
        ///     objects using the <see cref="TypeServices.IEntity_ValuesMngr">IEntity_ValuesMngr</see> Interface which can be
        ///     passed in and used to populate <see cref="ucTaskList">ucTaskList</see> using the
        ///     <see cref="EntityBll.Task_List.ReplaceList">EntityBll.Task_List.ReplaceList</see>
        ///     method. This allows logic known only to the parent to create a filtered list.
        /// </param>
        /// <param name="newText">
        /// In cases when an entity is used in a dropdown list and the user wants to edit an
        /// item in the list, or add a new item; the text the user entered could be passed into
        /// this parameter and preloaded into the data entry part of the screen.
        /// </param>
        public void SetStateFromParent(string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, IBusinessObject currentBusinessObject, IEntity_ValuesMngr[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 7
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Enter);
                _intCurrentId = intId;
                _intParentId = intParentId;
                _guidParentId = guidParentId;
                _newText = newText;
                if (null != list)
                {
                    NavList_ItemSource.ReplaceList(list);
                }    
                else
                {
                    if (guidParentId == null)
                    {
                        NavListRefreshFromObjectArray(null);
                        navList.IsEnabled = false;
                    }
                    else
                    {
                        //  Add code here for multiple parent types
                        navList.IsEnabled = true;
                        _Proxy.Begin_Task_GetListByFK((Guid)_guidParentId);
                    }
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Leave);
            }
        }


        private void XamMenuItem_Click(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click", IfxTraceCategory.Enter);

                XamMenuItem mnu = (XamMenuItem)sender;
                switch (mnu.Name)
                {
                    case "mnuSplitScreen":
                        _isSplitSreenMode = !_isSplitSreenMode;
                        navList_SplitScreenMode();
                        break;
                    //case "mnuNewRowOnTop":
                    //    //if (_gridAllowAddRowMode == AddNewRowLocation.Top)
                    //    //{
                    //    //    _gridAllowAddRowMode = AddNewRowLocation.Bottom;
                    //    //}
                    //    //else
                    //    //{
                    //    //    _gridAllowAddRowMode = AddNewRowLocation.Top;
                    //    //}
                    //    navList_AllowAddRowMode();
                    //    break;
                    case "mnuColumnChooser":
                        navList.ShowColumnChooser();
                        break;
                    case "mnuDisplyHideColumnButton":
                        navList_DisplyHideColumnButton();
                        break;
                    case "mnuColumnGrouping":
                        navList_ColumnGroupingMode();
                        break;
                    case "mnuColumnSummary":
                        navList_ColumnSummaryMode();
                        break;
                    case "mnuFilterNone":
                        _FilterType = FilterUIType.None;
                        navList_FilterMode();
                        break;
                    case "mnuFilterMenu":
                        _FilterType = FilterUIType.FilterMenu;
                        navList_FilterMode();
                        break;
                    case "mnuFilterTop":
                        _FilterType = FilterUIType.FilterRowTop;
                        navList_FilterMode();
                        break;
                    case "mnuFilterBottom":
                        _FilterType = FilterUIType.FilterRowBottom;
                        navList_FilterMode();
                        break;
                    case "mnuFixedColumn_UsePin":
                        //_allowFixedColumns = FixedColumnType.Disabled;
                        navList_FixedColumnMode();
                        break;
                    //case "mnuFixedColumn_Indicator":
                    //    _allowFixedColumns = FixedColumnType.Indicator;
                    //    navList_FixedColumnMode();
                    //    break;
                    case "mnuFixedColumn_UseDropArea":
                        //_allowFixedColumns = FixedColumnType.DropArea;
                        navList_FixedColumnMode();
                        break;
                    //case "mnuFixedColumn_Both":
                    //    _allowFixedColumns = FixedColumnType.Both;
                    //    navList_FixedColumnMode();
                    //    break;
                    case "mnuFixedColumn_Indicator_Left":
                        _fixedColumnIndicatorDirection = FixedIndicatorDirection.Left;
                        navList_FixedColumnIndicatorDirection();
                        break;
                    case "mnuFixedColumn_Indicator_Right":
                        _fixedColumnIndicatorDirection = FixedIndicatorDirection.Right;
                        navList_FixedColumnIndicatorDirection();
                        break;
                    case "mnuFixedColumn_DropArea_Left":
                        _fixedColumnDropAreaLocation = FixedDropAreaLocation.Left;
                        navList_FixedColumnDropAreaLocation();
                        break;
                    case "mnuFixedColumn_DropArea_Right":
                        _fixedColumnDropAreaLocation = FixedDropAreaLocation.Right;
                        navList_FixedColumnDropAreaLocation();
                        break;
                    case "mnuFixedColumn_DropArea_Both":
                        _fixedColumnDropAreaLocation = FixedDropAreaLocation.Both;
                        navList_FixedColumnDropAreaLocation();
                        break;
                    case "mnuRichGrid":
                        navList_ShowHideRichGrid();
                        break;
                    case "mnuExcelExport":
                        ExportToExcel();
                        break;
                    case "mnuClearFilters":
                        ClearFilterCells(navList, navList.Rows);
                        break;
                    case "mnuRefresh":
                        RefreshData();
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click", IfxTraceCategory.Leave);
            }
        }


        #endregion CodeGen Methods for modification

        
        #region Custom Code



        public Guid? Prj_Id
        {
            get { return _prj_Id; }
            set { _prj_Id = value; }
        }


        public ucTask ucTk
        {
            get { return _ucTk; }
            set { _ucTk = value; }
        }


        private bool _useForEntityColumns = false;

        public bool UseForEntityColumns
        {
            get { return _useForEntityColumns; }
            set
            {
                _useForEntityColumns = value;
            }
        }

        public void ShowHideForEntityColumns()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ShowHideForEntityColumns", IfxTraceCategory.Enter);

                if (_useForEntityColumns == false)
                {
                    navList.Columns["TaskForEntity"].Visibility = Visibility.Collapsed;
                    navList.Columns["TaskForItem"].Visibility = Visibility.Collapsed;
                    //navList.Columns.Remove(navList.Columns["TaskForEntity"]);
                    //navList.Columns.Remove(navList.Columns["TaskForItem"]);
                }
                else
                {
                    navList.Columns["TaskForEntity"].Visibility = Visibility.Visible;
                    navList.Columns["TaskForItem"].Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ShowHideForEntityColumns", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ShowHideForEntityColumns", IfxTraceCategory.Leave);
            }
        }
       
        private void RefreshData()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshData", IfxTraceCategory.Enter);

                //Contract_Bll_staticLists.LoadStaticLists((Guid)ContextValues.CurrentProjectId);
                SupportDataHelper.ReloadStaticSupportData();
                _ucTk.LoadData();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshData", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshData", IfxTraceCategory.Leave);
            }
        }


        public void RefreshRow(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshRow", IfxTraceCategory.Enter);
                foreach (Row rw in navList.Rows)
                {

                    if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        if (((Task_Bll)rw.Data).Tk_Id == id)
                        {

                            ((Task_Bll)rw.Data).RefreshRowFromTaskAssigneeUpdate();

                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshRow", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshRow", IfxTraceCategory.Leave);
            }
        }
        


        #region Button Columns

        private void btnOpenAttaments_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenAttaments_Click", IfxTraceCategory.Enter);

                _task = (Task_Bll)e.Data;

                var ucFP = new ucFilePopup {DialogMode = vDialogMode.Unrestricted};
                ucFP.SetStateFromParent(null, "Task", null, _task.Tk_Id, null, null, null, null, null);

                var attachmentDialog = vDialogHelper.InitializeAttachmentDialog(ucFP, "Task", _task.Tk_Number.ToString());

                ucFP.ClosePopup += (o, args) =>
                {
                    _task.GetEntityRow(_task.Tk_Id);
                    attachmentDialog.Close();
                };

                attachmentDialog.Show();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenAttaments_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenAttaments_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnOpenDiscussions_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenDiscussions_Click", IfxTraceCategory.Enter);

                _task = (Task_Bll)e.Data;
                _discussionProxy.Begin_GetDiscussion_lstBy_Task(_task.Tk_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenDiscussions_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenDiscussions_Click", IfxTraceCategory.Leave);
            }
        }

        #region Button Column Callbacks

        void _discussionProxy_GetDiscussion_lstBy_TaskCompleted(object sender, GetDiscussion_lstBy_TaskCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_discussionProxy_GetDiscussion_lstBy_TaskCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                var array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                var discussionDialog = vDialogHelper.InitializeDiscussionDialog(array, vDialogMode.Unrestricted, _task.Tk_Id,
                    ApplicationLevelVariables.DiscussionParentTypes.Task, _task.Tk_Number.ToString());

                if (discussionDialog.ShowDialog() == true)
                {
                    discussionDialog.Result.RequestClose += (_, __) =>
                    {
                        _task.GetEntityRow(_task.Tk_Id);
                        discussionDialog = null;
                    };
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_discussionProxy_GetDiscussion_lstBy_TaskCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_discussionProxy_GetDiscussion_lstBy_TaskCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Button Column Callbacks






        private void btnOpenEntityInNewWindow_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenEntityInNewWindow_Click", IfxTraceCategory.Enter);

                var task = (Task_Bll)e.Data;

                if (task.Ct_Id != null)
                {
                    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucContract", "ucContract", (Guid)task.Ct_Id, "Section for Task #" + task.Tk_Number.ToString());
                }
                else if (task.CtD_Id != null)
                {
                    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucContractDt", "ucContractDt", (Guid)task.CtD_Id, "Section Detail for Task #" + task.Tk_Number.ToString());
                }
                else if (task.Df_Id != null)
                {
                    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucDefect", "ucDefect", (Guid)task.Df_Id, "Issue for Task #" + task.Tk_Number.ToString());
                }
                else if (task.Ls_Id != null)
                {
                    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucLease", "ucLease", (Guid)task.Ls_Id, "Agreement for Task #" + task.Tk_Number.ToString());
                }
                else if (task.LsTr_Id != null)
                {
                    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucLeaseTract", "ucLeaseTract", (Guid)task.LsTr_Id, "Tract for Task #" + task.Tk_Number.ToString());
                }
                else if (task.Ob_Id != null)
                {
                    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucObligation", "ucObligation", (Guid)task.Ob_Id, "Obligation/Provision for Task #" + task.Tk_Number.ToString());
                }
                else if (task.Pp_Id != null)
                {
                    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucProspect", "ucProspect", (Guid)task.Pp_Id, "Prospect for Task #" + task.Tk_Number.ToString());
                }
                else if (task.Wl_Id != null)
                {
                    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucWell", "ucWell", (Guid)task.Wl_Id, "Pipeline for Task #" + task.Tk_Number.ToString());
                }
                else
                {
                    MessageBox.Show("This task a 'Project Level' task and therefore, this option is not available.",
                        "Option Not Available", MessageBoxButton.OK);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenEntityInNewWindow_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenEntityInNewWindow_Click", IfxTraceCategory.Leave);
            }
        }




        #endregion Button Columns



        #endregion Custom Code


        #region Fetch Data





        #endregion Fetch Data



    }
}
