using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class TaskConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "TaskConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public TaskConcurrencyList(Task_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucTaskConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Attachments", data.C.AttachmentCount, data.X.AttachmentCount));
                _concurrencyList.Add(new ConcurrencyItem("Discussions", data.C.DiscussionCount, data.X.DiscussionCount));
                _concurrencyList.Add(new ConcurrencyItem("Type", data.C.Tk_TkTp_Id, data.X.Tk_TkTp_Id));
                _concurrencyList.Add(new ConcurrencyItem("Category", data.C.Tk_TkCt_Id, data.X.Tk_TkCt_Id));
                _concurrencyList.Add(new ConcurrencyItem("Number", data.C.Tk_Number, data.X.Tk_Number));
                _concurrencyList.Add(new ConcurrencyItem("Name", data.C.Tk_Name, data.X.Tk_Name));
                _concurrencyList.Add(new ConcurrencyItem("Description", data.C.Tk_Desc, data.X.Tk_Desc));
                _concurrencyList.Add(new ConcurrencyItem("Estimated Time", data.C.Tk_EstimatedTime, data.X.Tk_EstimatedTime));
                _concurrencyList.Add(new ConcurrencyItem("Actual Time", data.C.Tk_ActualTime, data.X.Tk_ActualTime));
                _concurrencyList.Add(new ConcurrencyItem("Percent Complete", data.C.Tk_PercentComplete, data.X.Tk_PercentComplete));
                _concurrencyList.Add(new ConcurrencyItem("Start Date", data.C.Tk_StartDate, data.X.Tk_StartDate));
                _concurrencyList.Add(new ConcurrencyItem("Deadline", data.C.Tk_Deadline, data.X.Tk_Deadline));
                _concurrencyList.Add(new ConcurrencyItem("Status", data.C.Tk_TkSt_Id, data.X.Tk_TkSt_Id));
                _concurrencyList.Add(new ConcurrencyItem("Priority", data.C.Tk_TkPr_Id, data.X.Tk_TkPr_Id));
                _concurrencyList.Add(new ConcurrencyItem("Results", data.C.Tk_Results, data.X.Tk_Results));
                _concurrencyList.Add(new ConcurrencyItem("Remarks", data.C.Tk_Remarks, data.X.Tk_Remarks));
                _concurrencyList.Add(new ConcurrencyItem("Private", data.C.Tk_IsPrivate, data.X.Tk_IsPrivate));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.Tk_IsActiveRow, data.X.Tk_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



