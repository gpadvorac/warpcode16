using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class WcTableColumnConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcTableColumnConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public WcTableColumnConcurrencyList(WcTableColumn_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcTableColumnConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Sort Order", data.C.TbC_SortOrder, data.X.TbC_SortOrder));
                _concurrencyList.Add(new ConcurrencyItem("Name", data.C.TbC_Name, data.X.TbC_Name));
                _concurrencyList.Add(new ConcurrencyItem("Control Type", data.C.TbC_CtlTp_Id, data.X.TbC_CtlTp_Id));
                _concurrencyList.Add(new ConcurrencyItem("Is Not vColumn", data.C.TbC_IsNonvColumn, data.X.TbC_IsNonvColumn));
                _concurrencyList.Add(new ConcurrencyItem("Description", data.C.TbC_Description, data.X.TbC_Description));
                _concurrencyList.Add(new ConcurrencyItem("SQL Data Type", data.C.TbC_DtSql_Id, data.X.TbC_DtSql_Id));
                _concurrencyList.Add(new ConcurrencyItem("DotNet Data Type", data.C.TbC_DtDtNt_Id, data.X.TbC_DtDtNt_Id));
                _concurrencyList.Add(new ConcurrencyItem("Data Length (0=MAX)", data.C.TbC_Length, data.X.TbC_Length));
                _concurrencyList.Add(new ConcurrencyItem("Precision", data.C.TbC_Precision, data.X.TbC_Precision));
                _concurrencyList.Add(new ConcurrencyItem("Scale", data.C.TbC_Scale, data.X.TbC_Scale));
                _concurrencyList.Add(new ConcurrencyItem("Is The Primary Key", data.C.TbC_IsPK, data.X.TbC_IsPK));
                _concurrencyList.Add(new ConcurrencyItem("Is Identity", data.C.TbC_IsIdentity, data.X.TbC_IsIdentity));
                _concurrencyList.Add(new ConcurrencyItem("Is The Foreign Key", data.C.TbC_IsFK, data.X.TbC_IsFK));
                _concurrencyList.Add(new ConcurrencyItem("Is Entity Column", data.C.TbC_IsEntityColumn, data.X.TbC_IsEntityColumn));
                _concurrencyList.Add(new ConcurrencyItem("Is System Field", data.C.TbC_IsSystemField, data.X.TbC_IsSystemField));
                _concurrencyList.Add(new ConcurrencyItem("Is Values Object Member", data.C.TbC_IsValuesObjectMember, data.X.TbC_IsValuesObjectMember));
                _concurrencyList.Add(new ConcurrencyItem("Is In Props Screen", data.C.TbC_IsInPropsScreen, data.X.TbC_IsInPropsScreen));
                _concurrencyList.Add(new ConcurrencyItem("Is In NavList", data.C.TbC_IsInNavList, data.X.TbC_IsInNavList));
                _concurrencyList.Add(new ConcurrencyItem("Is Required", data.C.TbC_IsRequired, data.X.TbC_IsRequired));
                _concurrencyList.Add(new ConcurrencyItem("Broken Rule Text", data.C.TbC_BrokenRuleText, data.X.TbC_BrokenRuleText));
                _concurrencyList.Add(new ConcurrencyItem("Allow Zero", data.C.TbC_AllowZero, data.X.TbC_AllowZero));
                _concurrencyList.Add(new ConcurrencyItem("Is Nullable In Db", data.C.TbC_IsNullableInDb, data.X.TbC_IsNullableInDb));
                _concurrencyList.Add(new ConcurrencyItem("Is Nullable In UI", data.C.TbC_IsNullableInUI, data.X.TbC_IsNullableInUI));
                _concurrencyList.Add(new ConcurrencyItem("Default Value", data.C.TbC_DefaultValue, data.X.TbC_DefaultValue));
                _concurrencyList.Add(new ConcurrencyItem("Is Read From Db", data.C.TbC_IsReadFromDb, data.X.TbC_IsReadFromDb));
                _concurrencyList.Add(new ConcurrencyItem("Is Send To Db", data.C.TbC_IsSendToDb, data.X.TbC_IsSendToDb));
                _concurrencyList.Add(new ConcurrencyItem("Is Insert Allowed", data.C.TbC_IsInsertAllowed, data.X.TbC_IsInsertAllowed));
                _concurrencyList.Add(new ConcurrencyItem("Is Edit Allowed", data.C.TbC_IsEditAllowed, data.X.TbC_IsEditAllowed));
                _concurrencyList.Add(new ConcurrencyItem("Is Read Only In The UI", data.C.TbC_IsReadOnlyInUI, data.X.TbC_IsReadOnlyInUI));
                _concurrencyList.Add(new ConcurrencyItem("Use For Audit", data.C.TbC_UseForAudit, data.X.TbC_UseForAudit));
                _concurrencyList.Add(new ConcurrencyItem("Default Caption", data.C.TbC_DefaultCaption, data.X.TbC_DefaultCaption));
                _concurrencyList.Add(new ConcurrencyItem("Column Header Text", data.C.TbC_ColumnHeaderText, data.X.TbC_ColumnHeaderText));
                _concurrencyList.Add(new ConcurrencyItem("Verbose Caption", data.C.TbC_LabelCaptionVerbose, data.X.TbC_LabelCaptionVerbose));
                _concurrencyList.Add(new ConcurrencyItem("Generate Label Caption", data.C.TbC_LabelCaptionGenerate, data.X.TbC_LabelCaptionGenerate));
                _concurrencyList.Add(new ConcurrencyItem("Show ToolTip In Grid Column", data.C.Tbc_ShowGridColumnToolTip, data.X.Tbc_ShowGridColumnToolTip));
                _concurrencyList.Add(new ConcurrencyItem("Show Props Tooltip", data.C.Tbc_ShowPropsToolTip, data.X.Tbc_ShowPropsToolTip));
                _concurrencyList.Add(new ConcurrencyItem("Create Props Resource Strings", data.C.TbC_IsCreatePropsStrings, data.X.TbC_IsCreatePropsStrings));
                _concurrencyList.Add(new ConcurrencyItem("Create Grid Col. Resource Strings", data.C.TbC_IsCreateGridStrings, data.X.TbC_IsCreateGridStrings));
                _concurrencyList.Add(new ConcurrencyItem("Is Available For Column Groups", data.C.TbC_IsAvailableForColumnGroups, data.X.TbC_IsAvailableForColumnGroups));
                _concurrencyList.Add(new ConcurrencyItem("Is TextWrap In Props", data.C.TbC_IsTextWrapInProp, data.X.TbC_IsTextWrapInProp));
                _concurrencyList.Add(new ConcurrencyItem("Is TextWrap In Grid", data.C.TbC_IsTextWrapInGrid, data.X.TbC_IsTextWrapInGrid));
                _concurrencyList.Add(new ConcurrencyItem("TextBox Format", data.C.TbC_TextBoxFormat, data.X.TbC_TextBoxFormat));
                _concurrencyList.Add(new ConcurrencyItem("TextColumn Format", data.C.TbC_TextColumnFormat, data.X.TbC_TextColumnFormat));
                _concurrencyList.Add(new ConcurrencyItem("Column Width", data.C.TbC_ColumnWidth, data.X.TbC_ColumnWidth));
                _concurrencyList.Add(new ConcurrencyItem("TextBox TextAlignment", data.C.TbC_TextBoxTextAlignment, data.X.TbC_TextBoxTextAlignment));
                _concurrencyList.Add(new ConcurrencyItem("Column TextAlignment", data.C.TbC_ColumnTextAlignment, data.X.TbC_ColumnTextAlignment));
                _concurrencyList.Add(new ConcurrencyItem("Stored Procedure", data.C.TbC_ListStoredProc_Id, data.X.TbC_ListStoredProc_Id));
                _concurrencyList.Add(new ConcurrencyItem("Is Static List", data.C.TbC_IsStaticList, data.X.TbC_IsStaticList));
                _concurrencyList.Add(new ConcurrencyItem("Use Not-In-List", data.C.TbC_UseNotInList, data.X.TbC_UseNotInList));
                _concurrencyList.Add(new ConcurrencyItem("Use List Edit Button", data.C.TbC_UseListEditBtn, data.X.TbC_UseListEditBtn));
                _concurrencyList.Add(new ConcurrencyItem("Parent Column Key", data.C.TbC_ParentColumnKey, data.X.TbC_ParentColumnKey));
                _concurrencyList.Add(new ConcurrencyItem("Use DisplayTextField Property", data.C.TbC_UseDisplayTextFieldProperty, data.X.TbC_UseDisplayTextFieldProperty));
                _concurrencyList.Add(new ConcurrencyItem("Is DisplayTextField Property", data.C.TbC_IsDisplayTextFieldProperty, data.X.TbC_IsDisplayTextFieldProperty));
                _concurrencyList.Add(new ConcurrencyItem("ComboList Table", data.C.TbC_ComboListTable_Id, data.X.TbC_ComboListTable_Id));
                _concurrencyList.Add(new ConcurrencyItem("ComboList DisplayText Column", data.C.TbC_ComboListDisplayColumn_Id, data.X.TbC_ComboListDisplayColumn_Id));
                _concurrencyList.Add(new ConcurrencyItem("Combo Max Dropdown Height", data.C.TbC_Combo_MaxDropdownHeight, data.X.TbC_Combo_MaxDropdownHeight));
                _concurrencyList.Add(new ConcurrencyItem("Combo Max Dropdown Width", data.C.TbC_Combo_MaxDropdownWidth, data.X.TbC_Combo_MaxDropdownWidth));
                _concurrencyList.Add(new ConcurrencyItem("Allow Dropdown Resizing", data.C.TbC_Combo_AllowDropdownResizing, data.X.TbC_Combo_AllowDropdownResizing));
                _concurrencyList.Add(new ConcurrencyItem("Is Reset Button Visible", data.C.TbC_Combo_IsResetButtonVisible, data.X.TbC_Combo_IsResetButtonVisible));
                _concurrencyList.Add(new ConcurrencyItem("Is Active Rec Column", data.C.TbC_IsActiveRecColumn, data.X.TbC_IsActiveRecColumn));
                _concurrencyList.Add(new ConcurrencyItem("Is Deleted Column", data.C.TbC_IsDeletedColumn, data.X.TbC_IsDeletedColumn));
                _concurrencyList.Add(new ConcurrencyItem("Is Created UserId Column", data.C.TbC_IsCreatedUserIdColumn, data.X.TbC_IsCreatedUserIdColumn));
                _concurrencyList.Add(new ConcurrencyItem("Is Created Date Column", data.C.TbC_IsCreatedDateColumn, data.X.TbC_IsCreatedDateColumn));
                _concurrencyList.Add(new ConcurrencyItem("Is Last Modified by UserId Column", data.C.TbC_IsUserIdColumn, data.X.TbC_IsUserIdColumn));
                _concurrencyList.Add(new ConcurrencyItem("Is Last Modified Date Column", data.C.TbC_IsModifiedDateColumn, data.X.TbC_IsModifiedDateColumn));
                _concurrencyList.Add(new ConcurrencyItem("Is Row Version Stamp Column", data.C.TbC_IsRowVersionStampColumn, data.X.TbC_IsRowVersionStampColumn));
                _concurrencyList.Add(new ConcurrencyItem("Is Browsable", data.C.TbC_IsBrowsable, data.X.TbC_IsBrowsable));
                _concurrencyList.Add(new ConcurrencyItem("Developer Note", data.C.TbC_DeveloperNote, data.X.TbC_DeveloperNote));
                _concurrencyList.Add(new ConcurrencyItem("User Note", data.C.TbC_UserNote, data.X.TbC_UserNote));
                _concurrencyList.Add(new ConcurrencyItem("Help File Additional Note", data.C.TbC_HelpFileAdditionalNote, data.X.TbC_HelpFileAdditionalNote));
                _concurrencyList.Add(new ConcurrencyItem("Notes", data.C.TbC_Notes, data.X.TbC_Notes));
                _concurrencyList.Add(new ConcurrencyItem("Completed - Ready for CodeGen", data.C.TbC_IsInputComplete, data.X.TbC_IsInputComplete));
                _concurrencyList.Add(new ConcurrencyItem("Is CodeGen", data.C.TbC_IsCodeGen, data.X.TbC_IsCodeGen));
                _concurrencyList.Add(new ConcurrencyItem("IsReadyCodeGen", data.C.TbC_IsReadyCodeGen, data.X.TbC_IsReadyCodeGen));
                _concurrencyList.Add(new ConcurrencyItem("IsCodeGenComplete", data.C.TbC_IsCodeGenComplete, data.X.TbC_IsCodeGenComplete));
                _concurrencyList.Add(new ConcurrencyItem("IsTagForCodeGen", data.C.TbC_IsTagForCodeGen, data.X.TbC_IsTagForCodeGen));
                _concurrencyList.Add(new ConcurrencyItem("IsTagForOther", data.C.TbC_IsTagForOther, data.X.TbC_IsTagForOther));
                _concurrencyList.Add(new ConcurrencyItem("ColumnGroups", data.C.TbC_ColumnGroups, data.X.TbC_ColumnGroups));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.TbC_IsActiveRow, data.X.TbC_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



