using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using EntityBll.SL;
using Ifx.SL;
using IfxUserViewLogSL;
using TypeServices;
using vUICommon;

namespace UIControls
{
    public partial class ucWcTableColumn
    {



        #region Initialize Variables

        private ucWcTableColumnGroupAssignment _ucTblColGrpAssign = null;


        #endregion Initialize Variables




        void CustomConstructionCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", IfxTraceCategory.Enter);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", IfxTraceCategory.Leave);
            }
        }




        #region Copy Code Gen Methods from the Codebehind class here



        public void SetStateFromParent(object ancestorId, string parentType, int? intParentId, Guid? guidParentId, object objParentId, int? intCurrentId, Guid? guidCurrentId, object objCurrentId, IBusinessObject currentBusinessObject, object[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 9
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Enter);
                _intParentId = intParentId;
                if (guidParentId == null)
                {
                    _guidParentId = objParentId as Guid?;
                }
                else
                {
                    _guidParentId = guidParentId;
                }
                
                _intCurrentId = intCurrentId;
                _guidCurrentId = guidCurrentId;
                //_guidCurrentId = objCurrentId as Guid?;
                //_oCurrentId = objCurrentId;

                _parentType = parentType;
                _currentBusinessObject = (WcTableColumn_Bll)currentBusinessObject;  

                ucNav.ParentType = _parentType;
                ucNav.GuidParentId = _guidParentId;  
                ucNav.ObjectParentId = objParentId;

                if (_FLG_IsLoaded == false)
                {
                    InitializeControl();
                }
                if (list == null)
                {
                    //  **********************************      N E E D   C U S T O M   C O D E    H E R E  ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! 
           
                    if (_guidParentId == null)
                    {
                        //ucNav.NavList_ItemSource.Clear();
                        NavListRefreshFromObjectArray(null);
                        ucNav.IsEnabled = false;
                    }
                    else
                    {
                        ucNav.IsEnabled = true;

                        _wcTableColumnProxy.Begin_WcTableColumn_GetListByFK((Guid)_guidParentId);
                        ucNav.navList.Cursor = Cursors.Wait;
                    }
                }
                else
                {
                    NavListRefreshFromObjectArray(list);
                }

                // Use    intParentId     if the parent is an Integer.  we might need to be using the Ancestor Id instead.
                if (guidParentId != null)
                {
                    ucNav.Set_vXamComboColumn_ItemSourcesWithParams();
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Leave);
            }
        }



        #endregion Copy Code Gen Methods from the Codebehind class here

        #region Insert Custom Code Here



        void ucTblColGrpAssign_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTblColGrpAssign_Load", IfxTraceCategory.Enter);
                if (_ucTblColGrpAssign == null)
                {
                    _ucTblColGrpAssign = new ucWcTableColumnGroupAssignment();
                    //_ucTblColGrpAssign.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    //_ucTblColGrpAssign.ChangeParentNavGridExpansion += ChangeParentNavGridExpansionFromNestedObject;
                    tbi_ucTbColGrpAsgn.Content = _ucTblColGrpAssign;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTblColGrpAssign_Load", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTblColGrpAssign_Load", IfxTraceCategory.Leave);
            }
        }


        #endregion Insert Custom Code Here

        #region  Combo DataSources and Events

        #endregion  Combo DataSources and Events

        #region Method Extentions For Custom Code


        void SyncControlsWithCurrentBusinessObject_CustomCode1()
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", IfxTraceCategory.Enter);


            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    _FLG_IsLoaded = true;
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", IfxTraceCategory.Leave);
            //}
        }

        void SyncControlsWithCurrentBusinessObject_CustomCode2()
            {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", IfxTraceCategory.Enter);


            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    _FLG_IsLoaded = true;
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", IfxTraceCategory.Leave);
            //}
        }

        #endregion Method Extentions For Custom Code



            #region Standard CodeGen Methods Subject to Customization



            #endregion Standard CodeGen Methods Subject to Customization



        }
    }
