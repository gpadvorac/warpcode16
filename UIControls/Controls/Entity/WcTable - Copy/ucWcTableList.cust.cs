using System;
using Ifx.SL;
using EntityBll.SL;
using TypeServices;
using Infragistics.Controls.Grids;
using vUICommon;
using Velocity.SL;
using vControls;
using Infragistics.Controls.Editors;
using System.Windows.Controls;
using Infragistics.Controls.Menus;

// Gen Timestamp:  9/1/2017 8:53:12 PM

namespace UIControls
{
    public partial class ucWcTableList
    {

        #region Initialize Variables

        bool _mnuChangeFilterType_AllowVisibility = true;
        bool _mnuClearFilters_AllowVisibility = true;  
        bool _mnuGridTools_AllowVisibility = true;
        bool _mnuRichGrid_AllowVisibility = true;
        bool _mnuSplitScreen_AllowVisibility = true;
        bool _isAllowNewRow = true;
        bool _mnuExcelExport_AllowVisibility = true;
        bool _gridDataSourceCombo_AllowVisibility = true;
        bool _menuGridRow_AllowVisibility = true;  // the area where the grid menu is located


        //ProxyWrapper.WcTableService_ProxyWrapper _wcTableProxy = null;

        #endregion Initialize Variables


        #region Constructor

        void CustomConstructionCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", IfxTraceCategory.Enter);

                _wcTableProxy = new ProxyWrapper.WcTableService_ProxyWrapper();
                _wcTableProxy.WcTable_GetByIdCompleted += WcTable_GetByIdCompleted;
                _wcTableProxy.WcTable_GetListByFKCompleted += WcTable_GetListByFKCompleted;
                _wcTableProxy.WcTable_GetAllCompleted += WcTable_GetAllCompleted;

                InitializeViewLogItem();

                //_FilterType = FilterUIType.FilterRowTop;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructor


        #region Security

        UserSecurityContext _userContext = new UserSecurityContext();
        //***  Add the ACTUAL Guid below, and then delete this comment
        private Guid _ancestorSecurityId = Guid.NewGuid();
        ControlCache cCache;
        EntityCache eCache = null;
        bool defaultEntityOperationPermission = true;
        bool _secuitySettingIsReadOnly = false;
        //***  Add the ACTUAL Guid below, and then delete this comment
        Guid _controlId = Guid.NewGuid();
        bool _isDeleteActionAllowed = false;
        Guid _deleteActionId = Guid.NewGuid();  // Hard code the actual guid from the security tool


        public void SetSecurityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Enter);

                _userContext.LoadArtifactPermissions(_ancestorSecurityId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Leave);
            }
        }

        void UserSecurityContext_SecurityArtifactsRetrieved(object sender, SecurityArtifactsRetrievedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Enter);
                SecurityCache.AddControlCacheForAncestor(e.ArtifactAncestorData);
                cCache = SecurityCache.GetControlGroupById(e.ArtifactAncestorData.Id);

                UiControlStateSetting setting = cCache.GetControlById(_controlId).Setting;
                if (setting == UiControlStateSetting.Enable)
                {
                    _secuitySettingIsReadOnly = false;
                }
                else
                {
                    _secuitySettingIsReadOnly = true;
                    navList_SplitScreenMode();
                }

                //_isDeleteActionAllowed = SecurityCache.IsEnabled(_deleteActionId);
                //if (_isDeleteActionAllowed)
                //{
                //    navList.DeleteKeyAction = DeleteKeyAction.DeleteSelectedRows;
                //}

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Leave);
            }
        }

        #endregion Security

        #region Method Extentions For Custom Code


        public void Set_vXamComboColumn_ItemSourcesWithParams()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", IfxTraceCategory.Enter);
                
                //_leaseProxy.Begin_GetLeaseAltNumberSource_ComboItemList((Guid)_prj_Id);

//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", IfxTraceCategory.Leave);
//            }
        }

        #region Combo ItemsSource for Non-Static Lists




        #endregion region Combo ItemsSource for Non-Static Lists



        void vXamComboColumn_SelectionChanged_CustomCode(object sender, vXamComboColumnSelectionChangedEventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void vXamMultiColumnComboColumn_SelectionChanged_CustomCode(object sender, vXamMultiColumnComboColumnSelectionChangedEventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Leave);
//            }
            }


        void vDatePickerColumn_TextChanged_CustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void XamColorPicker_SelectedColorChanged_CustomCode(XamColorPicker ctl, SelectedColorChangedEventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void vCheckColumn_CheckedChanged_CustomCode(vCheckBox ctl, IBusinessObject obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void vTextColumn_TextChanged_CustomCode(TextBox ctl, IBusinessObject obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void vDecimalColumn_TextChanged_CustomCode(TextBox ctl, IBusinessObject obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void navList_RowEnteredEditMode_Custom(object sender, EditingRowEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", IfxTraceCategory.Enter);
                WcTable_Bll obj = e.Row.Data as WcTable_Bll;
                if (obj == null) { return; }

                        // For multi parets
////                if (_guidParentId == null)
////                {
////                    throw new Exception(_as + "." + _cn + ".navList_RowEnteredEditMode:  _guidParentId  was null.  _guidParentId must be used for the FK.");
////                }
//                //obj.StandingFK = (Guid)_prj_Id;    // added this line
//
//                //switch (_parentType)
//                //{
//                //    case "ucWell":
//                //        // this might not ever get his.
//                //        break;
//                //    case "ucWellDt":
//                //        obj.Current.WlD_Id_noevents = _guidParentId;
//                //        break;
//                //    case "ucContractDt":
//                //        obj.Current.CtD_Id_noevents = _guidParentId;
//                //        break;
//                //}

                //  Or For a Fixed Single Parent
                _isRowInEditMode = true;
                if (obj.State.IsNew() == true)
                {
                    SetNewRowValidation();
                }
                obj.Current.Tb_ApVrsn_Id_noevents = _guidParentId;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", IfxTraceCategory.Leave);
            }
        }


        void navList_SelectedRowsCollectionChanged_Custom(object sender, SelectionCollectionChangedEventArgs<SelectedRowsCollection> e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", IfxTraceCategory.Leave);
//            }
        }




        void ConfigureColumnHeaderTooltips_CustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void XamMenuItem_Click_CustomCode(object sender, EventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click_CustomCode", IfxTraceCategory.Enter);
//

            //    XamMenuItem mnu = (XamMenuItem)sender;
            //    switch (mnu.Name)
            //    {
            //        case "xxxxxxxxxx":
            //            xxxxxxxxxxxxx();
            //            break;
            //    }

//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click_CustomCode", IfxTraceCategory.Leave);
//            }
        }



        #endregion Method Extentions For Custom Code


        #region CodeGen Methods for modification


        public void SetStateFromParent(object ancestorId, string parentType, int? intParentId, Guid? guidParentId, object objParentId, int? intCurrentId, Guid? guidCurrentId, object objCurrentId, IBusinessObject currentBusinessObject, IEntity_ValuesMngr[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 7
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Enter);
                _intParentId = intParentId;
                _guidParentId = guidParentId;
                _guidParentId = objParentId as Guid?;
                
                _intCurrentId = intCurrentId;
                _guidCurrentId = guidCurrentId;
                _guidCurrentId = objCurrentId as Guid?;

                _parentType = parentType;
                _newText = newText;
                if (null != list)
                {
                    NavList_ItemSource.ReplaceList(list);
                }    
                else
                {
                    if (_guidParentId == null)
                    {
                        NavListRefreshFromObjectArray(null);
                        navList.IsEnabled = false;
                    }
                    else
                    {
                        //  Add code here for multiple parent types
                        navList.IsEnabled = true;
                        _wcTableProxy.Begin_WcTable_GetListByFK((Guid)_guidParentId);
                    }
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Leave);
            }
        }


        #endregion CodeGen Methods for modification


        #region Custom Code



        #region Button Columns

        private void btnOpenAttaments_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenAttaments_Click", IfxTraceCategory.Enter);

                //_task = (Task_Bll)e.Data;

                //var ucFP = new ucFilePopup { DialogMode = vDialogMode.Unrestricted };
                //ucFP.SetStateFromParent(null, "Task", null, _task.Tk_Id, null, null, null, null, null);

                //var attachmentDialog = vDialogHelper.InitializeAttachmentDialog(ucFP, "Task", _task.Tk_Number.ToString());

                //ucFP.ClosePopup += (o, args) =>
                //{
                //    _task.GetEntityRow(_task.Tk_Id);
                //    attachmentDialog.Close();
                //};

                //attachmentDialog.Show();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenAttaments_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenAttaments_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnOpenDiscussions_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenDiscussions_Click", IfxTraceCategory.Enter);

                //_task = (Task_Bll)e.Data;
                //_discussionProxy.Begin_GetDiscussion_lstBy_Task(_task.Tk_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenDiscussions_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenDiscussions_Click", IfxTraceCategory.Leave);
            }
        }

        #region Button Column Callbacks

        void _discussionProxy_GetDiscussion_lstBy_TaskCompleted(object sender, GetDiscussion_lstBy_TaskCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_discussionProxy_GetDiscussion_lstBy_TaskCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                var array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                //var discussionDialog = vDialogHelper.InitializeDiscussionDialog(array, vDialogMode.Unrestricted, _task.Tk_Id,
                //    ApplicationLevelVariables.DiscussionParentTypes.Task, _task.Tk_Number.ToString());

                //if (discussionDialog.ShowDialog() == true)
                //{
                //    discussionDialog.Result.RequestClose += (_, __) =>
                //    {
                //        _task.GetEntityRow(_task.Tk_Id);
                //        discussionDialog = null;
                //    };
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_discussionProxy_GetDiscussion_lstBy_TaskCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_discussionProxy_GetDiscussion_lstBy_TaskCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Button Column Callbacks






        private void btnOpenEntityInNewWindow_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenEntityInNewWindow_Click", IfxTraceCategory.Enter);

                //var task = (Task_Bll)e.Data;

                //if (task.Ct_Id != null)
                //{
                //    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucContract", "ucContract", (Guid)task.Ct_Id, "Section for Task #" + task.Tk_Number.ToString());
                //}
                //else if (task.CtD_Id != null)
                //{
                //    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucContractDt", "ucContractDt", (Guid)task.CtD_Id, "Section Detail for Task #" + task.Tk_Number.ToString());
                //}
                //else if (task.Df_Id != null)
                //{
                //    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucDefect", "ucDefect", (Guid)task.Df_Id, "Issue for Task #" + task.Tk_Number.ToString());
                //}
                //else if (task.Ls_Id != null)
                //{
                //    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucLease", "ucLease", (Guid)task.Ls_Id, "Agreement for Task #" + task.Tk_Number.ToString());
                //}
                //else if (task.LsTr_Id != null)
                //{
                //    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucLeaseTract", "ucLeaseTract", (Guid)task.LsTr_Id, "Tract for Task #" + task.Tk_Number.ToString());
                //}
                //else if (task.Ob_Id != null)
                //{
                //    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucObligation", "ucObligation", (Guid)task.Ob_Id, "Obligation/Provision for Task #" + task.Tk_Number.ToString());
                //}
                //else if (task.Pp_Id != null)
                //{
                //    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucProspect", "ucProspect", (Guid)task.Pp_Id, "Prospect for Task #" + task.Tk_Number.ToString());
                //}
                //else if (task.Wl_Id != null)
                //{
                //    ApplicationLevelVariables.MainPg.OpenEntityInPopup("ucWell", "ucWell", (Guid)task.Wl_Id, "Pipeline for Task #" + task.Tk_Number.ToString());
                //}
                //else
                //{
                //    MessageBox.Show("This task a 'Project Level' task and therefore, this option is not available.",
                //        "Option Not Available", MessageBoxButton.OK);
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenEntityInNewWindow_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenEntityInNewWindow_Click", IfxTraceCategory.Leave);
            }
        }




        #endregion Button Columns



        #endregion Custom Code


        #region Fetch Data



        #endregion Fetch Data



    }
}
