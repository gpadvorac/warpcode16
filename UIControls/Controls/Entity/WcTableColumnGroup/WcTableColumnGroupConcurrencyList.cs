using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class WcTableColumnGroupConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcTableColumnGroupConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public WcTableColumnGroupConcurrencyList(WcTableColumnGroup_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcTableColumnGroupConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Sort Order", data.C.TbCGrp_SortOrder, data.X.TbCGrp_SortOrder));
                _concurrencyList.Add(new ConcurrencyItem("Column Group Name", data.C.TbCGrp_Name, data.X.TbCGrp_Name));
                _concurrencyList.Add(new ConcurrencyItem("Description", data.C.TbCGrp_Desc, data.X.TbCGrp_Desc));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.TbCGrp_IsActiveRow, data.X.TbCGrp_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



