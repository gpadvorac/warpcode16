using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using EntityBll.SL;
using Ifx.SL;
using IfxUserViewLogSL;
using TypeServices;
using vUICommon;

namespace UIControls
{
    public partial class ucWcTableColumnGroup
    {

  

        #region Initialize Variables



        #endregion Initialize Variables




        void CustomConstructionCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", IfxTraceCategory.Enter);


                _wcTableColumnGroupProxy.GetWcTableColumnGroup_lstByParentIdCompleted += _wcTableColumnGroupProxy_GetWcTableColumnGroup_lstByParentIdCompleted;



            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", IfxTraceCategory.Leave);
            }
        }





        #region Copy Code Gen Methods from the Codebehind class here



        public void SetStateFromParent(object ancestorId, string parentType, int? intParentId, Guid? guidParentId, object objParentId, int? intCurrentId, Guid? guidCurrentId, object objCurrentId, IBusinessObject currentBusinessObject, object[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 9
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Enter);
                _intParentId = intParentId;
                _guidParentId = guidParentId;
                _guidParentId = objParentId as Guid?;
                
                _intCurrentId = intCurrentId;
                _guidCurrentId = guidCurrentId;
                //_guidCurrentId = objCurrentId as Guid?;
                //_oCurrentId = objCurrentId;

                _parentType = parentType;
                _currentBusinessObject = (WcTableColumnGroup_Bll)currentBusinessObject;  

                ucNav.ParentType = _parentType;
                ucNav.GuidParentId = _guidParentId;  
                ucNav.ObjectParentId = objParentId;

                if (_FLG_IsLoaded == false)
                {
                    InitializeControl();
                }
                if (list == null)
                {
                    //  **********************************      N E E D   C U S T O M   C O D E    H E R E  ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! 
           
                    //if (_guidParentId == null)
                    //{
                    //    //ucNav.NavList_ItemSource.Clear();
                    //    NavListRefreshFromObjectArray(null);
                    //    ucNav.IsEnabled = false;
                    //}
                    //else
                    //{
                        ucNav.IsEnabled = true;
                    switch (parentType)
                    {
                        case "ucWcTable":
                            _wcTableColumnGroupProxy.Begin_GetWcTableColumnGroup_lstByParentId(null, _guidParentId);
                            break;
                        case "ucWcApplicationVersion":
                            _wcTableColumnGroupProxy.Begin_GetWcTableColumnGroup_lstByParentId(_guidParentId, null);
                            break;
                    }
                    ucNav.navList.Cursor = Cursors.Wait;
                //    }
                }
                else
                {
                    NavListRefreshFromObjectArray(list);
                }

                // Use    intParentId     if the parent is an Integer.  we might need to be using the Ancestor Id instead.
                if (guidParentId != null)
                {
                    ucNav.Set_vXamComboColumn_ItemSourcesWithParams();
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Creates a new instance of the business object (<see cref="EntityBll.WcTableColumnGroup_Bll">WcTableColumnGroup_Bll</see>) and sets it as the <see cref="CurrentBusinessObject">CurrentBusinessObject</see> as which ucProps will be bound
        ///     to. If this control is using <see cref="ucWcTableColumnGroupList">ucWcTableColumnGroupList</see>, then the
        ///     new instance of <see cref="EntityBll.WcTableColumnGroup_Bll">WcTableColumnGroup_Bll</see> will also be added
        ///     to <see cref="_list">_list</see>.
        /// </summary>
        public void NewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Enter);
                // If we arleady have a new row, dont allow adding a 2nd one.
                if (ucNav.NavList_ItemSource.Count > 0 && ucNav.FindNewRow() == true)
                {
                    return;
                }

                WcTableColumnGroup_Bll obj = new WcTableColumnGroup_Bll();
                obj.NewEntityRow();

                obj.SetStandingFK(_parentType, (Guid)_guidParentId);

                CurrentBusinessObject = obj;
                //**  FIX  or  DELETE this.
                //CurrentBusinessObject.StandingFK = (Guid)_prj_Id;

                ucNav.NavList_ItemSource.Add(CurrentBusinessObject);
                // Activate new rec in list which will fire evenst to pass the new business object into the props control
                ucNav.ActivateNewRecord();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Leave);
            }
        }





        #endregion Copy Code Gen Methods from the Codebehind class here

        #region Insert Custom Code Here


        private void _wcTableColumnGroupProxy_GetWcTableColumnGroup_lstByParentIdCompleted(object sender, GetWcTableColumnGroup_lstByParentIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcTableColumnGroupProxy_GetWcTableColumnGroup_lstByParentIdCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                NavListRefreshFromObjectArray(array);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcTableColumnGroupProxy_GetWcTableColumnGroup_lstByParentIdCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcTableColumnGroupProxy_GetWcTableColumnGroup_lstByParentIdCompleted", IfxTraceCategory.Leave);
                ucNav.navList.Cursor = Cursors.Arrow;
            }
        }







        #endregion Insert Custom Code Here

        #region  Combo DataSources and Events

        #endregion  Combo DataSources and Events

        #region Stubs from CodeGen Codebehind


        #endregion  Stubs from CodeGen Codebehind



        #region Standard CodeGen Methods Subject to Customization



        #endregion Standard CodeGen Methods Subject to Customization



    }
}
