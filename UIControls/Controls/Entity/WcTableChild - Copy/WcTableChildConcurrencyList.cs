using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class WcTableChildConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcTableChildConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public WcTableChildConcurrencyList(WcTableChild_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcTableChildConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Sort Order", data.C.TbChd_SortOrder, data.X.TbChd_SortOrder));
                _concurrencyList.Add(new ConcurrencyItem("Child Table", data.C.TbChd_Child_Tb_Id, data.X.TbChd_Child_Tb_Id));
                _concurrencyList.Add(new ConcurrencyItem("Foreign Key Column Name", data.C.TbChd_ChildForeignKeyColumn_Id, data.X.TbChd_ChildForeignKeyColumn_Id));
                _concurrencyList.Add(new ConcurrencyItem("Display As Child Grid", data.C.TbChd_IsDisplayAsChildGrid, data.X.TbChd_IsDisplayAsChildGrid));
                _concurrencyList.Add(new ConcurrencyItem("Display As Child Tab", data.C.TbChd_IsDisplayAsChildTab, data.X.TbChd_IsDisplayAsChildTab));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.TbChd_IsActiveRow, data.X.TbChd_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



