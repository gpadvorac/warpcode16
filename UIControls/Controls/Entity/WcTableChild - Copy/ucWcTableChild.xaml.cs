using System;
using System.Windows.Controls;
using TypeServices;
using EntityBll.SL;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using Ifx.SL;
using vUICommon;
using Velocity.SL;
using vDP;
using ApplicationTypeServices;
using SLcompression;
using System.Windows.Input;

// Gen Timestamp:  12/2/2016 4:08:59 PM

namespace UIControls
{

    /// <summary>
    /// 	<para><strong>About this Entity:</strong></para>
    /// 	<para>***General description of the entity from WC***</para>
    /// 	<para></para><br/>
    /// 	<para><strong>About this Control:</strong></para>
    /// 	<para>
    ///         Known as the “<a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/The_Entity_Manager.html">Entity
    ///         Manager</a>”, ucWcTableChild manages the UI operations for this entity – WcTableChild, by
    ///         managing the events and user interaction between the navigation list
    ///         (<see cref="ucWcTableChildList">ucWcTableChildList</see>) and the data entry screen
    ///         (<see cref="ucWcTableChildProps">ucWcTableChildProps</see>).<br/>
    ///         Additionally, an Entity Manager may have nested child entity managers. For
    ///         example, a ‘Customer’ Entity Manager may have a nested child Orders Entity
    ///         Manager, Contacts Entity Manager, Addresses Entity Manager, Phone Entity
    ///         Manager, and so one. Likewise each of the nested Entity Managers may also have
    ///         an unlimited number of nested Entity Managers where the customer’s contacts
    ///         could also have addresses and phone numbers, etc. For more information about
    ///         the Entity Manager and how this class is broken into partial classes, please
    ///         see Entity Manager for information about how it can be used and the structure
    ///         of its partial classes.
    ///     </para>
    /// </summary>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Entity_Design_Pattern_In_The_UI_Layer.html" cat="Framework and Design Pattern">Entity Design Pattern In The UI Layer</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntity.html" cat="Framework and Design Pattern">ucEntity</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntityList.html" cat="Framework and Design Pattern">ucEntityList</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntityProps.html" cat="Framework and Design Pattern">ucEntityProps</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Tracing_Overview.html" cat="Framework and Design Pattern">Tracing Overview</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Exception_Handling_Overview.html" cat="Framework and Design Pattern">Exception Handling Overview</seealso>
    public partial class ucWcTableChild : UserControl, IEntityControl
    {

        #region Initialize Variables


        private static string _as = "UIControls";

        private static string _cn = "ucWcTableChild";

        string _parentType;
 
        Guid? _guidParentId;
        Int64? _longParentId;
        Int32? _intParentId;
        Int16? _shortParentId;
        Byte? _byteParentId;

        Guid? _guidCurrentId;
        Int64? _longCurrentId;
        Int32? _intCurrentId;
        Int16? _shortCurrentId;
        Byte? _byteCurrentId;
        object _oCurrentId;

		Guid? _parentId;

        /// <summary>
        /// A flag that is false until the control has finished loading. Some methods execute
        /// while the control is loading as well as during normal operations, however, sometimes
        /// they require different behavior when is loading versus normal operations.
        /// </summary>
        bool _FLG_IsLoaded = false;
        /// <summary>
        /// 	<para>A flag telling us if this is the active entity control. A complex screen can
        ///     have many entity controls each with additional nested entity controls. Only one
        ///     entity control can be active at a time. When a user clicks or tabs into a,
        ///     EntityList, EntityProps, or any other child control of an entity control, this flag
        ///     is set to true. As code bubbles up or tunnels down through the many layers of WPF
        ///     elements, its often important to know when its entering the active entity
        ///     control.</para>
        /// 	<para>
        ///         Also see <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> which bubbles
        ///         up to the top level element (typically the main window) and passes a reference
        ///         up a reference of the active entity control.
        ///     </para>
        /// </summary>
        private bool _isActiveEntityControl = false;
        /// <summary>
        /// 	<para>
        ///         Member of <see cref="TypeServices.IEntityControl">IEntityControl</see>. A flag
        ///         telling is if the Properties tab is currently selected which means the
        ///         properties are visible.
        ///     </para>
        /// 	<para>A common UI layout design has the EntityList control on the left side, and a
        ///     tab control to the right. One of the tabs will contain the EntityProps control and
        ///     other tabs could contain other things related to the entity including child Entity
        ///     controls.</para>
        /// 	<para>
        ///         Sometimes when certain events fire, we need a quick way to know if the
        ///         Properties are the visible pain or screen. See also <see cref="SyncControlsWithCurrentBusinessObject(string)">SyncControlsWithCurrentBusinessObject</see>.
        ///     </para>
        /// </summary>
        bool _isPropsTabSelected = false;
        /// <summary>
        ///     Holds a reference to an instance of <see cref="ucWcTableChildProps">ucWcTableChildProps</see>
        ///     (used for the properties screen). This is not instantiated when the entity control
        ///     is initialized because there’s a chance it will never by use or seen depending on
        ///     this entity control’s configuration and how it’s being used. ucProps is
        ///     instantiated in the “<see cref="SetCurrent">SetCurrent</see>” method sometimes when
        ///     the properties tab is clicked at which time ucProps must be loaded into the tab, or
        ///     from the “<see cref="SetStateFromParent">SetStateFromParent</see>” method depending
        ///     on the charecotristics of the application configuratoin.
        /// </summary>
        ucWcTableChildProps ucProps = null;

        public event CrudFailedEventHandler CrudFailed;

        /// <summary>
        ///     See <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> for
        ///     information.
        /// </summary>
        public event CurrentEntityStateEventHandler CurrentEntityStateChanged;

        /// <summary>See <see cref="OnBrokenRuleChanged">OnBrokenRuleChanged</see> for information.</summary>
        public event BrokenRuleEventHandler BrokenRuleChanged;

        public event CollapseExpandAllParentNavGridsEventHandler ChangeParentNavGridExpansion;

        /// <summary>
        ///     A reference to the current business object (<see cref="EntityBll.WcTableChild_Bll">WcTableChild_Bll</see>). This would be the object bound to the
        ///     selected row in <see cref="ucWcTableChildList">ucWcTableChildList</see> and the same business
        ///     object populating ucWcTableChildProps.
        /// </summary>
        WcTableChild_Bll _currentBusinessObject = null;
        IBusinessObjectV2 _currentChildBusinessObject = null;

        System.Windows.GridLength _navColumnWidth;

        ProxyWrapper.WcTableChildService_ProxyWrapper _wcTableChildProxy = null;

        UserSecurityContext _userContext = new UserSecurityContext();

        #endregion Initialize Variables

        #region Constructors

        /// <summary>
        ///     Entity Manager, a control that manages the UI operations for this entity – WcTableChild.
        ///     ucWcTableChild manages the events and user interaction between the navigation list
        ///     ucWcTableChildList and ucWcTableChildProps (the data entry control for the entity WcTableChild). Other
        ///     child controls including other entity managers can also be nested in this
        ///     control.</para>
        /// </summary>
        public ucWcTableChild()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcTableChild", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();
                ucNav.NavigationListSelectedItemChanged += new NavigationListSelectedItemChangedEventHandler(ucNav_NavigationListSelectedItemChanged);
                ucNav.SplitScreenModeChanged += new SplitScreenModeChangedEventHandler(ucNav_SplitScreenModeChanged);
                gdSplitter.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(gdSplitter_MouseLeftButtonUp);
                this.Name = "WcTableChild";
                _navColumnWidth = new System.Windows.GridLength(232);
                tbcWcTableChild.SelectionChanged += new SelectionChangedEventHandler(MainTabControl_SelectionChanged);
                // Initialize proxy
                _wcTableChildProxy = new ProxyWrapper.WcTableChildService_ProxyWrapper();
                _wcTableChildProxy.WcTableChild_GetByIdCompleted += WcTableChild_GetByIdCompleted;
                _wcTableChildProxy.WcTableChild_GetListByFKCompleted += WcTableChild_GetListByFKCompleted;
                _wcTableChildProxy.WcTableChild_GetAllCompleted += WcTableChild_GetAllCompleted;
                _wcTableChildProxy.WcTableChild_SetIsDeletedCompleted += WcTableChild_SetIsDeletedCompleted;

                // Initialize the split screen mode and read only modes here.  you can change this here, or someday we might be able to do it via xaml or a config file.
                ucNav.InitializeSplitScreenAndReadOnlyModes(true, true, false, true);

                _userContext.SecurityArtifactsRetrieved += new SecurityArtifactsRetrievedEventHandler(UserSecurityContext_SecurityArtifactsRetrieved);
                SetSecurityState();

                ucNav.ChangeParentNavGridExpansion += ChangeParentNavGridExpansionFromNestedObject;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcTableChild", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcTableChild", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        #region Security

        private Guid _ancestorSecurityId = new Guid("59b35b3f-5d3c-4116-bd4a-6cc76bca4cea");
        ControlCache cCache;
        EntityCache eCache = null;
        bool defaultEntityOperationPermission = true;


        private void SetSecurityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityStateWcTableChild", IfxTraceCategory.Enter);
                _userContext.LoadArtifactPermissions(_ancestorSecurityId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityStateWcTableChild", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityStateWcTableChild", IfxTraceCategory.Leave);
            }
        }

        void UserSecurityContext_SecurityArtifactsRetrieved(object sender, SecurityArtifactsRetrievedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrievedWcTableChild", IfxTraceCategory.Enter);

                SecurityCache.AddControlCacheForAncestor(e.ArtifactAncestorData);
                cCache = SecurityCache.GetControlGroupById(e.ArtifactAncestorData.Id);

                //// Lease Props
                //DP.SetControlSecurityId(tbiLeaseProps, new Guid("f02b70cc-2eba-4ab9-bd6a-dbae7022178b"));
                //SecurityCache.SetWPFActionControlState(tbiLeaseProps, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //// Lease Tracts  (hidden by default for eog)
                //DP.SetControlSecurityId(tbiTracts, new Guid("496c56e6-1c53-4e05-90c9-f29a6b5d7669"));
                //SecurityCache.SetWPFActionControlState(tbiTracts, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //tbcLease.SelectionChanged -= new SelectionChangedEventHandler(MainTabControl_SelectionChanged);
                //if (tbiTracts.Visibility == System.Windows.Visibility.Collapsed)
                //{
                //    tbiObligations.IsSelected = true;
                //}
                //else
                //{
                //    if (tbiLeaseProps.Visibility == System.Windows.Visibility.Collapsed)
                //    {
                //        tbiTracts.IsSelected = true;
                //    }
                //}
                //tbcLease.SelectionChanged += new SelectionChangedEventHandler(MainTabControl_SelectionChanged);

                ////***
                //ucProps.SecuitySettingIsReadOnly = SecurityCache.IsViewOnly(ucProps.ControlId);
                //***
                if (ucProps.SecuitySettingIsReadOnly)
                {
                    btnNew.Visibility = System.Windows.Visibility.Collapsed;
                    btnSave.Visibility = System.Windows.Visibility.Collapsed;
                    btnUnDo.Visibility = System.Windows.Visibility.Collapsed;
                    btnDelete.Visibility = System.Windows.Visibility.Collapsed;
                }
                else
                {
                    btnNew.Visibility = System.Windows.Visibility.Visible;
                    btnSave.Visibility = System.Windows.Visibility.Visible;
                    btnUnDo.Visibility = System.Windows.Visibility.Visible;
                    if (ApplicationLevelVariables.Flag_DeleteDataAllowed == true)
                    {
                        btnDelete.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        btnDelete.Visibility = System.Windows.Visibility.Collapsed;
                    }
                }

                //***
                //ucProps.FieldReadOnlySettings();


                //// Let ucNav configure it's security now.  it will use the same cache just downloaded.
                //ucNav.SetSecurityState();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Leave);
            }
        }


        #endregion Security


        #region Load this

        public void InitializeSplitScreenConfigureation(bool isSplitSreenMode)
        {
            ucNav.IsSplitSreenMode = isSplitSreenMode;
        }

        public void InitializeSplitScreenAndReadOnlyModes(bool isSplit, bool allowSplit, bool isReadOnly, bool isAllowNewRow)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeSplitScreenAndReadOnlyModes", IfxTraceCategory.Enter);
                ucNav.InitializeSplitScreenAndReadOnlyModes(isSplit, allowSplit, isReadOnly, isAllowNewRow);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeSplitScreenAndReadOnlyModes", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeSplitScreenAndReadOnlyModes", IfxTraceCategory.Leave);
            }
        }


        public void SetStateFromParent(object ancestorId, string parentType, int? intParentId, Guid? guidParentId, object objParentId, int? intCurrentId, Guid? guidCurrentId, object objCurrentId, IBusinessObject currentBusinessObject, object[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 9
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Enter);
                _intParentId = intParentId;
                _guidParentId = guidParentId;
                _guidParentId = objParentId as Guid?;
                
                _intCurrentId = intCurrentId;
                _guidCurrentId = guidCurrentId;
                _guidCurrentId = objCurrentId as Guid?;
                _oCurrentId = objCurrentId;

                _parentType = parentType;
                _currentBusinessObject = (WcTableChild_Bll)currentBusinessObject;  

                ucNav.ParentType = _parentType;
                ucNav.GuidParentId = _guidParentId;  
                ucNav.ObjectParentId = objParentId;

                if (_FLG_IsLoaded == false)
                {
                    InitializeControl();
                }
                if (list == null)
                {
                    //  **********************************      N E E D   C U S T O M   C O D E    H E R E  ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! 
           
                    if (_guidParentId == null)
                    {
                        //ucNav.NavList_ItemSource.Clear();
                        NavListRefreshFromObjectArray(null);
                        ucNav.IsEnabled = false;
                    }
                    else
                    {
                        ucNav.IsEnabled = true;

                        _wcTableChildProxy.Begin_WcTableChild_GetListByFK((Guid)_guidParentId);

                        ucProps.LoadListsFilteredByParentId((Guid)_guidParentId);

                        ucNav.navList.Cursor = Cursors.Wait;
                    }
                }
                else
                {
                    NavListRefreshFromObjectArray(list);
                }

                // Use    intParentId     if the parent is an Integer.  we might need to be using the Ancestor Id instead.
                if (guidParentId != null)
                {
                    ucNav.Set_vXamComboColumn_ItemSourcesWithParams();
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Leave);
            }
        }

        public void SetStateFromParent(object ancestorId, string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, IBusinessObject currentBusinessObject, object[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 8
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 8", IfxTraceCategory.Enter);

                SetStateFromParent(null, parentType, intParentId, guidParentId, null, intId, guidId, null, currentBusinessObject, list, newText);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 8", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 8", IfxTraceCategory.Leave);
            }
        }

        public void SetStateFromParent(string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, IBusinessObject currentBusinessObject, object[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 7
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Enter);

                SetStateFromParent(null, parentType, intParentId, guidParentId, intId, guidId, currentBusinessObject, list, newText);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Leave);
            }
        }

        public void SetStateFromParent(int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 1
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 1", IfxTraceCategory.Enter);
                SetStateFromParent("", intParentId, guidParentId, intId, guidId, null, null, "");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 1", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 1", IfxTraceCategory.Leave);
            }
        }

        public void SetStateFromParent(string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 2
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 2", IfxTraceCategory.Enter);
                SetStateFromParent(parentType, intParentId, guidParentId, intId, guidId, null, null, "");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 2", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 2", IfxTraceCategory.Leave);
            }
        }

        public void SetStateFromParent(string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 3
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 3", IfxTraceCategory.Enter);
                SetStateFromParent(parentType, intParentId, guidParentId, intId, guidId, null, null, newText);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 3", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 3", IfxTraceCategory.Leave);
            }
        }

        public void SetStateFromParent(int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, IEntity_ValuesMngr[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 4
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 4", IfxTraceCategory.Enter);
                SetStateFromParent("", intParentId, guidParentId, intId, guidId, null, list, newText);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 4", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 4", IfxTraceCategory.Leave);
            }
        }

        public void SetStateFromParent(string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, IEntity_ValuesMngr[] list)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 5
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 5", IfxTraceCategory.Enter);
                SetStateFromParent(parentType, intParentId, guidParentId, intId, guidId, null, list, "");

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 5", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 5", IfxTraceCategory.Leave);
            }
        }

        public void SetStateFromParent(string parentType, int? intParentId, Guid? guidParentId, int? intId, Guid? guidId, IEntity_ValuesMngr[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 6
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 6", IfxTraceCategory.Enter);
                SetStateFromParent(parentType, intParentId, guidParentId, intId, guidId, null, list, newText);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 6", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 6", IfxTraceCategory.Leave);
            }
        }

        void InitializeControl()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeControl", IfxTraceCategory.Enter);
                ucProps_Load();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeControl", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeControl", IfxTraceCategory.Leave);
            }
        }

        void NavListRefreshFromObjectArray(object[] data)
        {
            // This method is usually a followup from calling SetStateFromParent and after the web service has returned.
            // Its also ALWAYS called by the web service reply when returning a new list for the grid.
            // Now reset the list and see to it that any other screens as reset accordingly.
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", IfxTraceCategory.Enter);
                if (data != null)
                {
                    ucNav.NavList_ItemSource = null;
                    ucNav.NavList_ItemSource = new WcTableChild_List();
                    // Sometimes this is still null because it hasn't loaded in the UI yet
                    if (ucNav.NavList_ItemSource != null)
                    {
                        ucNav.NavList_ItemSource.ReplaceList(data);
                    }
                }
                else
                {
                    if (ucNav.NavList_ItemSource != null)
                    {
                        ucNav.NavList_ItemSource.Clear();
                    }
                }
                SyncControlsWithCurrentBusinessObject();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", IfxTraceCategory.Leave);
            }
        }

        #endregion  Load this


        #region Load Controls

        void ucProps_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProps_Load", IfxTraceCategory.Enter);
                if (ucProps == null)
                {
                    ucProps = new ucWcTableChildProps();
                    ucProps.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    ucProps.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                    //ucProps.OnListColumnListMustUpdate += new ListColumnListMustUpdateEventHandler(ucProps_OnListColumnListMustUpdate);
                    

                    // Always call the security code (next 2 lines) first because you never know when other code will depend on it already being set
                    //ucProps.ControlId = new Guid("xxxxxxxxxxxxxxxxxxxxx");
                   //** SecurityCache.SetWPFActionControlState(ucProps, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                    svProps.Content = ucProps;
                    ucProps.LoadControl();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProps_Load", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProps_Load", IfxTraceCategory.Leave);
            }
        }



        #endregion  Load Controls


        #region Control Methods


        #region Data Related

        /// <summary>
        ///     Creates a new instance of the business object (<see cref="EntityBll.WcTableChild_Bll">WcTableChild_Bll</see>) and sets it as the <see cref="CurrentBusinessObject">CurrentBusinessObject</see> as which ucProps will be bound
        ///     to. If this control is using <see cref="ucWcTableChildList">ucWcTableChildList</see>, then the
        ///     new instance of <see cref="EntityBll.WcTableChild_Bll">WcTableChild_Bll</see> will also be added
        ///     to <see cref="_list">_list</see>.
        /// </summary>
        public void NewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Enter);
                // If we arleady have a new row, dont allow adding a 2nd one.
                if (ucNav.NavList_ItemSource.Count > 0 && ucNav.FindNewRow() == true)
                {
                    return;
                }

                WcTableChild_Bll obj = new WcTableChild_Bll();
                obj.NewEntityRow();

                obj.SetStandingFK(_parentType, (Guid)_guidParentId);
                
                CurrentBusinessObject = obj;
                //**  FIX  or  DELETE this.
                //CurrentBusinessObject.StandingFK = (Guid)_prj_Id;
				CurrentBusinessObject.StandingFK = (Guid)_guidParentId;
                ucNav.NavList_ItemSource.Add(CurrentBusinessObject);
                // Activate new rec in list which will fire evenst to pass the new business object into the props control
                ucNav.ActivateNewRecord();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Calls <see cref="ucWcTableChildProps.Save">ucProps.Save</see>.</summary>
        public int Save()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                bool flgIsNew = _currentBusinessObject.State.IsNew();

                // Stub for when we have different entity types
                //switch (_personType)
                //{
                //    case PersonType.Contact:
                //        ucCntProps.Save();
                //        break;
                //    case PersonType.Employee:
                //        ucEmpProps.Save();
                //        break;
                //}

                int iSuccess = ucProps.Save();
                if (flgIsNew == true)
                {
                    // Commit the new item to the list
                    //** _list.EndNew(_list.IndexOf(_currentBusinessObject));
                }
                return iSuccess;

                //if (flgIsNew == true)
                //{
                //    int success = 0;
                //    //switch (_parentType)
                //    //{
                //    //    case "WcTableChild":
                //    //        success = WcTableChild_Bll.Insert_PersonRole_MM(Guid.NewGuid(), (Guid)_parentId, _currentBusinessObject.Role_Id);
                //   //         break;
                //   //     case "Permission_LU_Bll":
                //   // 
                //   //         break;
                //    //}
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
				throw IfxWrapperException.GetError(ex);
                return -1;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Calls the <see cref="ucWcTableChildProps.UnDo">ucProps.UnDo</see> method.</summary>
        public void UnDo()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Enter);
//                if (CurrentBusinessObject.State.IsNew() == true)
//                {
//                    // Role back the new item in the list and have it removed.
//                    _list.CancelNew(_list.IndexOf(CurrentBusinessObject));
//                    //  It was a new business object and we clicked UnDo which means - get rid of the new business object
//                    CurrentBusinessObject = null;
//                    //PropertiesControlHasBusinessObject = false;
//                    PassBusinessObjectToPropertiesControl();
//                    //OnCurrentEntityStateChanged(this, new CurrentEntityStateArgs(EntityStateSwitch.None , this));
//                }
//                else
//                {
                if (CurrentBusinessObject.State.IsNew() == true)
                {
                    // Its a new row, so skip the UnDo method on the biz object and kill the row all together.
                    ucNav.CancelNewRow(CurrentBusinessObject);
                    ucProps.CurrentBusinessObject = null;
                    CurrentBusinessObject = null;
                }
                else
                {
                    ucProps.UnDo();
                }
//                }

                    // Stub for when we have different entity types
                    //switch (_personType)
                    //{
                    //    case PersonType.Contact:
                    //        ucCntProps.UnDo();
                    //        break;
                    //    case PersonType.Employee:
                    //        ucEmpProps.UnDo();
                    //        break;
                    //}

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Leave);
            }
        }

        #endregion Data Related

        #region State Related

        /// <summary>
        ///     Called from <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> as it bubble up
        ///     from the business object. It Configures UI elements according to the current
        ///     <see cref="TypeServices.EntityState">state</see> of the entity. For example: if the
        ///     state is dirty and not valid, the Save button should be disabled and the UnDo
        ///     button should be enabled. Typically when the state is dirty, most areas of the UI
        ///     such as the navigation control (<see cref="ucWcTableChildList">ucWcTableChildList</see>) and
        ///     child entity controls are disabled except for <see cref="ucWcTableChildProps">ucProps</see> (the entity data entry screen). This prevents the user
        ///     from navigating away from data entry area until finishing the job – Saving or
        ///     UnDoing the transaction helps prevent confusion and helps assure data integrity.
        /// </summary>
        /// <param name="state">
        /// 	<see cref="TypeServices.EntityStateSwitch">EntityStateSwitch</see> which represents
        ///     the entity's current <see cref="TypeServices.EntityState">state</see> (dirty, not
        ///     dirty, valid, etc.)
        /// </param>
        void ConfigureToCurrentEntityState(object sender, EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState", IfxTraceCategory.Enter);
                    switch (state)
                    {
                        case EntityStateSwitch.None:
                            ucNav.IsEnabled = true;
                            if (_currentBusinessObject == null)
                            {
                                tbiWcTableChildProps.IsEnabled = false;
                                EnableActiveTab();
                            }
                            else
                            {
                                tbiWcTableChildProps.IsEnabled = true;
                            }
                            btnSave.IsEnabled = false;
                            btnUnDo.IsEnabled = false;
                            btnDelete.IsEnabled = false;
                            if(_guidParentId == null)
                            {
                                btnNew.IsEnabled = false;
                            }
                            else
                            {
                                btnNew.IsEnabled = true;
                            }
                            EnableActiveTab();
                            break;
                        case EntityStateSwitch.NewInvalidNotDirty:
                            ucNav.IsEnabled = false;
                            tbiWcTableChildProps.IsEnabled = false;
                            btnNew.IsEnabled = false;
                            btnSave.IsEnabled = false;
                            btnUnDo.IsEnabled = true;
                            btnDelete.IsEnabled = false;
                            EnableActiveTab();
                            break;
                        case EntityStateSwitch.NewValidNotDirty:
                            ucNav.IsEnabled = false;
                            tbiWcTableChildProps.IsEnabled = false;
                            btnNew.IsEnabled = false;
                            btnSave.IsEnabled = false;
                            btnUnDo.IsEnabled = true;
                            btnDelete.IsEnabled = false;
                            EnableActiveTab();
                            break;
                        case EntityStateSwitch.NewValidDirty:
                            ucNav.IsEnabled = false;
                            tbiWcTableChildProps.IsEnabled = false;
                            btnNew.IsEnabled = false;
                            btnSave.IsEnabled = true;
                            btnUnDo.IsEnabled = true;
                            btnDelete.IsEnabled = false;
                            EnableActiveTab();
                            break;
                        case EntityStateSwitch.NewInvalidDirty:
                            ucNav.IsEnabled = false;
                            tbiWcTableChildProps.IsEnabled = false;
                            btnNew.IsEnabled = false;
                            btnSave.IsEnabled = false;
                            btnUnDo.IsEnabled = true;
                            btnDelete.IsEnabled = false;
                            EnableActiveTab();
                            break;
                        case EntityStateSwitch.ExistingInvalidDirty:
                            ucNav.IsEnabled = false;
                            tbiWcTableChildProps.IsEnabled = false;
                            btnNew.IsEnabled = false;
                            btnSave.IsEnabled = false;
                            btnUnDo.IsEnabled = true;
                            btnDelete.IsEnabled = true;
                            EnableActiveTab();
                            break;
                        case EntityStateSwitch.ExistingValidDirty:
                            ucNav.IsEnabled = false;
                            tbiWcTableChildProps.IsEnabled = false;
                            btnNew.IsEnabled = false;
                            btnSave.IsEnabled = true;
                            btnUnDo.IsEnabled = true;
                            btnDelete.IsEnabled = true;
                            EnableActiveTab();
                            break;
                        case EntityStateSwitch.ExistingValidNotDirty:
                            ucNav.IsEnabled = true;
                            tbiWcTableChildProps.IsEnabled = true;
                            btnNew.IsEnabled = true;
                            btnSave.IsEnabled = false;
                            btnUnDo.IsEnabled = false;
                            btnDelete.IsEnabled = true;
                            break;
                    }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     This is called from the <see cref="ConfigureToCurrentEntityState">ConfigureToCurrentEntityState</see> method and sets
        ///     the active tab’s IsEnabled property to true. You may ask “How can a tab become
        ///     active if it’s not already enabled”. Users clicking on a tab is one way to active
        ///     it, but often a tab is activated programmatically. In this case the <see cref="ConfigureToCurrentEntityState">ConfigureToCurrentEntityState</see> method will
        ///     know which tabs are allowed to be enabled in the current <see cref="TypeServices.EntityState">state</see>.
        /// </summary>
        void EnableActiveTab()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EnableActiveTab", IfxTraceCategory.Enter);
                //ContentPane cp = (ContentPane)tgp.SelectedItem;
                //cp.IsEnabled = true;
                ((TabItem)tbcWcTableChild.SelectedItem).IsEnabled = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EnableActiveTab", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EnableActiveTab", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Loop through all the ContentPanes in the DocumentContentHost control (tab
        ///     control) and enable or disable them depending on the Boolean value passed
        ///     in.</para>
        /// </summary>
        void EnableTabs(bool flg)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EnableTabs", IfxTraceCategory.Enter);
                //if (flg == true)
                //{
                //    foreach (ContentPane cp in tgp.Items)
                //    {
                //        cp.IsEnabled = true;
                //    }
                //}
                //else
                //{
                //    foreach (ContentPane cp in tgp.Items)
                //    {
                //        if (cp.IsActiveDocument == false)
                //        {
                //            cp.IsEnabled = false;
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EnableTabs", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EnableTabs", IfxTraceCategory.Leave);
            }
        }

        #endregion  State Related


        #region Managing Objects


        void MainTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "MainTabControl_SelectionChanged", IfxTraceCategory.Enter);

                SyncControlsWithCurrentBusinessObject(((TabItem)((TabControl)sender).SelectedItem).Name);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "MainTabControl_SelectionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "MainTabControl_SelectionChanged", IfxTraceCategory.Leave);
            }
        }


        /// <overloads>
        /// Call this method to synchronize child controls to the current business object.
        /// For example, if a different item is selected in this control’s <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Entity_List_Type.html">navigation
        /// list</a>, then this control’s properties screen should be updated with that items data.
        /// However, if this control is using a tab control and one of its tabs has the properties
        /// control, then the other tabs may have other types of controls and screens such as other
        /// child <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Entity_Values_Manager_Type.html">
        /// Entity Managers</a>. These controls would need to be filtered and configured using the
        /// current business object’s Id value. This method will determine which tab is active,
        /// load and initialize the child controls if they haven’t been loaded yet, and then call
        /// the child control’s SetStateFromParent method (which will continue all appropriate
        /// synchronizing).
        /// </overloads>
        /// <summary>
        /// Call override from any event other than the tab control’s tab index changed event
        /// (or the equivalent of).
        /// </summary>
        private void SyncControlsWithCurrentBusinessObject()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject", IfxTraceCategory.Enter);
                //if (((ContentPane)tgp.SelectedItem) == null)
                //{
                //    // Log Error because we should have had a selected item.
                //    return;
                //}
                //SyncControlsWithCurrentBusinessObject(((ContentPane)tgp.SelectedItem).Name);
                SyncControlsWithCurrentBusinessObject(((TabItem)tbcWcTableChild.SelectedItem).Name);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Call override from the tab control’s tab index changed event (or the equivalent
        /// of).
        /// </summary>
        private void SyncControlsWithCurrentBusinessObject(string selectedTab)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject", IfxTraceCategory.Enter);
                if (ucNav.IsSplitSreenMode)
                {
                    Guid? currentId = null;
                    if (_currentBusinessObject != null)
                    {
                        //**  need this here becuase if we are hiding the props tab, it will never get called and the well tab will remain disabled.
                        ConfigureToCurrentEntityState(this, _currentBusinessObject.StateSwitch);
                        currentId = _currentBusinessObject.TbChd_Id;
                    }
                    switch (selectedTab)
                    {
                        case "tbiWcTableChildProps":
                             _isPropsTabSelected = true;

                            if (ucNav.IsSplitSreenMode == false) { return; }  // this might be a bad idea of we fail to put the correct biz object in ucProps as it may become out of synch with the grid.

                            if (ucProps == null) { ucProps_Load(); }

                            ucProps.CurrentBusinessObject = _currentBusinessObject;
                            break;
                        case "xxxxxxxxxxxx":
                            //ucWlD.SetStateFromParent(_prj_Id, "ucWell", null, currentId, null, null, null, null, null);
                            break;
                        default:
                            _isPropsTabSelected = false;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///  Get the entity state from the active business object and then call OnCurrentEntityStateChanged to set the app state accordingly.
        /// </summary>
        public void GetSetCurrentAppEntityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSetCurrentAppEntityState", IfxTraceCategory.Enter);
                CurrentEntityStateArgs args = null;
                if (_currentBusinessObject == null)
                {
                    args = new CurrentEntityStateArgs(EntityStateSwitch.None, this, ucProps, null);
                }
                else
                {
                    args = new CurrentEntityStateArgs(_currentBusinessObject.StateSwitch, this, ucProps, _currentBusinessObject);
                }
                OnCurrentEntityStateChanged(this, args);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSetCurrentAppEntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSetCurrentAppEntityState", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Passes the <see cref="CurrentBusinessObject">CurrentBusinessObject</see> to
        ///     <see cref="ucWcTableChildProps">ucProps.CurrentBusinessObject</see>
        /// </summary>
        void PassBusinessObjectToPropertiesControl()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PassBusinessObjectToPropertiesControl", IfxTraceCategory.Enter);
                ucProps.CurrentBusinessObject = CurrentBusinessObject;
                // Stub for when we have different types of the same entity
                //switch (_personType)
                //{
                //    case PersonType.Contact:
                //        ucCntProps.CurrentBusinessObject = CurrentBusinessObject;
                //        break;
                //    case PersonType.Employee:
                //        ucEmpProps.CurrentBusinessObject = CurrentBusinessObject;
                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PassBusinessObjectToPropertiesControl", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PassBusinessObjectToPropertiesControl", IfxTraceCategory.Leave);
            }
        }


        void AttachEventsToActiveUserControls(bool attach)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachEventsToActiveUserControls", IfxTraceCategory.Enter);
                if (attach == true)
                {
                    //  Attatch events to only the active user control
                    string selectedTab = ((TabItem)tbcWcTableChild.SelectedItem).Name;
                    switch (selectedTab)
                    {
                        case "tbiWcTableChildProps":
                            if (ucProps == null)
                            {
                                ucProps_Load();
                            }
                            else
                            {
                                ucProps.AddBusinessObjectEvents();
                            }
                            break;
                        case "xxxxxxxxxxxx":

                            break;
                    }
                }
                else
                {
                    //  Detatch events from all known user controls

                    //  ucProps
                    if (ucProps != null)
                    {
                        ucProps.RemoveBusnessObjectEvents();
                    }
                    //  ucXX
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachEventsToActiveUserControls", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachEventsToActiveUserControls", IfxTraceCategory.Leave);
            }
        }



        #region Collapse - Expand Parents

        public void CollapseExapndNavGrid(CollapseExpandParentEntityOptions option)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CollapseExapndNavGrid", IfxTraceCategory.Enter);

                if (option == CollapseExpandParentEntityOptions.Collapse)
                {
                    gdSplitter.CollapseSplitter();
                }
                else if (option == CollapseExpandParentEntityOptions.Expand)
                {
                    gdSplitter.ExpandSplitter();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CollapseExapndNavGrid", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CollapseExapndNavGrid", IfxTraceCategory.Leave);
            }
        }

        public void CollapseExpandAllParentNavGrids(CollapseExpandParentEntityOptions option)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CollapseExpandAllParentNavGrids", IfxTraceCategory.Enter);

                Raise_ParentNavGridExpansionChanged(option);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CollapseExpandAllParentNavGrids", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CollapseExpandAllParentNavGrids", IfxTraceCategory.Leave);
            }
        }

        void ChangeParentNavGridExpansionFromNestedObject(object sender, CollapseExpandAllParentNavGridsArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ChangeParentNavGridExpansionFromNestedObject", IfxTraceCategory.Enter);
                if (sender != ucNav)
                {
                    CollapseExapndNavGrid(e.Option);
                }
                if (e.Option == CollapseExpandParentEntityOptions.Expand)
                {
                    ucNav.Reset_mnuCollapsAllParentNavGrids();
                }
                Raise_ParentNavGridExpansionChanged(e.Option);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ChangeParentNavGridExpansionFromNestedObject", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ChangeParentNavGridExpansionFromNestedObject", IfxTraceCategory.Leave);
            }
        }

        void Raise_ParentNavGridExpansionChanged(CollapseExpandParentEntityOptions option)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Raise_ParentNavGridExpansionChanged", IfxTraceCategory.Enter);

                CollapseExpandAllParentNavGridsEventHandler handler = ChangeParentNavGridExpansion;
                if (handler != null)
                {
                    handler(this, new CollapseExpandAllParentNavGridsArgs(option));
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Raise_ParentNavGridExpansionChanged", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Raise_ParentNavGridExpansionChanged", IfxTraceCategory.Leave);
            }
        }


        #endregion Collapse - Expand Parents



        #endregion Managing Objects


        #endregion Control Methods


        #region Properties, Getters and Setters


        /// <summary>
        ///     For compliense with <see cref="TypeServices.IEntityControl">IEntityControl</see>,
        ///     IEntityControlCurrentBusinessObject acts as a serogate for CurrentBusinessObject.
        ///     IEntityControlCurrentBusinessObject passes the IBusinessObject object to or from
        ///     the CurrentBusinessObject property.
        /// </summary>
        public IBusinessObject IEntityControlCurrentBusinessObject
        {
            get { return CurrentBusinessObject; }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                     if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IEntityControlCurrentBusinessObject  Setter", IfxTraceCategory.Enter);
                    CurrentBusinessObject = (WcTableChild_Bll)value;
                }
                catch (Exception ex)
                {
                     if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IEntityControlCurrentBusinessObject  Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                     if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IEntityControlCurrentBusinessObject  Setter", IfxTraceCategory.Leave);
                }
            }
        }

        /// <summary>
        ///     A reference to the current business object (<see cref="EntityBll.WcTableChild_Bll">WcTableChild_Bll</see>). This would be the object bound to the
        ///     selected row in <see cref="ucWcTableChildList">ucWcTableChildList</see> and the same business
        ///     object populating <see cref="ucWcTableChildProps">ucWcTableChildProps</see>.
        /// </summary>
        /// 
        public WcTableChild_Bll CurrentBusinessObject
        {
            get { return _currentBusinessObject; }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                     if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_Bll Setter", IfxTraceCategory.Enter);
                    _currentBusinessObject = value;
                    if (_currentBusinessObject != null)
                    {
                        _oCurrentId = value.TbChd_Id;
                    }
                    else
                    {
                        _oCurrentId = null;
                    }
                }
                catch (Exception ex)
                {
                     if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_Bll Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                     if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_Bll Setter", IfxTraceCategory.Leave);
                }
            }
        }

//        Guid? _ancestorId = null;
//        public Guid? AncestorId
//        {
//            get { return _ancestorId; }
//            set
//            {
//                _ancestorId = value;
//                ucNav.AncestorId = _ancestorId;
//                // Set to all child entity controls
//                if (ucWlD != null)
//                {
//                    ucWlD.AncestorId = _ancestorId;
//                }
//            }
//        }


        #region IEntityControl Members

        /// <summary>
        /// 	<para>
        ///         Sets <see cref="_isActiveEntityControl">_isActiveEntityControl</see>.
        ///     </para>
        /// </summary>
        public void SetIsActiveEntityControl(bool value)
        {
            _isActiveEntityControl=value;
        }

        /// <summary>
        /// 	<para>
        ///         Gets <see cref="_isActiveEntityControl">_isActiveEntityControl</see>.
        ///     </para>
        /// </summary>
        public bool GetIsActiveEntityControl()
        {
            return _isActiveEntityControl;
        }

        /// <summary>
        ///     Gets <see cref="ucProps">ucProps</see> using the <see cref="TypeServices.IEntitiyPropertiesControl">IEntitiyPropertiesControl</see> interface.
        /// </summary>
        public IEntitiyPropertiesControl GetPropsControl()
        {
            return ucProps;
        }

        /// <summary>
        /// 	<para>
        ///         Gets <see cref="_isPropsTabSelected">_isPropsTabSelected</see>.
        ///     </para>
        /// </summary>
        public bool GetIsPropsTabSelected()
        {
            return _isPropsTabSelected;
        }

        public bool GetIsInSplitScreenMode()
        {
            return ucNav.IsSplitSreenMode;
        }


        /// <summary>
        /// Get the Id of the row that was last updated. This method is used when the primary
        /// key is an int data type. There is also a Guid version of this method allowing the
        /// interface to be extendable to both data types.
        /// </summary>
        public int? GetLastUpdatedId_Int()
        {
            return null;
        }

        /// <summary>
        /// 	<para>Get the Id of the row that was last updated. This method is used when the
        ///     primary key is an Guid data type. There is also a int version of this method
        ///     allowing the interface to be extendable to both data types.</para>
        /// </summary>
        public Guid? GetLastUpdatedId_Guid()
        {
            if (_currentBusinessObject == null)
            {
                return null;
            }
            else
            {
                return _currentBusinessObject.TbChd_Id;
            }
        }

        public double GetEntityWidth()
        {
            return 630;  
        }

        public double GetEntityHeight()
        {
            return 360;  
        }

        #endregion IEntityControl Members


        #endregion Properties, Getters and Setters


        #region Events


        /// <summary>
        ///     An event that bubbles up from <see cref="ucWcTableChildList">ucWcTableChildList</see> when the
        ///     selected index changes.
        /// </summary>
        void ucNav_NavigationListSelectedItemChanged(object sender, NavigationListSelectedItemChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNav_NavigationListSelectedItemChanged", IfxTraceCategory.Enter);

                if (e.SelectedItem is WcTableChild_Bll)
                {
                    _currentBusinessObject = (WcTableChild_Bll)e.SelectedItem;
                    _currentChildBusinessObject = null;
                    //_oCurrentId = _currentBusinessObject.TbChd_Id;
                }
                SyncControlsWithCurrentBusinessObject();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNav_NavigationListSelectedItemChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNav_NavigationListSelectedItemChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Bubbles up from the business object when the current entity’s <see cref="TypeServices.EntityState">state</see> has changed. It calls the <see cref="ConfigureToCurrentEntityState">ConfigureToCurrentEntityStatemethod</see> and then
        ///     continues to bubble up to notify other parent controls.
        /// </summary>
        void OnCurrentEntityStateChanged(object sender, CurrentEntityStateArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Enter);
                // If we are not in split sreen mode, then this event is being raised by a nested user control (such as ucProps) which 
                // is not the control doing the editing.  the editing should be taking place in the nav grid and therefore we dont want to run this code which
                // is going to configure a bunch of stuff.  Probably what happened is the business object in the grid where the state changed is also the business
                //  object in ucProps and the biz object raised this event via ucProps.

                //** Just added this - Before we would just return, but now we need to skip ConfigureToCurrentEntityState and still raise the event.
                //** test for true instead of false
                //** need to test this more to make sure we're not executing code uneccessarily.
                //if (ucNav.IsSplitSreenMode == false) { return; }
                if (ucNav.IsSplitSreenMode == true)
                {
                    ConfigureToCurrentEntityState(sender, e.State);
                }
                CurrentEntityStateEventHandler handler = CurrentEntityStateChanged;
                CurrentEntityStateArgs args;
                if (sender == null) { sender = this; }
                if (e.ActiveEntityControl == null)
                {
                    args = new CurrentEntityStateArgs(e.State, this, e.ActivePropertiesControl, e.ActiveBusinessObject);
                }
                else
                {
                    args = e;
                }
                if (handler != null)
                {
                    handler(sender, args);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     OnBrokenRuleChanged starts from the business object and bubbles up through
        ///     <see cref="ucWcTableChildProps">ucProps</see> (or <see cref="ucWcTableChildList">ucWcTableChildList</see> when it’s configured to add/edit data) where the
        ///     current control’s (the control being edited - TextBox, ComboBox, etc.) appearance
        ///     will be modified according to its valid state. OnBrokenRuleChanged also passes up
        ///     the broken rule text so that it can be added or removed from the control’s
        ///     (TextBox, ComboBox, etc.) <see cref="ucWcTableChildProps.tt_Loaded">BrokenRule
        ///     Tooltip</see>. OnBrokenRuleChanged then continues to bubble up to the top level
        ///     control (probably a window) where you have an option to use the broken rule text
        ///     for other means of notifying the user.
        /// </summary>
        void OnBrokenRuleChanged(object sender, BrokenRuleArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Enter);
                //SetBrokenRuleText(e.Rule);
                BrokenRuleEventHandler handler = BrokenRuleChanged;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Leave);
            }
        }
        
//        /// <summary>
//        /// This event is raised from Entity Properties control's <see cref="UIControls.ucWcTableChildProps.RaiseListColumnListMustUpdate()"/> method which is called
//        /// when the data in a list bound to a data field's list control such as a ComboBox is changed (the user added, removed or edited an item in the list).
//        /// This event will pass the ColumnName property value from the event args to the <see cref="ucNav"/> control’s <see cref="UpdateListColumnList()"/> method
//        /// which will then update the corresponding grid cell's list.
//        /// </summary>
//        /// <param name="sender"></param>
//        /// <param name="e">ListColumnListMustUpdateArgs</param>
//        void ucProps_OnListColumnListMustUpdate(object sender, ListColumnListMustUpdateArgs e)
//        {
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProps_OnListColumnListMustUpdate", IfxTraceCategory.Enter);
//                ucNav.UpdateListColumnList(e.ColumnName);
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProps_OnListColumnListMustUpdate", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProps_OnListColumnListMustUpdate", IfxTraceCategory.Leave);
//            }
//        }


        void ucNav_SplitScreenModeChanged(object sender, SplitScreenModeChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNav_SplitScreenModeChanged", IfxTraceCategory.Enter);
                ucNav.IsSplitSreenMode = e.IsSplitScreen;
                SetSplitScreenState();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNav_SplitScreenModeChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucNav_SplitScreenModeChanged", IfxTraceCategory.Leave);
            }
        }

        void SetSplitScreenState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSplitScreenState", IfxTraceCategory.Enter);
                AttachEventsToActiveUserControls(ucNav.IsSplitSreenMode);
                if (ucNav.IsSplitSreenMode)
                {
                    SyncControlsWithCurrentBusinessObject();
                    ucNav.CurrentEntityStateChanged -= new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    myGrid.ColumnDefinitions[0].Width = _navColumnWidth;
                    myGrid.ColumnDefinitions[1].Width = new System.Windows.GridLength(8);
                    myGrid.ColumnDefinitions[2].Width = new GridLength(1, GridUnitType.Star);
                }
                else
                {
                    GetSetCurrentAppEntityState();
                    ucNav.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    myGrid.ColumnDefinitions[0].Width = new GridLength(1, GridUnitType.Star);
                    myGrid.ColumnDefinitions[1].Width = new System.Windows.GridLength(0);
                    myGrid.ColumnDefinitions[2].Width = new System.Windows.GridLength(0);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSplitScreenState", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSplitScreenState", IfxTraceCategory.Leave);
            }
        }

        void gdSplitter_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _navColumnWidth = myGrid.ColumnDefinitions[0].Width;
            //System.Diagnostics.Debug.WriteLine(_navColumnWidth.ToString());
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnNew_Click", IfxTraceCategory.Enter);
                NewEntityRow();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnNew_Click", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnNew_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnSave_Click", IfxTraceCategory.Enter);
                Save();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnSave_Click", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnSave_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnUnDo_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnUnDo_Click", IfxTraceCategory.Enter);
                UnDo();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnUnDo_Click", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnUnDo_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDelete_Click", IfxTraceCategory.Enter);
                if (_currentBusinessObject != null)
                {
                    // call ws and set IsDeleted to true
                    string msg = "Are you sure you want to DELETE this record from the database and all data under it?";
                    MessageBoxResult result = MessageBox.Show(msg, "Delete Warning", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {
                        _wcTableChildProxy.Begin_WcTableChild_SetIsDeleted(_currentBusinessObject.TbChd_Id, true);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDelete_Click", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDelete_Click", IfxTraceCategory.Leave);
            }
        }


        #endregion Events

        #region Fetch Data


        void WcTableChild_GetByIdCompleted(object sender, WcTableChild_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetByIdCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                NavListRefreshFromObjectArray(array);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetByIdCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetByIdCompleted", IfxTraceCategory.Leave);
                ucNav.navList.Cursor = Cursors.Arrow;
            }
        }

        void WcTableChild_GetListByFKCompleted(object sender, WcTableChild_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                NavListRefreshFromObjectArray(array);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetListByFKCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetListByFKCompleted", IfxTraceCategory.Leave);
                ucNav.navList.Cursor = Cursors.Arrow;
            }
        }

        void WcTableChild_GetAllCompleted(object sender, WcTableChild_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetAllCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                NavListRefreshFromObjectArray(array);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetAllCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetAllCompleted", IfxTraceCategory.Leave);
                ucNav.navList.Cursor = Cursors.Arrow;
            }
        }

        void WcTableChild_SetIsDeletedCompleted(object sender, WcTableChild_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                int? success = null;
                Guid? id = null;
                object[] data = e.Result;
                if (data == null)
                {
                    MessageBox.Show("There was problem making this assignment at the server." + Environment.NewLine + "If this continues, please contact suport.", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    success = data[0] as int?;
                    id = data[1] as Guid?;

                    if (success == 1 && id != null)
                    {
                        foreach (WcTableChild_Bll item in ucNav.NavList_ItemSource)
                        {
                            if (item.TbChd_Id == _currentBusinessObject.TbChd_Id)
                            {
                                ((WcTableChild_List)ucNav.NavList_ItemSource).Remove(item);
                                _currentBusinessObject = null;
                                ucProps.SetBusinessObject(null);
                                break;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("There was problem making this assignment at the server." + Environment.NewLine + "If this continues, please contact suport.", "Error", MessageBoxButton.OK);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_SetIsDeletedCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_SetIsDeletedCompleted", IfxTraceCategory.Leave);
                ucNav.navList.Cursor = Cursors.Arrow;
            }
        }


        #endregion Fetch Data

    }
}


