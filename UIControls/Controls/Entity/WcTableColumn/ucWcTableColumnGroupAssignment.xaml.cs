﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using EntityBll.SL;
using EntityWireTypeSL;
using Ifx.SL;
using Infragistics.Controls.Grids;
using ProxyWrapper;
using vControls;
using vUICommon;
using Velocity.SL;

namespace UIControls
{
    public partial class ucWcTableColumnGroupAssignment : UserControl
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "ucWcTableColumnGroupAssignment";

        ObservableCollection<wcTableColumnGroup_Assignments_Binding> _list = new ObservableCollection<wcTableColumnGroup_Assignments_Binding>();

        Guid? _parentId = null;
        string _parentType = "";


        Guid? _tbC_Id = null;
        Guid? _tb_Id = null;
        Guid? _apVrsn_Id = null;

        WcTableColumnService_ProxyWrapper _proxy = null;

        public event GroupAssignmentUpdatedEventHandler XRefUpdated;
        
        #endregion Initialize Variables


        public ucWcTableColumnGroupAssignment()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucWcTableColumnGroupAssignment Constructor", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();

                _proxy = new WcTableColumnService_ProxyWrapper();
                _proxy.GetwcTableColumnGroup_AssignmentsCompleted += _proxy_GetwcTableColumnGroup_AssignmentsCompleted;
                _proxy.ExecutewcTableColumnGroup_AssignColumnsCompleted += _proxy_ExecutewcTableColumnGroup_AssignColumnsCompleted;

                XgdAssignments.ItemsSource = _list;

                XgdAssignments.EditingSettings.IsMouseActionEditingEnabled = MouseEditingAction.SingleClick;
                XgdAssignments.RowHover = RowHoverType.Row;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucWcTableColumnGroupAssignment Constructor", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucWcTableColumnGroupAssignment Constructor", IfxTraceCategory.Leave);
            }
        }
        


        public void SetStateFromParent(Guid? tb_id, Guid? tbC_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent", IfxTraceCategory.Enter);

                _tbC_Id = tbC_Id;
                _tb_Id = tb_id;


                if (_tbC_Id == null)
                {
                    ClearGrid();
                    MessageBox.Show("Missing Table Column Id", "Missing Data", MessageBoxButton.OK);
                    return;
                }
                else if (_tb_Id == null)
                {
                    ClearGrid();
                    MessageBox.Show("Missing Table Id", "Missing Data", MessageBoxButton.OK);
                    return;
                }

                _proxy.Begin_GetwcTableColumnGroup_Assignments( (Guid)_tb_Id, (Guid)_tbC_Id);
                
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent", IfxTraceCategory.Leave);
            }
        }

        public void ClearGrid()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearGrid", IfxTraceCategory.Enter);
                System.Diagnostics.Debug.WriteLine("ClearGrid");

                XgdAssignments.ItemsSource = null;
                _list.Clear();
                XgdAssignments.ItemsSource = _list;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearGrid", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearGrid", IfxTraceCategory.Leave);
            }
        }



        #region Events

        //Guid? _activeCell_Id = null;
        //bool? _activeCellValue = null;



        private void vCheckColumn_CheckedChanged(object sender, vCheckColumnCheckedChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Enter);

                Console.WriteLine(sender.ToString());
                wcTableColumnGroup_Assignments_Binding data = e.Data as wcTableColumnGroup_Assignments_Binding;
                if (data == null)
                {
                    //  alert user
                }
                else
                {
                    // update database
                    //  NOTE:  We will do the OPOSITE of what the checked value suggest.  the value of the data object has not been set by the ckeckbox yet
                    //  so its current value at this moment will be the oposite of what the checkbox is.  if the user checked it, this will be false - and we need to make the assignment.
                    //if (data.XRef == false)

                    Guid grpId = data.TbCGrp_Id;

                    if (((vCheckBox)sender).IsChecked == true)
                    {
                        //Boolean Insert, TbCGrp_Id, TbC_Id, CreatedUserId
                        _proxy.Begin_ExecutewcTableColumnGroup_AssignColumns(true, grpId, (Guid)_tbC_Id, (Guid)Credentials.UserId);
                    }
                    else
                    {
                        _proxy.Begin_ExecutewcTableColumnGroup_AssignColumns(false, grpId, (Guid)_tbC_Id, (Guid)Credentials.UserId);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Leave);
            }
        }



        //void XgdAssignments_CellEnteredEditMode(object sender, Infragistics.Controls.Grids.EditingCellEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XgdAssignments_CellExitedEditMode", IfxTraceCategory.Enter);

        //        Prospect_lstSelectedByParent_Binding data = e.Cell.Row.Data as Prospect_lstSelectedByParent_Binding;
        //        if (data == null)
        //        {
        //            _activeCell_Id = null;
        //            _activeCellValue = null;
        //        }
        //        else
        //        {
        //            _activeCell_Id = data.Pp_Id;
        //            _activeCellValue = data.IsSelected;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XgdAssignments_CellExitedEditMode", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XgdAssignments_CellExitedEditMode", IfxTraceCategory.Leave);
        //    }
        //}


        //void XgdAssignments_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XgdAssignments_CellExitedEditMode", IfxTraceCategory.Enter);

        //        Prospect_lstSelectedByParent_Binding data = e.Cell.Row.Data as Prospect_lstSelectedByParent_Binding;
        //        if (data == null)
        //        {
        //            //  alert user
        //        }
        //        else
        //        {
        //            if (data.Pp_Id == _activeCell_Id && _activeCellValue != data.IsSelected)
        //            {
        //                if (data.IsSelected == true)
        //                {
        //                    _ProspectProxy.Begin_Prospect_XRef_Assign((Guid)_parentId, _parentType, data.Pp_Id, (Guid)Credentials.UserId);
        //                }
        //                else
        //                {
        //                    _ProspectProxy.Begin_Prospect_XRef_Remove((Guid)_parentId, _parentType, data.Pp_Id, (Guid)Credentials.UserId);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XgdAssignments_CellExitedEditMode", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XgdAssignments_CellExitedEditMode", IfxTraceCategory.Leave);
        //    }
        //}


        private void RaiseEvent_XRefUpdated()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEvent_XRefUpdated", IfxTraceCategory.Enter);

                if (XRefUpdated != null)
                {
                    GroupAssignmentUpdatedEventHandler handler = XRefUpdated;
                    GroupAssignmentUpdatedArgs args = new GroupAssignmentUpdatedArgs();
                    handler(this, args);
                }


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEvent_XRefUpdated", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEvent_XRefUpdated", IfxTraceCategory.Leave);
            }
        }



        #endregion Events


        #region Fetch Data


        void XgdAssignments_RowSource(object[] data)
        {
            // This method is usually a followup from calling SetStateFromParent and after the web service has returned.
            // Its also ALWAYS called by the web service reply when returning a new list for the grid.
            // Now reset the list and see to it that any other screens as reset accordingly.
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XgdAssignments_RowSource", IfxTraceCategory.Enter);
                System.Diagnostics.Debug.WriteLine("XgdAssignments_RowSource");

                XgdAssignments.ItemsSource = null;
                _list = null;

                _list = new ObservableCollection<wcTableColumnGroup_Assignments_Binding>();


                if (data != null)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        _list.Add(new wcTableColumnGroup_Assignments_Binding((object[])data[i]));
                    }

                }

                XgdAssignments.ItemsSource = _list;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XgdAssignments_RowSource", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                XgdAssignments.IsEnabled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XgdAssignments_RowSource", IfxTraceCategory.Leave);
            }
        }

        //void _ProspectProxy_GetProspect_lstSelectedByWellCompleted(object sender, GetProspect_lstSelectedByWellCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_GetProspect_lstSelectedByWellCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;
        //        object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
        //        XgdAssignments_RowSource(array);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_GetProspect_lstSelectedByWellCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_GetProspect_lstSelectedByWellCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void _ProspectProxy_GetProspect_lstSelectedByLeaseTractCompleted(object sender, GetProspect_lstSelectedByLeaseTractCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_GetProspect_lstSelectedByLeaseTractCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;
        //        object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
        //        XgdAssignments_RowSource(array);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_GetProspect_lstSelectedByLeaseTractCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_GetProspect_lstSelectedByLeaseTractCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void _ProspectProxy_GetProspect_lstSelectedByContractDetailCompleted(object sender, GetProspect_lstSelectedByContractDetailCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_GetProspect_lstSelectedByContractDetailCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;
        //        object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
        //        XgdAssignments_RowSource(array);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_GetProspect_lstSelectedByContractDetailCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_GetProspect_lstSelectedByContractDetailCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void _ProspectProxy_Prospect_XRef_AssignCompleted(object sender, Prospect_XRef_AssignCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_Prospect_XRef_AssignCompleted", IfxTraceCategory.Enter);
        //        object[] data = e.Result;
        //        if (data == null)
        //        {
        //            MessageBox.Show("XRefs may not have been created.  PProspect check the results, and if this continues, pProspect contact support.", "Possible Error", MessageBoxButton.OK);
        //            return;
        //        }
        //        else
        //        {
        //            int? success = data[0] as int?;
        //            if (success == 1)
        //            {
        //                RaiseEvent_XRefUpdated();
        //            }
        //            else
        //            {
        //                MessageBox.Show("XRefs may not have been created.  PProspect check the results, and if this continues, pProspect contact support.", "Possible Error", MessageBoxButton.OK);
        //                return;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_Prospect_XRef_AssignCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_Prospect_XRef_AssignCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void _ProspectProxy_Prospect_XRef_RemoveCompleted(object sender, Prospect_XRef_RemoveCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_Prospect_XRef_RemoveCompleted", IfxTraceCategory.Enter);
        //        object[] data = e.Result;
        //        if (data == null)
        //        {
        //            MessageBox.Show("XRefs may not have been removed.  Please check the results, and if this continues, contact support.", "Possible Error", MessageBoxButton.OK);
        //            return;
        //        }
        //        else
        //        {
        //            int? success = data[0] as int?;
        //            if (success == 1)
        //            {
        //                RaiseEvent_XRefUpdated();
        //            }
        //            else
        //            {
        //                MessageBox.Show("XRefs may not have been removed.  Please check the results, and if this continues, contact support.", "Possible Error", MessageBoxButton.OK);
        //                return;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_Prospect_XRef_RemoveCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_ProspectProxy_Prospect_XRef_RemoveCompleted", IfxTraceCategory.Leave);
        //    }
        //}



            

        private void _proxy_GetwcTableColumnGroup_AssignmentsCompleted(object sender, GetwcTableColumnGroup_AssignmentsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetwcTableColumnGroup_AssignmentsCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                XgdAssignments_RowSource(array);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetwcTableColumnGroup_AssignmentsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetwcTableColumnGroup_AssignmentsCompleted", IfxTraceCategory.Leave);
            }
        }

        private void _proxy_ExecutewcTableColumnGroup_AssignColumnsCompleted(object sender, ExecutewcTableColumnGroup_AssignColumnsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_ExecutewcTableColumnGroup_AssignColumnsCompleted", IfxTraceCategory.Enter);
                object[] data = e.Result;
                if (data == null)
                {
                    MessageBox.Show("XRefs may not have been removed.  Please check the results, and if this continues, contact support.", "Possible Error", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    int? success = data[0] as int?;
                    if (success == 1)
                    {
                        RaiseEvent_XRefUpdated();
                    }
                    else
                    {
                        MessageBox.Show("XRefs may not have been removed.  Please check the results, and if this continues, contact support.", "Possible Error", MessageBoxButton.OK);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_ExecutewcTableColumnGroup_AssignColumnsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_ExecutewcTableColumnGroup_AssignColumnsCompleted", IfxTraceCategory.Leave);
            }
        }









        #endregion Fetch Data




    }
}
