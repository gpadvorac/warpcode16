using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Infragistics.Controls.Editors;
using System.ComponentModel;
using TypeServices;
using EntityWireTypeSL;
using EntityBll.SL;
using Ifx.SL;
using vUICommon;
using vUICommon.Controls;
using vControls;
using vComboDataTypes;
using vTooltipProvider;
using Velocity.SL;
using Infragistics.Controls.Layouts;
using ProxyWrapper;
using UIControls.Globalization.WcTableColumn;

// Gen Timestamp:  12/17/2017 8:41:09 PM

namespace UIControls
{


    /// <summary>
    /// 	<para><strong>About this Entity:</strong></para>
    /// 	<para>***General description of the entity from WC***</para>
    /// 	<para></para><br/>
    /// 	<para><strong>About this Control:</strong></para>
    /// 	<para>
    ///     Used as the entity’s (WcTableColumn’s) data entry screen. This control can be embedded
    ///     nearly anywhere, but typically is used in the <a href="D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\The_Entity_Manager.html">
    ///     Entity Manager</a> (<see cref="ucWcTableColumn">ucWcTableColumn</see>) and known as <a href="D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntityProps.html">ucProps</a>.
    ///     ucProps has no UI features for CRUD operations such as buttons or menus and depends
    ///     on either bubbling events up to the top level container where these object usually
    ///     reside, or on user initiated events starting from the top level control and using
    ///     it’s reference to the ‘<see cref="ucWcTableColumn.GetIsActiveEntityControl">Active Entity
    ///     Control’</see> or Active Properties Control’ where the CRUD operation will be
    ///     called to ucProps and then to the business object (<see cref="EntityBll.WcTableColumn_Bll">WcTableColumn_Bll</see>).</para>
    /// </summary>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Entity_Design_Pattern_In_The_UI_Layer.html" cat="Framework and Design Pattern">Entity Design Pattern In The UI Layer</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntity.html" cat="Framework and Design Pattern">ucEntity</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntityList.html" cat="Framework and Design Pattern">ucEntityList</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\ucEntityProps.html" cat="Framework and Design Pattern">ucEntityProps</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Tracing_Overview.html" cat="Framework and Design Pattern">Tracing Overview</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Exception_Handling_Overview.html" cat="Framework and Design Pattern">Exception Handling Overview</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Entity_Design_Pattern_In_The_Business_Logic_Layer.html" cat="Framework and Design Pattern In The Business Logic Layer">Business Objects</seealso>
    public partial class ucWcTableColumnProps : UserControl, IEntitiyPropertiesControl
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "ucWcTableColumnProps";

        WcTableColumn_Bll objB;

        /// <summary>
        /// A Boolean flag initialize to true and set to false when the control has loaded.
        /// Sometimes when setting default values in controls their value changed events or
        /// selected index changed fire causing a write action to a business object or some other
        /// action that’s not appropriate when the control is being loaded. When this flag is true,
        /// these events can return without executing any additional code.
        /// </summary>
        bool FLG_FORM_IS_LOADING;
        /// <summary>
        /// A Boolean flag initialize to true when a business object’s data begins to load
        /// into this control, and is set to false when the data load has completed. Usually when
        /// setting values in controls their value changed events or selected index changed fire
        /// causing a write action to a business object or some other action that’s not appropriate
        /// when the data is being loaded. When this flag is true, these events can return without
        /// executing any additional code.
        /// </summary>
        bool FLG_LOADING_REC;
        /// <summary>
        /// A Boolean flag initialize to true when a single data control’s data is being
        /// loaded and you don’t want this control to write to the business object. When this flag
        /// is true, these events can return without executing any additional code.
        /// </summary>
        bool FLG_UPDATING_FIELDVALUE;
        /// <summary>See <see cref="OnCrudFailed">OnCrudFailed</see> for information.</summary>
        public event CrudFailedEventHandler CrudFailed;
        /// <summary>
        ///     See <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> for
        ///     information.
        /// </summary>
        public event CurrentEntityStateEventHandler CurrentEntityStateChanged;
        //public event PropertiesControlActivatedEventHandler PropertiesControlActivated;
        /// <summary>See <see cref="OnBrokenRuleChanged">OnBrokenRuleChanged</see> for information.</summary>
        public event BrokenRuleEventHandler BrokenRuleChanged;
        /// <summary>
        /// Used in situations where this properties control is used in conjunction with a
        /// list control (ucWcTableColumnList). If this properties control has a list data field such as a
        /// ComboBox and users are allowed to edit the ComboBox list, then the corresponding column
        /// in ucEntityList must be updated also. Often this corresponding column in ucEntityList
        /// uses an embedded ComboBox to display the text for the Id bound in that column. If this
        /// properties control’s ComboBox’s list has been changed, then the corresponding
        /// ComboBox’s list in ucWcTableColumnList must be updated also.
        /// </summary>
        public event ListColumnListMustUpdateEventHandler OnListColumnListMustUpdate; 
        /// <summary>
        ///     The data field for the property <see cref="IsActivePropertiesControl">IsActivePropertiesControl</see>. Refer to its
        ///     documentation on how its used.
        /// </summary>
        bool _isActivePropertiesControl = false;
        /// <summary>
        ///     This properties control uses a Business Object (<see cref="EntityBll.WcTableColumn_Bll">WcTableColumn_Bll</see>) for its data source. There are times when
        ///     there is no business object such as when this control first loads. There is code
        ///     that expects a business object to be in place and can use this flag to make
        ///     corrections when no business object is found.
        /// </summary>
        bool _hasBusinessObject = false;

        bool _hasBusinessObjectEventsAttached = false;

        /// <summary>
        /// A hard coded value used in positioning the TextLengthCounter on the Canvas. To
        /// position the TextLengthCounter at the bottom of the active text control, this formula
        /// is used:<br/>
        /// TextLengthCounter.Top = TextControl.Top +TextControl.Height +
        /// _textLengthCounterTopOffset<br/>
        /// When the control container is a Grid rather than a Canvas, positioning the
        /// TextLengthCounter is much more complicated and is performed by the
        /// PositionTextLengthCounter.PositionTextLenghtLabel_InGrid method.
        /// </summary>
        int _textLengthCounterTopOffset = -7;
        /// <summary>
        /// A hard coded value used in positioning the TextLengthCounter on the Canvas. To
        /// position the TextLengthCounter so it’s Right Aligned with the active text control, this
        /// formula is used:<br/>
        /// TextLengthCounter.Top = TextControl.Left +TextControl.Width +
        /// _textLengthCounterLeftOffset<br/>
        /// When the control container is a Grid rather than a Canvas, positioning the
        /// TextLengthCounter is much more complicated and is performed by the
        /// PositionTextLengthCounter.PositionTextLenghtLabel_InGrid method.
        /// </summary>
        int _textLengthCounterLeftOffset = -36;
        /// <summary>
        /// 	<para>A Brush value used to set a data control’s border when the control is in a
        ///     valid state.</para>
        /// 	<para>
        ///         This value is set dynamically one time when ucWcTableColumnProps first loads from the
        ///         <see cref="ControlBrushes.DataControlValidColor">ControlBrushes.DataControlValidColor</see>
        ///         method. This function has been refactored to the <see cref="ControlBrushes">ControlBurshes</see> class so the hard coded value could be
        ///         alternatively be retrieved from a config file or data store.
        ///     </para>
        /// </summary>
        Brush brshDataControlValid;
        /// <summary>
        /// 	<para>A Brush value used to set a data control’s border when the control is in a
        ///     NON valid state (has one or more broken rules) and in a Dirty state.</para>
        /// 	<para><br/>
        ///     The two states ‘New – Not Dirty’ and ‘New – Dirty’ have a different appearance.
        ///     When a Properties screen first loads, some data controls may be Not-Valid by
        ///     default such as cases where the field is required and has not default value. In
        ///     this case, rather than giving it the Not-Valid appearance using a bright red
        ///     boarder which would be rather annoying and in-the-face of the user, a softer darker
        ///     red color is used to let user know this field is Not-Valid while not being too
        ///     abrasive or harsh on the eye (and emotions of the user).</para>
        /// 	<para>
        /// 		<br/>
        ///         This value is set dynamically one time when ucWcTableColumnProps first loads from the
        ///         <see cref="ControlBrushes.DataControlNotValidColor">ControlBrushes.DataControlNotValidColor</see>
        ///         method. This function has been refactored to the <see cref="ControlBrushes">ControlBurshes</see> class so the hard coded value could be
        ///         alternatively be retrieved from a config file or data store.
        ///     </para>
        /// </summary>
        Brush brshDataControlNotValid;
        /// <summary>
        /// 	<para>A Brush value used to set a data control’s border when the control is in a
        ///     NON valid state (has one or more broken rules) and in a New – Not Dirty
        ///     state.</para>
        /// 	<para><br/>
        ///     The two states ‘New – Not Dirty’ and ‘New – Dirty’ have a different appearance.
        ///     When a Properties screen first loads, some data controls may be Not-Valid by
        ///     default such as cases where the field is required and has not default value. In
        ///     this case, rather than giving it the Not-Valid appearance using a bright red
        ///     boarder which would be rather annoying and in-the-face of the user, a softer darker
        ///     red color is used to let user know this field is Not-Valid while not being too
        ///     abrasive or harsh on the eye (and emotions of the user).</para>
        /// 	<para>
        /// 		<br/>
        ///         This value is set dynamically one time when ucWcTableColumnProps first loads from the
        ///         <see cref="ControlBrushes.DataControlNotValidNewRecColor">ControlBrushes.DataControlNotValidNewRecColor</see>
        ///         method. This function has been refactored to the <see cref="ControlBrushes">ControlBurshes</see> class so the hard coded value could be
        ///         alternatively be retrieved from a config file or data store.
        ///     </para>
        /// </summary>
        Brush brshDataControlNotValidNewRec;

        /// <summary>
        ///  The class that manages the AdornerLabel class (the 'Text Length Label Adorner).  The AdornerLabel is used to 
        ///  show the remaining number of charectors available in text controls - mainly TextBoxes.
        /// </summary>
        AdornerLabelManager _lblAdrnMngr;

        bool _secuitySettingIsReadOnly = false;

        private DataControlEventManager _dataControlEventManager = new DataControlEventManager();

        private WcTableColumnService_ProxyWrapper _wcTableColumnProxy = null;
		private CommonClientDataService_ProxyWrapper _CommonClientDataServiceProxy = null;
		private WcApplicationVersionService_ProxyWrapper _WcApplicationVersionServiceProxy = null;
		private WcTableService_ProxyWrapper _WcTableServiceProxy = null;

		private ComboItemList _TbC_CtlTp_Id_ComboItemList = new ComboItemList(ComboItemList.DataType.IntegerType);
		private ComboItemList _TbC_DtDtNt_Id_ComboItemList = new ComboItemList(ComboItemList.DataType.IntegerType);
		private ComboItemList _TbC_DtSql_Id_ComboItemList = new ComboItemList(ComboItemList.DataType.IntegerType);
		private ComboItemList _TbC_ListStoredProc_Id_ComboItemList = new ComboItemList(ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);
		private ComboItemList _TbC_ComboListTable_Id_ComboItemList = new ComboItemList(ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);
		private ComboItemList _TbC_ComboListDisplayColumn_Id_ComboItemList = new ComboItemList(ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);
		private ComboItemList _TbC_ParentColumnKey_ComboItemList = new ComboItemList(ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);

        #endregion Initialize Variables


        #region Constructors

        /// <summary>
        /// Various events are wired up here and a call is made to the CustomConstructorCode
        /// method in the ucWcTableColumnProps.xaml.cust.cs partial class.
        /// </summary>
        public ucWcTableColumnProps()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcTableColumnProps", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();
                this.IsEnabled = false;
                this.Name = "WcTableColumnProps";
                _lblAdrnMngr = new AdornerLabelManager();
				_CommonClientDataServiceProxy = new CommonClientDataService_ProxyWrapper();
				_WcApplicationVersionServiceProxy = new WcApplicationVersionService_ProxyWrapper();
				_WcTableServiceProxy = new WcTableService_ProxyWrapper();

                InitializeProxyWrapper();
                CustomConstructorCode();
                FormatFields();
                ReadOnlyAssignments();

                ConfigureXamTileView();
                this.Loaded += new RoutedEventHandler(ucWcTableColumnProps_Loaded);

                _userContext.SecurityArtifactsRetrieved += new SecurityArtifactsRetrievedEventHandler(UserSecurityContext_SecurityArtifactsRetrieved);
                SetSecurityState();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcTableColumnProps", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_FORM_IS_LOADING = false; 
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcTableColumnProps", IfxTraceCategory.Leave);
            }
        }

        void ucWcTableColumnProps_Loaded(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnProps_Loaded", IfxTraceCategory.Enter);
                CreateBusinessRuleTooltips();
                SetControlEvents();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnProps_Loaded", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnProps_Loaded", IfxTraceCategory.Leave);
            }
        }


        #endregion Constructors


        #region Load this

        void InitializeProxyWrapper()
        {
            if (_wcTableColumnProxy == null)
            {
                _wcTableColumnProxy = new ProxyWrapper.WcTableColumnService_ProxyWrapper();
                //_wcTableColumnProxy.GetWcControlType_ComboItemListCompleted += new EventHandler<GetWcControlType_ComboItemListCompletedEventArgs>(WcTableColumnProxy_GetWcControlType_ComboItemListCompleted);
                //_wcTableColumnProxy.GetWcDataTypeSQL_ComboItemListCompleted += new EventHandler<GetWcDataTypeSQL_ComboItemListCompletedEventArgs>(WcTableColumnProxy_GetWcDataTypeSQL_ComboItemListCompleted);
                //_wcTableColumnProxy.GetWcDataTypeDotNet_ComboItemListCompleted += new EventHandler<GetWcDataTypeDotNet_ComboItemListCompletedEventArgs>(WcTableColumnProxy_GetWcDataTypeDotNet_ComboItemListCompleted);
                //_wcTableColumnProxy.GetWcStoredProc_ComboItemListCompleted += new EventHandler<GetWcStoredProc_ComboItemListCompletedEventArgs>(WcTableColumnProxy_GetWcStoredProc_ComboItemListCompleted);
                //_wcTableColumnProxy.GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted += new EventHandler<GetWcTableColumn_lstByApVersion_Id_ComboItemListCompletedEventArgs>(WcTableColumnProxy_GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted);
                //_wcTableColumnProxy.GetWcTable_ComboItemListCompleted += new EventHandler<GetWcTable_ComboItemListCompletedEventArgs>(WcTableColumnProxy_GetWcTable_ComboItemListCompleted);
                //_wcTableColumnProxy.GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted += new EventHandler<GetWcTableColumn_lstByApVersion_Id_ComboItemListCompletedEventArgs>(WcTableColumnProxy_GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted);
            }
        }

        /// <summary>
        /// This can be called internally from the constructor or optionally – externally
        /// after this control has loaded. Being called externally after all the other
        /// initialization code has executed allows the certain things to be preconfigured prior to
        /// running additional code such as loading data that may be dependent on these
        /// configurations being in place.
        /// </summary>
        public void LoadControl()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadControl", IfxTraceCategory.Enter);
                FLG_FORM_IS_LOADING = true;
//                brshDataControlValid = ControlBrushes.DataControlValidColor;
//                brshDataControlNotValid = ControlBrushes.DataControlNotValidColor;
//                brshDataControlNotValidNewRec = ControlBrushes.DataControlNotValidNewRecColor;
                if (objB == null)
                {
                    objB = new WcTableColumn_Bll();
                    _hasBusinessObject = true;
                    objB.NewEntityRow();
                }

                AddBusinessObjectEvents();
				TbC_CtlTp_Id_DataSource();
				TbC_DtSql_Id_DataSource();
				TbC_DtDtNt_Id_DataSource();
				TbC_ListStoredProc_Id_DataSource();
				TbC_ParentColumnKey_DataSource();
				TbC_ComboListTable_Id_DataSource();
				TbC_ComboListDisplayColumn_Id_DataSource();

                CustomLoadMethods();
                AssignTextLengthLabels();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadControl", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_FORM_IS_LOADING = false; 
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadControl", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>When a business object (Entity_Bll) is being loaded to support this control,
        ///     all events associated with the outgoing business object must be removed and then
        ///     reattached to the new business object such as:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="EntityBll.WcTableColumn_Bll.CurrentEntityStateChanged">CurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="EntityBll.WcTableColumn_Bll.BrokenRuleChanged">BrokenRuleChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="EntityBll.WcTableColumn_Bll.ControlValidStateChanged">ControlValidStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="EntityBll.WcTableColumn_Bll.RestrictedTextLengthChanged">RestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="CrudFailed">CrudFailed</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public void AddBusinessObjectEvents()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AddBusinessObjectEvents", IfxTraceCategory.Enter);
                if (objB == null) { return; }
                RemoveBusnessObjectEvents();
                objB.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                objB.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                objB.ControlValidStateChanged += new ControlValidStateChangedEventHandler(OnControlValidStateChanged);
                objB.CrudFailed += new CrudFailedEventHandler(OnCrudFailed);
                objB.EntityRowReceived += new EntityRowReceivedEventHandler(OnEntityRowReceived);
                objB.AsyncSaveWithResponseComplete += new AsyncSaveWithResponseCompleteEventHandler(objB_AsyncSaveWithResponseComplete);
                AddBusinessObjectEvents_CustomCode();
                _hasBusinessObjectEventsAttached = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AddBusinessObjectEvents", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_FORM_IS_LOADING = false; 
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AddBusinessObjectEvents", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>When a business object (Entity_Bll) is being unloaded prior to loading next
        ///     business object to support this control, all events associated with the business
        ///     object must be removed such as:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="EntityBll.WcTableColumn_Bll.CurrentEntityStateChanged">CurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="EntityBll.WcTableColumn_Bll.BrokenRuleChanged">BrokenRuleChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="EntityBll.WcTableColumn_Bll.ControlValidStateChanged">ControlValidStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="EntityBll.WcTableColumn_Bll.RestrictedTextLengthChanged">RestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="CrudFailed">CrudFailed</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public void RemoveBusnessObjectEvents()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RemoveBusnessObjectEvents", IfxTraceCategory.Enter);
                if (objB == null) { return; }
                objB.CurrentEntityStateChanged -= new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                objB.BrokenRuleChanged -= new BrokenRuleEventHandler(OnBrokenRuleChanged);
                objB.ControlValidStateChanged -= new ControlValidStateChangedEventHandler(OnControlValidStateChanged);
                objB.CrudFailed -= new CrudFailedEventHandler(OnCrudFailed);
                objB.EntityRowReceived -= new EntityRowReceivedEventHandler(OnEntityRowReceived);
                objB.AsyncSaveWithResponseComplete -= new AsyncSaveWithResponseCompleteEventHandler(objB_AsyncSaveWithResponseComplete);
                RemoveBusnessObjectEvents_CustomCode();
                _hasBusinessObjectEventsAttached = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RemoveBusnessObjectEvents", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RemoveBusnessObjectEvents", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Wires up all standard events for the data controls. For example, a reference
        ///     to each TextBox is passed onto the SetTextBoxEvents method where its events are
        ///     attached. This is a list of methods that may be called from this method:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="SetTextBoxEvents">SetTextBoxEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="SetTextBoxForStringsEvents">SetTextBoxForStringsEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="SetCheckBoxEvents">SetCheckBoxEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="SetRadioButtonEvents">SetRadioButtonEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="SetXamDateTimeEditorEvents">SetXamDateTimeEditorEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="SetComboBoxEvents">SetComboBoxEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="SetXamComboEditorEvents">SetXamComboEditorEvents</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="AttachToolTip">AttachToolTip</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void SetControlEvents()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlEvents", IfxTraceCategory.Enter);
                //TEXT BOXES
				SetTextBoxEvents(TbC_SortOrder);
				SetTextBoxEvents(TbC_Name);
				SetTextBoxEvents(TbC_Description);
				SetTextBoxEvents(TbC_Length);
				SetTextBoxEvents(TbC_Precision);
				SetTextBoxEvents(TbC_Scale);
				SetTextBoxEvents(TbC_BrokenRuleText);
				SetTextBoxEvents(TbC_DefaultValue);
				SetTextBoxEvents(TbC_DefaultCaption);
				SetTextBoxEvents(TbC_ColumnHeaderText);
				SetTextBoxEvents(TbC_LabelCaptionVerbose);
				SetTextBoxEvents(TbC_TextBoxFormat);
				SetTextBoxEvents(TbC_TextColumnFormat);
				SetTextBoxEvents(TbC_ColumnWidth);
				SetTextBoxEvents(TbC_TextBoxTextAlignment);
				SetTextBoxEvents(TbC_ColumnTextAlignment);
				SetTextBoxEvents(TbC_Combo_MaxDropdownHeight);
				SetTextBoxEvents(TbC_Combo_MaxDropdownWidth);
				SetTextBoxEvents(TbC_DeveloperNote);
				SetTextBoxEvents(TbC_UserNote);
				SetTextBoxEvents(TbC_HelpFileAdditionalNote);
				SetTextBoxEvents(TbC_Notes);
				SetTextBoxEvents(TbC_ColumnGroups);

				//DATETIME CONTROLS   DatePicker

				//TIME CONTROLS   TimePicker

				//COMBO BOXES

				//vXamComboEditor
				SetVXamComboEditorEvents(TbC_CtlTp_Id);
				SetVXamComboEditorEvents(TbC_DtSql_Id);
				SetVXamComboEditorEvents(TbC_DtDtNt_Id);
				SetVXamComboEditorEvents(TbC_ListStoredProc_Id);
				SetVXamComboEditorEvents(TbC_ParentColumnKey);
				SetVXamComboEditorEvents(TbC_ComboListTable_Id);
				SetVXamComboEditorEvents(TbC_ComboListDisplayColumn_Id);

				//vXamMultiColumnComboEditor

				//CHECK BOXES
				SetCheckBoxEvents(TbC_IsNonvColumn);
				SetCheckBoxEvents(TbC_IsPK);
				SetCheckBoxEvents(TbC_IsIdentity);
				SetCheckBoxEvents(TbC_IsFK);
				SetCheckBoxEvents(TbC_IsEntityColumn);
				SetCheckBoxEvents(TbC_IsSystemField);
				SetCheckBoxEvents(TbC_IsValuesObjectMember);
				SetCheckBoxEvents(TbC_IsInPropsScreen);
				SetCheckBoxEvents(TbC_IsInNavList);
				SetCheckBoxEvents(TbC_IsRequired);
				SetCheckBoxEvents(TbC_AllowZero);
				SetCheckBoxEvents(TbC_IsNullableInDb);
				SetCheckBoxEvents(TbC_IsNullableInUI);
				SetCheckBoxEvents(TbC_IsReadFromDb);
				SetCheckBoxEvents(TbC_IsSendToDb);
				SetCheckBoxEvents(TbC_IsInsertAllowed);
				SetCheckBoxEvents(TbC_IsEditAllowed);
				SetCheckBoxEvents(TbC_IsReadOnlyInUI);
				SetCheckBoxEvents(TbC_UseForAudit);
				SetCheckBoxEvents(TbC_LabelCaptionGenerate);
				SetCheckBoxEvents(Tbc_ShowGridColumnToolTip);
				SetCheckBoxEvents(Tbc_ShowPropsToolTip);
				SetCheckBoxEvents(TbC_IsCreatePropsStrings);
				SetCheckBoxEvents(TbC_IsCreateGridStrings);
				SetCheckBoxEvents(TbC_IsAvailableForColumnGroups);
				SetCheckBoxEvents(TbC_IsTextWrapInProp);
				SetCheckBoxEvents(TbC_IsTextWrapInGrid);
				SetCheckBoxEvents(TbC_IsStaticList);
				SetCheckBoxEvents(TbC_UseNotInList);
				SetCheckBoxEvents(TbC_UseListEditBtn);
				SetCheckBoxEvents(TbC_UseDisplayTextFieldProperty);
				SetCheckBoxEvents(TbC_IsDisplayTextFieldProperty);
				SetCheckBoxEvents(TbC_Combo_AllowDropdownResizing);
				SetCheckBoxEvents(TbC_Combo_IsResetButtonVisible);
				SetCheckBoxEvents(TbC_IsActiveRecColumn);
				SetCheckBoxEvents(TbC_IsDeletedColumn);
				SetCheckBoxEvents(TbC_IsCreatedUserIdColumn);
				SetCheckBoxEvents(TbC_IsCreatedDateColumn);
				SetCheckBoxEvents(TbC_IsUserIdColumn);
				SetCheckBoxEvents(TbC_IsModifiedDateColumn);
				SetCheckBoxEvents(TbC_IsRowVersionStampColumn);
				SetCheckBoxEvents(TbC_IsBrowsable);
				SetCheckBoxEvents(TbC_IsInputComplete);
				SetCheckBoxEvents(TbC_IsCodeGen);
				SetCheckBoxEvents(TbC_IsReadyCodeGen);
				SetCheckBoxEvents(TbC_IsCodeGenComplete);
				SetCheckBoxEvents(TbC_IsTagForCodeGen);
				SetCheckBoxEvents(TbC_IsTagForOther);
				SetCheckBoxEvents(TbC_IsActiveRow);

				//RADIO BUTTONS

				//vRadioButtonGroup CONTROLS

				// ColorPicker

				//TOOLTIPS FOR BROKEN RULES
				AttachToolTip(TbC_SortOrder);
				AttachToolTip(TbC_Name);
				AttachToolTip(TbC_CtlTp_Id);
				AttachToolTip(TbC_IsNonvColumn);
				AttachToolTip(TbC_Description);
				AttachToolTip(TbC_DtSql_Id);
				AttachToolTip(TbC_DtDtNt_Id);
				AttachToolTip(TbC_Scale);
				AttachToolTip(TbC_IsPK);
				AttachToolTip(TbC_IsIdentity);
				AttachToolTip(TbC_IsFK);
				AttachToolTip(TbC_IsEntityColumn);
				AttachToolTip(TbC_IsSystemField);
				AttachToolTip(TbC_IsValuesObjectMember);
				AttachToolTip(TbC_IsInPropsScreen);
				AttachToolTip(TbC_IsInNavList);
				AttachToolTip(TbC_IsRequired);
				AttachToolTip(TbC_BrokenRuleText);
				AttachToolTip(TbC_AllowZero);
				AttachToolTip(TbC_IsNullableInDb);
				AttachToolTip(TbC_IsNullableInUI);
				AttachToolTip(TbC_DefaultValue);
				AttachToolTip(TbC_IsReadFromDb);
				AttachToolTip(TbC_IsSendToDb);
				AttachToolTip(TbC_IsInsertAllowed);
				AttachToolTip(TbC_IsEditAllowed);
				AttachToolTip(TbC_IsReadOnlyInUI);
				AttachToolTip(TbC_UseForAudit);
				AttachToolTip(TbC_DefaultCaption);
				AttachToolTip(TbC_ColumnHeaderText);
				AttachToolTip(TbC_LabelCaptionVerbose);
				AttachToolTip(TbC_LabelCaptionGenerate);
				AttachToolTip(Tbc_ShowGridColumnToolTip);
				AttachToolTip(Tbc_ShowPropsToolTip);
				AttachToolTip(TbC_IsCreatePropsStrings);
				AttachToolTip(TbC_IsCreateGridStrings);
				AttachToolTip(TbC_IsAvailableForColumnGroups);
				AttachToolTip(TbC_IsTextWrapInProp);
				AttachToolTip(TbC_IsTextWrapInGrid);
				AttachToolTip(TbC_TextBoxFormat);
				AttachToolTip(TbC_TextColumnFormat);
				AttachToolTip(TbC_ColumnWidth);
				AttachToolTip(TbC_TextBoxTextAlignment);
				AttachToolTip(TbC_ColumnTextAlignment);
				AttachToolTip(TbC_IsStaticList);
				AttachToolTip(TbC_UseNotInList);
				AttachToolTip(TbC_UseListEditBtn);
				AttachToolTip(TbC_UseDisplayTextFieldProperty);
				AttachToolTip(TbC_IsDisplayTextFieldProperty);
				AttachToolTip(TbC_Combo_MaxDropdownHeight);
				AttachToolTip(TbC_Combo_MaxDropdownWidth);
				AttachToolTip(TbC_Combo_AllowDropdownResizing);
				AttachToolTip(TbC_Combo_IsResetButtonVisible);
				AttachToolTip(TbC_IsActiveRecColumn);
				AttachToolTip(TbC_IsDeletedColumn);
				AttachToolTip(TbC_IsCreatedUserIdColumn);
				AttachToolTip(TbC_IsCreatedDateColumn);
				AttachToolTip(TbC_IsUserIdColumn);
				AttachToolTip(TbC_IsModifiedDateColumn);
				AttachToolTip(TbC_IsRowVersionStampColumn);
				AttachToolTip(TbC_IsBrowsable);
				AttachToolTip(TbC_DeveloperNote);
				AttachToolTip(TbC_UserNote);
				AttachToolTip(TbC_HelpFileAdditionalNote);
				AttachToolTip(TbC_Notes);
				AttachToolTip(TbC_IsInputComplete);
				AttachToolTip(TbC_IsCodeGen);
				AttachToolTip(TbC_IsReadyCodeGen);
				AttachToolTip(TbC_IsCodeGenComplete);
				AttachToolTip(TbC_IsTagForCodeGen);
				AttachToolTip(TbC_IsTagForOther);
				AttachToolTip(TbC_ColumnGroups);
				AttachToolTip(TbC_IsActiveRow);

                SetControlEvents_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlEvents", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlEvents", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>
        ///         This sets the state of the UI by loading the data of a newly loaded business
        ///         object (<see cref="EntityBll.WcTableColumn_Bll">WcTableColumn_Bll</see>) into all of the data
        ///         fields, refreshing any lists which need to be updated with the new data and any
        ///         other configurations that need to be made after loading a business object.
        ///         There are 2 types of business object <see cref="TypeServices.EntityState">states</see> that affect the UI at this point.
        ///     </para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>Existing-Saved (Valid)</item>
        /// 		<item>New-NotDirty (Valid or UnValid)</item>
        /// 	</list>
        /// 	<para>
        ///         This method is called when loading a business object into this control, by
        ///         executing the <see cref="UnDo">UnDo</see> method where the UI’s state is
        ///         returned to its original state – one of the 2 bullets listed above or by
        ///         executing the <see cref="NewEntityRow">NewEntityRow</see> method (which also executes after
        ///         a delete action has been performed if deletes are allowed).
        ///     </para>
        /// </summary>
        void SetState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetState", IfxTraceCategory.Enter);
                FLG_LOADING_REC = true;
                // Remove all TextBox TextChanged Events so they don't fire when we set thier new values.  The TextChanged Event is unpredictable in SL so we need to remove it here.
                DataControlDataChangedEvents_RemoveAll();
				TbC_SortOrder.Text = objB.TbC_SortOrder_asString;
				if (objB.TbC_Name == null)
                {
                    TbC_Name.Text = "";
                }
                else
                {
                    TbC_Name.Text = objB.TbC_Name;
                }

                Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_CtlTp_Id, objB.TbC_CtlTp_Id);
				TbC_IsNonvColumn.IsChecked = objB.TbC_IsNonvColumn;
				if (objB.TbC_Description == null)
                {
                    TbC_Description.Text = "";
                }
                else
                {
                    TbC_Description.Text = objB.TbC_Description;
                }

                Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_DtSql_Id, objB.TbC_DtSql_Id);

                Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_DtDtNt_Id, objB.TbC_DtDtNt_Id);
				TbC_Length.Text = objB.TbC_Length_asString;
				TbC_Precision.Text = objB.TbC_Precision_asString;
				TbC_Scale.Text = objB.TbC_Scale_asString;
				TbC_IsPK.IsChecked = objB.TbC_IsPK;
				TbC_IsIdentity.IsChecked = objB.TbC_IsIdentity;
				TbC_IsFK.IsChecked = objB.TbC_IsFK;
				TbC_IsEntityColumn.IsChecked = objB.TbC_IsEntityColumn;
				TbC_IsSystemField.IsChecked = objB.TbC_IsSystemField;
				TbC_IsValuesObjectMember.IsChecked = objB.TbC_IsValuesObjectMember;
				TbC_IsInPropsScreen.IsChecked = objB.TbC_IsInPropsScreen;
				TbC_IsInNavList.IsChecked = objB.TbC_IsInNavList;
				TbC_IsRequired.IsChecked = objB.TbC_IsRequired;
				if (objB.TbC_BrokenRuleText == null)
                {
                    TbC_BrokenRuleText.Text = "";
                }
                else
                {
                    TbC_BrokenRuleText.Text = objB.TbC_BrokenRuleText;
                }
				TbC_AllowZero.IsChecked = objB.TbC_AllowZero;
				TbC_IsNullableInDb.IsChecked = objB.TbC_IsNullableInDb;
				TbC_IsNullableInUI.IsChecked = objB.TbC_IsNullableInUI;
				if (objB.TbC_DefaultValue == null)
                {
                    TbC_DefaultValue.Text = "";
                }
                else
                {
                    TbC_DefaultValue.Text = objB.TbC_DefaultValue;
                }
				TbC_IsReadFromDb.IsChecked = objB.TbC_IsReadFromDb;
				TbC_IsSendToDb.IsChecked = objB.TbC_IsSendToDb;
				TbC_IsInsertAllowed.IsChecked = objB.TbC_IsInsertAllowed;
				TbC_IsEditAllowed.IsChecked = objB.TbC_IsEditAllowed;
				TbC_IsReadOnlyInUI.IsChecked = objB.TbC_IsReadOnlyInUI;
				TbC_UseForAudit.IsChecked = objB.TbC_UseForAudit;
				if (objB.TbC_DefaultCaption == null)
                {
                    TbC_DefaultCaption.Text = "";
                }
                else
                {
                    TbC_DefaultCaption.Text = objB.TbC_DefaultCaption;
                }
				if (objB.TbC_ColumnHeaderText == null)
                {
                    TbC_ColumnHeaderText.Text = "";
                }
                else
                {
                    TbC_ColumnHeaderText.Text = objB.TbC_ColumnHeaderText;
                }
				if (objB.TbC_LabelCaptionVerbose == null)
                {
                    TbC_LabelCaptionVerbose.Text = "";
                }
                else
                {
                    TbC_LabelCaptionVerbose.Text = objB.TbC_LabelCaptionVerbose;
                }
				TbC_LabelCaptionGenerate.IsChecked = objB.TbC_LabelCaptionGenerate;
				Tbc_ShowGridColumnToolTip.IsChecked = objB.Tbc_ShowGridColumnToolTip;
				Tbc_ShowPropsToolTip.IsChecked = objB.Tbc_ShowPropsToolTip;
				TbC_IsCreatePropsStrings.IsChecked = objB.TbC_IsCreatePropsStrings;
				TbC_IsCreateGridStrings.IsChecked = objB.TbC_IsCreateGridStrings;
				TbC_IsAvailableForColumnGroups.IsChecked = objB.TbC_IsAvailableForColumnGroups;
				TbC_IsTextWrapInProp.IsChecked = objB.TbC_IsTextWrapInProp;
				TbC_IsTextWrapInGrid.IsChecked = objB.TbC_IsTextWrapInGrid;
				if (objB.TbC_TextBoxFormat == null)
                {
                    TbC_TextBoxFormat.Text = "";
                }
                else
                {
                    TbC_TextBoxFormat.Text = objB.TbC_TextBoxFormat;
                }
				if (objB.TbC_TextColumnFormat == null)
                {
                    TbC_TextColumnFormat.Text = "";
                }
                else
                {
                    TbC_TextColumnFormat.Text = objB.TbC_TextColumnFormat;
                }
				TbC_ColumnWidth.Text = objB.TbC_ColumnWidth_asString;
				if (objB.TbC_TextBoxTextAlignment == null)
                {
                    TbC_TextBoxTextAlignment.Text = "";
                }
                else
                {
                    TbC_TextBoxTextAlignment.Text = objB.TbC_TextBoxTextAlignment;
                }
				if (objB.TbC_ColumnTextAlignment == null)
                {
                    TbC_ColumnTextAlignment.Text = "";
                }
                else
                {
                    TbC_ColumnTextAlignment.Text = objB.TbC_ColumnTextAlignment;
                }

                Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_ListStoredProc_Id, objB.TbC_ListStoredProc_Id);
				TbC_IsStaticList.IsChecked = objB.TbC_IsStaticList;
				TbC_UseNotInList.IsChecked = objB.TbC_UseNotInList;
				TbC_UseListEditBtn.IsChecked = objB.TbC_UseListEditBtn;

                Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_ParentColumnKey, objB.TbC_ParentColumnKey);
				TbC_UseDisplayTextFieldProperty.IsChecked = objB.TbC_UseDisplayTextFieldProperty;
				TbC_IsDisplayTextFieldProperty.IsChecked = objB.TbC_IsDisplayTextFieldProperty;

                Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_ComboListTable_Id, objB.TbC_ComboListTable_Id);

                Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_ComboListDisplayColumn_Id, objB.TbC_ComboListDisplayColumn_Id);
				TbC_Combo_MaxDropdownHeight.Text = objB.TbC_Combo_MaxDropdownHeight_asString;
				TbC_Combo_MaxDropdownWidth.Text = objB.TbC_Combo_MaxDropdownWidth_asString;
				TbC_Combo_AllowDropdownResizing.IsChecked = objB.TbC_Combo_AllowDropdownResizing;
				TbC_Combo_IsResetButtonVisible.IsChecked = objB.TbC_Combo_IsResetButtonVisible;
				TbC_IsActiveRecColumn.IsChecked = objB.TbC_IsActiveRecColumn;
				TbC_IsDeletedColumn.IsChecked = objB.TbC_IsDeletedColumn;
				TbC_IsCreatedUserIdColumn.IsChecked = objB.TbC_IsCreatedUserIdColumn;
				TbC_IsCreatedDateColumn.IsChecked = objB.TbC_IsCreatedDateColumn;
				TbC_IsUserIdColumn.IsChecked = objB.TbC_IsUserIdColumn;
				TbC_IsModifiedDateColumn.IsChecked = objB.TbC_IsModifiedDateColumn;
				TbC_IsRowVersionStampColumn.IsChecked = objB.TbC_IsRowVersionStampColumn;
				TbC_IsBrowsable.IsChecked = objB.TbC_IsBrowsable;
				if (objB.TbC_DeveloperNote == null)
                {
                    TbC_DeveloperNote.Text = "";
                }
                else
                {
                    TbC_DeveloperNote.Text = objB.TbC_DeveloperNote;
                }
				if (objB.TbC_UserNote == null)
                {
                    TbC_UserNote.Text = "";
                }
                else
                {
                    TbC_UserNote.Text = objB.TbC_UserNote;
                }
				if (objB.TbC_HelpFileAdditionalNote == null)
                {
                    TbC_HelpFileAdditionalNote.Text = "";
                }
                else
                {
                    TbC_HelpFileAdditionalNote.Text = objB.TbC_HelpFileAdditionalNote;
                }
				if (objB.TbC_Notes == null)
                {
                    TbC_Notes.Text = "";
                }
                else
                {
                    TbC_Notes.Text = objB.TbC_Notes;
                }
				TbC_IsInputComplete.IsChecked = objB.TbC_IsInputComplete;
				TbC_IsCodeGen.IsChecked = objB.TbC_IsCodeGen;
				TbC_IsReadyCodeGen.IsChecked = objB.TbC_IsReadyCodeGen;
				TbC_IsCodeGenComplete.IsChecked = objB.TbC_IsCodeGenComplete;
				TbC_IsTagForCodeGen.IsChecked = objB.TbC_IsTagForCodeGen;
				TbC_IsTagForOther.IsChecked = objB.TbC_IsTagForOther;
				if (objB.TbC_ColumnGroups == null)
                {
                    TbC_ColumnGroups.Text = "";
                }
                else
                {
                    TbC_ColumnGroups.Text = objB.TbC_ColumnGroups;
                }
				TbC_IsActiveRow.IsChecked = objB.TbC_IsActiveRow;

                SetState_CustomCode();
                //  Reset all TextBox TextChanged Events now.
                DataControlDataChangedEvents_SetAll();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_LOADING_REC = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetState", IfxTraceCategory.Leave);
            }
        }

        void Set_ComboEditorItem_SelectedItem_ComboItemType(XamComboEditor cmb, object value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_ComboItemType", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }

                ComboItemList itemsSource = (ComboItemList)cmb.ItemsSource;
                if (itemsSource == null) {return;}

                switch (itemsSource.Id_DataType)
                {
                    case ComboItemList.DataType.GuidType:
                        foreach (ComboEditorItem obj in cmb.Items)
                        {
                            if ((Guid)((ComboItem)obj.Data).Id == (Guid)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.LongIntegerType:
                        foreach (ComboEditorItem obj in cmb.Items)
                        {
                            if ((long)((ComboItem)obj.Data).Id == (long)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.IntegerType:
                        foreach (ComboEditorItem obj in cmb.Items)
                        {
                            if ((int)((ComboItem)obj.Data).Id == (int)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.ShortType:
                        foreach (ComboEditorItem obj in cmb.Items)
                        {
                            if ((short)((ComboItem)obj.Data).Id == (short)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.ByteType:
                        foreach (ComboEditorItem obj in cmb.Items)
                        {
                            if ((byte)((ComboItem)obj.Data).Id == (byte)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.StringType:
                        foreach (ComboEditorItem obj in cmb.Items)
                        {
                            if ((string)((ComboItem)obj.Data).Id == (string)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.NA:
                        throw new Exception("UIControls.ucWcTableColumnProps.Set_ComboEditorItem_SelectedItem_ComboItemType():  DataType not found in switch statement.");
                }
                // Items was not found in the list so make sure nothing is selected.
                cmb.SelectedIndex = -1;
                return;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_ComboItemType", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_ComboItemType", IfxTraceCategory.Leave);
            }
        }

        void Set_ComboEditorItem_SelectedItem_Int(XamComboEditor cmb, int? value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_Int", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }
                foreach (ComboEditorItem obj in cmb.Items)
                {
                    if (((IWireTypeBinding)obj.Data).Get_Int_Id() == value)
                    {
                        obj.IsSelected = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_Int", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_Int", IfxTraceCategory.Leave);
            }
        }

        void Set_ComboEditorItem_SelectedItem_Guid(XamComboEditor cmb, Guid? value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_Guid", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }
                foreach (ComboEditorItem obj in cmb.Items)
                {
                    if (((IWireTypeBinding)obj.Data).Get_Guid_Id() == value)
                    {
                        obj.IsSelected = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_Guid", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_Guid", IfxTraceCategory.Leave);
            }
        }

        void Set_ComboEditorItem_SelectedItem_String(XamComboEditor cmb, string value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_String", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }
                foreach (ComboEditorItem obj in cmb.Items)
                {
                    if (((IWireTypeBinding)obj.Data).Get_String_Id() == value)
                    {
                        obj.IsSelected = true;
                        return;
                    }
                }
            }            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_String", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_ComboEditorItem_SelectedItem_String", IfxTraceCategory.Leave);
            }

        }

        void Set_MultiColumnComboEditorItem_SelectedItem_ComboItemType(XamMultiColumnComboEditor cmb, object value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_ComboItemType", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }

                ComboItemList itemsSource = (ComboItemList)cmb.ItemsSource;

                switch (itemsSource.Id_DataType)
                {
                    case ComboItemList.DataType.GuidType:
                        foreach (ComboRow obj in cmb.Items)
                        {
                            if ((Guid)((ComboItem)obj.Data).Id == (Guid)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.LongIntegerType:
                        foreach (ComboRow obj in cmb.Items)
                        {
                            if ((int)((ComboItem)obj.Data).Id == (long)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.IntegerType:
                        foreach (ComboRow obj in cmb.Items)
                        {
                            if ((int)((ComboItem)obj.Data).Id == (int)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.ShortType:
                        foreach (ComboRow obj in cmb.Items)
                        {
                            if ((int)((ComboItem)obj.Data).Id == (short)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.ByteType:
                        foreach (ComboRow obj in cmb.Items)
                        {
                            if ((int)((ComboItem)obj.Data).Id == (byte)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.StringType:
                        foreach (ComboRow obj in cmb.Items)
                        {
                            if ((string)((ComboItem)obj.Data).Id == (string)value)
                            {
                                obj.IsSelected = true;
                                return;
                            }
                        }
                        break;
                    case ComboItemList.DataType.NA:
                        throw new Exception("UIControls.ucWcTableColumnProps.Set_MultiColumnComboEditorItem_SelectedItem_ComboItemType():  DataType not found in switch statement.");
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_ComboItemType", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_ComboItemType", IfxTraceCategory.Leave);
            }
        }

        void Set_MultiColumnComboEditorItem_SelectedItem_Int(XamMultiColumnComboEditor cmb, int? value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_Int", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }
                foreach (ComboRow obj in cmb.Items)
                {
                    if (((IWireTypeBinding)obj.Data).Get_Int_Id() == value)
                    {
                        obj.IsSelected = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_Int", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_Int", IfxTraceCategory.Leave);
            }
        }

        void Set_MultiColumnComboEditorItem_SelectedItem_Guid(XamMultiColumnComboEditor cmb, Guid? value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_Guid", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }
                foreach (ComboRow obj in cmb.Items)
                {
                    if (((IWireTypeBinding)obj.Data).Get_Guid_Id() == value)
                    {
                        obj.IsSelected = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_Guid", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_Guid", IfxTraceCategory.Leave);
            }
        }

        void Set_MultiColumnComboEditorItem_SelectedItem_String(XamMultiColumnComboEditor cmb, string value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_String", IfxTraceCategory.Enter);
                if (value == null)
                {
                    cmb.SelectedIndex = -1;
                    return;
                }
                foreach (ComboRow obj in cmb.Items)
                {
                    if (((IWireTypeBinding)obj.Data).Get_String_Id() == value)
                    {
                        obj.IsSelected = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_String", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_MultiColumnComboEditorItem_SelectedItem_String", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Passes a reference of each control that has one or more validation rules into the
        ///     <see cref="SetControlDefalutValidAppearance">SetControlDefalutValidAppearance</see>
        ///     method where the default valid appearance is set. The return value of the business
        ///     object’s <see cref="EntityBll.WcTableColumn_Bll.IsPropertyValid">IsPropertyValid</see>
        ///     function is also passed into this method.
        /// </summary>
        void SetControlsDefaultValidState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlsDefaultValidState", IfxTraceCategory.Enter);

                #region Fields with validation

				SetControlDefaultValidAppearance(TbC_SortOrder, objB.IsPropertyValid("TbC_SortOrder"));
				SetControlDefaultValidAppearance(TbC_Name, objB.IsPropertyValid("TbC_Name"));
				SetControlDefaultValidAppearance(TbC_CtlTp_Id, objB.IsPropertyValid("TbC_CtlTp_Id"));
				SetControlDefaultValidAppearance(TbC_IsNonvColumn, objB.IsPropertyValid("TbC_IsNonvColumn"));
				SetControlDefaultValidAppearance(TbC_Description, objB.IsPropertyValid("TbC_Description"));
				SetControlDefaultValidAppearance(TbC_DtSql_Id, objB.IsPropertyValid("TbC_DtSql_Id"));
				SetControlDefaultValidAppearance(TbC_DtDtNt_Id, objB.IsPropertyValid("TbC_DtDtNt_Id"));
				SetControlDefaultValidAppearance(TbC_Scale, objB.IsPropertyValid("TbC_Scale"));
				SetControlDefaultValidAppearance(TbC_IsPK, objB.IsPropertyValid("TbC_IsPK"));
				SetControlDefaultValidAppearance(TbC_IsIdentity, objB.IsPropertyValid("TbC_IsIdentity"));
				SetControlDefaultValidAppearance(TbC_IsFK, objB.IsPropertyValid("TbC_IsFK"));
				SetControlDefaultValidAppearance(TbC_IsEntityColumn, objB.IsPropertyValid("TbC_IsEntityColumn"));
				SetControlDefaultValidAppearance(TbC_IsSystemField, objB.IsPropertyValid("TbC_IsSystemField"));
				SetControlDefaultValidAppearance(TbC_IsValuesObjectMember, objB.IsPropertyValid("TbC_IsValuesObjectMember"));
				SetControlDefaultValidAppearance(TbC_IsInPropsScreen, objB.IsPropertyValid("TbC_IsInPropsScreen"));
				SetControlDefaultValidAppearance(TbC_IsInNavList, objB.IsPropertyValid("TbC_IsInNavList"));
				SetControlDefaultValidAppearance(TbC_IsRequired, objB.IsPropertyValid("TbC_IsRequired"));
				SetControlDefaultValidAppearance(TbC_BrokenRuleText, objB.IsPropertyValid("TbC_BrokenRuleText"));
				SetControlDefaultValidAppearance(TbC_AllowZero, objB.IsPropertyValid("TbC_AllowZero"));
				SetControlDefaultValidAppearance(TbC_IsNullableInDb, objB.IsPropertyValid("TbC_IsNullableInDb"));
				SetControlDefaultValidAppearance(TbC_IsNullableInUI, objB.IsPropertyValid("TbC_IsNullableInUI"));
				SetControlDefaultValidAppearance(TbC_DefaultValue, objB.IsPropertyValid("TbC_DefaultValue"));
				SetControlDefaultValidAppearance(TbC_IsReadFromDb, objB.IsPropertyValid("TbC_IsReadFromDb"));
				SetControlDefaultValidAppearance(TbC_IsSendToDb, objB.IsPropertyValid("TbC_IsSendToDb"));
				SetControlDefaultValidAppearance(TbC_IsInsertAllowed, objB.IsPropertyValid("TbC_IsInsertAllowed"));
				SetControlDefaultValidAppearance(TbC_IsEditAllowed, objB.IsPropertyValid("TbC_IsEditAllowed"));
				SetControlDefaultValidAppearance(TbC_IsReadOnlyInUI, objB.IsPropertyValid("TbC_IsReadOnlyInUI"));
				SetControlDefaultValidAppearance(TbC_UseForAudit, objB.IsPropertyValid("TbC_UseForAudit"));
				SetControlDefaultValidAppearance(TbC_DefaultCaption, objB.IsPropertyValid("TbC_DefaultCaption"));
				SetControlDefaultValidAppearance(TbC_ColumnHeaderText, objB.IsPropertyValid("TbC_ColumnHeaderText"));
				SetControlDefaultValidAppearance(TbC_LabelCaptionVerbose, objB.IsPropertyValid("TbC_LabelCaptionVerbose"));
				SetControlDefaultValidAppearance(TbC_LabelCaptionGenerate, objB.IsPropertyValid("TbC_LabelCaptionGenerate"));
				SetControlDefaultValidAppearance(Tbc_ShowGridColumnToolTip, objB.IsPropertyValid("Tbc_ShowGridColumnToolTip"));
				SetControlDefaultValidAppearance(Tbc_ShowPropsToolTip, objB.IsPropertyValid("Tbc_ShowPropsToolTip"));
				SetControlDefaultValidAppearance(TbC_IsCreatePropsStrings, objB.IsPropertyValid("TbC_IsCreatePropsStrings"));
				SetControlDefaultValidAppearance(TbC_IsCreateGridStrings, objB.IsPropertyValid("TbC_IsCreateGridStrings"));
				SetControlDefaultValidAppearance(TbC_IsAvailableForColumnGroups, objB.IsPropertyValid("TbC_IsAvailableForColumnGroups"));
				SetControlDefaultValidAppearance(TbC_IsTextWrapInProp, objB.IsPropertyValid("TbC_IsTextWrapInProp"));
				SetControlDefaultValidAppearance(TbC_IsTextWrapInGrid, objB.IsPropertyValid("TbC_IsTextWrapInGrid"));
				SetControlDefaultValidAppearance(TbC_TextBoxFormat, objB.IsPropertyValid("TbC_TextBoxFormat"));
				SetControlDefaultValidAppearance(TbC_TextColumnFormat, objB.IsPropertyValid("TbC_TextColumnFormat"));
				SetControlDefaultValidAppearance(TbC_ColumnWidth, objB.IsPropertyValid("TbC_ColumnWidth"));
				SetControlDefaultValidAppearance(TbC_TextBoxTextAlignment, objB.IsPropertyValid("TbC_TextBoxTextAlignment"));
				SetControlDefaultValidAppearance(TbC_ColumnTextAlignment, objB.IsPropertyValid("TbC_ColumnTextAlignment"));
				SetControlDefaultValidAppearance(TbC_IsStaticList, objB.IsPropertyValid("TbC_IsStaticList"));
				SetControlDefaultValidAppearance(TbC_UseNotInList, objB.IsPropertyValid("TbC_UseNotInList"));
				SetControlDefaultValidAppearance(TbC_UseListEditBtn, objB.IsPropertyValid("TbC_UseListEditBtn"));
				SetControlDefaultValidAppearance(TbC_UseDisplayTextFieldProperty, objB.IsPropertyValid("TbC_UseDisplayTextFieldProperty"));
				SetControlDefaultValidAppearance(TbC_IsDisplayTextFieldProperty, objB.IsPropertyValid("TbC_IsDisplayTextFieldProperty"));
				SetControlDefaultValidAppearance(TbC_Combo_MaxDropdownHeight, objB.IsPropertyValid("TbC_Combo_MaxDropdownHeight"));
				SetControlDefaultValidAppearance(TbC_Combo_MaxDropdownWidth, objB.IsPropertyValid("TbC_Combo_MaxDropdownWidth"));
				SetControlDefaultValidAppearance(TbC_Combo_AllowDropdownResizing, objB.IsPropertyValid("TbC_Combo_AllowDropdownResizing"));
				SetControlDefaultValidAppearance(TbC_Combo_IsResetButtonVisible, objB.IsPropertyValid("TbC_Combo_IsResetButtonVisible"));
				SetControlDefaultValidAppearance(TbC_IsActiveRecColumn, objB.IsPropertyValid("TbC_IsActiveRecColumn"));
				SetControlDefaultValidAppearance(TbC_IsDeletedColumn, objB.IsPropertyValid("TbC_IsDeletedColumn"));
				SetControlDefaultValidAppearance(TbC_IsCreatedUserIdColumn, objB.IsPropertyValid("TbC_IsCreatedUserIdColumn"));
				SetControlDefaultValidAppearance(TbC_IsCreatedDateColumn, objB.IsPropertyValid("TbC_IsCreatedDateColumn"));
				SetControlDefaultValidAppearance(TbC_IsUserIdColumn, objB.IsPropertyValid("TbC_IsUserIdColumn"));
				SetControlDefaultValidAppearance(TbC_IsModifiedDateColumn, objB.IsPropertyValid("TbC_IsModifiedDateColumn"));
				SetControlDefaultValidAppearance(TbC_IsRowVersionStampColumn, objB.IsPropertyValid("TbC_IsRowVersionStampColumn"));
				SetControlDefaultValidAppearance(TbC_IsBrowsable, objB.IsPropertyValid("TbC_IsBrowsable"));
				SetControlDefaultValidAppearance(TbC_DeveloperNote, objB.IsPropertyValid("TbC_DeveloperNote"));
				SetControlDefaultValidAppearance(TbC_UserNote, objB.IsPropertyValid("TbC_UserNote"));
				SetControlDefaultValidAppearance(TbC_HelpFileAdditionalNote, objB.IsPropertyValid("TbC_HelpFileAdditionalNote"));
				SetControlDefaultValidAppearance(TbC_Notes, objB.IsPropertyValid("TbC_Notes"));
				SetControlDefaultValidAppearance(TbC_IsInputComplete, objB.IsPropertyValid("TbC_IsInputComplete"));
				SetControlDefaultValidAppearance(TbC_IsCodeGen, objB.IsPropertyValid("TbC_IsCodeGen"));
				SetControlDefaultValidAppearance(TbC_IsReadyCodeGen, objB.IsPropertyValid("TbC_IsReadyCodeGen"));
				SetControlDefaultValidAppearance(TbC_IsCodeGenComplete, objB.IsPropertyValid("TbC_IsCodeGenComplete"));
				SetControlDefaultValidAppearance(TbC_IsTagForCodeGen, objB.IsPropertyValid("TbC_IsTagForCodeGen"));
				SetControlDefaultValidAppearance(TbC_IsTagForOther, objB.IsPropertyValid("TbC_IsTagForOther"));
				SetControlDefaultValidAppearance(TbC_ColumnGroups, objB.IsPropertyValid("TbC_ColumnGroups"));
				SetControlDefaultValidAppearance(TbC_IsActiveRow, objB.IsPropertyValid("TbC_IsActiveRow"));

                #endregion Fields with validation

                #region Fields with no validation
                //These are listed for reference so can see what doesn’t have validation
                //And also so you can move a line from here to above if it needs validation later on.

				//SetControlDefaultValidAppearance(TbC_Length, true);
				//SetControlDefaultValidAppearance(TbC_Precision, true);
				//SetControlDefaultValidAppearance(TbC_ListStoredProc_Id, true);
				//SetControlDefaultValidAppearance(TbC_ParentColumnKey, true);
				//SetControlDefaultValidAppearance(TbC_ComboListTable_Id, true);
				//SetControlDefaultValidAppearance(TbC_ComboListDisplayColumn_Id, true);


                #endregion Fields with no validation

                SetControlsDefaultValidState_Custom();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlsDefaultValidState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlsDefaultValidState", IfxTraceCategory.Leave);
            }
        }

        #endregion Load this


        #region Load List Controls

		#region TbC_CtlTp_Id XamComboEditor

        /// <summary>
        /// Sets the data source for this list control. Usually a call to the data store is
        /// made and lightweight list of WireType objects is returned for this data source.
        /// </summary>
        void TbC_CtlTp_Id_DataSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_CtlTp_Id_DataSource", IfxTraceCategory.Enter);
                TbC_CtlTp_Id.DisplayMemberPath = "ItemName";
                TbC_CtlTp_Id.ItemsSource = CommonClientData_Bll_staticLists.WcControlType_ComboItemList_BindingListProperty;
                CommonClientData_Bll_staticLists.WcControlType_ComboItemList_BindingListProperty.DataSourceUpdated -= new StaticComboItemListUpdatedEventHandler(WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated);
                CommonClientData_Bll_staticLists.WcControlType_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_CtlTp_Id_DataSource", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_CtlTp_Id_DataSource", IfxTraceCategory.Leave);
            }
        }

        #endregion TbC_CtlTp_Id XamComboEditor

		#region TbC_DtSql_Id XamComboEditor

        /// <summary>
        /// Sets the data source for this list control. Usually a call to the data store is
        /// made and lightweight list of WireType objects is returned for this data source.
        /// </summary>
        void TbC_DtSql_Id_DataSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtSql_Id_DataSource", IfxTraceCategory.Enter);
                TbC_DtSql_Id.DisplayMemberPath = "ItemName";
                TbC_DtSql_Id.ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty;
                CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty.DataSourceUpdated -= new StaticComboItemListUpdatedEventHandler(WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated);
                CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtSql_Id_DataSource", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtSql_Id_DataSource", IfxTraceCategory.Leave);
            }
        }

        #endregion TbC_DtSql_Id XamComboEditor

		#region TbC_DtDtNt_Id XamComboEditor

        /// <summary>
        /// Sets the data source for this list control. Usually a call to the data store is
        /// made and lightweight list of WireType objects is returned for this data source.
        /// </summary>
        void TbC_DtDtNt_Id_DataSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtDtNt_Id_DataSource", IfxTraceCategory.Enter);
                TbC_DtDtNt_Id.DisplayMemberPath = "ItemName";
                TbC_DtDtNt_Id.ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty;
                CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty.DataSourceUpdated -= new StaticComboItemListUpdatedEventHandler(WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated);
                CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtDtNt_Id_DataSource", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_DtDtNt_Id_DataSource", IfxTraceCategory.Leave);
            }
        }

        #endregion TbC_DtDtNt_Id XamComboEditor

		#region TbC_ListStoredProc_Id XamComboEditor

        /// <summary>
        /// Sets the data source for this list control. Usually a call to the data store is
        /// made and lightweight list of WireType objects is returned for this data source.
        /// </summary>
        void TbC_ListStoredProc_Id_DataSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ListStoredProc_Id_DataSource", IfxTraceCategory.Enter);
                TbC_ListStoredProc_Id.DisplayMemberPath = "ItemName";
                TbC_ListStoredProc_Id.ItemsSource = WcApplicationVersion_Bll_staticLists.WcStoredProc_ComboItemList_BindingListProperty;
                WcApplicationVersion_Bll_staticLists.WcStoredProc_ComboItemList_BindingListProperty.DataSourceUpdated -= new StaticComboItemListUpdatedEventHandler(WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated);
                WcApplicationVersion_Bll_staticLists.WcStoredProc_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ListStoredProc_Id_DataSource", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ListStoredProc_Id_DataSource", IfxTraceCategory.Leave);
            }
        }

        #endregion TbC_ListStoredProc_Id XamComboEditor

		#region TbC_ParentColumnKey XamComboEditor

        /// <summary>
        /// Sets the data source for this list control. Usually a call to the data store is
        /// made and lightweight list of WireType objects is returned for this data source.
        /// </summary>
        void TbC_ParentColumnKey_DataSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ParentColumnKey_DataSource", IfxTraceCategory.Enter);
                TbC_ParentColumnKey.DisplayMemberPath = "ItemName";
                TbC_ParentColumnKey.ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;
                WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.DataSourceUpdated -= new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated);
                WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ParentColumnKey_DataSource", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ParentColumnKey_DataSource", IfxTraceCategory.Leave);
            }
        }

        #endregion TbC_ParentColumnKey XamComboEditor

		#region TbC_ComboListTable_Id XamComboEditor

        /// <summary>
        /// Sets the data source for this list control. Usually a call to the data store is
        /// made and lightweight list of WireType objects is returned for this data source.
        /// </summary>
        void TbC_ComboListTable_Id_DataSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListTable_Id_DataSource", IfxTraceCategory.Enter);
                TbC_ComboListTable_Id.DisplayMemberPath = "ItemName";
                TbC_ComboListTable_Id.ItemsSource = WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty;
                WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty.DataSourceUpdated -= new StaticComboItemListUpdatedEventHandler(WcTable_ComboItemList_BindingListProperty_DataSourceUpdated);
                WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTable_ComboItemList_BindingListProperty_DataSourceUpdated);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListTable_Id_DataSource", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListTable_Id_DataSource", IfxTraceCategory.Leave);
            }
        }

        #endregion TbC_ComboListTable_Id XamComboEditor

		#region TbC_ComboListDisplayColumn_Id XamComboEditor

        /// <summary>
        /// Sets the data source for this list control. Usually a call to the data store is
        /// made and lightweight list of WireType objects is returned for this data source.
        /// </summary>
        void TbC_ComboListDisplayColumn_Id_DataSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListDisplayColumn_Id_DataSource", IfxTraceCategory.Enter);
                TbC_ComboListDisplayColumn_Id.DisplayMemberPath = "ItemName";
                TbC_ComboListDisplayColumn_Id.ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;
                WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.DataSourceUpdated -= new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated);
                WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListDisplayColumn_Id_DataSource", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbC_ComboListDisplayColumn_Id_DataSource", IfxTraceCategory.Leave);
            }
        }

        #endregion TbC_ComboListDisplayColumn_Id XamComboEditor



        void WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);
                TbC_ComboListDisplayColumn_Id.vXamComboEditorSelectionChanged -= new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                TbC_ParentColumnKey.vXamComboEditorSelectionChanged -= new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                if (objB != null)
                {
                    Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_ComboListDisplayColumn_Id, objB.TbC_ComboListDisplayColumn_Id);
                    Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_ParentColumnKey, objB.TbC_ParentColumnKey);
                }
                TbC_ComboListDisplayColumn_Id.vXamComboEditorSelectionChanged += new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                TbC_ParentColumnKey.vXamComboEditorSelectionChanged += new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        void WcTable_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);
                TbC_ComboListTable_Id.vXamComboEditorSelectionChanged -= new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                if (objB != null)
                {
                    Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_ComboListTable_Id, objB.TbC_ComboListTable_Id);
                }
                TbC_ComboListTable_Id.vXamComboEditorSelectionChanged += new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        void WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);
                TbC_CtlTp_Id.vXamComboEditorSelectionChanged -= new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                if (objB != null)
                {
                    Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_CtlTp_Id, objB.TbC_CtlTp_Id);
                }
                TbC_CtlTp_Id.vXamComboEditorSelectionChanged += new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        void WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);
                TbC_DtDtNt_Id.vXamComboEditorSelectionChanged -= new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                if (objB != null)
                {
                    Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_DtDtNt_Id, objB.TbC_DtDtNt_Id);
                }
                TbC_DtDtNt_Id.vXamComboEditorSelectionChanged += new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        void WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);
                TbC_DtSql_Id.vXamComboEditorSelectionChanged -= new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                if (objB != null)
                {
                    Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_DtSql_Id, objB.TbC_DtSql_Id);
                }
                TbC_DtSql_Id.vXamComboEditorSelectionChanged += new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        void WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);
                TbC_ListStoredProc_Id.vXamComboEditorSelectionChanged -= new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                if (objB != null)
                {
                    Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_ListStoredProc_Id, objB.TbC_ListStoredProc_Id);
                }
                TbC_ListStoredProc_Id.vXamComboEditorSelectionChanged += new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        #endregion Load List Controls


        #region Events

        #region ProxyWrapperEvents


        #endregion ProxyWrapperEvents

        #region Control Events


        /// <summary>
        /// 	<para>Part of the standard control events in the Control Events region.<br/>
        ///     If the _isActivePropertiesControl variable is false, then this control
        ///     (ucWcTableColumnProps) was not the current ‘Active Properties Control’. However, now that
        ///     this field has the focus means that ucWcTableColumnProps is now the ‘Active Properties
        ///     Control’. Therefore:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<div class="xmldocbulletlist">
        /// 				<see cref="_isActivePropertiesControl">_isActivePropertiesControl</see>
        ///                 will be set to true (so this code is not executed again until
        ///                 ucWcTableColumnProps has returned to Not Active).
        ///             </div>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="RaiseCurrentEntityStateChanged">RaiseCurrentEntityStateChanged</see> will
        ///             be called. Refer to its documentation for information on what it does and
        ///             why it’s called here.
        ///         </item>
        /// 	</list>
        /// </summary>
        void OnDataControlGotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDataControlGotFocus", IfxTraceCategory.Enter);
                if (_isActivePropertiesControl == false)
                {
                    _isActivePropertiesControl = true;
                    RaiseCurrentEntityStateChanged();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDataControlGotFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDataControlGotFocus", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every TextBox so the ‘TextChanged’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="TextBoxTextChanged">TextBoxTextChanged</see> located in the
        ///         ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no TextBox controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                if (sender.GetType().Name != "vTextBox") { return; }
                vTextBox bx = (vTextBox)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTextChanged", new ValuePair[] {new ValuePair("bx.Name", bx.Name) }, IfxTraceCategory.Enter);
                switch (bx.Name)
                {
					case "TbC_SortOrder":
                        objB.TbC_SortOrder_asString = bx.Text;
                        break;

					case "TbC_Name":
                        objB.TbC_Name = bx.Text;
                        break;

					case "TbC_Description":
                        objB.TbC_Description = bx.Text;
                        break;

					case "TbC_Length":
                        objB.TbC_Length_asString = bx.Text;
                        break;

					case "TbC_Precision":
                        objB.TbC_Precision_asString = bx.Text;
                        break;

					case "TbC_Scale":
                        objB.TbC_Scale_asString = bx.Text;
                        break;

					case "TbC_BrokenRuleText":
                        objB.TbC_BrokenRuleText = bx.Text;
                        break;

					case "TbC_DefaultValue":
                        objB.TbC_DefaultValue = bx.Text;
                        break;

					case "TbC_DefaultCaption":
                        objB.TbC_DefaultCaption = bx.Text;
                        break;

					case "TbC_ColumnHeaderText":
                        objB.TbC_ColumnHeaderText = bx.Text;
                        break;

					case "TbC_LabelCaptionVerbose":
                        objB.TbC_LabelCaptionVerbose = bx.Text;
                        break;

					case "TbC_TextBoxFormat":
                        objB.TbC_TextBoxFormat = bx.Text;
                        break;

					case "TbC_TextColumnFormat":
                        objB.TbC_TextColumnFormat = bx.Text;
                        break;

					case "TbC_ColumnWidth":
                        objB.TbC_ColumnWidth_asString = bx.Text;
                        break;

					case "TbC_TextBoxTextAlignment":
                        objB.TbC_TextBoxTextAlignment = bx.Text;
                        break;

					case "TbC_ColumnTextAlignment":
                        objB.TbC_ColumnTextAlignment = bx.Text;
                        break;

					case "TbC_Combo_MaxDropdownHeight":
                        objB.TbC_Combo_MaxDropdownHeight_asString = bx.Text;
                        break;

					case "TbC_Combo_MaxDropdownWidth":
                        objB.TbC_Combo_MaxDropdownWidth_asString = bx.Text;
                        break;

					case "TbC_DeveloperNote":
                        objB.TbC_DeveloperNote = bx.Text;
                        break;

					case "TbC_UserNote":
                        objB.TbC_UserNote = bx.Text;
                        break;

					case "TbC_HelpFileAdditionalNote":
                        objB.TbC_HelpFileAdditionalNote = bx.Text;
                        break;

					case "TbC_Notes":
                        objB.TbC_Notes = bx.Text;
                        break;

					case "TbC_ColumnGroups":
                        objB.TbC_ColumnGroups = bx.Text;
                        break;

				}
                OnTextBoxTextChanged_Custom(bx);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTextChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTextChanged", IfxTraceCategory.Leave);
            }
        }

        void TextControlGotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                Control ctl = (Control)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TextControlGotFocus", new ValuePair[] {new ValuePair("ctl.Name", ctl.Name) }, IfxTraceCategory.Enter);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TextControlGotFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TextControlGotFocus", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every TextBox so the ‘TextChanged’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="TextBoxLostFocus">TextBoxLostFocus</see> located in the
        ///         ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no TextBox controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnTexBoxLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; } 
                FLG_UPDATING_FIELDVALUE = true;
                TextBox bx = sender as TextBox;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTexBoxLostFocus", new ValuePair[] {new ValuePair("bx.Name", bx.Name) }, IfxTraceCategory.Enter);
                if (bx == null) { return; }
                switch (bx.Name)
                {
					case "TbC_SortOrder":
                        bx.Text = objB.TbC_SortOrder_asString;
                        break;

					case "TbC_Name":
                        if (objB.TbC_Name == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_Name;
                        }
                        break;

					case "TbC_Description":
                        if (objB.TbC_Description == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_Description;
                        }
                        break;

					case "TbC_Length":
                        bx.Text = objB.TbC_Length_asString;
                        break;

					case "TbC_Precision":
                        bx.Text = objB.TbC_Precision_asString;
                        break;

					case "TbC_Scale":
                        bx.Text = objB.TbC_Scale_asString;
                        break;

					case "TbC_BrokenRuleText":
                        if (objB.TbC_BrokenRuleText == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_BrokenRuleText;
                        }
                        break;

					case "TbC_DefaultValue":
                        if (objB.TbC_DefaultValue == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_DefaultValue;
                        }
                        break;

					case "TbC_DefaultCaption":
                        if (objB.TbC_DefaultCaption == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_DefaultCaption;
                        }
                        break;

					case "TbC_ColumnHeaderText":
                        if (objB.TbC_ColumnHeaderText == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_ColumnHeaderText;
                        }
                        break;

					case "TbC_LabelCaptionVerbose":
                        if (objB.TbC_LabelCaptionVerbose == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_LabelCaptionVerbose;
                        }
                        break;

					case "TbC_TextBoxFormat":
                        if (objB.TbC_TextBoxFormat == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_TextBoxFormat;
                        }
                        break;

					case "TbC_TextColumnFormat":
                        if (objB.TbC_TextColumnFormat == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_TextColumnFormat;
                        }
                        break;

					case "TbC_ColumnWidth":
                        bx.Text = objB.TbC_ColumnWidth_asString;
                        break;

					case "TbC_TextBoxTextAlignment":
                        if (objB.TbC_TextBoxTextAlignment == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_TextBoxTextAlignment;
                        }
                        break;

					case "TbC_ColumnTextAlignment":
                        if (objB.TbC_ColumnTextAlignment == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_ColumnTextAlignment;
                        }
                        break;

					case "TbC_Combo_MaxDropdownHeight":
                        bx.Text = objB.TbC_Combo_MaxDropdownHeight_asString;
                        break;

					case "TbC_Combo_MaxDropdownWidth":
                        bx.Text = objB.TbC_Combo_MaxDropdownWidth_asString;
                        break;

					case "TbC_DeveloperNote":
                        if (objB.TbC_DeveloperNote == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_DeveloperNote;
                        }
                        break;

					case "TbC_UserNote":
                        if (objB.TbC_UserNote == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_UserNote;
                        }
                        break;

					case "TbC_HelpFileAdditionalNote":
                        if (objB.TbC_HelpFileAdditionalNote == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_HelpFileAdditionalNote;
                        }
                        break;

					case "TbC_Notes":
                        if (objB.TbC_Notes == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_Notes;
                        }
                        break;

					case "TbC_ColumnGroups":
                        if (objB.TbC_ColumnGroups == null)
                        {
                            bx.Text = "";
                        }
                        else
                        {
                            bx.Text = objB.TbC_ColumnGroups;
                        }
                        break;

				}
                OnTextBoxLostFocus_Custom(bx);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTexBoxLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTexBoxLostFocus", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every DatePicker so the ‘SelectionChanged’ event handling
        ///     is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnDatePicker_TextChanged">OnDatePicker_TextChanged(ctl)</see>
        ///         located in the ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no DatePicker controls in
        ///     case some are added at a later date.</para>
        /// </summary>
        void OnDatePicker_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePicker_TextChanged", IfxTraceCategory.Enter);
                if (ctl == null) { return; }
                switch (ctl.Name)
                {
                }
                OnDatePicker_TextChanged_Custom(ctl);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePicker_TextChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePicker_TextChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every DatePicker so the ‘LostFocus’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnDatePickerLostFocus">OnDatePickerLostFocus(ctl)</see>
        ///         located in the ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no DatePicker controls in
        ///     case some are added at a later date.</para>
        /// </summary>
        void OnDatePickerLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                FLG_UPDATING_FIELDVALUE = true;
                DatePicker ctl = (DatePicker)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePickerLostFocus", IfxTraceCategory.Enter);
                switch (ctl.Name)
                {
        
                }
                OnDatePickerLostFocus_Custom(ctl);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePickerLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePickerLostFocus", IfxTraceCategory.Leave);
            }
        }
        
        /// <summary>
        /// 	<para>Is wired up to every TimePicker so the ‘ValueChanging’ event handling
        ///     is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnTimePicker_ValueChanged">OnTimePicker_ValueChanged(ctl)</see>
        ///         located in the ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no TimePicker controls in
        ///     case some are added at a later date.</para>
        /// </summary>
        void OnTimePicker_ValueChanged(object sender, RoutedPropertyChangedEventArgs<DateTime?> e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                TimePicker tp = sender as TimePicker;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePicker_ValueChanged", IfxTraceCategory.Enter);
                if (tp == null) { return; }
                switch (tp.Name)
                {
                }
                OnTimePicker_ValueChanged_Custom(tp);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePicker_ValueChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePicker_ValueChanged", IfxTraceCategory.Leave);
            }
        }
 

        /// <summary>
        /// 	<para>Is wired up to every TimePicker so the ‘LostFocus’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnTimePickerLostFocus">OnTimePickerLostFocus(ctl)</see>
        ///         located in the ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no TimePicker controls in
        ///     case some are added at a later Time.</para>
        /// </summary>        void OnTimePickerLostFocus(object sender, RoutedEventArgs e)
        void OnTimePickerLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                TimePicker tp = sender as TimePicker;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePickerLostFocus", IfxTraceCategory.Enter);
                if (tp == null) { return; }
                switch (tp.Name)
                {
                }
                OnTimePickerLostFocus_Custom(tp);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePickerLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePickerLostFocus", IfxTraceCategory.Leave);
            }
        }

 
        
        /// <summary>
        /// 	<para>Is wired up to every XamComboEditor so the ‘OnXamComboEditorSelectionChanged’
        ///     event handling is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnXamComboSelectedItemChanged_Custom">OnXamComboSelectedItemChanged_Custom(cmb)</see>
        ///         located in the ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no XamComboEditor controls in case
        ///     some are added at a later date.</para>
        /// </summary>
        void OnXamComboEditorSelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            // Used for the Infragistics XamComboEditor.  There may not be any here, but this code is inplace incase we add one later
            try
            {
                if (FLG_FORM_IS_LOADING == true) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                XamComboEditor cmb = (XamComboEditor)sender;
                ComboItemList data = cmb.ItemsSource as ComboItemList;
                if (data != null && data.IsRefreshingData == true) { return; } // The combo's list is being updated to don't do anything.
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboEditorSelectionChanged", new ValuePair[] {new ValuePair("cmb.Name", cmb.Name) }, IfxTraceCategory.Enter);
                switch (cmb.Name)
                {
                    case "xxx":

                        break;
				}
				
                OnXamComboSelectedItemChanged_Custom(cmb);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboEditorSelectionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboEditorSelectionChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every XamComboEditor so the ‘OnVXamComboEditorSelectionChanged’
        ///     event handling is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnVXamComboSelectedItemChanged_Custom">OnVXamComboSelectedItemChanged_Custom(cmb)</see>
        ///         located in the ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no XamComboEditor controls in case
        ///     some are added at a later date.</para>
        /// </summary>
        void OnVXamComboEditorSelectionChanged(object sender, vXamComboEditorSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            // Used for the Infragistics XamComboEditor.  There may not be any here, but this code is inplace incase we add one later
            try
            {
                if (FLG_FORM_IS_LOADING == true) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                if (objB == null) { return; }  // For some reason this event is fired before it's time as there is no underling biz object yet, and we get an error when referencing objB.
                vXamComboEditor cmb = (vXamComboEditor)sender;
                ComboItemList data = cmb.ItemsSource as ComboItemList;
                if (data != null && data.IsRefreshingData == true)
                { 
                    // remove the event
                    cmb.vXamComboEditorSelectionChanged -= new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                    // re-set the selected index which we lost when updateing the ItemsSource
                    cmb.SelectedIndex = e.OldIndex;
                    // add the event back on
                    cmb.vXamComboEditorSelectionChanged += new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged);
                    return;  // The combo's list is being updated to don't do anything.
                } 
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamComboEditorSelectionChanged", new ValuePair[] {new ValuePair("cmb.Name", cmb.Name) }, IfxTraceCategory.Enter);
                switch (cmb.Name)
                {

                    case "TbC_CtlTp_Id":
                        if (TbC_CtlTp_Id.SelectedItem == null)
                        {
                            objB.TbC_CtlTp_Id = null;
                        }
                        else
                        {
                            objB.TbC_CtlTp_Id = (Int32)((ComboItem)TbC_CtlTp_Id.SelectedItem).Id;
                        }
                        break;


                    case "TbC_DtSql_Id":
                        if (TbC_DtSql_Id.SelectedItem == null)
                        {
                            objB.TbC_DtSql_Id = null;
                        }
                        else
                        {
                            objB.TbC_DtSql_Id = (Int32)((ComboItem)TbC_DtSql_Id.SelectedItem).Id;
                        }
                        break;


                    case "TbC_DtDtNt_Id":
                        if (TbC_DtDtNt_Id.SelectedItem == null)
                        {
                            objB.TbC_DtDtNt_Id = null;
                        }
                        else
                        {
                            objB.TbC_DtDtNt_Id = (Int32)((ComboItem)TbC_DtDtNt_Id.SelectedItem).Id;
                        }
                        break;


                    case "TbC_ListStoredProc_Id":
                        if (TbC_ListStoredProc_Id.SelectedItem == null)
                        {
                            objB.TbC_ListStoredProc_Id = null;
                        }
                        else
                        {
                            objB.TbC_ListStoredProc_Id = (Guid)((ComboItem)TbC_ListStoredProc_Id.SelectedItem).Id;
                        }
                        break;


                    case "TbC_ParentColumnKey":
                        if (TbC_ParentColumnKey.SelectedItem == null)
                        {
                            objB.TbC_ParentColumnKey = null;
                        }
                        else
                        {
                            objB.TbC_ParentColumnKey = (Guid)((ComboItem)TbC_ParentColumnKey.SelectedItem).Id;
                        }
                        break;


                    case "TbC_ComboListTable_Id":
                        if (TbC_ComboListTable_Id.SelectedItem == null)
                        {
                            objB.TbC_ComboListTable_Id = null;
                        }
                        else
                        {
                            objB.TbC_ComboListTable_Id = (Guid)((ComboItem)TbC_ComboListTable_Id.SelectedItem).Id;
                        }
                        break;


                    case "TbC_ComboListDisplayColumn_Id":
                        if (TbC_ComboListDisplayColumn_Id.SelectedItem == null)
                        {
                            objB.TbC_ComboListDisplayColumn_Id = null;
                        }
                        else
                        {
                            objB.TbC_ComboListDisplayColumn_Id = (Guid)((ComboItem)TbC_ComboListDisplayColumn_Id.SelectedItem).Id;
                        }
                        break;

				}
                OnVXamComboSelectedItemChanged_Custom(cmb);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamComboEditorSelectionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamComboEditorSelectionChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every XamComboEditor so the ‘OnXamComboEditorLostFocus’ event
        ///     handling is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnXamComboLostFocus_Custom">OnXamComboLostFocus_Custom(cmb)</see> located in the
        ///         ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no XamComboEditor controls in case
        ///     some are added at a later date.<br/></para>
        /// </summary>
        void OnXamComboEditorLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                FLG_UPDATING_FIELDVALUE = true;
                XamComboEditor cmb = (XamComboEditor)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboEditorLostFocus", new ValuePair[] {new ValuePair("cmb.Name", cmb.Name) }, IfxTraceCategory.Enter);
                switch (cmb.Name)
                {
					case "TbC_CtlTp_Id":
                        Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_CtlTp_Id, objB.TbC_CtlTp_Id);
                        break;

					case "TbC_DtSql_Id":
                        Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_DtSql_Id, objB.TbC_DtSql_Id);
                        break;

					case "TbC_DtDtNt_Id":
                        Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_DtDtNt_Id, objB.TbC_DtDtNt_Id);
                        break;

					case "TbC_ListStoredProc_Id":
                        Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_ListStoredProc_Id, objB.TbC_ListStoredProc_Id);
                        break;

					case "TbC_ParentColumnKey":
                        Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_ParentColumnKey, objB.TbC_ParentColumnKey);
                        break;

					case "TbC_ComboListTable_Id":
                        Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_ComboListTable_Id, objB.TbC_ComboListTable_Id);
                        break;

					case "TbC_ComboListDisplayColumn_Id":
                        Set_ComboEditorItem_SelectedItem_ComboItemType(TbC_ComboListDisplayColumn_Id, objB.TbC_ComboListDisplayColumn_Id);
                        break;

				}
                OnXamComboLostFocus_Custom(cmb);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboEditorLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();

            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboEditorLostFocus", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every XamMultiColumnComboEditor so the ‘OnXamMultiColumnComboEditorSelectionChanged’
        ///     event handling is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnXamComboSelectedItemChanged_Custom">OnXamComboSelectedItemChanged_Custom(cmb)</see>
        ///         located in the ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no XamMultiColumnComboEditor controls in case
        ///     some are added at a later date.</para>
        /// </summary>
        void OnXamMultiColumnComboEditorSelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            // Used for the Infragistics XamMultiColumnComboEditor.  There may not be any here, but this code is inplace incase we add one later
            try
            {
                if (FLG_FORM_IS_LOADING == true) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                XamMultiColumnComboEditor cmb = (XamMultiColumnComboEditor)sender;
                ComboItemList data = cmb.ItemsSource as ComboItemList;
                if (data != null && data.IsRefreshingData == true) { return; } // The combo's list is being updated to don't do anything.
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboEditorSelectionChanged", new ValuePair[] {new ValuePair("cmb.Name", cmb.Name) }, IfxTraceCategory.Enter);
                switch (cmb.Name)
                {
                    case "xxx":

                        break;
                }
				
                OnXamMultiColumnComboSelectedItemChanged_Custom(cmb);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboEditorSelectionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboEditorSelectionChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every vXamMultiColumnComboEditor so the ‘OnVXamMultiColumnComboEditorSelectionChanged’
        ///     event handling is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnXamComboSelectedItemChanged_Custom">OnXamComboSelectedItemChanged_Custom(cmb)</see>
        ///         located in the ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your
        ///         custom code should go so it’s not lost when this class is regenerated again.
        ///         Read here for more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no vXamMultiColumnComboEditor controls in case
        ///     some are added at a later date.</para>
        /// </summary>
        void OnVXamMultiColumnComboEditorSelectionChanged(object sender, vXamMultiColumnComboEditorSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            // Used for the Infragistics vXamMultiColumnComboEditor.  There may not be any here, but this code is inplace incase we add one later
            try
            {
                if (FLG_FORM_IS_LOADING == true) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                vXamMultiColumnComboEditor cmb = (vXamMultiColumnComboEditor)sender;
                ComboItemList data = cmb.ItemsSource as ComboItemList;
                if (data != null && data.IsRefreshingData == true)
                {
                    // remove the event
                    cmb.vXamMultiColumnComboEditorSelectionChanged -= new vXamMultiColumnComboEditorSelectionChangedEventHandler(OnVXamMultiColumnComboEditorSelectionChanged);
                    // re-set the selected index which we lost when updateing the ItemsSource
                    cmb.SelectedIndex = e.OldIndex;
                    // add the event back on
                    cmb.vXamMultiColumnComboEditorSelectionChanged += new vXamMultiColumnComboEditorSelectionChangedEventHandler(OnVXamMultiColumnComboEditorSelectionChanged);
                    return;  // The combo's list is being updated to don't do anything.
                } 
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamMultiColumnComboEditorSelectionChanged", new ValuePair[] {new ValuePair("cmb.Name", cmb.Name) }, IfxTraceCategory.Enter);
                switch (cmb.Name)
                {
					case "xxxxxxxxxxxx": 
                        //objB.xxxxxxxxxxxx = xxxxxxxxxxxx_item.xxxxxxxxxxxxColumnName;
                        break;
				}
                OnVXamMultiColumnComboSelectedItemChanged_Custom(cmb);
                FLG_UPDATING_FIELDVALUE = false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamMultiColumnComboEditorSelectionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamMultiColumnComboEditorSelectionChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every XamMultiColumnComboEditor so the ‘OnXamMultiColumnComboEditorLostFocus’ event
        ///     handling is centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnXamComboLostFocus_Custom">OnXamComboLostFocus_Custom(cmb)</see> located in the
        ///         ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no XamMultiColumnComboEditor controls in case
        ///     some are added at a later date.<br/></para>
        /// </summary>
        void OnXamMultiColumnComboEditorLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                FLG_UPDATING_FIELDVALUE = true;
                XamMultiColumnComboEditor cmb = (XamMultiColumnComboEditor)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboEditorLostFocus", new ValuePair[] {new ValuePair("cmb.Name", cmb.Name) }, IfxTraceCategory.Enter);
                switch (cmb.Name)
                {
				}
                OnXamMultiColumnComboLostFocus_Custom(cmb);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboEditorLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();

            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboEditorLostFocus", IfxTraceCategory.Leave);
            }
        }

        private void XamColorPicker_SelectedColorChanged(object sender, SelectedColorChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (FLG_FORM_IS_LOADING == true) { return; }
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                if (sender.GetType().Name != "XamColorPicker") { return; }
                XamColorPicker cp = sender as XamColorPicker;
                if (cp == null) { return; }
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Enter);

                switch (cp.Name)
                {
                }
                XamColorPicker_SelectedColorChanged_custom(cp, e);                
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Leave);
            }
        }

      
        
        /// <summary>
        /// 	<para>Is wired up to every CheckBox so the ‘Click’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="CheckBoxClick">CheckBoxClick(chk)</see> located in the
        ///         ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no CheckBox controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnCheckBoxClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                if (sender.GetType().Name != "vCheckBox") { return; }
                vCheckBox chk = (vCheckBox)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxClick", new ValuePair[] {new ValuePair("chk.Name", chk.Name) }, IfxTraceCategory.Enter);
                switch (chk.Name)
                {
					case "TbC_IsNonvColumn":
                        objB.TbC_IsNonvColumn = (bool)chk.IsChecked;
                        break;

					case "TbC_IsPK":
                        objB.TbC_IsPK = (bool)chk.IsChecked;
                        break;

					case "TbC_IsIdentity":
                        objB.TbC_IsIdentity = (bool)chk.IsChecked;
                        break;

					case "TbC_IsFK":
                        objB.TbC_IsFK = (bool)chk.IsChecked;
                        break;

					case "TbC_IsEntityColumn":
                        objB.TbC_IsEntityColumn = (bool)chk.IsChecked;
                        break;

					case "TbC_IsSystemField":
                        objB.TbC_IsSystemField = (bool)chk.IsChecked;
                        break;

					case "TbC_IsValuesObjectMember":
                        objB.TbC_IsValuesObjectMember = (bool)chk.IsChecked;
                        break;

					case "TbC_IsInPropsScreen":
                        objB.TbC_IsInPropsScreen = (bool)chk.IsChecked;
                        break;

					case "TbC_IsInNavList":
                        objB.TbC_IsInNavList = (bool)chk.IsChecked;
                        break;

					case "TbC_IsRequired":
                        objB.TbC_IsRequired = (bool)chk.IsChecked;
                        break;

					case "TbC_AllowZero":
                        objB.TbC_AllowZero = (bool)chk.IsChecked;
                        break;

					case "TbC_IsNullableInDb":
                        objB.TbC_IsNullableInDb = (bool)chk.IsChecked;
                        break;

					case "TbC_IsNullableInUI":
                        objB.TbC_IsNullableInUI = (bool)chk.IsChecked;
                        break;

					case "TbC_IsReadFromDb":
                        objB.TbC_IsReadFromDb = (bool)chk.IsChecked;
                        break;

					case "TbC_IsSendToDb":
                        objB.TbC_IsSendToDb = (bool)chk.IsChecked;
                        break;

					case "TbC_IsInsertAllowed":
                        objB.TbC_IsInsertAllowed = (bool)chk.IsChecked;
                        break;

					case "TbC_IsEditAllowed":
                        objB.TbC_IsEditAllowed = (bool)chk.IsChecked;
                        break;

					case "TbC_IsReadOnlyInUI":
                        objB.TbC_IsReadOnlyInUI = (bool)chk.IsChecked;
                        break;

					case "TbC_UseForAudit":
                        objB.TbC_UseForAudit = (bool)chk.IsChecked;
                        break;

					case "TbC_LabelCaptionGenerate":
                        objB.TbC_LabelCaptionGenerate = (bool)chk.IsChecked;
                        break;

					case "Tbc_ShowGridColumnToolTip":
                        objB.Tbc_ShowGridColumnToolTip = (bool)chk.IsChecked;
                        break;

					case "Tbc_ShowPropsToolTip":
                        objB.Tbc_ShowPropsToolTip = (bool)chk.IsChecked;
                        break;

					case "TbC_IsCreatePropsStrings":
                        objB.TbC_IsCreatePropsStrings = (bool)chk.IsChecked;
                        break;

					case "TbC_IsCreateGridStrings":
                        objB.TbC_IsCreateGridStrings = (bool)chk.IsChecked;
                        break;

					case "TbC_IsAvailableForColumnGroups":
                        objB.TbC_IsAvailableForColumnGroups = (bool)chk.IsChecked;
                        break;

					case "TbC_IsTextWrapInProp":
                        objB.TbC_IsTextWrapInProp = (bool)chk.IsChecked;
                        break;

					case "TbC_IsTextWrapInGrid":
                        objB.TbC_IsTextWrapInGrid = (bool)chk.IsChecked;
                        break;

					case "TbC_IsStaticList":
                        objB.TbC_IsStaticList = (bool)chk.IsChecked;
                        break;

					case "TbC_UseNotInList":
                        objB.TbC_UseNotInList = (bool)chk.IsChecked;
                        break;

					case "TbC_UseListEditBtn":
                        objB.TbC_UseListEditBtn = (bool)chk.IsChecked;
                        break;

					case "TbC_UseDisplayTextFieldProperty":
                        objB.TbC_UseDisplayTextFieldProperty = (bool)chk.IsChecked;
                        break;

					case "TbC_IsDisplayTextFieldProperty":
                        objB.TbC_IsDisplayTextFieldProperty = (bool)chk.IsChecked;
                        break;

					case "TbC_Combo_AllowDropdownResizing":
                        objB.TbC_Combo_AllowDropdownResizing = (bool)chk.IsChecked;
                        break;

					case "TbC_Combo_IsResetButtonVisible":
                        objB.TbC_Combo_IsResetButtonVisible = (bool)chk.IsChecked;
                        break;

					case "TbC_IsActiveRecColumn":
                        objB.TbC_IsActiveRecColumn = (bool)chk.IsChecked;
                        break;

					case "TbC_IsDeletedColumn":
                        objB.TbC_IsDeletedColumn = (bool)chk.IsChecked;
                        break;

					case "TbC_IsCreatedUserIdColumn":
                        objB.TbC_IsCreatedUserIdColumn = (bool)chk.IsChecked;
                        break;

					case "TbC_IsCreatedDateColumn":
                        objB.TbC_IsCreatedDateColumn = (bool)chk.IsChecked;
                        break;

					case "TbC_IsUserIdColumn":
                        objB.TbC_IsUserIdColumn = (bool)chk.IsChecked;
                        break;

					case "TbC_IsModifiedDateColumn":
                        objB.TbC_IsModifiedDateColumn = (bool)chk.IsChecked;
                        break;

					case "TbC_IsRowVersionStampColumn":
                        objB.TbC_IsRowVersionStampColumn = (bool)chk.IsChecked;
                        break;

					case "TbC_IsBrowsable":
                        objB.TbC_IsBrowsable = (bool)chk.IsChecked;
                        break;

					case "TbC_IsInputComplete":
                        objB.TbC_IsInputComplete = (bool)chk.IsChecked;
                        break;

					case "TbC_IsCodeGen":
                        objB.TbC_IsCodeGen = (bool)chk.IsChecked;
                        break;

					case "TbC_IsReadyCodeGen":
                        objB.TbC_IsReadyCodeGen = (bool)chk.IsChecked;
                        break;

					case "TbC_IsCodeGenComplete":
                        objB.TbC_IsCodeGenComplete = (bool)chk.IsChecked;
                        break;

					case "TbC_IsTagForCodeGen":
                        objB.TbC_IsTagForCodeGen = (bool)chk.IsChecked;
                        break;

					case "TbC_IsTagForOther":
                        objB.TbC_IsTagForOther = (bool)chk.IsChecked;
                        break;

					case "TbC_IsActiveRow":
                        objB.TbC_IsActiveRow = (bool)chk.IsChecked;
                        break;


                }
                OnCheckBoxClick_Custom(chk);
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();

            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxClick", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every CheckBox so the ‘LostFocus’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="CheckBoxLostFocus">CheckBoxLostFocus(chk)</see> located in the
        ///         ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no CheckBox controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnCheckBoxLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                FLG_UPDATING_FIELDVALUE = true;
                if (sender.GetType().Name != "vCheckBox") { return; }
                vCheckBox chk = (vCheckBox)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxLostFocus", IfxTraceCategory.Enter);
                switch (chk.Name)
                {
					case "TbC_IsNonvColumn":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsNonvColumn);
                        break;

					case "TbC_IsPK":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsPK);
                        break;

					case "TbC_IsIdentity":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsIdentity);
                        break;

					case "TbC_IsFK":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsFK);
                        break;

					case "TbC_IsEntityColumn":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsEntityColumn);
                        break;

					case "TbC_IsSystemField":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsSystemField);
                        break;

					case "TbC_IsValuesObjectMember":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsValuesObjectMember);
                        break;

					case "TbC_IsInPropsScreen":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsInPropsScreen);
                        break;

					case "TbC_IsInNavList":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsInNavList);
                        break;

					case "TbC_IsRequired":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsRequired);
                        break;

					case "TbC_AllowZero":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_AllowZero);
                        break;

					case "TbC_IsNullableInDb":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsNullableInDb);
                        break;

					case "TbC_IsNullableInUI":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsNullableInUI);
                        break;

					case "TbC_IsReadFromDb":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsReadFromDb);
                        break;

					case "TbC_IsSendToDb":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsSendToDb);
                        break;

					case "TbC_IsInsertAllowed":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsInsertAllowed);
                        break;

					case "TbC_IsEditAllowed":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsEditAllowed);
                        break;

					case "TbC_IsReadOnlyInUI":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsReadOnlyInUI);
                        break;

					case "TbC_UseForAudit":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_UseForAudit);
                        break;

					case "TbC_LabelCaptionGenerate":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_LabelCaptionGenerate);
                        break;

					case "Tbc_ShowGridColumnToolTip":
                        chk.IsChecked = Convert.ToBoolean(objB.Tbc_ShowGridColumnToolTip);
                        break;

					case "Tbc_ShowPropsToolTip":
                        chk.IsChecked = Convert.ToBoolean(objB.Tbc_ShowPropsToolTip);
                        break;

					case "TbC_IsCreatePropsStrings":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsCreatePropsStrings);
                        break;

					case "TbC_IsCreateGridStrings":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsCreateGridStrings);
                        break;

					case "TbC_IsAvailableForColumnGroups":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsAvailableForColumnGroups);
                        break;

					case "TbC_IsTextWrapInProp":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsTextWrapInProp);
                        break;

					case "TbC_IsTextWrapInGrid":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsTextWrapInGrid);
                        break;

					case "TbC_IsStaticList":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsStaticList);
                        break;

					case "TbC_UseNotInList":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_UseNotInList);
                        break;

					case "TbC_UseListEditBtn":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_UseListEditBtn);
                        break;

					case "TbC_UseDisplayTextFieldProperty":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_UseDisplayTextFieldProperty);
                        break;

					case "TbC_IsDisplayTextFieldProperty":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsDisplayTextFieldProperty);
                        break;

					case "TbC_Combo_AllowDropdownResizing":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_Combo_AllowDropdownResizing);
                        break;

					case "TbC_Combo_IsResetButtonVisible":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_Combo_IsResetButtonVisible);
                        break;

					case "TbC_IsActiveRecColumn":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsActiveRecColumn);
                        break;

					case "TbC_IsDeletedColumn":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsDeletedColumn);
                        break;

					case "TbC_IsCreatedUserIdColumn":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsCreatedUserIdColumn);
                        break;

					case "TbC_IsCreatedDateColumn":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsCreatedDateColumn);
                        break;

					case "TbC_IsUserIdColumn":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsUserIdColumn);
                        break;

					case "TbC_IsModifiedDateColumn":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsModifiedDateColumn);
                        break;

					case "TbC_IsRowVersionStampColumn":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsRowVersionStampColumn);
                        break;

					case "TbC_IsBrowsable":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsBrowsable);
                        break;

					case "TbC_IsInputComplete":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsInputComplete);
                        break;

					case "TbC_IsCodeGen":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsCodeGen);
                        break;

					case "TbC_IsReadyCodeGen":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsReadyCodeGen);
                        break;

					case "TbC_IsCodeGenComplete":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsCodeGenComplete);
                        break;

					case "TbC_IsTagForCodeGen":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsTagForCodeGen);
                        break;

					case "TbC_IsTagForOther":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsTagForOther);
                        break;

					case "TbC_IsActiveRow":
                        chk.IsChecked = Convert.ToBoolean(objB.TbC_IsActiveRow);
                        break;


                }
                OnCheckBoxLostFocus_Custom(chk);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxLostFocus", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every RadioButton so the ‘Click’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="RadioButtonClick">RadioButtonClick(rdb)</see> located in the
        ///         ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no RadioButton controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnRadioButtonClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                if (sender.GetType().Name != "RadioButton") { return; }
                RadioButton rdb = (RadioButton)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonClick", new ValuePair[] {new ValuePair("rdb.Name", rdb.Name) }, IfxTraceCategory.Enter);
                switch (rdb.Name)
                {

                }
                OnRadioButtonClick_Custom(rdb);
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonClick", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every RadioButton so the ‘LostFocus’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="OnRadioButtonLostFocus_Custom">OnRadioButtonLostFocus_Custom(rdb)</see> located in the
        ///         ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no RadioButton controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnRadioButtonLostFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (objB == null) { return; }
                FLG_UPDATING_FIELDVALUE = true;
                RadioButton rdb = (RadioButton)sender;
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonLostFocus", IfxTraceCategory.Enter);
                if (sender.GetType().Name != "RadioButton") { return; }
                switch (rdb.Name)
                {

                }
                OnRadioButtonLostFocus_Custom(rdb);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonLostFocus", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonLostFocus", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>Is wired up to every vRadioButtonGroup so the ‘Checked’ event handling is
        ///     centralized.</para>
        /// 	<para>
        ///         Since this class is ‘Code-Genned’, the last line of code in this method calls
        ///         <see cref="vRadioButtonGroupClick">RadioButtonItemChecked(rbg)</see> located in the
        ///         ucWcTableColumnProps.xaml.cust.cs partial class file. This is where your custom code
        ///         should go so it’s not lost when this class is regenerated again. Read here for
        ///         more information about Code-Generation and persisting code.
        ///     </para>
        /// 	<para>This method is created even when there are no vRadioButtonGroup controls in case some
        ///     are added at a later date.</para>
        /// </summary>
        void OnvRadioButtonGroupItemChecked(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (FLG_UPDATING_FIELDVALUE == true) { return; }
                if (FLG_LOADING_REC == true) { return; }
                if (sender.GetType().Name != "RadioButton") { return; }
                vRadioButtonGroup rbg = sender as vRadioButtonGroup;
                if (rbg == null) { return; }
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnvRadioButtonGroupItemChecked", new ValuePair[] {new ValuePair("rbg.Name", rbg.Name) }, IfxTraceCategory.Enter);
                switch (rbg.Name)
                {

                }
                OnvRadioButtonGroupItemChecked_Custom(rbg);
                FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnvRadioButtonGroupItemChecked", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                //FLG_UPDATING_FIELDVALUE = false;
                //e.Handled = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnvRadioButtonGroupItemChecked", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Call this method to disable all read only controls. This is a stub to be used by
        /// the security layer.
        /// </summary>
        void DisableReadonlycontrols()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DisableReadonlycontrols", IfxTraceCategory.Enter);
				TbC_SortOrder.IsEnabled = false;
				TbC_Name.IsEnabled = false;
				TbC_CtlTp_Id.IsEnabled = false;
				TbC_IsNonvColumn.IsEnabled = false;
				TbC_Description.IsEnabled = false;
				TbC_DtSql_Id.IsEnabled = false;
				TbC_DtDtNt_Id.IsEnabled = false;
				TbC_Length.IsEnabled = false;
				TbC_Precision.IsEnabled = false;
				TbC_Scale.IsEnabled = false;
				TbC_IsPK.IsEnabled = false;
				TbC_IsIdentity.IsEnabled = false;
				TbC_IsFK.IsEnabled = false;
				TbC_IsEntityColumn.IsEnabled = false;
				TbC_IsSystemField.IsEnabled = false;
				TbC_IsValuesObjectMember.IsEnabled = false;
				TbC_IsInPropsScreen.IsEnabled = false;
				TbC_IsInNavList.IsEnabled = false;
				TbC_IsRequired.IsEnabled = false;
				TbC_BrokenRuleText.IsEnabled = false;
				TbC_AllowZero.IsEnabled = false;
				TbC_IsNullableInDb.IsEnabled = false;
				TbC_IsNullableInUI.IsEnabled = false;
				TbC_DefaultValue.IsEnabled = false;
				TbC_IsReadFromDb.IsEnabled = false;
				TbC_IsSendToDb.IsEnabled = false;
				TbC_IsInsertAllowed.IsEnabled = false;
				TbC_IsEditAllowed.IsEnabled = false;
				TbC_IsReadOnlyInUI.IsEnabled = false;
				TbC_UseForAudit.IsEnabled = false;
				TbC_DefaultCaption.IsEnabled = false;
				TbC_ColumnHeaderText.IsEnabled = false;
				TbC_LabelCaptionVerbose.IsEnabled = false;
				TbC_LabelCaptionGenerate.IsEnabled = false;
				Tbc_ShowGridColumnToolTip.IsEnabled = false;
				Tbc_ShowPropsToolTip.IsEnabled = false;
				TbC_IsCreatePropsStrings.IsEnabled = false;
				TbC_IsCreateGridStrings.IsEnabled = false;
				TbC_IsAvailableForColumnGroups.IsEnabled = false;
				TbC_IsTextWrapInProp.IsEnabled = false;
				TbC_IsTextWrapInGrid.IsEnabled = false;
				TbC_TextBoxFormat.IsEnabled = false;
				TbC_TextColumnFormat.IsEnabled = false;
				TbC_ColumnWidth.IsEnabled = false;
				TbC_TextBoxTextAlignment.IsEnabled = false;
				TbC_ColumnTextAlignment.IsEnabled = false;
				TbC_ListStoredProc_Id.IsEnabled = false;
				TbC_IsStaticList.IsEnabled = false;
				TbC_UseNotInList.IsEnabled = false;
				TbC_UseListEditBtn.IsEnabled = false;
				TbC_ParentColumnKey.IsEnabled = false;
				TbC_UseDisplayTextFieldProperty.IsEnabled = false;
				TbC_IsDisplayTextFieldProperty.IsEnabled = false;
				TbC_ComboListTable_Id.IsEnabled = false;
				TbC_ComboListDisplayColumn_Id.IsEnabled = false;
				TbC_Combo_MaxDropdownHeight.IsEnabled = false;
				TbC_Combo_MaxDropdownWidth.IsEnabled = false;
				TbC_Combo_AllowDropdownResizing.IsEnabled = false;
				TbC_Combo_IsResetButtonVisible.IsEnabled = false;
				TbC_IsActiveRecColumn.IsEnabled = false;
				TbC_IsDeletedColumn.IsEnabled = false;
				TbC_IsCreatedUserIdColumn.IsEnabled = false;
				TbC_IsCreatedDateColumn.IsEnabled = false;
				TbC_IsUserIdColumn.IsEnabled = false;
				TbC_IsModifiedDateColumn.IsEnabled = false;
				TbC_IsRowVersionStampColumn.IsEnabled = false;
				TbC_IsBrowsable.IsEnabled = false;
				TbC_DeveloperNote.IsEnabled = false;
				TbC_UserNote.IsEnabled = false;
				TbC_HelpFileAdditionalNote.IsEnabled = false;
				TbC_Notes.IsEnabled = false;
				TbC_IsInputComplete.IsEnabled = false;
				TbC_IsCodeGen.IsEnabled = false;
				TbC_IsReadyCodeGen.IsEnabled = false;
				TbC_IsCodeGenComplete.IsEnabled = false;
				TbC_IsTagForCodeGen.IsEnabled = false;
				TbC_IsTagForOther.IsEnabled = false;
				TbC_ColumnGroups.IsEnabled = false;
				TbC_IsActiveRow.IsEnabled = false;

                DisableReadonlycontrols_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DisableReadonlycontrols", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DisableReadonlycontrols", IfxTraceCategory.Leave);
            }
        }

        #endregion Control Events


        #region Form Events  (Mostly State Related)

        /// <summary>
        ///     This method has no other option than to pass in ‘this’ control’s <see cref="CurrentBusinessObject">CurrentBusinessObject</see> as the input parameter for the
        ///     <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event.
        /// </summary>
        /// <overloads>
        /// 	<para>These overloads raise the CurrentEntityStateChanged event passing in
        ///     the:</para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>ActiveEntityControl</item>
        /// 		<item>ActivePropertiesControl</item>
        /// 		<item>CurrentBusinessObject</item>
        /// 	</list>
        /// 	<para>and bubbles up to the top level control. This notifies all controls along the
        ///     about which controls are active and the current state so they can always be
        ///     configures accordingly. Now that the top level control (perhaps the main
        ///     application window) has a reference to these 3 important objects, it can easily
        ///     communicate with then as the use interacts with the application.</para>
        /// 	<para></para>
        /// </overloads>
        public void RaiseCurrentEntityStateChanged()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseCurrentEntityStateChanged", IfxTraceCategory.Enter);
                RaiseCurrentEntityStateChanged(CurrentBusinessObject.StateSwitch);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseCurrentEntityStateChanged", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseCurrentEntityStateChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// This method passes null for the activeEntityControl parameter. This way, when the
        /// event bubbles up to the parent (an <a href="D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\The_Entity_Manager.html">Entity Manager</a> control), when the parent sees this
        /// parameter is null, it will pass in a reference to itself before raising the event up to
        /// the next parent.
        /// </summary>
        /// <param name="state">
        /// As this event bubbles up through the various <a href="D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\The_Entity_Manager.html">Entity Managers</a>, they can read this
        /// state and configure themselves accordingly. When this event reaches the top level
        /// control, the application, including all menus, will configure itself according to the
        /// current state.
        /// </param>
        public void RaiseCurrentEntityStateChanged(EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseCurrentEntityStateChanged(EntityStateSwitch state)", IfxTraceCategory.Enter);
                OnCurrentEntityStateChanged(this, new CurrentEntityStateArgs(state, null, this, CurrentBusinessObject));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseCurrentEntityStateChanged(EntityStateSwitch state)", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseCurrentEntityStateChanged(EntityStateSwitch state)", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     This event is usually initiated by the by the business object when the <see cref="OnCurrentEntityStateChanged">StateSwitch</see> changes, or when some controls get
        ///     the focus. This event will bubble up through the various <a href="D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\The_Entity_Manager.html">Entity Managers</a> so they
        ///     can read this <see cref="TypeServices.EntityState">state</see> and configure
        ///     themselves accordingly. When this event reaches the top level control, the
        ///     application, including all menus, will configure itself according to the current
        ///     state. The Args are very important as they contain a reference to the Active Entity
        ///     Manager and Active Properties control. See <see cref="TypeServices.CurrentEntityStateArgs">CurrentEntityStateArgs</see> to learn more
        ///     about how they help manage the overall application state.
        /// </summary>
        /// <param name="e">See <see cref="TypeServices.CurrentEntityStateArgs">CurrentEntityStateArgs</see></param>
        void OnCurrentEntityStateChanged(object sender, CurrentEntityStateArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Enter);
                ConfigureToCurrentEntityState(e.State);
                CurrentEntityStateEventHandler handler = CurrentEntityStateChanged;
                CurrentEntityStateArgs args;
                if (e.ActivePropertiesControl == null)
                {
                    args = new CurrentEntityStateArgs(e.State, e.ActiveEntityControl, this, e.ActiveBusinessObject);
                }
                else
                {
                    args = e;
                }
                if (handler != null)
                {
                    handler(this, args);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>This method is mostly obsolete, but remains because it’s an easy way to be
        ///     notified of the current broken rule.</para>
        /// 	<para>
        ///         This method is hooked to the <see cref="BrokenRuleChanged">BrokenRuleChanged</see> event which is initiated by
        ///         BrokenRuleManager and bubbles up to the business object and then to this
        ///         control (ucWcTableColumnProps) (or in some cases a ucEntityList control when it’s in
        ///         Read/Write mode).
        ///     </para>
        /// 	<para><br/>
        ///     For information on how Broken Rules are currently being managed, see (in the order
        ///     of execution):</para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>In the business object’s data property – FieldName_Validate();</item>
        /// 		<item>
        ///             In the FieldName_Validate method this line executes:<br/>
        ///             _brokenRuleManager.<see cref="TypeServices.BrokenRuleManager.AddBrokenRuleForProperty">AddBrokenRuleForProperty</see>("FieldName",
        ///             BROKENRULE_ FieldName _SomeRuleName);
        ///         </item>
        /// 		<item>
        /// 			<see cref="TypeServices.BrokenRuleManager.SetEntityValidState">BrokenRuleManager.SetEntityValidState()</see>
        /// 		</item>
        /// 	</list>
        /// 	<para><br/>
        ///     And for the Broken Rule Tooltips, see:</para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>
        /// 			<see cref="tt_Loaded">tt_Loaded</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="LoadToolTipControl">LoadToolTipControl(string fieldName)</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="GetListOfValidationRuleMessagesFromBusinessObject">GetListOfValidationRuleMessagesFromBusinessObject(string
        ///             fieldName)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void OnBrokenRuleChanged(object sender, BrokenRuleArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Enter);
                SetBrokenRuleText(e.Rule);
                BrokenRuleEventHandler handler = BrokenRuleChanged;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     This method is hooked to the business object’s <see cref="EntityBll.WcTableColumn_Bll.ControlValidStateChanged">ControlValidStateChanged</see> event
        ///     which is raised by the <see cref="EntityBll.WcTableColumn_Bll.RaiseEventControlValidStateChanged">RaiseEventControlValidStateChanged</see>
        ///     method which is called by called by the business object’s FieldName_Validate
        ///     method.
        /// </summary>
        private void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", IfxTraceCategory.Enter);
                switch (e.PropertyName)
                {
					case "TbC_SortOrder":
                        SetControlValidAppearance(TbC_SortOrder, e.IsValid, e.IsDirty);
                        break;
					case "TbC_Name":
                        SetControlValidAppearance(TbC_Name, e.IsValid, e.IsDirty);
                        break;
					case "TbC_CtlTp_Id":
                        SetControlValidAppearance(TbC_CtlTp_Id, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsNonvColumn":
                        SetControlValidAppearance(TbC_IsNonvColumn, e.IsValid, e.IsDirty);
                        break;
					case "TbC_Description":
                        SetControlValidAppearance(TbC_Description, e.IsValid, e.IsDirty);
                        break;
					case "TbC_DtSql_Id":
                        SetControlValidAppearance(TbC_DtSql_Id, e.IsValid, e.IsDirty);
                        break;
					case "TbC_DtDtNt_Id":
                        SetControlValidAppearance(TbC_DtDtNt_Id, e.IsValid, e.IsDirty);
                        break;
					case "TbC_Scale":
                        SetControlValidAppearance(TbC_Scale, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsPK":
                        SetControlValidAppearance(TbC_IsPK, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsIdentity":
                        SetControlValidAppearance(TbC_IsIdentity, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsFK":
                        SetControlValidAppearance(TbC_IsFK, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsEntityColumn":
                        SetControlValidAppearance(TbC_IsEntityColumn, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsSystemField":
                        SetControlValidAppearance(TbC_IsSystemField, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsValuesObjectMember":
                        SetControlValidAppearance(TbC_IsValuesObjectMember, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsInPropsScreen":
                        SetControlValidAppearance(TbC_IsInPropsScreen, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsInNavList":
                        SetControlValidAppearance(TbC_IsInNavList, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsRequired":
                        SetControlValidAppearance(TbC_IsRequired, e.IsValid, e.IsDirty);
                        break;
					case "TbC_BrokenRuleText":
                        SetControlValidAppearance(TbC_BrokenRuleText, e.IsValid, e.IsDirty);
                        break;
					case "TbC_AllowZero":
                        SetControlValidAppearance(TbC_AllowZero, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsNullableInDb":
                        SetControlValidAppearance(TbC_IsNullableInDb, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsNullableInUI":
                        SetControlValidAppearance(TbC_IsNullableInUI, e.IsValid, e.IsDirty);
                        break;
					case "TbC_DefaultValue":
                        SetControlValidAppearance(TbC_DefaultValue, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsReadFromDb":
                        SetControlValidAppearance(TbC_IsReadFromDb, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsSendToDb":
                        SetControlValidAppearance(TbC_IsSendToDb, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsInsertAllowed":
                        SetControlValidAppearance(TbC_IsInsertAllowed, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsEditAllowed":
                        SetControlValidAppearance(TbC_IsEditAllowed, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsReadOnlyInUI":
                        SetControlValidAppearance(TbC_IsReadOnlyInUI, e.IsValid, e.IsDirty);
                        break;
					case "TbC_UseForAudit":
                        SetControlValidAppearance(TbC_UseForAudit, e.IsValid, e.IsDirty);
                        break;
					case "TbC_DefaultCaption":
                        SetControlValidAppearance(TbC_DefaultCaption, e.IsValid, e.IsDirty);
                        break;
					case "TbC_ColumnHeaderText":
                        SetControlValidAppearance(TbC_ColumnHeaderText, e.IsValid, e.IsDirty);
                        break;
					case "TbC_LabelCaptionVerbose":
                        SetControlValidAppearance(TbC_LabelCaptionVerbose, e.IsValid, e.IsDirty);
                        break;
					case "TbC_LabelCaptionGenerate":
                        SetControlValidAppearance(TbC_LabelCaptionGenerate, e.IsValid, e.IsDirty);
                        break;
					case "Tbc_ShowGridColumnToolTip":
                        SetControlValidAppearance(Tbc_ShowGridColumnToolTip, e.IsValid, e.IsDirty);
                        break;
					case "Tbc_ShowPropsToolTip":
                        SetControlValidAppearance(Tbc_ShowPropsToolTip, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsCreatePropsStrings":
                        SetControlValidAppearance(TbC_IsCreatePropsStrings, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsCreateGridStrings":
                        SetControlValidAppearance(TbC_IsCreateGridStrings, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsAvailableForColumnGroups":
                        SetControlValidAppearance(TbC_IsAvailableForColumnGroups, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsTextWrapInProp":
                        SetControlValidAppearance(TbC_IsTextWrapInProp, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsTextWrapInGrid":
                        SetControlValidAppearance(TbC_IsTextWrapInGrid, e.IsValid, e.IsDirty);
                        break;
					case "TbC_TextBoxFormat":
                        SetControlValidAppearance(TbC_TextBoxFormat, e.IsValid, e.IsDirty);
                        break;
					case "TbC_TextColumnFormat":
                        SetControlValidAppearance(TbC_TextColumnFormat, e.IsValid, e.IsDirty);
                        break;
					case "TbC_ColumnWidth":
                        SetControlValidAppearance(TbC_ColumnWidth, e.IsValid, e.IsDirty);
                        break;
					case "TbC_TextBoxTextAlignment":
                        SetControlValidAppearance(TbC_TextBoxTextAlignment, e.IsValid, e.IsDirty);
                        break;
					case "TbC_ColumnTextAlignment":
                        SetControlValidAppearance(TbC_ColumnTextAlignment, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsStaticList":
                        SetControlValidAppearance(TbC_IsStaticList, e.IsValid, e.IsDirty);
                        break;
					case "TbC_UseNotInList":
                        SetControlValidAppearance(TbC_UseNotInList, e.IsValid, e.IsDirty);
                        break;
					case "TbC_UseListEditBtn":
                        SetControlValidAppearance(TbC_UseListEditBtn, e.IsValid, e.IsDirty);
                        break;
					case "TbC_UseDisplayTextFieldProperty":
                        SetControlValidAppearance(TbC_UseDisplayTextFieldProperty, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsDisplayTextFieldProperty":
                        SetControlValidAppearance(TbC_IsDisplayTextFieldProperty, e.IsValid, e.IsDirty);
                        break;
					case "TbC_Combo_MaxDropdownHeight":
                        SetControlValidAppearance(TbC_Combo_MaxDropdownHeight, e.IsValid, e.IsDirty);
                        break;
					case "TbC_Combo_MaxDropdownWidth":
                        SetControlValidAppearance(TbC_Combo_MaxDropdownWidth, e.IsValid, e.IsDirty);
                        break;
					case "TbC_Combo_AllowDropdownResizing":
                        SetControlValidAppearance(TbC_Combo_AllowDropdownResizing, e.IsValid, e.IsDirty);
                        break;
					case "TbC_Combo_IsResetButtonVisible":
                        SetControlValidAppearance(TbC_Combo_IsResetButtonVisible, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsActiveRecColumn":
                        SetControlValidAppearance(TbC_IsActiveRecColumn, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsDeletedColumn":
                        SetControlValidAppearance(TbC_IsDeletedColumn, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsCreatedUserIdColumn":
                        SetControlValidAppearance(TbC_IsCreatedUserIdColumn, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsCreatedDateColumn":
                        SetControlValidAppearance(TbC_IsCreatedDateColumn, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsUserIdColumn":
                        SetControlValidAppearance(TbC_IsUserIdColumn, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsModifiedDateColumn":
                        SetControlValidAppearance(TbC_IsModifiedDateColumn, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsRowVersionStampColumn":
                        SetControlValidAppearance(TbC_IsRowVersionStampColumn, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsBrowsable":
                        SetControlValidAppearance(TbC_IsBrowsable, e.IsValid, e.IsDirty);
                        break;
					case "TbC_DeveloperNote":
                        SetControlValidAppearance(TbC_DeveloperNote, e.IsValid, e.IsDirty);
                        break;
					case "TbC_UserNote":
                        SetControlValidAppearance(TbC_UserNote, e.IsValid, e.IsDirty);
                        break;
					case "TbC_HelpFileAdditionalNote":
                        SetControlValidAppearance(TbC_HelpFileAdditionalNote, e.IsValid, e.IsDirty);
                        break;
					case "TbC_Notes":
                        SetControlValidAppearance(TbC_Notes, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsInputComplete":
                        SetControlValidAppearance(TbC_IsInputComplete, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsCodeGen":
                        SetControlValidAppearance(TbC_IsCodeGen, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsReadyCodeGen":
                        SetControlValidAppearance(TbC_IsReadyCodeGen, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsCodeGenComplete":
                        SetControlValidAppearance(TbC_IsCodeGenComplete, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsTagForCodeGen":
                        SetControlValidAppearance(TbC_IsTagForCodeGen, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsTagForOther":
                        SetControlValidAppearance(TbC_IsTagForOther, e.IsValid, e.IsDirty);
                        break;
					case "TbC_ColumnGroups":
                        SetControlValidAppearance(TbC_ColumnGroups, e.IsValid, e.IsDirty);
                        break;
					case "TbC_IsActiveRow":
                        SetControlValidAppearance(TbC_IsActiveRow, e.IsValid, e.IsDirty);
                        break;
				}
                OnControlValidStateChanged_Custom(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     This method is hooked to the business object’s <see cref="EntityBll.WellSLTest_Bll.CrudFailed">CrudFailed</see> event and will continue to bubble
        ///     up to the top level control. The CrudFailed event is raised when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnCrudFailed(object sender, CrudFailedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Enter);
                CrudFailedEventHandler handler = CrudFailed;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Leave);
            }
        }
   
        #endregion Form Events  (Mostly State Related)

        #region Button Events

//        void OnbtnCloseClick(object sender, RoutedEventArgs e)
//        {
//            CloseMe();
//        }
//
//        void OnbtnNewClick(object sender, RoutedEventArgs e)
//        {
//            NewEntityRow();
//        }
//
//        void OnbtnOpenClick(object sender, RoutedEventArgs e)
//        {
//            //Open();
//        }
//
//        void OnbtnSaveClick(object sender, RoutedEventArgs e)
//        {
//            Save();
//        }
//
//        void OnbtnUnDoClick(object sender, RoutedEventArgs e)
//        {
//            UnDo();
//        }

        #endregion Button Events

        #endregion Events


        #region General Methods and Properties


        #region General Methods


        #region Data Related

        /// <summary>
        ///     Obsolete. This was used to load the initial data. Call <see cref="GetEntityRow">GetEntityRow</see> instead.
        /// </summary>
        void LoadData(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadData", IfxTraceCategory.Enter);
                if (Id == null)
                {
                    NewEntityRow();
                }
                else
                {
                    GetEntityRow(Id);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadData", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadData", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Can be called by a parent object to pass in the parent’s Id which will be based
        /// to he business object’s StandingFK property. For information on the StandingFK
        /// property, look up the documentation on a business object which is in a child
        /// relationship to another entity.
        /// </summary>
        public void SetParent(long Id)
        {
            throw new NotImplementedException();
        }

        public void SetParent(int Id)
        {
            throw new NotImplementedException();
        }

        public void SetParent(short Id)
        {
            throw new NotImplementedException();
        }

        public void SetParent(byte Id)
        {
            throw new NotImplementedException();
        }

        public void SetParent(Guid Id)
        {
            throw new NotImplementedException();
        }




        /// <summary>
        ///     Acts as a surrogate for <see cref="SetBusinessObject">SetBusinessObject</see> by
        ///     receiving the business object as <see cref="TypeServices.IBusinessObject">IBusinessObject</see> and then passed it to
        ///     SetBusinessObject. This allows the SetBusinessObject functionality to be available
        ///     in the <see cref="TypeServices.IEntitiyPropertiesControl">IEntitiyPropertiesControl</see> interface
        ///     making this very extendable.
        /// </summary>
        public void SetIBusinessObject(IBusinessObject obj)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetIBusinessObject(IBusinessObject obj)", IfxTraceCategory.Enter);
                SetBusinessObject((WcTableColumn_Bll)obj);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetIBusinessObject(IBusinessObject obj)", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetIBusinessObject(IBusinessObject obj)", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Typically receives a business object (<see cref="EntityBll.WcTableColumn_Bll">WcTableColumn_Bll</see>) from the parent control (typically
        ///     <see cref="ucWcTableColumn">ucWcTableColumn</see>) and performs all of the loading functions to
        ///     make it the current business object; and refreshes all the data fields with its
        ///     data and sets the correct <see cref="TypeServices.EntityState">EntityState</see>.
        /// </summary>
        public void SetBusinessObject(WcTableColumn_Bll obj)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetIBusinessObject", IfxTraceCategory.Enter);
                EntityStateSwitch state;
                if (obj == null)
                {
                    //SetControlsDefaultValidState()  should we call NewEntityRow first?
                    RemoveBusnessObjectEvents();
                    ClearDataFromUI();
                    state = EntityStateSwitch.None;
                    objB = null;
                    _hasBusinessObject = false;
                }
                else
                {
                    if (objB != null)
                    {
                        RemoveBusnessObjectEvents();
                        objB = null;
                    }
                    objB = obj;
                    _hasBusinessObject = true;
                    AddBusinessObjectEvents();
                    SetState();
                    SetControlsDefaultValidState();
                    state = objB.StateSwitch;
                }
                SetEnabledState(objB != null);
                RaiseCurrentEntityStateChanged(state);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetBusinessObject", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetBusinessObject", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Acts as a surrogate for <see cref="GetBusinessObject">GetBusinessObject</see> by
        ///     returning the current business object (<see cref="EntityBll.WcTableColumn_Bll">WcTableColumn_Bll</see>) as <see cref="TypeServices.IBusinessObject">IBusinessObject</see>. This method is part of the
        ///     <see cref="TypeServices.IEntitiyPropertiesControl">IEntitiyPropertiesControl</see>
        ///     interface making this control very extendable.
        /// </summary>
        public IBusinessObject GetIBusinessObject()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIBusinessObject", IfxTraceCategory.Enter);
                return (IBusinessObject)objB;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIBusinessObject", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIBusinessObject", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Returns the current business object (<see cref="EntityBll.WcTableColumn_Bll">WcTableColumn_Bll</see>).
        /// </summary>
        public WcTableColumn_Bll GetBusinessObject()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBusinessObject", IfxTraceCategory.Enter);
                return objB;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBusinessObject", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBusinessObject", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Clears all data fields. Is called from the <see cref="SetBusinessObject">SetBusinessObject</see> method when a null business object
        ///     (<see cref="EntityBll.WcTableColumn_Bll">WcTableColumn_Bll</see>) is passed in and therefore making
        ///     the WcTableColumn_Bll default values not available.
        /// </summary>
        private void ClearDataFromUI()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearDataFromUI", IfxTraceCategory.Enter);
                FLG_LOADING_REC = true;
                // Remove all TextBox TextChanged Events so they don't fire when we set thier new values.  The TextChanged Event is unpredictable in SL so we need to remove it here.
                DataControlDataChangedEvents_RemoveAll();
				TbC_SortOrder.Text ="";
				TbC_Name.Text ="";
				TbC_CtlTp_Id.SelectedIndex =-1;
				TbC_IsNonvColumn.IsChecked =false;
				TbC_Description.Text ="";
				TbC_DtSql_Id.SelectedIndex =-1;
				TbC_DtDtNt_Id.SelectedIndex =-1;
				TbC_Length.Text ="";
				TbC_Precision.Text ="";
				TbC_Scale.Text ="";
				TbC_IsPK.IsChecked =false;
				TbC_IsIdentity.IsChecked =false;
				TbC_IsFK.IsChecked =false;
				TbC_IsEntityColumn.IsChecked =true;
				TbC_IsSystemField.IsChecked =true;
				TbC_IsValuesObjectMember.IsChecked =true;
				TbC_IsInPropsScreen.IsChecked =true;
				TbC_IsInNavList.IsChecked =true;
				TbC_IsRequired.IsChecked =false;
				TbC_BrokenRuleText.Text ="";
				TbC_AllowZero.IsChecked =false;
				TbC_IsNullableInDb.IsChecked =false;
				TbC_IsNullableInUI.IsChecked =false;
				TbC_DefaultValue.Text ="";
				TbC_IsReadFromDb.IsChecked =true;
				TbC_IsSendToDb.IsChecked =true;
				TbC_IsInsertAllowed.IsChecked =true;
				TbC_IsEditAllowed.IsChecked =true;
				TbC_IsReadOnlyInUI.IsChecked =false;
				TbC_UseForAudit.IsChecked =false;
				TbC_DefaultCaption.Text ="";
				TbC_ColumnHeaderText.Text ="";
				TbC_LabelCaptionVerbose.Text ="";
				TbC_LabelCaptionGenerate.IsChecked =true;
				Tbc_ShowGridColumnToolTip.IsChecked =false;
				Tbc_ShowPropsToolTip.IsChecked =false;
				TbC_IsCreatePropsStrings.IsChecked =true;
				TbC_IsCreateGridStrings.IsChecked =true;
				TbC_IsAvailableForColumnGroups.IsChecked =true;
				TbC_IsTextWrapInProp.IsChecked =false;
				TbC_IsTextWrapInGrid.IsChecked =false;
				TbC_TextBoxFormat.Text ="";
				TbC_TextColumnFormat.Text ="";
				TbC_ColumnWidth.Text ="";
				TbC_TextBoxTextAlignment.Text ="";
				TbC_ColumnTextAlignment.Text ="";
				TbC_ListStoredProc_Id.SelectedIndex =-1;
				TbC_IsStaticList.IsChecked =false;
				TbC_UseNotInList.IsChecked =false;
				TbC_UseListEditBtn.IsChecked =false;
				TbC_ParentColumnKey.SelectedIndex =-1;
				TbC_UseDisplayTextFieldProperty.IsChecked =false;
				TbC_IsDisplayTextFieldProperty.IsChecked =false;
				TbC_ComboListTable_Id.SelectedIndex =-1;
				TbC_ComboListDisplayColumn_Id.SelectedIndex =-1;
				TbC_Combo_MaxDropdownHeight.Text ="";
				TbC_Combo_MaxDropdownWidth.Text ="";
				TbC_Combo_AllowDropdownResizing.IsChecked =false;
				TbC_Combo_IsResetButtonVisible.IsChecked =false;
				TbC_IsActiveRecColumn.IsChecked =false;
				TbC_IsDeletedColumn.IsChecked =false;
				TbC_IsCreatedUserIdColumn.IsChecked =false;
				TbC_IsCreatedDateColumn.IsChecked =false;
				TbC_IsUserIdColumn.IsChecked =false;
				TbC_IsModifiedDateColumn.IsChecked =false;
				TbC_IsRowVersionStampColumn.IsChecked =false;
				TbC_IsBrowsable.IsChecked =true;
				TbC_DeveloperNote.Text ="";
				TbC_UserNote.Text ="";
				TbC_HelpFileAdditionalNote.Text ="";
				TbC_Notes.Text ="";
				TbC_IsInputComplete.IsChecked =false;
				TbC_IsCodeGen.IsChecked =true;
				TbC_IsReadyCodeGen.IsChecked =null;
				TbC_IsCodeGenComplete.IsChecked =null;
				TbC_IsTagForCodeGen.IsChecked =null;
				TbC_IsTagForOther.IsChecked =null;
				TbC_ColumnGroups.Text ="";
				TbC_IsActiveRow.IsChecked =true;
				this.ClearDataFromUI_CustomCode();
                //  Reset all TextBox TextChanged Events now.
                DataControlDataChangedEvents_SetAll();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearDataFromUI", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_LOADING_REC = false;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearDataFromUI", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     A stub for the <see cref="TypeServices.IEntitiyPropertiesControl">IEntitiyPropertiesControl</see> interface.
        /// </summary>
        public void GetEntityRow(Int64? Id)
        {
            throw new NotImplementedException();
        }
        public void GetEntityRow(Int32? Id)
        {
            throw new NotImplementedException();
        }
        public void GetEntityRow(Int16? Id)
        {
            throw new NotImplementedException();
        }
        public void GetEntityRow(Byte? Id)
        {
            throw new NotImplementedException();
        }

        public void GetEntityRow(Guid? Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", new ValuePair[] {new ValuePair("Id", Id) }, IfxTraceCategory.Enter);
                if (Id == null)
                {
                    NewEntityRow();
                }
                else
                {
                    if (objB == null)
                    {
                        LoadControl();
                    }
                    objB.GetEntityRow((Guid)Id);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", IfxTraceCategory.Leave);
            }
        }


        void OnEntityRowReceived(object sender, EntityRowReceivedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnEntityRowReceived", IfxTraceCategory.Enter);
                SetState();
                SetControlsDefaultValidState();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnEntityRowReceived", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnEntityRowReceived", IfxTraceCategory.Leave);
            }
        }


        public void NewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Enter);
                objB.NewEntityRow();
                SetState();
                SetControlsDefaultValidState();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Leave);
            }
        }

        wnConcurrencyManager wn;

        /// <summary>
        ///     Calls the <see cref="EntityBll.WcTableColumn_Bll.Save">Save</see> method on <see cref="EntityBll.WcTableColumn_Bll">WcTableColumn_Bll</see>.
        /// </summary>
        public int Save()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                objB.Save( this, ParentEditObjectType.EntitiyPropertiesControl, UseConcurrencyCheck.UseDefaultSetting);
//                DataServiceInsertUpdateResponseClientSide result = objB.Save(UseConcurrencyCheck.UseDefaultSetting);
//                if (result.Result == DataOperationResult.ConcurrencyFailure)
//                {
//                    wn = new wnConcurrencyManager(new WcTableColumnConcurrencyList(objB.Wire), objB);
//                    wn.Show();
//                    return 1;
//                }
//                else if (result.Result == DataOperationResult.Success)
//                {
//                    // do nothing.
//
//                    if (result.ReturnCurrentRowOnInsertUpdate == true)
//                    {
//                        SetState();
//                    }
//
                    return 1;
//                }
//                else
//                {
//                    string msg = "An error occured:  " + result.Result.ToString() + Environment.NewLine + "If you continue to get this error, please contact support.";  // result.Exception.Message;
//                    MessageBox.Show(msg, "Save Operation Error", MessageBoxButton.OK);
//                    return -1;
//                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
                return -1;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }


        void objB_AsyncSaveWithResponseComplete(object sender, AsyncSaveWithResponseCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "objB_AsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
        
                if (e.Response.Result == DataOperationResult.ConcurrencyFailure)
                {
                    wn = new wnConcurrencyManager(new WcTableColumnConcurrencyList(objB.Wire), objB);
                    wn.Show();
                    //return 1;
                }
                else if (e.Response.Result == DataOperationResult.Success && e.Response.ReturnCurrentRowOnInsertUpdate==true)
                {
                    SetState();
                }
                else if (e.Response.Result == DataOperationResult.Success )
                {
                   // do nothing
                }
                else
                {
                    string msg = "An error occured:  " + e.Response.Result.ToString() + Environment.NewLine + "If you continue to get this error, please contact support.";  // result.Exception.Message;
                    MessageBox.Show(msg, "Save Operation Error", MessageBoxButton.OK);
                    //return -1;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "objB_AsyncSaveWithResponseComplete", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
                //return -1;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "objB_AsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Calls the <see cref="EntityBll.WcTableColumn_Bll.UnDo">UnDo</see> method on <see cref="EntityBll.WcTableColumn_Bll">WcTableColumn_Bll</see> and sets this control back to the previous
        ///     Non-Dirty <see cref="TypeServices.EntityState">state</see>.
        /// </summary>
        public void UnDo()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Enter);
                objB.UnDo();
                SetState();
                SetControlsDefaultValidState();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Not being used. A stub for future use.</summary>
        void CloseMe()
        {
            ////this.Close();
        }

        #endregion Data Related


        #region State Related

        /// <summary>
        ///     Called from <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> as it bubble up
        ///     from the business object. It Configures UI elements according to the current
        ///     <see cref="TypeServices.EntityState">state</see> of the entity. For example: if the
        ///     state is dirty and not valid, the Save button should be disabled and the UnDo
        ///     button should be enabled. Typically when the state is dirty, most areas of the UI
        ///     such as the navigation control (<see cref="ucWcTableColumnList">ucWcTableColumnList</see>) and
        ///     child entity controls are disabled except for this screen (ucWcTableColumnProps). This
        ///     prevents the user from navigating away from data entry area until finishing the job
        ///     – Saving or UnDoing the transaction helps prevent confusion and helps assure data
        ///     integrity.
        /// </summary>
        void ConfigureToCurrentEntityState(EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState", IfxTraceCategory.Enter);
            //    switch (state)
            //    {
            //        case EntityStateSwitch.NewInvalidNotDirty:
            //            btnNew.IsEnabled = false;
            //            btnSave.IsEnabled = false;
            //            btnUnDo.IsEnabled = false;
            //            btnDelete.IsEnabled = false;
            //            break;
            //        case EntityStateSwitch.NewValidNotDirty:
            //            //case DataState.New_NotDirty:
            //            btnNew.IsEnabled = false;
            //            btnSave.IsEnabled = false;
            //            btnUnDo.IsEnabled = false;
            //            btnDelete.IsEnabled = false;
            //            break;
            //        case EntityStateSwitch.NewValidDirty:
            //            //case DataState.New_Dirty_Valid:
            //            btnNew.IsEnabled = false;
            //            btnSave.IsEnabled = true;
            //            btnUnDo.IsEnabled = true;
            //            btnDelete.IsEnabled = true;
            //            break;
            //        case EntityStateSwitch.NewInvalidDirty:
            //            //case DataState.New_Dirty_NotValid:
            //            btnNew.IsEnabled = false;
            //            btnSave.IsEnabled = false;
            //            btnUnDo.IsEnabled = true;
            //            btnDelete.IsEnabled = true;
            //            break;
            //        case EntityStateSwitch.ExistingInvalidDirty:
            //            //case DataState.Existing_Dirty_NotValid:
            //            btnNew.IsEnabled = false;
            //            btnSave.IsEnabled = false;
            //            btnUnDo.IsEnabled = true;
            //            btnDelete.IsEnabled = true;
            //            break;
            //        case EntityStateSwitch.ExistingValidDirty:
            //            //case DataState.Existing_Dirty_Valid:
            //            btnNew.IsEnabled = false;
            //            btnSave.IsEnabled = true;
            //            btnUnDo.IsEnabled = true;
            //            btnDelete.IsEnabled = true;
            //            break;
            //        case EntityStateSwitch.ExistingValidNotDirty:
            //            //case DataState.Existing_Saved:
            //            btnNew.IsEnabled = true;
            //            btnSave.IsEnabled = false;
            //            btnUnDo.IsEnabled = false;
            //            btnDelete.IsEnabled = false;
            //            break;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState", IfxTraceCategory.Leave);
            //}
        }

        void SetControlValidAppearance(IvControlsValidation ctl, bool isValid, bool isDirty)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlValidAppearance", IfxTraceCategory.Enter);
            if (isValid)
            {
                ctl.ValidStateAppearance = ValidationState.Valid;
            }
            else
            {
                if (isDirty)
                {
                    ctl.ValidStateAppearance = ValidationState.NotValidIsDirty;
                }
                else
                {
                    ctl.ValidStateAppearance = ValidationState.NotValidNotDirty;
                }
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlValidAppearance", IfxTraceCategory.Leave);
        }


        void SetControlDefaultValidAppearance(IvControlsValidation ctl, bool isValid)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlDefaultValidAppearance", IfxTraceCategory.Enter);
            if (isValid == true)
            {
                ctl.ValidStateAppearance = ValidationState.Valid;
            }
            else
            {
                ctl.ValidStateAppearance = ValidationState.NotValidNotDirty;
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlDefaultValidAppearance", IfxTraceCategory.Leave);
        }


        /// <summary>
        /// Sets the restricted length value and Valid or Not Valid appearance of the Text
        /// Length label (lblTextLength).
        /// </summary>
        void SetRestrictedStringLengthText()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetRestrictedStringLengthText", IfxTraceCategory.Enter);
                _lblAdrnMngr.LblTextLength.Content = objB.ActiveRestrictedStringPropertyLength;  
                if (objB.ActiveRestrictedStringPropertyLength < 0)
                {
                    _lblAdrnMngr.SetTextLengthNotValidAppearance();
                }
                else
                {
                    _lblAdrnMngr.SetTextLengthValidAppearance();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetRestrictedStringLengthText", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetRestrictedStringLengthText", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Obsolete. This used to set the Broken Rule text in the status bar at the bottom of
        ///     the screen, but now a different mechanism using tooltips is used to notify about
        ///     broken rules.
        ///     <para><br/>
        ///     And for the Broken Rule Tooltips, see:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="tt_Loaded">tt_Loaded</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="LoadToolTipControl">LoadToolTipControl(string fieldName)</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="GetListOfValidationRuleMessagesFromBusinessObject">GetListOfValidationRuleMessagesFromBusinessObject(string
        ///             fieldName)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void SetBrokenRuleText(string rule)
        {
            // Set broken rule here
        }


        #endregion  State Related


        #region General

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a TextBox
        ///     and wires up all of the standard TextBox events to their respective
        ///     handlers:</para>
        /// 	<para class="xmldocbulletlist"></para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>
        ///             TextChanged event to the handler <see cref="OnTextChanged">OnTextChanged</see>
        /// 		</item>
        /// 		<item>
        ///             LostFocus event to the handler <see cref="OnTexBoxLostFocus">OnTexBoxLostFocus</see>
        /// 		</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>This way all event handling is centralized, code is reduced and maintenance
        ///     is improved. All of these methods are auto-generated so even if there are no
        ///     TexBoxes, this code will still be here. Therefore, if TextBoxes are added in the
        ///     future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetTextBoxEvents(TextBox ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new TextChangedEventHandler(OnTextChanged));
            ctl.LostFocus += new RoutedEventHandler(OnTexBoxLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a CheckBox
        ///     and wires up all of the standard CheckBox events to their respective
        ///     handlers:</para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>
        ///             Click event to the handler <see cref="OnCheckBoxClick">OnCheckBoxClick</see>
        /// 		</item>
        /// 		<item>LostFocus event to the handler
        ///         %OnCheckBoxLostFocus:OnCheckBoxLostFocus</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>This way all event handling is centralized, code is reduced and maintenance
        ///     is improved. All of these methods are auto-generated so even if there are no
        ///     CheckBoxs, this code will still be here. Therefore, if CheckBoxs are added in the
        ///     future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetCheckBoxEvents(vCheckBox ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new RoutedEventHandler(OnCheckBoxClick));
            ctl.LostFocus += new RoutedEventHandler(OnCheckBoxLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a RadioButton
        ///     and wires up all of the standard RadioButton events to their respective
        ///     handlers:</para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>
        ///             Click event to the handler <see cref="OnRadioButtonClick">OnRadioButtonClick</see>
        /// 		</item>
        /// 		<item>LostFocus event to the handler
        ///         %OnRadioButtonLostFocus:OnRadioButtonLostFocus</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>This way all event handling is centralized, code is reduced and maintenance
        ///     is improved. All of these methods are auto-generated so even if there are no
        ///     RadioButtons, this code will still be here. Therefore, if RadioButtons are added in the
        ///     future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetRadioButtonEvents(RadioButton ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new RoutedEventHandler(OnRadioButtonClick));
            ctl.LostFocus += new RoutedEventHandler(OnRadioButtonLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a vRadioButtonGroup
        ///     and wires up all of the standard vRadioButtonGroup events to their respective
        ///     handlers:</para>
        /// 	<list type="bullet">
        /// 		<item></item>
        /// 		<item>
        ///             Click event to the handler <see cref="OnvRadioButtonGroupClick">OnvRadioButtonGroupClick</see>
        /// 		</item>
        /// 		<item>LostFocus event to the handler
        ///         %OnvRadioButtonGroupLostFocus:OnvRadioButtonGroupLostFocus</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>This way all event handling is centralized, code is reduced and maintenance
        ///     is improved. All of these methods are auto-generated so even if there are no
        ///     vRadioButtonGroups, this code will still be here. Therefore, if vRadioButtonGroups are added in the
        ///     future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetvRadioButtonGroupEvents(vRadioButtonGroup ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new EventHandler(OnvRadioButtonGroupItemChecked));
        }

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a
        ///     XamComboEditor and wires up all of the standard DatePicker events to their
        ///     respective handlers:</para>
        /// 	<list type="bullet">
        /// 		<item>
        ///             SelectedItemChanged event to the handler <see cref="OnDatePicker_TextChanged">OnDatePicker_TextChanged</see>
        /// 		</item>
        /// 		<item>
        ///             LostFocus event to the handler <see cref="OnDatePickerLostFocus">OnDatePickerLostFocus</see>
        /// 		</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>
        ///     This way all event handling is centralized, code is reduced and maintenance is
        ///     improved. All of these methods are auto-generated so even if there are no
        ///     DatePicker, this code will still be here. Therefore, if XamComboEditors are
        ///     added in the future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetDatePickerEvents(DatePicker ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new TextChangedEventHandler(OnDatePicker_TextChanged));
            ctl.LostFocus += new RoutedEventHandler(OnDatePickerLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }


        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a
        ///     XamComboEditor and wires up all of the standard TimePicker events to their
        ///     respective handlers:</para>
        /// 	<list type="bullet">
        /// 		<item>
        ///             SelectedItemChanged event to the handler <see cref="OnTimePicker_ValueChanged">OnTimePicker_ValueChanged</see>
        /// 		</item>
        /// 		<item>
        ///             LostFocus event to the handler <see cref="OnTimePickerLostFocus">OnTimePickerLostFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>
        ///     This way all event handling is centralized, code is reduced and maintenance is
        ///     improved. All of these methods are auto-generated so even if there are no
        ///     TimePicker, this code will still be here. Therefore, if xxx are
        ///     added in the future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetTimePickerEvents(TimePicker ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new RoutedPropertyChangedEventHandler<DateTime?>(OnTimePicker_ValueChanged));
            ctl.LostFocus += new RoutedEventHandler(OnTimePickerLostFocus);
            //ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a
        ///     XamComboEditor and wires up all of the standard XamComboEditor events to their
        ///     respective handlers:</para>
        /// 	<list type="bullet">
        /// 		<item>
        ///             SelectedItemChanged event to the handler <see cref="OnXamComboEditorSelectionChanged">OnXamComboEditorSelectionChanged</see>
        /// 		</item>
        /// 		<item>
        ///             LostFocus event to the handler <see cref="OnXamComboEditorLostFocus">OnXamComboEditorLostFocus</see>
        /// 		</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>
        ///     This way all event handling is centralized, code is reduced and maintenance is
        ///     improved. All of these methods are auto-generated so even if there are no
        ///     XamComboEditors, this code will still be here. Therefore, if XamComboEditors are
        ///     added in the future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetXamComboEditorEvents(XamComboEditor ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new EventHandler(OnXamComboEditorSelectionChanged));
            ctl.LostFocus += new RoutedEventHandler(OnXamComboEditorLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        void SetVXamComboEditorEvents(vXamComboEditor ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
            ctl.LostFocus += new RoutedEventHandler(OnXamComboEditorLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a
        ///     XamMultiColumnComboEditor and wires up all of the standard XamMultiColumnComboEditor events to their
        ///     respective handlers:</para>
        /// 	<list type="bullet">
        /// 		<item>
        ///             SelectedItemChanged event to the handler <see cref="OnXamMultiColumnComboEditorSelectionChanged">OnXamMultiColumnComboEditorSelectionChanged</see>
        /// 		</item>
        /// 		<item>
        ///             LostFocus event to the handler <see cref="OnXamMultiColumnComboEditorLostFocus">OnXamMultiColumnComboEditorLostFocus</see>
        /// 		</item>
        /// 		<item>
        ///             GotFocus event to the handler <see cref="OnDataControlGotFocus">OnDataControlGotFocus</see>
        /// 		</item>
        /// 	</list>
        /// 	<para>
        ///     This way all event handling is centralized, code is reduced and maintenance is
        ///     improved. All of these methods are auto-generated so even if there are no
        ///     XamMultiColumnComboEditors, this code will still be here. Therefore, if XamMultiColumnComboEditors are
        ///     added in the future, all of the supporting code will already be in place.</para>
        /// </summary>
        void SetXamMultiColumnComboEditorEvents(XamMultiColumnComboEditor ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new EventHandler(OnXamMultiColumnComboEditorSelectionChanged));
            ctl.LostFocus += new RoutedEventHandler(OnXamMultiColumnComboEditorLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        void SetVXamMultiColumnComboEditorEvents(vXamMultiColumnComboEditor ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new vXamMultiColumnComboEditorSelectionChangedEventHandler(OnVXamMultiColumnComboEditorSelectionChanged));
            ctl.LostFocus += new RoutedEventHandler(OnXamMultiColumnComboEditorLostFocus);
            ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        void SetTXamColorPickerEvents(XamColorPicker ctl)
        {
            _dataControlEventManager.AddHandler(ctl, new EventHandler<SelectedColorChangedEventArgs>(XamColorPicker_SelectedColorChanged));
            //ctl.LostFocus += new RoutedEventHandler(OnTexBoxLostFocus);
            //ctl.GotFocus += new RoutedEventHandler(OnDataControlGotFocus);
        }

        void DataControlDataChangedEvents_SetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DataControlDataChangedEvents_SetAll", IfxTraceCategory.Enter);

                //TextBox
				_dataControlEventManager.AddHandler(TbC_SortOrder, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_Name, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_Description, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_Length, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_Precision, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_Scale, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_BrokenRuleText, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_DefaultValue, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_DefaultCaption, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_ColumnHeaderText, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_LabelCaptionVerbose, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_TextBoxFormat, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_TextColumnFormat, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_ColumnWidth, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_TextBoxTextAlignment, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_ColumnTextAlignment, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_Combo_MaxDropdownHeight, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_Combo_MaxDropdownWidth, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_DeveloperNote, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_UserNote, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_HelpFileAdditionalNote, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_Notes, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.AddHandler(TbC_ColumnGroups, new TextChangedEventHandler(OnTextChanged));

                //XamComboEditor

                //vXamComboEditor
				_dataControlEventManager.AddHandler(TbC_CtlTp_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
				_dataControlEventManager.AddHandler(TbC_DtSql_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
				_dataControlEventManager.AddHandler(TbC_DtDtNt_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
				_dataControlEventManager.AddHandler(TbC_ListStoredProc_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
				_dataControlEventManager.AddHandler(TbC_ParentColumnKey, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
				_dataControlEventManager.AddHandler(TbC_ComboListTable_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
				_dataControlEventManager.AddHandler(TbC_ComboListDisplayColumn_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));

                //CheckBox
				_dataControlEventManager.AddHandler(TbC_IsNonvColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsPK, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsIdentity, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsFK, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsEntityColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsSystemField, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsValuesObjectMember, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsInPropsScreen, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsInNavList, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsRequired, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_AllowZero, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsNullableInDb, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsNullableInUI, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsReadFromDb, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsSendToDb, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsInsertAllowed, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsEditAllowed, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsReadOnlyInUI, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_UseForAudit, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_LabelCaptionGenerate, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(Tbc_ShowGridColumnToolTip, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(Tbc_ShowPropsToolTip, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsCreatePropsStrings, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsCreateGridStrings, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsAvailableForColumnGroups, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsTextWrapInProp, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsTextWrapInGrid, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsStaticList, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_UseNotInList, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_UseListEditBtn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_UseDisplayTextFieldProperty, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsDisplayTextFieldProperty, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_Combo_AllowDropdownResizing, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_Combo_IsResetButtonVisible, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsActiveRecColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsDeletedColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsCreatedUserIdColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsCreatedDateColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsUserIdColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsModifiedDateColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsRowVersionStampColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsBrowsable, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsInputComplete, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsCodeGen, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsReadyCodeGen, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsCodeGenComplete, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsTagForCodeGen, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsTagForOther, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.AddHandler(TbC_IsActiveRow, new RoutedEventHandler(OnCheckBoxClick));

                DataControlDataChangedEvents_SetAll_Custom();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DataControlDataChangedEvents_SetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DataControlDataChangedEvents_SetAll", IfxTraceCategory.Leave);
            }
        }

        void DataControlDataChangedEvents_RemoveAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DataControlDataChangedEvents_RemoveAll", IfxTraceCategory.Enter);

                //TextBox
				_dataControlEventManager.RemoveHandler(TbC_SortOrder, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_Name, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_Description, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_Length, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_Precision, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_Scale, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_BrokenRuleText, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_DefaultValue, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_DefaultCaption, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_ColumnHeaderText, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_LabelCaptionVerbose, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_TextBoxFormat, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_TextColumnFormat, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_ColumnWidth, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_TextBoxTextAlignment, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_ColumnTextAlignment, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_Combo_MaxDropdownHeight, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_Combo_MaxDropdownWidth, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_DeveloperNote, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_UserNote, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_HelpFileAdditionalNote, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_Notes, new TextChangedEventHandler(OnTextChanged));
				_dataControlEventManager.RemoveHandler(TbC_ColumnGroups, new TextChangedEventHandler(OnTextChanged));

                //XamComboEditor

                // vXamComboEditor
				_dataControlEventManager.RemoveHandler(TbC_CtlTp_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
				_dataControlEventManager.RemoveHandler(TbC_DtSql_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
				_dataControlEventManager.RemoveHandler(TbC_DtDtNt_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
				_dataControlEventManager.RemoveHandler(TbC_ListStoredProc_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
				_dataControlEventManager.RemoveHandler(TbC_ParentColumnKey, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
				_dataControlEventManager.RemoveHandler(TbC_ComboListTable_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));
				_dataControlEventManager.RemoveHandler(TbC_ComboListDisplayColumn_Id, new vXamComboEditorSelectionChangedEventHandler(OnVXamComboEditorSelectionChanged));

                //CheckBox
				_dataControlEventManager.RemoveHandler(TbC_IsNonvColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsPK, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsIdentity, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsFK, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsEntityColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsSystemField, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsValuesObjectMember, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsInPropsScreen, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsInNavList, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsRequired, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_AllowZero, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsNullableInDb, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsNullableInUI, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsReadFromDb, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsSendToDb, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsInsertAllowed, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsEditAllowed, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsReadOnlyInUI, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_UseForAudit, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_LabelCaptionGenerate, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(Tbc_ShowGridColumnToolTip, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(Tbc_ShowPropsToolTip, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsCreatePropsStrings, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsCreateGridStrings, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsAvailableForColumnGroups, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsTextWrapInProp, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsTextWrapInGrid, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsStaticList, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_UseNotInList, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_UseListEditBtn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_UseDisplayTextFieldProperty, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsDisplayTextFieldProperty, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_Combo_AllowDropdownResizing, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_Combo_IsResetButtonVisible, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsActiveRecColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsDeletedColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsCreatedUserIdColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsCreatedDateColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsUserIdColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsModifiedDateColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsRowVersionStampColumn, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsBrowsable, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsInputComplete, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsCodeGen, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsReadyCodeGen, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsCodeGenComplete, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsTagForCodeGen, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsTagForOther, new RoutedEventHandler(OnCheckBoxClick));
				_dataControlEventManager.RemoveHandler(TbC_IsActiveRow, new RoutedEventHandler(OnCheckBoxClick));

                DataControlDataChangedEvents_RemoveAll_Custom();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DataControlDataChangedEvents_RemoveAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DataControlDataChangedEvents_RemoveAll", IfxTraceCategory.Leave);
            }
        }

        #endregion General


        #endregion General Methods


        #region General Properties, Getters and Setters

        /// <returns>List&lt;ValidationRuleMessage&gt;</returns>
        /// <summary>
        ///     Returns a list of all the BrokenRules for WcTableColumn from the <see cref="EntityBll.WcTableColumn_Bll.GetBrokenRulesForEntity">GetBrokenRuleListForEntity</see>
        ///     method. This could be used to present a full list of all BrokenRules rather than
        ///     just showing a subset in the BrokenRules ToolTip.
        /// </summary>
        /// <seealso cref="tt_Loaded">tt_Loaded Method</seealso>
        /// <seealso cref="TypeServices.BrokenRuleManager.GetBrokenRulesForEntity">GetBrokenRulesForEntity Method (TypeServices.BrokenRuleManager)</seealso>
        public List<vRuleItem> GetBrokenRuleListForEntity()
        {
            return objB.GetBrokenRuleListForEntity();
        }

        /// <summary>
        /// 	<para>
        ///         Gets or Sets the current <see cref="EntityBll.WcTableColumn_Bll">Enty_Bll</see>
        ///         supporting this control.
        ///     </para>
        /// 	<para>
        ///         Important: You avoid using this setter because - if your intent is to set a
        ///         different instance of WcTableColumn_Bll as this control’s current business object, many
        ///         other actions must be performed for this control to work properly. Use the
        ///         <see cref="SetBusinessObject">SetBusinessObject</see> method instead.
        ///     </para>
        /// 	<para>
        ///         This is a surrogate for the $CurrentBusinessObject:CurrentBusinessObject%
        ///         property for compatibility with the <see cref="IEntitiyPropertiesControl">IEntitiyPropertiesControl</see> interface making
        ///         this control more extendable.
        ///     </para>
        /// </summary>
        /// <value>WcTableColumn_Bll</value>
        public IBusinessObject IEntityControlCurrentBusinessObject
        {
            get { return objB; }
            set { SetBusinessObject((WcTableColumn_Bll)value); }
        }

        /// <summary>
        /// 	<para>
        ///         Gets or Sets the current <see cref="EntityBll.WcTableColumn_Bll">Enty_Bll</see>
        ///         supporting this control.
        ///     </para>
        /// 	<para>
        ///         Important: You avoid using this setter because - if your intent is to set a
        ///         different instance of WcTableColumn_Bll as this control’s current business object, many
        ///         other actions must be performed for this control to work properly. Use the
        ///         <see cref="SetBusinessObject">SetBusinessObject</see> method instead.
        ///     </para>
        /// 	<para>
        /// 		<see cref="IEntityControlCurrentBusinessObject">IEntityControlCurrentBusinessObject</see>
        ///         is a surrogate for this property for compatibility with the <see cref="IEntitiyPropertiesControl">IEntitiyPropertiesControl</see> interface making
        ///         this control more extendable.
        ///     </para>
        /// </summary>
        /// <value>WcTableColumn_Bll</value>
        public WcTableColumn_Bll CurrentBusinessObject
        {
            get { return objB; }
            set { SetBusinessObject(value); }
        }

        /// <summary>
        ///     Sets the value of IsActivePropertiesControl. Refer to the <see cref=" IsActivePropertiesControl">IsActivePropertiesControl</see> documentation for its
        ///     usage.
        /// </summary>
        /// <seealso cref="IsActivePropertiesControl">IsActivePropertiesControl Property</seealso>
        public void SetIsActivePropertiesControl(bool value)
        {
            _isActivePropertiesControl = value;
        }

        /// <summary>
        ///     Gets the value of IsActivePropertiesControl. Refer to the <see cref=" IsActivePropertiesControl">IsActivePropertiesControl</see> documentation for its
        ///     usage.
        /// </summary>
        /// <seealso cref="IsActivePropertiesControl">IsActivePropertiesControl Property</seealso>
        public bool GetIsActivePropertiesControl()
        {
            return _isActivePropertiesControl;
        }

        /// <summary>
        /// A flag telling us if this is the active properties control. A complex screen can
        /// have many entity controls each with its own properties control (ucProps) and additional
        /// nested entity controls. Only one entity control can be active at a time. When ucProps
        /// becomes the active properties control, it raises an event that tells its entity control
        /// (or parent control) that it’s active. At this point the entity control becomes the
        /// active entity control. When a user clicks or tabs into a, EntityList, EntityProps, or
        /// any other child control of an entity control, this flag is set to true. As code bubbles
        /// up or tunnels down through the many layers of WPF elements, its often important to know
        /// when its entering the active entity control.
        /// </summary>
        /// <seealso cref="RaiseCurrentEntityStateChanged">RaiseCurrentEntityStateChanged Method</seealso>
        public bool IsActivePropertiesControl
        {
            get { return _isActivePropertiesControl; }
            set 
            {
                _isActivePropertiesControl = value; 
            }
        }

        /// <seealso cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged Method</seealso>
        /// <seealso cref="OnControlValidStateChanged">OnControlValidStateChanged Method</seealso>
        /// <seealso cref="TypeServices.EntityState">EntityState Class</seealso>
        /// <seealso cref="TypeServices.EntityStateSwitch">EntityStateSwitch Enumeration</seealso>
        /// <summary>
        ///     Gets the current <see cref="TypeServices.EntityStateSwitch">EntityStateSwitch</see>
        ///     from the <see cref="EntityBll.WcTableColumn_Bll.StateSwitch">WcTableColumn_Bll.StateSwitch</see>
        ///     property.
        /// </summary>
        public EntityStateSwitch GetEntityStateSwitch()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityStateSwitch", IfxTraceCategory.Enter);
                return objB.StateSwitch;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityStateSwitch", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityStateSwitch", IfxTraceCategory.Leave);
            }
        }

        /// <seealso cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged Method</seealso>
        /// <seealso cref="OnControlValidStateChanged">OnControlValidStateChanged Method</seealso>
        /// <seealso cref="TypeServices.EntityState">EntityState Class</seealso>
        /// <seealso cref="TypeServices.EntityStateSwitch">EntityStateSwitch Enumeration</seealso>
        /// <summary>
        ///     Gets the current <see cref="TypeServices.EntityState">EntityState</see> from the
        ///     <see cref="EntityBll.WcTableColumn_Bll.State">WcTableColumn_Bll.State</see> property.
        /// </summary>
        public EntityState GetEntityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityState", IfxTraceCategory.Enter);
                return objB.State;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityState", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Returns true if this control has a business object (bound to an instance of
        ///     <see cref="EntityBll.WcTableColumn_Bll">WcTableColumn_Bll</see> – note, this control doesn’t
        ///     actually bind to WcTableColumn_Bll, but rather emulates binding through custom code for
        ///     greater control of its behavior.)
        /// </summary>
        public bool HasBusinessObject()
        {
            return _hasBusinessObject;
        }



//        public void SetParentContainerType(bool isGrid)
//        {
//            _parentIsGrid = isGrid;
//        }

        #endregion General Properties, Getters and Setters


        #region  Edit Combo Dropdown List Code

        #region Support Methods

        /// <summary>
        /// 	<para>
        ///         Called from the SomeComboBox_EditDropDownList_AcceptCancelChanges
        ///         event. Normaly this raised event will cause the control hosting a navigation to
        ///         update a list used to popate a list control such as a combox being used as an
        ///         editor control in a grid cell.
        ///     </para>
        /// </summary>
        /// <param name="columnName">Name of the property window/control's data field's list control ( i.e. ComboBox) who's list was just updated.  This name will be used to identify the column in a grid to update.</param>
        public void RaiseListColumnListMustUpdate(string columnName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseListColumnListMustUpdate", IfxTraceCategory.Enter);
                ListColumnListMustUpdateEventHandler handler = OnListColumnListMustUpdate;
                if (handler != null)
                {
                    handler(this, new ListColumnListMustUpdateArgs(columnName));
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseListColumnListMustUpdate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseListColumnListMustUpdate", IfxTraceCategory.Leave);
            }
        }
        
        #endregion Support Methods

        #endregion  Edit Combo Dropdown List Code



        #endregion General Methods and Properties


        #region ToolTip Stuff

        #region Validation Tooltips

        /// <summary>
        /// 	<para>Called from SetControlEvents, this method receives a reference to a Control
        ///     and wires up all of the Tooltip events used by the BrokenRules Tooltip. This
        ///     ToolTip will show a list of one or more BrokenRules for each control. All controls
        ///     that have validation are passed into this method for wiring.</para>
        /// 	<para>This way all event handling for ToolTips is centralized, code is reduced and
        ///     maintenance is improved. These ToolTip methods are auto-generated so even if
        ///     currently there are no controls with validation, this code will still be here.
        ///     Therefore, if control validation is added in the future, all of the supporting code
        ///     will already be in place.</para>
        /// </summary>
        void AttachToolTip(Control ctl)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachToolTip", IfxTraceCategory.Enter);
                vToolTip tt = vToolTipHelper.GetTooltipInstanceForValidationRuleToolTip();
                ControlTemplate ct = (ControlTemplate)Application.Current.Resources["ToolTipTemplate"];
                tt.Template = ct;
                tt.Content = new TooltipBrokenRuleContent();
                // These lines before and after need to be set in this order or things may not work as expected.
                tt.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
                tt.PlacementTarget = ctl;

                tt.Loaded += new RoutedEventHandler(tt_Loaded);
                vToolTipService.SetToolTip(ctl, tt);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachToolTip", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachToolTip", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     The ToolTip Loaded event is wired to this method (in the <see cref="AttachToolTip">AttachToolTip</see> method) for every data control that has
        ///     validation. When the mouse passes over the control; this method is called,
        ///     configures the tooltip dynamically with the control’s BrokenRules and displays the
        ///     tooltip.
        /// </summary>
        /// <seealso cref="LoadToolTipControl">LoadToolTipControl Method</seealso>
        /// <seealso cref="GetListOfValidationRuleMessagesFromBusinessObject">GetListOfValidationRuleMessagesFromBusinessObject Method</seealso>
        void tt_Loaded(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "tt_Loaded", IfxTraceCategory.Enter);
                Control ctl = (Control)sender;
                if (objB.IsPropertyValid(((Control)((ToolTip)sender).PlacementTarget).Name))
                {
                    ctl.Visibility = Visibility.Collapsed;
                    return;
                }
                else
                {
                    ctl.Visibility = Visibility.Visible;
                }
                vToolTip tt = (vToolTip)sender;
                TooltipBrokenRuleContent uc = tt.Content as TooltipBrokenRuleContent;
                if (uc != null)
                {
                    List<vRuleItem> rules = objB.GetBrokenRulesForProperty(((Control)tt.PlacementTarget).Name);
                    uc.ItemsSource = rules;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "tt_Loaded", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "tt_Loaded", IfxTraceCategory.Leave);
            }
        }    

        #endregion Validation Tooltips

        #region Business Rule Tooltips


        void CreateBusinessRuleTooltips()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateBusinessRuleTooltips", IfxTraceCategory.Enter);

                // lblTbC_IsNonvColumn
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsNonvColumn_1, 
                    StringsWcTableColumnPropsTooltips.TbC_IsNonvColumn_2}, StringsWcTableColumnProps.TbC_IsNonvColumn_Vbs), lblTbC_IsNonvColumn);

                // lblTbC_Precision
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_Precision_1}, StringsWcTableColumnProps.TbC_Precision_Vbs), lblTbC_Precision);

                // lblTbC_Scale
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_Scale_1, 
                    StringsWcTableColumnPropsTooltips.TbC_Scale_2}, StringsWcTableColumnProps.TbC_Scale_Vbs), lblTbC_Scale);

                // lblTbC_IsIdentity
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsIdentity_1}, StringsWcTableColumnProps.TbC_IsIdentity_Vbs), lblTbC_IsIdentity);

                // lblTbC_IsEntityColumn
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsEntityColumn_1}, StringsWcTableColumnProps.TbC_IsEntityColumn_Vbs), lblTbC_IsEntityColumn);

                // lblTbC_IsSystemField
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsSystemField_1, 
                    StringsWcTableColumnPropsTooltips.TbC_IsSystemField_2}, StringsWcTableColumnProps.TbC_IsSystemField_Vbs), lblTbC_IsSystemField);

                // lblTbC_IsValuesObjectMember
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsValuesObjectMember_1}, StringsWcTableColumnProps.TbC_IsValuesObjectMember_Vbs), lblTbC_IsValuesObjectMember);

                // lblTbC_IsInPropsScreen
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsInPropsScreen_1}, StringsWcTableColumnProps.TbC_IsInPropsScreen_Vbs), lblTbC_IsInPropsScreen);

                // lblTbC_IsInNavList
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(StringsWcTableColumnProps.TbC_IsInNavList_Vbs), lblTbC_IsInNavList);

                // lblTbC_IsReadFromDb
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsReadFromDb_1}, StringsWcTableColumnProps.TbC_IsReadFromDb_Vbs), lblTbC_IsReadFromDb);

                // lblTbC_IsSendToDb
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsSendToDb_1}, StringsWcTableColumnProps.TbC_IsSendToDb_Vbs), lblTbC_IsSendToDb);

                // lblTbC_IsInsertAllowed
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsInsertAllowed_1}, StringsWcTableColumnProps.TbC_IsInsertAllowed_Vbs), lblTbC_IsInsertAllowed);

                // lblTbC_IsEditAllowed
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsEditAllowed_1, 
                    StringsWcTableColumnPropsTooltips.TbC_IsEditAllowed_2}, StringsWcTableColumnProps.TbC_IsEditAllowed_Vbs), lblTbC_IsEditAllowed);

                // lblTbC_IsReadOnlyInUI
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsReadOnlyInUI_1, 
                    StringsWcTableColumnPropsTooltips.TbC_IsReadOnlyInUI_2}, StringsWcTableColumnProps.TbC_IsReadOnlyInUI_Vbs), lblTbC_IsReadOnlyInUI);

                // lblTbC_UseForAudit
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_UseForAudit_1}, StringsWcTableColumnProps.TbC_UseForAudit_Vbs), lblTbC_UseForAudit);

                // lblTbC_DefaultCaption
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_DefaultCaption_1}, StringsWcTableColumnProps.TbC_DefaultCaption_Vbs), lblTbC_DefaultCaption);

                // lblTbC_ColumnHeaderText
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_ColumnHeaderText_1}, StringsWcTableColumnProps.TbC_ColumnHeaderText_Vbs), lblTbC_ColumnHeaderText);

                // lblTbC_LabelCaptionVerbose
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_LabelCaptionVerbose_1}, StringsWcTableColumnProps.TbC_LabelCaptionVerbose_Vbs), lblTbC_LabelCaptionVerbose);

                // lblTbC_LabelCaptionGenerate
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_LabelCaptionGenerate_1, 
                    StringsWcTableColumnPropsTooltips.TbC_LabelCaptionGenerate_2}, StringsWcTableColumnProps.TbC_LabelCaptionGenerate_Vbs), lblTbC_LabelCaptionGenerate);

                // lblTbc_ShowGridColumnToolTip
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.Tbc_ShowGridColumnToolTip_1}, StringsWcTableColumnProps.Tbc_ShowGridColumnToolTip_Vbs), lblTbc_ShowGridColumnToolTip);

                // lblTbc_ShowPropsToolTip
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(StringsWcTableColumnProps.Tbc_ShowPropsToolTip_Vbs), lblTbc_ShowPropsToolTip);

                // lblTbC_IsCreatePropsStrings
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsCreatePropsStrings_1, 
                    StringsWcTableColumnPropsTooltips.TbC_IsCreatePropsStrings_2}, StringsWcTableColumnProps.TbC_IsCreatePropsStrings_Vbs), lblTbC_IsCreatePropsStrings);

                // lblTbC_IsCreateGridStrings
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsCreateGridStrings_1, 
                    StringsWcTableColumnPropsTooltips.TbC_IsCreateGridStrings_2}, StringsWcTableColumnProps.TbC_IsCreateGridStrings_Vbs), lblTbC_IsCreateGridStrings);

                // lblTbC_IsAvailableForColumnGroups
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsAvailableForColumnGroups_1}, StringsWcTableColumnProps.TbC_IsAvailableForColumnGroups_Vbs), lblTbC_IsAvailableForColumnGroups);

                // lblTbC_IsTextWrapInProp
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(StringsWcTableColumnProps.TbC_IsTextWrapInProp_Vbs), lblTbC_IsTextWrapInProp);

                // lblTbC_ColumnWidth
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_ColumnWidth_1}, StringsWcTableColumnProps.TbC_ColumnWidth_Vbs), lblTbC_ColumnWidth);

                // lblTbC_ListStoredProc_Id
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_ListStoredProc_Id_1}, StringsWcTableColumnProps.TbC_ListStoredProc_Id_Vbs), lblTbC_ListStoredProc_Id);

                // lblTbC_IsStaticList
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsStaticList_1}, StringsWcTableColumnProps.TbC_IsStaticList_Vbs), lblTbC_IsStaticList);

                // lblTbC_UseNotInList
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_UseNotInList_1, 
                    StringsWcTableColumnPropsTooltips.TbC_UseNotInList_2}, StringsWcTableColumnProps.TbC_UseNotInList_Vbs), lblTbC_UseNotInList);

                // lblTbC_UseListEditBtn
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_UseListEditBtn_1}, StringsWcTableColumnProps.TbC_UseListEditBtn_Vbs), lblTbC_UseListEditBtn);

                // lblTbC_ParentColumnKey
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_ParentColumnKey_1, 
                    StringsWcTableColumnPropsTooltips.TbC_ParentColumnKey_2, 
                    StringsWcTableColumnPropsTooltips.TbC_ParentColumnKey_3}, StringsWcTableColumnProps.TbC_ParentColumnKey_Vbs), lblTbC_ParentColumnKey);

                // lblTbC_UseDisplayTextFieldProperty
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_UseDisplayTextFieldProperty_1, 
                    StringsWcTableColumnPropsTooltips.TbC_UseDisplayTextFieldProperty_2}, StringsWcTableColumnProps.TbC_UseDisplayTextFieldProperty_Vbs), lblTbC_UseDisplayTextFieldProperty);

                // lblTbC_IsDisplayTextFieldProperty
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsDisplayTextFieldProperty_1}, StringsWcTableColumnProps.TbC_IsDisplayTextFieldProperty_Vbs), lblTbC_IsDisplayTextFieldProperty);

                // lblTbC_ComboListTable_Id
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_ComboListTable_Id_1, 
                    StringsWcTableColumnPropsTooltips.TbC_ComboListTable_Id_2, 
                    StringsWcTableColumnPropsTooltips.TbC_ComboListTable_Id_3}, StringsWcTableColumnProps.TbC_ComboListTable_Id_Vbs), lblTbC_ComboListTable_Id);

                // lblTbC_ComboListDisplayColumn_Id
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_ComboListDisplayColumn_Id_1, 
                    StringsWcTableColumnPropsTooltips.TbC_ComboListDisplayColumn_Id_2}, StringsWcTableColumnProps.TbC_ComboListDisplayColumn_Id_Vbs), lblTbC_ComboListDisplayColumn_Id);

                // lblTbC_Combo_MaxDropdownHeight
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_Combo_MaxDropdownHeight_1, 
                    StringsWcTableColumnPropsTooltips.TbC_Combo_MaxDropdownHeight_2}, StringsWcTableColumnProps.TbC_Combo_MaxDropdownHeight_Vbs), lblTbC_Combo_MaxDropdownHeight);

                // lblTbC_Combo_MaxDropdownWidth
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_Combo_MaxDropdownWidth_1, 
                    StringsWcTableColumnPropsTooltips.TbC_Combo_MaxDropdownWidth_2}, StringsWcTableColumnProps.TbC_Combo_MaxDropdownWidth_Vbs), lblTbC_Combo_MaxDropdownWidth);

                // lblTbC_Combo_AllowDropdownResizing
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_Combo_AllowDropdownResizing_1}, StringsWcTableColumnProps.TbC_Combo_AllowDropdownResizing_Vbs), lblTbC_Combo_AllowDropdownResizing);

                // lblTbC_Combo_IsResetButtonVisible
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_Combo_IsResetButtonVisible_1, 
                    StringsWcTableColumnPropsTooltips.TbC_Combo_IsResetButtonVisible_2}, StringsWcTableColumnProps.TbC_Combo_IsResetButtonVisible_Vbs), lblTbC_Combo_IsResetButtonVisible);

                // lblTbC_IsActiveRecColumn
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsActiveRecColumn_1}, StringsWcTableColumnProps.TbC_IsActiveRecColumn_Vbs), lblTbC_IsActiveRecColumn);

                // lblTbC_IsDeletedColumn
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsDeletedColumn_1, 
                    StringsWcTableColumnPropsTooltips.TbC_IsDeletedColumn_2}, StringsWcTableColumnProps.TbC_IsDeletedColumn_Vbs), lblTbC_IsDeletedColumn);

                // lblTbC_IsCreatedUserIdColumn
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsCreatedUserIdColumn_1, 
                    StringsWcTableColumnPropsTooltips.TbC_IsCreatedUserIdColumn_2}, StringsWcTableColumnProps.TbC_IsCreatedUserIdColumn_Vbs), lblTbC_IsCreatedUserIdColumn);

                // lblTbC_IsCreatedDateColumn
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsCreatedDateColumn_1, 
                    StringsWcTableColumnPropsTooltips.TbC_IsCreatedDateColumn_2}, StringsWcTableColumnProps.TbC_IsCreatedDateColumn_Vbs), lblTbC_IsCreatedDateColumn);

                // lblTbC_IsUserIdColumn
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsUserIdColumn_1, 
                    StringsWcTableColumnPropsTooltips.TbC_IsUserIdColumn_2}, StringsWcTableColumnProps.TbC_IsUserIdColumn_Vbs), lblTbC_IsUserIdColumn);

                // lblTbC_IsModifiedDateColumn
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsModifiedDateColumn_1, 
                    StringsWcTableColumnPropsTooltips.TbC_IsModifiedDateColumn_2}, StringsWcTableColumnProps.TbC_IsModifiedDateColumn_Vbs), lblTbC_IsModifiedDateColumn);

                // lblTbC_IsRowVersionStampColumn
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsRowVersionStampColumn_1}, StringsWcTableColumnProps.TbC_IsRowVersionStampColumn_Vbs), lblTbC_IsRowVersionStampColumn);

                // lblTbC_IsBrowsable
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsBrowsable_1, 
                    StringsWcTableColumnPropsTooltips.TbC_IsBrowsable_2, 
                    StringsWcTableColumnPropsTooltips.TbC_IsBrowsable_3}, StringsWcTableColumnProps.TbC_IsBrowsable_Vbs), lblTbC_IsBrowsable);

                // lblTbC_DeveloperNote
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_DeveloperNote_1, 
                    StringsWcTableColumnPropsTooltips.TbC_DeveloperNote_2}, StringsWcTableColumnProps.TbC_DeveloperNote_Vbs), lblTbC_DeveloperNote);

                // lblTbC_UserNote
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_UserNote_1, 
                    StringsWcTableColumnPropsTooltips.TbC_UserNote_2, 
                    StringsWcTableColumnPropsTooltips.TbC_UserNote_3}, StringsWcTableColumnProps.TbC_UserNote_Vbs), lblTbC_UserNote);

                // lblTbC_HelpFileAdditionalNote
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_HelpFileAdditionalNote_1}, StringsWcTableColumnProps.TbC_HelpFileAdditionalNote_Vbs), lblTbC_HelpFileAdditionalNote);

                // lblTbC_Notes
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_Notes_1}, StringsWcTableColumnProps.TbC_Notes_Vbs), lblTbC_Notes);

                // lblTbC_IsInputComplete
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsInputComplete_1}, StringsWcTableColumnProps.TbC_IsInputComplete_Vbs), lblTbC_IsInputComplete);

                // lblTbC_IsCodeGen
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsCodeGen_1}, StringsWcTableColumnProps.TbC_IsCodeGen_Vbs), lblTbC_IsCodeGen);

                // lblTbC_IsActiveRow
                vToolTipHelper.BindBusinessRuleToolTipToElement(new TooltipBusinessRuleContent(new string[] {
                    StringsWcTableColumnPropsTooltips.TbC_IsActiveRow_1, 
                    StringsWcTableColumnPropsTooltips.TbC_IsActiveRow_2}, StringsWcTableColumnProps.TbC_IsActiveRow_Vbs), lblTbC_IsActiveRow);
                DefineCustomToolTips();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateBusinessRuleTooltips", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateBusinessRuleTooltips", IfxTraceCategory.Leave);
            }
        }

 
        #endregion Business Rule Tooltips

        #endregion ToolTip Stuff


        #region Format Fields and ReadOnly Assignments

        void FormatFields()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FormatFields", IfxTraceCategory.Enter);
                FormatFields_Custom();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FormatFields", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FormatFields", IfxTraceCategory.Leave);
            }
        }



        void ReadOnlyAssignments()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReadOnlyAssignments", IfxTraceCategory.Enter);
                ReadOnlyAssignments_Custom();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReadOnlyAssignments", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReadOnlyAssignments", IfxTraceCategory.Leave);
            }
        }



        #endregion Format Fields and ReadOnly Assignments


        #region Assign Text Length Labels


        void AssignTextLengthLabels()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Enter);
                TbC_Name.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_Name);
                TbC_Description.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_Description);
                TbC_BrokenRuleText.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_BrokenRuleText);
                TbC_DefaultValue.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_DefaultValue);
                TbC_DefaultCaption.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_DefaultCaption);
                TbC_ColumnHeaderText.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_ColumnHeaderText);
                TbC_LabelCaptionVerbose.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_LabelCaptionVerbose);
                TbC_TextBoxFormat.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_TextBoxFormat);
                TbC_TextColumnFormat.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_TextColumnFormat);
                TbC_TextBoxTextAlignment.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_TextBoxTextAlignment);
                TbC_ColumnTextAlignment.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_ColumnTextAlignment);
                TbC_DeveloperNote.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_DeveloperNote);
                TbC_UserNote.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_UserNote);
                TbC_HelpFileAdditionalNote.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_HelpFileAdditionalNote);
                TbC_Notes.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_Notes);
                TbC_ColumnGroups.SetTextLengthControlForGrid(WcTableColumn_Bll.STRINGSIZE_TbC_ColumnGroups);
                AssignTextLengthLabels_Custom();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Leave);
            }
        }


        #endregion Assign Text Length Labels



        #region Tile Operations

        XamTileManager _xtv = null;
        public void ConfigureXamTileView()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureXamTileView", IfxTraceCategory.Enter);
                //  For design purposes, we have each child grid layed out in a rows of a parent grid.
                //  At runtime - remove the child grids from the LayoutRoot grid, remove the LayoutRoot grid rows
                //    then create XamTileView objects and add the child grids to them.

                    _xtv = new XamTileManager();
                    ConfigureXamTileView_CustomSettings();
                LayoutRoot.Children.Remove(gdBase);
                LayoutRoot.Children.Remove(gdContraints);
                LayoutRoot.Children.Remove(gdCaptionsFormat);
                LayoutRoot.Children.Remove(gdControlType);
                LayoutRoot.Children.Remove(gdWorkflow);
                LayoutRoot.Children.Remove(gdNotesTooltips);
                XamTileView_AddItem(gdBase, "Base Info.");
                XamTileView_AddItem(gdContraints, "Constraints");
                XamTileView_AddItem(gdCaptionsFormat, "Captions and Format");
                XamTileView_AddItem(gdControlType, "Control Type and Config");
                XamTileView_AddItem(gdWorkflow, "Workflow");
                XamTileView_AddItem(gdNotesTooltips, "Notes and Tooltips");
                LayoutRoot.RowDefinitions.Remove(LayoutRoot.RowDefinitions[6]);
                LayoutRoot.RowDefinitions.Remove(LayoutRoot.RowDefinitions[5]);
                LayoutRoot.RowDefinitions.Remove(LayoutRoot.RowDefinitions[4]);
                LayoutRoot.RowDefinitions.Remove(LayoutRoot.RowDefinitions[3]);
                LayoutRoot.RowDefinitions.Remove(LayoutRoot.RowDefinitions[2]);
                LayoutRoot.RowDefinitions.Remove(LayoutRoot.RowDefinitions[1]);

                TileConstraints tc = new TileConstraints();
                _xtv.NormalModeSettings.TileConstraints = tc;
                LayoutRoot.Children.Add(_xtv);
                Grid.SetRow(_xtv, 1);
                ((XamTile)_xtv.Items[0]).IsMaximized = true;

}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureXamTileView", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_FORM_IS_LOADING = false; 
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureXamTileView", IfxTraceCategory.Leave);
            }
        }

        void XamTileView_AddItem(Grid gd, string header)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamTileView_AddItem", IfxTraceCategory.Enter);

                XamTile xt = new XamTile();
                xt.Header = header;
                Viewbox vb = new Viewbox();
                vb.Child = gd;
                vb.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                xt.Content = vb;
                _xtv.Items.Add(xt);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamTileView_AddItem", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                FLG_FORM_IS_LOADING = false; 
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamTileView_AddItem", IfxTraceCategory.Leave);
            }
        }

        #endregion Tile Operations



        public bool SecuitySettingIsReadOnly
        {
            get { return _secuitySettingIsReadOnly; }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SecuitySettingIsReadOnly - Setter", IfxTraceCategory.Enter);
                    _secuitySettingIsReadOnly = value;
                    if (_secuitySettingIsReadOnly == true)
                    {
                        gdBase_ReadOnly1.Visibility = System.Windows.Visibility.Visible;
                        gdContraints_ReadOnly1.Visibility = System.Windows.Visibility.Visible;
                        gdCaptionsFormat_ReadOnly1.Visibility = System.Windows.Visibility.Visible;
                        gdControlType_ReadOnly1.Visibility = System.Windows.Visibility.Visible;
                        gdWorkflow_ReadOnly1.Visibility = System.Windows.Visibility.Visible;
                        gdNotesTooltips_ReadOnly1.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        gdBase_ReadOnly1.Visibility = System.Windows.Visibility.Collapsed;
                        gdContraints_ReadOnly1.Visibility = System.Windows.Visibility.Collapsed;
                        gdCaptionsFormat_ReadOnly1.Visibility = System.Windows.Visibility.Collapsed;
                        gdControlType_ReadOnly1.Visibility = System.Windows.Visibility.Collapsed;
                        gdWorkflow_ReadOnly1.Visibility = System.Windows.Visibility.Collapsed;
                        gdNotesTooltips_ReadOnly1.Visibility = System.Windows.Visibility.Collapsed;
                    }
                    ReadOnlySettings_Custom();
                }
                catch (Exception ex)
                {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SecuitySettingIsReadOnly - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SecuitySettingIsReadOnly - Setter", IfxTraceCategory.Leave);
                }

            }
        }





//        #region CRUD Buttons
//
//        private void btnNew_Click(object sender, RoutedEventArgs e)
//        {
//            NewEntityRow();
//        }
//
//        private void btnUnDo_Click(object sender, RoutedEventArgs e)
//        {
//            UnDo();
//        }
//
//        private void btnSave_Click(object sender, RoutedEventArgs e)
//        {
//            Save();
//        }
//
//        private void btnDelete_Click(object sender, RoutedEventArgs e)
//        {
//            // No code for this yet.
//            //MessageBox.Show("btnDelete_Click");
//        }
//
//        #endregion CRUD Buttons
    }
}

