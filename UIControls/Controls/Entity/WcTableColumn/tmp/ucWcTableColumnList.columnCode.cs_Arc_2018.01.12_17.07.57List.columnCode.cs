using System;
using System.Windows;
using System.Windows.Controls;
using Ifx.SL;
using TypeServices;
using vControls;
using EntityBll.SL;
using Infragistics.Controls.Editors;
using vUICommon;
using EntityWireTypeSL;
using Infragistics.Controls.Grids;
using vComboDataTypes;
using System.Collections.Generic;
using vUICommon.Controls;
using UIControls.Globalization.WcTableColumn;
using UIControls.Globalization.WcToolTip;
namespace UIControls
{
    public partial class ucWcTableColumnList
    {

        
        #region GridCell Editor Events
        

        private void XamGridTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                if ((ctl.Text.Length>0) && (ctl.Text.Substring(ctl.Text.Length - 1, 1) == " "))
                {
                    return;
                }
                WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                if (obj == null) { return; }
                //switch (ctl.Name)
                //{
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vTextColumn_TextChanged(object sender, vTextColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (e.Key)
                    {
                        case "TbC_SortOrder":
                            obj.TbC_SortOrder_asString = ctl.Text;
                            break;
                        case "TbC_Name":
                            obj.TbC_Name = ctl.Text;
                            break;
                        case "TbC_Description":
                            obj.TbC_Description = ctl.Text;
                            break;
                        case "TbC_Length":
                            obj.TbC_Length_asString = ctl.Text;
                            break;
                        case "TbC_Precision":
                            obj.TbC_Precision_asString = ctl.Text;
                            break;
                        case "TbC_Scale":
                            obj.TbC_Scale_asString = ctl.Text;
                            break;
                        case "TbC_BrokenRuleText":
                            obj.TbC_BrokenRuleText = ctl.Text;
                            break;
                        case "TbC_DefaultValue":
                            obj.TbC_DefaultValue = ctl.Text;
                            break;
                        case "TbC_DefaultCaption":
                            obj.TbC_DefaultCaption = ctl.Text;
                            break;
                        case "TbC_ColumnHeaderText":
                            obj.TbC_ColumnHeaderText = ctl.Text;
                            break;
                        case "TbC_LabelCaptionVerbose":
                            obj.TbC_LabelCaptionVerbose = ctl.Text;
                            break;
                        case "TbC_TextBoxFormat":
                            obj.TbC_TextBoxFormat = ctl.Text;
                            break;
                        case "TbC_TextColumnFormat":
                            obj.TbC_TextColumnFormat = ctl.Text;
                            break;
                        case "TbC_TextBoxTextAlignment":
                            obj.TbC_TextBoxTextAlignment = ctl.Text;
                            break;
                        case "TbC_ColumnTextAlignment":
                            obj.TbC_ColumnTextAlignment = ctl.Text;
                            break;
                        case "TbC_DeveloperNote":
                            obj.TbC_DeveloperNote = ctl.Text;
                            break;
                        case "TbC_UserNote":
                            obj.TbC_UserNote = ctl.Text;
                            break;
                        case "TbC_HelpFileAdditionalNote":
                            obj.TbC_HelpFileAdditionalNote = ctl.Text;
                            break;
                        case "TbC_Notes":
                            obj.TbC_Notes = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcToolTip_Bll)
                {
                    WcToolTip_Bll obj = navList.ActiveCell.Row.Data as WcToolTip_Bll;
                    switch (e.Key)
                    {
                        case "Tt_Tip":
                            obj.Tt_Tip = ctl.Text;
                            break;
                        case "Tt_Sort":
                            obj.Tt_Sort_asString = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vDecimalColumn_TextChanged(object sender, vDecimalColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (e.Key)
                    {
                        case "TbC_ColumnWidth":
                            obj.TbC_ColumnWidth_asString = ctl.Text;
                            break;
                        case "TbC_Combo_MaxDropdownHeight":
                            obj.TbC_Combo_MaxDropdownHeight_asString = ctl.Text;
                            break;
                        case "TbC_Combo_MaxDropdownWidth":
                            obj.TbC_Combo_MaxDropdownWidth_asString = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcToolTip_Bll)
                {
                    WcToolTip_Bll obj = navList.ActiveCell.Row.Data as WcToolTip_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamComboEditor comboEditor = sender as XamComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_SelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (ctl.Name)
                    {
                        case "TbC_CtlTp_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_CtlTp_Id = null;
                            }
                            else
                            {
                                obj.TbC_CtlTp_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_DtSql_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_DtSql_Id = null;
                            }
                            else
                            {
                                obj.TbC_DtSql_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_DtNt_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_DtNt_Id = null;
                            }
                            else
                            {
                                obj.TbC_DtNt_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ListStoredProc_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ListStoredProc_Id = null;
                            }
                            else
                            {
                                obj.TbC_ListStoredProc_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ParentColumnKey":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ParentColumnKey = null;
                            }
                            else
                            {
                                obj.TbC_ParentColumnKey = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ComboListTable_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ComboListTable_Id = null;
                            }
                            else
                            {
                                obj.TbC_ComboListTable_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ComboListDisplayColumn_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ComboListDisplayColumn_Id = null;
                            }
                            else
                            {
                                obj.TbC_ComboListDisplayColumn_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcToolTip_Bll)
                {
                    WcToolTip_Bll obj = navList.ActiveCell.Row.Data as WcToolTip_Bll;
                    switch (ctl.Name)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_SelectionChanged(object sender, vXamComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                // The next 2 lines are for the Not In List feature.  If the 'Non-In-List' event is fireing, we dont want to run this event right now.
                if (ctl.SelectedItem == null) { return; }
                if (((ComboItem)ctl.SelectedItem).Id == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (e.Key)
                    {
                        case "TbC_CtlTp_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_CtlTp_Id = null;
                            }
                            else
                            {
                                obj.TbC_CtlTp_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_DtSql_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_DtSql_Id = null;
                            }
                            else
                            {
                                obj.TbC_DtSql_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_DtNt_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_DtNt_Id = null;
                            }
                            else
                            {
                                obj.TbC_DtNt_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ListStoredProc_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ListStoredProc_Id = null;
                            }
                            else
                            {
                                obj.TbC_ListStoredProc_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ParentColumnKey":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ParentColumnKey = null;
                            }
                            else
                            {
                                obj.TbC_ParentColumnKey = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ComboListTable_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ComboListTable_Id = null;
                            }
                            else
                            {
                                obj.TbC_ComboListTable_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ComboListDisplayColumn_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ComboListDisplayColumn_Id = null;
                            }
                            else
                            {
                                obj.TbC_ComboListDisplayColumn_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
                if (navList.ActiveCell.Row.Data is WcToolTip_Bll)
                {
                    WcToolTip_Bll obj = navList.ActiveCell.Row.Data as WcToolTip_Bll;
                    switch (e.Key)
                    {
                    }
                }


                vXamComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Enter);
                if (e.Key == System.Windows.Input.Key.Escape)
                {
                    WcTableColumn_Bll obj = _activeRow.Data as WcTableColumn_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_KeyUp(object sender, vXamComboColumnKeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Enter);
                if (e.XamComboColumnKeyEventArgs.Key == System.Windows.Input.Key.Escape)
                {
                    WcTableColumn_Bll obj = _activeRow.Data as WcTableColumn_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void XamMultiColumnComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamMultiColumnComboEditor comboEditor = sender as XamMultiColumnComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void vXamMultiColumnComboColumn_SelectionChanged(object sender, vXamMultiColumnComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vXamMultiColumnComboEditor ctl = sender as vXamMultiColumnComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (e.Key)
                    {
                    }
                }
                if (navList.ActiveCell.Row.Data is WcToolTip_Bll)
                {
                    WcToolTip_Bll obj = navList.ActiveCell.Row.Data as WcToolTip_Bll;
                    switch (e.Key)
                    {
                    }
                }
                vXamMultiColumnComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridDatePicker_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                if (obj == null) { return; }
                switch (ctl.Name)
                {
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Leave);
            }
        }





        private void XamColorPicker_SelectedColorChanged(object sender, SelectedColorChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamColorPicker ctl = sender as XamColorPicker;
                if (ctl == null) { return; }
                WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                if (obj == null) { return; }
                switch (((CellControl)ctl.Parent).Cell.Column.Key)
                {
                }
                XamColorPicker_SelectedColorChanged_CustomCode(ctl, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Leave);
            }
        }





        //  may not need this as the prop gets hit through binding.
        public void vDatePickerColumn_TextChanged(object sender, vDatePickerColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcToolTip_Bll)
                {
                    WcToolTip_Bll obj = navList.ActiveCell.Row.Data as WcToolTip_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                vDatePickerColumn_TextChanged_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vCheckColumn_CheckedChanged(object sender, vCheckColumnCheckedChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Enter);
                //if (_isRowInEditMode == false) { return; }
                vCheckBox ctl = sender as vCheckBox;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (e.Key)
                    {
                    case "TbC_IsNonvColumn":
                        obj.TbC_IsNonvColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsPK":
                        obj.TbC_IsPK = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsIdentity":
                        obj.TbC_IsIdentity = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsFK":
                        obj.TbC_IsFK = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsEntityColumn":
                        obj.TbC_IsEntityColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsSystemField":
                        obj.TbC_IsSystemField = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsValuesObjectMember":
                        obj.TbC_IsValuesObjectMember = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsInPropsScreen":
                        obj.TbC_IsInPropsScreen = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsInNavList":
                        obj.TbC_IsInNavList = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsRequired":
                        obj.TbC_IsRequired = (bool)ctl.IsChecked;
                        break;
                    case "TbC_AllowZero":
                        obj.TbC_AllowZero = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsNullableInDb":
                        obj.TbC_IsNullableInDb = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsNullableInUI":
                        obj.TbC_IsNullableInUI = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsReadFromDb":
                        obj.TbC_IsReadFromDb = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsSendToDb":
                        obj.TbC_IsSendToDb = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsInsertAllowed":
                        obj.TbC_IsInsertAllowed = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsEditAllowed":
                        obj.TbC_IsEditAllowed = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsReadOnlyInUI":
                        obj.TbC_IsReadOnlyInUI = (bool)ctl.IsChecked;
                        break;
                    case "TbC_UseForAudit":
                        obj.TbC_UseForAudit = (bool)ctl.IsChecked;
                        break;
                    case "TbC_LabelCaptionGenerate":
                        obj.TbC_LabelCaptionGenerate = (bool)ctl.IsChecked;
                        break;
                    case "Tbc_ShowGridColumnToolTip":
                        obj.Tbc_ShowGridColumnToolTip = (bool)ctl.IsChecked;
                        break;
                    case "Tbc_ShowPropsToolTip":
                        obj.Tbc_ShowPropsToolTip = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsCreatePropsStrings":
                        obj.TbC_IsCreatePropsStrings = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsCreateGridStrings":
                        obj.TbC_IsCreateGridStrings = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsAvailableForColumnGroups":
                        obj.TbC_IsAvailableForColumnGroups = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsTextWrapInProp":
                        obj.TbC_IsTextWrapInProp = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsTextWrapInGrid":
                        obj.TbC_IsTextWrapInGrid = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsStaticList":
                        obj.TbC_IsStaticList = (bool)ctl.IsChecked;
                        break;
                    case "TbC_UseNotInList":
                        obj.TbC_UseNotInList = (bool)ctl.IsChecked;
                        break;
                    case "TbC_UseListEditBtn":
                        obj.TbC_UseListEditBtn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_UseDisplayTextFieldProperty":
                        obj.TbC_UseDisplayTextFieldProperty = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsDisplayTextFieldProperty":
                        obj.TbC_IsDisplayTextFieldProperty = (bool)ctl.IsChecked;
                        break;
                    case "TbC_Combo_AllowDropdownResizing":
                        obj.TbC_Combo_AllowDropdownResizing = (bool)ctl.IsChecked;
                        break;
                    case "TbC_Combo_IsResetButtonVisible":
                        obj.TbC_Combo_IsResetButtonVisible = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsActiveRecColumn":
                        obj.TbC_IsActiveRecColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsDeletedColumn":
                        obj.TbC_IsDeletedColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsCreatedUserIdColumn":
                        obj.TbC_IsCreatedUserIdColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsCreatedDateColumn":
                        obj.TbC_IsCreatedDateColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsUserIdColumn":
                        obj.TbC_IsUserIdColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsModifiedDateColumn":
                        obj.TbC_IsModifiedDateColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsRowVersionStampColumn":
                        obj.TbC_IsRowVersionStampColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsBrowsable":
                        obj.TbC_IsBrowsable = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsInputComplete":
                        obj.TbC_IsInputComplete = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsCodeGen":
                        obj.TbC_IsCodeGen = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsReadyCodeGen":
                        obj.TbC_IsReadyCodeGen = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsCodeGenComplete":
                        obj.TbC_IsCodeGenComplete = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsTagForCodeGen":
                        obj.TbC_IsTagForCodeGen = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsTagForOther":
                        obj.TbC_IsTagForOther = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsActiveRow":
                        obj.TbC_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcToolTip_Bll)
                {
                    WcToolTip_Bll obj = navList.ActiveCell.Row.Data as WcToolTip_Bll;
                    switch (e.Key)
                    {
                    case "Tt_IsActiveRow":
                        obj.Tt_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Leave);
            }
        }

        #endregion GridCell Editor Events

        
        #region Grid Other Events and Event Support Methods
        

        void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Enter);
                Cell cell = (Cell)navList.ActiveCell;
                IvControlsValidation ctl = cell.Control.Content as IvControlsValidation;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;

                    switch (e.PropertyName)
                    {
                        case "ImportColumn":
                            SetGridCellValidationAppeance(ctl, obj, "ImportColumn");
                            break;
                        case "TbC_SortOrder":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_SortOrder");
                            break;
                        case "DbSortOrder":
                            SetGridCellValidationAppeance(ctl, obj, "DbSortOrder");
                            break;
                        case "TbC_Name":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_Name");
                            break;
                        case "ColumnInDatabase":
                            SetGridCellValidationAppeance(ctl, obj, "ColumnInDatabase");
                            break;
                        case "TbC_CtlTp_Id":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_CtlTp_Id");
                            break;
                        case "TbC_IsNonvColumn":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsNonvColumn");
                            break;
                        case "TbC_Description":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_Description");
                            break;
                        case "TbC_DtSql_Id":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_DtSql_Id");
                            break;
                        case "TbC_DtNt_Id":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_DtNt_Id");
                            break;
                        case "TbC_Length":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_Length");
                            break;
                        case "TbC_Precision":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_Precision");
                            break;
                        case "TbC_Scale":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_Scale");
                            break;
                        case "TbC_IsPK":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsPK");
                            break;
                        case "TbC_IsIdentity":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsIdentity");
                            break;
                        case "TbC_IsFK":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsFK");
                            break;
                        case "TbC_IsEntityColumn":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsEntityColumn");
                            break;
                        case "TbC_IsSystemField":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsSystemField");
                            break;
                        case "TbC_IsValuesObjectMember":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsValuesObjectMember");
                            break;
                        case "TbC_IsInPropsScreen":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsInPropsScreen");
                            break;
                        case "TbC_IsInNavList":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsInNavList");
                            break;
                        case "TbC_IsRequired":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsRequired");
                            break;
                        case "TbC_BrokenRuleText":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_BrokenRuleText");
                            break;
                        case "TbC_AllowZero":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_AllowZero");
                            break;
                        case "TbC_IsNullableInDb":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsNullableInDb");
                            break;
                        case "TbC_IsNullableInUI":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsNullableInUI");
                            break;
                        case "TbC_DefaultValue":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_DefaultValue");
                            break;
                        case "TbC_IsReadFromDb":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsReadFromDb");
                            break;
                        case "TbC_IsSendToDb":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsSendToDb");
                            break;
                        case "TbC_IsInsertAllowed":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsInsertAllowed");
                            break;
                        case "TbC_IsEditAllowed":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsEditAllowed");
                            break;
                        case "TbC_IsReadOnlyInUI":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsReadOnlyInUI");
                            break;
                        case "TbC_UseForAudit":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_UseForAudit");
                            break;
                        case "TbC_DefaultCaption":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_DefaultCaption");
                            break;
                        case "TbC_ColumnHeaderText":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_ColumnHeaderText");
                            break;
                        case "TbC_LabelCaptionVerbose":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_LabelCaptionVerbose");
                            break;
                        case "TbC_LabelCaptionGenerate":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_LabelCaptionGenerate");
                            break;
                        case "Tbc_ShowGridColumnToolTip":
                            SetGridCellValidationAppeance(ctl, obj, "Tbc_ShowGridColumnToolTip");
                            break;
                        case "Tbc_ShowPropsToolTip":
                            SetGridCellValidationAppeance(ctl, obj, "Tbc_ShowPropsToolTip");
                            break;
                        case "Tbc_TooltipsRolledUp":
                            SetGridCellValidationAppeance(ctl, obj, "Tbc_TooltipsRolledUp");
                            break;
                        case "TbC_IsCreatePropsStrings":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsCreatePropsStrings");
                            break;
                        case "TbC_IsCreateGridStrings":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsCreateGridStrings");
                            break;
                        case "TbC_IsAvailableForColumnGroups":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsAvailableForColumnGroups");
                            break;
                        case "TbC_IsTextWrapInProp":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsTextWrapInProp");
                            break;
                        case "TbC_IsTextWrapInGrid":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsTextWrapInGrid");
                            break;
                        case "TbC_TextBoxFormat":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_TextBoxFormat");
                            break;
                        case "TbC_TextColumnFormat":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_TextColumnFormat");
                            break;
                        case "TbC_ColumnWidth":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_ColumnWidth");
                            break;
                        case "TbC_TextBoxTextAlignment":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_TextBoxTextAlignment");
                            break;
                        case "TbC_ColumnTextAlignment":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_ColumnTextAlignment");
                            break;
                        case "TbC_ListStoredProc_Id":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_ListStoredProc_Id");
                            break;
                        case "TbC_IsStaticList":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsStaticList");
                            break;
                        case "TbC_UseNotInList":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_UseNotInList");
                            break;
                        case "TbC_UseListEditBtn":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_UseListEditBtn");
                            break;
                        case "TbC_ParentColumnKey":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_ParentColumnKey");
                            break;
                        case "TbC_UseDisplayTextFieldProperty":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_UseDisplayTextFieldProperty");
                            break;
                        case "TbC_IsDisplayTextFieldProperty":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsDisplayTextFieldProperty");
                            break;
                        case "TbC_ComboListTable_Id":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_ComboListTable_Id");
                            break;
                        case "TbC_ComboListDisplayColumn_Id":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_ComboListDisplayColumn_Id");
                            break;
                        case "TbC_Combo_MaxDropdownHeight":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_Combo_MaxDropdownHeight");
                            break;
                        case "TbC_Combo_MaxDropdownWidth":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_Combo_MaxDropdownWidth");
                            break;
                        case "TbC_Combo_AllowDropdownResizing":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_Combo_AllowDropdownResizing");
                            break;
                        case "TbC_Combo_IsResetButtonVisible":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_Combo_IsResetButtonVisible");
                            break;
                        case "TbC_IsActiveRecColumn":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsActiveRecColumn");
                            break;
                        case "TbC_IsDeletedColumn":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsDeletedColumn");
                            break;
                        case "TbC_IsCreatedUserIdColumn":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsCreatedUserIdColumn");
                            break;
                        case "TbC_IsCreatedDateColumn":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsCreatedDateColumn");
                            break;
                        case "TbC_IsUserIdColumn":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsUserIdColumn");
                            break;
                        case "TbC_IsModifiedDateColumn":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsModifiedDateColumn");
                            break;
                        case "TbC_IsRowVersionStampColumn":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsRowVersionStampColumn");
                            break;
                        case "TbC_IsBrowsable":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsBrowsable");
                            break;
                        case "TbC_DeveloperNote":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_DeveloperNote");
                            break;
                        case "TbC_UserNote":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_UserNote");
                            break;
                        case "TbC_HelpFileAdditionalNote":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_HelpFileAdditionalNote");
                            break;
                        case "TbC_Notes":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_Notes");
                            break;
                        case "TbC_IsInputComplete":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsInputComplete");
                            break;
                        case "TbC_IsCodeGen":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsCodeGen");
                            break;
                        case "TbC_IsReadyCodeGen":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsReadyCodeGen");
                            break;
                        case "TbC_IsCodeGenComplete":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsCodeGenComplete");
                            break;
                        case "TbC_IsTagForCodeGen":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsTagForCodeGen");
                            break;
                        case "TbC_IsTagForOther":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsTagForOther");
                            break;
                        case "TbC_ColumnGroups":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_ColumnGroups");
                            break;
                        case "TbC_IsActiveRow":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_IsActiveRow");
                            break;
                        case "UserName":
                            SetGridCellValidationAppeance(ctl, obj, "UserName");
                            break;
                        case "TbC_LastModifiedDate":
                            SetGridCellValidationAppeance(ctl, obj, "TbC_LastModifiedDate");
                            break;
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcToolTip_Bll)
                {
                    WcToolTip_Bll obj = navList.ActiveCell.Row.Data as WcToolTip_Bll;
                    switch (e.PropertyName)
                    {
                    case "Tt_Tip":
                        SetGridCellValidationAppeance(ctl, obj, "Tt_Tip");
                        break;
                    case "Tt_Sort":
                        SetGridCellValidationAppeance(ctl, obj, "Tt_Sort");
                        break;
                    case "Tt_IsActiveRow":
                        SetGridCellValidationAppeance(ctl, obj, "Tt_IsActiveRow");
                        break;
                    case "Tt_LastModifiedDate":
                        SetGridCellValidationAppeance(ctl, obj, "Tt_LastModifiedDate");
                        break;
                    case "UserName":
                        SetGridCellValidationAppeance(ctl, obj, "UserName");
                        break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Leave);
            }
        }

        #endregion Grid Other Events and Event Support Methods

        
        #region Methods


        void AssignTextLengthLabels()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Enter);

                ((vTextColumn)navList.Columns["TbC_Name"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["ColumnInDatabase"]).MaxTextLength = 128;
                ((vTextColumn)navList.Columns["TbC_Description"]).MaxTextLength = 1000;
                ((vTextColumn)navList.Columns["TbC_BrokenRuleText"]).MaxTextLength = 255;
                ((vTextColumn)navList.Columns["TbC_DefaultValue"]).MaxTextLength = 1000;
                ((vTextColumn)navList.Columns["TbC_DefaultCaption"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["TbC_ColumnHeaderText"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["TbC_LabelCaptionVerbose"]).MaxTextLength = 250;
                ((vTextColumn)navList.Columns["Tbc_TooltipsRolledUp"]).MaxTextLength = 4000;
                ((vTextColumn)navList.Columns["TbC_TextBoxFormat"]).MaxTextLength = 50;
                ((vTextColumn)navList.Columns["TbC_TextColumnFormat"]).MaxTextLength = 50;
                ((vTextColumn)navList.Columns["TbC_TextBoxTextAlignment"]).MaxTextLength = 10;
                ((vTextColumn)navList.Columns["TbC_ColumnTextAlignment"]).MaxTextLength = 10;
                ((vTextColumn)navList.Columns["TbC_DeveloperNote"]).MaxTextLength = 1000;
                ((vTextColumn)navList.Columns["TbC_UserNote"]).MaxTextLength = 1000;
                ((vTextColumn)navList.Columns["TbC_HelpFileAdditionalNote"]).MaxTextLength = 1000;
                ((vTextColumn)navList.Columns["TbC_Notes"]).MaxTextLength = 1000;
                ((vTextColumn)navList.Columns["TbC_ColumnGroups"]).MaxTextLength = 500;
                ((vTextColumn)navList.Columns["UserName"]).MaxTextLength = 60;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcToolTip"]).Columns["Tt_Tip"]).MaxTextLength = 1000;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcToolTip"]).Columns["UserName"]).MaxTextLength = 60;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Leave);
            }
        }



        void ConfigureColumnHeaderTooltips()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Enter);

                    // ImportColumn
                    ((IvColumn)navList.Columns["ImportColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.ImportColumn_Vbs; 
                    ((IvColumn)navList.Columns["ImportColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.ImportColumn_1};
                    ((IvColumn)navList.Columns["ImportColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // DbSortOrder
                    ((IvColumn)navList.Columns["DbSortOrder"]).HeaderToolTipCaption = StringsWcTableColumnList.DbSortOrder_Vbs; 
                    ((IvColumn)navList.Columns["DbSortOrder"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.DbSortOrder_1};
                    ((IvColumn)navList.Columns["DbSortOrder"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Name
                    ((IvColumn)navList.Columns["TbC_Name"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Name_Vbs; 
                    ((IvColumn)navList.Columns["TbC_Name"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Name_1};
                    ((IvColumn)navList.Columns["TbC_Name"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ColumnInDatabase
                    ((IvColumn)navList.Columns["ColumnInDatabase"]).HeaderToolTipCaption = StringsWcTableColumnList.ColumnInDatabase_Vbs; 
                    ((IvColumn)navList.Columns["ColumnInDatabase"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.ColumnInDatabase_1, StringsWcTableColumnListTooltips.ColumnInDatabase_2};
                    ((IvColumn)navList.Columns["ColumnInDatabase"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_CtlTp_Id
                    ((vComboColumnBase)navList.Columns["TbC_CtlTp_Id"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_CtlTp_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["TbC_CtlTp_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsNonvColumn
                    ((IvColumn)navList.Columns["TbC_IsNonvColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsNonvColumn_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsNonvColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsNonvColumn_1, StringsWcTableColumnListTooltips.TbC_IsNonvColumn_2};
                    ((IvColumn)navList.Columns["TbC_IsNonvColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_DtSql_Id
                    ((vComboColumnBase)navList.Columns["TbC_DtSql_Id"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_DtSql_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["TbC_DtSql_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_DtNt_Id
                    ((vComboColumnBase)navList.Columns["TbC_DtNt_Id"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_DtNt_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["TbC_DtNt_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Length
                    ((IvColumn)navList.Columns["TbC_Length"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Length_Vbs; 
                    ((IvColumn)navList.Columns["TbC_Length"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Precision
                    ((IvColumn)navList.Columns["TbC_Precision"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Precision_Vbs; 
                    ((IvColumn)navList.Columns["TbC_Precision"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Precision_1};
                    ((IvColumn)navList.Columns["TbC_Precision"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Scale
                    ((IvColumn)navList.Columns["TbC_Scale"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Scale_Vbs; 
                    ((IvColumn)navList.Columns["TbC_Scale"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Scale_1, StringsWcTableColumnListTooltips.TbC_Scale_2};
                    ((IvColumn)navList.Columns["TbC_Scale"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsPK
                    ((IvColumn)navList.Columns["TbC_IsPK"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsPK_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsPK"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsIdentity
                    ((IvColumn)navList.Columns["TbC_IsIdentity"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsIdentity_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsIdentity"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsIdentity_1};
                    ((IvColumn)navList.Columns["TbC_IsIdentity"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsFK
                    ((IvColumn)navList.Columns["TbC_IsFK"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsFK_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsFK"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsEntityColumn
                    ((IvColumn)navList.Columns["TbC_IsEntityColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsEntityColumn_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsEntityColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsEntityColumn_1};
                    ((IvColumn)navList.Columns["TbC_IsEntityColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsSystemField
                    ((IvColumn)navList.Columns["TbC_IsSystemField"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsSystemField_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsSystemField"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsSystemField_1, StringsWcTableColumnListTooltips.TbC_IsSystemField_2};
                    ((IvColumn)navList.Columns["TbC_IsSystemField"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsValuesObjectMember
                    ((IvColumn)navList.Columns["TbC_IsValuesObjectMember"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsValuesObjectMember_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsValuesObjectMember"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsValuesObjectMember_1};
                    ((IvColumn)navList.Columns["TbC_IsValuesObjectMember"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsInPropsScreen
                    ((IvColumn)navList.Columns["TbC_IsInPropsScreen"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsInPropsScreen_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsInPropsScreen"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsInPropsScreen_1};
                    ((IvColumn)navList.Columns["TbC_IsInPropsScreen"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsInNavList
                    ((IvColumn)navList.Columns["TbC_IsInNavList"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsInNavList_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsInNavList"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsRequired
                    ((IvColumn)navList.Columns["TbC_IsRequired"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsRequired_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsRequired"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_AllowZero
                    ((IvColumn)navList.Columns["TbC_AllowZero"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_AllowZero_Vbs; 
                    ((IvColumn)navList.Columns["TbC_AllowZero"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsNullableInDb
                    ((IvColumn)navList.Columns["TbC_IsNullableInDb"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsNullableInDb_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsNullableInDb"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsNullableInUI
                    ((IvColumn)navList.Columns["TbC_IsNullableInUI"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsNullableInUI_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsNullableInUI"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_DefaultValue
                    ((IvColumn)navList.Columns["TbC_DefaultValue"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_DefaultValue_Vbs; 
                    ((IvColumn)navList.Columns["TbC_DefaultValue"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsReadFromDb
                    ((IvColumn)navList.Columns["TbC_IsReadFromDb"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsReadFromDb_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsReadFromDb"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsReadFromDb_1};
                    ((IvColumn)navList.Columns["TbC_IsReadFromDb"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsSendToDb
                    ((IvColumn)navList.Columns["TbC_IsSendToDb"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsSendToDb_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsSendToDb"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsSendToDb_1};
                    ((IvColumn)navList.Columns["TbC_IsSendToDb"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsInsertAllowed
                    ((IvColumn)navList.Columns["TbC_IsInsertAllowed"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsInsertAllowed_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsInsertAllowed"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsInsertAllowed_1};
                    ((IvColumn)navList.Columns["TbC_IsInsertAllowed"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsEditAllowed
                    ((IvColumn)navList.Columns["TbC_IsEditAllowed"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsEditAllowed_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsEditAllowed"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsEditAllowed_1, StringsWcTableColumnListTooltips.TbC_IsEditAllowed_2};
                    ((IvColumn)navList.Columns["TbC_IsEditAllowed"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsReadOnlyInUI
                    ((IvColumn)navList.Columns["TbC_IsReadOnlyInUI"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsReadOnlyInUI_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsReadOnlyInUI"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsReadOnlyInUI_1, StringsWcTableColumnListTooltips.TbC_IsReadOnlyInUI_2};
                    ((IvColumn)navList.Columns["TbC_IsReadOnlyInUI"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_UseForAudit
                    ((IvColumn)navList.Columns["TbC_UseForAudit"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_UseForAudit_Vbs; 
                    ((IvColumn)navList.Columns["TbC_UseForAudit"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_UseForAudit_1};
                    ((IvColumn)navList.Columns["TbC_UseForAudit"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_DefaultCaption
                    ((IvColumn)navList.Columns["TbC_DefaultCaption"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_DefaultCaption_Vbs; 
                    ((IvColumn)navList.Columns["TbC_DefaultCaption"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_DefaultCaption_1};
                    ((IvColumn)navList.Columns["TbC_DefaultCaption"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ColumnHeaderText
                    ((IvColumn)navList.Columns["TbC_ColumnHeaderText"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ColumnHeaderText_Vbs; 
                    ((IvColumn)navList.Columns["TbC_ColumnHeaderText"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_ColumnHeaderText_1};
                    ((IvColumn)navList.Columns["TbC_ColumnHeaderText"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_LabelCaptionVerbose
                    ((IvColumn)navList.Columns["TbC_LabelCaptionVerbose"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_LabelCaptionVerbose_Vbs; 
                    ((IvColumn)navList.Columns["TbC_LabelCaptionVerbose"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_LabelCaptionVerbose_1};
                    ((IvColumn)navList.Columns["TbC_LabelCaptionVerbose"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_LabelCaptionGenerate
                    ((IvColumn)navList.Columns["TbC_LabelCaptionGenerate"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_LabelCaptionGenerate_Vbs; 
                    ((IvColumn)navList.Columns["TbC_LabelCaptionGenerate"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_LabelCaptionGenerate_1, StringsWcTableColumnListTooltips.TbC_LabelCaptionGenerate_2};
                    ((IvColumn)navList.Columns["TbC_LabelCaptionGenerate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tbc_ShowGridColumnToolTip
                    ((IvColumn)navList.Columns["Tbc_ShowGridColumnToolTip"]).HeaderToolTipCaption = StringsWcTableColumnList.Tbc_ShowGridColumnToolTip_Vbs; 
                    ((IvColumn)navList.Columns["Tbc_ShowGridColumnToolTip"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.Tbc_ShowGridColumnToolTip_1};
                    ((IvColumn)navList.Columns["Tbc_ShowGridColumnToolTip"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tbc_ShowPropsToolTip
                    ((IvColumn)navList.Columns["Tbc_ShowPropsToolTip"]).HeaderToolTipCaption = StringsWcTableColumnList.Tbc_ShowPropsToolTip_Vbs; 
                    ((IvColumn)navList.Columns["Tbc_ShowPropsToolTip"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsCreatePropsStrings
                    ((IvColumn)navList.Columns["TbC_IsCreatePropsStrings"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsCreatePropsStrings_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsCreatePropsStrings"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsCreatePropsStrings_1, StringsWcTableColumnListTooltips.TbC_IsCreatePropsStrings_2};
                    ((IvColumn)navList.Columns["TbC_IsCreatePropsStrings"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsCreateGridStrings
                    ((IvColumn)navList.Columns["TbC_IsCreateGridStrings"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsCreateGridStrings_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsCreateGridStrings"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsCreateGridStrings_1, StringsWcTableColumnListTooltips.TbC_IsCreateGridStrings_2};
                    ((IvColumn)navList.Columns["TbC_IsCreateGridStrings"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsAvailableForColumnGroups
                    ((IvColumn)navList.Columns["TbC_IsAvailableForColumnGroups"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsAvailableForColumnGroups_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsAvailableForColumnGroups"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsAvailableForColumnGroups_1};
                    ((IvColumn)navList.Columns["TbC_IsAvailableForColumnGroups"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsTextWrapInProp
                    ((IvColumn)navList.Columns["TbC_IsTextWrapInProp"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsTextWrapInProp_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsTextWrapInProp"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_TextColumnFormat
                    ((IvColumn)navList.Columns["TbC_TextColumnFormat"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_TextColumnFormat_Vbs; 
                    ((IvColumn)navList.Columns["TbC_TextColumnFormat"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ColumnWidth
                    ((IvColumn)navList.Columns["TbC_ColumnWidth"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ColumnWidth_Vbs; 
                    ((IvColumn)navList.Columns["TbC_ColumnWidth"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_ColumnWidth_1};
                    ((IvColumn)navList.Columns["TbC_ColumnWidth"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_TextBoxTextAlignment
                    ((IvColumn)navList.Columns["TbC_TextBoxTextAlignment"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_TextBoxTextAlignment_Vbs; 
                    ((IvColumn)navList.Columns["TbC_TextBoxTextAlignment"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ColumnTextAlignment
                    ((IvColumn)navList.Columns["TbC_ColumnTextAlignment"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ColumnTextAlignment_Vbs; 
                    ((IvColumn)navList.Columns["TbC_ColumnTextAlignment"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ListStoredProc_Id
                    ((vComboColumnBase)navList.Columns["TbC_ListStoredProc_Id"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ListStoredProc_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["TbC_ListStoredProc_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_ListStoredProc_Id_1};
                    ((vComboColumnBase)navList.Columns["TbC_ListStoredProc_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsStaticList
                    ((IvColumn)navList.Columns["TbC_IsStaticList"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsStaticList_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsStaticList"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsStaticList_1};
                    ((IvColumn)navList.Columns["TbC_IsStaticList"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_UseNotInList
                    ((IvColumn)navList.Columns["TbC_UseNotInList"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_UseNotInList_Vbs; 
                    ((IvColumn)navList.Columns["TbC_UseNotInList"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_UseNotInList_1, StringsWcTableColumnListTooltips.TbC_UseNotInList_2};
                    ((IvColumn)navList.Columns["TbC_UseNotInList"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_UseListEditBtn
                    ((IvColumn)navList.Columns["TbC_UseListEditBtn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_UseListEditBtn_Vbs; 
                    ((IvColumn)navList.Columns["TbC_UseListEditBtn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_UseListEditBtn_1};
                    ((IvColumn)navList.Columns["TbC_UseListEditBtn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ParentColumnKey
                    ((vComboColumnBase)navList.Columns["TbC_ParentColumnKey"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ParentColumnKey_Vbs; 
                    ((vComboColumnBase)navList.Columns["TbC_ParentColumnKey"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_ParentColumnKey_1, StringsWcTableColumnListTooltips.TbC_ParentColumnKey_2, StringsWcTableColumnListTooltips.TbC_ParentColumnKey_3};
                    ((vComboColumnBase)navList.Columns["TbC_ParentColumnKey"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_UseDisplayTextFieldProperty
                    ((IvColumn)navList.Columns["TbC_UseDisplayTextFieldProperty"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_UseDisplayTextFieldProperty_Vbs; 
                    ((IvColumn)navList.Columns["TbC_UseDisplayTextFieldProperty"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_UseDisplayTextFieldProperty_1, StringsWcTableColumnListTooltips.TbC_UseDisplayTextFieldProperty_2};
                    ((IvColumn)navList.Columns["TbC_UseDisplayTextFieldProperty"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsDisplayTextFieldProperty
                    ((IvColumn)navList.Columns["TbC_IsDisplayTextFieldProperty"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsDisplayTextFieldProperty_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsDisplayTextFieldProperty"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsDisplayTextFieldProperty_1};
                    ((IvColumn)navList.Columns["TbC_IsDisplayTextFieldProperty"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ComboListTable_Id
                    ((vComboColumnBase)navList.Columns["TbC_ComboListTable_Id"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ComboListTable_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["TbC_ComboListTable_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_ComboListTable_Id_1, StringsWcTableColumnListTooltips.TbC_ComboListTable_Id_2, StringsWcTableColumnListTooltips.TbC_ComboListTable_Id_3};
                    ((vComboColumnBase)navList.Columns["TbC_ComboListTable_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ComboListDisplayColumn_Id
                    ((vComboColumnBase)navList.Columns["TbC_ComboListDisplayColumn_Id"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ComboListDisplayColumn_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["TbC_ComboListDisplayColumn_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_ComboListDisplayColumn_Id_1, StringsWcTableColumnListTooltips.TbC_ComboListDisplayColumn_Id_2};
                    ((vComboColumnBase)navList.Columns["TbC_ComboListDisplayColumn_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Combo_MaxDropdownHeight
                    ((IvColumn)navList.Columns["TbC_Combo_MaxDropdownHeight"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Combo_MaxDropdownHeight_Vbs; 
                    ((IvColumn)navList.Columns["TbC_Combo_MaxDropdownHeight"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Combo_MaxDropdownHeight_1, StringsWcTableColumnListTooltips.TbC_Combo_MaxDropdownHeight_2};
                    ((IvColumn)navList.Columns["TbC_Combo_MaxDropdownHeight"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Combo_MaxDropdownWidth
                    ((IvColumn)navList.Columns["TbC_Combo_MaxDropdownWidth"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Combo_MaxDropdownWidth_Vbs; 
                    ((IvColumn)navList.Columns["TbC_Combo_MaxDropdownWidth"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Combo_MaxDropdownWidth_1, StringsWcTableColumnListTooltips.TbC_Combo_MaxDropdownWidth_2};
                    ((IvColumn)navList.Columns["TbC_Combo_MaxDropdownWidth"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Combo_AllowDropdownResizing
                    ((IvColumn)navList.Columns["TbC_Combo_AllowDropdownResizing"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Combo_AllowDropdownResizing_Vbs; 
                    ((IvColumn)navList.Columns["TbC_Combo_AllowDropdownResizing"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Combo_AllowDropdownResizing_1};
                    ((IvColumn)navList.Columns["TbC_Combo_AllowDropdownResizing"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Combo_IsResetButtonVisible
                    ((IvColumn)navList.Columns["TbC_Combo_IsResetButtonVisible"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Combo_IsResetButtonVisible_Vbs; 
                    ((IvColumn)navList.Columns["TbC_Combo_IsResetButtonVisible"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Combo_IsResetButtonVisible_1, StringsWcTableColumnListTooltips.TbC_Combo_IsResetButtonVisible_2};
                    ((IvColumn)navList.Columns["TbC_Combo_IsResetButtonVisible"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsActiveRecColumn
                    ((IvColumn)navList.Columns["TbC_IsActiveRecColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsActiveRecColumn_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsActiveRecColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsActiveRecColumn_1};
                    ((IvColumn)navList.Columns["TbC_IsActiveRecColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsDeletedColumn
                    ((IvColumn)navList.Columns["TbC_IsDeletedColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsDeletedColumn_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsDeletedColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsDeletedColumn_1, StringsWcTableColumnListTooltips.TbC_IsDeletedColumn_2};
                    ((IvColumn)navList.Columns["TbC_IsDeletedColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsCreatedUserIdColumn
                    ((IvColumn)navList.Columns["TbC_IsCreatedUserIdColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsCreatedUserIdColumn_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsCreatedUserIdColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsCreatedUserIdColumn_1, StringsWcTableColumnListTooltips.TbC_IsCreatedUserIdColumn_2};
                    ((IvColumn)navList.Columns["TbC_IsCreatedUserIdColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsCreatedDateColumn
                    ((IvColumn)navList.Columns["TbC_IsCreatedDateColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsCreatedDateColumn_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsCreatedDateColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsCreatedDateColumn_1, StringsWcTableColumnListTooltips.TbC_IsCreatedDateColumn_2};
                    ((IvColumn)navList.Columns["TbC_IsCreatedDateColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsUserIdColumn
                    ((IvColumn)navList.Columns["TbC_IsUserIdColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsUserIdColumn_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsUserIdColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsUserIdColumn_1, StringsWcTableColumnListTooltips.TbC_IsUserIdColumn_2};
                    ((IvColumn)navList.Columns["TbC_IsUserIdColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsModifiedDateColumn
                    ((IvColumn)navList.Columns["TbC_IsModifiedDateColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsModifiedDateColumn_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsModifiedDateColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsModifiedDateColumn_1, StringsWcTableColumnListTooltips.TbC_IsModifiedDateColumn_2};
                    ((IvColumn)navList.Columns["TbC_IsModifiedDateColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsRowVersionStampColumn
                    ((IvColumn)navList.Columns["TbC_IsRowVersionStampColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsRowVersionStampColumn_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsRowVersionStampColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsRowVersionStampColumn_1};
                    ((IvColumn)navList.Columns["TbC_IsRowVersionStampColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsBrowsable
                    ((IvColumn)navList.Columns["TbC_IsBrowsable"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsBrowsable_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsBrowsable"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsBrowsable_1, StringsWcTableColumnListTooltips.TbC_IsBrowsable_2, StringsWcTableColumnListTooltips.TbC_IsBrowsable_3};
                    ((IvColumn)navList.Columns["TbC_IsBrowsable"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_DeveloperNote
                    ((IvColumn)navList.Columns["TbC_DeveloperNote"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_DeveloperNote_Vbs; 
                    ((IvColumn)navList.Columns["TbC_DeveloperNote"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_DeveloperNote_1, StringsWcTableColumnListTooltips.TbC_DeveloperNote_2};
                    ((IvColumn)navList.Columns["TbC_DeveloperNote"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_UserNote
                    ((IvColumn)navList.Columns["TbC_UserNote"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_UserNote_Vbs; 
                    ((IvColumn)navList.Columns["TbC_UserNote"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_UserNote_1, StringsWcTableColumnListTooltips.TbC_UserNote_2, StringsWcTableColumnListTooltips.TbC_UserNote_3};
                    ((IvColumn)navList.Columns["TbC_UserNote"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_HelpFileAdditionalNote
                    ((IvColumn)navList.Columns["TbC_HelpFileAdditionalNote"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_HelpFileAdditionalNote_Vbs; 
                    ((IvColumn)navList.Columns["TbC_HelpFileAdditionalNote"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_HelpFileAdditionalNote_1};
                    ((IvColumn)navList.Columns["TbC_HelpFileAdditionalNote"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Notes
                    ((IvColumn)navList.Columns["TbC_Notes"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Notes_Vbs; 
                    ((IvColumn)navList.Columns["TbC_Notes"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Notes_1};
                    ((IvColumn)navList.Columns["TbC_Notes"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsInputComplete
                    ((IvColumn)navList.Columns["TbC_IsInputComplete"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsInputComplete_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsInputComplete"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsInputComplete_1};
                    ((IvColumn)navList.Columns["TbC_IsInputComplete"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsCodeGen
                    ((IvColumn)navList.Columns["TbC_IsCodeGen"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsCodeGen_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsCodeGen"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsCodeGen_1};
                    ((IvColumn)navList.Columns["TbC_IsCodeGen"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsReadyCodeGen
                    ((IvColumn)navList.Columns["TbC_IsReadyCodeGen"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsReadyCodeGen_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsReadyCodeGen"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsReadyCodeGen_1};
                    ((IvColumn)navList.Columns["TbC_IsReadyCodeGen"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsCodeGenComplete
                    ((IvColumn)navList.Columns["TbC_IsCodeGenComplete"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsCodeGenComplete_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsCodeGenComplete"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsCodeGenComplete_1, StringsWcTableColumnListTooltips.TbC_IsCodeGenComplete_2};
                    ((IvColumn)navList.Columns["TbC_IsCodeGenComplete"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsActiveRow
                    ((IvColumn)navList.Columns["TbC_IsActiveRow"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsActiveRow_Vbs; 
                    ((IvColumn)navList.Columns["TbC_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsActiveRow_1, StringsWcTableColumnListTooltips.TbC_IsActiveRow_2};
                    ((IvColumn)navList.Columns["TbC_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)navList.Columns["UserName"]).HeaderToolTipCaption = StringsWcTableColumnList.UserName_Vbs; 
                    ((IvColumn)navList.Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.UserName_1};
                    ((IvColumn)navList.Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_LastModifiedDate
                    ((IvColumn)navList.Columns["TbC_LastModifiedDate"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_LastModifiedDate_Vbs; 
                    ((IvColumn)navList.Columns["TbC_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_LastModifiedDate_1};
                    ((IvColumn)navList.Columns["TbC_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tt_IsActiveRow
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcToolTip"]).Columns["Tt_IsActiveRow"]).HeaderToolTipCaption = StringsWcToolTipList.Tt_IsActiveRow_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcToolTip"]).Columns["Tt_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcToolTipListTooltips.Tt_IsActiveRow_1, StringsWcToolTipListTooltips.Tt_IsActiveRow_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcToolTip"]).Columns["Tt_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcToolTip"]).Columns["UserName"]).HeaderToolTipCaption = StringsWcToolTipList.UserName_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcToolTip"]).Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsWcToolTipListTooltips.UserName_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcToolTip"]).Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tt_LastModifiedDate
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcToolTip"]).Columns["Tt_LastModifiedDate"]).HeaderToolTipCaption = StringsWcToolTipList.Tt_LastModifiedDate_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcToolTip"]).Columns["Tt_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsWcToolTipListTooltips.Tt_LastModifiedDate_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcToolTip"]).Columns["Tt_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();
                ConfigureColumnHeaderTooltips_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Leave);
            }
        }


        void Set_vXamComboColumn_ItemSources()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["TbC_CtlTp_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcControlType_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["TbC_CtlTp_Id"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcControlType_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)navList.Columns["TbC_DtSql_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["TbC_DtSql_Id"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)navList.Columns["TbC_DtNt_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["TbC_DtNt_Id"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)navList.Columns["TbC_ListStoredProc_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcStoredProc_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["TbC_ListStoredProc_Id"]).DisplayMemberPath = "ItemName";
                WcApplicationVersion_Bll_staticLists.WcStoredProc_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)navList.Columns["TbC_ParentColumnKey"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["TbC_ParentColumnKey"]).DisplayMemberPath = "ItemName";
                WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)navList.Columns["TbC_ComboListTable_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTable_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["TbC_ComboListTable_Id"]).DisplayMemberPath = "ItemName";
                WcApplicationVersion_Bll_staticLists.WcTable_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTable_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)navList.Columns["TbC_ComboListDisplayColumn_Id"]).ItemsSource = new ComboItemList(WcTable_Bll_staticLists.WcTableColumn_lstByTable_ComboItemList_BindingListProperty, ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);
                ((vXamComboColumn)navList.Columns["TbC_ComboListDisplayColumn_Id"]).DisplayMemberPath = "ItemName";
                WcTable_Bll_staticLists.WcTableColumn_lstByTable_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByTable_ComboItemList_BindingListProperty_DataSourceUpdated);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Leave);
            }
        }

        void WcTableColumn_lstByTable_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["TbC_ComboListDisplayColumn_Id"]).ItemsSource = WcTable_Bll_staticLists.WcTableColumn_lstByTable_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByTable_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcTable_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["TbC_ComboListTable_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTable_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["TbC_CtlTp_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcControlType_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["TbC_DtNt_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["TbC_DtSql_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["TbC_ListStoredProc_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcStoredProc_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["TbC_ParentColumnKey"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        #region Combo ItemsSource for Non-Static Lists

        #endregion region Combo ItemsSource for Non-Static Lists

        void SetNewRowValidation()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Enter);
                if (_activeRow == null) { return; }
                WcTableColumn_Bll obj = _activeRow.Data as WcTableColumn_Bll;
                if (obj == null) { return; }
                // Note:  The lines to set the validaton styles below are not needed as the styles are being set from converters in the xaml style propeties, but they are left here incase needed in the future.  this is new code and we are unsure how it will evolve.
                // "TbC_ColumnWidth"
                if (_activeRow.Cells["TbC_ColumnWidth"].Control != null && _activeRow.Cells["TbC_ColumnWidth"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["TbC_ColumnWidth"].Control.Content), obj, "TbC_ColumnWidth");
                }

                // "TbC_Combo_MaxDropdownHeight"
                if (_activeRow.Cells["TbC_Combo_MaxDropdownHeight"].Control != null && _activeRow.Cells["TbC_Combo_MaxDropdownHeight"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["TbC_Combo_MaxDropdownHeight"].Control.Content), obj, "TbC_Combo_MaxDropdownHeight");
                }

                // "TbC_Combo_MaxDropdownWidth"
                if (_activeRow.Cells["TbC_Combo_MaxDropdownWidth"].Control != null && _activeRow.Cells["TbC_Combo_MaxDropdownWidth"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["TbC_Combo_MaxDropdownWidth"].Control.Content), obj, "TbC_Combo_MaxDropdownWidth");
                }

                WcToolTip_Bll oWcToolTip = _activeRow.Data as WcToolTip_Bll;
                if (oWcToolTip == null) { return; }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Leave);
            }
        }

        #endregion Methods


    }
}
