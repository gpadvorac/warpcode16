using System;
using System.Text;
using System.Windows;
using Ifx.SL;
using EntityBll.SL;
using TypeServices;
using Infragistics.Controls.Grids;
using vUICommon;
using Velocity.SL;
using vControls;
using Infragistics.Controls.Editors;
using System.Windows.Controls;
using System.Windows.Input;
using Infragistics.Controls.Grids.Primitives;
using Infragistics.Controls.Menus;

// Gen Timestamp:  12/19/2017 5:10:14 PM

namespace UIControls
{
    public partial class ucWcTableColumnList
    {

        #region Initialize Variables

        bool _mnuChangeFilterType_AllowVisibility = true;
        bool _mnuClearFilters_AllowVisibility = true;  
        bool _mnuGridTools_AllowVisibility = true;
        bool _mnuRichGrid_AllowVisibility = true;
        bool _mnuSplitScreen_AllowVisibility = true;
        bool _isAllowNewRow = true;
        bool _mnuExcelExport_AllowVisibility = true;
        bool _gridDataSourceCombo_AllowVisibility = true;
        bool _menuGridRow_AllowVisibility = true;  // the area where the grid menu is located


        //ProxyWrapper.WcTableColumnService_ProxyWrapper _wcTableColumnProxy = null;

        #endregion Initialize Variables


        #region Constructor

        void CustomConstructionCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", IfxTraceCategory.Enter);

                _wcTableColumnProxy = new ProxyWrapper.WcTableColumnService_ProxyWrapper();
                _wcTableColumnProxy.WcTableColumn_GetByIdCompleted += WcTableColumn_GetByIdCompleted;
                _wcTableColumnProxy.WcTableColumn_GetListByFKCompleted += WcTableColumn_GetListByFKCompleted;
                _wcTableColumnProxy.WcTableColumn_GetAllCompleted += WcTableColumn_GetAllCompleted;

                _wcTableColumnProxy.GetWcTableColumn_lstNamesFromAppAndDBCompleted += _wcTableColumnProxy_GetWcTableColumn_lstNamesFromAppAndDBCompleted;
                _wcTableColumnProxy.ExecutewcTableColumnGroup_AssignColumnsCompleted += _wcTableColumnProxy_ExecutewcTableColumnGroup_AssignColumnsCompleted;
                _wcTableColumnProxy.ExecuteWcTableColumn_Check_UnCheckColumnsCompleted += _wcTableColumnProxy_ExecuteWcTableColumn_Check_UnCheckColumnsCompleted;
                InitializeViewLogItem();


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructor


        #region Security

        UserSecurityContext _userContext = new UserSecurityContext();
        //***  Add the ACTUAL Guid below, and then delete this comment
        private Guid _ancestorSecurityId = Guid.NewGuid();
        ControlCache cCache;
        EntityCache eCache = null;
        bool defaultEntityOperationPermission = true;
        bool _secuitySettingIsReadOnly = false;
        //***  Add the ACTUAL Guid below, and then delete this comment
        Guid _controlId = Guid.NewGuid();
        bool _isDeleteActionAllowed = false;
        Guid _deleteActionId = Guid.NewGuid();  // Hard code the actual guid from the security tool


        public void SetSecurityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Enter);

                _userContext.LoadArtifactPermissions(_ancestorSecurityId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Leave);
            }
        }

        void UserSecurityContext_SecurityArtifactsRetrieved(object sender, SecurityArtifactsRetrievedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Enter);
                SecurityCache.AddControlCacheForAncestor(e.ArtifactAncestorData);
                cCache = SecurityCache.GetControlGroupById(e.ArtifactAncestorData.Id);

                UiControlStateSetting setting = cCache.GetControlById(_controlId).Setting;
                if (setting == UiControlStateSetting.Enable)
                {
                    _secuitySettingIsReadOnly = false;
                }
                else
                {
                    _secuitySettingIsReadOnly = true;
                    navList_SplitScreenMode();
                }

                //_isDeleteActionAllowed = SecurityCache.IsEnabled(_deleteActionId);
                //if (_isDeleteActionAllowed)
                //{
                //    navList.DeleteKeyAction = DeleteKeyAction.DeleteSelectedRows;
                //}

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Leave);
            }
        }

        #endregion Security

        #region Method Extentions For Custom Code


        public void Set_vXamComboColumn_ItemSourcesWithParams()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", IfxTraceCategory.Enter);
                
                //_leaseProxy.Begin_GetLeaseAltNumberSource_ComboItemList((Guid)_prj_Id);

//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSourcesWithParams", IfxTraceCategory.Leave);
//            }
        }

        #region Combo ItemsSource for Non-Static Lists




        #endregion region Combo ItemsSource for Non-Static Lists



        void vXamComboColumn_SelectionChanged_CustomCode(object sender, vXamComboColumnSelectionChangedEventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void vXamMultiColumnComboColumn_SelectionChanged_CustomCode(object sender, vXamMultiColumnComboColumnSelectionChangedEventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged_CustomCode", IfxTraceCategory.Leave);
//            }
            }


        void vDatePickerColumn_TextChanged_CustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void XamColorPicker_SelectedColorChanged_CustomCode(XamColorPicker ctl, SelectedColorChangedEventArgs e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void vCheckColumn_CheckedChanged_CustomCode(vCheckBox ctl, IBusinessObject obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void vTextColumn_TextChanged_CustomCode(TextBox ctl, IBusinessObject obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }

        void vDecimalColumn_TextChanged_CustomCode(TextBox ctl, IBusinessObject obj, string key)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void navList_RowEnteredEditMode_Custom(object sender, EditingRowEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", IfxTraceCategory.Enter);
                WcTableColumn_Bll obj = e.Row.Data as WcTableColumn_Bll;
                if (obj == null) { return; }

                        // For multi parets
////                if (_guidParentId == null)
////                {
////                    throw new Exception(_as + "." + _cn + ".navList_RowEnteredEditMode:  _guidParentId  was null.  _guidParentId must be used for the FK.");
////                }
//                //obj.StandingFK = (Guid)_prj_Id;    // added this line
//
//                //switch (_parentType)
//                //{
//                //    case "ucWell":
//                //        // this might not ever get his.
//                //        break;
//                //    case "ucWellDt":
//                //        obj.Current.WlD_Id_noevents = _guidParentId;
//                //        break;
//                //    case "ucContractDt":
//                //        obj.Current.CtD_Id_noevents = _guidParentId;
//                //        break;
//                //}

                //  Or For a Fixed Single Parent
                _isRowInEditMode = true;
                if (obj.State.IsNew() == true)
                {
                    SetNewRowValidation();
                }
                obj.Current.TbC_Tb_Id_noevents = _guidParentId;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_RowEnteredEditMode_Custom", IfxTraceCategory.Leave);
            }
        }


        void navList_SelectedRowsCollectionChanged_Custom(object sender, SelectionCollectionChangedEventArgs<SelectedRowsCollection> e)
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged_Custom", IfxTraceCategory.Leave);
//            }
        }




        void ConfigureColumnHeaderTooltips_CustomCode()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips_CustomCode", IfxTraceCategory.Enter);
//
//
//
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips_CustomCode", IfxTraceCategory.Leave);
//            }
        }


        void XamMenuItem_Click_CustomCode(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click_CustomCode", IfxTraceCategory.Enter);


                XamMenuItem mnu = (XamMenuItem)sender;
                switch (mnu.Name)
                {
                    case "mnuShowDbColumns":
                        ShowDatabaseColumns();
                        break;
                    case "mnuImportSelectedTableColumns":
                        ImportSelectedTableColumnsFromDatabase();
                        break;
                }


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click_CustomCode", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click_CustomCode", IfxTraceCategory.Leave);
            }
        }


        #endregion Method Extentions For Custom Code


        #region CodeGen Methods for modification


        public void SetStateFromParent(object ancestorId, string parentType, int? intParentId, Guid? guidParentId, object objParentId, int? intCurrentId, Guid? guidCurrentId, object objCurrentId, IBusinessObject currentBusinessObject, IEntity_ValuesMngr[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 7
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Enter);
                _intParentId = intParentId;
                _guidParentId = guidParentId;
                //_guidParentId = objParentId as Guid?;
                
                _intCurrentId = intCurrentId;
                _guidCurrentId = guidCurrentId;
                //_guidCurrentId = objCurrentId as Guid?;

                _parentType = parentType;
                _newText = newText;
                if (null != list)
                {
                    NavList_ItemSource.ReplaceList(list);
                }    
                else
                {
                    if (_guidParentId == null)
                    {
                        NavListRefreshFromObjectArray(null);
                        navList.IsEnabled = false;
                    }
                    else
                    {
                        //  Add code here for multiple parent types
                        navList.IsEnabled = true;
                        _wcTableColumnProxy.Begin_WcTableColumn_GetListByFK((Guid)_guidParentId);
                    }
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 7", IfxTraceCategory.Leave);
            }
        }


        #endregion CodeGen Methods for modification


        #region Custom Code


        void ShowDatabaseColumns()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ShowDatabaseColumns", IfxTraceCategory.Enter);

                navList.IsEnabled = true;
                if (mnuShowDbColumns.IsChecked == true)
                {
                    SetViewMode(ViewMode.ShowDbColumns);
                    _wcTableColumnProxy.Begin_GetWcTableColumn_lstNamesFromAppAndDB((Guid)_guidParentId);
                }
                else
                {
                    SetViewMode(ViewMode.Normal);
                    _wcTableColumnProxy.Begin_WcTableColumn_GetListByFK((Guid)_guidParentId);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ShowDatabaseColumns", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ShowDatabaseColumns", IfxTraceCategory.Leave);
            }
        }
        
        public void LoadGridWithDbColumns()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter)
                    IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGridWithDbColumns", IfxTraceCategory.Enter);
                SetViewMode(ViewMode.ShowDbColumns);
                _wcTableColumnProxy.Begin_GetWcTableColumn_lstNamesFromAppAndDB((Guid) _guidParentId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGridWithDbColumns", ex);
                throw IfxWrapperException.GetError(ex, (Guid) traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave)
                    IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGridWithDbColumns", IfxTraceCategory.Leave);
            }
        }
        

        public enum ViewMode
        {
            Normal,
            ShowDbColumns
        }

        ViewMode _currentViewMode = ViewMode.Normal;

        public ViewMode CurrentViewMode
        {
            get { return _currentViewMode; }
            set { _currentViewMode = value; }
        }

        public void SetViewMode(ViewMode mode)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetViewMode", IfxTraceCategory.Enter);
                _currentViewMode = mode;
                switch (mode)
                {
                    case ViewMode.Normal:
                        mnuShowDbColumns.IsChecked = false;
                        navList.Columns["ImportColumn"].Visibility = Visibility.Collapsed;
                        navList.Columns["DbSortOrder"].Visibility = Visibility.Collapsed;
                        navList.Columns["ColumnInDatabase"].Visibility = Visibility.Collapsed;
                        break;
                    case ViewMode.ShowDbColumns:
                        mnuShowDbColumns.IsChecked = true;
                        navList.Columns["ImportColumn"].Visibility = Visibility.Visible;
                        navList.Columns["DbSortOrder"].Visibility = Visibility.Visible;
                        navList.Columns["ColumnInDatabase"].Visibility = Visibility.Visible;
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetViewMode", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetViewMode", IfxTraceCategory.Leave);
            }
        }
        

        private void btnImportColumn_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnImportColumn_Click", IfxTraceCategory.Enter);

                var col = (WcTableColumn_Bll)e.Data;
                if (col.IsImported == false)
                {
                    col.ImportSelectedTableColumn();
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnImportColumn_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnImportColumn_Click", IfxTraceCategory.Leave);
            }
        }


        void ImportSelectedTableColumnsFromDatabase()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSelectedTableColumnsFromDatabase", IfxTraceCategory.Enter);


                foreach (Row rw in navList.SelectionSettings.SelectedRows)
                {
                    var col = (WcTableColumn_Bll)rw.Data;
                    if (col.IsImported == false)
                    {
                        col.ImportSelectedTableColumn();
                    }
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSelectedTableColumnsFromDatabase", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ImportSelectedTableColumnsFromDatabase", IfxTraceCategory.Leave);
            }
        }

        


        private void HeaderCheckBoxCheckedChanged(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "HeaderCheckBoxCheckedChanged", IfxTraceCategory.Enter);
                bool isChecked = (bool)((CheckBox)e.OriginalSource).IsChecked;
                string key = ((HeaderCellControl)((vColumnHeaderGrid)((CheckBox)e.OriginalSource).Parent).Parent).Cell.Column.Key;
                StringBuilder sb = new StringBuilder();
                foreach (var row in navList.Rows)
                {
                    WcTableColumn_Bll obj = row.Data as WcTableColumn_Bll;
                    if (obj != null)
                    {
                        sb.Append("'" + obj.TbC_Id.ToString() + "',");
                    }
                }
                if (sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                    _wcTableColumnProxy.Begin_ExecuteWcTableColumn_Check_UnCheckColumns(sb.ToString(), key, isChecked, (Guid)Credentials.UserId);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "HeaderCheckBoxCheckedChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "HeaderCheckBoxCheckedChanged", IfxTraceCategory.Leave);
            }
        }









        #endregion Custom Code


        #region Fetch Data


        private void _wcTableColumnProxy_ExecutewcTableColumnGroup_AssignColumnsCompleted(object sender, ExecutewcTableColumnGroup_AssignColumnsCompletedEventArgs e)
        {
            throw new NotImplementedException();
        }


        public event OnColumnHeaderCheckboxChecked_ReloadWcTableColumnListEventHandler
            OnColumnHeaderCheckboxChecked_ReloadWcTableColumnList;

        private void _wcTableColumnProxy_ExecuteWcTableColumn_Check_UnCheckColumnsCompleted(object sender, ExecuteWcTableColumn_Check_UnCheckColumnsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_Proxy_ExecuteFunc_Check_UnCheckColumnsCompleted", IfxTraceCategory.Enter);
                object[] data = e.Result;

                if (data != null)
                {
                    bool? isChecked = data[0] as bool?;
                    string column = data[1] as string;
                    int? success = data[2] as int?;

                    //string column = data[2] as string;
                    if (success == 1 && isChecked != null)
                    {
                        switch (column)
                        {
                            case "TbC_IsCodeGen":
                                foreach (var row in navList.Rows)
                                {
                                    WcTableColumn_Bll obj = row.Data as WcTableColumn_Bll;
                                    if (obj != null)
                                    {
                                        obj.TbC_IsCodeGen_noevents = (bool)isChecked;
                                    }
                                }
                                break;
                            case "xxx":

                                break;
                        }




                        //OnColumnHeaderCheckboxChecked_ReloadWcTableColumnListEventHandler handler = OnColumnHeaderCheckboxChecked_ReloadWcTableColumnList;
                        //if (handler != null)
                        //{
                        //    handler(this, new OnColumnHeaderCheckboxChecked_ReloadWcTableColumnListArgs((bool)isChecked));
                        //}

                        return;
                    }
                }
                MessageBox.Show(
                    "There was a problem on the server in updating these rows.  If this continues, please contact support.",
                    "Error", MessageBoxButton.OK);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_Proxy_ExecuteFunc_Check_UnCheckColumnsCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_Proxy_ExecuteFunc_Check_UnCheckColumnsCompleted", IfxTraceCategory.Leave);
            }
        }




        private void _wcTableColumnProxy_GetWcTableColumn_lstNamesFromAppAndDBCompleted(object sender, GetWcTableColumn_lstNamesFromAppAndDBCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcTableColumnProxy_GetWcTableColumn_lstNamesFromAppAndDBCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                NavListRefreshFromObjectArray(array);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcTableColumnProxy_GetWcTableColumn_lstNamesFromAppAndDBCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                navList.Cursor = Cursors.Arrow;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_wcTableColumnProxy_GetWcTableColumn_lstNamesFromAppAndDBCompleted", IfxTraceCategory.Leave);
            }
        }


        #endregion Fetch Data



    }



    #region OnColumnHeaderCheckboxChecked_ReloadWcTableColumnList

    public delegate void OnColumnHeaderCheckboxChecked_ReloadWcTableColumnListEventHandler(object sender, OnColumnHeaderCheckboxChecked_ReloadWcTableColumnListArgs e);

    public class OnColumnHeaderCheckboxChecked_ReloadWcTableColumnListArgs : EventArgs
    {
        private readonly bool _isChecked;
        public OnColumnHeaderCheckboxChecked_ReloadWcTableColumnListArgs(bool isChecked)
        {
            _isChecked = isChecked;
        }

        public bool IsChecked
        {
            get { return _isChecked; }
        }
    }

    #endregion OnColumnHeaderCheckboxChecked_ReloadWcTableColumnList




}
