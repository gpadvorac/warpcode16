using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class WcApplicationConnectionStringKeyConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcApplicationConnectionStringKeyConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public WcApplicationConnectionStringKeyConcurrencyList(WcApplicationConnectionStringKey_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcApplicationConnectionStringKeyConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Name", data.C.ApCnSK_Name, data.X.ApCnSK_Name));
                _concurrencyList.Add(new ConcurrencyItem("Is Default", data.C.ApCnSK_IsDefault, data.X.ApCnSK_IsDefault));
                _concurrencyList.Add(new ConcurrencyItem("Server", data.C.ApCnSK_Server, data.X.ApCnSK_Server));
                _concurrencyList.Add(new ConcurrencyItem("Database", data.C.ApCnSK_Database, data.X.ApCnSK_Database));
                _concurrencyList.Add(new ConcurrencyItem("User Name", data.C.ApCnSK_UserName, data.X.ApCnSK_UserName));
                _concurrencyList.Add(new ConcurrencyItem("Password", data.C.ApCnSK_Password, data.X.ApCnSK_Password));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.ApCnSK_IsActiveRow, data.X.ApCnSK_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



