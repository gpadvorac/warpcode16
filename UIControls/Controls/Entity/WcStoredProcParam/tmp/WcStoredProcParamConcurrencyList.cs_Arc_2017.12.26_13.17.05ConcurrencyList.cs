using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class WcStoredProcParamConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcStoredProcParamConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public WcStoredProcParamConcurrencyList(WcStoredProcParam_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcStoredProcParamConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Sort Order", data.C.SpP_SortOrder, data.X.SpP_SortOrder));
                _concurrencyList.Add(new ConcurrencyItem("Name", data.C.SpP_Name, data.X.SpP_Name));
                _concurrencyList.Add(new ConcurrencyItem("Direction", data.C.SpP_SpPDr_Id, data.X.SpP_SpPDr_Id));
                _concurrencyList.Add(new ConcurrencyItem("DotNet Type", data.C.SpP_DtNt_ID, data.X.SpP_DtNt_ID));
                _concurrencyList.Add(new ConcurrencyItem("SQL Type", data.C.SpP_DtSq_ID, data.X.SpP_DtSq_ID));
                _concurrencyList.Add(new ConcurrencyItem("Nullable", data.C.SpP_IsNullable, data.X.SpP_IsNullable));
                _concurrencyList.Add(new ConcurrencyItem("Default Value", data.C.SpP_DefaultValue, data.X.SpP_DefaultValue));
                _concurrencyList.Add(new ConcurrencyItem("Value", data.C.SpP_Value, data.X.SpP_Value));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.SpP_IsActiveRow, data.X.SpP_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



