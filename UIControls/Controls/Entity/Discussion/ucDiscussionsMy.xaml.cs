﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationTypeServices;
using EntityBll.SL;
using EntityWireTypeSL;
using Ifx.SL;
using Infragistics.Controls.Grids;
using Infragistics.Controls.Grids.Primitives;
using Infragistics.Controls.Interactions;
using Infragistics.Controls.Menus;
using ProxyWrapper;
using vControls;
using vDialogControl;
using vDiscussionDialog;
using vUICommon;
using Velocity.SL;

namespace UIControls
{
    public partial class ucDiscussionsMy : UserControl
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "ucDiscussionsMy";


        private DiscussionSupportService_ProxyWrapper _proxy = null;

        private Discussion_MyDiscussions_Binding _myDiscussions = null;

        DiscussionViewModel.DiscussionViewContext _viewContext = DiscussionViewModel.DiscussionViewContext.Owner;

        FilterUIType _FilterType = FilterUIType.FilterRowTop;

        #endregion Initialize Variables

        #region Constructor

        public ucDiscussionsMy()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucDiscussionsMy", IfxTraceCategory.Enter);
                InitializeComponent();

                _proxy = new DiscussionSupportService_ProxyWrapper();
                _proxy.GetDiscussion_MyDiscussionsCompleted += _proxy_GetDiscussion_MyDiscussionsCompleted;
                _proxy.GetDiscussion_DiscussionsImInCompleted += _proxy_GetDiscussion_DiscussionsImInCompleted;

                //_proxy.GetDiscussion_lstBy_ContractMyCompleted += _proxy_GetDiscussion_lstBy_ContractMyCompleted;
                //_proxy.GetDiscussion_lstBy_DefectMyCompleted += _proxy_GetDiscussion_lstBy_DefectMyCompleted;
                //_proxy.GetDiscussion_lstBy_LeaseMyCompleted += _proxy_GetDiscussion_lstBy_LeaseMyCompleted;
                //_proxy.GetDiscussion_lstBy_ObligationMyCompleted += _proxy_GetDiscussion_lstBy_ObligationMyCompleted;
                _proxy.GetDiscussion_lstBy_TaskMyCompleted += _proxy_GetDiscussion_lstBy_TaskMyCompleted;
                //_proxy.GetDiscussion_lstBy_WellMyCompleted += _proxy_GetDiscussion_lstBy_WellMyCompleted;
                //_proxy.GetDiscussion_lstBy_ProjectMyCompleted += _proxy_GetDiscussion_lstBy_ProjectMyCompleted;


                //_proxy.GetDiscussion_lstBy_ContractMemberCompleted += _proxy_GetDiscussion_lstBy_ContractMemberCompleted;
                //_proxy.GetDiscussion_lstBy_DefectMemberCompleted += _proxy_GetDiscussion_lstBy_DefectMemberCompleted;
                //_proxy.GetDiscussion_lstBy_LeaseMemberCompleted += _proxy_GetDiscussion_lstBy_LeaseMemberCompleted;
                //_proxy.GetDiscussion_lstBy_ObligationMemberCompleted += _proxy_GetDiscussion_lstBy_ObligationMemberCompleted;
                _proxy.GetDiscussion_lstBy_TaskMemberCompleted += _proxy_GetDiscussion_lstBy_TaskMemberCompleted;
                //_proxy.GetDiscussion_lstBy_WellMemberCompleted += _proxy_GetDiscussion_lstBy_WellMemberCompleted;
                //_proxy.GetDiscussion_lstBy_ProjectMemberCompleted += _proxy_GetDiscussion_lstBy_ProjectMemberCompleted;


                xgdMyDiscussions.FilteringSettings.AllowFiltering = _FilterType;


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucDiscussionsMy", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucDiscussionsMy", IfxTraceCategory.Leave);
            }
        }


        #endregion Constructor



        public DiscussionViewModel.DiscussionViewContext ViewContext
        {
            get { return _viewContext; }
            set { _viewContext = value; }
        }


        #region Load Grid


        public void LoadGrid_MyDiscusssions()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGrid_MyDiscusssions", IfxTraceCategory.Enter);

                _proxy.Begin_GetDiscussion_MyDiscussions((Guid)Credentials.UserId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGrid_MyDiscusssions", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGrid_MyDiscusssions", IfxTraceCategory.Leave);
            }
        }


        public void LoadGrid_DiscusssionsImIn()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGrid_MyDiscusssions", IfxTraceCategory.Enter);

                _proxy.Begin_GetDiscussion_DiscussionsImIn((Guid)Credentials.UserId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGrid_MyDiscusssions", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadGrid_MyDiscusssions", IfxTraceCategory.Leave);
            }
        }

        private void LoadDiscussionGrid(byte[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadDiscussionGrid", IfxTraceCategory.Enter);
                xgdMyDiscussions.ItemsSource = null;
                if (data == null)
                {
                    MessageBox.Show("No discussions were found.", "No Discussions", MessageBoxButton.OK);
                    return;
                }
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array == null)
                {
                    MessageBox.Show("No discussions were found.", "No Discussions", MessageBoxButton.OK);
                    return;
                }

                if (array != null && array.Count() > 0)
                {
                    List<Discussion_MyDiscussions_Binding> list = new List<Discussion_MyDiscussions_Binding>();
                    for (int i = 0; i < array.GetUpperBound(0) + 1; i++)
                    {
                        list.Add(new Discussion_MyDiscussions_Binding((object[])array[i]));
                    }
                    xgdMyDiscussions.ItemsSource = list;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadDiscussionGrid", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadDiscussionGrid", IfxTraceCategory.Leave);
            }
        }


        void _proxy_GetDiscussion_MyDiscussionsCompleted(object sender, GetDiscussion_MyDiscussionsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_MyDiscussionsCompleted", IfxTraceCategory.Enter);

                LoadDiscussionGrid(e.Result);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_MyDiscussionsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_MyDiscussionsCompleted", IfxTraceCategory.Leave);
            }
        }



        void _proxy_GetDiscussion_DiscussionsImInCompleted(object sender, GetDiscussion_DiscussionsImInCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_DiscussionsImInCompleted", IfxTraceCategory.Enter);

                LoadDiscussionGrid(e.Result);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_DiscussionsImInCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_DiscussionsImInCompleted", IfxTraceCategory.Leave);
            }
        }







        #endregion Load Grid


        #region Discussion Popup


        private void btnOpenDiscussions_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenDiscussions_Click", IfxTraceCategory.Enter);

                _myDiscussions = (Discussion_MyDiscussions_Binding)e.Data;

                if (_viewContext == DiscussionViewModel.DiscussionViewContext.Owner)
                {
                    //if (_myDiscussions.DscXR_Ct_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_ContractMy((Guid)_myDiscussions.DscXR_Ct_Id, (Guid)Credentials.UserId);
                    //}
                    //else if (_myDiscussions.DscXR_Df_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_DefectMy((Guid)_myDiscussions.DscXR_Df_Id, (Guid)Credentials.UserId);
                    //}
                    //else if (_myDiscussions.DscXR_Ls_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_LeaseMy((Guid)_myDiscussions.DscXR_Ls_Id, (Guid)Credentials.UserId);
                    //}
                    //else if (_myDiscussions.DscXR_Ob_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_ObligationMy((Guid)_myDiscussions.DscXR_Ob_Id, (Guid)Credentials.UserId);
                    //}
                    //else if (_myDiscussions.DscXR_Tk_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_TaskMy((Guid)_myDiscussions.DscXR_Tk_Id, (Guid)Credentials.UserId);
                    //}
                    //else if (_myDiscussions.DscXR_Wl_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_WellMy((Guid)_myDiscussions.DscXR_Wl_Id, (Guid)Credentials.UserId);
                    //}
                    //else if (_myDiscussions.DscXR_Pj_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_ProjectMy((Guid)_myDiscussions.DscXR_Pj_Id, (Guid)Credentials.UserId);
                    //}
                    //else
                    //{
                    //    throw new Exception("No parent ID found.");
                    //}
                }
                else
                {
                    //if (_myDiscussions.DscXR_Ct_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_ContractMember((Guid)_myDiscussions.DscXR_Ct_Id, (Guid)Credentials.UserId);
                    //}
                    //else if (_myDiscussions.DscXR_Df_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_DefectMember((Guid)_myDiscussions.DscXR_Df_Id, (Guid)Credentials.UserId);
                    //}
                    //else if (_myDiscussions.DscXR_Ls_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_LeaseMember((Guid)_myDiscussions.DscXR_Ls_Id, (Guid)Credentials.UserId);
                    //}
                    //else if (_myDiscussions.DscXR_Ob_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_ObligationMember((Guid)_myDiscussions.DscXR_Ob_Id, (Guid)Credentials.UserId);
                    //}
                    //else if (_myDiscussions.DscXR_Tk_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_TaskMember((Guid)_myDiscussions.DscXR_Tk_Id, (Guid)Credentials.UserId);
                    //}
                    //else if (_myDiscussions.DscXR_Wl_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_WellMember((Guid)_myDiscussions.DscXR_Wl_Id, (Guid)Credentials.UserId);
                    //}
                    //else if (_myDiscussions.DscXR_Pj_Id != null)
                    //{
                    //    _proxy.Begin_GetDiscussion_lstBy_ProjectMember((Guid)_myDiscussions.DscXR_Pj_Id, (Guid)Credentials.UserId);
                    //}
                    //else
                    //{
                    //    throw new Exception("No parent ID found.");
                    //}
                }


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenDiscussions_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnOpenDiscussions_Click", IfxTraceCategory.Leave);
            }
        }

        private void OpenDiscussionDialog(byte[] data, Guid id, string parentType)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OpenDiscussionDialog", IfxTraceCategory.Enter);

                var array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                if (array == null)
                {
                    MessageBox.Show("No discussions found.  Please notify support if this continues.", "Data Not Found",
                        MessageBoxButton.OK);
                    IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_CustomerCompleted", new Exception("Expecting discussions, but none were returned."));
                    return;
                }



                //var discussions = new Discussion_List();
                //discussions.ReplaceList(array);
                var discussionDialog = new DiscussionDialogDirector(array, vDialogMode.Unrestricted, id, parentType, _viewContext, Credentials.UserId);
                discussionDialog.Dialog.IsModal = true;
                discussionDialog.Dialog.StartupPosition = StartupPosition.Center;
                discussionDialog.Dialog.HeaderHeight = 24;
                //discussionDialog.Dialog.Padding = new Thickness(0);
                discussionDialog.Dialog.Header = "My Disussions    " + GetDiscussionWindowHeader();
                //discussionDialog.Dialog.HeaderForeground = new SolidColorBrush(Colors.White);

                if (discussionDialog.ShowDialog() == true)
                {
                    discussionDialog.Result.RequestClose += (_, __) =>
                    {
                        //_customer.DiscussionCount = discussionDialog.Result.Discussions.Count;
                        discussionDialog = null;
                    };
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OpenDiscussionDialog", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OpenDiscussionDialog", IfxTraceCategory.Leave);
            }
        }

        string GetDiscussionWindowHeader()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussionWindowHeader", IfxTraceCategory.Enter);

                string entity = "";
                if (_myDiscussions.DscXR_Ct_Id != null)
                {
                    entity = "Section: " + _myDiscussions.Ct_Number;
                }
                else if (_myDiscussions.DscXR_Df_Id != null)
                {
                    entity = "Issue: " + _myDiscussions.Df_Number;
                }
                else if (_myDiscussions.DscXR_Ls_Id != null)
                {
                    entity = "Agreement: " + _myDiscussions.Ls_Number;
                }
                else if (_myDiscussions.DscXR_Ob_Id != null)
                {
                    entity = "Obligation/Provision: " + _myDiscussions.Ob_Number;
                }
                else if (_myDiscussions.DscXR_Tk_Id != null)
                {
                    entity = "Task: " + _myDiscussions.Tk_Number;
                }
                else if (_myDiscussions.DscXR_Wl_Id != null)
                {
                    entity = "Pipeline: " + _myDiscussions.Well;
                }
                else if (_myDiscussions.DscXR_Pj_Id != null)
                {
                    entity = "Project";
                }

                return entity;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussionWindowHeader", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
                return "";
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussionWindowHeader", IfxTraceCategory.Leave);
            }
        }





        #region My Discussion Results

        //void _proxy_GetDiscussion_lstBy_ContractMyCompleted(object sender, GetDiscussion_lstBy_ContractMyCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ContractMyCompleted", IfxTraceCategory.Enter);

        //        OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Ct_Id, ApplicationLevelVariables.DiscussionParentTypes.Contract);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ContractMyCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ContractMyCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void _proxy_GetDiscussion_lstBy_DefectMyCompleted(object sender, GetDiscussion_lstBy_DefectMyCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_DefectMyCompleted", IfxTraceCategory.Enter);

        //        OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Df_Id, ApplicationLevelVariables.DiscussionParentTypes.Defect);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_DefectMyCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_DefectMyCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void _proxy_GetDiscussion_lstBy_LeaseMyCompleted(object sender, GetDiscussion_lstBy_LeaseMyCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_LeaseMyCompleted", IfxTraceCategory.Enter);

        //        OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Ls_Id, ApplicationLevelVariables.DiscussionParentTypes.Lease);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_LeaseMyCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_LeaseMyCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void _proxy_GetDiscussion_lstBy_ObligationMyCompleted(object sender, GetDiscussion_lstBy_ObligationMyCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ObligationMyCompleted", IfxTraceCategory.Enter);

        //        OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Ob_Id, ApplicationLevelVariables.DiscussionParentTypes.Obligation);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ObligationMyCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ObligationMyCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        void _proxy_GetDiscussion_lstBy_TaskMyCompleted(object sender, GetDiscussion_lstBy_TaskMyCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_TaskMyCompleted", IfxTraceCategory.Enter);

                OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Tk_Id, ApplicationLevelVariables.DiscussionParentTypes.Task);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_TaskMyCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_TaskMyCompleted", IfxTraceCategory.Leave);
            }
        }

        //void _proxy_GetDiscussion_lstBy_WellMyCompleted(object sender, GetDiscussion_lstBy_WellMyCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_WellMyCompleted", IfxTraceCategory.Enter);

        //        OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Wl_Id, ApplicationLevelVariables.DiscussionParentTypes.Well);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_WellMyCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_WellMyCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void _proxy_GetDiscussion_lstBy_ProjectMyCompleted(object sender, GetDiscussion_lstBy_ProjectMyCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ProjectMyCompleted", IfxTraceCategory.Enter);

        //        OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Pj_Id, ApplicationLevelVariables.DiscussionParentTypes.Project);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ProjectMyCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ProjectMyCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        #endregion My Discussion Results


        #region Discussions Im In Results

        //void _proxy_GetDiscussion_lstBy_ContractMemberCompleted(object sender, GetDiscussion_lstBy_ContractMemberCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ContractMemberCompleted", IfxTraceCategory.Enter);

        //        OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Ct_Id, ApplicationLevelVariables.DiscussionParentTypes.Contract);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ContractMemberCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ContractMemberCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void _proxy_GetDiscussion_lstBy_DefectMemberCompleted(object sender, GetDiscussion_lstBy_DefectMemberCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_DefectMemberCompleted", IfxTraceCategory.Enter);

        //        OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Df_Id, ApplicationLevelVariables.DiscussionParentTypes.Defect);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_DefectMemberCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_DefectMemberCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void _proxy_GetDiscussion_lstBy_LeaseMemberCompleted(object sender, GetDiscussion_lstBy_LeaseMemberCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_LeaseMemberCompleted", IfxTraceCategory.Enter);

        //        OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Ls_Id, ApplicationLevelVariables.DiscussionParentTypes.Lease);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_LeaseMemberCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_LeaseMemberCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void _proxy_GetDiscussion_lstBy_ObligationMemberCompleted(object sender, GetDiscussion_lstBy_ObligationMemberCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ObligationMemberCompleted", IfxTraceCategory.Enter);

        //        OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Ob_Id, ApplicationLevelVariables.DiscussionParentTypes.Obligation);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ObligationMemberCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ObligationMemberCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        void _proxy_GetDiscussion_lstBy_TaskMemberCompleted(object sender, GetDiscussion_lstBy_TaskMemberCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_TaskMemberCompleted", IfxTraceCategory.Enter);

                OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Tk_Id, ApplicationLevelVariables.DiscussionParentTypes.Task);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_TaskMemberCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_TaskMemberCompleted", IfxTraceCategory.Leave);
            }
        }

        //void _proxy_GetDiscussion_lstBy_WellMemberCompleted(object sender, GetDiscussion_lstBy_WellMemberCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_WellMemberCompleted", IfxTraceCategory.Enter);

        //        OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Wl_Id, ApplicationLevelVariables.DiscussionParentTypes.Well);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_WellMemberCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_WellMemberCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void _proxy_GetDiscussion_lstBy_ProjectMemberCompleted(object sender, GetDiscussion_lstBy_ProjectMemberCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ProjectMemberCompleted", IfxTraceCategory.Enter);

        //        OpenDiscussionDialog(e.Result, (Guid)_myDiscussions.DscXR_Pj_Id, ApplicationLevelVariables.DiscussionParentTypes.Project);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ProjectMemberCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetDiscussion_lstBy_ProjectMemberCompleted", IfxTraceCategory.Leave);
        //    }
        //}


        #endregion Discussions Im In Results


        #endregion Discussion Popup


        
        private void XamMenuItem_Click(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click", IfxTraceCategory.Enter);

                XamMenuItem mnu = (XamMenuItem)sender;
                switch (mnu.Name)
                {
                    case "mnuClearFilters":
                        ClearFilterCells(xgdMyDiscussions, xgdMyDiscussions.Rows);
                        break;
                    case "mnuFilterNone":
                        _FilterType = FilterUIType.None;
                        navList_FilterMode();
                        break;
                    case "mnuFilterMenu":
                        _FilterType = FilterUIType.FilterMenu;
                        navList_FilterMode();
                        break;
                    case "mnuFilterTop":
                        _FilterType = FilterUIType.FilterRowTop;
                        navList_FilterMode();
                        break;
                    case "mnuFilterBottom":
                        _FilterType = FilterUIType.FilterRowBottom;
                        navList_FilterMode();
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMenuItem_Click", IfxTraceCategory.Leave);
            }
        }
        
        public void navList_FilterMode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FilterMode", IfxTraceCategory.Enter);
                xgdMyDiscussions.FilteringSettings.AllowFiltering = _FilterType;
                switch (_FilterType)
                {
                    case FilterUIType.None:
                        mnuFilterNone.IsChecked = true;
                        mnuFilterMenu.IsChecked = false;
                        mnuFilterTop.IsChecked = false;
                        mnuFilterBottom.IsChecked = false;
                        break;
                    case FilterUIType.FilterMenu:
                        mnuFilterNone.IsChecked = false;
                        mnuFilterMenu.IsChecked = true;
                        mnuFilterTop.IsChecked = false;
                        mnuFilterBottom.IsChecked = false;
                        break;
                    case FilterUIType.FilterRowTop:
                        mnuFilterNone.IsChecked = false;
                        mnuFilterMenu.IsChecked = false;
                        mnuFilterTop.IsChecked = true;
                        mnuFilterBottom.IsChecked = false;
                        break;
                    case FilterUIType.FilterRowBottom:
                        mnuFilterNone.IsChecked = false;
                        mnuFilterMenu.IsChecked = false;
                        mnuFilterTop.IsChecked = false;
                        mnuFilterBottom.IsChecked = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FilterMode", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_FilterMode", IfxTraceCategory.Leave);
            }
        }

        public void ClearFilterCells(XamGrid grid, RowCollection rows)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearFilterCells", IfxTraceCategory.Enter);

                if (rows.Count == 0) { return; }

                grid.FilteringSettings.RowFiltersCollection.Clear();
                if (rows != null)
                {
                    if (rows[0].RowType == Infragistics.Controls.Grids.RowType.DataRow)
                    {
                        foreach (Cell cell in ((RowsManager)rows[0].Manager).FilterRowBottom.Cells)
                        {
                            (cell as FilterRowCell).FilterCellValue = null;
                        }

                        foreach (Cell cell in ((RowsManager)rows[0].Manager).FilterRowTop.Cells)
                        {
                            (cell as FilterRowCell).FilterCellValue = null;
                        }

                        foreach (Row dr in rows)
                        {
                            if (dr.HasChildren)
                            {
                                foreach (ChildBand childBand in dr.ChildBands)
                                {
                                    childBand.RowFiltersCollection.Clear();
                                }
                                ClearFilterCells(grid, dr.ChildBands[0].Rows);
                            }
                        }
                    }
                    else
                    {
                        if (rows[0].GetType() == typeof(GroupByRow))
                        {
                            foreach (GroupByRow gbr in rows)
                            {
                                gbr.RowFiltersCollection.Clear();
                                ClearFilterCells(grid, gbr.Rows);

                                foreach (Cell cell in ((RowsManager)gbr.Rows[0].Manager).FilterRowTop.Cells)
                                {
                                    (cell as FilterRowCell).FilterCellValue = null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearFilterCells", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearFilterCells", IfxTraceCategory.Leave);
            }
        }
        

    }
}
