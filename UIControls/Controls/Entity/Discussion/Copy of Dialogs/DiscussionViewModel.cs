using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using EntityBll.SL;
using TypeServices;
using vCommands;

namespace UIControls
{
    public class DiscussionViewModel : IDialogViewModel
    {
        private readonly ICommand _addNewDiscussionCommand;

        private readonly ICommand _closeCommand;

        public DiscussionViewModel()
        {
            _addNewDiscussionCommand = new ActionCommand(() =>
            {
                var discussionBll = new Discussion_Bll();

                var dialog = new NewDiscussionDialogDirector();
                if (dialog.ShowDialog() == true)
                {
                    dialog.Result.RequestClose += (_, __) =>
                    {
                        if (dialog.Result.IsEditComplete)
                        {
                            discussionBll.Dsc_Title = dialog.Result.Title;

                            switch (ParentType)
                            {
                                case DiscussionParentTypes.Contract:
                                    discussionBll.Dsc_Ct_Id = ParentId;
                                    break;
                                case DiscussionParentTypes.Defect:
                                    discussionBll.Dsc_Df_Id = ParentId;
                                    break;
                                case DiscussionParentTypes.Lease:
                                    discussionBll.Dsc_Ls_Id = ParentId;
                                    break;
                                case DiscussionParentTypes.Obligation:
                                    discussionBll.Dsc_Ob_Id = ParentId;
                                    break;
                                case DiscussionParentTypes.Task:
                                    discussionBll.Dsc_Tk_Id = ParentId;
                                    break;
                                case DiscussionParentTypes.Well
:
                                    discussionBll.Dsc_Wl_Id = ParentId;
                                    break;
                            }

                            discussionBll.AsyncSaveWithResponseComplete += (sender, args) =>
                            {
                                if (args.Response.Result == DataOperationResult.Success)
                                {
                                    Discussions.Insert(0, discussionBll);
                                }
                                else
                                {
                                    string msg = "An error occured:  " + args.Response.Result + Environment.NewLine + "If you continue to get this error, please contact support.";
                                    MessageBox.Show(msg, "Save Operation Error", MessageBoxButton.OK);
                                }
                            };
                            discussionBll.Save(UseConcurrencyCheck.UseDefaultSetting);
                        }
                    };
                }
            });

            _closeCommand = new ActionCommand(() =>
            {
                if (Discussions.Any(d => !string.IsNullOrEmpty(d.Comment) || d.NewComment.Attachments.Count > 0))
                {
                    var dialog = new YesNoDialogDirector("Do you want to close this form and discard your new comment?", "Cancel", "Discard");
                    if (dialog.ShowDialog() == true)
                    {
                        dialog.Result.RequestClose += (_, __) =>
                        {
                            //if user ok with discarding then just close, if cancel then do nothing
                            if (dialog.Result.IsEditComplete)
                            {
                                IsEditComplete = false;
                                OnRequestClose();
                            }
                        };
                    }
                }
                else
                {
                    IsEditComplete = false;
                    OnRequestClose();
                }
            });

            
        }

        public bool IsEditComplete { get; set; }

        public Discussion_List Discussions { get; set; }

        public DiscussionParentTypes ParentType { get; set; }

        public Guid ParentId { get; set; }

        public ICommand AddNewDiscussionCommand
        {
            get { return _addNewDiscussionCommand; }
        }

        public ICommand CloseCommand
        {
            get { return _closeCommand; }
        }

        #region Events

        public event EventHandler RequestClose;

        private void OnRequestClose()
        {
            EventHandler handler = RequestClose;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        #endregion Events
    }
}