﻿namespace UIControls
{
    public class NewDiscussionDialogDirector : EditDialogDirector<NewDiscussionViewModel>
    {
        public NewDiscussionDialogDirector()
        {
            Dialog = new NewDiscussionView(this);
        }
    }
}