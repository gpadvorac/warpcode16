using System;
using System.Windows.Input;
using vCommands;

namespace UIControls
{
    public class EditCommentViewModel : IDialogViewModel
    {
        private readonly ICommand _okCommand;
        private readonly ICommand _cancelCommand;

        public EditCommentViewModel()
        {
            _okCommand = new ActionCommand(() =>
            {
                IsEditComplete = true;
                OnRequestClose();
            }, () => !string.IsNullOrEmpty(Comment));

            _cancelCommand = new ActionCommand(() =>
            {
                IsEditComplete = false;
                OnRequestClose();
            });
        }

        public bool IsEditComplete { get; set; }

        public string Comment { get; set; }

        public ICommand OkCommand
        {
            get { return _okCommand; }
        }

        public ICommand CancelCommand
        {
            get { return _cancelCommand; }
        }

        #region Events

        public event EventHandler RequestClose;

        private void OnRequestClose()
        {
            EventHandler handler = RequestClose;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        #endregion Events
    }
}