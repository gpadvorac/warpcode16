using System;
using vCommands;

namespace UIControls
{
    public class NewDiscussionViewModel : IDialogViewModel
    {
        private readonly ActionCommand _okCommand;
        private readonly ActionCommand _cancelCommand;
        private string _title;

        public NewDiscussionViewModel()
        {
            _okCommand = new ActionCommand(() =>
            {
                IsEditComplete = true;
                OnRequestClose();
            }, () => !string.IsNullOrEmpty(Title));

            _cancelCommand = new ActionCommand(() =>
            {
                IsEditComplete = false;
                OnRequestClose();
            });
        }

        public bool IsEditComplete { get; set; }

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value; 
                OkCommand.RaiseCanExecuteChanged();
            }
        }

        public ActionCommand OkCommand
        {
            get { return _okCommand; }
        }

        public ActionCommand CancelCommand
        {
            get { return _cancelCommand; }
        }

        #region Events

        public event EventHandler RequestClose;

        private void OnRequestClose()
        {
            EventHandler handler = RequestClose;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        #endregion Events
    }
}