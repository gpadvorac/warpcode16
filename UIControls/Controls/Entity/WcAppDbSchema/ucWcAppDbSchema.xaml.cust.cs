using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using EntityBll.SL;
using Ifx.SL;
using IfxUserViewLogSL;
using TypeServices;
using vUICommon;

// Gen Timestamp:  1/7/2018 10:51:55 PM

namespace UIControls
{
    public partial class ucWcAppDbSchema
    {

  

        #region Initialize Variables



        #endregion Initialize Variables




        void CustomConstructionCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", IfxTraceCategory.Enter);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructionCode", IfxTraceCategory.Leave);
            }
        }




        #region Copy Code Gen Methods from the Codebehind class here



        public void SetStateFromParent(object ancestorId, string parentType, int? intParentId, Guid? guidParentId, object objParentId, int? intCurrentId, Guid? guidCurrentId, object objCurrentId, IBusinessObject currentBusinessObject, object[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 9
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Enter);
                _intParentId = intParentId;
                if (guidParentId == null)
                {
                    _guidParentId = objParentId as Guid?;
                }
                else
                {
                    _guidParentId = guidParentId;
                }
                
                _intCurrentId = intCurrentId;
                _guidCurrentId = guidCurrentId;
                //_guidCurrentId = objCurrentId as Guid?;
                //_oCurrentId = objCurrentId;

                _parentType = parentType;
                _currentBusinessObject = (WcAppDbSchema_Bll)currentBusinessObject;  

                ucNav.ParentType = _parentType;
                ucNav.GuidParentId = _guidParentId;  
                ucNav.ObjectParentId = objParentId;

                if (_FLG_IsLoaded == false)
                {
                    InitializeControl();
                }
                if (list == null)
                {
                    //  **********************************      N E E D   C U S T O M   C O D E    H E R E  ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! 
           
                    if (_guidParentId == null)
                    {
                        //ucNav.NavList_ItemSource.Clear();
                        NavListRefreshFromObjectArray(null);
                        ucNav.IsEnabled = false;
                    }
                    else
                    {
                        ucNav.IsEnabled = true;

                        LoadNavlistData();
                        //_wcAppDbSchemaProxy.Begin_WcAppDbSchema_GetListByFK((Guid)_guidParentId);
                        //ucNav.navList.Cursor = Cursors.Wait;
                    }
                }
                else
                {
                    NavListRefreshFromObjectArray(list);
                }

                // Use    intParentId     if the parent is an Integer.  we might need to be using the Ancestor Id instead.
                if (guidParentId != null)
                {
                    ucNav.Set_vXamComboColumn_ItemSourcesWithParams();
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Leave);
            }
        }

        
        public void LoadNavlistData()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadNavlistData", IfxTraceCategory.Enter);

                _currentBusinessObject = null;
                ucNav.IsEnabled = true;
                switch (_parentType)
                {
                    case "ucWcApplication":
                        _wcAppDbSchemaProxy.Begin_WcAppDbSchema_GetListByFK((Guid)_guidParentId);
                        ucNav.navList.Cursor = Cursors.Wait;
                        break;
                    default:
                        throw new Exception("Missing Parent Type");
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadNavlistData", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadNavlistData", IfxTraceCategory.Leave);
            }
        }


#endregion Copy Code Gen Methods from the Codebehind class here

        #region Insert Custom Code Here

        #endregion Insert Custom Code Here

        #region  Combo DataSources and Events

        #endregion  Combo DataSources and Events

        #region Method Extentions For Custom Code

        void ConfigureToCurrentEntityState_CustomCode(EntityStateSwitch state)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState_CustomCode", IfxTraceCategory.Enter);
                //switch (state)
                //{
                //    case EntityStateSwitch.None:
                //        if (_currentBusinessObject == null)
                //        {
                //            xxx.IsEnabled = false;
                //        }
                //        else
                //        {
                //            xxx.IsEnabled = true;
                //        }
                //        break;
                //    case EntityStateSwitch.NewInvalidNotDirty:
                //        xxx.IsEnabled = false;
                //        break;
                //    case EntityStateSwitch.NewValidNotDirty:
                //        xxx.IsEnabled = false;
                //        break;
                //    case EntityStateSwitch.NewValidDirty:
                //        xxx.IsEnabled = false;
                //        break;
                //    case EntityStateSwitch.NewInvalidDirty:
                //        xxx.IsEnabled = false;
                //        break;
                //    case EntityStateSwitch.ExistingInvalidDirty:
                //        xxx.IsEnabled = false;
                //        break;
                //    case EntityStateSwitch.ExistingValidDirty:
                //        xxx.IsEnabled = false;
                //        break;
                //    case EntityStateSwitch.ExistingValidNotDirty:
                //        xxx.IsEnabled = true;
                //        break;
                //}
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    _FLG_IsLoaded = true;
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState_CustomCode", IfxTraceCategory.Leave);
            //}
        }

        void SyncControlsWithCurrentBusinessObject_CustomCode1()
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", IfxTraceCategory.Enter);


            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    _FLG_IsLoaded = true;
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", IfxTraceCategory.Leave);
            //}
        }

        void SyncControlsWithCurrentBusinessObject_CustomCode2(string selectedTab, object currentId)
            {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", IfxTraceCategory.Enter);
            //    switch (selectedTab)
            //    {
            //        case "tbi_ucTbCol":
                //            if (_ucTbCol == null) { ucTbCol_Load(); }
                //            _ucTbCol.SetStateFromParent(null, "ucWcTable", null, null, currentId, null, null, currentChildId, null, null, "");
                //            break;
                //        case "tbi_ucTbGrpAsgn":
                //            if (_ucTblGrpAssign == null) { ucTblGrpAssign_Load(); }
                //            if (_currentBusinessObject != null)
                //            {
                //                _ucTblGrpAssign.SetStateFromParent(_currentBusinessObject.Tb_ApVrsn_Id, _currentBusinessObject.Tb_Id);
                //            }
                //            break;
                //    }

            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    _FLG_IsLoaded = true;
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", IfxTraceCategory.Leave);
            //}
            }

        #endregion Method Extentions For Custom Code



        #region Standard CodeGen Methods Subject to Customization



        #endregion Standard CodeGen Methods Subject to Customization



        }
    }
