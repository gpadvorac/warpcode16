using System;
using System.Windows;
using System.Diagnostics;
using System.Collections.Generic;
using Infragistics.Controls.Grids;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  11/17/2016 4:33:36 PM

namespace UIControls
{
    public partial class ucWcApplicationVersionList
    {

        #region Column Groups

        List<ColumnGroup> _columnGroups = new List<ColumnGroup>();

        public partial class ColumnGroupNames
        {
            public const string Navigation = "Navigation";
            public const string Navigation_Header = "Navigation";
            public const string Paths = "Paths";
            public const string Paths_Header = "Paths";
            public const string Notesstatus = "Notesstatus";
            public const string Notesstatus_Header = "Notes, Status";
            public const string Actioncolumns = "Actioncolumns";
            public const string Actioncolumns_Header = "Action Columns";
        }

        public class ColumnGroup
        {
            string _groupName;
            string _groupHeader;
            bool _isVisible = true;
            bool _isDefault = false;
            List<Column> _fields = new List<Column>();


            public ColumnGroup(string groupName, string groupHeader, bool isDefault)
            {
                _groupName = groupName;
                _groupHeader = groupHeader;
                _isDefault = isDefault;
            }

            public string GroupName
            {
                get { return _groupName; }
                set { _groupName = value; }
            }

            public string GroupHeader
            {
                get { return _groupHeader; }
                set { _groupHeader = value; }
            }

            public bool IsVisible
            {
                get { return _isVisible; }
                set
                {
                    _isVisible = value;
                    if (_isVisible == true)
                    {
                        foreach (var item in _fields)
                        {
                            if (item != null)
                            {
                                item.Visibility = Visibility.Visible;
                            }
                        }
                    }
                }
            }

            public bool IsDefault
            {
                get { return _isDefault; }
                set { _isDefault = value; }
            }

            public List<Column> Fields
            {
                get { return _fields; }
                set { _fields = value; }
            }
        }

        void Configure_cmbGrouping()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Configure_cmbGrouping", IfxTraceCategory.Enter);

                #region Create Groups
                //ColumnGroup grp;

                // Navigation
                //grp = new ColumnGroup(ColumnGroupNames.Navigation, ColumnGroupNames.Navigation_Header, true);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_MajorVersion"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_MinorVersion"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_VersionIteration"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_Server"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_DbName"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_IsMulticultural"]);
                //_columnGroups.Add(grp);
                // Paths
                //grp = new ColumnGroup(ColumnGroupNames.Paths, ColumnGroupNames.Paths_Header, false);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_MajorVersion"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_SolutionPath"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_DefaultUIAssembly"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_DefaultUIAssemblyPath"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_DefaultWireTypePath"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_WebServerURL"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_WebsiteCodeFolderPath"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_WebserviceCodeFolderPath"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_StoredProcCodeFolder"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_DefaultConnectionCodeId"]);
                //_columnGroups.Add(grp);
                // NotesStatus
                //grp = new ColumnGroup(ColumnGroupNames.Notesstatus, ColumnGroupNames.Notesstatus_Header, false);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_Notes"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_IsActiveRow"]);
                //grp.Fields.Add(navList.Columns.DataColumns["UserName"]);
                //grp.Fields.Add(navList.Columns.DataColumns["ApVrsn_LastModifiedDate"]);
                //_columnGroups.Add(grp);
                // ActionColumns
                //grp = new ColumnGroup(ColumnGroupNames.Actioncolumns, ColumnGroupNames.Actioncolumns_Header, false);
                //grp.Fields.Add(navList.Columns.DataColumns["AttachmentCount"]);
                //grp.Fields.Add(navList.Columns.DataColumns["DiscussionCount"]);
                //_columnGroups.Add(grp);

                //Configure_cmbGrouping_Cust();

                //cmbGrouping.ItemsSource = _columnGroups;
                //cmbGrouping.DisplayMemberPath = "GroupHeader";

                //SetDefaultGroupsVisiblity();

                #endregion Create Groups

                //foreach (var item in cmbGrouping.Items)
                //{
                    //if (((ColumnGroup)item.Data).IsDefault == true)
                    //{
                    //    item.IsSelected = true;
                    //}
                    //else
                    //{
                    //    ((ColumnGroup)item.Data).IsVisible = false;
                    //}
                //}

                //cmbGrouping.SelectionChanged += new EventHandler(cmbGrouping_SelectionChanged);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Configure_cmbGrouping", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Configure_cmbGrouping", IfxTraceCategory.Leave);
            }
        }

        void SetDefaultGroupsVisiblity()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "cmbGridDataSource_DataSource", IfxTraceCategory.Enter);
                // Hide all columns in all groups
                foreach (var grp in _columnGroups)
                {
                    foreach (var col in grp.Fields)
                    {
                        try
                        {
                            col.Visibility = System.Windows.Visibility.Collapsed;
                        }
                        catch (Exception exx)
                        {
                            IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultGroupsVisiblity", new Exception("Column Group: " + grp.GroupHeader + " had one or more null group fields."));
                        }
                    }
                    grp.IsVisible = false;
                }

                // Show all columns in the default groups
                foreach (var grp in _columnGroups)
                {
                    if (grp.IsDefault == true)
                    {
                        grp.IsVisible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultGroupsVisiblity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultGroupsVisiblity", IfxTraceCategory.Leave);
            }
        }


        void cmbGrouping_SelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "cmbGrouping_SelectionChanged", IfxTraceCategory.Enter);
                // Loop through the selected items in the combo and see if any don't exist in _selectedGroups.
                // If they dont exist in _selectedGroups, than this means we are selecting (checing) that item and it needs to be added to _selectedGroups
                //  If we identify the item being checked, then we need to now run our other code to manage our visibility settings
                //foreach (var item in cmbGrouping.Items)
                //{
                //    if (item.IsSelected == true)
                //    {
                //        if (((ColumnGroup)item.Data).IsVisible == false)
                //        {
                //            // This item was just checked becuase it's checked in the list, but its visibility isnt set to true yet
                //            ((ColumnGroup)item.Data).IsVisible = true;
                //        }
                //    }
                //    else
                //    {
                //        if (((ColumnGroup)item.Data).IsVisible == true)
                //        {
                //            // This item was just UN-checked becuase it's un-checked in the list, but its visibility isnt set to false yet
                //            HideGroupColumns((ColumnGroup)item.Data);
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "cmbGrouping_SelectionChanged", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "cmbGrouping_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        void HideGroupColumns(ColumnGroup groupToHide)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGroupColumns", IfxTraceCategory.Enter);
                // Loop thru this groups columns to hide
                foreach (var columnToHide in groupToHide.Fields)
                {
                    bool matchFound = false;
                    // Loop thru all groups
                    foreach (var colGrp in _columnGroups)
                    {
                        // If its not the group we're hiding and the group is visible, check to see if any of it's column are the same as this colum we are currently going to hide
                        if (colGrp.GroupName != groupToHide.GroupName && colGrp.IsVisible == true)
                        {
                            foreach (var col in colGrp.Fields)
                            {
                                if (col.Key == columnToHide.Key)
                                {
                                    matchFound = true;
                                    break;
                                }
                            }  //  foreach (var col in colGrp.Value.Fields)
                        }  //  if...

                        // If we found a match, don't hide the column becuase another visible group needs to show it
                        if (matchFound == true)
                        {
                            break;
                        }
                        // no match found so hide the column
                    }  //  foreach (var colGrp in _columnGroups)
                    if (matchFound == false)
                    {
                        if (columnToHide != null)  // Normaly we should not need this, but sometimes a null column gets code genned in here.
                        {
                            columnToHide.Visibility = System.Windows.Visibility.Collapsed;
                            groupToHide.IsVisible = false;
                        }
                    }
                }  // foreach (var columnToHide in groupToHide.Fields)
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGroupColumns", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGroupColumns", IfxTraceCategory.Leave);
            }
        }


        #endregion Column Groups


    }
}
