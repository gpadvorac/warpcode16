using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class WcDatabaseConnectionStringKeyConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcDatabaseConnectionStringKeyConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public WcDatabaseConnectionStringKeyConcurrencyList(WcDatabaseConnectionStringKey_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcDatabaseConnectionStringKeyConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Key Name", data.C.DbCnSK_Name, data.X.DbCnSK_Name));
                _concurrencyList.Add(new ConcurrencyItem("Is Default", data.C.DbCnSK_IsDefault, data.X.DbCnSK_IsDefault));
                _concurrencyList.Add(new ConcurrencyItem("Server", data.C.DbCnSK_Server, data.X.DbCnSK_Server));
                _concurrencyList.Add(new ConcurrencyItem("Database", data.C.DbCnSK_Database, data.X.DbCnSK_Database));
                _concurrencyList.Add(new ConcurrencyItem("UserName", data.C.DbCnSK_UserName, data.X.DbCnSK_UserName));
                _concurrencyList.Add(new ConcurrencyItem("Password", data.C.DbCnSK_Password, data.X.DbCnSK_Password));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.DbCnSK_IsActiveRow, data.X.DbCnSK_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



