using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class WcTableConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcTableConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public WcTableConcurrencyList(WcTable_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcTableConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Attachments", data.C.AttachmentCount, data.X.AttachmentCount));
                _concurrencyList.Add(new ConcurrencyItem("Discussions", data.C.DiscussionCount, data.X.DiscussionCount));
                _concurrencyList.Add(new ConcurrencyItem("Db Table Name", data.C.Tb_Name, data.X.Tb_Name));
                _concurrencyList.Add(new ConcurrencyItem("Entity Root Name", data.C.Tb_EntityRootName, data.X.Tb_EntityRootName));
                _concurrencyList.Add(new ConcurrencyItem("Variable Name", data.C.Tb_VariableName, data.X.Tb_VariableName));
                _concurrencyList.Add(new ConcurrencyItem("Screen Caption", data.C.Tb_ScreenCaption, data.X.Tb_ScreenCaption));
                _concurrencyList.Add(new ConcurrencyItem("Description", data.C.Tb_Description, data.X.Tb_Description));
                _concurrencyList.Add(new ConcurrencyItem("Development Notes", data.C.Tb_DevelopmentNote, data.X.Tb_DevelopmentNote));
                _concurrencyList.Add(new ConcurrencyItem("UI Assembly Name", data.C.Tb_UIAssemblyName, data.X.Tb_UIAssemblyName));
                _concurrencyList.Add(new ConcurrencyItem("UI Namespace", data.C.Tb_UINamespace, data.X.Tb_UINamespace));
                _concurrencyList.Add(new ConcurrencyItem("UI Assembly Path", data.C.Tb_UIAssemblyPath, data.X.Tb_UIAssemblyPath));
                _concurrencyList.Add(new ConcurrencyItem("Proxy Assembly Name", data.C.Tb_ProxyAssemblyName, data.X.Tb_ProxyAssemblyName));
                _concurrencyList.Add(new ConcurrencyItem("Proxy Assembly Path", data.C.Tb_ProxyAssemblyPath, data.X.Tb_ProxyAssemblyPath));
                _concurrencyList.Add(new ConcurrencyItem("Web Service Name", data.C.Tb_WebServiceName, data.X.Tb_WebServiceName));
                _concurrencyList.Add(new ConcurrencyItem("Web Service Folder", data.C.Tb_WebServiceFolder, data.X.Tb_WebServiceFolder));
                _concurrencyList.Add(new ConcurrencyItem("DataService Assembly Name", data.C.Tb_DataServiceAssemblyName, data.X.Tb_DataServiceAssemblyName));
                _concurrencyList.Add(new ConcurrencyItem("Data Service Path", data.C.Tb_DataServicePath, data.X.Tb_DataServicePath));
                _concurrencyList.Add(new ConcurrencyItem("WireType Assembly Name", data.C.Tb_WireTypeAssemblyName, data.X.Tb_WireTypeAssemblyName));
                _concurrencyList.Add(new ConcurrencyItem("WireType Path", data.C.Tb_WireTypePath, data.X.Tb_WireTypePath));
                _concurrencyList.Add(new ConcurrencyItem("Use Tiles In Props Screen", data.C.Tb_UseTilesInPropsScreen, data.X.Tb_UseTilesInPropsScreen));
                _concurrencyList.Add(new ConcurrencyItem("Use Grid Column Groups", data.C.Tb_UseGridColumnGroups, data.X.Tb_UseGridColumnGroups));
                _concurrencyList.Add(new ConcurrencyItem("Use Grid DataSource Combo", data.C.Tb_UseGridDataSourceCombo, data.X.Tb_UseGridDataSourceCombo));
                _concurrencyList.Add(new ConcurrencyItem("App Connection String Key", data.C.Tb_DbCnSK_Id, data.X.Tb_DbCnSK_Id));
                _concurrencyList.Add(new ConcurrencyItem("Use Legacy Connection Code", data.C.Tb_UseLegacyConnectionCode, data.X.Tb_UseLegacyConnectionCode));
                _concurrencyList.Add(new ConcurrencyItem("Pk Is Identity", data.C.Tb_PkIsIdentity, data.X.Tb_PkIsIdentity));
                _concurrencyList.Add(new ConcurrencyItem("Is Virtual Entity", data.C.Tb_IsVirtual, data.X.Tb_IsVirtual));
                _concurrencyList.Add(new ConcurrencyItem("Is Screen Place Holder", data.C.Tb_IsScreenPlaceHolder, data.X.Tb_IsScreenPlaceHolder));
                _concurrencyList.Add(new ConcurrencyItem("Is Not An Entity", data.C.Tb_IsNotEntity, data.X.Tb_IsNotEntity));
                _concurrencyList.Add(new ConcurrencyItem("Is Many 2 Many", data.C.Tb_IsMany2Many, data.X.Tb_IsMany2Many));
                _concurrencyList.Add(new ConcurrencyItem("Pull UserName In Sproc", data.C.Tb_UseLastModifiedByUserNameInSproc, data.X.Tb_UseLastModifiedByUserNameInSproc));
                _concurrencyList.Add(new ConcurrencyItem("Use User TimeStamp", data.C.Tb_UseUserTimeStamp, data.X.Tb_UseUserTimeStamp));
                _concurrencyList.Add(new ConcurrencyItem("Use For Audit", data.C.Tb_UseForAudit, data.X.Tb_UseForAudit));
                _concurrencyList.Add(new ConcurrencyItem("Is Allow Delete", data.C.Tb_IsAllowDelete, data.X.Tb_IsAllowDelete));
                _concurrencyList.Add(new ConcurrencyItem("Is Menu Row Visible", data.C.Tb_CnfgGdMnu_MenuRow_IsVisible, data.X.Tb_CnfgGdMnu_MenuRow_IsVisible));
                _concurrencyList.Add(new ConcurrencyItem("Is Grid Tools Btn Visible", data.C.Tb_CnfgGdMnu_GridTools_IsVisible, data.X.Tb_CnfgGdMnu_GridTools_IsVisible));
                _concurrencyList.Add(new ConcurrencyItem("Is Split Screen Btn Visible", data.C.Tb_CnfgGdMnu_SplitScreen_IsVisible, data.X.Tb_CnfgGdMnu_SplitScreen_IsVisible));
                _concurrencyList.Add(new ConcurrencyItem("Is Split Screen Default Split", data.C.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default, data.X.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default));
                _concurrencyList.Add(new ConcurrencyItem("Nav Column Width", data.C.Tb_CnfgGdMnu_NavColumnWidth, data.X.Tb_CnfgGdMnu_NavColumnWidth));
                _concurrencyList.Add(new ConcurrencyItem("Is Read Only", data.C.Tb_CnfgGdMnu_IsReadOnly, data.X.Tb_CnfgGdMnu_IsReadOnly));
                _concurrencyList.Add(new ConcurrencyItem("Is Allow New Row", data.C.Tb_CnfgGdMnu_IsAllowNewRow, data.X.Tb_CnfgGdMnu_IsAllowNewRow));
                _concurrencyList.Add(new ConcurrencyItem("Is Excel Export Btn Visible", data.C.Tb_CnfgGdMnu_ExcelExport_IsVisible, data.X.Tb_CnfgGdMnu_ExcelExport_IsVisible));
                _concurrencyList.Add(new ConcurrencyItem("Is Column Chooser Btn Visible", data.C.Tb_CnfgGdMnu_ColumnChooser_IsVisible, data.X.Tb_CnfgGdMnu_ColumnChooser_IsVisible));
                _concurrencyList.Add(new ConcurrencyItem("Is Show-Hide Column Btn Visible", data.C.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible, data.X.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible));
                _concurrencyList.Add(new ConcurrencyItem("Is Refresh Grid Btn Visible", data.C.Tb_CnfgGdMnu_RefreshGrid_IsVisible, data.X.Tb_CnfgGdMnu_RefreshGrid_IsVisible));
                _concurrencyList.Add(new ConcurrencyItem("Is Collaps All Parent NavGrids Btn Visible", data.C.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible, data.X.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible));
                _concurrencyList.Add(new ConcurrencyItem("Input Complete", data.C.Tb_IsInputComplete, data.X.Tb_IsInputComplete));
                _concurrencyList.Add(new ConcurrencyItem("Is CodeGen", data.C.Tb_IsCodeGen, data.X.Tb_IsCodeGen));
                _concurrencyList.Add(new ConcurrencyItem("Is Ready For CodeGen", data.C.Tb_IsReadyCodeGen, data.X.Tb_IsReadyCodeGen));
                _concurrencyList.Add(new ConcurrencyItem("Is CodeGen Complete", data.C.Tb_IsCodeGenComplete, data.X.Tb_IsCodeGenComplete));
                _concurrencyList.Add(new ConcurrencyItem("Tag For CodeGen", data.C.Tb_IsTagForCodeGen, data.X.Tb_IsTagForCodeGen));
                _concurrencyList.Add(new ConcurrencyItem("Is Tag For Other", data.C.Tb_IsTagForOther, data.X.Tb_IsTagForOther));
                _concurrencyList.Add(new ConcurrencyItem("Table Groups", data.C.Tb_TableGroups, data.X.Tb_TableGroups));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.Tb_IsActiveRow, data.X.Tb_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



