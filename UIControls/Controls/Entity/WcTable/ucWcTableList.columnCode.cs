using System;
using System.Windows;
using System.Windows.Controls;
using Ifx.SL;
using TypeServices;
using vControls;
using EntityBll.SL;
using Infragistics.Controls.Editors;
using vUICommon;
using EntityWireTypeSL;
using Infragistics.Controls.Grids;
using vComboDataTypes;
using System.Collections.Generic;
using vUICommon.Controls;
using UIControls.Globalization.WcTable;
namespace UIControls
{
    public partial class ucWcTableList
    {

        
        #region GridCell Editor Events
        

        private void XamGridTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                if ((ctl.Text.Length>0) && (ctl.Text.Substring(ctl.Text.Length - 1, 1) == " "))
                {
                    return;
                }
                WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                if (obj == null) { return; }
                //switch (ctl.Name)
                //{
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vTextColumn_TextChanged(object sender, vTextColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (e.Key)
                    {
                        case "Tb_Name":
                            obj.Tb_Name = ctl.Text;
                            break;
                        case "Tb_EntityRootName":
                            obj.Tb_EntityRootName = ctl.Text;
                            break;
                        case "Tb_VariableName":
                            obj.Tb_VariableName = ctl.Text;
                            break;
                        case "Tb_ScreenCaption":
                            obj.Tb_ScreenCaption = ctl.Text;
                            break;
                        case "Tb_Description":
                            obj.Tb_Description = ctl.Text;
                            break;
                        case "Tb_DevelopmentNote":
                            obj.Tb_DevelopmentNote = ctl.Text;
                            break;
                        case "Tb_UIAssemblyName":
                            obj.Tb_UIAssemblyName = ctl.Text;
                            break;
                        case "Tb_UINamespace":
                            obj.Tb_UINamespace = ctl.Text;
                            break;
                        case "Tb_UIAssemblyPath":
                            obj.Tb_UIAssemblyPath = ctl.Text;
                            break;
                        case "Tb_ProxyAssemblyName":
                            obj.Tb_ProxyAssemblyName = ctl.Text;
                            break;
                        case "Tb_ProxyNamespace":
                            obj.Tb_ProxyNamespace = ctl.Text;
                            break;
                        case "Tb_ProxyAssemblyPath":
                            obj.Tb_ProxyAssemblyPath = ctl.Text;
                            break;
                        case "Tb_WireTypeAssemblyName":
                            obj.Tb_WireTypeAssemblyName = ctl.Text;
                            break;
                        case "Tb_WireTypeNamespace":
                            obj.Tb_WireTypeNamespace = ctl.Text;
                            break;
                        case "Tb_WireTypePath":
                            obj.Tb_WireTypePath = ctl.Text;
                            break;
                        case "Tb_WebServiceName":
                            obj.Tb_WebServiceName = ctl.Text;
                            break;
                        case "Tb_WebServiceFolder":
                            obj.Tb_WebServiceFolder = ctl.Text;
                            break;
                        case "Tb_DataServiceAssemblyName":
                            obj.Tb_DataServiceAssemblyName = ctl.Text;
                            break;
                        case "Tb_DataServiceNamespace":
                            obj.Tb_DataServiceNamespace = ctl.Text;
                            break;
                        case "Tb_DataServicePath":
                            obj.Tb_DataServicePath = ctl.Text;
                            break;
                        case "Tb_CnfgGdMnu_NavColumnWidth":
                            obj.Tb_CnfgGdMnu_NavColumnWidth_asString = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vDecimalColumn_TextChanged(object sender, vDecimalColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamComboEditor comboEditor = sender as XamComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_SelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (ctl.Name)
                    {
                        case "Tb_DbCnSK_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Tb_DbCnSK_Id = null;
                            }
                            else
                            {
                                obj.Tb_DbCnSK_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "Tb_ApDbScm_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Tb_ApDbScm_Id = null;
                            }
                            else
                            {
                                obj.Tb_ApDbScm_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_SelectionChanged(object sender, vXamComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                // The next 2 lines are for the Not In List feature.  If the 'Non-In-List' event is fireing, we dont want to run this event right now.
                if (ctl.SelectedItem == null) { return; }
                if (((ComboItem)ctl.SelectedItem).Id == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (e.Key)
                    {
                        case "Tb_DbCnSK_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Tb_DbCnSK_Id = null;
                            }
                            else
                            {
                                obj.Tb_DbCnSK_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "Tb_ApDbScm_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Tb_ApDbScm_Id = null;
                            }
                            else
                            {
                                obj.Tb_ApDbScm_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
                vXamComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Enter);
                if (e.Key == System.Windows.Input.Key.Escape)
                {
                    WcTable_Bll obj = _activeRow.Data as WcTable_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_KeyUp(object sender, vXamComboColumnKeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Enter);
                if (e.XamComboColumnKeyEventArgs.Key == System.Windows.Input.Key.Escape)
                {
                    WcTable_Bll obj = _activeRow.Data as WcTable_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void XamMultiColumnComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamMultiColumnComboEditor comboEditor = sender as XamMultiColumnComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void vXamMultiColumnComboColumn_SelectionChanged(object sender, vXamMultiColumnComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vXamMultiColumnComboEditor ctl = sender as vXamMultiColumnComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (e.Key)
                    {
                    }
                }
                vXamMultiColumnComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridDatePicker_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                if (obj == null) { return; }
                switch (ctl.Name)
                {
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Leave);
            }
        }





        private void XamColorPicker_SelectedColorChanged(object sender, SelectedColorChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamColorPicker ctl = sender as XamColorPicker;
                if (ctl == null) { return; }
                WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                if (obj == null) { return; }
                switch (((CellControl)ctl.Parent).Cell.Column.Key)
                {
                }
                XamColorPicker_SelectedColorChanged_CustomCode(ctl, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Leave);
            }
        }





        //  may not need this as the prop gets hit through binding.
        public void vDatePickerColumn_TextChanged(object sender, vDatePickerColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                vDatePickerColumn_TextChanged_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vCheckColumn_CheckedChanged(object sender, vCheckColumnCheckedChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Enter);
                //if (_isRowInEditMode == false) { return; }
                vCheckBox ctl = sender as vCheckBox;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (e.Key)
                    {
                    case "Tb_UseLegacyConnectionCode":
                        obj.Tb_UseLegacyConnectionCode = (bool)ctl.IsChecked;
                        break;
                    case "Tb_UseTilesInPropsScreen":
                        obj.Tb_UseTilesInPropsScreen = (bool)ctl.IsChecked;
                        break;
                    case "Tb_UseGridColumnGroups":
                        obj.Tb_UseGridColumnGroups = (bool)ctl.IsChecked;
                        break;
                    case "Tb_UseGridDataSourceCombo":
                        obj.Tb_UseGridDataSourceCombo = (bool)ctl.IsChecked;
                        break;
                    case "Tb_PkIsIdentity":
                        obj.Tb_PkIsIdentity = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsVirtual":
                        obj.Tb_IsVirtual = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsScreenPlaceHolder":
                        obj.Tb_IsScreenPlaceHolder = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsNotEntity":
                        obj.Tb_IsNotEntity = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsMany2Many":
                        obj.Tb_IsMany2Many = (bool)ctl.IsChecked;
                        break;
                    case "Tb_UseLastModifiedByUserNameInSproc":
                        obj.Tb_UseLastModifiedByUserNameInSproc = (bool)ctl.IsChecked;
                        break;
                    case "Tb_UseUserTimeStamp":
                        obj.Tb_UseUserTimeStamp = (bool)ctl.IsChecked;
                        break;
                    case "Tb_UseForAudit":
                        obj.Tb_UseForAudit = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsAllowDelete":
                        obj.Tb_IsAllowDelete = (bool)ctl.IsChecked;
                        break;
                    case "Tb_CnfgGdMnu_MenuRow_IsVisible":
                        obj.Tb_CnfgGdMnu_MenuRow_IsVisible = (bool)ctl.IsChecked;
                        break;
                    case "Tb_CnfgGdMnu_GridTools_IsVisible":
                        obj.Tb_CnfgGdMnu_GridTools_IsVisible = (bool)ctl.IsChecked;
                        break;
                    case "Tb_CnfgGdMnu_SplitScreen_IsVisible":
                        obj.Tb_CnfgGdMnu_SplitScreen_IsVisible = (bool)ctl.IsChecked;
                        break;
                    case "Tb_CnfgGdMnu_SplitScreen_IsSplit_Default":
                        obj.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default = (bool)ctl.IsChecked;
                        break;
                    case "Tb_CnfgGdMnu_IsReadOnly":
                        obj.Tb_CnfgGdMnu_IsReadOnly = (bool)ctl.IsChecked;
                        break;
                    case "Tb_CnfgGdMnu_IsAllowNewRow":
                        obj.Tb_CnfgGdMnu_IsAllowNewRow = (bool)ctl.IsChecked;
                        break;
                    case "Tb_CnfgGdMnu_ExcelExport_IsVisible":
                        obj.Tb_CnfgGdMnu_ExcelExport_IsVisible = (bool)ctl.IsChecked;
                        break;
                    case "Tb_CnfgGdMnu_ColumnChooser_IsVisible":
                        obj.Tb_CnfgGdMnu_ColumnChooser_IsVisible = (bool)ctl.IsChecked;
                        break;
                    case "Tb_CnfgGdMnu_ShowHideColBtn_IsVisible":
                        obj.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible = (bool)ctl.IsChecked;
                        break;
                    case "Tb_CnfgGdMnu_RefreshGrid_IsVisible":
                        obj.Tb_CnfgGdMnu_RefreshGrid_IsVisible = (bool)ctl.IsChecked;
                        break;
                    case "Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible":
                        obj.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsInputComplete":
                        obj.Tb_IsInputComplete = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsCodeGen":
                        obj.Tb_IsCodeGen = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsReadyCodeGen":
                        obj.Tb_IsReadyCodeGen = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsCodeGenComplete":
                        obj.Tb_IsCodeGenComplete = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsTagForCodeGen":
                        obj.Tb_IsTagForCodeGen = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsTagForOther":
                        obj.Tb_IsTagForOther = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsActiveRow":
                        obj.Tb_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Leave);
            }
        }

        #endregion GridCell Editor Events

        
        #region Grid Other Events and Event Support Methods
        

        void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Enter);
                Cell cell = (Cell)navList.ActiveCell;
                IvControlsValidation ctl = cell.Control.Content as IvControlsValidation;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;

                    switch (e.PropertyName)
                    {
                        case "AttachmentCount":
                            SetGridCellValidationAppeance(ctl, obj, "AttachmentCount");
                            break;
                        case "DiscussionCount":
                            SetGridCellValidationAppeance(ctl, obj, "DiscussionCount");
                            break;
                        case "ImportTable":
                            SetGridCellValidationAppeance(ctl, obj, "ImportTable");
                            break;
                        case "Tb_Name":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_Name");
                            break;
                        case "TableInDatabase":
                            SetGridCellValidationAppeance(ctl, obj, "TableInDatabase");
                            break;
                        case "Tb_EntityRootName":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_EntityRootName");
                            break;
                        case "Tb_VariableName":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_VariableName");
                            break;
                        case "Tb_ScreenCaption":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_ScreenCaption");
                            break;
                        case "Tb_Description":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_Description");
                            break;
                        case "Tb_DevelopmentNote":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_DevelopmentNote");
                            break;
                        case "Tb_UseLegacyConnectionCode":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseLegacyConnectionCode");
                            break;
                        case "Tb_DbCnSK_Id":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_DbCnSK_Id");
                            break;
                        case "Tb_ApDbScm_Id":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_ApDbScm_Id");
                            break;
                        case "Tb_UIAssemblyName":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UIAssemblyName");
                            break;
                        case "Tb_UINamespace":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UINamespace");
                            break;
                        case "Tb_UIAssemblyPath":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UIAssemblyPath");
                            break;
                        case "Tb_ProxyAssemblyName":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_ProxyAssemblyName");
                            break;
                        case "Tb_ProxyNamespace":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_ProxyNamespace");
                            break;
                        case "Tb_ProxyAssemblyPath":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_ProxyAssemblyPath");
                            break;
                        case "Tb_WireTypeAssemblyName":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_WireTypeAssemblyName");
                            break;
                        case "Tb_WireTypeNamespace":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_WireTypeNamespace");
                            break;
                        case "Tb_WireTypePath":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_WireTypePath");
                            break;
                        case "Tb_WebServiceName":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_WebServiceName");
                            break;
                        case "Tb_WebServiceFolder":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_WebServiceFolder");
                            break;
                        case "Tb_DataServiceAssemblyName":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_DataServiceAssemblyName");
                            break;
                        case "Tb_DataServiceNamespace":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_DataServiceNamespace");
                            break;
                        case "Tb_DataServicePath":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_DataServicePath");
                            break;
                        case "Tb_UseTilesInPropsScreen":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseTilesInPropsScreen");
                            break;
                        case "Tb_UseGridColumnGroups":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseGridColumnGroups");
                            break;
                        case "Tb_UseGridDataSourceCombo":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseGridDataSourceCombo");
                            break;
                        case "Tb_PkIsIdentity":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_PkIsIdentity");
                            break;
                        case "Tb_IsVirtual":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsVirtual");
                            break;
                        case "Tb_IsScreenPlaceHolder":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsScreenPlaceHolder");
                            break;
                        case "Tb_IsNotEntity":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsNotEntity");
                            break;
                        case "Tb_IsMany2Many":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsMany2Many");
                            break;
                        case "Tb_UseLastModifiedByUserNameInSproc":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseLastModifiedByUserNameInSproc");
                            break;
                        case "Tb_UseUserTimeStamp":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseUserTimeStamp");
                            break;
                        case "Tb_UseForAudit":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseForAudit");
                            break;
                        case "Tb_IsAllowDelete":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsAllowDelete");
                            break;
                        case "Tb_CnfgGdMnu_MenuRow_IsVisible":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_CnfgGdMnu_MenuRow_IsVisible");
                            break;
                        case "Tb_CnfgGdMnu_GridTools_IsVisible":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_CnfgGdMnu_GridTools_IsVisible");
                            break;
                        case "Tb_CnfgGdMnu_SplitScreen_IsVisible":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_CnfgGdMnu_SplitScreen_IsVisible");
                            break;
                        case "Tb_CnfgGdMnu_SplitScreen_IsSplit_Default":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_CnfgGdMnu_SplitScreen_IsSplit_Default");
                            break;
                        case "Tb_CnfgGdMnu_NavColumnWidth":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_CnfgGdMnu_NavColumnWidth");
                            break;
                        case "Tb_CnfgGdMnu_IsReadOnly":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_CnfgGdMnu_IsReadOnly");
                            break;
                        case "Tb_CnfgGdMnu_IsAllowNewRow":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_CnfgGdMnu_IsAllowNewRow");
                            break;
                        case "Tb_CnfgGdMnu_ExcelExport_IsVisible":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_CnfgGdMnu_ExcelExport_IsVisible");
                            break;
                        case "Tb_CnfgGdMnu_ColumnChooser_IsVisible":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_CnfgGdMnu_ColumnChooser_IsVisible");
                            break;
                        case "Tb_CnfgGdMnu_ShowHideColBtn_IsVisible":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_CnfgGdMnu_ShowHideColBtn_IsVisible");
                            break;
                        case "Tb_CnfgGdMnu_RefreshGrid_IsVisible":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_CnfgGdMnu_RefreshGrid_IsVisible");
                            break;
                        case "Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible");
                            break;
                        case "Tb_IsInputComplete":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsInputComplete");
                            break;
                        case "Tb_IsCodeGen":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsCodeGen");
                            break;
                        case "Tb_IsReadyCodeGen":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsReadyCodeGen");
                            break;
                        case "Tb_IsCodeGenComplete":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsCodeGenComplete");
                            break;
                        case "Tb_IsTagForCodeGen":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsTagForCodeGen");
                            break;
                        case "Tb_IsTagForOther":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsTagForOther");
                            break;
                        case "Tb_TableGroups":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_TableGroups");
                            break;
                        case "Tb_IsActiveRow":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsActiveRow");
                            break;
                        case "UserName":
                            SetGridCellValidationAppeance(ctl, obj, "UserName");
                            break;
                        case "Tb_LastModifiedDate":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_LastModifiedDate");
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Leave);
            }
        }

        #endregion Grid Other Events and Event Support Methods

        
        #region Methods


        void AssignTextLengthLabels()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Enter);

                ((vTextColumn)navList.Columns["Tb_Name"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["TableInDatabase"]).MaxTextLength = 128;
                ((vTextColumn)navList.Columns["Tb_EntityRootName"]).MaxTextLength = 75;
                ((vTextColumn)navList.Columns["Tb_VariableName"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_ScreenCaption"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_Description"]).MaxTextLength = 2000;
                ((vTextColumn)navList.Columns["Tb_DevelopmentNote"]).MaxTextLength = 2000;
                ((vTextColumn)navList.Columns["Tb_UIAssemblyName"]).MaxTextLength = 75;
                ((vTextColumn)navList.Columns["Tb_UINamespace"]).MaxTextLength = 75;
                ((vTextColumn)navList.Columns["Tb_UIAssemblyPath"]).MaxTextLength = 250;
                ((vTextColumn)navList.Columns["Tb_ProxyAssemblyName"]).MaxTextLength = 75;
                ((vTextColumn)navList.Columns["Tb_ProxyNamespace"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_ProxyAssemblyPath"]).MaxTextLength = 250;
                ((vTextColumn)navList.Columns["Tb_WireTypeAssemblyName"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_WireTypeNamespace"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_WireTypePath"]).MaxTextLength = 250;
                ((vTextColumn)navList.Columns["Tb_WebServiceName"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_WebServiceFolder"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_DataServiceAssemblyName"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_DataServiceNamespace"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_DataServicePath"]).MaxTextLength = 250;
                ((vTextColumn)navList.Columns["Tb_TableGroups"]).MaxTextLength = 500;
                ((vTextColumn)navList.Columns["UserName"]).MaxTextLength = 60;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Leave);
            }
        }



        void ConfigureColumnHeaderTooltips()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Enter);

                    // AttachmentCount
                    ((IvColumn)navList.Columns["AttachmentCount"]).HeaderToolTipCaption = StringsWcTableList.AttachmentCount_Vbs; 
                    ((IvColumn)navList.Columns["AttachmentCount"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.AttachmentCount_1};
                    ((IvColumn)navList.Columns["AttachmentCount"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // DiscussionCount
                    ((IvColumn)navList.Columns["DiscussionCount"]).HeaderToolTipCaption = StringsWcTableList.DiscussionCount_Vbs; 
                    ((IvColumn)navList.Columns["DiscussionCount"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.DiscussionCount_1};
                    ((IvColumn)navList.Columns["DiscussionCount"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ImportTable
                    ((IvColumn)navList.Columns["ImportTable"]).HeaderToolTipCaption = StringsWcTableList.ImportTable_Vbs; 
                    ((IvColumn)navList.Columns["ImportTable"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.ImportTable_1};
                    ((IvColumn)navList.Columns["ImportTable"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_Name
                    ((IvColumn)navList.Columns["Tb_Name"]).HeaderToolTipCaption = StringsWcTableList.Tb_Name_Vbs; 
                    ((IvColumn)navList.Columns["Tb_Name"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_Name_1, StringsWcTableListTooltips.Tb_Name_2, StringsWcTableListTooltips.Tb_Name_3};
                    ((IvColumn)navList.Columns["Tb_Name"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TableInDatabase
                    ((IvColumn)navList.Columns["TableInDatabase"]).HeaderToolTipCaption = StringsWcTableList.TableInDatabase_Vbs; 
                    ((IvColumn)navList.Columns["TableInDatabase"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.TableInDatabase_1};
                    ((IvColumn)navList.Columns["TableInDatabase"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_EntityRootName
                    ((IvColumn)navList.Columns["Tb_EntityRootName"]).HeaderToolTipCaption = StringsWcTableList.Tb_EntityRootName_Vbs; 
                    ((IvColumn)navList.Columns["Tb_EntityRootName"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_EntityRootName_1, StringsWcTableListTooltips.Tb_EntityRootName_2, StringsWcTableListTooltips.Tb_EntityRootName_3};
                    ((IvColumn)navList.Columns["Tb_EntityRootName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_VariableName
                    ((IvColumn)navList.Columns["Tb_VariableName"]).HeaderToolTipCaption = StringsWcTableList.Tb_VariableName_Vbs; 
                    ((IvColumn)navList.Columns["Tb_VariableName"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_VariableName_1, StringsWcTableListTooltips.Tb_VariableName_2, StringsWcTableListTooltips.Tb_VariableName_3};
                    ((IvColumn)navList.Columns["Tb_VariableName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_ScreenCaption
                    ((IvColumn)navList.Columns["Tb_ScreenCaption"]).HeaderToolTipCaption = StringsWcTableList.Tb_ScreenCaption_Vbs; 
                    ((IvColumn)navList.Columns["Tb_ScreenCaption"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_ScreenCaption_1};
                    ((IvColumn)navList.Columns["Tb_ScreenCaption"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_UseLegacyConnectionCode
                    ((IvColumn)navList.Columns["Tb_UseLegacyConnectionCode"]).HeaderToolTipCaption = StringsWcTableList.Tb_UseLegacyConnectionCode_Vbs; 
                    ((IvColumn)navList.Columns["Tb_UseLegacyConnectionCode"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_UseLegacyConnectionCode_1};
                    ((IvColumn)navList.Columns["Tb_UseLegacyConnectionCode"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_DbCnSK_Id
                    ((vComboColumnBase)navList.Columns["Tb_DbCnSK_Id"]).HeaderToolTipCaption = StringsWcTableList.Tb_DbCnSK_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["Tb_DbCnSK_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_DbCnSK_Id_1};
                    ((vComboColumnBase)navList.Columns["Tb_DbCnSK_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_ApDbScm_Id
                    ((vComboColumnBase)navList.Columns["Tb_ApDbScm_Id"]).HeaderToolTipCaption = StringsWcTableList.Tb_ApDbScm_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["Tb_ApDbScm_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_UIAssemblyPath
                    ((IvColumn)navList.Columns["Tb_UIAssemblyPath"]).HeaderToolTipCaption = StringsWcTableList.Tb_UIAssemblyPath_Vbs; 
                    ((IvColumn)navList.Columns["Tb_UIAssemblyPath"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_UIAssemblyPath_1};
                    ((IvColumn)navList.Columns["Tb_UIAssemblyPath"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_WireTypeAssemblyName
                    ((IvColumn)navList.Columns["Tb_WireTypeAssemblyName"]).HeaderToolTipCaption = StringsWcTableList.Tb_WireTypeAssemblyName_Vbs; 
                    ((IvColumn)navList.Columns["Tb_WireTypeAssemblyName"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_WireTypeAssemblyName_1};
                    ((IvColumn)navList.Columns["Tb_WireTypeAssemblyName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_WireTypePath
                    ((IvColumn)navList.Columns["Tb_WireTypePath"]).HeaderToolTipCaption = StringsWcTableList.Tb_WireTypePath_Vbs; 
                    ((IvColumn)navList.Columns["Tb_WireTypePath"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_WireTypePath_1, StringsWcTableListTooltips.Tb_WireTypePath_2};
                    ((IvColumn)navList.Columns["Tb_WireTypePath"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_WebServiceFolder
                    ((IvColumn)navList.Columns["Tb_WebServiceFolder"]).HeaderToolTipCaption = StringsWcTableList.Tb_WebServiceFolder_Vbs; 
                    ((IvColumn)navList.Columns["Tb_WebServiceFolder"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_WebServiceFolder_1};
                    ((IvColumn)navList.Columns["Tb_WebServiceFolder"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_DataServicePath
                    ((IvColumn)navList.Columns["Tb_DataServicePath"]).HeaderToolTipCaption = StringsWcTableList.Tb_DataServicePath_Vbs; 
                    ((IvColumn)navList.Columns["Tb_DataServicePath"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_DataServicePath_1};
                    ((IvColumn)navList.Columns["Tb_DataServicePath"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_UseGridDataSourceCombo
                    ((IvColumn)navList.Columns["Tb_UseGridDataSourceCombo"]).HeaderToolTipCaption = StringsWcTableList.Tb_UseGridDataSourceCombo_Vbs; 
                    ((IvColumn)navList.Columns["Tb_UseGridDataSourceCombo"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_UseGridDataSourceCombo_1};
                    ((IvColumn)navList.Columns["Tb_UseGridDataSourceCombo"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_PkIsIdentity
                    ((IvColumn)navList.Columns["Tb_PkIsIdentity"]).HeaderToolTipCaption = StringsWcTableList.Tb_PkIsIdentity_Vbs; 
                    ((IvColumn)navList.Columns["Tb_PkIsIdentity"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_PkIsIdentity_1};
                    ((IvColumn)navList.Columns["Tb_PkIsIdentity"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsVirtual
                    ((IvColumn)navList.Columns["Tb_IsVirtual"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsVirtual_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsVirtual"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsVirtual_1, StringsWcTableListTooltips.Tb_IsVirtual_2, StringsWcTableListTooltips.Tb_IsVirtual_3};
                    ((IvColumn)navList.Columns["Tb_IsVirtual"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsScreenPlaceHolder
                    ((IvColumn)navList.Columns["Tb_IsScreenPlaceHolder"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsScreenPlaceHolder_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsScreenPlaceHolder"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsScreenPlaceHolder_1, StringsWcTableListTooltips.Tb_IsScreenPlaceHolder_2, StringsWcTableListTooltips.Tb_IsScreenPlaceHolder_3};
                    ((IvColumn)navList.Columns["Tb_IsScreenPlaceHolder"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsNotEntity
                    ((IvColumn)navList.Columns["Tb_IsNotEntity"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsNotEntity_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsNotEntity"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsNotEntity_1, StringsWcTableListTooltips.Tb_IsNotEntity_2};
                    ((IvColumn)navList.Columns["Tb_IsNotEntity"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsMany2Many
                    ((IvColumn)navList.Columns["Tb_IsMany2Many"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsMany2Many_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsMany2Many"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsMany2Many_1, StringsWcTableListTooltips.Tb_IsMany2Many_2, StringsWcTableListTooltips.Tb_IsMany2Many_3};
                    ((IvColumn)navList.Columns["Tb_IsMany2Many"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_UseLastModifiedByUserNameInSproc
                    ((IvColumn)navList.Columns["Tb_UseLastModifiedByUserNameInSproc"]).HeaderToolTipCaption = StringsWcTableList.Tb_UseLastModifiedByUserNameInSproc_Vbs; 
                    ((IvColumn)navList.Columns["Tb_UseLastModifiedByUserNameInSproc"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_UseLastModifiedByUserNameInSproc_1, StringsWcTableListTooltips.Tb_UseLastModifiedByUserNameInSproc_2};
                    ((IvColumn)navList.Columns["Tb_UseLastModifiedByUserNameInSproc"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_UseUserTimeStamp
                    ((IvColumn)navList.Columns["Tb_UseUserTimeStamp"]).HeaderToolTipCaption = StringsWcTableList.Tb_UseUserTimeStamp_Vbs; 
                    ((IvColumn)navList.Columns["Tb_UseUserTimeStamp"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_UseUserTimeStamp_1};
                    ((IvColumn)navList.Columns["Tb_UseUserTimeStamp"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_UseForAudit
                    ((IvColumn)navList.Columns["Tb_UseForAudit"]).HeaderToolTipCaption = StringsWcTableList.Tb_UseForAudit_Vbs; 
                    ((IvColumn)navList.Columns["Tb_UseForAudit"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_UseForAudit_1};
                    ((IvColumn)navList.Columns["Tb_UseForAudit"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_CnfgGdMnu_MenuRow_IsVisible
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_MenuRow_IsVisible"]).HeaderToolTipCaption = StringsWcTableList.Tb_CnfgGdMnu_MenuRow_IsVisible_Vbs; 
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_MenuRow_IsVisible"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_CnfgGdMnu_MenuRow_IsVisible_1, StringsWcTableListTooltips.Tb_CnfgGdMnu_MenuRow_IsVisible_2};
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_MenuRow_IsVisible"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_CnfgGdMnu_GridTools_IsVisible
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_GridTools_IsVisible"]).HeaderToolTipCaption = StringsWcTableList.Tb_CnfgGdMnu_GridTools_IsVisible_Vbs; 
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_GridTools_IsVisible"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_CnfgGdMnu_GridTools_IsVisible_1, StringsWcTableListTooltips.Tb_CnfgGdMnu_GridTools_IsVisible_2};
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_GridTools_IsVisible"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_CnfgGdMnu_SplitScreen_IsVisible
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_SplitScreen_IsVisible"]).HeaderToolTipCaption = StringsWcTableList.Tb_CnfgGdMnu_SplitScreen_IsVisible_Vbs; 
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_SplitScreen_IsVisible"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_SplitScreen_IsSplit_Default"]).HeaderToolTipCaption = StringsWcTableList.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default_Vbs; 
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_SplitScreen_IsSplit_Default"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_CnfgGdMnu_NavColumnWidth
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_NavColumnWidth"]).HeaderToolTipCaption = StringsWcTableList.Tb_CnfgGdMnu_NavColumnWidth_Vbs; 
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_NavColumnWidth"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_CnfgGdMnu_NavColumnWidth_1};
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_NavColumnWidth"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_CnfgGdMnu_IsAllowNewRow
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_IsAllowNewRow"]).HeaderToolTipCaption = StringsWcTableList.Tb_CnfgGdMnu_IsAllowNewRow_Vbs; 
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_IsAllowNewRow"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_CnfgGdMnu_IsAllowNewRow_1, StringsWcTableListTooltips.Tb_CnfgGdMnu_IsAllowNewRow_2};
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_IsAllowNewRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_CnfgGdMnu_ExcelExport_IsVisible
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_ExcelExport_IsVisible"]).HeaderToolTipCaption = StringsWcTableList.Tb_CnfgGdMnu_ExcelExport_IsVisible_Vbs; 
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_ExcelExport_IsVisible"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_CnfgGdMnu_ColumnChooser_IsVisible
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_ColumnChooser_IsVisible"]).HeaderToolTipCaption = StringsWcTableList.Tb_CnfgGdMnu_ColumnChooser_IsVisible_Vbs; 
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_ColumnChooser_IsVisible"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_ShowHideColBtn_IsVisible"]).HeaderToolTipCaption = StringsWcTableList.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible_Vbs; 
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_ShowHideColBtn_IsVisible"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_CnfgGdMnu_RefreshGrid_IsVisible
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_RefreshGrid_IsVisible"]).HeaderToolTipCaption = StringsWcTableList.Tb_CnfgGdMnu_RefreshGrid_IsVisible_Vbs; 
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_RefreshGrid_IsVisible"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible"]).HeaderToolTipCaption = StringsWcTableList.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible_Vbs; 
                    ((IvColumn)navList.Columns["Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsInputComplete
                    ((IvColumn)navList.Columns["Tb_IsInputComplete"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsInputComplete_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsInputComplete"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsInputComplete_1};
                    ((IvColumn)navList.Columns["Tb_IsInputComplete"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsCodeGen
                    ((IvColumn)navList.Columns["Tb_IsCodeGen"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsCodeGen_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsCodeGen"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsCodeGen_1};
                    ((IvColumn)navList.Columns["Tb_IsCodeGen"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsReadyCodeGen
                    ((IvColumn)navList.Columns["Tb_IsReadyCodeGen"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsReadyCodeGen_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsReadyCodeGen"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsReadyCodeGen_1};
                    ((IvColumn)navList.Columns["Tb_IsReadyCodeGen"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsCodeGenComplete
                    ((IvColumn)navList.Columns["Tb_IsCodeGenComplete"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsCodeGenComplete_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsCodeGenComplete"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsCodeGenComplete_1, StringsWcTableListTooltips.Tb_IsCodeGenComplete_2};
                    ((IvColumn)navList.Columns["Tb_IsCodeGenComplete"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsTagForCodeGen
                    ((IvColumn)navList.Columns["Tb_IsTagForCodeGen"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsTagForCodeGen_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsTagForCodeGen"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsTagForCodeGen_1};
                    ((IvColumn)navList.Columns["Tb_IsTagForCodeGen"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsTagForOther
                    ((IvColumn)navList.Columns["Tb_IsTagForOther"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsTagForOther_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsTagForOther"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsTagForOther_1};
                    ((IvColumn)navList.Columns["Tb_IsTagForOther"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsActiveRow
                    ((IvColumn)navList.Columns["Tb_IsActiveRow"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsActiveRow_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsActiveRow_1, StringsWcTableListTooltips.Tb_IsActiveRow_2};
                    ((IvColumn)navList.Columns["Tb_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)navList.Columns["UserName"]).HeaderToolTipCaption = StringsWcTableList.UserName_Vbs; 
                    ((IvColumn)navList.Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.UserName_1};
                    ((IvColumn)navList.Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_LastModifiedDate
                    ((IvColumn)navList.Columns["Tb_LastModifiedDate"]).HeaderToolTipCaption = StringsWcTableList.Tb_LastModifiedDate_Vbs; 
                    ((IvColumn)navList.Columns["Tb_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_LastModifiedDate_1};
                    ((IvColumn)navList.Columns["Tb_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();
                ConfigureColumnHeaderTooltips_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Leave);
            }
        }


        void Set_vXamComboColumn_ItemSources()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["Tb_DbCnSK_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcDatabaseConnectionStringKey_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["Tb_DbCnSK_Id"]).DisplayMemberPath = "ItemName";
                WcApplicationVersion_Bll_staticLists.WcDatabaseConnectionStringKey_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcDatabaseConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)navList.Columns["Tb_ApDbScm_Id"]).ItemsSource = WcApplication_Bll_staticLists.WcAppDbSchema_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["Tb_ApDbScm_Id"]).DisplayMemberPath = "ItemName";
                WcApplication_Bll_staticLists.WcAppDbSchema_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcAppDbSchema_ComboItemList_BindingListProperty_DataSourceUpdated);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Leave);
            }
        }

        void WcAppDbSchema_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["Tb_ApDbScm_Id"]).ItemsSource = WcApplication_Bll_staticLists.WcAppDbSchema_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcAppDbSchema_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcDatabaseConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["Tb_DbCnSK_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcDatabaseConnectionStringKey_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDatabaseConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        #region Combo ItemsSource for Non-Static Lists

        #endregion region Combo ItemsSource for Non-Static Lists

        void SetNewRowValidation()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Enter);
                if (_activeRow == null) { return; }
                WcTable_Bll obj = _activeRow.Data as WcTable_Bll;
                if (obj == null) { return; }
                // Note:  The lines to set the validaton styles below are not needed as the styles are being set from converters in the xaml style propeties, but they are left here incase needed in the future.  this is new code and we are unsure how it will evolve.
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Leave);
            }
        }

        #endregion Methods


    }
}
