﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationTypeServices;
using Ifx.SL;
using vUICommon;

namespace UIControls
{
    public partial class ucCodeGenSelector : UserControl
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "ucCodeGenSelector";

        #endregion Initialize Variables

        public ucCodeGenSelector()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucCodeGenSelector", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucCodeGenSelector", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucCodeGenSelector", IfxTraceCategory.Leave);
            }
        }


        private void BtnRunCodeGen_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnRunCodeGen_OnClick", IfxTraceCategory.Enter);

                if (CkSproc_List.IsChecked == true ||
                    CkSproc_ListAll.IsChecked == true ||
                    CkSproc_Row.IsChecked == true ||
                    CkSproc_Put.IsChecked == true ||
                    CkSproc_PutInsert.IsChecked == true ||
                    CkSproc_PutUpdate.IsChecked == true ||
                    CkSproc_Delete.IsChecked == true )
                {
                    GenerateSprocsFromTableList obj = new GenerateSprocsFromTableList();
                    obj.Generate((Guid) ContextValues.CurrentApVersionId,
                        (bool) CkSproc_List.IsChecked,
                        (bool) CkSproc_ListAll.IsChecked,
                        (bool) CkSproc_Row.IsChecked,
                        (bool) CkSproc_Put.IsChecked,
                        (bool) CkSproc_PutInsert.IsChecked,
                        (bool) CkSproc_PutUpdate.IsChecked,
                        (bool) CkSproc_Delete.IsChecked);
                }


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnRunCodeGen_OnClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnRunCodeGen_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void BtnClearAllCheckBoxes_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnClearAllCheckBoxes_OnClick", IfxTraceCategory.Enter);

                CkDS_All.IsChecked = false;
                CkWT_All.IsChecked = false;
                CkPxW_All.IsChecked = false;
                CkBObj_All.IsChecked = false;
                CkUiEnt_All.IsChecked = false;
                CkUiNav_All.IsChecked = false;
                CkUiProps_All.IsChecked = false;
                CkUiStr_All.IsChecked = false;
                CkSproc_All.IsChecked = false;

                CkDS_Base.IsChecked = false;
                CkDS_Cust.IsChecked = false;
                CkDS_Audit.IsChecked = false;
                CkWS.IsChecked = false;
                CkWT_Entity.IsChecked = false;
                CkWT_ReadOnly.IsChecked = false;
                CkPxW_Base.IsChecked = false;
                CkPxW_Cust.IsChecked = false;
                CkPxW_BatF.IsChecked = false;
                CkBObj_Base.IsChecked = false;
                CkBObj_Cust.IsChecked = false;
                CkBObj_Validation.IsChecked = false;
                CkBObj_Static.IsChecked = false;
                CkBObj_StaticCust.IsChecked = false;
                CkUiEnt_Xaml.IsChecked = false;
                CkUiEnt_Base.IsChecked = false;
                CkUiEnt_ConcurList.IsChecked = false;
                CkUiEnt_Cust.IsChecked = false;
                CkUiNav_Xaml.IsChecked = false;
                CkUiNav_Base.IsChecked = false;
                CkUiNav_ColumnCode.IsChecked = false;
                CkUiNav_Cust.IsChecked = false;
                CkUiNav_CustGrid.IsChecked = false;
                CkUiProps_Xaml.IsChecked = false;
                CkUiProps_Base.IsChecked = false;
                CkUiProps_Cust.IsChecked = false;
                CkUiStr_Props.IsChecked = false;
                CkUiStr_NavList.IsChecked = false;
                CkSproc_List.IsChecked = false;
                CkSproc_ListAll.IsChecked = false;
                CkSproc_Row.IsChecked = false;
                CkSproc_Put.IsChecked = false;
                CkSproc_PutInsert.IsChecked = false;
                CkSproc_PutUpdate.IsChecked = false;
                CkSproc_Delete.IsChecked = false;

                CkSqlRptReportColumnNames.IsChecked = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnClearAllCheckBoxes_OnClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnClearAllCheckBoxes_OnClick", IfxTraceCategory.Leave);
            }
        }


        private void CkDS_All_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CkDS_All_OnClick", IfxTraceCategory.Enter);
                CheckBox ck = sender as CheckBox;
                CkDS_Base.IsChecked = ck.IsChecked;
                CkDS_Cust.IsChecked = ck.IsChecked;
                CkDS_Audit.IsChecked = ck.IsChecked;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CkDS_All_OnClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CkDS_All_OnClick", IfxTraceCategory.Leave);
            }
        }


        private void CkWS_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CkWS_OnClick", IfxTraceCategory.Enter);
                CheckBox ck = sender as CheckBox;
                CkWS.IsChecked = ck.IsChecked;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CkWS_OnClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CkWS_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void CkWT_All_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CkWT_All_OnClick", IfxTraceCategory.Enter);
                CheckBox ck = sender as CheckBox;
                CkWT_Entity.IsChecked = ck.IsChecked;
                CkWT_ReadOnly.IsChecked = ck.IsChecked;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CkWT_All_OnClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CkWT_All_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void CkPxW_All_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CkPxW_All_OnClick", IfxTraceCategory.Enter);
                CheckBox ck = sender as CheckBox;
                CkPxW_Base.IsChecked = ck.IsChecked;
                CkPxW_Cust.IsChecked = ck.IsChecked;
                CkPxW_BatF.IsChecked = ck.IsChecked;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CkPxW_All_OnClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CkPxW_All_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void CkBObj_All_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CkBObj_All_OnClick", IfxTraceCategory.Enter);
                CheckBox ck = sender as CheckBox;
                CkBObj_Base.IsChecked = ck.IsChecked;
                CkBObj_Cust.IsChecked = ck.IsChecked;
                CkBObj_Validation.IsChecked = ck.IsChecked;
                CkBObj_Static.IsChecked = ck.IsChecked;
                CkBObj_StaticCust.IsChecked = ck.IsChecked;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CkBObj_All_OnClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CkBObj_All_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void CkUiEnt_All_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CkUiEnt_All_OnClick", IfxTraceCategory.Enter);
                CheckBox ck = sender as CheckBox;
                CkUiEnt_Xaml.IsChecked = ck.IsChecked;
                CkUiEnt_Base.IsChecked = ck.IsChecked;
                CkUiEnt_ConcurList.IsChecked = ck.IsChecked;
                CkUiEnt_Cust.IsChecked = ck.IsChecked;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CkUiEnt_All_OnClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CkUiEnt_All_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void CkUiNav_All_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CkUiNav_All_OnClick", IfxTraceCategory.Enter);
                CheckBox ck = sender as CheckBox;
                CkUiNav_Xaml.IsChecked = ck.IsChecked;
                CkUiNav_Base.IsChecked = ck.IsChecked;
                CkUiNav_ColumnCode.IsChecked = ck.IsChecked;
                CkUiNav_Cust.IsChecked = ck.IsChecked;
                CkUiNav_CustGrid.IsChecked = ck.IsChecked;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CkUiNav_All_OnClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CkUiNav_All_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void CkUiProps_All_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CkUiProps_All_OnClick", IfxTraceCategory.Enter);
                CheckBox ck = sender as CheckBox;
                CkUiProps_Xaml.IsChecked = ck.IsChecked;
                CkUiProps_Base.IsChecked = ck.IsChecked;
                CkUiProps_Cust.IsChecked = ck.IsChecked;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CkUiProps_All_OnClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CkUiProps_All_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void CkUiStr_All_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CkUiStr_All_OnClick", IfxTraceCategory.Enter);
                CheckBox ck = sender as CheckBox;
                CkUiStr_Props.IsChecked = ck.IsChecked;
                CkUiStr_NavList.IsChecked = ck.IsChecked;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CkUiStr_All_OnClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CkUiStr_All_OnClick", IfxTraceCategory.Leave);
            }
        }

        private void CkSproc_All_OnClick(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CkSproc_All_OnClick", IfxTraceCategory.Enter);
                CheckBox ck = sender as CheckBox;
                CkSproc_List.IsChecked = ck.IsChecked;
                CkSproc_ListAll.IsChecked = ck.IsChecked;
                CkSproc_Row.IsChecked = ck.IsChecked;
                CkSproc_Put.IsChecked = ck.IsChecked;
                CkSproc_PutInsert.IsChecked = ck.IsChecked;
                CkSproc_PutUpdate.IsChecked = ck.IsChecked;
                CkSproc_Delete.IsChecked = ck.IsChecked;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CkSproc_All_OnClick", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CkSproc_All_OnClick", IfxTraceCategory.Leave);
            }
        }



    }
}
