using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using EntityBll.SL;
using Ifx.SL;
using IfxUserViewLogSL;
using TypeServices;
using vUICommon;

// Gen Timestamp:  12/19/2017 9:20:54 PM

namespace UIControls
{
    public partial class ucWcTable
    {



        #region Initialize Variables

        private ucWcTableGroupAssignment _ucTblGrpAssign = null;
        private ucWcPropsTile _ucPropsTl = null;
        private ucWcGridColumnGroup _ucGridColGrp = null;

        #endregion Initialize Variables




        void CustomConstructionCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", IfxTraceCategory.Enter);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", IfxTraceCategory.Leave);
            }
        }




        #region Copy Code Gen Methods from the Codebehind class here



        public void SetStateFromParent(object ancestorId, string parentType, int? intParentId, Guid? guidParentId, object objParentId, int? intCurrentId, Guid? guidCurrentId, object objCurrentId, IBusinessObject currentBusinessObject, object[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 9
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Enter);
                _intParentId = intParentId;
                if (guidParentId == null)
                {
                    _guidParentId = objParentId as Guid?;
                }
                else
                {
                    _guidParentId = guidParentId;
                }
                
                _intCurrentId = intCurrentId;
                _guidCurrentId = guidCurrentId;
                //_guidCurrentId = objCurrentId as Guid?;
                //_oCurrentId = objCurrentId;

                _parentType = parentType;
                _currentBusinessObject = (WcTable_Bll)currentBusinessObject;  

                ucNav.ParentType = _parentType;
                ucNav.GuidParentId = _guidParentId;  
                ucNav.ObjectParentId = objParentId;

                if (_FLG_IsLoaded == false)
                {
                    InitializeControl();
                }
                if (list == null)
                {
                    //  **********************************      N E E D   C U S T O M   C O D E    H E R E  ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! 
           
                    if (_guidParentId == null)
                    {
                        //ucNav.NavList_ItemSource.Clear();
                        NavListRefreshFromObjectArray(null);
                        ucNav.IsEnabled = false;
                    }
                    else
                    {
                        ucNav.IsEnabled = true;


                        // Since this is being called from the parent, reset the viewmode back to normal
                        ucNav.SetViewMode(ucWcTableList.ViewMode.Normal);

                        LoadNavlistData();
                        //_wcTableProxy.Begin_WcTable_GetListByFK((Guid)_guidParentId);
                        //ucNav.navList.Cursor = Cursors.Wait;
                    }
                }
                else
                {
                    NavListRefreshFromObjectArray(list);
                }

                // Use    intParentId     if the parent is an Integer.  we might need to be using the Ancestor Id instead.
                if (guidParentId != null)
                {
                    ucNav.Set_vXamComboColumn_ItemSourcesWithParams();
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Leave);
            }
        }

        
        public void LoadNavlistData()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadNavlistData", IfxTraceCategory.Enter);

                _currentBusinessObject = null;
                ucNav.IsEnabled = true;
                switch (_parentType)
                {
                    case "ucWcApplicationVersion":

                        if (ucNav.CurrentViewMode == ucWcTableList.ViewMode.Normal)
                        {
                            _wcTableProxy.Begin_WcTable_GetListByFK((Guid)_guidParentId);
                            ucNav.navList.Cursor = Cursors.Wait;
                        }
                        else
                        {
                            ucNav.LoadGridFromConnKeyId();
                        }

                        break;

                    default:
                        throw new Exception("Missing Parent Type");
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadNavlistData", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadNavlistData", IfxTraceCategory.Leave);
            }
        }


        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDelete_Click", IfxTraceCategory.Enter);
                if (_currentBusinessObject != null)
                {
                    // call ws and set IsDeleted to true
                    string msg = "Are you sure you want to DELETE this record from the database and all data under it?";
                    MessageBoxResult result = MessageBox.Show(msg, "Delete Warning", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {
                        if (_currentBusinessObject.Tb_Id != null)
                        {
                            _wcTableProxy.Begin_WcTable_SetIsDeleted((Guid)_currentBusinessObject.Tb_Id, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDelete_Click", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnDelete_Click", IfxTraceCategory.Leave);
            }
        }







        #endregion Copy Code Gen Methods from the Codebehind class here

        #region Insert Custom Code Here

        void ucTblGrpAssign_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTblGrpAssign_Load", IfxTraceCategory.Enter);
                if (_ucTblGrpAssign == null)
                {
                    _ucTblGrpAssign = new ucWcTableGroupAssignment();
                    //_ucTblColGrpAssign.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    //_ucTblColGrpAssign.ChangeParentNavGridExpansion += ChangeParentNavGridExpansionFromNestedObject;
                    tbi_ucTbGrpAsgn.Content = _ucTblGrpAssign;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTblGrpAssign_Load", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTblGrpAssign_Load", IfxTraceCategory.Leave);
            }
        }


        void ucPropsTl_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucPropsTl_Load", IfxTraceCategory.Enter);
                if (_ucPropsTl == null)
                {
                    _ucPropsTl = new ucWcPropsTile();
                    //_ucTblColGrpAssign.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    //_ucTblColGrpAssign.ChangeParentNavGridExpansion += ChangeParentNavGridExpansionFromNestedObject;
                    tbi_ucPropsTl.Content = _ucPropsTl;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucPropsTl_Load", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucPropsTl_Load", IfxTraceCategory.Leave);
            }
        }

        void ucGridColGrp_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucGridColGrp_Load", IfxTraceCategory.Enter);
                if (_ucGridColGrp == null)
                {
                    _ucGridColGrp = new ucWcGridColumnGroup();
                    //_ucTblColGrpAssign.CurrentEntityStateChanged += new CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                    //_ucTblColGrpAssign.ChangeParentNavGridExpansion += ChangeParentNavGridExpansionFromNestedObject;
                    tbi_ucGridColGrp.Content = _ucGridColGrp;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucGridColGrp_Load", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucGridColGrp_Load", IfxTraceCategory.Leave);
            }
        }


        private ucCodeGenSelector _ucCdGen = null;
        void ucCdGen_Load()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucCdGen_Load", IfxTraceCategory.Enter);
                if (_ucCdGen == null)
                {
                    _ucCdGen = new ucCodeGenSelector();
                    tbi_ucCdGn.Content = _ucCdGen;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucCdGen_Load", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucCdGen_Load", IfxTraceCategory.Leave);
            }
        }



        #endregion Insert Custom Code Here

        #region  Combo DataSources and Events

        #endregion  Combo DataSources and Events

        #region Method Extentions For Custom Code

        void ConfigureToCurrentEntityState_CustomCode(EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState_CustomCode", IfxTraceCategory.Enter);
                switch (state)
                {
                    case EntityStateSwitch.None:
                        if (_currentBusinessObject == null)
                        {
                            tbi_ucTbGrpAsgn.IsEnabled = false;
                            tbi_ucGridColGrp.IsEnabled = false;
                            tbi_ucPropsTl.IsEnabled = false;
                            tbi_ucCdGn.IsEnabled = false;
                        }
                        else
                        {
                            tbi_ucTbGrpAsgn.IsEnabled = true;
                            tbi_ucGridColGrp.IsEnabled = true;
                            tbi_ucPropsTl.IsEnabled = true;
                            tbi_ucCdGn.IsEnabled = true;
                        }
                        break;
                    case EntityStateSwitch.NewInvalidNotDirty:
                        tbi_ucTbGrpAsgn.IsEnabled = false;
                        tbi_ucGridColGrp.IsEnabled = false;
                        tbi_ucPropsTl.IsEnabled = false;
                        tbi_ucCdGn.IsEnabled = false;
                        break;
                    case EntityStateSwitch.NewValidNotDirty:
                        tbi_ucTbGrpAsgn.IsEnabled = false;
                        tbi_ucGridColGrp.IsEnabled = false;
                        tbi_ucPropsTl.IsEnabled = false;
                        tbi_ucCdGn.IsEnabled = false;
                        break;
                    case EntityStateSwitch.NewValidDirty:
                        tbi_ucTbGrpAsgn.IsEnabled = false;
                        tbi_ucGridColGrp.IsEnabled = false;
                        tbi_ucPropsTl.IsEnabled = false;
                        tbi_ucCdGn.IsEnabled = false;
                        break;
                    case EntityStateSwitch.NewInvalidDirty:
                        tbi_ucTbGrpAsgn.IsEnabled = false;
                        tbi_ucGridColGrp.IsEnabled = false;
                        tbi_ucPropsTl.IsEnabled = false;
                        tbi_ucCdGn.IsEnabled = false;
                        break;
                    case EntityStateSwitch.ExistingInvalidDirty:
                        tbi_ucTbGrpAsgn.IsEnabled = false;
                        tbi_ucGridColGrp.IsEnabled = false;
                        tbi_ucPropsTl.IsEnabled = false;
                        tbi_ucCdGn.IsEnabled = false;
                        break;
                    case EntityStateSwitch.ExistingValidDirty:
                        tbi_ucTbGrpAsgn.IsEnabled = false;
                        tbi_ucGridColGrp.IsEnabled = false;
                        tbi_ucPropsTl.IsEnabled = false;
                        tbi_ucCdGn.IsEnabled = false;
                        break;
                    case EntityStateSwitch.ExistingValidNotDirty:
                        tbi_ucTbGrpAsgn.IsEnabled = true;
                        tbi_ucGridColGrp.IsEnabled = true;
                        tbi_ucPropsTl.IsEnabled = true;
                        tbi_ucCdGn.IsEnabled = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState_CustomCode", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState_CustomCode", IfxTraceCategory.Leave);
            }
        }

        void SyncControlsWithCurrentBusinessObject_CustomCode1()
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", IfxTraceCategory.Enter);


            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    _FLG_IsLoaded = true;
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", IfxTraceCategory.Leave);
            //}
        }

        void SyncControlsWithCurrentBusinessObject_CustomCode2(string selectedTab, object currentId)
            {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", IfxTraceCategory.Enter);
                Guid? id = currentId as Guid?;
                if (id != null)
                {
                    WcTable_Bll_staticLists.LoadStaticLists((Guid)id);
                }
                
                switch (selectedTab)
                {
                    case "tbi_ucTbGrpAsgn":
                        if (_ucTblGrpAssign == null) { ucTblGrpAssign_Load(); }
                        if (_currentBusinessObject != null)
                        {
                            _ucTblGrpAssign.SetStateFromParent(_currentBusinessObject.Tb_ApVrsn_Id, _currentBusinessObject.Tb_Id);
                        }
                        break;
                    case "tbi_ucPropsTl":
                        if (_ucPropsTl == null) { ucPropsTl_Load(); }
                        if (_currentBusinessObject != null)
                        {
                            //_ucPropsTl.SetStateFromParent(_currentBusinessObject.Tb_ApVrsn_Id, _currentBusinessObject.Tb_Id);
                            _ucPropsTl.SetStateFromParent(null, "ucWcTable", null, null, currentId, null, null, null, null, null, "");
                        }
                        break;
                    case "tbi_ucGridColGrp":
                        if (_ucGridColGrp == null) { ucGridColGrp_Load(); }
                        if (_currentBusinessObject != null)
                        {
                            //_ucGridColGrp.SetStateFromParent(_currentBusinessObject.Tb_ApVrsn_Id, _currentBusinessObject.Tb_Id);
                            _ucGridColGrp.SetStateFromParent(null, "ucWcTable", null, null, currentId, null, null, null, null, null, "");
                        }
                        break;
                    case "tbi_ucTbSort":
                        if (currentId != null)
                        {
                            WcTable_Bll_staticLists
                                .Refresh_WcTableColumn_lstByTable_ComboItemList_BindingList((Guid)currentId);
                        }
                        break;
                    case "tbi_ucCdGn":
                        if (_ucCdGen == null) { ucCdGen_Load(); }
                        break;

                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", IfxTraceCategory.Leave);
            }
        }

        #endregion Method Extentions For Custom Code



        #region Standard CodeGen Methods Subject to Customization



        #endregion Standard CodeGen Methods Subject to Customization



    }
}
