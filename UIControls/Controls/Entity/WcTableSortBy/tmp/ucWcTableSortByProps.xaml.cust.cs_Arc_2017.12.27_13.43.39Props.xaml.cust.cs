using System;
using System.Windows.Controls;
using Infragistics.Controls.Editors;
using System.Diagnostics;
using TypeServices;
using EntityBll.SL;
using EntityWireTypeSL;
using Velocity.SL;
using Ifx.SL;
using ProxyWrapper;
using vControls;
using vComboDataTypes;
using vUICommon;
using vDP;
using System.Windows;
using Infragistics.Controls.Layouts;


namespace UIControls
{
    public partial class ucWcTableSortByProps
    {


        /// <summary>
        /// Called from the class constructor in the ucEntryProps.xaml code file which is for
        /// <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        /// code-gen</a> and not for custom code. Custom code for the constructor should go in this
        /// method located in the ucEntryProps.xaml.cust.cs code file so it’s not lost the next
        /// time this class is code-genned.
        /// </summary>
       void CustomConstructorCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructorCode", IfxTraceCategory.Enter);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructorCode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomConstructorCode", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="LoadControl">LoadControl</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>LoadControl</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void CustomLoadMethods()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomLoadMethods", IfxTraceCategory.Enter);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomLoadMethods", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CustomLoadMethods", IfxTraceCategory.Leave);
            }
        }


        #region Security
        
        UserSecurityContext _userContext = new UserSecurityContext();
        private Guid _ancestorSecurityId = new Guid("59b35b3f-5d3c-4116-bd4a-6cc76bca4cea");
        ControlCache cCache;
        EntityCache eCache = null;
        bool defaultEntityOperationPermission = true;


        private void SetSecurityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Enter);
                _userContext.LoadArtifactPermissions(_ancestorSecurityId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Leave);
            }
        }

        void UserSecurityContext_SecurityArtifactsRetrieved(object sender, SecurityArtifactsRetrievedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Enter);

                SecurityCache.AddControlCacheForAncestor(e.ArtifactAncestorData);
                cCache = SecurityCache.GetControlGroupById(e.ArtifactAncestorData.Id);

                //// Some UI Control...
                //DP.SetControlSecurityId(SomeUIControl, new Guid("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"));
                //SecurityCache.SetWPFActionControlState(SomeUIControl, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                ////***
                //SecuitySettingIsReadOnly = SecurityCache.IsViewOnly(ControlId);
  
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Leave);
            }
        }
        

        #endregion Security


        void DefineCustomToolTips()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DefineCustomToolTips", IfxTraceCategory.Enter);

                // Note:  You must copy code-genned rules into this method for controls listed here or they will get over-writen.

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DefineCustomToolTips", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DefineCustomToolTips", IfxTraceCategory.Leave);
            }
        }



        #region Copy Code Gen Methods from the Codebehind class here

        #endregion Copy Code Gen Methods from the Codebehind class here

        #region Insert Custom Code Here

        #endregion Insert Custom Code Here

        #region  Combo DataSources and Events

        #endregion  Combo DataSources and Events

        #region Stubs from CodeGen Codebehind


        private void AddBusinessObjectEvents_CustomCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AddBusinessObjectEvents_CustomCode", IfxTraceCategory.Enter);
              
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AddBusinessObjectEvents_CustomCode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AddBusinessObjectEvents_CustomCode", IfxTraceCategory.Leave);
            }
        }

        private void RemoveBusnessObjectEvents_CustomCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RemoveBusnessObjectEvents_CustomCode", IfxTraceCategory.Enter);
              
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RemoveBusnessObjectEvents_CustomCode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RemoveBusnessObjectEvents_CustomCode", IfxTraceCategory.Leave);
            }
        }

        private void SetControlEvents_CustomCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlEvents_CustomCode", IfxTraceCategory.Enter);
              
                // Set Events For Controls


                //TOOLTIPS FOR BROKEN RULES



            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlEvents_CustomCode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlEvents_CustomCode", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="SetState">SetState</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>SetState</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        private void SetState_CustomCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetState_CustomCode", IfxTraceCategory.Enter);
              
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetState_CustomCode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetState_CustomCode", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="ClearDataFromUI">ClearDataFromUI</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>ClearDataFromUI</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        private void ClearDataFromUI_CustomCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearDataFromUI_CustomCode", IfxTraceCategory.Enter);
              
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearDataFromUI_CustomCode", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearDataFromUI_CustomCode", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="DataControlDataChangedEvents_SetAll">DataControlDataChangedEvents_SetAll</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the DataControlDataChangedEvents_SetAll method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void DataControlDataChangedEvents_SetAll_Custom()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Un-rem all ifx code in this method when this method is actually used.
                ////if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "DataControlDataChangedEvents_SetAll_Custom", IfxTraceCategory.Enter);

                //SomeTextBox.TextChanged += new TextChangedEventHandler(OnTextChanged);

            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_ns, _cn, "DataControlDataChangedEvents_SetAll_Custom", IfxTraceCategory.Catch);
                // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                ////if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_ns, _cn, "DataControlDataChangedEvents_SetAll_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="DataControlDataChangedEvents_RemoveAll">DataControlDataChangedEvents_RemoveAll</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for DataControlDataChangedEvents_RemoveAll  should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void DataControlDataChangedEvents_RemoveAll_Custom()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Un-rem all ifx code in this method when this method is actually used.
                ////if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "DataControlDataChangedEvents_RemoveAll_Custom", IfxTraceCategory.Enter);

                //SomeTextBox.TextChanged -= new TextChangedEventHandler(OnTextChanged);

            }
            catch (Exception ex)
            {
                //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(_ns, _cn, "DataControlDataChangedEvents_RemoveAll_Custom", IfxTraceCategory.Catch);
                // throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                ////if (IfxTrace._traceLeave) IfxEvent.PublishTrace(_ns, _cn, "DataControlDataChangedEvents_RemoveAll_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnTextChanged">OnTextChanged</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnTextChanged</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnTextBoxTextChanged_Custom(TextBox bx)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTextBoxTextChanged_Custom", IfxTraceCategory.Enter);
                //switch (bx.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTextBoxTextChanged_Custom", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTextBoxTextChanged_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnTexBoxLostFocus">OnTexBoxLostFocus</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnTexBoxLostFocus</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnTextBoxLostFocus_Custom(TextBox bx)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTextBoxLostFocus_Custom", IfxTraceCategory.Enter);
                //switch (bx.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTextBoxLostFocus_Custom", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTextBoxLostFocus_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnComboSelectionChanged">OnComboSelectionChanged</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnComboSelectionChanged</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnComboSelectionChanged_Custom(ComboBox cmb)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnComboSelectionChanged_Custom", IfxTraceCategory.Enter);
                //switch (cmb.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnComboSelectionChanged_Custom", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnComboSelectionChanged_Custom", IfxTraceCategory.Leave);
            }
        }
        
        /// <summary>
        ///     Called from the <see cref="OnComboBoxLostFocus">OnComboBoxLostFocus</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnComboBoxLostFocus</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnComboBoxLostFocus_Custom(ComboBox cmb)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnComboBoxLostFocus_Custom", IfxTraceCategory.Enter);
                //switch (cmb.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnComboBoxLostFocus_Custom", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnComboBoxLostFocus_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnCheckBoxClick">OnCheckBoxClick</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnCheckBoxClick</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnCheckBoxClick_Custom(CheckBox chk)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxClick_Custom", IfxTraceCategory.Enter);
                //switch (chk.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxClick_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxClick_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnCheckBoxLostFocus">OnCheckBoxLostFocus</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnCheckBoxLostFocus</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnCheckBoxLostFocus_Custom(CheckBox chk)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxLostFocus_Custom", IfxTraceCategory.Enter);
                //switch (chk.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxLostFocus_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCheckBoxLostFocus_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnRadioButtonClick">OnRadioButtonClick</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnRadioButtonClick</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnRadioButtonClick_Custom(RadioButton rdb)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonClick_Custom", IfxTraceCategory.Enter);
                //switch (rdb.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonClick_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonClick_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnRadioButtonLostFocus">OnRadioButtonLostFocus</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnRadioButtonLostFocus</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnRadioButtonLostFocus_Custom(RadioButton rdb)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonLostFocus_Custom", IfxTraceCategory.Enter);
                //switch (rdb.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonLostFocus_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonLostFocus_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnvRadioButtonGroupItemChecked">OnvRadioButtonGroupItemChecked</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnvRadioButtonGroupItemChecked</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnvRadioButtonGroupItemChecked_Custom(vRadioButtonGroup rbg)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
                //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonClick_Custom", IfxTraceCategory.Enter);
                //switch (rbg.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonClick_Custom", ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnRadioButtonClick_Custom", IfxTraceCategory.Leave);
            //}
        }

        /// <summary>
        ///     Called from the <see cref="OnXamComboSelectedItemChanged">OnXamComboSelectedItemChanged</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnXamComboSelectedItemChanged</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnXamComboSelectedItemChanged_Custom(XamComboEditor cmb)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboSelectedItemChanged_Custom", IfxTraceCategory.Enter);
                //switch (cmb.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboSelectedItemChanged_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboSelectedItemChanged_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnVXamComboSelectedItemChanged">OnVXamComboSelectedItemChanged</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnVXamComboSelectedItemChanged</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnVXamComboSelectedItemChanged_Custom(XamComboEditor cmb)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamComboSelectedItemChanged_Custom", IfxTraceCategory.Enter);
                //switch (cmb.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamComboSelectedItemChanged_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamComboSelectedItemChanged_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnOnXamComboLostFocus_Custom">OnOnXamComboLostFocus_Custom</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnOnXamComboLostFocus_Custom</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnXamComboLostFocus_Custom(XamComboEditor cmb)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboLostFocus_Custom", IfxTraceCategory.Enter);
                //switch (cmb.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboLostFocus_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamComboLostFocus_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnXamMultiColumnComboSelectedItemChanged_Custom">OnXamMultiColumnComboSelectedItemChanged</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnXamMultiColumnComboSelectedItemChanged</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnXamMultiColumnComboSelectedItemChanged_Custom(XamMultiColumnComboEditor cmb)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboSelectedItemChanged_Custom", IfxTraceCategory.Enter);
                //switch (cmb.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboSelectedItemChanged_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboSelectedItemChanged_Custom", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Called from the <see cref="OnVXamMultiColumnComboSelectedItemChanged_Custom">OnVXamMultiColumnComboSelectedItemChanged</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnVXamMultiColumnComboSelectedItemChanged</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnVXamMultiColumnComboSelectedItemChanged_Custom(XamMultiColumnComboEditor cmb)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamMultiColumnComboSelectedItemChanged_Custom", IfxTraceCategory.Enter);
                //switch (cmb.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamMultiColumnComboSelectedItemChanged_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnVXamMultiColumnComboSelectedItemChanged_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnXamMultiColumnComboLostFocus_Custom">OnOnXamMultiColumnComboLostFocus_Custom</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnOnXamMultiColumnComboLostFocus_Custom</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnXamMultiColumnComboLostFocus_Custom(XamMultiColumnComboEditor cmb)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboLostFocus_Custom", IfxTraceCategory.Enter);
                //switch (cmb.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboLostFocus_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnXamMultiColumnComboLostFocus_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnDatePicker_TextChanged">OnDatePicker_TextChanged</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnDatePicker_TextChanged</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnDatePicker_TextChanged_Custom(DatePicker ctl)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePicker_TextChanged_Custom", IfxTraceCategory.Enter);
                //switch (ctl.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePicker_TextChanged_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePicker_TextChanged_Custom", IfxTraceCategory.Leave);
            }
        }



        /// <summary>
        ///     Called from the <see cref="TimePicker_ValueChanged">TimePicker_ValueChanged</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>TimePicker_ValueChanged</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnTimePicker_ValueChanged_Custom(TimePicker tp)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePicker_ValueChanged_Custom", IfxTraceCategory.Enter);
                //switch (tp.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePicker_ValueChanged_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePicker_ValueChanged_Custom", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Called from the <see cref="OnTimePickerLostFocus">OnTimePickerLostFocus</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnTimePickerLostFocus</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnTimePickerLostFocus_Custom(TimePicker tp)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePickerLostFocus_Custom", IfxTraceCategory.Enter);
                //switch (tp.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePickerLostFocus_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnTimePickerLostFocus_Custom", IfxTraceCategory.Leave);
            }
        }




        /// <summary>
        ///     Called from the <see cref="OnDatePickerLostFocus">OnDatePickerLostFocus</see> method in the
        ///     ucEntryProps.xaml.cs code file which is for <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Code_Generation_and_Partial_Classes.html">
        ///     code-gen</a> and not for custom code. Custom code for the
        ///     <strong>OnDatePickerLostFocus</strong> method should go in this method located in the
        ///     ucEntryProps.xaml.cust.cs code file so it’s not lost the next time this class is
        ///     code-genned.
        /// </summary>
        void OnDatePickerLostFocus_Custom(DatePicker ctl)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePickerLostFocus_Custom", IfxTraceCategory.Enter);
                //switch (ctl.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePickerLostFocus_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnDatePickerLostFocus_Custom", IfxTraceCategory.Leave);
            }
        }



        private void XamColorPicker_SelectedColorChanged_custom(XamColorPicker cp, SelectedColorChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_custom", IfxTraceCategory.Enter);
                //switch (cp.Name)
                //{
                //    case "xxxxxxxxxx":

                //        break;
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged_custom", IfxTraceCategory.Leave);
            }
        }




        void SetControlsDefaultValidState_Custom()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlsDefaultValidState_Custom", IfxTraceCategory.Enter);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlsDefaultValidState_Custom", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetControlsDefaultValidState_Custom", IfxTraceCategory.Leave);
            }
        }


        void DisableReadonlycontrols_CustomCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DisableReadonlycontrols_CustomCode", IfxTraceCategory.Enter);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DisableReadonlycontrols_CustomCode", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DisableReadonlycontrols_CustomCode", IfxTraceCategory.Leave);
            }
        }

        private void OnControlValidStateChanged_Custom(object sender, ControlValidStateChangedArgs e)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged_Custom", IfxTraceCategory.Enter);
            //    //switch (e.PropertyName)
            //    //{
            //    //    case "xxx":
            //    //        SetControlValidAppearance(xxx, e.IsValid, e.IsDirty);
            //    //        break;
            //    //}
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged_Custom", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged_Custom", IfxTraceCategory.Leave);
            //}
        }

        void FormatFields_Custom()
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FormatFields_Custom", IfxTraceCategory.Enter);


            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FormatFields_Custom", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FormatFields_Custom", IfxTraceCategory.Leave);
            //}
        }

        void ReadOnlyAssignments_Custom()
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReadOnlyAssignments_Custom", IfxTraceCategory.Enter);


            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReadOnlyAssignments_Custom", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReadOnlyAssignments_Custom", IfxTraceCategory.Leave);
            //}
        }

        void ReadOnlySettings_Custom()
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReadOnlySettings_Custom", IfxTraceCategory.Enter);

            //    if (_secuitySettingIsReadOnly == true)
            //    {
            //        gdXXX_ReadOnly1.Visibility = System.Windows.Visibility.Visible;
            //    }
            //    else
            //    {
            //        gdXXX_ReadOnly1.Visibility = System.Windows.Visibility.Collapsed;
            //    }

            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReadOnlySettings_Custom", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReadOnlySettings_Custom", IfxTraceCategory.Leave);
            //}
        }


        void AssignTextLengthLabels_Custom()
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels_Custom", IfxTraceCategory.Enter);


            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels_Custom", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels_Custom", IfxTraceCategory.Leave);
            //}
        }

        void ConfigureXamTileView_CustomSettings()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureXamTileView_CustomSettings", IfxTraceCategory.Enter);
                if (_xtv.NormalModeSettings == null)
                {
                    _xtv.NormalModeSettings = new NormalModeSettings();
                }
                _xtv.NormalModeSettings.MaxColumns = 1;
                _xtv.NormalModeSettings.MaxRows = 7;

                _xtv.NormalModeSettings.TileLayoutOrder = TileLayoutOrder.Horizontal;

                _xtv.MaximizedModeSettings = new MaximizedModeSettings
                {
                    MinimizedExpandedTileConstraints = new TileConstraints
                    {
                        PreferredHeight = 100,
                        PreferredWidth = 200,
                    },
                    MinimizedTileConstraints = new TileConstraints
                    {
                        MinWidth = 250,
                    },
                    MaximizedTileConstraints = new TileConstraints
                    {
                        MinWidth = 504,     // Grid Width = 14
                        MaxWidth = 504,
                    }
                };
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureXamTileView_CustomSettings", ex);
				   // throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureXamTileView_CustomSettings", IfxTraceCategory.Leave);
            }
        }









        #endregion  Stubs from CodeGen Codebehind



        #region Standard CodeGen Methods Subject to Customization



        #region ISecurity Members

        Guid _controlId = new Guid();
        public Guid ControlId
        {
            get { return _controlId; }
            set { _controlId = value; }
        }

        #endregion ISecurity Members




        /// <summary>
        ///     Typically, the execution of this code is initiated by the parent object (<see cref="ucWcTableSortBy">ucWcTableSortBy</see>) passing an instance of <see cref="EntityBll.WcTableSortBy_Bll">WcTableSortBy_Bll</see> into the <see cref="SetBusinessObject">SetBusinessObject</see> method where the WcTableSortBy_Bll object is
        ///     configured as this control’s current business object. SetBusinessObject calls this
        ///     method and ucWcTableSortByProps is enabled or disabled based on the value of the flag
        ///     passed in.
        /// </summary>
        public void SetEnabledState(bool isEnabled)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetEnabledState", IfxTraceCategory.Enter);
                this.IsEnabled = isEnabled;
                //// Use this line instead of the one above when you need to assign UI security to this control
                //this.IsEnabled = SecurityCache.IsEnabled(_controlId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetEnabledState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetEnabledState", IfxTraceCategory.Leave);
            }
        }



        #endregion Standard CodeGen Methods Subject to Customization






    }
}
