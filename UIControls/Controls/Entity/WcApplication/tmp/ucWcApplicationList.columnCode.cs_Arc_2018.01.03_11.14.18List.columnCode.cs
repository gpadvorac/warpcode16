using System;
using System.Windows;
using System.Windows.Controls;
using Ifx.SL;
using TypeServices;
using vControls;
using EntityBll.SL;
using Infragistics.Controls.Editors;
using vUICommon;
using EntityWireTypeSL;
using Infragistics.Controls.Grids;
using vComboDataTypes;
using System.Collections.Generic;
using vUICommon.Controls;
using UIControls.Globalization.WcApplication;
using UIControls.Globalization.WcApplicationVersion;
namespace UIControls
{
    public partial class ucWcApplicationList
    {

        
        #region GridCell Editor Events
        

        private void XamGridTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                if ((ctl.Text.Length>0) && (ctl.Text.Substring(ctl.Text.Length - 1, 1) == " "))
                {
                    return;
                }
                WcApplication_Bll obj = navList.ActiveCell.Row.Data as WcApplication_Bll;
                if (obj == null) { return; }
                //switch (ctl.Name)
                //{
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vTextColumn_TextChanged(object sender, vTextColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcApplication_Bll)
                {
                    WcApplication_Bll obj = navList.ActiveCell.Row.Data as WcApplication_Bll;
                    switch (e.Key)
                    {
                        case "Ap_Name":
                            obj.Ap_Name = ctl.Text;
                            break;
                        case "Ap_Desc":
                            obj.Ap_Desc = ctl.Text;
                            break;
                        case "Ap_v_Application_Id_asString":
                            obj.Ap_v_Application_Id_asString = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (e.Key)
                    {
                        case "ApVrsn_MajorVersion":
                            obj.ApVrsn_MajorVersion_asString = ctl.Text;
                            break;
                        case "ApVrsn_MinorVersion":
                            obj.ApVrsn_MinorVersion_asString = ctl.Text;
                            break;
                        case "ApVrsn_VersionIteration":
                            obj.ApVrsn_VersionIteration_asString = ctl.Text;
                            break;
                        case "ApVrsn_Server":
                            obj.ApVrsn_Server = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vDecimalColumn_TextChanged(object sender, vDecimalColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcApplication_Bll)
                {
                    WcApplication_Bll obj = navList.ActiveCell.Row.Data as WcApplication_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamComboEditor comboEditor = sender as XamComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_SelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcApplication_Bll)
                {
                    WcApplication_Bll obj = navList.ActiveCell.Row.Data as WcApplication_Bll;
                    switch (ctl.Name)
                    {
                        case "Ap_TkSt_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Ap_TkSt_Id = null;
                            }
                            else
                            {
                                obj.Ap_TkSt_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (ctl.Name)
                    {
                        case "ApVrsn_DefaultConnectionCodeId":
                            if (ctl.SelectedItem == null)
                            {
                                obj.ApVrsn_DefaultConnectionCodeId = null;
                            }
                            else
                            {
                                obj.ApVrsn_DefaultConnectionCodeId = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_SelectionChanged(object sender, vXamComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                // The next 2 lines are for the Not In List feature.  If the 'Non-In-List' event is fireing, we dont want to run this event right now.
                if (ctl.SelectedItem == null) { return; }
                if (((ComboItem)ctl.SelectedItem).Id == null) { return; }
                if (navList.ActiveCell.Row.Data is WcApplication_Bll)
                {
                    WcApplication_Bll obj = navList.ActiveCell.Row.Data as WcApplication_Bll;
                    switch (e.Key)
                    {
                        case "Ap_TkSt_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Ap_TkSt_Id = null;
                            }
                            else
                            {
                                obj.Ap_TkSt_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
                if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (e.Key)
                    {
                        case "ApVrsn_DefaultConnectionCodeId":
                            if (ctl.SelectedItem == null)
                            {
                                obj.ApVrsn_DefaultConnectionCodeId = null;
                            }
                            else
                            {
                                obj.ApVrsn_DefaultConnectionCodeId = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }


                vXamComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Enter);
                if (e.Key == System.Windows.Input.Key.Escape)
                {
                    WcApplication_Bll obj = _activeRow.Data as WcApplication_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_KeyUp(object sender, vXamComboColumnKeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Enter);
                if (e.XamComboColumnKeyEventArgs.Key == System.Windows.Input.Key.Escape)
                {
                    WcApplication_Bll obj = _activeRow.Data as WcApplication_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void XamMultiColumnComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamMultiColumnComboEditor comboEditor = sender as XamMultiColumnComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void vXamMultiColumnComboColumn_SelectionChanged(object sender, vXamMultiColumnComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vXamMultiColumnComboEditor ctl = sender as vXamMultiColumnComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcApplication_Bll)
                {
                    WcApplication_Bll obj = navList.ActiveCell.Row.Data as WcApplication_Bll;
                    switch (e.Key)
                    {
                    }
                }
                if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (e.Key)
                    {
                    }
                }
                vXamMultiColumnComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridDatePicker_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                WcApplication_Bll obj = navList.ActiveCell.Row.Data as WcApplication_Bll;
                if (obj == null) { return; }
                switch (ctl.Name)
                {
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Leave);
            }
        }





        private void XamColorPicker_SelectedColorChanged(object sender, SelectedColorChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamColorPicker ctl = sender as XamColorPicker;
                if (ctl == null) { return; }
                WcApplication_Bll obj = navList.ActiveCell.Row.Data as WcApplication_Bll;
                if (obj == null) { return; }
                switch (((CellControl)ctl.Parent).Cell.Column.Key)
                {
                }
                XamColorPicker_SelectedColorChanged_CustomCode(ctl, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Leave);
            }
        }





        //  may not need this as the prop gets hit through binding.
        public void vDatePickerColumn_TextChanged(object sender, vDatePickerColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcApplication_Bll)
                {
                    WcApplication_Bll obj = navList.ActiveCell.Row.Data as WcApplication_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                vDatePickerColumn_TextChanged_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vCheckColumn_CheckedChanged(object sender, vCheckColumnCheckedChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Enter);
                //if (_isRowInEditMode == false) { return; }
                vCheckBox ctl = sender as vCheckBox;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcApplication_Bll)
                {
                    WcApplication_Bll obj = navList.ActiveCell.Row.Data as WcApplication_Bll;
                    switch (e.Key)
                    {
                    case "Ap_IsActiveRow":
                        obj.Ap_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Leave);
            }
        }

        #endregion GridCell Editor Events

        
        #region Grid Other Events and Event Support Methods
        

        void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Enter);
                Cell cell = (Cell)navList.ActiveCell;
                IvControlsValidation ctl = cell.Control.Content as IvControlsValidation;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcApplication_Bll)
                {
                    WcApplication_Bll obj = navList.ActiveCell.Row.Data as WcApplication_Bll;

                    switch (e.PropertyName)
                    {
                        case "Ap_Name":
                            SetGridCellValidationAppeance(ctl, obj, "Ap_Name");
                            break;
                        case "Ap_Desc":
                            SetGridCellValidationAppeance(ctl, obj, "Ap_Desc");
                            break;
                        case "Ap_TkSt_Id":
                            SetGridCellValidationAppeance(ctl, obj, "Ap_TkSt_Id");
                            break;
                        case "Ap_v_Application_Id_asString":
                            SetGridCellValidationAppeance(ctl, obj, "Ap_v_Application_Id");
                            break;
                        case "Ap_IsActiveRow":
                            SetGridCellValidationAppeance(ctl, obj, "Ap_IsActiveRow");
                            break;
                        case "UserName":
                            SetGridCellValidationAppeance(ctl, obj, "UserName");
                            break;
                        case "Ap_LastModifiedDate":
                            SetGridCellValidationAppeance(ctl, obj, "Ap_LastModifiedDate");
                            break;
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (e.PropertyName)
                    {
                    case "ApVrsn_MajorVersion":
                        SetGridCellValidationAppeance(ctl, obj, "ApVrsn_MajorVersion");
                        break;
                    case "ApVrsn_MinorVersion":
                        SetGridCellValidationAppeance(ctl, obj, "ApVrsn_MinorVersion");
                        break;
                    case "ApVrsn_VersionIteration":
                        SetGridCellValidationAppeance(ctl, obj, "ApVrsn_VersionIteration");
                        break;
                    case "ApVrsn_Server":
                        SetGridCellValidationAppeance(ctl, obj, "ApVrsn_Server");
                        break;
                    case "ApVrsn_DefaultConnectionCodeId":
                        SetGridCellValidationAppeance(ctl, obj, "ApVrsn_DefaultConnectionCodeId");
                        break;
                    case "AttachmentCount":
                        SetGridCellValidationAppeance(ctl, obj, "AttachmentCount");
                        break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Leave);
            }
        }

        #endregion Grid Other Events and Event Support Methods

        
        #region Methods


        void AssignTextLengthLabels()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Enter);

                ((vTextColumn)navList.Columns["Ap_Name"]).MaxTextLength = 75;
                ((vTextColumn)navList.Columns["Ap_Desc"]).MaxTextLength = 200;
                ((vTextColumn)navList.Columns["UserName"]).MaxTextLength = 60;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcApplicationVersion"]).Columns["ApVrsn_Server"]).MaxTextLength = 25;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Leave);
            }
        }



        void ConfigureColumnHeaderTooltips()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Enter);

                    // Ap_TkSt_Id
                    ((vComboColumnBase)navList.Columns["Ap_TkSt_Id"]).HeaderToolTipCaption = StringsWcApplicationList.Ap_TkSt_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["Ap_TkSt_Id"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationListTooltips.Ap_TkSt_Id_1, StringsWcApplicationListTooltips.Ap_TkSt_Id_2};
                    ((vComboColumnBase)navList.Columns["Ap_TkSt_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Ap_v_Application_Id
                    ((IvColumn)navList.Columns["Ap_v_Application_Id_asString"]).HeaderToolTipCaption = StringsWcApplicationList.Ap_v_Application_Id_Vbs; 
                    ((IvColumn)navList.Columns["Ap_v_Application_Id_asString"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationListTooltips.Ap_v_Application_Id_1};
                    ((IvColumn)navList.Columns["Ap_v_Application_Id_asString"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Ap_IsActiveRow
                    ((IvColumn)navList.Columns["Ap_IsActiveRow"]).HeaderToolTipCaption = StringsWcApplicationList.Ap_IsActiveRow_Vbs; 
                    ((IvColumn)navList.Columns["Ap_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationListTooltips.Ap_IsActiveRow_1, StringsWcApplicationListTooltips.Ap_IsActiveRow_2};
                    ((IvColumn)navList.Columns["Ap_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)navList.Columns["UserName"]).HeaderToolTipCaption = StringsWcApplicationList.UserName_Vbs; 
                    ((IvColumn)navList.Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationListTooltips.UserName_1};
                    ((IvColumn)navList.Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Ap_LastModifiedDate
                    ((IvColumn)navList.Columns["Ap_LastModifiedDate"]).HeaderToolTipCaption = StringsWcApplicationList.Ap_LastModifiedDate_Vbs; 
                    ((IvColumn)navList.Columns["Ap_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationListTooltips.Ap_LastModifiedDate_1};
                    ((IvColumn)navList.Columns["Ap_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // AttachmentCount
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcApplicationVersion"]).Columns["AttachmentCount"]).HeaderToolTipCaption = StringsWcApplicationVersionList.AttachmentCount_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcApplicationVersion"]).Columns["AttachmentCount"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.AttachmentCount_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcApplicationVersion"]).Columns["AttachmentCount"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_Server
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcApplicationVersion"]).Columns["ApVrsn_Server"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_Server_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcApplicationVersion"]).Columns["ApVrsn_Server"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.ApVrsn_Server_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcApplicationVersion"]).Columns["ApVrsn_Server"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_DefaultConnectionCodeId
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcApplicationVersion"]).Columns["ApVrsn_DefaultConnectionCodeId"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_DefaultConnectionCodeId_Vbs; 
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcApplicationVersion"]).Columns["ApVrsn_DefaultConnectionCodeId"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.ApVrsn_DefaultConnectionCodeId_1, StringsWcApplicationVersionListTooltips.ApVrsn_DefaultConnectionCodeId_2};
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcApplicationVersion"]).Columns["ApVrsn_DefaultConnectionCodeId"]).HeaderTooltipContent = new TooltipBusinessRuleContent();
                ConfigureColumnHeaderTooltips_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Leave);
            }
        }


        void Set_vXamComboColumn_ItemSources()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["Ap_TkSt_Id"]).ItemsSource = CommonClientData_Bll_staticLists.TaskStatus_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["Ap_TkSt_Id"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.TaskStatus_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(TaskStatus_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcApplicationVersion"]).Columns["ApVrsn_DefaultConnectionCodeId"]).ItemsSource = WcApplication_Bll_staticLists.WcApplicationConnectionStringKey_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcApplicationVersion"]).Columns["ApVrsn_DefaultConnectionCodeId"]).DisplayMemberPath = "ItemName";
                WcApplication_Bll_staticLists.WcApplicationConnectionStringKey_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcApplicationConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Leave);
            }
        }

        void TaskStatus_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskStatus_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["Ap_TkSt_Id"]).ItemsSource = CommonClientData_Bll_staticLists.TaskStatus_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskStatus_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskStatus_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcApplicationConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcApplicationVersion"]).Columns["ApVrsn_DefaultConnectionCodeId"]).ItemsSource = WcApplication_Bll_staticLists.WcApplicationConnectionStringKey_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        #region Combo ItemsSource for Non-Static Lists

        #endregion region Combo ItemsSource for Non-Static Lists

        void SetNewRowValidation()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Enter);
                if (_activeRow == null) { return; }
                WcApplication_Bll obj = _activeRow.Data as WcApplication_Bll;
                if (obj == null) { return; }
                // Note:  The lines to set the validaton styles below are not needed as the styles are being set from converters in the xaml style propeties, but they are left here incase needed in the future.  this is new code and we are unsure how it will evolve.
                WcApplicationVersion_Bll oWcApplicationVersion = _activeRow.Data as WcApplicationVersion_Bll;
                if (oWcApplicationVersion == null) { return; }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Leave);
            }
        }

        #endregion Methods


    }
}
