using System;
using System.Windows;
using System.Windows.Controls;
using Ifx.SL;
using TypeServices;
using vControls;
using EntityBll.SL;
using Infragistics.Controls.Editors;
using vUICommon;
using EntityWireTypeSL;
using Infragistics.Controls.Grids;
using vComboDataTypes;
using System.Collections.Generic;
using vUICommon.Controls;
using UIControls.Globalization.WcTableChild;
namespace UIControls
{
    public partial class ucWcTableChildList
    {

        
        #region GridCell Editor Events
        

        private void XamGridTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                if ((ctl.Text.Length>0) && (ctl.Text.Substring(ctl.Text.Length - 1, 1) == " "))
                {
                    return;
                }
                WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                if (obj == null) { return; }
                //switch (ctl.Name)
                //{
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vTextColumn_TextChanged(object sender, vTextColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (e.Key)
                    {
                        case "TbChd_SortOrder":
                            obj.TbChd_SortOrder_asString = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vDecimalColumn_TextChanged(object sender, vDecimalColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamComboEditor comboEditor = sender as XamComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_SelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (ctl.Name)
                    {
                        case "TbChd_Child_Tb_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_Child_Tb_Id = null;
                            }
                            else
                            {
                                obj.TbChd_Child_Tb_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_ChildForeignKeyColumn_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_ChildForeignKeyColumn_Id = null;
                            }
                            else
                            {
                                obj.TbChd_ChildForeignKeyColumn_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_ChildForeignKeyColumn22_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_ChildForeignKeyColumn22_Id = null;
                            }
                            else
                            {
                                obj.TbChd_ChildForeignKeyColumn22_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_ChildForeignKeyColumn33_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_ChildForeignKeyColumn33_Id = null;
                            }
                            else
                            {
                                obj.TbChd_ChildForeignKeyColumn33_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_SelectionChanged(object sender, vXamComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                // The next 2 lines are for the Not In List feature.  If the 'Non-In-List' event is fireing, we dont want to run this event right now.
                if (ctl.SelectedItem == null) { return; }
                if (((ComboItem)ctl.SelectedItem).Id == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (e.Key)
                    {
                        case "TbChd_Child_Tb_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_Child_Tb_Id = null;
                            }
                            else
                            {
                                obj.TbChd_Child_Tb_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_ChildForeignKeyColumn_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_ChildForeignKeyColumn_Id = null;
                            }
                            else
                            {
                                obj.TbChd_ChildForeignKeyColumn_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_ChildForeignKeyColumn22_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_ChildForeignKeyColumn22_Id = null;
                            }
                            else
                            {
                                obj.TbChd_ChildForeignKeyColumn22_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_ChildForeignKeyColumn33_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_ChildForeignKeyColumn33_Id = null;
                            }
                            else
                            {
                                obj.TbChd_ChildForeignKeyColumn33_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
                vXamComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Enter);
                if (e.Key == System.Windows.Input.Key.Escape)
                {
                    WcTableChild_Bll obj = _activeRow.Data as WcTableChild_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_KeyUp(object sender, vXamComboColumnKeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Enter);
                if (e.XamComboColumnKeyEventArgs.Key == System.Windows.Input.Key.Escape)
                {
                    WcTableChild_Bll obj = _activeRow.Data as WcTableChild_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void XamMultiColumnComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamMultiColumnComboEditor comboEditor = sender as XamMultiColumnComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void vXamMultiColumnComboColumn_SelectionChanged(object sender, vXamMultiColumnComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vXamMultiColumnComboEditor ctl = sender as vXamMultiColumnComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (e.Key)
                    {
                    }
                }
                vXamMultiColumnComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridDatePicker_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                if (obj == null) { return; }
                switch (ctl.Name)
                {
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Leave);
            }
        }





        private void XamColorPicker_SelectedColorChanged(object sender, SelectedColorChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamColorPicker ctl = sender as XamColorPicker;
                if (ctl == null) { return; }
                WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                if (obj == null) { return; }
                switch (((CellControl)ctl.Parent).Cell.Column.Key)
                {
                }
                XamColorPicker_SelectedColorChanged_CustomCode(ctl, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Leave);
            }
        }





        //  may not need this as the prop gets hit through binding.
        public void vDatePickerColumn_TextChanged(object sender, vDatePickerColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                vDatePickerColumn_TextChanged_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vCheckColumn_CheckedChanged(object sender, vCheckColumnCheckedChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Enter);
                //if (_isRowInEditMode == false) { return; }
                vCheckBox ctl = sender as vCheckBox;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (e.Key)
                    {
                    case "TbChd_IsDisplayAsChildGrid":
                        obj.TbChd_IsDisplayAsChildGrid = (bool)ctl.IsChecked;
                        break;
                    case "TbChd_IsDisplayAsChildTab":
                        obj.TbChd_IsDisplayAsChildTab = (bool)ctl.IsChecked;
                        break;
                    case "TbChd_IsActiveRow":
                        obj.TbChd_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Leave);
            }
        }

        #endregion GridCell Editor Events

        
        #region Grid Other Events and Event Support Methods
        

        void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Enter);
                Cell cell = (Cell)navList.ActiveCell;
                IvControlsValidation ctl = cell.Control.Content as IvControlsValidation;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;

                    switch (e.PropertyName)
                    {
                        case "TbChd_SortOrder":
                            SetGridCellValidationAppeance(ctl, obj, "TbChd_SortOrder");
                            break;
                        case "TbChd_Child_Tb_Id":
                            SetGridCellValidationAppeance(ctl, obj, "TbChd_Child_Tb_Id");
                            break;
                        case "TbChd_ChildForeignKeyColumn_Id":
                            SetGridCellValidationAppeance(ctl, obj, "TbChd_ChildForeignKeyColumn_Id");
                            break;
                        case "TbChd_ChildForeignKeyColumn22_Id":
                            SetGridCellValidationAppeance(ctl, obj, "TbChd_ChildForeignKeyColumn22_Id");
                            break;
                        case "TbChd_ChildForeignKeyColumn33_Id":
                            SetGridCellValidationAppeance(ctl, obj, "TbChd_ChildForeignKeyColumn33_Id");
                            break;
                        case "TbChd_IsDisplayAsChildGrid":
                            SetGridCellValidationAppeance(ctl, obj, "TbChd_IsDisplayAsChildGrid");
                            break;
                        case "TbChd_IsDisplayAsChildTab":
                            SetGridCellValidationAppeance(ctl, obj, "TbChd_IsDisplayAsChildTab");
                            break;
                        case "TbChd_IsActiveRow":
                            SetGridCellValidationAppeance(ctl, obj, "TbChd_IsActiveRow");
                            break;
                        case "UserName":
                            SetGridCellValidationAppeance(ctl, obj, "UserName");
                            break;
                        case "TbChd_LastModifiedDate":
                            SetGridCellValidationAppeance(ctl, obj, "TbChd_LastModifiedDate");
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Leave);
            }
        }

        #endregion Grid Other Events and Event Support Methods

        
        #region Methods


        void AssignTextLengthLabels()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Enter);

                ((vTextColumn)navList.Columns["UserName"]).MaxTextLength = 60;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Leave);
            }
        }



        void ConfigureColumnHeaderTooltips()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Enter);

                    // TbChd_Child_Tb_Id
                    ((vComboColumnBase)navList.Columns["TbChd_Child_Tb_Id"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_Child_Tb_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["TbChd_Child_Tb_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_Child_Tb_Id_1};
                    ((vComboColumnBase)navList.Columns["TbChd_Child_Tb_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_ChildForeignKeyColumn_Id
                    ((vComboColumnBase)navList.Columns["TbChd_ChildForeignKeyColumn_Id"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_ChildForeignKeyColumn_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["TbChd_ChildForeignKeyColumn_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_ChildForeignKeyColumn_Id_1};
                    ((vComboColumnBase)navList.Columns["TbChd_ChildForeignKeyColumn_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_ChildForeignKeyColumn22_Id
                    ((vComboColumnBase)navList.Columns["TbChd_ChildForeignKeyColumn22_Id"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_ChildForeignKeyColumn22_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["TbChd_ChildForeignKeyColumn22_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_ChildForeignKeyColumn22_Id_1, StringsWcTableChildListTooltips.TbChd_ChildForeignKeyColumn22_Id_2, StringsWcTableChildListTooltips.TbChd_ChildForeignKeyColumn22_Id_3};
                    ((vComboColumnBase)navList.Columns["TbChd_ChildForeignKeyColumn22_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_ChildForeignKeyColumn33_Id
                    ((vComboColumnBase)navList.Columns["TbChd_ChildForeignKeyColumn33_Id"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_ChildForeignKeyColumn33_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["TbChd_ChildForeignKeyColumn33_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_ChildForeignKeyColumn33_Id_1, StringsWcTableChildListTooltips.TbChd_ChildForeignKeyColumn33_Id_2};
                    ((vComboColumnBase)navList.Columns["TbChd_ChildForeignKeyColumn33_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_IsDisplayAsChildGrid
                    ((IvColumn)navList.Columns["TbChd_IsDisplayAsChildGrid"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_IsDisplayAsChildGrid_Vbs; 
                    ((IvColumn)navList.Columns["TbChd_IsDisplayAsChildGrid"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_IsDisplayAsChildGrid_1};
                    ((IvColumn)navList.Columns["TbChd_IsDisplayAsChildGrid"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_IsDisplayAsChildTab
                    ((IvColumn)navList.Columns["TbChd_IsDisplayAsChildTab"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_IsDisplayAsChildTab_Vbs; 
                    ((IvColumn)navList.Columns["TbChd_IsDisplayAsChildTab"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_IsDisplayAsChildTab_1};
                    ((IvColumn)navList.Columns["TbChd_IsDisplayAsChildTab"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_IsActiveRow
                    ((IvColumn)navList.Columns["TbChd_IsActiveRow"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_IsActiveRow_Vbs; 
                    ((IvColumn)navList.Columns["TbChd_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_IsActiveRow_1, StringsWcTableChildListTooltips.TbChd_IsActiveRow_2};
                    ((IvColumn)navList.Columns["TbChd_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)navList.Columns["UserName"]).HeaderToolTipCaption = StringsWcTableChildList.UserName_Vbs; 
                    ((IvColumn)navList.Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.UserName_1};
                    ((IvColumn)navList.Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_LastModifiedDate
                    ((IvColumn)navList.Columns["TbChd_LastModifiedDate"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_LastModifiedDate_Vbs; 
                    ((IvColumn)navList.Columns["TbChd_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_LastModifiedDate_1};
                    ((IvColumn)navList.Columns["TbChd_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();
                ConfigureColumnHeaderTooltips_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Leave);
            }
        }


        void Set_vXamComboColumn_ItemSources()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["TbChd_Child_Tb_Id"]).ItemsSource = WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["TbChd_Child_Tb_Id"]).DisplayMemberPath = "ItemName";
                WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTable_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)navList.Columns["TbChd_ChildForeignKeyColumn_Id"]).ItemsSource = new ComboItemList(WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty, ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);
                ((vXamComboColumn)navList.Columns["TbChd_ChildForeignKeyColumn_Id"]).DisplayMemberPath = "ItemName";
                WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)navList.Columns["TbChd_ChildForeignKeyColumn22_Id"]).ItemsSource = new ComboItemList(WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty, ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);
                ((vXamComboColumn)navList.Columns["TbChd_ChildForeignKeyColumn22_Id"]).DisplayMemberPath = "ItemName";
                WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)navList.Columns["TbChd_ChildForeignKeyColumn33_Id"]).ItemsSource = new ComboItemList(WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty, ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);
                ((vXamComboColumn)navList.Columns["TbChd_ChildForeignKeyColumn33_Id"]).DisplayMemberPath = "ItemName";
                WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Leave);
            }
        }

        void WcTable_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["TbChd_Child_Tb_Id"]).ItemsSource = WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["TbChd_ChildForeignKeyColumn_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["TbChd_ChildForeignKeyColumn22_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["TbChd_ChildForeignKeyColumn33_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        #region Combo ItemsSource for Non-Static Lists

        #endregion region Combo ItemsSource for Non-Static Lists

        void SetNewRowValidation()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Enter);
                if (_activeRow == null) { return; }
                WcTableChild_Bll obj = _activeRow.Data as WcTableChild_Bll;
                if (obj == null) { return; }
                // Note:  The lines to set the validaton styles below are not needed as the styles are being set from converters in the xaml style propeties, but they are left here incase needed in the future.  this is new code and we are unsure how it will evolve.
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Leave);
            }
        }

        #endregion Methods


    }
}
