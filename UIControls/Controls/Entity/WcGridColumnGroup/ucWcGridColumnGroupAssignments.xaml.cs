﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationTypeServices;
using EntityWireTypeSL;
using Ifx.SL;
using ProxyWrapper;
using vControls;
using vUICommon;
using Velocity.SL;

namespace UIControls
{
    public partial class ucWcGridColumnGroupAssignments : UserControl
    {



        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "ucWcPropsTileItemAssignments";

        ObservableCollection<WcGridColumnGroupItem_lstAssigned_Binding> _listAssigned = new ObservableCollection<WcGridColumnGroupItem_lstAssigned_Binding>();
        ObservableCollection<WcGridColumnGroupItem_lstNotAssigned_Binding> _listAvailable = new ObservableCollection<WcGridColumnGroupItem_lstNotAssigned_Binding>();

        WcGridColumnGroupService_ProxyWrapper _proxy = null;
        Guid? _grdColGrp_Id = null;
        Guid? _tb_Id = null;

        #endregion Initialize Variables



        #region Constructors



        public ucWcGridColumnGroupAssignments()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcPropsTileItemAssignments", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();

                _proxy = new WcGridColumnGroupService_ProxyWrapper();

                _proxy.ExecuteWcGridColumnGroup_AssignItemsCompleted += _proxy_ExecuteWcGridColumnGroup_AssignItemsCompleted;
                _proxy.GetWcGridColumnGroupItem_lstAssignedCompleted += _proxy_GetWcGridColumnGroupItem_lstAssignedCompleted;
                _proxy.GetWcGridColumnGroupItem_lstNotAssignedCompleted += _proxy_GetWcGridColumnGroupItem_lstNotAssignedCompleted;


                //xgdAssigned.CellClicked += new EventHandler<Infragistics.Controls.Grids.CellClickedEventArgs>(xgdAssigned_CellClicked);
                //xgdAvailable.CellClicked += new EventHandler<Infragistics.Controls.Grids.CellClickedEventArgs>(xgdAvailable_CellClicked);

                xgdAssigned.ItemsSource = _listAssigned;
                xgdAvailable.ItemsSource = _listAvailable;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcPropsTileItemAssignments", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcPropsTileItemAssignments", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors


        public Guid? GrdColGrp_Id
        {
            get { return _grdColGrp_Id; }
            set { _grdColGrp_Id = value; }
        }

        public Guid? Tb_Id
        {
            get { return _tb_Id; }
            set { _tb_Id = value; }
        }


        public void LoadData(Guid? grdColGrp_Id, Guid? tb_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadData", IfxTraceCategory.Enter);

                _grdColGrp_Id = grdColGrp_Id;
                _tb_Id = tb_Id;
                xgdAvailable_RowSource();
                xgdAssigned_RowSource();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadData", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadData", IfxTraceCategory.Leave);
            }
        }
        

        void xgdAvailable_RowSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "xgdAvailable_RowSource", IfxTraceCategory.Enter);

                if (_tb_Id == null || _grdColGrp_Id == null)
                {
                    _listAvailable.Clear();
                    return;
                }
                _proxy.Begin_GetWcGridColumnGroupItem_lstNotAssigned((Guid)_tb_Id, (Guid)_grdColGrp_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "xgdAvailable_RowSource", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "xgdAvailable_RowSource", IfxTraceCategory.Leave);
            }
        }

        void xgdAssigned_RowSource()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "xgdAssigned_RowSource", IfxTraceCategory.Enter);

                if (_grdColGrp_Id == null)
                {
                    _listAssigned.Clear();
                    return;
                }

                _proxy.Begin_GetWcGridColumnGroupItem_lstAssigned((Guid)_grdColGrp_Id);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "xgdAssigned_RowSource", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "xgdAssigned_RowSource", IfxTraceCategory.Leave);
            }
        }


        #region Events


        //void xgdUsers_SelectedRowsCollectionChanged(object sender, Infragistics.Controls.Grids.SelectionCollectionChangedEventArgs<Infragistics.Controls.Grids.SelectedRowsCollection> e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged", IfxTraceCategory.Enter);

        //        if (e.NewSelectedItems.Count == 1)
        //        {
        //            aspnet_UserNames_Binding obj = e.NewSelectedItems[0].Data as aspnet_UserNames_Binding;
        //            if (obj == null) { return; }
        //            CurrentUserId = obj.UserId;
        //            xgdAvailable_RowSource();
        //            xgdAssigned_RowSource();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged", ex);
        //        IfxWrapperException.GetError(ex, (Guid)traceId);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_SelectedRowsCollectionChanged", IfxTraceCategory.Leave);
        //    }
        //}


        #region Grid Buttons

        private void btnAssign_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAssign_Click", IfxTraceCategory.Enter);

                WcGridColumnGroupItem_lstNotAssigned_Binding data = e.Data as WcGridColumnGroupItem_lstNotAssigned_Binding;
                if (data == null) { return; }

                _proxy.Begin_ExecuteWcGridColumnGroup_AssignItems(true, null, _grdColGrp_Id, data.TbC_Id, (Guid)Credentials.UserId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAssign_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAssign_Click", IfxTraceCategory.Leave);
            }
        }

        private void btnRemove_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRemove_Click", IfxTraceCategory.Enter);

                WcGridColumnGroupItem_lstAssigned_Binding data = e.Data as WcGridColumnGroupItem_lstAssigned_Binding;
                if (data == null) { return; }

                _proxy.Begin_ExecuteWcGridColumnGroup_AssignItems(false, data.GrdColGrpItm_Id, null, null, null);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRemove_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRemove_Click", IfxTraceCategory.Leave);
            }
        }

        #endregion Grid Buttons


        #endregion Events


        #region Fetch and Set Data





        private void _proxy_GetWcGridColumnGroupItem_lstNotAssignedCompleted(object sender, GetWcGridColumnGroupItem_lstNotAssignedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetWcGridColumnGroupItem_lstNotAssignedCompleted", IfxTraceCategory.Enter);
                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                if (data != null)
                {
                    _listAvailable.Clear();
                    for (int i = 0; i < data.Length; i++)
                    {
                        object[] obj = (object[])data[i];
                        _listAvailable.Add(new WcGridColumnGroupItem_lstNotAssigned_Binding(obj));
                    }
                }
                else
                {
                    _listAvailable.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetWcGridColumnGroupItem_lstNotAssignedCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetWcGridColumnGroupItem_lstNotAssignedCompleted", IfxTraceCategory.Leave);
            }
        }

        private void _proxy_GetWcGridColumnGroupItem_lstAssignedCompleted(object sender, GetWcGridColumnGroupItem_lstAssignedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetWcGridColumnGroupItem_lstAssignedCompleted", IfxTraceCategory.Enter);
                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                if (data != null)
                {
                    _listAssigned.Clear();
                    for (int i = 0; i < data.Length; i++)
                    {
                        object[] obj = (object[])data[i];
                        _listAssigned.Add(new WcGridColumnGroupItem_lstAssigned_Binding(obj));
                    }
                }
                else
                {
                    _listAssigned.Clear();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetWcGridColumnGroupItem_lstAssignedCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetWcGridColumnGroupItem_lstAssignedCompleted", IfxTraceCategory.Leave);
            }
        }

        //void Execute_aspnet_UsersInRoles_AssignRolesToUserCompleted(object sender, Execute_aspnet_UsersInRoles_AssignRolesToUserCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Execute_aspnet_UsersInRoles_AssignRolesToUserCompleted", IfxTraceCategory.Enter);
        //        object[] data = e.Result;
        //        int? success = data[0] as int?;
        //        if (success == null)
        //        {
        //            MessageBox.Show("There was an error in assigning a role to this user.  Please contact support.", "", MessageBoxButton.OK);
        //            return;
        //        }
        //        else
        //        {
        //            if (success == 0)
        //            {
        //                MessageBox.Show("There was an error in assigning a role to this user.  Please contact support.", "", MessageBoxButton.OK);
        //                return;
        //            }
        //            else
        //            {
        //                xgdAvailable_RowSource();
        //                xgdAssigned_RowSource();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Execute_aspnet_UsersInRoles_AssignRolesToUserCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Execute_aspnet_UsersInRoles_AssignRolesToUserCompleted", IfxTraceCategory.Leave);
        //    }
        //}


        private void _proxy_ExecuteWcGridColumnGroup_AssignItemsCompleted(object sender, ExecuteWcGridColumnGroup_AssignItemsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_ExecuteWcGridColumnGroup_AssignItemsCompleted", IfxTraceCategory.Enter);
                object[] data = e.Result;
                int? success = data[0] as int?;
                if (success == null)
                {
                    MessageBox.Show("There was an error in removing a role.  Please contact support.", "", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    if (success == 0)
                    {
                        MessageBox.Show("There was an error in removing a role.  Please contact support.", "", MessageBoxButton.OK);
                        return;
                    }
                    else
                    {
                        xgdAssigned_RowSource();
                        xgdAvailable_RowSource();
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_ExecuteWcGridColumnGroup_AssignItemsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_ExecuteWcGridColumnGroup_AssignItemsCompleted", IfxTraceCategory.Leave);
            }
        }


        #endregion Fetch and Set Data


    }
}
