using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class WcGridColumnGroupConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcGridColumnGroupConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public WcGridColumnGroupConcurrencyList(WcGridColumnGroup_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcGridColumnGroupConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Sort", data.C.GrdColGrp_Sort, data.X.GrdColGrp_Sort));
                _concurrencyList.Add(new ConcurrencyItem("System Name", data.C.GrdColGrp_Name, data.X.GrdColGrp_Name));
                _concurrencyList.Add(new ConcurrencyItem("Group Name", data.C.GrdColGrp_HeaderText, data.X.GrdColGrp_HeaderText));
                _concurrencyList.Add(new ConcurrencyItem("Is Default View", data.C.GrdColGrp_IsDefaultView, data.X.GrdColGrp_IsDefaultView));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.GrdColGrp_IsActiveRow, data.X.GrdColGrp_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



