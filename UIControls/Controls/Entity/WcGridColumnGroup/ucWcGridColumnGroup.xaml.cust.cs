using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using EntityBll.SL;
using Ifx.SL;
using IfxUserViewLogSL;
using TypeServices;
using vUICommon;

// Gen Timestamp:  12/27/2017 7:56:47 PM

namespace UIControls
{
    public partial class ucWcGridColumnGroup
    {



        #region Initialize Variables

        private ucWcGridColumnGroupAssignments _ucAssignments = null;

        #endregion Initialize Variables




        void CustomConstructionCode()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", IfxTraceCategory.Enter);

                Load_ucAssignments();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "HideGridColumns", IfxTraceCategory.Leave);
            }
        }




        #region Copy Code Gen Methods from the Codebehind class here



        public void SetStateFromParent(object ancestorId, string parentType, int? intParentId, Guid? guidParentId, object objParentId, int? intCurrentId, Guid? guidCurrentId, object objCurrentId, IBusinessObject currentBusinessObject, object[] list, string newText)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                // Overload 9
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Enter);
                _intParentId = intParentId;
                if (guidParentId == null)
                {
                    _guidParentId = objParentId as Guid?;
                }
                else
                {
                    _guidParentId = guidParentId;
                }
                
                _intCurrentId = intCurrentId;
                _guidCurrentId = guidCurrentId;
                //_guidCurrentId = objCurrentId as Guid?;
                //_oCurrentId = objCurrentId;

                _parentType = parentType;
                _currentBusinessObject = (WcGridColumnGroup_Bll)currentBusinessObject;  

                ucNav.ParentType = _parentType;
                ucNav.GuidParentId = _guidParentId;  
                ucNav.ObjectParentId = objParentId;

                if (_FLG_IsLoaded == false)
                {
                    InitializeControl();
                }
                if (list == null)
                {
                    //  **********************************      N E E D   C U S T O M   C O D E    H E R E  ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! 
           
                    if (_guidParentId == null)
                    {
                        //ucNav.NavList_ItemSource.Clear();
                        NavListRefreshFromObjectArray(null);
                        ucNav.IsEnabled = false;
                    }
                    else
                    {
                        ucNav.IsEnabled = true;

                        LoadNavlistData();
                        //_wcGridColumnGroupProxy.Begin_WcGridColumnGroup_GetListByFK((Guid)_guidParentId);
                        //ucNav.navList.Cursor = Cursors.Wait;
                    }
                }
                else
                {
                    NavListRefreshFromObjectArray(list);
                }

                // Use    intParentId     if the parent is an Integer.  we might need to be using the Ancestor Id instead.
                if (guidParentId != null)
                {
                    ucNav.Set_vXamComboColumn_ItemSourcesWithParams();
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetStateFromParent Overload 9", IfxTraceCategory.Leave);
            }
        }

        
        public void LoadNavlistData()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadNavlistData", IfxTraceCategory.Enter);

                _currentBusinessObject = null;
                ucNav.IsEnabled = true;
                _wcGridColumnGroupProxy.Begin_WcGridColumnGroup_GetListByFK((Guid)_guidParentId);
                //switch (_parentType)
                //{
                //    case "someParent":
                //        //do something
                //        break;
                //    default:
                //        throw new Exception("Missing Parent Type");
                //}

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadNavlistData", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _FLG_IsLoaded = true;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadNavlistData", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// Call override from the tab controlís tab index changed event (or the equivalent
        /// of).
        /// </summary>
        private void SyncControlsWithCurrentBusinessObject(string selectedTab)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject", IfxTraceCategory.Enter);
                object currentId = null;
                if (ucNav.IsSplitSreenMode)
                {
                    if (_currentBusinessObject != null)
                    {
                        //**  need this here becuase if we are hiding the props tab, it will never get called and the well tab will remain disabled.
                        ConfigureToCurrentEntityState(this, _currentBusinessObject.StateSwitch);
                        currentId = _currentBusinessObject.GrdColGrp_Id;

                        _ucAssignments.LoadData(_currentBusinessObject.GrdColGrp_Id, _currentBusinessObject.GrdColGrp_Tb_Id);

                    }
                    else
                    {
                        _ucAssignments.LoadData(null, null);

                    }
                    //SyncControlsWithCurrentBusinessObject_CustomCode1();
                    //switch (selectedTab)
                    //{
                    //    case "tbiWcGridColumnGroupProps":
                    //        _isPropsTabSelected = true;

                    //        if (ucNav.IsSplitSreenMode == false) { return; }  // this might be a bad idea of we fail to put the correct biz object in ucProps as it may become out of synch with the grid.

                    //        if (ucProps == null) { ucProps_Load(); }

                    //        ucProps.CurrentBusinessObject = _currentBusinessObject;
                    //        break;
                    //    case "xxxxxxxxxxxx":
                    //        //ucWlD.SetStateFromParent(_prj_Id, "ucWell", null, currentId, null, null, null, null, null);
                    //        break;
                    //    default:
                    //        _isPropsTabSelected = false;
                    //        break;
                    //}
                }
                //SyncControlsWithCurrentBusinessObject_CustomCode2(selectedTab, currentId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject", IfxTraceCategory.Leave);
            }
        }



        #endregion Copy Code Gen Methods from the Codebehind class here

        #region Insert Custom Code Here


        void Load_ucAssignments()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Load_ucAssignments", IfxTraceCategory.Enter);

                _ucAssignments = new ucWcGridColumnGroupAssignments();
                myGrid.Children.Add(_ucAssignments);
                Grid.SetColumn(_ucAssignments, 2);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Load_ucAssignments", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Load_ucAssignments", IfxTraceCategory.Leave);
            }
        }



        #endregion Insert Custom Code Here

        #region  Combo DataSources and Events

        #endregion  Combo DataSources and Events

        #region Method Extentions For Custom Code

        void ConfigureToCurrentEntityState_CustomCode(EntityStateSwitch state)
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState_CustomCode", IfxTraceCategory.Enter);
                //switch (state)
                //{
                //    case EntityStateSwitch.None:
                //        if (_currentBusinessObject == null)
                //        {
                //            xxx.IsEnabled = false;
                //        }
                //        else
                //        {
                //            xxx.IsEnabled = true;
                //        }
                //        break;
                //    case EntityStateSwitch.NewInvalidNotDirty:
                //        xxx.IsEnabled = false;
                //        break;
                //    case EntityStateSwitch.NewValidNotDirty:
                //        xxx.IsEnabled = false;
                //        break;
                //    case EntityStateSwitch.NewValidDirty:
                //        xxx.IsEnabled = false;
                //        break;
                //    case EntityStateSwitch.NewInvalidDirty:
                //        xxx.IsEnabled = false;
                //        break;
                //    case EntityStateSwitch.ExistingInvalidDirty:
                //        xxx.IsEnabled = false;
                //        break;
                //    case EntityStateSwitch.ExistingValidDirty:
                //        xxx.IsEnabled = false;
                //        break;
                //    case EntityStateSwitch.ExistingValidNotDirty:
                //        xxx.IsEnabled = true;
                //        break;
                //}
            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState_CustomCode", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    _FLG_IsLoaded = true;
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureToCurrentEntityState_CustomCode", IfxTraceCategory.Leave);
            //}
        }

        void SyncControlsWithCurrentBusinessObject_CustomCode1()
        {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", IfxTraceCategory.Enter);


            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    _FLG_IsLoaded = true;
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode1", IfxTraceCategory.Leave);
            //}
        }

        void SyncControlsWithCurrentBusinessObject_CustomCode2(string selectedTab, object currentId)
            {
            //Guid? traceId = Guid.NewGuid();
            //try
            //{
            //    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", IfxTraceCategory.Enter);
            //    switch (selectedTab)
            //    {
            //        case "tbi_ucTbCol":
                //            if (_ucTbCol == null) { ucTbCol_Load(); }
                //            _ucTbCol.SetStateFromParent(null, "ucWcTable", null, null, currentId, null, null, currentChildId, null, null, "");
                //            break;
                //        case "tbi_ucTbGrpAsgn":
                //            if (_ucTblGrpAssign == null) { ucTblGrpAssign_Load(); }
                //            if (_currentBusinessObject != null)
                //            {
                //                _ucTblGrpAssign.SetStateFromParent(_currentBusinessObject.Tb_ApVrsn_Id, _currentBusinessObject.Tb_Id);
                //            }
                //            break;
                //    }

            //}
            //catch (Exception ex)
            //{
            //    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", ex);
				   // throw IfxWrapperException.GetError(ex);
            //}
            //finally
            //{
            //    _FLG_IsLoaded = true;
            //    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncControlsWithCurrentBusinessObject_CustomCode2", IfxTraceCategory.Leave);
            //}
            }

        #endregion Method Extentions For Custom Code



        #region Standard CodeGen Methods Subject to Customization



        #endregion Standard CodeGen Methods Subject to Customization



        }
    }
