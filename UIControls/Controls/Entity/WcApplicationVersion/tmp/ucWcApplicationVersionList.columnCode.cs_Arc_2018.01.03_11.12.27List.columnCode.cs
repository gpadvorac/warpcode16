using System;
using System.Windows;
using System.Windows.Controls;
using Ifx.SL;
using TypeServices;
using vControls;
using EntityBll.SL;
using Infragistics.Controls.Editors;
using vUICommon;
using EntityWireTypeSL;
using Infragistics.Controls.Grids;
using vComboDataTypes;
using System.Collections.Generic;
using vUICommon.Controls;
using UIControls.Globalization.WcApplicationVersion;
namespace UIControls
{
    public partial class ucWcApplicationVersionList
    {

        
        #region GridCell Editor Events
        

        private void XamGridTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                if ((ctl.Text.Length>0) && (ctl.Text.Substring(ctl.Text.Length - 1, 1) == " "))
                {
                    return;
                }
                WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                if (obj == null) { return; }
                //switch (ctl.Name)
                //{
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vTextColumn_TextChanged(object sender, vTextColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (e.Key)
                    {
                        case "ApVrsn_MajorVersion":
                            obj.ApVrsn_MajorVersion_asString = ctl.Text;
                            break;
                        case "ApVrsn_MinorVersion":
                            obj.ApVrsn_MinorVersion_asString = ctl.Text;
                            break;
                        case "ApVrsn_VersionIteration":
                            obj.ApVrsn_VersionIteration_asString = ctl.Text;
                            break;
                        case "ApVrsn_Server":
                            obj.ApVrsn_Server = ctl.Text;
                            break;
                        case "ApVrsn_DbName":
                            obj.ApVrsn_DbName = ctl.Text;
                            break;
                        case "ApVrsn_SolutionPath":
                            obj.ApVrsn_SolutionPath = ctl.Text;
                            break;
                        case "ApVrsn_DefaultUIAssembly":
                            obj.ApVrsn_DefaultUIAssembly = ctl.Text;
                            break;
                        case "ApVrsn_DefaultUIAssemblyPath":
                            obj.ApVrsn_DefaultUIAssemblyPath = ctl.Text;
                            break;
                        case "ApVrsn_DefaultWireTypePath":
                            obj.ApVrsn_DefaultWireTypePath = ctl.Text;
                            break;
                        case "ApVrsn_WebServerURL":
                            obj.ApVrsn_WebServerURL = ctl.Text;
                            break;
                        case "ApVrsn_WebsiteCodeFolderPath":
                            obj.ApVrsn_WebsiteCodeFolderPath = ctl.Text;
                            break;
                        case "ApVrsn_WebserviceCodeFolderPath":
                            obj.ApVrsn_WebserviceCodeFolderPath = ctl.Text;
                            break;
                        case "ApVrsn_StoredProcCodeFolder":
                            obj.ApVrsn_StoredProcCodeFolder = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vDecimalColumn_TextChanged(object sender, vDecimalColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamComboEditor comboEditor = sender as XamComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_SelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (ctl.Name)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_SelectionChanged(object sender, vXamComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                // The next 2 lines are for the Not In List feature.  If the 'Non-In-List' event is fireing, we dont want to run this event right now.
                if (ctl.SelectedItem == null) { return; }
                if (((ComboItem)ctl.SelectedItem).Id == null) { return; }
                if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (e.Key)
                    {
                    }
                }
                vXamComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Enter);
                if (e.Key == System.Windows.Input.Key.Escape)
                {
                    WcApplicationVersion_Bll obj = _activeRow.Data as WcApplicationVersion_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_KeyUp(object sender, vXamComboColumnKeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Enter);
                if (e.XamComboColumnKeyEventArgs.Key == System.Windows.Input.Key.Escape)
                {
                    WcApplicationVersion_Bll obj = _activeRow.Data as WcApplicationVersion_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void XamMultiColumnComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamMultiColumnComboEditor comboEditor = sender as XamMultiColumnComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void vXamMultiColumnComboColumn_SelectionChanged(object sender, vXamMultiColumnComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vXamMultiColumnComboEditor ctl = sender as vXamMultiColumnComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (e.Key)
                    {
                    }
                }
                vXamMultiColumnComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridDatePicker_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                if (obj == null) { return; }
                switch (ctl.Name)
                {
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Leave);
            }
        }





        private void XamColorPicker_SelectedColorChanged(object sender, SelectedColorChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamColorPicker ctl = sender as XamColorPicker;
                if (ctl == null) { return; }
                WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                if (obj == null) { return; }
                switch (((CellControl)ctl.Parent).Cell.Column.Key)
                {
                }
                XamColorPicker_SelectedColorChanged_CustomCode(ctl, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Leave);
            }
        }





        //  may not need this as the prop gets hit through binding.
        public void vDatePickerColumn_TextChanged(object sender, vDatePickerColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                vDatePickerColumn_TextChanged_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vCheckColumn_CheckedChanged(object sender, vCheckColumnCheckedChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Enter);
                //if (_isRowInEditMode == false) { return; }
                vCheckBox ctl = sender as vCheckBox;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;
                    switch (e.Key)
                    {
                    case "ApVrsn_UseLegacyConnectionCode":
                        obj.ApVrsn_UseLegacyConnectionCode = (bool)ctl.IsChecked;
                        break;
                    case "ApVrsn_IsMulticultural":
                        obj.ApVrsn_IsMulticultural = (bool)ctl.IsChecked;
                        break;
                    case "ApVrsn_IsActiveRow":
                        obj.ApVrsn_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Leave);
            }
        }

        #endregion GridCell Editor Events

        
        #region Grid Other Events and Event Support Methods
        

        void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Enter);
                Cell cell = (Cell)navList.ActiveCell;
                IvControlsValidation ctl = cell.Control.Content as IvControlsValidation;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcApplicationVersion_Bll)
                {
                    WcApplicationVersion_Bll obj = navList.ActiveCell.Row.Data as WcApplicationVersion_Bll;

                    switch (e.PropertyName)
                    {
                        case "AttachmentCount":
                            SetGridCellValidationAppeance(ctl, obj, "AttachmentCount");
                            break;
                        case "ApVrsn_MajorVersion":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_MajorVersion");
                            break;
                        case "ApVrsn_MinorVersion":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_MinorVersion");
                            break;
                        case "ApVrsn_VersionIteration":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_VersionIteration");
                            break;
                        case "ApVrsn_Server":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_Server");
                            break;
                        case "ApVrsn_DbName":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_DbName");
                            break;
                        case "ApVrsn_SolutionPath":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_SolutionPath");
                            break;
                        case "ApVrsn_DefaultUIAssembly":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_DefaultUIAssembly");
                            break;
                        case "ApVrsn_DefaultUIAssemblyPath":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_DefaultUIAssemblyPath");
                            break;
                        case "ApVrsn_DefaultWireTypePath":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_DefaultWireTypePath");
                            break;
                        case "ApVrsn_WebServerURL":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_WebServerURL");
                            break;
                        case "ApVrsn_WebsiteCodeFolderPath":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_WebsiteCodeFolderPath");
                            break;
                        case "ApVrsn_WebserviceCodeFolderPath":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_WebserviceCodeFolderPath");
                            break;
                        case "ApVrsn_StoredProcCodeFolder":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_StoredProcCodeFolder");
                            break;
                        case "ApVrsn_UseLegacyConnectionCode":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_UseLegacyConnectionCode");
                            break;
                        case "ApVrsn_IsMulticultural":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_IsMulticultural");
                            break;
                        case "ApVrsn_IsActiveRow":
                            SetGridCellValidationAppeance(ctl, obj, "ApVrsn_IsActiveRow");
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Leave);
            }
        }

        #endregion Grid Other Events and Event Support Methods

        
        #region Methods


        void AssignTextLengthLabels()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Enter);

                ((vTextColumn)navList.Columns["ApVrsn_Server"]).MaxTextLength = 25;
                ((vTextColumn)navList.Columns["ApVrsn_DbName"]).MaxTextLength = 75;
                ((vTextColumn)navList.Columns["ApVrsn_SolutionPath"]).MaxTextLength = 255;
                ((vTextColumn)navList.Columns["ApVrsn_DefaultUIAssembly"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["ApVrsn_DefaultUIAssemblyPath"]).MaxTextLength = 255;
                ((vTextColumn)navList.Columns["ApVrsn_DefaultWireTypePath"]).MaxTextLength = 255;
                ((vTextColumn)navList.Columns["ApVrsn_WebServerURL"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["ApVrsn_WebsiteCodeFolderPath"]).MaxTextLength = 255;
                ((vTextColumn)navList.Columns["ApVrsn_WebserviceCodeFolderPath"]).MaxTextLength = 255;
                ((vTextColumn)navList.Columns["ApVrsn_StoredProcCodeFolder"]).MaxTextLength = 255;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Leave);
            }
        }



        void ConfigureColumnHeaderTooltips()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Enter);

                    // AttachmentCount
                    ((IvColumn)navList.Columns["AttachmentCount"]).HeaderToolTipCaption = StringsWcApplicationVersionList.AttachmentCount_Vbs; 
                    ((IvColumn)navList.Columns["AttachmentCount"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.AttachmentCount_1};
                    ((IvColumn)navList.Columns["AttachmentCount"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_Server
                    ((IvColumn)navList.Columns["ApVrsn_Server"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_Server_Vbs; 
                    ((IvColumn)navList.Columns["ApVrsn_Server"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.ApVrsn_Server_1};
                    ((IvColumn)navList.Columns["ApVrsn_Server"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_DbName
                    ((IvColumn)navList.Columns["ApVrsn_DbName"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_DbName_Vbs; 
                    ((IvColumn)navList.Columns["ApVrsn_DbName"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.ApVrsn_DbName_1};
                    ((IvColumn)navList.Columns["ApVrsn_DbName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_SolutionPath
                    ((IvColumn)navList.Columns["ApVrsn_SolutionPath"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_SolutionPath_Vbs; 
                    ((IvColumn)navList.Columns["ApVrsn_SolutionPath"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.ApVrsn_SolutionPath_1};
                    ((IvColumn)navList.Columns["ApVrsn_SolutionPath"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_DefaultUIAssembly
                    ((IvColumn)navList.Columns["ApVrsn_DefaultUIAssembly"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_DefaultUIAssembly_Vbs; 
                    ((IvColumn)navList.Columns["ApVrsn_DefaultUIAssembly"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.ApVrsn_DefaultUIAssembly_1};
                    ((IvColumn)navList.Columns["ApVrsn_DefaultUIAssembly"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_DefaultUIAssemblyPath
                    ((IvColumn)navList.Columns["ApVrsn_DefaultUIAssemblyPath"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_DefaultUIAssemblyPath_Vbs; 
                    ((IvColumn)navList.Columns["ApVrsn_DefaultUIAssemblyPath"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.ApVrsn_DefaultUIAssemblyPath_1};
                    ((IvColumn)navList.Columns["ApVrsn_DefaultUIAssemblyPath"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_DefaultWireTypePath
                    ((IvColumn)navList.Columns["ApVrsn_DefaultWireTypePath"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_DefaultWireTypePath_Vbs; 
                    ((IvColumn)navList.Columns["ApVrsn_DefaultWireTypePath"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.ApVrsn_DefaultWireTypePath_1};
                    ((IvColumn)navList.Columns["ApVrsn_DefaultWireTypePath"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_WebsiteCodeFolderPath
                    ((IvColumn)navList.Columns["ApVrsn_WebsiteCodeFolderPath"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_WebsiteCodeFolderPath_Vbs; 
                    ((IvColumn)navList.Columns["ApVrsn_WebsiteCodeFolderPath"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.ApVrsn_WebsiteCodeFolderPath_1};
                    ((IvColumn)navList.Columns["ApVrsn_WebsiteCodeFolderPath"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_WebserviceCodeFolderPath
                    ((IvColumn)navList.Columns["ApVrsn_WebserviceCodeFolderPath"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_WebserviceCodeFolderPath_Vbs; 
                    ((IvColumn)navList.Columns["ApVrsn_WebserviceCodeFolderPath"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.ApVrsn_WebserviceCodeFolderPath_1};
                    ((IvColumn)navList.Columns["ApVrsn_WebserviceCodeFolderPath"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_StoredProcCodeFolder
                    ((IvColumn)navList.Columns["ApVrsn_StoredProcCodeFolder"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_StoredProcCodeFolder_Vbs; 
                    ((IvColumn)navList.Columns["ApVrsn_StoredProcCodeFolder"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.ApVrsn_StoredProcCodeFolder_1, StringsWcApplicationVersionListTooltips.ApVrsn_StoredProcCodeFolder_2, StringsWcApplicationVersionListTooltips.ApVrsn_StoredProcCodeFolder_3};
                    ((IvColumn)navList.Columns["ApVrsn_StoredProcCodeFolder"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_UseLegacyConnectionCode
                    ((IvColumn)navList.Columns["ApVrsn_UseLegacyConnectionCode"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_UseLegacyConnectionCode_Vbs; 
                    ((IvColumn)navList.Columns["ApVrsn_UseLegacyConnectionCode"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_IsMulticultural
                    ((IvColumn)navList.Columns["ApVrsn_IsMulticultural"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_IsMulticultural_Vbs; 
                    ((IvColumn)navList.Columns["ApVrsn_IsMulticultural"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.ApVrsn_IsMulticultural_1};
                    ((IvColumn)navList.Columns["ApVrsn_IsMulticultural"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // ApVrsn_IsActiveRow
                    ((IvColumn)navList.Columns["ApVrsn_IsActiveRow"]).HeaderToolTipCaption = StringsWcApplicationVersionList.ApVrsn_IsActiveRow_Vbs; 
                    ((IvColumn)navList.Columns["ApVrsn_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcApplicationVersionListTooltips.ApVrsn_IsActiveRow_1, StringsWcApplicationVersionListTooltips.ApVrsn_IsActiveRow_2};
                    ((IvColumn)navList.Columns["ApVrsn_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();
                ConfigureColumnHeaderTooltips_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Leave);
            }
        }


        void Set_vXamComboColumn_ItemSources()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Enter);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Leave);
            }
        }


        #region Combo ItemsSource for Non-Static Lists

        #endregion region Combo ItemsSource for Non-Static Lists

        void SetNewRowValidation()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Enter);
                if (_activeRow == null) { return; }
                WcApplicationVersion_Bll obj = _activeRow.Data as WcApplicationVersion_Bll;
                if (obj == null) { return; }
                // Note:  The lines to set the validaton styles below are not needed as the styles are being set from converters in the xaml style propeties, but they are left here incase needed in the future.  this is new code and we are unsure how it will evolve.
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Leave);
            }
        }

        #endregion Methods


    }
}
