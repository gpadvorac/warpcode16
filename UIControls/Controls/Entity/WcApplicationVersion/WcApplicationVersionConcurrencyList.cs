using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class WcApplicationVersionConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcApplicationVersionConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public WcApplicationVersionConcurrencyList(WcApplicationVersion_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcApplicationVersionConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Attachments", data.C.AttachmentCount, data.X.AttachmentCount));
                _concurrencyList.Add(new ConcurrencyItem("Discussions", data.C.DiscussionCount, data.X.DiscussionCount));
                _concurrencyList.Add(new ConcurrencyItem("Major Version", data.C.ApVrsn_MajorVersion, data.X.ApVrsn_MajorVersion));
                _concurrencyList.Add(new ConcurrencyItem("Minor Version", data.C.ApVrsn_MinorVersion, data.X.ApVrsn_MinorVersion));
                _concurrencyList.Add(new ConcurrencyItem("Version Iteration", data.C.ApVrsn_VersionIteration, data.X.ApVrsn_VersionIteration));
                _concurrencyList.Add(new ConcurrencyItem("Database Name", data.C.ApVrsn_DbName, data.X.ApVrsn_DbName));
                _concurrencyList.Add(new ConcurrencyItem("Server", data.C.ApVrsn_Server, data.X.ApVrsn_Server));
                _concurrencyList.Add(new ConcurrencyItem("Use Legacy Connection Code", data.C.ApVrsn_UseLegacyConnectionCode, data.X.ApVrsn_UseLegacyConnectionCode));
                _concurrencyList.Add(new ConcurrencyItem("Solution Path", data.C.ApVrsn_SolutionPath, data.X.ApVrsn_SolutionPath));
                _concurrencyList.Add(new ConcurrencyItem("Default UI Assembly", data.C.ApVrsn_DefaultUIAssembly, data.X.ApVrsn_DefaultUIAssembly));
                _concurrencyList.Add(new ConcurrencyItem("Default UI Namespace", data.C.ApVrsn_DefaultUINamespace, data.X.ApVrsn_DefaultUINamespace));
                _concurrencyList.Add(new ConcurrencyItem("Default UI Assembly Path", data.C.ApVrsn_DefaultUIAssemblyPath, data.X.ApVrsn_DefaultUIAssemblyPath));
                _concurrencyList.Add(new ConcurrencyItem("Proxy Assembly Name", data.C.ApVrsn_ProxyAssemblyName, data.X.ApVrsn_ProxyAssemblyName));
                _concurrencyList.Add(new ConcurrencyItem("Proxy Namespace", data.C.ApVrsn_ProxyNamespace, data.X.ApVrsn_ProxyNamespace));
                _concurrencyList.Add(new ConcurrencyItem("Proxy Assembly Path", data.C.ApVrsn_ProxyAssemblyPath, data.X.ApVrsn_ProxyAssemblyPath));
                _concurrencyList.Add(new ConcurrencyItem("Default WireType Assembly", data.C.ApVrsn_DefaultWireTypeAssembly, data.X.ApVrsn_DefaultWireTypeAssembly));
                _concurrencyList.Add(new ConcurrencyItem("Default WireType Namespace", data.C.ApVrsn_DefaultWireTypeNamespace, data.X.ApVrsn_DefaultWireTypeNamespace));
                _concurrencyList.Add(new ConcurrencyItem("Default Wire Type Path", data.C.ApVrsn_DefaultWireTypePath, data.X.ApVrsn_DefaultWireTypePath));
                _concurrencyList.Add(new ConcurrencyItem("Web Server URL", data.C.ApVrsn_WebServerURL, data.X.ApVrsn_WebServerURL));
                _concurrencyList.Add(new ConcurrencyItem("Website Code Folder Path", data.C.ApVrsn_WebsiteCodeFolderPath, data.X.ApVrsn_WebsiteCodeFolderPath));
                _concurrencyList.Add(new ConcurrencyItem("Default W.S. Code Fldr Path", data.C.ApVrsn_WebserviceCodeFolderPath, data.X.ApVrsn_WebserviceCodeFolderPath));
                _concurrencyList.Add(new ConcurrencyItem("Stored Proc. Code Folder", data.C.ApVrsn_StoredProcCodeFolder, data.X.ApVrsn_StoredProcCodeFolder));
                _concurrencyList.Add(new ConcurrencyItem("DataService Assembly Name", data.C.ApVrsn_DataServiceAssemblyName, data.X.ApVrsn_DataServiceAssemblyName));
                _concurrencyList.Add(new ConcurrencyItem("DataService Namespace", data.C.ApVrsn_DataServiceNamespace, data.X.ApVrsn_DataServiceNamespace));
                _concurrencyList.Add(new ConcurrencyItem("DataService Path", data.C.ApVrsn_DataServicePath, data.X.ApVrsn_DataServicePath));
                _concurrencyList.Add(new ConcurrencyItem("Is Multicultural", data.C.ApVrsn_IsMulticultural, data.X.ApVrsn_IsMulticultural));
                _concurrencyList.Add(new ConcurrencyItem("Notes", data.C.ApVrsn_Notes, data.X.ApVrsn_Notes));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.ApVrsn_IsActiveRow, data.X.ApVrsn_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



