﻿#pragma checksum "D:\Apps\WarpCode16\App\UIControls\Controls\Entity\Task\ucTaskAssignees.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F13E5CC17E2991749D03B7941028A371"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using CollapsingGridSplitter;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;
using vControls;


namespace UIControls {
    
    
    public partial class ucTaskAssignees : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal Infragistics.Controls.Grids.XamGrid xgdAval;
        
        internal vControls.vUnboundButtonColumn AddPerson;
        
        internal CollapsingGridSplitter.ExtendedGridSplitter gdSplitter;
        
        internal Infragistics.Controls.Grids.XamGrid xgdAssigned;
        
        internal vControls.vUnboundButtonColumn RemovePerson;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/UIControls;component/Controls/Entity/Task/ucTaskAssignees.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.xgdAval = ((Infragistics.Controls.Grids.XamGrid)(this.FindName("xgdAval")));
            this.AddPerson = ((vControls.vUnboundButtonColumn)(this.FindName("AddPerson")));
            this.gdSplitter = ((CollapsingGridSplitter.ExtendedGridSplitter)(this.FindName("gdSplitter")));
            this.xgdAssigned = ((Infragistics.Controls.Grids.XamGrid)(this.FindName("xgdAssigned")));
            this.RemovePerson = ((vControls.vUnboundButtonColumn)(this.FindName("RemovePerson")));
        }
    }
}

