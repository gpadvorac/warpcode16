﻿#pragma checksum "D:\Apps\WarpCode16\App\UIControls\Controls\Entity\Task\ucTaskProps.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "22134F505B7E109B2B6426D9ECDA09FA"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;
using vControls;


namespace UIControls {
    
    
    public partial class ucTaskProps : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.TextBlock lblTk_TkTp_Id;
        
        internal vControls.vXamComboEditor Tk_TkTp_Id;
        
        internal System.Windows.Controls.TextBlock lblTk_TkCt_Id;
        
        internal vControls.vXamComboEditor Tk_TkCt_Id;
        
        internal System.Windows.Controls.TextBlock lblCategoryDescription;
        
        internal vControls.vTextBox CategoryDescription;
        
        internal System.Windows.Controls.TextBlock lblTk_Name;
        
        internal vControls.vTextBox Tk_Name;
        
        internal System.Windows.Controls.TextBlock lblTk_Desc;
        
        internal vControls.vTextBox Tk_Desc;
        
        internal System.Windows.Controls.TextBlock lblTk_EstimatedTime;
        
        internal vControls.vTextBox Tk_EstimatedTime;
        
        internal System.Windows.Controls.TextBlock lblTk_ActualTime;
        
        internal vControls.vTextBox Tk_ActualTime;
        
        internal System.Windows.Controls.TextBlock lblTk_PercentComplete;
        
        internal vControls.vTextBox Tk_PercentComplete;
        
        internal System.Windows.Controls.TextBlock lblTk_StartDate;
        
        internal vControls.vDatePicker Tk_StartDate;
        
        internal System.Windows.Controls.TextBlock lblTk_Deadline;
        
        internal vControls.vDatePicker Tk_Deadline;
        
        internal System.Windows.Controls.TextBlock lblTk_TkSt_Id;
        
        internal vControls.vXamComboEditor Tk_TkSt_Id;
        
        internal System.Windows.Controls.TextBlock lblTk_TkPr_Id;
        
        internal vControls.vXamComboEditor Tk_TkPr_Id;
        
        internal System.Windows.Controls.TextBlock lblTk_Results;
        
        internal vControls.vTextBox Tk_Results;
        
        internal System.Windows.Controls.TextBlock lblTk_Remarks;
        
        internal vControls.vTextBox Tk_Remarks;
        
        internal System.Windows.Controls.TextBlock lblTk_IsPrivate;
        
        internal vControls.vCheckBox Tk_IsPrivate;
        
        internal System.Windows.Controls.TextBlock lblTk_IsActiveRow;
        
        internal vControls.vCheckBox Tk_IsActiveRow;
        
        internal System.Windows.Controls.TextBlock gdMain_ReadOnly1;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/UIControls;component/Controls/Entity/Task/ucTaskProps.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.lblTk_TkTp_Id = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_TkTp_Id")));
            this.Tk_TkTp_Id = ((vControls.vXamComboEditor)(this.FindName("Tk_TkTp_Id")));
            this.lblTk_TkCt_Id = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_TkCt_Id")));
            this.Tk_TkCt_Id = ((vControls.vXamComboEditor)(this.FindName("Tk_TkCt_Id")));
            this.lblCategoryDescription = ((System.Windows.Controls.TextBlock)(this.FindName("lblCategoryDescription")));
            this.CategoryDescription = ((vControls.vTextBox)(this.FindName("CategoryDescription")));
            this.lblTk_Name = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_Name")));
            this.Tk_Name = ((vControls.vTextBox)(this.FindName("Tk_Name")));
            this.lblTk_Desc = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_Desc")));
            this.Tk_Desc = ((vControls.vTextBox)(this.FindName("Tk_Desc")));
            this.lblTk_EstimatedTime = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_EstimatedTime")));
            this.Tk_EstimatedTime = ((vControls.vTextBox)(this.FindName("Tk_EstimatedTime")));
            this.lblTk_ActualTime = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_ActualTime")));
            this.Tk_ActualTime = ((vControls.vTextBox)(this.FindName("Tk_ActualTime")));
            this.lblTk_PercentComplete = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_PercentComplete")));
            this.Tk_PercentComplete = ((vControls.vTextBox)(this.FindName("Tk_PercentComplete")));
            this.lblTk_StartDate = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_StartDate")));
            this.Tk_StartDate = ((vControls.vDatePicker)(this.FindName("Tk_StartDate")));
            this.lblTk_Deadline = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_Deadline")));
            this.Tk_Deadline = ((vControls.vDatePicker)(this.FindName("Tk_Deadline")));
            this.lblTk_TkSt_Id = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_TkSt_Id")));
            this.Tk_TkSt_Id = ((vControls.vXamComboEditor)(this.FindName("Tk_TkSt_Id")));
            this.lblTk_TkPr_Id = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_TkPr_Id")));
            this.Tk_TkPr_Id = ((vControls.vXamComboEditor)(this.FindName("Tk_TkPr_Id")));
            this.lblTk_Results = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_Results")));
            this.Tk_Results = ((vControls.vTextBox)(this.FindName("Tk_Results")));
            this.lblTk_Remarks = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_Remarks")));
            this.Tk_Remarks = ((vControls.vTextBox)(this.FindName("Tk_Remarks")));
            this.lblTk_IsPrivate = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_IsPrivate")));
            this.Tk_IsPrivate = ((vControls.vCheckBox)(this.FindName("Tk_IsPrivate")));
            this.lblTk_IsActiveRow = ((System.Windows.Controls.TextBlock)(this.FindName("lblTk_IsActiveRow")));
            this.Tk_IsActiveRow = ((vControls.vCheckBox)(this.FindName("Tk_IsActiveRow")));
            this.gdMain_ReadOnly1 = ((System.Windows.Controls.TextBlock)(this.FindName("gdMain_ReadOnly1")));
        }
    }
}

