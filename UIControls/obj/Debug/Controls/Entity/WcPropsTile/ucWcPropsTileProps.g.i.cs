﻿#pragma checksum "D:\Apps\WarpCode16\App\UIControls\Controls\Entity\WcPropsTile\ucWcPropsTileProps.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "FCD29FB9B2786DC3F46B260BFAFDBBFF"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;
using vControls;


namespace UIControls {
    
    
    public partial class ucWcPropsTileProps : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.TextBlock lblPrpTl_Sort;
        
        internal vControls.vTextBox PrpTl_Sort;
        
        internal System.Windows.Controls.TextBlock lblPrpTl_GridName;
        
        internal vControls.vTextBox PrpTl_GridName;
        
        internal System.Windows.Controls.TextBlock lblPrpTl_Header;
        
        internal vControls.vTextBox PrpTl_Header;
        
        internal System.Windows.Controls.TextBlock lblPrpTl_IsDefaultView;
        
        internal vControls.vCheckBox PrpTl_IsDefaultView;
        
        internal System.Windows.Controls.TextBlock lblPrpTl_IsActiveRow;
        
        internal vControls.vCheckBox PrpTl_IsActiveRow;
        
        internal System.Windows.Controls.TextBlock gdMain_ReadOnly1;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/UIControls;component/Controls/Entity/WcPropsTile/ucWcPropsTileProps.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.lblPrpTl_Sort = ((System.Windows.Controls.TextBlock)(this.FindName("lblPrpTl_Sort")));
            this.PrpTl_Sort = ((vControls.vTextBox)(this.FindName("PrpTl_Sort")));
            this.lblPrpTl_GridName = ((System.Windows.Controls.TextBlock)(this.FindName("lblPrpTl_GridName")));
            this.PrpTl_GridName = ((vControls.vTextBox)(this.FindName("PrpTl_GridName")));
            this.lblPrpTl_Header = ((System.Windows.Controls.TextBlock)(this.FindName("lblPrpTl_Header")));
            this.PrpTl_Header = ((vControls.vTextBox)(this.FindName("PrpTl_Header")));
            this.lblPrpTl_IsDefaultView = ((System.Windows.Controls.TextBlock)(this.FindName("lblPrpTl_IsDefaultView")));
            this.PrpTl_IsDefaultView = ((vControls.vCheckBox)(this.FindName("PrpTl_IsDefaultView")));
            this.lblPrpTl_IsActiveRow = ((System.Windows.Controls.TextBlock)(this.FindName("lblPrpTl_IsActiveRow")));
            this.PrpTl_IsActiveRow = ((vControls.vCheckBox)(this.FindName("PrpTl_IsActiveRow")));
            this.gdMain_ReadOnly1 = ((System.Windows.Controls.TextBlock)(this.FindName("gdMain_ReadOnly1")));
        }
    }
}

