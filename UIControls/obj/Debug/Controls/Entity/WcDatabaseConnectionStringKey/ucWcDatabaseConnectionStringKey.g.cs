﻿#pragma checksum "D:\Apps\WarpCode16\App\UIControls\Controls\Entity\WcDatabaseConnectionStringKey\ucWcDatabaseConnectionStringKey.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "454C5F54B50599225B4E0E2FF83E0A3E"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using CollapsingGridSplitter;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;
using UIControls;


namespace UIControls {
    
    
    public partial class ucWcDatabaseConnectionStringKey : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid myGrid;
        
        internal CollapsingGridSplitter.ExtendedGridSplitter gdSplitter;
        
        internal UIControls.ucWcDatabaseConnectionStringKeyList ucNav;
        
        public System.Windows.Controls.TabControl tbcWcDatabaseConnectionStringKey;
        
        internal System.Windows.Controls.TabItem tbiWcDatabaseConnectionStringKeyProps;
        
        internal System.Windows.Controls.Grid gdProps;
        
        internal System.Windows.Controls.ScrollViewer svProps;
        
        internal System.Windows.Controls.Button btnNew;
        
        internal System.Windows.Controls.Button btnSave;
        
        internal System.Windows.Controls.Button btnUnDo;
        
        internal System.Windows.Controls.Button btnDelete;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/UIControls;component/Controls/Entity/WcDatabaseConnectionStringKey/ucWcDatabaseC" +
                        "onnectionStringKey.xaml", System.UriKind.Relative));
            this.myGrid = ((System.Windows.Controls.Grid)(this.FindName("myGrid")));
            this.gdSplitter = ((CollapsingGridSplitter.ExtendedGridSplitter)(this.FindName("gdSplitter")));
            this.ucNav = ((UIControls.ucWcDatabaseConnectionStringKeyList)(this.FindName("ucNav")));
            this.tbcWcDatabaseConnectionStringKey = ((System.Windows.Controls.TabControl)(this.FindName("tbcWcDatabaseConnectionStringKey")));
            this.tbiWcDatabaseConnectionStringKeyProps = ((System.Windows.Controls.TabItem)(this.FindName("tbiWcDatabaseConnectionStringKeyProps")));
            this.gdProps = ((System.Windows.Controls.Grid)(this.FindName("gdProps")));
            this.svProps = ((System.Windows.Controls.ScrollViewer)(this.FindName("svProps")));
            this.btnNew = ((System.Windows.Controls.Button)(this.FindName("btnNew")));
            this.btnSave = ((System.Windows.Controls.Button)(this.FindName("btnSave")));
            this.btnUnDo = ((System.Windows.Controls.Button)(this.FindName("btnUnDo")));
            this.btnDelete = ((System.Windows.Controls.Button)(this.FindName("btnDelete")));
        }
    }
}

