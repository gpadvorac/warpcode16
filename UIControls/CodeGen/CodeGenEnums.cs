﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace UIControls
{





    public enum ControlClass
    {
        TextBox = 1,
        ComboBox = 2,
        DataGrid = 3,
        CheckBox = 4,
        XamComboEditor = 5,
        XamDateTimeEditor = 6,
        XamDataGrid = 7,
        RadioButton = 8,
        DatePicker = 9,
        vRadioButtonGroup = 10,
        XamMultiColumnComboEditor = 11,
        ColorPicker = 12,
        vUnboundButtonColumn = 13,
        vItemCountButtonColumn = 14,
        TimePicker = 15,
        vContextMenuButtonColumn = 16
    }

    public enum ParentOrChild
    {
        None = 0,
        ParentControl = 1,
        ChildControl = 2
    }

    public enum SimpleDataType
    {
        String = 1,
        Number = 2,
        DateTime = 3,
        Object = 4,
        Guid = 5
    }

    public static class DotNetDataType
    {

        public const string Boolean = "Boolean";
        public const string Byte = "Byte";
        public const string Char = "Char";
        public const string DateTime = "DateTime";
        public const string Decimal = "Decimal";
        public const string Double = "Double";
        public const string Int16 = "Int16";
        public const string Int32 = "Int32";
        public const string Int64 = "Int64";
        public const string SByte = "SByte";
        public const string Single = "Single";
        public const string String = "String";
        public const string TimeSpan = "TimeSpan";
        public const string UInt16 = "UInt16";
        public const string UInt32 = "UInt32";
        public const string UInt64 = "UInt64";
        public const string Object = "Object";
        public const string ByteArray = "Byte[]";
        public const string CharArray = "Char[]";
        public const string StringArray = "String[]";
        public const string Guid = "Guid";
        public const string ObjectArray = "object[]";

    }


    public static class DotNetDataTypeId
    {
        public const int Boolean = 1;
        public const int Byte = 2;
        public const int Char = 3;
        public const int DateTime = 4;
        public const int Decimal = 5;
        public const int Double = 6;
        public const int Int16 = 7;
        public const int Int32 = 8;
        public const int Int64 = 9;
        public const int SByte = 10;
        public const int Single = 11;
        public const int String = 12;
        public const int UInt16 = 13;
        public const int UInt32 = 14;
        public const int UInt64 = 15;
        public const int Object = 16;
        public const int ByteArray = 17;
        public const int CharArray = 18;
        public const int StringArray = 19;
        public const int Guid = 20;
        public const int ObjectArray = 21;
        public const int TimeSpan = 22;
    }




    /// <summary>
    /// This is all SQL Data Types, but not all of these are mapped to .net data types in WC.
    /// and not all of these are listed in Warpcode's SQL Data Types table
    /// </summary>
    public class SqlTypesConst
    {
        public const string BigInt = "BigInt";
        public const string Binary = "Binary";
        public const string Bit = "Bit";
        public const string Char = "Char";
        public const string Date = "Date";
        public const string DateTime = "DateTime";
        public const string DateTime2 = "DateTime2";
        public const string DateTimeOffset = "DateTimeOffset";
        public const string Decimal = "Decimal";
        public const string Float = "Float";
        public const string Int = "Int";
        public const string Money = "Money";
        public const string NChar = "NChar";
        public const string NText = "NText";
        public const string NVarChar = "NVarChar";
        public const string Real = "Real";
        public const string SmallInt = "SmallInt";
        public const string SmallMoney = "SmallMoney";
        public const string Text = "Text";
        public const string Time = "Time";
        public const string Timestamp = "Timestamp";
        public const string TinyInt = "TinyInt";
        public const string UniqueIdentifier = "UniqueIdentifier";
        public const string VarBinary = "VarBinary";
        public const string VarChar = "VarChar";
        public const string Variant = "Variant";
        public const string Xml = "Xml";
    }

    /// <summary>
    /// This is all SQL Data Types, but not all of these are mapped to .net data types in WC.
    /// and not all of these are listed in Warpcode's SQL Data Types table
    /// </summary>
    public class SqlTypesVar
    {
        public static readonly string BigInt = "BigInt";
        public static readonly string Binary = "Binary";
        public static readonly string Bit = "Bit";
        public static readonly string Char = "Char";
        public static readonly string Date = "Date";
        public static readonly string DateTime = "DateTime";
        public static readonly string DateTime2 = "DateTime2";
        public static readonly string DateTimeOffset = "DateTimeOffset";
        public static readonly string Decimal = "Decimal";
        public static readonly string Float = "Float";
        public static readonly string Int = "Int";
        public static readonly string Money = "Money";
        public static readonly string NChar = "NChar";
        public static readonly string NText = "NText";
        public static readonly string NVarChar = "NVarChar";
        public static readonly string Real = "Real";
        public static readonly string SmallInt = "SmallInt";
        public static readonly string SmallMoney = "SmallMoney";
        public static readonly string Text = "Text";
        public static readonly string Time = "Time";
        public static readonly string Timestamp = "Timestamp";
        public static readonly string TinyInt = "TinyInt";
        public static readonly string UniqueIdentifier = "UniqueIdentifier";
        public static readonly string VarBinary = "VarBinary";
        public static readonly string VarChar = "VarChar";
        public static readonly string Variant = "Variant";
        public static readonly string Xml = "Xml";
    }


    public class DataReaderSchemaTableColumnIndex
    {
        public const int ColumnName = 0;
        public const int ColumnOrdinal = 1;
        public const int ColumnSize = 2;
        public const int NumericPrecision = 3;
        public const int NumericScale = 4;
        public const int IsUnique = 5;
        public const int IsKey = 6;
        public const int BaseServerName = 7;
        public const int BaseCatalogName = 8;
        public const int BaseColumnName = 9;
        public const int BaseSchemaName = 10;
        public const int BaseTableName = 11;
        public const int DataType = 12;
        public const int AllowDBNull = 13;
        public const int ProviderType = 14;
        public const int IsAliased = 15;
        public const int IsExpression = 16;
        public const int IsIdentity = 17;
        public const int IsAutoIncrement = 18;
        public const int IsRowVersion = 19;
        public const int IsHidden = 20;
        public const int IsLong = 21;
        public const int IsReadOnly = 22;
        public const int ProviderSpecificDataType = 23;
        public const int DataTypeName = 24;
        public const int XmlSchemaCollectionDatabase = 25;
        public const int XmlSchemaCollectionOwningSchema = 26;
        public const int XmlSchemaCollectionName = 27;
        public const int UdtAssemblyQualifiedName = 28;
        public const int NonVersionedProviderType = 29;
        public const int IsColumnSet = 30;
    }













}
