﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using EntityBll.SL;
using EntityWireTypeSL;
using Ifx.SL;
using TypeServices;

namespace UIControls
{
    public class GenerateSprocs_Put
    {


        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "GenerateSprocs_Put";

        private WcCodeGen_Tables_Binding _tbl = null;

        //bool _lst;
        //bool _all;
        //bool _row;
        bool _put;
        bool _ins;
        bool _upd;
        bool _del;

        private StringBuilder _code = new StringBuilder();

        string _sRt = "\r\n";
        string _strPKName = "";
        string _strPKDataType = "";
        private bool _isPkIdentity = false;
        string _sFKName = "";
        string _sFKDataType = "";
        string _sIsDeleted = "";
        string _schema = "";
        string _schemaBracket = "";
        string _sUserIdColumnName = "";
        string _sTimeStampName = "";
        string _sTb = "\t";
        string _sTb2;
        string _sTb3;
        string _sTb4;
        string _sTb5;


        enum ResultType
        {
            Row,
            Lst,
            All,
            Del
        }

        #endregion Initialize Variables



        public GenerateSprocs_Put()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - GenerateSprocs_Put", IfxTraceCategory.Enter);

                _sTb2 = _sTb + _sTb;
                _sTb3 = _sTb + _sTb + _sTb;
                _sTb4 = _sTb + _sTb + _sTb + _sTb;
                _sTb5 = _sTb + _sTb + _sTb + _sTb + _sTb;


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - GenerateSprocs_Put", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - GenerateSprocs_Put", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Fetch the metadata and begin the code gen
        /// </summary>
        /// <param name="tbl"></param>
        /// <param name="put"></param>
        /// <param name="ins"></param>
        /// <param name="upd"></param>
        /// <param name="del"></param>
        public string Generate(WcCodeGen_Tables_Binding tbl, bool put, bool ins, bool upd, bool del)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Generate", IfxTraceCategory.Enter);
                _tbl = tbl;
                _put = put;
                _ins = ins;
                _upd = upd;
                _del = del;

                foreach (WcCodeGen_TableColumn_Binding col in _tbl.TableColumns)
                {
                    if (col.TbC_IsPK)
                    {
                        _strPKName = col.TbC_Name;
                        _strPKDataType = col.TbC_DtSql_Id_TextField;
                        _isPkIdentity = col.TbC_IsIdentity;
                    }
                    if (col.TbC_IsFK)
                    {
                        _sFKName = col.TbC_Name;
                        _sFKDataType = col.TbC_DtSql_Id_TextField;
                    }
                    if (col.TbC_IsRowVersionStampColumn)
                    {
                        _sTimeStampName = col.TbC_Name;
                    }
                    if (col.TbC_IsUserIdColumn)
                    {
                        _sUserIdColumnName = col.TbC_Name;
                    }
                    if (col.TbC_IsDeletedColumn)
                    {
                        _sIsDeleted = col.TbC_Name;
                    }
                }

                _schema = _tbl.Tb_ApDbScm_Id_TextField;
                if (_schema.Length > 0)
                {
                    _schemaBracket = "[" + _schema + "].";
                    _schema = _schema + ".";
                }

                if (_put == true)
                {
                    Get_Put();
                }
                if (_ins == true)
                {
                    Get_PutInsert();
                }
                if (_upd == true)
                {
                    Get_PutUpdate();
                }
                if (_del == true)
                {
                    Get_IsDeleted();
                }



                return _code.ToString();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Generate", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Generate", IfxTraceCategory.Leave);
            }
        }
        
        void Get_Put()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_Put", IfxTraceCategory.Enter);
                _code.Append(SprocHelpers.GetHeader(_tbl, "put"));
                // BUILD THE PARAMTER LIST
                _code.Append(_sRt + "(" + _sRt);
                foreach (WcCodeGen_TableColumn_Binding dr in _tbl.TableColumns)
                {
                    if (SprocHelpers.IsSqlPut_InputParameter(_tbl, dr))
                    {
                        _code.Append(SprocHelpers.GetSqlStoredProcedureParameter(_tbl, dr) + _sRt);
                    }
                }
                //_code.Remove(_code.Length - 2, 2);
                // BUILD THE BODY
                _code.Append(@"@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY
");
                // For Sproc with GUID PKs
                if (_strPKDataType.ToLower() == "uniqueidentifier")
                {
                    _code.Append(@"
IF (@" + _strPKName + @" Is NULL)
	BEGIN
		SET @" + _strPKName + @" = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM " + _tbl.Tb_Name + " WHERE @" + _strPKName + " = @" + _strPKName + @")= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END
");
                }
                else
                {
                    _code.Append(@"
IF ((@" + _strPKName + " IS NULL) OR (@" + _strPKName + @" < 1))
    BEGIN
		SET @IsNew = 1;
	END
");
                }

                _code.Append(@"	
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute sp" + _tbl.Tb_EntityRootName + @"_putInsert'
		EXEC sp" + _tbl.Tb_EntityRootName + @"_putInsert
");


                foreach (WcCodeGen_TableColumn_Binding dr in _tbl.TableColumns)
                {
                    if (SprocHelpers.IsSqlPut_InputParameter(_tbl, dr))
                    {
                        _code.Append(_sTb2 + "@" + dr.TbC_Name.ToString() + "," + _sRt);
                    }
                }

                _code.Append(@"
		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @" + _sTimeStampName + " =( SELECT " + _sTimeStampName + " FROM " + _tbl.Tb_EntityRootName + " WHERE (" + _strPKName + " = @" + _strPKName + @"))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM " + _tbl.Tb_Name + " WHERE (" + _strPKName + " = @" + _strPKName + @") AND (" + _sTimeStampName + " = @" + _sTimeStampName + @")) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute sp" + _tbl.Tb_EntityRootName + @"_putUpdate'
				EXEC sp" + _tbl.Tb_EntityRootName + @"_putUpdate
");

                foreach (WcCodeGen_TableColumn_Binding dr in _tbl.TableColumns)
                {
                    if (SprocHelpers.IsSqlPut_InputParameter(_tbl, dr))
                    {
                        _code.Append(_sTb4 + "@" + dr.TbC_Name.ToString() + "," + _sRt);
                    }
                }

                _code.Append(@"
				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @" + _sTimeStampName + " =( SELECT " + _sTimeStampName + " FROM " + _tbl.Tb_Name + " WHERE (" + _strPKName + " = @" + _strPKName + @"))
			END
	END
");
                EndTryCatch_AndLogException(true);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_Put", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_Put", IfxTraceCategory.Leave);
            }
        }
        
        void Get_PutInsert()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_PutInsert", IfxTraceCategory.Enter);
                _code.Append(SprocHelpers.GetHeader(_tbl, "putInsert"));
                // BUILD THE PARAMTER LIST
                _code.Append(_sRt + "(" + _sRt);
                foreach (WcCodeGen_TableColumn_Binding dr in _tbl.TableColumns)
                {
                    if (SprocHelpers.IsSqlPut_InputParameter(_tbl, dr))
                    {
                        _code.Append(SprocHelpers.GetSqlStoredProcedureParameter(_tbl, dr) + _sRt);
                    }
                }
                //_code.Remove(_code.Length - 2, 2);
                // BUILD THE BODY
                _code.Append(@"@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN");


                _code.Append(@"
      SET @ErrMsg = 'Attempting to insert into " + _tbl.Tb_Name + @"'
      INSERT INTO " + _tbl.Tb_Name + @"
          (
");
                
                foreach (WcCodeGen_TableColumn_Binding dr in _tbl.TableColumns)
                {
                    if (SprocHelpers.IsSqlPut_WriteToDatabaseTable(_tbl, dr))
                    {
                        if (dr.TbC_IsInsertAllowed == true && dr.TbC_IsIdentity == false)
                        {
                            _code.Append(_sTb2 + dr.TbC_Name.ToString() + "," + _sRt);
                        }
                    }
                }

                //Remove the last " , " becuase the last param doesnt need it.  this also reoves the carrage return
                _code.Remove(_code.Length - 3, 3);
                _code.Append(_sRt + _sTb4 + ")");

                foreach (WcCodeGen_TableColumn_Binding dr in _tbl.TableColumns)
                {
                    if (SprocHelpers.IsSqlPut_WriteToDatabaseTable(_tbl, dr))
                    {
                        if (dr.TbC_IsInsertAllowed == true && dr.TbC_IsIdentity == false)
                        {
                            //_code.Append(_sTb2 + dr.TbC_Name.ToString() + "," + _sRt);
                            if (dr.TbC_IsCreatedDateColumn == true || dr.TbC_IsModifiedDateColumn == true)
                            {
                                _code.Append(_sTb2 + "GETDATE()");
                            }
                            else if (dr.TbC_IsCreatedDateColumn == true || dr.TbC_IsUserIdColumn == true)
                            {
                                _code.Append(_sTb2 + "@" + _sUserIdColumnName);
                            }
                            else if (dr.TbC_DtNt_Id_TextField.ToLower() == "string")
                            {
                                _code.Append(_sTb2 + "RTRIM(LTRIM(@" + dr.TbC_Name + "))");
                            }
                            else
                            {
                                _code.Append(_sTb2 + "@" + dr.TbC_Name);
                            }
                            _code.Append(", " + _sRt);
                        }
                    }
                }
                //Remove the last " , " becuase the last param doesnt need it.  this also reoves the carrage return
                _code.Remove(_code.Length - 4, 4);
                _code.Append(_sRt + _sRt + @")
                ");


                if (_isPkIdentity)
                {
                    _code.Append(@"
		SELECT  @" + _strPKName + @" = @@Identity");
                }

                _code.Append(@"
	END");

                EndTryCatch_AndLogException(false);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_PutInsert", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_PutInsert", IfxTraceCategory.Leave);
            }
        }
        
        void Get_PutUpdate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_PutUpdate", IfxTraceCategory.Enter);
                _code.Append(SprocHelpers.GetHeader(_tbl, "putUpdate"));
                // BUILD THE PARAMTER LIST
                _code.Append(_sRt + "(" + _sRt);
                foreach (WcCodeGen_TableColumn_Binding dr in _tbl.TableColumns)
                {
                    if (SprocHelpers.IsSqlPut_InputParameter(_tbl, dr))
                    {
                        _code.Append(SprocHelpers.GetSqlStoredProcedureParameter(_tbl, dr) + _sRt);
                    }
                }
                //_code.Remove(_code.Length - 2, 2);
                // BUILD THE BODY
                _code.Append(@"@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN");


                _code.Append(@"
      SET @ErrMsg = 'Attempting to update " + _tbl.Tb_Name + @"'
		UPDATE	" + _tbl.Tb_Name + @"
		SET
");

                foreach (WcCodeGen_TableColumn_Binding dr in _tbl.TableColumns)
                {
                    if (SprocHelpers.IsSqlPut_WriteToDatabaseTable(_tbl, dr))
                    {
                        if (dr.TbC_IsEditAllowed == true && dr.TbC_IsPK == false && dr.TbC_IsFK == false)
                        {
                            if (dr.TbC_DtNt_Id_TextField.ToLower() == "string")
                            {
                                _code.Append(_sTb2 + dr.TbC_Name + " = @" + dr.TbC_Name + "," + _sRt);
                            }
                            else
                            {
                                _code.Append(_sTb2 + dr.TbC_Name + " = @" + dr.TbC_Name + "," + _sRt);
                            }
                        }
                    }
                }

                //Remove the last " , " becuase the last param doesnt need it.  this also reoves the carrage return
                _code.Remove(_code.Length - 3, 3);
                _code.Append(_sRt + _sTb4 + ")");

                foreach (WcCodeGen_TableColumn_Binding dr in _tbl.TableColumns)
                {
                    if (SprocHelpers.IsSqlPut_WriteToDatabaseTable(_tbl, dr))
                    {
                        if (dr.TbC_IsInsertAllowed == true && dr.TbC_IsIdentity == false)
                        {
                            //_code.Append(_sTb2 + dr.TbC_Name.ToString() + "," + _sRt);
                            if (dr.TbC_IsCreatedDateColumn == true || dr.TbC_IsModifiedDateColumn == true)
                            {
                                _code.Append(_sTb2 + "GETDATE()");
                            }
                            else if (dr.TbC_IsCreatedDateColumn == true || dr.TbC_IsUserIdColumn == true)
                            {
                                _code.Append(_sTb2 + "@" + _sUserIdColumnName);
                            }
                            else if (dr.TbC_DtNt_Id_TextField.ToLower() == "string")
                            {
                                _code.Append(_sTb2 + "RTRIM(LTRIM(@" + dr.TbC_Name + "))");
                            }
                            else
                            {
                                _code.Append(_sTb2 + "@" + dr.TbC_Name);
                            }
                            _code.Append(", " + _sRt);
                        }
                    }
                }
                //Remove the last " , " becuase the last param doesnt need it.  this also reoves the carrage return
                _code.Remove(_code.Length - 4, 4);
                _code.Append(_sRt + _sRt + @")
                ");


                if (_isPkIdentity)
                {
                    _code.Append(@"
		SELECT  @" + _strPKName + @" = @@Identity");
                }

                _code.Append(@"
	END");

                EndTryCatch_AndLogException(false);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_PutUpdate", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_PutUpdate", IfxTraceCategory.Leave);
            }
        }
        
        void Get_IsDeleted()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_IsDeleted", IfxTraceCategory.Enter);
                _code.Append(SprocHelpers.GetHeader(_tbl, "putIsDeleted"));
                // BUILD THE PARAMTER LIST
                _code.Append(_sRt + "(" + _sRt);
                foreach (WcCodeGen_TableColumn_Binding dr in _tbl.TableColumns)
                {
                    if (SprocHelpers.IsSqlPut_InputParameter(_tbl, dr))
                    {
                        _code.Append(SprocHelpers.GetSqlStoredProcedureParameter(_tbl, dr) + _sRt);
                    }
                }
                //_code.Remove(_code.Length - 2, 2);
                // BUILD THE BODY
                _code.Append(@"@Id " + _strPKDataType + @",
@IsDeleted bit,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

BEGIN TRANSACTION;
BEGIN TRY
    BEGIN

	   UPDATE       " + _tbl.Tb_Name + @"
	   SET          " + _sIsDeleted + @" = @IsDeleted
	   WHERE        (" + _strPKName + @" = @Id)

	END
    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END

END TRY

BEGIN CATCH
    SET @Success = 0
    IF @TranCount = 0
    ROLLBACK TRANSACTION;

    SET @ErrorLogId = NewID()
    INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
    VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
    --  Log Stored Procedure Parameter Values
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Id', '" + _strPKDataType + @"', @Id, 1;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@IsDeleted', 'bit', @IsDeleted, 2;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 3;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 4;

END CATCH
	
RETURN



");


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_IsDeleted", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_IsDeleted", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// This is either a put or a (insert or update).
        /// A put has a little different syntax in the exception handling than inserts and updates.
        /// </summary>
        /// <param name="isPutOnly"></param>
        void EndTryCatch_AndLogException(bool isPutOnly)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EndTryCatch_AndLogException", IfxTraceCategory.Enter);


                if (isPutOnly)
                {
                    _code.Append(@"
END TRY

BEGIN CATCH
	SET @Success = 0
");

                }
                else
                {
                    _code.Append(@"

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
");

                }



                _code.Append(@"
END TRY
    -- Log Error Data
    SET @ErrorLogId = NewID()
    INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
    VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
    --  Log Stored Procedure Parameter Values
");
                // BUILD THE INPUT PARAMETER LOG FOR EXCEPTIONS
                int cnt = 1;
                foreach (WcCodeGen_TableColumn_Binding dr in _tbl.TableColumns)
                {
                    if (SprocHelpers.IsSqlPut_InputParameter(_tbl, dr))
                    {
                        _code.Append(_sTb + "EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@" + dr.TbC_Name.ToString() + "', '" + dr.TbC_DtSql_Id_TextField + "', @" + dr.TbC_Name.ToString() + ", " + cnt + ";" + _sRt);
                        cnt += 1;
                    }
                }
                _code.Append(_sTb + "EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, " + cnt.ToString() + ";" + _sRt);
                cnt += 1;
                _code.Append(_sTb + "EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, " + cnt.ToString() + ";" + _sRt);

                _code.Append(@"
END CATCH
	
RETURN
GO
");

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EndTryCatch_AndLogException", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EndTryCatch_AndLogException", IfxTraceCategory.Leave);
            }
        }


    }
}
