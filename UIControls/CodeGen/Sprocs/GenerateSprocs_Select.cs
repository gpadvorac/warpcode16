﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using EntityBll.SL;
using EntityWireTypeSL;
using Ifx.SL;

namespace UIControls
{
    public class GenerateSprocs_Select
    {


        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "GenerateSprocs_Select";

        private WcCodeGen_Tables_Binding _tbl = null;

        bool _lst;
        bool _all;
        bool _row;
        //bool _put;
        //bool _ins;
        //bool _upd;
        //bool _del;

        private StringBuilder _code = new StringBuilder();

        string _sRt = "\r\n";
        string _strPKName = "";
        string _strPKDataType = "";
        string _sFKName = "";
        string _sFKDataType = "";
        string _schema = "";
        string _schemaBracket = "";


        enum ResultType
        {
            Row,
            Lst,
            All
        }

        #endregion Initialize Variables

        
        public GenerateSprocs_Select()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - GenerateSprocs_Select", IfxTraceCategory.Enter);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - GenerateSprocs_Select", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - GenerateSprocs_Select", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Fetch the metadata and begin the code gen
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="lst"></param>
        /// <param name="lstAll"></param>
        /// <param name="row"></param>
        public string Generate(WcCodeGen_Tables_Binding tbl, bool lst, bool lstAll, bool row)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Generate", IfxTraceCategory.Enter);
                _tbl = tbl;
                _lst = lst;
                _all = lstAll;
                _row = row;
            
                foreach (WcCodeGen_TableColumn_Binding col in _tbl.TableColumns)
                {
                    if (col.TbC_IsPK)
                    {
                        _strPKName = col.TbC_Name;
                        _strPKDataType = col.TbC_DtSql_Id_TextField;
                    }
                    if (col.TbC_IsFK)
                    {
                        _sFKName = col.TbC_Name;
                        _sFKDataType = col.TbC_DtSql_Id_TextField;
                    }
                }

                _schema = _tbl.Tb_ApDbScm_Id_TextField;
                if (_schema !=null && _schema.Length > 0)
                {
                    _schemaBracket = "[" + _schema + "].";
                    _schema = _schema + ".";
                }

                if (_lst == true)
                {
                    Get_Lst();
                }
                if (_all == true)
                {
                    Get_All();
                }
                if (_row == true)
                {
                    Get_Row();
                }



                return _code.ToString();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Generate", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Generate", IfxTraceCategory.Leave);
            }
        }



        void Get_Lst()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_Lst", IfxTraceCategory.Enter);
                _code.Append(SprocHelpers.GetHeader(_tbl, "lst"));
                GetSelectClause(ResultType.Lst);
                GetFromClause();
                GetWhereClause(ResultType.Lst);
                GetOrderByCluase();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_Lst", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_Lst", IfxTraceCategory.Leave);
            }
        }

        void Get_All()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_All", IfxTraceCategory.Enter);
                _code.Append(SprocHelpers.GetHeader(_tbl, "All"));
                GetSelectClause(ResultType.All);
                GetFromClause();
                GetWhereClause(ResultType.All);
                GetOrderByCluase();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_All", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_All", IfxTraceCategory.Leave);
            }
        }

        void Get_Row()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_Row", IfxTraceCategory.Enter);
                _code.Append(SprocHelpers.GetHeader(_tbl, "row"));
                GetSelectClause(ResultType.Row);
                GetFromClause();
                GetWhereClause(ResultType.Row);
                GetOrderByCluase();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_Row", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_Row", IfxTraceCategory.Leave);
            }
        }


        //void Get_Del()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_Row", IfxTraceCategory.Enter);
        //        GetHeader("sp" + _tbl.Tb_EntityRootName + "_row");
        //        GetSelectClause(ResultType.Row);
        //        GetFromClause();
        //        GetWhereClause(ResultType.Row);
        //        GetOrderByCluase();
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_Row", ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_Row", IfxTraceCategory.Leave);
        //    }
        //}



//        void GetHeader(string spName)
//        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSelectClause", IfxTraceCategory.Enter);

//                _code.Append(@"
//--==========================================================================================================================================
//--==========================================================================================================================================
//-- " + spName + @"
//IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'" + _schemaBracket + "[" + spName + @"]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
//	BEGIN --1
//	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'" + _schemaBracket + "[" + spName + @"__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
//		 BEGIN --2
//			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'" + _schemaBracket + "[" + spName + @"__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
//				 BEGIN --3
//					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'" + _schemaBracket + "[" + spName + @"__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
//						 BEGIN --4
//							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'" + _schemaBracket + "[" + spName + @"__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
//								 BEGIN --5
//									 DROP PROCEDURE " + _schemaBracket + "[" + spName + @"__FLAG_DELETE_ME]
//									 EXEC sp_rename '" + _schema + spName + @"', '" + spName + @"__FLAG_DELETE_ME'
//								 END
//							 ELSE
//								 BEGIN
//									 EXEC sp_rename '" + _schema + spName + @"', '" + spName + @"__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
//								 END --5
//						 END
//					 ELSE
//						 BEGIN
//							 EXEC sp_rename '" + _schema + spName + @"', '" + spName + @"__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
//						 END --4
//				 END
//			 ELSE
//				 BEGIN
//					 EXEC sp_rename '" + _schema + spName + @"', '" + spName + @"__FLAG_DELETE_ME__FLAG_DELETE_ME'
//				 END --3
//		 END
//	 ELSE
//		 BEGIN
//			 EXEC sp_rename '" + _schema + spName + @"', '" + spName + @"__FLAG_DELETE_ME'
//		 END --2
//	 END
//GO
//CREATE PROCEDURE " + _schema + "[" + spName + "]" + @"
//-- Script for this SP was codegenned on: " + DateTime.Now);
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSelectClause", ex);
//                throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSelectClause", IfxTraceCategory.Leave);
//            }
//        }

        void GetSelectClause(ResultType type)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSelectClause", IfxTraceCategory.Enter);
                // Get Input Param
                string sParam = "";
                if (type == ResultType.Lst)
                {
                    if (_sFKName.Length > 0)
                    {
                        sParam = @"
(
@" + _sFKName + " " + _sFKDataType + @"
)";
                    }
                }
                else if (type == ResultType.Row)
                {
                    sParam = @"
(
@Id " + _strPKDataType + @"
)";
                }
                _code.Append(sParam + @"
AS
SET NOCOUNT ON

SELECT 	
");

                foreach (WcCodeGen_TableColumn_Binding dr in _tbl.TableColumns)
                {
                    if (dr.TbC_IsReadFromDb == true && dr.TbC_IsCodeGen==true)
                    {

                        if (dr.TbC_IsDisplayTextFieldProperty == false)
                        {
                            _code.Append(dr.TbC_Name + "," + _sRt);
                            if (dr.TbC_UseDisplayTextFieldProperty == true)
                            {
                                _code.Append(dr.TbC_ComboListDisplayColumn_Id_TextField + " AS " + dr.TbC_Name + "," + _sRt + _sRt);
                            }
                        }
                    }

                    if (dr.TbC_IsUserIdColumn == true && _tbl.Tb_UseLastModifiedByUserNameInSproc == true)
                    {
                        _code.Append("dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName," + _sRt + _sRt);
                    }

                }

                _code.Remove(_code.Length - 3, 1);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSelectClause", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSelectClause", IfxTraceCategory.Leave);
            }
        }

        void GetFromClause()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFromClause", IfxTraceCategory.Enter);
                StringBuilder from = new StringBuilder();
                from.Append(@"
FROM 		" + _tbl.Tb_Name);

                if (_tbl.Tb_UseUserTimeStamp == true)
                {
                    string userId = "";

                    foreach (WcCodeGen_TableColumn_Binding col in _tbl.TableColumns)
                    {
                        if (col.TbC_IsUserIdColumn == true)
                        {
                            userId = col.TbC_Name;
                            break;
                        }
                    }
                    

                    from.Append(@" LEFT OUTER JOIN
                tbPerson ON " + _tbl.Tb_Name + "." + userId + " = tbPerson.Pn_SecurityUserId");
                }

                foreach (WcCodeGen_TableColumn_Binding col in _tbl.TableColumns)
                {
                    if (col.TbC_ComboListTable_Id_TextField!=null && col.TbC_ComboListDisplayColumn_Id_TextField !=null)
                    {
                        string tableName = col.TbC_ComboListTable_Id_TextField;
                        //string pK = _strPKName;
                        from.Append(@" LEFT OUTER JOIN
                " + tableName + " ON " + _tbl.Tb_Name + "." + col.TbC_Name + " = " + tableName + "." + col.TbC_ComboListTable_Id_PkName);
                    }
                }

                _code.Append(from + _sRt);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFromClause", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFromClause", IfxTraceCategory.Leave);
            }
        }

        void GetWhereClause(ResultType type)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWhereClause", IfxTraceCategory.Enter);
                StringBuilder where = new StringBuilder();
                
                if (type == ResultType.Lst)
                {
                    // FK
                    foreach (WcCodeGen_TableColumn_Binding col in _tbl.TableColumns)
                    {
                        if (col.TbC_IsFK && col.TbC_IsCodeGen == true)
                        {
                            where.Append(_sRt + "WHERE   (" + _sFKName + " = @" + _sFKName + ")");
                            break;
                        }
                    }

                    // If it has IsDeleted, add it
                    foreach (WcCodeGen_TableColumn_Binding col in _tbl.TableColumns)
                    {
                        if (col.TbC_IsDeletedColumn && col.TbC_IsCodeGen == true)
                        {
                            where.Append(" AND (" + col.TbC_Name + " = 0)");
                            break;
                        }
                    }
                }
                else if (type == ResultType.All)
                {
                    // Do nothing
                }
                _code.Append(where);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWhereClause", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWhereClause", IfxTraceCategory.Leave);
            }
        }


        void GetOrderByCluase()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOrderByCluase", IfxTraceCategory.Enter);

                if (_tbl.TableSortBys.Count > 0)
                {
                    _code.Append(_sRt + "ORDER BY  ");
                    foreach (WcCodeGen_TableSortBy_Binding obj in _tbl.TableSortBys)
                    {
                        if (obj.TbSb_TbC_Id != null)
                        {
                            _code.Append(obj.TbSb_TbC_Id_TextField);
                            if (obj.TbSb_Direction != null && obj.TbSb_Direction.Length > 0)
                            {
                                _code.Append(" " + obj.TbSb_Direction);
                            }
                            _code.Append(", ");
                        }
                    }
                    _code.Remove(_code.Length - 2, 2);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOrderByCluase", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetOrderByCluase", IfxTraceCategory.Leave);
            }
        }




    }
}
