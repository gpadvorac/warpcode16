﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using EntityBll.SL;
using EntityWireTypeSL;
using Ifx.SL;

namespace UIControls
{
    public static class SprocHelpers
    {


        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "SprocHelpers";
        
        static string _sRt = "\r\n";


        enum ResultType
        {
            Row,
            Lst,
            All,
            Del
        }

        #endregion Initialize Variables
        
        public static string GetHeader(WcCodeGen_Tables_Binding tbl, string suffix)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSelectClause", IfxTraceCategory.Enter);

                StringBuilder code = new StringBuilder();
                string _schemaBracket="";
                string _schema="";
                string spName;
                _schema = tbl.Tb_ApDbScm_Id_TextField;
                if (_schema != null && _schema.Length > 0)
                {
                    _schemaBracket = "[" + _schema + "].";
                    _schema = _schema + ".";
                }

                spName = "sp" + tbl.Tb_EntityRootName + "_" + suffix;

                code.Append(@"
--==========================================================================================================================================
--==========================================================================================================================================
-- " + spName + @"
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'" + _schemaBracket + "[" + spName + @"]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'" + _schemaBracket + "[" + spName + @"__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'" + _schemaBracket + "[" + spName + @"__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'" + _schemaBracket + "[" + spName + @"__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'" + _schemaBracket + "[" + spName + @"__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE " + _schemaBracket + "[" + spName + @"__FLAG_DELETE_ME]
									 EXEC sp_rename '" + _schema + spName + @"', '" + spName + @"__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename '" + _schema + spName + @"', '" + spName + @"__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename '" + _schema + spName + @"', '" + spName + @"__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename '" + _schema + spName + @"', '" + spName + @"__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename '" + _schema + spName + @"', '" + spName + @"__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE " + _schema + "[" + spName + "]" + @"
-- Script for this SP was codegenned on: " + DateTime.Now);
                return code.ToString();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSelectClause", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSelectClause", IfxTraceCategory.Leave);
            }
        }


        public static string GetSqlStoredProcedureParameter(WcCodeGen_Tables_Binding tbl, WcCodeGen_TableColumn_Binding dr)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSqlStoredProcedureParameter", IfxTraceCategory.Enter);
                string param = null;
                if (dr.TbC_IsPK == true)
                {
                    param = "@" + dr.TbC_Name + " " + dr.TbC_DtSql_Id_TextField + " = NULL OUTPUT,";
                }
                else if (dr.TbC_IsRowVersionStampColumn == true)
                {
                    param = "@" + dr.TbC_Name + " timestamp = NULL OUTPUT,";
                }
                else if (dr.TbC_DtNt_Id_TextField.ToLower() == "string")
                {
                    string size = "";
                    size = "(" + dr.TbC_Length.ToString() + "),";
                    param = "@" + dr.TbC_Name + " " + dr.TbC_DtSql_Id_TextField + size + ",";
                }
                else if (dr.TbC_DtSql_Id_TextField.ToLower() == "decimal")
                {
                    string size = "";
                    size = "(" + dr.TbC_Precision.ToString() + "," + dr.TbC_Scale + "),";
                    param = "@" + dr.TbC_Name + " " + dr.TbC_DtSql_Id_TextField + size + ",";
                }
                else
                {
                    param = "@" + dr.TbC_Name + " " + dr.TbC_DtSql_Id_TextField + ",";
                }

                return param;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSqlStoredProcedureParameter", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSqlStoredProcedureParameter", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// Is a parameter that's passed into a put, insertPut or updatePut stored procedure
        /// </summary>
        /// <param name="tbl"></param>
        /// <param name="dr"></param>
        /// <returns></returns>
        public static bool IsSqlPut_InputParameter(WcCodeGen_Tables_Binding tbl, WcCodeGen_TableColumn_Binding dr)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsSqlPut_InputParameter", IfxTraceCategory.Enter);
                if (dr.TbC_IsCodeGen == true &&
                    dr.TbC_IsSendToDb == true &&
                    (dr.TbC_IsModifiedDateColumn == false && dr.TbC_IsCreatedDateColumn == false &&
                     dr.TbC_IsCreatedUserIdColumn == false && dr.TbC_IsCreatedUserIdColumn == false))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsSqlPut_InputParameter", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsSqlPut_InputParameter", IfxTraceCategory.Leave);
            }
        }




        /// <summary>
        /// Does we insert or update this to a database table?
        /// </summary>
        /// <param name="tbl"></param>
        /// <param name="dr"></param>
        /// <returns></returns>
        public static bool IsSqlPut_WriteToDatabaseTable(WcCodeGen_Tables_Binding tbl, WcCodeGen_TableColumn_Binding dr)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsSqlPut_InputParameter", IfxTraceCategory.Enter);
                if (dr.TbC_IsCodeGen == true &&
                    dr.TbC_IsSendToDb == true && dr.TbC_IsEntityColumn == true && (dr.TbC_IsInsertAllowed == true || dr.TbC_IsEditAllowed == true) &&
                    (dr.TbC_IsModifiedDateColumn == false && dr.TbC_IsCreatedDateColumn == false &&
                     dr.TbC_IsCreatedUserIdColumn == false && dr.TbC_IsCreatedUserIdColumn == false))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsSqlPut_InputParameter", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsSqlPut_InputParameter", IfxTraceCategory.Leave);
            }
        }




    }
}
