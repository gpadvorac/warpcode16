﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using EntityBll.SL;
using EntityWireTypeSL;
using Ifx.SL;
using ProxyWrapper;
using TypeServices;
using vUICommon;

namespace UIControls
{

    public class GenerateSprocsFromTableList
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "GenerateSprocsFromTableList";

        private WcApplicationVersion_Bll _apVrsn = null;
        List<WcCodeGen_Tables_Binding> _tables = new List<WcCodeGen_Tables_Binding>();


        private WcCodeGenService_ProxyWrapper _proxy = null;
        bool _lst;
        bool _lstAll;
        bool _row;
        bool _put;
        bool _ins;
        bool _upd;
        bool _del;

        private StringBuilder _code = new StringBuilder();

        string _sRt = "\r\n";
        string _strPKName = "";
        string _strPKDataType = "";
        string _sFKName = "";
        string _sFKDataType = "";
        string _schema = "";



        #endregion Initialize Variables


        public GenerateSprocsFromTableList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - GenerateSprocsFromTableList", IfxTraceCategory.Enter);

                _proxy = new WcCodeGenService_ProxyWrapper();
                _proxy.GetTable_TableColumnMetadataCompleted += _proxy_GetTable_TableColumnMetadataCompleted;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - GenerateSprocsFromTableList", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - GenerateSprocsFromTableList", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Fetch the metadata and begin the code gen
        /// </summary>
        /// <param name="apVrsnId"></param>
        /// <param name="lst"></param>
        /// <param name="lstAll"></param>
        /// <param name="row"></param>
        /// <param name="put"></param>
        /// <param name="ins"></param>
        /// <param name="upd"></param>
        /// <param name="del"></param>
        public void Generate(Guid apVrsnId, bool lst, bool lstAll, bool row, bool put, bool ins, bool upd, bool del)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Generate", IfxTraceCategory.Enter);
                _lst = lst;
                _lstAll = lstAll;
                _row = row;
                _put = put;
                _ins = ins;
                _upd = upd;
                _del = del;
                if (_lst == false && _lstAll == false && _row == false && _put == false && _ins == false &&
                    _upd == false && _del == false)
                {
                    MessageBox.Show("You must select Stored Procedrue scripting operation first.", "",
                        MessageBoxButton.OK);
                    return;
                }
                _proxy.Begin_GetTable_TableColumnMetadata(apVrsnId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Generate", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Generate", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Generate the stored procedures now.
        /// </summary>
        void Generate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Generate", IfxTraceCategory.Enter);

                _code.Clear();




                foreach (WcCodeGen_Tables_Binding tbl in _tables)
                {
                    _code.Append("-- " + tbl.Tb_Name + _sRt);

                }
                _code.Append(_sRt);
                _code.Append(_sRt);
                

                foreach (WcCodeGen_Tables_Binding tbl in _tables)
                {
                    if (tbl.DbCnSK_Database != null)
                    {
                        _code.Append("USE [" + tbl.DbCnSK_Database + "]" + _sRt + "GO" + _sRt);
                    }
                    else
                    {
                        _code.Append("USE [xxxxx]" + _sRt + "GO" + _sRt);
                    }
                    _code.Append("-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   " + _sRt);
                    _code.Append("-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   " + _sRt);
                    _code.Append("--     " + tbl.Tb_Name + _sRt);
                    
                    if (_lst || _lstAll || _row)
                    {
                        GenerateSprocs_Select obj = new GenerateSprocs_Select();
                        _code.Append(obj.Generate(tbl, _lst, _lstAll, _row));
                    }
                    if (_put || _ins || _upd || _del)
                    {
                        GenerateSprocs_Put obj = new GenerateSprocs_Put();
                        _code.Append(obj.Generate(tbl, _put, _ins, _upd, _del));
                    }
                }
                foreach (WcCodeGen_Tables_Binding tbl in _tables)
                {
                    _code.Append(GetConnectionReference(tbl));
                }
                CodeGenHelpers.WriteCodeFile(_apVrsn.ApVrsn_StoredProcCodeFolder, "MultipleSPs", _code.ToString(), ".sql", false);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Generate", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Generate", IfxTraceCategory.Leave);
            }
        }

        string GetConnectionReference(WcCodeGen_Tables_Binding tbl)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetConnectionReference", IfxTraceCategory.Enter);
                StringBuilder sb = new StringBuilder();
                string rem = "";
                string connectionKey = tbl.Tb_DbCnSK_Id_TextField;

                if (tbl.Tb_UseLegacyConnectionCode == true)
                {
                    rem = "--";
                }

          
                sb.Append(@"

" + rem + @"GO

" + rem + @"IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'" + tbl.Tb_EntityRootName + @"')))
" + rem + @"BEGIN
" + rem + @"	INSERT        TOP (100) PERCENT
" + rem + @"	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
" + rem + @"	VALUES        (NEWID(), N'" + tbl.Tb_EntityRootName + @"', N'" + connectionKey + @"', GETDATE(), GETDATE())
" + rem + @"END

" + rem + @"GO

");
               
                return sb.ToString();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetConnectionReference", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetConnectionReference", IfxTraceCategory.Leave);
            }
        }




        #region WS Callbacks






        private void _proxy_GetTable_TableColumnMetadataCompleted(object sender, GetTable_TableColumnMetadataCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableSortByService_WcTableSortBy_GetListByFKCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                _tables.Clear();
                object[] arApVrsn = null;
                object[] arTbl = null;
                object[] arSortBy = null;
                object[] arChdTbl = null;
                object[] arCol = null;
                object[] arTip = null;
                object[] arCboCol = null;
                
                if (array != null)
                {
                    arApVrsn = array[0] as object[];
                    arTbl = array[1] as object[];
                    arSortBy = array[2] as object[];
                    arChdTbl = array[3] as object[];
                    arCol = array[4] as object[];
                    arTip = array[5] as object[];
                    arCboCol = array[6] as object[];

                    if (arTbl == null || arCol == null)
                    {
                        MessageBox.Show("No tables and or columns were returned from WC.", "Metadata Error",
                            MessageBoxButton.OK);
                        return;
                    }
                    EntityState state = new EntityState(false,true,false);
                    _apVrsn = new WcApplicationVersion_Bll(new WcApplicationVersion_ValuesMngr((object[])arApVrsn[0], state), state);
                    //_tables.ReplaceList(arTbl);
                    for (int i = 0; i < arTbl.GetUpperBound(0) + 1; i++)
                    {
                        _tables.Add(new WcCodeGen_Tables_Binding((object[])arTbl[i]));
                    }

                    //Get the initial list of Child Tables to be filtered by each table 
                    List<WcCodeGen_TableSortBy_Binding> tblSortBys = new List<WcCodeGen_TableSortBy_Binding>();
                    if (arSortBy != null)
                    {
                        for (int i = 0; i < arSortBy.GetUpperBound(0) + 1; i++)
                        {
                            tblSortBys.Add(new WcCodeGen_TableSortBy_Binding((object[])arSortBy[i]));
                        }
                    }

                    //Get the initial list of Child Tables to be filtered by each table 
                    List<WcCodeGen_ChildTables_Binding> childTable = new List<WcCodeGen_ChildTables_Binding>();
                    if (arChdTbl != null)
                    {
                        for (int i = 0; i < arChdTbl.GetUpperBound(0) + 1; i++)
                        {
                            childTable.Add(new WcCodeGen_ChildTables_Binding((object[])arChdTbl[i]));
                        }
                    }
                    //Get the initial list of Table Columns to be filtered by each table 
                    List<WcCodeGen_TableColumn_Binding> tableColumns = new List<WcCodeGen_TableColumn_Binding>();
                    for (int i = 0; i < arCol.GetUpperBound(0) + 1; i++)
                    {
                        tableColumns.Add(new WcCodeGen_TableColumn_Binding((object[])arCol[i]));
                    }
                    

                    //Get the initial list of tooltips to be filtered by each table column as it's created
                    List<WcCodeGen_ToolTip_Binding> toolTips = new List<WcCodeGen_ToolTip_Binding>();
                    if (arTip != null)
                    {
                        for (int i = 0; i < arTip.GetUpperBound(0) + 1; i++)
                        {
                            toolTips.Add(new WcCodeGen_ToolTip_Binding((object[])arTip[i]));
                        }
                    }

                    //Get the initial list of combo columns to be filtered by each table column as it's created
                    List<WcCodeGen_ComboColumn_Binding> comboColumns = new List<WcCodeGen_ComboColumn_Binding>();
                    if (arCboCol != null)
                    {
                        for (int i = 0; i < arCboCol.GetUpperBound(0) + 1; i++)
                        {
                            comboColumns.Add(new WcCodeGen_ComboColumn_Binding((object[])arCboCol[i]));
                        }
                    }

                    foreach (WcCodeGen_Tables_Binding tbl in _tables)
                    {
                        tbl.LoadTableSortBysFromMasterList(tblSortBys);
                        tbl.LoadChildTablesFromMasterList(childTable);
                        tbl.LoadTableColumnsFromMasterList(tableColumns);
                        foreach (WcCodeGen_TableColumn_Binding col in tbl.TableColumns)
                        {
                            col.LoadToolTipsFromMasterList(toolTips);
                            col.LoadComboColumnsFromMasterList(comboColumns);
                        }
                    }
                    Generate();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableSortByService_WcTableSortBy_GetListByFKCompleted", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxyWcTableSortByService_WcTableSortBy_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WS Callbacks

    }


}
