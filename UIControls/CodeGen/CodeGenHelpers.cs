﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Ifx.SL;
using Infragistics.Controls.Editors.Primitives;
using Infragistics.Controls.Interactions;
using vDialogControl;
using vUICommon;
using WindowState = Infragistics.Controls.Interactions.WindowState;

namespace UIControls
{
    public class CodeGenHelpers
    {


        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "CodeGenHelpers";

        #endregion Initialize Variables



        public static void WriteCodeFile(string path, string file, string code, string fileExtention, bool overWrite)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WriteCodeFile", IfxTraceCategory.Enter);


                OpenScriptInWindow(code, file);


                //if (!Directory.Exists(path))
                //{
                //    // Create the directory it does not exist.
                //    Directory.CreateDirectory(path);
                //}
                //string sFile = path + @"\" + file + fileExtention;

                ////if (overWrite == false)
                ////{
                //if (File.Exists(sFile))
                //{

                //    if (!Directory.Exists(path + @"\tmp"))
                //    {
                //        // Create the directory it does not exist.
                //        Directory.CreateDirectory(path + @"\tmp");
                //    }

                //    string tmpFilePath = path + @"\tmp\" + file + fileExtention;
                //    string sTime = DateTime.Now.Year.ToString("0000") + "." + DateTime.Now.Month.ToString("00") + "." + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.ToString("HH") + "." + DateTime.Now.Minute.ToString("00") + "." + DateTime.Now.Second.ToString("00") + fileExtention;
                //    string sNewFileName = tmpFilePath + "_Arc_" + sTime;
                //    //sFile = path + @"\" + file + "_" + sTime + fileExtention;
                //    File.Move(sFile, sNewFileName);
                //}
                ////}

                //TextWriter tw = new StreamWriter(sFile);
                //tw.WriteLine(code);
                //tw.Close();
        }
        catch (Exception ex)
        {
            if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WriteCodeFile", ex);
            throw IfxWrapperException.GetError(ex);
        }
        finally
        {
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WriteCodeFile", IfxTraceCategory.Leave);
        }
    }


        static void OpenScriptInWindow(string script, string fileName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter)
                    IfxEvent.PublishTrace(traceId, _as, _cn, "OpenScriptInWindow", IfxTraceCategory.Enter);
                TextBox txb = new ByteTextBox();
                txb.VerticalScrollBarVisibility= ScrollBarVisibility.Visible;
                txb.Text = script;
                txb.TextWrapping = TextWrapping.NoWrap;
                vDialog dialog = new vDialog();
                dialog.StartupPosition = StartupPosition.Center;
                //dialog.MinimizeButtonVisibility = System.Windows.Visibility.Collapsed;
                //dialog.MaximizeButtonVisibility = System.Windows.Visibility.Collapsed;
                dialog.IsShowSizeInHeader = ApplicationTypeServices.ApplicationLevelVariables.IsShowSizeInHeader;
                dialog.MinHeight = 250;
                dialog.Height = 750;
                dialog.Width = 1100;
                //HeaderHeight = _headerHeight,
                //Padding = _thickness,
                //HeaderForeground = _headerForeground,
                dialog.IsModal = true;
                dialog.Content = txb;
                dialog.WindowStateChanged += Dialog_WindowStateChanged;
                dialog.Show();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OpenScriptInWindow", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave)
                    IfxEvent.PublishTrace(traceId, _as, _cn, "OpenScriptInWindow", IfxTraceCategory.Leave);
            }
        }

        static private void Dialog_WindowStateChanged(object sender, WindowStateChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Dialog_WindowStateChanged", IfxTraceCategory.Enter);

                if (e.NewWindowState == WindowState.Hidden)
                {
                    sender = null;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Dialog_WindowStateChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Dialog_WindowStateChanged", IfxTraceCategory.Leave);
            }
        }
    }
}
