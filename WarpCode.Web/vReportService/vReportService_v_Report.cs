using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  11/12/2016 4:09:23 PM

namespace VelocityService
{

    public partial class vReportService   //: IvReportService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "vReportService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] v_Report_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetById", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Report_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetAll", IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Report_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetListByFK", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_GetListByFK(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Report_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Save", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Report_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Delete", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] v_Report_SetIsDeleted(Guid id, bool isDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_SetIsDeleted", new ValuePair[] { new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return v_Report_DataServices.v_Report_SetIsDeleted(id, isDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Report_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Deactivate", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Report_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Remove", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods


        [OperationContract]
        public byte[] Getv_Report_ReadOnlyStaticLists(Guid UserId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_Report_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[2][];

                obj[0] = v_Report_DataServices.Getv_Report_ComboItemList();
                obj[1] = v_Report_DataServices.Getv_Report_lstRptExplorer_Restricted(UserId);

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_Report_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_Report_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] Executev_UiArtifactPermission_AssignRolesToReport(Guid ReportId, Guid ApplicationId, String Roles)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UiArtifactPermission_AssignRolesToReport", IfxTraceCategory.Enter);
                return v_Report_DataServices.Executev_UiArtifactPermission_AssignRolesToReport(ReportId, ApplicationId, Roles);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UiArtifactPermission_AssignRolesToReport", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UiArtifactPermission_AssignRolesToReport", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] Executev_UiArtifactPermission_RemoveRolesFromReport(Guid ReportId, String Roles)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UiArtifactPermission_RemoveRolesFromReport", IfxTraceCategory.Enter);
                return v_Report_DataServices.Executev_UiArtifactPermission_RemoveRolesFromReport(ReportId, Roles);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UiArtifactPermission_RemoveRolesFromReport", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UiArtifactPermission_RemoveRolesFromReport", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_UiArtifactPermission_RolesAssignedToReport(Guid ReportId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPermission_RolesAssignedToReport", IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.Getv_UiArtifactPermission_RolesAssignedToReport(ReportId);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPermission_RolesAssignedToReport", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPermission_RolesAssignedToReport", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_UiArtifactPermission_RolesNotAssignedToReport(Guid ApplicationId, Guid ReportId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPermission_RolesNotAssignedToReport", IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.Getv_UiArtifactPermission_RolesNotAssignedToReport(ApplicationId, ReportId);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPermission_RolesNotAssignedToReport", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPermission_RolesNotAssignedToReport", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] Executev_Report_updateRptExplorer(Guid NodeId, String NodeType, Guid? NewParentNodeId, String NewParentNodeType, Int32 Index, Guid? OldParentNodeId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_Report_updateRptExplorer", IfxTraceCategory.Enter);
                return v_Report_DataServices.Executev_Report_updateRptExplorer(NodeId, NodeType, NewParentNodeId, NewParentNodeType, Index, OldParentNodeId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_Report_updateRptExplorer", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_Report_updateRptExplorer", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_Report_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.Getv_Report_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_Report_lstAvailableForExplorer()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_lstAvailableForExplorer", IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.Getv_Report_lstAvailableForExplorer();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_lstAvailableForExplorer", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_lstAvailableForExplorer", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_Report_lstRptExplorer()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_lstRptExplorer", IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.Getv_Report_lstRptExplorer();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_lstRptExplorer", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_lstRptExplorer", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_Report_lstRptExplorer_Restricted(Guid UserId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_lstRptExplorer_Restricted", IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.Getv_Report_lstRptExplorer_Restricted(UserId);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_lstRptExplorer_Restricted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_lstRptExplorer_Restricted", IfxTraceCategory.Leave);
            }
        }





        [OperationContract]
        public byte[] Getv_Report_GetDataForReport(Guid ReportId, String Crit)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_GetDataForReport", IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.Getv_Report_GetDataForReport(ReportId, Crit);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_GetDataForReport", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_GetDataForReport", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_Report_GetSprocName(Guid ReportId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_GetSprocName", IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.Getv_Report_GetSprocName(ReportId);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_GetSprocName", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_GetSprocName", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


