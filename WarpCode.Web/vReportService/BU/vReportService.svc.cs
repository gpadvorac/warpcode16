﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Ifx;
using vDataServices;
using vReportParameter;
using System.Web;
using TypeServices;
using SLcompression;
namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class vReportService
    {

        #region Initialize Variables

        private static string _as = "Pipeline.Web";
        private static string _cn = "vReportService";

        #endregion Initialize Variables





        [OperationContract]
        public byte[] Getv_Report_Server(Guid? UserId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_Server", IfxTraceCategory.Enter);
                object[] obj = new object[1];
                string reportServer = System.Configuration.ConfigurationManager.AppSettings["WebServerUrl"];
                obj[0] = reportServer;
                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_Server", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_Server", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_Report_cmbByUserId(Guid? UserId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_cmbByUserId", IfxTraceCategory.Enter);
                object[][] obj = new object[2][];
                string reportServer = System.Configuration.ConfigurationManager.AppSettings["WebServerUrl"];
                //object[] rServer = new object[1] { reportServer };
                obj[0] = new object[1] { reportServer };
                obj[1] = v_ReportClient_DataServices.Getv_Report_cmbByUserId(UserId);
                //return data;

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_cmbByUserId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_cmbByUserId", IfxTraceCategory.Leave);
            }
        }



        [OperationContract]
        public byte[] Getv_Report_FilterSessionByUserId(Guid? UserId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_FilterSessionByUserId", IfxTraceCategory.Enter);
                object[][] obj = new object[2][];
                string reportServer = System.Configuration.ConfigurationManager.AppSettings["WebServerUrl"];
                //object[] rServer = new object[1] { reportServer };
                obj[0] = new object[1] { reportServer };
                obj[1] = v_ReportClient_DataServices.Getv_Report_FilterSessionByUserId(UserId);
                //return data;

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_FilterSessionByUserId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_FilterSessionByUserId", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public Guid? RunReport(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", IfxTraceCategory.Enter);
                vReport rpt = new vReport(data);
                //Guid key = Guid.NewGuid();
                // If the key wasnt passed in, create one.
                if (rpt.InstanceKey == null)
                {
                    rpt.InstanceKey = Guid.NewGuid();
                }
                HttpRuntime.Cache.Insert(rpt.InstanceKey.ToString().ToString(), rpt, null, DateTime.Now.AddMinutes(1), System.Web.Caching.Cache.NoSlidingExpiration);
                return rpt.InstanceKey;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", IfxTraceCategory.Leave);
            }
        }

        //[OperationContract]
        //public Guid? RunReportWithKey(Guid key, object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", IfxTraceCategory.Enter);
        //        vReport rpt = new vReport(data);
        //        //Guid key = Guid.NewGuid();
        //        HttpRuntime.Cache.Insert(key.ToString(), rpt, null, DateTime.Now.AddMinutes(1), System.Web.Caching.Cache.NoSlidingExpiration);
        //        return key;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunReport", IfxTraceCategory.Leave);
        //    }
        //}



        [OperationContract]
        public byte[] Getv_Report_ReportDataForGridOrExcel(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_ReportDataForGridOrExcel", IfxTraceCategory.Enter);
                vReport rpt = new vReport(data);
                // Build execution string
                string sql = "";

                foreach (var item in rpt.Parameters)
                {
                    if (sql.Length == 0)
                    {
                        sql = GetParamValue(item);
                    }
                    else
                    {
                        sql += ", " + GetParamValue(item);
                    }
                }

                //object[] obj = v_ReportClient_DataServices.Getv_Report_GetDataForReport((Guid)rpt.ReportId, sql);
                object[] obj = v_ReportClient_DataServices.Getv_Report_GetDataForReport(rpt);

                //byte[] array = Serialization.SilverlightSerializer.Serialize(obj);
                //return array;



                byte[] array = PayloadHelper.ObjectToCompressedBytes(obj);
                return array;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_ReportDataForGridOrExcel", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_ReportDataForGridOrExcel", IfxTraceCategory.Leave);
            }
        }


        string GetParamValue(vParameter param)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParamValue", IfxTraceCategory.Enter);
                switch (param.SimpleType)
                {
                    case SimpleDataType.Guid:
                        return "'" + param.Value + "'";
                    case SimpleDataType.Integer:
                        return param.Value;
                    case SimpleDataType.Double:
                        return param.Value;
                    case SimpleDataType.DateTime:
                        return "'" + param.Value + "'";
                    case SimpleDataType.String:
                        return "'" + param.Value + "'";
                    default:
                        throw new Exception(_as + "." + _cn + "GetParamValue:  param.SimpleType value was not part of the switch statement - " + param.SimpleType.ToString());
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParamValue", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParamValue", IfxTraceCategory.Leave);
            }
        }





        [OperationContract]
        public byte[] Getv_Report_DataForWireBindingObject(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_DataForWireBindingObject", IfxTraceCategory.Enter);
                vReport rpt = new vReport(data);

                object[] obj = v_ReportClient_DataServices.Getv_Report_GetDataForReport(rpt);

                byte[] array = Serialization.SilverlightSerializer.Serialize(obj);
                return array;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_DataForWireBindingObject", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_DataForWireBindingObject", IfxTraceCategory.Leave);
            }
        }



    }
}
