﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Ifx;
using vDataServices;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class vReportAdminService
    {


        //#region Initialize Variables

        //private static string _as = "VelocityService";
        //private static string _cn = "vReportAdminService";

        //#endregion Initialize Variables








        #region Add - Remove Roles from Reports and Roles assigned to Reports

        [OperationContract]
        public byte[] GetUiArtifactPermission_RolesNotAssignedToReport(Guid applicationId, Guid reportId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactPermission_RolesNotAssignedToReport", IfxTraceCategory.Enter);
                //return v_UiArtifact_DataServices.GetUiArtifactPermission_RolesNotAssignedToReport(applicationId, reportId);
                object[] list = v_UiArtifact_DataServices.GetUiArtifactPermission_RolesNotAssignedToReport(applicationId, reportId);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactPermission_RolesNotAssignedToReport", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactPermission_RolesNotAssignedToReport", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] GetUiArtifactPermission_RolesAssignedToReport(Guid reportId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactPermission_RolesAssignedToReport", IfxTraceCategory.Enter);
                //return v_UiArtifact_DataServices.GetUiArtifactPermission_RolesAssignedToReport(reportId);
                object[] list = v_UiArtifact_DataServices.GetUiArtifactPermission_RolesAssignedToReport(reportId);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactPermission_RolesAssignedToReport", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactPermission_RolesAssignedToReport", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public int UiArtifactPermission_AssignRolesToReport(Guid reportId, Guid applicationId, string roles)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UiArtifactPermission_AssignRolesToReport", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.UiArtifactPermission_AssignRolesToReport(reportId, applicationId, roles);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UiArtifactPermission_AssignRolesToReport", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UiArtifactPermission_AssignRolesToReport", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public int UiArtifactPermission_RemoveRolesFromReport(Guid reportId, string roles)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UiArtifactPermission_RemoveRolesFromReport", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.UiArtifactPermission_RemoveRolesFromReport(reportId, roles);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UiArtifactPermission_RemoveRolesFromReport", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UiArtifactPermission_RemoveRolesFromReport", IfxTraceCategory.Leave);
            }
        }


        #endregion Add - Remove Roles from Reports and Roles assigned to Reports










    }
}
