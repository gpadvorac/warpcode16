using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using vDataServices;
using Ifx;

// Gen Timestamp:  2/5/2013 5:52:47 PM

namespace VelocityService
{

    public partial class vReportAdminService   //: IvReportAdminService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "vReportAdminService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] v_Report_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Report_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetAll", IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Report_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_GetListByFK(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Report_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Report_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Delete", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Report_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Report_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.v_Report_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Report_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods



        [OperationContract]
        public object[]  Executev_UiArtifactPermission_AssignRolesToReport(Guid ReportId, Guid ApplicationId, String Roles )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UiArtifactPermission_AssignRolesToReport", IfxTraceCategory.Enter);
                return v_Report_DataServices.Executev_UiArtifactPermission_AssignRolesToReport(ReportId, ApplicationId, Roles );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UiArtifactPermission_AssignRolesToReport", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UiArtifactPermission_AssignRolesToReport", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[]  Executev_UiArtifactPermission_RemoveRolesFromReport(Guid ReportId, String Roles )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UiArtifactPermission_RemoveRolesFromReport", IfxTraceCategory.Enter);
                return v_Report_DataServices.Executev_UiArtifactPermission_RemoveRolesFromReport(ReportId, Roles );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UiArtifactPermission_RemoveRolesFromReport", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UiArtifactPermission_RemoveRolesFromReport", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_UiArtifactPermission_RolesAssignedToReport(Guid ReportId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPermission_RolesAssignedToReport", IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.Getv_UiArtifactPermission_RolesAssignedToReport(ReportId );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPermission_RolesAssignedToReport", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPermission_RolesAssignedToReport", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_UiArtifactPermission_RolesNotAssignedToReport(Guid ApplicationId, Guid ReportId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPermission_RolesNotAssignedToReport", IfxTraceCategory.Enter);
                object[] list = v_Report_DataServices.Getv_UiArtifactPermission_RolesNotAssignedToReport(ApplicationId, ReportId );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPermission_RolesNotAssignedToReport", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPermission_RolesNotAssignedToReport", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[]  Executev_Report_updateRptExplorer(Guid NodeId, String NodeType, Guid? NewParentNodeId, String NewParentNodeType, Int32 Index, Guid? OldParentNodeId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_Report_updateRptExplorer", IfxTraceCategory.Enter);
                return v_Report_DataServices.Executev_Report_updateRptExplorer(NodeId, NodeType, NewParentNodeId, NewParentNodeType, Index, OldParentNodeId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_Report_updateRptExplorer", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_Report_updateRptExplorer", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


