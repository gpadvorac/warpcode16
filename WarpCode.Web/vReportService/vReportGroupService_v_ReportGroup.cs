using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  11/12/2016 10:08:36 PM

namespace VelocityService
{

    public partial class vReportGroupService   //: IvReportGroupService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "vReportGroupService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] v_ReportGroup_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_GetById", IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.v_ReportGroup_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_ReportGroup_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_GetAll", IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.v_ReportGroup_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_ReportGroup_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_GetListByFK", IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.v_ReportGroup_GetListByFK();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_ReportGroup_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_Save", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.v_ReportGroup_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_ReportGroup_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_Delete", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.v_ReportGroup_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] v_ReportGroup_SetIsDeleted(Guid id, bool isDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_SetIsDeleted", new ValuePair[] { new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return v_ReportGroup_DataServices.v_ReportGroup_SetIsDeleted(id, isDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_ReportGroup_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_Deactivate", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.v_ReportGroup_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_ReportGroup_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_Remove", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.v_ReportGroup_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ReportGroup_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods


        [OperationContract]
        public byte[] Getv_ReportGroup_ReadOnlyStaticLists(Guid UserId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_ReportGroup_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[3][];

                obj[0] = v_ReportGroup_DataServices.Getv_ReportGroup_ComboItemList();
                obj[1] = v_ReportGroup_DataServices.Getv_ReportGroup_lstRptExplorer_Restricted(UserId);
                obj[2] = v_ReportGroup_DataServices.Getv_ReportGroupType_ComboItemList();

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_ReportGroup_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_ReportGroup_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_ReportGroup_lstRptExplorer()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstRptExplorer", IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.Getv_ReportGroup_lstRptExplorer();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstRptExplorer", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstRptExplorer", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_ReportGroup_lstRptExplorer_Restricted(Guid UserId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstRptExplorer_Restricted", IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.Getv_ReportGroup_lstRptExplorer_Restricted(UserId);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstRptExplorer_Restricted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstRptExplorer_Restricted", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_ReportGroup_lstActive()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstActive", IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.Getv_ReportGroup_lstActive();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstActive", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstActive", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_ReportGroupType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroupType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.Getv_ReportGroupType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroupType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroupType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_ReportGroup_lstRptExplorer_ByGroupId(Guid v_RptGrp_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstRptExplorer_ByGroupId", IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.Getv_ReportGroup_lstRptExplorer_ByGroupId(v_RptGrp_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstRptExplorer_ByGroupId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstRptExplorer_ByGroupId", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_ReportGroup_lstRptExplorer_Restricted_ByGroupId(Guid UserId, Guid v_RptGrp_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstRptExplorer_Restricted_ByGroupId", IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.Getv_ReportGroup_lstRptExplorer_Restricted_ByGroupId(UserId, v_RptGrp_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstRptExplorer_Restricted_ByGroupId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_lstRptExplorer_Restricted_ByGroupId", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_ReportGroup_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_ReportGroup_DataServices.Getv_ReportGroup_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ReportGroup_ComboItemList", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


