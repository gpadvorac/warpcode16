﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
//using vDataServices;
using Ifx;
using DataServices;
//using DataService;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class vFilterDataService
    {


        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "vFilterDataService";

        #endregion Initialize Variables




        [OperationContract]
        public byte[] GetvFilterSessionMD_AllSessionsAllLevels()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetvFilterSessionMD_AllSessionsAllLevels", IfxTraceCategory.Enter);
                object[][] obj = new object[6][];

                obj[0] = v_FilterSessionMD_DataServices.v_FilterSessionMD_GetAll();
                obj[1] = v_FilterSessionContextParameter_DataServices.v_FilterSessionContextParameter_GetAll();
                obj[2] = v_FilterControlMD_DataServices.v_FilterControlMD_GetAll();
                obj[3] = v_FilterControlFieldMD_DataServices.v_FilterControlFieldMD_GetAll();
                obj[4] = v_FilterControlFieldAttributeMD_DataServices.v_FilterControlFieldAttributeMD_GetAll();
                obj[5] = v_FilterControlFieldParameter_DataServices.v_FilterControlFieldParameterMD_GetAll();

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetvFilterSessionMD_AllSessionsAllLevels", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetvFilterSessionMD_AllSessionsAllLevels", IfxTraceCategory.Leave);
            }
        }





        [OperationContract]
        public byte[] GetFilterList(string listName, object[] parms)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterList", new ValuePair[] { new ValuePair("listName", listName), new ValuePair("parms", parms) }, IfxTraceCategory.Enter);
                Guid? prj_Id = null;
                object[] list = null;
                byte[] returnData = null;

                //switch (listName)
                //{
                //    case "Prospects":
                //        prj_Id = parms[0] as Guid?;

                //        list = CommonClientData_DataServices.GetProspect_ComboItemList((Guid)prj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "ProspectsLinkedToContracts":
                //        prj_Id = parms[0] as Guid?;

                //        list = Prospect_DataServices.GetProspect_linkedToContracts_ComboItemList((Guid)prj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "ProspectsLinkedToLeases":
                //        prj_Id = parms[0] as Guid?;

                //        list = Prospect_DataServices.GetProspect_linkedToLeases_ComboItemList((Guid)prj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "ProspectsLinkedToWells":
                //        prj_Id = parms[0] as Guid?;

                //        list = Prospect_DataServices.GetProspect_linkedToWells_ComboItemList((Guid)prj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "WellReserveType":
                //        list = Well_DataServices.GetWellReserveType_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "LeaseType":
                //        list = Lease_DataServices.GetLeaseType_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "LeaseRightsType":
                //        list = Lease_DataServices.GetLeaseRightsType_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "LeaseStatus":
                //        list = Lease_DataServices.GetLeaseStatus_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "States":
                //        list = CommonClientData_DataServices.GetState_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "WellProductType":
                //        list = Well_DataServices.GetWellProductType_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "WellAltCategory1":
                //        prj_Id = parms[0] as Guid?;
                //        if (prj_Id == null)
                //        {
                //            throw new Exception("vFilterDataService.GetFilterList:  Could not parse Project Id from param array for WellAltCategory1");
                //        }

                //        list = Well_DataServices.GetWellAltCategory1_ComboItemList((Guid)prj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "WellStatus":
                //        list = Well_DataServices.GetWellStatus_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                
                //    case "Blocks":
                //        prj_Id = parms[0] as Guid?;
                //        if (prj_Id == null)
                //        {
                //            throw new Exception("vFilterDataService.GetFilterList:  Could not parse Project Id from param array for Feching Blocks.");
                //        }

                //        list = CommonClientData_DataServices.GetBlock_ComboItemList((Guid)prj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "StateCounty":
                //        list = CommonClientData_DataServices.GetStateCounty_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "LeaseFlagType":
                //        prj_Id = parms[0] as Guid?;
                //        if (prj_Id == null)
                //        {
                //            throw new Exception("vFilterDataService.GetFilterList:  Could not parse Project Id from param array for the Lease Export Report");
                //        }

                //        list = LeaseFlag_DataServices.GetLeaseFlagType_ComboItemList((Guid)prj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "LeaseTractFlagType":
                //        prj_Id = parms[0] as Guid?;
                //        if (prj_Id == null)
                //        {
                //            throw new Exception("vFilterDataService.GetFilterList:  Could not parse Project Id from param array for the Lease Export Report");
                //        }

                //        list = LeaseTractFlag_DataServices.GetLeaseTractFlagType_ComboItemList((Guid)prj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "Township":
                //        list = CommonClientData_DataServices.GetTownshipDirection_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "Range":
                //        list = CommonClientData_DataServices.GetRangeDirection_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "DefectCategory":
                //        list = Defect_DataServices.GetDefectClass_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "ContractAltNumberSource":
                //        prj_Id = parms[0] as Guid?;
                //        if (prj_Id == null)
                //        {
                //            throw new Exception("vFilterDataService.GetFilterList:  Project Id was null in the param array for ContractAltNumberSource");
                //        }
                //        list = Contract_DataServices.GetContractAltNumberSource_ComboItemList((Guid)prj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "LeaseAltNumberSource":
                //        prj_Id = parms[0] as Guid?;
                //        if (prj_Id == null)
                //        {
                //            throw new Exception("vFilterDataService.GetFilterList:  Project Id was null in the param array for LeaseAltNumberSource");
                //        }
                //        list = Lease_DataServices.GetLeaseAltNumberSource_ComboItemList((Guid)prj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "WellAltNumberSource":
                //        prj_Id = parms[0] as Guid?;
                //        if (prj_Id == null)
                //        {
                //            throw new Exception("vFilterDataService.GetFilterList:  Project Id was null in the param array for WellAltNumberSource");
                //        }
                //        list = Well_DataServices.GetWellAltNumberSource_ComboItemList((Guid)prj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "Entity":
                //        prj_Id = parms[0] as Guid?;
                //        if (prj_Id == null)
                //        {
                //            throw new Exception("vFilterDataService.GetFilterList:  Could not parse Project Id from param array for Feching Entities.");
                //        }

                //        list = Entity_DataServices.GetEntity_ComboItemList((Guid)prj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "Contract_Alt1":
                //        prj_Id = parms[0] as Guid?;
                //        if (prj_Id == null)
                //        {
                //            throw new Exception("vFilterDataService.GetFilterList:  Could not parse Project Id from param array for Feching Entities.");
                //        }

                //        list = Contract_DataServices.GetContract_Alt1_ComboItemList((Guid)prj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "4xxxx":

                //        break;
                //    default:
                //        return null;
                //}





                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilterList", IfxTraceCategory.Leave);
            }
        }





        [OperationContract]
        public byte[] Get2ndFilterList(string listName, object[] parms)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get2ndFilterList", new ValuePair[] { new ValuePair("listName", listName), new ValuePair("parms", parms) }, IfxTraceCategory.Enter);
                //Guid? prj_Id = null;
                object[] list = null;
                byte[] returnData = null;

                //switch (listName)
                //{
                //    //case "ExampleForWhenWeHaveANullParam":
                        
                //    //    prj_Id = parms[0] as Guid?;
                //    //    if (prj_Id == null)
                //    //    {
                //    //        throw new Exception("vFilterDataService.GetFilterList:  Could not parse Project Id from param array for WellAltCategory1");
                //    //    }
                //    //    list = Well_DataServices.GetWellAltCategory1_ComboItemList((Guid)prj_Id);
                //    //    returnData = Serialization.SilverlightSerializer.Serialize(list);
                //    //    return returnData;
                //    case "CountiesByState":
                //        Guid? St_Id = parms[0] as Guid?;
                //        if (St_Id == null)
                //        {
                //            throw new Exception("vFilterDataService.GetFilterList:  Could not parse State Id from param array for Feching Counties by State.");
                //        }

                //        list = CommonClientData_DataServices.GetCounty_ByStateIdComboItemList((Guid)St_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "4xxxx":

                //        break;
                //    default:
                //        return null;
                //}

                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get2ndFilterList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get2ndFilterList", IfxTraceCategory.Leave);
            }
        }





        [OperationContract]
        public byte[] Getv_FilterSessionMD_cmbForReports()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterSessionMD_cmbForReports", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterSessionMD_cmbForReports();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterSessionMD_cmbForReports", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterSessionMD_cmbForReports", IfxTraceCategory.Leave);
            }
        }



    }
}
