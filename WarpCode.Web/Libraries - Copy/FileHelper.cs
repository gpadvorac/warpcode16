﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Ifx;

namespace VelocityService
{
    public class FileHelper
    {


        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "FileHelper";

        #endregion Initialize Variables




        /// <summary>
        /// 
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="fileName"></param>
        /// <param name="file"></param>
        /// <returns>1 = Success, 0 = Fail</returns>
        public static int WriteFileToDisk(string folder, string fileName, byte[] file)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WriteFileToDisk",
                    new ValuePair[] { new ValuePair("folder", folder), new ValuePair("fileName", fileName) }, IfxTraceCategory.Enter);

                string path="";

                // Check to see if the folder exists for this file
                if (!Directory.Exists(folder))
                {
                    // It doesnt exist, so create it
                    Directory.CreateDirectory(folder);
                }
                path = folder + @"\" + fileName;

                File.WriteAllBytes(path, file);

                return 1;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WriteFileToDisk", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WriteFileToDisk", IfxTraceCategory.Leave);
            }
        }



    }
}