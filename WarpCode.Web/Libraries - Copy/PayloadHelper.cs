﻿using System;
using System.Net;
using System.Collections.Generic;
namespace vSLcompression
{
    public static class PayloadHelper
    {
        public static byte[] ObjectToCompressedBytes(object obj)
        {
            byte[] retval = null;
            byte[] b = vSerialization.SilverlightSerializer.Serialize(obj);
            retval = vQuickLZSharp.QuickLZ.compress(b);
            return retval;
        }

        public static object CompressedBytesToObject(byte[] compressed)
        {
            object retval = null;
            byte[] b = vQuickLZSharp.QuickLZ.decompress(compressed);
            //retval = (List<TestObject>)Serialization.SilverlightSerializer.Deserialize(b);
            retval = (object)vSerialization.SilverlightSerializer.Deserialize(b);
            return retval;
        }
    }
}
