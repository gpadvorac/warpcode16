﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class vFilterSessionMDService
    {


        #region From the old vFilterAdminService


        #region Filter Admin


      
        [OperationContract]
        public Int32? Execute_FilterControlMD_insertControlAndChildData(Guid session_Id, int filter_Id, Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Execute_FilterControlMD_insertControlAndChildData", IfxTraceCategory.Enter);
                return v_FilterControlMD_DataServices.Execute_FilterControlMD_insertControlAndChildData(session_Id, filter_Id, userId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Execute_FilterControlMD_insertControlAndChildData", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Execute_FilterControlMD_insertControlAndChildData", IfxTraceCategory.Leave);
            }
        }

        //[OperationContract]
        //public byte[] v_FilterRuleMD_GetListByFK()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterRuleMD_GetListByFK", IfxTraceCategory.Enter);
        //        throw new Exception("Can't call v_FilterRuleMD_GetListByFK:  it requires 1 of 2 different FKs which will requires a specialized method and not this one.");
        //        //object[] list = v_FilterRuleMD_DataServices.v_FilterRuleMD_GetListByFK();
        //        //byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        //return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterRuleMD_GetListByFK", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterRuleMD_GetListByFK", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public Int32 v_FilterControlMD_Delete(object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlMD_Delete", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
        //        return v_FilterControlMD_DataServices.v_FilterControlMD_Delete(data);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlMD_Delete", ex);
        //        return 0;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlMD_Delete", IfxTraceCategory.Leave);
        //    }
        //}


        #endregion  Filter Admin



        #region Filter Import Export



        [OperationContract]
        public byte[] Getv_FilterTree_Session_Alternate(string string1, string string2, string string3, string string4)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Session", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_Session_Alternate(string1, string2, string3, string4);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Session", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Session", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_FilterTree_FilterControl_Alternate(Guid v_FCM_FSM_Id, string string1, string string2, string string3, string string4)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControl", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_FilterControl_Alternate(v_FCM_FSM_Id, string1, string2, string3, string4);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControl", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControl", IfxTraceCategory.Leave);
            }
        }



        [OperationContract]
        public byte[] Getv_FilterTree_Rule_ByControlId_Alternate(Guid v_FRM_FCM_Id, string string1, string string2, string string3, string string4)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Rule_ByControlId_Alternate", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_Rule_ByControlId_Alternate(v_FRM_FCM_Id, string1, string2, string3, string4);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Rule_ByControlId_Alternate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Rule_ByControlId_Alternate", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_FilterTree_Rule_ByFieldId_Alternate(Guid v_FRM_FCIFM_Id, string string1, string string2, string string3, string string4)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Rule_ByFieldId_Alternate", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_Rule_ByFieldId_Alternate(v_FRM_FCIFM_Id, string1, string2, string3, string4);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Rule_ByFieldId_Alternate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Rule_ByFieldId_Alternate", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_FilterTree_ControlFieldAttribute_Alternate(Guid v_FCIFMA_FCIFM_Id, string string1, string string2, string string3, string string4)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_ControlFieldAttribute_Alternate", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_ControlFieldAttribute_Alternate(v_FCIFMA_FCIFM_Id, string1, string2, string3, string4);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_ControlFieldAttribute_Alternate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_ControlFieldAttribute_Alternate", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_FilterTree_FilterControlField_Alternate(Guid v_FCIFM_FCM_Id, string string1, string string2, string string3, string string4)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControlField_Alternate", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_FilterControlField_Alternate(v_FCIFM_FCM_Id, string1, string2, string3, string4);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControlField_Alternate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControlField_Alternate", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_FilterTree_FilterControlFieldParameter_Alternate(Guid v_FCFPm_FCIFM_Id, string string1, string string2, string string3, string string4)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControlFieldParameter_Alternate", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_FilterControlFieldParameter_Alternate(v_FCFPm_FCIFM_Id, string1, string2, string3, string4);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControlFieldParameter_Alternate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControlFieldParameter_Alternate", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_FilterTree_SessionContextParameter_Alternate(Guid v_FSCP_FSM_Id, string string1, string string2, string string3, string string4)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_SessionContextParameter_Alternate", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_SessionContextParameter_Alternate(v_FSCP_FSM_Id, string1, string2, string3, string4);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_SessionContextParameter_Alternate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_SessionContextParameter_Alternate", IfxTraceCategory.Leave);
            }
        }







        [OperationContract]
        public object[] ExecuteFilterTree_ExportSession(Guid FSM_Id, string source1, string source2, string source3, string source4, string target1, string target2, string target3, string target4)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_SessionContextParameter_Alternate", IfxTraceCategory.Enter);
                return v_FilterSessionMD_DataServices.ExecuteFilterTree_ExportSession(FSM_Id, source1, source2, source3, source4, target1, target2, target3, target4);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_SessionContextParameter_Alternate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_SessionContextParameter_Alternate", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] ExecuteFilterTree_ExportFilter(Guid FCM_Id, string source1, string source2, string source3, string source4, string target1, string target2, string target3, string target4)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteFilterTree_ExportFilter", IfxTraceCategory.Enter);
                return v_FilterSessionMD_DataServices.ExecuteFilterTree_ExportFilter(FCM_Id, source1, source2, source3, source4, target1, target2, target3, target4);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteFilterTree_ExportFilter", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteFilterTree_ExportFilter", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] ExecuteFilterTree_DuplicateFilter(Guid TargetSessionId, Guid FCM_Id, string source1, string source2, string source3, string source4, string target1, string target2, string target3, string target4)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteFilterTree_DuplicateFilter", IfxTraceCategory.Enter);
                return v_FilterSessionMD_DataServices.ExecuteFilterTree_DuplicateFilter(TargetSessionId, FCM_Id, source1, source2, source3, source4, target1, target2, target3, target4);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteFilterTree_DuplicateFilter", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteFilterTree_DuplicateFilter", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] Executev_FilterSessionMD_delete2(Guid v_FSM_Id, string string1, string string2, string string3, string string4)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_FilterSessionMD_delete2", IfxTraceCategory.Enter);
                return v_FilterSessionMD_DataServices.Executev_FilterSessionMD_delete(v_FSM_Id, string1, string2, string3, string4);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_FilterSessionMD_delete2", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_FilterSessionMD_delete2", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] ExecuteFilterControlMD_Delete(Guid FCM_Id, string string1, string string2, string string3, string string4)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteFilterControlMD_Delete", IfxTraceCategory.Enter);
                return v_FilterControlMD_DataServices.ExecuteFilterControlMD_Delete(FCM_Id, string1, string2, string3, string4);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteFilterControlMD_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteFilterControlMD_Delete", IfxTraceCategory.Leave);
            }
        }






        #endregion Filter Import Export

        
        #endregion From the old vFilterAdminService




        //[OperationContract]
        //public byte[] Getv_SysDotNetDataType_ComboItemList()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysDotNetDataType_ComboItemList", IfxTraceCategory.Enter);
        //        object[] list = CommonClientData_DataServices.Getv_SysDotNetDataType_ComboItemList();
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysDotNetDataType_ComboItemList", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysDotNetDataType_ComboItemList", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[] Getv_SysSqlDataType_ComboItemList()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysSqlDataType_ComboItemList", IfxTraceCategory.Enter);
        //        object[] list = CommonClientData_DataServices.Getv_SysSqlDataType_ComboItemList();
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysSqlDataType_ComboItemList", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_SysSqlDataType_ComboItemList", IfxTraceCategory.Leave);
        //    }
        //}





    }
}
