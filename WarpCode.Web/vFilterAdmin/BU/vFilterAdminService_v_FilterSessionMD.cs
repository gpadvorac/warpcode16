using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  11/6/2013 11:32:18 PM

namespace VelocityService
{

    public partial class vFilterAdminService   //: IvFilterAdminService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "vFilterAdminService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] v_FilterSessionMD_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_GetById", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.v_FilterSessionMD_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterSessionMD_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_GetAll", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.v_FilterSessionMD_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterSessionMD_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_GetListByFK", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.v_FilterSessionMD_GetListByFK();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterSessionMD_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.v_FilterSessionMD_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterSessionMD_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.v_FilterSessionMD_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] v_FilterSessionMD_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_SetIsDeleted", new ValuePair[] {new ValuePair("IsDeleted", IsDeleted), new ValuePair("Id", Id) }, IfxTraceCategory.Enter);
                return v_FilterSessionMD_DataServices.v_FilterSessionMD_SetIsDeleted(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterSessionMD_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.v_FilterSessionMD_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterSessionMD_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.v_FilterSessionMD_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionMD_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods


        [OperationContract]
        public byte[] Getv_FilterSessionMD_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_FilterSessionMD_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[4][];

                obj[0] = v_FilterSessionMD_DataServices.Getv_FilterControlFieldParameterSource_ComboItemList();
                obj[1] = v_FilterSessionMD_DataServices.Getv_FilterDataType_ComboItemList();
                obj[2] = v_FilterSessionMD_DataServices.Getv_FilterTargetType_ComboItemList();
                obj[3] = v_FilterSessionMD_DataServices.Getv_FilterType_ComboItemList();

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_FilterSessionMD_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_FilterSessionMD_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_FilterTargetType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTargetType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTargetType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTargetType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTargetType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_FilterType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_FilterDataType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterDataType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterDataType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterDataType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterDataType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_FilterControlFieldParameterSource_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterControlFieldParameterSource_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterControlFieldParameterSource_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterControlFieldParameterSource_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterControlFieldParameterSource_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_FilterTree_FilterControl(Guid v_FCM_FSM_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControl", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_FilterControl(v_FCM_FSM_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControl", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControl", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_FilterTree_Reports(Guid v_Rpt_ApplicationId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Reports", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_Reports(v_Rpt_ApplicationId );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Reports", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Reports", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_FilterTree_Session()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Session", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_Session();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Session", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Session", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_FilterTree_Rule_ByControlId(Guid v_FRM_FCM_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Rule_ByControlId", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_Rule_ByControlId(v_FRM_FCM_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Rule_ByControlId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Rule_ByControlId", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_FilterTree_Rule_ByFieldId(Guid v_FRM_FCIFM_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Rule_ByFieldId", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_Rule_ByFieldId(v_FRM_FCIFM_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Rule_ByFieldId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_Rule_ByFieldId", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_FilterTree_ControlFieldAttribute(Guid v_FCIFMA_FCIFM_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_ControlFieldAttribute", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_ControlFieldAttribute(v_FCIFMA_FCIFM_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_ControlFieldAttribute", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_ControlFieldAttribute", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_FilterTree_FilterControlField(Guid v_FCIFM_FCM_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControlField", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_FilterControlField(v_FCIFM_FCM_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControlField", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControlField", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_FilterTree_FilterControlFieldParameter(Guid v_FCFPm_FCIFM_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControlFieldParameter", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_FilterControlFieldParameter(v_FCFPm_FCIFM_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControlFieldParameter", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_FilterControlFieldParameter", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_FilterTree_SessionContextParameter(Guid v_FSCP_FSM_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_SessionContextParameter", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterTree_SessionContextParameter(v_FSCP_FSM_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_SessionContextParameter", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterTree_SessionContextParameter", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[]  Executev_FilterSessionMD_delete(Guid v_FSM_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_FilterSessionMD_delete", IfxTraceCategory.Enter);
                return v_FilterSessionMD_DataServices.Executev_FilterSessionMD_delete(v_FSM_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_FilterSessionMD_delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_FilterSessionMD_delete", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


