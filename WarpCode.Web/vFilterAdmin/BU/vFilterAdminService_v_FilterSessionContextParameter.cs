using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  2/18/2012 5:14:26 PM

namespace VelocityService
{

    public partial class vFilterAdminService   //: IvFilterAdminService
    {

        #region Initialize Variables

        //private static string _as = "EntityService";
        //private static string _cn = "vFilterAdminService";

        #endregion Initialize Variables


        #region Base Methods

        [OperationContract]
        public byte[] v_FilterSessionContextParameter_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = v_FilterSessionContextParameter_DataServices.v_FilterSessionContextParameter_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterSessionContextParameter_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_GetAll", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionContextParameter_DataServices.v_FilterSessionContextParameter_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterSessionContextParameter_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = v_FilterSessionContextParameter_DataServices.v_FilterSessionContextParameter_GetListByFK(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterSessionContextParameter_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_FilterSessionContextParameter_DataServices.v_FilterSessionContextParameter_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterSessionContextParameter_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_FilterSessionContextParameter_DataServices.v_FilterSessionContextParameter_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_Delete", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterSessionContextParameter_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_FilterSessionContextParameter_DataServices.v_FilterSessionContextParameter_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterSessionContextParameter_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_FilterSessionContextParameter_DataServices.v_FilterSessionContextParameter_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterSessionContextParameter_Remove", IfxTraceCategory.Leave);
            }
        }

        #endregion Base Methods


        #region Other Methods



        #endregion Other Methods

    }
}


