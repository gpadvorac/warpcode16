using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  11/8/2016 5:06:34 PM

namespace VelocityService
{

    public partial class vFilterControlFieldParameterService   //: IvFilterControlFieldParameterService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "vFilterControlFieldParameterService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] v_FilterControlFieldParameter_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = v_FilterControlFieldParameter_DataServices.v_FilterControlFieldParameter_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterControlFieldParameter_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_GetAll", IfxTraceCategory.Enter);
                object[] list = v_FilterControlFieldParameter_DataServices.v_FilterControlFieldParameter_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterControlFieldParameter_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = v_FilterControlFieldParameter_DataServices.v_FilterControlFieldParameter_GetListByFK(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterControlFieldParameter_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_FilterControlFieldParameter_DataServices.v_FilterControlFieldParameter_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterControlFieldParameter_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_FilterControlFieldParameter_DataServices.v_FilterControlFieldParameter_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] v_FilterControlFieldParameter_SetIsDeleted(Guid id, bool isDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_SetIsDeleted", new ValuePair[] {new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return v_FilterControlFieldParameter_DataServices.v_FilterControlFieldParameter_SetIsDeleted(id, isDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterControlFieldParameter_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_FilterControlFieldParameter_DataServices.v_FilterControlFieldParameter_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_FilterControlFieldParameter_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_FilterControlFieldParameter_DataServices.v_FilterControlFieldParameter_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_FilterControlFieldParameter_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods






        #endregion Other Methods

    }
}


