﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pipeline.aspx.cs" Inherits="VelocityService.Pipeline" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Pipeline</title>
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="pragma" content="no-cache" />

    <style type="text/css">      
    html, body {
	    height: 100%;
	    overflow: auto;
    }
    body {
	    padding: 0;
	    margin: 0;
    }
    #silverlightControlHost {
	    height: 100%;
	    text-align:center;
    }
    </style>
    <script type="text/javascript" src="Silverlight.js"></script>
    <script type="text/javascript">
        function HideHelp() {
            document.onhelp = function () { return (false); }
            window.onhelp = function () { return (false); }
        }

        function onSilverlightError(sender, args) {
            var appSource = "";
            if (sender != null && sender != 0) {
                appSource = sender.getHost().Source;
            }

            var errorType = args.ErrorType;
            var iErrorCode = args.ErrorCode;

            if (errorType == "ImageError" || errorType == "MediaError") {
                return;
            }

            var errMsg = "Unhandled Error in Silverlight Application " + appSource + "\n";

            errMsg += "Code: " + iErrorCode + "    \n";
            errMsg += "Category: " + errorType + "       \n";
            errMsg += "Message: " + args.ErrorMessage + "     \n";

            if (errorType == "ParserError") {
                errMsg += "File: " + args.xamlFile + "     \n";
                errMsg += "Line: " + args.lineNumber + "     \n";
                errMsg += "Position: " + args.charPosition + "     \n";
            }
            else if (errorType == "RuntimeError") {
                if (args.lineNumber != 0) {
                    errMsg += "Line: " + args.lineNumber + "     \n";
                    errMsg += "Position: " + args.charPosition + "     \n";
                }
                errMsg += "MethodName: " + args.methodName + "     \n";
            }

            throw new Error(errMsg);
        }
    </script>
</head>
<body onload="HideHelp()">
    <form id="form1" runat="server" style="height:100%">
    <div id="silverlightControlHost">
        <object data="data:application/x-silverlight-2," type="application/x-silverlight-2" width="100%" height="100%">



        <%--      <%
                  string strSourceFile = @"ClientBin/Pipeline.xap";
           string param;    
           if (System.Diagnostics.Debugger.IsAttached)
               //Debugger Attached - Refresh the XAP file.
               param = "<param name=\"source\" value=\"" + strSourceFile + "?" + DateTime.Now.Ticks + "\" />";
           else
           {
               //Production Mode 
               //param = "<param name=\"source\" value=\"" + strSourceFile + "\" />";
               
               // just use the same method we did in the dev enviroment
               param = "<param name=\"source\" value=\"" + strSourceFile + "?" + DateTime.Now.Ticks + "\" />";
               
           }
           Response.Write(param);
        %> --%>

        <%
            string strSourceFile = @"ClientBin/WarpCode.xap";
            string param;
            if (System.Diagnostics.Debugger.IsAttached)
                param = "<param name=\"source\" value=\"" + strSourceFile + "?" + DateTime.Now.Ticks + "\" />";
            else
            {
                string xappath = HttpContext.Current.Server.MapPath(@"") + @"\" + strSourceFile;
                DateTime xapCreationDate = System.IO.File.GetLastWriteTime(xappath);
                param = "<param name=\"source\" value=\"" + strSourceFile + "?ignore="
                        + xapCreationDate.ToString() + "\" />";
            }
            Response.Write(param);
%> 


<%--		  <param name="source" value="ClientBin/Pipeline.xap"/> --%>		  
          <param name="onError" value="onSilverlightError" />
		  <param name="background" value="white" />
		  <param name="minRuntimeVersion" value="4.0.50401.0" />
		  <param name="autoUpgrade" value="true" />
		  <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=4.0.50401.0" style="text-decoration:none">
 			  <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight" style="border-style:none"/>
		  </a>
	    </object><iframe id="_sl_historyFrame" style="visibility:hidden;height:0px;width:0px;border:0px"></iframe></div>
    </form>
</body>
</html>
