﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using DataServices;
using Ifx;
using Serialization;
using vDataServices;

namespace VelocityService
{
    public partial class IfxService
    {


        #region ix_TraceSession






        [OperationContract]
        public byte[] ix_TraceSession_GetAll()
        {
            object[] session = ix_TraceSession_DataServices.ix_TraceSession_GetAll();
            byte[] data = SilverlightSerializer.Serialize(session);
            return data;
        }

        [OperationContract]
        public byte[] GetTraceSession_lstMin()
        {
            object[] session = ix_TraceSession_DataServices.GetTraceSession_lstMin();
            byte[] data = SilverlightSerializer.Serialize(session);
            return data;
        }





        [OperationContract]
        public byte[] GetTraceSessionDataSerialized(Guid id)
        {
            object[] list = new object[3];
            //list[0] = ix_TraceSession_DataServices.ix_TraceSession_GetById(id);
            list[0] = ix_Trace_DataServices.GetTrace_lstBy_SessionId_ReadOnlyGrid(id);
            list[1] = ix_TraceValuePair_DataServices.GetTraceValuePair_lstBy_SessionId(id);
            list[2] = ix_Excpetion_DataServices.GetExcpetion_lstBy_SessionId(id);
            byte[] data = SilverlightSerializer.Serialize(list);
            return data;
        }


        #endregion ix_TraceSession



        #region ix_Trace



        [OperationContract]
        public int ix_Trace_Save(object[] data)
        {
            string ip = System.Web.HttpContext.Current.Request.UserHostAddress;
            return IfxEventDataService.ix_Trace_Save(ip, (object[])data);
        }


        [OperationContract]
        public int ix_Trace_SaveList(object[] data)
        {
            string ip = System.Web.HttpContext.Current.Request.UserHostAddress;
            ix_TraceSession_DataServices.ix_TraceSession_Save((object[])data[0]);
            IfxEventDataService.ix_Trace_SaveList(ip, (object[])data[1]);
            return IfxEventDataService.ix_Excpetion_Save((object[])data[2]);
        }



        [OperationContract]
        public int ix_Trace_SaveSerialized(byte[] data)
        {
            string ip = System.Web.HttpContext.Current.Request.UserHostAddress;
            object[] newData = SilverlightSerializer.Deserialize(data) as object[];
            return IfxEventDataService.ix_Trace_Save(ip, (object[])newData);
        }

        [OperationContract]
        public int ix_Trace_SaveListSerialized(byte[] data)
        {
            string ip = System.Web.HttpContext.Current.Request.UserHostAddress;
            object[] newData = SilverlightSerializer.Deserialize(data) as object[];
            ix_TraceSession_DataServices.ix_TraceSession_Save((object[])newData[0]);
            IfxEventDataService.ix_Trace_SaveList(ip, (object[])newData[1]);
            return IfxEventDataService.ix_Excpetion_Save((object[])newData[2]);
        }





        #endregion ix_Trace




        #region ix_Excpetion



        [OperationContract]
        public int ix_Excpetion_Save(object[] data)
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "Excpetion_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
            return IfxEventDataService.ix_Excpetion_Save(data);
        }



        #endregion ix_Excpetion



        #region Utility Methods - Test Server Functionality


        // Test WS
        // Test reading the config
        // Test reading the config from Ifx
        // Test writing a file to disk from Ifx
        // Test writing an exception to xml on disk
        // Test db connection
        // Test writing a trace to the db

        [OperationContract]
        public string ServerUtility_TestWebService()
        {
            try
            {
                return "Web Service test is successful";
            }
            catch (Exception ex)
            {
                return "EntityService.IfxService.ServerUtility_TestWebService():  Returning text from the Catch statement on the server;   Message: " + ex.Message + ";   StackTrace: " + ex.StackTrace.ToString();
            }
        }

        [OperationContract]
        public string ServerUtility_ReadingFromTheConfig()
        {
            try
            {
                string sTest = "";
                sTest = System.Configuration.ConfigurationManager.AppSettings["ConfigTest"];
                return "From the web service call:  " + sTest;
            }
            catch (Exception ex)
            {
                return "EntityService.IfxService.ServerUtility_ReadingFromTheConfig():  Returning text from the Catch statement on the server;   Message: " + ex.Message + ";   StackTrace: " + ex.StackTrace.ToString();
            }
        }

        [OperationContract]
        public string ServerUtility_IfxReadingFromTheConfig()
        {
            try
            {
                return "From the Ifx assembly:  " + IfxEvent.ServerUtility_IfxReadingFromTheConfig();
            }
            catch (Exception ex)
            {
                return "EntityService.IfxService.ServerUtility_IfxReadingFromTheConfig():  Returning text from the Catch statement on the server;   Message: " + ex.Message + ";   StackTrace: " + ex.StackTrace.ToString();
            }
        }

        [OperationContract]
        public string ServerUtility_TestWritingXmlFileToDisk()
        {
            try
            {
                return IfxEvent.ServerUtility_TestWritingXmlFileToDisk();
            }
            catch (Exception ex)
            {
                return "EntityService.IfxService.ServerUtility_TestWritingXmlFileToDisk():  Returning text from the Catch statement on the server;   Message: " + ex.Message + ";   StackTrace: " + ex.StackTrace.ToString();
            }
        }

        [OperationContract]
        public string ServerUtility_Test_WriteIfxExceptionToXml()
        {
            try
            {
                return IfxEvent.ServerUtility_Test_WriteIfxExceptionToXml();
            }
            catch (Exception ex)
            {
                return "EntityService.IfxService.ServerUtility_TestWritingXmlFileToDisk():  Returning text from the Catch statement on the server;   Message: " + ex.Message + ";   StackTrace: " + ex.StackTrace.ToString();
            }
        }

        [OperationContract]
        public string ServerUtility_TestDatabaseConnection()
        {
            try
            {
                return IfxEvent.ServerUtility_TestDatabaseConnection();
            }
            catch (Exception ex)
            {
                return "EntityService.IfxService.ServerUtility_TestDatabaseConnection():  Returning text from the Catch statement on the server;   Message: " + ex.Message + ";   StackTrace: " + ex.StackTrace.ToString();
            }
        }

        [OperationContract]
        public string ServerUtility_Test_WriteIfxTraceToDatabase()
        {
            try
            {
                return IfxEvent.ServerUtility_Test_WriteIfxTraceToDatabase();
            }
            catch (Exception ex)
            {
                return "EntityService.IfxService.ServerUtility_Test_WriteIfxTraceToDatabase():  Returning text from the Catch statement on the server;   Message: " + ex.Message + ";   StackTrace: " + ex.StackTrace.ToString();
            }
        }




        [OperationContract]
        public string ServerUtility_ThrowExceptionOnServer()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ServerUtility_ThrowExceptionOnServer", IfxTraceCategory.Enter);

                TestBubbleException();
                return "EntityService.IfxService.ServerUtility_ThrowExceptionOnServer():  Exception was not thrown.";

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ServerUtility_ThrowExceptionOnServer", ex);
                return "EntityService.IfxService.ServerUtility_ThrowExceptionOnServer():  Exception has been thrown.  Check the DB and Log Files.";
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ServerUtility_ThrowExceptionOnServer", IfxTraceCategory.Leave);
            }
        }


        void TestBubbleException()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TestBubbleException", IfxTraceCategory.Enter);

                string msg =
                    "This is a text bubble exception being thrown on the server.  See if its recorded in both the DB and Log File.";
                throw new Exception(msg);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TestBubbleException", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TestBubbleException", IfxTraceCategory.Leave);
            }
        }


        #endregion Utility Methods - Test Server Functionality










    }
}