using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  9/23/2015 3:23:19 PM

namespace VelocityService
{

    public partial class IfxService   //: IIfxService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "IfxService";

        #endregion Initialize Variables



        #region Other Methods




        [OperationContract]
        public byte[] GetIxExceptionTraceReview_ValuePairs(DateTime? Start, DateTime? End, String User)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIxExceptionTraceReview_ValuePairs", IfxTraceCategory.Enter);
                object[] list = Ifx_DataServices.GetIxExceptionTraceReview_ValuePairs(Start, End, User);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIxExceptionTraceReview_ValuePairs", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIxExceptionTraceReview_ValuePairs", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getix_AuthenticationLog_lst()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getix_AuthenticationLog_lst", IfxTraceCategory.Enter);
                object[] list = Ifx_DataServices.Getix_AuthenticationLog_lst();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getix_AuthenticationLog_lst", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getix_AuthenticationLog_lst", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] Executeix_AuthenticationLog_putInsert(Guid? IxAuthLog_UserId, String IxAuthLog_UserName, String IxAuthLog_IP, String IxAuthLog_Platform, String IxAuthLog_Message, Boolean IxAuthLog_Success)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executeix_AuthenticationLog_putInsert", IfxTraceCategory.Enter);
                return Ifx_DataServices.Executeix_AuthenticationLog_putInsert(IxAuthLog_UserId, IxAuthLog_UserName, IxAuthLog_IP, IxAuthLog_Platform, IxAuthLog_Message, IxAuthLog_Success);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executeix_AuthenticationLog_putInsert", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executeix_AuthenticationLog_putInsert", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


