using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using vDataServices;
//using EntityWireType;
//using Ifx;

// Gen Timestamp:  1/3/2011 4:13:05 PM

namespace VelocityService
{

    public partial class IfxService
    {



        [OperationContract]
        public object[] ix_Trace_GetById(Guid id)
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Trace_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
            return ix_Trace_DataServices.ix_Trace_GetById(id);
        }

        [OperationContract]
        public object[] ix_Trace_GetAll()
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Trace_GetAll", IfxTraceCategory.Enter);
            return ix_Trace_DataServices.ix_Trace_GetAll();
        }

        [OperationContract]
        public object[] ix_Trace_GetListByFK(Guid id)
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Trace_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
            return ix_Trace_DataServices.ix_Trace_GetListByFK(id);
        }

        //[OperationContract]
        //public object[] ix_Trace_Save(object[] data, int check)
        //{
        //    //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Trace_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
        //    return ix_Trace_DataServices.ix_Trace_Save(data, check);
        //}

        [OperationContract]
        public object[] ix_Trace_Delete(object[] data)
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Trace_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
            return ix_Trace_DataServices.ix_Trace_Delete(data);
        }

        [OperationContract]
        public object[] ix_Trace_Deactivate(object[] data)
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Trace_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
            return ix_Trace_DataServices.ix_Trace_Deactivate(data);
        }

        [OperationContract]
        public object[] ix_Trace_Remove(object[] data)
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Trace_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
            return ix_Trace_DataServices.ix_Trace_Remove(data);
        }


        #region Other Methods


    


        #endregion Other Methods




    }
}


