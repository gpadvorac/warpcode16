using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using vDataServices;
//using EntityWireType;
//using Ifx;

// Gen Timestamp:  1/3/2011 4:13:05 PM

namespace VelocityService
{

    public partial class IfxService   //: IIfxService
    {



        [OperationContract]
        public object[] ix_Excpetion_GetById(Guid id)
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Excpetion_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
            return ix_Excpetion_DataServices.ix_Excpetion_GetById(id);
        }

        [OperationContract]
        public object[] ix_Excpetion_GetAll()
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Excpetion_GetAll", IfxTraceCategory.Enter);
            return ix_Excpetion_DataServices.ix_Excpetion_GetAll();
        }

        [OperationContract]
        public object[] ix_Excpetion_GetListByFK(Guid id)
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Excpetion_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
            return ix_Excpetion_DataServices.ix_Excpetion_GetListByFK(id);
        }

        //[OperationContract]
        //public int ix_Excpetion_Save(object[] data)
        //{
        //    //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Excpetion_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
        //    return ix_Excpetion_DataServices.ix_Excpetion_Save(data);
        //}

        [OperationContract]
        public object[] ix_Excpetion_Delete(object[] data)
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Excpetion_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
            return ix_Excpetion_DataServices.ix_Excpetion_Delete(data);
        }

        [OperationContract]
        public object[] ix_Excpetion_Deactivate(object[] data)
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Excpetion_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
            return ix_Excpetion_DataServices.ix_Excpetion_Deactivate(data);
        }

        [OperationContract]
        public object[] ix_Excpetion_Remove(object[] data)
        {
            //if (IfxTrace._traceEnter) IfxEvent.PublishTrace(_ns, _cn, "ix_Excpetion_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
            return ix_Excpetion_DataServices.ix_Excpetion_Remove(data);
        }


        #region Other Methods


    


        #endregion Other Methods




    }
}


