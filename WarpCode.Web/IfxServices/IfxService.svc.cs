﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class IfxService
    {







        [OperationContract]
        public byte[] GetIxExceptionTraceReview(DateTime? Start, DateTime? End, String User)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIxExceptionTraceReview", IfxTraceCategory.Enter);

                object[][] obj = new object[2][];
                obj[0] = Ifx_DataServices.GetIxExceptionTraceReview(Start, End, User);
                obj[1] = Ifx_DataServices.GetIxExceptionTraceReview_ValuePairs(Start, End, User);

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIxExceptionTraceReview", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIxExceptionTraceReview", IfxTraceCategory.Leave);
            }
        }



        [OperationContract]
        public object[] ExecuteIx_Excpetion_deleteList(string crit)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteIx_Excpetion_deleteList", IfxTraceCategory.Enter);

                return Ifx_DataServices.ExecuteIx_Excpetion_deleteList(crit);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteIx_Excpetion_deleteList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteIx_Excpetion_deleteList", IfxTraceCategory.Leave);
            }
        }








    }
}
