--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- user id=VEMaster_User;initial catalog=WarpCodeV16;data source=Voyager3;Connect Timeout=20

exec spWcStoredProc_put 
'4733d227-2684-44cf-9ac0-500820a6b02b'  	-- @Sp_Id
,'aa41875c-14a8-43d3-a796-ac7b6c166f22'  	-- @Sp_ApVrsn_Id
,'zspAddition_ComboItemList'  	-- @Sp_Name
,0  	-- @Sp_IsCodeGen
,0  	-- @Sp_IsReadyCodeGen
,0  	-- @Sp_IsCodeGenComplete
,0  	-- @Sp_IsTagForCodeGen
,0  	-- @Sp_IsTagForOther
,null  	-- @Sp_SprocGroups
,1  	-- @Sp_IsBuildDataAccessMethod
,0  	-- @Sp_BuildListClass
,0  	-- @Sp_IsFetchEntity
,1  	-- @Sp_IsStaticList
,1  	-- @Sp_IsTypeComboItem
,0  	-- @Sp_IsCreateWireType
,'Addition_ComboItemList'  	-- @Sp_WireTypeName
,'GetAddition_ComboItemList'  	-- @Sp_MethodName
,'43fff7a5-24f8-4639-9a17-f734224d54ad'  	-- @Sp_AssocEntity_Id
,1  	-- @Sp_SpRsTp_Id
,12  	-- @Sp_SpRtTp_Id
,0  	-- @Sp_IsInputParamsAsObjectArray
,1  	-- @Sp_IsParamsAutoRefresh
,0  	-- @Sp_HasNewParams
,1  	-- @Sp_IsParamsValid
,1  	-- @Sp_IsParamValueSetValid
,1  	-- @Sp_HasNewColumns
,0  	-- @Sp_IsColumnsValid
,1  	-- @Sp_IsValid
,''  	-- @Sp_Notes
,1  	-- @Sp_IsActiveRow
,'0ff1f126-8e1f-403a-9340-bcd99b74283f'  	-- @Sp_UserId
,null  	-- @Sp_Stamp
,0  	-- @BypassConcurrencyCheck
,null  	-- @Success
,null  	-- @IsConcurrencyGood
,null  	-- @ErrorLogId

--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
