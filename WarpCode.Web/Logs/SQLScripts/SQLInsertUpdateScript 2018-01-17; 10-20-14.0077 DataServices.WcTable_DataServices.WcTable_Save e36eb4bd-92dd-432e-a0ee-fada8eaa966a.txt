--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- user id=WarpCode_User;initial catalog=WarpCodeV16;data source=Voyager3;Connect Timeout=20

exec spWcTable_put 
'25014e6a-4ab0-4200-a6e1-60c33edfd712'  	-- @Tb_Id
,'aa41875c-14a8-43d3-a796-ac7b6c166f22'  	-- @Tb_ApVrsn_Id
,'tbContract'  	-- @Tb_Name
,'Contract'  	-- @Tb_EntityRootName
,''  	-- @Tb_VariableName
,''  	-- @Tb_ScreenCaption
,''  	-- @Tb_Description
,'xx'  	-- @Tb_DevelopmentNote
,1  	-- @Tb_UseLegacyConnectionCode
,'02601568-04eb-4030-a97c-828d83bd8d8b'  	-- @Tb_DbCnSK_Id
,null  	-- @Tb_ApDbScm_Id
,'UIControls'  	-- @Tb_UIAssemblyName
,'UIControls'  	-- @Tb_UINamespace
,'UIControls'  	-- @Tb_UIAssemblyPath
,null  	-- @Tb_ProxyAssemblyName
,null  	-- @Tb_ProxyNamespace
,null  	-- @Tb_ProxyAssemblyPath
,null  	-- @Tb_WireTypeAssemblyName
,null  	-- @Tb_WireTypeNamespace
,'WireTypes'  	-- @Tb_WireTypePath
,'ContractService'  	-- @Tb_WebServiceName
,'WebServices\Contract'  	-- @Tb_WebServiceFolder
,null  	-- @Tb_DataServiceAssemblyName
,null  	-- @Tb_DataServiceNamespace
,'DataServices'  	-- @Tb_DataServicePath
,1  	-- @Tb_UseTilesInPropsScreen
,1  	-- @Tb_UseGridColumnGroups
,1  	-- @Tb_UseGridDataSourceCombo
,0  	-- @Tb_PkIsIdentity
,0  	-- @Tb_IsVirtual
,0  	-- @Tb_IsScreenPlaceHolder
,0  	-- @Tb_IsNotEntity
,0  	-- @Tb_IsMany2Many
,0  	-- @Tb_UseLastModifiedByUserNameInSproc
,0  	-- @Tb_UseUserTimeStamp
,1  	-- @Tb_UseForAudit
,0  	-- @Tb_IsAllowDelete
,1  	-- @Tb_CnfgGdMnu_MenuRow_IsVisible
,1  	-- @Tb_CnfgGdMnu_GridTools_IsVisible
,1  	-- @Tb_CnfgGdMnu_SplitScreen_IsVisible
,1  	-- @Tb_CnfgGdMnu_SplitScreen_IsSplit_Default
,232  	-- @Tb_CnfgGdMnu_NavColumnWidth
,0  	-- @Tb_CnfgGdMnu_IsReadOnly
,1  	-- @Tb_CnfgGdMnu_IsAllowNewRow
,1  	-- @Tb_CnfgGdMnu_ExcelExport_IsVisible
,1  	-- @Tb_CnfgGdMnu_ColumnChooser_IsVisible
,1  	-- @Tb_CnfgGdMnu_ShowHideColBtn_IsVisible
,1  	-- @Tb_CnfgGdMnu_RefreshGrid_IsVisible
,1  	-- @Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible
,0  	-- @Tb_IsInputComplete
,0  	-- @Tb_IsCodeGen
,0  	-- @Tb_IsReadyCodeGen
,0  	-- @Tb_IsCodeGenComplete
,1  	-- @Tb_IsTagForCodeGen
,0  	-- @Tb_IsTagForOther
,1  	-- @Tb_IsActiveRow
,'0ff1f126-8e1f-403a-9340-bcd99b74283f'  	-- @Tb_UserId
,null  	-- @Tb_Stamp
,0  	-- @BypassConcurrencyCheck
,null  	-- @Success
,null  	-- @IsConcurrencyGood
,null  	-- @ErrorLogId

--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
