--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- user id=WarpCode_User;initial catalog=WarpCodeV16;data source=Voyager3;Connect Timeout=20

exec dbo.spWcStoredProcParamValueGroup_put 
'9382bc29-0d49-465d-b537-62f530218ed4'  	-- @SpPVGrp_Id
,'6c6dc515-d8c1-4e1f-8828-1b801b14c00c'  	-- @SpPVGrp_Sp_Id
,'Test2'  	-- @SpPVGrp_Name
,null  	-- @SpPVGrp_Notes
,1  	-- @SpPVGrp_IsActiveRow
,'0ff1f126-8e1f-403a-9340-bcd99b74283f'  	-- @SpPVGrp_UserId
,null  	-- @SpPVGrp_Stamp
,0  	-- @BypassConcurrencyCheck
,null  	-- @Success
,null  	-- @IsConcurrencyGood
,null  	-- @ErrorLogId

--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
