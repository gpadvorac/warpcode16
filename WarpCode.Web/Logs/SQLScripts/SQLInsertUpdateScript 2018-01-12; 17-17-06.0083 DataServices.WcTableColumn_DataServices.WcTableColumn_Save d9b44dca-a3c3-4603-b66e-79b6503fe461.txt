--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- user id=WarpCode_User;initial catalog=WarpCodeV16;data source=Voyager3;Connect Timeout=20

exec spWcTableColumn_put 
'522b81bc-98f9-401a-809e-21c2e149a36f'  	-- @TbC_Id
,'25014e6a-4ab0-4200-a6e1-60c33edfd712'  	-- @TbC_Tb_Id
,8  	-- @TbC_SortOrder
,'Ct_CtAltNbrSrc_Id'  	-- @TbC_Name
,5  	-- @TbC_CtlTp_Id
,0  	-- @TbC_IsNonvColumn
,''  	-- @TbC_Description
,23  	-- @TbC_DtSql_Id
,20  	-- @TbC_DtNt_Id
,16  	-- @TbC_Length
,0  	-- @TbC_Precision
,0  	-- @TbC_Scale
,0  	-- @TbC_IsPK
,0  	-- @TbC_IsIdentity
,0  	-- @TbC_IsFK
,1  	-- @TbC_IsEntityColumn
,0  	-- @TbC_IsSystemField
,1  	-- @TbC_IsValuesObjectMember
,1  	-- @TbC_IsInPropsScreen
,1  	-- @TbC_IsInNavList
,0  	-- @TbC_IsRequired
,''  	-- @TbC_BrokenRuleText
,0  	-- @TbC_AllowZero
,1  	-- @TbC_IsNullableInDb
,0  	-- @TbC_IsNullableInUI
,''  	-- @TbC_DefaultValue
,1  	-- @TbC_IsReadFromDb
,1  	-- @TbC_IsSendToDb
,1  	-- @TbC_IsInsertAllowed
,1  	-- @TbC_IsEditAllowed
,0  	-- @TbC_IsReadOnlyInUI
,1  	-- @TbC_UseForAudit
,'Section Summary Status'  	-- @TbC_DefaultCaption
,'Sec. Sum. Status'  	-- @TbC_ColumnHeaderText
,'Section Summary Status'  	-- @TbC_LabelCaptionVerbose
,1  	-- @TbC_LabelCaptionGenerate
,1  	-- @Tbc_ShowGridColumnToolTip
,1  	-- @Tbc_ShowPropsToolTip
,0  	-- @TbC_IsCreatePropsStrings
,0  	-- @TbC_IsCreateGridStrings
,0  	-- @TbC_IsAvailableForColumnGroups
,0  	-- @TbC_IsTextWrapInProp
,0  	-- @TbC_IsTextWrapInGrid
,''  	-- @TbC_TextBoxFormat
,''  	-- @TbC_TextColumnFormat
,-1  	-- @TbC_ColumnWidth
,''  	-- @TbC_TextBoxTextAlignment
,''  	-- @TbC_ColumnTextAlignment
,'f837a67d-1023-4f1e-9413-4481c6c7ba96'  	-- @TbC_ListStoredProc_Id
,1  	-- @TbC_IsStaticList
,0  	-- @TbC_UseNotInList
,0  	-- @TbC_UseListEditBtn
,null  	-- @TbC_ParentColumnKey
,1  	-- @TbC_UseDisplayTextFieldProperty
,0  	-- @TbC_IsDisplayTextFieldProperty
,'93396322-72e9-4919-bec0-a7d981f5fd41'  	-- @TbC_ComboListTable_Id
,'a1ba3978-0074-45ad-831d-678b8ee70bd9'  	-- @TbC_ComboListDisplayColumn_Id
,0  	-- @TbC_Combo_MaxDropdownHeight
,0  	-- @TbC_Combo_MaxDropdownWidth
,0  	-- @TbC_Combo_AllowDropdownResizing
,0  	-- @TbC_Combo_IsResetButtonVisible
,0  	-- @TbC_IsActiveRecColumn
,0  	-- @TbC_IsDeletedColumn
,0  	-- @TbC_IsCreatedUserIdColumn
,0  	-- @TbC_IsCreatedDateColumn
,0  	-- @TbC_IsUserIdColumn
,0  	-- @TbC_IsModifiedDateColumn
,0  	-- @TbC_IsRowVersionStampColumn
,1  	-- @TbC_IsBrowsable
,''  	-- @TbC_DeveloperNote
,''  	-- @TbC_UserNote
,''  	-- @TbC_HelpFileAdditionalNote
,''  	-- @TbC_Notes
,0  	-- @TbC_IsInputComplete
,1  	-- @TbC_IsCodeGen
,0  	-- @TbC_IsReadyCodeGen
,0  	-- @TbC_IsCodeGenComplete
,0  	-- @TbC_IsTagForCodeGen
,0  	-- @TbC_IsTagForOther
,1  	-- @TbC_IsActiveRow
,'0ff1f126-8e1f-403a-9340-bcd99b74283f'  	-- @TbC_UserId
,null  	-- @TbC_Stamp
,0  	-- @BypassConcurrencyCheck
,null  	-- @Success
,null  	-- @IsConcurrencyGood
,null  	-- @ErrorLogId

--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
