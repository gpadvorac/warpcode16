--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- user id=WarpCode_User;initial catalog=WarpCodeV16;data source=Voyager3;Connect Timeout=20

exec spWcTableColumn_put 
'57a89f05-703b-496c-a228-0e71c1a727f8'  	-- @TbC_Id
,'694a50b0-e360-4250-87f8-e0345e173b1e'  	-- @TbC_Tb_Id
,15  	-- @TbC_SortOrder
,'AstSys_AstSysOpSt_Id'  	-- @TbC_Name
,5  	-- @TbC_CtlTp_Id
,0  	-- @TbC_IsNonvColumn
,null  	-- @TbC_Description
,9  	-- @TbC_DtSql_Id
,8  	-- @TbC_DtNt_Id
,4  	-- @TbC_Length
,10  	-- @TbC_Precision
,0  	-- @TbC_Scale
,0  	-- @TbC_IsPK
,0  	-- @TbC_IsIdentity
,0  	-- @TbC_IsFK
,1  	-- @TbC_IsEntityColumn
,1  	-- @TbC_IsSystemField
,1  	-- @TbC_IsValuesObjectMember
,1  	-- @TbC_IsInPropsScreen
,1  	-- @TbC_IsInNavList
,0  	-- @TbC_IsRequired
,null  	-- @TbC_BrokenRuleText
,0  	-- @TbC_AllowZero
,1  	-- @TbC_IsNullableInDb
,0  	-- @TbC_IsNullableInUI
,null  	-- @TbC_DefaultValue
,1  	-- @TbC_IsReadFromDb
,1  	-- @TbC_IsSendToDb
,1  	-- @TbC_IsInsertAllowed
,1  	-- @TbC_IsEditAllowed
,0  	-- @TbC_IsReadOnlyInUI
,0  	-- @TbC_UseForAudit
,'System Operating Status'  	-- @TbC_DefaultCaption
,'System Operating Status'  	-- @TbC_ColumnHeaderText
,'System Operating Status'  	-- @TbC_LabelCaptionVerbose
,1  	-- @TbC_LabelCaptionGenerate
,0  	-- @Tbc_ShowGridColumnToolTip
,0  	-- @Tbc_ShowPropsToolTip
,0  	-- @TbC_IsCreatePropsStrings
,0  	-- @TbC_IsCreateGridStrings
,1  	-- @TbC_IsAvailableForColumnGroups
,0  	-- @TbC_IsTextWrapInProp
,0  	-- @TbC_IsTextWrapInGrid
,null  	-- @TbC_TextBoxFormat
,null  	-- @TbC_TextColumnFormat
,-1  	-- @TbC_ColumnWidth
,null  	-- @TbC_TextBoxTextAlignment
,null  	-- @TbC_ColumnTextAlignment
,'add0f53c-83aa-43c6-b2ee-0c872c6e2547'  	-- @TbC_ListStoredProc_Id
,0  	-- @TbC_IsStaticList
,0  	-- @TbC_UseNotInList
,0  	-- @TbC_UseListEditBtn
,null  	-- @TbC_ParentColumnKey
,0  	-- @TbC_UseDisplayTextFieldProperty
,0  	-- @TbC_IsDisplayTextFieldProperty
,'1c3f3cfa-17dd-4232-9370-cb0ce9376b19'  	-- @TbC_ComboListTable_Id
,'c0c3bec5-5c18-43cc-ba0e-2285b32d20c6'  	-- @TbC_ComboListDisplayColumn_Id
,0  	-- @TbC_Combo_MaxDropdownHeight
,0  	-- @TbC_Combo_MaxDropdownWidth
,0  	-- @TbC_Combo_AllowDropdownResizing
,0  	-- @TbC_Combo_IsResetButtonVisible
,0  	-- @TbC_IsActiveRecColumn
,0  	-- @TbC_IsDeletedColumn
,0  	-- @TbC_IsCreatedUserIdColumn
,0  	-- @TbC_IsCreatedDateColumn
,0  	-- @TbC_IsUserIdColumn
,0  	-- @TbC_IsModifiedDateColumn
,0  	-- @TbC_IsRowVersionStampColumn
,1  	-- @TbC_IsBrowsable
,null  	-- @TbC_DeveloperNote
,null  	-- @TbC_UserNote
,null  	-- @TbC_HelpFileAdditionalNote
,null  	-- @TbC_Notes
,0  	-- @TbC_IsInputComplete
,1  	-- @TbC_IsCodeGen
,0  	-- @TbC_IsReadyCodeGen
,0  	-- @TbC_IsCodeGenComplete
,0  	-- @TbC_IsTagForCodeGen
,0  	-- @TbC_IsTagForOther
,1  	-- @TbC_IsActiveRow
,'0ff1f126-8e1f-403a-9340-bcd99b74283f'  	-- @TbC_UserId
,null  	-- @TbC_Stamp
,0  	-- @BypassConcurrencyCheck
,null  	-- @Success
,null  	-- @IsConcurrencyGood
,null  	-- @ErrorLogId

--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
