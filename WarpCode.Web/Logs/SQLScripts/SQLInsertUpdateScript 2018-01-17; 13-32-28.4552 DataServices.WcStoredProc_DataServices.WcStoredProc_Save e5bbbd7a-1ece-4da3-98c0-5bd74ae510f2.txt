--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- user id=WarpCode_User;initial catalog=WarpCodeV16;data source=Voyager3;Connect Timeout=20

exec spWcStoredProc_put 
'dc47a63e-14be-43ab-b7bf-c0320863e1ca'  	-- @Sp_Id
,'aa41875c-14a8-43d3-a796-ac7b6c166f22'  	-- @Sp_ApVrsn_Id
,'spAssetSystemProductType_ComboItemList'  	-- @Sp_Name
,0  	-- @Sp_IsCodeGen
,0  	-- @Sp_IsReadyCodeGen
,0  	-- @Sp_IsCodeGenComplete
,0  	-- @Sp_IsTagForCodeGen
,0  	-- @Sp_IsTagForOther
,null  	-- @Sp_SprocGroups
,1  	-- @Sp_IsBuildDataAccessMethod
,0  	-- @Sp_BuildListClass
,0  	-- @Sp_IsFetchEntity
,1  	-- @Sp_IsStaticList
,1  	-- @Sp_IsTypeComboItem
,0  	-- @Sp_IsCreateWireType
,null  	-- @Sp_WireTypeName
,'GetAssetSystemProductType_ComboItemList'  	-- @Sp_MethodName
,'b9225708-f6a8-45c4-b5ce-4da77f5b8ba5'  	-- @Sp_AssocEntity_Id
,1  	-- @Sp_UseLegacyConnectionCode
,'02601568-04eb-4030-a97c-828d83bd8d8b'  	-- @Sp_DbCnSK_Id
,null  	-- @Sp_ApDbScm_Id
,1  	-- @Sp_SpRsTp_Id
,12  	-- @Sp_SpRtTp_Id
,0  	-- @Sp_IsInputParamsAsObjectArray
,1  	-- @Sp_IsParamsAutoRefresh
,0  	-- @Sp_HasNewParams
,0  	-- @Sp_IsParamsValid
,0  	-- @Sp_IsParamValueSetValid
,0  	-- @Sp_HasNewColumns
,0  	-- @Sp_IsColumnsValid
,0  	-- @Sp_IsValid
,null  	-- @Sp_Notes
,1  	-- @Sp_IsActiveRow
,'0ff1f126-8e1f-403a-9340-bcd99b74283f'  	-- @Sp_UserId
,null  	-- @Sp_Stamp
,0  	-- @BypassConcurrencyCheck
,null  	-- @Success
,null  	-- @IsConcurrencyGood
,null  	-- @ErrorLogId

--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
