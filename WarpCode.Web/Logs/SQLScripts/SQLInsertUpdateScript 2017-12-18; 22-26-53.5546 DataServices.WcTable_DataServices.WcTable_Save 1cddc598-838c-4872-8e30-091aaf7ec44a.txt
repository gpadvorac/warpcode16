--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- user id=VEMaster_User;initial catalog=WarpCodeV16;data source=Voyager3;Connect Timeout=20

exec spWcTable_put 
'b093e000-66f1-48ec-9c07-53c4ce5d6801'  	-- @Tb_Id
,'aa41875c-14a8-43d3-a796-ac7b6c166f22'  	-- @Tb_ApVrsn_Id
,'tbLease'  	-- @Tb_Name
,'Lease'  	-- @Tb_EntityRootName
,''  	-- @Tb_VariableName
,''  	-- @Tb_ScreenCaption
,''  	-- @Tb_Description
,''  	-- @Tb_DevelopmentNote
,'UIControls'  	-- @Tb_UIAssemblyName
,'UIControls'  	-- @Tb_UINamespace
,'D:\Apps\Pipeline\AppV3\UIControls'  	-- @Tb_UIAssemblyPath
,'LeaseService'  	-- @Tb_WebServiceName
,'WS\Ap\Lease'  	-- @Tb_WebServiceFolder
,'DataServices'  	-- @Tb_DataServiceAssemblyName
,'D:\Apps\Pipeline\AppV3\DataServices'  	-- @Tb_DataServicePath
,'WireTypes'  	-- @Tb_WireTypeAssemblyName
,'D:\Apps\Pipeline\AppV3\WireTypes'  	-- @Tb_WireTypePath
,1  	-- @Tb_UseTilesInPropsScreen
,1  	-- @Tb_UseGridColumnGroups
,1  	-- @Tb_UseGridDataSourceCombo
,null  	-- @Tb_ApCnSK_Id
,1  	-- @Tb_UseLegacyConnectionCode
,0  	-- @Tb_PkIsIdentity
,0  	-- @Tb_IsVirtual
,0  	-- @Tb_IsNotEntity
,0  	-- @Tb_IsMany2Many
,0  	-- @Tb_UseLastModifiedByUserNameInSproc
,0  	-- @Tb_UseUserTimeStamp
,1  	-- @Tb_UseForAudit
,0  	-- @Tb_IsInputComplete
,0  	-- @Tb_IsCodeGen
,1  	-- @Tb_IsReadyCodeGen
,0  	-- @Tb_IsCodeGenComplete
,0  	-- @Tb_IsTagForCodeGen
,0  	-- @Tb_IsTagForOther
,1  	-- @Tb_IsActiveRow
,'0ff1f126-8e1f-403a-9340-bcd99b74283f'  	-- @Tb_UserId
,null  	-- @Tb_Stamp
,0  	-- @BypassConcurrencyCheck
,null  	-- @Success
,null  	-- @IsConcurrencyGood
,null  	-- @ErrorLogId

--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
