﻿using System;
using Ifx;
using vDataServices;
using System.Web.Security;


namespace VelocityService
{
    public class MembershipHelper
    {

        #region Initialize Variables

        private static string _as = "EntityService";
        private static string _cn = "MembershipHelper";

        #endregion Initialize Variables

        public static bool ChangeUserPassword(Guid UserId, string newPW)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_RegisterUser", IfxTraceCategory.Enter);
                string userName = v_UserProfile_DataServices.GetUserEmail(UserId);
                MembershipUser user = Membership.GetUser(userName);
                string oldPW = user.GetPassword();
                return user.ChangePassword("oldpw", newPW);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_RegisterUser", ex);
                return false;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Users_RegisterUser", IfxTraceCategory.Leave);
            }
        }
    }
}
