﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using DataServices;
using EntityWireType;
using Ifx;
using vReportSSRSProvider;

namespace VelocityService
{

    /// <summary>
    /// This class must be manually synchronized between the web project and  vScheduleAndNotificationServices
    /// and across all applications.
    /// </summary>
    public partial class JobManager
    {



        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "JobManager";

        #endregion Initialize Variables



        public static void RunJob(Guid jobId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunJob", IfxTraceCategory.Enter);

                object[] obj = v_ScheduleJob_DataServices.v_ScheduleJob_GetById(jobId);
                v_ScheduleJob_Values job = new v_ScheduleJob_Values((object[])obj[0], null);
                StringBuilder sb;
                switch ((int)job.v_SchlJb_SchlJbTp_Id)
                {
                    case 1:  // Notification
                        //sb = new StringBuilder();
                        //sb.Append("Successfuly tried to run this job.");
                        //sb.Append(Environment.NewLine + "Job ID: " + jobId.ToString());
                        //IfxEvent.PublishTrace(traceId, _as, _cn, "RunJob", sb.ToString(), null, null, new Exception(sb.ToString()), IfxTraceCategory.Catch);

                        RunNotification(jobId);

                        break;
                    case 2:  // Execute Stored Procedure
                        sb = new StringBuilder();
                        sb.Append("No code for 'Execute Stored Procedure' yet.");
                        sb.Append(Environment.NewLine + "Job ID: " + jobId.ToString());
                        IfxEvent.PublishTrace(traceId, _as, _cn, "RunJob", sb.ToString(), null, null, new Exception(sb.ToString()), IfxTraceCategory.Catch);
                        break;
                    case 3:  // Other

                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunJob", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunJob", IfxTraceCategory.Leave);
            }
        }

        public static void RunNotificationX(Guid jobId)
        {
            int step = 0;
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunNotification", IfxTraceCategory.Enter);

                int iSuccess = 0;
                object[] obj = v_Notification_DataServices.Getv_Notification_row_BySchlJb_Id(jobId);
                step = 1;
                //LogTrace("RunNotification", jobId, 1);
                v_Notification_Values notification = new v_Notification_Values((object[])obj[0], null);
                List<v_NotificationRecipientList_Binding> list = new List<v_NotificationRecipientList_Binding>();
                switch ((int)notification.v_Ntfc_NtfcTp_Id)
                {
                    case 1:  // Email, text from stored procedure

                        // Recipient Source Type
                        //Are the Recipients from a SP or are they assigned to the Notification?
                        object[] data;
                        switch ((int)notification.v_Ntfc_NtfcRcSrcTp_Id)
                        {
                            case 1:  // Recipients manualy assigned
                                data = v_Notification_DataServices.Getv_NotificationRecipient_ListByJobId(notification.v_Ntfc_SchlJb_Id);
                                step = 2;
                                //LogTrace("RunNotification", jobId, 2);
                                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                                {
                                    list.Add(new v_NotificationRecipientList_Binding((object[])data[i]));
                                }
                                step = 3;
                                //LogTrace("RunNotification", jobId, 3);
                                break;
                            case 2:  // Recipients queried via stored procedure
                                data = v_Notification_DataServices.GetRecipientList(notification.v_Ntfc_RecipientList_StoredProcedure);
                                step = 4;
                                //LogTrace("RunNotification", jobId, 4);
                                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                                {
                                    list.Add(new v_NotificationRecipientList_Binding((object[])data[i]));
                                }
                                step = 5;
                                //LogTrace("RunNotification", jobId, 5);
                                break;
                        }
                        if (list == null || list.Count == 0)
                        {
                            // ToDo:  Log Exception that there were no recipients
                            step = 55;
                            LogTrace("RunNotification - No Recipients Available", jobId, 55);
                        }
                        else
                        {
                            // Now we have the list of recipients
                            // Log the notification event
                            Guid eventId = Guid.NewGuid();
                            iSuccess = DataServices.v_NotificationEvent_DataServices.ExecuteNotificationEvent_insert(eventId, notification.v_Ntfc_SchlJb_Id, null, notification.v_Ntfc_Title, "", null);
                            step = 6;
                            //LogTrace("RunNotification", jobId, 6);
                            // Is the content the same for everyone or Recipient Specific?
                            if (notification.v_Ntfc_IsContentSpeficicToRecipient == false)
                            {
                                // ToDo: Call a SP to get the body of this notification
                                string body = "ToDo: Need to get the content for this notifiation...";
                                // Assign the same content to all emails
                                foreach (var recipient in list)
                                {
                                    NotificationMailHelper.SendMail(eventId, recipient.Pn_EMail, notification.v_Ntfc_Title, body, null, null, null);
                                }
                                step = 7;
                                //LogTrace("RunNotification", jobId, 7);
                            }
                            else
                            {
                                // Loop thru all recipients and call a SP for each one getting content unique to each recipient.

                                foreach (var recipient in list)
                                {
                                    object[] parms;
                                    // Get the notification subject and content
                                    parms = v_Notification_DataServices.GetNotificationContentByRecipientIdAndSproName(
                                        recipient.Pn_Id, notification.v_Ntfc_Content_StoredProcedure);
                                    step = 8;
                                    //LogTrace("RunNotification", jobId, 8);
                                    // Send notifiation to each recipient
                                    if (parms != null && parms[0] != null && parms[1] != null)
                                    {
                                        step = 9;
                                        //LogTrace("RunNotification", jobId, 9);
                                        NotificationMailHelper.SendMail(eventId, recipient.Pn_EMail, (string)parms[0], (string)parms[1], null, null, null);
                                        step = 10;
                                        //LogTrace("RunNotification", jobId, 10);
                                    }
                                    else
                                    {
                                        // ToDo: Log exception
                                    }
                                }
                            }
                        }
                        step = 11;
                        //LogTrace("RunNotification", jobId, 11);
                        break;
                    case 2:  // Email, SSRS report attachment
                        step = 12;
                        //LogTrace("RunNotification", jobId, 12);
                        break;
                    case 3:  // Don't know yet...
                        step = 13;
                        //LogTrace("RunNotification", jobId, 13);
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunNotification", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {

                StringBuilder sb = new StringBuilder();
                sb.Append("Identifing error location. Made it to step " + step.ToString());
                sb.Append(Environment.NewLine + "Job ID: " + jobId.ToString());
                IfxEvent.PublishTrace(traceId, _as, _cn, "RunNotification", sb.ToString(), null, null, new Exception(sb.ToString()), IfxTraceCategory.Catch);

                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunNotification", IfxTraceCategory.Leave);
            }
        }





        public static void RunNotification(Guid jobId)
        {
            double step = 0;
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunNotification", IfxTraceCategory.Enter);
                string msg = "";
                int iSuccess = 0;
                object[] obj = v_Notification_DataServices.Getv_Notification_row_BySchlJb_Id(jobId);
                //step = 1;
                //LogTrace("RunNotification", jobId, 1);
                v_Notification_Values notification = new v_Notification_Values((object[])obj[0], null);
                List<v_NotificationRecipientList_Binding> list = new List<v_NotificationRecipientList_Binding>();

                // Get Recipeint List
                object[] data;
                switch ((int)notification.v_Ntfc_NtfcRcSrcTp_Id)
                {
                    case 1:  // Recipients manualy assigned
                        data = v_Notification_DataServices.Getv_NotificationRecipient_ListByJobId(notification.v_Ntfc_SchlJb_Id);
                        if (data == null)
                        {
                            msg = "No recipients assigned to job: " + notification.v_Ntfc_Title;
                            IfxEvent.PublishTrace(traceId, _as, _cn, "RunNotification", msg, null, null, new Exception(msg), IfxTraceCategory.Catch);
                            return;
                        }
                        //step = 2;
                        //LogTrace("RunNotification", jobId, 2);
                        for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                        {
                            list.Add(new v_NotificationRecipientList_Binding((object[])data[i]));
                        }
                        //step = 3;
                        //LogTrace("RunNotification", jobId, 3);
                        break;
                    case 2:  // Recipients queried via stored procedure
                        data = v_Notification_DataServices.GetRecipientList(notification.v_Ntfc_RecipientList_StoredProcedure);
                        if (data == null)
                        {
                            msg = "No recipients returned from query for job: " + notification.v_Ntfc_Title;
                            IfxEvent.PublishTrace(traceId, _as, _cn, "RunNotification", msg, null, null, new Exception(msg), IfxTraceCategory.Catch);
                            return;
                        }
                        //step = 4;
                        //LogTrace("RunNotification", jobId, 4);
                        for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                        {
                            list.Add(new v_NotificationRecipientList_Binding((object[])data[i]));
                        }
                        //step = 5;
                        //LogTrace("RunNotification", jobId, 5);
                        break;
                }

                if (list == null || list.Count == 0)
                {
                    // ToDo:  Log Exception that there were no recipients
                    step = 55;
                    LogTrace("RunNotification - No Recipients Available", jobId, 55);
                }


                // Log the notification event
                Guid eventId = Guid.NewGuid();
                iSuccess = DataServices.v_NotificationEvent_DataServices.ExecuteNotificationEvent_insert(eventId, notification.v_Ntfc_SchlJb_Id, null, notification.v_Ntfc_Title, "", null);

                // Process according to notification type:
                // 1	Email, text from stored procedure
                // 2	Email, SSRS report attachment
                switch ((int)notification.v_Ntfc_NtfcTp_Id)
                {
                    case 1: // Email, text from stored procedure
                        ProcessEmailWithTextContent(jobId, eventId, notification, list);
                        break;
                    case 2: // Email attachment from SSRS report
                        ProcessEmailWithSsrsAttachment(jobId, eventId, notification, list);
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunNotification", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {

                //StringBuilder sb = new StringBuilder();
                //sb.Append("Identifing error location. Made it to step " + step.ToString());
                //sb.Append(Environment.NewLine + "Job ID: " + jobId.ToString());
                //IfxEvent.PublishTrace(traceId, _as, _cn, "RunNotification", sb.ToString(), null, null, new Exception(sb.ToString()), IfxTraceCategory.Catch);

                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunNotification", IfxTraceCategory.Leave);
            }
        }



        static void ProcessEmailWithTextContent(Guid jobId, Guid eventId, v_Notification_Values notification, List<v_NotificationRecipientList_Binding> recipients)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ProcessEmailWithTextContent", IfxTraceCategory.Enter);
                int step;
                step = 6;
                //LogTrace("RunNotification", jobId, 6);
                // Is the content the same for everyone or Recipient Specific?
                if (notification.v_Ntfc_IsContentSpeficicToRecipient == false)
                {
                    // ToDo: Call a SP to get the body of this notification
                    string body = "ToDo: Need to get the content for this notifiation...";


                    // Assign the same content to all emails
                    foreach (var recipient in recipients)
                    {
                        NotificationMailHelper.SendMail(eventId, recipient.Pn_EMail, notification.v_Ntfc_Title, body, null, null, null);
                    }
                    step = 7;
                    //LogTrace("RunNotification", jobId, 7);
                }
                else
                {
                    // Loop thru all recipients and call a SP for each one getting content unique to each recipient.
                    foreach (var recipient in recipients)
                    {
                        object[] parms;
                        // Get the notification subject and content
                        parms = v_Notification_DataServices.GetNotificationContentByRecipientIdAndSproName(
                            recipient.Pn_Id, notification.v_Ntfc_Content_StoredProcedure);
                        step = 8;
                        //LogTrace("RunNotification", jobId, 8);
                        // Send notifiation to each recipient
                        if (parms != null && parms[0] != null && parms[1] != null)
                        {
                            step = 9;
                            //LogTrace("RunNotification", jobId, 9);
                            NotificationMailHelper.SendMail(eventId, recipient.Pn_EMail, (string)parms[0], (string)parms[1], null, null, null);
                            step = 10;
                            //LogTrace("RunNotification", jobId, 10);
                        }
                        else
                        {
                            // ToDo: Log exception
                        }
                    }
                }
                step = 11;
                //LogTrace("RunNotification", jobId, 11);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ProcessEmailWithTextContent", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ProcessEmailWithTextContent", IfxTraceCategory.Leave);
            }
        }






        static void ProcessEmailWithSsrsAttachment(Guid jobId, Guid eventId, v_Notification_Values notification, List<v_NotificationRecipientList_Binding> recipients)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ProcessEmailWithSsrsAttachment", IfxTraceCategory.Enter);
                int step;
                step = 20;
                //LogTrace("RunNotification", jobId, step);
                // Is the content the same for everyone or Recipient Specific?
                if (notification.v_Ntfc_IsContentSpeficicToRecipient == false)
                {
                    // Get Report Attachment (same for all recipients)
                    //Attachment attachment = MailHelper.GetReportAsAttachment(notification, null);

                    ReportMetadata rpt = NotificationMailHelper.GetReportObject(notification, null);
                    ReportProvider provider = new ReportProvider();
                    byte[] reportData = provider.GetReportAsByteArray(rpt);
                    //MemoryStream stream = new MemoryStream(reportData);
                    //Attachment attachment = new Attachment(stream, notification.v_Ntfc_Rpt_Id_TextField + "." + MailHelper.GetAttachmentFileExtentionFromInt((int)notification.v_Ntfc_NtfcSsrsF_Id));
                    //// https://msdn.microsoft.com/en-us/library/system.net.mime.contenttype.mediatype(v=vs.110).aspx
                    //ContentType content = attachment.ContentType;
                    //content.MediaType = MediaTypeNames.Text.Plain;


                    string body = "Please review the attachment.";
                    // Assign the same content to all emails

                    foreach (var recipient in recipients)
                    {

                        byte[] reportDataCopy = (byte[])reportData.Clone();
                        MemoryStream stream = new MemoryStream(reportDataCopy);
                        Attachment attachment = new Attachment(stream, notification.v_Ntfc_Rpt_Id_TextField + "." + NotificationMailHelper.GetAttachmentFileExtentionFromInt((int)notification.v_Ntfc_NtfcSsrsF_Id));
                        // https://msdn.microsoft.com/en-us/library/system.net.mime.contenttype.mediatype(v=vs.110).aspx
                        ContentType content = attachment.ContentType;
                        content.MediaType = MediaTypeNames.Text.Plain;
                        NotificationMailHelper.SendMail(eventId, recipient.Pn_EMail, notification.v_Ntfc_Title, body, attachment, null, null);
                    }

                    //MailHelper.SendMail(eventId, "recipient.Pn_EMail", notification.v_Ntfc_Title, body, attachment, null);



                    step = 21;
                    //LogTrace("RunNotification", jobId, step);
                }
                else
                {
                    // Loop thru all recipients and get a unique report each recipient.
                    foreach (var recipient in recipients)
                    {

                        step = 22;
                        //LogTrace("RunNotification", jobId, step);
                        //Attachment attachment = MailHelper.GetReportAsAttachment(notification, recipient.Pn_Id);

                        ReportMetadata rpt = NotificationMailHelper.GetReportObject(notification, recipient.Pn_Id);
                        ReportProvider provider = new ReportProvider();
                        byte[] reportData = provider.GetReportAsByteArray(rpt);
                        MemoryStream stream = new MemoryStream(reportData);
                        Attachment attachment = new Attachment(stream, notification.v_Ntfc_Rpt_Id_TextField + "." + NotificationMailHelper.GetAttachmentFileExtentionFromInt((int)notification.v_Ntfc_NtfcSsrsF_Id));
                        // https://msdn.microsoft.com/en-us/library/system.net.mime.contenttype.mediatype(v=vs.110).aspx
                        ContentType content = attachment.ContentType;
                        content.MediaType = MediaTypeNames.Text.Plain;
                        //MailHelper.LogAttachment(notification, recipient.Pn_Id, reportData);

                        step = 23;
                        //LogTrace("RunNotification", jobId, step);

                        string body = "Please review the attachment.";
                        NotificationMailHelper.SendMail(eventId, recipient.Pn_EMail, notification.v_Ntfc_Title, body, attachment, null, null);

                        step = 24;
                        //LogTrace("RunNotification", jobId, step);

                    }
                }
                step = 25;
                //LogTrace("RunNotification", jobId, step);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ProcessEmailWithSsrsAttachment", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ProcessEmailWithSsrsAttachment", IfxTraceCategory.Leave);
            }
        }












        //static void ProcessEmailWithSsrsAttachment(Guid jobId, Guid eventId, v_Notification_Values notification, List<v_NotificationRecipientList_Binding> recipients)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ProcessEmailWithSsrsAttachment", IfxTraceCategory.Enter);
        //        int step;
        //        step = 20;
        //        //LogTrace("RunNotification", jobId, step);
        //        // Is the content the same for everyone or Recipient Specific?
        //        if (notification.v_Ntfc_IsContentSpeficicToRecipient == false)
        //        {
        //            // Get Report Attachment (same for all recipients)
        //            //Attachment attachment = MailHelper.GetReportAsAttachment(notification, null);

        //            ReportMetadata rpt = MailHelper.GetReportObject(notification, null);
        //            ReportProvider provider = new ReportProvider();
        //            byte[] reportData = provider.GetReportAsByteArray(rpt);
        //            MemoryStream stream = new MemoryStream(reportData);
        //            Attachment attachment = new Attachment(stream, notification.v_Ntfc_Rpt_Id_TextField + "." + MailHelper.GetAttachmentFileExtentionFromInt((int)notification.v_Ntfc_NtfcSsrsF_Id));
        //            // https://msdn.microsoft.com/en-us/library/system.net.mime.contenttype.mediatype(v=vs.110).aspx
        //            ContentType content = attachment.ContentType;
        //            content.MediaType = MediaTypeNames.Text.Plain;
        //            //MailHelper.LogAttachment(notification, null, reportData);


        //            string body = "Please review the attachment.";
        //            // Assign the same content to all emails

        //            // TEST:  Replace these 3 lines with the block below from MailHelper.SendMail and modified
        //            foreach (var recipient in recipients)
        //            {
        //                MailHelper.SendMail(eventId, recipient.Pn_EMail, notification.v_Ntfc_Title, body, attachment, null);
        //            }




        //            //string logEmail = System.Configuration.ConfigurationManager.AppSettings["Test_LogEmailAsException"];
        //            //////if (logEmail == "true")
        //            //////{
        //            //////    IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", new Exception("To: " + addressTo + ", Subject: " + subject + ", Body: " + body));
        //            //////}


        //            ////string host = System.Configuration.ConfigurationManager.AppSettings["MailSmtpServer"];
        //            ////string userName = System.Configuration.ConfigurationManager.AppSettings["MailAdminName"];
        //            ////string userPW = System.Configuration.ConfigurationManager.AppSettings["MailAdminPassword"];
        //            ////string mailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
        //            ////int iSuccess = 0;



        //            ////MailMessage mail = new MailMessage();
        //            ////mail.From = new MailAddress(mailFrom);
        //            //////////mail.To.Add(addressTo);
        //            ////mail.BodyEncoding = System.Text.Encoding.UTF8;
        //            ////mail.SubjectEncoding = System.Text.Encoding.UTF8;

        //            ////string subject = notification.v_Ntfc_Title.Replace("\r\n", " ");

        //            ////mail.Subject = subject;
        //            ////mail.Body = body;
        //            ////if (attachment != null)
        //            ////{
        //            ////    mail.Attachments.Add(attachment);
        //            ////}



        //            //////////MailHelper.NotificationLogItem log = new MailHelper.NotificationLogItem(Guid.NewGuid(), eventId, subject, body, mailFrom, recipient.Pn_EMail, userId);

        //            ////SmtpClient smtp = new SmtpClient(host);
        //            ////smtp.Credentials = new System.Net.NetworkCredential(userName, userPW);
        //            ////smtp.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);


        //            ////foreach (var recipient in recipients)
        //            ////{

        //            ////    mail.To.Clear();
        //            ////    mail.To.Add(recipient.Pn_EMail);
        //            ////    if (logEmail == "true")
        //            ////    {
        //            ////        IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", new Exception("To: " + recipient.Pn_EMail + ", Subject: " + subject + ", Body: " + body));
        //            ////    }
        //            ////    MailHelper.NotificationLogItem log = new MailHelper.NotificationLogItem(Guid.NewGuid(), eventId, subject, body, mailFrom, recipient.Pn_EMail, null);
        //            ////    smtp.SendAsync(mail, log);
        //            ////}


        //            step = 21;
        //            //LogTrace("RunNotification", jobId, step);
        //        }
        //        else
        //        {
        //            // Loop thru all recipients and get a unique report each recipient.
        //            foreach (var recipient in recipients)
        //            {

        //                step = 22;
        //                //LogTrace("RunNotification", jobId, step);
        //                //Attachment attachment = MailHelper.GetReportAsAttachment(notification, recipient.Pn_Id);

        //                ReportMetadata rpt = MailHelper.GetReportObject(notification, recipient.Pn_Id);
        //                ReportProvider provider = new ReportProvider();
        //                byte[] reportData = provider.GetReportAsByteArray(rpt);
        //                MemoryStream stream = new MemoryStream(reportData);
        //                Attachment attachment = new Attachment(stream, notification.v_Ntfc_Rpt_Id_TextField + "." + MailHelper.GetAttachmentFileExtentionFromInt((int)notification.v_Ntfc_NtfcSsrsF_Id));
        //                // https://msdn.microsoft.com/en-us/library/system.net.mime.contenttype.mediatype(v=vs.110).aspx
        //                ContentType content = attachment.ContentType;
        //                content.MediaType = MediaTypeNames.Text.Plain;
        //                //MailHelper.LogAttachment(notification, recipient.Pn_Id, reportData);

        //                step = 23;
        //                //LogTrace("RunNotification", jobId, step);

        //                string body = "Please review the attachment.";
        //                MailHelper.SendMail(eventId, recipient.Pn_EMail, notification.v_Ntfc_Title, body, attachment, null);

        //                step = 24;
        //                //LogTrace("RunNotification", jobId, step);

        //            }
        //        }
        //        step = 25;
        //        //LogTrace("RunNotification", jobId, step);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ProcessEmailWithSsrsAttachment", ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ProcessEmailWithSsrsAttachment", IfxTraceCategory.Leave);
        //    }
        //}




        //static bool mailSent = false;
        //private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SendCompletedCallback", null, IfxTraceCategory.Enter);
        //        // Get the unique identifier for this asynchronous operation.
        //        MailHelper.NotificationLogItem log = e.UserState as MailHelper.NotificationLogItem;
        //        if (e.Cancelled)
        //        {
        //            // this line should not ever be hit
        //            //Console.WriteLine("[{0}] Send canceled.", token);
        //        }
        //        else
        //        {
        //            string err = "";
        //            if (e.Error != null) { err = e.Error.ToString(); }
        //            int iSuccess = DataServices.v_NotificationLog_DataServices.v_NotificationLog_Save(log.NtfcLg_Id, log.NtfcEv_Id, log.Subject, log.Content, log.AddressFrom, "", log.AddressTo, err, log.UserId);

        //        }
        //        if (e.Error != null)
        //        {
        //            string msg = "Notification email to " + log.AddressTo + " failed.  e.Error  = " + e.Error.ToString(); ;
        //            Exception exx = new Exception(msg);
        //            if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SendCompletedCallback", exx);
        //        }
        //        mailSent = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SendCompletedCallback", ex);
        //        mailSent = false;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SendCompletedCallback", IfxTraceCategory.Leave);
        //    }
        //}





        static void LogTrace(string method, Guid jobId, int step)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LogTrace", IfxTraceCategory.Enter);

                StringBuilder sb = new StringBuilder();
                sb.Append("Identifing error location. Made it to step " + step.ToString());
                sb.Append(Environment.NewLine + "Job ID: " + jobId.ToString());
                IfxEvent.PublishTrace(traceId, _as, _cn, method, sb.ToString(), null, null, new Exception(sb.ToString()), IfxTraceCategory.Catch);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LogTrace", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LogTrace", IfxTraceCategory.Leave);
            }
        }


    }
}