﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using EntityWireType;
using Ifx;
using vReportSSRSProvider;

namespace VelocityService
{
    public class MailHelper
    {


        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "MailHelper";

        #endregion Initialize Variables




        // ToDo: put this in a public class as a static method so it can be called from anywhere
        public static void SendMail(string addressTo, string subject, string body, Attachment attachment)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", null, IfxTraceCategory.Enter);

                string logEmail = System.Configuration.ConfigurationManager.AppSettings["Test_LogEmailAsException"];
                if (logEmail == "true")
                {
                    IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", new Exception("To: " + addressTo + ", Subject: " + subject + ", Body: " + body));
                }


                string host = System.Configuration.ConfigurationManager.AppSettings["MailSmtpServer"];
                string userName = System.Configuration.ConfigurationManager.AppSettings["MailAdminName"];
                string userPW = System.Configuration.ConfigurationManager.AppSettings["MailAdminPassword"];
                string mailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
                int iSuccess = 0;



                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(mailFrom);
                mail.To.Add(addressTo);
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                subject = subject.Replace("\r\n", " ");

                mail.Subject = subject;
                mail.Body = body;
                if (attachment != null)
                {
                    mail.Attachments.Add(attachment);
                }


                SmtpClient smtp = new SmtpClient(host);
                smtp.Credentials = new System.Net.NetworkCredential(userName, userPW);
                smtp.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                smtp.SendAsync(mail, mail);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", ex);
                //return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", IfxTraceCategory.Leave);
            }
        }


        static bool mailSent = false;
        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SendCompletedCallback", null, IfxTraceCategory.Enter);
                // Get the unique identifier for this asynchronous operation.
                MailMessage mail = e.UserState as MailMessage;
                if (e.Cancelled)
                {
                    // this line should not ever be hit
                    //Console.WriteLine("[{0}] Send canceled.", token);
                }
                else
                {
                    string err = "";
                    if (e.Error != null) { err = e.Error.ToString(); }
                }
                if (e.Error != null)
                {


                    string msg = "" ;
                    if (mail != null)
                    {
                        msg = "Email to " + mail.To + " failed.  e.Error  = " + e.Error.ToString() + ", Subject = " + mail.Subject;
                    }
                    else
                    {
                        msg = "Email failed.  e.Error  = " + e.Error.ToString();
                    }
                    Exception exx = new Exception(msg);
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SendCompletedCallback", exx);
                }
                mailSent = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SendCompletedCallback", ex);
                mailSent = false;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SendCompletedCallback", IfxTraceCategory.Leave);
            }
        }





        public class NotificationLogItem
        {

            public NotificationLogItem(Guid ntfcLg_Id, Guid ntfcEv_Id, string subject, string content, string addressFrom, string addressTo, Guid? userId)
            {
                NtfcLg_Id = ntfcLg_Id;
                NtfcEv_Id = ntfcEv_Id;
                Subject = subject;
                Content = content;
                AddressFrom = addressFrom;
                AddressTo = addressTo;
                UserId = userId;
            }

            Guid _ntfcLg_Id;
            public Guid NtfcLg_Id
            {
                get { return _ntfcLg_Id; }
                set { _ntfcLg_Id = value; }
            }

            Guid _ntfcEv_Id;
            public Guid NtfcEv_Id
            {
                get { return _ntfcEv_Id; }
                set { _ntfcEv_Id = value; }
            }

            string _subject;
            public string Subject
            {
                get { return _subject; }
                set { _subject = value; }
            }

            string _content;
            public string Content
            {
                get { return _content; }
                set { _content = value; }
            }

            string _addressFrom;
            public string AddressFrom
            {
                get { return _addressFrom; }
                set { _addressFrom = value; }
            }

            string _addressTo;
            public string AddressTo
            {
                get { return _addressTo; }
                set { _addressTo = value; }
            }

            Guid? _userId;
            public Guid? UserId
            {
                get { return _userId; }
                set { _userId = value; }
            }

        }













    }
}