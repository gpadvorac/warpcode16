﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using EntityWireType;
using Ifx;

namespace VelocityService
{
    public class FileHelper
    {


        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "FileHelper";

        #endregion Initialize Variables

        public class FileStorageParentType
        {
            public static string Idea = "Idea";
            public static string Idea_KB = "Idea_KB";
            public static string Finding = "Finding";
            public static string Task = "Task";
            public static string Project = "Project";
            public static string DiscussionComment = "DiscussionComment";
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="fileName"></param>
        /// <param name="file"></param>
        /// <returns>1 = Success, 0 = Fail</returns>
        public static int WriteFileToDisk(string folder, string fileName, byte[] file)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WriteFileToDisk",
                    new ValuePair[] { new ValuePair("folder", folder), new ValuePair("fileName", fileName) }, IfxTraceCategory.Enter);

                string path="";

                // Check to see if the folder exists for this file
                if (!Directory.Exists(folder))
                {
                    // It doesnt exist, so create it
                    Directory.CreateDirectory(folder);
                }
                path = folder + @"\" + fileName;

                File.WriteAllBytes(path, file);

                return 1;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WriteFileToDisk", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WriteFileToDisk", IfxTraceCategory.Leave);
            }
        }




        public static int WriteFileToDisk(FileStorage_Values data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);

                //FileStoragePath
                string path = System.Configuration.ConfigurationManager.AppSettings["FileStoragePath"];

                // Check to see if the folder exists for this file
                if (!Directory.Exists(path + data.FS_ParentType))
                {
                    // It doesnt exist, so create it
                    Directory.CreateDirectory(path + data.FS_ParentType);
                }
                path = path + data.FS_ParentType + @"\" + data.FS_Id.ToString() + "_" + data.FS_FileName;

                File.WriteAllBytes(path, data.FileData);

                return 1;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", IfxTraceCategory.Leave);
            }
        }










    }
}