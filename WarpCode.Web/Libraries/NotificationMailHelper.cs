﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using EntityWireType;
using Ifx;
using vReportSSRSProvider;

namespace VelocityService
{

    /// <summary>
    /// This class must be manually synchronized between the web project and  vScheduleAndNotificationServices
    /// and across all applications.
    /// </summary>
    public class NotificationMailHelper
    {


        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "NotificationMailHelper";

        #endregion Initialize Variables






        public static Attachment GetReportAsAttachment(v_Notification_Values notification, Guid? personId)
        {
            MemoryStream ms = null;
            //Code to render in pdf format
            // http://forums.asp.net/t/1549431.aspx?Code+to+render+the+report+in+PDF+format+using+C+in+SSRS+2008
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Enter);
                // Make sure SSRS Format is never null
                if (notification.v_Ntfc_NtfcSsrsF_Id == null)
                {
                    notification.v_Ntfc_NtfcSsrsF_Id = (int)ReportFormat.PDF;
                }
                ReportMetadata rpt = new ReportMetadata();

                // For now we are assuming all reports will take only one parameter by this name
                // or no paramters at all.
                if (personId != null)
                {
                    rpt.AddReportParam("Pn_Id", personId.ToString());
                }

                rpt.Format = GetReportFormatFromInt((int)notification.v_Ntfc_NtfcSsrsF_Id);
                rpt.ReportSysName = notification.v_Ntfc_Rpt_Id_TextField;
                ReportProvider provider = new ReportProvider();
                byte[] reportData = provider.GetReportAsByteArray(rpt);
                ms = new MemoryStream(reportData);

                Attachment attachment = new Attachment(ms, notification.v_Ntfc_Rpt_Id_TextField + "." + GetAttachmentFileExtentionFromInt((int)notification.v_Ntfc_NtfcSsrsF_Id));
                // https://msdn.microsoft.com/en-us/library/system.net.mime.contenttype.mediatype(v=vs.110).aspx
                ContentType content = attachment.ContentType;
                content.MediaType = MediaTypeNames.Text.Plain;
                LogAttachment(notification, personId, reportData);
                return attachment;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (ms != null)
                {
                    ms.Close();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Leave);
            }
        }




        public static ReportMetadata GetReportObject(v_Notification_Values notification, Guid? personId)
        {
            //MemoryStream ms = null;
            //Code to render in pdf format
            // http://forums.asp.net/t/1549431.aspx?Code+to+render+the+report+in+PDF+format+using+C+in+SSRS+2008
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Enter);
                // Make sure SSRS Format is never null
                if (notification.v_Ntfc_NtfcSsrsF_Id == null)
                {
                    notification.v_Ntfc_NtfcSsrsF_Id = (int)ReportFormat.PDF;
                }
                ReportMetadata rpt = new ReportMetadata();

                // For now we are assuming all reports will take only one parameter by this name
                // or no paramters at all.
                if (personId != null)
                {
                    rpt.AddReportParam("Pn_Id", personId.ToString());
                }

                rpt.Format = GetReportFormatFromInt((int)notification.v_Ntfc_NtfcSsrsF_Id);
                rpt.ReportSysName = notification.v_Ntfc_Rpt_Id_TextField;
                return rpt;
                //ReportProvider provider = new ReportProvider();
                //byte[] reportData = provider.GetReportAsByteArray(rpt);
                //ms = new MemoryStream(reportData);

                //Attachment attachment = new Attachment(ms, notification.v_Ntfc_Rpt_Id_TextField + "." + GetAttachmentFileExtentionFromInt((int)notification.v_Ntfc_NtfcSsrsF_Id));
                //// https://msdn.microsoft.com/en-us/library/system.net.mime.contenttype.mediatype(v=vs.110).aspx
                //ContentType content = attachment.ContentType;
                //content.MediaType = MediaTypeNames.Text.Plain;
                //LogAttachment(notification, personId, reportData);
                //return attachment;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                //if (ms != null)
                //{
                //    ms.Close();
                //}
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Leave);
            }
        }




        //public static ReportProvider GetReportProvider(ReportMetadata rpt)
        //{
        //    //MemoryStream ms = null;
        //    //Code to render in pdf format
        //    // http://forums.asp.net/t/1549431.aspx?Code+to+render+the+report+in+PDF+format+using+C+in+SSRS+2008
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Enter);
        //        //// Make sure SSRS Format is never null
        //        //if (notification.v_Ntfc_NtfcSsrsF_Id == null)
        //        //{
        //        //    notification.v_Ntfc_NtfcSsrsF_Id = (int)ReportFormat.PDF;
        //        //}
        //        //ReportMetadata rpt = new ReportMetadata();

        //        //// For now we are assuming all reports will take only one parameter by this name
        //        //// or no paramters at all.
        //        //if (personId != null)
        //        //{
        //        //    rpt.AddReportParam("Pn_Id", personId.ToString());
        //        //}

        //        //rpt.Format = GetReportFormatFromInt((int)notification.v_Ntfc_NtfcSsrsF_Id);
        //        //rpt.ReportSysName = notification.v_Ntfc_Rpt_Id_TextField;
        //        //return rpt;
        //        ReportProvider provider = new ReportProvider();
        //        //byte[] reportData = provider.GetReportAsByteArray(rpt);
        //        //ms = new MemoryStream(reportData);

        //        Attachment attachment = new Attachment(ms, notification.v_Ntfc_Rpt_Id_TextField + "." + GetAttachmentFileExtentionFromInt((int)notification.v_Ntfc_NtfcSsrsF_Id));
        //        // https://msdn.microsoft.com/en-us/library/system.net.mime.contenttype.mediatype(v=vs.110).aspx
        //        ContentType content = attachment.ContentType;
        //        content.MediaType = MediaTypeNames.Text.Plain;
        //        LogAttachment(notification, personId, reportData);
        //        return attachment;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        //if (ms != null)
        //        //{
        //        //    ms.Close();
        //        //}
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Leave);
        //    }
        //}




        //public static void Send()
        //{

        //    MailMessage message = new MailMessage("from@foo.com", "to@foo.com");
        //    MemoryStream stream = new MemoryStream(new byte[64000]);
        //    Attachment attachment = new Attachment(stream, "my attachment");
        //    message.Attachments.Add(attachment);
        //    message.Body = "This is an async test.";
        //    SmtpClient smtp = new SmtpClient("localhost");
        //    //smtp.Credentials = new NetworkCredential("login", "password");

        //    smtp.SendCompleted += delegate(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        //    {
        //        if (e.Error != null)
        //        {
        //            System.Diagnostics.Trace.TraceError(e.Error.ToString());

        //        }
        //        MailMessage userMessage = e.UserState as MailMessage;
        //        if (userMessage != null)
        //        {
        //            userMessage.Dispose();
        //        }
        //    };

        //    smtp.SendAsync(message, message);
        //}
















        public static void LogAttachment(v_Notification_Values notification, Guid? personId, byte[] reportData)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LogAttachment", IfxTraceCategory.Enter);

                string isLogReport = ConfigurationManager.AppSettings["LogEmailAttachmentsToDisk"];
                if (isLogReport == "true")
                {
                    string folder = ConfigurationManager.AppSettings["EmailAttachmentFolderPath"];
                    string date = DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss.ffff");
                    string fileName = date + " " + notification.v_Ntfc_Rpt_Id_TextField + "." +
                                      GetAttachmentFileExtentionFromInt((int)notification.v_Ntfc_NtfcSsrsF_Id);
                    FileHelper.WriteFileToDisk(folder, fileName, reportData);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LogAttachment", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LogAttachment", IfxTraceCategory.Leave);
            }
        }

        public static string GetAttachmentFileExtentionFromInt(int formatId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAttachmentFileExtentionFromInt", IfxTraceCategory.Enter);
                string ext;
                switch (formatId)
                {
                    case (int)ReportFormat.Excel:
                        ext = "xlsx";
                        break;
                    case (int)ReportFormat.Word:
                        ext = "docx";
                        break;
                    case (int)ReportFormat.PDF:
                        ext = "pdf";
                        break;
                    case (int)ReportFormat.CSV:
                        ext = "csv";
                        break;
                    case (int)ReportFormat.PNG:
                        ext = "png";
                        break;
                    case (int)ReportFormat.JPG:
                        ext = "jpg";
                        break;
                    case (int)ReportFormat.TIFF:
                        ext = "TIF";
                        break;
                    case (int)ReportFormat.XML:
                        ext = "xml";
                        break;
                    case (int)ReportFormat.MHTML:
                        ext = "mhtml";
                        break;
                    default:
                        ext = "pdf";
                        break;
                }
                return ext;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAttachmentFileExtentionFromInt", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAttachmentFileExtentionFromInt", IfxTraceCategory.Leave);
            }
        }

        static ReportFormat GetReportFormatFromInt(int formatId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Enter);
                ReportFormat format;
                switch (formatId)
                {
                    case (int)ReportFormat.Excel:
                        format = ReportFormat.Excel;
                        break;
                    case (int)ReportFormat.Word:
                        format = ReportFormat.Word;
                        break;
                    case (int)ReportFormat.PDF:
                        format = ReportFormat.PDF;
                        break;
                    case (int)ReportFormat.CSV:
                        format = ReportFormat.CSV;
                        break;
                    case (int)ReportFormat.PNG:
                        format = ReportFormat.PNG;
                        break;
                    case (int)ReportFormat.JPG:
                        format = ReportFormat.JPG;
                        break;
                    case (int)ReportFormat.TIFF:
                        format = ReportFormat.TIFF;
                        break;
                    case (int)ReportFormat.XML:
                        format = ReportFormat.XML;
                        break;
                    case (int)ReportFormat.MHTML:
                        format = ReportFormat.MHTML;
                        break;
                    default:
                        format = ReportFormat.PDF;
                        break;
                }
                return format;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Leave);
            }
        }



        // ToDo: put this in a public class as a static method so it can be called from anywhere
        public static void SendMail(Guid eventId, string addressTo, string subject, string body, Attachment attachment, Guid? userId, string otherInfo)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", null, IfxTraceCategory.Enter);

                string logEmail = System.Configuration.ConfigurationManager.AppSettings["Test_LogEmailAsException"];
                if (logEmail == "true")
                {
                    IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", new Exception("To: " + addressTo + ", Subject: " + subject + ", Body: " + body));
                }

                string host = System.Configuration.ConfigurationManager.AppSettings["MailSmtpServer"];
                string userName = System.Configuration.ConfigurationManager.AppSettings["MailAdminName"];
                string userPW = System.Configuration.ConfigurationManager.AppSettings["MailAdminPassword"];
                string mailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
                int iSuccess = 0;

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(mailFrom);
                mail.To.Add(addressTo);
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                subject = subject.Replace("\r\n", " ");

                mail.Subject = subject;
                mail.Body = body;
                if (attachment != null)
                {
                    mail.Attachments.Add(attachment);
                }

                NotificationLogItem log;
                if (otherInfo == null || otherInfo.Trim().Length == 0)
                {
                    otherInfo = "";
                }
                else
                {
                    otherInfo = otherInfo + ";  ";
                }

                //If the the sending of email is allowed and configured in the webconfig
                string isSendingMailAllowed = System.Configuration.ConfigurationManager.AppSettings["IsSendingMailAllowed"];
                if (isSendingMailAllowed == "true")
                {
                    log = new NotificationLogItem(Guid.NewGuid(), eventId, subject, body, mailFrom, addressTo, userId, otherInfo);
                    SmtpClient smtp = new SmtpClient(host);
                    smtp.Credentials = new System.Net.NetworkCredential(userName, userPW);
                    smtp.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                    smtp.SendAsync(mail, log);
                }
                else
                {
                    log = new NotificationLogItem(Guid.NewGuid(), eventId, subject, body, mailFrom, addressTo, userId, otherInfo + "Sending email not allowed or configured in the webconfig.");
                    int iSuccess2 = DataServices.v_NotificationLog_DataServices.v_NotificationLog_Save(log.NtfcLg_Id, log.NtfcEv_Id, log.Subject, log.Content, log.AddressFrom, "", log.AddressTo, "", log.OtherInfo, log.UserId);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", ex);
                //return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", IfxTraceCategory.Leave);
            }
        }




        // ToDo: put this in a public class as a static method so it can be called from anywhere
        public static void SendMail(Guid eventId, string addressTo, string addressToDisplayName, string ccTo, string ccToDisplayName, string subject, string body, Attachment attachment, Guid? userId, string otherInfo)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", null, IfxTraceCategory.Enter);

                string logEmail = System.Configuration.ConfigurationManager.AppSettings["Test_LogEmailAsException"];
                if (logEmail == "true")
                {
                    IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", new Exception("To: " + addressTo + ", Subject: " + subject + ", Body: " + body));
                }

                string host = System.Configuration.ConfigurationManager.AppSettings["MailSmtpServer"];
                string userName = System.Configuration.ConfigurationManager.AppSettings["MailAdminName"];
                string userPW = System.Configuration.ConfigurationManager.AppSettings["MailAdminPassword"];
                string mailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
                int iSuccess = 0;

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(mailFrom);
                if (addressToDisplayName.Trim().Length > 0)
                {
                    mail.To.Add(new MailAddress(addressTo, addressToDisplayName));
                }
                else
                {
                    mail.To.Add(addressTo);
                }

                if (ccTo.Trim().Length > 0)
                {
                    if (ccToDisplayName.Trim().Length > 0)
                    {
                        mail.CC.Add(new MailAddress(ccTo, ccToDisplayName));
                    }
                    else
                    {
                        mail.CC.Add(ccTo);
                    }
                }
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                subject = subject.Replace("\r\n", " ");

                mail.Subject = subject;
                mail.Body = body;
                if (attachment != null)
                {
                    mail.Attachments.Add(attachment);
                }

                NotificationLogItem log;
                if (otherInfo == null || otherInfo.Trim().Length == 0)
                {
                    otherInfo = "";
                }
                else
                {
                    otherInfo = otherInfo + ";  ";
                }

                //If the the sending of email is allowed and configured in the webconfig
                string isSendingMailAllowed = System.Configuration.ConfigurationManager.AppSettings["IsSendingMailAllowed"];
                if (isSendingMailAllowed == "true")
                {
                    log = new NotificationLogItem(Guid.NewGuid(), eventId, subject, body, mailFrom, addressTo, userId, otherInfo);
                    SmtpClient smtp = new SmtpClient(host);
                    smtp.Credentials = new System.Net.NetworkCredential(userName, userPW);
                    smtp.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                    smtp.SendAsync(mail, log);
                }
                else
                {
                    log = new NotificationLogItem(Guid.NewGuid(), eventId, subject, body, mailFrom, addressTo, userId, otherInfo + "Sending email not allowed or configured in the webconfig.");
                    int iSuccess2 = DataServices.v_NotificationLog_DataServices.v_NotificationLog_Save(log.NtfcLg_Id, log.NtfcEv_Id, log.Subject, log.Content, log.AddressFrom, "", log.AddressTo, "", log.OtherInfo, log.UserId);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", ex);
                //return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SendMail", IfxTraceCategory.Leave);
            }
        }






        static bool mailSent = false;
        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SendCompletedCallback", null, IfxTraceCategory.Enter);
                // Get the unique identifier for this asynchronous operation.
                NotificationLogItem log = e.UserState as NotificationLogItem;
                if (e.Cancelled)
                {
                    // this line should not ever be hit
                    //Console.WriteLine("[{0}] Send canceled.", token);
                }
                else
                {
                    string err = "";
                    if (e.Error != null) { err = e.Error.ToString(); }
                    int iSuccess = DataServices.v_NotificationLog_DataServices.v_NotificationLog_Save(log.NtfcLg_Id, log.NtfcEv_Id, log.Subject, log.Content, log.AddressFrom, "", log.AddressTo, err, log.OtherInfo, log.UserId);

                }
                if (e.Error != null)
                {
                    string msg = "Notification email to " + log.AddressTo + " failed.  e.Error  = " + e.Error.ToString(); ;
                    Exception exx = new Exception(msg);
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SendCompletedCallback", exx);
                }
                mailSent = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SendCompletedCallback", ex);
                mailSent = false;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SendCompletedCallback", IfxTraceCategory.Leave);
            }
        }





        public class NotificationLogItem
        {

            public NotificationLogItem(Guid ntfcLg_Id, Guid ntfcEv_Id, string subject, string content, string addressFrom, string addressTo, Guid? userId, string otherInfo)
            {
                NtfcLg_Id = ntfcLg_Id;
                NtfcEv_Id = ntfcEv_Id;
                Subject = subject;
                Content = content;
                AddressFrom = addressFrom;
                AddressTo = addressTo;
                UserId = userId;
                OtherInfo = otherInfo;
            }

            Guid _ntfcLg_Id;
            public Guid NtfcLg_Id
            {
                get { return _ntfcLg_Id; }
                set { _ntfcLg_Id = value; }
            }

            Guid _ntfcEv_Id;
            public Guid NtfcEv_Id
            {
                get { return _ntfcEv_Id; }
                set { _ntfcEv_Id = value; }
            }

            string _subject;
            public string Subject
            {
                get { return _subject; }
                set { _subject = value; }
            }

            string _content;
            public string Content
            {
                get { return _content; }
                set { _content = value; }
            }

            string _addressFrom;
            public string AddressFrom
            {
                get { return _addressFrom; }
                set { _addressFrom = value; }
            }

            string _addressTo;
            public string AddressTo
            {
                get { return _addressTo; }
                set { _addressTo = value; }
            }

            Guid? _userId;
            public Guid? UserId
            {
                get { return _userId; }
                set { _userId = value; }
            }

            string _otherInfo;
            public string OtherInfo
            {
                get { return _otherInfo; }
                set { _otherInfo = value; }
            }


        }













    }
}