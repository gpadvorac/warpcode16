﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Web;
using Crypto;
using DataServices;
using Ifx;
using vCrypto;
using vReportSSRSProvider;


namespace VelocityService
{
    public class UsageLogHelper
    {



        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "UsageLogHelper";

        #endregion Initialize Variables



        public static string ObjectToString(object obj)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                new BinaryFormatter().Serialize(ms, obj);
                return Convert.ToBase64String(ms.ToArray());
            }
        }

        public static object StringToObject(string base64String)
        {
            byte[] bytes = Convert.FromBase64String(base64String);
            using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
            {
                ms.Write(bytes, 0, bytes.Length);
                ms.Position = 0;
                return new BinaryFormatter().Deserialize(ms);
            }
        }





        public static void TestEncryptDecryptObjectArray()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Enter);


                Encryptor enCrypto = new Encryptor();
                string text;
                string encryptedText;
                object[] data = CommonClientData_DataServices.Get_AuthenticationLog_lstByDateRang(DateTime.Today.AddMonths(-1), DateTime.Today);
                //text = ConvertObjectArrayToDelimitedString(data, ",", System.Environment.NewLine);
                text = ObjectToString(data);
               
                string folder = ConfigurationManager.AppSettings["UsageLogPath"];
                string date = DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss.ffff");
                string fileName = folder + "AuthenticationLog " + date + ".txt";

                //enCrypto.EncryptedTextAndWriteToFile(Database.PublicKeyValue, Database.DataToEncrypt, fileName);
                enCrypto.EncryptedTextAndWriteToFile(Database.PublicKeyValue, text, fileName);






                Decryptor deCrypto = new Decryptor();
                //string text;
                string decryptedText;
                //object[] data = CommonClientData_DataServices.Get_AuthenticationLog_lstByDateRang(DateTime.Today.AddMonths(-1), DateTime.Today);
                //text = ConvertObjectArrayToDelimitedString(data, ",", System.Environment.NewLine);
                //string folder = ConfigurationManager.AppSettings["UsageLogPath"];
                //string date = DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss.ffff");
                //string fileName = folder + "AuthenticationLog 2016-12-18 00.43.25.0945.txt";

                decryptedText = deCrypto.Decrypt_FromFilePath(Database.RsaEncryptedRandomKeyValue, Database.PrivateKey, fileName);
                object obj = StringToObject(decryptedText);
                Console.WriteLine(obj.ToString());
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {

                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Leave);
            }
        }



        public static void TestRecoverEncryptedFile()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Enter);

                Decryptor enCrypto = new Decryptor();
                string text;
                string decryptedText;
                //object[] data = CommonClientData_DataServices.Get_AuthenticationLog_lstByDateRang(DateTime.Today.AddMonths(-1), DateTime.Today);
                //text = ConvertObjectArrayToDelimitedString(data, ",", System.Environment.NewLine);
                string folder = ConfigurationManager.AppSettings["UsageLogPath"];
                //string date = DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss.ffff");
                string fileName = folder + "AuthenticationLog 2016-12-18 00.43.25.0945.txt";

                decryptedText = enCrypto.Decrypt_FromFilePath(Database.RsaEncryptedRandomKeyValue, Database.PrivateKey, fileName);
                Console.WriteLine(decryptedText);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {

                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Leave);
            }
        }


        public static void LogRecentUserAccountStatus()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Enter);

                Encryptor enCrypto = new Encryptor();
                string text;
                string encryptedText;
                object[] data = CommonClientData_DataServices.Get_AuthenticationLog_lstByDateRang(DateTime.Today.AddMonths(-1), DateTime.Today);
                text = ConvertObjectArrayToDelimitedString(data, ",", System.Environment.NewLine);
                string folder = ConfigurationManager.AppSettings["UsageLogPath"];
                string date = DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss.ffff");
                string fileName = folder + "AuthenticationLog " + date + ".txt";

                enCrypto.EncryptedTextAndWriteToFile(Database.PublicKeyValue, Database.DataToEncrypt, fileName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {

                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Leave);
            }
        }


        static string ConvertObjectArrayToDelimitedString(object[] data, string itemSep, string rowSep)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Enter);
                StringBuilder sb = new StringBuilder();
                foreach (object[] row in data)
                {
                    foreach (object item in row)
                    {
                        if (item == null)
                        {
                            sb.Append("" + itemSep);
                        }
                        else
                        {
                            sb.Append(item.ToString() + itemSep);
                        }
                    }
                    sb.Remove(sb.Length - itemSep.Length, itemSep.Length);
                    sb.Append(rowSep);
                }
                sb.Remove(sb.Length - rowSep.Length, rowSep.Length);
                return sb.ToString();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {

                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetReportAsAttachment", IfxTraceCategory.Leave);
            }
        }


    }
}