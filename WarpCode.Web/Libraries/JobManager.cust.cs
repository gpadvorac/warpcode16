﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataServices;
using EntityWireType;
using Ifx;

namespace VelocityService
{
    public partial class JobManager
    {





        public static void ProcessSignatureNotifications(Guid VmpSg_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ProcessSignatureNotifications", IfxTraceCategory.Enter);

                object[] list = VMPSignature_DataServices.GetVMP_SignatureAlert_PersonIds(VmpSg_Id);
                if (list == null)
                {
                    return;
                }
                VMP_SignatureAlert_PersonIds_Binding obj = new VMP_SignatureAlert_PersonIds_Binding((object[])list[0]);
                if (obj == null)
                {
                    return;
                }

                Guid eventId = Guid.NewGuid();
                //  hack hack hack.  Hard coded job id and title
                Guid jobId = new Guid("3e718e92-d063-42f2-9c4e-5c732f8410ff");
                int iSuccess = v_NotificationEvent_DataServices.ExecuteNotificationEvent_insert(eventId, jobId, null, "VMP Signature Signed", "", null);
                if (iSuccess == 1)
                {
                    NotificationMailHelper.SendMail(eventId, 
                        obj.MailToAddress, 
                        obj.MailToDisplayName, 
                        obj.CcToAddress, 
                        obj.CcToDisplayName, 
                        obj.Subject, 
                        obj.Content, 
                        null, 
                        null, 
                        null);
                }
                else
                {
                    throw new Exception("ExecuteNotificationEvent_insert failed.  jobId=" + jobId.ToString());
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ProcessSignatureNotifications", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ProcessSignatureNotifications", IfxTraceCategory.Leave);
            }
        }








    }
}