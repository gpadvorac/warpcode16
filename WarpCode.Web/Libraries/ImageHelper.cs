﻿using Ifx;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace VelocityService
{
    public class ImageHelper
    {
        
        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "ImageHelper";

        #endregion Initialize Variables



        public static Image CreateThumbnailFromByteArray_UsingMaxSize(byte[] array, int maxSize)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateThumbnailFromByteArray_UsingMaxSize", IfxTraceCategory.Enter);

                MemoryStream ms = new MemoryStream(array);
                ms.Seek(0, SeekOrigin.Begin);
                Bitmap bmp = new Bitmap(ms);

                double reduceBy;

                double originalWidth = bmp.Width;
                double originalHeight = bmp.Height;

                if (originalWidth > originalHeight)
                {
                    reduceBy = originalWidth / maxSize;
                }
                else
                {
                    reduceBy = originalHeight / maxSize;
                }

                double newWidth = originalWidth / reduceBy;
                double newHeight = originalHeight / reduceBy;



                Image thumb = bmp.GetThumbnailImage((int)newWidth, (int)newHeight, null, IntPtr.Zero);

               return thumb;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateThumbnailFromByteArray_UsingMaxSize", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateThumbnailFromByteArray_UsingMaxSize", IfxTraceCategory.Leave);
            }
        }




        public static byte[] GetFileAsByteArray(string path)
        {
            Guid? traceId = Guid.NewGuid();
            FileStream fs = null;

            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileAsByteArray", IfxTraceCategory.Enter);

                if (!File.Exists(path))
                {
                    IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileAsByteArray",
                        new Exception("File Not Found: " + path));
                    return null;
                }
                fs = default(FileStream);
                fs = File.Open(path, FileMode.Open, FileAccess.Read);
                long lngLen = fs.Length;
                byte[] abytBuffer = new byte[Convert.ToInt32(lngLen - 1) + 1];
                fs.Read(abytBuffer, 0, Convert.ToInt32(lngLen));
                return abytBuffer;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileAsByteArray", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileAsByteArray", IfxTraceCategory.Leave);
                if (fs != null)
                {
                    fs.Close();
                    fs = null;
                }
            }
        }









        //public static int SaveImageToFile(Image img, string path, string fileName)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveImageToFile", IfxTraceCategory.Enter);

        //        img.Save("thumb.png");


        //        return 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveImageToFile", ex);
        //        return 0;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveImageToFile", IfxTraceCategory.Leave);
        //    }
        //}






    }
}