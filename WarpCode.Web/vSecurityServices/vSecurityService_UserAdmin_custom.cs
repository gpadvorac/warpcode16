﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using vDataServices;
using Ifx;
using TypeServices;
using EntityWireType;
using System.Web.Security;

// Gen Timestamp:  5/4/2011 10:22:10 PM

namespace VelocityService
{
    public partial class vSecurityService_UserAdmin   //: ISecurityService_UserAdmin
    {




        //[OperationContract]
        //public object[] GetUserProfile_lstByAp_Id(Guid ap_Id)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserProfile_lstByAp_Id", IfxTraceCategory.Enter);
        //        return UserProfile_DataServices.GetUserProfile_lstByAp_Id(ap_Id);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserProfile_lstByAp_Id", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserProfile_lstByAp_Id", IfxTraceCategory.Leave);
        //    }
        //}




        //[OperationContract]
        //public object[] UserProfile_Save(object[] data, int check)
        //{
        //    // This is used by admin to create or edit users
        //    Guid? traceId = Guid.NewGuid();
        //    int iSuccess = 0;
        //    bool hasMembershipError = false;
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);


        //        UserProfile_Values userProfile = new UserProfile_Values(data, null);
        //        string userName = userProfile.UserName;
        //        string passWord = userProfile.Password;
        //        MembershipUser user = null;
        //        //Guid userId = userProfile.UserId;
        //        bool userExists = aspnet_Users_DataServices.aspnet_Users_VerifyUserExistsWithUserId(userProfile.UserId);
        //        // Create new User
        //        if (userExists == false)
        //        {
        //            try
        //            {
        //                user = Membership.CreateUser(userName, passWord);
        //                //userId = (Guid)user.ProviderUserKey;
        //            }
        //            catch (MembershipCreateUserException e)
        //            {
        //                iSuccess = ((int)e.StatusCode + 100) * -1;
        //                hasMembershipError = true;
        //                goto Finish;
        //            }
        //            userProfile.UserId_noevents = (Guid)user.ProviderUserKey;
        //            //string exceptionMessageXXX = "this is a test for UserProfile_Save: userProfile = : " + userProfile.ToString();
        //            //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, new Exception(exceptionMessageXXX));

        //            // User does not exist so it will be isnerted into tbPerson and v_User here.  use the Id from Membership.user
        //            //iSuccess = 1;
        //            iSuccess = aspnet_Users_DataServices.aspnet_Users_RegisterUser((Guid)user.ProviderUserKey, (Guid)userProfile.ApplicationId, userProfile.UserName, userProfile.FirstName, userProfile.LastName, userProfile.IsActiveRow);
        //            if (iSuccess == -99)
        //            {

        //                // the user has alread

        //                // I think the way to handle this is accept that the user exists in both tbPerson and aspnet_Users becuase:
        //                //  the user could have pre-existed in tbPerson due to a business process.  not the use is being regestered in aspnet, so now just syn


        //                ////// The user already exists in the Persons table.  it was created in the aspnet tables but not linked to the person table.
        //                ////// we need a better way of resolving this.
        //                hasMembershipError = true;
        //                goto Finish;
        //            }
        //            else if (iSuccess < 1)
        //            {

        //                int isDeleteSuccess = aspnet_Users_DataServices.aspnet_Users_Delete((Guid)user.ProviderUserKey);
        //                string exceptionMessage = "Attempted to execute aspnet_Users_DataServices.aspnet_Users_Delete becuase executing aspnet_Users_DataServices.aspnet_Users_RegisterUser, however, aspnet_Users_Delete failed also.  See the SQL logs for more info.";
        //                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, new Exception(exceptionMessage));
        //                goto Finish;
        //            }

        //        }
        //        else
        //        {
        //            // Get the original user name (incase the user name being passed in has been changed) so we can get a reference to the membership user.
        //            string userEmail = aspnet_Users_DataServices.GetUserEmail(userProfile.UserId);
        //            // Get reference to existing user
        //            user = Membership.GetUser(userEmail);
        //        }

        //        // Set the password question and answer
        //        if (userProfile.PasswordQuestion != null && userProfile.PasswordAnswer != null)
        //        {
        //            if (userProfile.PasswordQuestion.Trim().Length > 0 && userProfile.PasswordAnswer.Trim().Length > 0)
        //            {
        //                user.ChangePasswordQuestionAndAnswer(passWord, userProfile.PasswordQuestion, userProfile.PasswordAnswer);
        //            }
        //        }
        //        // Set the IsApproved and IsLockedOut status
        //        user.IsApproved = (bool)userProfile.IsApproved;
        //        if ((bool)userProfile.IsLockedOut == false)
        //        {
        //            user.UnlockUser();
        //        }

        //        // Change the password
        //        if (userExists == true)
        //        {
        //            string oldPassWord = user.GetPassword();
        //            if (passWord != null)
        //            {
        //                if (passWord.Trim().Length > 0)
        //                {
        //                    if (oldPassWord != passWord)
        //                    {
        //                        user.ChangePassword(oldPassWord, passWord);
        //                        int? changPasswordFlagSuccess = v_User_DataService.v_User_AdminChangedPassword_ResetUserFlag((Guid)userProfile.ApplicationId, (Guid)userProfile.UserId);
        //                        string exceptionMessage = "Updating the UserProfile was successful, but there was an error in resetting the UserMustChangePassword flag in v_User when calling v_User_DataService.AdminChangedPassword_ResetUserFlag - null was returned fromthe SP.  See the SQL logs for more info.";
        //                        if (changPasswordFlagSuccess == null)
        //                        {
        //                            exceptionMessage = "Updating the UserProfile was successful, but there was an error in resetting the UserMustChangePassword flag in v_User when calling v_User_DataService.AdminChangedPassword_ResetUserFlag - null was returned from the SP.  See the SQL logs for more info.";
        //                            if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, new Exception(exceptionMessage));
        //                        }
        //                        else if (changPasswordFlagSuccess == 0)
        //                        {
        //                            exceptionMessage = "Updating the UserProfile was successful, but there was an error in resetting the UserMustChangePassword flag in v_User when calling v_User_DataService.AdminChangedPassword_ResetUserFlag - 0 was returned from the SP.  See the SQL logs for more info.";
        //                            if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, new Exception(exceptionMessage));
        //                        }
        //                    }
        //                }

        //                //  update the user names
        //            }
        //            iSuccess = UserProfile_DataServices.UserProfile_UpdateUserNamesAndActiveStatus(userProfile.UserId, userName, userProfile.FirstName, userProfile.LastName, userProfile.IsActiveRow, userProfile.PasswordQuestion, userProfile.PasswordAnswer, (bool)userProfile.IsApproved, (bool)userProfile.IsLockedOut);
        //        }
        //    Finish:
        //        // The code below has been modified becuase we are not taking data concurrency into consideration.
        //        DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
        //        //**//  Hack:  we need to get this true or false value from the business object by using a 
        //        //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
        //        //dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
        //        dataServiceResponse.ReturnCurrentRowOnInsertUpdate = true;
        //        //  Set the DataServiceInsertUpdateResponse Result
        //        if (iSuccess > 0)
        //        {
        //            dataServiceResponse.Result = DataOperationResult.Success;
        //            //dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@Ad_Stamp"].Value));
        //        }
        //        else
        //        {
        //            //***
        //            dataServiceResponse.Exception = new ServiceException();
        //            dataServiceResponse.Exception.ExceptionCode = iSuccess;
        //            if (hasMembershipError == true)
        //            {
        //                dataServiceResponse.Result = DataOperationResult.HasMembershipException;
        //            }
        //            else
        //            {
        //                dataServiceResponse.Result = DataOperationResult.HandledException;
        //            }
        //        }
        //        if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
        //        {
        //            //  we need a flag to determin if we return new data on a successful insert/update
        //            dataServiceResponse.CurrentValues = UserProfile_DataServices.UserProfile_GetById(userProfile.UserId);
        //        }
        //        return dataServiceResponse.ReturnAsObjectArray();
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", IfxTraceCategory.Leave);
        //    }
        //}



        //[OperationContract]
        //public object[][] Getaspnet_Roles_ReadOnlyStaticLists(Guid Ap_Id)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_aspnet_Roles_ReadOnlyStaticLists", IfxTraceCategory.Enter);
        //        object[][] obj = new object[1][];

        //        obj[0] = aspnet_Roles_DataServices.GetAspnet_Roles_cmbBy_ApplicationId(Ap_Id);

        //        return obj;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_aspnet_Roles_ReadOnlyStaticLists", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_aspnet_Roles_ReadOnlyStaticLists", IfxTraceCategory.Leave);
        //    }
        //}











    }
}