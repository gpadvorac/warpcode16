using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using vDataServices;
using Ifx;

// Gen Timestamp:  5/4/2011 10:22:10 PM

namespace VelocityService
{

    public partial class vSecurityService_UserAdmin   //: ISecurityService_UserAdmin
    {

        #region Base Methods

        [OperationContract]
        public object[] UserProfile_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetById", IfxTraceCategory.Enter);
                return UserProfile_DataServices.UserProfile_GetById(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] UserProfile_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetAll", IfxTraceCategory.Enter);
                return UserProfile_DataServices.UserProfile_GetAll();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] UserProfile_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetListByFK", IfxTraceCategory.Enter);
                return UserProfile_DataServices.UserProfile_GetListByFK();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] UserProfile_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                //return UserProfile_DataServices.UserProfile_Delete(data);
                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Delete", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] UserProfile_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                return UserProfile_DataServices.UserProfile_Deactivate(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] UserProfile_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                return UserProfile_DataServices.UserProfile_Remove(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Remove", IfxTraceCategory.Leave);
            }
        }

        #endregion Base Methods


        #region Other Methods



        #endregion Other Methods

    }
}


