﻿using System;
using System.ComponentModel.Design;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web.Security;
using DataServices;
using EntityWireType;
using vDataServices;
using Ifx;
using TypeServices;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class vSecurityService_UserAdmin
    {




        [OperationContract]
        public object[] GetUserProfile_lstByAp_Id(Guid ap_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserProfile_lstByAp_Id", IfxTraceCategory.Enter);
                return UserProfile_DataServices.GetUserProfile_lstByAp_Id(ap_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserProfile_lstByAp_Id", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserProfile_lstByAp_Id", IfxTraceCategory.Leave);
            }
        }




        [OperationContract]
        public object[][] Getaspnet_Roles_ReadOnlyStaticLists(Guid Ap_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_aspnet_Roles_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[1][];

                obj[0] = aspnet_Roles_DataServices.GetAspnet_Roles_cmbBy_ApplicationId(Ap_Id);

                return obj;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_aspnet_Roles_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_aspnet_Roles_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }





        [OperationContract]
        public byte[] v_UserProfile_Save(object[] data, int check)
        {
            // This is used by admin to create or edit users
            Guid? traceId = Guid.NewGuid();
            int iSuccess = 0;
            bool hasMembershipError = false;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_Save", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // Pass in a flag that this is being set by admin
                // therefore we require a password change next time they login.
                return v_UserProfile_Save(data, check, true);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_Save", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] v_UserProfile_UserEdit_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_UserEdit_Save", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                //object[] list = v_UserProfile_UserEdit_DataServices.v_UserProfile_UserEdit_Save(data, check);

                v_UserProfile_UserEdit_Values profileFromUser = new v_UserProfile_UserEdit_Values(data, null);
                v_UserProfile_Values profile = new v_UserProfile_Values();

                profile.UserId_noevents = profileFromUser.UserId;
                profile.ApplicationId_noevents = profileFromUser.ApplicationId;
                profile.FirstName_noevents = profileFromUser.FirstName;
                profile.MiddleNameInitial_noevents = profileFromUser.MiddleNameInitial;
                profile.LastName_noevents = profileFromUser.LastName;
                profile.UserName_noevents = profileFromUser.UserName;
                profile.Password_noevents = profileFromUser.Password;
                profile.PasswordQuestion_noevents = profileFromUser.PasswordQuestion;
                profile.PasswordAnswer_noevents = profileFromUser.PasswordAnswer;
                profile.IsApproved_noevents = profileFromUser.IsApproved;
                profile.IsLockedOut_noevents = profileFromUser.IsLockedOut;
                profile.IsActiveRow_noevents = profileFromUser.IsActiveRow;

                // Pass in a flag that this is being set by a user changing his/her profile
                // therefore we dont require a password change next time they login.
                return v_UserProfile_Save(profile.GetValues(), check, false);

                //return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_UserEdit_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_UserEdit_Save", IfxTraceCategory.Leave);
            }
        }



 
        private byte[] v_UserProfile_Save(object[] data, int check, bool isSetByAdmin)
        {
            // This is used by admin to create or edit users
            Guid? traceId = Guid.NewGuid();
            int iSuccess = 0;
            bool hasMembershipError = false;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_Save", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);


                v_UserProfile_Values userProfile = new v_UserProfile_Values(data, null);
                string userName = userProfile.UserName;
                string passWord = userProfile.Password;
                MembershipUser user = null;
                //Guid userId = userProfile.UserId;
                bool userExists = aspnet_Users_DataServices.aspnet_Users_VerifyUserExistsWithUserId(userProfile.UserId);
                // Create new User
                if (userExists == false)
                {
                    try
                    {
                        user = Membership.CreateUser(userName, passWord);
                        //userId = (Guid)user.ProviderUserKey;
                    }
                    catch (MembershipCreateUserException e)
                    {
                        iSuccess = ((int)e.StatusCode + 100) * -1;
                        hasMembershipError = true;
                        goto Finish;
                    }
                    userProfile.UserId_noevents = (Guid)user.ProviderUserKey;
                    //string exceptionMessageXXX = "this is a test for UserProfile_Save: userProfile = : " + userProfile.ToString();
                    //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, new Exception(exceptionMessageXXX));

                    // User does not exist so it will be isnerted into tbPerson and v_User here.  use the Id from Membership.user
                    //iSuccess = 1;
                    iSuccess = aspnet_Users_DataServices.aspnet_Users_RegisterUser((Guid)user.ProviderUserKey, (Guid)userProfile.ApplicationId, userProfile.UserName, userProfile.FirstName, userProfile.LastName, userProfile.IsActiveRow);
                    if (iSuccess == -99)
                    {

                        // the user has alread

                        // I think the way to handle this is accept that the user exists in both tbPerson and aspnet_Users becuase:
                        //  the user could have pre-existed in tbPerson due to a business process.  not the use is being regestered in aspnet, so now just syn


                        ////// The user already exists in the Persons table.  it was created in the aspnet tables but not linked to the person table.
                        ////// we need a better way of resolving this.
                        hasMembershipError = true;
                        goto Finish;
                    }
                    else if (iSuccess < 1)
                    {

                        int isDeleteSuccess = aspnet_Users_DataServices.aspnet_Users_Delete((Guid)user.ProviderUserKey);
                        string exceptionMessage = "Attempted to execute aspnet_Users_DataServices.aspnet_Users_Delete becuase executing aspnet_Users_DataServices.aspnet_Users_RegisterUser, however, aspnet_Users_Delete failed also.  See the SQL logs for more info.";
                        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, new Exception(exceptionMessage));
                        goto Finish;
                    }

                }
                else
                {
                    // Get the original user name (incase the user name being passed in has been changed) so we can get a reference to the membership user.
                    string userEmail = aspnet_Users_DataServices.GetUserEmail(userProfile.UserId);
                    // Get reference to existing user
                    user = Membership.GetUser(userEmail);
                }

                // Set the password question and answer
                if (userProfile.PasswordQuestion != null && userProfile.PasswordAnswer != null)
                {
                    if (userProfile.PasswordQuestion.Trim().Length > 0 && userProfile.PasswordAnswer.Trim().Length > 0)
                    {
                        user.ChangePasswordQuestionAndAnswer(user.GetPassword(), userProfile.PasswordQuestion, userProfile.PasswordAnswer);
                    }
                }
                // Set the IsApproved and IsLockedOut status
                user.IsApproved = (bool)userProfile.IsApproved;
                if ((bool)userProfile.IsLockedOut == false)
                {
                    user.UnlockUser();
                }

                // Change the password
                if (userExists == true)
                {
                    string oldPassWord = user.GetPassword();
                    if (passWord != null)
                    {
                        if (passWord.Trim().Length > 0)
                        {
                            if (oldPassWord != passWord)
                            {
                                user.ChangePassword(oldPassWord, passWord);

                                if (isSetByAdmin == true)
                                {
                                    int? changPasswordFlagSuccess = v_User_DataService.v_User_AdminChangedPassword_ResetUserFlag((Guid)userProfile.ApplicationId, (Guid)userProfile.UserId);
                                    string exceptionMessage = "Updating the UserProfile was successful, but there was an error in resetting the UserMustChangePassword flag in v_User when calling v_User_DataService.AdminChangedPassword_ResetUserFlag - null was returned fromthe SP.  See the SQL logs for more info.";
                                    if (changPasswordFlagSuccess == null)
                                    {
                                        exceptionMessage = "Updating the UserProfile was successful, but there was an error in resetting the UserMustChangePassword flag in v_User when calling v_User_DataService.AdminChangedPassword_ResetUserFlag - null was returned from the SP.  See the SQL logs for more info.";
                                        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, new Exception(exceptionMessage));
                                    }
                                    else if (changPasswordFlagSuccess == 0)
                                    {
                                        exceptionMessage = "Updating the UserProfile was successful, but there was an error in resetting the UserMustChangePassword flag in v_User when calling v_User_DataService.AdminChangedPassword_ResetUserFlag - 0 was returned from the SP.  See the SQL logs for more info.";
                                        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserProfile_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, null, new Exception(exceptionMessage));
                                    }
                                }
                                
                            }
                        }

                        //  update the user names
                    }
                    iSuccess = UserProfile_DataServices.UserProfile_UpdateUserNamesAndActiveStatus(userProfile.UserId, userName, userProfile.FirstName, userProfile.MiddleNameInitial, userProfile.LastName, userProfile.IsActiveRow, userProfile.PasswordQuestion, userProfile.PasswordAnswer, (bool)userProfile.IsApproved, (bool)userProfile.IsLockedOut);
                }
            Finish:
                // The code below has been modified becuase we are not taking data concurrency into consideration.
                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                //**//  Hack:  we need to get this true or false value from the business object by using a 
                //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
                //dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
                dataServiceResponse.ReturnCurrentRowOnInsertUpdate = true;
                //  Set the DataServiceInsertUpdateResponse Result
                if (iSuccess > 0)
                {
                    dataServiceResponse.Result = DataOperationResult.Success;
                    //dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@Ad_Stamp"].Value));
                }
                else
                {
                    //***
                    dataServiceResponse.Exception = new ServiceException();
                    dataServiceResponse.Exception.ExceptionCode = iSuccess;
                    if (hasMembershipError == true)
                    {
                        dataServiceResponse.Result = DataOperationResult.HasMembershipException;
                    }
                    else
                    {
                        dataServiceResponse.Result = DataOperationResult.HandledException;
                    }
                }
                if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
                {
                    //  we need a flag to determin if we return new data on a successful insert/update
                    dataServiceResponse.CurrentValues = UserProfile_DataServices.UserProfile_GetById(userProfile.UserId);
                }
                object[] list = dataServiceResponse.ReturnAsObjectArray();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_Save", IfxTraceCategory.Leave);
            }
        }






        [OperationContract]
        public object[] v_UserProfile_GetQuestionByUserName(string userName, Guid app_Id)
        {
            // This is used by admin to create or edit users
            Guid? traceId = Guid.NewGuid();
            int iSuccess = 0;
            bool hasMembershipError = false;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_Save", new ValuePair[] { new ValuePair("userName", userName), new ValuePair("app_Id", app_Id) }, IfxTraceCategory.Enter);

                return v_UserProfile_UserEdit_DataServices.v_UserProfile_GetQuestionByUserName(userName, app_Id);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_Save", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] Executev_UserProfile_ForgotPassword_ResetToTemporaryPassword(string userName, string passWordAnser, Guid app_Id)
        {
            Guid? traceId = Guid.NewGuid();
            int? iSuccess = 0;
            Guid? userId = null;
            bool hasMembershipError = false;
            object[] returnData = new object[1];
            string msg;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UserProfile_ForgotPassword_ResetToTemporaryPassword", new ValuePair[] { new ValuePair("userName", userName), new ValuePair("passWordAnser", passWordAnser), new ValuePair("app_Id", app_Id) }, IfxTraceCategory.Enter);

                // Rename to:  ForgotPassword_Reset
                // 1) Verify Username and Security Question
                // 2) If verified, create temp password
                // 3) If creation succeeded, 
                //      - set MustchangePW flag
                //      - send emial
                // 4) return flag stating the status of this operation

                object[] retult = v_UserProfile_UserEdit_DataServices.Executev_UserProfile_VerifyUserNameAndSecurityAnswer(userName, passWordAnser, app_Id);
                if (retult == null || retult[0] == null || retult[1] == null)
                {
                    returnData[0] = 0;
                    return returnData;
                }

                iSuccess = retult[0] as int?;
                userId = retult[1] as Guid?;
                if (iSuccess != null && iSuccess == 1 && userId != null)
                {
                    MembershipUser user = Membership.GetUser(userId);
                    if (user == null)
                    {
                        // Membership could not return a user for some reason.
                        msg = "User Id was found from 'Executev_UserProfile_VerifyUserNameAndSecurityAnswer' but could not get a user from:  MembershipUser user = Membership.GetUser(" + userId.ToString() + ");";
                        Exception XX = new Exception(msg);
                        if (IfxTrace._traceCatch)
                            IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UserProfile_ForgotPassword_ResetToTemporaryPassword",
                                new ValuePair[] {new ValuePair("userName", userName), new ValuePair("passWordAnser", passWordAnser), new ValuePair("app_Id", app_Id)}, null,
                                XX);
                        returnData[0] = 0;
                        return returnData;
                    }

                    // Attempt to reset a temp password
                    string newPW = user.ResetPassword(passWordAnser);
                    if (newPW == null)
                    {
                        // Membership could not return a user for some reason.
                        msg = "Failed to reset the passwored.  'string newPW = user.ResetPassword(passWordAnser)' returned null.";
                        Exception XX = new Exception(msg);
                        if (IfxTrace._traceCatch)
                            IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UserProfile_ForgotPassword_ResetToTemporaryPassword",
                                new ValuePair[] {new ValuePair("userName", userName), new ValuePair("passWordAnser", passWordAnser), new ValuePair("app_Id", app_Id)}, null,
                                XX);
                        returnData[0] = 0;
                        return returnData;
                    }
                    else
                    {
                        // We're not going to do anything with "mustChangePwSuccess" below.  Assume it succeeded.
                        int? mustChangePwSuccess = v_User_DataService.v_User_AdminChangedPassword_ResetUserFlag((Guid)app_Id, (Guid)userId);
                        string appName = System.Configuration.ConfigurationManager.AppSettings["AppName"];

                        msg = "This is your temporary password." + System.Environment.NewLine + newPW + System.Environment.NewLine +
                              "Please login using your Username and this Password, then enter a new password when prompted.";
                        MailHelper.SendMail(userName, appName + " Password Recovery", msg, null);
                        returnData[0] = 1;
                        return returnData;
                    }
                }
                else
                {
                    string stringSuccess = "null";
                    if (iSuccess != null)
                    {
                        stringSuccess = iSuccess.ToString();
                    }
                    msg = "Failed to reset the passwored. Success result from spv_UserProfile_VerifyUserNameAndSecurityAnswer was:  Success = " + stringSuccess;
                    Exception XX = new Exception(msg);
                    if (IfxTrace._traceCatch)
                        IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UserProfile_ForgotPassword_ResetToTemporaryPassword",
                            new ValuePair[] { new ValuePair("userName", userName), new ValuePair("passWordAnser", passWordAnser), new ValuePair("app_Id", app_Id) }, null,
                            XX);
                    returnData[0] = iSuccess;
                    return returnData;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UserProfile_ForgotPassword_ResetToTemporaryPassword", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_UserProfile_ForgotPassword_ResetToTemporaryPassword", IfxTraceCategory.Leave);
            }
        }




        /// <summary>
        ///  This was in "vSecurityService_Authentication" but svcutil.exe was not creating the prox methods for it so now we have to access it from here.
        ///     Originaly it was named "v_User_ResetPassword_SecurityQuestionUpdate".
        /// </summary>
        /// <param name="aspnetUser_Id"></param>
        /// <param name="oldPassWord"></param>
        /// <param name="newPassWord"></param>
        /// <param name="question"></param>
        /// <param name="answer"></param>
        /// <param name="actionType"></param>
        /// <returns></returns>
        [OperationContract]
        public int v_UserProfile_ResetPassword_SecurityQuestionUpdate(Guid aspnetUser_Id, string oldPassWord, string newPassWord, string question, string answer, int actionType)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_ResetPassword_SecurityQuestionUpdate", IfxTraceCategory.Enter);
                Int32? iSuccess = 0;
                string msg;
                MembershipUser user = Membership.GetUser(aspnetUser_Id);
                if (user == null)
                {
                    msg = "The parameter aspnetUser_Id value was not found in the Membership table which should not happen since this was called from PasswordReset.";
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_ResetPassword_SecurityQuestionUpdate", new ValuePair[] { new ValuePair("aspnetUser_Id", aspnetUser_Id.ToString()) }, null, new Exception("VelocityService.vSecurityService_Authentication.aspnet_Authenticate() Error -123: " + msg));
                    return -123;
                }

                bool successPwChange;
                int successQaUpdate;
                if ((TypeServices.MembershipCreateStatus)actionType == TypeServices.MembershipCreateStatus.PasswordChangeRequired || (TypeServices.MembershipCreateStatus)actionType == TypeServices.MembershipCreateStatus.PasswordChangeRequiredSecurityQaRequired)
                {
                    successPwChange = user.ChangePassword(oldPassWord, newPassWord);
                    if (successPwChange == true)
                    {
                        // Set the flag 'Must Change Password' in v_User back to false becuase the user just changed the password.
                        iSuccess = v_User_DataService.v_User_Set_Flag_MustChangePassword_ToFalse(aspnetUser_Id);
                        if (iSuccess == null)
                        {
                            msg = "The password was changed, but there was an error in reseting the flag.";
                            if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_ResetPassword_SecurityQuestionUpdate", new ValuePair[] { new ValuePair("aspnetUser_Id", aspnetUser_Id.ToString()) }, null, new Exception("VelocityService.vSecurityService_Authentication.aspnet_Authenticate() Error -123: " + msg));
                            return -124;
                        }
                        else if (iSuccess == 0)
                        {
                            msg = "The password was changed, but there was an error in reseting the flag.";
                            if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_ResetPassword_SecurityQuestionUpdate", new ValuePair[] { new ValuePair("aspnetUser_Id", aspnetUser_Id.ToString()) }, null, new Exception("VelocityService.vSecurityService_Authentication.aspnet_Authenticate() Error -123: " + msg));
                            return -124;
                        }
                    }
                }
                if ((TypeServices.MembershipCreateStatus)actionType == TypeServices.MembershipCreateStatus.SecurityQaRequired || (TypeServices.MembershipCreateStatus)actionType == TypeServices.MembershipCreateStatus.PasswordChangeRequiredSecurityQaRequired)
                {
                    successQaUpdate = UserProfile_DataServices.UserProfile_updateQuestionAnswer(aspnetUser_Id, question, answer);
                    if (successQaUpdate != 1)
                    {
                        msg = "Unable to update the sucurity question and answer.";
                        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_ResetPassword_SecurityQuestionUpdate", new ValuePair[] { new ValuePair("aspnetUser_Id", aspnetUser_Id.ToString()), new ValuePair("question", aspnetUser_Id.ToString()), new ValuePair("answer", aspnetUser_Id.ToString()) }, null, new Exception("VelocityService.vSecurityService_Authentication.aspnet_Authenticate() Error -123: " + msg));
                        return 0;
                    }
                }
                return 1;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_ResetPassword_SecurityQuestionUpdate", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UserProfile_ResetPassword_SecurityQuestionUpdate", IfxTraceCategory.Leave);
            }
        }








    }
}
