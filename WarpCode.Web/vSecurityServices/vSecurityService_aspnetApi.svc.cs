﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using Ifx;
using System.ServiceModel.Activation;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class vSecurityService_aspnetApi
    {


        #region Initialize Variables

        private static string _as = "VelocityPrototype.Web";
        private static string _cn = "vSecurityService_aspnetApi";

        #endregion Initialize Variables


        #region User Management

        [OperationContract]
        public object[] GetUserList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserList", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.GetUserList() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserList", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] GetUserEmail(Guid aspNetId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserEmail", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.GetUserEmail() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserEmail", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserEmail", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] GetUserName(Guid aspNetId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserName", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.GetUserName() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserName", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserName", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] UpdateUserEmail(Guid aspNetId, string email)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UpdateUserEmail", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.UpdateUserEmail() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UpdateUserEmail", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UpdateUserEmail", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] AddUser(Guid aspNetId, bool isGlobalAdmin, string email)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AddUser", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.AddUser() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AddUser", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AddUser", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] RemoveUser(Guid aspNetId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RemoveUser", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.RemoveUser() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RemoveUser", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RemoveUser", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] UnlockUser(Guid aspNetId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnlockUser", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.UnlockUser() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnlockUser", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnlockUser", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] RecoverUserPassword(string userName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RecoverUserPassword", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.RecoverUserPassword() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RecoverUserPassword", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RecoverUserPassword", IfxTraceCategory.Leave);
            }
        }

        //[OperationContract]
        //public object[] RecoverUserPassword_byUserSecurityId(Guid aspNetId)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RecoverUserPassword", IfxTraceCategory.Enter);
        //        throw new Exception("VelocityService.vSecurityService_aspnetApi.RecoverUserPassword() is not implemented yet.");
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RecoverUserPassword", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RecoverUserPassword", IfxTraceCategory.Leave);
        //    }
        //}

        [OperationContract]
        public object[] VerifyUserExists(string email)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "VerifyUserExists", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.VerifyUserExists() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "VerifyUserExists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "VerifyUserExists", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] GetUserIdFromUserName(string email)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserIdFromUserName", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.GetUserIdFromUserName() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserIdFromUserName", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserIdFromUserName", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] GetUser(Guid aspnetId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUser", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.GetUser() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUser", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUser", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        //public object[] UpdateUser(User user, Guid aspnetId)
        public object[] UpdateUser(object[] user, Guid aspnetId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UpdateUser", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.UpdateUser() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UpdateUser", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UpdateUser", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] GetUserIdFromAspNetUserId(Guid aspnetId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserIdFromAspNetUserId", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.GetUserIdFromAspNetUserId() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserIdFromAspNetUserId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserIdFromAspNetUserId", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] GetUserProfile(Guid aspnetId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserProfile", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.GetUserProfile() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserProfile", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserProfile", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] GetUserId()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserId", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.GetUserId() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserId", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        //public object[] CreateUser(User user, Guid memberId)
        public object[] CreateUser(object[] user, Guid memberId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateUser", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.CreateUser() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateUser", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CreateUser", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] GetMemberList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetMemberList", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.GetMemberList() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetMemberList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetMemberList", IfxTraceCategory.Leave);
            }
        }

        #endregion User Management


        #region Role Management



        [OperationContract]
        public object[] GetUserRoles(Guid aspnetId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserRoles", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.GetUserRoles() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserRoles", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserRoles", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] GetAllRoles()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAllRoles", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.GetAllRoles() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAllRoles", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAllRoles", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        //public object[] UpdateUserRoles(Guid id, List<Guid> ids)
        public object[] UpdateUserRoles(Guid id, object[] ids)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UpdateUserRoles", IfxTraceCategory.Enter);
                throw new Exception("VelocityService.vSecurityService_aspnetApi.UpdateUserRoles() is not implemented yet.");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UpdateUserRoles", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UpdateUserRoles", IfxTraceCategory.Leave);
            }
        }

        #endregion Role Management





    }
}
