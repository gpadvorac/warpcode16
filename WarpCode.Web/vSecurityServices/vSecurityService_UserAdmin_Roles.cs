﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using vDataServices;
using Ifx;

// Gen Timestamp:  5/4/2011 10:22:10 PM

namespace VelocityService
{

    public partial class vSecurityService_UserAdmin
    {


        [OperationContract]
        public object[] aspnet_Roles_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_GetById", IfxTraceCategory.Enter);
                return aspnet_Roles_DataServices.aspnet_Roles_GetById(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_GetById", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] aspnet_Roles_GetListByApplicatoinId(Guid ap_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_GetListByApplicatoinId", IfxTraceCategory.Enter);
                return aspnet_Roles_DataServices.aspnet_Roles_GetListByApplicatoinId(ap_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_GetListByApplicatoinId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_GetListByApplicatoinId", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] aspnet_Roles_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_Save", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                return aspnet_Roles_DataServices.aspnet_Roles_Save(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_Save", IfxTraceCategory.Leave);
            }
        }

        #region Roles to user

        //[OperationContract]
        //public object[] GetAspnet_Roles_cmbBy_UserId(Guid UserId)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_Roles_cmbBy_UserId", IfxTraceCategory.Enter);
        //        return aspnet_Roles_DataServices.GetAspnet_Roles_cmbBy_UserId(UserId);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_Roles_cmbBy_UserId", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_Roles_cmbBy_UserId", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public object[] GetAspnet_Roles_cmbNotAssignedToUser(Guid ap_Id, Guid userId)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_Roles_cmbNotAssignedToUser", IfxTraceCategory.Enter);
        //        return aspnet_Roles_DataServices.GetAspnet_Roles_cmbNotAssignedToUser(ap_Id, userId);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_Roles_cmbNotAssignedToUser", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_Roles_cmbNotAssignedToUser", IfxTraceCategory.Leave);
        //    }
        //}


        [OperationContract]
        public object[] GetAspnet_UserNames_All(Guid ap_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_UserNames_All", IfxTraceCategory.Enter);
                return aspnet_Roles_DataServices.GetAspnet_UserNames_All(ap_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_UserNames_All", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_UserNames_All", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public int aspnet_Roles_AssignRolesToUser(Guid UserId, string roles)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_Roles_cmbBy_UserId", IfxTraceCategory.Enter);
                return aspnet_Roles_DataServices.aspnet_Roles_AssignRolesToUser(UserId, roles);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_Roles_cmbBy_UserId", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_Roles_cmbBy_UserId", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public int aspnet_Roles_RemoveRolesFromUser(Guid UserId, string roles)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_RemoveRolesFromUser", IfxTraceCategory.Enter);
                return aspnet_Roles_DataServices.aspnet_Roles_RemoveRolesFromUser(UserId, roles);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_RemoveRolesFromUser", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_RemoveRolesFromUser", IfxTraceCategory.Leave);
            }
        }




        [OperationContract]
        public object[] GetAspnet_RoleNames_ByUser_AssignedToUser(Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_RoleNames_ByUser_AssignedToUser", IfxTraceCategory.Enter);
                return aspnet_Roles_DataServices.GetAspnet_RoleNames_ByUser_AssignedToUser(userId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_RoleNames_ByUser_AssignedToUser", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_RoleNames_ByUser_AssignedToUser", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] GetAspnet_RoleNames_ByUser_NotAssignedToUser(Guid ap_Id, Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_RoleNames_ByUser_NotAssignedToUser", IfxTraceCategory.Enter);
                return aspnet_Roles_DataServices.GetAspnet_RoleNames_ByUser_NotAssignedToUser(ap_Id, userId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_RoleNames_ByUser_NotAssignedToUser", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetAspnet_RoleNames_ByUser_NotAssignedToUser", IfxTraceCategory.Leave);
            }
        }






        #endregion Roles to user


        #region Users to role


        [OperationContract]
        public object[] GetUserNames_ByRole_NotAssignedToRole(Guid ap_Id, Guid roleId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserNames_ByRole_NotAssignedToRole", IfxTraceCategory.Enter);
                return aspnet_Roles_DataServices.GetUserNames_ByRole_NotAssignedToRole(ap_Id, roleId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserNames_ByRole_NotAssignedToRole", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserNames_ByRole_NotAssignedToRole", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] GetUserNames_ByRole_AssignedToRole(Guid roleId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserNames_ByRole_AssignedToRole", IfxTraceCategory.Enter);
                return aspnet_Roles_DataServices.GetUserNames_ByRole_AssignedToRole(roleId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserNames_ByRole_AssignedToRole", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUserNames_ByRole_AssignedToRole", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public int aspnet_Roles_AssignUsersToRole(Guid roleId, string users)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_AssignUsersToRole", IfxTraceCategory.Enter);
                return aspnet_Roles_DataServices.aspnet_Roles_AssignUsersToRole(roleId, users);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_AssignUsersToRole", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_AssignUsersToRole", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public int aspnet_Roles_RemoveUsersFromRole(Guid roleId, string users)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_RemoveUsersFromRole", IfxTraceCategory.Enter);
                return aspnet_Roles_DataServices.aspnet_Roles_RemoveUsersFromRole(roleId, users);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_RemoveUsersFromRole", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Roles_RemoveUsersFromRole", IfxTraceCategory.Leave);
            }
        }




        #endregion Users to role




    }
}