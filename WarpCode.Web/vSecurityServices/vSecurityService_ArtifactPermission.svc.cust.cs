﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Ifx;
using vDataServices;

namespace VelocityService
{
    public partial class vSecurityService_ArtifactPermission   //: IvSecurityService_ArtifactPermission
    {



        [OperationContract]
        public object[][] Getv_UiArtifact_ReadOnlyStaticLists(Guid ap_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_UiArtifact_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[3][];

                obj[0] = v_UiArtifact_DataServices.Getv_UiArtifact_cmbAncestors_ByAp_Id(ap_Id);
                obj[1] = v_UiArtifact_DataServices.Getv_UiArtifactPlatform_cmb();
                obj[2] = v_UiArtifact_DataServices.Getv_UiArtifactType_cmbAll();

                return obj;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_UiArtifact_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_UiArtifact_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] GetUiArtifactPermission_lstBy_Artifact_Id(Guid Artifact_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactPermission_lstBy_Artifact_Id", IfxTraceCategory.Enter);
                return v_UiArtifactPermission_DataServices.GetUiArtifactPermission_lstBy_Artifact_Id(Artifact_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactPermission_lstBy_Artifact_Id", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactPermission_lstBy_Artifact_Id", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] v_UiArtifactPermission_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_GetById", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return v_UiArtifactPermission_DataServices.v_UiArtifactPermission_GetById(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] v_UiArtifactPermission_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_Save", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                return v_UiArtifactPermission_DataServices.v_UiArtifactPermission_Save(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public int v_UiArtifactPermission_Delete(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_Delete", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return v_UiArtifactPermission_DataServices.v_UiArtifactPermission_Delete(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_Delete", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifactPermission_Delete", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public int v_UiArtifact_Delete(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Delete", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.v_UiArtifact_Delete(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Delete", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Delete", IfxTraceCategory.Leave);
            }
        }

        ////////////////////////////////


        #region For Security Explorer

        // not being used anymore.  should delete this
        [OperationContract]
        public object[] GetSecurity_RootNodes(Guid Ap_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSecurity_RootNodes", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.GetSecurity_RootNodes(Ap_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSecurity_RootNodes", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSecurity_RootNodes", IfxTraceCategory.Leave);
            }
        }
        // not being used anymore.  should delete this
        [OperationContract]
        public object[] GetSecurity_GetChildNodeGroups(String ParentType, Guid ParentId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSecurity_GetChildNodeGroups", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.GetSecurity_GetChildNodeGroups(ParentType, ParentId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSecurity_GetChildNodeGroups", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSecurity_GetChildNodeGroups", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] GetUiArtifact_cmbNameAndPaths_RootNodes_By_Ap_Id(Guid Ap_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_cmbNameAndPaths_RootNodes_By_Ap_Id", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.GetUiArtifact_cmbNameAndPaths_RootNodes_By_Ap_Id(Ap_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_cmbNameAndPaths_RootNodes_By_Ap_Id", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_cmbNameAndPaths_RootNodes_By_Ap_Id", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] GetUiArtifact_cmbNameAndPaths_By_Ap_Id(Guid Ap_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_cmbNameAndPaths_By_Ap_Id", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.GetUiArtifact_cmbNameAndPaths_By_Ap_Id(Ap_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_cmbNameAndPaths_By_Ap_Id", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_cmbNameAndPaths_By_Ap_Id", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] GetUiArtifact_cmbNameAndPaths_By_ParentId(Guid ParentId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_cmbNameAndPaths_By_ParentId", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.GetUiArtifact_cmbNameAndPaths_By_ParentId(ParentId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_cmbNameAndPaths_By_ParentId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_cmbNameAndPaths_By_ParentId", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] GetUiArtifact_cmbNameAndPaths_Ancestors_By_Ap_Id(Guid Ap_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_cmbNameAndPaths_Ancestors_By_Ap_Id", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.GetUiArtifact_cmbNameAndPaths_Ancestors_By_Ap_Id(Ap_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_cmbNameAndPaths_Ancestors_By_Ap_Id", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_cmbNameAndPaths_Ancestors_By_Ap_Id", IfxTraceCategory.Leave);
            }
        }

        #endregion For Security Explorer



        #region Artifact Grid


        [OperationContract]
        public object[] GetUiArtifact_lstRootArtifacts(Guid Ap_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_lstRootArtifacts", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.GetUiArtifact_lstRootArtifacts(Ap_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_lstRootArtifacts", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_lstRootArtifacts", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] GetUiArtifact_lstByParent_Id(Guid Parent_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_lstByParent_Id", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.GetUiArtifact_lstByParent_Id(Parent_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_lstByParent_Id", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifact_lstByParent_Id", IfxTraceCategory.Leave);
            }
        }








        #endregion Artifact Grid





    }
}