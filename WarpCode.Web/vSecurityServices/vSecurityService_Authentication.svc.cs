﻿using System;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using Ifx;
using vDataServices;
using EntityWireType;
using System.Web.Security;
using DataServices;
using TypeServices;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class vSecurityService_Authentication
    {


        #region Initialize Variables

        private static string _as = "VelocityPrototype.Web";
        private static string _cn = "vSecurityService_Authentication";

        #endregion Initialize Variables




        #region Login,  Register and Edit



        [OperationContract]
        public object[] aspnet_Authenticate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Authenticate", IfxTraceCategory.Enter);
                string msg;
                Guid ap_Id = (Guid)data[0];
                Guid? userId = null;
                Guid? altId = null;
                Guid? pnId = null;
                string userName = (string)data[1];
                string pw = (string)data[2];
                int iSuccess = -2;
                //  1st element is the result code, 
                //  2nd element is the aspnet user id
                //  3rd element is the alternate User Id
                //  4th element is the token
                //  5th element is the Person Id
                object[] result = new object[5];
                //MembershipUser user = Membership.Provider.


                // fix password    -     Use this 
                //MembershipUser user = null;  // Membership.GetUser("georgep@nwis.net");
                //user = Membership.CreateUser(userName, pw);
                //string psw = user.GetPassword();
                // end fix password


                if (Membership.ValidateUser(userName, pw))
                {
                    userId = aspnet_Users_DataServices.GetUserIdFromUserName(ap_Id, userName);
                    if (userId == null)
                    {
                        iSuccess = -99;  // our own MembershipCreateStatus.UserNotFound:     "System error - User not found.";
                        Ifx_DataServices.Executeix_AuthenticationLog_putInsert(userId, userName, GetIPAddress(), GetIPAddress2(),
                            "- 99; User was authentiated, but user name was not found in aspnet_User_GetIdFromUsernameAndApId.",
                            false);
                        result[1] = null;
                    }
                    else
                    {
                        iSuccess = 1;
                        //iSuccess = 1;
                        Ifx_DataServices.Executeix_AuthenticationLog_putInsert(userId, userName, GetIPAddress(), GetIPAddress2(),
                            "1; Authenticatoin was successful.",
                            true);
                        result[1] = userId;
                    }
                }
                else
                {

                    iSuccess = -90;  // our own MembershipCreateStatus.UserNameOrPasswordNotFound: 
                    Ifx_DataServices.Executeix_AuthenticationLog_putInsert(userId, userName, GetIPAddress(), GetIPAddress2(),
                        "-90; User name or password not found.",
                        false);
                    result[1] = null;
                }

                if (iSuccess != 1)
                {
                    // Authenticatoin was not successful, so return the result now.
                    // I dont think this should ever get hit.
                    Ifx_DataServices.Executeix_AuthenticationLog_putInsert(userId, userName, GetIPAddress(), GetIPAddress2(),
                        "<>1; Authenticatoin was not successful.",
                        false);
                    result[0] = iSuccess;
                    //return result;
                }
                else
                {
                    iSuccess = aspnet_Users_DataServices.Person_IsUserActive((Guid)userId);
                    if (iSuccess == 1)
                    {
                        ////iSuccess = 1;
                        //Ifx_DataServices.Executeix_AuthenticationLog_putInsert(null, userName, GetIPAddress(), GetIPAddress2(),
                        //    "1; Authenticatoin was successful.",
                        //    true);
                        result[0] = iSuccess;
                        altId = aspnet_Users_DataServices.GetUser2AlternateId_AltId((Guid)userId);

                        if (altId == null)
                        {
                            IfxEvent.PublishTrace(traceId, _as, _cn, "GetUser2AlternateId_AltId", new Exception("Alternate User Id not found for " + userName + " and User Id " + userId.ToString()));
                        }
                        result[2] = altId;


                        pnId = aspnet_Users_DataServices.GetPersonIdFromUserId((Guid)userId);
                        if (pnId == null)
                        {
                            IfxEvent.PublishTrace(traceId, _as, _cn, "GetPersonIdFromUserId", new Exception("Person Id not found for " + userName + " and User Id " + userId.ToString()));
                        }
                        result[4] = pnId;

                        //return result;
                    }
                    else
                    {
                        if (iSuccess == 0)
                        {
                            //      The user was authenticated, but the user is not active.
                            result[0] = -120;
                            Ifx_DataServices.Executeix_AuthenticationLog_putInsert(userId, userName, GetIPAddress(), GetIPAddress2(),
                                "-120; The user was authenticated, but the user is not active.",
                                false);
                            //return result;
                        }
                        else
                        {
                            //      The user was authenticated, but the Id fetched from the aspnetUser table was not found in tbPerson table 
                            //       where we are looking for the user active status.
                            result[0] = -121;
                            Ifx_DataServices.Executeix_AuthenticationLog_putInsert(userId, userName, GetIPAddress(), GetIPAddress2(),
                                "-121; The user was authenticated, but the Id fetched from the aspnetUser table was not found in tbPerson table.",
                                false);


                            if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Authenticate", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, new ValuePair[] { new ValuePair("userId", userId) }, new Exception("VelocityService.vSecurityService_Authentication.aspnet_Authenticate() Error -121: The user was authenticated, but the Id fetched from the aspnetUser table was not found."));
                            //return result;
                        }
                    }
                    if (iSuccess == 1)
                    {
                        //  All aspects of the login process have been successful.  
                        //  The last thing to check is if the user must change the password.
                        //  If changing the password is required, adding a security question and answer may also be required.  Check for that too.
                        bool? mustChangePassword = v_User_DataService.v_User_GetFlag_UserMustChangePassword((Guid)userId);
                        bool? mustCreateSecurityQuestionAnswer = v_User_DataService.aspnet_Membership_VerifyUserHasQuestionAnswer((Guid)userId);

                        if (mustChangePassword == null)
                        {
                            result[0] = -122;
                            if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Authenticate", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, new ValuePair[] { new ValuePair("userId", userId) }, new Exception("VelocityService.vSecurityService_Authentication.aspnet_Authenticate() Error -122: The user was authenticated, but there was an error when checking to see if the password must be changed."));
                            Ifx_DataServices.Executeix_AuthenticationLog_putInsert(userId, userName, GetIPAddress(), GetIPAddress2(),
                                "-122; The user was authenticated, but there was an error when checking to see if the password must be changed.",
                                false);
                        }
                        else if (mustCreateSecurityQuestionAnswer == null)
                        {
                            result[0] = -125;
                            if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Authenticate", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, new ValuePair[] { new ValuePair("userId", userId) }, new Exception("VelocityService.vSecurityService_Authentication.aspnet_Authenticate() Error -122: The user was authenticated, but there was an error when checking to see if the password must be changed."));
                            Ifx_DataServices.Executeix_AuthenticationLog_putInsert(userId, userName, GetIPAddress(), GetIPAddress2(),
                                "-125; The user was authenticated, but there was an error when checking to see if the the user has a security question and answer.   If not, the user will be prompted to enter a security question and answer before gaining access to the application.",
                                false);
                        }
                        else if (mustChangePassword == true && mustCreateSecurityQuestionAnswer == false)
                        {
                            result[0] = 2;
                            msg = "result = 2; The user was authenticated and the password must be changed.";
                            Ifx_DataServices.Executeix_AuthenticationLog_putInsert(userId, userName, GetIPAddress(), GetIPAddress2(), msg, true);
                        }
                        else if (mustChangePassword == false && mustCreateSecurityQuestionAnswer == true)
                        {
                            result[0] = 3;
                            msg = "result = 3; The user was authenticated and must enter the security question and answer before accessing the application.";
                            Ifx_DataServices.Executeix_AuthenticationLog_putInsert(userId, userName, GetIPAddress(), GetIPAddress2(), msg, true);
                        }
                        else if (mustChangePassword == true && mustCreateSecurityQuestionAnswer == true)
                        {
                            result[0] = 4;
                            msg = "result = 4; The user was authenticated and the password must be changedand must also enter the security question and answer before accessing the application.";
                            Ifx_DataServices.Executeix_AuthenticationLog_putInsert(userId, userName, GetIPAddress(), GetIPAddress2(), msg, true);
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Authenticate", ex);
                Ifx_DataServices.Executeix_AuthenticationLog_putInsert(null, null, GetIPAddress(), GetIPAddress2(),
                    "0; There was an exception in authenticating the user.  data = :" + ObjectHelper.ObjectArrayToString(data),
                    false);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "aspnet_Authenticate", IfxTraceCategory.Leave);
            }
        }


        string GetIPAddress()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIPAddress", IfxTraceCategory.Enter);
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        return addresses[0];
                    }
                }
                return context.Request.ServerVariables["REMOTE_ADDR"];
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIPAddress", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIPAddress", IfxTraceCategory.Leave);
            }
        }

        string GetIPAddress2()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIPAddress2", IfxTraceCategory.Enter);


                //HttpBrowserCapabilities browse = Request.Browser;

                //OperationContext context = OperationContext.Current;
                //MessageProperties prop = context.IncomingMessageProperties;
                //RemoteEndpointMessageProperty endpoint = prop[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
                //string ip = endpoint.Address;
                //return ip;
                string xip = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();
                return xip;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIPAddress2", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIPAddress2", IfxTraceCategory.Leave);
            }
        }
    


        #endregion Login,  Register and Edit



        #region Artifacts and Permissions



        [OperationContract]
        public object[] GetUiArtifactPermission_securityByUserId(Guid UserId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactPermission_securityByUserId", IfxTraceCategory.Enter);
                return v_UiArtifactPermission_DataServices.GetUiArtifactPermission_securityByUserId(UserId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactPermission_securityByUserId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactPermission_securityByUserId", IfxTraceCategory.Leave);
            }
        }



        [OperationContract]
        public object[] GetUiArtifactAndPermission_securityByUserIdAndAncestorId(Guid? UserId, Guid AncestorId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactAndPermission_securityByUserIdAndAncestorId", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.GetUiArtifactAndPermission_securityByUserIdAndAncestorId(UserId, AncestorId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactAndPermission_securityByUserIdAndAncestorId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetUiArtifactAndPermission_securityByUserIdAndAncestorId", IfxTraceCategory.Leave);
            }
        }









        #endregion Artifacts and Permissions













    }
}
