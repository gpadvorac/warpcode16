using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Ifx;
using vDataServices;

namespace VelocityService
{

    public partial class vSecurityService_ArtifactPermission   //: IvSecurityService_ArtifactPermission
    {




        #region Base Methods

        [OperationContract]
        public object[] v_UiArtifact_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetById", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.v_UiArtifact_GetById(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] v_UiArtifact_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetAll", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.v_UiArtifact_GetAll();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] v_UiArtifact_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetListByFK", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.v_UiArtifact_GetListByFK();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] v_UiArtifact_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.v_UiArtifact_Save(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] v_UiArtifact_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.v_UiArtifact_Deactivate(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public object[] v_UiArtifact_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.v_UiArtifact_Remove(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_UiArtifact_Remove", IfxTraceCategory.Leave);
            }
        }

        #endregion Base Methods


        #region Other Methods


        [OperationContract]
        public object[] Getv_UiArtifactPlatform_cmb()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPlatform_cmb", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.Getv_UiArtifactPlatform_cmb();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPlatform_cmb", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactPlatform_cmb", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] Getv_UiArtifactType_cmbAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactType_cmbAll", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.Getv_UiArtifactType_cmbAll();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactType_cmbAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifactType_cmbAll", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] Getv_UiArtifact_cmbAncestors_ByAp_Id(Guid Ap_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifact_cmbAncestors_ByAp_Id", IfxTraceCategory.Enter);
                return v_UiArtifact_DataServices.Getv_UiArtifact_cmbAncestors_ByAp_Id(Ap_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifact_cmbAncestors_ByAp_Id", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_UiArtifact_cmbAncestors_ByAp_Id", IfxTraceCategory.Leave);
            }
        }



        #endregion Other Methods

    }
}


