using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  12/15/2015 12:07:17 PM

namespace VelocityService
{

    public partial class vFileCacheUtilityService   //: IvFileCacheUtilityService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "vFileCacheUtilityService";

        #endregion Initialize Variables



        [OperationContract]
        public byte[]  GetClientDataCache_ValueAttribute(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_ValueAttribute", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_ValueAttribute(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_ValueAttribute", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_ValueAttribute", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_ValueAttribute_PairedComp(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_ValueAttribute_PairedComp", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_ValueAttribute_PairedComp(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_ValueAttribute_PairedComp", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_ValueAttribute_PairedComp", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_ValueAttribute_PrioritisedList(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_ValueAttribute_PrioritisedList", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_ValueAttribute_PrioritisedList(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_ValueAttribute_PrioritisedList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_ValueAttribute_PrioritisedList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_Competitor_DecisionAttribute2CompetitorEval(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Competitor_DecisionAttribute2CompetitorEval", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_Competitor_DecisionAttribute2CompetitorEval(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Competitor_DecisionAttribute2CompetitorEval", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Competitor_DecisionAttribute2CompetitorEval", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_Competitor_FulfillmentGraph(Guid Mk_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Competitor_FulfillmentGraph", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_Competitor_FulfillmentGraph(Mk_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Competitor_FulfillmentGraph", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Competitor_FulfillmentGraph", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_Competitor_MarketDetails(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Competitor_MarketDetails", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_Competitor_MarketDetails(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Competitor_MarketDetails", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Competitor_MarketDetails", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_DecisionAttribute(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_DecisionAttribute", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_DecisionAttribute(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_DecisionAttribute", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_DecisionAttribute", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_DecisionAttribute_MarketDetails(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_DecisionAttribute_MarketDetails", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_DecisionAttribute_MarketDetails(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_DecisionAttribute_MarketDetails", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_DecisionAttribute_MarketDetails", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_DecisionAttribute_PairedComp(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_DecisionAttribute_PairedComp", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_DecisionAttribute_PairedComp(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_DecisionAttribute_PairedComp", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_DecisionAttribute_PairedComp", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_FuncDetail(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_FuncDetail", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_FuncDetail(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_FuncDetail", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_FuncDetail", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_FuncStudyItem(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_FuncStudyItem", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_FuncStudyItem(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_FuncStudyItem", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_FuncStudyItem", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_FuncCostOnStudyItem(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_FuncCostOnStudyItem", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_FuncCostOnStudyItem(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_FuncCostOnStudyItem", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_FuncCostOnStudyItem", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  ClientDataCache_FuncCostOnStudyItemMatrix(Guid Tm_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ClientDataCache_FuncCostOnStudyItemMatrix", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.ClientDataCache_FuncCostOnStudyItemMatrix(Tm_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ClientDataCache_FuncCostOnStudyItemMatrix", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ClientDataCache_FuncCostOnStudyItemMatrix", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_FuncCostMatrixForBarChart(Guid Tm_Id, Guid Mk_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_FuncCostMatrixForBarChart", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_FuncCostMatrixForBarChart(Tm_Id, Mk_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_FuncCostMatrixForBarChart", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_FuncCostMatrixForBarChart", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_Func2DecisionAttributeInfluence(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Func2DecisionAttributeInfluence", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_Func2DecisionAttributeInfluence(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Func2DecisionAttributeInfluence", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Func2DecisionAttributeInfluence", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_Findings(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Findings", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_Findings(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Findings", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Findings", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_Idea_Risks(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Idea_Risks", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_Idea_Risks(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Idea_Risks", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Idea_Risks", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetClientDataCache_Tasks(Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Tasks", IfxTraceCategory.Enter);
                object[] list = vFileCacheUtility_DataServices.GetClientDataCache_Tasks(Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Tasks", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Tasks", IfxTraceCategory.Leave);
            }
        }




    }
}


