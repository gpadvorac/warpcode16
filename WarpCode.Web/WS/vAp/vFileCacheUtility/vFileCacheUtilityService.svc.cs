﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;
using vDataServices;
using vReportParameter;
using vReportSSRSProvider;
using VelocityWCF;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class vFileCacheUtilityService
    {




        [OperationContract]
        public string GetTestData(string data)
        {
            return data;
        }
        

        [OperationContract]
        public byte[] GetProject_HasNewData(Guid pj_Id, DateTime? timeStamp)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProject_HasNewData", IfxTraceCategory.Enter);

                object[] returnData = new object[7];
                DateTime newTimeStamp = DateTime.Now;
                returnData[0] = newTimeStamp;

                object[] results = Project_DataServices.GetProject_HasNewData(pj_Id, timeStamp);
                bool hasVAs = (bool)results[0];
                bool hasDAs = (bool)results[1];
                bool hasCompetitors = (bool)results[2];
                //bool hasStudyItems = (bool)results[3];
                bool hasFuncs = (bool)results[3];
                bool hasIdeas = (bool)results[4];
                bool hasManagement = (bool)results[5];

                object[] markets = Market_DataServices.GetMarket_lstActiveNames(pj_Id);
                object[] market = null;

                object[] teams = Team_DataServices.GetTeam_lstActiveNames(pj_Id);
                object[] team = null;

                // VALUE ATTRIBUTES
                if (hasVAs == true)
                {
                    object[] oVA = new object[3];
                    oVA[0] = vFileCacheUtility_DataServices.GetClientDataCache_ValueAttribute(pj_Id);
                    oVA[1] = vFileCacheUtility_DataServices.GetClientDataCache_ValueAttribute_PairedComp(pj_Id);
                    oVA[2] = vFileCacheUtility_DataServices.GetClientDataCache_ValueAttribute_PrioritisedList(pj_Id);
                    returnData[1] = oVA;
                }

                // DECISION ATTRIBUTES
                if (hasDAs == true)
                {
                    object[] oDA = new object[3];

                    oDA[0] = vFileCacheUtility_DataServices.GetClientDataCache_DecisionAttribute(pj_Id);
                    oDA[1] = vFileCacheUtility_DataServices.GetClientDataCache_DecisionAttribute_PairedComp(pj_Id);
                    oDA[2] = vFileCacheUtility_DataServices.GetClientDataCache_DecisionAttribute_MarketDetails(pj_Id);
                    returnData[2] = oDA;
                }

                // COMPETITORS
                if (hasCompetitors == true)
                {
                    object[] oComps = new object[6];
                    oComps[0] = vFileCacheUtility_DataServices.GetClientDataCache_Competitor(pj_Id);
                    oComps[1] = vFileCacheUtility_DataServices.GetClientDataCache_Competitor_DecisionAttribute2CompetitorEval(pj_Id);
                    // Loop thru the markets and get a list for each one
                    if (markets != null)
                    {
                        object[] oDaCompEvalMatirxData = new object[markets.Length];
                        for (var i = 0; i < markets.GetUpperBound(0) + 1; i++)
                        {
                            market = markets[i] as object[];
                            if (market != null)
                            {
                                Guid? mk_Id = market[0] as Guid?;
                                string marketName = market[1] as string;
                                if (mk_Id != null)
                                {
                                    object[] oDaCompEvalMatirx = new object[2];
                                    oDaCompEvalMatirx[0] = marketName;
                                    oDaCompEvalMatirx[1] = vFileCacheUtility_DataServices.GetClientDataCache_Competitor_EvalMatrixByMarket((Guid)mk_Id);
                                    oDaCompEvalMatirxData[i] = oDaCompEvalMatirx;
                                }
                            }
                        }
                        oComps[2] = oDaCompEvalMatirxData;
                    }
                    oComps[3] = vFileCacheUtility_DataServices.GetClientDataCache_Competitor_MarketDetails(pj_Id);


                    if (markets != null)
                    {
                        object[] oFulfillmentGraphData = new object[markets.Length];
                        for (var i = 0; i < markets.GetUpperBound(0) + 1; i++)
                        {
                            market = markets[i] as object[];
                            if (market != null)
                            {
                                Guid? mk_Id = market[0] as Guid?;
                                string marketName = market[1] as string;
                                if (mk_Id != null)
                                {
                                    object[] oFulfillmentGraph = new object[2];
                                    oFulfillmentGraph[0] = marketName;
                                    oFulfillmentGraph[1] = vFileCacheUtility_DataServices.GetClientDataCache_Competitor_FulfillmentGraph((Guid)mk_Id);
                                    oFulfillmentGraphData[i] = oFulfillmentGraph;
                                }
                            }
                        }
                        oComps[4] = oFulfillmentGraphData;
                    }
                    returnData[3] = oComps;
                }

                // FUNCTIONS
                if (hasFuncs == true)
                {
                    object[] oFuncs = new object[7];
                    oFuncs[0] = vFileCacheUtility_DataServices.GetClientDataCache_FuncDetail(pj_Id);
                    oFuncs[1] = vFileCacheUtility_DataServices.GetClientDataCache_FuncStudyItem(pj_Id);
                    oFuncs[2] = vFileCacheUtility_DataServices.GetClientDataCache_FuncCostOnStudyItem(pj_Id);
                    // *Func-Study Item Cost Matrix
                    if (teams != null)
                    {
                        object[] oFuncCostMatrixData = new object[teams.Length];
                        for (var i = 0; i < teams.GetUpperBound(0) + 1; i++)
                        {
                            team = teams[i] as object[];
                            if (team != null)
                            {
                                Guid? tm_Id = team[0] as Guid?;
                                string teamName = team[1] as string;
                                if (tm_Id != null)
                                {
                                    object[] oFuncCostMatrix = new object[3];
                                    object[] oData = vFileCacheUtility_DataServices.ClientDataCache_FuncCostOnStudyItemMatrix((Guid)tm_Id);
                                    if (oData != null)
                                    {
                                        oFuncCostMatrix[0] = teamName;
                                        oFuncCostMatrix[1] = oData[0];
                                        oFuncCostMatrix[2] = oData[1];
                                        oFuncCostMatrixData[i] = oFuncCostMatrix;
                                    }
                                }
                            }
                        }
                        oFuncs[3] = oFuncCostMatrixData;
                    }

                    // *Func Influence on DA
                    oFuncs[4] = vFileCacheUtility_DataServices.GetClientDataCache_Func2DecisionAttributeInfluence(pj_Id);

                    // *
                    if (teams != null)
                    {
                        // Create the containter for each TEAM's data
                        object[] oFuncInfluenceTeamsData = new object[teams.Length];
                        // Loop thru TEAMs
                        for (var i = 0; i < teams.GetUpperBound(0) + 1; i++)
                        {
                            team = teams[i] as object[];
                            if (team != null)
                            {
                                // Container for a TEAM.  Team name and all Markets
                                object[] oFuncInfluenceTeam = new object[2];
                                Guid? tm_Id = team[0] as Guid?;
                                string teamName = team[1] as string;
                                if (tm_Id != null)
                                {
                                    // Create the containter for each MARKET's data
                                    object[] oFuncInfluenceMarketsData = new object[markets.Length];
                                    // Loop thru MARKETs
                                    for (var ii = 0; ii < markets.GetUpperBound(0) + 1; ii++)
                                    {
                                        market = markets[ii] as object[];
                                        if (market != null)
                                        {
                                            Guid? mk_Id = market[0] as Guid?;
                                            string marketName = market[1] as string;
                                            if (mk_Id != null)
                                            {
                                                //Matrix for this market.  Market name, headers, matrix
                                                object[] oFuncInfluenceMarket = new object[3];
                                                object[] oData = Func_DataServices.GetFunc2DecisionAttribute_InfluenceMatrix_ByTeamAndMarket((Guid)tm_Id, (Guid)mk_Id);
                                                if (oData != null)
                                                {
                                                    oFuncInfluenceMarket[0] = marketName;
                                                    oFuncInfluenceMarket[1] = oData[0];
                                                    oFuncInfluenceMarket[2] = oData[1];
                                                    oFuncInfluenceMarketsData[ii] = oFuncInfluenceMarket;
                                                }
                                            }
                                        }
                                        oFuncInfluenceTeam[0] = teamName;
                                        oFuncInfluenceTeam[1] = oFuncInfluenceMarketsData;
                                    }
                                }
                                oFuncInfluenceTeamsData[i] = oFuncInfluenceTeam;
                            }
                        }
                        oFuncs[5] = oFuncInfluenceTeamsData;
                    }


                    if (teams != null)
                    {
                        // Create the containter for each TEAM's data
                        object[] oFuncCostBarChartTeamsData = new object[teams.Length];
                        // Loop thru TEAMs
                        for (var i = 0; i < teams.GetUpperBound(0) + 1; i++)
                        {
                            team = teams[i] as object[];
                            if (team != null)
                            {
                                // Container for a TEAM.  Team name and all Markets
                                object[] oFuncCostBarChartTeam = new object[2];
                                Guid? tm_Id = team[0] as Guid?;
                                string teamName = team[1] as string;
                                if (tm_Id != null)
                                {
                                    // Create the containter for each MARKET's data
                                    object[] oFuncCostBarChartMarketsData = new object[markets.Length];
                                    // Loop thru MARKETs
                                    for (var ii = 0; ii < markets.GetUpperBound(0) + 1; ii++)
                                    {
                                        market = markets[ii] as object[];
                                        if (market != null)
                                        {
                                            Guid? mk_Id = market[0] as Guid?;
                                            string marketName = market[1] as string;
                                            if (mk_Id != null)
                                            {
                                                //Matrix for this market.  Market name, matrix
                                                object[] oFuncCostBarChartMarket = new object[2];
                                                object[] oData = vFileCacheUtility_DataServices.GetClientDataCache_FuncCostMatrixForBarChart((Guid)tm_Id, (Guid)mk_Id);
                                                if (oData != null)
                                                {
                                                    oFuncCostBarChartMarket[0] = marketName;
                                                    oFuncCostBarChartMarket[1] = oData;
                                                    oFuncCostBarChartMarketsData[ii] = oFuncCostBarChartMarket;
                                                }
                                            }
                                        }
                                        oFuncCostBarChartTeam[0] = teamName;
                                        oFuncCostBarChartTeam[1] = oFuncCostBarChartMarketsData;
                                    }
                                }
                                oFuncCostBarChartTeamsData[i] = oFuncCostBarChartTeam;
                            }
                        }
                        oFuncs[6] = oFuncCostBarChartTeamsData;
                    }
                    returnData[4] = oFuncs;

                }

                // IDEAS
                if (hasIdeas == true)
                {

                    object[] oIdeas = new object[3];
                    oIdeas[0] = vFileCacheUtility_DataServices.GetClientDataCache_Idea_RatingsImpacts(pj_Id);
                    oIdeas[1] = vFileCacheUtility_DataServices.GetClientDataCache_Idea_Risks(pj_Id);
                    oIdeas[2] = GetExhibitsAsFile(pj_Id);

                    returnData[5] = oIdeas;
                }

                // MANAGEMENT
                if (hasManagement == true)
                {

                    object[] oMngmt = new object[2];

                    oMngmt[0] = vFileCacheUtility_DataServices.GetClientDataCache_Tasks(pj_Id);
                    oMngmt[1] = vFileCacheUtility_DataServices.GetClientDataCache_Findings(pj_Id);

                    returnData[6] = oMngmt;
                }

                byte[] retData = Serialization.SilverlightSerializer.Serialize(returnData);
                return retData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProject_HasNewData", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetProject_HasNewData", IfxTraceCategory.Leave);
            }
        }

        
        [OperationContract]
        public byte[] Project_lstByUserAccess_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_lstByUserAccess_ComboItemList", IfxTraceCategory.Enter);
                byte[] returnData = null;
                Guid? _alternateId = Provider.GetAlternateId();
                if (_alternateId != null)
                {
                    object[] ret = v_UserProfile_DataServices.Getv_User2AlternateId_GetUserId((Guid)_alternateId);
                    Guid? userId = null;
                    if (ret != null)
                    {
                        userId = ret[0] as Guid?;
                    }
                    if (userId != null)
                    {
                        object[] list = Project_DataServices.Project_lstByUserAccess_ComboItemList((Guid)userId);
                        returnData = Serialization.SilverlightSerializer.Serialize(list);
                    }
                    else
                    {
                        return null;
                    }
                }
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_lstByUserAccess_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Project_lstByUserAccess_ComboItemList", IfxTraceCategory.Leave);
            }
        }
        

        [OperationContract]
        public byte[] GetClientDataCache_Competitor_EvalMatrix(Guid pj_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Competitor_EvalMatrix", IfxTraceCategory.Enter);
                object[] markets = Market_DataServices.GetMarket_lstActiveNames(pj_Id);
                if (markets == null) { return null;}
                object[] list = new object[markets.Length];
                //object[] matrixItem = null;
                for (int i = 0; i < markets.GetUpperBound(0) + 1; i++)
                {
                    Guid id = (Guid)((object[])(object[])markets[i])[0];
                    string marketName = (string)((object[])(object[])markets[i])[1];
                    object[] data = Competitor_DataServices.GetDecisionAttribute2CompetitorEvalMatrixByMarket(id);
                    if (data != null)
                    {
                        object[] matrixItem = new object[3];
                        matrixItem[0] = marketName;
                        matrixItem[1] = data[0];
                        matrixItem[2] = data[1];
                        list[i] = matrixItem;
                    }
                }


                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Competitor_EvalMatrix", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetClientDataCache_Competitor_EvalMatrix", IfxTraceCategory.Leave);
            }
        }


        private byte[] GetExhibitsAsFile(Guid pj_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetExhibitsAsFile", IfxTraceCategory.Enter);

                //vReport rpt = new vReport(new Guid("4c1ff1bc-7ac2-4967-a76c-1531aff9e565"), "MainCnst_IdeaExhibits_ByWorkshop", "Listing of All Exhibits");

                ReportMetadata rmd = new ReportMetadata();
                rmd.Report_Id = new Guid("4c1ff1bc-7ac2-4967-a76c-1531aff9e565");
                rmd.ReportSysName = "MainCnst_IdeaExhibits_ByWorkshop";
                rmd.Format = ReportFormat.PDF;


                //rmd.AddParameter("Pj_Id", pj_Id.ToString());
                //rpt.AddParameter("ReportVersionTypeId", "0");


                rmd.Parameters.Add(new ReportParameter("Pj_Id", pj_Id.ToString()));
                rmd.Parameters.Add(new ReportParameter("ReportVersionTypeId", "0"));




                ReportProvider provider = new ReportProvider();
                byte[] reportData = provider.GetReportAsByteArray(rmd);



                //byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return reportData;
       
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetExhibitsAsFile", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetExhibitsAsFile", IfxTraceCategory.Leave);
            }
        }





    }
}
