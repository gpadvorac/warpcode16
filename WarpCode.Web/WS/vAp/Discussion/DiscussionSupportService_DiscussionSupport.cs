using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  5/18/2016 7:47:41 PM

namespace VelocityService
{

    public partial class DiscussionSupportService   //: IDiscussionSupportService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "DiscussionSupportService";

        #endregion Initialize Variables


        //#region Base Methods


        //[OperationContract]
        //public byte[] DiscussionSupport_GetById( id)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_GetById", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.DiscussionSupport_GetById(id);
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_GetById", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_GetById", IfxTraceCategory.Leave);
        //    }
        //}

        //[OperationContract]
        //public byte[] DiscussionSupport_GetAll()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_GetAll", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.DiscussionSupport_GetAll();
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_GetAll", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_GetAll", IfxTraceCategory.Leave);
        //    }
        //}

        //[OperationContract]
        //public byte[] DiscussionSupport_GetListByFK()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_GetListByFK", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.DiscussionSupport_GetListByFK();
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_GetListByFK", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_GetListByFK", IfxTraceCategory.Leave);
        //    }
        //}

        //[OperationContract]
        //public byte[] DiscussionSupport_Save(object[] data, int check)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.DiscussionSupport_Save(data, check);
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_Save", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_Save", IfxTraceCategory.Leave);
        //    }
        //}

        //[OperationContract]
        //public byte[] DiscussionSupport_Delete(object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.DiscussionSupport_Delete(data);
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_Delete", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_Delete", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public object[] DiscussionSupport_SetIsDeleted( id, bool isDeleted)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_SetIsDeleted", new ValuePair[] {new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
        //        return DiscussionSupport_DataServices.DiscussionSupport_SetIsDeleted(id, isDeleted);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_SetIsDeleted", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_SetIsDeleted", IfxTraceCategory.Leave);
        //    }
        //}

        //[OperationContract]
        //public byte[] DiscussionSupport_Deactivate(object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.DiscussionSupport_Deactivate(data);
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_Deactivate", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_Deactivate", IfxTraceCategory.Leave);
        //    }
        //}

        //[OperationContract]
        //public byte[] DiscussionSupport_Remove(object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.DiscussionSupport_Remove(data);
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_Remove", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionSupport_Remove", IfxTraceCategory.Leave);
        //    }
        //}


        //#endregion Base Methods


        #region Other Methods



        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_Finding(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Finding", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_Finding(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Finding", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Finding", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_Idea(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Idea", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_Idea(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Idea", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Idea", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_Project(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Project", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_Project(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Project", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Project", IfxTraceCategory.Leave);
        //    }
        //}


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_Task(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_Task(Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_MyDiscussions(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_MyDiscussions(Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_DiscussionsImIn(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_DiscussionsImIn(Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", IfxTraceCategory.Leave);
            }
        }


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_FindingMember(Guid Id, Guid MemberId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_FindingMember", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_FindingMember(Id, MemberId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_FindingMember", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_FindingMember", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_IdeaMember(Guid Id, Guid MemberId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaMember", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_IdeaMember(Id, MemberId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaMember", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaMember", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_ProjectMember(Guid Id, Guid MemberId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ProjectMember", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_ProjectMember(Id, MemberId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ProjectMember", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ProjectMember", IfxTraceCategory.Leave);
        //    }
        //}


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_TaskMember(Guid Id, Guid MemberId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_TaskMember(Id, MemberId );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", IfxTraceCategory.Leave);
            }
        }


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_FindingMy(Guid Id, Guid OwnerId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_FindingMy", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_FindingMy(Id, OwnerId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_FindingMy", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_FindingMy", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_IdeaMy(Guid Id, Guid OwnerId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaMy", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_IdeaMy(Id, OwnerId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaMy", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaMy", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_ProjectMy(Guid Id, Guid OwnerId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ProjectMy", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_ProjectMy(Id, OwnerId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ProjectMy", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ProjectMy", IfxTraceCategory.Leave);
        //    }
        //}


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_TaskMy(Guid Id, Guid OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_TaskMy(Id, OwnerId );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", IfxTraceCategory.Leave);
            }
        }


        //[OperationContract]
        //public byte[]  GetDiscussion_lstByProjectAll(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstByProjectAll", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstByProjectAll(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstByProjectAll", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstByProjectAll", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstByProjectOnly(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstByProjectOnly", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstByProjectOnly(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstByProjectOnly", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstByProjectOnly", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_IdeaKB(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaKB", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_IdeaKB(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaKB", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaKB", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_IdeaKBMember(Guid Id, Guid MemberId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaKBMember", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_IdeaKBMember(Id, MemberId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaKBMember", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaKBMember", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_IdeaKBMy(Guid Id, Guid OwnerId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaKBMy", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_IdeaKBMy(Id, OwnerId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaKBMy", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaKBMy", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_DiscussionsImIn_ByProject(Guid Pj_Id, Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn_ByProject", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_DiscussionsImIn_ByProject(Pj_Id, Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn_ByProject", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn_ByProject", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_MyDiscussions_ByProject(Guid Pj_Id, Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions_ByProject", IfxTraceCategory.Enter);
        //        object[] list = DiscussionSupport_DataServices.GetDiscussion_MyDiscussions_ByProject(Pj_Id, Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions_ByProject", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions_ByProject", IfxTraceCategory.Leave);
        //    }
        //}





        #endregion Other Methods

    }
}


