using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  9/26/2015 5:10:40 PM

namespace VelocityService
{

    public partial class DiscussionService   //: IDiscussionService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "DiscussionService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] Discussion_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_GetById", IfxTraceCategory.Enter);
                object[] list = Discussion_DataServices.Discussion_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] Discussion_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_GetAll", IfxTraceCategory.Enter);
                object[] list = Discussion_DataServices.Discussion_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] Discussion_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_GetListByFK", IfxTraceCategory.Enter);
                object[] list = Discussion_DataServices.Discussion_GetListByFK();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] Discussion_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = Discussion_DataServices.Discussion_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] Discussion_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = Discussion_DataServices.Discussion_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] Discussion_SetIsDeleted(Guid id, bool isDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_SetIsDeleted", new ValuePair[] {new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return Discussion_DataServices.Discussion_SetIsDeleted(id, isDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] Discussion_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = Discussion_DataServices.Discussion_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] Discussion_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = Discussion_DataServices.Discussion_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Discussion_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        //#region Other Methods



        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_Finding(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Finding", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_Finding(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Finding", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Finding", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_Idea(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Idea", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_Idea(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Idea", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Idea", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_Owner(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Owner", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_Owner(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Owner", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Owner", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_Project(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Project", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_Project(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Project", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Project", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_Task(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_Task(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_MyDiscussions(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_MyDiscussions(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_DiscussionsImIn(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_DiscussionsImIn(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_FindingMember(Guid Id, Guid MemberId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_FindingMember", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_FindingMember(Id, MemberId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_FindingMember", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_FindingMember", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_IdeaMember(Guid Id, Guid MemberId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaMember", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_IdeaMember(Id, MemberId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaMember", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaMember", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_OwnerMember(Guid Id, Guid MemberId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_OwnerMember", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_OwnerMember(Id, MemberId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_OwnerMember", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_OwnerMember", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_ProjectMember(Guid Id, Guid MemberId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ProjectMember", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_ProjectMember(Id, MemberId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ProjectMember", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ProjectMember", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_TaskMember(Guid Id, Guid MemberId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_TaskMember(Id, MemberId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_FindingMy(Guid Id, Guid OwnerId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_FindingMy", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_FindingMy(Id, OwnerId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_FindingMy", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_FindingMy", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_IdeaMy(Guid Id, Guid OwnerId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaMy", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_IdeaMy(Id, OwnerId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaMy", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_IdeaMy", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_OwnerMy(Guid Id, Guid OwnerId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_OwnerMy", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_OwnerMy(Id, OwnerId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_OwnerMy", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_OwnerMy", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_ProjectMy(Guid Id, Guid OwnerId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ProjectMy", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_ProjectMy(Id, OwnerId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ProjectMy", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ProjectMy", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstBy_TaskMy(Guid Id, Guid OwnerId )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstBy_TaskMy(Id, OwnerId );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstByProjectAll(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstByProjectAll", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstByProjectAll(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstByProjectAll", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstByProjectAll", IfxTraceCategory.Leave);
        //    }
        //}


        //[OperationContract]
        //public byte[]  GetDiscussion_lstByProjectOnly(Guid Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstByProjectOnly", IfxTraceCategory.Enter);
        //        object[] list = Discussion_DataServices.GetDiscussion_lstByProjectOnly(Id );
        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstByProjectOnly", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstByProjectOnly", IfxTraceCategory.Leave);
        //    }
        //}





        //#endregion Other Methods

    }
}


