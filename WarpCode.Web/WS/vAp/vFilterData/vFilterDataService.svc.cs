﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
//using vDataServices;
using Ifx;
using DataServices;
using vDataServices;
//using Person_DataServices = DataServices.Person_DataServices;

//using DataService;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class vFilterDataService
    {


        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "vFilterDataService";

        #endregion Initialize Variables




        [OperationContract]
        public byte[] GetvFilterSessionMD_AllSessionsAllLevels()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetvFilterSessionMD_AllSessionsAllLevels", IfxTraceCategory.Enter);
                object[][] obj = new object[6][];

                obj[0] = v_FilterSessionMD_DataServices.v_FilterSessionMD_GetAll();
                obj[1] = v_FilterSessionContextParameter_DataServices.v_FilterSessionContextParameter_GetAll();
                obj[2] = v_FilterControlMD_DataServices.v_FilterControlMD_GetAll();
                obj[3] = v_FilterControlFieldMD_DataServices.v_FilterControlFieldMD_GetAll();
                obj[4] = v_FilterControlFieldAttributeMD_DataServices.v_FilterControlFieldAttributeMD_GetAll();
                //obj[5] = v_FilterControlFieldParameter_DataServices.v_FilterControlFieldParameterMD_GetAll();
                obj[5] = v_FilterControlFieldParameter_DataServices.v_FilterControlFieldParameter_GetAll();

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetvFilterSessionMD_AllSessionsAllLevels", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetvFilterSessionMD_AllSessionsAllLevels", IfxTraceCategory.Leave);
            }
        }


        /*

        spSysYesNo_ComboItemList
        spIdeaCategory_ComboItemList
         * spProjectSpecialty_ComboItemList
         * spProjectVocation_ComboItemList
         * spProjectMarket_ComboItemList
         * spProjectIndustry_ComboItemList

        */

        [OperationContract]
        public byte[] GetFilterList(string listName, object[] parms)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "listName", new ValuePair[] { new ValuePair("listName", listName), new ValuePair("parms", parms) }, IfxTraceCategory.Enter);
                Guid? pj_Id = null;
                object[] list = null;
                byte[] returnData = null;

                //switch (listName)
                //{
                //    case "HardSoftType":
                //        list = Project_DataServices.GetHardSoftType_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;

                //    case "SystemComponentList":
                //        list = Project_DataServices.GetSystemComponentType_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "IndustryNameList":
                //        list = Project_DataServices.GetProjectIndustry_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "IndustrySub1NameList":
                //        //list = CommonClientData_DataServices.GetState_ComboItemList();
                //        //returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        //return returnData;
                //        return null;
                //    case "IndustrySub2NameList":
                //        //list = CommonClientData_DataServices.GetState_ComboItemList();
                //        //returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        //return returnData;
                //        return null;
                //    case "IndustrySub3NameList":
                //        //list = CommonClientData_DataServices.GetState_ComboItemList();
                //        //returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        //return returnData;
                //        return null;
                //    case "MarketNameList":
                //        list = Project_DataServices.GetProjectMarket_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "MarketSub1NameList":
                //        //list = CommonClientData_DataServices.GetState_ComboItemList();
                //        //returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        //return returnData;
                //        return null;
                //    case "MarketSub2NameList":
                //        //list = CommonClientData_DataServices.GetState_ComboItemList();
                //        //returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        //return returnData;
                //        return null;
                //    case "VocationNameList":
                //        list = Project_DataServices.GetProjectVocation_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "SpecialtyNameList":
                //        list = Project_DataServices.GetProjectSpecialty_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "FunctionListAll":
                //        list = Func_DataServices.GetFuncListAll_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "VerbListAll":
                //        list = Func_DataServices.GetFuncVerb_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "NounListAll":
                //        list = Func_DataServices.GetFuncNoun_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "CategoryNameList":
                //        list = Idea_DataServices.GetIdeaCategory_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "IsGameChanger":
                //        list = CommonClientData_DataServices.GetSysYesNo_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "IdeaApprovalStatus":
                //        list = Idea_DataServices.GetIdeaApprovalStatus_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "ProjectManagers":
                //        list = Person_DataServices.GetPerson_lstAssignedAsProjectManager_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    case "ReportVersionType":
                //        list = CommonClientData_DataServices.Getv_ReportVersionType_ComboItemList();
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    //case "WellProductType":
                //    //    list = Well_DataServices.GetWellProductType_ComboItemList();
                //    //    returnData = Serialization.SilverlightSerializer.Serialize(list);
                //    //    return returnData;
                //    case "Teams":
                //        pj_Id = parms[0] as Guid?;
                //        if (pj_Id == null)
                //        {
                //            throw new Exception("vFilterDataService.GetFilterList:  Could not parse Project Id from param array for Teams");
                //        }
                //        list = Team_DataServices.GetTeam_ComboItemList((Guid)pj_Id);
                //        returnData = Serialization.SilverlightSerializer.Serialize(list);
                //        return returnData;
                //    //case "WellStatus":
                //    //    list = Well_DataServices.GetWellStatus_ComboItemList();
                //    //    returnData = Serialization.SilverlightSerializer.Serialize(list);
                //    //    return returnData;
                //    //case "ContractType":
                //    //    list = Contract_DataServices.GetContractType_ComboItemList();
                //    //    returnData = Serialization.SilverlightSerializer.Serialize(list);
                //    //    return returnData;
                //    //case "ObligationType":
                //    //    list = Obligation_DataServices.GetObligationType_ComboItemList();
                //    //    returnData = Serialization.SilverlightSerializer.Serialize(list);
                //    //    return returnData;
                //    //case "Blocks":
                //    //    prj_Id = parms[0] as Guid?;
                //    //    if (prj_Id == null)
                //    //    {
                //    //        throw new Exception("vFilterDataService.GetFilterList:  Could not parse Project Id from param array for Feching Blocks.");
                //    //    }

                //    //    list = CommonClientData_DataServices.GetBlock_ComboItemList((Guid)prj_Id);
                //    //    returnData = Serialization.SilverlightSerializer.Serialize(list);
                //    //    return returnData;
                //    //case "StateCounty":
                //    //    list = CommonClientData_DataServices.GetStateCounty_ComboItemList();
                //    //    returnData = Serialization.SilverlightSerializer.Serialize(list);
                //    //    return returnData;
                //    case "4xxxx":

                //        break;
                //    default:
                //        return null;
                //}





                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "listName", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "listName", IfxTraceCategory.Leave);
            }
        }




        [OperationContract]
        public byte[] Getv_FilterSessionMD_cmbForReports()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterSessionMD_cmbForReports", IfxTraceCategory.Enter);
                object[] list = v_FilterSessionMD_DataServices.Getv_FilterSessionMD_cmbForReports();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterSessionMD_cmbForReports", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_FilterSessionMD_cmbForReports", IfxTraceCategory.Leave);
            }
        }




        [OperationContract]
        public byte[] Get2ndFilterList(string listName, object[] parms)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get2ndFilterList", new ValuePair[] { new ValuePair("listName", listName), new ValuePair("parms", parms) }, IfxTraceCategory.Enter);
                //Guid? prj_Id = null;
                object[] list = null;
                byte[] returnData = null;

                switch (listName)
                {
                    //case "ExampleForWhenWeHaveANullParam":

                    //    prj_Id = parms[0] as Guid?;
                    //    if (prj_Id == null)
                    //    {
                    //        throw new Exception("vFilterDataService.GetFilterList:  Could not parse Project Id from param array for WellAltCategory1");
                    //    }
                    //    list = Well_DataServices.GetWellAltCategory1_ComboItemList((Guid)prj_Id);
                    //    returnData = Serialization.SilverlightSerializer.Serialize(list);
                    //    return returnData;
                    case "CountiesByState":
                        Guid? St_Id = parms[0] as Guid?;
                        if (St_Id == null)
                        {
                            throw new Exception("vFilterDataService.GetFilterList:  Could not parse State Id from param array for Feching Counties by State.");
                        }

                        //list = CommonClientData_DataServices.GetCounty_ByStateIdComboItemList((Guid)St_Id);
                        returnData = Serialization.SilverlightSerializer.Serialize(list);
                        return returnData;
                    case "4xxxx":

                        break;
                    default:
                        return null;
                }

                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get2ndFilterList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get2ndFilterList", IfxTraceCategory.Leave);
            }
        }





    }
}
