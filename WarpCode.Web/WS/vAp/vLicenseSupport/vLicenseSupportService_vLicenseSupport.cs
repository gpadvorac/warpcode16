using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  3/22/2017 1:00:49 AM

namespace VelocityService
{

    public partial class vLicenseSupportService   //: IvLicenseSupportService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "vLicenseSupportService";

        #endregion Initialize Variables


        [OperationContract]
        public byte[] GetvLicenseSupport_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_vLicenseSupport_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[3][];


                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_vLicenseSupport_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_vLicenseSupport_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


                [OperationContract]
        public byte[]  GetPerson_ForLicenseAssignment_ComboItemList(Guid LicClSet_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment_ComboItemList", IfxTraceCategory.Enter);
                object[] list = vLicenseSupport_DataServices.GetPerson_ForLicenseAssignment_ComboItemList(LicClSet_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment_ComboItemList", IfxTraceCategory.Leave);
            }
        }


                [OperationContract]
        public object[]  Executev_LicenseClient_AssignUser(Guid v_LicCl_LicClSet_Id, Guid v_LicCl_AssignedUserId, Guid v_LicCl_UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_LicenseClient_AssignUser", IfxTraceCategory.Enter);
                return vLicenseSupport_DataServices.Executev_LicenseClient_AssignUser(v_LicCl_LicClSet_Id, v_LicCl_AssignedUserId, v_LicCl_UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_LicenseClient_AssignUser", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_LicenseClient_AssignUser", IfxTraceCategory.Leave);
            }
        }


                [OperationContract]
        public byte[]  GetPerson_ForLicenseAssignment(Guid LicClSet_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment", IfxTraceCategory.Enter);
                object[] list = vLicenseSupport_DataServices.GetPerson_ForLicenseAssignment(LicClSet_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_ForLicenseAssignment", IfxTraceCategory.Leave);
            }
        }




    }
}


