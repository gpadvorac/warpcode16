﻿using DataServices;
using Ifx;
using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class vGridColumnGroupService
    {




        //[OperationContract]
        //public byte[] Getv_GridColumnGroup_ReadOnlyStaticLists(Guid v_GdColGrp_Gd_Id, Guid v_GdColGrp_OwnerId)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_GridColumnGroup_ReadOnlyStaticLists", IfxTraceCategory.Enter);
        //        object[][] obj = new object[3][];

        //        obj[0] = v_GridColumnGroup_DataServices.Getv_GridColumnGroup_SystemList_ComboItemList(v_GdColGrp_Gd_Id);
        //        obj[1] = v_GridColumnGroup_DataServices.Getv_GridColumnGroup_UserList_ComboItemList(v_GdColGrp_Gd_Id, v_GdColGrp_OwnerId);
        //        obj[2] = v_GridColumnGroup_DataServices.Getv_GridColumnGroupType_ComboItemList();

        //        byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_GridColumnGroup_ReadOnlyStaticLists", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_GridColumnGroup_ReadOnlyStaticLists", IfxTraceCategory.Leave);
        //    }
        //}







        [OperationContract]
        public int v_GridColumnGroup_AssignColumns(object[] data, Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_AssignColumns", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                int success = 0;
                for (int i = 0; i < data.Length; i++)
                {
                    success = v_GridColumnGroup_DataServices.v_GridColumnGroup_AssignColumns((object[])data[i], userId);
                    if (success == 0)
                    {
                        break;
                    }
                }
                return success;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_AssignColumns", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_AssignColumns", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public int v_GridColumnGroup_RemoveColumns(string crit)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_RemoveColumns", new ValuePair[] { new ValuePair("crit", crit) }, IfxTraceCategory.Enter);
                int success = 0;
                success = v_GridColumnGroup_DataServices.v_GridColumnGroup_RemoveColumns(crit);
                return success;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_RemoveColumns", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroup_RemoveColumns", IfxTraceCategory.Leave);
            }
        }






        [OperationContract]
        public byte[] GetGridColumnGroupsAndColumns(Guid v_GdColGrp_Gd_Id, Guid? OwnerId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_GridColumnGroup_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[2][];

                obj[0] = v_GridColumnGroup_DataServices.Getv_GridColumnGroup_cmbByType(v_GdColGrp_Gd_Id, OwnerId);
                obj[1] = v_GridColumnGroup_DataServices.Getv_GridColumnGroupColumn_lstByGroupType(v_GdColGrp_Gd_Id, OwnerId);

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_GridColumnGroup_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_GridColumnGroup_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }







        [OperationContract]
        public int Get_GridColumnGroupTypeUserPreference_GroupType(Guid userId, Guid gridId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_GridColumnGroupTypeUserPreference_GroupType", new ValuePair[] { new ValuePair("userId", userId), new ValuePair("gridId", gridId) }, IfxTraceCategory.Enter);
                int type = 1;
                type = v_GridColumnGroup_DataServices.Get_GridColumnGroupTypeUserPreference_GroupType(userId, gridId);
                return type;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_GridColumnGroupTypeUserPreference_GroupType", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_GridColumnGroupTypeUserPreference_GroupType", IfxTraceCategory.Leave);
            }
        }



        [OperationContract]
        public int v_GridColumnGroupTypeUserPreference_AddUpdate(Guid userId, Guid gridId, int groupType)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_AddUpdate", new ValuePair[] { new ValuePair("userId", userId), new ValuePair("gridId", gridId) }, IfxTraceCategory.Enter);
                int success = 0;
                success = v_GridColumnGroup_DataServices.v_GridColumnGroupTypeUserPreference_AddUpdate(userId, gridId, groupType);
                return success;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_AddUpdate", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_AddUpdate", IfxTraceCategory.Leave);
            }
        }





        [OperationContract]
        public byte[] v_GridColumnGroupTypeUserPreference_lstByUserId(Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_lstByUserId", new ValuePair[] { new ValuePair("userId", userId) }, IfxTraceCategory.Enter);
                object[] list = v_GridColumnGroup_DataServices.v_GridColumnGroupTypeUserPreference_lstByUserId(userId);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_lstByUserId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_GridColumnGroupTypeUserPreference_lstByUserId", IfxTraceCategory.Leave);
            }
        }








    }
}
