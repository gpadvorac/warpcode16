using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  12/1/2016 10:48:54 AM

namespace VelocityService
{

    public partial class WcStoredProcParamValueGroupService   //: IWcStoredProcParamValueGroupService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "WcStoredProcParamValueGroupService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] WcStoredProcParamValueGroup_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = WcStoredProcParamValueGroup_DataServices.WcStoredProcParamValueGroup_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcStoredProcParamValueGroup_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetAll", IfxTraceCategory.Enter);
                object[] list = WcStoredProcParamValueGroup_DataServices.WcStoredProcParamValueGroup_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcStoredProcParamValueGroup_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = WcStoredProcParamValueGroup_DataServices.WcStoredProcParamValueGroup_GetListByFK(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcStoredProcParamValueGroup_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcStoredProcParamValueGroup_DataServices.WcStoredProcParamValueGroup_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcStoredProcParamValueGroup_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcStoredProcParamValueGroup_DataServices.WcStoredProcParamValueGroup_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] WcStoredProcParamValueGroup_SetIsDeleted(Guid id, bool isDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_SetIsDeleted", new ValuePair[] {new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return WcStoredProcParamValueGroup_DataServices.WcStoredProcParamValueGroup_SetIsDeleted(id, isDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcStoredProcParamValueGroup_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcStoredProcParamValueGroup_DataServices.WcStoredProcParamValueGroup_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcStoredProcParamValueGroup_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcStoredProcParamValueGroup_DataServices.WcStoredProcParamValueGroup_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods






        #endregion Other Methods

    }
}


