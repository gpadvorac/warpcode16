﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class WcStoredProcService
    {



        [OperationContract]
        public int RefreshStoredProcedureColumns(Guid sp_Id, Guid? DbCnSK_Id, Guid userId, int refreshType)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshStoredProcedureColumns", IfxTraceCategory.Enter);
                object[][] obj = new object[4][];
                int success = WcStoredProc_DataServices.RefreshStoredProcedureColumns(sp_Id, DbCnSK_Id, userId, refreshType);
                return success;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshStoredProcedureColumns", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshStoredProcedureColumns", IfxTraceCategory.Leave);
            }
        }

    }
}
