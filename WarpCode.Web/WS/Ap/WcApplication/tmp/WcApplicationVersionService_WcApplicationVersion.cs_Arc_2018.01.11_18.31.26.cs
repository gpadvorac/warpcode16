using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  1/3/2018 11:10:25 PM

namespace VelocityService
{

    public partial class WcApplicationVersionService   //: IWcApplicationVersionService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "WcApplicationVersionService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] WcApplicationVersion_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetById", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = WcApplicationVersion_DataServices.WcApplicationVersion_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcApplicationVersion_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetAll", IfxTraceCategory.Enter);
                object[] list = WcApplicationVersion_DataServices.WcApplicationVersion_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcApplicationVersion_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetListByFK", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = WcApplicationVersion_DataServices.WcApplicationVersion_GetListByFK(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcApplicationVersion_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Save", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcApplicationVersion_DataServices.WcApplicationVersion_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcApplicationVersion_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Delete", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcApplicationVersion_DataServices.WcApplicationVersion_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] WcApplicationVersion_SetIsDeleted(Guid id, bool isDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_SetIsDeleted", new ValuePair[] { new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return WcApplicationVersion_DataServices.WcApplicationVersion_SetIsDeleted(id, isDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcApplicationVersion_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Deactivate", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcApplicationVersion_DataServices.WcApplicationVersion_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcApplicationVersion_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Remove", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcApplicationVersion_DataServices.WcApplicationVersion_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods


        [OperationContract]
        public byte[] GetWcApplicationVersion_ReadOnlyStaticLists(Guid ApVrsn_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcApplicationVersion_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[3][];

                obj[0] = WcApplicationVersion_DataServices.GetWcDatabaseConnectionStringKey_ComboItemList(ApVrsn_Id);
                obj[1] = WcApplicationVersion_DataServices.GetWcStoredProc_ComboItemList(ApVrsn_Id);
                obj[2] = WcApplicationVersion_DataServices.GetWcTableColumn_lstByApVersion_Id_ComboItemList(ApVrsn_Id);

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcApplicationVersion_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcApplicationVersion_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] GetWcStoredProc_ComboItemList(Guid ApVrsn_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ComboItemList", IfxTraceCategory.Enter);
                object[] list = WcApplicationVersion_DataServices.GetWcStoredProc_ComboItemList(ApVrsn_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] GetWcTableColumn_lstByApVersion_Id_ComboItemList(Guid ApVrsn_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByApVersion_Id_ComboItemList", IfxTraceCategory.Enter);
                object[] list = WcApplicationVersion_DataServices.GetWcTableColumn_lstByApVersion_Id_ComboItemList(ApVrsn_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByApVersion_Id_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByApVersion_Id_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] GetWcDatabaseConnectionStringKey_ComboItemList(Guid ApVrsn_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDatabaseConnectionStringKey_ComboItemList", IfxTraceCategory.Enter);
                object[] list = WcApplicationVersion_DataServices.GetWcDatabaseConnectionStringKey_ComboItemList(ApVrsn_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDatabaseConnectionStringKey_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDatabaseConnectionStringKey_ComboItemList", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


