using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  1/7/2018 8:59:34 PM

namespace VelocityService
{

    public partial class WcApplicationService   //: IWcApplicationService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "WcApplicationService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] WcApplication_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetById", IfxTraceCategory.Enter);
                object[] list = WcApplication_DataServices.WcApplication_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcApplication_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetAll", IfxTraceCategory.Enter);
                object[] list = WcApplication_DataServices.WcApplication_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcApplication_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetListByFK", IfxTraceCategory.Enter);
                object[] list = WcApplication_DataServices.WcApplication_GetListByFK();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcApplication_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcApplication_DataServices.WcApplication_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcApplication_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcApplication_DataServices.WcApplication_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] WcApplication_SetIsDeleted(Guid id, bool isDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_SetIsDeleted", new ValuePair[] {new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return WcApplication_DataServices.WcApplication_SetIsDeleted(id, isDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcApplication_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcApplication_DataServices.WcApplication_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcApplication_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcApplication_DataServices.WcApplication_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplication_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods


        [OperationContract]
        public byte[] GetWcApplication_ReadOnlyStaticLists(Guid ApCnSK_Ap_ID )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcApplication_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[3][];

                obj[0] = WcApplication_DataServices.GetWcApplicationConnectionStringKey_ComboItemList(ApCnSK_Ap_ID );
                obj[1] = WcApplication_DataServices.GetWcCulture_ComboItemList();

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcApplication_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcApplication_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


                [OperationContract]
        public byte[]  GetWcCulture_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCulture_ComboItemList", IfxTraceCategory.Enter);
                object[] list = WcApplication_DataServices.GetWcCulture_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCulture_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCulture_ComboItemList", IfxTraceCategory.Leave);
            }
        }


                [OperationContract]
        public byte[]  GetWcApplicationConnectionStringKey_ComboItemList(Guid ApCnSK_Ap_ID )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcApplicationConnectionStringKey_ComboItemList", IfxTraceCategory.Enter);
                object[] list = WcApplication_DataServices.GetWcApplicationConnectionStringKey_ComboItemList(ApCnSK_Ap_ID );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcApplicationConnectionStringKey_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcApplicationConnectionStringKey_ComboItemList", IfxTraceCategory.Leave);
            }
        }


                [OperationContract]
        public void  GetWcAppDbSchema_ComboItemList(Guid Ap_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcAppDbSchema_ComboItemList", IfxTraceCategory.Enter);
                WcApplication_DataServices.GetWcAppDbSchema_ComboItemList(Ap_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcAppDbSchema_ComboItemList", ex);
                throw ex;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcAppDbSchema_ComboItemList", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


