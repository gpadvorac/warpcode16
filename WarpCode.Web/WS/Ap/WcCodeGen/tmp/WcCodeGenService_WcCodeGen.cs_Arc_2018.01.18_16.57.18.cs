using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  1/18/2018 4:21:17 PM

namespace VelocityService
{

    public partial class WcCodeGenService   //: IWcCodeGenService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "WcCodeGenService";

        #endregion Initialize Variables


        [OperationContract]
        public byte[] GetWcCodeGen_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcCodeGen_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[5][];


                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcCodeGen_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcCodeGen_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcCg_GetTableColumns(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTableColumns", IfxTraceCategory.Enter);
                object[] list = WcCodeGen_DataServices.GetWcCg_GetTableColumns(ApVrsn_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTableColumns", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTableColumns", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcCg_GetTables(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTables", IfxTraceCategory.Enter);
                object[] list = WcCodeGen_DataServices.GetWcCg_GetTables(ApVrsn_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTables", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTables", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcCg_ToolTips(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_ToolTips", IfxTraceCategory.Enter);
                object[] list = WcCodeGen_DataServices.GetWcCg_ToolTips(ApVrsn_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_ToolTips", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_ToolTips", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcCg_TableColumnComboColumn(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableColumnComboColumn", IfxTraceCategory.Enter);
                object[] list = WcCodeGen_DataServices.GetWcCg_TableColumnComboColumn(ApVrsn_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableColumnComboColumn", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableColumnComboColumn", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcCg_TableSortBys(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableSortBys", IfxTraceCategory.Enter);
                object[] list = WcCodeGen_DataServices.GetWcCg_TableSortBys(ApVrsn_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableSortBys", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_TableSortBys", IfxTraceCategory.Leave);
            }
        }




    }
}


