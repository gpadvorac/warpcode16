using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  1/18/2018 12:48:27 PM

namespace VelocityService
{

    public partial class WcCodeGenService   //: IWcCodeGenService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "WcCodeGenService";

        #endregion Initialize Variables


        [OperationContract]
        public byte[] GetWcCodeGen_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcCodeGen_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[1][];


                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcCodeGen_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcCodeGen_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public void  GetWcCg_GetTableColumns(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTableColumns", IfxTraceCategory.Enter);
                WcCodeGen_DataServices.GetWcCg_GetTableColumns(ApVrsn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTableColumns", ex);
                throw ex;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcCg_GetTableColumns", IfxTraceCategory.Leave);
            }
        }




    }
}


