using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  12/27/2016 12:46:34 PM

namespace VelocityService
{

    public partial class CommonClientDataService   //: ICommonClientDataService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "CommonClientDataService";

        #endregion Initialize Variables


        [OperationContract]
        public byte[] GetCommonClientData_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_CommonClientData_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[9][];

                obj[0] = CommonClientData_DataServices.GetTaskStatus_ComboItemList();
                obj[1] = CommonClientData_DataServices.GetWcControlType_ComboItemList();
                obj[2] = CommonClientData_DataServices.GetWcDataTypeDotNet_ComboItemList();
                obj[3] = CommonClientData_DataServices.GetWcDataTypeSQL_ComboItemList();
                obj[4] = CommonClientData_DataServices.GetWcStoredProcParamDirection_ComboItemList();
                obj[5] = CommonClientData_DataServices.GetWcStoredProcResultType_ComboItemList();
                obj[6] = CommonClientData_DataServices.GetWcStoredProcReturnType_ComboItemList();
                obj[7] = CommonClientData_DataServices.GetWcTableColumnComboListType_ComboItemList();
                obj[8] = CommonClientData_DataServices.GetWcTableSortByDirection_ComboItemList();

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_CommonClientData_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_CommonClientData_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcTableSortByDirection_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableSortByDirection_ComboItemList", IfxTraceCategory.Enter);
                object[] list = CommonClientData_DataServices.GetWcTableSortByDirection_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableSortByDirection_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableSortByDirection_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcControlType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcControlType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = CommonClientData_DataServices.GetWcControlType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcControlType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcControlType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcDataTypeDotNet_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDotNet_ComboItemList", IfxTraceCategory.Enter);
                object[] list = CommonClientData_DataServices.GetWcDataTypeDotNet_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDotNet_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDotNet_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcDataTypeSQL_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeSQL_ComboItemList", IfxTraceCategory.Enter);
                object[] list = CommonClientData_DataServices.GetWcDataTypeSQL_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeSQL_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeSQL_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcStoredProcResultType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcResultType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = CommonClientData_DataServices.GetWcStoredProcResultType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcResultType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcResultType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcStoredProcReturnType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcReturnType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = CommonClientData_DataServices.GetWcStoredProcReturnType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcReturnType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcReturnType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcTableColumnComboListType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnComboListType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = CommonClientData_DataServices.GetWcTableColumnComboListType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnComboListType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnComboListType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcStoredProcParamDirection_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParamDirection_ComboItemList", IfxTraceCategory.Enter);
                object[] list = CommonClientData_DataServices.GetWcStoredProcParamDirection_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParamDirection_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParamDirection_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetTaskStatus_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", IfxTraceCategory.Enter);
                object[] list = CommonClientData_DataServices.GetTaskStatus_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", IfxTraceCategory.Leave);
            }
        }




    }
}


