﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;
using System.Configuration;
using SLcompression;

namespace VelocityService
{

    public partial class CommonClientDataService   //: ICommonClientDataService
    {

        [OperationContract]
        public string GetHelpDocumentationPath()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetHelpDocumentationPath", IfxTraceCategory.Enter);
                return ConfigurationManager.AppSettings["HelpPath"];
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetHelpDocumentationPath", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetHelpDocumentationPath", IfxTraceCategory.Leave);
            }
        }






        //[OperationContract]
        //public byte[] GetCustomGridData_Compressed(Guid Prj_Id, string SprocName)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCustomGridData_Compressed", new ValuePair[] { new ValuePair("Prj_Id", Prj_Id), new ValuePair("SprocName", SprocName) }, IfxTraceCategory.Enter);
        //        object[] list = null;
        //        switch (SprocName)
        //        {
        //                // Contracts
        //            case "spv_Grid_ContractAndDetail":
        //                list = CommonClientData_DataServices.Getv_Grid_ContractAndDetail(Prj_Id);
        //                break;
        //            case "Cxxx":

        //                break;
        //                // Leases
        //            case "spv_Grid_LeaseAndTract":
        //                list = CommonClientData_DataServices.Getv_Grid_LeaseAndTract(Prj_Id);
        //                break;
        //            case "Lxxx":

        //                break;
        //                // Wells
        //            case "spv_Grid_Well":
        //                list = CommonClientData_DataServices.Getv_Grid_Well(Prj_Id);
        //                break;
        //            case "Wxxx":

        //                break;
        //                // Prospects
        //            case "spv_Grid_Prospect":
        //                list = CommonClientData_DataServices.Getv_Grid_Prospect(Prj_Id);
        //                break;
        //            case "Pxxx":

        //                break;
        //                // Defects
        //            case "spv_Grid_Defect":
        //                list = CommonClientData_DataServices.Getv_Grid_Defect(Prj_Id);
        //                break;
        //            case "Dxxx":

        //                break;
        //                //  Obligations
        //            case "spv_Grid_Obligation":
        //                list = CommonClientData_DataServices.Getv_Grid_Obligation(Prj_Id);
        //                break;
        //            case "Oxxx":

        //                break;
        //                //  Payouts
        //            case "spv_Grid_Payout":
        //                list = CommonClientData_DataServices.Getv_Grid_Payout(Prj_Id);
        //                break;
        //            case "Payxxx":

        //                break;
        //        }

        //        byte[] returnData = null;
        //        if (list != null)
        //        {
        //            returnData = PayloadHelper.ObjectToCompressedBytes(list);
        //        }
        //        return returnData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCustomGridData_Compressed", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCustomGridData_Compressed", IfxTraceCategory.Leave);
        //    }
        //}




        //[OperationContract]
        //public byte[] GetCommonClientData_ReadOnlyStaticLists_All(Guid Prj_Id, Guid UserId)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_CommonClientData_ReadOnlyStaticLists", IfxTraceCategory.Enter);
        //        object[][] lists = new object[24][];

        //        object[][] obj = null;

        //        //GetCommonClientData_ReadOnlyStaticLists
        //        obj = new object[26][];
              
              
        //        obj[17] = CommonClientData_DataServices.GetSysYesNo_ComboItemList();
            
        //        obj[19] = CommonClientData_DataServices.Getv_GridMetaData_lst();
        //        obj[20] = CommonClientData_DataServices.Getv_Report_ComboItemList();
        //        obj[21] = CommonClientData_DataServices.Getv_Report_lstRptExplorer();
        //        obj[22] = CommonClientData_DataServices.Getv_Report_lstAvailableForExplorer();
        //        obj[23] = CommonClientData_DataServices.Getv_Report_lstRptExplorer_Restricted(UserId);
        //        obj[24] = CommonClientData_DataServices.Getv_ReportGroup_lstRptExplorer();
        //        obj[25] = CommonClientData_DataServices.Getv_ReportGroup_lstRptExplorer_Restricted(UserId);
                
        //        // Moved this to reports
        //        //obj[26] = CommonClientData_DataServices.Getv_SysDotNetDataType_ComboItemList();
        //        //obj[27] = CommonClientData_DataServices.Getv_SysSqlDataType_ComboItemList();
        //        lists[0] = obj;



        //        // GetTask_ReadOnlyStaticLists
        //        obj = new object[4][];
        //        obj[0] = Task_DataServices.GetTaskCategory_ComboItemList();
        //        obj[1] = Task_DataServices.GetTaskPriority_ComboItemList();
        //        obj[2] = Task_DataServices.GetTaskStatus_ComboItemList();
        //        obj[3] = Task_DataServices.GetTaskType_ComboItemList();
        //        lists[17] = obj;


        //        //// Getv_MapService_ReadOnlyStaticLists
        //        //obj = new object[1][];
        //        //obj[0] = v_MapService_DataServices.Getv_MapServiceLayerType_ComboItemList();
        //        //lists[18] = obj;


        //        //// Getv_MapServiceLayer_ReadOnlyStaticLists
        //        //obj = new object[3][];
        //        //obj[0] = v_MapServiceLayer_DataServices.Getv_MapServiceLayerShapePointType_ComboItemList();
        //        //obj[1] = v_MapServiceLayer_DataServices.Getv_MapServiceLayerShapeType_ComboItemList();
        //        //obj[2] = v_MapServiceLayer_DataServices.Getv_MapServiceLayerWhereType_ComboItemList();
        //        //lists[19] = obj;



        //        //// Getv_MapServiceLayerFilter_ReadOnlyStaticLists
        //        //obj = new object[2][];
        //        //obj[0] = v_MapServiceLayerFilter_DataServices.Getv_MapServiceFilterDataType_ComboItemList();
        //        //obj[1] = v_MapServiceLayerFilter_DataServices.Getv_MapServiceFilterType_ComboItemList();
        //        //lists[20] = obj;




        //        // GetProject_ReadOnlyStaticLists
        //        obj = new object[1][];
        //        obj[0] = Project_DataServices.GetPerson_lstByProject_ComboItemList(Prj_Id);
        //        lists[21] = obj;


        //        // GetPersonContact_ReadOnlyStaticLists
        //        obj = new object[1][];
        //        obj[0] = PersonContact_DataServices.GetPerson_LU_ComboItemList();
        //        lists[22] = obj;



        //        //// GetRelatedContract_ReadOnlyStaticLists
        //        //obj = new object[4][];
        //        //obj[0] = RelatedContract_DataServices.GetRelatedContractAltNumberSource_ComboItemList(Prj_Id);
        //        //obj[1] = RelatedContract_DataServices.GetRelatedContractStatus_ComboItemList();
        //        //obj[2] = RelatedContract_DataServices.GetRelatedContractType_ComboItemList();
        //        //obj[3] = RelatedContract_DataServices.GetRelatedContractUnitOfMeasure_ComboItemList();

        //        //lists[23] = obj;





        //        byte[] data = Serialization.SilverlightSerializer.Serialize(lists);
        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_CommonClientData_ReadOnlyStaticLists", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_CommonClientData_ReadOnlyStaticLists", IfxTraceCategory.Leave);
        //    }
        //}







    }
}