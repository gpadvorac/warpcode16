﻿using System;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Ifx;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class vScheduleJobService
    {





        [OperationContract]
        public int StartJobSchedulerService()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "StartJobSchedulerService", null, IfxTraceCategory.Enter);



                //Svc.ClientCredentials.Windows.ClientCredential = new NetworkCredential();
                //ServiceSecurityContext dc = ServiceSecurityContext.Current;
                


                //ToDo:  Start the scheduler here...
                // Return zero if it failes to start.
                return 1;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "StartJobSchedulerService", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "StartJobSchedulerService", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public int RunJob(Guid jobId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunJob", new ValuePair[] { new ValuePair("jobId", jobId) }, IfxTraceCategory.Enter);

                JobManager.RunJob(jobId);

                return 1;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunJob", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunJob", IfxTraceCategory.Leave);
            }
        }













    }
}
