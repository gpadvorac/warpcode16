using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  7/21/2015 7:08:03 PM

namespace VelocityService
{

    public partial class vScheduleJobService   //: IvScheduleJobService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "vScheduleJobService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] v_ScheduleJob_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_GetById", IfxTraceCategory.Enter);
                object[] list = v_ScheduleJob_DataServices.v_ScheduleJob_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_ScheduleJob_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_GetAll", IfxTraceCategory.Enter);
                object[] list = v_ScheduleJob_DataServices.v_ScheduleJob_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_ScheduleJob_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_GetListByFK", IfxTraceCategory.Enter);
                object[] list = v_ScheduleJob_DataServices.v_ScheduleJob_GetListByFK();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_ScheduleJob_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_ScheduleJob_DataServices.v_ScheduleJob_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_ScheduleJob_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_ScheduleJob_DataServices.v_ScheduleJob_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] v_ScheduleJob_SetIsDeleted(Guid id, bool isDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_SetIsDeleted", new ValuePair[] {new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return v_ScheduleJob_DataServices.v_ScheduleJob_SetIsDeleted(id, isDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_ScheduleJob_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_ScheduleJob_DataServices.v_ScheduleJob_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_ScheduleJob_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_ScheduleJob_DataServices.v_ScheduleJob_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_ScheduleJob_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods


        [OperationContract]
        public byte[] Getv_ScheduleJob_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_ScheduleJob_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[2][];

                obj[0] = v_ScheduleJob_DataServices.Getv_ScheduleJob_ComboItemList();
                obj[1] = v_ScheduleJob_DataServices.Getv_ScheduleJobType_ComboItemList();

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_ScheduleJob_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_ScheduleJob_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_ScheduleJobType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ScheduleJobType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_ScheduleJob_DataServices.Getv_ScheduleJobType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ScheduleJobType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ScheduleJobType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  Getv_ScheduleJob_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ScheduleJob_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_ScheduleJob_DataServices.Getv_ScheduleJob_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ScheduleJob_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_ScheduleJob_ComboItemList", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


