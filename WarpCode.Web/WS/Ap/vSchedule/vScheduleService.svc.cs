﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Security;
using DataServices;
using EntityWireType;
using Ifx;
using TypeServices;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class vScheduleService
    {


        [OperationContract]
        public byte[] v_Schedule_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Schedule_Save", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_Schedule_DataServices.v_Schedule_Save(data, check);


                DataServiceInsertUpdateResponse obj = new DataServiceInsertUpdateResponse();
                obj.SetFromObjectArray(list);
                if (obj.Result == DataOperationResult.Success)
                {
                    v_Schedule_Values _data = new v_Schedule_Values(obj.CurrentValues, null);
                    //_data.v_Schl_OccursEveryEndTime
                    // Add/Edit job in pool


                    //var job = new Job<SomeData>("Job 1") { Data = new SomeData("every 20 sec", DateTime.Now) };
                    //job.Run.Every.Seconds(20);
                    //Scheduler.SubmitJob(job, LogJobExecution);        
                    
                }
               
                //obj.Exception


                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Schedule_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Schedule_Save", IfxTraceCategory.Leave);
            }
        }


    }
}
