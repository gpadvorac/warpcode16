using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  11/17/2016 8:47:24 PM

namespace VelocityService
{

    public partial class DiscussionSupportService   //: IDiscussionSupportService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "DiscussionSupportService";

        #endregion Initialize Variables



        [OperationContract]
        public byte[]  GetDiscussion_DiscussionsImIn(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_DiscussionsImIn(Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_Application(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Application", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_Application(Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Application", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Application", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_ApplicationMember(Guid Id, Guid MemberId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMember", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_ApplicationMember(Id, MemberId );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMember", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMember", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_ApplicationMy(Guid Id, Guid OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMy", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_ApplicationMy(Id, OwnerId );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMy", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMy", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_ApplicationVersion(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersion", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_ApplicationVersion(Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersion", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersion", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_ApplicationVersionMember(Guid Id, Guid MemberId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMember", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_ApplicationVersionMember(Id, MemberId );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMember", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMember", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_ApplicationVersionMy(Guid Id, Guid OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMy", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_ApplicationVersionMy(Id, OwnerId );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMy", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMy", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_Owner(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Owner", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_Owner(Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Owner", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Owner", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_Table(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Table", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_Table(Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Table", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Table", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_TableMember(Guid Id, Guid MemberId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMember", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_TableMember(Id, MemberId );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMember", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMember", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_TableMy(Guid Id, Guid OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMy", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_TableMy(Id, OwnerId );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMy", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMy", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_Task(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_Task(Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_TaskMy(Guid Id, Guid OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_TaskMy(Id, OwnerId );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_MyDiscussions(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_MyDiscussions(Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetDiscussion_lstBy_TaskMember(Guid Id, Guid MemberId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", IfxTraceCategory.Enter);
                object[] list = DiscussionSupport_DataServices.GetDiscussion_lstBy_TaskMember(Id, MemberId );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", IfxTraceCategory.Leave);
            }
        }




    }
}


