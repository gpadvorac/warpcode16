﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class WcTableService
    {





        [OperationContract]
        public byte[] TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList(Guid Tb_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Enter);
                object[] list = WcTable_DataServices.GetWcTableColumn_lstByTable_ComboItemList(Tb_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList(Guid Tb_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Enter);
                object[] list = WcTable_DataServices.GetWcTableColumn_lstByTable_ComboItemList(Tb_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn22_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList(Guid Tb_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Enter);
                object[] list = WcTable_DataServices.GetWcTableColumn_lstByTable_ComboItemList(Tb_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbChd_ChildForeignKeyColumn33_Id_GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }









    }
}
