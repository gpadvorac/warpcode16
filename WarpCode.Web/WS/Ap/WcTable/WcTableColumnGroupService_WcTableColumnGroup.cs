using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  12/15/2017 5:06:08 PM

namespace VelocityService
{

    public partial class WcTableColumnGroupService   //: IWcTableColumnGroupService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "WcTableColumnGroupService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] WcTableColumnGroup_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = WcTableColumnGroup_DataServices.WcTableColumnGroup_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcTableColumnGroup_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetAll", IfxTraceCategory.Enter);
                object[] list = WcTableColumnGroup_DataServices.WcTableColumnGroup_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcTableColumnGroup_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = WcTableColumnGroup_DataServices.WcTableColumnGroup_GetListByFK(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcTableColumnGroup_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcTableColumnGroup_DataServices.WcTableColumnGroup_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcTableColumnGroup_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcTableColumnGroup_DataServices.WcTableColumnGroup_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] WcTableColumnGroup_SetIsDeleted(Guid id, bool isDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_SetIsDeleted", new ValuePair[] {new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return WcTableColumnGroup_DataServices.WcTableColumnGroup_SetIsDeleted(id, isDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcTableColumnGroup_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcTableColumnGroup_DataServices.WcTableColumnGroup_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcTableColumnGroup_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcTableColumnGroup_DataServices.WcTableColumnGroup_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnGroup_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods


        [OperationContract]
        public byte[] GetWcTableColumnGroup_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcTableColumnGroup_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[1][];


                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcTableColumnGroup_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcTableColumnGroup_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


                [OperationContract]
        public byte[]  GetWcTableColumnGroup_lstByParentId(Guid? TbCGrp_ApVrsn_Id, Guid? TbCGrp_Tb_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnGroup_lstByParentId", IfxTraceCategory.Enter);
                object[] list = WcTableColumnGroup_DataServices.GetWcTableColumnGroup_lstByParentId(TbCGrp_ApVrsn_Id, TbCGrp_Tb_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnGroup_lstByParentId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnGroup_lstByParentId", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


