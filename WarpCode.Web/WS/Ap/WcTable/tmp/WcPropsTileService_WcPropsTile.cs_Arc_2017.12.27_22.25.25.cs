using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  12/27/2017 6:34:48 PM

namespace VelocityService
{

    public partial class WcPropsTileService   //: IWcPropsTileService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "WcPropsTileService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] WcPropsTile_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetById", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = WcPropsTile_DataServices.WcPropsTile_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcPropsTile_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetAll", IfxTraceCategory.Enter);
                object[] list = WcPropsTile_DataServices.WcPropsTile_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcPropsTile_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetListByFK", new ValuePair[] { new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = WcPropsTile_DataServices.WcPropsTile_GetListByFK(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcPropsTile_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_Save", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcPropsTile_DataServices.WcPropsTile_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcPropsTile_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_Delete", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcPropsTile_DataServices.WcPropsTile_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] WcPropsTile_SetIsDeleted(Guid id, bool isDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_SetIsDeleted", new ValuePair[] { new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return WcPropsTile_DataServices.WcPropsTile_SetIsDeleted(id, isDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcPropsTile_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_Deactivate", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcPropsTile_DataServices.WcPropsTile_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcPropsTile_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_Remove", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcPropsTile_DataServices.WcPropsTile_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcPropsTile_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods


        [OperationContract]
        public byte[] GetWcPropsTile_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcPropsTile_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[3][];


                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcPropsTile_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcPropsTile_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] GetWcPropsTileItem_lstAssigned(Guid PrpTl_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcPropsTileItem_lstAssigned", IfxTraceCategory.Enter);
                object[] list = WcPropsTile_DataServices.GetWcPropsTileItem_lstAssigned(PrpTl_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcPropsTileItem_lstAssigned", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcPropsTileItem_lstAssigned", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] GetWcPropsTileItem_lstNotAssigned(Guid Tb_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcPropsTileItem_lstNotAssigned", IfxTraceCategory.Enter);
                object[] list = WcPropsTile_DataServices.GetWcPropsTileItem_lstNotAssigned(Tb_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcPropsTileItem_lstNotAssigned", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcPropsTileItem_lstNotAssigned", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


