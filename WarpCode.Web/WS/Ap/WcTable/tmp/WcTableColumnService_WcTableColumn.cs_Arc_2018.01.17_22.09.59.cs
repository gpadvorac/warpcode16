using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  1/17/2018 3:52:18 PM

namespace VelocityService
{

    public partial class WcTableColumnService   //: IWcTableColumnService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "WcTableColumnService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] WcTableColumn_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = WcTableColumn_DataServices.WcTableColumn_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcTableColumn_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetAll", IfxTraceCategory.Enter);
                object[] list = WcTableColumn_DataServices.WcTableColumn_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcTableColumn_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = WcTableColumn_DataServices.WcTableColumn_GetListByFK(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcTableColumn_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcTableColumn_DataServices.WcTableColumn_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcTableColumn_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcTableColumn_DataServices.WcTableColumn_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] WcTableColumn_SetIsDeleted(Guid id, bool isDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SetIsDeleted", new ValuePair[] {new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return WcTableColumn_DataServices.WcTableColumn_SetIsDeleted(id, isDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcTableColumn_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcTableColumn_DataServices.WcTableColumn_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcTableColumn_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcTableColumn_DataServices.WcTableColumn_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods


        [OperationContract]
        public byte[] GetWcTableColumn_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcTableColumn_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[6][];


                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcTableColumn_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcTableColumn_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[]  ExecutewcTableColumnGroup_AssignColumns(Boolean Insert, Guid TbCGrp2TbC_TbCGrp_Id, Guid TbCGrp2TbC_TbC_Id, Guid TbCGrp2TbC_CreatedUserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableColumnGroup_AssignColumns", IfxTraceCategory.Enter);
                return WcTableColumn_DataServices.ExecutewcTableColumnGroup_AssignColumns(Insert, TbCGrp2TbC_TbCGrp_Id, TbCGrp2TbC_TbC_Id, TbCGrp2TbC_CreatedUserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableColumnGroup_AssignColumns", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableColumnGroup_AssignColumns", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetwcTableColumnGroup_Assignments(Guid Tb_Id, Guid TbC_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableColumnGroup_Assignments", IfxTraceCategory.Enter);
                object[] list = WcTableColumn_DataServices.GetwcTableColumnGroup_Assignments(Tb_Id, TbC_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableColumnGroup_Assignments", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableColumnGroup_Assignments", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[]  GetWcTableColumn_lstNamesFromAppAndDB(Guid Tb_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstNamesFromAppAndDB", IfxTraceCategory.Enter);
                object[] list = WcTableColumn_DataServices.GetWcTableColumn_lstNamesFromAppAndDB(Tb_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstNamesFromAppAndDB", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstNamesFromAppAndDB", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[]  ExecuteWcTableColumn_InsertListOfColumnsFromDb(Guid Tb_Id, Guid? TbC_Id, String ColumnName, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_InsertListOfColumnsFromDb", IfxTraceCategory.Enter);
                return WcTableColumn_DataServices.ExecuteWcTableColumn_InsertListOfColumnsFromDb(Tb_Id, TbC_Id, ColumnName, UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_InsertListOfColumnsFromDb", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_InsertListOfColumnsFromDb", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[]  ExecuteWcTableColumn_Check_UnCheckColumns(String Crit, String Column, Boolean IsChecked, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_Check_UnCheckColumns", IfxTraceCategory.Enter);
                return WcTableColumn_DataServices.ExecuteWcTableColumn_Check_UnCheckColumns(Crit, Column, IsChecked, UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_Check_UnCheckColumns", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_Check_UnCheckColumns", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[]  ExecuteWcTableColumn_Create_TextFieldProperty(Guid TbC_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_Create_TextFieldProperty", IfxTraceCategory.Enter);
                return WcTableColumn_DataServices.ExecuteWcTableColumn_Create_TextFieldProperty(TbC_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_Create_TextFieldProperty", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_Create_TextFieldProperty", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


