using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  12/28/2017 12:08:27 AM

namespace VelocityService
{

    public partial class WcGridColumnGroupService   //: IWcGridColumnGroupService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "WcGridColumnGroupService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] WcGridColumnGroup_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = WcGridColumnGroup_DataServices.WcGridColumnGroup_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcGridColumnGroup_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetAll", IfxTraceCategory.Enter);
                object[] list = WcGridColumnGroup_DataServices.WcGridColumnGroup_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcGridColumnGroup_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = WcGridColumnGroup_DataServices.WcGridColumnGroup_GetListByFK(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcGridColumnGroup_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcGridColumnGroup_DataServices.WcGridColumnGroup_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcGridColumnGroup_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcGridColumnGroup_DataServices.WcGridColumnGroup_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] WcGridColumnGroup_SetIsDeleted(Guid id, bool isDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_SetIsDeleted", new ValuePair[] {new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return WcGridColumnGroup_DataServices.WcGridColumnGroup_SetIsDeleted(id, isDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcGridColumnGroup_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcGridColumnGroup_DataServices.WcGridColumnGroup_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] WcGridColumnGroup_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = WcGridColumnGroup_DataServices.WcGridColumnGroup_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods


        [OperationContract]
        public byte[] GetWcGridColumnGroup_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcGridColumnGroup_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[3][];


                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcGridColumnGroup_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_WcGridColumnGroup_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


                [OperationContract]
        public object[]  ExecuteWcGridColumnGroup_AssignItems(Boolean Insert, Guid? GrdColGrpItm_Id, Guid? GrdColGrpItm_Grp_Id, Guid? GrdColGrpItm_TbC_Id, Guid? GrdColGrpItm_UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcGridColumnGroup_AssignItems", IfxTraceCategory.Enter);
                return WcGridColumnGroup_DataServices.ExecuteWcGridColumnGroup_AssignItems(Insert, GrdColGrpItm_Id, GrdColGrpItm_Grp_Id, GrdColGrpItm_TbC_Id, GrdColGrpItm_UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcGridColumnGroup_AssignItems", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcGridColumnGroup_AssignItems", IfxTraceCategory.Leave);
            }
        }


                [OperationContract]
        public byte[]  GetWcGridColumnGroupItem_lstAssigned(Guid Grp_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstAssigned", IfxTraceCategory.Enter);
                object[] list = WcGridColumnGroup_DataServices.GetWcGridColumnGroupItem_lstAssigned(Grp_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstAssigned", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstAssigned", IfxTraceCategory.Leave);
            }
        }


                [OperationContract]
        public byte[]  GetWcGridColumnGroupItem_lstNotAssigned(Guid Tb_Id, Guid Grp_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstNotAssigned", IfxTraceCategory.Enter);
                object[] list = WcGridColumnGroup_DataServices.GetWcGridColumnGroupItem_lstNotAssigned(Tb_Id, Grp_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstNotAssigned", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstNotAssigned", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


