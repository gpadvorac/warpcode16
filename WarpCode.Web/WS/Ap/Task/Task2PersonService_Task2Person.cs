using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  12/8/2012 12:09:01 AM

namespace VelocityService
{

    public partial class Task2PersonService   //: ITask2PersonService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "Task2PersonService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] Task2Person_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = Task2Person_DataServices.Task2Person_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] Task2Person_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetAll", IfxTraceCategory.Enter);
                object[] list = Task2Person_DataServices.Task2Person_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] Task2Person_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                object[] list = Task2Person_DataServices.Task2Person_GetListByFK(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] Task2Person_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_Save", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = Task2Person_DataServices.Task2Person_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] Task2Person_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = Task2Person_DataServices.Task2Person_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_Delete", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] Task2Person_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = Task2Person_DataServices.Task2Person_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] Task2Person_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = Task2Person_DataServices.Task2Person_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods



        [OperationContract]
        public byte[] GetPerson_lstAssignedToTask(Guid Tk_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstAssignedToTask", IfxTraceCategory.Enter);
                object[] list = Task2Person_DataServices.GetPerson_lstAssignedToTask(Tk_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstAssignedToTask", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstAssignedToTask", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] GetPerson_lstNotAssignedToTask(Guid Tk_Id, Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToTask", IfxTraceCategory.Enter);
                object[] list = Task2Person_DataServices.GetPerson_lstNotAssignedToTask(Tk_Id, Pj_Id );
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToTask", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToTask", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[]  ExecuteTask2Person_assignPerson(Guid Tk2Pn_Tk_Id, Guid Tk2Pn_Pn_Id, Guid Tk2Pn_UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_assignPerson", IfxTraceCategory.Enter);
                return Task2Person_DataServices.ExecuteTask2Person_assignPerson(Tk2Pn_Tk_Id, Tk2Pn_Pn_Id, Tk2Pn_UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_assignPerson", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_assignPerson", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[]  ExecuteTask2Person_removePerson(Guid Tk2Pn_Tk_Id, Guid Tk2Pn_Pn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_removePerson", IfxTraceCategory.Enter);
                return Task2Person_DataServices.ExecuteTask2Person_removePerson(Tk2Pn_Tk_Id, Tk2Pn_Pn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_removePerson", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_removePerson", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


