﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class vNotificationEventService
    {





        [OperationContract]
        public byte[] v_NotificationEvent_InsertEventAndLogItems(Guid Pj_Id, Guid Ntfc_Id, Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EmailTeamMembers_IdeasPastDue", new ValuePair[] { new ValuePair("Pj_Id", Pj_Id) }, IfxTraceCategory.Enter);

                int iSuccess = 0;
                Guid eventId = Guid.NewGuid();
                iSuccess = v_NotificationEvent_DataServices.ExecuteNotificationEvent_AndLogs_insert(Pj_Id, Ntfc_Id, eventId, userId);


                object[][] obj = new object[2][];

                obj[0] = v_NotificationEvent_DataServices.v_NotificationEvent_GetById(eventId);
                obj[1] = v_NotificationLog_DataServices.v_NotificationLog_GetListByFK(eventId);

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EmailTeamMembers_IdeasPastDue", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EmailTeamMembers_IdeasPastDue", IfxTraceCategory.Leave);
            }
        }




    }
}
