using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;

// Gen Timestamp:  9/17/2015 11:32:15 AM

namespace VelocityService
{

    public partial class vNotificationService   //: IvNotificationService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "vNotificationService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] v_Notification_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_GetById", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.v_Notification_GetById(id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Notification_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_GetAll", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.v_Notification_GetAll();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Notification_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_GetListByFK", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.v_Notification_GetListByFK();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Notification_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_Save", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.v_Notification_Save(data, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_Save", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Notification_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_Delete", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.v_Notification_Delete(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_Delete", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] v_Notification_SetIsDeleted(Guid id, bool isDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_SetIsDeleted", new ValuePair[] { new ValuePair("isDeleted", isDeleted), new ValuePair("id", id) }, IfxTraceCategory.Enter);
                return v_Notification_DataServices.v_Notification_SetIsDeleted(id, isDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_SetIsDeleted", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Notification_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_Deactivate", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.v_Notification_Deactivate(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] v_Notification_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_Remove", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.v_Notification_Remove(data);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "v_Notification_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods


        [OperationContract]
        public byte[] Getv_Notification_ReadOnlyStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_Notification_ReadOnlyStaticLists", IfxTraceCategory.Enter);
                object[][] obj = new object[8][];

                obj[0] = v_Notification_DataServices.Getv_NotificationContentTemplate_ComboItemList();
                obj[1] = v_Notification_DataServices.Getv_NotificationContentType_ComboItemList();
                obj[2] = v_Notification_DataServices.Getv_NotificationEventType_ComboItemList();
                obj[3] = v_Notification_DataServices.Getv_NotificationRecipientContentSourceType_ComboItemList();
                obj[4] = v_Notification_DataServices.Getv_NotificationRecipientSourceType_ComboItemList();
                obj[5] = v_Notification_DataServices.Getv_NotificationSsrsFormat_ComboItemList();
                obj[6] = v_Notification_DataServices.Getv_NotificationType_ComboItemList();
                obj[7] = v_Notification_DataServices.Getv_Report_cmb_ComboItemList();

                byte[] data = Serialization.SilverlightSerializer.Serialize(obj);
                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_Notification_ReadOnlyStaticLists", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Get_v_Notification_ReadOnlyStaticLists", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_NotificationContentTemplate_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationContentTemplate_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.Getv_NotificationContentTemplate_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationContentTemplate_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationContentTemplate_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_NotificationContentType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationContentType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.Getv_NotificationContentType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationContentType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationContentType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_NotificationEventType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationEventType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.Getv_NotificationEventType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationEventType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationEventType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_NotificationRecipientContentSourceType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipientContentSourceType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.Getv_NotificationRecipientContentSourceType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipientContentSourceType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipientContentSourceType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_NotificationRecipientSourceType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipientSourceType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.Getv_NotificationRecipientSourceType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipientSourceType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipientSourceType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_NotificationType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationType_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.Getv_NotificationType_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationType_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_Report_cmb_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_cmb_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.Getv_Report_cmb_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_cmb_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Report_cmb_ComboItemList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_Notification_row_BySchlJb_Id(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Notification_row_BySchlJb_Id", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.Getv_Notification_row_BySchlJb_Id(Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Notification_row_BySchlJb_Id", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_Notification_row_BySchlJb_Id", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_NotificationRecipient_Assigned(Guid SchlJb_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipient_Assigned", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.Getv_NotificationRecipient_Assigned(SchlJb_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipient_Assigned", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipient_Assigned", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_NotificationRecipient_UnAssignedAll(Guid SchlJb_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipient_UnAssignedAll", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.Getv_NotificationRecipient_UnAssignedAll(SchlJb_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipient_UnAssignedAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipient_UnAssignedAll", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_NotificationRecipient_UnAssignedByProject(Guid Prj_Id, Guid SchlJb_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipient_UnAssignedByProject", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.Getv_NotificationRecipient_UnAssignedByProject(Prj_Id, SchlJb_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipient_UnAssignedByProject", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipient_UnAssignedByProject", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] Executev_NotificationRecipient_assignedPerson(Guid SchlJb_Id, Guid Pn_Id, Guid UserId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_NotificationRecipient_assignedPerson", IfxTraceCategory.Enter);
                return v_Notification_DataServices.Executev_NotificationRecipient_assignedPerson(SchlJb_Id, Pn_Id, UserId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_NotificationRecipient_assignedPerson", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_NotificationRecipient_assignedPerson", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] Executev_NotificationRecipient_removePerson(Guid SchlJb_Id, Guid Pn_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_NotificationRecipient_removePerson", IfxTraceCategory.Enter);
                return v_Notification_DataServices.Executev_NotificationRecipient_removePerson(SchlJb_Id, Pn_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_NotificationRecipient_removePerson", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executev_NotificationRecipient_removePerson", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] GetRecipientList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRecipientList", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.GetRecipientList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRecipientList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetRecipientList", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_NotificationRecipient_ListByJobId(Guid SchlJb_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipient_ListByJobId", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.Getv_NotificationRecipient_ListByJobId(SchlJb_Id);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipient_ListByJobId", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationRecipient_ListByJobId", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] Getv_NotificationSsrsFormat_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationSsrsFormat_ComboItemList", IfxTraceCategory.Enter);
                object[] list = v_Notification_DataServices.Getv_NotificationSsrsFormat_ComboItemList();
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationSsrsFormat_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getv_NotificationSsrsFormat_ComboItemList", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


