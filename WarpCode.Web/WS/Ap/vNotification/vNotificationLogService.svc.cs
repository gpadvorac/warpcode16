﻿using DataServices;
using Ifx;
using System;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class vNotificationLogService
    {






        [OperationContract]
        public object[] SendLogNotice(Guid logId, string subject, string contentHeader, string content, string addressTo)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EmailTeamMembers_IdeasPastDue", new ValuePair[] { new ValuePair("logId", logId), new ValuePair("subject", subject), new ValuePair("content", content), new ValuePair("addressTo", addressTo) }, IfxTraceCategory.Enter);

                string host = System.Configuration.ConfigurationManager.AppSettings["MailSmtpServer"];
                string userName = System.Configuration.ConfigurationManager.AppSettings["MailAdminName"];
                string userPW = System.Configuration.ConfigurationManager.AppSettings["MailAdminPassword"];
                string mailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
                int iSuccess = 0;
                
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(mailFrom);
                mail.To.Add(addressTo);
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Subject = subject;
                mail.Body = contentHeader + System.Environment.NewLine + System.Environment.NewLine + content;


                SmtpClient smtp = new SmtpClient(host);
                smtp.Credentials = new System.Net.NetworkCredential(userName, userPW);
                string error = "";
                string status = "Success";
                try
                {
                    smtp.Send(mail);
                }
                catch (Exception exx)
                {
                    status = "Failed";
                    error = exx.Message;
                    if (exx.InnerException != null)
                    {
                        error = error + System.Environment.NewLine + exx.InnerException.Message;
                        if (exx.InnerException.InnerException != null)
                        {
                            error = error + System.Environment.NewLine + exx.InnerException.InnerException.Message;
                        }
                    }
                }

                iSuccess = v_NotificationLog_DataServices.v_NotificationLog_UpdateAfterSendingNotice(logId, status, error);
                if (iSuccess != 1)
                {
                    string msg = "Then notice was sent, but there was a problem in updating this log item in the database.  Please check with the recipient to see if the notice was recieved.";
                    if (error.Length == 0)
                    {
                        error = msg;
                    }
                    else
                    {
                        error = error + System.Environment.NewLine + msg;
                    }
                }

                object[] data = new object[3];
                data[0] = logId;
                data[1] = status;
                data[2] = error;
                return data;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EmailTeamMembers_IdeasPastDue", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EmailTeamMembers_IdeasPastDue", IfxTraceCategory.Leave);
            }
        }




   








    }
}
