﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;
using vDataServices;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class vNotificationService
    {









        /// <summary>
        /// Sample from VE Master
        /// or look in NotificationMailHelper.cs
        /// </summary>
        /// <param name="Ntfc_Id"></param>
        /// <param name="Pj_Id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        //[OperationContract]
        //public int EmailTeamMembers_IdeasPastDue(Guid Ntfc_Id, Guid Pj_Id, Guid userId)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EmailTeamMembers_IdeasPastDue", new ValuePair[] { new ValuePair("Pj_Id", Pj_Id) }, IfxTraceCategory.Enter);
        //        object[] list = Person_DataServices.GetPerson_lstWithIdeasPastDueByProjectId(Pj_Id);
        //        string host = System.Configuration.ConfigurationManager.AppSettings["MailSmtpServer"];
        //        string userName = System.Configuration.ConfigurationManager.AppSettings["MailAdminName"];
        //        string userPW = System.Configuration.ConfigurationManager.AppSettings["MailAdminPassword"];
        //        string mailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
        //        int iSuccess = 0;
        //        if (list != null && list.Length > 0)
        //        {


        //            string subject = "VE Master has Ideas Past Due for ";
        //            Guid eventId = Guid.NewGuid();
        //            iSuccess = DataServices.v_NotificationEvent_DataServices.ExecuteNotificationEvent_insert(eventId, Ntfc_Id, Pj_Id, subject, "", userId);
        //            // MailServer credentials


        //            foreach (object[] item in list)
        //            {
        //                Guid? Pn_Id = item[0] as Guid?;
        //                if (Pn_Id == null)
        //                {
        //                    Exception exx = new Exception("Person ID returned null after calling Person_DataServices.GetPerson_lstWithIdeasPastDueByProjectId.  Pj_Id = " + Pj_Id.ToString());
        //                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EmailTeamMembers_IdeasPastDue", exx);
        //                }
        //                else
        //                {
        //                    string first = item[1] as string;
        //                    string last = item[2] as string;
        //                    string addressTo = item[3] as string;
        //                    string content = ""; //v_Notification_DataServices.GetIdeasPastDueForAnalysisOrApprovalByProjectAndPerson_ForEmailBody(Pj_Id, (Guid)Pn_Id);

        //                    MailMessage mail = new MailMessage();
        //                    mail.From = new MailAddress(mailFrom);
        //                    mail.To.Add(addressTo);
        //                    mail.BodyEncoding = System.Text.Encoding.UTF8;
        //                    mail.SubjectEncoding = System.Text.Encoding.UTF8;
        //                    mail.Subject = subject + first + " " + last;
        //                    mail.Body = ((string)first + " " + last).Trim() + "," + System.Environment.NewLine + System.Environment.NewLine + content;

        //                    NotificationLogItem log = new NotificationLogItem(Guid.NewGuid(), eventId, subject, content, mailFrom, addressTo, userId);

        //                    SmtpClient smtp = new SmtpClient(host);
        //                    smtp.Credentials = new System.Net.NetworkCredential(userName, userPW);
        //                    smtp.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
        //                    smtp.SendAsync(mail, log);
        //                }

        //            }
        //        }
        //        return 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EmailTeamMembers_IdeasPastDue", ex);
        //        return 0;
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EmailTeamMembers_IdeasPastDue", IfxTraceCategory.Leave);
        //    }
        //}














    }
}
