using System;
using System.ServiceModel;
using DataServices;
using Ifx;
using Serialization;

// Gen Timestamp:  8/23/2012 1:31:55 PM

namespace VelocityService
{

    public partial class FileStorageService   //: IFileStorageService
    {

        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "FileStorageService";

        #endregion Initialize Variables


        #region Base Methods


        [OperationContract]
        public byte[] FileStorage_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_GetById", IfxTraceCategory.Enter);
                object[] list = FileStorage_DataServices.FileStorage_GetById(id);
                byte[] returnData = SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_GetById", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_GetById", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] FileStorage_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_GetAll", IfxTraceCategory.Enter);
                object[] list = FileStorage_DataServices.FileStorage_GetAll();
                byte[] returnData = SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_GetAll", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_GetAll", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] FileStorage_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_GetListByFK", IfxTraceCategory.Enter);
                object[] list = FileStorage_DataServices.FileStorage_GetListByFK();
                byte[] returnData = SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] FileStorage_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = FileStorage_DataServices.FileStorage_Deactivate(data);
                byte[] returnData = SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Deactivate", IfxTraceCategory.Leave);
            }
        }

        [OperationContract]
        public byte[] FileStorage_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] list = FileStorage_DataServices.FileStorage_Remove(data);
                byte[] returnData = SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods


        #region Other Methods



        [OperationContract]
        public byte[] GetFileStorage_lstByLinkedRecord(Guid Id, String Type )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedRecord", IfxTraceCategory.Enter);
                object[] list = FileStorage_DataServices.GetFileStorage_lstByLinkedRecord(Id, Type );
                byte[] returnData = SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedRecord", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedRecord", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] GetFileStorage_lstByLinkedXRefRecords(Guid Id, String Type )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedXRefRecords", IfxTraceCategory.Enter);
                object[] list = FileStorage_DataServices.GetFileStorage_lstByLinkedXRefRecords(Id, Type );
                byte[] returnData = SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedXRefRecords", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedXRefRecords", IfxTraceCategory.Leave);
            }
        }





        #endregion Other Methods

    }
}


