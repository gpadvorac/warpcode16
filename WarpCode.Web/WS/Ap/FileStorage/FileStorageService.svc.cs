﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using DataServices;
using Ifx;
using EntityWireType;
using TypeServices;
using System.IO;

using System.Web;
using System.Web.UI;
using System.Threading;

namespace VelocityService
{
    [ServiceContract(Namespace = "")]
    [SilverlightFaultBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class FileStorageService
    {




        [OperationContract]
        public byte[] FileStorage_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Save", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                FileStorage_Values obj = new FileStorage_Values(data, null);
                object[] list = FileStorage_DataServices.FileStorage_Save(obj, check);
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(list);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Save", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Save", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public object[] SaveFile(Byte[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                int success = 0;
                object[] returnData = new object[1];
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array == null)
                {
                    IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", new Exception("variable 'array' was null after deserializing the data."));
                    returnData[0] = -1;
                    return returnData;
                }
                FileStorage_Values obj = new FileStorage_Values(array, null);

                if (obj.IsNew == true)
                {
                    // Save file to disk
                    success = WriteFileToDisk(obj);
                    if (success == 1)
                    {
                        // Save info to db
                        // Since this is new, we need to generate the file path.
                        string path = System.Configuration.ConfigurationManager.AppSettings["FileStoragePath"];
                        path = path + obj.FS_ParentType;
                        obj.FS_FilePath_noevents = path;

                        object[] response = FileStorage_DataServices.FileStorage_Save(obj, 0);
                        DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse(response);
                        if (dataServiceResponse.Result == DataOperationResult.Success)
                        {
                            returnData[0] = 1;
                            return returnData;
                        }
                        else
                        {
                            // Fail, so remove the inserted row from the db and return zero
                            FileStorage_DataServices.FileStorage_Delete(array);
                            returnData[0] = 0;
                            return returnData;
                        }
                    }
                }
                else
                {
                    // update db
                    object[] response = FileStorage_DataServices.FileStorage_Save(obj, 0);
                    DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse(response);
                    if (dataServiceResponse.Result == DataOperationResult.Success)
                    {
                        // write the file to disk  (overwriting the old one)
                        success = WriteFileToDisk(obj);
                        returnData[0] = success;
                        return returnData;
                    }
                    else
                    {
                        //// Fail, so  return zero
                        returnData[0] = 0;
                        return returnData;
                    }
                }


                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", IfxTraceCategory.Leave);
            }
        }




        int WriteFileToDisk(FileStorage_Values data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);

                //FileStoragePath
                string path = System.Configuration.ConfigurationManager.AppSettings["FileStoragePath"];

                // Check to see if the folder exists for this file
                if (!Directory.Exists(path + data.FS_ParentType))
                {
                    // It doesnt exist, so create it
                    Directory.CreateDirectory(path + data.FS_ParentType);
                }
                path = path + data.FS_ParentType + @"\" + data.FS_Id.ToString() + "_" + data.FS_FileName;

                File.WriteAllBytes(path, data.FileData);

                return 1;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", ex);
                return 0;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", IfxTraceCategory.Leave);
            }
        }



        [OperationContract]
        public Byte[] GetFile(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            FileStream fs = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFile", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);

                FileStorage_Values obj = new FileStorage_Values(data, null);

                string path = System.Configuration.ConfigurationManager.AppSettings["FileStoragePath"];
                path = path + obj.FS_ParentType + @"\" + obj.FS_Id.ToString() + "_" + obj.FS_FileName;

                // Check to see if the file exists.  if not, return null
                if (!File.Exists(path))
                {
                    string msg = "File Not Found:  " + path;
                    IfxEvent.PublishTrace(traceId, _as, _cn, "GetFile", new Exception(msg));
                    return null;
                }


                fs = default(FileStream);
                fs = File.Open(path, FileMode.Open, FileAccess.Read);
                long lngLen = fs.Length;
                byte[] abytBuffer = new byte[Convert.ToInt32(lngLen - 1) + 1];
                fs.Read(abytBuffer, 0, Convert.ToInt32(lngLen));

                obj.FileData_noevents = abytBuffer;

                byte[] returnData = Serialization.SilverlightSerializer.Serialize(obj.GetValues());
                return returnData;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFile", ex);
                return null;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs = null;
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFile", IfxTraceCategory.Leave);
            }
        }



        //[OperationContract]
        //public Byte[] GetFileName(object[] data)
        //{
        //    //  Note:  This WS name is missleading.  It returns the full URL path to the file
        //    Guid? traceId = Guid.NewGuid();
        //    FileStream fs = null;
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFile", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);

        //        FileStorage_Values obj = new FileStorage_Values(data, null);

        //        string path = System.Configuration.ConfigurationManager.AppSettings["FileStorageFolder"];
        //        string webServer = System.Configuration.ConfigurationManager.AppSettings["WebServerUrl"];

        //        path = webServer + @"\" + path + obj.FS_ParentType + @"\" + obj.FS_Id.ToString() + "_" + obj.FS_FileName;

        //        string[] returnStirngs = new string[2];
        //        returnStirngs[0] = path;
        //        returnStirngs[1] = obj.FS_FileName;

        //        byte[] returnData = Serialization.SilverlightSerializer.Serialize(returnStirngs);
        //        return returnData;

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFile", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (fs != null)
        //        {
        //            fs.Close();
        //            fs = null;
        //        }
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFile", IfxTraceCategory.Leave);
        //    }
        //}



        [OperationContract]
        public Byte[] GetFilePathFromTempDir(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            FileStream fs = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilePathFromTempDir", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);

                FileStorage_Values obj = new FileStorage_Values(data, null);

                string urlPath;
                string fileStoragePath = System.Configuration.ConfigurationManager.AppSettings["FileStoragePath"];
                string fileStorageFolder = System.Configuration.ConfigurationManager.AppSettings["FileStorageFolder"];
                string tempFileStorageFolder = System.Configuration.ConfigurationManager.AppSettings["TempFileStorageFolder"];
                string tempFileStorageFolderForURL = System.Configuration.ConfigurationManager.AppSettings["TempFileStorageFolderForURL"];

                Guid folderId = Guid.NewGuid();

                string originalFilePath = obj.FS_ParentType + @"\" + obj.FS_Id.ToString() + "_" + obj.FS_FileName;

                // Create a temp directory where we will open the file from
                DirectoryInfo di = Directory.CreateDirectory(tempFileStorageFolder + folderId.ToString());

                // Check to see if the file exists.  if not, return null
                if (!File.Exists(fileStoragePath + originalFilePath))
                {
                    string msg = "File Not Found:  " + fileStoragePath + originalFilePath;
                    IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilePathFromTempDir", new Exception(msg));
                    return null;
                }
                File.Copy(fileStoragePath + originalFilePath, tempFileStorageFolder + folderId.ToString() + @"\" + obj.FS_FileName);

                string webServer = System.Configuration.ConfigurationManager.AppSettings["WebServerUrl"];
                urlPath = webServer + @"\" + tempFileStorageFolderForURL + folderId.ToString() + @"\" + obj.FS_FileName;
                string fileExt = GetFileExtention(obj.FS_FileName);



                string[] returnStirngs = new string[4];
                // Need to replace any back slaches with forward slashes otherwise we will get an error:  "Invalid URI: The hostname could not be parsed."
                // Back slaches work when url is on the local machine or when pasting into the browser, but not here.
                string newString = @"/";
                urlPath = urlPath.Replace(@"\", newString);

                returnStirngs[0] = urlPath;
                returnStirngs[1] = folderId.ToString();
                returnStirngs[2] = obj.FS_FileName;
                //returnStirngs[3] = fileExt;
                string flgOpenInBrowser = System.Configuration.ConfigurationManager.AppSettings[fileExt];
                returnStirngs[3] = flgOpenInBrowser;


                byte[] returnData = Serialization.SilverlightSerializer.Serialize(returnStirngs);
                return returnData;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilePathFromTempDir", ex);
                return null;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs = null;
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFilePathFromTempDir", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// A Helper method to get the file extention from the file name
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        string GetFileExtention(string fileName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileExtention", IfxTraceCategory.Enter);

                int pos = fileName.LastIndexOf(".");
                if (pos < 1)
                {
                    return null;
                }

                pos = pos + 1;
                if (pos == fileName.Length)
                {
                    return null;
                }

                string fileExtention = fileName.Substring(pos, fileName.Length - pos);
                return fileExtention;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileExtention", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileExtention", IfxTraceCategory.Leave);
            }
        }



        [OperationContract]
        public byte[] FileStorage_DeleteTempFile(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteTempFile", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);

                object[] result = new object[1];
                if (data == null)
                {
                    result[0] = 0;
                }
                else
                {
                    string tempFolderId = data[0] as string;
                    string tempFileStorageFolder = System.Configuration.ConfigurationManager.AppSettings["TempFileStorageFolder"];

                    if (tempFolderId == null || tempFolderId.Trim().Length == 0)
                    {
                        result[0] = 0;

                    }
                    else
                    {
                        tempFileStorageFolder = tempFileStorageFolder + tempFolderId;

                        new Thread(() =>
                        {
                            Thread.CurrentThread.IsBackground = false;
                            /* run your code here */
                            FileStorage_DeleteTempFile_NewThread(tempFileStorageFolder);
                        }).Start();

                        result[0] = 1;
                    }
                }

                byte[] returnData = Serialization.SilverlightSerializer.Serialize(result);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteTempFile", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteTempFile", IfxTraceCategory.Leave);
            }
        }


        static void FileStorage_DeleteTempFile_NewThread(string tempPath)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteTempFile", new ValuePair[] { new ValuePair("tempPath", tempPath) }, IfxTraceCategory.Enter);
                string deleteWaitTimeAsString = System.Configuration.ConfigurationManager.AppSettings["TempFileStorageDeleteWaitTime"];
                int deleteWaitTime = 30000;
                bool parsed;
                try
                {
                    parsed = int.TryParse(deleteWaitTimeAsString, out deleteWaitTime);
                }
                catch (Exception exx)
                { }


                if (tempPath == null || tempPath.Trim().Length == 0)
                {

                }
                else
                {
                    // Wait for x seconds before deleting the file.  Need to make sure the client had time to dowload it first.
                    System.Threading.Thread.Sleep(deleteWaitTime);
                    Directory.Delete(tempPath, true);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteTempFile", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_DeleteTempFile", IfxTraceCategory.Leave);
            }
        }


        [OperationContract]
        public byte[] FileStorage_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Delete", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                object[] result = new object[1];
                int success = FileStorage_DataServices.FileStorage_Delete(data);
                if (success == 1)
                {
                    FileStorage_Values obj = new FileStorage_Values(data, null);
                    string path = System.Configuration.ConfigurationManager.AppSettings["FileStoragePath"];
                    path = path + obj.FS_ParentType + @"\" + obj.FS_Id.ToString() + "_" + obj.FS_FileName;
                    System.IO.File.Delete(path);
                    result[0] = 1;
                }
                else
                {
                    result[0] = 0;
                }
                byte[] returnData = Serialization.SilverlightSerializer.Serialize(result);
                return returnData;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Delete", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Delete", IfxTraceCategory.Leave);
            }
        }



        [OperationContract]
        public object[] RenameFile(Guid fs_Id, string parentType, string oldFileName, string newFileName, Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", null, IfxTraceCategory.Enter);
                int success = 0;
                object[] returnData = new object[1];
                //object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                //if (array == null)
                //{
                //    IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", new Exception("variable 'array' was null after deserializing the data."));
                //    returnData[0] = -1;
                //    return returnData;
                //}

                //FileStorage_Values obj = new FileStorage_Values((object[])array[0], null);
                //string newName = array[1] as string;




                // Rename file
                // If successful, save new name to database
                // return success or fail
                string oldNamePath = System.Configuration.ConfigurationManager.AppSettings["FileStoragePath"];
                oldNamePath = oldNamePath + parentType + @"\" + fs_Id.ToString() + "_" + oldFileName;
                string newNamePath = System.Configuration.ConfigurationManager.AppSettings["FileStoragePath"];
                newNamePath = newNamePath + parentType + @"\" + fs_Id.ToString() + "_" + newFileName;
                if (!File.Exists(oldNamePath))
                {
                    returnData[0] = -1;  // File doesnt exist
                    return returnData;
                }
                else if (File.Exists(newNamePath))
                {
                    returnData[0] = -2;  // File with the new name already exists.
                    return returnData;
                }
                else
                {
                    try
                    {
                        FileInfo file = new FileInfo(oldNamePath);
                        file.MoveTo(newNamePath);
                    }
                    catch (Exception exx)
                    {
                        string info = "There was a problem renaming " + oldNamePath + " to " + newNamePath + ".";
                        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RenameFile", info, null, null, exx, IfxTraceCategory.Catch);
                        returnData[0] = 1;
                        return returnData;
                    }
                    success = FileStorage_DataServices.FileStorage_RenameFile(fs_Id, newFileName, userId);
                    if (success == 1)
                    {
                        returnData[0] = 1;
                        return returnData;
                    }
                    else
                    {
                        try
                        {
                            FileInfo file = new FileInfo(oldNamePath);
                            file.MoveTo(newNamePath);
                            returnData[0] = success;
                            return returnData;
                        }
                        catch (Exception exx)
                        {
                            string info = "The file was renamed " + oldNamePath + " to " + newNamePath + ", but there was a problem making the name change in the database and we could not roll back the name change on the file.";
                            if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RenameFile", info, null, null, exx, IfxTraceCategory.Catch);
                            returnData[0] = 1;
                            return returnData;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveFile", IfxTraceCategory.Leave);
            }
        }





    }
}
