﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ifx;

namespace VelocityService
{
    public partial class JobManagerService : System.Web.UI.Page
    {




        #region Initialize Variables

        private static string _as = "VelocityService";
        private static string _cn = "JobManagerService";

        #endregion Initialize Variables



        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod()]
        public static string RunJob(string authKey, string message)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RunJob", IfxTraceCategory.Enter);

                string serviceAuthKey = ConfigurationManager.AppSettings["SchedulerWindowsServiceWebMethodKey"];
                string returnMessage = string.Empty;

                if (serviceAuthKey.Equals(authKey))
                {
                    try
                    {
                        Guid jobId = new Guid(message);
                        JobManager.RunJob(jobId);
                    }
                    catch (Exception)
                    {
                        string paramValue = "null";
                        if (message != null)
                        {
                            paramValue = message;
                        }
                        string msg = "Parameter 'message' could not be converted to a Guid for the Job ID.  Paramter value was " + paramValue;
                        IfxEvent.PublishTrace(traceId, _as, _cn, "RunJob", new Exception(msg));
                    }
                }
                else
                {
                    string paramValue = "null";
                    if (authKey != null)
                    {
                        paramValue = authKey;
                    }

                    string msg = "Authentication Key does not matched.  Key value was " + paramValue;
                    IfxEvent.PublishTrace(traceId, _as, _cn, "RunJob", new Exception(msg));
                    returnMessage = "Error: Authentication Key does not matched.";
                }

                return returnMessage;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RunJob", ex);
                return "Error: There was a problem.  Check the exception logs.";
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RunJob", IfxTraceCategory.Leave);
            }
        }





    }
}