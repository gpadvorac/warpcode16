﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using Ifx;
using Microsoft.Reporting.WebForms;
using vReportParameter;
using System.Net;
using System.Configuration;
using System.Security.Principal;
using System.Text;

namespace EntityService
{
    public partial class VelocityReportService : System.Web.UI.Page
    {

        private static string _as = "VelocityServer_v01";
        private static string _cn = "VelocityReportService";

        string _qsParam;

        protected void Page_Load(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Page_Load", IfxTraceCategory.Enter);
                if (!Page.IsPostBack)
                {
                    //if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Page_Load:  Test vReport rpt", new Exception("Starting code here"));
                    _qsParam = Request.QueryString["Param"];
                    vReport rpt = (vReport)HttpRuntime.Cache[_qsParam];
                    if (rpt == null)
                    {
                        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Page_Load:  Test vReport rpt", new Exception("rpt was null"));
                    }


                    ReportViewer1.ShowParameterPrompts = false;
                    ReportViewer1.ProcessingMode = ProcessingMode.Remote;
                    ServerReport serverReport = ReportViewer1.ServerReport;
                    // Assign credentials to the report
                    ReportViewer1.ServerReport.ReportServerCredentials = new MyReportServerCredentials();

                    ReportViewer1.KeepSessionAlive = true;


                    string reportServerUrl = ConfigurationManager.AppSettings["ReportServerUrl"];
                    string reportPath = ConfigurationManager.AppSettings["ReportPath"];
                    reportPath = reportPath + rpt.ReportName;

                    serverReport.ReportServerUrl = new Uri(reportServerUrl);
                    serverReport.ReportPath = reportPath;

                    if (rpt.Parameters.Count > 0)
                    {
                        Microsoft.Reporting.WebForms.ReportParameter[] parms = new Microsoft.Reporting.WebForms.ReportParameter[rpt.Parameters.Count];
                        for (int i = 0; i < rpt.Parameters.Count; i++)
                        {
                            parms[i] = new Microsoft.Reporting.WebForms.ReportParameter(rpt.Parameters[i].ParamName, rpt.Parameters[i].Value);
                        }

                        // Log the report parameters
                        string collectReportParamters = ConfigurationManager.AppSettings["CollectReportParamters"];
                        if (collectReportParamters == "true")
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append("Report Name:  " + rpt.ReportName + Environment.NewLine);
                            sb.Append("Report Id:  " + rpt.ReportId.ToString() + Environment.NewLine);
                            sb.Append("Timestampe:  " + DateTime.Now + Environment.NewLine);
                            sb.Append("--Start params" + Environment.NewLine);

                            for (int i = 0; i < rpt.Parameters.Count; i++)
                            {
                                if (rpt.Parameters[i].Value == null)
                                {
                                    sb.Append("NULL,  --" + rpt.Parameters[i].ParamName + Environment.NewLine);
                                }
                                else
                                {
                                    sb.Append(rpt.Parameters[i].Value + ",  --" + rpt.Parameters[i].ParamName + Environment.NewLine);
                                }
                            }
                            sb.Append("--End params");
                            IfxEvent.PublishTrace(traceId, _as, _cn, "Log Report Params", new Exception(sb.ToString()));
                        }

                        ReportViewer1.ServerReport.SetParameters(parms);
                    }

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Page_Load", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Page_Load", IfxTraceCategory.Leave);
            }
        }





        /////////////////////////////////////////////////////////////////////////
        // Don't know if we need this and it doesnt seem to be working as it iether runs 

        [Serializable]
        public sealed class MyReportServerCredentials :
            IReportServerCredentials
        {
            public WindowsIdentity ImpersonationUser
            {
                get
                {
                    // Use the default Windows user.  Credentials will be
                    // provided by the NetworkCredentials property.
                    return null;
                }
            }

            public ICredentials NetworkCredentials
            {
                get
                {
                    // Read the user information from the Web.config file.  
                    // By reading the information on demand instead of 
                    // storing it, the credentials will not be stored in 
                    // session, reducing the vulnerable surface area to the
                    // Web.config file, which can be secured with an ACL.

                    // User name
                    string userName = ConfigurationManager.AppSettings["ReportUser"];

                    if (string.IsNullOrEmpty(userName))
                    {
                        throw new Exception("Missing user name from web.config file");
                    }

                    // Password
                    string password = ConfigurationManager.AppSettings["ReportUserPassword"];

                    if (string.IsNullOrEmpty(password))
                    {
                        throw new Exception("Missing password from web.config file");
                    }

                    // Domain
                    string domain = ConfigurationManager.AppSettings["ReportUserDomain"];

                    if (string.IsNullOrEmpty(domain))
                    {
                        throw new Exception("Missing domain from web.config file");
                    }

                    return new NetworkCredential(userName, password, domain);
                }
            }

            public bool GetFormsCredentials(out Cookie authCookie, out string userName, out string password, out string authority)
            {
                authCookie = null;
                userName = null;
                password = null;
                authority = null;

                // Not using form credentials
                return false;
            }
        }


        //////////////////////////////////////////////////////////////////////













        //  http://www.sqlservercentral.com/Forums/Topic575131-150-1.aspx
        //  http://msdn.microsoft.com/en-us/library/microsoft.reporting.webforms.ireportservercredentials(v=VS.100).aspx
        //http://social.msdn.microsoft.com/Forums/en/sqlreportingservices/thread/2edc5e29-b55c-4667-9b64-e12a566c6c89
        //http://social.msdn.microsoft.com/forums/en-US/vsreportcontrols/thread/f6eb4b2a-d410-409a-97c7-19de3026403f/





        private System.Net.NetworkCredential GetCredentials()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCredentials", IfxTraceCategory.Enter);
                string user = System.Configuration.ConfigurationManager.AppSettings["ReportUser"];
                string pw = System.Configuration.ConfigurationManager.AppSettings["ReportUserPassword"];
                string domain = System.Configuration.ConfigurationManager.AppSettings["ReportUserDomain"];
                return new System.Net.NetworkCredential(user, pw, domain);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCredentials", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCredentials", IfxTraceCategory.Leave);
            }
        }



    }
}