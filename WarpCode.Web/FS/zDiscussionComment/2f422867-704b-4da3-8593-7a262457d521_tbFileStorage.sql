/*
   Wednesday, June 17, 20154:52:31 PM
   User: 
   Server: VOYAGER2
   Database: VelocityPrototype
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tbFileStorage
	DROP CONSTRAINT FK_tbFileStorage_tbOrder
GO
ALTER TABLE dbo.tbOrder SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tbFileStorage
	DROP CONSTRAINT FK_tbFileStorage_tbTask
GO
ALTER TABLE dbo.tbTask SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tbFileStorage
	DROP CONSTRAINT FK_tbFileStorage_tbCustomer
GO
ALTER TABLE dbo.tbCustomer SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_tbFileStorage
	(
	FS_Id uniqueidentifier NOT NULL,
	FS_Cust_Id uniqueidentifier NULL,
	FS_Or_Id uniqueidentifier NULL,
	FS_Tk_Id uniqueidentifier NULL,
	FS_DscCm_Id uniqueidentifier NULL,
	FS_ParentType varchar(20) NULL,
	Fs_FileName varchar(150) NULL,
	FS_FilePath varchar(200) NULL,
	FS_FileSize bigint NULL,
	FS_XrefIdentifier varchar(50) NULL,
	FS_UserId uniqueidentifier NULL,
	FS_CreatedDate datetime NULL,
	FS_LastModifiedDate datetime NULL,
	FS_Stamp timestamp NOT NULL,
	zFs_FilePath_old varchar(200) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tbFileStorage SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_tbFileStorage TO WarpCode_User  AS dbo
GO
GRANT DELETE ON dbo.Tmp_tbFileStorage TO VelocityPrototype_User  AS dbo
GO
GRANT DELETE ON dbo.Tmp_tbFileStorage TO NetworkService  AS dbo
GO
GRANT INSERT ON dbo.Tmp_tbFileStorage TO NetworkService  AS dbo
GO
GRANT SELECT ON dbo.Tmp_tbFileStorage TO WarpCode_User  AS dbo
GO
GRANT SELECT ON dbo.Tmp_tbFileStorage TO VelocityPrototype_User  AS dbo
GO
GRANT SELECT ON dbo.Tmp_tbFileStorage TO NetworkService  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_tbFileStorage TO NetworkService  AS dbo
GO
IF EXISTS(SELECT * FROM dbo.tbFileStorage)
	 EXEC('INSERT INTO dbo.Tmp_tbFileStorage (FS_Id, FS_Cust_Id, FS_Or_Id, FS_Tk_Id, FS_ParentType, Fs_FileName, FS_FilePath, FS_FileSize, FS_XrefIdentifier, FS_UserId, FS_CreatedDate, FS_LastModifiedDate, zFs_FilePath_old)
		SELECT FS_Id, FS_Cust_Id, FS_Or_Id, FS_Tk_Id, FS_ParentType, Fs_FileName, FS_FilePath, FS_FileSize, FS_XrefIdentifier, FS_UserId, FS_CreatedDate, FS_LastModifiedDate, zFs_FilePath_old FROM dbo.tbFileStorage WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.tbFileStorage
GO
EXECUTE sp_rename N'dbo.Tmp_tbFileStorage', N'tbFileStorage', 'OBJECT' 
GO
ALTER TABLE dbo.tbFileStorage ADD CONSTRAINT
	PK_tbFileStorage PRIMARY KEY CLUSTERED 
	(
	FS_Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tbFileStorage WITH NOCHECK ADD CONSTRAINT
	FK_tbFileStorage_tbCustomer FOREIGN KEY
	(
	FS_Cust_Id
	) REFERENCES dbo.tbCustomer
	(
	Cust_Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	 NOT FOR REPLICATION

GO
ALTER TABLE dbo.tbFileStorage
	NOCHECK CONSTRAINT FK_tbFileStorage_tbCustomer
GO
ALTER TABLE dbo.tbFileStorage WITH NOCHECK ADD CONSTRAINT
	FK_tbFileStorage_tbTask FOREIGN KEY
	(
	FS_Tk_Id
	) REFERENCES dbo.tbTask
	(
	Tk_Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	 NOT FOR REPLICATION

GO
ALTER TABLE dbo.tbFileStorage
	NOCHECK CONSTRAINT FK_tbFileStorage_tbTask
GO
ALTER TABLE dbo.tbFileStorage WITH NOCHECK ADD CONSTRAINT
	FK_tbFileStorage_tbOrder FOREIGN KEY
	(
	FS_Or_Id
	) REFERENCES dbo.tbOrder
	(
	Or_Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	 NOT FOR REPLICATION

GO
ALTER TABLE dbo.tbFileStorage
	NOCHECK CONSTRAINT FK_tbFileStorage_tbOrder
GO
CREATE TRIGGER [dbo].[DTrig_FileStorage]
ON dbo.tbFileStorage
AFTER DELETE
AS
BEGIN
SET NOCOUNT ON;





IF (SELECT COUNT(1) FROM tbFileStorage WHERE (NOT (FS_Cust_Id IS NULL))) > 0
    BEGIN
	   UPDATE tbCustomer
	   SET                AttachmentFileNames = dbo.udf_RollupAttachmentName_Customer(tbCustomer.Cust_Id)

	   FROM            (SELECT DISTINCT FS_Cust_Id
						    FROM DELETED) AS tmp INNER JOIN
						   tbCustomer ON tmp.FS_Cust_Id = tbCustomer.Cust_Id

	   UPDATE       tbCustomer
	   SET                AttachmentCount = tmp.Cnt
	   FROM            (SELECT        tbFileStorage.FS_Cust_Id, COUNT(tbFileStorage.FS_Id) AS Cnt
						    FROM            (SELECT DISTINCT FS_Cust_Id
												FROM   DELETED) AS filter INNER JOIN
												tbCustomer ON filter.FS_Cust_Id = tbCustomer.Cust_Id LEFT OUTER JOIN
												tbFileStorage ON tbCustomer.Cust_Id = tbFileStorage.FS_Cust_Id
						    GROUP BY tbFileStorage.FS_Cust_Id) AS tmp INNER JOIN
						   tbCustomer ON tmp.FS_Cust_Id = tbCustomer.Cust_Id

    END



IF (SELECT COUNT(1) FROM tbFileStorage WHERE (NOT (FS_Or_Id IS NULL))) > 0
    BEGIN
	   UPDATE tbOrder
	   SET                AttachmentFileNames = dbo.udf_RollupAttachmentName_Order(tbOrder.Or_Id)

	   FROM            (SELECT DISTINCT FS_Or_Id
						    FROM DELETED) AS tmp INNER JOIN
						   tbOrder ON tmp.FS_Or_Id = tbOrder.Or_Id

	   UPDATE       tbOrder
	   SET                AttachmentCount = tmp.Cnt
	   FROM            (SELECT        tbFileStorage.FS_Or_Id, COUNT(tbFileStorage.FS_Id) AS Cnt
						    FROM            (SELECT DISTINCT FS_Or_Id
												FROM  DELETED) AS filter INNER JOIN
												tbOrder ON filter.FS_Or_Id = tbOrder.Or_Id LEFT OUTER JOIN
												tbFileStorage ON tbOrder.Or_Id = tbFileStorage.FS_Or_Id
						    GROUP BY tbFileStorage.FS_Or_Id) AS tmp INNER JOIN
						   tbOrder ON tmp.FS_Or_Id = tbOrder.Or_Id

    END



IF (SELECT COUNT(1) FROM tbFileStorage WHERE (NOT (FS_Tk_Id IS NULL))) > 0
    BEGIN
	   UPDATE tbTask
	   SET                AttachmentFileNames = dbo.udf_RollupAttachmentName_Task(tbTask.Tk_Id)

	   FROM            (SELECT DISTINCT FS_Tk_Id
						    FROM DELETED) AS tmp INNER JOIN
						   tbTask ON tmp.FS_Tk_Id = tbTask.Tk_Id

	   UPDATE       tbTask
	   SET                AttachmentCount = tmp.Cnt
	   FROM            (
	   
			 SELECT        dbo.tbFileStorage.FS_Tk_Id, COUNT(dbo.tbFileStorage.FS_Id) AS Cnt
			 FROM            (SELECT DISTINCT FS_Tk_Id
								  FROM    DELETED) AS filter INNER JOIN
								 dbo.tbTask ON filter.FS_Tk_Id = dbo.tbTask.Tk_Id LEFT OUTER JOIN
								 dbo.tbFileStorage ON dbo.tbTask.Tk_Id = dbo.tbFileStorage.FS_Tk_Id
			 GROUP BY dbo.tbFileStorage.FS_Tk_Id
						    
						    ) AS tmp INNER JOIN
						   tbTask ON tmp.FS_Tk_Id = tbTask.Tk_Id

    END




return;


END
GO
CREATE TRIGGER [dbo].[ITrig_FileStorage]
ON dbo.tbFileStorage
AFTER INSERT
AS
BEGIN
SET NOCOUNT ON;






IF (SELECT COUNT(1) FROM tbFileStorage WHERE (NOT (FS_Cust_Id IS NULL))) > 0
    BEGIN
	   UPDATE tbCustomer
	   SET                AttachmentFileNames = dbo.udf_RollupAttachmentName_Customer(tbCustomer.Cust_Id)

	   FROM            (SELECT DISTINCT FS_Cust_Id
						    FROM INSERTED) AS tmp INNER JOIN
						   tbCustomer ON tmp.FS_Cust_Id = tbCustomer.Cust_Id

	   UPDATE       tbCustomer
	   SET                AttachmentCount = tmp.Cnt
	   FROM            (SELECT        tbFileStorage.FS_Cust_Id, COUNT(tbFileStorage.FS_Id) AS Cnt
						    FROM            (SELECT DISTINCT FS_Cust_Id
												FROM   INSERTED) AS filter INNER JOIN
												tbCustomer ON filter.FS_Cust_Id = tbCustomer.Cust_Id LEFT OUTER JOIN
												tbFileStorage ON tbCustomer.Cust_Id = tbFileStorage.FS_Cust_Id
						    GROUP BY tbFileStorage.FS_Cust_Id) AS tmp INNER JOIN
						   tbCustomer ON tmp.FS_Cust_Id = tbCustomer.Cust_Id

    END






IF (SELECT COUNT(1) FROM tbFileStorage WHERE (NOT (FS_Or_Id IS NULL))) > 0
    BEGIN
	   UPDATE tbOrder
	   SET                AttachmentFileNames = dbo.udf_RollupAttachmentName_Order(tbOrder.Or_Id)

	   FROM            (SELECT DISTINCT FS_Or_Id
						    FROM INSERTED) AS tmp INNER JOIN
						   tbOrder ON tmp.FS_Or_Id = tbOrder.Or_Id

	   UPDATE       tbOrder
	   SET                AttachmentCount = tmp.Cnt
	   FROM            (SELECT        tbFileStorage.FS_Or_Id, COUNT(tbFileStorage.FS_Id) AS Cnt
						    FROM            (SELECT DISTINCT FS_Or_Id
												FROM  INSERTED) AS filter INNER JOIN
												tbOrder ON filter.FS_Or_Id = tbOrder.Or_Id LEFT OUTER JOIN
												tbFileStorage ON tbOrder.Or_Id = tbFileStorage.FS_Or_Id
						    GROUP BY tbFileStorage.FS_Or_Id) AS tmp INNER JOIN
						   tbOrder ON tmp.FS_Or_Id = tbOrder.Or_Id

    END






IF (SELECT COUNT(1) FROM tbFileStorage WHERE (NOT (FS_Tk_Id IS NULL))) > 0
    BEGIN
	   UPDATE tbTask
	   SET                AttachmentFileNames = dbo.udf_RollupAttachmentName_Task(tbTask.Tk_Id)

	   FROM            (SELECT DISTINCT FS_Tk_Id
						    FROM INSERTED) AS tmp INNER JOIN
						   tbTask ON tmp.FS_Tk_Id = tbTask.Tk_Id

	   UPDATE       tbTask
	   SET                AttachmentCount = tmp.Cnt
	   FROM            (
	   
			 SELECT        dbo.tbFileStorage.FS_Tk_Id, COUNT(dbo.tbFileStorage.FS_Id) AS Cnt
			 FROM            (SELECT DISTINCT FS_Tk_Id
								  FROM    INSERTED) AS filter INNER JOIN
								 dbo.tbTask ON filter.FS_Tk_Id = dbo.tbTask.Tk_Id LEFT OUTER JOIN
								 dbo.tbFileStorage ON dbo.tbTask.Tk_Id = dbo.tbFileStorage.FS_Tk_Id
			 GROUP BY dbo.tbFileStorage.FS_Tk_Id
						    
						    ) AS tmp INNER JOIN
						   tbTask ON tmp.FS_Tk_Id = tbTask.Tk_Id

    END


















return;









END
GO
COMMIT
