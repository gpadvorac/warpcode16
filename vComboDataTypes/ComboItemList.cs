﻿using System.Collections.ObjectModel;
using System.Collections;
using System;
using System.Diagnostics;
using System.Windows.Data;

namespace vComboDataTypes
{


    public class ComboItemList : ObservableCollection<ComboItem>
    {

        #region Initialize Variables

        public event StaticComboItemListUpdatedEventHandler DataSourceUpdated;

        public enum DataType
        {
            IntegerType = 1,
            GuidType = 2,
            StringType = 3,
            LongIntegerType = 4,
            ShortType = 5,
            ByteType = 6,
            NA = 7
        }

        bool _isChildList = false;
        DataType _id_DataType;
        DataType _fK_DataType = DataType.NA;
        ObservableCollection<ComboItem> _cachedList = new ObservableCollection<ComboItem>();
        GetNameFromListConverter _getNameFromListConverter = new GetNameFromListConverter();
        GetNameFromCachedListConverter _getNameFromCachedListConverter = new GetNameFromCachedListConverter();

        bool _IsRefreshingData = false;

        #endregion Initialize Variables


        #region Constructors

        public ComboItemList() { }

        public ComboItemList(DataType id_DataType)
        {
            try
            {
                _id_DataType = id_DataType;
            }
            catch (Exception ex)
            {

            }
        }

        public ComboItemList(DataType id_DataType, DataType fk_DataType)
        {
            try
            {
                _id_DataType = id_DataType;
                _fK_DataType = fk_DataType;
                _isChildList = true;
            }
            catch (Exception ex)
            {

            }
        }

        public ComboItemList(object[] data, DataType id_DataType)
        {
            try
            {
                _id_DataType = id_DataType;
                ReplaceList(data);
            }
            catch (Exception ex)
            {

            }
        }

        public ComboItemList(object[] data, DataType id_DataType, DataType fk_DataType)
        {
            try
            {
                _id_DataType = id_DataType;
                _fK_DataType = fk_DataType;
                _isChildList = true;
                ReplaceList(data);
            }
            catch (Exception ex)
            {

            }
        }

        public ComboItemList(ObservableCollection<ComboItem> data, DataType id_DataType)
        {
            try
            {
                _id_DataType = id_DataType;
                ReplaceList(data);
            }
            catch (Exception ex)
            {

            }
        }

        public ComboItemList(ObservableCollection<ComboItem> data, DataType id_DataType, DataType fk_DataType)
        {
            try
            {
                _id_DataType = id_DataType;
                _fK_DataType = fk_DataType;
                _isChildList = true;
                ReplaceList(data);
            }
            catch (Exception ex)
            {

            }
        }


        #endregion Constructors


        #region Methods

        new public void Add(ComboItem item)
        {
            try
            {
                base.Add(item);
                if (_isChildList == true)
                {
                    _cachedList.Add(item);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void ReplaceList(object[] data)
        {
            try
            {
                if (_isChildList == true)
                {
                    LoadDataChildList(data);
                }
                else
                {
                    LoadDataStandard(data);
                }
                RaiseDataSourceUpdated();
            }
            catch (Exception ex)
            {

            }
        }

        public void ReplaceList(ObservableCollection<ComboItem> data)
        {
            try
            {
                if (_isChildList == true)
                {
                    LoadDataChildList(data);
                }
                else
                {
                    LoadDataStandard(data);
                }
                RaiseDataSourceUpdated();
            }
            catch (Exception ex)
            {

            }
        }

        void LoadDataStandard(object[] data)
        {
            try
            {
                base.Clear();
                if (data != null)
                {
                    for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                    {
                        base.Add(new ComboItem((object[]) data[i]));
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        void LoadDataStandard(ObservableCollection<ComboItem> data)
        {
            try
            {
                base.Clear(); 
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        base.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        void LoadDataChildList(object[] data)
        {
            try
            {
                base.Clear();
                _cachedList.Clear();
                if (data != null)
                {
                    for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                    {
                        base.Add(new ComboItem((object[]) data[i]));
                        _cachedList.Add(new ComboItem((object[]) data[i]));
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        void LoadDataChildList(ObservableCollection<ComboItem> data)
        {
            try
            {
                base.Clear();
                _cachedList.Clear();
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        base.Add(item);
                        _cachedList.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void FilterByFK(object fk)
        {
            try
            {
                base.Clear();

                if (fk == null) { return; }
                if (_fK_DataType == ComboItemList.DataType.IntegerType)
                {
                    foreach (var item in _cachedList)
                    {
                        if (item.FK != null)
                        {
                            if ((int)item.FK == (int)fk)
                            {
                                base.Add(item);
                            }
                        }
                    }
                }
                else if (_fK_DataType == ComboItemList.DataType.GuidType)
                {
                    foreach (var item in _cachedList)
                    {
                        if (item.FK != null)
                        {
                            if ((Guid)item.FK == (Guid)fk)
                            {
                                base.Add(item);
                            }
                        }
                    }
                }
                else if (_fK_DataType == ComboItemList.DataType.StringType)
                {
                    foreach (var item in _cachedList)
                    {
                        if (item.FK != null)
                        {
                            if ((string)item.FK == (string)fk)
                            {
                                base.Add(item);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public ComboItem FindItemById(object id)
        {
            try
            {
                if (id == null)
                {
                    return null;
                }

                switch (_id_DataType)
                {
                    case DataType.GuidType:
                        foreach (var item in base.Items)
                        {
                            if ((Guid)item.Id == (Guid)id)
                            {
                                return item;
                            }
                        }
                        break;
                    case DataType.LongIntegerType:
                        foreach (var item in base.Items)
                        {
                            if ((long)item.Id == (long)id)
                            {
                                return item;
                            }
                        }
                        break;
                    case DataType.IntegerType:
                        foreach (var item in base.Items)
                        {
                            if ((int)item.Id == (int)id)
                            {
                                return item;
                            }
                        }
                        break;
                    case DataType.ShortType:
                        foreach (var item in base.Items)
                        {
                            if ((short)item.Id == (short)id)
                            {
                                return item;
                            }
                        }
                        break;
                    case DataType.ByteType:
                        foreach (var item in base.Items)
                        {
                            if ((byte)item.Id == (byte)id)
                            {
                                return item;
                            }
                        }
                        break;
                    case DataType.StringType:
                        foreach (var item in base.Items)
                        {
                            if ((string)item.Id == (string)id)
                            {
                                return item;
                            }
                        }
                        break;
                    default:
                        return null;
                }
                return null;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public string FindItemNameById(object id)
        {
            try
            {
                if (id == null)
                {
                    return null;
                }
                if (_id_DataType == ComboItemList.DataType.IntegerType)
                {
                    foreach (var item in base.Items)
                    {
                        if ((int)item.Id == (int)id)
                        {
                            return item.ItemName;
                        }
                    }
                }
                else if (_id_DataType == ComboItemList.DataType.GuidType)
                {
                    foreach (var item in base.Items)
                    {
                        if ((Guid)item.Id == (Guid)id)
                        {
                            return item.ItemName;
                        }
                    }
                }
                else if (_id_DataType == ComboItemList.DataType.StringType)
                {
                    foreach (var item in base.Items)
                    {
                        if ((string)item.Id == (string)id)
                        {
                            return item.ItemName;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        #endregion Methods


        #region Properties


        public DataType Id_DataType
        {
            get { return _id_DataType; }
            set { _id_DataType = value; }
        }

        public DataType FK_DataType
        {
            get { return _fK_DataType; }
            set { _fK_DataType = value; }
        }

        public ObservableCollection<ComboItem> CachedList
        {
            get { return _cachedList; }
            set { _cachedList = value; }
        }

        public GetNameFromListConverter GetNameFromListConverterProperty
        {
            get { return _getNameFromListConverter; }
            set { _getNameFromListConverter = value; }
        }

        public GetNameFromCachedListConverter GetNameFromCachedListConverterProperty
        {
            get { return _getNameFromCachedListConverter; }
            set { _getNameFromCachedListConverter = value; }
        }

        public bool IsRefreshingData
        {
            get { return _IsRefreshingData; }
            set { _IsRefreshingData = value; }
        }

        #endregion Properties


        #region IEnumerable Members

        new public IEnumerator GetEnumerator()
        {
            try
            {
                return base.GetEnumerator();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion IEnumerable Members

        public class GetNameFromListConverter : IValueConverter
        {

            #region IValueConverter Members

            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                try
                {
                    if (parameter == null || value == null)
                    {
                        return null;
                    }

                    ComboItemList data = parameter as ComboItemList;
                    if (data == null) { return null; }
                    string val = null;
                    foreach (var item in data.Items)
                    {
                        if (item.Id.Equals(value))
                        {
                            return item.ItemName;
                        }
                    }
                    return val;

                    //ComboItemList data = parameter as ComboItemList;
                    //if (data == null) { return null; }
                    //string val = null;
                    //if (data.Id_DataType == ComboItemList.DataType.IntegerType)
                    //{

                    //    foreach (var item in data.Items)
                    //    {
                    //        if ((int)item.Id == (int)value)
                    //        {
                    //            return item.ItemName;
                    //        }
                    //    }

                    //}
                    //else if (data.Id_DataType == ComboItemList.DataType.GuidType)
                    //{
                    //    foreach (var item in data.Items)
                    //    {
                    //        if ((Guid)item.Id == (Guid)value)
                    //        {
                    //            return item.ItemName;
                    //        }
                    //    }

                    //}
                    //else if (data.Id_DataType == ComboItemList.DataType.StringType)
                    //{
                    //    foreach (var item in data.Items)
                    //    {
                    //        if ((string)item.Id == (string)value)
                    //        {
                    //            return item.ItemName;
                    //        }
                    //    }
                    //}

                    //return val;
                }
                catch (Exception ex)
                {
                    //Debugger.Break();
                    return null;
                }
            }

            public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return null;
            }

            #endregion
        }

        public class GetNameFromCachedListConverter : IValueConverter
        {
            // Main list is:  In the case of lists with foriegn keys, the main list is the full list before it's filtered by the FK.
            #region IValueConverter Members

            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                try
                {
                    if (parameter == null || value == null)
                    {
                        return null;
                    }

                    ComboItemList data = parameter as ComboItemList;
                    if (data == null) { return null; }
                    string val = null;
                    if (data.Id_DataType == ComboItemList.DataType.IntegerType)
                    {

                        foreach (var item in data.CachedList)
                        {
                            if ((int)item.Id == (int)value)
                            {
                                return item.ItemName;
                            }
                        }

                    }
                    else if (data.Id_DataType == ComboItemList.DataType.GuidType)
                    {
                        foreach (var item in data.CachedList)
                        {
                            if ((Guid)item.Id == (Guid)value)
                            {
                                return item.ItemName;
                            }
                        }

                    }
                    else if (data.Id_DataType == ComboItemList.DataType.StringType)
                    {
                        foreach (var item in data.CachedList)
                        {
                            if ((string)item.Id == (string)value)
                            {
                                return item.ItemName;
                            }
                        }
                    }

                    return val;

                }
                catch (Exception ex)
                {
                    return null;
                }
            }

            public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                try
                {
                    return null;
                    //throw new NotImplementedException("vComboDataTypes.ComboItemList.GetNameFromListConverter.Convert()  is not implemented.");
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

            #endregion
        }

        void RaiseDataSourceUpdated()
        {
            StaticComboItemListUpdatedEventArgs args = new StaticComboItemListUpdatedEventArgs(this);
            StaticComboItemListUpdatedEventHandler handler = DataSourceUpdated;
            if (handler != null)
            {
                handler(this, args);
            }
        }
    }



    #region StaticComboItemListUpdated

    public delegate void StaticComboItemListUpdatedEventHandler(object sender, StaticComboItemListUpdatedEventArgs e);

    public class StaticComboItemListUpdatedEventArgs : EventArgs
    {
        ComboItemList _newList;

        public StaticComboItemListUpdatedEventArgs(ComboItemList newList)
        {
            try
            {
                _newList = newList;
            }
            catch (Exception ex)
            {

            }
        }

        public ComboItemList NewList
        {
            get { return _newList; }
            set { _newList = value; }
        }
    }

    #endregion StaticComboItemListUpdated








}
