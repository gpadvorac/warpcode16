﻿using ApplicationTypeServices;
using EntityBll.SL;
using Ifx.SL;
using ProxyWrapper;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Velocity.SL;
using vUICommon;

namespace UIControls
{
    public class SupportDataHelper
    {

        private static string _as = "UIControls";
        private static string _cn = "SupportDataHelper";
        static CommonClientDataService_ProxyWrapper _commonDataProxy = null;


        public static void ReloadStaticSupportData()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReloadStaticSupportData", IfxTraceCategory.Enter);

                if (_commonDataProxy == null)
                {
                    _commonDataProxy = new CommonClientDataService_ProxyWrapper();
                    _commonDataProxy.GetCommonClientData_ReadOnlyStaticListsCompleted += _commonDataProxy_GetCommonClientData_ReadOnlyStaticListsCompleted;
                }
                _commonDataProxy.Begin_GetCommonClientData_ReadOnlyStaticLists();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReloadStaticSupportData", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReloadStaticSupportData", IfxTraceCategory.Leave);
            }
        }



        static void _commonDataProxy_GetCommonClientData_ReadOnlyStaticListsCompleted(object sender, GetCommonClientData_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_commonDataProxy_GetCommonClientData_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                if (data != null)
                {


                    // 2016-12-27  Remmed this out.
                    //  data[0] passed in only the list for the first comboList.
                    //CommonClientData_Bll_staticLists.GetCommonClientData_ReadOnlyStaticListsLoadData((object[])data[0]);
                    CommonClientData_Bll_staticLists.GetCommonClientData_ReadOnlyStaticListsLoadData((object[])data);
                 

                    //Task_Bll_staticLists.GetTask_ReadOnlyStaticListsCompleted((object[])data[17]);
                  
                    //Project_Bll_staticLists.GetProject_ReadOnlyStaticListsCompleted((object[])data[21]);
                    //PersonContact_Bll_staticLists.GetPersonContact_ReadOnlyStaticListsCompleted((object[])data[22]);

                

                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_commonDataProxy_GetCommonClientData_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_commonDataProxy_GetCommonClientData_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }




    }
}
