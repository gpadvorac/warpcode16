using System;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using EntityWireTypeSL;
using Ifx.SL;
using Infragistics.Controls.Interactions;
using ProxyWrapper;
using vDialogControl;
using vUICommon;

namespace UIControls
{
    public static class FilesHelper
    {
        private const string AssemblyName = "UIControls";
        private const string TypeName = "FilesHelper";

        private static SaveFileDialog _saveFileDialog;

        private static readonly FileStorageService_ProxyWrapper Proxy;

        static FilesHelper()
        {
            Proxy = new FileStorageService_ProxyWrapper();

            Proxy.GetFilePathFromTempDirCompleted += GetFilePathFromTempDirCompleted;
            Proxy.FileStorage_DeleteTempFileCompleted += DeleteTempFileCompleted;
            Proxy.GetFileCompleted += GetFileCompleted;

        }

        public static void OpenFileInBrowser(FileStorage_Values fileStorage)
        {
            Proxy.Begin_GetFilePathFromTempDir(fileStorage.GetValues());
        }

        public static void OpenFileInBrowser(string fileUrl, string path, string fileName, string flgOpenInBrowser)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "OpenFileInBrowser", IfxTraceCategory.Enter);

                if (flgOpenInBrowser == "false")
                {
                    // This file type needs to be opened via the html control thats already open and in a visible state in the screen.
                    // Otherwise, we would need to embed the html control in a popup window like below.  In that case after the file loads, we 
                    // would not be able to close the window and would be stuck with an empty window (because the file ended up opening outside the window).
                    //var htmlViewer = new XamHtmlViewer { SourceUri = new Uri(fileUrl) };
                    HtmlPage.Window.Navigate(new Uri(fileUrl));
                }
                else
                {
                    // this file type will not jump out of the browser such as excel, so we need to load it in an html control embeded in a popup window.
                    var dialog = new vDialog
                    {
                        Header = fileName,
                        IsModal = true,
                        HeaderIconVisibility = Visibility.Collapsed,
                        MaximizeButtonVisibility = Visibility.Visible,
                        MinimizeButtonVisibility = Visibility.Visible,
                        StartupPosition = StartupPosition.Center,
                        Height = 450,
                        Width = 400
                    };
                    dialog.Content = new XamHtmlViewer { SourceUri = new Uri(fileUrl) };
                    dialog.Show();
                }

                var obj = new object[] { path };
                Proxy.Begin_FileStorage_DeleteTempFile(obj);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "OpenFileInBrowser", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "OpenFileInBrowser", IfxTraceCategory.Leave);
            }
        }

        public static void DownloadFile(FileStorage_Values fileStorageValues)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "DownloadFile", IfxTraceCategory.Enter);

                _saveFileDialog = new SaveFileDialog { DefaultFileName = fileStorageValues.FS_FileName };
                if (_saveFileDialog.ShowDialog() == true)
                {
                    Proxy.Begin_GetFile(fileStorageValues.GetValues());
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "DownloadFile", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "DownloadFile", IfxTraceCategory.Leave);
            }
        }

        private static void GetFilePathFromTempDirCompleted(object sender, GetFilePathFromTempDirCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "GetFilePathFromTempDirCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;

                if (data != null)
                {
                    var returnStirngs = Serialization.SilverlightSerializer.Deserialize(data) as string[];
                    if (returnStirngs != null && returnStirngs[0] != null && returnStirngs[1] != null && returnStirngs[2] != null && returnStirngs[3] != null)
                    {
                        OpenFileInBrowser(returnStirngs[0], returnStirngs[1], returnStirngs[2], returnStirngs[3]);
                    }
                    else
                    {
                        const string message = "The server failed to return information.  If this problem continues, please contact support.";
                        IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "GetFileCompleted", new Exception(message + ";  e.Result returned no data."));
                        MessageBox.Show(message, "Download Problem", MessageBoxButton.OK);
                    }
                }
                else
                {
                    MessageBox.Show("File not found on server.", "", MessageBoxButton.OK);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "GetFilePathFromTempDirCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "GetFilePathFromTempDirCompleted", IfxTraceCategory.Leave);
            }
        }

        private static void DeleteTempFileCompleted(object sender, FileStorage_DeleteTempFileCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "DeleteTempFileCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                var array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                if (array != null)
                {
                    var success = array[0] as int?;
                    if (success == 0)
                    {
                        MessageBox.Show("There was a problem deleting the file on the server.  If this continues, please contact support.", "Error", MessageBoxButton.OK);
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "DeleteTempFileCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "DeleteTempFileCompleted", IfxTraceCategory.Leave);
            }
        }

        private static void GetFileCompleted(object sender, GetFileCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "GetFileCompleted", IfxTraceCategory.Enter);
                string msessage;

                byte[] data = e.Result;
                var array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array == null)
                {
                    msessage = "The server failed to return the file.  If this problem continues, please contact support.";
                    IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "GetFileCompleted", new Exception(msessage + ";  e.Result returned no data."));
                    MessageBox.Show(msessage, "Download Problem", MessageBoxButton.OK);
                    return;
                }

                var obj = new FileStorage_Values(array, null);
                if (obj.FileData == null)
                {
                    msessage = "The file failed to download from the server.  If this problem continues, please contact support.";
                    IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "GetFileCompleted", new Exception(msessage + ";  FileStorage_Values.FileData was null."));
                    MessageBox.Show(msessage, "Download Problem", MessageBoxButton.OK);
                    return;
                }

                byte[] fileBytes = obj.FileData;

                using (var fs = _saveFileDialog.OpenFile())
                {
                    fs.Write(fileBytes, 0, fileBytes.Length);
                    fs.Close();
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "GetFileCompleted", ex);
                IfxWrapperException.GetError(ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, AssemblyName, TypeName, "GetFileCompleted", IfxTraceCategory.Leave);
            }
        }
    }
}