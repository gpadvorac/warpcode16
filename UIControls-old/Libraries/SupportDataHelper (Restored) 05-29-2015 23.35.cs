﻿using ApplicationTypeServices;
using EntityBll.SL;
using Ifx.SL;
using ProxyWrapper;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Velocity.SL;
using vUICommon;

namespace UIControls
{
    public class SupportDataHelper
    {

        private static string _as = "UIControls";
        private static string _cn = "SupportDataHelper";
        static CommonClientDataService_ProxyWrapper _commonDataProxy = null;


        public static void ReloadStaticSupportData()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ReloadStaticSupportData", IfxTraceCategory.Enter);

                if (_commonDataProxy == null)
                {
                    _commonDataProxy = new CommonClientDataService_ProxyWrapper();
                    _commonDataProxy.GetCommonClientData_ReadOnlyStaticLists_AllCompleted += _commonDataProxy_GetCommonClientData_ReadOnlyStaticLists_AllCompleted;
                }
                _commonDataProxy.Begin_GetCommonClientData_ReadOnlyStaticLists_All((Guid)ContextValues.CurrentProjectId, (Guid)Credentials.UserId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ReloadStaticSupportData", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ReloadStaticSupportData", IfxTraceCategory.Leave);
            }
        }



        static void _commonDataProxy_GetCommonClientData_ReadOnlyStaticLists_AllCompleted(object sender, GetCommonClientData_ReadOnlyStaticLists_AllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_commonDataProxy_GetCommonClientData_ReadOnlyStaticLists_AllCompleted", IfxTraceCategory.Enter);
                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                if (data != null)
                {
                    CommonClientData_Bll_staticLists.GetCommonClientData_ReadOnlyStaticListsLoadData((object[])data[0]);
                    Contract_Bll_staticLists.GetContract_ReadOnlyStaticListsLoadData((object[])data[1]);
                    ContractDtFlag_Bll_staticLists.GetContractDtFlag_ReadOnlyStaticListsLoadData((object[])data[2]);
                    ContractFlag_Bll_staticLists.GetContractFlag_ReadOnlyStaticListsLoadData((object[])data[3]);
                    Defect_Bll_staticLists.GetDefect_ReadOnlyStaticListsLoadData((object[])data[4]);
                    DepthIndicator_Bll_staticLists.GetDepthIndicator_ReadOnlyStaticListsLoadData((object[])data[5]);

                    Lease_Bll_staticLists.GetLease_ReadOnlyStaticListsLoadData((object[])data[6]);
                    LeaseFlag_Bll_staticLists.GetLeaseFlag_ReadOnlyStaticListsLoadData((object[])data[7]);
                    LeaseTract_Bll_staticLists.GetLeaseTract_ReadOnlyStaticListsLoadData((object[])data[8]);
                    LeaseTractFlag_Bll_staticLists.GetLeaseTractFlag_ReadOnlyStaticListsLoadData((object[])data[9]);
                    Obligation_Bll_staticLists.GetObligation_ReadOnlyStaticListsLoadData((object[])data[10]);
                    Payout_Bll_staticLists.GetPayout_ReadOnlyStaticListsLoadData((object[])data[11]);
                    Prospect_Bll_staticLists.GetProspect_ReadOnlyStaticListsLoadData((object[])data[12]);
                    Well_Bll_staticLists.GetWell_ReadOnlyStaticListsLoadData((object[])data[13]);
                    WellFlag_Bll_staticLists.GetWellFlag_ReadOnlyStaticListsLoadData((object[])data[14]);
                    WellInterest_Bll_staticLists.GetWellInterest_ReadOnlyStaticListsLoadData((object[])data[15]);
                    WellProduction_Bll_staticLists.GetWellProduction_ReadOnlyStaticListsLoadData((object[])data[16]);

                    Task_Bll_staticLists.GetTask_ReadOnlyStaticListsCompleted((object[])data[17]);
                    v_MapService_Bll_staticLists.Getv_MapService_ReadOnlyStaticListsCompleted((object[])data[18]);
                    v_MapServiceLayer_Bll_staticLists.Getv_MapServiceLayer_ReadOnlyStaticListsCompleted((object[])data[19]);
                    v_MapServiceLayerFilter_Bll_staticLists.Getv_MapServiceLayerFilter_ReadOnlyStaticListsCompleted((object[])data[20]);

                    Project_Bll_staticLists.GetProject_ReadOnlyStaticListsCompleted((object[])data[21]);
                    PersonContact_Bll_staticLists.GetPersonContact_ReadOnlyStaticListsCompleted((object[])data[22]);

                    v_Schedule_Bll_staticLists.Getv_Schedule_ReadOnlyStaticListsCompleted((object[])data[23]);

                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_commonDataProxy_GetCommonClientData_ReadOnlyStaticLists_AllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_commonDataProxy_GetCommonClientData_ReadOnlyStaticLists_AllCompleted", IfxTraceCategory.Leave);
            }
        }




    }
}
