﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace UIControls
{
    /// <summary>
    /// Opens a new browser window to the specified URL while running either in- or out-of-browser
    /// </summary>
    public class WebBrowserOpener
    {
        private class HyperlinkButtonWrapper : HyperlinkButton
        {
            public void OpenURL(string navigateUri)
            {
                OpenURL(new Uri(navigateUri, UriKind.Absolute));
            }

            public void OpenURL(Uri navigateUri)
            {
                base.NavigateUri = navigateUri;
                TargetName = "_blank";
                base.OnClick();
            }
        }

        public static void OpenURL(string navigateUri)
        {
            HyperlinkButtonWrapper hlbw = new HyperlinkButtonWrapper();
            hlbw.OpenURL(navigateUri);
        }

        public static void OpenURL(Uri navigateUri)
        {
            HyperlinkButtonWrapper hlbw = new HyperlinkButtonWrapper();
            hlbw.OpenURL(navigateUri);
        }
    }
}
