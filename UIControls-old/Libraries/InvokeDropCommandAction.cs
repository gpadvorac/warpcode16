using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using EntityBll.SL;

namespace UIControls
{
    public class InvokeDropCommandAction : TriggerAction<Grid>
    {
        protected override void Invoke(object parameter)
        {
            var args = (DragEventArgs)parameter;
            if (args != null)
            {
                var discussion = (Discussion_Bll)AssociatedObject.DataContext;
                var files = args.Data.GetData(DataFormats.FileDrop) as FileInfo[];
                if (files != null)
                {
                    discussion.FileDropCommand.Execute(files);
                }
            }
        }
    }
}