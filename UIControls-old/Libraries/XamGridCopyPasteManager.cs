﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Threading;
using Ifx.SL;

namespace UIControls
{
    public class XamGridCopyPasteManager
    {

        #region Variables

        private static string _as = "UIControls";
        private static string _cn = "XamGridCopyPasteManager";

        //static List<object> _selectedItems = new List<object>();
        static List<object> _cutCopyClipboard = new List<object>();

        static DateTime _dtm = DateTime.Now;

        static GridItemType _selectedGridItemType = GridItemType.None;

        static CutCopyTimerObject _timeObject = null;

        public enum GridItemType
        {
            None,
            LeaseTract,
            LeaseTractSub,
            LeaseTractSubQuarter
        }

        public enum CutCopyOperation
        {
            None,
            Cut,
            Copy
        }

        static CutCopyOperation _cutCopyOperationType = CutCopyOperation.None;

        static object _sourceNavListControl = null;
        static object _targetNavListControl = null;

        #endregion Variables


        #region Properties


        public static DateTime Dtm
        {
            get { return XamGridCopyPasteManager._dtm; }
            set { XamGridCopyPasteManager._dtm = value; }
        }

        //static public List<object> SelectedItems
        //{
        //    get 
        //    { 
        //        return _selectedItems;
        //    }
        //    set
        //    {
        //        //_dtm = DateTime.Now;
        //        _selectedItems = value; 
        //    }
        //}

        public static List<object> CutCopyClipboard
        {
            get { return XamGridCopyPasteManager._cutCopyClipboard; }
            //set { XamGridCopyPasteManager._copiedItems = value; }
        }

        static public GridItemType SelectedGridItemType
        {
            get { return _selectedGridItemType; }
            set { _selectedGridItemType = value; }
        }

        static public CutCopyOperation CutCopyOperationType
        {
            get { return _cutCopyOperationType; }
            set { _cutCopyOperationType = value; }
        }
        
        static public object SourceNavListControl
        {
            get { return _sourceNavListControl; }
            set { _sourceNavListControl = value; }
        }

        static public object TargetNavListControl
        {
            get { return _targetNavListControl; }
            set { _targetNavListControl = value; }
        }

        #endregion Properties


        #region Methods

        static public void SetSelectedItemsToCutCopyClipboard(List<object> selectedItems)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCopiedItems", IfxTraceCategory.Enter);

                _cutCopyClipboard.Clear();
                foreach (var item in selectedItems)
                {
                    _cutCopyClipboard.Add(item);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCopiedItems", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetCopiedItems", IfxTraceCategory.Leave);
            }
        }


        static public void StartCutCopyTimer()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "StartCutCopyTimer", IfxTraceCategory.Enter);
                if (_timeObject == null)
                {
                    _timeObject = new CutCopyTimerObject();
                }
                _timeObject.StartTimer();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "StartCutCopyTimer", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "StartCutCopyTimer", IfxTraceCategory.Leave);
            }
        }

        static public void StopCutCopyTimer()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "StopCutCopyTimer", IfxTraceCategory.Enter);
                if (_timeObject == null)
                {
                    return;
                }
                _timeObject.StopTimer();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "StopCutCopyTimer", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "StopCutCopyTimer", IfxTraceCategory.Leave);
            }
        }


        #endregion Methods


        #region Timer Code


        public class CutCopyTimerObject
        {
            private static string _as = "UIControls";
            private static string _cn = "XamGridCopyPasteManager.CutCopyTimerObject";

            public CutCopyTimerObject()
            { }


            public void StopTimer()
            {
                _dispatcherTimer.Stop();
                _cutCopyClipboard.Clear();
            }


            void StopTimer(object o, EventArgs sender)
            {
                _dispatcherTimer.Stop();
                _cutCopyClipboard.Clear();
            }


            DispatcherTimer _dispatcherTimer = null;
            public void StartTimer()
            {
                _dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 20, 0); // 100 Milliseconds 
                _dispatcherTimer.Tick += new EventHandler(StopTimer);
                _dispatcherTimer.Start();
            }


        }


        #endregion Timer Code


    }
}
