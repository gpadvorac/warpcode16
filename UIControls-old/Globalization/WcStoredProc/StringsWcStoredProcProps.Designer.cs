﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UIControls.Globalization.WcStoredProc {
    using System;
    
    
    /// <summary>
    /// A strongly-typed resource class, for looking up localized strings, formatting them, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilderEx class via the ResXFileCodeGeneratorEx custom tool.
    // To add or remove a member, edit your .ResX file then rerun the ResXFileCodeGeneratorEx custom tool or rebuild your VS.NET project.
    // Copyright (c) Dmytro Kryvko 2006-2016 (http://dmytro.kryvko.googlepages.com/)
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("DMKSoftware.CodeGenerators.Tools.StronglyTypedResourceBuilderEx", "2.6.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
#if !SILVERLIGHT
    [global::System.Reflection.ObfuscationAttribute(Exclude=true, ApplyToMembers=true)]
#endif
    [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
    public partial class StringsWcStoredProcProps {
        
        private static global::System.Resources.ResourceManager _resourceManager;
        
        private static object _internalSyncObject;
        
        private static global::System.Globalization.CultureInfo _resourceCulture;
        
        /// <summary>
        /// Initializes a StringsWcStoredProcProps object.
        /// </summary>
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public StringsWcStoredProcProps() {
        }
        
        /// <summary>
        /// Thread safe lock object used by this class.
        /// </summary>
        public static object InternalSyncObject {
            get {
                if (object.ReferenceEquals(_internalSyncObject, null)) {
                    global::System.Threading.Interlocked.CompareExchange(ref _internalSyncObject, new object(), null);
                }
                return _internalSyncObject;
            }
        }
        
        /// <summary>
        /// Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(_resourceManager, null)) {
                    global::System.Threading.Monitor.Enter(InternalSyncObject);
                    try {
                        if (object.ReferenceEquals(_resourceManager, null)) {
                            global::System.Threading.Interlocked.Exchange(ref _resourceManager, new global::System.Resources.ResourceManager("UIControls.Globalization.WcStoredProc.StringsWcStoredProcProps", typeof(StringsWcStoredProcProps).Assembly));
                        }
                    }
                    finally {
                        global::System.Threading.Monitor.Exit(InternalSyncObject);
                    }
                }
                return _resourceManager;
            }
        }
        
        /// <summary>
        /// Overrides the current thread's CurrentUICulture property for all
        /// resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return _resourceCulture;
            }
            set {
                _resourceCulture = value;
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Associated Entity'.
        /// </summary>
        public static string Sp_AssocEntity_Id {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_AssocEntity_Id, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Associated Entity'.
        /// </summary>
        public static string Sp_AssocEntity_Id_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_AssocEntity_Id_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Has New Columns'.
        /// </summary>
        public static string Sp_HasNewColumns {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_HasNewColumns, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Has New Columns'.
        /// </summary>
        public static string Sp_HasNewColumns_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_HasNewColumns_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Has New Params'.
        /// </summary>
        public static string Sp_HasNewParams {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_HasNewParams, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Has New Parameters'.
        /// </summary>
        public static string Sp_HasNewParams_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_HasNewParams_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Active record'.
        /// </summary>
        public static string Sp_IsActiveRow {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsActiveRow, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is an active record'.
        /// </summary>
        public static string Sp_IsActiveRow_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsActiveRow_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Build Data Access Method'.
        /// </summary>
        public static string Sp_IsBuildDataAccessMethod {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsBuildDataAccessMethod, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Build Data Access Method'.
        /// </summary>
        public static string Sp_IsBuildDataAccessMethod_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsBuildDataAccessMethod_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Columns Valid'.
        /// </summary>
        public static string Sp_IsColumnsValid {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsColumnsValid, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Columns Valid'.
        /// </summary>
        public static string Sp_IsColumnsValid_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsColumnsValid_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Fetch Entity'.
        /// </summary>
        public static string Sp_IsFetchEntity {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsFetchEntity, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Fetch Entity'.
        /// </summary>
        public static string Sp_IsFetchEntity_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsFetchEntity_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Input Params As Object Array'.
        /// </summary>
        public static string Sp_IsInputParamsAsObjectArray {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsInputParamsAsObjectArray, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Input Params As Object Array'.
        /// </summary>
        public static string Sp_IsInputParamsAsObjectArray_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsInputParamsAsObjectArray_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Params Valid'.
        /// </summary>
        public static string Sp_IsParamsValid {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsParamsValid, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Are Parameters Valid'.
        /// </summary>
        public static string Sp_IsParamsValid_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsParamsValid_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Param Value Set Valid'.
        /// </summary>
        public static string Sp_IsParamValueSetValid {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsParamValueSetValid, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is The Parameter Value Set (Group) Valid'.
        /// </summary>
        public static string Sp_IsParamValueSetValid_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsParamValueSetValid_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Static List'.
        /// </summary>
        public static string Sp_IsStaticList {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsStaticList, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Static List'.
        /// </summary>
        public static string Sp_IsStaticList_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsStaticList_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is ComboItem List'.
        /// </summary>
        public static string Sp_IsTypeComboItem {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsTypeComboItem, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is ComboItem List'.
        /// </summary>
        public static string Sp_IsTypeComboItem_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsTypeComboItem_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Valid'.
        /// </summary>
        public static string Sp_IsValid {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsValid, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Is Valid'.
        /// </summary>
        public static string Sp_IsValid_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_IsValid_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Method Name'.
        /// </summary>
        public static string Sp_MethodName {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_MethodName, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Method Name'.
        /// </summary>
        public static string Sp_MethodName_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_MethodName_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Name'.
        /// </summary>
        public static string Sp_Name {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_Name, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Name'.
        /// </summary>
        public static string Sp_Name_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_Name_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Notes'.
        /// </summary>
        public static string Sp_Notes {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_Notes, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Notes'.
        /// </summary>
        public static string Sp_Notes_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_Notes_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Result Type'.
        /// </summary>
        public static string Sp_SpRsTp_Id {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_SpRsTp_Id, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Result Type'.
        /// </summary>
        public static string Sp_SpRsTp_Id_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_SpRsTp_Id_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Return Type'.
        /// </summary>
        public static string Sp_SpRtTp_Id {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_SpRtTp_Id, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Return Type'.
        /// </summary>
        public static string Sp_SpRtTp_Id_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_SpRtTp_Id_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Type Name'.
        /// </summary>
        public static string Sp_TypeName {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_TypeName, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Type Name'.
        /// </summary>
        public static string Sp_TypeName_Vbs {
            get {
                return ResourceManager.GetString(ResourceNames.Sp_TypeName_Vbs, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Lists all the resource names as constant string fields.
        /// </summary>
        public class ResourceNames {
            
            /// <summary>
            /// Stores the resource name 'Sp_AssocEntity_Id'.
            /// </summary>
            public const string Sp_AssocEntity_Id = "Sp_AssocEntity_Id";
            
            /// <summary>
            /// Stores the resource name 'Sp_AssocEntity_Id_Vbs'.
            /// </summary>
            public const string Sp_AssocEntity_Id_Vbs = "Sp_AssocEntity_Id_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_HasNewColumns'.
            /// </summary>
            public const string Sp_HasNewColumns = "Sp_HasNewColumns";
            
            /// <summary>
            /// Stores the resource name 'Sp_HasNewColumns_Vbs'.
            /// </summary>
            public const string Sp_HasNewColumns_Vbs = "Sp_HasNewColumns_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_HasNewParams'.
            /// </summary>
            public const string Sp_HasNewParams = "Sp_HasNewParams";
            
            /// <summary>
            /// Stores the resource name 'Sp_HasNewParams_Vbs'.
            /// </summary>
            public const string Sp_HasNewParams_Vbs = "Sp_HasNewParams_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsActiveRow'.
            /// </summary>
            public const string Sp_IsActiveRow = "Sp_IsActiveRow";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsActiveRow_Vbs'.
            /// </summary>
            public const string Sp_IsActiveRow_Vbs = "Sp_IsActiveRow_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsBuildDataAccessMethod'.
            /// </summary>
            public const string Sp_IsBuildDataAccessMethod = "Sp_IsBuildDataAccessMethod";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsBuildDataAccessMethod_Vbs'.
            /// </summary>
            public const string Sp_IsBuildDataAccessMethod_Vbs = "Sp_IsBuildDataAccessMethod_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsColumnsValid'.
            /// </summary>
            public const string Sp_IsColumnsValid = "Sp_IsColumnsValid";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsColumnsValid_Vbs'.
            /// </summary>
            public const string Sp_IsColumnsValid_Vbs = "Sp_IsColumnsValid_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsFetchEntity'.
            /// </summary>
            public const string Sp_IsFetchEntity = "Sp_IsFetchEntity";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsFetchEntity_Vbs'.
            /// </summary>
            public const string Sp_IsFetchEntity_Vbs = "Sp_IsFetchEntity_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsInputParamsAsObjectArray'.
            /// </summary>
            public const string Sp_IsInputParamsAsObjectArray = "Sp_IsInputParamsAsObjectArray";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsInputParamsAsObjectArray_Vbs'.
            /// </summary>
            public const string Sp_IsInputParamsAsObjectArray_Vbs = "Sp_IsInputParamsAsObjectArray_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsParamsValid'.
            /// </summary>
            public const string Sp_IsParamsValid = "Sp_IsParamsValid";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsParamsValid_Vbs'.
            /// </summary>
            public const string Sp_IsParamsValid_Vbs = "Sp_IsParamsValid_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsParamValueSetValid'.
            /// </summary>
            public const string Sp_IsParamValueSetValid = "Sp_IsParamValueSetValid";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsParamValueSetValid_Vbs'.
            /// </summary>
            public const string Sp_IsParamValueSetValid_Vbs = "Sp_IsParamValueSetValid_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsStaticList'.
            /// </summary>
            public const string Sp_IsStaticList = "Sp_IsStaticList";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsStaticList_Vbs'.
            /// </summary>
            public const string Sp_IsStaticList_Vbs = "Sp_IsStaticList_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsTypeComboItem'.
            /// </summary>
            public const string Sp_IsTypeComboItem = "Sp_IsTypeComboItem";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsTypeComboItem_Vbs'.
            /// </summary>
            public const string Sp_IsTypeComboItem_Vbs = "Sp_IsTypeComboItem_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsValid'.
            /// </summary>
            public const string Sp_IsValid = "Sp_IsValid";
            
            /// <summary>
            /// Stores the resource name 'Sp_IsValid_Vbs'.
            /// </summary>
            public const string Sp_IsValid_Vbs = "Sp_IsValid_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_MethodName'.
            /// </summary>
            public const string Sp_MethodName = "Sp_MethodName";
            
            /// <summary>
            /// Stores the resource name 'Sp_MethodName_Vbs'.
            /// </summary>
            public const string Sp_MethodName_Vbs = "Sp_MethodName_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_Name'.
            /// </summary>
            public const string Sp_Name = "Sp_Name";
            
            /// <summary>
            /// Stores the resource name 'Sp_Name_Vbs'.
            /// </summary>
            public const string Sp_Name_Vbs = "Sp_Name_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_Notes'.
            /// </summary>
            public const string Sp_Notes = "Sp_Notes";
            
            /// <summary>
            /// Stores the resource name 'Sp_Notes_Vbs'.
            /// </summary>
            public const string Sp_Notes_Vbs = "Sp_Notes_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_SpRsTp_Id'.
            /// </summary>
            public const string Sp_SpRsTp_Id = "Sp_SpRsTp_Id";
            
            /// <summary>
            /// Stores the resource name 'Sp_SpRsTp_Id_Vbs'.
            /// </summary>
            public const string Sp_SpRsTp_Id_Vbs = "Sp_SpRsTp_Id_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_SpRtTp_Id'.
            /// </summary>
            public const string Sp_SpRtTp_Id = "Sp_SpRtTp_Id";
            
            /// <summary>
            /// Stores the resource name 'Sp_SpRtTp_Id_Vbs'.
            /// </summary>
            public const string Sp_SpRtTp_Id_Vbs = "Sp_SpRtTp_Id_Vbs";
            
            /// <summary>
            /// Stores the resource name 'Sp_TypeName'.
            /// </summary>
            public const string Sp_TypeName = "Sp_TypeName";
            
            /// <summary>
            /// Stores the resource name 'Sp_TypeName_Vbs'.
            /// </summary>
            public const string Sp_TypeName_Vbs = "Sp_TypeName_Vbs";
        }
    }
}
