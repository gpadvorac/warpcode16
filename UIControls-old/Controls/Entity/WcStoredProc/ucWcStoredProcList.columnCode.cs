using System;
using System.Windows;
using System.Windows.Controls;
using Ifx.SL;
using TypeServices;
using vControls;
using EntityBll.SL;
using Infragistics.Controls.Editors;
using vUICommon;
using EntityWireTypeSL;
using Infragistics.Controls.Grids;
using vComboDataTypes;
using System.Collections.Generic;
using vUICommon.Controls;
using UIControls.Globalization.WcStoredProc;
using UIControls.Globalization.WcStoredProcParam;
using UIControls.Globalization.WcStoredProcParamValueGroup;
using UIControls.Globalization.WcStoredProcColumn;
namespace UIControls
{
    public partial class ucWcStoredProcList
    {

        
        #region GridCell Editor Events
        

        private void XamGridTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                if ((ctl.Text.Length>0) && (ctl.Text.Substring(ctl.Text.Length - 1, 1) == " "))
                {
                    return;
                }
                WcStoredProc_Bll obj = navList.ActiveCell.Row.Data as WcStoredProc_Bll;
                if (obj == null) { return; }
                //switch (ctl.Name)
                //{
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vTextColumn_TextChanged(object sender, vTextColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = navList.ActiveCell.Row.Data as WcStoredProc_Bll;
                    switch (e.Key)
                    {
                        case "Sp_Name":
                            obj.Sp_Name = ctl.Text;
                            break;
                        case "Sp_TypeName":
                            obj.Sp_TypeName = ctl.Text;
                            break;
                        case "Sp_MethodName":
                            obj.Sp_MethodName = ctl.Text;
                            break;
                        case "Sp_Notes":
                            obj.Sp_Notes = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParam_Bll;
                    switch (e.Key)
                    {
                        case "SpP_SortOrder":
                            obj.SpP_SortOrder_asString = ctl.Text;
                            break;
                        case "SpP_Name":
                            obj.SpP_Name = ctl.Text;
                            break;
                        case "SpP_DefaultValue":
                            obj.SpP_DefaultValue = ctl.Text;
                            break;
                        case "SpP_Value":
                            obj.SpP_Value = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParamValueGroup_Bll;
                    switch (e.Key)
                    {
                        case "SpPVGrp_Name":
                            obj.SpPVGrp_Name = ctl.Text;
                            break;
                        case "SpPVGrp_Notes":
                            obj.SpPVGrp_Notes = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcColumn_Bll;
                    switch (e.Key)
                    {
                        case "SpCl_Name":
                            obj.SpCl_Name = ctl.Text;
                            break;
                        case "SpCl_ColIndex":
                            obj.SpCl_ColIndex_asString = ctl.Text;
                            break;
                        case "SpCl_ColLetter":
                            obj.SpCl_ColLetter = ctl.Text;
                            break;
                        case "SpCl_Notes":
                            obj.SpCl_Notes = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vDecimalColumn_TextChanged(object sender, vDecimalColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = navList.ActiveCell.Row.Data as WcStoredProc_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParam_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParamValueGroup_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcColumn_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamComboEditor comboEditor = sender as XamComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_SelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = navList.ActiveCell.Row.Data as WcStoredProc_Bll;
                    switch (ctl.Name)
                    {
                        case "Sp_AssocEntity_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Sp_AssocEntity_Id = null;
                            }
                            else
                            {
                                obj.Sp_AssocEntity_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "Sp_SpRsTp_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Sp_SpRsTp_Id = null;
                            }
                            else
                            {
                                obj.Sp_SpRsTp_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "Sp_SpRtTp_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Sp_SpRtTp_Id = null;
                            }
                            else
                            {
                                obj.Sp_SpRtTp_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParam_Bll;
                    switch (ctl.Name)
                    {
                        case "SpP_SpPDr_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.SpP_SpPDr_Id = null;
                            }
                            else
                            {
                                obj.SpP_SpPDr_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "SpP_DtNt_ID":
                            if (ctl.SelectedItem == null)
                            {
                                obj.SpP_DtNt_ID = null;
                            }
                            else
                            {
                                obj.SpP_DtNt_ID = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "SpP_DtSq_ID":
                            if (ctl.SelectedItem == null)
                            {
                                obj.SpP_DtSq_ID = null;
                            }
                            else
                            {
                                obj.SpP_DtSq_ID = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParamValueGroup_Bll;
                    switch (ctl.Name)
                    {
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcColumn_Bll;
                    switch (ctl.Name)
                    {
                        case "SpCl_DtNt_ID":
                            if (ctl.SelectedItem == null)
                            {
                                obj.SpCl_DtNt_ID = null;
                            }
                            else
                            {
                                obj.SpCl_DtNt_ID = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "SpCl_DtSq_ID":
                            if (ctl.SelectedItem == null)
                            {
                                obj.SpCl_DtSq_ID = null;
                            }
                            else
                            {
                                obj.SpCl_DtSq_ID = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_SelectionChanged(object sender, vXamComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                // The next 2 lines are for the Not In List feature.  If the 'Non-In-List' event is fireing, we dont want to run this event right now.
                if (ctl.SelectedItem == null) { return; }
                if (((ComboItem)ctl.SelectedItem).Id == null) { return; }
                if (navList.ActiveCell.Row.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = navList.ActiveCell.Row.Data as WcStoredProc_Bll;
                    switch (e.Key)
                    {
                        case "Sp_AssocEntity_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Sp_AssocEntity_Id = null;
                            }
                            else
                            {
                                obj.Sp_AssocEntity_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "Sp_SpRsTp_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Sp_SpRsTp_Id = null;
                            }
                            else
                            {
                                obj.Sp_SpRsTp_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "Sp_SpRtTp_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Sp_SpRtTp_Id = null;
                            }
                            else
                            {
                                obj.Sp_SpRtTp_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
                if (navList.ActiveCell.Row.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParam_Bll;
                    switch (e.Key)
                    {
                        case "SpP_SpPDr_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.SpP_SpPDr_Id = null;
                            }
                            else
                            {
                                obj.SpP_SpPDr_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "SpP_DtNt_ID":
                            if (ctl.SelectedItem == null)
                            {
                                obj.SpP_DtNt_ID = null;
                            }
                            else
                            {
                                obj.SpP_DtNt_ID = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "SpP_DtSq_ID":
                            if (ctl.SelectedItem == null)
                            {
                                obj.SpP_DtSq_ID = null;
                            }
                            else
                            {
                                obj.SpP_DtSq_ID = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }


                if (navList.ActiveCell.Row.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParamValueGroup_Bll;
                    switch (e.Key)
                    {
                    }
                }


                if (navList.ActiveCell.Row.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcColumn_Bll;
                    switch (e.Key)
                    {
                        case "SpCl_DtNt_ID":
                            if (ctl.SelectedItem == null)
                            {
                                obj.SpCl_DtNt_ID = null;
                            }
                            else
                            {
                                obj.SpCl_DtNt_ID = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "SpCl_DtSq_ID":
                            if (ctl.SelectedItem == null)
                            {
                                obj.SpCl_DtSq_ID = null;
                            }
                            else
                            {
                                obj.SpCl_DtSq_ID = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }


                vXamComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Enter);
                if (e.Key == System.Windows.Input.Key.Escape)
                {
                    WcStoredProc_Bll obj = _activeRow.Data as WcStoredProc_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_KeyUp(object sender, vXamComboColumnKeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Enter);
                if (e.XamComboColumnKeyEventArgs.Key == System.Windows.Input.Key.Escape)
                {
                    WcStoredProc_Bll obj = _activeRow.Data as WcStoredProc_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void XamMultiColumnComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamMultiColumnComboEditor comboEditor = sender as XamMultiColumnComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void vXamMultiColumnComboColumn_SelectionChanged(object sender, vXamMultiColumnComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vXamMultiColumnComboEditor ctl = sender as vXamMultiColumnComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = navList.ActiveCell.Row.Data as WcStoredProc_Bll;
                    switch (e.Key)
                    {
                    }
                }
                if (navList.ActiveCell.Row.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParam_Bll;
                    switch (e.Key)
                    {
                    }
                }
                if (navList.ActiveCell.Row.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParamValueGroup_Bll;
                    switch (e.Key)
                    {
                    }
                }
                if (navList.ActiveCell.Row.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcColumn_Bll;
                    switch (e.Key)
                    {
                    }
                }
                vXamMultiColumnComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridDatePicker_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                WcStoredProc_Bll obj = navList.ActiveCell.Row.Data as WcStoredProc_Bll;
                if (obj == null) { return; }
                switch (ctl.Name)
                {
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Leave);
            }
        }





        private void XamColorPicker_SelectedColorChanged(object sender, SelectedColorChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamColorPicker ctl = sender as XamColorPicker;
                if (ctl == null) { return; }
                WcStoredProc_Bll obj = navList.ActiveCell.Row.Data as WcStoredProc_Bll;
                if (obj == null) { return; }
                switch (((CellControl)ctl.Parent).Cell.Column.Key)
                {
                }
                XamColorPicker_SelectedColorChanged_CustomCode(ctl, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Leave);
            }
        }





        //  may not need this as the prop gets hit through binding.
        public void vDatePickerColumn_TextChanged(object sender, vDatePickerColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = navList.ActiveCell.Row.Data as WcStoredProc_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParam_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParamValueGroup_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcColumn_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                vDatePickerColumn_TextChanged_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vCheckColumn_CheckedChanged(object sender, vCheckColumnCheckedChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Enter);
                //if (_isRowInEditMode == false) { return; }
                vCheckBox ctl = sender as vCheckBox;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = navList.ActiveCell.Row.Data as WcStoredProc_Bll;
                    switch (e.Key)
                    {
                    case "Sp_IsBuildDataAccessMethod":
                        obj.Sp_IsBuildDataAccessMethod = (bool)ctl.IsChecked;
                        break;
                    case "Sp_IsFetchEntity":
                        obj.Sp_IsFetchEntity = (bool)ctl.IsChecked;
                        break;
                    case "Sp_IsTypeComboItem":
                        obj.Sp_IsTypeComboItem = (bool)ctl.IsChecked;
                        break;
                    case "Sp_IsStaticList":
                        obj.Sp_IsStaticList = (bool)ctl.IsChecked;
                        break;
                    case "Sp_IsInputParamsAsObjectArray":
                        obj.Sp_IsInputParamsAsObjectArray = (bool)ctl.IsChecked;
                        break;
                    case "Sp_HasNewParams":
                        obj.Sp_HasNewParams = (bool)ctl.IsChecked;
                        break;
                    case "Sp_IsParamsValid":
                        obj.Sp_IsParamsValid = (bool)ctl.IsChecked;
                        break;
                    case "Sp_IsParamValueSetValid":
                        obj.Sp_IsParamValueSetValid = (bool)ctl.IsChecked;
                        break;
                    case "Sp_HasNewColumns":
                        obj.Sp_HasNewColumns = (bool)ctl.IsChecked;
                        break;
                    case "Sp_IsColumnsValid":
                        obj.Sp_IsColumnsValid = (bool)ctl.IsChecked;
                        break;
                    case "Sp_IsValid":
                        obj.Sp_IsValid = (bool)ctl.IsChecked;
                        break;
                    case "Sp_IsActiveRow":
                        obj.Sp_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParam_Bll;
                    switch (e.Key)
                    {
                    case "SpP_IsNullable":
                        obj.SpP_IsNullable = (bool)ctl.IsChecked;
                        break;
                    case "SpP_IsActiveRow":
                        obj.SpP_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParamValueGroup_Bll;
                    switch (e.Key)
                    {
                    case "SpPVGrp_IsActiveRow":
                        obj.SpPVGrp_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcColumn_Bll;
                    switch (e.Key)
                    {
                    case "SpCl_IsNullable":
                        obj.SpCl_IsNullable = (bool)ctl.IsChecked;
                        break;
                    case "SpCl_IsVisible":
                        obj.SpCl_IsVisible = (bool)ctl.IsChecked;
                        break;
                    case "SpCl_Browsable":
                        obj.SpCl_Browsable = (bool)ctl.IsChecked;
                        break;
                    case "SpCl_IsActiveRow":
                        obj.SpCl_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Leave);
            }
        }

        #endregion GridCell Editor Events

        
        #region Grid Other Events and Event Support Methods
        

        void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Enter);
                Cell cell = (Cell)navList.ActiveCell;
                IvControlsValidation ctl = cell.Control.Content as IvControlsValidation;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcStoredProc_Bll)
                {
                    WcStoredProc_Bll obj = navList.ActiveCell.Row.Data as WcStoredProc_Bll;

                    switch (e.PropertyName)
                    {
                        case "Sp_Name":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_Name");
                            break;
                        case "Sp_TypeName":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_TypeName");
                            break;
                        case "Sp_MethodName":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_MethodName");
                            break;
                        case "Sp_IsBuildDataAccessMethod":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_IsBuildDataAccessMethod");
                            break;
                        case "Sp_AssocEntity_Id":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_AssocEntity_Id");
                            break;
                        case "Sp_IsFetchEntity":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_IsFetchEntity");
                            break;
                        case "Sp_IsTypeComboItem":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_IsTypeComboItem");
                            break;
                        case "Sp_IsStaticList":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_IsStaticList");
                            break;
                        case "Sp_SpRsTp_Id":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_SpRsTp_Id");
                            break;
                        case "Sp_SpRtTp_Id":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_SpRtTp_Id");
                            break;
                        case "Sp_IsInputParamsAsObjectArray":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_IsInputParamsAsObjectArray");
                            break;
                        case "Sp_HasNewParams":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_HasNewParams");
                            break;
                        case "Sp_IsParamsValid":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_IsParamsValid");
                            break;
                        case "Sp_IsParamValueSetValid":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_IsParamValueSetValid");
                            break;
                        case "Sp_HasNewColumns":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_HasNewColumns");
                            break;
                        case "Sp_IsColumnsValid":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_IsColumnsValid");
                            break;
                        case "Sp_IsValid":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_IsValid");
                            break;
                        case "Sp_Notes":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_Notes");
                            break;
                        case "Sp_IsActiveRow":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_IsActiveRow");
                            break;
                        case "UserName":
                            SetGridCellValidationAppeance(ctl, obj, "UserName");
                            break;
                        case "Sp_LastModifiedDate":
                            SetGridCellValidationAppeance(ctl, obj, "Sp_LastModifiedDate");
                            break;
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcParam_Bll)
                {
                    WcStoredProcParam_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParam_Bll;
                    switch (e.PropertyName)
                    {
                    case "SpP_SortOrder":
                        SetGridCellValidationAppeance(ctl, obj, "SpP_SortOrder");
                        break;
                    case "SpP_Name":
                        SetGridCellValidationAppeance(ctl, obj, "SpP_Name");
                        break;
                    case "SpP_SpPDr_Id":
                        SetGridCellValidationAppeance(ctl, obj, "SpP_SpPDr_Id");
                        break;
                    case "SpP_DtNt_ID":
                        SetGridCellValidationAppeance(ctl, obj, "SpP_DtNt_ID");
                        break;
                    case "SpP_DtSq_ID":
                        SetGridCellValidationAppeance(ctl, obj, "SpP_DtSq_ID");
                        break;
                    case "SpP_IsNullable":
                        SetGridCellValidationAppeance(ctl, obj, "SpP_IsNullable");
                        break;
                    case "SpP_DefaultValue":
                        SetGridCellValidationAppeance(ctl, obj, "SpP_DefaultValue");
                        break;
                    case "SpP_Value":
                        SetGridCellValidationAppeance(ctl, obj, "SpP_Value");
                        break;
                    case "SpP_IsActiveRow":
                        SetGridCellValidationAppeance(ctl, obj, "SpP_IsActiveRow");
                        break;
                    case "SpP_LastModifiedDate":
                        SetGridCellValidationAppeance(ctl, obj, "SpP_LastModifiedDate");
                        break;
                    case "UserName":
                        SetGridCellValidationAppeance(ctl, obj, "UserName");
                        break;
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcParamValueGroup_Bll)
                {
                    WcStoredProcParamValueGroup_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcParamValueGroup_Bll;
                    switch (e.PropertyName)
                    {
                    case "SpPVGrp_Name":
                        SetGridCellValidationAppeance(ctl, obj, "SpPVGrp_Name");
                        break;
                    case "SpPVGrp_Notes":
                        SetGridCellValidationAppeance(ctl, obj, "SpPVGrp_Notes");
                        break;
                    case "SpPVGrp_IsActiveRow":
                        SetGridCellValidationAppeance(ctl, obj, "SpPVGrp_IsActiveRow");
                        break;
                    case "SpPVGrp_LastModifiedDate":
                        SetGridCellValidationAppeance(ctl, obj, "SpPVGrp_LastModifiedDate");
                        break;
                    case "UserName":
                        SetGridCellValidationAppeance(ctl, obj, "UserName");
                        break;
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcStoredProcColumn_Bll)
                {
                    WcStoredProcColumn_Bll obj = navList.ActiveCell.Row.Data as WcStoredProcColumn_Bll;
                    switch (e.PropertyName)
                    {
                    case "SpCl_Name":
                        SetGridCellValidationAppeance(ctl, obj, "SpCl_Name");
                        break;
                    case "SpCl_ColIndex":
                        SetGridCellValidationAppeance(ctl, obj, "SpCl_ColIndex");
                        break;
                    case "SpCl_ColLetter":
                        SetGridCellValidationAppeance(ctl, obj, "SpCl_ColLetter");
                        break;
                    case "SpCl_DtNt_ID":
                        SetGridCellValidationAppeance(ctl, obj, "SpCl_DtNt_ID");
                        break;
                    case "SpCl_DtSq_ID":
                        SetGridCellValidationAppeance(ctl, obj, "SpCl_DtSq_ID");
                        break;
                    case "SpCl_IsNullable":
                        SetGridCellValidationAppeance(ctl, obj, "SpCl_IsNullable");
                        break;
                    case "SpCl_IsVisible":
                        SetGridCellValidationAppeance(ctl, obj, "SpCl_IsVisible");
                        break;
                    case "SpCl_Browsable":
                        SetGridCellValidationAppeance(ctl, obj, "SpCl_Browsable");
                        break;
                    case "SpCl_Notes":
                        SetGridCellValidationAppeance(ctl, obj, "SpCl_Notes");
                        break;
                    case "SpCl_IsActiveRow":
                        SetGridCellValidationAppeance(ctl, obj, "SpCl_IsActiveRow");
                        break;
                    case "SpCl_LastModifiedDate":
                        SetGridCellValidationAppeance(ctl, obj, "SpCl_LastModifiedDate");
                        break;
                    case "UserName":
                        SetGridCellValidationAppeance(ctl, obj, "UserName");
                        break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Leave);
            }
        }

        #endregion Grid Other Events and Event Support Methods

        
        #region Methods


        void AssignTextLengthLabels()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Enter);

                ((vTextColumn)navList.Columns["Sp_Name"]).MaxTextLength = 75;
                ((vTextColumn)navList.Columns["Sp_TypeName"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Sp_MethodName"]).MaxTextLength = 75;
                ((vTextColumn)navList.Columns["Sp_Notes"]).MaxTextLength = 255;
                ((vTextColumn)navList.Columns["UserName"]).MaxTextLength = 60;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_Name"]).MaxTextLength = 50;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_DefaultValue"]).MaxTextLength = 100;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_Value"]).MaxTextLength = 300;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["UserName"]).MaxTextLength = 60;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParamValueGroup"]).Columns["SpPVGrp_Name"]).MaxTextLength = 50;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParamValueGroup"]).Columns["SpPVGrp_Notes"]).MaxTextLength = 255;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParamValueGroup"]).Columns["UserName"]).MaxTextLength = 60;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_Name"]).MaxTextLength = 128;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_ColLetter"]).MaxTextLength = 3;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_Notes"]).MaxTextLength = 255;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["UserName"]).MaxTextLength = 60;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Leave);
            }
        }



        void ConfigureColumnHeaderTooltips()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Enter);

                    // Sp_TypeName
                    ((IvColumn)navList.Columns["Sp_TypeName"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_TypeName_Vbs; 
                    ((IvColumn)navList.Columns["Sp_TypeName"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_TypeName_1, StringsWcStoredProcListTooltips.Sp_TypeName_2};
                    ((IvColumn)navList.Columns["Sp_TypeName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_MethodName
                    ((IvColumn)navList.Columns["Sp_MethodName"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_MethodName_Vbs; 
                    ((IvColumn)navList.Columns["Sp_MethodName"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_MethodName_1};
                    ((IvColumn)navList.Columns["Sp_MethodName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_IsBuildDataAccessMethod
                    ((IvColumn)navList.Columns["Sp_IsBuildDataAccessMethod"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_IsBuildDataAccessMethod_Vbs; 
                    ((IvColumn)navList.Columns["Sp_IsBuildDataAccessMethod"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_IsBuildDataAccessMethod_1, StringsWcStoredProcListTooltips.Sp_IsBuildDataAccessMethod_2};
                    ((IvColumn)navList.Columns["Sp_IsBuildDataAccessMethod"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_AssocEntity_Id
                    ((vComboColumnBase)navList.Columns["Sp_AssocEntity_Id"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_AssocEntity_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["Sp_AssocEntity_Id"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_AssocEntity_Id_1, StringsWcStoredProcListTooltips.Sp_AssocEntity_Id_2};
                    ((vComboColumnBase)navList.Columns["Sp_AssocEntity_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_IsFetchEntity
                    ((IvColumn)navList.Columns["Sp_IsFetchEntity"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_IsFetchEntity_Vbs; 
                    ((IvColumn)navList.Columns["Sp_IsFetchEntity"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_IsFetchEntity_1, StringsWcStoredProcListTooltips.Sp_IsFetchEntity_2};
                    ((IvColumn)navList.Columns["Sp_IsFetchEntity"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_IsTypeComboItem
                    ((IvColumn)navList.Columns["Sp_IsTypeComboItem"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_IsTypeComboItem_Vbs; 
                    ((IvColumn)navList.Columns["Sp_IsTypeComboItem"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_IsTypeComboItem_1, StringsWcStoredProcListTooltips.Sp_IsTypeComboItem_2};
                    ((IvColumn)navList.Columns["Sp_IsTypeComboItem"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_IsStaticList
                    ((IvColumn)navList.Columns["Sp_IsStaticList"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_IsStaticList_Vbs; 
                    ((IvColumn)navList.Columns["Sp_IsStaticList"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_IsStaticList_1};
                    ((IvColumn)navList.Columns["Sp_IsStaticList"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_SpRsTp_Id
                    ((vComboColumnBase)navList.Columns["Sp_SpRsTp_Id"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_SpRsTp_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["Sp_SpRsTp_Id"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_SpRsTp_Id_1, StringsWcStoredProcListTooltips.Sp_SpRsTp_Id_2};
                    ((vComboColumnBase)navList.Columns["Sp_SpRsTp_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_SpRtTp_Id
                    ((vComboColumnBase)navList.Columns["Sp_SpRtTp_Id"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_SpRtTp_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["Sp_SpRtTp_Id"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_SpRtTp_Id_1, StringsWcStoredProcListTooltips.Sp_SpRtTp_Id_2};
                    ((vComboColumnBase)navList.Columns["Sp_SpRtTp_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_IsInputParamsAsObjectArray
                    ((IvColumn)navList.Columns["Sp_IsInputParamsAsObjectArray"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_IsInputParamsAsObjectArray_Vbs; 
                    ((IvColumn)navList.Columns["Sp_IsInputParamsAsObjectArray"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_IsInputParamsAsObjectArray_1, StringsWcStoredProcListTooltips.Sp_IsInputParamsAsObjectArray_2};
                    ((IvColumn)navList.Columns["Sp_IsInputParamsAsObjectArray"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_HasNewParams
                    ((IvColumn)navList.Columns["Sp_HasNewParams"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_HasNewParams_Vbs; 
                    ((IvColumn)navList.Columns["Sp_HasNewParams"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_HasNewParams_1, StringsWcStoredProcListTooltips.Sp_HasNewParams_2};
                    ((IvColumn)navList.Columns["Sp_HasNewParams"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_IsParamsValid
                    ((IvColumn)navList.Columns["Sp_IsParamsValid"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_IsParamsValid_Vbs; 
                    ((IvColumn)navList.Columns["Sp_IsParamsValid"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_IsParamsValid_1};
                    ((IvColumn)navList.Columns["Sp_IsParamsValid"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_IsParamValueSetValid
                    ((IvColumn)navList.Columns["Sp_IsParamValueSetValid"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_IsParamValueSetValid_Vbs; 
                    ((IvColumn)navList.Columns["Sp_IsParamValueSetValid"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_IsParamValueSetValid_1, StringsWcStoredProcListTooltips.Sp_IsParamValueSetValid_2};
                    ((IvColumn)navList.Columns["Sp_IsParamValueSetValid"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_HasNewColumns
                    ((IvColumn)navList.Columns["Sp_HasNewColumns"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_HasNewColumns_Vbs; 
                    ((IvColumn)navList.Columns["Sp_HasNewColumns"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_HasNewColumns_1, StringsWcStoredProcListTooltips.Sp_HasNewColumns_2};
                    ((IvColumn)navList.Columns["Sp_HasNewColumns"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_IsColumnsValid
                    ((IvColumn)navList.Columns["Sp_IsColumnsValid"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_IsColumnsValid_Vbs; 
                    ((IvColumn)navList.Columns["Sp_IsColumnsValid"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_IsColumnsValid_1, StringsWcStoredProcListTooltips.Sp_IsColumnsValid_2};
                    ((IvColumn)navList.Columns["Sp_IsColumnsValid"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_IsValid
                    ((IvColumn)navList.Columns["Sp_IsValid"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_IsValid_Vbs; 
                    ((IvColumn)navList.Columns["Sp_IsValid"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_IsValid_1, StringsWcStoredProcListTooltips.Sp_IsValid_2};
                    ((IvColumn)navList.Columns["Sp_IsValid"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_IsActiveRow
                    ((IvColumn)navList.Columns["Sp_IsActiveRow"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_IsActiveRow_Vbs; 
                    ((IvColumn)navList.Columns["Sp_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_IsActiveRow_1, StringsWcStoredProcListTooltips.Sp_IsActiveRow_2};
                    ((IvColumn)navList.Columns["Sp_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)navList.Columns["UserName"]).HeaderToolTipCaption = StringsWcStoredProcList.UserName_Vbs; 
                    ((IvColumn)navList.Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.UserName_1};
                    ((IvColumn)navList.Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Sp_LastModifiedDate
                    ((IvColumn)navList.Columns["Sp_LastModifiedDate"]).HeaderToolTipCaption = StringsWcStoredProcList.Sp_LastModifiedDate_Vbs; 
                    ((IvColumn)navList.Columns["Sp_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcListTooltips.Sp_LastModifiedDate_1};
                    ((IvColumn)navList.Columns["Sp_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // SpP_IsActiveRow
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_IsActiveRow"]).HeaderToolTipCaption = StringsWcStoredProcParamList.SpP_IsActiveRow_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcParamListTooltips.SpP_IsActiveRow_1, StringsWcStoredProcParamListTooltips.SpP_IsActiveRow_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["UserName"]).HeaderToolTipCaption = StringsWcStoredProcParamList.UserName_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcParamListTooltips.UserName_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // SpP_LastModifiedDate
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_LastModifiedDate"]).HeaderToolTipCaption = StringsWcStoredProcParamList.SpP_LastModifiedDate_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcParamListTooltips.SpP_LastModifiedDate_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // SpPVGrp_IsActiveRow
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParamValueGroup"]).Columns["SpPVGrp_IsActiveRow"]).HeaderToolTipCaption = StringsWcStoredProcParamValueGroupList.SpPVGrp_IsActiveRow_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParamValueGroup"]).Columns["SpPVGrp_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcParamValueGroupListTooltips.SpPVGrp_IsActiveRow_1, StringsWcStoredProcParamValueGroupListTooltips.SpPVGrp_IsActiveRow_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParamValueGroup"]).Columns["SpPVGrp_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParamValueGroup"]).Columns["UserName"]).HeaderToolTipCaption = StringsWcStoredProcParamValueGroupList.UserName_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParamValueGroup"]).Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcParamValueGroupListTooltips.UserName_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParamValueGroup"]).Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // SpPVGrp_LastModifiedDate
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParamValueGroup"]).Columns["SpPVGrp_LastModifiedDate"]).HeaderToolTipCaption = StringsWcStoredProcParamValueGroupList.SpPVGrp_LastModifiedDate_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParamValueGroup"]).Columns["SpPVGrp_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcParamValueGroupListTooltips.SpPVGrp_LastModifiedDate_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParamValueGroup"]).Columns["SpPVGrp_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // SpCl_ColLetter
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_ColLetter"]).HeaderToolTipCaption = StringsWcStoredProcColumnList.SpCl_ColLetter_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_ColLetter"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcColumnListTooltips.SpCl_ColLetter_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_ColLetter"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // SpCl_IsActiveRow
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_IsActiveRow"]).HeaderToolTipCaption = StringsWcStoredProcColumnList.SpCl_IsActiveRow_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcColumnListTooltips.SpCl_IsActiveRow_1, StringsWcStoredProcColumnListTooltips.SpCl_IsActiveRow_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["UserName"]).HeaderToolTipCaption = StringsWcStoredProcColumnList.UserName_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcColumnListTooltips.UserName_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // SpCl_LastModifiedDate
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_LastModifiedDate"]).HeaderToolTipCaption = StringsWcStoredProcColumnList.SpCl_LastModifiedDate_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsWcStoredProcColumnListTooltips.SpCl_LastModifiedDate_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();
                ConfigureColumnHeaderTooltips_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Leave);
            }
        }


        void Set_vXamComboColumn_ItemSources()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["Sp_AssocEntity_Id"]).ItemsSource = WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["Sp_AssocEntity_Id"]).DisplayMemberPath = "ItemName";
                WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTable_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)navList.Columns["Sp_SpRsTp_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcStoredProcResultType_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["Sp_SpRsTp_Id"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcStoredProcResultType_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcStoredProcResultType_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)navList.Columns["Sp_SpRtTp_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcStoredProcReturnType_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["Sp_SpRtTp_Id"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcStoredProcReturnType_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcStoredProcReturnType_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_SpPDr_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcStoredProcParamDirection_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_SpPDr_Id"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcStoredProcParamDirection_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcStoredProcParamDirection_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_DtNt_ID"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_DtNt_ID"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_DtSq_ID"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcParam"]).Columns["SpP_DtSq_ID"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_DtNt_ID"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_DtNt_ID"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_DtSq_ID"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcStoredProcColumn"]).Columns["SpCl_DtSq_ID"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Leave);
            }
        }

        void WcTable_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["Sp_AssocEntity_Id"]).ItemsSource = WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcStoredProcResultType_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcResultType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["Sp_SpRsTp_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcStoredProcResultType_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcResultType_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcResultType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcStoredProcReturnType_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcReturnType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["Sp_SpRtTp_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcStoredProcReturnType_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcReturnType_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcReturnType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_wcStoredProcColumn"]).Columns["SpCl_DtNt_ID"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_wcStoredProcParam"]).Columns["SpP_DtNt_ID"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_wcStoredProcColumn"]).Columns["SpCl_DtSq_ID"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_wcStoredProcParam"]).Columns["SpP_DtSq_ID"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcStoredProcParamDirection_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamDirection_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_wcStoredProcParam"]).Columns["SpP_SpPDr_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcStoredProcParamDirection_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamDirection_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamDirection_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        #region Combo ItemsSource for Non-Static Lists

        #endregion region Combo ItemsSource for Non-Static Lists

        void SetNewRowValidation()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Enter);
                if (_activeRow == null) { return; }
                WcStoredProc_Bll obj = _activeRow.Data as WcStoredProc_Bll;
                if (obj == null) { return; }
                // Note:  The lines to set the validaton styles below are not needed as the styles are being set from converters in the xaml style propeties, but they are left here incase needed in the future.  this is new code and we are unsure how it will evolve.
                WcStoredProcParam_Bll oWcStoredProcParam = _activeRow.Data as WcStoredProcParam_Bll;
                if (oWcStoredProcParam == null) { return; }
                WcStoredProcParamValueGroup_Bll oWcStoredProcParamValueGroup = _activeRow.Data as WcStoredProcParamValueGroup_Bll;
                if (oWcStoredProcParamValueGroup == null) { return; }
                WcStoredProcColumn_Bll oWcStoredProcColumn = _activeRow.Data as WcStoredProcColumn_Bll;
                if (oWcStoredProcColumn == null) { return; }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Leave);
            }
        }

        #endregion Methods


    }
}
