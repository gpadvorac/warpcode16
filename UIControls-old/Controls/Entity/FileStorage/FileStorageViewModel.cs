﻿using System.Windows.Input;
using EntityWireTypeSL;
using vCommands;

namespace UIControls.Controls.Entity.FileStorage
{
    public class FileStorageViewModel
    {
        private readonly ICommand _downloadFileCommand;

        public FileStorageViewModel(FileStorage_Values fileStorageValues)
        {
            _downloadFileCommand = new ActionCommand(() => FilesHelper.DownloadFile(FileStorageValues));

            FileStorageValues = fileStorageValues;
        }

        public FileStorage_Values FileStorageValues { get; set; }

        public ICommand DownloadFileCommand
        {
            get { return _downloadFileCommand; }
        }
    }
}