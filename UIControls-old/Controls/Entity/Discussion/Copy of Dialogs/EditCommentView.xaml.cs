﻿using System;

namespace UIControls
{
    public partial class EditCommentView : IDisposable
    {
        private EditCommentDialogDirector _mediator;

        public EditCommentView(EditCommentDialogDirector mediator)
        {
            InitializeComponent();
            _mediator = mediator;
        }

        #region IDisposable

        void IDisposable.Dispose()
        {
            _mediator.Dispose();
            _mediator = null;
        }

        #endregion IDisposable
    }
}
