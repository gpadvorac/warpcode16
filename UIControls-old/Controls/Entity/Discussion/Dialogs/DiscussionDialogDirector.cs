﻿using System;
using EntityBll.SL;
using Infragistics.Controls.Interactions;
using UIControls.Controls.Entity.Discussion;
using vDialogControl;

namespace UIControls
{
    public class DiscussionDialogDirector : EditDialogDirector<DiscussionViewModel>
    {
        public DiscussionDialogDirector(Discussion_List discussions, Guid parentId, DiscussionParentTypes parentType)
        {
            Dialog = new vDialog
            {
                Width = 700,
                UseCloseCommand = true,
                Height = 500,
                StartupPosition = StartupPosition.Center,
                Content = new DiscussionView(),
            };
            Dialog.CloseCommand = _vm.CloseCommand;
            _vm.Discussions = discussions;
            _vm.ParentId = parentId;
            _vm.ParentType= parentType;
        }
    }
}