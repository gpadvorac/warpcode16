﻿using System;

namespace UIControls
{
    public partial class NewDiscussionView : IDisposable
    {
        private NewDiscussionDialogDirector _mediator;

        public NewDiscussionView(NewDiscussionDialogDirector mediator)
        {
            InitializeComponent();
            _mediator = mediator;
        }

        #region IDisposable

        void IDisposable.Dispose()
        {
            _mediator.Dispose();
            _mediator = null;
        }

        #endregion IDisposable
    }
}
