﻿namespace UIControls
{
    public class EditCommentDialogDirector : EditDialogDirector<EditCommentViewModel>
    {
        public EditCommentDialogDirector(string comment)
        {
            Dialog = new EditCommentView(this);
            _vm.Comment = comment;
        }
    }
}