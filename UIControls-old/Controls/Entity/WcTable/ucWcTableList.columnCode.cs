using System;
using System.Windows;
using System.Windows.Controls;
using Ifx.SL;
using TypeServices;
using vControls;
using EntityBll.SL;
using Infragistics.Controls.Editors;
using vUICommon;
using EntityWireTypeSL;
using Infragistics.Controls.Grids;
using vComboDataTypes;
using System.Collections.Generic;
using vUICommon.Controls;
using UIControls.Globalization.WcTable;
using UIControls.Globalization.WcTableChild;
using UIControls.Globalization.WcTableSortBy;
using UIControls.Globalization.WcTableColumn;
namespace UIControls
{
    public partial class ucWcTableList
    {

        
        #region GridCell Editor Events
        

        private void XamGridTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                if ((ctl.Text.Length>0) && (ctl.Text.Substring(ctl.Text.Length - 1, 1) == " "))
                {
                    return;
                }
                WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                if (obj == null) { return; }
                //switch (ctl.Name)
                //{
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vTextColumn_TextChanged(object sender, vTextColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (e.Key)
                    {
                        case "Tb_Name":
                            obj.Tb_Name = ctl.Text;
                            break;
                        case "Tb_EntityRootName":
                            obj.Tb_EntityRootName = ctl.Text;
                            break;
                        case "Tb_VariableName":
                            obj.Tb_VariableName = ctl.Text;
                            break;
                        case "Tb_ScreenCaption":
                            obj.Tb_ScreenCaption = ctl.Text;
                            break;
                        case "Tb_Description":
                            obj.Tb_Description = ctl.Text;
                            break;
                        case "Tb_DevelopmentNote":
                            obj.Tb_DevelopmentNote = ctl.Text;
                            break;
                        case "Tb_UIAssemblyPath":
                            obj.Tb_UIAssemblyPath = ctl.Text;
                            break;
                        case "Tb_UIAssemblyName":
                            obj.Tb_UIAssemblyName = ctl.Text;
                            break;
                        case "Tb_UINamespace":
                            obj.Tb_UINamespace = ctl.Text;
                            break;
                        case "Tb_WebServiceFolder":
                            obj.Tb_WebServiceFolder = ctl.Text;
                            break;
                        case "Tb_WebServiceName":
                            obj.Tb_WebServiceName = ctl.Text;
                            break;
                        case "Tb_DataServicePath":
                            obj.Tb_DataServicePath = ctl.Text;
                            break;
                        case "Tb_DataServiceAssemblyName":
                            obj.Tb_DataServiceAssemblyName = ctl.Text;
                            break;
                        case "Tb_WireTypePath":
                            obj.Tb_WireTypePath = ctl.Text;
                            break;
                        case "Tb_WireTypeAssemblyName":
                            obj.Tb_WireTypeAssemblyName = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (e.Key)
                    {
                        case "TbChd_SortOrder":
                            obj.TbChd_SortOrder_asString = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcTableSortBy_Bll)
                {
                    WcTableSortBy_Bll obj = navList.ActiveCell.Row.Data as WcTableSortBy_Bll;
                    switch (e.Key)
                    {
                        case "TbSb_SortOrder":
                            obj.TbSb_SortOrder_asString = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (e.Key)
                    {
                        case "TbC_SortOrder":
                            obj.TbC_SortOrder_asString = ctl.Text;
                            break;
                        case "TbC_Name":
                            obj.TbC_Name = ctl.Text;
                            break;
                        case "TbC_Description":
                            obj.TbC_Description = ctl.Text;
                            break;
                        case "TbC_Length":
                            obj.TbC_Length_asString = ctl.Text;
                            break;
                        case "TbC_Precision":
                            obj.TbC_Precision_asString = ctl.Text;
                            break;
                        case "TbC_Scale":
                            obj.TbC_Scale_asString = ctl.Text;
                            break;
                        case "TbC_BrokenRuleText":
                            obj.TbC_BrokenRuleText = ctl.Text;
                            break;
                        case "TbC_DefaultValue":
                            obj.TbC_DefaultValue = ctl.Text;
                            break;
                        case "TbC_DefaultCaption":
                            obj.TbC_DefaultCaption = ctl.Text;
                            break;
                        case "TbC_ColumnHeaderText":
                            obj.TbC_ColumnHeaderText = ctl.Text;
                            break;
                        case "TbC_LabelCaptionVerbose":
                            obj.TbC_LabelCaptionVerbose = ctl.Text;
                            break;
                        case "TbC_TextBoxFormat":
                            obj.TbC_TextBoxFormat = ctl.Text;
                            break;
                        case "TbC_TextColumnFormat":
                            obj.TbC_TextColumnFormat = ctl.Text;
                            break;
                        case "TbC_TextBoxTextAlignment":
                            obj.TbC_TextBoxTextAlignment = ctl.Text;
                            break;
                        case "TbC_ColumnTextAlignment":
                            obj.TbC_ColumnTextAlignment = ctl.Text;
                            break;
                        case "TbC_DeveloperNote":
                            obj.TbC_DeveloperNote = ctl.Text;
                            break;
                        case "TbC_UserNote":
                            obj.TbC_UserNote = ctl.Text;
                            break;
                        case "TbC_HelpFileAdditionalNote":
                            obj.TbC_HelpFileAdditionalNote = ctl.Text;
                            break;
                        case "TbC_Notes":
                            obj.TbC_Notes = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vDecimalColumn_TextChanged(object sender, vDecimalColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                int pos = ctl.SelectionStart;
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcTableSortBy_Bll)
                {
                    WcTableSortBy_Bll obj = navList.ActiveCell.Row.Data as WcTableSortBy_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (e.Key)
                    {
                        case "TbC_ColumnWidth":
                            obj.TbC_ColumnWidth_asString = ctl.Text;
                            break;
                        case "TbC_Combo_MaxDropdownHeight":
                            obj.TbC_Combo_MaxDropdownHeight_asString = ctl.Text;
                            break;
                        case "TbC_Combo_MaxDropdownWidth":
                            obj.TbC_Combo_MaxDropdownWidth_asString = ctl.Text;
                            break;
                        default:
                            break;
                    }
                    ctl.SelectionStart = pos;
                    vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamComboEditor comboEditor = sender as XamComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_SelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (ctl.Name)
                    {
                        case "Tb_ApCnSK_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Tb_ApCnSK_Id = null;
                            }
                            else
                            {
                                obj.Tb_ApCnSK_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (ctl.Name)
                    {
                        case "TbChd_Child_Tb_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_Child_Tb_Id = null;
                            }
                            else
                            {
                                obj.TbChd_Child_Tb_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_ChildForeignKeyColumn_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_ChildForeignKeyColumn_Id = null;
                            }
                            else
                            {
                                obj.TbChd_ChildForeignKeyColumn_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_ChildForeignKeyColumn22_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_ChildForeignKeyColumn22_Id = null;
                            }
                            else
                            {
                                obj.TbChd_ChildForeignKeyColumn22_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_DifferentCombo_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_DifferentCombo_Id = null;
                            }
                            else
                            {
                                obj.TbChd_DifferentCombo_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_ChildForeignKeyColumn33_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_ChildForeignKeyColumn33_Id = null;
                            }
                            else
                            {
                                obj.TbChd_ChildForeignKeyColumn33_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcTableSortBy_Bll)
                {
                    WcTableSortBy_Bll obj = navList.ActiveCell.Row.Data as WcTableSortBy_Bll;
                    switch (ctl.Name)
                    {
                        case "TbSb_TbC_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbSb_TbC_Id = null;
                            }
                            else
                            {
                                obj.TbSb_TbC_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbSb_Direction":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbSb_Direction = null;
                            }
                            else
                            {
                                obj.TbSb_Direction = (String)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (ctl.Name)
                    {
                        case "TbC_DtSql_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_DtSql_Id = null;
                            }
                            else
                            {
                                obj.TbC_DtSql_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_DtDtNt_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_DtDtNt_Id = null;
                            }
                            else
                            {
                                obj.TbC_DtDtNt_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_CtlTp_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_CtlTp_Id = null;
                            }
                            else
                            {
                                obj.TbC_CtlTp_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ListStoredProc_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ListStoredProc_Id = null;
                            }
                            else
                            {
                                obj.TbC_ListStoredProc_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ParentColumnKey":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ParentColumnKey = null;
                            }
                            else
                            {
                                obj.TbC_ParentColumnKey = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ComboListTable_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ComboListTable_Id = null;
                            }
                            else
                            {
                                obj.TbC_ComboListTable_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ComboListDisplayColumn_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ComboListDisplayColumn_Id = null;
                            }
                            else
                            {
                                obj.TbC_ComboListDisplayColumn_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_SelectionChanged(object sender, vXamComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                // The next 2 lines are for the Not In List feature.  If the 'Non-In-List' event is fireing, we dont want to run this event right now.
                if (ctl.SelectedItem == null) { return; }
                if (((ComboItem)ctl.SelectedItem).Id == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (e.Key)
                    {
                        case "Tb_ApCnSK_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.Tb_ApCnSK_Id = null;
                            }
                            else
                            {
                                obj.Tb_ApCnSK_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }
                if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (e.Key)
                    {
                        case "TbChd_Child_Tb_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_Child_Tb_Id = null;
                            }
                            else
                            {
                                obj.TbChd_Child_Tb_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_ChildForeignKeyColumn_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_ChildForeignKeyColumn_Id = null;
                            }
                            else
                            {
                                obj.TbChd_ChildForeignKeyColumn_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_ChildForeignKeyColumn22_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_ChildForeignKeyColumn22_Id = null;
                            }
                            else
                            {
                                obj.TbChd_ChildForeignKeyColumn22_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_DifferentCombo_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_DifferentCombo_Id = null;
                            }
                            else
                            {
                                obj.TbChd_DifferentCombo_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbChd_ChildForeignKeyColumn33_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbChd_ChildForeignKeyColumn33_Id = null;
                            }
                            else
                            {
                                obj.TbChd_ChildForeignKeyColumn33_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }


                if (navList.ActiveCell.Row.Data is WcTableSortBy_Bll)
                {
                    WcTableSortBy_Bll obj = navList.ActiveCell.Row.Data as WcTableSortBy_Bll;
                    switch (e.Key)
                    {
                        case "TbSb_TbC_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbSb_TbC_Id = null;
                            }
                            else
                            {
                                obj.TbSb_TbC_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbSb_Direction":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbSb_Direction = null;
                            }
                            else
                            {
                                obj.TbSb_Direction = (String)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }


                if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (e.Key)
                    {
                        case "TbC_DtSql_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_DtSql_Id = null;
                            }
                            else
                            {
                                obj.TbC_DtSql_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_DtDtNt_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_DtDtNt_Id = null;
                            }
                            else
                            {
                                obj.TbC_DtDtNt_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_CtlTp_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_CtlTp_Id = null;
                            }
                            else
                            {
                                obj.TbC_CtlTp_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ListStoredProc_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ListStoredProc_Id = null;
                            }
                            else
                            {
                                obj.TbC_ListStoredProc_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ParentColumnKey":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ParentColumnKey = null;
                            }
                            else
                            {
                                obj.TbC_ParentColumnKey = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ComboListTable_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ComboListTable_Id = null;
                            }
                            else
                            {
                                obj.TbC_ComboListTable_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                        case "TbC_ComboListDisplayColumn_Id":
                            if (ctl.SelectedItem == null)
                            {
                                obj.TbC_ComboListDisplayColumn_Id = null;
                            }
                            else
                            {
                                obj.TbC_ComboListDisplayColumn_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                            }
                            break;
                    }
                }


                vXamComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Enter);
                if (e.Key == System.Windows.Input.Key.Escape)
                {
                    WcTable_Bll obj = _activeRow.Data as WcTable_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_KeyUp(object sender, vXamComboColumnKeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Enter);
                if (e.XamComboColumnKeyEventArgs.Key == System.Windows.Input.Key.Escape)
                {
                    WcTable_Bll obj = _activeRow.Data as WcTable_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void XamMultiColumnComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamMultiColumnComboEditor comboEditor = sender as XamMultiColumnComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void vXamMultiColumnComboColumn_SelectionChanged(object sender, vXamMultiColumnComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vXamMultiColumnComboEditor ctl = sender as vXamMultiColumnComboEditor;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (e.Key)
                    {
                    }
                }
                if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (e.Key)
                    {
                    }
                }
                if (navList.ActiveCell.Row.Data is WcTableSortBy_Bll)
                {
                    WcTableSortBy_Bll obj = navList.ActiveCell.Row.Data as WcTableSortBy_Bll;
                    switch (e.Key)
                    {
                    }
                }
                if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (e.Key)
                    {
                    }
                }
                vXamMultiColumnComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridDatePicker_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                if (obj == null) { return; }
                switch (ctl.Name)
                {
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Leave);
            }
        }





        private void XamColorPicker_SelectedColorChanged(object sender, SelectedColorChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamColorPicker ctl = sender as XamColorPicker;
                if (ctl == null) { return; }
                WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                if (obj == null) { return; }
                switch (((CellControl)ctl.Parent).Cell.Column.Key)
                {
                }
                XamColorPicker_SelectedColorChanged_CustomCode(ctl, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Leave);
            }
        }





        //  may not need this as the prop gets hit through binding.
        public void vDatePickerColumn_TextChanged(object sender, vDatePickerColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcTableSortBy_Bll)
                {
                    WcTableSortBy_Bll obj = navList.ActiveCell.Row.Data as WcTableSortBy_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (e.Key)
                    {
                        default:
                            break;
                    }
                }
                vDatePickerColumn_TextChanged_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vCheckColumn_CheckedChanged(object sender, vCheckColumnCheckedChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Enter);
                //if (_isRowInEditMode == false) { return; }
                vCheckBox ctl = sender as vCheckBox;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;
                    switch (e.Key)
                    {
                    case "Tb_IsComplete":
                        obj.Tb_IsComplete = (bool)ctl.IsChecked;
                        break;
                    case "Tb_UseTilesInPropsScreen":
                        obj.Tb_UseTilesInPropsScreen = (bool)ctl.IsChecked;
                        break;
                    case "Tb_UseGridColumnGroups":
                        obj.Tb_UseGridColumnGroups = (bool)ctl.IsChecked;
                        break;
                    case "Tb_UseGridDataSourceCombo":
                        obj.Tb_UseGridDataSourceCombo = (bool)ctl.IsChecked;
                        break;
                    case "Tb_UseLegacyConnectionCode":
                        obj.Tb_UseLegacyConnectionCode = (bool)ctl.IsChecked;
                        break;
                    case "Tb_PkIsIdentity":
                        obj.Tb_PkIsIdentity = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsVirtual":
                        obj.Tb_IsVirtual = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsNotEntity":
                        obj.Tb_IsNotEntity = (bool)ctl.IsChecked;
                        break;
                    case "Tb_UseLastModifiedByUserNameInSproc":
                        obj.Tb_UseLastModifiedByUserNameInSproc = (bool)ctl.IsChecked;
                        break;
                    case "Tb_UseUserTimeStamp":
                        obj.Tb_UseUserTimeStamp = (bool)ctl.IsChecked;
                        break;
                    case "Tb_UseForAudit":
                        obj.Tb_UseForAudit = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsCodeGen":
                        obj.Tb_IsCodeGen = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsCodeGenComplete":
                        obj.Tb_IsCodeGenComplete = (bool)ctl.IsChecked;
                        break;
                    case "Tb_IsActiveRow":
                        obj.Tb_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (e.Key)
                    {
                    case "TbChd_IsActiveRow":
                        obj.TbChd_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                    case "TbChd_IsDisplayAsChildGrid":
                        obj.TbChd_IsDisplayAsChildGrid = (bool)ctl.IsChecked;
                        break;
                    case "TbChd_IsDisplayAsChildTab":
                        obj.TbChd_IsDisplayAsChildTab = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcTableSortBy_Bll)
                {
                    WcTableSortBy_Bll obj = navList.ActiveCell.Row.Data as WcTableSortBy_Bll;
                    switch (e.Key)
                    {
                    case "TbSb_IsActiveRow":
                        obj.TbSb_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
                else if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (e.Key)
                    {
                    case "TbC_IsPK":
                        obj.TbC_IsPK = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsIdentity":
                        obj.TbC_IsIdentity = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsFK":
                        obj.TbC_IsFK = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsEntityColumn":
                        obj.TbC_IsEntityColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsValuesObjectMember":
                        obj.TbC_IsValuesObjectMember = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsReadFromDb":
                        obj.TbC_IsReadFromDb = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsInPropsScreen":
                        obj.TbC_IsInPropsScreen = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsInNavList":
                        obj.TbC_IsInNavList = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsRequired":
                        obj.TbC_IsRequired = (bool)ctl.IsChecked;
                        break;
                    case "TbC_AllowZero":
                        obj.TbC_AllowZero = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsNullableInDb":
                        obj.TbC_IsNullableInDb = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsNullableInUI":
                        obj.TbC_IsNullableInUI = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsSendToDb":
                        obj.TbC_IsSendToDb = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsInsertAllowed":
                        obj.TbC_IsInsertAllowed = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsEditAllowed":
                        obj.TbC_IsEditAllowed = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsReadOnlyInUI":
                        obj.TbC_IsReadOnlyInUI = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsSystemField":
                        obj.TbC_IsSystemField = (bool)ctl.IsChecked;
                        break;
                    case "TbC_UseForAudit":
                        obj.TbC_UseForAudit = (bool)ctl.IsChecked;
                        break;
                    case "TbC_LabelCaptionGenerate":
                        obj.TbC_LabelCaptionGenerate = (bool)ctl.IsChecked;
                        break;
                    case "Tbc_ShowGridColumnToolTip":
                        obj.Tbc_ShowGridColumnToolTip = (bool)ctl.IsChecked;
                        break;
                    case "Tbc_ShowPropsToolTip":
                        obj.Tbc_ShowPropsToolTip = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsCreatePropsStrings":
                        obj.TbC_IsCreatePropsStrings = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsCreateGridStrings":
                        obj.TbC_IsCreateGridStrings = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsAvailableForColumnGroups":
                        obj.TbC_IsAvailableForColumnGroups = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsTextWrapInProp":
                        obj.TbC_IsTextWrapInProp = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsTextWrapInGrid":
                        obj.TbC_IsTextWrapInGrid = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsStaticList":
                        obj.TbC_IsStaticList = (bool)ctl.IsChecked;
                        break;
                    case "TbC_UseNotInList":
                        obj.TbC_UseNotInList = (bool)ctl.IsChecked;
                        break;
                    case "TbC_UseListEditBtn":
                        obj.TbC_UseListEditBtn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsNotvColumn":
                        obj.TbC_IsNotvColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_UseDisplayTextFieldProperty":
                        obj.TbC_UseDisplayTextFieldProperty = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsDisplayTextFieldProperty":
                        obj.TbC_IsDisplayTextFieldProperty = (bool)ctl.IsChecked;
                        break;
                    case "TbC_Combo_AllowDropdownResizing":
                        obj.TbC_Combo_AllowDropdownResizing = (bool)ctl.IsChecked;
                        break;
                    case "TbC_Combo_IsResetButtonVisible":
                        obj.TbC_Combo_IsResetButtonVisible = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsActiveRecColumn":
                        obj.TbC_IsActiveRecColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsDeletedColumn":
                        obj.TbC_IsDeletedColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsCreatedUserIdColumn":
                        obj.TbC_IsCreatedUserIdColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsCreatedDateColumn":
                        obj.TbC_IsCreatedDateColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsUserIdColumn":
                        obj.TbC_IsUserIdColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsModifiedDateColumn":
                        obj.TbC_IsModifiedDateColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsRowVersionStampColumn":
                        obj.TbC_IsRowVersionStampColumn = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsBrowsable":
                        obj.TbC_IsBrowsable = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsCodeGen":
                        obj.TbC_IsCodeGen = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsActiveRow":
                        obj.TbC_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                    case "TbC_IsComplete":
                        obj.TbC_IsComplete = (bool)ctl.IsChecked;
                        break;
                        default:
                            break;
                    }
                    vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Leave);
            }
        }

        #endregion GridCell Editor Events

        
        #region Grid Other Events and Event Support Methods
        

        void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Enter);
                Cell cell = (Cell)navList.ActiveCell;
                IvControlsValidation ctl = cell.Control.Content as IvControlsValidation;
                if (ctl == null) { return; }
                if (navList.ActiveCell.Row.Data is WcTable_Bll)
                {
                    WcTable_Bll obj = navList.ActiveCell.Row.Data as WcTable_Bll;

                    switch (e.PropertyName)
                    {
                        case "AttachmentCount":
                            SetGridCellValidationAppeance(ctl, obj, "AttachmentCount");
                            break;
                        case "DiscussionCount":
                            SetGridCellValidationAppeance(ctl, obj, "DiscussionCount");
                            break;
                        case "Tb_IsComplete":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsComplete");
                            break;
                        case "Tb_Name":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_Name");
                            break;
                        case "Tb_EntityRootName":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_EntityRootName");
                            break;
                        case "Tb_VariableName":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_VariableName");
                            break;
                        case "Tb_ScreenCaption":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_ScreenCaption");
                            break;
                        case "Tb_Description":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_Description");
                            break;
                        case "Tb_DevelopmentNote":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_DevelopmentNote");
                            break;
                        case "Tb_UIAssemblyPath":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UIAssemblyPath");
                            break;
                        case "Tb_UIAssemblyName":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UIAssemblyName");
                            break;
                        case "Tb_UINamespace":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UINamespace");
                            break;
                        case "Tb_WebServiceFolder":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_WebServiceFolder");
                            break;
                        case "Tb_WebServiceName":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_WebServiceName");
                            break;
                        case "Tb_DataServicePath":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_DataServicePath");
                            break;
                        case "Tb_DataServiceAssemblyName":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_DataServiceAssemblyName");
                            break;
                        case "Tb_WireTypePath":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_WireTypePath");
                            break;
                        case "Tb_WireTypeAssemblyName":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_WireTypeAssemblyName");
                            break;
                        case "Tb_UseTilesInPropsScreen":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseTilesInPropsScreen");
                            break;
                        case "Tb_UseGridColumnGroups":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseGridColumnGroups");
                            break;
                        case "Tb_UseGridDataSourceCombo":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseGridDataSourceCombo");
                            break;
                        case "Tb_ApCnSK_Id":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_ApCnSK_Id");
                            break;
                        case "Tb_UseLegacyConnectionCode":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseLegacyConnectionCode");
                            break;
                        case "Tb_PkIsIdentity":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_PkIsIdentity");
                            break;
                        case "Tb_IsVirtual":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsVirtual");
                            break;
                        case "Tb_IsNotEntity":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsNotEntity");
                            break;
                        case "Tb_UseLastModifiedByUserNameInSproc":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseLastModifiedByUserNameInSproc");
                            break;
                        case "Tb_UseUserTimeStamp":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseUserTimeStamp");
                            break;
                        case "Tb_UseForAudit":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_UseForAudit");
                            break;
                        case "Tb_IsCodeGen":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsCodeGen");
                            break;
                        case "Tb_IsCodeGenComplete":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsCodeGenComplete");
                            break;
                        case "Tb_IsActiveRow":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_IsActiveRow");
                            break;
                        case "UserName":
                            SetGridCellValidationAppeance(ctl, obj, "UserName");
                            break;
                        case "Tb_LastModifiedDate":
                            SetGridCellValidationAppeance(ctl, obj, "Tb_LastModifiedDate");
                            break;
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcTableChild_Bll)
                {
                    WcTableChild_Bll obj = navList.ActiveCell.Row.Data as WcTableChild_Bll;
                    switch (e.PropertyName)
                    {
                    case "TbChd_SortOrder":
                        SetGridCellValidationAppeance(ctl, obj, "TbChd_SortOrder");
                        break;
                    case "TbChd_Child_Tb_Id":
                        SetGridCellValidationAppeance(ctl, obj, "TbChd_Child_Tb_Id");
                        break;
                    case "TbChd_ChildForeignKeyColumn_Id":
                        SetGridCellValidationAppeance(ctl, obj, "TbChd_ChildForeignKeyColumn_Id");
                        break;
                    case "TbChd_IsActiveRow":
                        SetGridCellValidationAppeance(ctl, obj, "TbChd_IsActiveRow");
                        break;
                    case "TbChd_LastModifiedDate":
                        SetGridCellValidationAppeance(ctl, obj, "TbChd_LastModifiedDate");
                        break;
                    case "UserName":
                        SetGridCellValidationAppeance(ctl, obj, "UserName");
                        break;
                    case "TbChd_IsDisplayAsChildGrid":
                        SetGridCellValidationAppeance(ctl, obj, "TbChd_IsDisplayAsChildGrid");
                        break;
                    case "TbChd_IsDisplayAsChildTab":
                        SetGridCellValidationAppeance(ctl, obj, "TbChd_IsDisplayAsChildTab");
                        break;
                    case "TbChd_ChildForeignKeyColumn22_Id":
                        SetGridCellValidationAppeance(ctl, obj, "TbChd_ChildForeignKeyColumn22_Id");
                        break;
                    case "TbChd_DifferentCombo_Id":
                        SetGridCellValidationAppeance(ctl, obj, "TbChd_DifferentCombo_Id");
                        break;
                    case "TbChd_ChildForeignKeyColumn33_Id":
                        SetGridCellValidationAppeance(ctl, obj, "TbChd_ChildForeignKeyColumn33_Id");
                        break;
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcTableSortBy_Bll)
                {
                    WcTableSortBy_Bll obj = navList.ActiveCell.Row.Data as WcTableSortBy_Bll;
                    switch (e.PropertyName)
                    {
                    case "TbSb_TbC_Id":
                        SetGridCellValidationAppeance(ctl, obj, "TbSb_TbC_Id");
                        break;
                    case "TbSb_SortOrder":
                        SetGridCellValidationAppeance(ctl, obj, "TbSb_SortOrder");
                        break;
                    case "TbSb_Direction":
                        SetGridCellValidationAppeance(ctl, obj, "TbSb_Direction");
                        break;
                    case "TbSb_IsActiveRow":
                        SetGridCellValidationAppeance(ctl, obj, "TbSb_IsActiveRow");
                        break;
                    case "TbSb_LastModifiedDate":
                        SetGridCellValidationAppeance(ctl, obj, "TbSb_LastModifiedDate");
                        break;
                    case "UserName":
                        SetGridCellValidationAppeance(ctl, obj, "UserName");
                        break;
                        default:
                            break;
                    }
                }
                else if (navList.ActiveCell.Row.Data is WcTableColumn_Bll)
                {
                    WcTableColumn_Bll obj = navList.ActiveCell.Row.Data as WcTableColumn_Bll;
                    switch (e.PropertyName)
                    {
                    case "TbC_SortOrder":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_SortOrder");
                        break;
                    case "TbC_Name":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_Name");
                        break;
                    case "TbC_Description":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_Description");
                        break;
                    case "TbC_DtSql_Id":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_DtSql_Id");
                        break;
                    case "TbC_DtDtNt_Id":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_DtDtNt_Id");
                        break;
                    case "TbC_Length":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_Length");
                        break;
                    case "TbC_Precision":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_Precision");
                        break;
                    case "TbC_Scale":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_Scale");
                        break;
                    case "TbC_IsPK":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsPK");
                        break;
                    case "TbC_IsIdentity":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsIdentity");
                        break;
                    case "TbC_IsFK":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsFK");
                        break;
                    case "TbC_IsEntityColumn":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsEntityColumn");
                        break;
                    case "TbC_IsValuesObjectMember":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsValuesObjectMember");
                        break;
                    case "TbC_IsReadFromDb":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsReadFromDb");
                        break;
                    case "TbC_IsInPropsScreen":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsInPropsScreen");
                        break;
                    case "TbC_IsInNavList":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsInNavList");
                        break;
                    case "TbC_IsRequired":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsRequired");
                        break;
                    case "TbC_BrokenRuleText":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_BrokenRuleText");
                        break;
                    case "TbC_AllowZero":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_AllowZero");
                        break;
                    case "TbC_IsNullableInDb":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsNullableInDb");
                        break;
                    case "TbC_IsNullableInUI":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsNullableInUI");
                        break;
                    case "TbC_DefaultValue":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_DefaultValue");
                        break;
                    case "TbC_IsSendToDb":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsSendToDb");
                        break;
                    case "TbC_IsInsertAllowed":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsInsertAllowed");
                        break;
                    case "TbC_IsEditAllowed":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsEditAllowed");
                        break;
                    case "TbC_IsReadOnlyInUI":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsReadOnlyInUI");
                        break;
                    case "TbC_IsSystemField":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsSystemField");
                        break;
                    case "TbC_UseForAudit":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_UseForAudit");
                        break;
                    case "TbC_CtlTp_Id":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_CtlTp_Id");
                        break;
                    case "TbC_DefaultCaption":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_DefaultCaption");
                        break;
                    case "TbC_ColumnHeaderText":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_ColumnHeaderText");
                        break;
                    case "TbC_LabelCaptionVerbose":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_LabelCaptionVerbose");
                        break;
                    case "TbC_LabelCaptionGenerate":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_LabelCaptionGenerate");
                        break;
                    case "Tbc_ShowGridColumnToolTip":
                        SetGridCellValidationAppeance(ctl, obj, "Tbc_ShowGridColumnToolTip");
                        break;
                    case "Tbc_ShowPropsToolTip":
                        SetGridCellValidationAppeance(ctl, obj, "Tbc_ShowPropsToolTip");
                        break;
                    case "TbC_IsCreatePropsStrings":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsCreatePropsStrings");
                        break;
                    case "TbC_IsCreateGridStrings":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsCreateGridStrings");
                        break;
                    case "TbC_IsAvailableForColumnGroups":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsAvailableForColumnGroups");
                        break;
                    case "TbC_IsTextWrapInProp":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsTextWrapInProp");
                        break;
                    case "TbC_IsTextWrapInGrid":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsTextWrapInGrid");
                        break;
                    case "TbC_TextBoxFormat":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_TextBoxFormat");
                        break;
                    case "TbC_TextColumnFormat":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_TextColumnFormat");
                        break;
                    case "TbC_ColumnWidth":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_ColumnWidth");
                        break;
                    case "TbC_TextBoxTextAlignment":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_TextBoxTextAlignment");
                        break;
                    case "TbC_ColumnTextAlignment":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_ColumnTextAlignment");
                        break;
                    case "TbC_ListStoredProc_Id":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_ListStoredProc_Id");
                        break;
                    case "TbC_IsStaticList":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsStaticList");
                        break;
                    case "TbC_UseNotInList":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_UseNotInList");
                        break;
                    case "TbC_UseListEditBtn":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_UseListEditBtn");
                        break;
                    case "TbC_IsNotvColumn":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsNotvColumn");
                        break;
                    case "TbC_ParentColumnKey":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_ParentColumnKey");
                        break;
                    case "TbC_UseDisplayTextFieldProperty":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_UseDisplayTextFieldProperty");
                        break;
                    case "TbC_IsDisplayTextFieldProperty":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsDisplayTextFieldProperty");
                        break;
                    case "TbC_ComboListTable_Id":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_ComboListTable_Id");
                        break;
                    case "TbC_ComboListDisplayColumn_Id":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_ComboListDisplayColumn_Id");
                        break;
                    case "TbC_Combo_MaxDropdownHeight":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_Combo_MaxDropdownHeight");
                        break;
                    case "TbC_Combo_MaxDropdownWidth":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_Combo_MaxDropdownWidth");
                        break;
                    case "TbC_Combo_AllowDropdownResizing":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_Combo_AllowDropdownResizing");
                        break;
                    case "TbC_Combo_IsResetButtonVisible":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_Combo_IsResetButtonVisible");
                        break;
                    case "TbC_IsActiveRecColumn":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsActiveRecColumn");
                        break;
                    case "TbC_IsDeletedColumn":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsDeletedColumn");
                        break;
                    case "TbC_IsCreatedUserIdColumn":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsCreatedUserIdColumn");
                        break;
                    case "TbC_IsCreatedDateColumn":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsCreatedDateColumn");
                        break;
                    case "TbC_IsUserIdColumn":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsUserIdColumn");
                        break;
                    case "TbC_IsModifiedDateColumn":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsModifiedDateColumn");
                        break;
                    case "TbC_IsRowVersionStampColumn":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsRowVersionStampColumn");
                        break;
                    case "TbC_IsBrowsable":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsBrowsable");
                        break;
                    case "TbC_DeveloperNote":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_DeveloperNote");
                        break;
                    case "TbC_UserNote":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_UserNote");
                        break;
                    case "TbC_HelpFileAdditionalNote":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_HelpFileAdditionalNote");
                        break;
                    case "TbC_Notes":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_Notes");
                        break;
                    case "TbC_IsCodeGen":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsCodeGen");
                        break;
                    case "TbC_IsActiveRow":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsActiveRow");
                        break;
                    case "TbC_LastModifiedDate":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_LastModifiedDate");
                        break;
                    case "UserName":
                        SetGridCellValidationAppeance(ctl, obj, "UserName");
                        break;
                    case "TooltipsRolledUp":
                        SetGridCellValidationAppeance(ctl, obj, "TooltipsRolledUp");
                        break;
                    case "TbC_IsComplete":
                        SetGridCellValidationAppeance(ctl, obj, "TbC_IsComplete");
                        break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Leave);
            }
        }

        #endregion Grid Other Events and Event Support Methods

        
        #region Methods


        void AssignTextLengthLabels()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Enter);

                ((vTextColumn)navList.Columns["Tb_Name"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_EntityRootName"]).MaxTextLength = 75;
                ((vTextColumn)navList.Columns["Tb_VariableName"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_ScreenCaption"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_Description"]).MaxTextLength = 2000;
                ((vTextColumn)navList.Columns["Tb_DevelopmentNote"]).MaxTextLength = 2000;
                ((vTextColumn)navList.Columns["Tb_UIAssemblyPath"]).MaxTextLength = 250;
                ((vTextColumn)navList.Columns["Tb_UIAssemblyName"]).MaxTextLength = 75;
                ((vTextColumn)navList.Columns["Tb_UINamespace"]).MaxTextLength = 75;
                ((vTextColumn)navList.Columns["Tb_WebServiceFolder"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_WebServiceName"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_DataServicePath"]).MaxTextLength = 250;
                ((vTextColumn)navList.Columns["Tb_DataServiceAssemblyName"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["Tb_WireTypePath"]).MaxTextLength = 250;
                ((vTextColumn)navList.Columns["Tb_WireTypeAssemblyName"]).MaxTextLength = 100;
                ((vTextColumn)navList.Columns["UserName"]).MaxTextLength = 60;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["UserName"]).MaxTextLength = 60;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["UserName"]).MaxTextLength = 60;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Name"]).MaxTextLength = 100;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Description"]).MaxTextLength = 1000;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_BrokenRuleText"]).MaxTextLength = 255;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DefaultValue"]).MaxTextLength = 1000;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DefaultCaption"]).MaxTextLength = 100;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ColumnHeaderText"]).MaxTextLength = 100;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_LabelCaptionVerbose"]).MaxTextLength = 250;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_TextBoxFormat"]).MaxTextLength = 50;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_TextColumnFormat"]).MaxTextLength = 50;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_TextBoxTextAlignment"]).MaxTextLength = 10;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ColumnTextAlignment"]).MaxTextLength = 10;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DeveloperNote"]).MaxTextLength = 1000;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UserNote"]).MaxTextLength = 1000;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_HelpFileAdditionalNote"]).MaxTextLength = 1000;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Notes"]).MaxTextLength = 1000;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["UserName"]).MaxTextLength = 60;
                ((vTextColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TooltipsRolledUp"]).MaxTextLength = 2000;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Leave);
            }
        }



        void ConfigureColumnHeaderTooltips()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Enter);

                    // AttachmentCount
                    ((IvColumn)navList.Columns["AttachmentCount"]).HeaderToolTipCaption = StringsWcTableList.AttachmentCount_Vbs; 
                    ((IvColumn)navList.Columns["AttachmentCount"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.AttachmentCount_1};
                    ((IvColumn)navList.Columns["AttachmentCount"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // DiscussionCount
                    ((IvColumn)navList.Columns["DiscussionCount"]).HeaderToolTipCaption = StringsWcTableList.DiscussionCount_Vbs; 
                    ((IvColumn)navList.Columns["DiscussionCount"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.DiscussionCount_1};
                    ((IvColumn)navList.Columns["DiscussionCount"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsComplete
                    ((IvColumn)navList.Columns["Tb_IsComplete"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsComplete_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsComplete"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsComplete_1};
                    ((IvColumn)navList.Columns["Tb_IsComplete"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_EntityRootName
                    ((IvColumn)navList.Columns["Tb_EntityRootName"]).HeaderToolTipCaption = StringsWcTableList.Tb_EntityRootName_Vbs; 
                    ((IvColumn)navList.Columns["Tb_EntityRootName"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_EntityRootName_1};
                    ((IvColumn)navList.Columns["Tb_EntityRootName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_VariableName
                    ((IvColumn)navList.Columns["Tb_VariableName"]).HeaderToolTipCaption = StringsWcTableList.Tb_VariableName_Vbs; 
                    ((IvColumn)navList.Columns["Tb_VariableName"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_VariableName_1, StringsWcTableListTooltips.Tb_VariableName_2, StringsWcTableListTooltips.Tb_VariableName_3};
                    ((IvColumn)navList.Columns["Tb_VariableName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_ScreenCaption
                    ((IvColumn)navList.Columns["Tb_ScreenCaption"]).HeaderToolTipCaption = StringsWcTableList.Tb_ScreenCaption_Vbs; 
                    ((IvColumn)navList.Columns["Tb_ScreenCaption"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_ScreenCaption_1};
                    ((IvColumn)navList.Columns["Tb_ScreenCaption"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_UIAssemblyPath
                    ((IvColumn)navList.Columns["Tb_UIAssemblyPath"]).HeaderToolTipCaption = StringsWcTableList.Tb_UIAssemblyPath_Vbs; 
                    ((IvColumn)navList.Columns["Tb_UIAssemblyPath"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_UIAssemblyPath_1};
                    ((IvColumn)navList.Columns["Tb_UIAssemblyPath"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_WebServiceFolder
                    ((IvColumn)navList.Columns["Tb_WebServiceFolder"]).HeaderToolTipCaption = StringsWcTableList.Tb_WebServiceFolder_Vbs; 
                    ((IvColumn)navList.Columns["Tb_WebServiceFolder"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_WebServiceFolder_1};
                    ((IvColumn)navList.Columns["Tb_WebServiceFolder"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_DataServicePath
                    ((IvColumn)navList.Columns["Tb_DataServicePath"]).HeaderToolTipCaption = StringsWcTableList.Tb_DataServicePath_Vbs; 
                    ((IvColumn)navList.Columns["Tb_DataServicePath"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_DataServicePath_1};
                    ((IvColumn)navList.Columns["Tb_DataServicePath"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_WireTypePath
                    ((IvColumn)navList.Columns["Tb_WireTypePath"]).HeaderToolTipCaption = StringsWcTableList.Tb_WireTypePath_Vbs; 
                    ((IvColumn)navList.Columns["Tb_WireTypePath"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_WireTypePath_1, StringsWcTableListTooltips.Tb_WireTypePath_2};
                    ((IvColumn)navList.Columns["Tb_WireTypePath"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_WireTypeAssemblyName
                    ((IvColumn)navList.Columns["Tb_WireTypeAssemblyName"]).HeaderToolTipCaption = StringsWcTableList.Tb_WireTypeAssemblyName_Vbs; 
                    ((IvColumn)navList.Columns["Tb_WireTypeAssemblyName"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_WireTypeAssemblyName_1};
                    ((IvColumn)navList.Columns["Tb_WireTypeAssemblyName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_UseGridDataSourceCombo
                    ((IvColumn)navList.Columns["Tb_UseGridDataSourceCombo"]).HeaderToolTipCaption = StringsWcTableList.Tb_UseGridDataSourceCombo_Vbs; 
                    ((IvColumn)navList.Columns["Tb_UseGridDataSourceCombo"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_UseGridDataSourceCombo_1};
                    ((IvColumn)navList.Columns["Tb_UseGridDataSourceCombo"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_ApCnSK_Id
                    ((vComboColumnBase)navList.Columns["Tb_ApCnSK_Id"]).HeaderToolTipCaption = StringsWcTableList.Tb_ApCnSK_Id_Vbs; 
                    ((vComboColumnBase)navList.Columns["Tb_ApCnSK_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_ApCnSK_Id_1};
                    ((vComboColumnBase)navList.Columns["Tb_ApCnSK_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_UseLegacyConnectionCode
                    ((IvColumn)navList.Columns["Tb_UseLegacyConnectionCode"]).HeaderToolTipCaption = StringsWcTableList.Tb_UseLegacyConnectionCode_Vbs; 
                    ((IvColumn)navList.Columns["Tb_UseLegacyConnectionCode"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_UseLegacyConnectionCode_1};
                    ((IvColumn)navList.Columns["Tb_UseLegacyConnectionCode"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_PkIsIdentity
                    ((IvColumn)navList.Columns["Tb_PkIsIdentity"]).HeaderToolTipCaption = StringsWcTableList.Tb_PkIsIdentity_Vbs; 
                    ((IvColumn)navList.Columns["Tb_PkIsIdentity"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_PkIsIdentity_1};
                    ((IvColumn)navList.Columns["Tb_PkIsIdentity"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsVirtual
                    ((IvColumn)navList.Columns["Tb_IsVirtual"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsVirtual_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsVirtual"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsVirtual_1, StringsWcTableListTooltips.Tb_IsVirtual_2, StringsWcTableListTooltips.Tb_IsVirtual_3};
                    ((IvColumn)navList.Columns["Tb_IsVirtual"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsNotEntity
                    ((IvColumn)navList.Columns["Tb_IsNotEntity"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsNotEntity_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsNotEntity"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsNotEntity_1, StringsWcTableListTooltips.Tb_IsNotEntity_2};
                    ((IvColumn)navList.Columns["Tb_IsNotEntity"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_UseLastModifiedByUserNameInSproc
                    ((IvColumn)navList.Columns["Tb_UseLastModifiedByUserNameInSproc"]).HeaderToolTipCaption = StringsWcTableList.Tb_UseLastModifiedByUserNameInSproc_Vbs; 
                    ((IvColumn)navList.Columns["Tb_UseLastModifiedByUserNameInSproc"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_UseLastModifiedByUserNameInSproc_1};
                    ((IvColumn)navList.Columns["Tb_UseLastModifiedByUserNameInSproc"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_UseUserTimeStamp
                    ((IvColumn)navList.Columns["Tb_UseUserTimeStamp"]).HeaderToolTipCaption = StringsWcTableList.Tb_UseUserTimeStamp_Vbs; 
                    ((IvColumn)navList.Columns["Tb_UseUserTimeStamp"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_UseUserTimeStamp_1};
                    ((IvColumn)navList.Columns["Tb_UseUserTimeStamp"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_UseForAudit
                    ((IvColumn)navList.Columns["Tb_UseForAudit"]).HeaderToolTipCaption = StringsWcTableList.Tb_UseForAudit_Vbs; 
                    ((IvColumn)navList.Columns["Tb_UseForAudit"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_UseForAudit_1};
                    ((IvColumn)navList.Columns["Tb_UseForAudit"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsCodeGen
                    ((IvColumn)navList.Columns["Tb_IsCodeGen"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsCodeGen_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsCodeGen"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsCodeGen_1};
                    ((IvColumn)navList.Columns["Tb_IsCodeGen"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsCodeGenComplete
                    ((IvColumn)navList.Columns["Tb_IsCodeGenComplete"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsCodeGenComplete_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsCodeGenComplete"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsCodeGenComplete_1, StringsWcTableListTooltips.Tb_IsCodeGenComplete_2};
                    ((IvColumn)navList.Columns["Tb_IsCodeGenComplete"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_IsActiveRow
                    ((IvColumn)navList.Columns["Tb_IsActiveRow"]).HeaderToolTipCaption = StringsWcTableList.Tb_IsActiveRow_Vbs; 
                    ((IvColumn)navList.Columns["Tb_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_IsActiveRow_1, StringsWcTableListTooltips.Tb_IsActiveRow_2};
                    ((IvColumn)navList.Columns["Tb_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)navList.Columns["UserName"]).HeaderToolTipCaption = StringsWcTableList.UserName_Vbs; 
                    ((IvColumn)navList.Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.UserName_1};
                    ((IvColumn)navList.Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tb_LastModifiedDate
                    ((IvColumn)navList.Columns["Tb_LastModifiedDate"]).HeaderToolTipCaption = StringsWcTableList.Tb_LastModifiedDate_Vbs; 
                    ((IvColumn)navList.Columns["Tb_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsWcTableListTooltips.Tb_LastModifiedDate_1};
                    ((IvColumn)navList.Columns["Tb_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_Child_Tb_Id
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_Child_Tb_Id"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_Child_Tb_Id_Vbs; 
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_Child_Tb_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_Child_Tb_Id_1};
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_Child_Tb_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_ChildForeignKeyColumn_Id
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn_Id"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_ChildForeignKeyColumn_Id_Vbs; 
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_ChildForeignKeyColumn_Id_1};
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_ChildForeignKeyColumn22_Id
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn22_Id"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_ChildForeignKeyColumn22_Id_Vbs; 
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn22_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_ChildForeignKeyColumn22_Id_1, StringsWcTableChildListTooltips.TbChd_ChildForeignKeyColumn22_Id_2, StringsWcTableChildListTooltips.TbChd_ChildForeignKeyColumn22_Id_3};
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn22_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_ChildForeignKeyColumn33_Id
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn33_Id"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_ChildForeignKeyColumn33_Id_Vbs; 
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn33_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_ChildForeignKeyColumn33_Id_1, StringsWcTableChildListTooltips.TbChd_ChildForeignKeyColumn33_Id_2};
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn33_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_DifferentCombo_Id
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_DifferentCombo_Id"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_DifferentCombo_Id_Vbs; 
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_DifferentCombo_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_DifferentCombo_Id_1, StringsWcTableChildListTooltips.TbChd_DifferentCombo_Id_2};
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_DifferentCombo_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_IsDisplayAsChildGrid
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_IsDisplayAsChildGrid"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_IsDisplayAsChildGrid_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_IsDisplayAsChildGrid"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_IsDisplayAsChildGrid_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_IsDisplayAsChildGrid"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_IsDisplayAsChildTab
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_IsDisplayAsChildTab"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_IsDisplayAsChildTab_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_IsDisplayAsChildTab"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_IsDisplayAsChildTab_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_IsDisplayAsChildTab"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_IsActiveRow
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_IsActiveRow"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_IsActiveRow_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_IsActiveRow_1, StringsWcTableChildListTooltips.TbChd_IsActiveRow_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["UserName"]).HeaderToolTipCaption = StringsWcTableChildList.UserName_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.UserName_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbChd_LastModifiedDate
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_LastModifiedDate"]).HeaderToolTipCaption = StringsWcTableChildList.TbChd_LastModifiedDate_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsWcTableChildListTooltips.TbChd_LastModifiedDate_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbSb_IsActiveRow
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["TbSb_IsActiveRow"]).HeaderToolTipCaption = StringsWcTableSortByList.TbSb_IsActiveRow_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["TbSb_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcTableSortByListTooltips.TbSb_IsActiveRow_1, StringsWcTableSortByListTooltips.TbSb_IsActiveRow_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["TbSb_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["UserName"]).HeaderToolTipCaption = StringsWcTableSortByList.UserName_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsWcTableSortByListTooltips.UserName_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbSb_LastModifiedDate
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["TbSb_LastModifiedDate"]).HeaderToolTipCaption = StringsWcTableSortByList.TbSb_LastModifiedDate_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["TbSb_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsWcTableSortByListTooltips.TbSb_LastModifiedDate_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["TbSb_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsComplete
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsComplete"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsComplete_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsComplete"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsComplete_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsComplete"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_CtlTp_Id
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_CtlTp_Id"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_CtlTp_Id_Vbs; 
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_CtlTp_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsNotvColumn
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsNotvColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsNotvColumn_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsNotvColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsNotvColumn_1, StringsWcTableColumnListTooltips.TbC_IsNotvColumn_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsNotvColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_DtSql_Id
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DtSql_Id"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_DtSql_Id_Vbs; 
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DtSql_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_DtDtNt_Id
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DtDtNt_Id"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_DtDtNt_Id_Vbs; 
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DtDtNt_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Length
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Length"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Length_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Length"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Precision
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Precision"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Precision_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Precision"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Precision_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Precision"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Scale
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Scale"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Scale_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Scale"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Scale_1, StringsWcTableColumnListTooltips.TbC_Scale_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Scale"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsPK
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsPK"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsPK_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsPK"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsIdentity
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsIdentity"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsIdentity_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsIdentity"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsIdentity_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsIdentity"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsFK
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsFK"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsFK_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsFK"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsEntityColumn
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsEntityColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsEntityColumn_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsEntityColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsEntityColumn_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsEntityColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsSystemField
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsSystemField"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsSystemField_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsSystemField"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsSystemField_1, StringsWcTableColumnListTooltips.TbC_IsSystemField_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsSystemField"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsValuesObjectMember
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsValuesObjectMember"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsValuesObjectMember_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsValuesObjectMember"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsValuesObjectMember_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsValuesObjectMember"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsInPropsScreen
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsInPropsScreen"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsInPropsScreen_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsInPropsScreen"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsInPropsScreen_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsInPropsScreen"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsInNavList
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsInNavList"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsInNavList_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsInNavList"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsRequired
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsRequired"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsRequired_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsRequired"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_AllowZero
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_AllowZero"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_AllowZero_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_AllowZero"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsNullableInDb
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsNullableInDb"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsNullableInDb_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsNullableInDb"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsNullableInUI
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsNullableInUI"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsNullableInUI_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsNullableInUI"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_DefaultValue
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DefaultValue"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_DefaultValue_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DefaultValue"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsReadFromDb
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsReadFromDb"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsReadFromDb_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsReadFromDb"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsReadFromDb_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsReadFromDb"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsSendToDb
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsSendToDb"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsSendToDb_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsSendToDb"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsSendToDb_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsSendToDb"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsInsertAllowed
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsInsertAllowed"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsInsertAllowed_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsInsertAllowed"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsInsertAllowed_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsInsertAllowed"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsEditAllowed
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsEditAllowed"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsEditAllowed_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsEditAllowed"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsEditAllowed_1, StringsWcTableColumnListTooltips.TbC_IsEditAllowed_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsEditAllowed"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsReadOnlyInUI
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsReadOnlyInUI"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsReadOnlyInUI_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsReadOnlyInUI"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsReadOnlyInUI_1, StringsWcTableColumnListTooltips.TbC_IsReadOnlyInUI_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsReadOnlyInUI"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_UseForAudit
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UseForAudit"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_UseForAudit_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UseForAudit"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_UseForAudit_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UseForAudit"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_DefaultCaption
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DefaultCaption"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_DefaultCaption_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DefaultCaption"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_DefaultCaption_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DefaultCaption"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ColumnHeaderText
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ColumnHeaderText"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ColumnHeaderText_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ColumnHeaderText"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_ColumnHeaderText_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ColumnHeaderText"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_LabelCaptionVerbose
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_LabelCaptionVerbose"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_LabelCaptionVerbose_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_LabelCaptionVerbose"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_LabelCaptionVerbose_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_LabelCaptionVerbose"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_LabelCaptionGenerate
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_LabelCaptionGenerate"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_LabelCaptionGenerate_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_LabelCaptionGenerate"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_LabelCaptionGenerate_1, StringsWcTableColumnListTooltips.TbC_LabelCaptionGenerate_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_LabelCaptionGenerate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tbc_ShowGridColumnToolTip
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["Tbc_ShowGridColumnToolTip"]).HeaderToolTipCaption = StringsWcTableColumnList.Tbc_ShowGridColumnToolTip_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["Tbc_ShowGridColumnToolTip"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.Tbc_ShowGridColumnToolTip_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["Tbc_ShowGridColumnToolTip"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tbc_ShowPropsToolTip
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["Tbc_ShowPropsToolTip"]).HeaderToolTipCaption = StringsWcTableColumnList.Tbc_ShowPropsToolTip_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["Tbc_ShowPropsToolTip"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TooltipsRolledUp
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TooltipsRolledUp"]).HeaderToolTipCaption = StringsWcTableColumnList.TooltipsRolledUp_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TooltipsRolledUp"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TooltipsRolledUp_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TooltipsRolledUp"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsCreatePropsStrings
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCreatePropsStrings"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsCreatePropsStrings_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCreatePropsStrings"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsCreatePropsStrings_1, StringsWcTableColumnListTooltips.TbC_IsCreatePropsStrings_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCreatePropsStrings"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsCreateGridStrings
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCreateGridStrings"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsCreateGridStrings_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCreateGridStrings"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsCreateGridStrings_1, StringsWcTableColumnListTooltips.TbC_IsCreateGridStrings_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCreateGridStrings"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsAvailableForColumnGroups
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsAvailableForColumnGroups"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsAvailableForColumnGroups_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsAvailableForColumnGroups"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsAvailableForColumnGroups_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsAvailableForColumnGroups"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsTextWrapInProp
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsTextWrapInProp"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsTextWrapInProp_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsTextWrapInProp"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_TextColumnFormat
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_TextColumnFormat"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_TextColumnFormat_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_TextColumnFormat"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ColumnWidth
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ColumnWidth"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ColumnWidth_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ColumnWidth"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_ColumnWidth_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ColumnWidth"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_TextBoxTextAlignment
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_TextBoxTextAlignment"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_TextBoxTextAlignment_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_TextBoxTextAlignment"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ColumnTextAlignment
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ColumnTextAlignment"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ColumnTextAlignment_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ColumnTextAlignment"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ListStoredProc_Id
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ListStoredProc_Id"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ListStoredProc_Id_Vbs; 
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ListStoredProc_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_ListStoredProc_Id_1};
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ListStoredProc_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsStaticList
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsStaticList"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsStaticList_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsStaticList"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsStaticList_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsStaticList"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_UseNotInList
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UseNotInList"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_UseNotInList_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UseNotInList"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_UseNotInList_1, StringsWcTableColumnListTooltips.TbC_UseNotInList_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UseNotInList"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_UseListEditBtn
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UseListEditBtn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_UseListEditBtn_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UseListEditBtn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_UseListEditBtn_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UseListEditBtn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ParentColumnKey
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ParentColumnKey"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ParentColumnKey_Vbs; 
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ParentColumnKey"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_ParentColumnKey_1, StringsWcTableColumnListTooltips.TbC_ParentColumnKey_2, StringsWcTableColumnListTooltips.TbC_ParentColumnKey_3};
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ParentColumnKey"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_UseDisplayTextFieldProperty
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UseDisplayTextFieldProperty"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_UseDisplayTextFieldProperty_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UseDisplayTextFieldProperty"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_UseDisplayTextFieldProperty_1, StringsWcTableColumnListTooltips.TbC_UseDisplayTextFieldProperty_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UseDisplayTextFieldProperty"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsDisplayTextFieldProperty
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsDisplayTextFieldProperty"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsDisplayTextFieldProperty_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsDisplayTextFieldProperty"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsDisplayTextFieldProperty_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsDisplayTextFieldProperty"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ComboListTable_Id
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ComboListTable_Id"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ComboListTable_Id_Vbs; 
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ComboListTable_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_ComboListTable_Id_1, StringsWcTableColumnListTooltips.TbC_ComboListTable_Id_2, StringsWcTableColumnListTooltips.TbC_ComboListTable_Id_3};
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ComboListTable_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_ComboListDisplayColumn_Id
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ComboListDisplayColumn_Id"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_ComboListDisplayColumn_Id_Vbs; 
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ComboListDisplayColumn_Id"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_ComboListDisplayColumn_Id_1, StringsWcTableColumnListTooltips.TbC_ComboListDisplayColumn_Id_2};
                    ((vComboColumnBase)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ComboListDisplayColumn_Id"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Combo_MaxDropdownHeight
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Combo_MaxDropdownHeight"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Combo_MaxDropdownHeight_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Combo_MaxDropdownHeight"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Combo_MaxDropdownHeight_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Combo_MaxDropdownHeight"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Combo_MaxDropdownWidth
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Combo_MaxDropdownWidth"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Combo_MaxDropdownWidth_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Combo_MaxDropdownWidth"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Combo_MaxDropdownWidth_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Combo_MaxDropdownWidth"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Combo_AllowDropdownResizing
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Combo_AllowDropdownResizing"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Combo_AllowDropdownResizing_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Combo_AllowDropdownResizing"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Combo_AllowDropdownResizing_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Combo_AllowDropdownResizing"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Combo_IsResetButtonVisible
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Combo_IsResetButtonVisible"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Combo_IsResetButtonVisible_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Combo_IsResetButtonVisible"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Combo_IsResetButtonVisible_1, StringsWcTableColumnListTooltips.TbC_Combo_IsResetButtonVisible_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Combo_IsResetButtonVisible"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsActiveRecColumn
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsActiveRecColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsActiveRecColumn_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsActiveRecColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsActiveRecColumn_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsActiveRecColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsDeletedColumn
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsDeletedColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsDeletedColumn_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsDeletedColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsDeletedColumn_1, StringsWcTableColumnListTooltips.TbC_IsDeletedColumn_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsDeletedColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsCreatedUserIdColumn
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCreatedUserIdColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsCreatedUserIdColumn_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCreatedUserIdColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsCreatedUserIdColumn_1, StringsWcTableColumnListTooltips.TbC_IsCreatedUserIdColumn_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCreatedUserIdColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsCreatedDateColumn
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCreatedDateColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsCreatedDateColumn_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCreatedDateColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsCreatedDateColumn_1, StringsWcTableColumnListTooltips.TbC_IsCreatedDateColumn_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCreatedDateColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsUserIdColumn
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsUserIdColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsUserIdColumn_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsUserIdColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsUserIdColumn_1, StringsWcTableColumnListTooltips.TbC_IsUserIdColumn_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsUserIdColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsModifiedDateColumn
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsModifiedDateColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsModifiedDateColumn_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsModifiedDateColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsModifiedDateColumn_1, StringsWcTableColumnListTooltips.TbC_IsModifiedDateColumn_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsModifiedDateColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsRowVersionStampColumn
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsRowVersionStampColumn"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsRowVersionStampColumn_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsRowVersionStampColumn"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsRowVersionStampColumn_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsRowVersionStampColumn"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsBrowsable
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsBrowsable"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsBrowsable_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsBrowsable"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsBrowsable_1, StringsWcTableColumnListTooltips.TbC_IsBrowsable_2, StringsWcTableColumnListTooltips.TbC_IsBrowsable_3};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsBrowsable"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_DeveloperNote
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DeveloperNote"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_DeveloperNote_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DeveloperNote"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_DeveloperNote_1, StringsWcTableColumnListTooltips.TbC_DeveloperNote_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DeveloperNote"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_UserNote
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UserNote"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_UserNote_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UserNote"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_UserNote_1, StringsWcTableColumnListTooltips.TbC_UserNote_2, StringsWcTableColumnListTooltips.TbC_UserNote_3};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_UserNote"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_HelpFileAdditionalNote
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_HelpFileAdditionalNote"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_HelpFileAdditionalNote_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_HelpFileAdditionalNote"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_HelpFileAdditionalNote_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_HelpFileAdditionalNote"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_Notes
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Notes"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_Notes_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Notes"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_Notes_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_Notes"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsCodeGen
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCodeGen"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsCodeGen_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCodeGen"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsCodeGen_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsCodeGen"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_IsActiveRow
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsActiveRow"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_IsActiveRow_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_IsActiveRow_1, StringsWcTableColumnListTooltips.TbC_IsActiveRow_2};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["UserName"]).HeaderToolTipCaption = StringsWcTableColumnList.UserName_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.UserName_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // TbC_LastModifiedDate
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_LastModifiedDate"]).HeaderToolTipCaption = StringsWcTableColumnList.TbC_LastModifiedDate_Vbs; 
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsWcTableColumnListTooltips.TbC_LastModifiedDate_1};
                    ((IvColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();
                ConfigureColumnHeaderTooltips_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Leave);
            }
        }


        void Set_vXamComboColumn_ItemSources()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["Tb_ApCnSK_Id"]).ItemsSource = WcApplication_Bll_staticLists.WcApplicationConnectionStringKey_ComboItemList_BindingListProperty;
                ((vXamComboColumn)navList.Columns["Tb_ApCnSK_Id"]).DisplayMemberPath = "ItemName";
                WcApplication_Bll_staticLists.WcApplicationConnectionStringKey_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcApplicationConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_Child_Tb_Id"]).ItemsSource = WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_Child_Tb_Id"]).DisplayMemberPath = "ItemName";
                WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTable_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn_Id"]).ItemsSource = new ComboItemList(WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty, ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn_Id"]).DisplayMemberPath = "ItemName";
                WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn22_Id"]).ItemsSource = new ComboItemList(WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty, ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn22_Id"]).DisplayMemberPath = "ItemName";
                WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_DifferentCombo_Id"]).ItemsSource = WcApplication_Bll_staticLists.zzzTestForCombo_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_DifferentCombo_Id"]).DisplayMemberPath = "ItemName";
                WcApplication_Bll_staticLists.zzzTestForCombo_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(zzzTestForCombo_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn33_Id"]).ItemsSource = new ComboItemList(WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty, ComboItemList.DataType.GuidType, ComboItemList.DataType.GuidType);
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn33_Id"]).DisplayMemberPath = "ItemName";
                WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["TbSb_TbC_Id"]).ItemsSource = WcTable_Bll_staticLists.WcTableColumn_lstByTable_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["TbSb_TbC_Id"]).DisplayMemberPath = "ItemName";
                WcTable_Bll_staticLists.WcTableColumn_lstByTable_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByTable_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["TbSb_Direction"]).ItemsSource = CommonClientData_Bll_staticLists.WcTableSortByDirection_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["TbSb_Direction"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcTableSortByDirection_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableSortByDirection_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DtSql_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DtSql_Id"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DtDtNt_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DtDtNt_Id"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_CtlTp_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcControlType_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_CtlTp_Id"]).DisplayMemberPath = "ItemName";
                CommonClientData_Bll_staticLists.WcControlType_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ListStoredProc_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcStoredProc_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ListStoredProc_Id"]).DisplayMemberPath = "ItemName";
                WcApplicationVersion_Bll_staticLists.WcStoredProc_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ParentColumnKey"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ParentColumnKey"]).DisplayMemberPath = "ItemName";
                WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ComboListTable_Id"]).ItemsSource = WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ComboListTable_Id"]).DisplayMemberPath = "ItemName";
                WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTable_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ComboListDisplayColumn_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ComboListDisplayColumn_Id"]).DisplayMemberPath = "ItemName";
                WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Leave);
            }
        }

        void WcApplicationConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)navList.Columns["Tb_ApCnSK_Id"]).ItemsSource = WcApplication_Bll_staticLists.WcApplicationConnectionStringKey_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcTable_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_Child_Tb_Id"]).ItemsSource = WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ComboListTable_Id"]).ItemsSource = WcTable_Bll_staticLists.WcTable_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn22_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_ChildForeignKeyColumn33_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ComboListDisplayColumn_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;
                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ParentColumnKey"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void zzzTestForCombo_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "zzzTestForCombo_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableChild"]).Columns["TbChd_DifferentCombo_Id"]).ItemsSource = WcApplication_Bll_staticLists.zzzTestForCombo_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "zzzTestForCombo_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "zzzTestForCombo_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_CtlTp_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcControlType_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcControlType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DtDtNt_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeDotNet_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeDotNet_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_DtSql_Id"]).ItemsSource = CommonClientData_Bll_staticLists.WcDataTypeSQL_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcDataTypeSQL_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableColumn"]).Columns["TbC_ListStoredProc_Id"]).ItemsSource = WcApplicationVersion_Bll_staticLists.WcStoredProc_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcTableSortByDirection_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortByDirection_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["TbSb_Direction"]).ItemsSource = CommonClientData_Bll_staticLists.WcTableSortByDirection_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortByDirection_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortByDirection_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void WcTableColumn_lstByTable_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)((ColumnLayout)navList.Columns["Children_WcTableSortBy"]).Columns["TbSb_TbC_Id"]).ItemsSource = WcTable_Bll_staticLists.WcTableColumn_lstByTable_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByTable_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_lstByTable_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        #region Combo ItemsSource for Non-Static Lists

        #endregion region Combo ItemsSource for Non-Static Lists

        void SetNewRowValidation()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Enter);
                if (_activeRow == null) { return; }
                WcTable_Bll obj = _activeRow.Data as WcTable_Bll;
                if (obj == null) { return; }
                // Note:  The lines to set the validaton styles below are not needed as the styles are being set from converters in the xaml style propeties, but they are left here incase needed in the future.  this is new code and we are unsure how it will evolve.
                WcTableChild_Bll oWcTableChild = _activeRow.Data as WcTableChild_Bll;
                if (oWcTableChild == null) { return; }
                WcTableSortBy_Bll oWcTableSortBy = _activeRow.Data as WcTableSortBy_Bll;
                if (oWcTableSortBy == null) { return; }
                WcTableColumn_Bll oWcTableColumn = _activeRow.Data as WcTableColumn_Bll;
                if (oWcTableColumn == null) { return; }
                // "TbC_ColumnWidth"
                if (_activeRow.Cells["TbC_ColumnWidth"].Control != null && _activeRow.Cells["TbC_ColumnWidth"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["TbC_ColumnWidth"].Control.Content), oWcTableColumn, "TbC_ColumnWidth");
                }

                // "TbC_Combo_MaxDropdownHeight"
                if (_activeRow.Cells["TbC_Combo_MaxDropdownHeight"].Control != null && _activeRow.Cells["TbC_Combo_MaxDropdownHeight"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["TbC_Combo_MaxDropdownHeight"].Control.Content), oWcTableColumn, "TbC_Combo_MaxDropdownHeight");
                }

                // "TbC_Combo_MaxDropdownWidth"
                if (_activeRow.Cells["TbC_Combo_MaxDropdownWidth"].Control != null && _activeRow.Cells["TbC_Combo_MaxDropdownWidth"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["TbC_Combo_MaxDropdownWidth"].Control.Content), oWcTableColumn, "TbC_Combo_MaxDropdownWidth");
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Leave);
            }
        }

        #endregion Methods


    }
}
