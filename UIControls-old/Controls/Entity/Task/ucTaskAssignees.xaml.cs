﻿using ApplicationTypeServices;
using EntityWireTypeSL;
using Ifx.SL;
using Infragistics.Controls.Grids;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using vControls;
using Velocity.SL;
using vUICommon;

namespace UIControls
{
    public partial class ucTaskAssignees : UserControl
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "ucTaskAssignees";



        System.Windows.GridLength _navColumnWidth;

        ProxyWrapper.Task2PersonService_ProxyWrapper _proxy = null;

        public event TaskAssigneesUpdatedEventHandler TaskAssigneesUpdated;


        #endregion Initialize Variables




        public ucTaskAssignees()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTaskAssignees", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();

                _proxy = new ProxyWrapper.Task2PersonService_ProxyWrapper();
                _proxy.GetPerson_lstNotAssignedToTaskCompleted += new EventHandler<GetPerson_lstNotAssignedToTaskCompletedEventArgs>(GetPerson_lstNotAssignedToTaskCompleted);
                _proxy.GetPerson_lstAssignedToTaskCompleted += new EventHandler<GetPerson_lstAssignedToTaskCompletedEventArgs>(GetPerson_lstAssignedToTaskCompleted);
                _proxy.ExecuteTask2Person_assignPersonCompleted += new EventHandler<ExecuteTask2Person_assignPersonCompletedEventArgs>(ExecuteTask2Person_assignPersonCompleted);
                _proxy.ExecuteTask2Person_removePersonCompleted += new EventHandler<ExecuteTask2Person_removePersonCompletedEventArgs>(ExecuteTask2Person_removePersonCompleted);



            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTaskAssignees", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucTaskAssignees", IfxTraceCategory.Leave);
            }
        }

        bool _isReadOnly = false;
        public void SetReadOnlyMode(bool isReadOnly)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetReadOnlyMode", IfxTraceCategory.Enter);
                _isReadOnly = isReadOnly;
                if (isReadOnly == true)
                {
                    xgdAval.EditingSettings.AllowEditing = EditingType.None;
                    xgdAssigned.EditingSettings.AllowEditing = EditingType.None;
                }
                else
                {
                    xgdAval.EditingSettings.AllowEditing = EditingType.Row;
                    xgdAssigned.EditingSettings.AllowEditing = EditingType.Row;
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetReadOnlyMode", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetReadOnlyMode", IfxTraceCategory.Leave);
            }
        }

        Guid? _tk_Id;

        public void LoadLists(Guid id_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadLists", IfxTraceCategory.Enter);

                //***  Need Idea ID
                _tk_Id = id_Id;
                //ucIdeaPersons.GuidParentId = _tk_Id;
                _proxy.Begin_GetPerson_lstNotAssignedToTask((Guid)_tk_Id, (Guid)ContextValues.CurrentProjectId);
                _proxy.Begin_GetPerson_lstAssignedToTask((Guid)_tk_Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadLists", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadLists", IfxTraceCategory.Leave);
            }
        }



        public void ClearList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearList", IfxTraceCategory.Enter);

                xgdAval.ItemsSource = null;
                xgdAssigned.ItemsSource = null;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearList", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ClearList", IfxTraceCategory.Leave);
            }
        }





        void RaiseEventTaskAssigneesUpdated()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventTaskAssigneesUpdated", IfxTraceCategory.Enter);

            if (_tk_Id == null) { return; }
            TaskAssigneesUpdatedEventHandler handler = TaskAssigneesUpdated;

            if (handler != null)
            {
                handler(this, new TaskAssigneesUpdatedArgs((Guid)_tk_Id, null));
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventTaskAssigneesUpdated", IfxTraceCategory.Leave);
        }





        #region xgdAval

        //private void btnAddPerson_Click(object sender, RoutedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAddPerson_Click", IfxTraceCategory.Enter);

        //        if (_isReadOnly == true)
        //        {
        //            return;
        //        }

        //        Person_lstAssignedToTask_Binding data = ((CellControl)((System.Windows.Controls.Button)sender).Parent).Cell.Row.Data as Person_lstAssignedToTask_Binding;
        //        if (data == null) { return; }
        //        if (data.Pn_FName == null && data.Pn_LName == null) { return; }

        //        _proxy.Begin_ExecuteTask2Person_assignPerson((Guid)_tk_Id, data.Pn_Id, (Guid)Credentials.UserId);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAddPerson_Click", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAddPerson_Click", IfxTraceCategory.Leave);
        //    }
        //}        


        private void btnAddPerson_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAddPerson_Click", IfxTraceCategory.Enter);

                Person_lstAssignedToTask_Binding data = e.Data as Person_lstAssignedToTask_Binding;
                if (data == null) { return; }
                if (data.Pn_FName == null && data.Pn_LName == null) { return; }

                _proxy.Begin_ExecuteTask2Person_assignPerson((Guid)_tk_Id, data.Pn_Id, (Guid)Credentials.UserId);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAddPerson_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAddPerson_Click", IfxTraceCategory.Leave);
            }
        }


        #endregion xgdAval
        

        #region xgdAssigned

        private void btnRemovePerson_Click(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRemovePerson_Click", IfxTraceCategory.Enter);

                if (_isReadOnly == true)
                {
                    return;
                }

                Person_lstAssignedToTask_Binding data = e.Data as Person_lstAssignedToTask_Binding;
                //Person_lstAssignedToTask_Binding data = ((CellControl)((System.Windows.Controls.Button)sender).Parent).Cell.Row.Data as Person_lstAssignedToTask_Binding;
                if (data == null) { return; }
                //if (data.Pn_FName == null && data.Pn_LName == null) { return; }
                _proxy.Begin_ExecuteTask2Person_removePerson((Guid)_tk_Id, (Guid)data.Pn_Id);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRemovePerson_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRemovePerson_Click", IfxTraceCategory.Leave);
            }
        }

        #endregion xgdAssigned









        #region Fetch Data






        void ExecuteTask2Person_removePersonCompleted(object sender, ExecuteTask2Person_removePersonCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_removePersonCompleted", IfxTraceCategory.Enter);
                int? success = null;
                object[] data = e.Result;
                if (data == null)
                {
                    MessageBox.Show("There was problem making this removal at the server." + Environment.NewLine + "If this continues, please contact suport.", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    success = data[0] as int?;
                    if (success == 1)
                    {
                        LoadLists((Guid)_tk_Id);
                        RaiseEventTaskAssigneesUpdated();
                    }
                    else
                    {
                        MessageBox.Show("There was problem making this removal at the server." + Environment.NewLine + "If this continues, please contact suport.", "Error", MessageBoxButton.OK);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_removePersonCompleted", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_removePersonCompleted", IfxTraceCategory.Leave);
            }
        }

        void ExecuteTask2Person_assignPersonCompleted(object sender, ExecuteTask2Person_assignPersonCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_assignPersonCompleted", IfxTraceCategory.Enter);
                int? success = null;
                object[] data = e.Result;
                if (data == null)
                {
                    MessageBox.Show("There was problem making this assignment at the server." + Environment.NewLine + "If this continues, please contact suport.", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    success = data[0] as int?;
                    if (success == 1)
                    {
                        LoadLists((Guid)_tk_Id);
                        RaiseEventTaskAssigneesUpdated();
                    }
                    else
                    {
                        MessageBox.Show("There was problem making this assignment at the server." + Environment.NewLine + "If this continues, please contact suport.", "Error", MessageBoxButton.OK);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_assignPersonCompleted", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_assignPersonCompleted", IfxTraceCategory.Leave);
            }
        }

        void GetPerson_lstAssignedToTaskCompleted(object sender, GetPerson_lstAssignedToTaskCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstAssignedToTaskCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                xgdAssigned.ItemsSource = null;
                xgdAssigned.ItemsSource = GetList(array);
                
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstAssignedToTaskCompleted", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstAssignedToTaskCompleted", IfxTraceCategory.Leave);
            }
        }

        //List<Person_lstAssignedToTask_Binding> _assigned = new List<Person_lstAssignedToTask_Binding>();
        //List<Person_lstAssignedToTask_Binding> _notAssigned = new List<Person_lstAssignedToTask_Binding>();

        void GetPerson_lstNotAssignedToTaskCompleted(object sender, GetPerson_lstNotAssignedToTaskCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToTaskCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                xgdAval.ItemsSource = null;
                xgdAval.ItemsSource = GetList(array);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToTaskCompleted", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToTaskCompleted", IfxTraceCategory.Leave);
            }
        }

        List<Person_lstAssignedToTask_Binding> GetList(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetList", IfxTraceCategory.Enter);
                List<Person_lstAssignedToTask_Binding> list = new List<Person_lstAssignedToTask_Binding>();
                if (data == null) { return list; }
                for (int i = 0; i < data.GetUpperBound(0) + 1; i++)
                {
                    list.Add(new Person_lstAssignedToTask_Binding((object[])data[i]));
                }

                return list;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetList", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetList", IfxTraceCategory.Leave);
            }
        }


        #endregion Fetch Data









    }
}
