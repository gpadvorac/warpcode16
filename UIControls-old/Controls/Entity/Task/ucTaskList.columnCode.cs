using System;
using System.Windows;
using System.Windows.Controls;
using Ifx.SL;
using TypeServices;
using vControls;
using EntityBll.SL;
using Infragistics.Controls.Editors;
using vUICommon;
using EntityWireTypeSL;
using Infragistics.Controls.Grids;
using vComboDataTypes;
using System.Collections.Generic;
using vUICommon.Controls;
using UIControls.Globalization.Task;

namespace UIControls
{
    public partial class ucTaskList
    {

        
        #region GridCell Editor Events
        

        private void XamGridTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                if ((ctl.Text.Length>0) && (ctl.Text.Substring(ctl.Text.Length - 1, 1) == " "))
                {
                    return;
                }
                Task_Bll obj = navList.ActiveCell.Row.Data as Task_Bll;
                if (obj == null) { return; }
                //switch (ctl.Name)
                //{
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridTextBox_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vTextColumn_TextChanged(object sender, vTextColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                Task_Bll obj = navList.ActiveCell.Row.Data as Task_Bll;
                if (obj == null) { return; }
                int pos = ctl.SelectionStart;
                switch (e.Key)
                {
                    case "Tk_Name":
                        obj.Tk_Name = ctl.Text;
                        break;
                    case "Tk_Desc":
                        obj.Tk_Desc = ctl.Text;
                        break;
                    case "Tk_Results":
                        obj.Tk_Results = ctl.Text;
                        break;
                    case "Tk_Remarks":
                        obj.Tk_Remarks = ctl.Text;
                        break;
                    default:
                        return;
                }
                ctl.SelectionStart = pos;
                vTextColumn_TextChanged_CustomCode(ctl, obj, e.Key);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vTextColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vDecimalColumn_TextChanged(object sender, vDecimalColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                TextBox ctl = sender as TextBox;
                if (ctl == null) { return; }
                Task_Bll obj = navList.ActiveCell.Row.Data as Task_Bll;
                if (obj == null) { return; }
                int pos = ctl.SelectionStart;
                switch (e.Key)
                {
                    case "Tk_EstimatedTime":
                        obj.Tk_EstimatedTime_asString = ctl.Text;
                        break;
                    case "Tk_PercentComplete":
                        obj.Tk_PercentComplete_asString = ctl.Text;
                        break;
                    default:
                        return;
                }
                ctl.SelectionStart = pos;
                vDecimalColumn_TextChanged_CustomCode(ctl, obj, e.Key);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDecimalColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamComboEditor comboEditor = sender as XamComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_SelectionChanged(object sender, EventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                Task_Bll obj = navList.ActiveCell.Row.Data as Task_Bll;
                if (obj == null) { return; }
                switch (ctl.Name)
                {
                    case "Tk_TkTp_Id":
                        if (ctl.SelectedItem == null)
                        {
                            obj.Tk_TkTp_Id = null;
                        }
                        else
                        {
                            obj.Tk_TkTp_Id = ((IWireTypeBinding)ctl.SelectedItem).Get_Int_Id();
                        }
                        break;
                    case "Tk_TkCt_Id":
                        if (ctl.SelectedItem == null)
                        {
                            obj.Tk_TkCt_Id = null;
                        }
                        else
                        {
                            obj.Tk_TkCt_Id = ((IWireTypeBinding)ctl.SelectedItem).Get_Guid_Id();
                        }
                        break;
                    case "Tk_TkSt_Id":
                        if (ctl.SelectedItem == null)
                        {
                            obj.Tk_TkSt_Id = null;
                        }
                        else
                        {
                            obj.Tk_TkSt_Id = ((IWireTypeBinding)ctl.SelectedItem).Get_Int_Id();
                        }
                        break;
                    case "Tk_TkPr_Id":
                        if (ctl.SelectedItem == null)
                        {
                            obj.Tk_TkPr_Id = null;
                        }
                        else
                        {
                            obj.Tk_TkPr_Id = ((IWireTypeBinding)ctl.SelectedItem).Get_Int_Id();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_SelectionChanged(object sender, vXamComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamComboEditor ctl = sender as XamComboEditor;
                if (ctl == null) { return; }
                Task_Bll obj = navList.ActiveCell.Row.Data as Task_Bll;
                if (obj == null) { return; }
                // The next 2 lines are for the Not In List feature.  If the 'Non-In-List' event is fireing, we dont want to run this event right now.
                if (ctl.SelectedItem == null) { return; }
                if (((ComboItem)ctl.SelectedItem).Id == null) { return; }

                switch (e.Key)
                {
                    case "Tk_TkTp_Id":
                        if (ctl.SelectedItem == null)
                        {
                            obj.Tk_TkTp_Id = null;
                        }
                        else
                        {
                            obj.Tk_TkTp_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                        }
                        break;
                    case "Tk_TkCt_Id":
                        if (ctl.SelectedItem == null)
                        {
                            obj.Tk_TkCt_Id = null;
                        }
                        else
                        {
                            obj.Tk_TkCt_Id = (Guid)((ComboItem)ctl.SelectedItem).Id;
                        }
                        break;
                    case "Tk_TkSt_Id":
                        if (ctl.SelectedItem == null)
                        {
                            obj.Tk_TkSt_Id = null;
                        }
                        else
                        {
                            obj.Tk_TkSt_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                        }
                        break;
                    case "Tk_TkPr_Id":
                        if (ctl.SelectedItem == null)
                        {
                            obj.Tk_TkPr_Id = null;
                        }
                        else
                        {
                            obj.Tk_TkPr_Id = (Int32)((ComboItem)ctl.SelectedItem).Id;
                        }
                        break;
                }
                vXamComboColumn_SelectionChanged_CustomCode(sender, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridXamComboEditor_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Enter);
                if (e.Key == System.Windows.Input.Key.Escape)
                {
                    Task_Bll obj = _activeRow.Data as Task_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridXamComboEditor_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void vXamComboColumn_KeyUp(object sender, vXamComboColumnKeyEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Enter);
                if (e.XamComboColumnKeyEventArgs.Key == System.Windows.Input.Key.Escape)
                {
                    Task_Bll obj = _activeRow.Data as Task_Bll;
                    CancelGridRowEdit();
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamComboColumn_KeyUp", IfxTraceCategory.Leave);
            }
        }

        private void XamMultiColumnComboEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Enter);
                XamMultiColumnComboEditor comboEditor = sender as XamMultiColumnComboEditor;
                if (comboEditor.IsDropDownOpen == true) { return; }
                comboEditor.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamMultiColumnComboEditor_GotFocus", IfxTraceCategory.Leave);
            }
        }

        private void vXamMultiColumnComboColumn_SelectionChanged(object sender, vXamMultiColumnComboColumnSelectionChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vXamMultiColumnComboEditor ctl = sender as vXamMultiColumnComboEditor;
                if (ctl == null) { return; }
                Task_Bll obj = navList.ActiveCell.Row.Data as Task_Bll;
                if (obj == null) { return; }
                switch (e.Key)
                {
                }
                vXamMultiColumnComboColumn_SelectionChanged_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vXamMultiColumnComboColumn_SelectionChanged", IfxTraceCategory.Leave);
            }
        }

        private void XamGridDatePicker_TextChanged(object sender, TextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                Task_Bll obj = navList.ActiveCell.Row.Data as Task_Bll;
                if (obj == null) { return; }
                switch (ctl.Name)
                {
                    case "Tk_StartDate":
                        obj.SetDateFromString("Tk_StartDate", ctl.DatePickerText);
                        break;
                    case "Tk_Deadline":
                        obj.SetDateFromString("Tk_Deadline", ctl.DatePickerText);
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamGridDatePicker_TextChanged", IfxTraceCategory.Leave);
            }
        }





        private void XamColorPicker_SelectedColorChanged(object sender, SelectedColorChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                XamColorPicker ctl = sender as XamColorPicker;
                if (ctl == null) { return; }
                Task_Bll obj = navList.ActiveCell.Row.Data as Task_Bll;
                if (obj == null) { return; }
                switch (((CellControl)ctl.Parent).Cell.Column.Key)
                {
                }
                XamColorPicker_SelectedColorChanged_CustomCode(ctl, e);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "XamColorPicker_SelectedColorChanged", IfxTraceCategory.Leave);
            }
        }





        //  may not need this as the prop gets hit through binding.
        public void vDatePickerColumn_TextChanged(object sender, vDatePickerColumnTextChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Enter);
                if (_isRowInEditMode == false) { return; }
                vDatePicker ctl = sender as vDatePicker;
                if (ctl == null) { return; }
                Task_Bll obj = navList.ActiveCell.Row.Data as Task_Bll;
                if (obj == null) { return; }
                switch (e.Key)
                {
                    case "Tk_StartDate":
                        //obj.SetDateFromString("Tk_StartDate", ctl.DatePickerText);
                        obj.Tk_StartDate_asString = ctl.DatePickerText;
                        break;
                    case "Tk_Deadline":
                        //obj.SetDateFromString("Tk_Deadline", ctl.DatePickerText);
                        obj.Tk_Deadline_asString = ctl.DatePickerText;
                        break;
                }
                vDatePickerColumn_TextChanged_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vDatePickerColumn_TextChanged", IfxTraceCategory.Leave);
            }
        }

        public void vCheckColumn_CheckedChanged(object sender, vCheckColumnCheckedChangedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Enter);
                //if (_isRowInEditMode == false) { return; }
                vCheckBox ctl = sender as vCheckBox;
                if (ctl == null) { return; }
                Task_Bll obj = navList.ActiveCell.Row.Data as Task_Bll;
                if (obj == null) { return; }
                switch (e.Key)
                {
                    case "Tk_IsPrivate":
                        obj.Tk_IsPrivate = (bool)ctl.IsChecked;
                        break;
                    case "Tk_IsActiveRow":
                        obj.Tk_IsActiveRow = (bool)ctl.IsChecked;
                        break;
                }
                vCheckColumn_CheckedChanged_CustomCode(ctl, obj, e.Key);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "vCheckColumn_CheckedChanged", IfxTraceCategory.Leave);
            }
        }

        #endregion GridCell Editor Events

        
        #region Grid Other Events and Event Support Methods
        

        void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Enter);
                Cell cell = (Cell)navList.ActiveCell;
                IvControlsValidation ctl = cell.Control.Content as IvControlsValidation;
                if (ctl == null) { return; }
                Task_Bll obj = sender as Task_Bll;
                if (obj == null) { return; }

                switch (e.PropertyName)
                {
                    case "AttachmentCount":
                        SetGridCellValidationAppeance(ctl, obj, "AttachmentCount");
                        break;
                    case "DiscussionCount":
                        SetGridCellValidationAppeance(ctl, obj, "DiscussionCount");
                        break;
                    case "Tk_TkTp_Id":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_TkTp_Id");
                        break;
                    case "Tk_TkCt_Id":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_TkCt_Id");
                        break;
                    case "Tk_Number":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_Number");
                        break;
                    case "Tk_Name":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_Name");
                        break;
                    case "Tk_Desc":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_Desc");
                        break;
                    case "Tk_EstimatedTime":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_EstimatedTime");
                        break;
                    case "Tk_ActualTime":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_ActualTime");
                        break;
                    case "Tk_PercentComplete":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_PercentComplete");
                        break;
                    case "Tk_StartDate":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_StartDate");
                        break;
                    case "Tk_Deadline":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_Deadline");
                        break;
                    case "Tk_TkSt_Id":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_TkSt_Id");
                        break;
                    case "Tk_TkPr_Id":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_TkPr_Id");
                        break;
                    case "Tk_Results":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_Results");
                        break;
                    case "Tk_Remarks":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_Remarks");
                        break;
                    case "Tk_IsPrivate":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_IsPrivate");
                        break;
                    case "Tk_IsActiveRow":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_IsActiveRow");
                        break;
                    case "UserName":
                        SetGridCellValidationAppeance(ctl, obj, "UserName");
                        break;
                    case "Tk_LastModifiedDate":
                        SetGridCellValidationAppeance(ctl, obj, "Tk_LastModifiedDate");
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "navList_CellClicked", IfxTraceCategory.Leave);
            }
        }

        #endregion Grid Other Events and Event Support Methods

        
        #region Methods


        void AssignTextLengthLabels()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Enter);

                ((vTextColumn)this.navList.Columns["Tk_Name"]).MaxTextLength = 200;
                ((vTextColumn)this.navList.Columns["Tk_Desc"]).MaxTextLength = 2000;
                ((vTextColumn)this.navList.Columns["Tk_Results"]).MaxTextLength = 1000;
                ((vTextColumn)this.navList.Columns["Tk_Remarks"]).MaxTextLength = 1000;
                ((vTextColumn)this.navList.Columns["UserName"]).MaxTextLength = 256;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignTextLengthLabels", IfxTraceCategory.Leave);
            }
        }



        void ConfigureColumnHeaderTooltips()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Enter);

                    // AttachmentCount
                    ((IvColumn)this.navList.Columns["AttachmentCount"]).HeaderToolTipCaption = StringsTaskList.AttachmentCount_Vbs; 
                    ((IvColumn)this.navList.Columns["AttachmentCount"]).HeaderToolTipStringArray = new String[] {StringsTaskListTooltips.AttachmentCount_1};
                    ((IvColumn)this.navList.Columns["AttachmentCount"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // DiscussionCount
                    ((IvColumn)this.navList.Columns["DiscussionCount"]).HeaderToolTipCaption = StringsTaskList.DiscussionCount_Vbs; 
                    ((IvColumn)this.navList.Columns["DiscussionCount"]).HeaderToolTipStringArray = new String[] {StringsTaskListTooltips.DiscussionCount_1};
                    ((IvColumn)this.navList.Columns["DiscussionCount"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tk_EstimatedTime
                    ((IvColumn)this.navList.Columns["Tk_EstimatedTime"]).HeaderToolTipCaption = StringsTaskList.Tk_EstimatedTime_Vbs; 
                    ((IvColumn)this.navList.Columns["Tk_EstimatedTime"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tk_PercentComplete
                    ((IvColumn)this.navList.Columns["Tk_PercentComplete"]).HeaderToolTipCaption = StringsTaskList.Tk_PercentComplete_Vbs; 
                    ((IvColumn)this.navList.Columns["Tk_PercentComplete"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tk_IsPrivate
                    ((IvColumn)this.navList.Columns["Tk_IsPrivate"]).HeaderToolTipCaption = StringsTaskList.Tk_IsPrivate_Vbs; 
                    ((IvColumn)this.navList.Columns["Tk_IsPrivate"]).HeaderToolTipStringArray = new String[] {StringsTaskListTooltips.Tk_IsPrivate_1};
                    ((IvColumn)this.navList.Columns["Tk_IsPrivate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tk_IsActiveRow
                    ((IvColumn)this.navList.Columns["Tk_IsActiveRow"]).HeaderToolTipCaption = StringsTaskList.Tk_IsActiveRow_Vbs; 
                    ((IvColumn)this.navList.Columns["Tk_IsActiveRow"]).HeaderToolTipStringArray = new String[] {StringsTaskListTooltips.Tk_IsActiveRow_1, StringsTaskListTooltips.Tk_IsActiveRow_2};
                    ((IvColumn)this.navList.Columns["Tk_IsActiveRow"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // UserName
                    ((IvColumn)this.navList.Columns["UserName"]).HeaderToolTipCaption = StringsTaskList.UserName_Vbs; 
                    ((IvColumn)this.navList.Columns["UserName"]).HeaderToolTipStringArray = new String[] {StringsTaskListTooltips.UserName_1};
                    ((IvColumn)this.navList.Columns["UserName"]).HeaderTooltipContent = new TooltipBusinessRuleContent();

                    // Tk_LastModifiedDate
                    ((IvColumn)this.navList.Columns["Tk_LastModifiedDate"]).HeaderToolTipCaption = StringsTaskList.Tk_LastModifiedDate_Vbs; 
                    ((IvColumn)this.navList.Columns["Tk_LastModifiedDate"]).HeaderToolTipStringArray = new String[] {StringsTaskListTooltips.Tk_LastModifiedDate_1};
                    ((IvColumn)this.navList.Columns["Tk_LastModifiedDate"]).HeaderTooltipContent = new TooltipBusinessRuleContent();
                //ConfigureColumnHeaderTooltips_CustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Leave);
            }
        }


        void Set_vXamComboColumn_ItemSources()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Enter);

                ((vXamComboColumn)this.navList.Columns["Tk_TkTp_Id"]).ItemsSource = Task_Bll_staticLists.TaskType_ComboItemList_BindingListProperty;
                ((vXamComboColumn)this.navList.Columns["Tk_TkTp_Id"]).DisplayMemberPath = "ItemName";
                Task_Bll_staticLists.TaskType_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(TaskType_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)this.navList.Columns["Tk_TkCt_Id"]).ItemsSource = Task_Bll_staticLists.TaskCategory_ComboItemList_BindingListProperty;
                ((vXamComboColumn)this.navList.Columns["Tk_TkCt_Id"]).DisplayMemberPath = "ItemName";
                Task_Bll_staticLists.TaskCategory_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(TaskCategory_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)this.navList.Columns["Tk_TkSt_Id"]).ItemsSource = Task_Bll_staticLists.TaskStatus_ComboItemList_BindingListProperty;
                ((vXamComboColumn)this.navList.Columns["Tk_TkSt_Id"]).DisplayMemberPath = "ItemName";
                Task_Bll_staticLists.TaskStatus_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(TaskStatus_ComboItemList_BindingListProperty_DataSourceUpdated);

                ((vXamComboColumn)this.navList.Columns["Tk_TkPr_Id"]).ItemsSource = Task_Bll_staticLists.TaskPriority_ComboItemList_BindingListProperty;
                ((vXamComboColumn)this.navList.Columns["Tk_TkPr_Id"]).DisplayMemberPath = "ItemName";
                Task_Bll_staticLists.TaskPriority_ComboItemList_BindingListProperty.DataSourceUpdated += new StaticComboItemListUpdatedEventHandler(TaskPriority_ComboItemList_BindingListProperty_DataSourceUpdated);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Set_vXamComboColumn_ItemSources", IfxTraceCategory.Leave);
            }
        }

        void TaskType_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)this.navList.Columns["Tk_TkTp_Id"]).ItemsSource = Task_Bll_staticLists.TaskType_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskType_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskType_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void TaskCategory_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)this.navList.Columns["Tk_TkCt_Id"]).ItemsSource = Task_Bll_staticLists.TaskCategory_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskCategory_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void TaskStatus_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskStatus_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)this.navList.Columns["Tk_TkSt_Id"]).ItemsSource = Task_Bll_staticLists.TaskStatus_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskStatus_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskStatus_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }

        void TaskPriority_ComboItemList_BindingListProperty_DataSourceUpdated(object sender, StaticComboItemListUpdatedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskPriority_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Enter);

                ((vXamComboColumn)this.navList.Columns["Tk_TkPr_Id"]).ItemsSource = Task_Bll_staticLists.TaskPriority_ComboItemList_BindingListProperty;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskPriority_ComboItemList_BindingListProperty_DataSourceUpdated", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TaskPriority_ComboItemList_BindingListProperty_DataSourceUpdated", IfxTraceCategory.Leave);
            }
        }


        #region Combo ItemsSource for Non-Static Lists

        #endregion region Combo ItemsSource for Non-Static Lists

        void SetNewRowValidation()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Enter);
                if (_activeRow == null) { return; }
                Task_Bll obj = _activeRow.Data as Task_Bll;
                if (obj == null) { return; }
                // Note:  The lines to set the validaton styles below are not needed as the styles are being set from converters in the xaml style propeties, but they are left here incase needed in the future.  this is new code and we are unsure how it will evolve.
                // "AttachmentCount"
                if (_activeRow.Cells["AttachmentCount"].Control != null && _activeRow.Cells["AttachmentCount"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["AttachmentCount"].Control.Content), obj, "AttachmentCount");
                }

                // "DiscussionCount"
                if (_activeRow.Cells["DiscussionCount"].Control != null && _activeRow.Cells["DiscussionCount"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["DiscussionCount"].Control.Content), obj, "DiscussionCount");
                }

                // "Tk_TkTp_Id"
                if (_activeRow.Cells["Tk_TkTp_Id"].Control != null && _activeRow.Cells["Tk_TkTp_Id"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_TkTp_Id"].Control.Content), obj, "Tk_TkTp_Id");
                }

                // "Tk_TkCt_Id"
                if (_activeRow.Cells["Tk_TkCt_Id"].Control != null && _activeRow.Cells["Tk_TkCt_Id"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_TkCt_Id"].Control.Content), obj, "Tk_TkCt_Id");
                }

                // "Tk_Number"
                if (_activeRow.Cells["Tk_Number"].Control != null && _activeRow.Cells["Tk_Number"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_Number"].Control.Content), obj, "Tk_Number");
                }

                // "Tk_Name"
                if (_activeRow.Cells["Tk_Name"].Control != null && _activeRow.Cells["Tk_Name"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_Name"].Control.Content), obj, "Tk_Name");
                }

                // "Tk_Desc"
                if (_activeRow.Cells["Tk_Desc"].Control != null && _activeRow.Cells["Tk_Desc"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_Desc"].Control.Content), obj, "Tk_Desc");
                }

                // "Tk_EstimatedTime"
                if (_activeRow.Cells["Tk_EstimatedTime"].Control != null && _activeRow.Cells["Tk_EstimatedTime"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_EstimatedTime"].Control.Content), obj, "Tk_EstimatedTime");
                }

                // "Tk_ActualTime"
                if (_activeRow.Cells["Tk_ActualTime"].Control != null && _activeRow.Cells["Tk_ActualTime"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_ActualTime"].Control.Content), obj, "Tk_ActualTime");
                }

                // "Tk_PercentComplete"
                if (_activeRow.Cells["Tk_PercentComplete"].Control != null && _activeRow.Cells["Tk_PercentComplete"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_PercentComplete"].Control.Content), obj, "Tk_PercentComplete");
                }

                // "Tk_StartDate"
                if (_activeRow.Cells["Tk_StartDate"].Control != null && _activeRow.Cells["Tk_StartDate"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_StartDate"].Control.Content), obj, "Tk_StartDate");
                }

                // "Tk_Deadline"
                if (_activeRow.Cells["Tk_Deadline"].Control != null && _activeRow.Cells["Tk_Deadline"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_Deadline"].Control.Content), obj, "Tk_Deadline");
                }

                // "Tk_TkSt_Id"
                if (_activeRow.Cells["Tk_TkSt_Id"].Control != null && _activeRow.Cells["Tk_TkSt_Id"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_TkSt_Id"].Control.Content), obj, "Tk_TkSt_Id");
                }

                // "Tk_TkPr_Id"
                if (_activeRow.Cells["Tk_TkPr_Id"].Control != null && _activeRow.Cells["Tk_TkPr_Id"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_TkPr_Id"].Control.Content), obj, "Tk_TkPr_Id");
                }

                // "Tk_Results"
                if (_activeRow.Cells["Tk_Results"].Control != null && _activeRow.Cells["Tk_Results"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_Results"].Control.Content), obj, "Tk_Results");
                }

                // "Tk_Remarks"
                if (_activeRow.Cells["Tk_Remarks"].Control != null && _activeRow.Cells["Tk_Remarks"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_Remarks"].Control.Content), obj, "Tk_Remarks");
                }

                // "Tk_IsPrivate"
                if (_activeRow.Cells["Tk_IsPrivate"].Control != null && _activeRow.Cells["Tk_IsPrivate"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_IsPrivate"].Control.Content), obj, "Tk_IsPrivate");
                }

                // "Tk_IsActiveRow"
                if (_activeRow.Cells["Tk_IsActiveRow"].Control != null && _activeRow.Cells["Tk_IsActiveRow"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_IsActiveRow"].Control.Content), obj, "Tk_IsActiveRow");
                }

                // "UserName"
                if (_activeRow.Cells["UserName"].Control != null && _activeRow.Cells["UserName"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["UserName"].Control.Content), obj, "UserName");
                }

                // "Tk_LastModifiedDate"
                if (_activeRow.Cells["Tk_LastModifiedDate"].Control != null && _activeRow.Cells["Tk_LastModifiedDate"].IsEditable == true)
                {
                    SetGridCellValidationAppeance(((IvControlsValidation)_activeRow.Cells["Tk_LastModifiedDate"].Control.Content), obj, "Tk_LastModifiedDate");
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewRowValidation", IfxTraceCategory.Leave);
            }
        }

        #endregion Methods


    }
}
