﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Ifx.SL;
using System.ComponentModel;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using EntityBll.SL;
using ApplicationTypeServices;
using vUICommon;
using Infragistics.Controls.Grids;
using Velocity.SL;
using vComboDataTypes;
using vDP;

namespace UIControls
{
    public partial class ucProjectContacts : UserControl
    {




        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "ucProjectContacts";



        System.Windows.GridLength _navColumnWidth;

        ProxyWrapper.ProjectService_ProxyWrapper _proxy = null;
        //ProxyWrapper.CompanyService_ProxyWrapper _proxyCompany = null;

        Guid? _selectedCompanyId = null;

        UserSecurityContext _userContext = new UserSecurityContext();

        #endregion Initialize Variables




        public ucProjectContacts()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectContacts", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();

                _proxy = new ProxyWrapper.ProjectService_ProxyWrapper();

                //??????
                //ucProjPersons.PersonRemovedFromProject += new PersonRemovedFromProjectEventHandler(ucProjPersons_PersonRemovedFromProject);

                //_proxy.Project2Person_GetListByFKCompleted += new EventHandler<Project2Person_GetListByFKCompletedEventArgs>(Project2Person_GetListByFKCompleted);
                _proxy.GetPerson_lstByProjectCompleted += new EventHandler<GetPerson_lstByProjectCompletedEventArgs>(_proxy_GetPerson_lstByProjectCompleted);
                _proxy.GetPerson_lstNotAssignedToProjectCompleted += new EventHandler<GetPerson_lstNotAssignedToProjectCompletedEventArgs>(GetPerson_lstNotAssignedToProjectCompleted);
                //_proxy.GetPerson_lstNotAssignedToProject_ByCompanyCompleted += new EventHandler<GetPerson_lstNotAssignedToProject_ByCompanyCompletedEventArgs>(GetPerson_lstNotAssignedToProject_ByCompanyCompleted);

                _proxy.ExecuteProject2Person_assignPersonCompleted += new EventHandler<ExecuteProject2Person_assignPersonCompletedEventArgs>(ExecuteProject2Person_assignPersonCompleted);
                _proxy.ExecuteProject2Person_removePersonCompleted += new EventHandler<ExecuteProject2Person_removePersonCompletedEventArgs>(ExecuteProject2Person_removePersonCompleted);

                //_proxyCompany = new ProxyWrapper.CompanyService_ProxyWrapper();
                //_proxyCompany.GetCompany_lstAll_ComboItemListCompleted += new EventHandler<GetCompany_lstAll_ComboItemListCompletedEventArgs>(GetCompany_lstAll_ComboItemListCompleted);
                //_proxyCompany.GetCompany_lstRelatedToClient_ComboItemListCompleted += new EventHandler<GetCompany_lstRelatedToClient_ComboItemListCompletedEventArgs>(GetCompany_lstRelatedToClient_ComboItemListCompleted);
                //_proxyCompany.GetCompany_lstRelatedToProject_ComboItemListCompleted += new EventHandler<GetCompany_lstRelatedToProject_ComboItemListCompletedEventArgs>(GetCompany_lstRelatedToProject_ComboItemListCompleted);

                //ucProjPersons.CurrentEntityStateChanged += new TypeServices.CurrentEntityStateEventHandler(OnCurrentEntityStateChanged);
                //ucProjPersons.InitializeSplitScreenAndReadOnlyModes(false, false, false);

                ////xgdCompanies.SelectedRowsCollectionChanged += new EventHandler<SelectionCollectionChangedEventArgs<SelectedRowsCollection>>(xgdCompanies_SelectedRowsCollectionChanged);
                //xgdCompanies.CellClicked += new EventHandler<CellClickedEventArgs>(xgdCompanies_CellClicked);


                _userContext.SecurityArtifactsRetrieved += new SecurityArtifactsRetrievedEventHandler(UserSecurityContext_SecurityArtifactsRetrieved);
                SetSecurityState();

          

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectContacts", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjectContacts", IfxTraceCategory.Leave);
            }
        }







        #region Security




        private Guid _ancestorSecurityId = new Guid("59b35b3f-5d3c-4116-bd4a-6cc76bca4cea");
        ControlCache cCache;
        EntityCache eCache = null;
        bool defaultEntityOperationPermission = true;


        private void SetSecurityState()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Enter);
                _userContext.LoadArtifactPermissions(_ancestorSecurityId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetSecurityState", IfxTraceCategory.Leave);
            }
        }

        void UserSecurityContext_SecurityArtifactsRetrieved(object sender, SecurityArtifactsRetrievedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Enter);

                SecurityCache.AddControlCacheForAncestor(e.ArtifactAncestorData);
                cCache = SecurityCache.GetControlGroupById(e.ArtifactAncestorData.Id);

                //// Radio button - For this workshop
                //DP.SetControlSecurityId(rdbCompaniesForThisProject, new Guid("ffb2469b-2f1f-4ee0-b281-c34e75efc022"));
                //SecurityCache.SetWPFActionControlState(rdbCompaniesForThisProject, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //// Radio button - For this client
                //DP.SetControlSecurityId(rdbCompaniesForThisClient, new Guid("8fe23c3d-5929-4ab0-a7b9-ff910a73a3b0"));
                //SecurityCache.SetWPFActionControlState(rdbCompaniesForThisClient, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //// Radio button - Show all companies
                //DP.SetControlSecurityId(rdbAllCompanies, new Guid("7432fb09-e407-4ea6-881f-325938a8cb29"));
                //SecurityCache.SetWPFActionControlState(rdbAllCompanies, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //// Companies grid
                //DP.SetControlSecurityId(xgdCompanies, new Guid("5fa23800-c995-4174-acf3-fd40ab6162ca"));
                //SecurityCache.SetWPFActionControlState(xgdCompanies, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);

                //// Persons grid
                //DP.SetControlSecurityId(xgdPersons, new Guid("5ec44057-7293-464d-8de4-bd690284aa73"));
                //SecurityCache.SetWPFActionControlState(xgdPersons, EntityOperationType.View, cCache, eCache, defaultEntityOperationPermission);




            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserSecurityContext_SecurityArtifactsRetrieved", IfxTraceCategory.Leave);
            }
        }




        #endregion Security










        public void LoadLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadLists", IfxTraceCategory.Enter);


                if (ContextValues.CurrentProjectId == null)
                {
                    _xgdPersons_ItemSource.Clear();
                    _xgdAssigned_ItemSource.Clear();
                    xgdPersons.ItemsSource = _xgdPersons_ItemSource;
                    xgdAssigned.ItemsSource = _xgdAssigned_ItemSource;
                    return;
                }
                _proxy.Begin_GetPerson_lstByProject((Guid)ContextValues.CurrentProjectId);
                _proxy.Begin_GetPerson_lstNotAssignedToProject((Guid)ContextValues.CurrentProjectId);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadLists", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadLists", IfxTraceCategory.Leave);
            }
        }


        #region Events


        //void ucProjPersons_PersonRemovedFromProject(object sender, PersonRemovedFromProjectArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjPersons_PersonRemovedFromProject", IfxTraceCategory.Enter);

        //        _proxy.Begin_ExecuteProject2Person_removePerson((Guid)ContextValues.CurrentProjectId, (Guid)e.Pn_Id);
                
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjPersons_PersonRemovedFromProject", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucProjPersons_PersonRemovedFromProject", IfxTraceCategory.Leave);
        //    }
        //}

        void OnCurrentEntityStateChanged(object sender, TypeServices.CurrentEntityStateArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Enter);


                // It might be better to do nothing here as we might open up oportunity for bugs and its not critical at this point to disable other controls when the grid is dirty.

              

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Leave);
            }
        }


        #endregion region Events


        //#region xgdCompanies


        ////private void btnAllCompanies_Checked(object sender, RoutedEventArgs e)
        ////{
        ////    Guid? traceId = Guid.NewGuid();
        ////    try
        ////    {
        ////        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", IfxTraceCategory.Enter);

        ////        _proxyCompany.Begin_GetCompany_lstAll_ComboItemList();
            
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", ex);
        ////        ExceptionHelper.NotifyUserAnExceptionOccured();
        ////    }
        ////    finally
        ////    {
        ////        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", IfxTraceCategory.Leave);
        ////    }
        ////}

        ////private void btnCompaniesForThisClient_Checked(object sender, RoutedEventArgs e)
        ////{
        ////    Guid? traceId = Guid.NewGuid();
        ////    try
        ////    {
        ////        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", IfxTraceCategory.Enter);

        ////        _proxyCompany.Begin_GetCompany_lstRelatedToProject_ComboItemList((Guid)ContextValues.CurrentProjectId );

        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", ex);
        ////        ExceptionHelper.NotifyUserAnExceptionOccured();
        ////    }
        ////    finally
        ////    {
        ////        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", IfxTraceCategory.Leave);
        ////    }
        ////}



        //private void rdbAllCompanies_Click(object sender, RoutedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", IfxTraceCategory.Enter);

        //        //_proxyCompany.Begin_GetCompany_lstAll_ComboItemList();
        //        LoadCompanyList();
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", IfxTraceCategory.Leave);
        //    }
        //}

        //private void rdbCompaniesForThisClient_Click(object sender, RoutedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", IfxTraceCategory.Enter);

        //        //_proxyCompany.Begin_GetCompany_lstRelatedToClient_ComboItemList((Guid)ContextValues.CurrentClientId);
        //        LoadCompanyList();
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", IfxTraceCategory.Leave);
        //    }
        //}

        //private void rdbCompaniesForThisProject_Click(object sender, RoutedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", IfxTraceCategory.Enter);

        //        //_proxyCompany.Begin_GetCompany_lstRelatedToProject_ComboItemList((Guid)ContextValues.CurrentProjectId);
        //        LoadCompanyList();

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAllCompanies_Checked", IfxTraceCategory.Leave);
        //    }
        //}

        //void LoadCompanyList()
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadCompanyList", IfxTraceCategory.Enter);

        //        //_proxyCompany.Begin_GetCompany_lstRelatedToProject_ComboItemList((Guid)ContextValues.CurrentProjectId);


        //      if (rdbCompaniesForThisProject.IsChecked == true)
        //        {
        //            _proxyCompany.Begin_GetCompany_lstRelatedToProject_ComboItemList((Guid)ContextValues.CurrentProjectId);
        //        }
        //        else if (rdbCompaniesForThisClient.IsChecked == true)
        //        {
        //            _proxyCompany.Begin_GetCompany_lstRelatedToClient_ComboItemList((Guid)ContextValues.CurrentClientId);
        //        }
        //        else
        //        {  //  all companies
        //            _proxyCompany.Begin_GetCompany_lstAll_ComboItemList();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadCompanyList", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadCompanyList", IfxTraceCategory.Leave);
        //    }
        //}





        //void xgdCompanies_CellClicked(object sender, CellClickedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "xgdCompanies_CellClicked", IfxTraceCategory.Enter);
      
        //            ComboItem obj = e.Cell.Row.Data as ComboItem;
        //            if (obj == null) { return; }
        //            _selectedCompanyId = (Guid)obj.Id;

        //            _proxy.Begin_GetPerson_lstNotAssignedToProject_ByCompany((Guid)ContextValues.CurrentProjectId,(Guid) _selectedCompanyId);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "xgdCompanies_CellClicked", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "xgdCompanies_CellClicked", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion xgdCompanies


        #region xgdPersons



        private void btnAddPerson_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAddPerson_Click", IfxTraceCategory.Enter);

                Person_lstNotAssignedToProject_Binding data = ((CellControl)((System.Windows.Controls.Button)sender).Parent).Cell.Row.Data as Person_lstNotAssignedToProject_Binding;
                if (data == null) { return; }
                //if (data.Pn_FName == null && data.Pn_LName == null)  {  return;  }

                _proxy.Begin_ExecuteProject2Person_assignPerson((Guid)ContextValues.CurrentProjectId, data.Pn_Id, (Guid)Credentials.UserId);



                //foreach (Row rw in xgdPersons.Rows)
                //{
                //    if (rw.RowType == Infragistics.Controls.Grids.RowType.DataRow)
                //    {
                //        Person_lstNotAssignedToProject_Binding obj = rw.Data as Person_lstNotAssignedToProject_Binding;
                //        if (obj != null)
                //        {
                //            if (rw.IsActive == true)
                //            {
                //                _proxy.Begin_ExecuteProject2Person_assignPerson((Guid)ContextValues.CurrentProjectId, obj.Pn_Id, (Guid)Credentials.UserId);
                //                return;
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAddPerson_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnAddPerson_Click", IfxTraceCategory.Leave);
            }
        }



        #endregion xgdPersons




        #region xgdAssigned

        private void btnRemovePerson_Click(object sender, RoutedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRemovePerson_Click", IfxTraceCategory.Enter);

                //if (_isReadOnly == true)
                //{
                //    return;
                //}

                Person_lstByProject_Binding data = ((CellControl)((System.Windows.Controls.Button)sender).Parent).Cell.Row.Data as Person_lstByProject_Binding;
                if (data == null) { return; }
                //if (data.Pn_FName == null && data.Pn_LName == null) { return; }
                _proxy.Begin_ExecuteProject2Person_removePerson((Guid)ContextValues.CurrentProjectId, (Guid)data.Pn_Id);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRemovePerson_Click", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "btnRemovePerson_Click", IfxTraceCategory.Leave);
            }
        }

        #endregion xgdAssigned






        #region Fetch Data


        //void NavListRefreshFromObjectArray(object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", IfxTraceCategory.Enter);
        //        if (data != null)
        //        {
        //            ////if (xgdAssigned_ItemSource == null)
        //            ////{
        //            ////    xgdAssigned_ItemSource = new Project2Person_List();
        //            ////}
        //            ////xgdAssigned_ItemSource.ReplaceList(data);


        //            //// this is a hack to try to prevent the filter bug from occuring
        //            //xgdAssigned.ItemsSource = null;
        //            //xgdAssigned_ItemSource = null;
        //            //xgdAssigned_ItemSource = new Project2Person_List();
        //            //xgdAssigned_ItemSource.ReplaceList(data);
        //            //xgdAssigned.ItemsSource = xgdAssigned_ItemSource;
        //        }
        //        else
        //        {
        //            //if (xgdAssigned_ItemSource != null)
        //            //{
        //            //    xgdAssigned_ItemSource.Clear();
        //            //}
        //        }
        //        //SyncControlsWithCurrentBusinessObject();
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", IfxTraceCategory.Leave);
        //    }
        //}


        //ComboItemList _xgdCompanies_ItemsSource = new ComboItemList();

        //void xgdCompanies_RefreshFromObjectArray(object[] data)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", IfxTraceCategory.Enter);
        //        if (data != null)
        //        {
        //            //xgdCompanies.ItemsSource = null;
        //            //_xgdCompanies_ItemsSource.Clear();

        //            //_xgdCompanies_ItemsSource.ReplaceList(data);
        //            //xgdCompanies.ItemsSource = _xgdCompanies_ItemsSource;

        //            // this is a hack to try to prevent the filter bug from occuring
        //            xgdCompanies.ItemsSource = null;
        //            xgdCompanies.ItemsSource = null;
        //            xgdCompanies.ItemsSource = new ComboItemList();
        //            _xgdCompanies_ItemsSource.ReplaceList(data);
        //            xgdCompanies.ItemsSource = _xgdCompanies_ItemsSource;


        //        }
        //        else
        //        {
        //            if (xgdCompanies.ItemsSource != null)
        //            {
        //                _xgdCompanies_ItemsSource.Clear();
        //                ((ComboItemList)xgdCompanies.ItemsSource).Clear();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", ex);
        //        throw IfxWrapperException.GetError(ex, (Guid)traceId);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", IfxTraceCategory.Leave);
        //    }
        //}
        
      
        //void GetCompany_lstAll_ComboItemListCompleted(object sender, GetCompany_lstAll_ComboItemListCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCompany_lstAll_ComboItemListCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;
        //        object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
        //        xgdCompanies_RefreshFromObjectArray(array);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCompany_lstAll_ComboItemListCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCompany_lstAll_ComboItemListCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        //void GetCompany_lstRelatedToClient_ComboItemListCompleted(object sender, GetCompany_lstRelatedToClient_ComboItemListCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCompany_lstRelatedToClient_ComboItemListCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;
        //        object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
        //        xgdCompanies_RefreshFromObjectArray(array);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCompany_lstRelatedToClient_ComboItemListCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCompany_lstRelatedToClient_ComboItemListCompleted", IfxTraceCategory.Leave);
        //    }
        //}


        //void GetCompany_lstRelatedToProject_ComboItemListCompleted(object sender, GetCompany_lstRelatedToProject_ComboItemListCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCompany_lstRelatedToClient_ComboItemListCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;
        //        object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
        //        xgdCompanies_RefreshFromObjectArray(array);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCompany_lstRelatedToClient_ComboItemListCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetCompany_lstRelatedToClient_ComboItemListCompleted", IfxTraceCategory.Leave);
        //    }
        //}




        Person_lstNotAssignedToProject_Binding_List _xgdPersons_ItemSource = new Person_lstNotAssignedToProject_Binding_List();
        Person_lstAssignedToProject_Binding_List _xgdAssigned_ItemSource = new Person_lstAssignedToProject_Binding_List();

        void xgdPersons_RefreshFromObjectArray(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", IfxTraceCategory.Enter);
                if (data != null)
                {
                    xgdPersons.ItemsSource = null;
                    _xgdPersons_ItemSource.Clear();

                    _xgdPersons_ItemSource.ReplaceList(data);
                    xgdPersons.ItemsSource = _xgdPersons_ItemSource;
                }
                else
                {
                    if (xgdPersons.ItemsSource != null)
                    {
                        _xgdPersons_ItemSource.Clear();
                        ((Person_lstNotAssignedToProject_Binding_List)xgdPersons.ItemsSource).Clear();
                    }
                }
                //SyncControlsWithCurrentBusinessObject();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NavListRefreshFromObjectArray", IfxTraceCategory.Leave);
            }
        }



        //void GetPerson_lstNotAssignedToProject_ByCompanyCompleted(object sender, GetPerson_lstNotAssignedToProject_ByCompanyCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToProject_ByCompanyCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;
        //        object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
        //        xgdPersons_RefreshFromObjectArray(array);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToProject_ByCompanyCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToProject_ByCompanyCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        void  GetPerson_lstNotAssignedToProjectCompleted(object sender, GetPerson_lstNotAssignedToProjectCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToProjectCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                //xgdPersons_RefreshFromObjectArray(array);
                
                _xgdPersons_ItemSource.Clear();
                xgdPersons.ItemsSource = null;

                if (array != null) 
                {
                    for (int i = 0; i < array.GetUpperBound(0) + 1; i++)
                    {
                        _xgdPersons_ItemSource.Add(new Person_lstNotAssignedToProject_Binding((object[])array[i]));
                    }                
                }

                xgdPersons.ItemsSource = _xgdPersons_ItemSource;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToProjectCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToProjectCompleted", IfxTraceCategory.Leave);
            }
        }



        //void Project2Person_GetListByFKCompleted(object sender, Project2Person_GetListByFKCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Project2Person_GetListByFKCompleted", IfxTraceCategory.Enter);
        //        byte[] data = e.Result;
        //        object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
        //        NavListRefreshFromObjectArray(array);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Project2Person_GetListByFKCompleted", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Project2Person_GetListByFKCompleted", IfxTraceCategory.Leave);
        //    }
        //}

        void _proxy_GetPerson_lstByProjectCompleted(object sender, GetPerson_lstByProjectCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetPerson_lstByProjectCompleted", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];

                _xgdAssigned_ItemSource.Clear();
                xgdAssigned.ItemsSource = null;

                if (array != null)
                {
                    for (int i = 0; i < array.GetUpperBound(0) + 1; i++)
                    {
                        _xgdAssigned_ItemSource.Add(new Person_lstByProject_Binding((object[])array[i]));
                    }
                }

                xgdAssigned.ItemsSource = _xgdAssigned_ItemSource;



            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetPerson_lstByProjectCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_proxy_GetPerson_lstByProjectCompleted", IfxTraceCategory.Leave);
            }
        }



        void ExecuteProject2Person_assignPersonCompleted(object sender, ExecuteProject2Person_assignPersonCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteProject2Person_assignPersonCompleted", IfxTraceCategory.Enter);
                int? success = null;
                object[] data = e.Result;
                if (data == null)
                {
                    MessageBox.Show("There was problem making this assignment at the server." + Environment.NewLine + "If this continues, please contact suport.", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    success = data[0] as int?;
                    if (success == 1)
                    {
                        LoadLists();
                    }
                    else
                    {
                        MessageBox.Show("There was problem making this assignment at the server." + Environment.NewLine + "If this continues, please contact suport.", "Error", MessageBoxButton.OK);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteProject2Person_assignPersonCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteProject2Person_assignPersonCompleted", IfxTraceCategory.Leave);
            }
        }


        void ExecuteProject2Person_removePersonCompleted(object sender, ExecuteProject2Person_removePersonCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteProject2Person_removePersonCompleted", IfxTraceCategory.Enter);
                int? success = null;
                object[] data = e.Result;
                if (data == null)
                {
                    MessageBox.Show("There was problem making this removal at the server." + Environment.NewLine + "If this continues, please contact suport.", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    success = data[0] as int?;
                    if (success == 1)
                    {
                        LoadLists();
                    }
                    else if (success == -99)
                    {
                        MessageBox.Show("This person is assigned to one or more ideas and therefore," + Environment.NewLine + "can not be removed from the project." + Environment.NewLine + "Remove this person form all ides first.", "Not Able To Remove", MessageBoxButton.OK);
                        return;
                    }
                    else
                    {
                        MessageBox.Show("There was problem making this removal at the server." + Environment.NewLine + "If this continues, please contact suport.", "Error", MessageBoxButton.OK);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteProject2Person_removePersonCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteProject2Person_removePersonCompleted", IfxTraceCategory.Leave);
            }
        }


        #endregion Fetch Data





        public class Person_lstNotAssignedToProject_Binding_List : ObservableCollection<Person_lstNotAssignedToProject_Binding>
        {

            public Person_lstNotAssignedToProject_Binding_List() { }

            public Person_lstNotAssignedToProject_Binding_List(object[] list)
            {
                ReplaceList(list);
            }

            public void ReplaceList(object[] list)
            {
                base.Clear();

                for (int i = 0; i < list.GetUpperBound(0) + 1; i++)
                {
                    base.Add(new Person_lstNotAssignedToProject_Binding((object[])list[i]));
                }
            }

        }



        public class Person_lstAssignedToProject_Binding_List : ObservableCollection<Person_lstByProject_Binding>
        {

            public Person_lstAssignedToProject_Binding_List() { }

            public Person_lstAssignedToProject_Binding_List(object[] list)
            {
                ReplaceList(list);
            }

            public void ReplaceList(object[] list)
            {
                base.Clear();

                for (int i = 0; i < list.GetUpperBound(0) + 1; i++)
                {
                    base.Add(new Person_lstByProject_Binding((object[])list[i]));
                }
            }

        }

   






    }
}
