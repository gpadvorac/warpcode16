using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityWireTypeSL;
using System.ComponentModel;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;


namespace UIControls
{
    public class WcTableColumnComboColumnConcurrencyList : IConcurrencyList
    {

        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcTableColumnComboColumnConcurrencyList";

        List<ConcurrencyItem> _concurrencyList;

        #endregion Initialize Variables

        #region Constructors

        public WcTableColumnComboColumnConcurrencyList(WcTableColumnComboColumn_ValuesMngr data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Constructor - ucWcTableColumnComboColumnConcurrencyList", IfxTraceCategory.Enter);
                _concurrencyList = new List<ConcurrencyItem>();

                _concurrencyList.Add(new ConcurrencyItem("Index", data.C.TbCCmbC_Index, data.X.TbCCmbC_Index));
                _concurrencyList.Add(new ConcurrencyItem("Header Text", data.C.TbCCmbC_HeaderText, data.X.TbCCmbC_HeaderText));
                _concurrencyList.Add(new ConcurrencyItem("Minimum Width", data.C.TbCCmbC_MinWidth, data.X.TbCCmbC_MinWidth));
                _concurrencyList.Add(new ConcurrencyItem("Maximum Width", data.C.TbCCmbC_MaxWidth, data.X.TbCCmbC_MaxWidth));
                _concurrencyList.Add(new ConcurrencyItem("Text Wrap", data.C.TbCCmbC_TextWrap, data.X.TbCCmbC_TextWrap));
                _concurrencyList.Add(new ConcurrencyItem("Active record", data.C.TbCCmbC_IsActiveRow, data.X.TbCCmbC_IsActiveRow));
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConcurrencyList", IfxTraceCategory.Leave);
            }
        }

        #endregion Constructors

        public  List<ConcurrencyItem>  GetConcurrencyList()
        {
            return _concurrencyList; 
        }
    }
}



