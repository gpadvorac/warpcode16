﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using EntityBll.SL;
using Ifx.SL;
using Infragistics.Controls.Interactions;
using ProxyWrapper;
using vComboDataTypes;
using vControls;
using vDialogControl;
using vNotification;
//using vNotification.Globalization.v_Notification;
using vScheduleManager;
using vUICommon;
using vUICommon.Controls;

namespace UIControls
{
    public partial class ucJobList : UserControl
    {


        #region Initialize Variables

        private static string _as = "VelocityPrototype";
        private static string _cn = "ucJobList";

        private vScheduleJobService_ProxyWrapper _scheduleJobProxyWrapper = null;
        private ComboItemList _jobs = new ComboItemList();

        public event ShowNotificationEventsEventHandler ShowNotificationEvents;


        #endregion Initialize Variables


        public ucJobList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ucJobList", IfxTraceCategory.Enter);
                if (DesignerProperties.GetIsInDesignMode(this)) { return; }
                InitializeComponent();

                _scheduleJobProxyWrapper = new vScheduleJobService_ProxyWrapper();
                _scheduleJobProxyWrapper.Getv_ScheduleJob_ComboItemListCompleted += _scheduleJobProxyWrapper_Getv_ScheduleJob_ComboItemListCompleted;
                _scheduleJobProxyWrapper.RunJobCompleted += _scheduleJobProxyWrapper_RunJobCompleted;
                _scheduleJobProxyWrapper.Begin_Getv_ScheduleJob_ComboItemList();


                ConfigureColumnHeaderTooltips();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ucJobList", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ucJobList", IfxTraceCategory.Leave);
            }
        }






        void ConfigureColumnHeaderTooltips()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Enter);



                //// EditSchedule
                //vUnboundButtonColumn col = xdgJobs.Columns["EditSchedule"] as vUnboundButtonColumn;
                //col.HeaderToolTipCaption = "Edit Job Schedule";

                //((vUnboundButtonColumn)this.xdgJobs.Columns["EditSchedule"]).HeaderToolTipCaption = "Edit Job Schedule";
                //((vUnboundButtonColumn)this.xdgJobs.Columns["EditSchedule"]).HeaderToolTipStringArray = new String[] { "Click on the Pencil Button below (availableon hover) to open the Schedule window." };
                //((vUnboundButtonColumn)this.xdgJobs.Columns["EditSchedule"]).HeaderTooltipContent = new TooltipBusinessRuleContent();


                //// EditAssignees
                //((IvColumn)this.xdgJobs.Columns["EditAssignees"]).HeaderToolTipCaption = "Assign Recipients";
                //((IvColumn)this.xdgJobs.Columns["EditAssignees"]).HeaderToolTipStringArray = new String[] { "Click on the Person Button below (availableon hover) to open the Assignment window." };
                //((IvColumn)this.xdgJobs.Columns["EditAssignees"]).HeaderTooltipContent = new TooltipBusinessRuleContent();


                //// ExecuteJob
                //((IvColumn)this.xdgJobs.Columns["ExecuteJob"]).HeaderToolTipCaption = "Execute/Run Job Now";
                //((IvColumn)this.xdgJobs.Columns["ExecuteJob"]).HeaderToolTipStringArray = new String[] { "Click on the Execute Button below (availableon hover) to run the job now." };
                //((IvColumn)this.xdgJobs.Columns["ExecuteJob"]).HeaderTooltipContent = new TooltipBusinessRuleContent();


                //// JobHistory
                //((IvColumn)this.xdgJobs.Columns["JobHistory"]).HeaderToolTipCaption = "View Job History";
                //((IvColumn)this.xdgJobs.Columns["JobHistory"]).HeaderToolTipStringArray = new String[] { "Click on the Log Button below (availableon hover) to open the Job Histore window to view all job logs." };
                //((IvColumn)this.xdgJobs.Columns["JobHistory"]).HeaderTooltipContent = new TooltipBusinessRuleContent();


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", ex);
                throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConfigureColumnHeaderTooltips", IfxTraceCategory.Leave);
            }
        }








        #region Button Clicks
        

        private void BtnEditSchedule_OnClick(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnEditSchedule_OnClick", IfxTraceCategory.Enter);


                var item = e.Data as ComboItem;
                if (item != null)
                {
                    Guid jobId = (Guid)item.Id;
                    ucv_ScheduleProps ucSched = new ucv_ScheduleProps();
                    ucSched.GdDataButtons.Visibility = Visibility.Visible;
                    ucSched.Margin = new Thickness(0, 4, 0, 0);

                    ucSched.LoadControl();
                    v_Schedule_Bll obj = new v_Schedule_Bll();
                    obj.GetEntityRow_ByJobId(jobId);

                    ucSched.SetBusinessObject(obj);
                    ucSched.Margin = new Thickness(0, 8, 0, 0);

                    var scheduleDialog = new vDialog
                    {
                        StartupPosition = StartupPosition.Center,
                        MinimizeButtonVisibility = Visibility.Collapsed,
                        MaximizeButtonVisibility = Visibility.Collapsed,
                        //MinHeight = 250,
                        HeaderHeight = 24,
                        Padding = new Thickness(2),
                        Content = ucSched,
                        HeaderIconVisibility = Visibility.Collapsed,
                        Header = "Job Scheduler",
                        HeaderForeground = new SolidColorBrush(Colors.White),
                        IsModal = true
                    };
                    scheduleDialog.Show();


                }



                //Button btn = sender as Button;
                //if (btn == null)
                //{
                //    return;
                //}

                //Guid jobId = (Guid)btn.Tag;
                //ucv_ScheduleProps ucSched = new ucv_ScheduleProps();
                //ucSched.GdDataButtons.Visibility = Visibility.Visible;
                //ucSched.Margin = new Thickness(0, 4, 0, 0);

                //ucSched.LoadControl();
                //v_Schedule_Bll obj = new v_Schedule_Bll();
                //obj.GetEntityRow_ByJobId(jobId);

                //ucSched.SetBusinessObject(obj);
                //ucSched.Margin = new Thickness(0, 8, 0, 0);

                //var scheduleDialog = new vDialog
                //{
                //    StartupPosition = StartupPosition.Center,
                //    MinimizeButtonVisibility = Visibility.Collapsed,
                //    MaximizeButtonVisibility = Visibility.Collapsed,
                //    //MinHeight = 250,
                //    HeaderHeight = 24,
                //    Padding = new Thickness(5),
                //    Content = ucSched,
                //    HeaderIconVisibility = Visibility.Collapsed,
                //    Header = "Job Scheduler",
                //};
                //scheduleDialog.Show();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnEditSchedule_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnEditSchedule_OnClick", IfxTraceCategory.Leave);
            }
        }
        
        private void BtnEditAssignees_OnClick(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnEditAssignees_OnClick", IfxTraceCategory.Enter);


                var item = e.Data as ComboItem;
                if (item != null)
                {
                    Guid jobId = (Guid)item.Id;
                    string isManualyAssignRecipients = item.Desc;
                    // 0=Recipients are not manually assigned, so ucNotificationRecipients does not apply.
                    // 1=true;
                    if (isManualyAssignRecipients == "0")
                    {
                        MessageBox.Show("Cannot manually assign recipients to this Job." + System.Environment.NewLine +
                                        "Recipients are automatically assigned from the database.", "No Manually Assigned Recipients", MessageBoxButton.OK);
                        return;
                    }
                    ucNotificationRecipients ucNotRecp = new ucNotificationRecipients();
                    //ucSched.GdDataButtons.Visibility = Visibility.Visible;
                    ucNotRecp.Margin = new Thickness(0, 8, 0, 0);

                    ucNotRecp.NotificationId = jobId;
                    ucNotRecp.LoadLists();



                    //ucSched.LoadControl();
                    //v_Schedule_Bll obj = new v_Schedule_Bll();
                    //obj.GetEntityRow_ByJobId(jobId);

                    //ucSched.SetBusinessObject(obj);
                    //ucSched.Margin = new Thickness(0, 8, 0, 0);

                    var scheduleDialog = new vDialog
                    {
                        StartupPosition = StartupPosition.Center,
                        MinimizeButtonVisibility = Visibility.Collapsed,
                        MaximizeButtonVisibility = Visibility.Collapsed,
                        //MinHeight = 250,
                        HeaderHeight = 24,
                        Width = 600,
                        Height = 400,
                        Padding = new Thickness(2),
                        Content = ucNotRecp,
                        HeaderIconVisibility = Visibility.Collapsed,
                        Header = "Assign Recipients to Notification",
                        HeaderForeground = new SolidColorBrush(Colors.White),
                        IsModal = true
                    };
                    scheduleDialog.Show();


                }



                //Button btn = sender as Button;
                //if (btn == null)
                //{
                //    return;
                //}

                //Guid jobId = (Guid)btn.Tag;
                //ucv_ScheduleProps ucSched = new ucv_ScheduleProps();
                //ucSched.GdDataButtons.Visibility = Visibility.Visible;
                //ucSched.Margin = new Thickness(0, 4, 0, 0);

                //ucSched.LoadControl();
                //v_Schedule_Bll obj = new v_Schedule_Bll();
                //obj.GetEntityRow_ByJobId(jobId);

                //ucSched.SetBusinessObject(obj);
                //ucSched.Margin = new Thickness(0, 8, 0, 0);

                //var scheduleDialog = new vDialog
                //{
                //    StartupPosition = StartupPosition.Center,
                //    MinimizeButtonVisibility = Visibility.Collapsed,
                //    MaximizeButtonVisibility = Visibility.Collapsed,
                //    //MinHeight = 250,
                //    HeaderHeight = 24,
                //    Padding = new Thickness(5),
                //    Content = ucSched,
                //    HeaderIconVisibility = Visibility.Collapsed,
                //    Header = "Job Scheduler",
                //};
                //scheduleDialog.Show();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnEditAssignees_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnEditAssignees_OnClick", IfxTraceCategory.Leave);
            }
        }
        
        private void BtnExecuteJob_OnClick(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnExecuteJob_OnClick", IfxTraceCategory.Enter);

                var item = e.Data as ComboItem;
                if (item != null)
                {
                    Guid jobId = (Guid)item.Id;
                    _scheduleJobProxyWrapper.Begin_RunJob(jobId);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnExecuteJob_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnExecuteJob_OnClick", IfxTraceCategory.Leave);
            }
        }


        private void BtnJobHistory_OnClick(object sender, ButtonColumnClickEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnJobHistory_OnClick", IfxTraceCategory.Enter);

                var item = e.Data as ComboItem;
                if (item != null)
                {
                    RaiseShowNotificationEvents((Guid)item.Id, item.ItemName);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnJobHistory_OnClick", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BtnJobHistory_OnClick", IfxTraceCategory.Leave);
            }
        }


        void RaiseShowNotificationEvents(Guid notificationId, string notificationTitle)
        {
            ShowNotificationEventsArgs e = new ShowNotificationEventsArgs(notificationId, notificationTitle);
            ShowNotificationEventsEventHandler handler = ShowNotificationEvents;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        #endregion Button Clicks




        #region Fetch Data Retults

        void _scheduleJobProxyWrapper_Getv_ScheduleJob_ComboItemListCompleted(object sender, Getv_ScheduleJob_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_scheduleJobProxyWrapper_Getv_ScheduleJob_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _jobs.ReplaceList(data);
                xdgJobs.ItemsSource = _jobs;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_scheduleJobProxyWrapper_Getv_ScheduleJob_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_scheduleJobProxyWrapper_Getv_ScheduleJob_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }


        void _scheduleJobProxyWrapper_RunJobCompleted(object sender, RunJobCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "_scheduleJobProxyWrapper_RunJobCompleted", IfxTraceCategory.Enter);
                int? success = e.Result;

                if (success == 0)
                {
                    MessageBox.Show("There was a problem in running this job.", "Job Failed", MessageBoxButton.OK);
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "_scheduleJobProxyWrapper_RunJobCompleted", ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "_scheduleJobProxyWrapper_RunJobCompleted", IfxTraceCategory.Leave);
            }
        }


        #endregion Fetch Data Retults



    }
}
