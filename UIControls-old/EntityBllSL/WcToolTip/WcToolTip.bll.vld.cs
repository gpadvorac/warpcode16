using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/2/2016 3:31:22 PM

namespace EntityBll.SL
{
    public partial class WcToolTip_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_Tt_Tip = 1000;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_Tt_Id_Required = "Id is a required field.";
		private const string BROKENRULE_Tt_TbC_Id_Required = "TbC_Id is a required field.";
		private const string BROKENRULE_Tt_Sort_Required = "Sort is a required field.";
		private const string BROKENRULE_Tt_Sort_ZeroNotAllowed = "Sort:  '0'  (zero) is not allowed.";
		private string BROKENRULE_Tt_Tip_TextLength = "Tip has a maximum text length of  '" + STRINGSIZE_Tt_Tip + "'.";
		private const string BROKENRULE_Tt_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_Tt_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_Tt_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("Tt_Id", BROKENRULE_Tt_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tt_TbC_Id", BROKENRULE_Tt_TbC_Id_Required);
				_brokenRuleManager.AddBrokenRuleForProperty("Tt_Sort", BROKENRULE_Tt_Sort_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tt_Sort", BROKENRULE_Tt_Sort_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tt_Tip", BROKENRULE_Tt_Tip_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tt_IsActiveRow", BROKENRULE_Tt_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tt_IsDeleted", BROKENRULE_Tt_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tt_Stamp", BROKENRULE_Tt_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void Tt_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_Id");
                string newBrokenRules = "";
                
                if (Tt_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tt_Id", BROKENRULE_Tt_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tt_Id", BROKENRULE_Tt_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tt_Id", _brokenRuleManager.IsPropertyValid("Tt_Id"), IsPropertyDirty("Tt_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tt_TbC_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_TbC_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_TbC_Id");
                string newBrokenRules = "";
                
                if (Tt_TbC_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tt_TbC_Id", BROKENRULE_Tt_TbC_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tt_TbC_Id", BROKENRULE_Tt_TbC_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_TbC_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tt_TbC_Id", _brokenRuleManager.IsPropertyValid("Tt_TbC_Id"), IsPropertyDirty("Tt_TbC_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_TbC_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_TbC_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tt_Sort_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Sort_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_Sort");
                string newBrokenRules = "";
                
                if (Tt_Sort == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tt_Sort", BROKENRULE_Tt_Sort_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tt_Sort", BROKENRULE_Tt_Sort_Required);
                }

                if (Tt_Sort == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tt_Sort", BROKENRULE_Tt_Sort_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tt_Sort", BROKENRULE_Tt_Sort_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_Sort");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tt_Sort", _brokenRuleManager.IsPropertyValid("Tt_Sort"), IsPropertyDirty("Tt_Sort"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Sort_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Sort_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tt_Tip_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Tip_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_Tip");
                string newBrokenRules = "";
                				int len = 0;
                if (Tt_Tip != null)
                {
                    len = Tt_Tip.Length;
                }

                if (len > STRINGSIZE_Tt_Tip)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tt_Tip", BROKENRULE_Tt_Tip_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tt_Tip", BROKENRULE_Tt_Tip_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_Tip");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tt_Tip", _brokenRuleManager.IsPropertyValid("Tt_Tip"), IsPropertyDirty("Tt_Tip"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Tip_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Tip_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tt_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_IsActiveRow");
                string newBrokenRules = "";
                
                if (Tt_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tt_IsActiveRow", BROKENRULE_Tt_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tt_IsActiveRow", BROKENRULE_Tt_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tt_IsActiveRow", _brokenRuleManager.IsPropertyValid("Tt_IsActiveRow"), IsPropertyDirty("Tt_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tt_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_IsDeleted");
                string newBrokenRules = "";
                
                if (Tt_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tt_IsDeleted", BROKENRULE_Tt_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tt_IsDeleted", BROKENRULE_Tt_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tt_IsDeleted", _brokenRuleManager.IsPropertyValid("Tt_IsDeleted"), IsPropertyDirty("Tt_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tt_CreatedUserId_Validate()
        {
        }

        private void Tt_CreatedDate_Validate()
        {
        }

        private void Tt_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tt_LastModifiedDate_Validate()
        {
        }

        private void Tt_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_Stamp");
                string newBrokenRules = "";
                
                if (Tt_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tt_Stamp", BROKENRULE_Tt_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tt_Stamp", BROKENRULE_Tt_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tt_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tt_Stamp", _brokenRuleManager.IsPropertyValid("Tt_Stamp"), IsPropertyDirty("Tt_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


