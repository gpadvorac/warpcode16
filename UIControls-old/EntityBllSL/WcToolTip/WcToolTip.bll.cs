using System;
using System.Collections.Generic;
using TypeServices;
using System.ComponentModel;
using System.Diagnostics;
using EntityWireTypeSL;
using Ifx.SL;
using Velocity.SL;
using ProxyWrapper;
using vComboDataTypes;

// Gen Timestamp:  12/2/2016 3:31:22 PM

namespace EntityBll.SL
{

    /// <summary>
    /// 	<para><strong>About this Entity:<br/></strong>***General description of the entity
    ///     from WC***</para>
    /// 	<para><strong>About this class:</strong></para>
    /// 	<para>The framework’s standard business objects have a lot of great features and
    ///     characteristics as they are designed for rich functionality and performance. These
    ///     types are parsed into 5 separate code files which facilitate ‘<a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Entity_Bll_File_Structure.html">code
    ///     generation</a>’ and improve maintainability.</para>
    /// 	<list type="bullet">
    /// 		<item>For an overview of the business object structure, see ‘<a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Entity_Design_Pattern_In_The_Business_Logic_Layer.html">Entity
    ///         Design Pattern in the Business Logic Layer</a>’.</item>
    /// 		<item>For information on the type’s file structure, see ‘<a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Entity_Bll_File_Structure.html">Entity_Bll
    ///         File Structure</a>’.</item>
    /// 		<item>For information on the type’s optimization and ‘plug-n-play’ data
    ///         objects, see ‘<a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Entity_Values_Manager_Type.html">Entity
    ///         Values Manager Type</a>’.</item>
    /// 		<item>
    ///             For general information about the type’s list class, see ‘<a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Entity_List_Type.html">Entity
    ///             List Type</a>’ or specifically for WcToolTip, see <see cref="WcToolTip_List">WcToolTip_List</see>.
    ///         </item>
    /// 	</list>
    /// </summary>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Entity_Design_Pattern_In_The_Business_Logic_Layer.html" cat="Framework and Design Pattern">Entity Design Pattern In The Business Logic Layer</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Entity_Bll_File_Structure.html" cat="Framework and Design Pattern">Code Gen and the Entity_Bll File Structure</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Entity_List_Type.html" cat="Framework and Design Pattern">Entity List Type</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Entity_List_File_Structure.html" cat="Framework and Design Pattern">Entity List File Structure</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Type_Overview.html" cat="Framework and Design Pattern">Wire Type Overview (Plug-n-Play Data Objects)</seealso>
    /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Framework and Design Pattern">Concurrency Overview</seealso>

    public partial class WcToolTip_Bll : BusinessObjectBase, ITraceItem, IBusinessObject, IBusinessObjectV2, INotifyPropertyChanged , IEditableObject
    {


        #region Initialize Variables

        /// <summary>
        /// This is a private instance of TraceItemList which is normaly used in every entity
        /// business type and is used to provide rich information abou the current state and values
        /// in this business object when included in a trace call. To see exactly what data will be
        /// provided in a trace, look at the GetTraceItemsShortList method in the WcToolTip_Bll code
        /// file.
        /// </summary>
        /// <seealso cref="!:file://D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Optimized_Deep_Tracing_of_Business_Object.html" cat="My Test Category">My Test Caption</seealso>
        TraceItemList _traceItems;
        /// <seealso cref="EntityWireType.WcToolTip_Values">WcToolTip_Values Class</seealso>
        /// <summary>
        ///     This is an instance of the wire type (<see cref="EntityWireType.WcToolTip_Values">WcToolTip_Values</see>) that’s wrapped by the Values
        ///     Manager (<see cref="EntityWireType.WcToolTip_ValuesMngr">WcToolTip_ValuesMngr</see>) and
        ///     used to Plug-n-Play into this business object.
        /// </summary>
        private WcToolTip_ValuesMngr _data = null;

        /// <summary>See <see cref="OnCrudFailed">OnCrudFailed</see> for information.</summary>
        public event CrudFailedEventHandler CrudFailed;

        public event AsyncSaveCompleteEventHandler AsyncSaveComplete;

        public event AsyncSaveWithResponseCompleteEventHandler AsyncSaveWithResponseComplete;

        public event EntityRowReceivedEventHandler EntityRowReceived;


        /// <summary>
        ///     See <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see> for
        ///     information.
        /// </summary>
        public event CurrentEntityStateEventHandler CurrentEntityStateChanged;
        /// <summary>See <see cref="OnBrokenRuleChanged">OnBrokenRuleChanged</see> for information.</summary>
        public event BrokenRuleEventHandler BrokenRuleChanged;
        /// <summary>
        /// 	<para>
        ///         Raised by the This event is currently <see cref="OnPropertyValueChanged">OnPropertyValueChanged</see> method, its currently not
        ///         being used and may be obsolete.
        ///     </para>
        /// </summary>
        public event PropertyValueChangedEventHandler PropertyValueChanged;
        /// <summary>This event is currently not being used and may be obsolete.</summary>
        public event BusinessObjectUpdatedEventHandler BusinessObjectUpdated;
        /// <summary>
        /// 	<para>
        ///         Required by the INotifyPropertyChanged interface, this event is raise by the
        ///         <see cref="Notify">Notify</see> method (also part of the INotifyPropertyChanged
        ///         interface. The Notify method is called in nearly all public data field
        ///         properties. The INotifyPropertyChanged interface is implemented to make
        ///         WcToolTip_Bll more extendable.
        ///     </para>
        /// 	<para>The <strong>INotifyPropertyChanged</strong> interface is used to notify
        ///     clients, typically binding clients, that a property value has changed. For more
        ///     information, search INotifyPropertyChanged on <strong>MSDN</strong>.</para>
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 	<para>
        ///         Private field for the <see cref="ActiveRestrictedStringProperty">ActiveRestrictedStringProperty</see> property.
        ///         See it’s documentation for details.
        ///     </para>
        /// 	<para></para>
        /// </summary>
        private string _activeRestrictedStringProperty;
        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcToolTip_Bll";
        /// <summary>
        /// 	<para>
        ///         Used in the validation section of this class in the WcToolTip.bll.vld.cs code file,
        ///         this event notifies event handlers that a data field’s valid state has changed
        ///         (valid or not valid).<br/>
        ///         For more information about this event, see <see cref="TypeServices.ControlValidStateChangedEventHandler">CurrentEntityStateEventHandler</see>.<br/>
        ///         For more information on how it’s used, see these methods:
        ///     </para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="NewEntityRow">NewEntityRow</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="GetEntityRow">GetEntityRow</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="Save">Save</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcToolTipProps.RaiseCurrentEntityStateChanged">ucWcToolTipProps.RaiseCurrentEntityStateChanged()</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcToolTipProps.RaiseCurrentEntityStateChanged">ucWcToolTipProps.RaiseCurrentEntityStateChanged(EntityStateSwitch
        ///             state)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public event ControlValidStateChangedEventHandler ControlValidStateChanged;
        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcToolTipProps.OnRestrictedTextLengthChanged">ucWcToolTipProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcToolTipProps.SetRestrictedStringLengthText">ucWcToolTipProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public event RestrictedTextLengthChangedEventHandler RestrictedTextLengthChanged;

        UserSecurityContext _context = null;

        private object _parentEditObject = null;
        private ParentEditObjectType _parentType = ParentEditObjectType.None;

        WcToolTipService_ProxyWrapper _wcToolTipProxy = null;



        Guid _StandingFK = Guid.Empty;
        string _parentEntityType = "";

        #endregion Initialize Variables

            
        #region Initialize Class

        public WcToolTip_Bll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_Bll()", IfxTraceCategory.Enter);
                _context = UserSecurityContext.GetCurrent();
                InitializeClass();
//                WcToolTipService_ProxyWrapper _wcToolTipProxy = new  WcToolTipService_ProxyWrapper();
//                _wcToolTipProxy.WcToolTip_SaveCompleted += new EventHandler<WcToolTip_SaveCompletedEventArgs>(SaveCompleted);
                SetNewEntityRow();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_Bll()", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_Bll()", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// 	<para>
        ///         Passes in the <see cref="EntityWireType.WcToolTip_ValuesMngr">WcToolTip_ValuesMngr</see> type and <see cref="TypeServices.EntityState">EntityState</see>. The WcToolTip_ValuesMngr type is the
        ///         data object used in a ‘Plug-n-Play’ fashion for loading data into WcToolTip_Bll
        ///         fast and efficiently. EntityState sets WcToolTip_Bll’s state.
        ///     </para>
        /// 	<para>Used by the following methods call this constructor:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="FromWire">FromWire</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcToolTip_List.Fill">Fill</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcToolTip_List.ReplaceList">ReplaceList(WcToolTip_ValuesMngr[]
        ///             list)</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="WcToolTip_List.ReplaceList">ReplaceList(IEntity_ValuesMngr[]
        ///             list)</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        public WcToolTip_Bll(WcToolTip_ValuesMngr valueObject, EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_Bll(WcToolTip_ValuesMngr valueObject, EntityState state)", IfxTraceCategory.Enter);
                _context = UserSecurityContext.GetCurrent(); 
                _data = valueObject;
                InitializeClass(state);
               // _data.SetClassStateAfterFetch(state);
//                 WcToolTipService_ProxyWrapper _wcToolTipProxy = new  WcToolTipService_ProxyWrapper();
//                _wcToolTipProxy.WcToolTip_SaveCompleted += new EventHandler<WcToolTip_SaveCompletedEventArgs>(SaveCompleted);
        }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_Bll(WcToolTip_ValuesMngr valueObject, EntityState state)", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_Bll(WcToolTip_ValuesMngr valueObject, EntityState state)", IfxTraceCategory.Leave);
            }
        }


        private void InitializeClass()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Enter);
                InitializeClass(new EntityState(true, true, false));
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Leave);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass", IfxTraceCategory.Leave);
            }
        }



        /// <summary>Called from the constructor, events are wired here.</summary>
        private void InitializeClass(EntityState state)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeClass(EntityState state)", IfxTraceCategory.Enter);
                if (_data == null)
                {
                    _data = GetDefault();
                    //**  This is a hack.  We should set the state elsewhere, however, when we get this from accross the wire,
                    //      we loose the state and therefore are forced to always call SetClassStateAfterFetch.
                   // _data.SetClassStateAfterFetch(state);
                }
                _data.SetClassStateAfterFetch(state);
                BrokenRuleManagerProperty = new BrokenRuleManager(this);
                _brokenRuleManager.BrokenRuleChanged += new BrokenRuleEventHandler(OnBrokenRuleChanged);
                //TODO: need to code it for a dynamic creation for an insert
                //TODO: need to move save, assignwithid, update, delete, deactivate, persist (generic, check the state) into this class
                //TODO: need to move the various lists of apps to get into an applicationprovider class for the client side that thunks to the proxywrapper
                //TODO: need to move field level validation into the setters and call the object validation on any persist operation
                //TODO:need to implement the two eh and tracing interfaces on the object
                InitializeClass_Cust();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EntityState", IfxTraceCategory.Leave);
            }
        }

        private  WcToolTipService_ProxyWrapper WcToolTipProxy
        {
            get
            {
                if (_wcToolTipProxy == null)
                {
                    _wcToolTipProxy = new  WcToolTipService_ProxyWrapper();
                    _wcToolTipProxy.WcToolTip_SaveCompleted += new EventHandler<WcToolTip_SaveCompletedEventArgs>(SaveCompleted);
                    _wcToolTipProxy.WcToolTip_GetByIdCompleted += new EventHandler<WcToolTip_GetByIdCompletedEventArgs>(GetEntityRowResponse);

                }

                return _wcToolTipProxy;
            }
        }

        #endregion  Initialize Class


		#region CRUD Methods



        /// <returns><see cref="EntityWireType.WcToolTip_ValuesMngr">WcToolTip_ValuesMngr</see></returns>
        /// <summary>
        ///     When this business type is set to a new state, a new data type also known as a wire
        ///     type (<see cref="EntityWireType.WcToolTip_Values">WcToolTip_Values</see>) is created, new
        ///     Id value is created, default values are set and appropriate state setting are made.
        ///     WcToolTip_Values is wrapped in the <see cref="EntityWireType.WcToolTip_ValuesMngr">WcToolTip_ValuesMngr</see> type and returned.
        /// </summary>
        private WcToolTip_ValuesMngr GetDefault()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                Guid Id = GetNewID();
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Enter);
                return new WcToolTip_ValuesMngr(new object[] 
                    {
                        Id,    //  Tt_Id
						null,		//  Tt_TbC_Id
						null,		//  Tt_Sort
						null,		//  Tt_Tip
						true,		//  Tt_IsActiveRow
						false,		//  Tt_IsDeleted
						null,		//  Tt_CreatedUserId
						null,		//  Tt_CreatedDate
						null,		//  Tt_UserId
						null,		//  UserName
						null,		//  Tt_LastModifiedDate
						null,		//  Tt_Stamp
                    }, new EntityState(true, true, false)
                );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDefault", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// Called by any parent object that wants to clear out the existing data and state,
        /// and replace all with new state and data object.
        /// </summary>
        public void NewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Enter);
                _data = GetDefault();
                NewEntityRowCustomCode();
                SetDefaultBrokenRules();
                //RaiseEventCurrentEntityStateChanged(EntityStateSwitch.NewInvalidNotDirty);
                RaiseEventCurrentEntityStateChanged(_data.S.Switch);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "NewEntityRow", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// This is called from the parameterless constructor.  It's assumed that a data object will not be passed (which would have
        /// cause the EntityState to be configured properly) and therefore this new entity must be properly configured.
        /// </summary>
        public void SetNewEntityRow()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", IfxTraceCategory.Enter);
                NewEntityRowCustomCode();
                SetDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetNewEntityRow", IfxTraceCategory.Leave);
            }
        }


        public void GetEntityRow(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", new ValuePair[] {new ValuePair("Id", Id) }, IfxTraceCategory.Enter);

                WcToolTipProxy.Begin_WcToolTip_GetById(Id);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRow", IfxTraceCategory.Leave);
            }
        }

        public void GetEntityRow(long Id)
        {
            throw new NotImplementedException("WcToolTip_Bll GetEntityRow(long Id) Not Implemented");
        }

        public void GetEntityRow(int Id)
        {
            throw new NotImplementedException("WcToolTip_Bll GetEntityRow(int Id) Not Implemented");
        }

        public void GetEntityRow(short Id)
        {
            throw new NotImplementedException("WcToolTip_Bll GetEntityRow(short Id) Not Implemented");
        }

        public void GetEntityRow(byte Id)
        {
            throw new NotImplementedException("WcToolTip_Bll GetEntityRow(byte Id) Not Implemented");
        }

        public void GetEntityRow(object Id)
        {
            throw new NotImplementedException("WcToolTip_Bll GetEntityRow(object Id) Not Implemented");
        }

        private void GetEntityRowResponse(object sender, WcToolTip_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", IfxTraceCategory.Enter);
                byte[] data = e.Result;
                if (data == null)
                {
                    // Alert UI
                    return;
                }
                object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                if (array == null)
                {
                    // Alert UI
                    return;
                }
                _brokenRuleManager.ClearAllBrokenRules();
                _data.ReplaceData((object[])array[0], new EntityState(false, true, false));
                RaiseEventEntityRowReceived();
                RefreshFields();
                // Do we still need this next line?
                //RaiseEventCurrentEntityStateChanged(_data.S.Switch);

                //  ToDo:  Later when we have a Foreign Key, do something like this:  DataProps.TbC_Tb_ID = DataProps.standing_FK;
                //              How do we handle the standing FK in the new framework?
 
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetEntityRowResponse", IfxTraceCategory.Leave);
            }
        }



        /// <summary>
        ///     Called by <see cref="GetDefault">GetDefault</see> and creates a new WcToolTip Id value
        ///     for a new data object (<see cref="EntityWireType.WcToolTip_ValuesMngr">WcToolTip_ValuesMngr</see>).
        /// </summary>
        private Guid GetNewID()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", IfxTraceCategory.Enter);
                return Guid.NewGuid();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetNewID", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Un-does the current data edits and restores everything back to the previous saved
        ///     or new <see cref="TypeServices.EntityState">state</see>. This will include calling
        ///     <see cref="TypeServices.BrokenRuleManager.ClearAllBrokenRules">_brokenRuleManager.ClearAllBrokenRules</see>
        ///     to clear any BrokenRules or calling <see cref="SetDefaultBrokenRules">SetDefaultBrokenRules</see> to set any default BrokenRules;
        ///     and raising the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event is raised to
        ///     notify the parent objects that the state has changed.
        /// </summary>
        public void UnDo()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Enter);
                EntityStateSwitch es = _data.S.Switch;
                _data.SetCurrentToOriginal();
                if (_data.S.IsNew() == true)
                {
                    SetDefaultBrokenRules();
                }
                else
                {
                    _brokenRuleManager.ClearAllBrokenRules();
                }
                CheckEntityState(es);

                UnDoCustomCode();
                RefreshFields();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UnDo", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Call the Notify method for all visible fields.  This allows the fields/properties to be refreshed in grids and other controls that are consuming them via databinding.
        /// </summary>
        public void RefreshFields()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", IfxTraceCategory.Enter);
				Notify("Tt_Sort");
				Notify("Tt_Tip");
				Notify("Tt_IsActiveRow");
				Notify("UserName");
				Notify("Tt_LastModifiedDate");

                RefreshFieldsCustomCode();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshFields", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Called from <see cref="wnConcurrencyManager">wnConcurrencyManager</see> and therefore does not need
        ///     to pass in the Parent and Parent Type parameters.
        ///     Saves the current data to the data store, configures the appropriate state and
        ///     calls <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see> to
        ///     notify the parent object of the state change.
        /// </summary>
        public void Save(UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                _data.C.Tt_UserId_noevents = Credentials.UserId;
                WcToolTipProxy.Begin_WcToolTip_Save(_data.C.GetValues(), (int)check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     Saves the current data to the data store, configures the appropriate state and
        ///     calls <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see> to
        ///     notify the parent object of the state change.
        /// </summary>
        public void Save(object parentEditObject, ParentEditObjectType parentType,  UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Enter);
                _parentEditObject = parentEditObject;
                _parentType = parentType;
                _data.C.Tt_UserId_noevents = Credentials.UserId;
                WcToolTipProxy.Begin_WcToolTip_Save(_data.C.GetValues(), (int)check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Save", IfxTraceCategory.Leave);
            }
        }

        void SaveCompleted(object sender, WcToolTip_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", IfxTraceCategory.Enter);
                DataServiceInsertUpdateResponse newData = null;
                if (e.Result == null)
                {
                    // return some message to the calling object
                    newData = new DataServiceInsertUpdateResponse();
                    newData.Result = DataOperationResult.HandledException;
                    //RaiseEventAsyncSaveComplete(newData.Result, null);
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);
                }
                else
                {
                    newData = new DataServiceInsertUpdateResponse();
                    byte[] data = e.Result;
                    object[] array = Serialization.SilverlightSerializer.Deserialize(data) as object[];
                    newData.SetFromObjectArray(array);
                }

                // Get the Result status
                if (newData.Result == DataOperationResult.Success)
                {
                    if (newData.ReturnCurrentRowOnInsertUpdate == true)
                    {
                        // If we returned the current row from the data store, then syncronize the wire object with it.
                        _data.C.ReplaceDataFromObjectArray(newData.CurrentValues);
                        RefreshFields();
                    }
                    else
                    {
                        // Otherwise:
                        //  Set the PK value from the data store incase we did an insert.
                        //  In theory the guid was already created by this busness object, but we're setting it here
                        //  just in case some code changed somewhere and it was created on the server.
                        _data.C._a = newData.guidPrimaryKey;
                        //  Set the new TimeStamp value
                        _data.C.Tt_Stamp_noevents = newData.CurrentTimestamp;
                    }
                    _data.S.SetNotNew();
                    _data.S.SetValid();
                    _data.S.SetNotDirty();
                    _data.SetOriginalToCurrent();
                  
                    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);

                    //if (_parentType == ParentEditObjectType.EntitiyPropertiesControl)
                    //{
                    //    // A EntitiyPropertiesControl called the same method, therefore raise the following event
                    //    // allowing all parent conrol to reconfigure thier entity state.
                    //    // If this was called from editing a grid control, then this is not nessesacy since we 
                    //    // dont change the state of surrounding controls.
                    //    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                    //
                    //    // Rasie this event to the props control can call it's SetState method to refresh the UI
                    //    // The Entity List control will be updated via the PropertyChanged event which gets called from the RefreshFields call above.
                    //    if (newData.ReturnCurrentRowOnInsertUpdate == true)
                    //    {
                    //        RaiseEventAsyncSaveWithResponseComplete(newData, null);
                    //    }
                    //
                    //}
                    //else
                    //{
                    //    RaiseEventAsyncSaveComplete(newData.Result, null);
                    //}
                }
                else
                {
                    // Add the current date from the database to the wire obect's concurrent property
                    //if (newData.Result == DataOperationResult.ConcurrencyFailure)
                    //{
                    //    _data.X.ReplaceDataFromObjectArray(newData.CurrentValues);
                    //}
                    // ToDo:  we need to develope logic and code to pass user friendly error message 
                    // instead of null when the save failed.

                    // We had some type of failure, so raise this event regardless of the parent type 
                    // so the UI can notify the user.
                    //RaiseEventAsyncSaveComplete(newData.Result, null);
                    //*** use this instead
                    RaiseEventAsyncSaveWithResponseComplete(newData, null);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                _parentEditObject = null;
                _parentType = ParentEditObjectType.None;
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SaveCompleted", IfxTraceCategory.Leave);
            }
        }







        /// <summary>
        /// Calls a Delete method on the server to delete this entity from the data
        /// store.
        /// </summary>
        /// <returns>1 = Success, 2 = Failed</returns>
        public int  Entity_Delete()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", IfxTraceCategory.Enter);
//                // add credentials
//                WcToolTip_ValuesMngr retObject = ProxyWrapper.EntityProxyWrapper.WcToolTip_Delete(_data);
//                return 1;
//                //  Needs further design.  What do we do when the delete fails or there is an concurrency issue?
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Delete", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Just a stub and not being used. Consider renaming this to ActivatOrDeactivate so
        /// it can be used either way.
        /// </summary>
        /// <returns>Returns 0 because this method is not currently functional.</returns>
        public int Entity_Deactivate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", IfxTraceCategory.Enter);
                //  This is currently setup to return a wire object, however, we might only need to return an int flag
                //  Needs further design
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Deactivate", IfxTraceCategory.Leave);
            }
        }

        /// <returns>Returns 0 because this method is not currently functional.</returns>
        /// <summary>Just a stub and not being used.</summary>
        public int Entity_Remove()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", IfxTraceCategory.Enter);
                //  This is currently setup to return a wire object, however, we might only need to return an int flag
                //  Needs further design
                return 0;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Entity_Remove", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        ///     Copies the values from another data object (<see cref="EntityWireType.WcToolTip_ValuesMngr">WcToolTip_ValuesMngr</see>) to the one plugged into
        ///     this business object. This is called from the Save method because it returns a
        ///     fresh copy of this entity’s data from the data store incase modifications were made
        ///     in the process of saving.
        /// </summary>
        /// <param name="thisData">
        /// The data object (WcToolTip_ValuesMngr) plugged into this business object which data
        /// will be copied to.
        /// </param>
        /// <param name="newData">The data object (WcToolTip_ValuesMngr) which data will be copied from.</param>
        private void SyncValueObjectCurrentProperties(WcToolTip_ValuesMngr thisData, WcToolTip_ValuesMngr newData)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", IfxTraceCategory.Enter);
				thisData.C.Tt_Id_noevents = newData.C.Tt_Id_noevents;
				thisData.C.Tt_TbC_Id_noevents = newData.C.Tt_TbC_Id_noevents;
				thisData.C.Tt_Sort_noevents = newData.C.Tt_Sort_noevents;
				thisData.C.Tt_Tip_noevents = newData.C.Tt_Tip_noevents;
				thisData.C.Tt_IsActiveRow_noevents = newData.C.Tt_IsActiveRow_noevents;
				thisData.C.Tt_IsDeleted_noevents = newData.C.Tt_IsDeleted_noevents;
				thisData.C.Tt_CreatedUserId_noevents = newData.C.Tt_CreatedUserId_noevents;
				thisData.C.Tt_CreatedDate_noevents = newData.C.Tt_CreatedDate_noevents;
				thisData.C.Tt_UserId_noevents = newData.C.Tt_UserId_noevents;
				thisData.C.UserName_noevents = newData.C.UserName_noevents;
				thisData.C.Tt_LastModifiedDate_noevents = newData.C.Tt_LastModifiedDate_noevents;
				thisData.C.Tt_Stamp_noevents = newData.C.Tt_Stamp_noevents;
			}
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SyncValueObjectCurrentProperties", IfxTraceCategory.Leave);
            }
        }


		#endregion CRUD Methods


		#region General Methods and Properties


		#region Wire Methods

        /// <summary>
        /// Returns a business object implementing the IBusinessObject interface from a wire
        /// object. This assumes the WcToolTip_ValuesMngr being passed in has been fetched from a
        /// reliable data store or some other reliable source which gives us appropriate data and
        /// state.
        /// </summary>
        public static WcToolTip_Bll FromWire(WcToolTip_ValuesMngr valueObject)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", new ValuePair[] {new ValuePair("valueObject", valueObject) }, IfxTraceCategory.Enter);
                //  This assumes that each WcToolTip_ValuesMngr has been fetched from a db or some other reliable source which gives us the following state
                //  Hack, Hack;  we have to add the state here becuase of how its handled (or not handled) in WcToolTip_ValuesMngr due to the problem of loosing this value when passed accross the wire
                WcToolTip_Bll obj = new WcToolTip_Bll(valueObject, new EntityState(false, true, false));
                return obj;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FromWire", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Returns the current wire object plugged into this business object.</summary>
        public WcToolTip_ValuesMngr ToWire()
        {
            return _data;
        }

        /// <summary>The wire object property.</summary>
        public WcToolTip_ValuesMngr Wire
        {
            get { return _data; }
            set
            {
                _data = value;
            }
        }        

		#endregion Wire Methods


        /// <overloads>Get a list of current BrokenRules for this entity.</overloads>
        /// <summary>Retuns the current BrokenRules as list of strings.</summary>
        public List<string> GetBrokenRulesForEntity()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRulesForEntity();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        /// Pass in a valid format as a string and return the current BrokenRules in a
        /// formatted list of strings.
        /// </summary>
        public List<string> GetBrokenRulesForEntity(string format)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", new ValuePair[] {new ValuePair("format", format) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRulesForEntity(format);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForEntity", IfxTraceCategory.Leave);
            }
        }

        // get the current list of broken rules for a property
        /// <summary>Pass in a property name and return a list of its current BrokenRules.</summary>
        public List<vRuleItem> GetBrokenRulesForProperty(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.GetBrokenRuleListForProperty(propertyName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRulesForProperty", IfxTraceCategory.Leave);
            }
        }

        /// <summary>Pass in a property name to find out if it’s valid or not.</summary>
        /// <returns>true = valid, false = not valid</returns>
        public bool IsPropertyValid(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);
                return _brokenRuleManager.IsPropertyValid(propertyName);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyValid", IfxTraceCategory.Leave);
            }
        }

        public bool IsPropertyDirty(string propertyName)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", new ValuePair[] {new ValuePair("propertyName", propertyName) }, IfxTraceCategory.Enter);

                if (propertyName == null)
                {
                    throw new Exception("WcToolTip_Bll.IsPropertyDirty: Parameter 'propertyName' had no value.");
                }
                else if (propertyName.Trim().Length == 0)
                {
                    throw new Exception("WcToolTip_Bll.IsPropertyDirty: Parameter 'propertyName' had no value.");
                }

                switch (propertyName)
                {
                    case "Tt_TbC_Id":
                        if (_data.C.Tt_TbC_Id != _data.O.Tt_TbC_Id)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tt_Sort":
                        if (_data.C.Tt_Sort != _data.O.Tt_Sort)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tt_Tip":
                        if (_data.C.Tt_Tip != _data.O.Tt_Tip)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tt_IsActiveRow":
                        if (_data.C.Tt_IsActiveRow != _data.O.Tt_IsActiveRow)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tt_IsDeleted":
                        if (_data.C.Tt_IsDeleted != _data.O.Tt_IsDeleted)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "Tt_CreatedUserId":
                        if (_data.C.Tt_CreatedUserId != _data.O.Tt_CreatedUserId)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case "UserName":
                        if (_data.C.UserName != _data.O.UserName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    default:
                        throw new Exception("WcToolTip_Bll.IsPropertyDirty found no matching propery name for " + propertyName);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "IsPropertyDirty", IfxTraceCategory.Leave);
            }
        }

        public void SetDateFromString(string propName, string value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", new ValuePair[] {new ValuePair("value", value), new ValuePair("propName", propName) }, IfxTraceCategory.Enter);
                DateTime? dt = null;
                if (BLLHelper.IsDate(value) == true)
                {
                    dt = DateTime.Parse(value);
                }
                switch (propName)
                {
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDateFromString", IfxTraceCategory.Leave);
            }
        }

        public object GetPropertyValueByKey(string key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", new ValuePair[] {new ValuePair("key", key) }, IfxTraceCategory.Enter);
                switch (key)
                {
                    case "Tt_Sort":
                        return Tt_Sort;

                    case "Tt_Tip":
                        return Tt_Tip;

                    case "Tt_IsActiveRow":
                        return Tt_IsActiveRow;

                    case "UserName":
                        return UserName;

                    case "Tt_LastModifiedDate":
                        return Tt_LastModifiedDate;

                    default:
                        return GetPropertyValueByKeyCustomCode(key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyValueByKey", IfxTraceCategory.Leave);
            }
        }

        public string GetPropertyFormattedStringValueByKey(string key)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", new ValuePair[] {new ValuePair("key", key) }, IfxTraceCategory.Enter);
                switch (key)
                {
                    case "EntityId":
                        return Tt_Id.ToString();

                    case "Tt_Sort":
                        if (Tt_Sort == null)
                        {
                            return null;
                        }
                        else
                        {
                            return Tt_Sort.ToString();
                        }

                    case "Tt_Tip":
                        return Tt_Tip;

                    case "Tt_IsActiveRow":
                        return Tt_IsActiveRow.ToString();

                    case "UserName":
                        return UserName;

                    case "Tt_LastModifiedDate":
                        if (Tt_LastModifiedDate == null)
                        {
                            return null;
                        }
                        else
                        {
                            return Tt_LastModifiedDate.ToString();
                        }

                    default:
                        return GetPropertyFormattedStringValueByKeyCustomCode(key);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPropertyFormattedStringValueByKey", IfxTraceCategory.Leave);
            }
        }

        
        #region General Properties

        /// <summary>
        ///     Returns a list of current BrokenRules for this entity as a list of <see cref="TypeServices.vRuleItem">vRuleItem</see> types.
        /// </summary>
        [Browsable(false)]
        public List<vRuleItem> GetBrokenRuleListForEntity()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", IfxTraceCategory.Enter);
                return BrokenRuleManagerProperty.GetBrokenRuleListForEntity();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetBrokenRuleListForEntity", IfxTraceCategory.Leave);
            }
        }

        #endregion General Properties        

		#endregion General Methods and Properties


		#region Events


        /// <summary>
        ///     Raises the <see cref="CrudFailed">CrudFailed</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnCrudFailed(object sender, CrudFailedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Enter);
            CrudFailedEventHandler handler = CrudFailed;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCrudFailed", IfxTraceCategory.Leave);
        }

        // THIS IS NOW OBSOLETE
        /// <summary>
        ///     Calls the <see cref="OnAsyncSaveComplete">OnAsyncSaveComplete</see> to raise the <see cref="AsyncSaveComplete">AsyncSaveComplete</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventAsyncSaveComplete(DataOperationResult result, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveComplete", IfxTraceCategory.Enter);
            AsyncSaveCompleteArgs e = new AsyncSaveCompleteArgs(result, failedReasonText);
            OnAsyncSaveComplete(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveComplete", IfxTraceCategory.Leave);
        }

        // THIS IS NOW OBSOLETE
        /// <summary>
        ///     Raises the <see cref="AsyncSaveComplete">AsyncSaveComplete</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnAsyncSaveComplete(object sender, AsyncSaveCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveComplete", IfxTraceCategory.Enter);
            AsyncSaveCompleteEventHandler handler = AsyncSaveComplete;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveComplete", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Calls the <see cref="OnAsyncSaveWithResponseComplete">OnAsyncSaveWithResponseComplete</see> to raise the <see cref="AsyncSaveWithResponseComplete">AsyncSaveWithResponseComplete</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventAsyncSaveWithResponseComplete(DataServiceInsertUpdateResponse response, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
            AsyncSaveWithResponseCompleteArgs e = new AsyncSaveWithResponseCompleteArgs(response, failedReasonText);
            OnAsyncSaveWithResponseComplete(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventAsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Raises the <see cref="AsyncSaveWithResponseComplete">AsyncSaveWithResponseComplete</see> event when a CRUD operation
        ///     fails and is used as a notification to UI controls so they can notify the user.
        /// </summary>
        void OnAsyncSaveWithResponseComplete(object sender, AsyncSaveWithResponseCompleteArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveWithResponseComplete", IfxTraceCategory.Enter);
            AsyncSaveWithResponseCompleteEventHandler handler = AsyncSaveWithResponseComplete;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnAsyncSaveWithResponseComplete", IfxTraceCategory.Leave);
        }










        void RaiseEventEntityRowReceived()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventEntityRowReceived", IfxTraceCategory.Enter);
            EntityRowReceivedEventHandler handler = EntityRowReceived;
            if (handler != null)
            {
                handler(this, new EntityRowReceivedArgs());
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventEntityRowReceived", IfxTraceCategory.Leave);
        }

        /// <summary>
        ///     Calls the <see cref="OnCrudFailed">OnCrudFailed</see> to raise the <see cref="CrudFailed">CrudFailed</see> event when a CRUD operation fails and is used as a
        ///     notification to UI controls so they can notify the user.
        /// </summary>
        void RaiseEventCrudFailed(int failureCode, string failedReasonText)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCrudFailed", IfxTraceCategory.Enter);
            CrudFailedArgs e = new CrudFailedArgs(failureCode, failedReasonText);
            OnCrudFailed(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCrudFailed", IfxTraceCategory.Leave);
        }


        /// <summary>
        ///     Calls the <see cref="OnCurrentEntityStateChanged">OnCurrentEntityStateChanged</see>
        ///     method to raise the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see> event passing in a
        ///     reference of this business object for the ActiveBusinessObject parameter. As it
        ///     bubbles up to an Entity Properties control, that control will pass in a reference
        ///     of itself for the ActivePropertiesControl parameter. As it bubbles up to the Entity
        ///     Manager control, that control will pass in a reference of itself for the
        ///     ActiveEntityControl parameter. It should continue to bubble up to the top level
        ///     control. This notifies all controls along the about which controls are active and
        ///     the current state so they can always be configures accordingly. Now that the top
        ///     level control (perhaps the main application window) has a reference to these 3
        ///     important objects, it can easily communicate with then as the use interacts with
        ///     the application.
        /// </summary>        
        void RaiseEventCurrentEntityStateChanged(EntityStateSwitch state)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCurrentEntityStateChanged", new ValuePair[] {new ValuePair("state", state) }, IfxTraceCategory.Enter);
            CurrentEntityStateArgs e = new CurrentEntityStateArgs(state, null, null, this);
            OnCurrentEntityStateChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventCurrentEntityStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     Raises the <see cref="CurrentEntityStateChanged">CurrentEntityStateChanged</see>
        ///     event passing in a reference of this business object for the ActiveBusinessObject
        ///     parameter. As it bubbles up to an Entity Properties control, that control will pass
        ///     in a reference of itself for the ActivePropertiesControl parameter. As it bubbles
        ///     up to the Entity Manager control, that control will pass in a reference of itself
        ///     for the ActiveEntityControl parameter. It should continue to bubble up to the top
        ///     level control. This notifies all controls along the about which controls are active
        ///     and the current state so they can always be configures accordingly. Now that the
        ///     top level control (perhaps the main application window) has a reference to these 3
        ///     important objects, it can easily communicate with then as the use interacts with
        ///     the application.
        /// </summary>
        void OnCurrentEntityStateChanged(object sender, CurrentEntityStateArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", new ValuePair[] {new ValuePair("e.State", e.State) }, IfxTraceCategory.Enter);
            CurrentEntityStateEventHandler handler = CurrentEntityStateChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnCurrentEntityStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     This method is obsolete and is replaced with <see cref="RaiseEventControlValidStateChanged">RaiseEventControlValidStateChanged</see> which
        ///     is called in the validation section of this class in the partial class
        ///     WcToolTip.bll.vld.cs code file.
        /// </summary>
        void RaiseEventBrokenRuleChanged(string rule)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventBrokenRuleChanged", new ValuePair[] {new ValuePair("rule", rule) }, IfxTraceCategory.Enter);
            BrokenRuleArgs e = new BrokenRuleArgs(rule);
            OnBrokenRuleChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventBrokenRuleChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>This method is mostly obsolete, but remains because it’s an easy way to be
        ///     notified of the current broken rule.</para>
        /// 	<para>
        ///         This method is hooked to the <see cref="BrokenRuleChanged">BrokenRuleChanged</see> event which is initiated by
        ///         BrokenRuleManager and bubbles up to the business object and then to this
        ///         control (ucWcToolTipProps) (or in some cases a ucEntityList control when it’s in
        ///         Read/Write mode).
        ///     </para>
        /// 	<para><br/>
        ///     For information on how Broken Rules are currently being managed, see (in the order
        ///     of execution):</para>
        /// 	<list type="bullet">
        /// 		<item>In the business object’s data property – FieldName_Validate();</item>
        /// 		<item>
        ///             In the FieldName_Validate method this line executes:<br/>
        ///             _brokenRuleManager.<see cref="TypeServices.BrokenRuleManager.AddBrokenRuleForProperty">AddBrokenRuleForProperty</see>("FieldName",
        ///             BROKENRULE_ FieldName _SomeRuleName);
        ///         </item>
        /// 		<item>
        /// 			<see cref="TypeServices.BrokenRuleManager.SetEntityValidState">BrokenRuleManager.SetEntityValidState()</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
       private void OnBrokenRuleChanged(object sender, BrokenRuleArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Enter);
            BrokenRuleEventHandler handler = BrokenRuleChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnBrokenRuleChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// Notifies an event handler that a property value as change. Currently not being
        /// uses.
        /// </summary>
        private void OnPropertyValueChanged(object sender, PropertyValueChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnPropertyValueChanged", new ValuePair[] {new ValuePair("e.PropertyName", e.PropertyName), new ValuePair("e.IsValid", e.IsValid), new ValuePair("e.BrokenRules", e.BrokenRules) }, IfxTraceCategory.Enter);
            PropertyValueChangedEventHandler handler = PropertyValueChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnPropertyValueChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        ///     Replaces the RaiseEventBrokenRuleChanged method and used in the validation section
        ///     of this class in the partial class WcToolTip.bll.vld.cs code file. Validation code will
        ///     pass in property name and an isValid flag. This will call the <see cref="OnControlValidStateChanged">OnControlValidStateChanged</see> method which will
        ///     raise the <see cref="ControlValidStateChanged">ControlValidStateChanged</see> event
        ///     up to the parent. The parent will then take the appropriate actions such as setting
        ///     the control’s valid/not valid appearance.
        /// </summary>
        void RaiseEventControlValidStateChanged(string propertyName, bool isValid, bool isDirty)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventControlValidStateChanged", new ValuePair[] {new ValuePair("isValid", isValid) }, IfxTraceCategory.Enter);
            ControlValidStateChangedArgs e = new ControlValidStateChangedArgs(propertyName, isValid, isDirty);
            OnControlValidStateChanged(this, e);
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventControlValidStateChanged", IfxTraceCategory.Leave);
        }       

        /// <summary>
        /// Raise the ControlValidStateChanged event up to the parent. The parent will then
        /// take the appropriate actions such as setting the control’s valid/not valid
        /// appearance.
        /// </summary>
        private void OnControlValidStateChanged(object sender, ControlValidStateChangedArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", new ValuePair[] {new ValuePair("e.IsValid", e.IsValid) }, IfxTraceCategory.Enter);
            ControlValidStateChangedEventHandler handler = ControlValidStateChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "OnControlValidStateChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcToolTipProps.OnRestrictedTextLengthChanged">ucWcToolTipProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcToolTipProps.SetRestrictedStringLengthText">ucWcToolTipProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void RaiseEventRestrictedTextLengthChanged()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Enter);
            RestrictedTextLengthChangedArgs e = new RestrictedTextLengthChangedArgs();
            RestrictedTextLengthChangedEventHandler handler = RestrictedTextLengthChanged;
            if (handler != null)
            {
                handler(this, e);
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Leave);
        }        

        /// <summary>
        /// 	<para>Notifies event handlers that a string data field’s text length has changed
        ///     and passes back the number of remaining available characters that can be
        ///     added.</para>
        /// 	<para>See how it’s used:</para>
        /// 	<param name="oldVal">The original property value.</param>
        /// 	<param name="newVal">The new property value.</param>        
        /// 	<list type="bullet">
        /// 		<item>
        /// 			<see cref="UIControls.ucWcToolTipProps.OnRestrictedTextLengthChanged">ucWcToolTipProps.OnRestrictedTextLengthChanged</see>
        /// 		</item>
        /// 		<item>
        /// 			<see cref="UIControls.ucWcToolTipProps.SetRestrictedStringLengthText">ucWcToolTipProps.SetRestrictedStringLengthText</see>
        /// 		</item>
        /// 	</list>
        /// </summary>
        void RaiseEventRestrictedTextLengthChanged(string oldVal, string newVal)
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Enter);
            bool lenChanged = false;
            if (oldVal == null && newVal != null)
            {
                if (newVal.Trim().Length > 0)
                {
                    lenChanged = true;
                }
            }
            else if (newVal == null && oldVal != null)
            {
                if (oldVal.Trim().Length > 0)
                {
                    lenChanged = true;
                }
            }
            else if (oldVal != null && newVal != null)
            {
                if (oldVal.Trim().Length != newVal.Trim().Length)
                {
                    lenChanged = true;
                }
            }
            if (lenChanged == true)
            {
                RestrictedTextLengthChangedArgs e = new RestrictedTextLengthChangedArgs();
                RestrictedTextLengthChangedEventHandler handler = RestrictedTextLengthChanged;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RaiseEventRestrictedTextLengthChanged", IfxTraceCategory.Leave);
        }        


		#endregion Events


		#region Flag Props


        /// <summary>
        ///     The name of the (text type – TextBox, TextBlock, etc.) control in the UI that
        ///     currently has the focus. This is used by the <see cref="ActiveRestrictedStringPropertyLength">ActiveRestrictedStringPropertyLength</see>
        ///     method to which returns the remaining available text length for the text control
        ///     with that name. It was decided to persist the name of the UI control in this
        ///     business object property rather than in the UI because it seemed to make sense to
        ///     have all of this logic centralized, and also because it simplifies things.
        /// </summary>
        [Browsable(false)]
        public string ActiveRestrictedStringProperty
        {
            get { return _activeRestrictedStringProperty; }
            set 
            { 
                _activeRestrictedStringProperty = value;
            }
        }

        /// <summary>
        ///     Returns the remaining available text length for the text control named by
        ///     <see cref="ActiveRestrictedStringProperty">ActiveRestrictedStringProperty</see>.
        /// </summary>
        [Browsable(false)]
        public int ActiveRestrictedStringPropertyLength
        {
            get
            {
                switch (_activeRestrictedStringProperty)
                {
                    case "Tt_Tip":
                        if (_data.C.Tt_Tip == null)
                        {
                            return STRINGSIZE_Tt_Tip;
                        }
                        else
                        {
                            return (STRINGSIZE_Tt_Tip - _data.C.Tt_Tip.Length);
                        }
                        break;
                    }
                return 0;
            }
        }

 

		#endregion Flag Props


		#region Data Props


        /// <summary>
        ///     Called by data field properties in their getter block. This method will determine
        ///     if he <see cref="TypeServices.EntityState">state</see> has changed by the getter
        ///     being called. If it has changed, the <see cref="RaiseEventCurrentEntityStateChanged">RaiseEventCurrentEntityStateChanged</see>
        ///     method will be called to notify the object using this business object.
        /// </summary>
       private void CheckEntityState(EntityStateSwitch es)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", new ValuePair[] {new ValuePair(" _data.S.Switch",  _data.S.Switch), new ValuePair("es", es) }, IfxTraceCategory.Enter);
                if (es != _data.S.Switch)
                {
                    RaiseEventCurrentEntityStateChanged(_data.S.Switch);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CheckEntityState", IfxTraceCategory.Leave);
            }
        }


        /// <summary>
        /// This property does nothing and is used as a stub for a 'Rich Data Grid Column' binding to this entity
        /// </summary>
        public string RichGrid
        {
            get { return ""; }
        }


        private IBusinessObjectV2 _parentBusinessObject = null;

        public IBusinessObjectV2 ParentBusinessObject
        {
            get { return _parentBusinessObject; }
            set { _parentBusinessObject = value; }
        }

       public Guid? GuidPrimaryKey()
       {
           return _data.C.Tt_Id; 
       }

       public long? LongPrimaryKey()
       {
           throw new NotImplementedException("WcToolTip_Bll LongPrimaryKey() Not Implemented"); 
       }

       public int? IntPrimaryKey()
       {
           throw new NotImplementedException("WcToolTip_Bll LongPrimaryKey() Not Implemented"); 
       }

       public short? ShortPrimaryKey()
       {
           throw new NotImplementedException("WcToolTip_Bll LongPrimaryKey() Not Implemented"); 
       }

       public byte? BytePrimaryKey()
       {
           throw new NotImplementedException("WcToolTip_Bll LongPrimaryKey() Not Implemented"); 
       }

       public object ObjectPrimaryKey()
       {
           return _data.C.Tt_Id;
       }

       /// <summary>
       /// 	<para>The Primary Key for WcToolTip</para>
       /// </summary>
        public Guid Tt_Id
        {
            get
            {
                return _data.C.Tt_Id;
            }
        }



        /// <summary>
        /// This is the Standing Foreign Key property. This value remains constant when
        /// calling the new method where all data fields are cleared and set to their ‘new’ default
        /// values. This allows creating new entities for the same parent. When an entity is
        /// fetched from the data store and used as the current entity in this business object, the
        /// Standing Foreign Key value is reset using the value from the fetched entity.
        /// </summary>
        public Guid StandingFK
        {
            get { return _StandingFK; }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    _StandingFK = value;
                    _data.C.Tt_TbC_Id_noevents = _StandingFK;
                    _data.O.Tt_TbC_Id_noevents = _StandingFK;
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "StandingFK - Setter", IfxTraceCategory.Leave);
                }
            }
        }		
        public Guid? Tt_TbC_Id
        {
            get
            {
                return _data.C.Tt_TbC_Id;
            }
        }

		
		/// <summary>
        /// 	<para>
        ///         The Getter returns <see cref="Tt_Sort">Tt_Sort</see> as a string.
        ///     </para>
        /// 	<para>The Setter accepts a string, parses the string and determines if it’s a
        ///     numeric. If it’s numeric, the string is converted to the appropriate number type
        ///     and persisted in the Tt_Sort field. If it’s not numeric, then Tt_Sort is set to
        ///     null.</para>
        /// </summary>
        public String Tt_Sort_asString
        {
            get
            {
                if (null == _data.C.Tt_Sort)
                {
                    return "";
                }
                else
                {
                    return _data.C.Tt_Sort.ToString();
                }
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Sort - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
					if (BLLHelper.IsNumber(value))
                    {
                        try
                        {
							Double newValNotNull;
                            Double.TryParse(Convert.ToString(value), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.CurrentInfo, out newValNotNull);
                            Tt_Sort = (Int32)newValNotNull;
                        }
                        catch (FormatException e)
                        {
                            Tt_Sort = null;
                        }
                    }
                    else
                    {
                        Tt_Sort = null;
                    }
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Sort - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Sort - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Int32? Tt_Sort
        {
            get
            {
                return _data.C.Tt_Sort;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Sort - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tt_Sort == value) { return; }
                    _data.C.Tt_Sort = value;
                    CustomPropertySetterCode("Tt_Sort");
                    Tt_Sort_Validate();
                    CheckEntityState(es);
                    Notify("Tt_Sort");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Sort - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Sort - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public String Tt_Tip
        {
            get
            {
                return _data.C.Tt_Tip;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Tip - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tt_Tip == value) 
                    {
                        return; 
                    }



                    bool oldValIsNothng = (_data.C.Tt_Tip == null || _data.C.Tt_Tip.Length == 0);
                    bool newValIsNothing = (value == null || value.Trim().Length == 0);
                    if (oldValIsNothng == true && newValIsNothing == true)
                    {
                        return;
                    }
                    else if (newValIsNothing == false)
                    {
                        if (_data.C.Tt_Tip == value.Trim()) { return; }
                    }

                    if (value == null)
                    {
                        _data.C.Tt_Tip = null;
                    }
                    else
                    {
                        _data.C.Tt_Tip = value.Trim();
                    }
                    //else if ((_data.C.Tt_Tip == null || _data.C.Tt_Tip.Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //else if (_data.C.Tt_Tip == value.Trim()) { return; }
                    //// If we're just converting from a null to and empty string, or visa-versa, the dont do anything, otherwise state and value changed events will fire.
                    //if ((_data.C.Tt_Tip == null || _data.C.Tt_Tip.Trim().Length == 0) && (value == null || value.Trim().Length == 0))
                    //{
                    //    return;
                    //}
                    //if (value != null)
                    //{
                    //    _data.C.Tt_Tip = value.Trim();
                    //}
                    //else
                    //{
                    //    _data.C.Tt_Tip = value;
                    //}
                    CustomPropertySetterCode("Tt_Tip");
                    Tt_Tip_Validate();
                    CheckEntityState(es);
                    RaiseEventRestrictedTextLengthChanged(_data.O.Tt_Tip, _data.C.Tt_Tip);
                        
					Notify("Tt_Tip");
                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Tip - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_Tip - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tt_IsActiveRow
        {
            get
            {
                return _data.C.Tt_IsActiveRow;
            }
            set
            {
                Guid? traceId = Guid.NewGuid();
                try
                {
                    if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_IsActiveRow - Setter", new ValuePair[] {new ValuePair("value", value) }, IfxTraceCategory.Enter);
                    EntityStateSwitch es = _data.S.Switch;
                    if (_data.C.Tt_IsActiveRow == value) { return; }
                    _data.C.Tt_IsActiveRow = value;
                    CustomPropertySetterCode("Tt_IsActiveRow");
                    Tt_IsActiveRow_Validate();
                    CheckEntityState(es);
                    Notify("Tt_IsActiveRow");

                }
                catch (Exception ex)
                {
                    if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_IsActiveRow - Setter", ex);
				throw IfxWrapperException.GetError(ex);
                }
                finally
                {
                    if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tt_IsActiveRow - Setter", IfxTraceCategory.Leave);
                }
            }
        }

		
        public Boolean Tt_IsDeleted
        {
            get
            {
                return _data.C.Tt_IsDeleted;
            }
        }

		
        public Guid? Tt_CreatedUserId
        {
            get
            {
                return _data.C.Tt_CreatedUserId;
            }
        }


		/// <summary>
        ///  Use this property when binding in Grids so the Notify method won't prevent you from entering complete dates or the year part of a date.  Notify will refresh the UI cutting off anything after the first diget of the month.
        /// </summary>
        public String Tt_CreatedDate_asString
        {
            get
            {
                if (null == _data.C.Tt_CreatedDate)
                {
                    return "";
                }
                else
                {
                    return _data.C.Tt_CreatedDate.ToString();
                }
            }
        }

		
        public DateTime? Tt_CreatedDate
        {
            get
            {
                return _data.C.Tt_CreatedDate;
            }
        }

		
        public Guid? Tt_UserId
        {
            get
            {
                return _data.C.Tt_UserId;
            }
        }


        public String UserName
        {
            get
            {
                return _data.C.UserName;
            }
        }


		/// <summary>
        ///  Use this property when binding in Grids so the Notify method won't prevent you from entering complete dates or the year part of a date.  Notify will refresh the UI cutting off anything after the first diget of the month.
        /// </summary>
        public String Tt_LastModifiedDate_asString
        {
            get
            {
                if (null == _data.C.Tt_LastModifiedDate)
                {
                    return "";
                }
                else
                {
                    return _data.C.Tt_LastModifiedDate.ToString();
                }
            }
        }

		
        public DateTime? Tt_LastModifiedDate
        {
            get
            {
                return _data.C.Tt_LastModifiedDate;
            }
        }


        public Byte[] Tt_Stamp
        {
            get
            {
                return _data.C.Tt_Stamp;
            }
        }



		#endregion Data Props


		#region Concurrency and Data State


        private bool DeterminConcurrencyCheck(UseConcurrencyCheck check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", IfxTraceCategory.Enter);
                switch (check)
                {
                    case UseConcurrencyCheck.UseDefaultSetting:
                        return true;
                    case UseConcurrencyCheck.UseConcurrencyCheck:
                        return true;
                    case UseConcurrencyCheck.BypassConcurrencyCheck:
                        return false;
                }
                return false;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DeterminConcurrencyCheck", IfxTraceCategory.Leave);
            }
        }

        /// <summary>
        ///     This is a reference to the current data (<see cref="EntityWireType.WcToolTip_Values">WcToolTip_Values</see>) for this business object
        ///     (WcToolTip_Bll). The Values Manager (<see cref="EntityWireType.WcToolTip_ValuesMngr">WcToolTip_ValuesMngr</see>) has three sets of WcToolTip
        ///     data used for managing state and concurrency. For more information about the other
        ///     two sets of data, <see cref="Original">Original</see> and <see cref="Concurrency">Concurrency</see>, and how they are used, see the overviews of
        ///     <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Concurrency_Overview.html">
        ///     Concurrency</a> and <a href="D:/nwis/Books/SourceDocumentation/HelpFiles/Build/Default/Wire_Object_Overview.html">
        ///     Wire Objects</a>.
        /// </summary>
        /// <seealso cref="Original">Original Property</seealso>
        /// <seealso cref="Concurrency">Concurrency Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcToolTip_Values Current
        {
            get
            {
                return _data.C;
            }
            set
            {
                //Management Code
                _data.C = value;
            }
        }

        /// <summary>
        ///     This is a reference to the original data (<see cref="EntityWireType.WcToolTip_Values">WcToolTip_Values</see>) for this business object
        ///     (WcToolTip_Bll) which was either data for a ‘new-not dirty’ WcToolTip or data for a WcToolTip
        ///     retrieved from the data store. The Values Manager (<see cref="EntityWireType.WcToolTip_ValuesMngr">WcToolTip_ValuesMngr</see>) has three sets of WcToolTip
        ///     data used for managing state and concurrency. For more information about the other
        ///     two sets of data, <see cref="Current">Current</see> and <see cref="Concurrency">Concurrency</see>, and how they are used, see the overview of
        ///     Concurrency and Wire Objects.
        /// </summary>
        /// <seealso cref="Current">Current Property</seealso>
        /// <seealso cref="Concurrency">Concurrency Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcToolTip_Values Original
        {
            get
            {
                return _data.O;
            }
            set
            {
                //Management Code
                _data.O = value;
            }
        }

        /// <summary>*** Need to review before documenting ***</summary>
        /// <seealso cref="Current">Current Property</seealso>
        /// <seealso cref="Original">Original Property</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Concurrency_Overview.html" cat="Architecture and How It’s Used">Concurrency Overview</seealso>
        /// <seealso cref="!:D:\nwis\Books\SourceDocumentation\HelpFiles\Build\Default\Wire_Object_Overview.html" cat="Architecture and How It’s Used">Wire Object / Values Manager Overview</seealso>
        [Browsable(false)]
        public WcToolTip_Values Concurrency
        {
            get
            {
                return _data.X;
            }
            set
            {
                //Management Code
                _data.X = value;
            }
        }

        /// <value>
        ///     This class manages the <see cref="TypeServices.EntityState">EntityStateSwitch</see>
        ///     and is used to describe the current <see cref="TypeServices.EntityState">state</see> of this business object such as IsDirty,
        ///     IsValid and IsNew. The combinations of these six possible values describe the
        ///     entity state and are used for logic in configuring settings in the business object
        ///     as well as the UI.
        /// </value>
        /// <seealso cref="EntityWireType.WcToolTip_ValuesMngr">WcToolTip_ValuesMngr Class</seealso>
        [Browsable(false)]
        public override EntityState State
        {
            get
            {
                return _data.S;
            }
            set
            {
                //Management Code
                _data.S = value;
            }
        }

        /// <summary>
        ///     This is the combination of IsDirty, IsValid and IsNew and is managed by the
        ///     <see cref="TypeServices.EntityState">EntityState</see> class. The combinations of
        ///     these six possible values describe the entity <see cref="TypeServices.EntityState">state</see> and are used for logic in configuring
        ///     settings in the business object as well as the UI.
        /// </summary>
        /// <seealso cref="EntityWireType.WcToolTip_ValuesMngr">WcToolTip_ValuesMngr Class</seealso>
        public EntityStateSwitch StateSwitch
        {
            get
            {
                return _data.S.Switch;
            }
        }




		#endregion Concurrency and Data State

        [Browsable(false)]
        public override object this[string name]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                //Management Code
                throw new NotImplementedException();
            }
        }


		#region ITraceItem Members


        /// <summary>
        /// 	<para>
        ///         Used in IFX (the instrumentation framework) for tracing. This returns the short
        ///         version of the <see cref="ManagementServices.TraceItemList">TraceItemList</see>. The ‘Short Version’
        ///         returns only the entity’s data fields, <see cref="TypeServices.BrokenRuleManager.GetBrokenRuleListForEntity">BrokenRules</see>
        ///         and <see cref="TypeServices.EntityStateSwitch">EntityStateSwitch</see>.
        ///         However, it also calls the <see cref="GetTraceItemsShortListCustom">GetTraceItemsShortListCustom</see> method in the
        ///         WcToolTip.bll.cust.cs code file so a developer can customize the list by adding
        ///         additional items to it.
        ///     </para>
        /// 	<para>
        ///         Keep in mind that this is the short versions so don’t get carried away adding
        ///         to many things to it. For a robust and extensive list of items to record in the
        ///         trace, use the <see cref="GetTraceItemsLongList">GetTraceItemsLongList</see>
        ///         and <see cref="GetTraceItemsLongListCustom">GetTraceItemsLongListCustom</see>
        ///         methods.
        ///     </para>
        /// </summary>
        public TraceItemList GetTraceItemsShortList()
        {
            if (null == _traceItems)
            {
                _traceItems = new TraceItemList("WcToolTip_Bll");
            }
            else
            {
                _traceItems.Clear();
            }
            _traceItems.Add("StateSwitch", StateSwitch.ToString(), TraceDataTypes.String);
            _traceItems.Add("Broken Rules", BrokenRuleManagerProperty.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tt_Id", _data.C.Tt_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tt_TbC_Id", _data.C.Tt_TbC_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tt_Sort", _data.C.Tt_Sort.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("Tt_Tip", _data.C.Tt_Tip.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tt_IsActiveRow", _data.C.Tt_IsActiveRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tt_IsDeleted", _data.C.Tt_IsDeleted.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tt_CreatedUserId", _data.C.Tt_CreatedUserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tt_CreatedDate", _data.C.Tt_CreatedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("Tt_UserId", _data.C.Tt_UserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("UserName", _data.C.UserName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tt_LastModifiedDate", _data.C.Tt_LastModifiedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("Tt_Stamp", _data.C.Tt_Stamp.ToString(), TraceDataTypes.ByteArray);
			GetTraceItemsShortListCustom();
            return _traceItems;
        }

        /// <summary>
        /// 	<para>
        ///         Used in IFX (the instrumentation framework) for tracing. By default, this is
        ///         code-genned to return the same information as <see cref="GetTraceItemsShortList">GetTraceItemsShortList</see>, but is intended for
        ///         developers to add additional informaiton that would be helpful in a trace. This
        ///         method also calls the <see cref="GetTraceItemsLongListCustom">GetTraceItemsLongListCustom</see> method in the
        ///         WcToolTip.bll.cust.cs code file so a developer can customize the list by adding
        ///         additional items to it.
        ///     </para>
        /// 	<para>
        ///         Remember: <see cref="GetTraceItemsShortList">GetTraceItemsShortList</see> is
        ///         intended for a limited list of items (the basic items) to trace and
        ///         <strong>GetTraceItemsLongList</strong> is intended for a robust and extensive
        ///         list of items to record in the trace.
        ///     </para>
        /// </summary>
        public TraceItemList GetTraceItemsLongList()
        {
            if (null == _traceItems)
            {
                _traceItems = new TraceItemList("WcToolTip_Bll");
            }
            else
            {
                _traceItems.Clear();
            }
            _traceItems.Add("StateSwitch", StateSwitch.ToString(), TraceDataTypes.String);
            _traceItems.Add("Broken Rules", BrokenRuleManagerProperty.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tt_Id", _data.C.Tt_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tt_TbC_Id", _data.C.Tt_TbC_Id.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tt_Sort", _data.C.Tt_Sort.ToString(), TraceDataTypes.Int32);
			_traceItems.Add("Tt_Tip", _data.C.Tt_Tip.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tt_IsActiveRow", _data.C.Tt_IsActiveRow.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tt_IsDeleted", _data.C.Tt_IsDeleted.ToString(), TraceDataTypes.Boolean);
			_traceItems.Add("Tt_CreatedUserId", _data.C.Tt_CreatedUserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("Tt_CreatedDate", _data.C.Tt_CreatedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("Tt_UserId", _data.C.Tt_UserId.ToString(), TraceDataTypes.Guid);
			_traceItems.Add("UserName", _data.C.UserName.ToString(), TraceDataTypes.String);
			_traceItems.Add("Tt_LastModifiedDate", _data.C.Tt_LastModifiedDate.ToString(), TraceDataTypes.DateTime);
			_traceItems.Add("Tt_Stamp", _data.C.Tt_Stamp.ToString(), TraceDataTypes.ByteArray);
			GetTraceItemsLongListCustom();
            return _traceItems;
        }

        public object[] GetTraceData()
        {
            object[] data = new object[11];
            data[0] = new object[] { "StateSwitch", StateSwitch.ToString() };
            data[1] = new object[] { "BrokenRuleManagerProperty", BrokenRuleManagerProperty.ToString() };
            data[2] = new object[] { "Tt_Id", _data.C.Tt_Id.ToString() };
            data[3] = new object[] { "Tt_TbC_Id", _data.C.Tt_TbC_Id.ToString() };
            data[4] = new object[] { "Tt_Sort", _data.C.Tt_Sort == null ? "<Null>" : _data.C.Tt_Sort.ToString() };
            data[5] = new object[] { "Tt_Tip", _data.C.Tt_Tip == null ? "<Null>" : _data.C.Tt_Tip.ToString() };
            data[6] = new object[] { "Tt_IsActiveRow", _data.C.Tt_IsActiveRow.ToString() };
            data[7] = new object[] { "Tt_IsDeleted", _data.C.Tt_IsDeleted.ToString() };
            data[8] = new object[] { "Tt_CreatedUserId", _data.C.Tt_CreatedUserId == null ? "<Null>" : _data.C.Tt_CreatedUserId.ToString() };
            data[9] = new object[] { "Tt_CreatedDate", _data.C.Tt_CreatedDate == null ? "<Null>" : _data.C.Tt_CreatedDate.ToString() };
            data[10] = new object[] { "Tt_UserId", _data.C.Tt_UserId == null ? "<Null>" : _data.C.Tt_UserId.ToString() };
            data[11] = new object[] { "UserName", _data.C.UserName == null ? "<Null>" : _data.C.UserName.ToString() };
            data[12] = new object[] { "Tt_LastModifiedDate", _data.C.Tt_LastModifiedDate == null ? "<Null>" : _data.C.Tt_LastModifiedDate.ToString() };
            return data;
        }


		#endregion ITraceItem Members


		#region INotifyPropertyChanged Members

        /// <summary>
        /// 	<para>
        ///         This method raises the <see cref="PropertyChanged">PropertyChanged</see> event
        ///         and is required by the INotifyPropertyChanged interface. It’s called in nearly
        ///         all public data field properties.
        ///     </para>
        /// 	<para>The <strong>INotifyPropertyChanged</strong> interface is used to notify
        ///     clients, typically binding clients, that a property value has changed. For more
        ///     information, search INotifyPropertyChanged on <strong>MSDN</strong>.</para>
        /// </summary>
        protected void Notify(string propName)
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }


		#endregion INotifyPropertyChanged Members


		#region INotifyDataErrorInfo Members

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        //public void RaiseErrorsChanged(string propertyName)
        //{
        //    if (ErrorsChanged != null)
        //        ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        //}

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            //throw new NotImplementedException();
            if (_brokenRuleManager.GetBrokenRulesForProperty(propertyName).Count == 0)
            {
                return "";
            }
            else
            {

                return _brokenRuleManager.GetBrokenRulesForProperty(propertyName)[0].ToString();
            }
        }

        public bool HasErrors
        {
            get { return _brokenRuleManager.IsEntityValid(); }
        }


		#endregion INotifyDataErrorInfo Members


        #region List Methods

        #region IEditableObject Members

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="CancelEdit">CancelEdit</see> and <see cref="EndEdit">EndEdit</see>.
        ///     </para>
        /// 	<para>Begins an edit on an object.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void BeginEdit()
        {
//            Guid? traceId = Guid.NewGuid();
//            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "BeginEdit", IfxTraceCategory.Enter);
//            Console.WriteLine("BeginEdit");
//            //***  ToDo:
//            // Set the parent type below, then when properties are being edited, raise an event to sync fields in the props control.
//            _parentType = ParentEditObjectType.EntitiyListControl;
//
//            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "BeginEdit", IfxTraceCategory.Leave);
        }

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="BeginEdit">BeginEdit</see> and <see cref="EndEdit">EndEdit</see>.
        ///     </para>
        /// 	<para>Discards changes since the last BeginEdit call.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void CancelEdit()
        {
            Guid? traceId = Guid.NewGuid();
            if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelEdit", IfxTraceCategory.Enter);
            UnDo();
            _parentType = ParentEditObjectType.None;

            if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CancelEdit", IfxTraceCategory.Leave);
        }

        /// <summary>
        /// 	<para>
        ///         Is a member of the <strong>IEditableObject</strong> Interface along with
        ///         <see cref="BeginEdit">BeginEdit</see> and <see cref="CancelEdit">CancelEdit</see>.
        ///     </para>
        /// 	<para>Pushes changes since the last BeginEdit or IBindingList.AddNew call into the
        ///     underlying object.</para>
        /// 	<para>For more information, search <strong>IEditableObject</strong> on
        ///     <strong>MSDN</strong>.</para>
        /// </summary>
        public void EndEdit()
        {
//            Guid? traceId = Guid.NewGuid();
//            try
//            {
//                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", IfxTraceCategory.Enter);
//                if (_data.S.IsDirty() && _data.S.IsValid())
//                {
//                    // ToDo - This needs more testing for Silverlight
//                    Save(null, ParentEditObjectType.EntitiyListControl, UseConcurrencyCheck.UseDefaultSetting);
//
//                    //DataServiceInsertUpdateResponseClientSide result = Save(UseConcurrencyCheck.UseDefaultSetting);
//                    //if (result.Result != DataOperationResult.Success)
//                    //{
//                    //    UnDo();
//                    //    Debugger.Break();
//                    //}
//                }
//            }
//            catch (Exception ex)
//            {
//                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", ex);
				   // throw IfxWrapperException.GetError(ex);
//            }
//            finally
//            {
//                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "EndEdit", IfxTraceCategory.Leave);
//            }
        }

        #endregion IEditableObject Members

        #endregion List Methods



//        #region IDataErrorInfo Members
//
//        /// <summary>
//        /// 	<para>Part of the <strong>IDataErrorInfo</strong> Interface. Gets an error message
//        ///     indicating what is wrong with this object.</para>
//        /// 	<para>For more information, search <strong>IDataErrorInfo</strong> on
//        ///     <strong>MSDN</strong>.</para>
//        /// </summary>
//        public string Error
//        {
//            get { throw new NotImplementedException(); }
//        }
//
//        /// <summary>
//        /// 	<para>Part of the <strong>IDataErrorInfo</strong> Interface. Gets the error message
//        ///     for the property with the given name.</para>
//        /// 	<para>For more information, search <strong>IDataErrorInfo</strong> on
//        ///     <strong>MSDN</strong>.</para>
//        /// </summary>
//        string IDataErrorInfo.this[string columnName]
//        {
//            get
//            {
//                if (!_brokenRuleManager.IsPropertyValid(columnName))
//                {
//                    //string err = "line 1 First validation msg" + Environment.NewLine + "line 2 Second validation msg";
//                    //return err;
//                    return _brokenRuleManager.GetBrokenRulesForProperty(columnName)[0].ToString();
//                }
//                else
//                {
//                    return null;
//                }
//            }
//        }
//
//        #endregion



    }

}



