using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;

// Gen Timestamp:  1/3/2017 11:03:06 AM

namespace EntityBll.SL
{

    public partial class WcApplicationVersion_Bll_staticLists
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "WcApplicationVersion_Bll_staticLists";



        private static ComboItemList _wcStoredProc_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _wcStoredProc_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList_HasItems = true;

        static bool isDataLoaded = false;
        static ProxyWrapper.WcApplicationVersionService_ProxyWrapper _staticWcApplicationVersionProxy = null;

        #endregion Initialize Variables


        #region Load Data

        public static void InitializeProxyWrapper()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Enter);

                if (_staticWcApplicationVersionProxy == null)
                {
                    _staticWcApplicationVersionProxy = new ProxyWrapper.WcApplicationVersionService_ProxyWrapper();
                    _staticWcApplicationVersionProxy.GetWcApplicationVersion_ReadOnlyStaticListsCompleted += new EventHandler<GetWcApplicationVersion_ReadOnlyStaticListsCompletedEventArgs>(GetWcApplicationVersion_ReadOnlyStaticListsCompleted);
                    _staticWcApplicationVersionProxy.GetWcStoredProc_ComboItemListCompleted += new EventHandler<GetWcStoredProc_ComboItemListCompletedEventArgs>(GetWcStoredProc_ComboItemListCompleted);
                    _staticWcApplicationVersionProxy.GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted += new EventHandler<GetWcTableColumn_lstByApVersion_Id_ComboItemListCompletedEventArgs>(GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Leave);
            }
        }

        public static void LoadStaticLists(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Enter);

                if (_staticWcApplicationVersionProxy == null)
                {
                    InitializeProxyWrapper();
                }

                _staticWcApplicationVersionProxy.Begin_GetWcApplicationVersion_ReadOnlyStaticLists(ApVrsn_Id );

                LoadStaticLists_Custom();
                isDataLoaded = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Leave);
            }
        }

        static void GetWcApplicationVersion_ReadOnlyStaticListsCompleted(object sender, GetWcApplicationVersion_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcApplicationVersion_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                if (data != null)
                {


                    // WcStoredProc_ComboItemList
                    _wcStoredProc_ComboItemList_BindingList.IsRefreshingData = true;
                    _wcStoredProc_ComboItemList_BindingList.CachedList.Clear();
                    _wcStoredProc_ComboItemList_BindingList.Clear();
                    _wcStoredProc_ComboItemList_BindingList.ReplaceList((object[])data[0]);

                    _wcStoredProc_ComboItemList_BindingList.IsRefreshingData = false;

                    // WcTableColumn_lstByApVersion_Id_ComboItemList
                    _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList.IsRefreshingData = true;
                    _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList.CachedList.Clear();
                    _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList.Clear();
                    _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList.ReplaceList((object[])data[1]);

                    _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList.IsRefreshingData = false;
                    isDataLoaded = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcApplicationVersion_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcApplicationVersion_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Load Data


        #region Properties

        public static bool IsDataLoaded
        {
            get { return isDataLoaded; }
        }

                    // WcStoredProc_ComboItemList
        public static ComboItemList WcStoredProc_ComboItemList_BindingListProperty
        {
            get
            {
                return _wcStoredProc_ComboItemList_BindingList;
            }
            set
            {
                _wcStoredProc_ComboItemList_BindingList = value;
            }
        }

                    // WcTableColumn_lstByApVersion_Id_ComboItemList
        public static ComboItemList WcTableColumn_lstByApVersion_Id_ComboItemList_BindingListProperty
        {
            get
            {
                return _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList;
            }
            set
            {
                _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList = value;
            }
        }

        #endregion Properties


        #region RefreshLists



        #region WcStoredProc_ComboItemList

        public static void Refresh_WcStoredProc_ComboItemList_BindingList(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcStoredProc_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticWcApplicationVersionProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticWcApplicationVersionProxy.Begin_GetWcStoredProc_ComboItemList(ApVrsn_Id );

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcStoredProc_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcStoredProc_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetWcStoredProc_ComboItemListCompleted(object sender, GetWcStoredProc_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _wcStoredProc_ComboItemList_BindingList.IsRefreshingData = true;
                _wcStoredProc_ComboItemList_BindingList.ReplaceList(data);
                _wcStoredProc_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProc_ComboItemList



        #region WcTableColumn_lstByApVersion_Id_ComboItemList

        public static void Refresh_WcTableColumn_lstByApVersion_Id_ComboItemList_BindingList(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcTableColumn_lstByApVersion_Id_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticWcApplicationVersionProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticWcApplicationVersionProxy.Begin_GetWcTableColumn_lstByApVersion_Id_ComboItemList(ApVrsn_Id );

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcTableColumn_lstByApVersion_Id_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_WcTableColumn_lstByApVersion_Id_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted(object sender, GetWcTableColumn_lstByApVersion_Id_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList.IsRefreshingData = true;
                _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList.ReplaceList(data);
                _wcTableColumn_lstByApVersion_Id_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByApVersion_Id_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_lstByApVersion_Id_ComboItemList



        #endregion RefreshLists


    }

    #region Classes for Binding

#endregion Classes for Binding

}

