using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  4/19/2012 9:43:00 PM

namespace EntityBll.SL
{
    public partial class Project_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_Prj_Name = 50;
		private const string BROKENRULE_Prj_Id_Required = "Id is a required field.";
		private string BROKENRULE_Prj_Name_TextLength = "Name has a maximum text length of  '" + STRINGSIZE_Prj_Name + "'.";
		private const string BROKENRULE_Prj_Name_Required = "Name is a required field.";
		private const string BROKENRULE_Prj_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_PpCat1_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("Prj_Id", BROKENRULE_Prj_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Prj_Name", BROKENRULE_Prj_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Prj_Name", BROKENRULE_Prj_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Prj_IsActiveRow", BROKENRULE_Prj_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("PpCat1_Stamp", BROKENRULE_PpCat1_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void Prj_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Prj_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Prj_Id");
                string newBrokenRules = "";
                
                if (Prj_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Prj_Id", BROKENRULE_Prj_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Prj_Id", BROKENRULE_Prj_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Prj_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Prj_Id", _brokenRuleManager.IsPropertyValid("Prj_Id"), IsPropertyDirty("Prj_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Prj_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Prj_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Prj_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Prj_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Prj_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (Prj_Name != null)
                {
                    len = Prj_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Prj_Name", BROKENRULE_Prj_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Prj_Name", BROKENRULE_Prj_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Prj_Name", BROKENRULE_Prj_Name_Required);

                    if (len > STRINGSIZE_Prj_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Prj_Name", BROKENRULE_Prj_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Prj_Name", BROKENRULE_Prj_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Prj_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Prj_Name", _brokenRuleManager.IsPropertyValid("Prj_Name"), IsPropertyDirty("Prj_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Prj_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Prj_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Prj_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Prj_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Prj_IsActiveRow");
                string newBrokenRules = "";
                
                if (Prj_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Prj_IsActiveRow", BROKENRULE_Prj_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Prj_IsActiveRow", BROKENRULE_Prj_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Prj_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Prj_IsActiveRow", _brokenRuleManager.IsPropertyValid("Prj_IsActiveRow"), IsPropertyDirty("Prj_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Prj_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Prj_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void PpCat1_UserId_Validate()
        {
        }

        private void PpCat1_CreatedDate_Validate()
        {
        }

        private void PpCat1_LastModifiedDate_Validate()
        {
        }

        private void PpCat1_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PpCat1_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PpCat1_Stamp");
                string newBrokenRules = "";
                
                if (PpCat1_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("PpCat1_Stamp", BROKENRULE_PpCat1_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("PpCat1_Stamp", BROKENRULE_PpCat1_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("PpCat1_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("PpCat1_Stamp", _brokenRuleManager.IsPropertyValid("PpCat1_Stamp"), IsPropertyDirty("PpCat1_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PpCat1_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PpCat1_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


