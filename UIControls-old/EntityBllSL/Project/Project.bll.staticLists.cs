using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;

// Gen Timestamp:  2/3/2015 8:58:23 PM

namespace EntityBll.SL
{

    public partial class Project_Bll_staticLists
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "Project_Bll_staticLists";



        private static ComboItemList _person_lstByProject_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _person_lstByProject_ComboItemList_BindingList_HasItems = true;

        static bool isDataLoaded = false;
        static ProxyWrapper.ProjectService_ProxyWrapper _staticProjectProxy = null;

        #endregion Initialize Variables


        #region Load Data

        public static void InitializeProxyWrapper()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Enter);

                if (_staticProjectProxy == null)
                {
                    _staticProjectProxy = new ProxyWrapper.ProjectService_ProxyWrapper();
                    _staticProjectProxy.GetProject_ReadOnlyStaticListsCompleted += new EventHandler<GetProject_ReadOnlyStaticListsCompletedEventArgs>(GetProject_ReadOnlyStaticListsCompleted);
                    _staticProjectProxy.GetPerson_lstByProject_ComboItemListCompleted += new EventHandler<GetPerson_lstByProject_ComboItemListCompletedEventArgs>(GetPerson_lstByProject_ComboItemListCompleted);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Leave);
            }
        }

        public static void LoadStaticLists(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Enter);

                if (_staticProjectProxy == null)
                {
                    InitializeProxyWrapper();
                }

                _staticProjectProxy.Begin_GetProject_ReadOnlyStaticLists(Prj_Id );

                LoadStaticLists_Custom();
                isDataLoaded = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Leave);
            }
        }


        #endregion Load Data


        #region Properties

        public static bool IsDataLoaded
        {
            get { return isDataLoaded; }
        }

                    // Person_lstByProject_ComboItemList
        public static ComboItemList Person_lstByProject_ComboItemList_BindingListProperty
        {
            get
            {
                return _person_lstByProject_ComboItemList_BindingList;
            }
            set
            {
                _person_lstByProject_ComboItemList_BindingList = value;
            }
        }

        #endregion Properties


        #region RefreshLists



        #region Person_lstByProject_ComboItemList

        public static void Refresh_Person_lstByProject_ComboItemList_BindingList(Guid Prj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_Person_lstByProject_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticProjectProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticProjectProxy.Begin_GetPerson_lstByProject_ComboItemList(Prj_Id );

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_Person_lstByProject_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_Person_lstByProject_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetPerson_lstByProject_ComboItemListCompleted(object sender, GetPerson_lstByProject_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstByProject_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _person_lstByProject_ComboItemList_BindingList.IsRefreshingData = true;
                _person_lstByProject_ComboItemList_BindingList.ReplaceList(data);
                _person_lstByProject_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstByProject_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstByProject_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Person_lstByProject_ComboItemList



        #endregion RefreshLists


    }

    #region Classes for Binding

#endregion Classes for Binding

}

