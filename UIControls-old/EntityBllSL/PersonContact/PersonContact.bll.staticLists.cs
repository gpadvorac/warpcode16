using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;

// Gen Timestamp:  5/7/2015 5:06:18 PM

namespace EntityBll.SL
{

    public partial class PersonContact_Bll_staticLists
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "PersonContact_Bll_staticLists";



        private static ComboItemList _person_LU_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _person_LU_ComboItemList_BindingList_HasItems = true;

        static bool isDataLoaded = false;
        static ProxyWrapper.PersonContactService_ProxyWrapper _staticPersonContactProxy = null;

        #endregion Initialize Variables


        #region Load Data

        public static void InitializeProxyWrapper()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Enter);

                if (_staticPersonContactProxy == null)
                {
                    _staticPersonContactProxy = new ProxyWrapper.PersonContactService_ProxyWrapper();
                    _staticPersonContactProxy.GetPersonContact_ReadOnlyStaticListsCompleted += new EventHandler<GetPersonContact_ReadOnlyStaticListsCompletedEventArgs>(GetPersonContact_ReadOnlyStaticListsCompleted);
                    _staticPersonContactProxy.GetPerson_LU_ComboItemListCompleted += new EventHandler<GetPerson_LU_ComboItemListCompletedEventArgs>(GetPerson_LU_ComboItemListCompleted);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Leave);
            }
        }

        public static void LoadStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Enter);

                if (_staticPersonContactProxy == null)
                {
                    InitializeProxyWrapper();
                }

                _staticPersonContactProxy.Begin_GetPersonContact_ReadOnlyStaticLists();

                LoadStaticLists_Custom();
                isDataLoaded = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Leave);
            }
        }


        #endregion Load Data


        #region Properties

        public static bool IsDataLoaded
        {
            get { return isDataLoaded; }
        }

                    // Person_LU_ComboItemList
        public static ComboItemList Person_LU_ComboItemList_BindingListProperty
        {
            get
            {
                return _person_LU_ComboItemList_BindingList;
            }
            set
            {
                _person_LU_ComboItemList_BindingList = value;
            }
        }

        #endregion Properties


        #region RefreshLists



        #region Person_LU_ComboItemList

        public static void Refresh_Person_LU_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_Person_LU_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticPersonContactProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticPersonContactProxy.Begin_GetPerson_LU_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_Person_LU_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_Person_LU_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetPerson_LU_ComboItemListCompleted(object sender, GetPerson_LU_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_LU_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _person_LU_ComboItemList_BindingList.IsRefreshingData = true;
                _person_LU_ComboItemList_BindingList.ReplaceList(data);
                _person_LU_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_LU_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_LU_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Person_LU_ComboItemList



        #endregion RefreshLists


    }

    #region Classes for Binding

#endregion Classes for Binding

}

