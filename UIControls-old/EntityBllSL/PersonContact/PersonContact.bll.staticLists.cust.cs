using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;


// Gen Timestamp:  5/7/2015 2:14:47 PM

namespace EntityBll.SL
{

    public partial class PersonContact_Bll_staticLists
    {

        #region Initialize Variables



        #endregion Initialize Variables




        public static void LoadStaticLists_Custom()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", IfxTraceCategory.Enter);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", IfxTraceCategory.Leave);
            }
        }


        static void GetPersonContact_ReadOnlyStaticListsCompleted(object sender, GetPersonContact_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPersonContact_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                if (data != null)
                {
                    GetPersonContact_ReadOnlyStaticListsCompleted(data);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPersonContact_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPersonContact_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }



        public static void GetPersonContact_ReadOnlyStaticListsCompleted(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPersonContact_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                if (data != null)
                {
                    // Person_LU_ComboItemList
                    _person_LU_ComboItemList_BindingList.IsRefreshingData = true;
                    _person_LU_ComboItemList_BindingList.CachedList.Clear();
                    _person_LU_ComboItemList_BindingList.Clear();
                    _person_LU_ComboItemList_BindingList.ReplaceList((object[])data[0]);

                    _person_LU_ComboItemList_BindingList.IsRefreshingData = false;
                    isDataLoaded = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPersonContact_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPersonContact_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }



    }

}


