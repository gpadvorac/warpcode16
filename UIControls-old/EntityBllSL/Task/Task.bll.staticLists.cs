using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;

// Gen Timestamp:  1/4/2015 12:10:48 AM

namespace EntityBll.SL
{

    public partial class Task_Bll_staticLists
    {


        #region Initialize Variables

        /// <summary>
        /// A hard coded variable to holding the namespace for this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _as = "UIControls";
        /// <summary>
        /// A hard coded variable to holding the class name of this control.  This is passed as a parameter into
        /// exception handling and tracing code.
        /// </summary>
        private static string _cn = "Task_Bll_staticLists";



        private static ComboItemList _taskCategory_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.GuidType);
        private static bool _taskCategory_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _taskPriority_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _taskPriority_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _taskStatus_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _taskStatus_ComboItemList_BindingList_HasItems = true;

        private static ComboItemList _taskType_ComboItemList_BindingList = new ComboItemList(ComboItemList.DataType.IntegerType);
        private static bool _taskType_ComboItemList_BindingList_HasItems = true;

        static bool isDataLoaded = false;
        static ProxyWrapper.TaskService_ProxyWrapper _staticTaskProxy = null;

        #endregion Initialize Variables


        #region Load Data

        public static void InitializeProxyWrapper()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Enter);

                if (_staticTaskProxy == null)
                {
                    _staticTaskProxy = new ProxyWrapper.TaskService_ProxyWrapper();
                    _staticTaskProxy.GetTask_ReadOnlyStaticListsCompleted += new EventHandler<GetTask_ReadOnlyStaticListsCompletedEventArgs>(GetTask_ReadOnlyStaticListsCompleted);
                    _staticTaskProxy.GetTaskCategory_ComboItemListCompleted += new EventHandler<GetTaskCategory_ComboItemListCompletedEventArgs>(GetTaskCategory_ComboItemListCompleted);
                    _staticTaskProxy.GetTaskPriority_ComboItemListCompleted += new EventHandler<GetTaskPriority_ComboItemListCompletedEventArgs>(GetTaskPriority_ComboItemListCompleted);
                    _staticTaskProxy.GetTaskStatus_ComboItemListCompleted += new EventHandler<GetTaskStatus_ComboItemListCompletedEventArgs>(GetTaskStatus_ComboItemListCompleted);
                    _staticTaskProxy.GetTaskType_ComboItemListCompleted += new EventHandler<GetTaskType_ComboItemListCompletedEventArgs>(GetTaskType_ComboItemListCompleted);

                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "InitializeProxyWrapper", IfxTraceCategory.Leave);
            }
        }

        public static void LoadStaticLists()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Enter);

                if (_staticTaskProxy == null)
                {
                    InitializeProxyWrapper();
                }

                _staticTaskProxy.Begin_GetTask_ReadOnlyStaticLists();

                LoadStaticLists_Custom();
                isDataLoaded = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists", IfxTraceCategory.Leave);
            }
        }

        #endregion Load Data


        #region Properties

        public static bool IsDataLoaded
        {
            get { return isDataLoaded; }
        }

                    // TaskCategory_ComboItemList
        public static ComboItemList TaskCategory_ComboItemList_BindingListProperty
        {
            get
            {
                return _taskCategory_ComboItemList_BindingList;
            }
            set
            {
                _taskCategory_ComboItemList_BindingList = value;
            }
        }

                    // TaskPriority_ComboItemList
        public static ComboItemList TaskPriority_ComboItemList_BindingListProperty
        {
            get
            {
                return _taskPriority_ComboItemList_BindingList;
            }
            set
            {
                _taskPriority_ComboItemList_BindingList = value;
            }
        }

                    // TaskStatus_ComboItemList
        public static ComboItemList TaskStatus_ComboItemList_BindingListProperty
        {
            get
            {
                return _taskStatus_ComboItemList_BindingList;
            }
            set
            {
                _taskStatus_ComboItemList_BindingList = value;
            }
        }

                    // TaskType_ComboItemList
        public static ComboItemList TaskType_ComboItemList_BindingListProperty
        {
            get
            {
                return _taskType_ComboItemList_BindingList;
            }
            set
            {
                _taskType_ComboItemList_BindingList = value;
            }
        }

        #endregion Properties


        #region RefreshLists



        #region TaskCategory_ComboItemList

        public static void Refresh_TaskCategory_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskCategory_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticTaskProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticTaskProxy.Begin_GetTaskCategory_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskCategory_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskCategory_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetTaskCategory_ComboItemListCompleted(object sender, GetTaskCategory_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskCategory_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _taskCategory_ComboItemList_BindingList.IsRefreshingData = true;
                _taskCategory_ComboItemList_BindingList.ReplaceList(data);
                _taskCategory_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskCategory_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskCategory_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TaskCategory_ComboItemList



        #region TaskPriority_ComboItemList

        public static void Refresh_TaskPriority_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskPriority_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticTaskProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticTaskProxy.Begin_GetTaskPriority_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskPriority_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskPriority_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetTaskPriority_ComboItemListCompleted(object sender, GetTaskPriority_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskPriority_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _taskPriority_ComboItemList_BindingList.IsRefreshingData = true;
                _taskPriority_ComboItemList_BindingList.ReplaceList(data);
                _taskPriority_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskPriority_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskPriority_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TaskPriority_ComboItemList



        #region TaskStatus_ComboItemList

        public static void Refresh_TaskStatus_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskStatus_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticTaskProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticTaskProxy.Begin_GetTaskStatus_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskStatus_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskStatus_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetTaskStatus_ComboItemListCompleted(object sender, GetTaskStatus_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _taskStatus_ComboItemList_BindingList.IsRefreshingData = true;
                _taskStatus_ComboItemList_BindingList.ReplaceList(data);
                _taskStatus_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TaskStatus_ComboItemList



        #region TaskType_ComboItemList

        public static void Refresh_TaskType_ComboItemList_BindingList()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskType_ComboItemList_BindingList", IfxTraceCategory.Enter);

                if ( _staticTaskProxy == null)
                {
                    InitializeProxyWrapper();
                }
                 _staticTaskProxy.Begin_GetTaskType_ComboItemList();

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskType_ComboItemList_BindingList", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Refresh_TaskType_ComboItemList_BindingList", IfxTraceCategory.Leave);
            }
        }

        static void GetTaskType_ComboItemListCompleted(object sender, GetTaskType_ComboItemListCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskType_ComboItemListCompleted", IfxTraceCategory.Enter);

                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                _taskType_ComboItemList_BindingList.IsRefreshingData = true;
                _taskType_ComboItemList_BindingList.ReplaceList(data);
                _taskType_ComboItemList_BindingList.IsRefreshingData = false;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskType_ComboItemListCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskType_ComboItemListCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion TaskType_ComboItemList



        #endregion RefreshLists


    }

    #region Classes for Binding

#endregion Classes for Binding

}

