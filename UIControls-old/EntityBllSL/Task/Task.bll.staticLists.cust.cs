using System;
using System.Collections.ObjectModel;
using EntityWireTypeSL;
using System.Diagnostics;
using Ifx.SL;
using vComboDataTypes;
using vUICommon;


// Gen Timestamp:  1/4/2015 12:04:02 AM

namespace EntityBll.SL
{

    public partial class Task_Bll_staticLists
    {

        #region Initialize Variables



        #endregion Initialize Variables




        public static void LoadStaticLists_Custom()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", IfxTraceCategory.Enter);


            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", ex);
                throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "LoadStaticLists_Custom", IfxTraceCategory.Leave);
            }
        }




        static void GetTask_ReadOnlyStaticListsCompleted(object sender, GetTask_ReadOnlyStaticListsCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                byte[] array = e.Result;
                object[] data = Serialization.SilverlightSerializer.Deserialize(array) as object[];
                if (data != null)
                {
                    GetTask_ReadOnlyStaticListsCompleted(data);

                    //// TaskCategory_ComboItemList
                    //_taskCategory_ComboItemList_BindingList.IsRefreshingData = true;
                    //_taskCategory_ComboItemList_BindingList.CachedList.Clear();
                    //_taskCategory_ComboItemList_BindingList.Clear();
                    //_taskCategory_ComboItemList_BindingList.ReplaceList((object[])data[0]);

                    //_taskCategory_ComboItemList_BindingList.IsRefreshingData = false;

                    //// TaskPriority_ComboItemList
                    //_taskPriority_ComboItemList_BindingList.IsRefreshingData = true;
                    //_taskPriority_ComboItemList_BindingList.CachedList.Clear();
                    //_taskPriority_ComboItemList_BindingList.Clear();
                    //_taskPriority_ComboItemList_BindingList.ReplaceList((object[])data[1]);

                    //_taskPriority_ComboItemList_BindingList.IsRefreshingData = false;

                    //// TaskStatus_ComboItemList
                    //_taskStatus_ComboItemList_BindingList.IsRefreshingData = true;
                    //_taskStatus_ComboItemList_BindingList.CachedList.Clear();
                    //_taskStatus_ComboItemList_BindingList.Clear();
                    //_taskStatus_ComboItemList_BindingList.ReplaceList((object[])data[2]);

                    //_taskStatus_ComboItemList_BindingList.IsRefreshingData = false;

                    //// TaskType_ComboItemList
                    //_taskType_ComboItemList_BindingList.IsRefreshingData = true;
                    //_taskType_ComboItemList_BindingList.CachedList.Clear();
                    //_taskType_ComboItemList_BindingList.Clear();
                    //_taskType_ComboItemList_BindingList.ReplaceList((object[])data[3]);

                    //_taskType_ComboItemList_BindingList.IsRefreshingData = false;
                    //isDataLoaded = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }



      public  static void GetTask_ReadOnlyStaticListsCompleted(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_ReadOnlyStaticListsCompleted", IfxTraceCategory.Enter);
                if (data != null)
                {
                    // TaskCategory_ComboItemList
                    _taskCategory_ComboItemList_BindingList.IsRefreshingData = true;
                    _taskCategory_ComboItemList_BindingList.CachedList.Clear();
                    _taskCategory_ComboItemList_BindingList.Clear();
                    _taskCategory_ComboItemList_BindingList.ReplaceList((object[])data[0]);

                    _taskCategory_ComboItemList_BindingList.IsRefreshingData = false;

                    // TaskPriority_ComboItemList
                    _taskPriority_ComboItemList_BindingList.IsRefreshingData = true;
                    _taskPriority_ComboItemList_BindingList.CachedList.Clear();
                    _taskPriority_ComboItemList_BindingList.Clear();
                    _taskPriority_ComboItemList_BindingList.ReplaceList((object[])data[1]);

                    _taskPriority_ComboItemList_BindingList.IsRefreshingData = false;

                    // TaskStatus_ComboItemList
                    _taskStatus_ComboItemList_BindingList.IsRefreshingData = true;
                    _taskStatus_ComboItemList_BindingList.CachedList.Clear();
                    _taskStatus_ComboItemList_BindingList.Clear();
                    _taskStatus_ComboItemList_BindingList.ReplaceList((object[])data[2]);

                    _taskStatus_ComboItemList_BindingList.IsRefreshingData = false;

                    // TaskType_ComboItemList
                    _taskType_ComboItemList_BindingList.IsRefreshingData = true;
                    _taskType_ComboItemList_BindingList.CachedList.Clear();
                    _taskType_ComboItemList_BindingList.Clear();
                    _taskType_ComboItemList_BindingList.ReplaceList((object[])data[3]);

                    _taskType_ComboItemList_BindingList.IsRefreshingData = false;
                    isDataLoaded = true;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_ReadOnlyStaticListsCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_ReadOnlyStaticListsCompleted", IfxTraceCategory.Leave);
            }
        }




    }

}


