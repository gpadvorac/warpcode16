using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  12/2/2016 3:31:22 PM

namespace EntityBll.SL
{
    public partial class WcTableSortBy_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_TbSb_Direction = 4;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_TbSb_Id_Required = "Id is a required field.";
		private const string BROKENRULE_TbSb_SortOrder_ZeroNotAllowed = "Sort Order:  '0'  (zero) is not allowed.";
		private string BROKENRULE_TbSb_Direction_TextLength = "Direction has a maximum text length of  '" + STRINGSIZE_TbSb_Direction + "'.";
		private const string BROKENRULE_TbSb_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_TbSb_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("TbSb_Id", BROKENRULE_TbSb_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbSb_Tb_Id", BROKENRULE_TbSb_Tb_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbSb_SortOrder", BROKENRULE_TbSb_SortOrder_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbSb_Direction", BROKENRULE_TbSb_Direction_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbSb_IsDeleted", BROKENRULE_TbSb_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("TbSb_Stamp", BROKENRULE_TbSb_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void TbSb_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbSb_Id");
                string newBrokenRules = "";
                
                if (TbSb_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbSb_Id", BROKENRULE_TbSb_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbSb_Id", BROKENRULE_TbSb_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbSb_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbSb_Id", _brokenRuleManager.IsPropertyValid("TbSb_Id"), IsPropertyDirty("TbSb_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbSb_Tb_Id_Validate()
        {
        }

        private void TbSb_TbC_Id_Validate()
        {
        }

        private void TbSb_TbC_Id_TextField_Validate()
        {
        }

        private void TbSb_SortOrder_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_SortOrder_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbSb_SortOrder");
                string newBrokenRules = "";
                
                if (TbSb_SortOrder == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbSb_SortOrder", BROKENRULE_TbSb_SortOrder_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbSb_SortOrder", BROKENRULE_TbSb_SortOrder_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbSb_SortOrder");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbSb_SortOrder", _brokenRuleManager.IsPropertyValid("TbSb_SortOrder"), IsPropertyDirty("TbSb_SortOrder"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_SortOrder_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_SortOrder_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbSb_Direction_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_Direction_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbSb_Direction");
                string newBrokenRules = "";
                				int len = 0;
                if (TbSb_Direction != null)
                {
                    len = TbSb_Direction.Length;
                }

                if (len > STRINGSIZE_TbSb_Direction)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbSb_Direction", BROKENRULE_TbSb_Direction_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbSb_Direction", BROKENRULE_TbSb_Direction_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbSb_Direction");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbSb_Direction", _brokenRuleManager.IsPropertyValid("TbSb_Direction"), IsPropertyDirty("TbSb_Direction"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_Direction_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_Direction_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbSb_IsActiveRow_Validate()
        {
        }

        private void TbSb_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbSb_IsDeleted");
                string newBrokenRules = "";
                
                if (TbSb_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbSb_IsDeleted", BROKENRULE_TbSb_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbSb_IsDeleted", BROKENRULE_TbSb_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbSb_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbSb_IsDeleted", _brokenRuleManager.IsPropertyValid("TbSb_IsDeleted"), IsPropertyDirty("TbSb_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbSb_CreatedUserId_Validate()
        {
        }

        private void TbSb_CreatedDate_Validate()
        {
        }

        private void TbSb_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void TbSb_LastModifiedDate_Validate()
        {
        }

        private void TbSb_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbSb_Stamp");
                string newBrokenRules = "";
                
                if (TbSb_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("TbSb_Stamp", BROKENRULE_TbSb_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("TbSb_Stamp", BROKENRULE_TbSb_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("TbSb_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("TbSb_Stamp", _brokenRuleManager.IsPropertyValid("TbSb_Stamp"), IsPropertyDirty("TbSb_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "TbSb_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


