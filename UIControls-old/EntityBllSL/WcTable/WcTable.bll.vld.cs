using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TypeServices;
using System.Diagnostics;
using Ifx.SL;

// Gen Timestamp:  1/3/2017 11:21:53 PM

namespace EntityBll.SL
{
    public partial class WcTable_Bll 
    {

        /*
         * 
         Things to ask ourselves:
         * 
         * 1)  We need to know exactly where an event should be called to notify the UI that the DataState has changed
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         *          
         * 2)  Where can/should we raise an event to notify the UI that a field's valid state has changed.
         *          a) broken rules class
         *          b) validation method
         *          c) the data prop
         * 
        */


        #region Initialize Variables

		public const int STRINGSIZE_Tb_Name = 100;
		public const int STRINGSIZE_Tb_EntityRootName = 75;
		public const int STRINGSIZE_Tb_VariableName = 100;
		public const int STRINGSIZE_Tb_ScreenCaption = 100;
		public const int STRINGSIZE_Tb_Description = 2000;
		public const int STRINGSIZE_Tb_DevelopmentNote = 2000;
		public const int STRINGSIZE_Tb_UIAssemblyPath = 250;
		public const int STRINGSIZE_Tb_UIAssemblyName = 75;
		public const int STRINGSIZE_Tb_UINamespace = 75;
		public const int STRINGSIZE_Tb_WebServiceFolder = 100;
		public const int STRINGSIZE_Tb_WebServiceName = 100;
		public const int STRINGSIZE_Tb_DataServicePath = 250;
		public const int STRINGSIZE_Tb_DataServiceAssemblyName = 100;
		public const int STRINGSIZE_Tb_WireTypePath = 250;
		public const int STRINGSIZE_Tb_WireTypeAssemblyName = 100;
		public const int STRINGSIZE_UserName = 60;
		private const string BROKENRULE_Tb_Id_Required = "Id is a required field.";
		private const string BROKENRULE_Tb_ApVrsn_Id_Required = "ApVrsn_Id is a required field.";
		private const string BROKENRULE_AttachmentCount_ZeroNotAllowed = "Attachments:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_DiscussionCount_ZeroNotAllowed = "Discussions:  '0'  (zero) is not allowed.";
		private const string BROKENRULE_Tb_IsComplete_Required = "Is Complete - Ready for Codegen is a required field.";
		private string BROKENRULE_Tb_Name_TextLength = "Name has a maximum text length of  '" + STRINGSIZE_Tb_Name + "'.";
		private const string BROKENRULE_Tb_Name_Required = "Name is a required field.";
		private string BROKENRULE_Tb_EntityRootName_TextLength = "Entity Root Name has a maximum text length of  '" + STRINGSIZE_Tb_EntityRootName + "'.";
		private const string BROKENRULE_Tb_EntityRootName_Required = "Entity Root Name is a required field.";
		private string BROKENRULE_Tb_VariableName_TextLength = "Variable Name has a maximum text length of  '" + STRINGSIZE_Tb_VariableName + "'.";
		private const string BROKENRULE_Tb_VariableName_Required = "Variable Name is a required field.";
		private string BROKENRULE_Tb_ScreenCaption_TextLength = "Screen Caption has a maximum text length of  '" + STRINGSIZE_Tb_ScreenCaption + "'.";
		private const string BROKENRULE_Tb_ScreenCaption_Required = "Screen Caption is a required field.";
		private string BROKENRULE_Tb_Description_TextLength = "Description has a maximum text length of  '" + STRINGSIZE_Tb_Description + "'.";
		private string BROKENRULE_Tb_DevelopmentNote_TextLength = "Development Notes has a maximum text length of  '" + STRINGSIZE_Tb_DevelopmentNote + "'.";
		private string BROKENRULE_Tb_UIAssemblyPath_TextLength = "UI Assembly Path has a maximum text length of  '" + STRINGSIZE_Tb_UIAssemblyPath + "'.";
		private string BROKENRULE_Tb_UIAssemblyName_TextLength = "UI Assembly Name has a maximum text length of  '" + STRINGSIZE_Tb_UIAssemblyName + "'.";
		private string BROKENRULE_Tb_UINamespace_TextLength = "UI Namespace has a maximum text length of  '" + STRINGSIZE_Tb_UINamespace + "'.";
		private string BROKENRULE_Tb_WebServiceFolder_TextLength = "Web Service Folder has a maximum text length of  '" + STRINGSIZE_Tb_WebServiceFolder + "'.";
		private string BROKENRULE_Tb_WebServiceName_TextLength = "Web Service Name has a maximum text length of  '" + STRINGSIZE_Tb_WebServiceName + "'.";
		private string BROKENRULE_Tb_DataServicePath_TextLength = "Data Service Path has a maximum text length of  '" + STRINGSIZE_Tb_DataServicePath + "'.";
		private string BROKENRULE_Tb_DataServiceAssemblyName_TextLength = "DataService Assembly Name has a maximum text length of  '" + STRINGSIZE_Tb_DataServiceAssemblyName + "'.";
		private string BROKENRULE_Tb_WireTypePath_TextLength = "WireType Path has a maximum text length of  '" + STRINGSIZE_Tb_WireTypePath + "'.";
		private string BROKENRULE_Tb_WireTypeAssemblyName_TextLength = "WireType Assembly Name has a maximum text length of  '" + STRINGSIZE_Tb_WireTypeAssemblyName + "'.";
		private const string BROKENRULE_Tb_IsCodeGenComplete_Required = "Is CodeGen Complete is a required field.";
		private const string BROKENRULE_Tb_IsActiveRow_Required = "Active record is a required field.";
		private const string BROKENRULE_Tb_IsDeleted_Required = "IsDeleted is a required field.";
		private string BROKENRULE_UserName_TextLength = "Email/User name has a maximum text length of  '" + STRINGSIZE_UserName + "'.";
		private const string BROKENRULE_Tb_Stamp_Required = "Stamp is a required field.";
		
        private BrokenRuleManager _brokenRuleManager;

        #endregion Initialize Variables


        public BrokenRuleManager BrokenRuleManagerProperty
        {
            get { return _brokenRuleManager; }
            set { _brokenRuleManager = value; }
        }

        public void SetDefaultBrokenRules()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Enter);
                _brokenRuleManager.ClearAllBrokenRules();
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_Id", BROKENRULE_Tb_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_ApVrsn_Id", BROKENRULE_Tb_ApVrsn_Id_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("AttachmentFileNames", BROKENRULE_AttachmentFileNames_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
				//_brokenRuleManager.AddBrokenRuleForProperty("DiscussionTitles", BROKENRULE_DiscussionTitles_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsComplete", BROKENRULE_Tb_IsComplete_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_TextLength);
				_brokenRuleManager.AddBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_Description", BROKENRULE_Tb_Description_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_DevelopmentNote", BROKENRULE_Tb_DevelopmentNote_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_UIAssemblyPath", BROKENRULE_Tb_UIAssemblyPath_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_UIAssemblyName", BROKENRULE_Tb_UIAssemblyName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_UINamespace", BROKENRULE_Tb_UINamespace_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_WebServiceFolder", BROKENRULE_Tb_WebServiceFolder_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_WebServiceName", BROKENRULE_Tb_WebServiceName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_DataServicePath", BROKENRULE_Tb_DataServicePath_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_DataServiceAssemblyName", BROKENRULE_Tb_DataServiceAssemblyName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_WireTypePath", BROKENRULE_Tb_WireTypePath_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_WireTypeAssemblyName", BROKENRULE_Tb_WireTypeAssemblyName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsCodeGenComplete", BROKENRULE_Tb_IsCodeGenComplete_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsActiveRow", BROKENRULE_Tb_IsActiveRow_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_IsDeleted", BROKENRULE_Tb_IsDeleted_Required);
				//_brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
				//_brokenRuleManager.AddBrokenRuleForProperty("Tb_Stamp", BROKENRULE_Tb_Stamp_Required);

                SetCustomDefaultBrokenRules();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "SetDefaultBrokenRules", IfxTraceCategory.Leave);
            }
        }


		#region Validation Logic


        private void Tb_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Id");
                string newBrokenRules = "";
                
                if (Tb_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_Id", BROKENRULE_Tb_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_Id", BROKENRULE_Tb_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_Id", _brokenRuleManager.IsPropertyValid("Tb_Id"), IsPropertyDirty("Tb_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_ApVrsn_Id_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ApVrsn_Id_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_ApVrsn_Id");
                string newBrokenRules = "";
                
                if (Tb_ApVrsn_Id == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_ApVrsn_Id", BROKENRULE_Tb_ApVrsn_Id_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_ApVrsn_Id", BROKENRULE_Tb_ApVrsn_Id_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_ApVrsn_Id");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_ApVrsn_Id", _brokenRuleManager.IsPropertyValid("Tb_ApVrsn_Id"), IsPropertyDirty("Tb_ApVrsn_Id"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ApVrsn_Id_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ApVrsn_Id_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_auditTable_Id_Validate()
        {
        }

        private void AttachmentCount_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("AttachmentCount");
                string newBrokenRules = "";
                
                if (AttachmentCount == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("AttachmentCount", BROKENRULE_AttachmentCount_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("AttachmentCount");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("AttachmentCount", _brokenRuleManager.IsPropertyValid("AttachmentCount"), IsPropertyDirty("AttachmentCount"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AttachmentCount_Validate", IfxTraceCategory.Leave);
            }
        }

        private void AttachmentFileNames_Validate()
        {
        }

        private void DiscussionCount_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DiscussionCount");
                string newBrokenRules = "";
                
                if (DiscussionCount == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("DiscussionCount", BROKENRULE_DiscussionCount_ZeroNotAllowed);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("DiscussionCount");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("DiscussionCount", _brokenRuleManager.IsPropertyValid("DiscussionCount"), IsPropertyDirty("DiscussionCount"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "DiscussionCount_Validate", IfxTraceCategory.Leave);
            }
        }

        private void DiscussionTitles_Validate()
        {
        }

        private void Tb_IsComplete_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsComplete_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsComplete");
                string newBrokenRules = "";
                
                if (Tb_IsComplete == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsComplete", BROKENRULE_Tb_IsComplete_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsComplete", BROKENRULE_Tb_IsComplete_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsComplete");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsComplete", _brokenRuleManager.IsPropertyValid("Tb_IsComplete"), IsPropertyDirty("Tb_IsComplete"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsComplete_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsComplete_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_Name_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Name_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Name");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_Name != null)
                {
                    len = Tb_Name.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_Required);
                    if (len > STRINGSIZE_Tb_Name)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Tb_Name", BROKENRULE_Tb_Name_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Name");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_Name", _brokenRuleManager.IsPropertyValid("Tb_Name"), IsPropertyDirty("Tb_Name"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Name_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Name_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_EntityRootName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_EntityRootName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_EntityRootName");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_EntityRootName != null)
                {
                    len = Tb_EntityRootName.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_Required);
                    if (len > STRINGSIZE_Tb_EntityRootName)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Tb_EntityRootName", BROKENRULE_Tb_EntityRootName_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_EntityRootName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_EntityRootName", _brokenRuleManager.IsPropertyValid("Tb_EntityRootName"), IsPropertyDirty("Tb_EntityRootName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_EntityRootName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_EntityRootName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_VariableName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_VariableName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_VariableName");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_VariableName != null)
                {
                    len = Tb_VariableName.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_Required);
                    if (len > STRINGSIZE_Tb_VariableName)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Tb_VariableName", BROKENRULE_Tb_VariableName_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_VariableName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_VariableName", _brokenRuleManager.IsPropertyValid("Tb_VariableName"), IsPropertyDirty("Tb_VariableName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_VariableName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_VariableName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_ScreenCaption_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ScreenCaption_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_ScreenCaption");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_ScreenCaption != null)
                {
                    len = Tb_ScreenCaption.Length;
                }

                if (len == 0)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_Required);
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_Required);
                    if (len > STRINGSIZE_Tb_ScreenCaption)
                    {
                        _brokenRuleManager.AddBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_TextLength);
                    }
                    else
                    {
                        _brokenRuleManager.ClearBrokenRuleForProperty("Tb_ScreenCaption", BROKENRULE_Tb_ScreenCaption_TextLength);
                    }
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_ScreenCaption");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_ScreenCaption", _brokenRuleManager.IsPropertyValid("Tb_ScreenCaption"), IsPropertyDirty("Tb_ScreenCaption"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ScreenCaption_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_ScreenCaption_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_Description_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Description_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Description");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_Description != null)
                {
                    len = Tb_Description.Length;
                }

                if (len > STRINGSIZE_Tb_Description)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_Description", BROKENRULE_Tb_Description_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_Description", BROKENRULE_Tb_Description_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Description");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_Description", _brokenRuleManager.IsPropertyValid("Tb_Description"), IsPropertyDirty("Tb_Description"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Description_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Description_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_DevelopmentNote_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DevelopmentNote_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_DevelopmentNote");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_DevelopmentNote != null)
                {
                    len = Tb_DevelopmentNote.Length;
                }

                if (len > STRINGSIZE_Tb_DevelopmentNote)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_DevelopmentNote", BROKENRULE_Tb_DevelopmentNote_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_DevelopmentNote", BROKENRULE_Tb_DevelopmentNote_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_DevelopmentNote");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_DevelopmentNote", _brokenRuleManager.IsPropertyValid("Tb_DevelopmentNote"), IsPropertyDirty("Tb_DevelopmentNote"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DevelopmentNote_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DevelopmentNote_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_UIAssemblyPath_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyPath_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UIAssemblyPath");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_UIAssemblyPath != null)
                {
                    len = Tb_UIAssemblyPath.Length;
                }

                if (len > STRINGSIZE_Tb_UIAssemblyPath)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_UIAssemblyPath", BROKENRULE_Tb_UIAssemblyPath_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_UIAssemblyPath", BROKENRULE_Tb_UIAssemblyPath_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UIAssemblyPath");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_UIAssemblyPath", _brokenRuleManager.IsPropertyValid("Tb_UIAssemblyPath"), IsPropertyDirty("Tb_UIAssemblyPath"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyPath_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyPath_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_UIAssemblyName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UIAssemblyName");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_UIAssemblyName != null)
                {
                    len = Tb_UIAssemblyName.Length;
                }

                if (len > STRINGSIZE_Tb_UIAssemblyName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_UIAssemblyName", BROKENRULE_Tb_UIAssemblyName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_UIAssemblyName", BROKENRULE_Tb_UIAssemblyName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UIAssemblyName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_UIAssemblyName", _brokenRuleManager.IsPropertyValid("Tb_UIAssemblyName"), IsPropertyDirty("Tb_UIAssemblyName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UIAssemblyName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_UINamespace_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UINamespace_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UINamespace");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_UINamespace != null)
                {
                    len = Tb_UINamespace.Length;
                }

                if (len > STRINGSIZE_Tb_UINamespace)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_UINamespace", BROKENRULE_Tb_UINamespace_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_UINamespace", BROKENRULE_Tb_UINamespace_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_UINamespace");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_UINamespace", _brokenRuleManager.IsPropertyValid("Tb_UINamespace"), IsPropertyDirty("Tb_UINamespace"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UINamespace_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_UINamespace_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_WebServiceFolder_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceFolder_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WebServiceFolder");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_WebServiceFolder != null)
                {
                    len = Tb_WebServiceFolder.Length;
                }

                if (len > STRINGSIZE_Tb_WebServiceFolder)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_WebServiceFolder", BROKENRULE_Tb_WebServiceFolder_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_WebServiceFolder", BROKENRULE_Tb_WebServiceFolder_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WebServiceFolder");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_WebServiceFolder", _brokenRuleManager.IsPropertyValid("Tb_WebServiceFolder"), IsPropertyDirty("Tb_WebServiceFolder"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceFolder_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceFolder_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_WebServiceName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WebServiceName");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_WebServiceName != null)
                {
                    len = Tb_WebServiceName.Length;
                }

                if (len > STRINGSIZE_Tb_WebServiceName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_WebServiceName", BROKENRULE_Tb_WebServiceName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_WebServiceName", BROKENRULE_Tb_WebServiceName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WebServiceName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_WebServiceName", _brokenRuleManager.IsPropertyValid("Tb_WebServiceName"), IsPropertyDirty("Tb_WebServiceName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WebServiceName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_DataServicePath_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServicePath_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_DataServicePath");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_DataServicePath != null)
                {
                    len = Tb_DataServicePath.Length;
                }

                if (len > STRINGSIZE_Tb_DataServicePath)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_DataServicePath", BROKENRULE_Tb_DataServicePath_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_DataServicePath", BROKENRULE_Tb_DataServicePath_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_DataServicePath");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_DataServicePath", _brokenRuleManager.IsPropertyValid("Tb_DataServicePath"), IsPropertyDirty("Tb_DataServicePath"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServicePath_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServicePath_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_DataServiceAssemblyName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServiceAssemblyName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_DataServiceAssemblyName");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_DataServiceAssemblyName != null)
                {
                    len = Tb_DataServiceAssemblyName.Length;
                }

                if (len > STRINGSIZE_Tb_DataServiceAssemblyName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_DataServiceAssemblyName", BROKENRULE_Tb_DataServiceAssemblyName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_DataServiceAssemblyName", BROKENRULE_Tb_DataServiceAssemblyName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_DataServiceAssemblyName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_DataServiceAssemblyName", _brokenRuleManager.IsPropertyValid("Tb_DataServiceAssemblyName"), IsPropertyDirty("Tb_DataServiceAssemblyName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServiceAssemblyName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_DataServiceAssemblyName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_WireTypePath_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypePath_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WireTypePath");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_WireTypePath != null)
                {
                    len = Tb_WireTypePath.Length;
                }

                if (len > STRINGSIZE_Tb_WireTypePath)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_WireTypePath", BROKENRULE_Tb_WireTypePath_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_WireTypePath", BROKENRULE_Tb_WireTypePath_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WireTypePath");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_WireTypePath", _brokenRuleManager.IsPropertyValid("Tb_WireTypePath"), IsPropertyDirty("Tb_WireTypePath"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypePath_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypePath_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_WireTypeAssemblyName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypeAssemblyName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WireTypeAssemblyName");
                string newBrokenRules = "";
                				int len = 0;
                if (Tb_WireTypeAssemblyName != null)
                {
                    len = Tb_WireTypeAssemblyName.Length;
                }

                if (len > STRINGSIZE_Tb_WireTypeAssemblyName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_WireTypeAssemblyName", BROKENRULE_Tb_WireTypeAssemblyName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_WireTypeAssemblyName", BROKENRULE_Tb_WireTypeAssemblyName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_WireTypeAssemblyName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_WireTypeAssemblyName", _brokenRuleManager.IsPropertyValid("Tb_WireTypeAssemblyName"), IsPropertyDirty("Tb_WireTypeAssemblyName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypeAssemblyName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_WireTypeAssemblyName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_UseTilesInPropsScreen_Validate()
        {
        }

        private void Tb_UseGridColumnGroups_Validate()
        {
        }

        private void Tb_UseGridDataSourceCombo_Validate()
        {
        }

        private void Tb_ApCnSK_Id_Validate()
        {
        }

        private void Tb_ApCnSK_Id_TextField_Validate()
        {
        }

        private void Tb_UseLegacyConnectionCode_Validate()
        {
        }

        private void Tb_PkIsIdentity_Validate()
        {
        }

        private void Tb_IsVirtual_Validate()
        {
        }

        private void Tb_IsNotEntity_Validate()
        {
        }

        private void Tb_UseLastModifiedByUserNameInSproc_Validate()
        {
        }

        private void Tb_UseUserTimeStamp_Validate()
        {
        }

        private void Tb_UseForAudit_Validate()
        {
        }

        private void Tb_IsCodeGen_Validate()
        {
        }

        private void Tb_IsCodeGenComplete_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGenComplete_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsCodeGenComplete");
                string newBrokenRules = "";
                
                if (Tb_IsCodeGenComplete == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsCodeGenComplete", BROKENRULE_Tb_IsCodeGenComplete_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsCodeGenComplete", BROKENRULE_Tb_IsCodeGenComplete_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsCodeGenComplete");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsCodeGenComplete", _brokenRuleManager.IsPropertyValid("Tb_IsCodeGenComplete"), IsPropertyDirty("Tb_IsCodeGenComplete"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGenComplete_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsCodeGenComplete_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_IsActiveRow_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsActiveRow_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsActiveRow");
                string newBrokenRules = "";
                
                if (Tb_IsActiveRow == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsActiveRow", BROKENRULE_Tb_IsActiveRow_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsActiveRow", BROKENRULE_Tb_IsActiveRow_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsActiveRow");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsActiveRow", _brokenRuleManager.IsPropertyValid("Tb_IsActiveRow"), IsPropertyDirty("Tb_IsActiveRow"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsActiveRow_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsActiveRow_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_IsDeleted_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsDeleted_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsDeleted");
                string newBrokenRules = "";
                
                if (Tb_IsDeleted == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_IsDeleted", BROKENRULE_Tb_IsDeleted_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_IsDeleted", BROKENRULE_Tb_IsDeleted_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_IsDeleted");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_IsDeleted", _brokenRuleManager.IsPropertyValid("Tb_IsDeleted"), IsPropertyDirty("Tb_IsDeleted"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsDeleted_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_IsDeleted_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_CreatedUserId_Validate()
        {
        }

        private void Tb_CreatedDate_Validate()
        {
        }

        private void Tb_UserId_Validate()
        {
        }

        private void UserName_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                string newBrokenRules = "";
                				int len = 0;
                if (UserName != null)
                {
                    len = UserName.Length;
                }

                if (len > STRINGSIZE_UserName)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("UserName", BROKENRULE_UserName_TextLength);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("UserName");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("UserName", _brokenRuleManager.IsPropertyValid("UserName"), IsPropertyDirty("UserName"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "UserName_Validate", IfxTraceCategory.Leave);
            }
        }

        private void Tb_LastModifiedDate_Validate()
        {
        }

        private void Tb_Stamp_Validate()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Stamp_Validate", IfxTraceCategory.Enter);
                string currentBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Stamp");
                string newBrokenRules = "";
                
                if (Tb_Stamp == null)
                {
                    _brokenRuleManager.AddBrokenRuleForProperty("Tb_Stamp", BROKENRULE_Tb_Stamp_Required);
                }
                else
                {
                    _brokenRuleManager.ClearBrokenRuleForProperty("Tb_Stamp", BROKENRULE_Tb_Stamp_Required);
                }

                newBrokenRules = _brokenRuleManager.GetBrokenRulesForPropertyAsString("Tb_Stamp");
                if(currentBrokenRules != newBrokenRules)
                {
                    RaiseEventControlValidStateChanged("Tb_Stamp", _brokenRuleManager.IsPropertyValid("Tb_Stamp"), IsPropertyDirty("Tb_Stamp"));
                }

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Stamp_Validate", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Tb_Stamp_Validate", IfxTraceCategory.Leave);
            }
        }


		#endregion Validation Logic


    }

}


