using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  11/17/2016 12:24:41 PM

namespace ProxyWrapper
{
    public partial class WcApplicationConnectionStringKeyService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcApplicationConnectionStringKey_GetByIdCompletedEventArgs> WcApplicationConnectionStringKey_GetByIdCompleted;
        public event System.EventHandler<WcApplicationConnectionStringKey_GetAllCompletedEventArgs> WcApplicationConnectionStringKey_GetAllCompleted;
        public event System.EventHandler<WcApplicationConnectionStringKey_GetListByFKCompletedEventArgs> WcApplicationConnectionStringKey_GetListByFKCompleted;
        public event System.EventHandler<WcApplicationConnectionStringKey_SaveCompletedEventArgs> WcApplicationConnectionStringKey_SaveCompleted;
        public event System.EventHandler<WcApplicationConnectionStringKey_DeleteCompletedEventArgs> WcApplicationConnectionStringKey_DeleteCompleted;
        public event System.EventHandler<WcApplicationConnectionStringKey_SetIsDeletedCompletedEventArgs> WcApplicationConnectionStringKey_SetIsDeletedCompleted;
        public event System.EventHandler<WcApplicationConnectionStringKey_DeactivateCompletedEventArgs> WcApplicationConnectionStringKey_DeactivateCompleted;
        public event System.EventHandler<WcApplicationConnectionStringKey_RemoveCompletedEventArgs> WcApplicationConnectionStringKey_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcApplicationConnectionStringKey_GetById

        public void Begin_WcApplicationConnectionStringKey_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_GetById", IfxTraceCategory.Enter);
                WcApplicationConnectionStringKeyServiceClient proxy = new WcApplicationConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationConnectionStringKey_GetByIdCompleted += new EventHandler<WcApplicationConnectionStringKey_GetByIdCompletedEventArgs>(proxy_WcApplicationConnectionStringKey_GetByIdCompleted);
                proxy.WcApplicationConnectionStringKey_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationConnectionStringKey_GetByIdCompleted(object sender, WcApplicationConnectionStringKey_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationConnectionStringKey_GetByIdCompletedEventArgs> handler = WcApplicationConnectionStringKey_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationConnectionStringKey_GetById

        #region WcApplicationConnectionStringKey_GetAll

        public void Begin_WcApplicationConnectionStringKey_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_GetAll", IfxTraceCategory.Enter);
                WcApplicationConnectionStringKeyServiceClient proxy = new WcApplicationConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationConnectionStringKey_GetAllCompleted += new EventHandler<WcApplicationConnectionStringKey_GetAllCompletedEventArgs>(proxy_WcApplicationConnectionStringKey_GetAllCompleted);
                proxy.WcApplicationConnectionStringKey_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationConnectionStringKey_GetAllCompleted(object sender, WcApplicationConnectionStringKey_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationConnectionStringKey_GetAllCompletedEventArgs> handler = WcApplicationConnectionStringKey_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationConnectionStringKey_GetAll

        #region WcApplicationConnectionStringKey_GetListByFK

        public void Begin_WcApplicationConnectionStringKey_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_GetListByFK", IfxTraceCategory.Enter);
                WcApplicationConnectionStringKeyServiceClient proxy = new WcApplicationConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationConnectionStringKey_GetListByFKCompleted += new EventHandler<WcApplicationConnectionStringKey_GetListByFKCompletedEventArgs>(proxy_WcApplicationConnectionStringKey_GetListByFKCompleted);
                proxy.WcApplicationConnectionStringKey_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationConnectionStringKey_GetListByFKCompleted(object sender, WcApplicationConnectionStringKey_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationConnectionStringKey_GetListByFKCompletedEventArgs> handler = WcApplicationConnectionStringKey_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationConnectionStringKey_GetListByFK

        #region WcApplicationConnectionStringKey_Save

        public void Begin_WcApplicationConnectionStringKey_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_Save", IfxTraceCategory.Enter);
                WcApplicationConnectionStringKeyServiceClient proxy = new WcApplicationConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                //proxy.WcApplicationConnectionStringKey_SaveCompleted += new EventHandler<WcApplicationConnectionStringKey_SaveCompletedEventArgs>(proxy_WcApplicationConnectionStringKey_SaveCompleted);
                proxy.WcApplicationConnectionStringKey_SaveCompleted += new EventHandler<WcApplicationConnectionStringKey_SaveCompletedEventArgs>(proxy_WcApplicationConnectionStringKey_SaveCompleted);
                proxy.WcApplicationConnectionStringKey_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationConnectionStringKey_SaveCompleted(object sender, WcApplicationConnectionStringKey_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationConnectionStringKey_SaveCompletedEventArgs> handler = WcApplicationConnectionStringKey_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationConnectionStringKey_Save

        #region WcApplicationConnectionStringKey_Delete

        public void Begin_WcApplicationConnectionStringKey_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_DeleteCompleted", IfxTraceCategory.Enter);
                WcApplicationConnectionStringKeyServiceClient proxy = new WcApplicationConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationConnectionStringKey_DeleteCompleted += new EventHandler<WcApplicationConnectionStringKey_DeleteCompletedEventArgs>(proxy_WcApplicationConnectionStringKey_DeleteCompleted);
                proxy.WcApplicationConnectionStringKey_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationConnectionStringKey_DeleteCompleted(object sender, WcApplicationConnectionStringKey_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationConnectionStringKey_DeleteCompletedEventArgs> handler = WcApplicationConnectionStringKey_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationConnectionStringKey_Delete

        #region WcApplicationConnectionStringKey_SetIsDeleted

        public void Begin_WcApplicationConnectionStringKey_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcApplicationConnectionStringKeyServiceClient proxy = new WcApplicationConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationConnectionStringKey_SetIsDeletedCompleted += new EventHandler<WcApplicationConnectionStringKey_SetIsDeletedCompletedEventArgs>(proxy_WcApplicationConnectionStringKey_SetIsDeletedCompleted);
                proxy.WcApplicationConnectionStringKey_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationConnectionStringKey_SetIsDeletedCompleted(object sender, WcApplicationConnectionStringKey_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationConnectionStringKey_SetIsDeletedCompletedEventArgs> handler = WcApplicationConnectionStringKey_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationConnectionStringKey_SetIsDeleted

        #region WcApplicationConnectionStringKey_Deactivate

        public void Begin_WcApplicationConnectionStringKey_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_Deactivate", IfxTraceCategory.Enter);
            WcApplicationConnectionStringKeyServiceClient proxy = new WcApplicationConnectionStringKeyServiceClient();
            AssignCredentials(proxy);
            proxy.WcApplicationConnectionStringKey_DeactivateCompleted += new EventHandler<WcApplicationConnectionStringKey_DeactivateCompletedEventArgs>(proxy_WcApplicationConnectionStringKey_DeactivateCompleted);
            proxy.WcApplicationConnectionStringKey_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationConnectionStringKey_DeactivateCompleted(object sender, WcApplicationConnectionStringKey_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationConnectionStringKey_DeactivateCompletedEventArgs> handler = WcApplicationConnectionStringKey_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcApplicationConnectionStringKey_Deactivate

        #region WcApplicationConnectionStringKey_Remove

        public void Begin_WcApplicationConnectionStringKey_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_Remove", IfxTraceCategory.Enter);
                WcApplicationConnectionStringKeyServiceClient proxy = new WcApplicationConnectionStringKeyServiceClient();
                AssignCredentials(proxy);
                proxy.WcApplicationConnectionStringKey_RemoveCompleted += new EventHandler<WcApplicationConnectionStringKey_RemoveCompletedEventArgs>(proxy_WcApplicationConnectionStringKey_RemoveCompleted);
                proxy.WcApplicationConnectionStringKey_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcApplicationConnectionStringKey_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcApplicationConnectionStringKey_RemoveCompleted(object sender, WcApplicationConnectionStringKey_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcApplicationConnectionStringKey_RemoveCompletedEventArgs> handler = WcApplicationConnectionStringKey_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationConnectionStringKey_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcApplicationConnectionStringKey_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


