using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  1/3/2017 11:12:19 PM

namespace ProxyWrapper
{
    public partial class WcToolTipService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcToolTip_GetByIdCompletedEventArgs> WcToolTip_GetByIdCompleted;
        public event System.EventHandler<WcToolTip_GetAllCompletedEventArgs> WcToolTip_GetAllCompleted;
        public event System.EventHandler<WcToolTip_GetListByFKCompletedEventArgs> WcToolTip_GetListByFKCompleted;
        public event System.EventHandler<WcToolTip_SaveCompletedEventArgs> WcToolTip_SaveCompleted;
        public event System.EventHandler<WcToolTip_DeleteCompletedEventArgs> WcToolTip_DeleteCompleted;
        public event System.EventHandler<WcToolTip_SetIsDeletedCompletedEventArgs> WcToolTip_SetIsDeletedCompleted;
        public event System.EventHandler<WcToolTip_DeactivateCompletedEventArgs> WcToolTip_DeactivateCompleted;
        public event System.EventHandler<WcToolTip_RemoveCompletedEventArgs> WcToolTip_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcToolTip_GetById

        public void Begin_WcToolTip_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_GetById", IfxTraceCategory.Enter);
                WcToolTipServiceClient proxy = new WcToolTipServiceClient();
                AssignCredentials(proxy);
                proxy.WcToolTip_GetByIdCompleted += new EventHandler<WcToolTip_GetByIdCompletedEventArgs>(proxy_WcToolTip_GetByIdCompleted);
                proxy.WcToolTip_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcToolTip_GetByIdCompleted(object sender, WcToolTip_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcToolTip_GetByIdCompletedEventArgs> handler = WcToolTip_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcToolTip_GetById

        #region WcToolTip_GetAll

        public void Begin_WcToolTip_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_GetAll", IfxTraceCategory.Enter);
                WcToolTipServiceClient proxy = new WcToolTipServiceClient();
                AssignCredentials(proxy);
                proxy.WcToolTip_GetAllCompleted += new EventHandler<WcToolTip_GetAllCompletedEventArgs>(proxy_WcToolTip_GetAllCompleted);
                proxy.WcToolTip_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcToolTip_GetAllCompleted(object sender, WcToolTip_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcToolTip_GetAllCompletedEventArgs> handler = WcToolTip_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcToolTip_GetAll

        #region WcToolTip_GetListByFK

        public void Begin_WcToolTip_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_GetListByFK", IfxTraceCategory.Enter);
                WcToolTipServiceClient proxy = new WcToolTipServiceClient();
                AssignCredentials(proxy);
                proxy.WcToolTip_GetListByFKCompleted += new EventHandler<WcToolTip_GetListByFKCompletedEventArgs>(proxy_WcToolTip_GetListByFKCompleted);
                proxy.WcToolTip_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcToolTip_GetListByFKCompleted(object sender, WcToolTip_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcToolTip_GetListByFKCompletedEventArgs> handler = WcToolTip_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcToolTip_GetListByFK

        #region WcToolTip_Save

        public void Begin_WcToolTip_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_Save", IfxTraceCategory.Enter);
                WcToolTipServiceClient proxy = new WcToolTipServiceClient();
                AssignCredentials(proxy);
                //proxy.WcToolTip_SaveCompleted += new EventHandler<WcToolTip_SaveCompletedEventArgs>(proxy_WcToolTip_SaveCompleted);
                proxy.WcToolTip_SaveCompleted += new EventHandler<WcToolTip_SaveCompletedEventArgs>(proxy_WcToolTip_SaveCompleted);
                proxy.WcToolTip_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcToolTip_SaveCompleted(object sender, WcToolTip_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcToolTip_SaveCompletedEventArgs> handler = WcToolTip_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcToolTip_Save

        #region WcToolTip_Delete

        public void Begin_WcToolTip_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_DeleteCompleted", IfxTraceCategory.Enter);
                WcToolTipServiceClient proxy = new WcToolTipServiceClient();
                AssignCredentials(proxy);
                proxy.WcToolTip_DeleteCompleted += new EventHandler<WcToolTip_DeleteCompletedEventArgs>(proxy_WcToolTip_DeleteCompleted);
                proxy.WcToolTip_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcToolTip_DeleteCompleted(object sender, WcToolTip_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcToolTip_DeleteCompletedEventArgs> handler = WcToolTip_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcToolTip_Delete

        #region WcToolTip_SetIsDeleted

        public void Begin_WcToolTip_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcToolTipServiceClient proxy = new WcToolTipServiceClient();
                AssignCredentials(proxy);
                proxy.WcToolTip_SetIsDeletedCompleted += new EventHandler<WcToolTip_SetIsDeletedCompletedEventArgs>(proxy_WcToolTip_SetIsDeletedCompleted);
                proxy.WcToolTip_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcToolTip_SetIsDeletedCompleted(object sender, WcToolTip_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcToolTip_SetIsDeletedCompletedEventArgs> handler = WcToolTip_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcToolTip_SetIsDeleted

        #region WcToolTip_Deactivate

        public void Begin_WcToolTip_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_Deactivate", IfxTraceCategory.Enter);
            WcToolTipServiceClient proxy = new WcToolTipServiceClient();
            AssignCredentials(proxy);
            proxy.WcToolTip_DeactivateCompleted += new EventHandler<WcToolTip_DeactivateCompletedEventArgs>(proxy_WcToolTip_DeactivateCompleted);
            proxy.WcToolTip_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcToolTip_DeactivateCompleted(object sender, WcToolTip_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcToolTip_DeactivateCompletedEventArgs> handler = WcToolTip_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcToolTip_Deactivate

        #region WcToolTip_Remove

        public void Begin_WcToolTip_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_Remove", IfxTraceCategory.Enter);
                WcToolTipServiceClient proxy = new WcToolTipServiceClient();
                AssignCredentials(proxy);
                proxy.WcToolTip_RemoveCompleted += new EventHandler<WcToolTip_RemoveCompletedEventArgs>(proxy_WcToolTip_RemoveCompleted);
                proxy.WcToolTip_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcToolTip_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcToolTip_RemoveCompleted(object sender, WcToolTip_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcToolTip_RemoveCompletedEventArgs> handler = WcToolTip_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcToolTip_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcToolTip_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


