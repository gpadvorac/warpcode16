using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  2/7/2015 8:42:41 PM

namespace ProxyWrapper
{
    public partial class TaskService_ProxyWrapper
    {

        #region Initialize Variables

        //public event System.EventHandler<GetTask_lstByMyTasksCompletedEventArgs> GetTask_lstByMyTasksCompleted;
        //public event System.EventHandler<GetTask_lstByProjectOnlyCompletedEventArgs> GetTask_lstByProjectOnlyCompleted;


        #endregion Initialize Variables



        //#region GetTask_lstByMyTasks

        //public void Begin_GetTask_lstByMyTasks(Guid Id)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByMyTasks", IfxTraceCategory.Enter);
        //        TaskServiceClient proxy = new TaskServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.GetTask_lstByMyTasksCompleted += new EventHandler<GetTask_lstByMyTasksCompletedEventArgs>(proxy_GetTask_lstByMyTasksCompleted);
        //        proxy.GetTask_lstByMyTasksAsync(Id);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByMyTasks", ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByMyTasks", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_GetTask_lstByMyTasksCompleted(object sender, GetTask_lstByMyTasksCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByMyTasks", IfxTraceCategory.Enter);
        //        System.EventHandler<GetTask_lstByMyTasksCompletedEventArgs> handler = GetTask_lstByMyTasksCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByMyTasks", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByMyTasks", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion GetTask_lstByMyTasks



        //#region GetTask_lstByProjectOnly

        //public void Begin_GetTask_lstByProjectOnly(Guid Tk_Prj_Id)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByProjectOnly", IfxTraceCategory.Enter);
        //        TaskServiceClient proxy = new TaskServiceClient();
        //        AssignCredentials(proxy);
        //        proxy.GetTask_lstByProjectOnlyCompleted += new EventHandler<GetTask_lstByProjectOnlyCompletedEventArgs>(proxy_GetTask_lstByProjectOnlyCompleted);
        //        proxy.GetTask_lstByProjectOnlyAsync(Tk_Prj_Id);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByProjectOnly", ex);
        //        throw IfxWrapperException.GetError(ex);
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetTask_lstByProjectOnly", IfxTraceCategory.Leave);
        //    }
        //}

        //void proxy_GetTask_lstByProjectOnlyCompleted(object sender, GetTask_lstByProjectOnlyCompletedEventArgs e)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProjectOnly", IfxTraceCategory.Enter);
        //        System.EventHandler<GetTask_lstByProjectOnlyCompletedEventArgs> handler = GetTask_lstByProjectOnlyCompleted;
        //        if (handler != null)
        //        {
        //            handler(sender, e);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProjectOnly", ex);
        //        ExceptionHelper.NotifyUserAnExceptionOccured();
        //    }
        //    finally
        //    {
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProjectOnly", IfxTraceCategory.Leave);
        //    }
        //}

        //#endregion GetTask_lstByProjectOnly




    }
}


