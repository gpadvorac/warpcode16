using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/1/2016 10:48:55 AM

namespace ProxyWrapper
{
    public partial class WcStoredProcParamValueGroupService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcStoredProcParamValueGroup_GetByIdCompletedEventArgs> WcStoredProcParamValueGroup_GetByIdCompleted;
        public event System.EventHandler<WcStoredProcParamValueGroup_GetAllCompletedEventArgs> WcStoredProcParamValueGroup_GetAllCompleted;
        public event System.EventHandler<WcStoredProcParamValueGroup_GetListByFKCompletedEventArgs> WcStoredProcParamValueGroup_GetListByFKCompleted;
        public event System.EventHandler<WcStoredProcParamValueGroup_SaveCompletedEventArgs> WcStoredProcParamValueGroup_SaveCompleted;
        public event System.EventHandler<WcStoredProcParamValueGroup_DeleteCompletedEventArgs> WcStoredProcParamValueGroup_DeleteCompleted;
        public event System.EventHandler<WcStoredProcParamValueGroup_SetIsDeletedCompletedEventArgs> WcStoredProcParamValueGroup_SetIsDeletedCompleted;
        public event System.EventHandler<WcStoredProcParamValueGroup_DeactivateCompletedEventArgs> WcStoredProcParamValueGroup_DeactivateCompleted;
        public event System.EventHandler<WcStoredProcParamValueGroup_RemoveCompletedEventArgs> WcStoredProcParamValueGroup_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcStoredProcParamValueGroup_GetById

        public void Begin_WcStoredProcParamValueGroup_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_GetById", IfxTraceCategory.Enter);
                WcStoredProcParamValueGroupServiceClient proxy = new WcStoredProcParamValueGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParamValueGroup_GetByIdCompleted += new EventHandler<WcStoredProcParamValueGroup_GetByIdCompletedEventArgs>(proxy_WcStoredProcParamValueGroup_GetByIdCompleted);
                proxy.WcStoredProcParamValueGroup_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValueGroup_GetByIdCompleted(object sender, WcStoredProcParamValueGroup_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValueGroup_GetByIdCompletedEventArgs> handler = WcStoredProcParamValueGroup_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValueGroup_GetById

        #region WcStoredProcParamValueGroup_GetAll

        public void Begin_WcStoredProcParamValueGroup_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_GetAll", IfxTraceCategory.Enter);
                WcStoredProcParamValueGroupServiceClient proxy = new WcStoredProcParamValueGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParamValueGroup_GetAllCompleted += new EventHandler<WcStoredProcParamValueGroup_GetAllCompletedEventArgs>(proxy_WcStoredProcParamValueGroup_GetAllCompleted);
                proxy.WcStoredProcParamValueGroup_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValueGroup_GetAllCompleted(object sender, WcStoredProcParamValueGroup_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValueGroup_GetAllCompletedEventArgs> handler = WcStoredProcParamValueGroup_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValueGroup_GetAll

        #region WcStoredProcParamValueGroup_GetListByFK

        public void Begin_WcStoredProcParamValueGroup_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_GetListByFK", IfxTraceCategory.Enter);
                WcStoredProcParamValueGroupServiceClient proxy = new WcStoredProcParamValueGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParamValueGroup_GetListByFKCompleted += new EventHandler<WcStoredProcParamValueGroup_GetListByFKCompletedEventArgs>(proxy_WcStoredProcParamValueGroup_GetListByFKCompleted);
                proxy.WcStoredProcParamValueGroup_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValueGroup_GetListByFKCompleted(object sender, WcStoredProcParamValueGroup_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValueGroup_GetListByFKCompletedEventArgs> handler = WcStoredProcParamValueGroup_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValueGroup_GetListByFK

        #region WcStoredProcParamValueGroup_Save

        public void Begin_WcStoredProcParamValueGroup_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_Save", IfxTraceCategory.Enter);
                WcStoredProcParamValueGroupServiceClient proxy = new WcStoredProcParamValueGroupServiceClient();
                AssignCredentials(proxy);
                //proxy.WcStoredProcParamValueGroup_SaveCompleted += new EventHandler<WcStoredProcParamValueGroup_SaveCompletedEventArgs>(proxy_WcStoredProcParamValueGroup_SaveCompleted);
                proxy.WcStoredProcParamValueGroup_SaveCompleted += new EventHandler<WcStoredProcParamValueGroup_SaveCompletedEventArgs>(proxy_WcStoredProcParamValueGroup_SaveCompleted);
                proxy.WcStoredProcParamValueGroup_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValueGroup_SaveCompleted(object sender, WcStoredProcParamValueGroup_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValueGroup_SaveCompletedEventArgs> handler = WcStoredProcParamValueGroup_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValueGroup_Save

        #region WcStoredProcParamValueGroup_Delete

        public void Begin_WcStoredProcParamValueGroup_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_DeleteCompleted", IfxTraceCategory.Enter);
                WcStoredProcParamValueGroupServiceClient proxy = new WcStoredProcParamValueGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParamValueGroup_DeleteCompleted += new EventHandler<WcStoredProcParamValueGroup_DeleteCompletedEventArgs>(proxy_WcStoredProcParamValueGroup_DeleteCompleted);
                proxy.WcStoredProcParamValueGroup_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValueGroup_DeleteCompleted(object sender, WcStoredProcParamValueGroup_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValueGroup_DeleteCompletedEventArgs> handler = WcStoredProcParamValueGroup_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValueGroup_Delete

        #region WcStoredProcParamValueGroup_SetIsDeleted

        public void Begin_WcStoredProcParamValueGroup_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcStoredProcParamValueGroupServiceClient proxy = new WcStoredProcParamValueGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParamValueGroup_SetIsDeletedCompleted += new EventHandler<WcStoredProcParamValueGroup_SetIsDeletedCompletedEventArgs>(proxy_WcStoredProcParamValueGroup_SetIsDeletedCompleted);
                proxy.WcStoredProcParamValueGroup_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValueGroup_SetIsDeletedCompleted(object sender, WcStoredProcParamValueGroup_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValueGroup_SetIsDeletedCompletedEventArgs> handler = WcStoredProcParamValueGroup_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValueGroup_SetIsDeleted

        #region WcStoredProcParamValueGroup_Deactivate

        public void Begin_WcStoredProcParamValueGroup_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_Deactivate", IfxTraceCategory.Enter);
            WcStoredProcParamValueGroupServiceClient proxy = new WcStoredProcParamValueGroupServiceClient();
            AssignCredentials(proxy);
            proxy.WcStoredProcParamValueGroup_DeactivateCompleted += new EventHandler<WcStoredProcParamValueGroup_DeactivateCompletedEventArgs>(proxy_WcStoredProcParamValueGroup_DeactivateCompleted);
            proxy.WcStoredProcParamValueGroup_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValueGroup_DeactivateCompleted(object sender, WcStoredProcParamValueGroup_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValueGroup_DeactivateCompletedEventArgs> handler = WcStoredProcParamValueGroup_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcStoredProcParamValueGroup_Deactivate

        #region WcStoredProcParamValueGroup_Remove

        public void Begin_WcStoredProcParamValueGroup_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_Remove", IfxTraceCategory.Enter);
                WcStoredProcParamValueGroupServiceClient proxy = new WcStoredProcParamValueGroupServiceClient();
                AssignCredentials(proxy);
                proxy.WcStoredProcParamValueGroup_RemoveCompleted += new EventHandler<WcStoredProcParamValueGroup_RemoveCompletedEventArgs>(proxy_WcStoredProcParamValueGroup_RemoveCompleted);
                proxy.WcStoredProcParamValueGroup_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcStoredProcParamValueGroup_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcStoredProcParamValueGroup_RemoveCompleted(object sender, WcStoredProcParamValueGroup_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcStoredProcParamValueGroup_RemoveCompletedEventArgs> handler = WcStoredProcParamValueGroup_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcParamValueGroup_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcStoredProcParamValueGroup_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


