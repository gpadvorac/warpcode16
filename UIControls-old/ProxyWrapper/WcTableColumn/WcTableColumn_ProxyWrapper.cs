using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/2/2016 1:01:25 PM

namespace ProxyWrapper
{
    public partial class WcTableColumnService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcTableColumn_GetByIdCompletedEventArgs> WcTableColumn_GetByIdCompleted;
        public event System.EventHandler<WcTableColumn_GetAllCompletedEventArgs> WcTableColumn_GetAllCompleted;
        public event System.EventHandler<WcTableColumn_GetListByFKCompletedEventArgs> WcTableColumn_GetListByFKCompleted;
        public event System.EventHandler<WcTableColumn_SaveCompletedEventArgs> WcTableColumn_SaveCompleted;
        public event System.EventHandler<WcTableColumn_DeleteCompletedEventArgs> WcTableColumn_DeleteCompleted;
        public event System.EventHandler<WcTableColumn_SetIsDeletedCompletedEventArgs> WcTableColumn_SetIsDeletedCompleted;
        public event System.EventHandler<WcTableColumn_DeactivateCompletedEventArgs> WcTableColumn_DeactivateCompleted;
        public event System.EventHandler<WcTableColumn_RemoveCompletedEventArgs> WcTableColumn_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcTableColumn_GetById

        public void Begin_WcTableColumn_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetById", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumn_GetByIdCompleted += new EventHandler<WcTableColumn_GetByIdCompletedEventArgs>(proxy_WcTableColumn_GetByIdCompleted);
                proxy.WcTableColumn_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_GetByIdCompleted(object sender, WcTableColumn_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_GetByIdCompletedEventArgs> handler = WcTableColumn_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_GetById

        #region WcTableColumn_GetAll

        public void Begin_WcTableColumn_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetAll", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumn_GetAllCompleted += new EventHandler<WcTableColumn_GetAllCompletedEventArgs>(proxy_WcTableColumn_GetAllCompleted);
                proxy.WcTableColumn_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_GetAllCompleted(object sender, WcTableColumn_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_GetAllCompletedEventArgs> handler = WcTableColumn_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_GetAll

        #region WcTableColumn_GetListByFK

        public void Begin_WcTableColumn_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetListByFK", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumn_GetListByFKCompleted += new EventHandler<WcTableColumn_GetListByFKCompletedEventArgs>(proxy_WcTableColumn_GetListByFKCompleted);
                proxy.WcTableColumn_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_GetListByFKCompleted(object sender, WcTableColumn_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_GetListByFKCompletedEventArgs> handler = WcTableColumn_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_GetListByFK

        #region WcTableColumn_Save

        public void Begin_WcTableColumn_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Save", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                //proxy.WcTableColumn_SaveCompleted += new EventHandler<WcTableColumn_SaveCompletedEventArgs>(proxy_WcTableColumn_SaveCompleted);
                proxy.WcTableColumn_SaveCompleted += new EventHandler<WcTableColumn_SaveCompletedEventArgs>(proxy_WcTableColumn_SaveCompleted);
                proxy.WcTableColumn_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_SaveCompleted(object sender, WcTableColumn_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_SaveCompletedEventArgs> handler = WcTableColumn_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_Save

        #region WcTableColumn_Delete

        public void Begin_WcTableColumn_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_DeleteCompleted", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumn_DeleteCompleted += new EventHandler<WcTableColumn_DeleteCompletedEventArgs>(proxy_WcTableColumn_DeleteCompleted);
                proxy.WcTableColumn_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_DeleteCompleted(object sender, WcTableColumn_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_DeleteCompletedEventArgs> handler = WcTableColumn_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_Delete

        #region WcTableColumn_SetIsDeleted

        public void Begin_WcTableColumn_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumn_SetIsDeletedCompleted += new EventHandler<WcTableColumn_SetIsDeletedCompletedEventArgs>(proxy_WcTableColumn_SetIsDeletedCompleted);
                proxy.WcTableColumn_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_SetIsDeletedCompleted(object sender, WcTableColumn_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_SetIsDeletedCompletedEventArgs> handler = WcTableColumn_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_SetIsDeleted

        #region WcTableColumn_Deactivate

        public void Begin_WcTableColumn_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Deactivate", IfxTraceCategory.Enter);
            WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
            AssignCredentials(proxy);
            proxy.WcTableColumn_DeactivateCompleted += new EventHandler<WcTableColumn_DeactivateCompletedEventArgs>(proxy_WcTableColumn_DeactivateCompleted);
            proxy.WcTableColumn_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_DeactivateCompleted(object sender, WcTableColumn_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_DeactivateCompletedEventArgs> handler = WcTableColumn_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumn_Deactivate

        #region WcTableColumn_Remove

        public void Begin_WcTableColumn_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Remove", IfxTraceCategory.Enter);
                WcTableColumnServiceClient proxy = new WcTableColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumn_RemoveCompleted += new EventHandler<WcTableColumn_RemoveCompletedEventArgs>(proxy_WcTableColumn_RemoveCompleted);
                proxy.WcTableColumn_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumn_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumn_RemoveCompleted(object sender, WcTableColumn_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumn_RemoveCompletedEventArgs> handler = WcTableColumn_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcTableColumn_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


