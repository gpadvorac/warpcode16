using System;
using Ifx.SL;
using Velocity.SL;
using VelocityWCF;

// Gen Timestamp:  12/2/2016 1:01:25 PM

namespace ProxyWrapper
{
    public partial class WcTableColumnService_ProxyWrapper
    {

        #region Initialize Variables

        private static string _as = "UIControls";
        private static string _cn = "WcTableColumnService_ProxyWrapper";

        #endregion Initialize Variables

        #region Constructors

        public WcTableColumnService_ProxyWrapper()
        {
            // create raw proxy
            _rawProxy = new WcTableColumnServiceClient();
            AssignCredentials(_rawProxy);
        }

        //private static void AssignCredentials(DataServiceClient proxy)        Do we nee this to be static?
        //private void AssignCredentials(WcTableColumnServiceClient proxy)
        private void AssignCredentials(WcTableColumnServiceClient proxy)
        {
            //// set user and password on the raw proxy
            //proxy.ClientCredentials.UserName.UserName = "username";
            //proxy.ClientCredentials.UserName.Password = "password";
            CustomBehavior customBehavior = CustomHeader.CreateCustomBehavior("HeaderName", "ns", null, Credentials.AlternateGuid, null, null);
            proxy.Endpoint.Behaviors.Add(customBehavior);
        }

        #endregion Constructors
        
        #region IDisposable Implementation

        private bool disposed = false;

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if(!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources
                if(disposing)
                {
                    // Dispose managed resources.
                    Release();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.
                // CloseHandle(handle);

                // Note disposing has been done.
                disposed = true;
            }
        }

        //// Use C# destructor syntax for finalization code.
        //// This destructor will run only if the Dispose method
        //// does not get called.
        //// It gives your base class the opportunity to finalize.
        //// Do not provide destructors in types derived from this class.
        //~DataServiceAsyncProxyExtender()
        //{
        //    // Do not re-create Dispose clean-up code here.
        //    // Calling Dispose(false) is optimal in terms of
        //    // readability and maintainability.
        //    Dispose(false);
        //}

        #endregion IDisposable Implementation

        #region Resource Management

        public void Release()
        {
            // Is there anything we want to through in here?

            //if (_timer != null)
            //{
            //    _timer.Stop();
            //    _timer = null;
            //}
            //_rawProxy = null;
            //_asyncResult = null;
            //_result = null;
            //_asyncException = null;
        }

        #endregion Resource Management

        #region Fields & Properties

        private WcTableColumnServiceClient _rawProxy = null;

        public WcTableColumnServiceClient RawProxy
        {
            get { return _rawProxy; }
        }

        #endregion Fields & Properties

        #region DataService Specific Support

        // synchronous Is Not Supported..........
        // synchronous IsAuthenticated call
        public bool IsAuthenticated()
        {

            // Need to come up with an Assync way of doing this.

            //// create raw proxy
            //_rawProxy = new DataServiceClient();

            //// assign user and password on the raw proxy
            //DataServiceAsyncProxyExtender.AssignCredentials(_rawProxy);

            //// return sync call results
            //return _rawProxy.IsAuthenticated();
            return true;
        }


        #endregion DataService Specific Support




    }
}


