using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/2/2016 1:01:25 PM

namespace ProxyWrapper
{
    public partial class WcTableSortByService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcTableSortBy_GetByIdCompletedEventArgs> WcTableSortBy_GetByIdCompleted;
        public event System.EventHandler<WcTableSortBy_GetAllCompletedEventArgs> WcTableSortBy_GetAllCompleted;
        public event System.EventHandler<WcTableSortBy_GetListByFKCompletedEventArgs> WcTableSortBy_GetListByFKCompleted;
        public event System.EventHandler<WcTableSortBy_SaveCompletedEventArgs> WcTableSortBy_SaveCompleted;
        public event System.EventHandler<WcTableSortBy_DeleteCompletedEventArgs> WcTableSortBy_DeleteCompleted;
        public event System.EventHandler<WcTableSortBy_SetIsDeletedCompletedEventArgs> WcTableSortBy_SetIsDeletedCompleted;
        public event System.EventHandler<WcTableSortBy_DeactivateCompletedEventArgs> WcTableSortBy_DeactivateCompleted;
        public event System.EventHandler<WcTableSortBy_RemoveCompletedEventArgs> WcTableSortBy_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcTableSortBy_GetById

        public void Begin_WcTableSortBy_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_GetById", IfxTraceCategory.Enter);
                WcTableSortByServiceClient proxy = new WcTableSortByServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableSortBy_GetByIdCompleted += new EventHandler<WcTableSortBy_GetByIdCompletedEventArgs>(proxy_WcTableSortBy_GetByIdCompleted);
                proxy.WcTableSortBy_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableSortBy_GetByIdCompleted(object sender, WcTableSortBy_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableSortBy_GetByIdCompletedEventArgs> handler = WcTableSortBy_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableSortBy_GetById

        #region WcTableSortBy_GetAll

        public void Begin_WcTableSortBy_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_GetAll", IfxTraceCategory.Enter);
                WcTableSortByServiceClient proxy = new WcTableSortByServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableSortBy_GetAllCompleted += new EventHandler<WcTableSortBy_GetAllCompletedEventArgs>(proxy_WcTableSortBy_GetAllCompleted);
                proxy.WcTableSortBy_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableSortBy_GetAllCompleted(object sender, WcTableSortBy_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableSortBy_GetAllCompletedEventArgs> handler = WcTableSortBy_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableSortBy_GetAll

        #region WcTableSortBy_GetListByFK

        public void Begin_WcTableSortBy_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_GetListByFK", IfxTraceCategory.Enter);
                WcTableSortByServiceClient proxy = new WcTableSortByServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableSortBy_GetListByFKCompleted += new EventHandler<WcTableSortBy_GetListByFKCompletedEventArgs>(proxy_WcTableSortBy_GetListByFKCompleted);
                proxy.WcTableSortBy_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableSortBy_GetListByFKCompleted(object sender, WcTableSortBy_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableSortBy_GetListByFKCompletedEventArgs> handler = WcTableSortBy_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableSortBy_GetListByFK

        #region WcTableSortBy_Save

        public void Begin_WcTableSortBy_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_Save", IfxTraceCategory.Enter);
                WcTableSortByServiceClient proxy = new WcTableSortByServiceClient();
                AssignCredentials(proxy);
                //proxy.WcTableSortBy_SaveCompleted += new EventHandler<WcTableSortBy_SaveCompletedEventArgs>(proxy_WcTableSortBy_SaveCompleted);
                proxy.WcTableSortBy_SaveCompleted += new EventHandler<WcTableSortBy_SaveCompletedEventArgs>(proxy_WcTableSortBy_SaveCompleted);
                proxy.WcTableSortBy_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableSortBy_SaveCompleted(object sender, WcTableSortBy_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableSortBy_SaveCompletedEventArgs> handler = WcTableSortBy_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableSortBy_Save

        #region WcTableSortBy_Delete

        public void Begin_WcTableSortBy_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_DeleteCompleted", IfxTraceCategory.Enter);
                WcTableSortByServiceClient proxy = new WcTableSortByServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableSortBy_DeleteCompleted += new EventHandler<WcTableSortBy_DeleteCompletedEventArgs>(proxy_WcTableSortBy_DeleteCompleted);
                proxy.WcTableSortBy_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableSortBy_DeleteCompleted(object sender, WcTableSortBy_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableSortBy_DeleteCompletedEventArgs> handler = WcTableSortBy_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableSortBy_Delete

        #region WcTableSortBy_SetIsDeleted

        public void Begin_WcTableSortBy_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcTableSortByServiceClient proxy = new WcTableSortByServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableSortBy_SetIsDeletedCompleted += new EventHandler<WcTableSortBy_SetIsDeletedCompletedEventArgs>(proxy_WcTableSortBy_SetIsDeletedCompleted);
                proxy.WcTableSortBy_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableSortBy_SetIsDeletedCompleted(object sender, WcTableSortBy_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableSortBy_SetIsDeletedCompletedEventArgs> handler = WcTableSortBy_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableSortBy_SetIsDeleted

        #region WcTableSortBy_Deactivate

        public void Begin_WcTableSortBy_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_Deactivate", IfxTraceCategory.Enter);
            WcTableSortByServiceClient proxy = new WcTableSortByServiceClient();
            AssignCredentials(proxy);
            proxy.WcTableSortBy_DeactivateCompleted += new EventHandler<WcTableSortBy_DeactivateCompletedEventArgs>(proxy_WcTableSortBy_DeactivateCompleted);
            proxy.WcTableSortBy_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableSortBy_DeactivateCompleted(object sender, WcTableSortBy_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableSortBy_DeactivateCompletedEventArgs> handler = WcTableSortBy_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableSortBy_Deactivate

        #region WcTableSortBy_Remove

        public void Begin_WcTableSortBy_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_Remove", IfxTraceCategory.Enter);
                WcTableSortByServiceClient proxy = new WcTableSortByServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableSortBy_RemoveCompleted += new EventHandler<WcTableSortBy_RemoveCompletedEventArgs>(proxy_WcTableSortBy_RemoveCompleted);
                proxy.WcTableSortBy_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableSortBy_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableSortBy_RemoveCompleted(object sender, WcTableSortBy_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableSortBy_RemoveCompletedEventArgs> handler = WcTableSortBy_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableSortBy_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcTableSortBy_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


