using Ifx.SL;
using System;
using vUICommon;

// Gen Timestamp:  9/7/2012 1:38:03 AM

namespace ProxyWrapper
{
    public partial class Task2PersonService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<Task2Person_GetByIdCompletedEventArgs> Task2Person_GetByIdCompleted;
        public event System.EventHandler<Task2Person_GetAllCompletedEventArgs> Task2Person_GetAllCompleted;
        public event System.EventHandler<Task2Person_GetListByFKCompletedEventArgs> Task2Person_GetListByFKCompleted;
        public event System.EventHandler<Task2Person_SaveCompletedEventArgs> Task2Person_SaveCompleted;
        public event System.EventHandler<Task2Person_DeleteCompletedEventArgs> Task2Person_DeleteCompleted;
        public event System.EventHandler<Task2Person_DeactivateCompletedEventArgs> Task2Person_DeactivateCompleted;
        public event System.EventHandler<Task2Person_RemoveCompletedEventArgs> Task2Person_RemoveCompleted;
		public event System.EventHandler<GetPerson_lstAssignedToTaskCompletedEventArgs> GetPerson_lstAssignedToTaskCompleted;
		public event System.EventHandler<GetPerson_lstNotAssignedToTaskCompletedEventArgs> GetPerson_lstNotAssignedToTaskCompleted;
		public event System.EventHandler<ExecuteTask2Person_assignPersonCompletedEventArgs> ExecuteTask2Person_assignPersonCompleted;
		public event System.EventHandler<ExecuteTask2Person_removePersonCompletedEventArgs> ExecuteTask2Person_removePersonCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region Task2Person_GetById

        public void Begin_Task2Person_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_GetById", IfxTraceCategory.Enter);
                Task2PersonServiceClient proxy = new Task2PersonServiceClient();
                AssignCredentials(proxy);
                proxy.Task2Person_GetByIdCompleted += new EventHandler<Task2Person_GetByIdCompletedEventArgs>(proxy_Task2Person_GetByIdCompleted);
                proxy.Task2Person_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_GetById", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task2Person_GetByIdCompleted(object sender, Task2Person_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task2Person_GetByIdCompletedEventArgs> handler = Task2Person_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Task2Person_GetById

        #region Task2Person_GetAll

        public void Begin_Task2Person_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_GetAll", IfxTraceCategory.Enter);
                Task2PersonServiceClient proxy = new Task2PersonServiceClient();
                AssignCredentials(proxy);
                proxy.Task2Person_GetAllCompleted += new EventHandler<Task2Person_GetAllCompletedEventArgs>(proxy_Task2Person_GetAllCompleted);
                proxy.Task2Person_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_GetAll", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task2Person_GetAllCompleted(object sender, Task2Person_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task2Person_GetAllCompletedEventArgs> handler = Task2Person_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Task2Person_GetAll

        #region Task2Person_GetListByFK

        public void Begin_Task2Person_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_GetListByFK", IfxTraceCategory.Enter);
                Task2PersonServiceClient proxy = new Task2PersonServiceClient();
                AssignCredentials(proxy);
                proxy.Task2Person_GetListByFKCompleted += new EventHandler<Task2Person_GetListByFKCompletedEventArgs>(proxy_Task2Person_GetListByFKCompleted);
                proxy.Task2Person_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task2Person_GetListByFKCompleted(object sender, Task2Person_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task2Person_GetListByFKCompletedEventArgs> handler = Task2Person_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Task2Person_GetListByFK

        #region Task2Person_Save

        public void Begin_Task2Person_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_Save", IfxTraceCategory.Enter);
                Task2PersonServiceClient proxy = new Task2PersonServiceClient();
                AssignCredentials(proxy);
                //proxy.Task2Person_SaveCompleted += new EventHandler<Task2Person_SaveCompletedEventArgs>(proxy_Task2Person_SaveCompleted);
                proxy.Task2Person_SaveCompleted += new EventHandler<Task2Person_SaveCompletedEventArgs>(proxy_Task2Person_SaveCompleted);
                proxy.Task2Person_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_Save", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task2Person_SaveCompleted(object sender, Task2Person_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task2Person_SaveCompletedEventArgs> handler = Task2Person_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Task2Person_Save

        #region Task2Person_Delete

        public void Begin_Task2Person_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_DeleteCompleted", IfxTraceCategory.Enter);
                Task2PersonServiceClient proxy = new Task2PersonServiceClient();
                AssignCredentials(proxy);
                proxy.Task2Person_DeleteCompleted += new EventHandler<Task2Person_DeleteCompletedEventArgs>(proxy_Task2Person_DeleteCompleted);
                proxy.Task2Person_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task2Person_DeleteCompleted(object sender, Task2Person_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task2Person_DeleteCompletedEventArgs> handler = Task2Person_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Task2Person_Delete

        #region Task2Person_Deactivate

        public void Begin_Task2Person_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_Deactivate", IfxTraceCategory.Enter);
            Task2PersonServiceClient proxy = new Task2PersonServiceClient();
            AssignCredentials(proxy);
            proxy.Task2Person_DeactivateCompleted += new EventHandler<Task2Person_DeactivateCompletedEventArgs>(proxy_Task2Person_DeactivateCompleted);
            proxy.Task2Person_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_Deactivate", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task2Person_DeactivateCompleted(object sender, Task2Person_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task2Person_DeactivateCompletedEventArgs> handler = Task2Person_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion Task2Person_Deactivate

        #region Task2Person_Remove

        public void Begin_Task2Person_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_Remove", IfxTraceCategory.Enter);
                Task2PersonServiceClient proxy = new Task2PersonServiceClient();
                AssignCredentials(proxy);
                proxy.Task2Person_RemoveCompleted += new EventHandler<Task2Person_RemoveCompletedEventArgs>(proxy_Task2Person_RemoveCompleted);
                proxy.Task2Person_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_Remove", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_Task2Person_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_Task2Person_RemoveCompleted(object sender, Task2Person_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<Task2Person_RemoveCompletedEventArgs> handler = Task2Person_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task2Person_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion Task2Person_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #region GetPerson_lstAssignedToTask

        public void Begin_GetPerson_lstAssignedToTask(Guid Tk_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_lstAssignedToTask", IfxTraceCategory.Enter);
                Task2PersonServiceClient proxy = new Task2PersonServiceClient();
                AssignCredentials(proxy);
                proxy.GetPerson_lstAssignedToTaskCompleted += new EventHandler<GetPerson_lstAssignedToTaskCompletedEventArgs>(proxy_GetPerson_lstAssignedToTaskCompleted);
                proxy.GetPerson_lstAssignedToTaskAsync(Tk_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_lstAssignedToTask", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_lstAssignedToTask", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetPerson_lstAssignedToTaskCompleted(object sender, GetPerson_lstAssignedToTaskCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstAssignedToTask", IfxTraceCategory.Enter);
                System.EventHandler<GetPerson_lstAssignedToTaskCompletedEventArgs> handler = GetPerson_lstAssignedToTaskCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstAssignedToTask", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstAssignedToTask", IfxTraceCategory.Leave);
            }
        }

        #endregion GetPerson_lstAssignedToTask

        #region GetPerson_lstNotAssignedToTask

        public void Begin_GetPerson_lstNotAssignedToTask(Guid Tk_Id, Guid Pj_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_lstNotAssignedToTask", IfxTraceCategory.Enter);
                Task2PersonServiceClient proxy = new Task2PersonServiceClient();
                AssignCredentials(proxy);
                proxy.GetPerson_lstNotAssignedToTaskCompleted += new EventHandler<GetPerson_lstNotAssignedToTaskCompletedEventArgs>(proxy_GetPerson_lstNotAssignedToTaskCompleted);
                proxy.GetPerson_lstNotAssignedToTaskAsync(Tk_Id, Pj_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_lstNotAssignedToTask", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_GetPerson_lstNotAssignedToTask", IfxTraceCategory.Leave);
            }
        }

        void proxy_GetPerson_lstNotAssignedToTaskCompleted(object sender, GetPerson_lstNotAssignedToTaskCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToTask", IfxTraceCategory.Enter);
                System.EventHandler<GetPerson_lstNotAssignedToTaskCompletedEventArgs> handler = GetPerson_lstNotAssignedToTaskCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToTask", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_lstNotAssignedToTask", IfxTraceCategory.Leave);
            }
        }

        #endregion GetPerson_lstNotAssignedToTask

        #region ExecuteTask2Person_assignPerson

        public void Begin_ExecuteTask2Person_assignPerson(Guid Tk2Pn_Tk_Id, Guid Tk2Pn_Pn_Id, Guid Tk2Pn_UserId )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask2Person_assignPerson", IfxTraceCategory.Enter);
                Task2PersonServiceClient proxy = new Task2PersonServiceClient();
                AssignCredentials(proxy);
                proxy.ExecuteTask2Person_assignPersonCompleted += new EventHandler<ExecuteTask2Person_assignPersonCompletedEventArgs>(proxy_ExecuteTask2Person_assignPersonCompleted);
                proxy.ExecuteTask2Person_assignPersonAsync(Tk2Pn_Tk_Id, Tk2Pn_Pn_Id, Tk2Pn_UserId );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask2Person_assignPerson", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask2Person_assignPerson", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecuteTask2Person_assignPersonCompleted(object sender, ExecuteTask2Person_assignPersonCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_assignPerson", IfxTraceCategory.Enter);
                System.EventHandler<ExecuteTask2Person_assignPersonCompletedEventArgs> handler = ExecuteTask2Person_assignPersonCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_assignPerson", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_assignPerson", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecuteTask2Person_assignPerson

        #region ExecuteTask2Person_removePerson

        public void Begin_ExecuteTask2Person_removePerson(Guid Tk2Pn_Tk_Id, Guid Tk2Pn_Pn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask2Person_removePerson", IfxTraceCategory.Enter);
                Task2PersonServiceClient proxy = new Task2PersonServiceClient();
                AssignCredentials(proxy);
                proxy.ExecuteTask2Person_removePersonCompleted += new EventHandler<ExecuteTask2Person_removePersonCompletedEventArgs>(proxy_ExecuteTask2Person_removePersonCompleted);
                proxy.ExecuteTask2Person_removePersonAsync(Tk2Pn_Tk_Id, Tk2Pn_Pn_Id );
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask2Person_removePerson", ex);
				throw IfxWrapperException.GetError(ex, (Guid)traceId);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_ExecuteTask2Person_removePerson", IfxTraceCategory.Leave);
            }
        }

        void proxy_ExecuteTask2Person_removePersonCompleted(object sender, ExecuteTask2Person_removePersonCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_removePerson", IfxTraceCategory.Enter);
                System.EventHandler<ExecuteTask2Person_removePersonCompletedEventArgs> handler = ExecuteTask2Person_removePersonCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_removePerson", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask2Person_removePerson", IfxTraceCategory.Leave);
            }
        }

        #endregion ExecuteTask2Person_removePerson

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


