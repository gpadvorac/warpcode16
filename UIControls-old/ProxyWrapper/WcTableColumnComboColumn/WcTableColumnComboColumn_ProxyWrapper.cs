using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  1/3/2017 11:12:36 PM

namespace ProxyWrapper
{
    public partial class WcTableColumnComboColumnService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcTableColumnComboColumn_GetByIdCompletedEventArgs> WcTableColumnComboColumn_GetByIdCompleted;
        public event System.EventHandler<WcTableColumnComboColumn_GetAllCompletedEventArgs> WcTableColumnComboColumn_GetAllCompleted;
        public event System.EventHandler<WcTableColumnComboColumn_GetListByFKCompletedEventArgs> WcTableColumnComboColumn_GetListByFKCompleted;
        public event System.EventHandler<WcTableColumnComboColumn_SaveCompletedEventArgs> WcTableColumnComboColumn_SaveCompleted;
        public event System.EventHandler<WcTableColumnComboColumn_DeleteCompletedEventArgs> WcTableColumnComboColumn_DeleteCompleted;
        public event System.EventHandler<WcTableColumnComboColumn_SetIsDeletedCompletedEventArgs> WcTableColumnComboColumn_SetIsDeletedCompleted;
        public event System.EventHandler<WcTableColumnComboColumn_DeactivateCompletedEventArgs> WcTableColumnComboColumn_DeactivateCompleted;
        public event System.EventHandler<WcTableColumnComboColumn_RemoveCompletedEventArgs> WcTableColumnComboColumn_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcTableColumnComboColumn_GetById

        public void Begin_WcTableColumnComboColumn_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_GetById", IfxTraceCategory.Enter);
                WcTableColumnComboColumnServiceClient proxy = new WcTableColumnComboColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumnComboColumn_GetByIdCompleted += new EventHandler<WcTableColumnComboColumn_GetByIdCompletedEventArgs>(proxy_WcTableColumnComboColumn_GetByIdCompleted);
                proxy.WcTableColumnComboColumn_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnComboColumn_GetByIdCompleted(object sender, WcTableColumnComboColumn_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnComboColumn_GetByIdCompletedEventArgs> handler = WcTableColumnComboColumn_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnComboColumn_GetById

        #region WcTableColumnComboColumn_GetAll

        public void Begin_WcTableColumnComboColumn_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_GetAll", IfxTraceCategory.Enter);
                WcTableColumnComboColumnServiceClient proxy = new WcTableColumnComboColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumnComboColumn_GetAllCompleted += new EventHandler<WcTableColumnComboColumn_GetAllCompletedEventArgs>(proxy_WcTableColumnComboColumn_GetAllCompleted);
                proxy.WcTableColumnComboColumn_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnComboColumn_GetAllCompleted(object sender, WcTableColumnComboColumn_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnComboColumn_GetAllCompletedEventArgs> handler = WcTableColumnComboColumn_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnComboColumn_GetAll

        #region WcTableColumnComboColumn_GetListByFK

        public void Begin_WcTableColumnComboColumn_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_GetListByFK", IfxTraceCategory.Enter);
                WcTableColumnComboColumnServiceClient proxy = new WcTableColumnComboColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumnComboColumn_GetListByFKCompleted += new EventHandler<WcTableColumnComboColumn_GetListByFKCompletedEventArgs>(proxy_WcTableColumnComboColumn_GetListByFKCompleted);
                proxy.WcTableColumnComboColumn_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnComboColumn_GetListByFKCompleted(object sender, WcTableColumnComboColumn_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnComboColumn_GetListByFKCompletedEventArgs> handler = WcTableColumnComboColumn_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnComboColumn_GetListByFK

        #region WcTableColumnComboColumn_Save

        public void Begin_WcTableColumnComboColumn_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_Save", IfxTraceCategory.Enter);
                WcTableColumnComboColumnServiceClient proxy = new WcTableColumnComboColumnServiceClient();
                AssignCredentials(proxy);
                //proxy.WcTableColumnComboColumn_SaveCompleted += new EventHandler<WcTableColumnComboColumn_SaveCompletedEventArgs>(proxy_WcTableColumnComboColumn_SaveCompleted);
                proxy.WcTableColumnComboColumn_SaveCompleted += new EventHandler<WcTableColumnComboColumn_SaveCompletedEventArgs>(proxy_WcTableColumnComboColumn_SaveCompleted);
                proxy.WcTableColumnComboColumn_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnComboColumn_SaveCompleted(object sender, WcTableColumnComboColumn_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnComboColumn_SaveCompletedEventArgs> handler = WcTableColumnComboColumn_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnComboColumn_Save

        #region WcTableColumnComboColumn_Delete

        public void Begin_WcTableColumnComboColumn_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_DeleteCompleted", IfxTraceCategory.Enter);
                WcTableColumnComboColumnServiceClient proxy = new WcTableColumnComboColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumnComboColumn_DeleteCompleted += new EventHandler<WcTableColumnComboColumn_DeleteCompletedEventArgs>(proxy_WcTableColumnComboColumn_DeleteCompleted);
                proxy.WcTableColumnComboColumn_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnComboColumn_DeleteCompleted(object sender, WcTableColumnComboColumn_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnComboColumn_DeleteCompletedEventArgs> handler = WcTableColumnComboColumn_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnComboColumn_Delete

        #region WcTableColumnComboColumn_SetIsDeleted

        public void Begin_WcTableColumnComboColumn_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcTableColumnComboColumnServiceClient proxy = new WcTableColumnComboColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumnComboColumn_SetIsDeletedCompleted += new EventHandler<WcTableColumnComboColumn_SetIsDeletedCompletedEventArgs>(proxy_WcTableColumnComboColumn_SetIsDeletedCompleted);
                proxy.WcTableColumnComboColumn_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnComboColumn_SetIsDeletedCompleted(object sender, WcTableColumnComboColumn_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnComboColumn_SetIsDeletedCompletedEventArgs> handler = WcTableColumnComboColumn_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnComboColumn_SetIsDeleted

        #region WcTableColumnComboColumn_Deactivate

        public void Begin_WcTableColumnComboColumn_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_Deactivate", IfxTraceCategory.Enter);
            WcTableColumnComboColumnServiceClient proxy = new WcTableColumnComboColumnServiceClient();
            AssignCredentials(proxy);
            proxy.WcTableColumnComboColumn_DeactivateCompleted += new EventHandler<WcTableColumnComboColumn_DeactivateCompletedEventArgs>(proxy_WcTableColumnComboColumn_DeactivateCompleted);
            proxy.WcTableColumnComboColumn_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnComboColumn_DeactivateCompleted(object sender, WcTableColumnComboColumn_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnComboColumn_DeactivateCompletedEventArgs> handler = WcTableColumnComboColumn_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableColumnComboColumn_Deactivate

        #region WcTableColumnComboColumn_Remove

        public void Begin_WcTableColumnComboColumn_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_Remove", IfxTraceCategory.Enter);
                WcTableColumnComboColumnServiceClient proxy = new WcTableColumnComboColumnServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableColumnComboColumn_RemoveCompleted += new EventHandler<WcTableColumnComboColumn_RemoveCompletedEventArgs>(proxy_WcTableColumnComboColumn_RemoveCompleted);
                proxy.WcTableColumnComboColumn_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableColumnComboColumn_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableColumnComboColumn_RemoveCompleted(object sender, WcTableColumnComboColumn_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableColumnComboColumn_RemoveCompletedEventArgs> handler = WcTableColumnComboColumn_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumnComboColumn_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcTableColumnComboColumn_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


