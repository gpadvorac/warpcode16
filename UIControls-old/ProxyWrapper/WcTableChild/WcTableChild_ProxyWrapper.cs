using System;
using Ifx.SL;
using vUICommon;

// Gen Timestamp:  12/2/2016 1:01:24 PM

namespace ProxyWrapper
{
    public partial class WcTableChildService_ProxyWrapper
    {


        #region Events

        public event System.EventHandler<WcTableChild_GetByIdCompletedEventArgs> WcTableChild_GetByIdCompleted;
        public event System.EventHandler<WcTableChild_GetAllCompletedEventArgs> WcTableChild_GetAllCompleted;
        public event System.EventHandler<WcTableChild_GetListByFKCompletedEventArgs> WcTableChild_GetListByFKCompleted;
        public event System.EventHandler<WcTableChild_SaveCompletedEventArgs> WcTableChild_SaveCompleted;
        public event System.EventHandler<WcTableChild_DeleteCompletedEventArgs> WcTableChild_DeleteCompleted;
        public event System.EventHandler<WcTableChild_SetIsDeletedCompletedEventArgs> WcTableChild_SetIsDeletedCompleted;
        public event System.EventHandler<WcTableChild_DeactivateCompletedEventArgs> WcTableChild_DeactivateCompleted;
        public event System.EventHandler<WcTableChild_RemoveCompletedEventArgs> WcTableChild_RemoveCompleted;

        #endregion Events

        #region Service Calls

        #region Standard Service Calls

        #region WcTableChild_GetById

        public void Begin_WcTableChild_GetById(Guid Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_GetById", IfxTraceCategory.Enter);
                WcTableChildServiceClient proxy = new WcTableChildServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableChild_GetByIdCompleted += new EventHandler<WcTableChild_GetByIdCompletedEventArgs>(proxy_WcTableChild_GetByIdCompleted);
                proxy.WcTableChild_GetByIdAsync(Id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_GetById", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_GetById", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableChild_GetByIdCompleted(object sender, WcTableChild_GetByIdCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetByIdCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableChild_GetByIdCompletedEventArgs> handler = WcTableChild_GetByIdCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetByIdCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetByIdCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableChild_GetById

        #region WcTableChild_GetAll

        public void Begin_WcTableChild_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_GetAll", IfxTraceCategory.Enter);
                WcTableChildServiceClient proxy = new WcTableChildServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableChild_GetAllCompleted += new EventHandler<WcTableChild_GetAllCompletedEventArgs>(proxy_WcTableChild_GetAllCompleted);
                proxy.WcTableChild_GetAllAsync();
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_GetAll", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_GetAll", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableChild_GetAllCompleted(object sender, WcTableChild_GetAllCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetAllCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableChild_GetAllCompletedEventArgs> handler = WcTableChild_GetAllCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetAllCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetAllCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableChild_GetAll

        #region WcTableChild_GetListByFK

        public void Begin_WcTableChild_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_GetListByFK", IfxTraceCategory.Enter);
                WcTableChildServiceClient proxy = new WcTableChildServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableChild_GetListByFKCompleted += new EventHandler<WcTableChild_GetListByFKCompletedEventArgs>(proxy_WcTableChild_GetListByFKCompleted);
                proxy.WcTableChild_GetListByFKAsync(id);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_GetListByFK", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableChild_GetListByFKCompleted(object sender, WcTableChild_GetListByFKCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetListByFKCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableChild_GetListByFKCompletedEventArgs> handler = WcTableChild_GetListByFKCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetListByFKCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_GetListByFKCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableChild_GetListByFK

        #region WcTableChild_Save

        public void Begin_WcTableChild_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_Save", IfxTraceCategory.Enter);
                WcTableChildServiceClient proxy = new WcTableChildServiceClient();
                AssignCredentials(proxy);
                //proxy.WcTableChild_SaveCompleted += new EventHandler<WcTableChild_SaveCompletedEventArgs>(proxy_WcTableChild_SaveCompleted);
                proxy.WcTableChild_SaveCompleted += new EventHandler<WcTableChild_SaveCompletedEventArgs>(proxy_WcTableChild_SaveCompleted);
                proxy.WcTableChild_SaveAsync(data, check);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_Save", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_Save", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableChild_SaveCompleted(object sender, WcTableChild_SaveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_SaveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableChild_SaveCompletedEventArgs> handler = WcTableChild_SaveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_SaveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_SaveCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableChild_Save

        #region WcTableChild_Delete

        public void Begin_WcTableChild_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_DeleteCompleted", IfxTraceCategory.Enter);
                WcTableChildServiceClient proxy = new WcTableChildServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableChild_DeleteCompleted += new EventHandler<WcTableChild_DeleteCompletedEventArgs>(proxy_WcTableChild_DeleteCompleted);
                proxy.WcTableChild_DeleteAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_DeleteCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableChild_DeleteCompleted(object sender, WcTableChild_DeleteCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_DeleteCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableChild_DeleteCompletedEventArgs> handler = WcTableChild_DeleteCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_DeleteCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_DeleteCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableChild_Delete

        #region WcTableChild_SetIsDeleted

        public void Begin_WcTableChild_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                WcTableChildServiceClient proxy = new WcTableChildServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableChild_SetIsDeletedCompleted += new EventHandler<WcTableChild_SetIsDeletedCompletedEventArgs>(proxy_WcTableChild_SetIsDeletedCompleted);
                proxy.WcTableChild_SetIsDeletedAsync(Id, IsDeleted);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_SetIsDeletedCompleted", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableChild_SetIsDeletedCompleted(object sender, WcTableChild_SetIsDeletedCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_SetIsDeletedCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableChild_SetIsDeletedCompletedEventArgs> handler = WcTableChild_SetIsDeletedCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_SetIsDeletedCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_SetIsDeletedCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableChild_SetIsDeleted

        #region WcTableChild_Deactivate

        public void Begin_WcTableChild_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_Deactivate", IfxTraceCategory.Enter);
            WcTableChildServiceClient proxy = new WcTableChildServiceClient();
            AssignCredentials(proxy);
            proxy.WcTableChild_DeactivateCompleted += new EventHandler<WcTableChild_DeactivateCompletedEventArgs>(proxy_WcTableChild_DeactivateCompleted);
            proxy.WcTableChild_DeactivateAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_Deactivate", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_Deactivate", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableChild_DeactivateCompleted(object sender, WcTableChild_DeactivateCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_DeactivateCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableChild_DeactivateCompletedEventArgs> handler = WcTableChild_DeactivateCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_DeactivateCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_DeactivateCompleted", IfxTraceCategory.Leave);
            }
        }

        #endregion WcTableChild_Deactivate

        #region WcTableChild_Remove

        public void Begin_WcTableChild_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_Remove", IfxTraceCategory.Enter);
                WcTableChildServiceClient proxy = new WcTableChildServiceClient();
                AssignCredentials(proxy);
                proxy.WcTableChild_RemoveCompleted += new EventHandler<WcTableChild_RemoveCompletedEventArgs>(proxy_WcTableChild_RemoveCompleted);
                proxy.WcTableChild_RemoveAsync(data);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_Remove", ex);
				throw IfxWrapperException.GetError(ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Begin_WcTableChild_Remove", IfxTraceCategory.Leave);
            }
        }

        void proxy_WcTableChild_RemoveCompleted(object sender, WcTableChild_RemoveCompletedEventArgs e)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_RemoveCompleted", IfxTraceCategory.Enter);
                System.EventHandler<WcTableChild_RemoveCompletedEventArgs> handler = WcTableChild_RemoveCompleted;
                if (handler != null)
                {
                    handler(sender, e);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_RemoveCompleted", ex);
                ExceptionHelper.NotifyUserAnExceptionOccured();
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableChild_RemoveCompleted", IfxTraceCategory.Leave);
            }
        }
        #endregion WcTableChild_Remove

        #endregion Standard Service Calls

        #region Other Service Calls
    
        #region ReadOnlyStaticLists


        // No static lists


        #endregion ReadOnlyStaticLists

        #endregion Other Service Calls

        #endregion Service Calls


    }
}


