﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Velocity.SL;

namespace UIControls
{
    public class UserIdToEditVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var userId = value as Guid?;
            if (userId == null)
            {
                return null;
            }

            var result = userId == Credentials.UserId
                ? Visibility.Visible
                : Visibility.Collapsed;

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}