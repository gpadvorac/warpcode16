﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using EntityWireTypeSL;

namespace UIControls
{
    public class FileNameToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var file= (FileStorage_Values) value;
            string extension = Path.GetExtension(file.FS_FileName);

            if (extension != null)
            {
                extension = extension.Remove(0, 1);
            }

            var source = new Uri(GetImagePathFromExtencion(extension), UriKind.RelativeOrAbsolute);
            return source;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private string GetImagePathFromExtencion(string extension)
        {
            switch (extension.ToLower())
            {
                case "log":
                    return "/UIControls;component/images/FileTXT.png";
                case "txt":
                    return "/UIControls;component/images/FileTXT.png";

                case "doc":
                    return "/UIControls;component/images/FileWord.png";
                case "rtf":
                    return "/UIControls;component/images/FileWord.png";
                case "docx":
                    return "/UIControls;component/images/FileWord.png";
                case "docm":
                    return "/UIControls;component/images/FileWord.png";
                case "dotx":
                    return "/UIControls;component/images/FileWord.png";
                case "dotm":
                    return "/UIControls;component/images/FileWord.png";

                case "pdf":
                    return "/UIControls;component/images/FilePDF.png";
                case "fdf":
                    return "/UIControls;component/images/FilePDF.png";

                case "xls":
                    return "/UIControls;component/images/FileExcel.png";
                case "xlsx":
                    return "/UIControls;component/images/FileExcel.png";
                case "xlsm":
                    return "/UIControls;component/images/FileExcel.png";
                case "xlsb":
                    return "/UIControls;component/images/FileExcel.png";
                //case "mhtml":
                //    return "/UIControls;component/images/FileExcel.png";
                case "sltx":
                    return "/UIControls;component/images/FileExcel.png";
                case "xltm":
                    return "/UIControls;component/images/FileExcel.png";
                case "xlt":
                    return "/UIControls;component/images/FileExcel.png";
                case "csv":
                    return "/UIControls;component/images/FileExcel.png";

                case "gif":
                    return "/UIControls;component/images/FileGif.png";

                case "jpg":
                    return "/UIControls;component/images/FileJPG.png";
                case "jpeg":
                    return "/UIControls;component/images/FileJPG.png";
                case "jpe":
                    return "/UIControls;component/images/FileJPG.png";

                case "png":
                    return "/UIControls;component/images/FilePNG.png";

                case "tiff":
                    return "/UIControls;component/images/FileIMG.png";
                case "tif":
                    return "/UIControls;component/images/FileIMG.png";
                case "bmp":
                    return "/UIControls;component/images/FileIMG.png";
                case "cur":
                    return "/UIControls;component/images/FileIMG.png";
                case "ico":
                    return "/UIControls;component/images/FileIMG.png";
            }
            return "/UIControls;component/images/File.png";
        }

    }
}