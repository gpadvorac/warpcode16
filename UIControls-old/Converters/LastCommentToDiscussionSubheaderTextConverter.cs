﻿using System;
using System.Globalization;
using System.Windows.Data;
using EntityBll.SL;

namespace UIControls
{
    public class LastCommentToDiscussionSubheaderTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var comment = (DiscussionComment_Bll)value;

            return string.Format("Last Comment Added by {0} on {1}", comment.UserName, comment.DscCm_CreatedDate);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}