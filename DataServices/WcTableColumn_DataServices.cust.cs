using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using EntityWireType;
using TypeServices;
using Ifx;
using vDataServices;

namespace DataServices
{
    public partial class WcTableColumn_DataServices
    {

        #region Initialize Variables



        #endregion Initialize Variables

        private static bool _returnCurrentRowOnInsertUpdate = false;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }


        public static object[] ExecuteWcTableColumn_Check_UnCheckColumns(String Crit, String Column, Boolean IsChecked, Guid UserId)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_Check_UnCheckColumns", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTableColumn_Check_UnCheckColumns";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Crit", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Column", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsChecked", SqlDbType.Bit).Direction = ParameterDirection.InputOutput;
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@Crit"].Value = Crit;
                cmd.Parameters["@Column"].Value = Column;
                cmd.Parameters["@IsChecked"].Value = IsChecked;
                cmd.Parameters["@UserId"].Value = UserId;
                cmd.ExecuteNonQuery();
                IsChecked = ((Boolean)(cmd.Parameters["@IsChecked"].Value));
                Int32 Success = ((Int32)(cmd.Parameters["@Success"].Value));
                //Guid ErrorLogId = ((Guid)(cmd.Parameters["@ErrorLogId"].Value));
                object[] ret = new object[3];
                ret[0] = IsChecked;
                ret[1] = Column;
                ret[2] = Success;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_Check_UnCheckColumns", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_Check_UnCheckColumns", IfxTraceCategory.Leave);
            }
        }






    }
}


