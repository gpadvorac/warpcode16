using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using EntityWireType;
using TypeServices;
using Ifx;
using vDataServices;

namespace DataServices
{
    public partial class WcCodeGen_DataServices
    {

        #region Initialize Variables



        #endregion Initialize Variables

        private static bool _returnCurrentRowOnInsertUpdate = false;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }


        public static object[] GetTable_TableColumnMetadata(Guid apVrsn_Id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTable_TableColumnMetadata", new ValuePair[] { new ValuePair("apVrsn_Id", apVrsn_Id) }, IfxTraceCategory.Enter);
                object[] data = new object[7];
                conn = DBConnectoinHelper.GetDBConnection();

                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcApplicationVersion_row", new SqlParameter("@Id", apVrsn_Id));
                data[0] = ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

                rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcCg_Tables", new SqlParameter("@ApVrsn_Id", apVrsn_Id));
                data[1] = ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

                rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcCg_TableSortBys", new SqlParameter("@ApVrsn_Id", apVrsn_Id));
                data[2] = ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

                rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcCg_ChildTables", new SqlParameter("@ApVrsn_Id", apVrsn_Id));
                data[3] = ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

                rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcCg_TableColumns", new SqlParameter("@ApVrsn_Id", apVrsn_Id));
                data[4] = ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

                rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcCg_ToolTips", new SqlParameter("@ApVrsn_Id", apVrsn_Id));
                data[5] = ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

                rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcCg_TableColumnComboColumn", new SqlParameter("@ApVrsn_Id", apVrsn_Id));
                data[6] = ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

                return data;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTable_TableColumnMetadata", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTable_TableColumnMetadata", IfxTraceCategory.Leave);
            }
        }








    }
}


