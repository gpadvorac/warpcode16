using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using EntityWireType;
using TypeServices;
using Ifx;
using TypeServicesCodeGen;
using vDataServices;

namespace DataServices
{
    public partial class WcStoredProc_DataServices
    {

        #region Initialize Variables

        private static bool _returnCurrentRowOnInsertUpdate = false;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }

        #endregion Initialize Variables

        
        public static object[] ExecuteWcStoredProc_importFromSourceDb(Guid ApVrsn_Id, Guid? DbCnSK_Id, String SprocName, Int32 ImportType, Guid UserId, Guid Sp_Id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcStoredProc_importFromSourceDb", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProc_importFromSourceDb";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@ApVrsn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@DbCnSK_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@SprocName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@ImportType", SqlDbType.Int).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Sp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@ApVrsn_Id"].Value = ApVrsn_Id;
                cmd.Parameters["@DbCnSK_Id"].Value = DbCnSK_Id;
                cmd.Parameters["@SprocName"].Value = SprocName;
                cmd.Parameters["@ImportType"].Value = ImportType;
                cmd.Parameters["@UserId"].Value = UserId;
                cmd.Parameters["@Sp_Id"].Value = Sp_Id;
                cmd.ExecuteNonQuery();
                Guid sp_Id = ((Guid)(cmd.Parameters["@Sp_Id"].Value));
                Int32 Success = ((Int32)(cmd.Parameters["@Success"].Value));
                if (Success == 1 && ImportType == 4)
                {
                    // Cant import columns here as its dependant on an associated table and or a linked AppDbSchema, both of which have not been assigned yet.
                    //// ImportType 4 is the only one which requires collecting TableColumn metadata.
                    Success = RefreshStoredProcedureColumns(Sp_Id, DbCnSK_Id, UserId, 4);
                }
                object[] ret = new object[2];
                ret[0] = Sp_Id;
                ret[1] = Success;

                return ret;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcStoredProc_importFromSourceDb", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcStoredProc_importFromSourceDb", IfxTraceCategory.Leave);
            }
        }
        


        ///// <summary>
        ///// This will either refresh the columns, or delete all columns and re-import them.
        ///// A refresh will update the column order.
        ///// </summary>
        ///// <param name="sp_Id"></param>
        ///// <param name="DbCnSK_Id"></param>
        ///// <param name="userId"></param>
        ///// <param name="refreshType">1 = Remove orphaned columns, import missing columns.  2 = Delete and Re-Import all columns</param>
        ///// <returns></returns>
        //public static int RefreshStoredProcedureParameters(Guid sp_Id, Guid? DbCnSK_Id, Guid userId, int refreshType)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    SqlConnection conn = null;
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshStoredProcedureParameters", IfxTraceCategory.Enter);

        //        //Get an instance of this sproc now and pass it around rather than just it's ID as other sproc props will be needed as well
        //        object[] result = WcStoredProc_GetById(sp_Id);
        //        if (result == null)
        //        {
        //            // return a msg that we could not get an instance of the sproc
        //        }
        //        WcStoredProc_Values sproc = new WcStoredProc_Values(result, null);

        //        // Get metadata from DbDataReader SchemaTable instead
        //        DataTable tbResultSet = GetDataReaderSchemaTable(sproc);

        //        if (tbResultSet == null)
        //        {
        //            throw new Exception("tbResultSet is null.  We should have returned something from executing the stored procedure " + sproc.Sp_Name);
        //        }

        //        // Get metadata from DbDataReader SchemaTable instead
        //        int iSuccess = 1;

        //        List<WcStoredProcColumn_Values> spCols = AddSprocColumnMetaData_FromDataReaderSchemaTable(tbResultSet, sp_Id);

        //        if (spCols == null)
        //        {
        //            throw new Exception("List<WcStoredProcColumn_Values> spCols is null.  We should have returned something from executing the stored procedure " + sproc.Sp_Name);
        //        }
        //        iSuccess = StoredProcColumns_insertFromTVP(spCols, userId);

        //        if (iSuccess < 1)
        //        {
        //            Debugger.Break();
        //            throw new Exception("StoredProcColumns_insertFromTVP was not successful for: " + sproc.Sp_Name);
        //        }
        //        return iSuccess;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshStoredProcedureParameters", ex);
        //        return 0;
        //    }
        //    finally
        //    {
        //        if (conn != null)
        //        {
        //            conn.Dispose();
        //        }
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshStoredProcedureParameters", IfxTraceCategory.Leave);
        //    }
        //}
        


        /// <summary>
        /// This will either refresh the columns, or delete all columns and re-import them.
        /// A refresh will update the column order.
        /// </summary>
        /// <param name="sp_Id"></param>
        /// <param name="DbCnSK_Id"></param>
        /// <param name="userId"></param>
        /// <param name="refreshType">1 = Remove orphaned columns, import missing columns.  2 = Delete and Re-Import all columns</param>
        /// <returns></returns>
        public static int RefreshStoredProcedureColumns(Guid sp_Id, Guid? DbCnSK_Id, Guid userId, int refreshType)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshStoredProcedureColumns", IfxTraceCategory.Enter);

                //Get an instance of this sproc now and pass it around rather than just it's ID as other sproc props will be needed as well
                object[] result = WcStoredProc_GetById(sp_Id);
                if (result == null)
                {
                    // return a msg that we could not get an instance of the sproc
                }
                object[] data = result[0] as object[];
                if (data == null)
                {
                    // return a msg that we could not get an instance of the sproc
                }

                WcStoredProc_Values sproc = new WcStoredProc_Values(data, null);

                // Get metadata from DbDataReader SchemaTable instead
                DataTable tbResultSet = GetDataReaderSchemaTable(sproc);

                if (tbResultSet == null)
                {
                    throw new Exception("tbResultSet is null.  We should have returned something from executing the stored procedure " + sproc.Sp_Name);
                }

                // Get metadata from DbDataReader SchemaTable instead
                int iSuccess = 1;

                List<WcStoredProcColumn_Values> spCols = AddSprocColumnMetaData_FromDataReaderSchemaTable(tbResultSet, sp_Id);

                if (spCols == null)
                {
                    throw new Exception("List<WcStoredProcColumn_Values> spCols is null.  We should have returned something from executing the stored procedure " + sproc.Sp_Name);
                }
                iSuccess = StoredProcColumns_insertFromTVP(spCols, userId);
                
                if (iSuccess < 1)
                {
                    Debugger.Break();
                    throw new Exception("StoredProcColumns_insertFromTVP was not successful for: " + sproc.Sp_Name);
                }
                return iSuccess;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshStoredProcedureColumns", ex);
                return 0;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "RefreshStoredProcedureColumns", IfxTraceCategory.Leave);
            }
        }

        private static DataTable GetDataReaderSchemaTable(WcStoredProc_Values sproc)
        {
            Guid? traceId = Guid.NewGuid();
            Guid? spPVGrp_Id = null;
            SqlCommand cmd = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataReaderSchemaTable", IfxTraceCategory.Enter);
                int iSuccess = 1;

                // Step 1:  See if this sproc has params.  If not, skip the next step.
                object[] result = WcStoredProcParamValueGroup_DataServices.GetWcStoredProcParamValueGroup_Id_GetFirstBy_Sp_Id(sproc.Sp_Id);
                spPVGrp_Id = result[0] as Guid?;

                if (sproc.Sp_HasNewParams == true && spPVGrp_Id == null)
                {
                    // raise a message/exeption that we need a param value group and dont have one
                    return null;
                }

                // Step 2: get a command
                cmd = GetStoredProcCommandObjectWithParametersAndParameterValues(sproc, spPVGrp_Id);
                if (cmd == null)
                {
                    string msg =
                        "CommandHelper.GetStoredProcCommandObjectWithParametersAndParameterValues returned null.";
                    Exception cmdEx = new Exception(msg);
                    IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataReaderSchemaTable", null, new ValuePair[] { new ValuePair("sproc", sproc), new ValuePair("spPVGrp_Id", spPVGrp_Id) }, null, cmdEx, IfxTraceCategory.Catch);
                    throw cmdEx;
                }

                // Step 3:  Get a Schema Table
                try
                {
                    DbDataReader rdr = cmd.ExecuteReader();
                    DataTable tb = new DataTable();
                    DataTable schemaTable = rdr.GetSchemaTable();
                    return schemaTable;
                }
                catch (Exception exx)
                {
                    IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataReaderSchemaTable", null, new ValuePair[] { new ValuePair("sproc", sproc), new ValuePair("spPVGrp_Id", spPVGrp_Id) }, null, exx, IfxTraceCategory.Catch);
                    throw exx;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataReaderSchemaTable", null, new ValuePair[] { new ValuePair("sproc", sproc), new ValuePair("spPVGrp_Id", spPVGrp_Id) }, null, ex, IfxTraceCategory.Catch);
                throw ex;
            }
            finally
            {
                if (cmd != null)
                {
                    if (cmd.Connection != null)
                    {
                        cmd.Connection.Dispose();
                    }
                    cmd.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDataReaderSchemaTable", IfxTraceCategory.Leave);
            }
        }
        
        private static List<WcStoredProcColumn_Values> AddSprocColumnMetaData_FromDataReaderSchemaTable(DataTable tbResultSet, Guid sp_Id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AddSprocColumnMetaData_FromDataReaderSchemaTable", IfxTraceCategory.Enter);
                List<WcStoredProcColumn_Values> spCols = new List<WcStoredProcColumn_Values>();
                int fldCount = tbResultSet.Columns.Count;
                int col1 = fldCount + 1;
                int i = 97;      // initialize  i  to the char value of 'a'
                string prfx = "";
                int loop = 0;
                int iPrfx = 97;
                int iCnt = 0;
                foreach (DataRow row in tbResultSet.Rows)
                {
                    loop += 1;
                    string hdr = prfx + Convert.ToChar(i).ToString().ToUpper();
                    WcStoredProcColumn_Values col = new WcStoredProcColumn_Values();
                    // Modify the Colun Letter so we dont get a conflic with the assembly variable name "_as" or class name variable name "_cn".
                    if (hdr == "AS" || hdr == "CN")
                    {
                        hdr = hdr + "X";
                    }
                    //dsWC.tbStoredProcColumnRow dr = tbCols.NewtbStoredProcColumnRow();
                    col.SpCl_Id_noevents = Guid.NewGuid();
                    col.SpCl_Sp_Id_noevents = sp_Id;
                    col.SpCl_ColIndex_noevents = iCnt;
                    col.SpCl_ColLetter_noevents = hdr;
                    col.SpCl_Name_noevents = row[DataReaderSchemaTableColumnIndex.ColumnName].ToString();
                    // the Substring function removes the data type prefix "System."
                    col.SpCl_DtNt_Id_noevents = GetDotNetDatatypeId_FromDotNetTypeString(row[DataReaderSchemaTableColumnIndex.DataType].ToString().Substring(7));
                    col.SpCl_DtSql_Id_noevents = GetSqlDatatypeId_FromDotNetTypeString(row[DataReaderSchemaTableColumnIndex.DataType].ToString().Substring(7));
                    col.SpCl_IsNullable_noevents = (bool)row[DataReaderSchemaTableColumnIndex.AllowDBNull];

                    //bool? isNullable = row[DataReaderSchemaTableColumnIndex.AllowDBNull] as bool?;
                    //if (isNullable == null) { isNullable = true; }
                    ////col.SpCl_IsNullable = col.AllowDBNull;
                    //col.SpCl_IsNullable = (bool)isNullable;
                    col.SpCl_IsVisible_noevents = true;
                    col.SpCl_Browsable_noevents = true;
                    //col.SpCl_Notes_noevents = "xx";
                    spCols.Add(col);
                    i += 1;
                    if (loop == 26)
                    {
                        loop = 0;
                        prfx = Convert.ToChar(iPrfx).ToString().ToUpper();
                        iPrfx += 1;
                        i = 97;
                    }
                    iCnt += 1;
                }
                return spCols;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AddSprocColumnMetaData_FromDataReaderSchemaTable", ex);
                throw ex;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AddSprocColumnMetaData_FromDataReaderSchemaTable", IfxTraceCategory.Leave);
            }
        }

        private static int StoredProcColumns_insertFromTVP(List<WcStoredProcColumn_Values> spCols, Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "StoredProcColumns_insertFromTVP", IfxTraceCategory.Enter);
                int iSuccess = 1;

                DataTable dt = new DataTable();
                dt.Columns.Add("SpCl_Id", typeof(Guid));
                dt.Columns.Add("SpCl_Sp_Id", typeof(Guid));
                dt.Columns.Add("SpCl_Name", typeof(string));
                dt.Columns.Add("SpCl_ColIndex", typeof(int));
                dt.Columns.Add("SpCl_ColLetter", typeof(string));
                dt.Columns.Add("SpCl_DtNt_Id", typeof(int));
                dt.Columns.Add("SpCl_DtSql_Id", typeof(int));
                dt.Columns.Add("SpCl_IsNullable", typeof(bool));

                foreach (WcStoredProcColumn_Values item in spCols)
                {
                    DataRow dr = dt.NewRow();
                    dr["SpCl_Id"] = item.SpCl_Id;
                    dr["SpCl_Sp_Id"] = item.SpCl_Sp_Id;
                    dr["SpCl_Name"] = item.SpCl_Name;
                    dr["SpCl_ColIndex"] = item.SpCl_ColIndex;
                    dr["SpCl_ColLetter"] = item.SpCl_ColLetter;
                    dr["SpCl_DtNt_Id"] = item.SpCl_DtNt_Id;
                    dr["SpCl_DtSql_Id"] = item.SpCl_DtSql_Id;
                    dr["SpCl_IsNullable"] = item.SpCl_IsNullable;
                    dt.Rows.Add(dr);
                }

                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spWcStoredProcColumn_insertFromTVP ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter param = cmd.Parameters.Add("@NewCols", SqlDbType.Structured);
                param.Value = dt;
                cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@UserId"].Value = userId;

                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                return iSuccess;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "StoredProcColumns_insertFromTVP", ex);
                return 0;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "StoredProcColumns_insertFromTVP", IfxTraceCategory.Leave);
            }
        }



        #region Methods For Commands

        /// <summary>
        /// Pass NULL for the 2nd parameter (Value Group Id) to return the first value group from the list of groups.
        /// </summary>
        /// <param name="sproc"></param>
        /// <param name="spPVGrp_Id"></param>
        /// <returns></returns>
        private static SqlCommand GetStoredProcCommandObjectWithParametersAndParameterValues(WcStoredProc_Values sproc, Guid? spPVGrp_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStoredProcCommandObjectWithParametersAndParameterValues", IfxTraceCategory.Enter);
                SqlCommand cmd = GetStoredProcCommandObject(sproc.Sp_Id);
                if (sproc.Sp_HasNewParams == false)
                {
                    return cmd;
                }

                //  Add Parameters and Parameter Values to the command object
                SqlParameter[] parms = GetSqlParamsWithParamValueGroup((Guid)spPVGrp_Id);
                if (parms != null)
                {
                    foreach (SqlParameter pm in parms)
                    {
                        cmd.Parameters.Add(pm);
                    }
                }
                return cmd;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStoredProcCommandObjectWithParametersAndParameterValues", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStoredProcCommandObjectWithParametersAndParameterValues", IfxTraceCategory.Leave);
            }
        }


        public static SqlCommand GetStoredProcCommandObject(Guid sp_Id)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStoredProcCommandObject", IfxTraceCategory.Enter);

                object[] resultArray = GetWcStoredProc_ConnectionInfo(sp_Id);

                if (resultArray == null)
                {
                    // return a msg that we could not get an instance of the sproc
                }
                object[] data = resultArray[0] as object[];
                if (data == null)
                {
                    // return a msg that we could not get an instance of the sproc
                }
                WcStoredProc_ConnectionInfo_Binding connInfo = new WcStoredProc_ConnectionInfo_Binding(data);

                string conn = "user id=" + connInfo.DbCnSK_UserName + ";password=" + connInfo.DbCnSK_Password + ";initial catalog=" + connInfo.DbCnSK_Database + ";data source=" + connInfo.DbCnSK_Server + ";Connect Timeout=10";
                SqlConnection cn = new System.Data.SqlClient.SqlConnection(conn);
                cn.Open();
                SqlCommand cmd = cn.CreateCommand();
                string schema = "";
                if (connInfo.ApDbScm_Name != null && connInfo.ApDbScm_Name.Length > 0)
                {
                    schema = connInfo.ApDbScm_Name + ".";
                }
                cmd.CommandText = schema + connInfo.Sp_Name;
                cmd.CommandType = CommandType.StoredProcedure;
                return cmd;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStoredProcCommandObject", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetStoredProcCommandObject", IfxTraceCategory.Leave);
            }
        }


        public static SqlParameter[] GetSqlParamsWithParamValueGroup(Guid spPVGrp_Id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSqlParamsWithParamValueGroup", IfxTraceCategory.Enter);


                object[] resultArray = WcStoredProcParam_DataServices.GetWcStoredProcParamValues_lstByValueGroupId(spPVGrp_Id);
                if (resultArray == null)
                {
                    // return msg/exception
                    return null;
                }

                List<WcStoredProcParamValues_Binding> parmValueList = new List<WcStoredProcParamValues_Binding>();
                foreach (object[] obj in resultArray)
                {
                    parmValueList.Add(new WcStoredProcParamValues_Binding(obj));
                }



                //DataTable tb = GetStoredProcParamValue_lst(spPVGrp_Id);
                //if (null == tb) { return null; }
                //if (tb.Rows.Count == 0) { return null; }

                SqlParameter[] parameters = new SqlParameter[parmValueList.Count];
                int iCnt = 0;
                foreach (WcStoredProcParamValues_Binding item in parmValueList)
                {
                    //There were various issues with adding all of these values into the param's contructor, so we will add them one at a time.  For one, when we added the SqlDbType into the constructor, it was added as a value and not a type.
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = item.SpPDr_Name;
                    SqlDbType? type = GetSqlDbTypeConverFromString(item.DtSql_Name);
                    if (type != null)
                    {
                        param.SqlDbType = (SqlDbType)type;
                    }
                    param.Direction = GetParameterDirection(item.SpPDr_Name);


                    if (item.SpPV_Value != null && item.SpPV_Value.Length > 0)
                    {
                        ConvertSQLParameterValueFromStringToCorrectType(ref param, item.DtNt_Name, item.SpPV_Value);
                    }
                    else if (item.SpP_DefaultValue == null && item.SpP_IsNullable == false)
                    {
                        // We dont have a default value or a param value to use so we can't proceed
                        // return a msg/exception

                        Debugger.Break();
                        string msg = "WcStoredProc_DataServices.GetSqlParamsWithParamValueGroup:  " +
                                     item.SpP_Name +
                                     "  is NOT nullable and does not have a parameter group value.";
                        Exception missingParamValue = new Exception(msg);
                        IfxEvent.PublishTrace(traceId, _as, _cn, "GetSqlParamsWithParamValueGroup", msg, new ValuePair[] { new ValuePair("item", item.ToString()) }, null, missingParamValue, IfxTraceCategory.Catch);
                        throw missingParamValue;
                    }


                    //if ((dr["DefaultValue"].ToString().Length > 0))
                    //{
                    //    if (dr["Value"].ToString().Length == 0)
                    //    {
                    //        //  do nothing since this param has a default value (is nullable) and we dont have anything to assign to it.
                    //    }
                    //    else
                    //    {
                    //        ConvertSQLParameterValueFromStringToCorrectType(ref param, dr["NetType"].ToString(), dr["Value"].ToString());
                    //    }
                    //}
                    //else
                    //{
                    //    if (dr["Value"].ToString().Length == 0 && (param.Direction == ParameterDirection.Input || param.Direction == ParameterDirection.InputOutput))
                    //        //if (dr["Value"].ToString().Length == 0)
                    //    {
                    //        Debugger.Break();
                    //        string msg = "WCDAL.CommandHelper.GetSqlParamsWithParamValueGroup:  " +
                    //                     dr["SpP_Name"].ToString() +
                    //                     "  is NOT nullable and does not have a parameter group value.";
                    //        IfxEvent.PublishTrace(traceId, _as, _cn, "GetSqlParamsWithParamValueGroup", new Exception(msg));
                    //    }
                    //    else
                    //    {
                    //        try
                    //        {
                    //            ConvertSQLParameterValueFromStringToCorrectType(ref param, dr["NetType"].ToString(), dr["Value"].ToString());
                    //        }
                    //        catch (Exception exx)
                    //        {
                    //            string msg =
                    //                "Look at this line: ConvertSQLParameterValueFromStringToCorrectType;  dr[NetType].ToString() = " +
                    //                dr["NetType"].ToString() + ", dr[Value].ToString() = " + dr["Value"].ToString() +
                    //                ", dr[SpP_Name].ToString() = " + dr["SpP_Name"].ToString();
                    //            IfxEvent.PublishTrace(traceId, _as, _cn, "GetSqlParamsWithParamValueGroup", new Exception(msg));
                    //        }
                    //    }
                    //}

                    parameters[iCnt] = param;
                    iCnt += 1;
                }
                return parameters;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSqlParamsWithParamValueGroup", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSqlParamsWithParamValueGroup", IfxTraceCategory.Leave);
            }
        }

        #endregion Methods For Commands


        #region Local Support Methods

        private static ParameterDirection GetParameterDirection(string dir)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParameterDirection", IfxTraceCategory.Enter);
                switch (dir)
                {
                    case "IN":
                        return ParameterDirection.Input;
                        break;
                    case "OUT":
                        return ParameterDirection.Output;
                        break;
                    case "INOUT":
                        return ParameterDirection.InputOutput;
                        break;
                }
                return ParameterDirection.Input;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParameterDirection", ex);
                throw ex;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetParameterDirection", IfxTraceCategory.Leave);
            }
        }
        
        private static void AssignSQLParameterType(ref SqlParameter param, string sqlDbType)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignSQLParameterType", IfxTraceCategory.Enter);
                SqlDbType? type = GetSqlDbTypeConverFromString(sqlDbType);
                if (type == null)
                {
                    throw new Exception("WCDAL.CommandHelper.AssignSQLParameterType.;  GetSqlDbTypeConverFromString() returned null.  This should not be null.");
                }
                else
                {
                    param.SqlDbType = (SqlDbType)type;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignSQLParameterType", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "AssignSQLParameterType", IfxTraceCategory.Leave);
            }
        }

        private static SqlDbType? GetSqlDbTypeConverFromString(String sqlDbType)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSqlDbTypeConverFromString", IfxTraceCategory.Enter);
                #region Get Param SQLDbDataType

                switch (sqlDbType.ToUpper())
                {
                    case "BIGINT":
                        return SqlDbType.BigInt;
                    case "BINARY":
                        return SqlDbType.Binary;
                    case "BIT":
                        return SqlDbType.Bit;
                    case "CHAR":
                        return SqlDbType.Char;
                    case "DATETIME":
                        return SqlDbType.DateTime;
                    case "DECIMAL":
                        return SqlDbType.Decimal;
                    case "FLOAT":
                        return SqlDbType.Float;
                    case "IMAGE":
                        return SqlDbType.Image;
                    case "INT":
                        return SqlDbType.Int;
                    case "MONEY":
                        return SqlDbType.Money;
                    case "NCHAR":
                        return SqlDbType.NChar;
                    case "NTEXT":
                        return SqlDbType.NText;
                    case "NVARCHAR":
                        return SqlDbType.NVarChar;
                    case "REAL":
                        return SqlDbType.Real;
                    case "UNIQUEIDENTIFIER":
                        return SqlDbType.UniqueIdentifier;
                    case "SMALLDATETIME":
                        return SqlDbType.SmallDateTime;
                    case "SMALLINT":
                        return SqlDbType.SmallInt;
                    case "SMALLMONEY":
                        return SqlDbType.SmallMoney;
                    case "TEXT":
                        return SqlDbType.Text;
                    case "TIMESTAMP":
                        return SqlDbType.Timestamp;
                    case "TINYINT":
                        return SqlDbType.TinyInt;
                    case "VARBINARY":
                        return SqlDbType.VarBinary;
                    case "VARCHAR":
                        return SqlDbType.VarChar;
                    case "VARIANT":
                        return SqlDbType.Variant;
                    case "XML":
                        return SqlDbType.Xml;
                    case "UDT":
                        return SqlDbType.Udt;
                        break;
                    case "STRUCTURED":
                        return SqlDbType.Structured;
                    case "DATE":
                        return SqlDbType.Date;
                    case "TIME":
                        return SqlDbType.Time;
                    case "DATETIME2":
                        return SqlDbType.DateTime2;
                }
                #endregion
                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSqlDbTypeConverFromString", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetSqlDbTypeConverFromString", IfxTraceCategory.Leave);
            }
        }

        private static void ConvertSQLParameterValueFromStringToCorrectType(ref SqlParameter parm, string dataType, string value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSQLParameterValueFromStringToCorrectType", IfxTraceCategory.Enter);
                switch (dataType)
                {
                    case "Boolean":
                        CastSQLParameterToBoolean(ref parm, value);
                        break;
                    case "Byte":
                        CastSQLParameterToByte(ref parm, value);
                        break;
                    case "Char":
                        CastSQLParameterToChar(ref parm, value);
                        break;
                    case "DateTime":
                        CastSQLParameterToDateTime(ref parm, value);
                        break;
                    case "Decimal":
                        CastSQLParameterToDecimal(ref parm, value);
                        break;
                    case "Double":
                        CastSQLParameterToDouble(ref parm, value);
                        break;
                    case "Int16":
                        CastSQLParameterToInt16(ref parm, value);
                        break;
                    case "Int32":
                        CastSQLParameterToInt32(ref parm, value);
                        break;
                    case "Int64":
                        CastSQLParameterToInt64(ref parm, value);
                        break;
                    case "SByte":
                        CastSQLParameterToSByte(ref parm, value);
                        break;
                    case "Single":
                        CastSQLParameterToSingle(ref parm, value);
                        break;
                    case "String":
                        CastSQLParameterToString(ref parm, value);
                        break;
                    case "UInt16":
                        throw new Exception("WCDAL.CommandHelper.ConvertSQLParameterValueFromStringToCorrectType.;  datatype UInt16 is not supported here.");
                        break;
                    case "UInt32":
                        throw new Exception("WCDAL.CommandHelper.ConvertSQLParameterValueFromStringToCorrectType.;  datatype UInt32 is not supported here.");
                        break;
                    case "UInt64":
                        throw new Exception("WCDAL.CommandHelper.ConvertSQLParameterValueFromStringToCorrectType.;  datatype UInt64 is not supported here.");
                        break;
                    case "Object":
                        throw new Exception("WCDAL.CommandHelper.ConvertSQLParameterValueFromStringToCorrectType.;  datatype Object is not supported here.");
                        break;
                    case "Byte[]":
                        CastSQLParameterToByteArray(ref parm, value);
                        break;
                    case "Char[]":
                        throw new Exception("WCDAL.CommandHelper.ConvertSQLParameterValueFromStringToCorrectType.;  datatype Char[] is not supported here.");
                        break;
                    case "String[]":
                        throw new Exception("WCDAL.CommandHelper.ConvertSQLParameterValueFromStringToCorrectType.;  datatype String[] is not supported here.");
                        break;
                    case "Guid":
                        CastSQLParameterToGuid(ref parm, value);
                        break;
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSQLParameterValueFromStringToCorrectType", ex);
                throw ex;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ConvertSQLParameterValueFromStringToCorrectType", IfxTraceCategory.Leave);
            }
        }
        

        #region Cast String to Types

        static void CastSQLParameterToBoolean(ref SqlParameter parm, string value)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "CastSQLParameterToBoolean", IfxTraceCategory.Enter);
                if (value == "0")
                {
                    parm.Value = false;
                }
                else if (value == "-1" || value == "1")
                {
                    parm.Value = true;
                }
                else
                {
                    parm.Value = Boolean.Parse(value);
                }
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "CastSQLParameterToBoolean", ex);
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "CastSQLParameterToBoolean", IfxTraceCategory.Leave);
            }
        }

        static void CastSQLParameterToByte(ref SqlParameter parm, string value)
        {
            parm.Value = Byte.Parse(value);
        }

        static void CastSQLParameterToChar(ref SqlParameter parm, string value)
        {
            parm.Value = Char.Parse(value);
        }

        static void CastSQLParameterToDateTime(ref SqlParameter parm, string value)
        {
            parm.Value = DateTime.Parse(value);
        }

        static void CastSQLParameterToDecimal(ref SqlParameter parm, string value)
        {
            parm.Value = Decimal.Parse(value);
        }

        static void CastSQLParameterToDouble(ref SqlParameter parm, string value)
        {
            parm.Value = Double.Parse(value);
        }

        static void CastSQLParameterToInt16(ref SqlParameter parm, string value)
        {
            parm.Value = Int16.Parse(value);
        }

        static void CastSQLParameterToInt32(ref SqlParameter parm, string value)
        {
            parm.Value = Int32.Parse(value);
        }

        static void CastSQLParameterToInt64(ref SqlParameter parm, string value)
        {
            parm.Value = Int64.Parse(value);
        }

        static void CastSQLParameterToSByte(ref SqlParameter parm, string value)
        {
            parm.Value = SByte.Parse(value);
        }

        static void CastSQLParameterToSingle(ref SqlParameter parm, string value)
        {
            parm.Value = Single.Parse(value);
        }

        static void CastSQLParameterToString(ref SqlParameter parm, string value)
        {
            parm.Value = value;
        }

        static void CastSQLParameterToGuid(ref SqlParameter parm, string value)
        {
            parm.Value = new Guid(value);
        }

        static void CastSQLParameterToByteArray(ref SqlParameter parm, string value)
        {
            if (value.ToUpper() == "NULL")
            {

                parm.Value = DBNull.Value;
            }
            else
            {
                ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                byte[] key = encoding.GetBytes(value);
                parm.Value = key;
            }
        }

        #endregion Cast String to Types
        

        #region .Net and SQL Data Types

        private static List<WcDataTypeDefaultMappingDotNet2Sql_Binding> _DotNet_Sql_DatatypeMapping = null;

        private static List<WcDataTypeDefaultMappingDotNet2Sql_Binding> DotNetSqlDatatypeMapping
        {
            get
            {

                if (_DotNet_Sql_DatatypeMapping == null)
                {
                    object[] data = GetWcDataTypeDefaultMappingDotNet2Sql_lst();
                    if (data == null)
                    {
                        throw new Exception("GetWcDataTypeDefaultMappingDotNet2Sql_lst returned null, but was expecting a list of dotnet and sql datatypes.");
                    }
                    _DotNet_Sql_DatatypeMapping = new List<WcDataTypeDefaultMappingDotNet2Sql_Binding>();
                    foreach (object[] obj in data)
                    {
                        _DotNet_Sql_DatatypeMapping.Add(new WcDataTypeDefaultMappingDotNet2Sql_Binding(obj));
                    }
                }
                return _DotNet_Sql_DatatypeMapping;
            }
            set { _DotNet_Sql_DatatypeMapping = value; }
        }

        private static int GetDotNetDatatypeId_FromDotNetTypeString(string type)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDotNetDatatypeId_FromDotNetTypeString", IfxTraceCategory.Enter);
                int id = 0;
                foreach (WcDataTypeDefaultMappingDotNet2Sql_Binding item in DotNetSqlDatatypeMapping)
                {
                    if (item.DtDM_DotNetName == type)
                    {
                        return item.DtDM_DtNt_Id;
                    }
                }
                throw new Exception("DotNet Type not found in wcDataTypeDefaultMappingDotNet2Sql");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDotNetDatatypeId_FromDotNetTypeString", new ValuePair[] { new ValuePair("type", type) }, null, ex);
                throw ex;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDotNetDatatypeId_FromDotNetTypeString", IfxTraceCategory.Leave);
            }
        }

        private static int GetSqlDatatypeId_FromDotNetTypeString(string type)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDotNetDatatypeId_FromDotNetTypeString", IfxTraceCategory.Enter);
                int id = 0;
                foreach (WcDataTypeDefaultMappingDotNet2Sql_Binding item in DotNetSqlDatatypeMapping)
                {
                    if (item.DtDM_DotNetName == type)
                    {
                        return item.DtDM_DtSql_Id;
                    }
                }
                throw new Exception("DotNet Type not found in wcDataTypeDefaultMappingDotNet2Sql");
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDotNetDatatypeId_FromDotNetTypeString", new ValuePair[] { new ValuePair("type", type) }, null, ex);
                throw ex;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDotNetDatatypeId_FromDotNetTypeString", IfxTraceCategory.Leave);
            }
        }

        #endregion .Net and SQL Data Types
        
        #endregion Local Support Methods

        

    }
}


