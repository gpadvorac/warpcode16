﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace Json
{
    /// <summary>
    /// This class assumes a specific structure passed in on the SqlDataReader.
    /// Becuase we ALWASYS	have the same structure, we can eliminate a lot of loops and type checking.
    /// The SqlDataReader will return 2 resultsets, the first contains header info such as the esri Geometry Type, wkid and latestWkid
    /// and he second contains the data.
    /// Column 1: String, GeometryType
    /// Column 2: String, ShapeText  (Geometry has already been converted to a string and parsed into Json)
    /// "paths":[... , "spatialReference":{"wkid":102100}... , etc.. still need to be added to the Json
    /// </summary>
    public static class StringGeometryWriter
    {

        #region Initialize Variables
        static string QT = "\"";

        #endregion Initialize Variables

        /// <summary>
        /// This SqlDataReader should return 1 row, 3 columns only.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="rdr"></param>
        public static void WriteHeader(StringBuilder writer, SqlDataReader rdr)
        {
            try
            {
                object[] values = null;
                int index = 0;
                while (rdr.Read())
                {
                    writer.Append(@"{
 " + QT + "geometryType" + QT + ": " + QT + (string)rdr[0] + QT + @",
 " + QT + "spatialReference" + QT + @": {
  " + QT + "wkid" + QT + ": " + ((int)rdr[1]).ToString() + @",
  " + QT + "latestWkid" + QT + ": " + ((int)rdr[2]).ToString() + @"
 },");
                    // only 1 row, so get out.
                    break;
                }
                // DON'T CLOSE THE READER.  WE NEED TO CONTINUE USING IT.
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// The first 2 fields in the next resultset will always be GeometryType and ShapeText.
        /// In the next loop, skip these 2 fields and always start with the 3rd field
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="rdr"></param>
        public static void WriteFields(StringBuilder writer, SqlDataReader rdr)
        {
            try
            {
                // Move to the next resultset in this reader
                rdr.NextResult();
                // Start the text block
                writer.Append(QT + "fields" + QT + ": [");
                // Loop thru the fields and record their names
                // For now we wont record data type info, but we will later if we find we need it.
                var flds = rdr.FieldCount;
                for (int i = 2; i < flds; i++)
                {
                    //var fType = rdr.GetFieldType(i);
                    //var name = rdr.GetName(i);
                    writer.Append(@"
  {
   " + QT + "name" + QT + ": " + QT + rdr.GetName(i) + QT + @",
   " + QT + "alias" + QT + ": " + QT + rdr.GetName(i) + QT + @"
  },");
                }

                writer.Remove(writer.Length - 1, 1);
                writer.Append(@"
 ],");
                // DON'T CLOSE THE READER.  WE NEED TO CONTINUE USING IT.
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// This SqlDataReader should return 1 row, 3 columns only.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="rdr"></param>
        public static void WriteFeatures(StringBuilder writer, SqlDataReader rdr)
        {
            try
            {
                // Start the text block
                writer.Append(QT + "features" + QT + @": [
 ");

                // Now add all the data
                while (rdr.Read())
                {
                    // Loop thru the fields and record their names
                    // For now we wont record data type info, but we will later if we find we need it.

                    writer.Append("{" + QT + "attributes" + QT + ": {");

                    var flds = rdr.FieldCount;
                    for (int i = 2; i < flds; i++)
                    {
                        //var fType = rdr.GetFieldType(i);
                        //var name = rdr.GetName(i);
                        writer.Append(QT + rdr.GetName(i) + QT + ": " + QT);

                        if (rdr[i] == DBNull.Value)
                        {
                            writer.Append(QT + ",");
                        }
                        else
                        {
                            writer.Append(rdr[i].ToString() + QT + ",");
                        }
                    }
                    writer.Remove(writer.Length - 1, 1);

                    // Add the Geometry path
                    writer.Append("}," + rdr[1].ToString() + ",");

                    //                    writer.Append(@"
                    //    },
                    //   " + QT + "geometry" + QT + @": {
                    //    " + QT + "paths" + QT + ": [");
                    //                    writer.Append(rdr[1].ToString());
                    //                    writer.Append(@"
                    // ]}},");

                }
                writer.Remove(writer.Length - 1, 1);
                writer.Append(@" ]}");

                // adding this for esriGeometryPolyline
                //writer.Append(@" }]}");



            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }









    }
}