using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;
using vDataServices;

// Gen Timestamp:  11/18/2016 4:07:34 PM

namespace DataServices
{
    public partial class DiscussionSupport_DataServices
    {

        #region Initialize Variables

        private static string _as = "DataServices";
        private static string _cn = "DiscussionSupport_DataServices";

        #endregion Initialize Variables


		#region Other Data Access Methods

        public static object[]  GetDiscussion_DiscussionsImIn(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_DiscussionsImIn";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_DiscussionsImIn", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_lstBy_Application(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Application", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_lstBy_Application";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Application", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Application", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_lstBy_ApplicationMember(Guid Id, Guid MemberId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMember", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_lstBy_ApplicationMember";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@MemberId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				cmd.Parameters["@MemberId"].Value = MemberId;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMember", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMember", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_lstBy_ApplicationMy(Guid Id, Guid OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMy", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_lstBy_ApplicationMy";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@OwnerId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				cmd.Parameters["@OwnerId"].Value = OwnerId;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMy", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationMy", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_lstBy_ApplicationVersion(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersion", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_lstBy_ApplicationVersion";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersion", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersion", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_lstBy_ApplicationVersionMember(Guid Id, Guid MemberId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMember", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_lstBy_ApplicationVersionMember";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@MemberId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				cmd.Parameters["@MemberId"].Value = MemberId;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMember", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMember", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_lstBy_ApplicationVersionMy(Guid Id, Guid OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMy", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_lstBy_ApplicationVersionMy";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@OwnerId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				cmd.Parameters["@OwnerId"].Value = OwnerId;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMy", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_ApplicationVersionMy", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_lstBy_Owner(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Owner", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_lstBy_Owner";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Owner", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Owner", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_lstBy_Table(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Table", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_lstBy_Table";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Table", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Table", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_lstBy_TableMember(Guid Id, Guid MemberId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMember", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_lstBy_TableMember";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@MemberId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				cmd.Parameters["@MemberId"].Value = MemberId;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMember", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMember", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_lstBy_TableMy(Guid Id, Guid OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMy", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_lstBy_TableMy";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@OwnerId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				cmd.Parameters["@OwnerId"].Value = OwnerId;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMy", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TableMy", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_lstBy_Task(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_lstBy_Task";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_Task", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_lstBy_TaskMy(Guid Id, Guid OwnerId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_lstBy_TaskMy";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@OwnerId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				cmd.Parameters["@OwnerId"].Value = OwnerId;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMy", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_MyDiscussions(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_MyDiscussions";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_MyDiscussions", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetDiscussion_lstBy_TaskMember(Guid Id, Guid MemberId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spDiscussion_lstBy_TaskMember";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@MemberId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				cmd.Parameters["@MemberId"].Value = MemberId;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetDiscussion_lstBy_TaskMember", IfxTraceCategory.Leave);
            }
        }

		#endregion Other Data Access Methods

    }
}


