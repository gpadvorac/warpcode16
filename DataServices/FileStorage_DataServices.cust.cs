using System;
using System.Data;
using System.Data.SqlClient;
using EntityWireType;
using Ifx;
using TypeServices;
using vDataServices;

namespace DataServices
{
    public partial class FileStorage_DataServices
    {

        #region Initialize Variables



        #endregion Initialize Variables

        private static bool _returnCurrentRowOnInsertUpdate = false;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }





        public static object[] FileStorage_Save(FileStorage_Values data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid FS_Id;
            Int32 iSuccess = 0;
            bool isConcurrencyGood = true;
            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Save", new ValuePair[] { new ValuePair("data", data.ToString()) }, IfxTraceCategory.Enter);
                //FileStorage_Values _data = new FileStorage_Values(data, null);
                FileStorage_Values _data = data;
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spFileStorage_put";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
                cmd.Parameters.Add("@FS_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
                cmd.Parameters.Add("@FS_ParentType", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@FS_ParentId_Guid", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@FS_ParentId_Int", SqlDbType.Int).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@FS_ParentId_Object", SqlDbType.Variant).Direction = ParameterDirection.Input;
                //cmd.Parameters.Add("@FS_DscCm_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@FS_FileName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@FS_FilePath", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@FS_FileSize", SqlDbType.BigInt).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@FS_XrefIdentifier", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@FS_UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@FS_Stamp", SqlDbType.Timestamp).Direction = ParameterDirection.InputOutput;
                cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters
                #region Add Command Parameter Values
                cmd.Parameters["@FS_Id"].Value = _data.FS_Id;

                if (null != _data.FS_ParentType)
                {
                    cmd.Parameters["@FS_ParentType"].Value = _data.FS_ParentType;
                }
                else
                {
                    cmd.Parameters["@FS_ParentType"].Value = DBNull.Value;
                }

                if (null != _data.FS_ParentId_Guid)
                {
                    cmd.Parameters["@FS_ParentId_Guid"].Value = _data.FS_ParentId_Guid;
                }
                else
                {
                    cmd.Parameters["@FS_ParentId_Guid"].Value = DBNull.Value;
                }

                if (null != _data.FS_ParentId_Int)
                {
                    cmd.Parameters["@FS_ParentId_Int"].Value = _data.FS_ParentId_Int;
                }
                else
                {
                    cmd.Parameters["@FS_ParentId_Int"].Value = DBNull.Value;
                }

                if (null != _data.FS_ParentId_Object)
                {
                    cmd.Parameters["@FS_ParentId_Object"].Value = _data.FS_ParentId_Object;
                }
                else
                {
                    cmd.Parameters["@FS_ParentId_Object"].Value = DBNull.Value;
                }

                //if (null != _data.FS_DscCm_Id)
                //{
                //    cmd.Parameters["@FS_DscCm_Id"].Value = _data.FS_DscCm_Id;
                //}
                //else
                //{
                //    cmd.Parameters["@FS_DscCm_Id"].Value = DBNull.Value;
                //}

                if (null != _data.FS_FileName)
                {
                    cmd.Parameters["@FS_FileName"].Value = _data.FS_FileName;
                }
                else
                {
                    cmd.Parameters["@FS_FileName"].Value = DBNull.Value;
                }

                if (null != _data.FS_FilePath)
                {
                    cmd.Parameters["@FS_FilePath"].Value = _data.FS_FilePath;
                }
                else
                {
                    cmd.Parameters["@FS_FilePath"].Value = DBNull.Value;
                }

                if (null != _data.FS_FileSize)
                {
                    cmd.Parameters["@FS_FileSize"].Value = _data.FS_FileSize;
                }
                else
                {
                    cmd.Parameters["@FS_FileSize"].Value = DBNull.Value;
                }

                if (null != _data.FS_XrefIdentifier)
                {
                    cmd.Parameters["@FS_XrefIdentifier"].Value = _data.FS_XrefIdentifier;
                }
                else
                {
                    cmd.Parameters["@FS_XrefIdentifier"].Value = DBNull.Value;
                }

                if (null != _data.FS_UserId)
                {
                    cmd.Parameters["@FS_UserId"].Value = _data.FS_UserId;
                }
                else
                {
                    cmd.Parameters["@FS_UserId"].Value = DBNull.Value;
                }

                cmd.Parameters["@FS_Stamp"].Value = _data.FS_Stamp;


                cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "FileStorage_Save", SQLScript.CreateScript(cmd));
                }

                //// Log all data changes
                //object[] original = null;
                //if (v_Audit_DataServices.UseAudit == true)
                //{
                //    original = FileStorage_GetById(_data.FS_Id);
                //}

                cmd.ExecuteNonQuery();
                FS_Id = ((Guid)(cmd.Parameters["@FS_Id"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                {
                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                }

                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                //**//  Hack:  we need to get this true or false value from the business object by using a 
                //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
                dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
                //  Set the DataServiceInsertUpdateResponse Result
                if (iSuccess > 0)
                {
                    dataServiceResponse.Result = DataOperationResult.Success;
                    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@FS_Stamp"].Value));

                    //// Log all data changes
                    //if (v_Audit_DataServices.UseAudit == true)
                    //{
                    //    FileStorage_Values originalValues = null;
                    //    if (original != null)
                    //    {
                    //        originalValues = new FileStorage_Values((object[])original[0], null);
                    //    }
                    //    FileStorage_LogAudit(_data, originalValues);
                    //}

                }
                else if (isConcurrencyGood == false)
                {
                    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
                }
                else
                {
                    dataServiceResponse.Result = DataOperationResult.HandledException;
                }
                if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
                {
                    //  we need a flag to determin if we return new data on a successful insert/update
                    dataServiceResponse.CurrentValues = FileStorage_GetById_ObjectArray(FS_Id);
                }
                // If the default is to not return the current row, then return the Primary Key here.
                if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
                {
                    dataServiceResponse.guidPrimaryKey = FS_Id;
                }

                return dataServiceResponse.ReturnAsObjectArray();
            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", data.ToString()), new ValuePair("sqlScript", sqlScript) }, null, ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                dataServiceResponse.Result = DataOperationResult.UnhandledException;
                return dataServiceResponse.ReturnAsObjectArray();
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Save", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }



        public static int FileStorage_RenameFile(Guid fs_Id, string newFileName, Guid userId)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            object[] returnData = new object[0];
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Delete", new ValuePair[] { new ValuePair("fs_Id", fs_Id), new ValuePair("newFileName", newFileName) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spFileStorage_RenameFile";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FS_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@NewFileName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@FS_UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@FS_Id"].Value = fs_Id;
                cmd.Parameters["@NewFileName"].Value = newFileName;
                cmd.Parameters["@FS_UserId"].Value = userId;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                //returnData[0] = iSuccess;
                return iSuccess;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Delete", ex);
                return 0;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Delete", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }



        public static int FileStorage_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            object[] returnData = new object[0];
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Delete", new ValuePair[] { new ValuePair("data", data) }, IfxTraceCategory.Enter);
                FileStorage_Values _data = new FileStorage_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spFileStorage_del ";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FS_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@FS_Id"].Value = _data.FS_Id;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                //returnData[0] = iSuccess;
                return iSuccess;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Delete", ex);
                return 0;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "FileStorage_Delete", new ValuePair[] { new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }



        public static object[] GetFileStorage_lstByLinkedRecord(Guid Id, String Type)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedRecord", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spFileStorage_lstByLinkedRecord";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Type", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                cmd.Parameters["@Id"].Value = Id;
                cmd.Parameters["@Type"].Value = Type;
                SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedRecord", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedRecord", IfxTraceCategory.Leave);
            }
        }



        public static object[] GetFileStorage_lstByLinkedXRefRecords(Guid Id, String Type)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedXRefRecords", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spFileStorage_lstByLinkedXRefRecords";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Type", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                cmd.Parameters["@Id"].Value = Id;
                cmd.Parameters["@Type"].Value = Type;
                SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedXRefRecords", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetFileStorage_lstByLinkedXRefRecords", IfxTraceCategory.Leave);
            }
        }

	



    }
}


