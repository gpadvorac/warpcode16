using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;
using vDataServices;

// Gen Timestamp:  5/7/2015 3:07:46 PM

namespace DataServices
{
    public partial class PersonContact_DataServices
    {

        #region Initialize Variables

        private static string _as = "DataServices";
        private static string _cn = "PersonContact_DataServices";

        #endregion Initialize Variables


        #region Base Methods


        public static object[] PersonContact_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spPersonContact_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetById", IfxTraceCategory.Leave);
            }
        }

        public static object[] PersonContact_GetById_ObjectArray(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spPersonContact_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetById_ObjectArray", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] PersonContact_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spPersonContact_lst";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] PersonContact_GetListByFK()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetListByFK", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spPersonContact_lst";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        public static object[] PersonContact_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid Pn_Id;
            Int32 iSuccess = 0;
            bool isConcurrencyGood = true;
            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_Save", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                PersonContact_Values _data = new PersonContact_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spPersonContact_put";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
				cmd.Parameters.Add("@Pn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@Pn_PhoneCell", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Pn_PhoneOther", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Pn_UserIdStamp", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Pn_Stamp", SqlDbType.Timestamp).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters
                #region Add Command Parameter Values
				cmd.Parameters["@Pn_Id"].Value = _data.Pn_Id;

				if (null != _data.Pn_PhoneCell)
                {
                    cmd.Parameters["@Pn_PhoneCell"].Value = _data.Pn_PhoneCell;
                }
                else
                {
                    cmd.Parameters["@Pn_PhoneCell"].Value = DBNull.Value;
                }

				if (null != _data.Pn_PhoneOther)
                {
                    cmd.Parameters["@Pn_PhoneOther"].Value = _data.Pn_PhoneOther;
                }
                else
                {
                    cmd.Parameters["@Pn_PhoneOther"].Value = DBNull.Value;
                }

				if (null != _data.Pn_UserIdStamp)
                {
                    cmd.Parameters["@Pn_UserIdStamp"].Value = _data.Pn_UserIdStamp;
                }
                else
                {
                    cmd.Parameters["@Pn_UserIdStamp"].Value = DBNull.Value;
                }

				cmd.Parameters["@Pn_Stamp"].Value = _data.Pn_Stamp;


                cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "PersonContact_Save", SQLScript.CreateScript(cmd));
                }

                //// Log all data changes
                //object[] original = null;
                //if (v_Audit_DataServices.UseAudit == true)
                //{
                //    original = PersonContact_GetById(_data.Pn_Id);
                //}

                cmd.ExecuteNonQuery();
                Pn_Id = ((Guid)(cmd.Parameters["@Pn_Id"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                {
                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                }

                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                //**//  Hack:  we need to get this true or false value from the business object by using a 
                //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
                dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
                //  Set the DataServiceInsertUpdateResponse Result
                if (iSuccess > 0)
                {
                    dataServiceResponse.Result = DataOperationResult.Success;
                    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@Pn_Stamp"].Value));

                    //// Log all data changes
                    //if (v_Audit_DataServices.UseAudit == true)
                    //{
                    //    PersonContact_Values originalValues = null;
                    //    if (original != null)
                    //    {
                    //        originalValues = new PersonContact_Values((object[])original[0], null);
                    //    }
                    //    PersonContact_LogAudit(_data, originalValues);
                    //}

                }
                else if (isConcurrencyGood == false)
                {
                    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
                }
                else
                {
                    dataServiceResponse.Result = DataOperationResult.HandledException;
                }
                if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
                {
                    //  we need a flag to determin if we return new data on a successful insert/update
                    dataServiceResponse.CurrentValues = PersonContact_GetById_ObjectArray(Pn_Id);
                }
                // If the default is to not return the current row, then return the Primary Key here.
                if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
                {
                    dataServiceResponse.guidPrimaryKey = Pn_Id;
                }

                return dataServiceResponse.ReturnAsObjectArray();
            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)), new ValuePair("sqlScript", sqlScript) }, null, ex);
                IfxWrapperException.GetError(ex);
                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                dataServiceResponse.Result = DataOperationResult.UnhandledException;
                return dataServiceResponse.ReturnAsObjectArray();
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] PersonContact_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
//                //SqlConnectionHelper helper = new SqlConnectionHelper();
//                conn = DBConnectoinHelper.GetDBConnection();
//                conn.Open();
//                SqlCommand cmd = conn.CreateCommand();
//                cmd.CommandText = "dbo.spPersonContact_del ";
//                cmd.CommandType = System.Data.CommandType.StoredProcedure;
//                cmd.Parameters.Add("@ID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
//                cmd.Parameters["@Pn_Id"].Value = _data.C._a;
//                cmd.ExecuteNonQuery();
//                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_Delete", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_Delete", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] PersonContact_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_SetIsDeleted", new ValuePair[] {new ValuePair("IsDeleted", IsDeleted), new ValuePair("Id", Id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spPersonContact_putIsDeleted ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = Id;
                cmd.Parameters["@IsDeleted"].Value = IsDeleted;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[2];
                ret[0] = iSuccess;
                ret[1] = Id;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_SetIsDeleted", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] PersonContact_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] PersonContact_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "PersonContact_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods



		#region Other Data Access Methods

        public static object[]  GetPerson_LU_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_LU_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spPerson_LU_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_LU_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetPerson_LU_ComboItemList", IfxTraceCategory.Leave);
            }
        }

		#endregion Other Data Access Methods

    }
}


