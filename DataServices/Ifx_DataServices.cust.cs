using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using EntityWireType;
using TypeServices;
using Ifx;
using vDataServices;

namespace DataServices
{
    public partial class Ifx_DataServices
    {

        #region Initialize Variables



        #endregion Initialize Variables

        private static bool _returnCurrentRowOnInsertUpdate = false;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }



        public static object[] Executeix_AuthenticationLog_putInsert(Guid? IxAuthLog_UserId, String IxAuthLog_UserName, String IxAuthLog_IP, String IxAuthLog_Platform, String IxAuthLog_Message, Boolean IxAuthLog_Success)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Executeix_AuthenticationLog_putInsert", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spix_AuthenticationLog_putInsert";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@IxAuthLog_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@IxAuthLog_UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IxAuthLog_UserName", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IxAuthLog_IP", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IxAuthLog_Platform", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IxAuthLog_Message", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IxAuthLog_Success", SqlDbType.Bit).Direction = ParameterDirection.Input;

                if (null != IxAuthLog_UserId)
                {
                    cmd.Parameters["@IxAuthLog_UserId"].Value = IxAuthLog_UserId;
                }
                else
                {
                    cmd.Parameters["@IxAuthLog_UserId"].Value = DBNull.Value;
                }

                if (null != IxAuthLog_UserName)
                {
                    cmd.Parameters["@IxAuthLog_UserName"].Value = IxAuthLog_UserName;
                }
                else
                {
                    cmd.Parameters["@IxAuthLog_UserName"].Value = DBNull.Value;
                }

                if (null != IxAuthLog_IP)
                {
                    cmd.Parameters["@IxAuthLog_IP"].Value = IxAuthLog_IP;
                }
                else
                {
                    cmd.Parameters["@IxAuthLog_IP"].Value = DBNull.Value;
                }

                if (null != IxAuthLog_Platform)
                {
                    cmd.Parameters["@IxAuthLog_Platform"].Value = IxAuthLog_Platform;
                }
                else
                {
                    cmd.Parameters["@IxAuthLog_Platform"].Value = DBNull.Value;
                }

                if (null != IxAuthLog_Message)
                {
                    cmd.Parameters["@IxAuthLog_Message"].Value = IxAuthLog_Message;
                }
                else
                {
                    cmd.Parameters["@IxAuthLog_Message"].Value = DBNull.Value;
                }

                //cmd.Parameters["@IxAuthLog_UserId"].Value = IxAuthLog_UserId;
                //cmd.Parameters["@IxAuthLog_UserName"].Value = IxAuthLog_UserName;
                //cmd.Parameters["@IxAuthLog_IP"].Value = IxAuthLog_IP;
                //cmd.Parameters["@IxAuthLog_Platform"].Value = IxAuthLog_Platform;
                //cmd.Parameters["@IxAuthLog_Message"].Value = IxAuthLog_Message;
                cmd.Parameters["@IxAuthLog_Success"].Value = IxAuthLog_Success;
                cmd.ExecuteNonQuery();
                Guid IxAuthLog_Id = ((Guid)(cmd.Parameters["@IxAuthLog_Id"].Value));
                object[] ret = new object[1];
                ret[0] = IxAuthLog_Id;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Executeix_AuthenticationLog_putInsert", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Executeix_AuthenticationLog_putInsert", IfxTraceCategory.Leave);
            }
        }


    }
}


