using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;
using vDataServices;

// Gen Timestamp:  12/27/2016 12:46:33 PM

namespace DataServices
{
    public partial class CommonClientData_DataServices
    {

        #region Initialize Variables

        private static string _as = "DataServices";
        private static string _cn = "CommonClientData_DataServices";

        #endregion Initialize Variables


		#region Other Data Access Methods

        public static object[]  GetWcTableSortByDirection_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableSortByDirection_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTableSortByDirection_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableSortByDirection_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableSortByDirection_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetWcControlType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcControlType_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcControlType_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcControlType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcControlType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetWcDataTypeDotNet_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDotNet_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcDataTypeDotNet_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDotNet_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeDotNet_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetWcDataTypeSQL_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeSQL_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcDataTypeSQL_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeSQL_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcDataTypeSQL_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetWcStoredProcResultType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcResultType_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProcResultType_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcResultType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcResultType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetWcStoredProcReturnType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcReturnType_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProcReturnType_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcReturnType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcReturnType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetWcTableColumnComboListType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnComboListType_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTableColumnComboListType_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnComboListType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumnComboListType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetWcStoredProcParamDirection_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParamDirection_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProcParamDirection_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParamDirection_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProcParamDirection_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTaskStatus_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTaskStatus_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", IfxTraceCategory.Leave);
            }
        }

		#endregion Other Data Access Methods

    }
}


