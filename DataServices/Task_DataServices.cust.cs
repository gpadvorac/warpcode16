using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using EntityWireType;
using TypeServices;
using Ifx;
using vDataServices;

namespace DataServices
{
    public partial class Task_DataServices
    {

        #region Initialize Variables



        #endregion Initialize Variables

        private static bool _returnCurrentRowOnInsertUpdate = true;

        private static bool GetReturnCurrentRowOnInsertUpdate()
        {
            return _returnCurrentRowOnInsertUpdate;
        }



        //public static object[] GetTask_lstByMyTasks(Guid Id)
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    SqlConnection conn = null;
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByMyTasks", IfxTraceCategory.Enter);
        //        conn = DBConnectoinHelper.GetDBConnection();
        //        conn.Open();
        //        SqlCommand cmd = conn.CreateCommand();
        //        cmd.CommandText = "spTask_lstByMyTasks";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters["@Id"].Value = Id;
        //        SqlDataReader rdr = cmd.ExecuteReader();

        //        return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByMyTasks", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (conn != null)
        //        {
        //            conn.Dispose();
        //        }
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByMyTasks", IfxTraceCategory.Leave);
        //    }
        //}



        public static object[] GetTask_lstByProjectOnly(Guid Tk_Prj_Id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProjectOnly", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_lstByProjectOnly";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Tk_Prj_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters["@Tk_Prj_Id"].Value = Tk_Prj_Id;
                SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProjectOnly", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProjectOnly", IfxTraceCategory.Leave);
            }
        }



    }
}


