using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;
using vDataServices;

// Gen Timestamp:  9/23/2015 3:20:41 PM

namespace DataServices
{
    public partial class Ifx_DataServices
    {

        #region Initialize Variables

        private static string _as = "DataServices";
        private static string _cn = "Ifx_DataServices";

        #endregion Initialize Variables



        #region Other Data Access Methods

        public static object[] GetIxExceptionTraceReview(DateTime? Start, DateTime? End, String User)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIxExceptionTraceReview", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spX_ixExceptionTraceReview";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Start", SqlDbType.DateTime).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@End", SqlDbType.DateTime).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@User", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters["@Start"].Value = Start;
                cmd.Parameters["@End"].Value = End;
                cmd.Parameters["@User"].Value = User;
                SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIxExceptionTraceReview", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIxExceptionTraceReview", IfxTraceCategory.Leave);
            }
        }

        public static object[] GetIxExceptionTraceReview_ValuePairs(DateTime? Start, DateTime? End, String User)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIxExceptionTraceReview_ValuePairs", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spX_ixExceptionTraceReview_ValuePairs";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Start", SqlDbType.DateTime).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@End", SqlDbType.DateTime).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@User", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
                cmd.Parameters["@Start"].Value = Start;
                cmd.Parameters["@End"].Value = End;
                cmd.Parameters["@User"].Value = User;
                SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIxExceptionTraceReview_ValuePairs", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetIxExceptionTraceReview_ValuePairs", IfxTraceCategory.Leave);
            }
        }


        public static object[] ExecuteIx_Excpetion_deleteList(string crit)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteIx_Excpetion_deleteList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spix_Excpetion_deleteList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@crit", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@crit"].Value = crit;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[1];
                ret[0] = iSuccess;
                return ret;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteIx_Excpetion_deleteList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteIx_Excpetion_deleteList", IfxTraceCategory.Leave);
            }
        }


        public static object[] Getix_AuthenticationLog_lst()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Getix_AuthenticationLog_lst", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spix_AuthenticationLog_lst";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Getix_AuthenticationLog_lst", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Getix_AuthenticationLog_lst", IfxTraceCategory.Leave);
            }
        }

        #endregion Other Data Access Methods

    }
}


