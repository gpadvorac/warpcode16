using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;
using vDataServices;

// Gen Timestamp:  12/15/2017 12:04:45 AM

namespace DataServices
{
    public partial class WcStoredProcGroup_DataServices
    {

        #region Initialize Variables

        private static string _as = "DataServices";
        private static string _cn = "WcStoredProcGroup_DataServices";

        #endregion Initialize Variables


        #region Base Methods


        public static object[] WcStoredProcGroup_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcStoredProcGroup_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetById", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProcGroup_GetById_ObjectArray(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcStoredProcGroup_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetById_ObjectArray", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProcGroup_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProcGroup_lstAll";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProcGroup_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProcGroup_lst";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@SpGrp_ApVrsn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@SpGrp_ApVrsn_Id"].Value = id;
				SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProcGroup_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid SpGrp_Id;
            Int32 iSuccess = 0;
            bool isConcurrencyGood = true;
            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Save", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                WcStoredProcGroup_Values _data = new WcStoredProcGroup_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProcGroup_put";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
				cmd.Parameters.Add("@SpGrp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@SpGrp_ApVrsn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@SpGrp_SortOrder", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@SpGrp_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@SpGrp_Desc", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@SpGrp_IsActiveRow", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@SpGrp_UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@SpGrp_Stamp", SqlDbType.Timestamp).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters
                #region Add Command Parameter Values
				cmd.Parameters["@SpGrp_Id"].Value = _data.SpGrp_Id;

				cmd.Parameters["@SpGrp_ApVrsn_Id"].Value = _data.SpGrp_ApVrsn_Id;

				if (null != _data.SpGrp_SortOrder)
                {
                    cmd.Parameters["@SpGrp_SortOrder"].Value = _data.SpGrp_SortOrder;
                }
                else
                {
                    cmd.Parameters["@SpGrp_SortOrder"].Value = DBNull.Value;
                }

				if (null != _data.SpGrp_Name)
                {
                    cmd.Parameters["@SpGrp_Name"].Value = _data.SpGrp_Name;
                }
                else
                {
                    cmd.Parameters["@SpGrp_Name"].Value = DBNull.Value;
                }

				if (null != _data.SpGrp_Desc)
                {
                    cmd.Parameters["@SpGrp_Desc"].Value = _data.SpGrp_Desc;
                }
                else
                {
                    cmd.Parameters["@SpGrp_Desc"].Value = DBNull.Value;
                }

				cmd.Parameters["@SpGrp_IsActiveRow"].Value = _data.SpGrp_IsActiveRow;

				if (null != _data.SpGrp_UserId)
                {
                    cmd.Parameters["@SpGrp_UserId"].Value = _data.SpGrp_UserId;
                }
                else
                {
                    cmd.Parameters["@SpGrp_UserId"].Value = DBNull.Value;
                }

				cmd.Parameters["@SpGrp_Stamp"].Value = _data.SpGrp_Stamp;


                cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "WcStoredProcGroup_Save", SQLScript.CreateScript(cmd));
                }

                //// Log all data changes
                //object[] original = null;
                //if (v_Audit_DataServices.UseAudit == true)
                //{
                //    original = WcStoredProcGroup_GetById(_data.SpGrp_Id);
                //}

                cmd.ExecuteNonQuery();
                SpGrp_Id = ((Guid)(cmd.Parameters["@SpGrp_Id"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                {
                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                }

                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                //**//  Hack:  we need to get this true or false value from the business object by using a 
                //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
                dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
                //  Set the DataServiceInsertUpdateResponse Result
                if (iSuccess > 0)
                {
                    dataServiceResponse.Result = DataOperationResult.Success;
                    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@SpGrp_Stamp"].Value));

                    //// Log all data changes
                    //if (v_Audit_DataServices.UseAudit == true)
                    //{
                    //    WcStoredProcGroup_Values originalValues = null;
                    //    if (original != null)
                    //    {
                    //        originalValues = new WcStoredProcGroup_Values((object[])original[0], null);
                    //    }
                    //    WcStoredProcGroup_LogAudit(_data, originalValues);
                    //}

                }
                else if (isConcurrencyGood == false)
                {
                    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
                }
                else
                {
                    dataServiceResponse.Result = DataOperationResult.HandledException;
                }
                if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
                {
                    //  we need a flag to determin if we return new data on a successful insert/update
                    dataServiceResponse.CurrentValues = WcStoredProcGroup_GetById_ObjectArray(SpGrp_Id);
                }
                // If the default is to not return the current row, then return the Primary Key here.
                if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
                {
                    dataServiceResponse.guidPrimaryKey = SpGrp_Id;
                }

                return dataServiceResponse.ReturnAsObjectArray();
            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)), new ValuePair("sqlScript", sqlScript) }, null, ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                dataServiceResponse.Result = DataOperationResult.UnhandledException;
                return dataServiceResponse.ReturnAsObjectArray();
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProcGroup_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                WcStoredProcGroup_Values _data = new WcStoredProcGroup_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProcGroup_del ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = _data.SpGrp_Id;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[1];
                ret[0] = iSuccess;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Delete", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Delete", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProcGroup_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_SetIsDeleted", new ValuePair[] {new ValuePair("IsDeleted", IsDeleted), new ValuePair("Id", Id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProcGroup_putIsDeleted ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = Id;
                cmd.Parameters["@IsDeleted"].Value = IsDeleted;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[2];
                ret[0] = iSuccess;
                ret[1] = Id;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_SetIsDeleted", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProcGroup_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProcGroup_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProcGroup_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods



		#region Other Data Access Methods

		#endregion Other Data Access Methods

    }
}


