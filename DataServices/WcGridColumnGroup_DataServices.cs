using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;
using vDataServices;

// Gen Timestamp:  12/28/2017 12:07:39 AM

namespace DataServices
{
    public partial class WcGridColumnGroup_DataServices
    {

        #region Initialize Variables

        private static string _as = "DataServices";
        private static string _cn = "WcGridColumnGroup_DataServices";

        #endregion Initialize Variables


        #region Base Methods


        public static object[] WcGridColumnGroup_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcGridColumnGroup_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetById", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcGridColumnGroup_GetById_ObjectArray(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcGridColumnGroup_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetById_ObjectArray", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcGridColumnGroup_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcGridColumnGroup_lstAll";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcGridColumnGroup_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcGridColumnGroup_lst";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@GrdColGrp_Tb_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@GrdColGrp_Tb_Id"].Value = id;
				SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcGridColumnGroup_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid GrdColGrp_Id;
            Int32 iSuccess = 0;
            bool isConcurrencyGood = true;
            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Save", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                WcGridColumnGroup_Values _data = new WcGridColumnGroup_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "spWcGridColumnGroup_put";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
				cmd.Parameters.Add("@GrdColGrp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@GrdColGrp_Tb_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@GrdColGrp_Sort", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@GrdColGrp_Name", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@GrdColGrp_HeaderText", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@GrdColGrp_IsDefaultView", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@GrdColGrp_IsActiveRow", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@GrdColGrp_UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@GrdColGrp_Stamp", SqlDbType.Timestamp).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters
                #region Add Command Parameter Values
				cmd.Parameters["@GrdColGrp_Id"].Value = _data.GrdColGrp_Id;

				if (null != _data.GrdColGrp_Tb_Id)
                {
                    cmd.Parameters["@GrdColGrp_Tb_Id"].Value = _data.GrdColGrp_Tb_Id;
                }
                else
                {
                    cmd.Parameters["@GrdColGrp_Tb_Id"].Value = DBNull.Value;
                }

				if (null != _data.GrdColGrp_Sort)
                {
                    cmd.Parameters["@GrdColGrp_Sort"].Value = _data.GrdColGrp_Sort;
                }
                else
                {
                    cmd.Parameters["@GrdColGrp_Sort"].Value = DBNull.Value;
                }

				cmd.Parameters["@GrdColGrp_Name"].Value = _data.GrdColGrp_Name;

				if (null != _data.GrdColGrp_HeaderText)
                {
                    cmd.Parameters["@GrdColGrp_HeaderText"].Value = _data.GrdColGrp_HeaderText;
                }
                else
                {
                    cmd.Parameters["@GrdColGrp_HeaderText"].Value = DBNull.Value;
                }

				cmd.Parameters["@GrdColGrp_IsDefaultView"].Value = _data.GrdColGrp_IsDefaultView;

				cmd.Parameters["@GrdColGrp_IsActiveRow"].Value = _data.GrdColGrp_IsActiveRow;

				if (null != _data.GrdColGrp_UserId)
                {
                    cmd.Parameters["@GrdColGrp_UserId"].Value = _data.GrdColGrp_UserId;
                }
                else
                {
                    cmd.Parameters["@GrdColGrp_UserId"].Value = DBNull.Value;
                }

				cmd.Parameters["@GrdColGrp_Stamp"].Value = _data.GrdColGrp_Stamp;


                cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "WcGridColumnGroup_Save", SQLScript.CreateScript(cmd));
                }

                //// Log all data changes
                //object[] original = null;
                //if (v_Audit_DataServices.UseAudit == true)
                //{
                //    original = WcGridColumnGroup_GetById(_data.GrdColGrp_Id);
                //}

                cmd.ExecuteNonQuery();
                GrdColGrp_Id = ((Guid)(cmd.Parameters["@GrdColGrp_Id"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                {
                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                }

                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                //**//  Hack:  we need to get this true or false value from the business object by using a 
                //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
                dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
                //  Set the DataServiceInsertUpdateResponse Result
                if (iSuccess > 0)
                {
                    dataServiceResponse.Result = DataOperationResult.Success;
                    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@GrdColGrp_Stamp"].Value));

                    //// Log all data changes
                    //if (v_Audit_DataServices.UseAudit == true)
                    //{
                    //    WcGridColumnGroup_Values originalValues = null;
                    //    if (original != null)
                    //    {
                    //        originalValues = new WcGridColumnGroup_Values((object[])original[0], null);
                    //    }
                    //    WcGridColumnGroup_LogAudit(_data, originalValues);
                    //}

                }
                else if (isConcurrencyGood == false)
                {
                    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
                }
                else
                {
                    dataServiceResponse.Result = DataOperationResult.HandledException;
                }
                if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
                {
                    //  we need a flag to determin if we return new data on a successful insert/update
                    dataServiceResponse.CurrentValues = WcGridColumnGroup_GetById_ObjectArray(GrdColGrp_Id);
                }
                // If the default is to not return the current row, then return the Primary Key here.
                if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
                {
                    dataServiceResponse.guidPrimaryKey = GrdColGrp_Id;
                }

                return dataServiceResponse.ReturnAsObjectArray();
            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)), new ValuePair("sqlScript", sqlScript) }, null, ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                dataServiceResponse.Result = DataOperationResult.UnhandledException;
                return dataServiceResponse.ReturnAsObjectArray();
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcGridColumnGroup_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                WcGridColumnGroup_Values _data = new WcGridColumnGroup_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcGridColumnGroup_del ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = _data.GrdColGrp_Id;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[1];
                ret[0] = iSuccess;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Delete", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Delete", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcGridColumnGroup_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_SetIsDeleted", new ValuePair[] {new ValuePair("IsDeleted", IsDeleted), new ValuePair("Id", Id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcGridColumnGroup_putIsDeleted ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = Id;
                cmd.Parameters["@IsDeleted"].Value = IsDeleted;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[2];
                ret[0] = iSuccess;
                ret[1] = Id;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_SetIsDeleted", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcGridColumnGroup_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcGridColumnGroup_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcGridColumnGroup_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods



		#region Other Data Access Methods

        public static object[]  ExecuteWcGridColumnGroup_AssignItems(Boolean Insert, Guid? GrdColGrpItm_Id, Guid? GrdColGrpItm_Grp_Id, Guid? GrdColGrpItm_TbC_Id, Guid? GrdColGrpItm_UserId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcGridColumnGroup_AssignItems", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcGridColumnGroup_AssignItems";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Insert", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@GrdColGrpItm_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@GrdColGrpItm_Grp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@GrdColGrpItm_TbC_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@GrdColGrpItm_UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
				cmd.Parameters["@Insert"].Value = Insert;
				cmd.Parameters["@GrdColGrpItm_Id"].Value = GrdColGrpItm_Id;
				cmd.Parameters["@GrdColGrpItm_Grp_Id"].Value = GrdColGrpItm_Grp_Id;
				cmd.Parameters["@GrdColGrpItm_TbC_Id"].Value = GrdColGrpItm_TbC_Id;
				cmd.Parameters["@GrdColGrpItm_UserId"].Value = GrdColGrpItm_UserId;
				cmd.ExecuteNonQuery();
				Int32 Success = ((Int32)(cmd.Parameters["@Success"].Value));
				//Guid ErrorLogId = ((Guid)(cmd.Parameters["@ErrorLogId"].Value));
				object[] ret = new object[1];
				ret[0] = Success;

                return ret;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcGridColumnGroup_AssignItems", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcGridColumnGroup_AssignItems", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetWcGridColumnGroupItem_lstAssigned(Guid Grp_Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstAssigned", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcGridColumnGroupItem_lstAssigned";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Grp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Grp_Id"].Value = Grp_Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstAssigned", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstAssigned", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetWcGridColumnGroupItem_lstNotAssigned(Guid Tb_Id, Guid Grp_Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstNotAssigned", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcGridColumnGroupItem_lstNotAssigned";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Tb_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Grp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Tb_Id"].Value = Tb_Id;
				cmd.Parameters["@Grp_Id"].Value = Grp_Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstNotAssigned", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcGridColumnGroupItem_lstNotAssigned", IfxTraceCategory.Leave);
            }
        }

		#endregion Other Data Access Methods

    }
}


