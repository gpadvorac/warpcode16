using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;
using vDataServices;

// Gen Timestamp:  6/21/2016 10:54:38 AM

namespace DataServices
{
    public partial class Task_DataServices
    {

        #region Initialize Variables

        private static string _as = "DataServices";
        private static string _cn = "Task_DataServices";

        #endregion Initialize Variables


        #region Base Methods


        public static object[] Task_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spTask_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetById", IfxTraceCategory.Leave);
            }
        }

        public static object[] Task_GetById_ObjectArray(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spTask_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetById_ObjectArray", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] Task_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spTask_lst";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] Task_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spTask_lst";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Tk_Prj_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Tk_Prj_Id"].Value = id;
				SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        public static object[] Task_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid Tk_Id;
            Int32 iSuccess = 0;
            bool isConcurrencyGood = true;
            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_Save", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                Task_Values _data = new Task_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spTask_put";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
				cmd.Parameters.Add("@Tk_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@Tk_Prj_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_Tk_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_TkTp_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_TkCt_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_Name", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_Desc", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_EstimatedTime", SqlDbType.Decimal).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_PercentComplete", SqlDbType.Decimal).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_StartDate", SqlDbType.Date).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_Deadline", SqlDbType.Date).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_TkSt_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_TkPr_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_Results", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_Remarks", SqlDbType.NVarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_IsPrivate", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_IsActiveRow", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_Stamp", SqlDbType.Timestamp).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@Ct_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@CtD_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Df_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Ls_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@LsTr_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Ob_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Pp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Wl_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@RCt_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters
                #region Add Command Parameter Values
				cmd.Parameters["@Tk_Id"].Value = _data.Tk_Id;

				cmd.Parameters["@Tk_Prj_Id"].Value = _data.Tk_Prj_Id;

				if (null != _data.Tk_Tk_Id)
                {
                    cmd.Parameters["@Tk_Tk_Id"].Value = _data.Tk_Tk_Id;
                }
                else
                {
                    cmd.Parameters["@Tk_Tk_Id"].Value = DBNull.Value;
                }

				cmd.Parameters["@Tk_TkTp_Id"].Value = _data.Tk_TkTp_Id;

				if (null != _data.Tk_TkCt_Id)
                {
                    cmd.Parameters["@Tk_TkCt_Id"].Value = _data.Tk_TkCt_Id;
                }
                else
                {
                    cmd.Parameters["@Tk_TkCt_Id"].Value = DBNull.Value;
                }

				if (null != _data.Tk_Name)
                {
                    cmd.Parameters["@Tk_Name"].Value = _data.Tk_Name;
                }
                else
                {
                    cmd.Parameters["@Tk_Name"].Value = DBNull.Value;
                }

				if (null != _data.Tk_Desc)
                {
                    cmd.Parameters["@Tk_Desc"].Value = _data.Tk_Desc;
                }
                else
                {
                    cmd.Parameters["@Tk_Desc"].Value = DBNull.Value;
                }

				if (null != _data.Tk_EstimatedTime)
                {
                    cmd.Parameters["@Tk_EstimatedTime"].Value = _data.Tk_EstimatedTime;
                }
                else
                {
                    cmd.Parameters["@Tk_EstimatedTime"].Value = DBNull.Value;
                }

				if (null != _data.Tk_PercentComplete)
                {
                    cmd.Parameters["@Tk_PercentComplete"].Value = _data.Tk_PercentComplete;
                }
                else
                {
                    cmd.Parameters["@Tk_PercentComplete"].Value = DBNull.Value;
                }

				if (null != _data.Tk_StartDate)
                {
                    cmd.Parameters["@Tk_StartDate"].Value = _data.Tk_StartDate;
                }
                else
                {
                    cmd.Parameters["@Tk_StartDate"].Value = DBNull.Value;
                }

				if (null != _data.Tk_Deadline)
                {
                    cmd.Parameters["@Tk_Deadline"].Value = _data.Tk_Deadline;
                }
                else
                {
                    cmd.Parameters["@Tk_Deadline"].Value = DBNull.Value;
                }

				if (null != _data.Tk_TkSt_Id)
                {
                    cmd.Parameters["@Tk_TkSt_Id"].Value = _data.Tk_TkSt_Id;
                }
                else
                {
                    cmd.Parameters["@Tk_TkSt_Id"].Value = DBNull.Value;
                }

				if (null != _data.Tk_TkPr_Id)
                {
                    cmd.Parameters["@Tk_TkPr_Id"].Value = _data.Tk_TkPr_Id;
                }
                else
                {
                    cmd.Parameters["@Tk_TkPr_Id"].Value = DBNull.Value;
                }

				if (null != _data.Tk_Results)
                {
                    cmd.Parameters["@Tk_Results"].Value = _data.Tk_Results;
                }
                else
                {
                    cmd.Parameters["@Tk_Results"].Value = DBNull.Value;
                }

				if (null != _data.Tk_Remarks)
                {
                    cmd.Parameters["@Tk_Remarks"].Value = _data.Tk_Remarks;
                }
                else
                {
                    cmd.Parameters["@Tk_Remarks"].Value = DBNull.Value;
                }

				cmd.Parameters["@Tk_IsPrivate"].Value = _data.Tk_IsPrivate;

				cmd.Parameters["@Tk_IsActiveRow"].Value = _data.Tk_IsActiveRow;

				if (null != _data.Tk_UserId)
                {
                    cmd.Parameters["@Tk_UserId"].Value = _data.Tk_UserId;
                }
                else
                {
                    cmd.Parameters["@Tk_UserId"].Value = DBNull.Value;
                }

				cmd.Parameters["@Tk_Stamp"].Value = _data.Tk_Stamp;

				if (null != _data.Ct_Id)
                {
                    cmd.Parameters["@Ct_Id"].Value = _data.Ct_Id;
                }
                else
                {
                    cmd.Parameters["@Ct_Id"].Value = DBNull.Value;
                }

				if (null != _data.CtD_Id)
                {
                    cmd.Parameters["@CtD_Id"].Value = _data.CtD_Id;
                }
                else
                {
                    cmd.Parameters["@CtD_Id"].Value = DBNull.Value;
                }

				if (null != _data.Df_Id)
                {
                    cmd.Parameters["@Df_Id"].Value = _data.Df_Id;
                }
                else
                {
                    cmd.Parameters["@Df_Id"].Value = DBNull.Value;
                }

				if (null != _data.Ls_Id)
                {
                    cmd.Parameters["@Ls_Id"].Value = _data.Ls_Id;
                }
                else
                {
                    cmd.Parameters["@Ls_Id"].Value = DBNull.Value;
                }

				if (null != _data.LsTr_Id)
                {
                    cmd.Parameters["@LsTr_Id"].Value = _data.LsTr_Id;
                }
                else
                {
                    cmd.Parameters["@LsTr_Id"].Value = DBNull.Value;
                }

				if (null != _data.Ob_Id)
                {
                    cmd.Parameters["@Ob_Id"].Value = _data.Ob_Id;
                }
                else
                {
                    cmd.Parameters["@Ob_Id"].Value = DBNull.Value;
                }

				if (null != _data.Pp_Id)
                {
                    cmd.Parameters["@Pp_Id"].Value = _data.Pp_Id;
                }
                else
                {
                    cmd.Parameters["@Pp_Id"].Value = DBNull.Value;
                }

				if (null != _data.Wl_Id)
                {
                    cmd.Parameters["@Wl_Id"].Value = _data.Wl_Id;
                }
                else
                {
                    cmd.Parameters["@Wl_Id"].Value = DBNull.Value;
                }

				if (null != _data.RCt_Id)
                {
                    cmd.Parameters["@RCt_Id"].Value = _data.RCt_Id;
                }
                else
                {
                    cmd.Parameters["@RCt_Id"].Value = DBNull.Value;
                }


                cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "Task_Save", SQLScript.CreateScript(cmd));
                }

                //// Log all data changes
                //object[] original = null;
                //if (v_Audit_DataServices.UseAudit == true)
                //{
                //    original = Task_GetById(_data.Tk_Id);
                //}

                cmd.ExecuteNonQuery();
                Tk_Id = ((Guid)(cmd.Parameters["@Tk_Id"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                {
                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                }

                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                //**//  Hack:  we need to get this true or false value from the business object by using a 
                //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
                dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
                //  Set the DataServiceInsertUpdateResponse Result
                if (iSuccess > 0)
                {
                    dataServiceResponse.Result = DataOperationResult.Success;
                    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@Tk_Stamp"].Value));

                    //// Log all data changes
                    //if (v_Audit_DataServices.UseAudit == true)
                    //{
                    //    Task_Values originalValues = null;
                    //    if (original != null)
                    //    {
                    //        originalValues = new Task_Values((object[])original[0], null);
                    //    }
                    //    Task_LogAudit(_data, originalValues);
                    //}

                }
                else if (isConcurrencyGood == false)
                {
                    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
                }
                else
                {
                    dataServiceResponse.Result = DataOperationResult.HandledException;
                }
                if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
                {
                    //  we need a flag to determin if we return new data on a successful insert/update
                    dataServiceResponse.CurrentValues = Task_GetById_ObjectArray(Tk_Id);
                }
                // If the default is to not return the current row, then return the Primary Key here.
                if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
                {
                    dataServiceResponse.guidPrimaryKey = Tk_Id;
                }

                return dataServiceResponse.ReturnAsObjectArray();
            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)), new ValuePair("sqlScript", sqlScript) }, null, ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                dataServiceResponse.Result = DataOperationResult.UnhandledException;
                return dataServiceResponse.ReturnAsObjectArray();
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] Task_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
//                //SqlConnectionHelper helper = new SqlConnectionHelper();
//                conn = DBConnectoinHelper.GetDBConnection();
//                conn.Open();
//                SqlCommand cmd = conn.CreateCommand();
//                cmd.CommandText = "dbo.spTask_del ";
//                cmd.CommandType = System.Data.CommandType.StoredProcedure;
//                cmd.Parameters.Add("@ID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
//                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
//                cmd.Parameters["@Tk_Id"].Value = _data.C._a;
//                cmd.ExecuteNonQuery();
//                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                return null;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_Delete", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_Delete", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] Task_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_SetIsDeleted", new ValuePair[] {new ValuePair("IsDeleted", IsDeleted), new ValuePair("Id", Id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.spTask_putIsDeleted ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = Id;
                cmd.Parameters["@IsDeleted"].Value = IsDeleted;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[2];
                ret[0] = iSuccess;
                ret[1] = Id;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_SetIsDeleted", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] Task_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] Task_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "Task_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods



		#region Other Data Access Methods

        public static object[]  GetTaskCategory_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskCategory_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTaskCategory_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskCategory_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskCategory_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTaskPriority_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskPriority_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTaskPriority_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskPriority_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskPriority_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTaskStatus_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTaskStatus_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskStatus_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTaskType_ComboItemList()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskType_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTaskType_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskType_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTaskType_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTask_lstByContract(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByContract", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_lstByContract";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByContract", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByContract", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTask_lstByContractDt(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByContractDt", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_lstByContractDt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByContractDt", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByContractDt", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTask_lstByDefect(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByDefect", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_lstByDefect";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByDefect", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByDefect", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTask_lstByLease(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByLease", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_lstByLease";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByLease", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByLease", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTask_lstByLeaseTract(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByLeaseTract", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_lstByLeaseTract";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByLeaseTract", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByLeaseTract", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTask_lstByObligation(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByObligation", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_lstByObligation";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByObligation", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByObligation", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTask_lstByPerson(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByPerson", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_lstByPerson";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByPerson", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByPerson", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTask_lstByProspect(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProspect", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_lstByProspect";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProspect", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProspect", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTask_lstByWell(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByWell", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_lstByWell";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByWell", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByWell", IfxTraceCategory.Leave);
            }
        }

        public static object[]  ExecuteTask_XRef_AssignDefectInSection(Guid Prj_Id, Guid Tk_Id, Guid Ct_Id, Int32 DefectNo, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_AssignDefectInSection", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_XRef_AssignDefectInSection";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Prj_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Ct_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@DefectNo", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters["@Prj_Id"].Value = Prj_Id;
				cmd.Parameters["@Tk_Id"].Value = Tk_Id;
				cmd.Parameters["@Ct_Id"].Value = Ct_Id;
				cmd.Parameters["@DefectNo"].Value = DefectNo;
				cmd.Parameters["@UserId"].Value = UserId;
				cmd.ExecuteNonQuery();
				Int32 Success = ((Int32)(cmd.Parameters["@Success"].Value));
				object[] ret = new object[1];
				ret[0] = Success;
				return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_AssignDefectInSection", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_AssignDefectInSection", IfxTraceCategory.Leave);
            }
        }

        public static object[]  ExecuteTask_XRef_RemoveDefect(Guid Tk_Id, Guid Df_Id, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_RemoveDefect", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_XRef_RemoveDefect";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Tk_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Df_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters["@Tk_Id"].Value = Tk_Id;
				cmd.Parameters["@Df_Id"].Value = Df_Id;
				cmd.Parameters["@UserId"].Value = UserId;
				cmd.ExecuteNonQuery();
				Int32 Success = ((Int32)(cmd.Parameters["@Success"].Value));
				object[] ret = new object[1];
				ret[0] = Success;
				return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_RemoveDefect", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_RemoveDefect", IfxTraceCategory.Leave);
            }
        }

        public static object[]  ExecuteTask_XRef_AssignContract(Guid Prj_Id, Guid Tk_Id, String SectionNo, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_AssignContract", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_XRef_AssignContract";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Prj_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tk_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@SectionNo", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters["@Prj_Id"].Value = Prj_Id;
				cmd.Parameters["@Tk_Id"].Value = Tk_Id;
				cmd.Parameters["@SectionNo"].Value = SectionNo;
				cmd.Parameters["@UserId"].Value = UserId;
				cmd.ExecuteNonQuery();
				Int32 Success = ((Int32)(cmd.Parameters["@Success"].Value));
				object[] ret = new object[1];
				ret[0] = Success;
				return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_AssignContract", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_AssignContract", IfxTraceCategory.Leave);
            }
        }

        public static object[]  ExecuteTask_XRef_RemoveContract(Guid Tk_Id, Guid Ct_Id, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_RemoveContract", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_XRef_RemoveContract";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Tk_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Ct_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters["@Tk_Id"].Value = Tk_Id;
				cmd.Parameters["@Ct_Id"].Value = Ct_Id;
				cmd.Parameters["@UserId"].Value = UserId;
				cmd.ExecuteNonQuery();
				Int32 Success = ((Int32)(cmd.Parameters["@Success"].Value));
				object[] ret = new object[1];
				ret[0] = Success;
				return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_RemoveContract", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteTask_XRef_RemoveContract", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetTask_lstByMyTasks(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByMyTasks", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_lstByMyTasks";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByMyTasks", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByMyTasks", IfxTraceCategory.Leave);
            }
        }

        //public static object[]  GetTask_lstByProjectOnly(Guid Tk_Prj_Id )
        //{
        //    Guid? traceId = Guid.NewGuid();
        //    SqlConnection conn = null;
        //    try
        //    {
        //        if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProjectOnly", IfxTraceCategory.Enter);
        //        conn = DBConnectoinHelper.GetDBConnection();
        //        conn.Open();
        //        SqlCommand cmd = conn.CreateCommand();
        //        cmd.CommandText = "spTask_lstByProjectOnly";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.Add("@Tk_Prj_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
        //        cmd.Parameters["@Tk_Prj_Id"].Value = Tk_Prj_Id;
        //        SqlDataReader rdr = cmd.ExecuteReader();

        //        return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProjectOnly", ex);
        //        return null;
        //    }
        //    finally
        //    {
        //        if (conn != null)
        //        {
        //            conn.Dispose();
        //        }
        //        if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByProjectOnly", IfxTraceCategory.Leave);
        //    }
        //}

        public static object[]  GetTask_lstByRelatedContract(Guid Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByRelatedContract", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spTask_lstByRelatedContract";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Id"].Value = Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByRelatedContract", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetTask_lstByRelatedContract", IfxTraceCategory.Leave);
            }
        }

		#endregion Other Data Access Methods

    }
}


