using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;
using vDataServices;

// Gen Timestamp:  12/23/2017 1:26:28 AM

namespace DataServices
{
    public partial class WcStoredProc_DataServices
    {

        #region Initialize Variables

        private static string _as = "DataServices";
        private static string _cn = "WcStoredProc_DataServices";

        #endregion Initialize Variables


        #region Base Methods


        public static object[] WcStoredProc_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcStoredProc_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetById", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProc_GetById_ObjectArray(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcStoredProc_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetById_ObjectArray", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProc_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProc_lstAll";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProc_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProc_lst";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Sp_ApVrsn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Sp_ApVrsn_Id"].Value = id;
				SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProc_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid Sp_Id;
            Int32 iSuccess = 0;
            bool isConcurrencyGood = true;
            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Save", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                WcStoredProc_Values _data = new WcStoredProc_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProc_put";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
				cmd.Parameters.Add("@Sp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@Sp_ApVrsn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_Name", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsCodeGen", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsReadyCodeGen", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsCodeGenComplete", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsTagForCodeGen", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsTagForOther", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_SprocGroups", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsBuildDataAccessMethod", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_BuildListClass", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsFetchEntity", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsStaticList", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsTypeComboItem", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsCreateWireType", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_WireTypeName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_MethodName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_AssocEntity_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_SpRsTp_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_SpRtTp_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsInputParamsAsObjectArray", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsParamsAutoRefresh", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_HasNewParams", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsParamsValid", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsParamValueSetValid", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_HasNewColumns", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsColumnsValid", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsValid", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_Notes", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_IsActiveRow", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Sp_Stamp", SqlDbType.Timestamp).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters
                #region Add Command Parameter Values
				cmd.Parameters["@Sp_Id"].Value = _data.Sp_Id;

				cmd.Parameters["@Sp_ApVrsn_Id"].Value = _data.Sp_ApVrsn_Id;

				if (null != _data.Sp_Name)
                {
                    cmd.Parameters["@Sp_Name"].Value = _data.Sp_Name;
                }
                else
                {
                    cmd.Parameters["@Sp_Name"].Value = DBNull.Value;
                }

				cmd.Parameters["@Sp_IsCodeGen"].Value = _data.Sp_IsCodeGen;

				cmd.Parameters["@Sp_IsReadyCodeGen"].Value = _data.Sp_IsReadyCodeGen;

				cmd.Parameters["@Sp_IsCodeGenComplete"].Value = _data.Sp_IsCodeGenComplete;

				cmd.Parameters["@Sp_IsTagForCodeGen"].Value = _data.Sp_IsTagForCodeGen;

				cmd.Parameters["@Sp_IsTagForOther"].Value = _data.Sp_IsTagForOther;

				if (null != _data.Sp_SprocGroups)
                {
                    cmd.Parameters["@Sp_SprocGroups"].Value = _data.Sp_SprocGroups;
                }
                else
                {
                    cmd.Parameters["@Sp_SprocGroups"].Value = DBNull.Value;
                }

				cmd.Parameters["@Sp_IsBuildDataAccessMethod"].Value = _data.Sp_IsBuildDataAccessMethod;

				cmd.Parameters["@Sp_BuildListClass"].Value = _data.Sp_BuildListClass;

				cmd.Parameters["@Sp_IsFetchEntity"].Value = _data.Sp_IsFetchEntity;

				cmd.Parameters["@Sp_IsStaticList"].Value = _data.Sp_IsStaticList;

				if (null != _data.Sp_IsTypeComboItem)
                {
                    cmd.Parameters["@Sp_IsTypeComboItem"].Value = _data.Sp_IsTypeComboItem;
                }
                else
                {
                    cmd.Parameters["@Sp_IsTypeComboItem"].Value = DBNull.Value;
                }

				cmd.Parameters["@Sp_IsCreateWireType"].Value = _data.Sp_IsCreateWireType;

				if (null != _data.Sp_WireTypeName)
                {
                    cmd.Parameters["@Sp_WireTypeName"].Value = _data.Sp_WireTypeName;
                }
                else
                {
                    cmd.Parameters["@Sp_WireTypeName"].Value = DBNull.Value;
                }

				if (null != _data.Sp_MethodName)
                {
                    cmd.Parameters["@Sp_MethodName"].Value = _data.Sp_MethodName;
                }
                else
                {
                    cmd.Parameters["@Sp_MethodName"].Value = DBNull.Value;
                }

				if (null != _data.Sp_AssocEntity_Id)
                {
                    cmd.Parameters["@Sp_AssocEntity_Id"].Value = _data.Sp_AssocEntity_Id;
                }
                else
                {
                    cmd.Parameters["@Sp_AssocEntity_Id"].Value = DBNull.Value;
                }

				if (null != _data.Sp_SpRsTp_Id)
                {
                    cmd.Parameters["@Sp_SpRsTp_Id"].Value = _data.Sp_SpRsTp_Id;
                }
                else
                {
                    cmd.Parameters["@Sp_SpRsTp_Id"].Value = DBNull.Value;
                }

				if (null != _data.Sp_SpRtTp_Id)
                {
                    cmd.Parameters["@Sp_SpRtTp_Id"].Value = _data.Sp_SpRtTp_Id;
                }
                else
                {
                    cmd.Parameters["@Sp_SpRtTp_Id"].Value = DBNull.Value;
                }

				cmd.Parameters["@Sp_IsInputParamsAsObjectArray"].Value = _data.Sp_IsInputParamsAsObjectArray;

				cmd.Parameters["@Sp_IsParamsAutoRefresh"].Value = _data.Sp_IsParamsAutoRefresh;

				cmd.Parameters["@Sp_HasNewParams"].Value = _data.Sp_HasNewParams;

				cmd.Parameters["@Sp_IsParamsValid"].Value = _data.Sp_IsParamsValid;

				cmd.Parameters["@Sp_IsParamValueSetValid"].Value = _data.Sp_IsParamValueSetValid;

				cmd.Parameters["@Sp_HasNewColumns"].Value = _data.Sp_HasNewColumns;

				cmd.Parameters["@Sp_IsColumnsValid"].Value = _data.Sp_IsColumnsValid;

				cmd.Parameters["@Sp_IsValid"].Value = _data.Sp_IsValid;

				if (null != _data.Sp_Notes)
                {
                    cmd.Parameters["@Sp_Notes"].Value = _data.Sp_Notes;
                }
                else
                {
                    cmd.Parameters["@Sp_Notes"].Value = DBNull.Value;
                }

				cmd.Parameters["@Sp_IsActiveRow"].Value = _data.Sp_IsActiveRow;

				if (null != _data.Sp_UserId)
                {
                    cmd.Parameters["@Sp_UserId"].Value = _data.Sp_UserId;
                }
                else
                {
                    cmd.Parameters["@Sp_UserId"].Value = DBNull.Value;
                }

				cmd.Parameters["@Sp_Stamp"].Value = _data.Sp_Stamp;


                cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "WcStoredProc_Save", SQLScript.CreateScript(cmd));
                }

                //// Log all data changes
                //object[] original = null;
                //if (v_Audit_DataServices.UseAudit == true)
                //{
                //    original = WcStoredProc_GetById(_data.Sp_Id);
                //}

                cmd.ExecuteNonQuery();
                Sp_Id = ((Guid)(cmd.Parameters["@Sp_Id"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                {
                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                }

                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                //**//  Hack:  we need to get this true or false value from the business object by using a 
                //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
                dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
                //  Set the DataServiceInsertUpdateResponse Result
                if (iSuccess > 0)
                {
                    dataServiceResponse.Result = DataOperationResult.Success;
                    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@Sp_Stamp"].Value));

                    //// Log all data changes
                    //if (v_Audit_DataServices.UseAudit == true)
                    //{
                    //    WcStoredProc_Values originalValues = null;
                    //    if (original != null)
                    //    {
                    //        originalValues = new WcStoredProc_Values((object[])original[0], null);
                    //    }
                    //    WcStoredProc_LogAudit(_data, originalValues);
                    //}

                }
                else if (isConcurrencyGood == false)
                {
                    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
                }
                else
                {
                    dataServiceResponse.Result = DataOperationResult.HandledException;
                }
                if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
                {
                    //  we need a flag to determin if we return new data on a successful insert/update
                    dataServiceResponse.CurrentValues = WcStoredProc_GetById_ObjectArray(Sp_Id);
                }
                // If the default is to not return the current row, then return the Primary Key here.
                if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
                {
                    dataServiceResponse.guidPrimaryKey = Sp_Id;
                }

                return dataServiceResponse.ReturnAsObjectArray();
            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)), new ValuePair("sqlScript", sqlScript) }, null, ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                dataServiceResponse.Result = DataOperationResult.UnhandledException;
                return dataServiceResponse.ReturnAsObjectArray();
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProc_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                WcStoredProc_Values _data = new WcStoredProc_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProc_del ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = _data.Sp_Id;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[1];
                ret[0] = iSuccess;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Delete", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Delete", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProc_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SetIsDeleted", new ValuePair[] {new ValuePair("IsDeleted", IsDeleted), new ValuePair("Id", Id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProc_putIsDeleted ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = Id;
                cmd.Parameters["@IsDeleted"].Value = IsDeleted;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[2];
                ret[0] = iSuccess;
                ret[1] = Id;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_SetIsDeleted", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProc_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcStoredProc_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcStoredProc_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods



		#region Other Data Access Methods

		#endregion Other Data Access Methods

    }
}


