using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;
using vDataServices;

// Gen Timestamp:  1/7/2018 10:04:38 PM

namespace DataServices
{
    public partial class WcTable_DataServices
    {

        #region Initialize Variables

        private static string _as = "DataServices";
        private static string _cn = "WcTable_DataServices";

        #endregion Initialize Variables


        #region Base Methods


        public static object[] WcTable_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcTable_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetById", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTable_GetById_ObjectArray(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcTable_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetById_ObjectArray", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTable_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTable_lstAll";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTable_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTable_lst";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Tb_ApVrsn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Tb_ApVrsn_Id"].Value = id;
				SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTable_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid Tb_Id;
            Int32 iSuccess = 0;
            bool isConcurrencyGood = true;
            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Save", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                WcTable_Values _data = new WcTable_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTable_put";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
				cmd.Parameters.Add("@Tb_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@Tb_ApVrsn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_Name", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_EntityRootName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_VariableName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_ScreenCaption", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_Description", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_DevelopmentNote", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_UseLegacyConnectionCode", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_DbCnSK_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_ApDbScm_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_UIAssemblyName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_UINamespace", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_UIAssemblyPath", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_ProxyAssemblyName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_ProxyNamespace", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_ProxyAssemblyPath", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_WireTypeAssemblyName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_WireTypeNamespace", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_WireTypePath", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_WebServiceName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_WebServiceFolder", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_DataServiceAssemblyName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_DataServiceNamespace", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_DataServicePath", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_UseTilesInPropsScreen", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_UseGridColumnGroups", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_UseGridDataSourceCombo", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_PkIsIdentity", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_IsVirtual", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_IsScreenPlaceHolder", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_IsNotEntity", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_IsMany2Many", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_UseLastModifiedByUserNameInSproc", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_UseUserTimeStamp", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_UseForAudit", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_IsAllowDelete", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_CnfgGdMnu_MenuRow_IsVisible", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_CnfgGdMnu_GridTools_IsVisible", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_CnfgGdMnu_SplitScreen_IsVisible", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_CnfgGdMnu_SplitScreen_IsSplit_Default", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_CnfgGdMnu_NavColumnWidth", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_CnfgGdMnu_IsReadOnly", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_CnfgGdMnu_IsAllowNewRow", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_CnfgGdMnu_ExcelExport_IsVisible", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_CnfgGdMnu_ColumnChooser_IsVisible", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_CnfgGdMnu_ShowHideColBtn_IsVisible", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_CnfgGdMnu_RefreshGrid_IsVisible", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_IsInputComplete", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_IsCodeGen", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_IsReadyCodeGen", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_IsCodeGenComplete", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_IsTagForCodeGen", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_IsTagForOther", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_IsActiveRow", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_Stamp", SqlDbType.Timestamp).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters
                #region Add Command Parameter Values
				cmd.Parameters["@Tb_Id"].Value = _data.Tb_Id;

				cmd.Parameters["@Tb_ApVrsn_Id"].Value = _data.Tb_ApVrsn_Id;

				if (null != _data.Tb_Name)
                {
                    cmd.Parameters["@Tb_Name"].Value = _data.Tb_Name;
                }
                else
                {
                    cmd.Parameters["@Tb_Name"].Value = DBNull.Value;
                }

				if (null != _data.Tb_EntityRootName)
                {
                    cmd.Parameters["@Tb_EntityRootName"].Value = _data.Tb_EntityRootName;
                }
                else
                {
                    cmd.Parameters["@Tb_EntityRootName"].Value = DBNull.Value;
                }

				if (null != _data.Tb_VariableName)
                {
                    cmd.Parameters["@Tb_VariableName"].Value = _data.Tb_VariableName;
                }
                else
                {
                    cmd.Parameters["@Tb_VariableName"].Value = DBNull.Value;
                }

				if (null != _data.Tb_ScreenCaption)
                {
                    cmd.Parameters["@Tb_ScreenCaption"].Value = _data.Tb_ScreenCaption;
                }
                else
                {
                    cmd.Parameters["@Tb_ScreenCaption"].Value = DBNull.Value;
                }

				if (null != _data.Tb_Description)
                {
                    cmd.Parameters["@Tb_Description"].Value = _data.Tb_Description;
                }
                else
                {
                    cmd.Parameters["@Tb_Description"].Value = DBNull.Value;
                }

				if (null != _data.Tb_DevelopmentNote)
                {
                    cmd.Parameters["@Tb_DevelopmentNote"].Value = _data.Tb_DevelopmentNote;
                }
                else
                {
                    cmd.Parameters["@Tb_DevelopmentNote"].Value = DBNull.Value;
                }

				cmd.Parameters["@Tb_UseLegacyConnectionCode"].Value = _data.Tb_UseLegacyConnectionCode;

				if (null != _data.Tb_DbCnSK_Id)
                {
                    cmd.Parameters["@Tb_DbCnSK_Id"].Value = _data.Tb_DbCnSK_Id;
                }
                else
                {
                    cmd.Parameters["@Tb_DbCnSK_Id"].Value = DBNull.Value;
                }

				if (null != _data.Tb_ApDbScm_Id)
                {
                    cmd.Parameters["@Tb_ApDbScm_Id"].Value = _data.Tb_ApDbScm_Id;
                }
                else
                {
                    cmd.Parameters["@Tb_ApDbScm_Id"].Value = DBNull.Value;
                }

				if (null != _data.Tb_UIAssemblyName)
                {
                    cmd.Parameters["@Tb_UIAssemblyName"].Value = _data.Tb_UIAssemblyName;
                }
                else
                {
                    cmd.Parameters["@Tb_UIAssemblyName"].Value = DBNull.Value;
                }

				if (null != _data.Tb_UINamespace)
                {
                    cmd.Parameters["@Tb_UINamespace"].Value = _data.Tb_UINamespace;
                }
                else
                {
                    cmd.Parameters["@Tb_UINamespace"].Value = DBNull.Value;
                }

				if (null != _data.Tb_UIAssemblyPath)
                {
                    cmd.Parameters["@Tb_UIAssemblyPath"].Value = _data.Tb_UIAssemblyPath;
                }
                else
                {
                    cmd.Parameters["@Tb_UIAssemblyPath"].Value = DBNull.Value;
                }

				if (null != _data.Tb_ProxyAssemblyName)
                {
                    cmd.Parameters["@Tb_ProxyAssemblyName"].Value = _data.Tb_ProxyAssemblyName;
                }
                else
                {
                    cmd.Parameters["@Tb_ProxyAssemblyName"].Value = DBNull.Value;
                }

				if (null != _data.Tb_ProxyNamespace)
                {
                    cmd.Parameters["@Tb_ProxyNamespace"].Value = _data.Tb_ProxyNamespace;
                }
                else
                {
                    cmd.Parameters["@Tb_ProxyNamespace"].Value = DBNull.Value;
                }

				if (null != _data.Tb_ProxyAssemblyPath)
                {
                    cmd.Parameters["@Tb_ProxyAssemblyPath"].Value = _data.Tb_ProxyAssemblyPath;
                }
                else
                {
                    cmd.Parameters["@Tb_ProxyAssemblyPath"].Value = DBNull.Value;
                }

				if (null != _data.Tb_WireTypeAssemblyName)
                {
                    cmd.Parameters["@Tb_WireTypeAssemblyName"].Value = _data.Tb_WireTypeAssemblyName;
                }
                else
                {
                    cmd.Parameters["@Tb_WireTypeAssemblyName"].Value = DBNull.Value;
                }

				if (null != _data.Tb_WireTypeNamespace)
                {
                    cmd.Parameters["@Tb_WireTypeNamespace"].Value = _data.Tb_WireTypeNamespace;
                }
                else
                {
                    cmd.Parameters["@Tb_WireTypeNamespace"].Value = DBNull.Value;
                }

				if (null != _data.Tb_WireTypePath)
                {
                    cmd.Parameters["@Tb_WireTypePath"].Value = _data.Tb_WireTypePath;
                }
                else
                {
                    cmd.Parameters["@Tb_WireTypePath"].Value = DBNull.Value;
                }

				if (null != _data.Tb_WebServiceName)
                {
                    cmd.Parameters["@Tb_WebServiceName"].Value = _data.Tb_WebServiceName;
                }
                else
                {
                    cmd.Parameters["@Tb_WebServiceName"].Value = DBNull.Value;
                }

				if (null != _data.Tb_WebServiceFolder)
                {
                    cmd.Parameters["@Tb_WebServiceFolder"].Value = _data.Tb_WebServiceFolder;
                }
                else
                {
                    cmd.Parameters["@Tb_WebServiceFolder"].Value = DBNull.Value;
                }

				if (null != _data.Tb_DataServiceAssemblyName)
                {
                    cmd.Parameters["@Tb_DataServiceAssemblyName"].Value = _data.Tb_DataServiceAssemblyName;
                }
                else
                {
                    cmd.Parameters["@Tb_DataServiceAssemblyName"].Value = DBNull.Value;
                }

				if (null != _data.Tb_DataServiceNamespace)
                {
                    cmd.Parameters["@Tb_DataServiceNamespace"].Value = _data.Tb_DataServiceNamespace;
                }
                else
                {
                    cmd.Parameters["@Tb_DataServiceNamespace"].Value = DBNull.Value;
                }

				if (null != _data.Tb_DataServicePath)
                {
                    cmd.Parameters["@Tb_DataServicePath"].Value = _data.Tb_DataServicePath;
                }
                else
                {
                    cmd.Parameters["@Tb_DataServicePath"].Value = DBNull.Value;
                }

				cmd.Parameters["@Tb_UseTilesInPropsScreen"].Value = _data.Tb_UseTilesInPropsScreen;

				cmd.Parameters["@Tb_UseGridColumnGroups"].Value = _data.Tb_UseGridColumnGroups;

				cmd.Parameters["@Tb_UseGridDataSourceCombo"].Value = _data.Tb_UseGridDataSourceCombo;

				cmd.Parameters["@Tb_PkIsIdentity"].Value = _data.Tb_PkIsIdentity;

				cmd.Parameters["@Tb_IsVirtual"].Value = _data.Tb_IsVirtual;

				cmd.Parameters["@Tb_IsScreenPlaceHolder"].Value = _data.Tb_IsScreenPlaceHolder;

				cmd.Parameters["@Tb_IsNotEntity"].Value = _data.Tb_IsNotEntity;

				cmd.Parameters["@Tb_IsMany2Many"].Value = _data.Tb_IsMany2Many;

				cmd.Parameters["@Tb_UseLastModifiedByUserNameInSproc"].Value = _data.Tb_UseLastModifiedByUserNameInSproc;

				cmd.Parameters["@Tb_UseUserTimeStamp"].Value = _data.Tb_UseUserTimeStamp;

				cmd.Parameters["@Tb_UseForAudit"].Value = _data.Tb_UseForAudit;

				cmd.Parameters["@Tb_IsAllowDelete"].Value = _data.Tb_IsAllowDelete;

				cmd.Parameters["@Tb_CnfgGdMnu_MenuRow_IsVisible"].Value = _data.Tb_CnfgGdMnu_MenuRow_IsVisible;

				cmd.Parameters["@Tb_CnfgGdMnu_GridTools_IsVisible"].Value = _data.Tb_CnfgGdMnu_GridTools_IsVisible;

				cmd.Parameters["@Tb_CnfgGdMnu_SplitScreen_IsVisible"].Value = _data.Tb_CnfgGdMnu_SplitScreen_IsVisible;

				cmd.Parameters["@Tb_CnfgGdMnu_SplitScreen_IsSplit_Default"].Value = _data.Tb_CnfgGdMnu_SplitScreen_IsSplit_Default;

				cmd.Parameters["@Tb_CnfgGdMnu_NavColumnWidth"].Value = _data.Tb_CnfgGdMnu_NavColumnWidth;

				cmd.Parameters["@Tb_CnfgGdMnu_IsReadOnly"].Value = _data.Tb_CnfgGdMnu_IsReadOnly;

				cmd.Parameters["@Tb_CnfgGdMnu_IsAllowNewRow"].Value = _data.Tb_CnfgGdMnu_IsAllowNewRow;

				cmd.Parameters["@Tb_CnfgGdMnu_ExcelExport_IsVisible"].Value = _data.Tb_CnfgGdMnu_ExcelExport_IsVisible;

				cmd.Parameters["@Tb_CnfgGdMnu_ColumnChooser_IsVisible"].Value = _data.Tb_CnfgGdMnu_ColumnChooser_IsVisible;

				cmd.Parameters["@Tb_CnfgGdMnu_ShowHideColBtn_IsVisible"].Value = _data.Tb_CnfgGdMnu_ShowHideColBtn_IsVisible;

				cmd.Parameters["@Tb_CnfgGdMnu_RefreshGrid_IsVisible"].Value = _data.Tb_CnfgGdMnu_RefreshGrid_IsVisible;

				cmd.Parameters["@Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible"].Value = _data.Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible;

				cmd.Parameters["@Tb_IsInputComplete"].Value = _data.Tb_IsInputComplete;

				cmd.Parameters["@Tb_IsCodeGen"].Value = _data.Tb_IsCodeGen;

				cmd.Parameters["@Tb_IsReadyCodeGen"].Value = _data.Tb_IsReadyCodeGen;

				cmd.Parameters["@Tb_IsCodeGenComplete"].Value = _data.Tb_IsCodeGenComplete;

				cmd.Parameters["@Tb_IsTagForCodeGen"].Value = _data.Tb_IsTagForCodeGen;

				cmd.Parameters["@Tb_IsTagForOther"].Value = _data.Tb_IsTagForOther;

				cmd.Parameters["@Tb_IsActiveRow"].Value = _data.Tb_IsActiveRow;

				if (null != _data.Tb_UserId)
                {
                    cmd.Parameters["@Tb_UserId"].Value = _data.Tb_UserId;
                }
                else
                {
                    cmd.Parameters["@Tb_UserId"].Value = DBNull.Value;
                }

				cmd.Parameters["@Tb_Stamp"].Value = _data.Tb_Stamp;


                cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "WcTable_Save", SQLScript.CreateScript(cmd));
                }

                //// Log all data changes
                //object[] original = null;
                //if (v_Audit_DataServices.UseAudit == true)
                //{
                //    original = WcTable_GetById(_data.Tb_Id);
                //}

                cmd.ExecuteNonQuery();
                Tb_Id = ((Guid)(cmd.Parameters["@Tb_Id"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                {
                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                }

                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                //**//  Hack:  we need to get this true or false value from the business object by using a 
                //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
                dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
                //  Set the DataServiceInsertUpdateResponse Result
                if (iSuccess > 0)
                {
                    dataServiceResponse.Result = DataOperationResult.Success;
                    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@Tb_Stamp"].Value));

                    //// Log all data changes
                    //if (v_Audit_DataServices.UseAudit == true)
                    //{
                    //    WcTable_Values originalValues = null;
                    //    if (original != null)
                    //    {
                    //        originalValues = new WcTable_Values((object[])original[0], null);
                    //    }
                    //    WcTable_LogAudit(_data, originalValues);
                    //}

                }
                else if (isConcurrencyGood == false)
                {
                    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
                }
                else
                {
                    dataServiceResponse.Result = DataOperationResult.HandledException;
                }
                if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
                {
                    //  we need a flag to determin if we return new data on a successful insert/update
                    dataServiceResponse.CurrentValues = WcTable_GetById_ObjectArray(Tb_Id);
                }
                // If the default is to not return the current row, then return the Primary Key here.
                if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
                {
                    dataServiceResponse.guidPrimaryKey = Tb_Id;
                }

                return dataServiceResponse.ReturnAsObjectArray();
            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)), new ValuePair("sqlScript", sqlScript) }, null, ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                dataServiceResponse.Result = DataOperationResult.UnhandledException;
                return dataServiceResponse.ReturnAsObjectArray();
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTable_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                WcTable_Values _data = new WcTable_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTable_del ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = _data.Tb_Id;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[1];
                ret[0] = iSuccess;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Delete", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Delete", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTable_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_SetIsDeleted", new ValuePair[] {new ValuePair("IsDeleted", IsDeleted), new ValuePair("Id", Id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTable_putIsDeleted ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = Id;
                cmd.Parameters["@IsDeleted"].Value = IsDeleted;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[2];
                ret[0] = iSuccess;
                ret[1] = Id;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_SetIsDeleted", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTable_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTable_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTable_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods



		#region Other Data Access Methods

        public static object[]  GetWcTable_ComboItemList(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTable_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTable_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@ApVrsn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@ApVrsn_Id"].Value = ApVrsn_Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTable_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetWcTableColumn_lstByTable_ComboItemList(Guid Tb_Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTableColumn_lstByTable_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Tb_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Tb_Id"].Value = Tb_Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByTable_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByTable_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetwcTableGroup_Assignments(Guid ApVrsn_Id, Guid Tb_Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableGroup_Assignments", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spwcTableGroup_Assignments";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@ApVrsn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@ApVrsn_Id"].Value = ApVrsn_Id;
				cmd.Parameters["@Tb_Id"].Value = Tb_Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableGroup_Assignments", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableGroup_Assignments", IfxTraceCategory.Leave);
            }
        }

        public static object[]  ExecutewcTableGroup_AssignTables(Boolean Insert, Guid TbGrp2Tb_TbGrp_Id, Guid TbGrp2Tb_Tb_Id, Guid TbGrp2Tb_CreatedUserId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableGroup_AssignTables", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spwcTableGroup_AssignTables";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Insert", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbGrp2Tb_TbGrp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbGrp2Tb_Tb_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbGrp2Tb_CreatedUserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
				cmd.Parameters["@Insert"].Value = Insert;
				cmd.Parameters["@TbGrp2Tb_TbGrp_Id"].Value = TbGrp2Tb_TbGrp_Id;
				cmd.Parameters["@TbGrp2Tb_Tb_Id"].Value = TbGrp2Tb_Tb_Id;
				cmd.Parameters["@TbGrp2Tb_CreatedUserId"].Value = TbGrp2Tb_CreatedUserId;
				cmd.ExecuteNonQuery();
				Int32 Success = ((Int32)(cmd.Parameters["@Success"].Value));
				//Guid ErrorLogId = ((Guid)(cmd.Parameters["@ErrorLogId"].Value));
				object[] ret = new object[1];
				ret[0] = Success;

                return ret;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableGroup_AssignTables", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableGroup_AssignTables", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyId(Guid DbCnSK_Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyId", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTable_lstNamesFromAppAndDB_ByConnectionKeyId";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@DbCnSK_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@DbCnSK_Id"].Value = DbCnSK_Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyId", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTable_lstNamesFromAppAndDB_ByConnectionKeyId", IfxTraceCategory.Leave);
            }
        }

        public static object[]  ExecuteWcTable_InsertTable_ByConnectionKeyId(Guid DbCnSK_Id, String TableName, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTable_InsertTable_ByConnectionKeyId", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTable_InsertTable_ByConnectionKeyId";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@DbCnSK_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TableName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tb_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters["@DbCnSK_Id"].Value = DbCnSK_Id;
				cmd.Parameters["@TableName"].Value = TableName;
				cmd.Parameters["@UserId"].Value = UserId;
				cmd.ExecuteNonQuery();
				Guid Tb_Id = ((Guid)(cmd.Parameters["@Tb_Id"].Value));
				Int32 Success = ((Int32)(cmd.Parameters["@Success"].Value));
				object[] ret = new object[2];
				ret[0] = Tb_Id;
				ret[1] = Success;

                return ret;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTable_InsertTable_ByConnectionKeyId", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTable_InsertTable_ByConnectionKeyId", IfxTraceCategory.Leave);
            }
        }

		#endregion Other Data Access Methods

    }
}


