using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;
using vDataServices;

// Gen Timestamp:  1/3/2018 11:03:49 PM

namespace DataServices
{
    public partial class WcApplicationVersion_DataServices
    {

        #region Initialize Variables

        private static string _as = "DataServices";
        private static string _cn = "WcApplicationVersion_DataServices";

        #endregion Initialize Variables


        #region Base Methods


        public static object[] WcApplicationVersion_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcApplicationVersion_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetById", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcApplicationVersion_GetById_ObjectArray(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcApplicationVersion_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetById_ObjectArray", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcApplicationVersion_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcApplicationVersion_lstAll";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcApplicationVersion_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcApplicationVersion_lst";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@ApVrsn_Ap_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@ApVrsn_Ap_Id"].Value = id;
				SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcApplicationVersion_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid ApVrsn_Id;
            Int32 iSuccess = 0;
            bool isConcurrencyGood = true;
            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Save", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                WcApplicationVersion_Values _data = new WcApplicationVersion_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "spWcApplicationVersion_put";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
				cmd.Parameters.Add("@ApVrsn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@ApVrsn_Ap_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_MajorVersion", SqlDbType.TinyInt).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_MinorVersion", SqlDbType.TinyInt).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_VersionIteration", SqlDbType.TinyInt).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_Server", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_DbName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_SolutionPath", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_DefaultUIAssembly", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_DefaultUIAssemblyPath", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_DefaultWireTypePath", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_WebServerURL", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_WebsiteCodeFolderPath", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_WebserviceCodeFolderPath", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_StoredProcCodeFolder", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_UseLegacyConnectionCode", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_IsMulticultural", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_Notes", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_IsActiveRow", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@ApVrsn_Stamp", SqlDbType.Timestamp).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters
                #region Add Command Parameter Values
				cmd.Parameters["@ApVrsn_Id"].Value = _data.ApVrsn_Id;

				cmd.Parameters["@ApVrsn_Ap_Id"].Value = _data.ApVrsn_Ap_Id;

				cmd.Parameters["@ApVrsn_MajorVersion"].Value = _data.ApVrsn_MajorVersion;

				if (null != _data.ApVrsn_MinorVersion)
                {
                    cmd.Parameters["@ApVrsn_MinorVersion"].Value = _data.ApVrsn_MinorVersion;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_MinorVersion"].Value = DBNull.Value;
                }

				if (null != _data.ApVrsn_VersionIteration)
                {
                    cmd.Parameters["@ApVrsn_VersionIteration"].Value = _data.ApVrsn_VersionIteration;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_VersionIteration"].Value = DBNull.Value;
                }

				if (null != _data.ApVrsn_Server)
                {
                    cmd.Parameters["@ApVrsn_Server"].Value = _data.ApVrsn_Server;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_Server"].Value = DBNull.Value;
                }

				if (null != _data.ApVrsn_DbName)
                {
                    cmd.Parameters["@ApVrsn_DbName"].Value = _data.ApVrsn_DbName;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_DbName"].Value = DBNull.Value;
                }

				if (null != _data.ApVrsn_SolutionPath)
                {
                    cmd.Parameters["@ApVrsn_SolutionPath"].Value = _data.ApVrsn_SolutionPath;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_SolutionPath"].Value = DBNull.Value;
                }

				if (null != _data.ApVrsn_DefaultUIAssembly)
                {
                    cmd.Parameters["@ApVrsn_DefaultUIAssembly"].Value = _data.ApVrsn_DefaultUIAssembly;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_DefaultUIAssembly"].Value = DBNull.Value;
                }

				if (null != _data.ApVrsn_DefaultUIAssemblyPath)
                {
                    cmd.Parameters["@ApVrsn_DefaultUIAssemblyPath"].Value = _data.ApVrsn_DefaultUIAssemblyPath;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_DefaultUIAssemblyPath"].Value = DBNull.Value;
                }

				if (null != _data.ApVrsn_DefaultWireTypePath)
                {
                    cmd.Parameters["@ApVrsn_DefaultWireTypePath"].Value = _data.ApVrsn_DefaultWireTypePath;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_DefaultWireTypePath"].Value = DBNull.Value;
                }

				if (null != _data.ApVrsn_WebServerURL)
                {
                    cmd.Parameters["@ApVrsn_WebServerURL"].Value = _data.ApVrsn_WebServerURL;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_WebServerURL"].Value = DBNull.Value;
                }

				if (null != _data.ApVrsn_WebsiteCodeFolderPath)
                {
                    cmd.Parameters["@ApVrsn_WebsiteCodeFolderPath"].Value = _data.ApVrsn_WebsiteCodeFolderPath;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_WebsiteCodeFolderPath"].Value = DBNull.Value;
                }

				if (null != _data.ApVrsn_WebserviceCodeFolderPath)
                {
                    cmd.Parameters["@ApVrsn_WebserviceCodeFolderPath"].Value = _data.ApVrsn_WebserviceCodeFolderPath;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_WebserviceCodeFolderPath"].Value = DBNull.Value;
                }

				if (null != _data.ApVrsn_StoredProcCodeFolder)
                {
                    cmd.Parameters["@ApVrsn_StoredProcCodeFolder"].Value = _data.ApVrsn_StoredProcCodeFolder;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_StoredProcCodeFolder"].Value = DBNull.Value;
                }

				cmd.Parameters["@ApVrsn_UseLegacyConnectionCode"].Value = _data.ApVrsn_UseLegacyConnectionCode;

				cmd.Parameters["@ApVrsn_IsMulticultural"].Value = _data.ApVrsn_IsMulticultural;

				if (null != _data.ApVrsn_Notes)
                {
                    cmd.Parameters["@ApVrsn_Notes"].Value = _data.ApVrsn_Notes;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_Notes"].Value = DBNull.Value;
                }

				cmd.Parameters["@ApVrsn_IsActiveRow"].Value = _data.ApVrsn_IsActiveRow;

				if (null != _data.ApVrsn_UserId)
                {
                    cmd.Parameters["@ApVrsn_UserId"].Value = _data.ApVrsn_UserId;
                }
                else
                {
                    cmd.Parameters["@ApVrsn_UserId"].Value = DBNull.Value;
                }

				cmd.Parameters["@ApVrsn_Stamp"].Value = _data.ApVrsn_Stamp;


                cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "WcApplicationVersion_Save", SQLScript.CreateScript(cmd));
                }

                //// Log all data changes
                //object[] original = null;
                //if (v_Audit_DataServices.UseAudit == true)
                //{
                //    original = WcApplicationVersion_GetById(_data.ApVrsn_Id);
                //}

                cmd.ExecuteNonQuery();
                ApVrsn_Id = ((Guid)(cmd.Parameters["@ApVrsn_Id"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                {
                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                }

                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                //**//  Hack:  we need to get this true or false value from the business object by using a 
                //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
                dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
                //  Set the DataServiceInsertUpdateResponse Result
                if (iSuccess > 0)
                {
                    dataServiceResponse.Result = DataOperationResult.Success;
                    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@ApVrsn_Stamp"].Value));

                    //// Log all data changes
                    //if (v_Audit_DataServices.UseAudit == true)
                    //{
                    //    WcApplicationVersion_Values originalValues = null;
                    //    if (original != null)
                    //    {
                    //        originalValues = new WcApplicationVersion_Values((object[])original[0], null);
                    //    }
                    //    WcApplicationVersion_LogAudit(_data, originalValues);
                    //}

                }
                else if (isConcurrencyGood == false)
                {
                    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
                }
                else
                {
                    dataServiceResponse.Result = DataOperationResult.HandledException;
                }
                if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
                {
                    //  we need a flag to determin if we return new data on a successful insert/update
                    dataServiceResponse.CurrentValues = WcApplicationVersion_GetById_ObjectArray(ApVrsn_Id);
                }
                // If the default is to not return the current row, then return the Primary Key here.
                if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
                {
                    dataServiceResponse.guidPrimaryKey = ApVrsn_Id;
                }

                return dataServiceResponse.ReturnAsObjectArray();
            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)), new ValuePair("sqlScript", sqlScript) }, null, ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                dataServiceResponse.Result = DataOperationResult.UnhandledException;
                return dataServiceResponse.ReturnAsObjectArray();
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcApplicationVersion_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                WcApplicationVersion_Values _data = new WcApplicationVersion_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcApplicationVersion_del ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = _data.ApVrsn_Id;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[1];
                ret[0] = iSuccess;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Delete", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Delete", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcApplicationVersion_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_SetIsDeleted", new ValuePair[] {new ValuePair("IsDeleted", IsDeleted), new ValuePair("Id", Id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcApplicationVersion_putIsDeleted ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = Id;
                cmd.Parameters["@IsDeleted"].Value = IsDeleted;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[2];
                ret[0] = iSuccess;
                ret[1] = Id;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_SetIsDeleted", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcApplicationVersion_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcApplicationVersion_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcApplicationVersion_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods



		#region Other Data Access Methods

        public static object[]  GetWcStoredProc_ComboItemList(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcStoredProc_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@ApVrsn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@ApVrsn_Id"].Value = ApVrsn_Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcStoredProc_ComboItemList", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetWcTableColumn_lstByApVersion_Id_ComboItemList(Guid ApVrsn_Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByApVersion_Id_ComboItemList", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTableColumn_lstByApVersion_Id_ComboItemList";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@ApVrsn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@ApVrsn_Id"].Value = ApVrsn_Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByApVersion_Id_ComboItemList", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstByApVersion_Id_ComboItemList", IfxTraceCategory.Leave);
            }
        }

		#endregion Other Data Access Methods

    }
}


