using System;
using System.Data.SqlClient;
using System.Data;
using TypeServices;
using EntityWireType;
using Ifx;
using vDataServices;

// Gen Timestamp:  1/17/2018 11:08:42 AM

namespace DataServices
{
    public partial class WcTableColumn_DataServices
    {

        #region Initialize Variables

        private static string _as = "DataServices";
        private static string _cn = "WcTableColumn_DataServices";

        #endregion Initialize Variables


        #region Base Methods


        public static object[] WcTableColumn_GetById(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetById", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcTableColumn_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetById", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetById", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTableColumn_GetById_ObjectArray(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetById_ObjectArray", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                SqlDataReader rdr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "spWcTableColumn_row", new SqlParameter("@Id", id));
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetById_ObjectArray", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetById_ObjectArray", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTableColumn_GetAll()
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetAll", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTableColumn_lstAll";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetAll", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetAll", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTableColumn_GetListByFK(Guid id)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetListByFK", new ValuePair[] {new ValuePair("id", id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTableColumn_lst";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@TbC_Tb_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@TbC_Tb_Id"].Value = id;
				SqlDataReader rdr = cmd.ExecuteReader();
                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetListByFK", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_GetListByFK", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTableColumn_Save(object[] data, int check)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            SqlCommand cmd = null;
            Guid TbC_Id;
            Int32 iSuccess = 0;
            bool isConcurrencyGood = true;
            Guid? guidErrorLogId = null;  // The error log Id returned from sql server if an error occured  (in case we want to call that data for any reason...)
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Save", new ValuePair[] { new ValuePair("data", ObjectHelper.ObjectArrayToString(data)) }, IfxTraceCategory.Enter);
                WcTableColumn_Values _data = new WcTableColumn_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTableColumn_put";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                #region Add Command Parameters
				cmd.Parameters.Add("@TbC_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@TbC_Tb_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_SortOrder", SqlDbType.SmallInt).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_Name", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_CtlTp_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsNonvColumn", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_Description", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_DtSql_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_DtNt_Id", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_Length", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_Precision", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_Scale", SqlDbType.Int).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsPK", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsIdentity", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsFK", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsEntityColumn", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsSystemField", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsValuesObjectMember", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsInPropsScreen", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsInNavList", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsRequired", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_BrokenRuleText", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_AllowZero", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsNullableInDb", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsNullableInUI", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_DefaultValue", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsReadFromDb", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsSendToDb", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsInsertAllowed", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsEditAllowed", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsReadOnlyInUI", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_UseForAudit", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_DefaultCaption", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_ColumnHeaderText", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_LabelCaptionVerbose", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_LabelCaptionGenerate", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tbc_ShowGridColumnToolTip", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Tbc_ShowPropsToolTip", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsCreatePropsStrings", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsCreateGridStrings", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsAvailableForColumnGroups", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsTextWrapInProp", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsTextWrapInGrid", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_TextBoxFormat", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_TextColumnFormat", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_ColumnWidth", SqlDbType.Float).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_TextBoxTextAlignment", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_ColumnTextAlignment", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_ListStoredProc_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsStaticList", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_UseNotInList", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_UseListEditBtn", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_ParentColumnKey", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_UseDisplayTextFieldProperty", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsDisplayTextFieldProperty", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_ComboListTable_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_ComboListDisplayColumn_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_Combo_MaxDropdownHeight", SqlDbType.Float).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_Combo_MaxDropdownWidth", SqlDbType.Float).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_Combo_AllowDropdownResizing", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_Combo_IsResetButtonVisible", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsActiveRecColumn", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsDeletedColumn", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsCreatedUserIdColumn", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsCreatedDateColumn", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsUserIdColumn", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsModifiedDateColumn", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsRowVersionStampColumn", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsBrowsable", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_DeveloperNote", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_UserNote", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_HelpFileAdditionalNote", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_Notes", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsInputComplete", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsCodeGen", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsReadyCodeGen", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsCodeGenComplete", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsTagForCodeGen", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsTagForOther", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_IsActiveRow", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_Stamp", SqlDbType.Timestamp).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@BypassConcurrencyCheck", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@IsConcurrencyGood", SqlDbType.Bit).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                #endregion Add Command Parameters
                #region Add Command Parameter Values
				cmd.Parameters["@TbC_Id"].Value = _data.TbC_Id;

				if (null != _data.TbC_Tb_Id)
                {
                    cmd.Parameters["@TbC_Tb_Id"].Value = _data.TbC_Tb_Id;
                }
                else
                {
                    cmd.Parameters["@TbC_Tb_Id"].Value = DBNull.Value;
                }

				if (null != _data.TbC_SortOrder)
                {
                    cmd.Parameters["@TbC_SortOrder"].Value = _data.TbC_SortOrder;
                }
                else
                {
                    cmd.Parameters["@TbC_SortOrder"].Value = DBNull.Value;
                }

				if (null != _data.TbC_Name)
                {
                    cmd.Parameters["@TbC_Name"].Value = _data.TbC_Name;
                }
                else
                {
                    cmd.Parameters["@TbC_Name"].Value = DBNull.Value;
                }

				if (null != _data.TbC_CtlTp_Id)
                {
                    cmd.Parameters["@TbC_CtlTp_Id"].Value = _data.TbC_CtlTp_Id;
                }
                else
                {
                    cmd.Parameters["@TbC_CtlTp_Id"].Value = DBNull.Value;
                }

				cmd.Parameters["@TbC_IsNonvColumn"].Value = _data.TbC_IsNonvColumn;

				if (null != _data.TbC_Description)
                {
                    cmd.Parameters["@TbC_Description"].Value = _data.TbC_Description;
                }
                else
                {
                    cmd.Parameters["@TbC_Description"].Value = DBNull.Value;
                }

				if (null != _data.TbC_DtSql_Id)
                {
                    cmd.Parameters["@TbC_DtSql_Id"].Value = _data.TbC_DtSql_Id;
                }
                else
                {
                    cmd.Parameters["@TbC_DtSql_Id"].Value = DBNull.Value;
                }

				if (null != _data.TbC_DtNt_Id)
                {
                    cmd.Parameters["@TbC_DtNt_Id"].Value = _data.TbC_DtNt_Id;
                }
                else
                {
                    cmd.Parameters["@TbC_DtNt_Id"].Value = DBNull.Value;
                }

				if (null != _data.TbC_Length)
                {
                    cmd.Parameters["@TbC_Length"].Value = _data.TbC_Length;
                }
                else
                {
                    cmd.Parameters["@TbC_Length"].Value = DBNull.Value;
                }

				if (null != _data.TbC_Precision)
                {
                    cmd.Parameters["@TbC_Precision"].Value = _data.TbC_Precision;
                }
                else
                {
                    cmd.Parameters["@TbC_Precision"].Value = DBNull.Value;
                }

				if (null != _data.TbC_Scale)
                {
                    cmd.Parameters["@TbC_Scale"].Value = _data.TbC_Scale;
                }
                else
                {
                    cmd.Parameters["@TbC_Scale"].Value = DBNull.Value;
                }

				cmd.Parameters["@TbC_IsPK"].Value = _data.TbC_IsPK;

				cmd.Parameters["@TbC_IsIdentity"].Value = _data.TbC_IsIdentity;

				cmd.Parameters["@TbC_IsFK"].Value = _data.TbC_IsFK;

				cmd.Parameters["@TbC_IsEntityColumn"].Value = _data.TbC_IsEntityColumn;

				cmd.Parameters["@TbC_IsSystemField"].Value = _data.TbC_IsSystemField;

				cmd.Parameters["@TbC_IsValuesObjectMember"].Value = _data.TbC_IsValuesObjectMember;

				cmd.Parameters["@TbC_IsInPropsScreen"].Value = _data.TbC_IsInPropsScreen;

				cmd.Parameters["@TbC_IsInNavList"].Value = _data.TbC_IsInNavList;

				cmd.Parameters["@TbC_IsRequired"].Value = _data.TbC_IsRequired;

				if (null != _data.TbC_BrokenRuleText)
                {
                    cmd.Parameters["@TbC_BrokenRuleText"].Value = _data.TbC_BrokenRuleText;
                }
                else
                {
                    cmd.Parameters["@TbC_BrokenRuleText"].Value = DBNull.Value;
                }

				cmd.Parameters["@TbC_AllowZero"].Value = _data.TbC_AllowZero;

				cmd.Parameters["@TbC_IsNullableInDb"].Value = _data.TbC_IsNullableInDb;

				cmd.Parameters["@TbC_IsNullableInUI"].Value = _data.TbC_IsNullableInUI;

				if (null != _data.TbC_DefaultValue)
                {
                    cmd.Parameters["@TbC_DefaultValue"].Value = _data.TbC_DefaultValue;
                }
                else
                {
                    cmd.Parameters["@TbC_DefaultValue"].Value = DBNull.Value;
                }

				cmd.Parameters["@TbC_IsReadFromDb"].Value = _data.TbC_IsReadFromDb;

				cmd.Parameters["@TbC_IsSendToDb"].Value = _data.TbC_IsSendToDb;

				cmd.Parameters["@TbC_IsInsertAllowed"].Value = _data.TbC_IsInsertAllowed;

				cmd.Parameters["@TbC_IsEditAllowed"].Value = _data.TbC_IsEditAllowed;

				cmd.Parameters["@TbC_IsReadOnlyInUI"].Value = _data.TbC_IsReadOnlyInUI;

				cmd.Parameters["@TbC_UseForAudit"].Value = _data.TbC_UseForAudit;

				if (null != _data.TbC_DefaultCaption)
                {
                    cmd.Parameters["@TbC_DefaultCaption"].Value = _data.TbC_DefaultCaption;
                }
                else
                {
                    cmd.Parameters["@TbC_DefaultCaption"].Value = DBNull.Value;
                }

				if (null != _data.TbC_ColumnHeaderText)
                {
                    cmd.Parameters["@TbC_ColumnHeaderText"].Value = _data.TbC_ColumnHeaderText;
                }
                else
                {
                    cmd.Parameters["@TbC_ColumnHeaderText"].Value = DBNull.Value;
                }

				if (null != _data.TbC_LabelCaptionVerbose)
                {
                    cmd.Parameters["@TbC_LabelCaptionVerbose"].Value = _data.TbC_LabelCaptionVerbose;
                }
                else
                {
                    cmd.Parameters["@TbC_LabelCaptionVerbose"].Value = DBNull.Value;
                }

				cmd.Parameters["@TbC_LabelCaptionGenerate"].Value = _data.TbC_LabelCaptionGenerate;

				cmd.Parameters["@Tbc_ShowGridColumnToolTip"].Value = _data.Tbc_ShowGridColumnToolTip;

				cmd.Parameters["@Tbc_ShowPropsToolTip"].Value = _data.Tbc_ShowPropsToolTip;

				cmd.Parameters["@TbC_IsCreatePropsStrings"].Value = _data.TbC_IsCreatePropsStrings;

				cmd.Parameters["@TbC_IsCreateGridStrings"].Value = _data.TbC_IsCreateGridStrings;

				cmd.Parameters["@TbC_IsAvailableForColumnGroups"].Value = _data.TbC_IsAvailableForColumnGroups;

				cmd.Parameters["@TbC_IsTextWrapInProp"].Value = _data.TbC_IsTextWrapInProp;

				cmd.Parameters["@TbC_IsTextWrapInGrid"].Value = _data.TbC_IsTextWrapInGrid;

				if (null != _data.TbC_TextBoxFormat)
                {
                    cmd.Parameters["@TbC_TextBoxFormat"].Value = _data.TbC_TextBoxFormat;
                }
                else
                {
                    cmd.Parameters["@TbC_TextBoxFormat"].Value = DBNull.Value;
                }

				if (null != _data.TbC_TextColumnFormat)
                {
                    cmd.Parameters["@TbC_TextColumnFormat"].Value = _data.TbC_TextColumnFormat;
                }
                else
                {
                    cmd.Parameters["@TbC_TextColumnFormat"].Value = DBNull.Value;
                }

				cmd.Parameters["@TbC_ColumnWidth"].Value = _data.TbC_ColumnWidth;

				if (null != _data.TbC_TextBoxTextAlignment)
                {
                    cmd.Parameters["@TbC_TextBoxTextAlignment"].Value = _data.TbC_TextBoxTextAlignment;
                }
                else
                {
                    cmd.Parameters["@TbC_TextBoxTextAlignment"].Value = DBNull.Value;
                }

				if (null != _data.TbC_ColumnTextAlignment)
                {
                    cmd.Parameters["@TbC_ColumnTextAlignment"].Value = _data.TbC_ColumnTextAlignment;
                }
                else
                {
                    cmd.Parameters["@TbC_ColumnTextAlignment"].Value = DBNull.Value;
                }

				if (null != _data.TbC_ListStoredProc_Id)
                {
                    cmd.Parameters["@TbC_ListStoredProc_Id"].Value = _data.TbC_ListStoredProc_Id;
                }
                else
                {
                    cmd.Parameters["@TbC_ListStoredProc_Id"].Value = DBNull.Value;
                }

				cmd.Parameters["@TbC_IsStaticList"].Value = _data.TbC_IsStaticList;

				cmd.Parameters["@TbC_UseNotInList"].Value = _data.TbC_UseNotInList;

				cmd.Parameters["@TbC_UseListEditBtn"].Value = _data.TbC_UseListEditBtn;

				if (null != _data.TbC_ParentColumnKey)
                {
                    cmd.Parameters["@TbC_ParentColumnKey"].Value = _data.TbC_ParentColumnKey;
                }
                else
                {
                    cmd.Parameters["@TbC_ParentColumnKey"].Value = DBNull.Value;
                }

				cmd.Parameters["@TbC_UseDisplayTextFieldProperty"].Value = _data.TbC_UseDisplayTextFieldProperty;

				cmd.Parameters["@TbC_IsDisplayTextFieldProperty"].Value = _data.TbC_IsDisplayTextFieldProperty;

				if (null != _data.TbC_ComboListTable_Id)
                {
                    cmd.Parameters["@TbC_ComboListTable_Id"].Value = _data.TbC_ComboListTable_Id;
                }
                else
                {
                    cmd.Parameters["@TbC_ComboListTable_Id"].Value = DBNull.Value;
                }

				if (null != _data.TbC_ComboListDisplayColumn_Id)
                {
                    cmd.Parameters["@TbC_ComboListDisplayColumn_Id"].Value = _data.TbC_ComboListDisplayColumn_Id;
                }
                else
                {
                    cmd.Parameters["@TbC_ComboListDisplayColumn_Id"].Value = DBNull.Value;
                }

				cmd.Parameters["@TbC_Combo_MaxDropdownHeight"].Value = _data.TbC_Combo_MaxDropdownHeight;

				cmd.Parameters["@TbC_Combo_MaxDropdownWidth"].Value = _data.TbC_Combo_MaxDropdownWidth;

				cmd.Parameters["@TbC_Combo_AllowDropdownResizing"].Value = _data.TbC_Combo_AllowDropdownResizing;

				cmd.Parameters["@TbC_Combo_IsResetButtonVisible"].Value = _data.TbC_Combo_IsResetButtonVisible;

				cmd.Parameters["@TbC_IsActiveRecColumn"].Value = _data.TbC_IsActiveRecColumn;

				cmd.Parameters["@TbC_IsDeletedColumn"].Value = _data.TbC_IsDeletedColumn;

				cmd.Parameters["@TbC_IsCreatedUserIdColumn"].Value = _data.TbC_IsCreatedUserIdColumn;

				cmd.Parameters["@TbC_IsCreatedDateColumn"].Value = _data.TbC_IsCreatedDateColumn;

				cmd.Parameters["@TbC_IsUserIdColumn"].Value = _data.TbC_IsUserIdColumn;

				cmd.Parameters["@TbC_IsModifiedDateColumn"].Value = _data.TbC_IsModifiedDateColumn;

				cmd.Parameters["@TbC_IsRowVersionStampColumn"].Value = _data.TbC_IsRowVersionStampColumn;

				cmd.Parameters["@TbC_IsBrowsable"].Value = _data.TbC_IsBrowsable;

				if (null != _data.TbC_DeveloperNote)
                {
                    cmd.Parameters["@TbC_DeveloperNote"].Value = _data.TbC_DeveloperNote;
                }
                else
                {
                    cmd.Parameters["@TbC_DeveloperNote"].Value = DBNull.Value;
                }

				if (null != _data.TbC_UserNote)
                {
                    cmd.Parameters["@TbC_UserNote"].Value = _data.TbC_UserNote;
                }
                else
                {
                    cmd.Parameters["@TbC_UserNote"].Value = DBNull.Value;
                }

				if (null != _data.TbC_HelpFileAdditionalNote)
                {
                    cmd.Parameters["@TbC_HelpFileAdditionalNote"].Value = _data.TbC_HelpFileAdditionalNote;
                }
                else
                {
                    cmd.Parameters["@TbC_HelpFileAdditionalNote"].Value = DBNull.Value;
                }

				if (null != _data.TbC_Notes)
                {
                    cmd.Parameters["@TbC_Notes"].Value = _data.TbC_Notes;
                }
                else
                {
                    cmd.Parameters["@TbC_Notes"].Value = DBNull.Value;
                }

				cmd.Parameters["@TbC_IsInputComplete"].Value = _data.TbC_IsInputComplete;

				cmd.Parameters["@TbC_IsCodeGen"].Value = _data.TbC_IsCodeGen;

				cmd.Parameters["@TbC_IsReadyCodeGen"].Value = _data.TbC_IsReadyCodeGen;

				cmd.Parameters["@TbC_IsCodeGenComplete"].Value = _data.TbC_IsCodeGenComplete;

				cmd.Parameters["@TbC_IsTagForCodeGen"].Value = _data.TbC_IsTagForCodeGen;

				cmd.Parameters["@TbC_IsTagForOther"].Value = _data.TbC_IsTagForOther;

				cmd.Parameters["@TbC_IsActiveRow"].Value = _data.TbC_IsActiveRow;

				if (null != _data.TbC_UserId)
                {
                    cmd.Parameters["@TbC_UserId"].Value = _data.TbC_UserId;
                }
                else
                {
                    cmd.Parameters["@TbC_UserId"].Value = DBNull.Value;
                }

				cmd.Parameters["@TbC_Stamp"].Value = _data.TbC_Stamp;


                cmd.Parameters["@BypassConcurrencyCheck"].Value = ConcurrencyService.BypassConcurrencyCheck((UseConcurrencyCheck)check);

                #endregion Add Command Parameter Values

                if (IfxTrace._scriptInsertUpdateOperations)
                {
                    IfxEvent.WriteSQLInsertUpdateScriptsToXml(_as, _cn, "WcTableColumn_Save", SQLScript.CreateScript(cmd));
                }

                //// Log all data changes
                //object[] original = null;
                //if (v_Audit_DataServices.UseAudit == true)
                //{
                //    original = WcTableColumn_GetById(_data.TbC_Id);
                //}

                cmd.ExecuteNonQuery();
                TbC_Id = ((Guid)(cmd.Parameters["@TbC_Id"].Value));
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                isConcurrencyGood = ((bool)(cmd.Parameters["@IsConcurrencyGood"].Value));
                if (cmd.Parameters["@ErrorLogId"].Value != System.DBNull.Value)
                {
                    guidErrorLogId = ((Guid?)(cmd.Parameters["@ErrorLogId"].Value));  // What do we want to do with this?
                }

                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                //**//  Hack:  we need to get this true or false value from the business object by using a 
                //**//    DataServiceInsertUpdateRequest object or by fetching it from a cache on the server.
                dataServiceResponse.ReturnCurrentRowOnInsertUpdate = GetReturnCurrentRowOnInsertUpdate();
                //  Set the DataServiceInsertUpdateResponse Result
                if (iSuccess > 0)
                {
                    dataServiceResponse.Result = DataOperationResult.Success;
                    dataServiceResponse.CurrentTimestamp = ((byte[])(cmd.Parameters["@TbC_Stamp"].Value));

                    //// Log all data changes
                    //if (v_Audit_DataServices.UseAudit == true)
                    //{
                    //    WcTableColumn_Values originalValues = null;
                    //    if (original != null)
                    //    {
                    //        originalValues = new WcTableColumn_Values((object[])original[0], null);
                    //    }
                    //    WcTableColumn_LogAudit(_data, originalValues);
                    //}

                }
                else if (isConcurrencyGood == false)
                {
                    dataServiceResponse.Result = DataOperationResult.ConcurrencyFailure;
                }
                else
                {
                    dataServiceResponse.Result = DataOperationResult.HandledException;
                }
                if (iSuccess == 0 || dataServiceResponse.ReturnCurrentRowOnInsertUpdate == true || dataServiceResponse.Result == DataOperationResult.ConcurrencyFailure)
                {
                    //  we need a flag to determin if we return new data on a successful insert/update
                    dataServiceResponse.CurrentValues = WcTableColumn_GetById_ObjectArray(TbC_Id);
                }
                // If the default is to not return the current row, then return the Primary Key here.
                if (dataServiceResponse.ReturnCurrentRowOnInsertUpdate == false)
                {
                    dataServiceResponse.guidPrimaryKey = TbC_Id;
                }

                return dataServiceResponse.ReturnAsObjectArray();
            }
            catch (Exception ex)
            {
                string sqlScript = SQLScript.CreateScript(cmd);
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Save", new ValuePair[] { new ValuePair("check.ToString()", check.ToString()), new ValuePair("data", ObjectHelper.ObjectArrayToString(data)), new ValuePair("sqlScript", sqlScript) }, null, ex);
                IfxWrapperException.GetError(ex, (Guid)traceId);
                DataServiceInsertUpdateResponse dataServiceResponse = new DataServiceInsertUpdateResponse();
                dataServiceResponse.Result = DataOperationResult.UnhandledException;
                return dataServiceResponse.ReturnAsObjectArray();
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Save", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTableColumn_Delete(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Delete", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);
                //SqlConnectionHelper helper = new SqlConnectionHelper();
                WcTableColumn_Values _data = new WcTableColumn_Values(data, null);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTableColumn_del ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = _data.TbC_Id;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[1];
                ret[0] = iSuccess;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Delete", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Delete", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTableColumn_SetIsDeleted(Guid Id, bool IsDeleted)
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            Int32 iSuccess = 0;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SetIsDeleted", new ValuePair[] {new ValuePair("IsDeleted", IsDeleted), new ValuePair("Id", Id) }, IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTableColumn_putIsDeleted ";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@IsDeleted", SqlDbType.Bit).Direction = ParameterDirection.Input;
                cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                cmd.Parameters["@Id"].Value = Id;
                cmd.Parameters["@IsDeleted"].Value = IsDeleted;
                cmd.ExecuteNonQuery();
                iSuccess = ((Int32)(cmd.Parameters["@Success"].Value));
                object[] ret = new object[2];
                ret[0] = iSuccess;
                ret[1] = Id;
                return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SetIsDeleted", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_SetIsDeleted", new ValuePair[] {new ValuePair("iSuccess", iSuccess) }, IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTableColumn_Deactivate(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Deactivate", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Deactivate", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Deactivate", IfxTraceCategory.Leave);
            }
        }

        public static object[] WcTableColumn_Remove(object[] data)
        {
            Guid? traceId = Guid.NewGuid();
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Remove", new ValuePair[] {new ValuePair("data", data) }, IfxTraceCategory.Enter);

                // no code yet
                return null;
            }
            catch(Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Remove", ex);
                return null;
            }
            finally
            {
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "WcTableColumn_Remove", IfxTraceCategory.Leave);
            }
        }


        #endregion Base Methods



		#region Other Data Access Methods

        public static object[]  ExecutewcTableColumnGroup_AssignColumns(Boolean Insert, Guid TbCGrp2TbC_TbCGrp_Id, Guid TbCGrp2TbC_TbC_Id, Guid TbCGrp2TbC_CreatedUserId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableColumnGroup_AssignColumns", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spwcTableColumnGroup_AssignColumns";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Insert", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbCGrp2TbC_TbCGrp_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbCGrp2TbC_TbC_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbCGrp2TbC_CreatedUserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
				cmd.Parameters["@Insert"].Value = Insert;
				cmd.Parameters["@TbCGrp2TbC_TbCGrp_Id"].Value = TbCGrp2TbC_TbCGrp_Id;
				cmd.Parameters["@TbCGrp2TbC_TbC_Id"].Value = TbCGrp2TbC_TbC_Id;
				cmd.Parameters["@TbCGrp2TbC_CreatedUserId"].Value = TbCGrp2TbC_CreatedUserId;
				cmd.ExecuteNonQuery();
				Int32 Success = ((Int32)(cmd.Parameters["@Success"].Value));
				//Guid ErrorLogId = ((Guid)(cmd.Parameters["@ErrorLogId"].Value));
				object[] ret = new object[1];
				ret[0] = Success;

                return ret;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableColumnGroup_AssignColumns", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecutewcTableColumnGroup_AssignColumns", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetwcTableColumnGroup_Assignments(Guid Tb_Id, Guid TbC_Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableColumnGroup_Assignments", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spwcTableColumnGroup_Assignments";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Tb_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Tb_Id"].Value = Tb_Id;
				cmd.Parameters["@TbC_Id"].Value = TbC_Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableColumnGroup_Assignments", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetwcTableColumnGroup_Assignments", IfxTraceCategory.Leave);
            }
        }

        public static object[]  GetWcTableColumn_lstNamesFromAppAndDB(Guid Tb_Id )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstNamesFromAppAndDB", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTableColumn_lstNamesFromAppAndDB";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Tb_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters["@Tb_Id"].Value = Tb_Id;
				SqlDataReader rdr = cmd.ExecuteReader();

                return ConvertToObjectArray.ConvertSqlDataReaderToObjectArray(rdr);

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstNamesFromAppAndDB", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "GetWcTableColumn_lstNamesFromAppAndDB", IfxTraceCategory.Leave);
            }
        }

        public static object[]  ExecuteWcTableColumn_InsertListOfColumnsFromDb(Guid Tb_Id, Guid? TbC_Id, String ColumnName, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_InsertListOfColumnsFromDb", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTableColumn_InsertListOfColumnsFromDb";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Tb_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@TbC_Id", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;
				cmd.Parameters.Add("@ColumnName", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters["@Tb_Id"].Value = Tb_Id;
				cmd.Parameters["@TbC_Id"].Value = TbC_Id;
				cmd.Parameters["@ColumnName"].Value = ColumnName;
				cmd.Parameters["@UserId"].Value = UserId;
				cmd.ExecuteNonQuery();
				TbC_Id = ((Guid)(cmd.Parameters["@TbC_Id"].Value));
				Int32 Success = ((Int32)(cmd.Parameters["@Success"].Value));
				object[] ret = new object[2];
				ret[0] = TbC_Id;
				ret[1] = Success;

                return ret;

            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_InsertListOfColumnsFromDb", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_InsertListOfColumnsFromDb", IfxTraceCategory.Leave);
            }
        }

        public static object[]  ExecuteWcTableColumn_Check_UnCheckColumns(String Crit, String Column, Boolean IsChecked, Guid UserId )
        {
            Guid? traceId = Guid.NewGuid();
            SqlConnection conn = null;
            try
            {
                if (IfxTrace._traceEnter) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_Check_UnCheckColumns", IfxTraceCategory.Enter);
                conn = DBConnectoinHelper.GetDBConnection();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "spWcTableColumn_Check_UnCheckColumns";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("@Crit", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Column", SqlDbType.VarChar).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@IsChecked", SqlDbType.Bit).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Input;
				cmd.Parameters.Add("@Success", SqlDbType.Int).Direction = ParameterDirection.Output;
				cmd.Parameters.Add("@ErrorLogId", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
				cmd.Parameters["@Crit"].Value = Crit;
				cmd.Parameters["@Column"].Value = Column;
				cmd.Parameters["@IsChecked"].Value = IsChecked;
				cmd.Parameters["@UserId"].Value = UserId;
				cmd.ExecuteNonQuery();
				Int32 Success = ((Int32)(cmd.Parameters["@Success"].Value));
				//Guid ErrorLogId = ((Guid)(cmd.Parameters["@ErrorLogId"].Value));
				object[] ret = new object[1];
				ret[0] = Success;
				return ret;
            }
            catch (Exception ex)
            {
                if (IfxTrace._traceCatch) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_Check_UnCheckColumns", ex);
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (IfxTrace._traceLeave) IfxEvent.PublishTrace(traceId, _as, _cn, "ExecuteWcTableColumn_Check_UnCheckColumns", IfxTraceCategory.Leave);
            }
        }

		#endregion Other Data Access Methods

    }
}


