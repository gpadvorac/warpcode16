

/*

SELECT name
FROM OPENROWSET
(
  'SQLNCLI', 
  'Server=Voyager3;Trusted_Connection=yes;',
 'select WarpCodeV16'
); 

*/


-- wcDatabaseConnectionStringKey


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcDatabaseConnectionStringKey

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcDatabaseConnectionStringKey_row
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_row]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_row__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_row__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcDatabaseConnectionStringKey_row__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_row', 'spWcDatabaseConnectionStringKey_row__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_row', 'spWcDatabaseConnectionStringKey_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcDatabaseConnectionStringKey_row', 'spWcDatabaseConnectionStringKey_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcDatabaseConnectionStringKey_row', 'spWcDatabaseConnectionStringKey_row__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcDatabaseConnectionStringKey_row', 'spWcDatabaseConnectionStringKey_row__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcDatabaseConnectionStringKey_row]
-- Script for this SP was created on: 1/3/2018 10:19:11 AM
(
@Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
DbCnSK_Id,
DbCnSK_ApVrsn_Id,
DbCnSK_Name,
DbCnSK_IsDefault,
DbCnSK_Server,
DbCnSK_Database,
DbCnSK_UserName,
DbCnSK_Password,
DbCnSK_IsActiveRow,
DbCnSK_IsDeleted,
DbCnSK_CreatedUserId,
DbCnSK_CreatedDate,
DbCnSK_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

DbCnSK_LastModifiedDate,
DbCnSK_Stamp


FROM 		wcDatabaseConnectionStringKey LEFT OUTER JOIN
                tbPerson ON wcDatabaseConnectionStringKey.DbCnSK_UserId = tbPerson.Pn_SecurityUserId


WHERE   (DbCnSK_Id = @Id)

ORDER BY    DbCnSK_Name


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcDatabaseConnectionStringKey_lst
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_lst]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_lst__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_lst__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcDatabaseConnectionStringKey_lst__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_lst', 'spWcDatabaseConnectionStringKey_lst__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_lst', 'spWcDatabaseConnectionStringKey_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcDatabaseConnectionStringKey_lst', 'spWcDatabaseConnectionStringKey_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcDatabaseConnectionStringKey_lst', 'spWcDatabaseConnectionStringKey_lst__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcDatabaseConnectionStringKey_lst', 'spWcDatabaseConnectionStringKey_lst__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcDatabaseConnectionStringKey_lst]
-- Script for this SP was created on: 1/3/2018 10:19:11 AM
(
@DbCnSK_ApVrsn_Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
DbCnSK_Id,
DbCnSK_ApVrsn_Id,
DbCnSK_Name,
DbCnSK_IsDefault,
DbCnSK_Server,
DbCnSK_Database,
DbCnSK_UserName,
DbCnSK_Password,
DbCnSK_IsActiveRow,
DbCnSK_IsDeleted,
DbCnSK_CreatedUserId,
DbCnSK_CreatedDate,
DbCnSK_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

DbCnSK_LastModifiedDate,
DbCnSK_Stamp


FROM 		wcDatabaseConnectionStringKey LEFT OUTER JOIN
                tbPerson ON wcDatabaseConnectionStringKey.DbCnSK_UserId = tbPerson.Pn_SecurityUserId


WHERE   (DbCnSK_ApVrsn_Id = @DbCnSK_ApVrsn_Id) AND (DbCnSK_IsDeleted = 0)

ORDER BY    DbCnSK_Name


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcDatabaseConnectionStringKey_lstAll
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_lstAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_lstAll__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcDatabaseConnectionStringKey_lstAll__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_lstAll', 'spWcDatabaseConnectionStringKey_lstAll__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_lstAll', 'spWcDatabaseConnectionStringKey_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcDatabaseConnectionStringKey_lstAll', 'spWcDatabaseConnectionStringKey_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcDatabaseConnectionStringKey_lstAll', 'spWcDatabaseConnectionStringKey_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcDatabaseConnectionStringKey_lstAll', 'spWcDatabaseConnectionStringKey_lstAll__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcDatabaseConnectionStringKey_lstAll]
-- Script for this SP was created on: 1/3/2018 10:19:11 AM
AS
SET NOCOUNT ON

SELECT 	
DbCnSK_Id,
DbCnSK_ApVrsn_Id,
DbCnSK_Name,
DbCnSK_IsDefault,
DbCnSK_Server,
DbCnSK_Database,
DbCnSK_UserName,
DbCnSK_Password,
DbCnSK_IsActiveRow,
DbCnSK_IsDeleted,
DbCnSK_CreatedUserId,
DbCnSK_CreatedDate,
DbCnSK_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

DbCnSK_LastModifiedDate,
DbCnSK_Stamp


FROM 		wcDatabaseConnectionStringKey LEFT OUTER JOIN
                tbPerson ON wcDatabaseConnectionStringKey.DbCnSK_UserId = tbPerson.Pn_SecurityUserId

WHERE   (DbCnSK_IsDeleted = 0)

ORDER BY    DbCnSK_Name


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcDatabaseConnectionStringKey_putUpdate
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putUpdate__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcDatabaseConnectionStringKey_putUpdate__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putUpdate', 'spWcDatabaseConnectionStringKey_putUpdate__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putUpdate', 'spWcDatabaseConnectionStringKey_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putUpdate', 'spWcDatabaseConnectionStringKey_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putUpdate', 'spWcDatabaseConnectionStringKey_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putUpdate', 'spWcDatabaseConnectionStringKey_putUpdate__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcDatabaseConnectionStringKey_putUpdate]
-- Script for this SP was created on: 1/3/2018 10:19:11 AM
(
@DbCnSK_Id uniqueidentifier = NULL OUTPUT,
@DbCnSK_ApVrsn_Id uniqueidentifier,
@DbCnSK_Name varchar(100),
@DbCnSK_IsDefault bit,
@DbCnSK_Server varchar(50),
@DbCnSK_Database varchar(50),
@DbCnSK_UserName varchar(50),
@DbCnSK_Password varchar(50),
@DbCnSK_IsActiveRow bit,
@DbCnSK_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
	IF @TranCount > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
		SET @ErrMsg = 'Attempting to update wcDatabaseConnectionStringKey'
		UPDATE	wcDatabaseConnectionStringKey
		SET
		DbCnSK_Name = RTRIM(LTRIM(@DbCnSK_Name)),
		DbCnSK_IsDefault = @DbCnSK_IsDefault,
		DbCnSK_Server = RTRIM(LTRIM(@DbCnSK_Server)),
		DbCnSK_Database = RTRIM(LTRIM(@DbCnSK_Database)),
		DbCnSK_UserName = RTRIM(LTRIM(@DbCnSK_UserName)),
		DbCnSK_Password = RTRIM(LTRIM(@DbCnSK_Password)),
		DbCnSK_IsActiveRow = @DbCnSK_IsActiveRow,
		DbCnSK_UserId = @DbCnSK_UserId,
		DbCnSK_LastModifiedDate = GETDATE()
		WHERE	(DbCnSK_Id=@DbCnSK_Id)
	END

	SET @TranCount = @@TRANCOUNT;
	IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Id', 'uniqueidentifier', @DbCnSK_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_ApVrsn_Id', 'uniqueidentifier', @DbCnSK_ApVrsn_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Name', 'varchar', @DbCnSK_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_IsDefault', 'bit', @DbCnSK_IsDefault, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Server', 'varchar', @DbCnSK_Server, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Database', 'varchar', @DbCnSK_Database, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_UserName', 'varchar', @DbCnSK_UserName, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Password', 'varchar', @DbCnSK_Password, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_IsActiveRow', 'bit', @DbCnSK_IsActiveRow, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_UserId', 'uniqueidentifier', @DbCnSK_UserId, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 12;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcDatabaseConnectionStringKey_putInsert
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putInsert__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcDatabaseConnectionStringKey_putInsert__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putInsert', 'spWcDatabaseConnectionStringKey_putInsert__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putInsert', 'spWcDatabaseConnectionStringKey_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putInsert', 'spWcDatabaseConnectionStringKey_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putInsert', 'spWcDatabaseConnectionStringKey_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putInsert', 'spWcDatabaseConnectionStringKey_putInsert__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcDatabaseConnectionStringKey_putInsert]
-- Script for this SP was created on: 1/3/2018 10:19:11 AM
(
@DbCnSK_Id uniqueidentifier = NULL OUTPUT,
@DbCnSK_ApVrsn_Id uniqueidentifier,
@DbCnSK_Name varchar(100),
@DbCnSK_IsDefault bit,
@DbCnSK_Server varchar(50),
@DbCnSK_Database varchar(50),
@DbCnSK_UserName varchar(50),
@DbCnSK_Password varchar(50),
@DbCnSK_IsActiveRow bit,
@DbCnSK_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
      SET @ErrMsg = 'Attempting to insert into wcDatabaseConnectionStringKey'
      INSERT INTO wcDatabaseConnectionStringKey
          (
		DbCnSK_Id,
		DbCnSK_ApVrsn_Id,
		DbCnSK_Name,
		DbCnSK_IsDefault,
		DbCnSK_Server,
		DbCnSK_Database,
		DbCnSK_UserName,
		DbCnSK_Password,
		DbCnSK_IsActiveRow,
		DbCnSK_CreatedUserId,
		DbCnSK_CreatedDate,
		DbCnSK_UserId,
		DbCnSK_LastModifiedDate
				)
VALUES	(
		@DbCnSK_Id, 
		@DbCnSK_ApVrsn_Id, 
		RTRIM(LTRIM(@DbCnSK_Name)), 
		@DbCnSK_IsDefault, 
		RTRIM(LTRIM(@DbCnSK_Server)), 
		RTRIM(LTRIM(@DbCnSK_Database)), 
		RTRIM(LTRIM(@DbCnSK_UserName)), 
		RTRIM(LTRIM(@DbCnSK_Password)), 
		@DbCnSK_IsActiveRow, 
		@DbCnSK_UserId, 
		GETDATE(), 
		@DbCnSK_UserId, 
		GETDATE()
				)
                
    END

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data

		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Id', 'uniqueidentifier', @DbCnSK_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_ApVrsn_Id', 'uniqueidentifier', @DbCnSK_ApVrsn_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Name', 'varchar', @DbCnSK_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_IsDefault', 'bit', @DbCnSK_IsDefault, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Server', 'varchar', @DbCnSK_Server, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Database', 'varchar', @DbCnSK_Database, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_UserName', 'varchar', @DbCnSK_UserName, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Password', 'varchar', @DbCnSK_Password, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_IsActiveRow', 'bit', @DbCnSK_IsActiveRow, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_UserId', 'uniqueidentifier', @DbCnSK_UserId, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 12;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcDatabaseConnectionStringKey_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcDatabaseConnectionStringKey_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_put', 'spWcDatabaseConnectionStringKey_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_put', 'spWcDatabaseConnectionStringKey_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcDatabaseConnectionStringKey_put', 'spWcDatabaseConnectionStringKey_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcDatabaseConnectionStringKey_put', 'spWcDatabaseConnectionStringKey_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcDatabaseConnectionStringKey_put', 'spWcDatabaseConnectionStringKey_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcDatabaseConnectionStringKey_put]
-- Script for this SP was created on: 1/3/2018 10:19:11 AM
(
@DbCnSK_Id uniqueidentifier = NULL OUTPUT,
@DbCnSK_ApVrsn_Id uniqueidentifier,
@DbCnSK_Name varchar(100),
@DbCnSK_IsDefault bit,
@DbCnSK_Server varchar(50),
@DbCnSK_Database varchar(50),
@DbCnSK_UserName varchar(50),
@DbCnSK_Password varchar(50),
@DbCnSK_IsActiveRow bit,
@DbCnSK_UserId uniqueidentifier,
@DbCnSK_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@DbCnSK_Id Is NULL)
	BEGIN
		SET @DbCnSK_Id = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcDatabaseConnectionStringKey WHERE DbCnSK_Id = @DbCnSK_Id)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcDatabaseConnectionStringKey_putInsert'
		EXEC spWcDatabaseConnectionStringKey_putInsert
		@DbCnSK_Id,
		@DbCnSK_ApVrsn_Id,
		@DbCnSK_Name,
		@DbCnSK_IsDefault,
		@DbCnSK_Server,
		@DbCnSK_Database,
		@DbCnSK_UserName,
		@DbCnSK_Password,
		@DbCnSK_IsActiveRow,
		@DbCnSK_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @DbCnSK_Stamp =( SELECT DbCnSK_Stamp FROM wcDatabaseConnectionStringKey WHERE (DbCnSK_Id = @DbCnSK_Id))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcDatabaseConnectionStringKey WHERE (DbCnSK_Id = @DbCnSK_Id) AND (DbCnSK_Stamp = @DbCnSK_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcDatabaseConnectionStringKey_putUpdate'
				EXEC spWcDatabaseConnectionStringKey_putUpdate
				@DbCnSK_Id,
				@DbCnSK_ApVrsn_Id,
				@DbCnSK_Name,
				@DbCnSK_IsDefault,
				@DbCnSK_Server,
				@DbCnSK_Database,
				@DbCnSK_UserName,
				@DbCnSK_Password,
				@DbCnSK_IsActiveRow,
				@DbCnSK_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @DbCnSK_Stamp =( SELECT DbCnSK_Stamp FROM wcDatabaseConnectionStringKey WHERE (DbCnSK_Id = @DbCnSK_Id))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Id', 'uniqueidentifier', @DbCnSK_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_ApVrsn_Id', 'uniqueidentifier', @DbCnSK_ApVrsn_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Name', 'varchar', @DbCnSK_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_IsDefault', 'bit', @DbCnSK_IsDefault, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Server', 'varchar', @DbCnSK_Server, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Database', 'varchar', @DbCnSK_Database, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_UserName', 'varchar', @DbCnSK_UserName, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_Password', 'varchar', @DbCnSK_Password, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_IsActiveRow', 'bit', @DbCnSK_IsActiveRow, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@DbCnSK_UserId', 'uniqueidentifier', @DbCnSK_UserId, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 12;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcDatabaseConnectionStringKey_putIsDeleted
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putIsDeleted]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putIsDeleted__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcDatabaseConnectionStringKey_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcDatabaseConnectionStringKey_putIsDeleted__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putIsDeleted', 'spWcDatabaseConnectionStringKey_putIsDeleted__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putIsDeleted', 'spWcDatabaseConnectionStringKey_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putIsDeleted', 'spWcDatabaseConnectionStringKey_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putIsDeleted', 'spWcDatabaseConnectionStringKey_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcDatabaseConnectionStringKey_putIsDeleted', 'spWcDatabaseConnectionStringKey_putIsDeleted__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcDatabaseConnectionStringKey_putIsDeleted]
-- Script for this SP was created on: 1/3/2018 10:19:11 AM
(
@Id UniqueIdentifier,
@IsDeleted bit,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

BEGIN TRANSACTION;
BEGIN TRY
    BEGIN

	   UPDATE       wcDatabaseConnectionStringKey
	   SET          DbCnSK_IsDeleted = @IsDeleted
	   WHERE        (DbCnSK_Id = @Id)

	END
    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END


END TRY


BEGIN CATCH
    SET @Success = 0
    IF @TranCount = 0
    ROLLBACK TRANSACTION;

    SET @ErrorLogId = NewID()
    INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
    VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
    --  Log Stored Procedure Parameter Values
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Id', 'uniqueidentifier', @Id, 1;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@IsDeleted', 'bit', @IsDeleted, 2;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 3;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 4;

END CATCH
	
RETURN





--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcDatabaseConnectionStringKey')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcDatabaseConnectionStringKey', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


