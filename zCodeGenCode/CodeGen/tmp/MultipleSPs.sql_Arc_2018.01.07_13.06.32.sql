

/*

SELECT name
FROM OPENROWSET
(
  'SQLNCLI', 
  'Server=Voyager3;Trusted_Connection=yes;',
 'select WarpCodeV16'
); 

*/


-- wcAppDbSchema


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcAppDbSchema

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcAppDbSchema_row
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_row]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_row__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_row__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcAppDbSchema_row__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcAppDbSchema_row', 'spWcAppDbSchema_row__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcAppDbSchema_row', 'spWcAppDbSchema_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcAppDbSchema_row', 'spWcAppDbSchema_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcAppDbSchema_row', 'spWcAppDbSchema_row__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcAppDbSchema_row', 'spWcAppDbSchema_row__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcAppDbSchema_row]
-- Script for this SP was created on: 1/6/2018 10:57:04 PM
(
@Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
ApDbScm_Id,
ApDbScm_Ap_Id,
ApDbScm_Name,
ApDbScm_Desc,
ApDbScm_IsActiveRow,
ApDbScm_IsDeleted,
ApDbScm_CreatedUserId,
ApDbScm_CreatedDate,
ApDbScm_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

ApDbScm_LastModifiedDate,
ApDbScm_Stamp


FROM 		wcAppDbSchema LEFT OUTER JOIN
                tbPerson ON wcAppDbSchema.ApDbScm_UserId = tbPerson.Pn_SecurityUserId


WHERE   (ApDbScm_Id = @Id)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcAppDbSchema_lst
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_lst]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_lst__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_lst__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcAppDbSchema_lst__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcAppDbSchema_lst', 'spWcAppDbSchema_lst__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcAppDbSchema_lst', 'spWcAppDbSchema_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcAppDbSchema_lst', 'spWcAppDbSchema_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcAppDbSchema_lst', 'spWcAppDbSchema_lst__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcAppDbSchema_lst', 'spWcAppDbSchema_lst__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcAppDbSchema_lst]
-- Script for this SP was created on: 1/6/2018 10:57:04 PM
(
@ApDbScm_Ap_Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
ApDbScm_Id,
ApDbScm_Ap_Id,
ApDbScm_Name,
ApDbScm_Desc,
ApDbScm_IsActiveRow,
ApDbScm_IsDeleted,
ApDbScm_CreatedUserId,
ApDbScm_CreatedDate,
ApDbScm_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

ApDbScm_LastModifiedDate,
ApDbScm_Stamp


FROM 		wcAppDbSchema LEFT OUTER JOIN
                tbPerson ON wcAppDbSchema.ApDbScm_UserId = tbPerson.Pn_SecurityUserId


WHERE   (ApDbScm_Ap_Id = @ApDbScm_Ap_Id) AND (ApDbScm_IsDeleted = 0)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcAppDbSchema_lstAll
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_lstAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_lstAll__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcAppDbSchema_lstAll__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcAppDbSchema_lstAll', 'spWcAppDbSchema_lstAll__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcAppDbSchema_lstAll', 'spWcAppDbSchema_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcAppDbSchema_lstAll', 'spWcAppDbSchema_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcAppDbSchema_lstAll', 'spWcAppDbSchema_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcAppDbSchema_lstAll', 'spWcAppDbSchema_lstAll__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcAppDbSchema_lstAll]
-- Script for this SP was created on: 1/6/2018 10:57:04 PM
AS
SET NOCOUNT ON

SELECT 	
ApDbScm_Id,
ApDbScm_Ap_Id,
ApDbScm_Name,
ApDbScm_Desc,
ApDbScm_IsActiveRow,
ApDbScm_IsDeleted,
ApDbScm_CreatedUserId,
ApDbScm_CreatedDate,
ApDbScm_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

ApDbScm_LastModifiedDate,
ApDbScm_Stamp


FROM 		wcAppDbSchema LEFT OUTER JOIN
                tbPerson ON wcAppDbSchema.ApDbScm_UserId = tbPerson.Pn_SecurityUserId

WHERE   (ApDbScm_IsDeleted = 0)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcAppDbSchema_putUpdate
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putUpdate__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcAppDbSchema_putUpdate__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcAppDbSchema_putUpdate', 'spWcAppDbSchema_putUpdate__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcAppDbSchema_putUpdate', 'spWcAppDbSchema_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcAppDbSchema_putUpdate', 'spWcAppDbSchema_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcAppDbSchema_putUpdate', 'spWcAppDbSchema_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcAppDbSchema_putUpdate', 'spWcAppDbSchema_putUpdate__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcAppDbSchema_putUpdate]
-- Script for this SP was created on: 1/6/2018 10:57:04 PM
(
@ApDbScm_Id uniqueidentifier = NULL OUTPUT,
@ApDbScm_Ap_Id uniqueidentifier,
@ApDbScm_Name varchar(50),
@ApDbScm_Desc varchar(200),
@ApDbScm_IsActiveRow bit,
@ApDbScm_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
	IF @TranCount > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
		SET @ErrMsg = 'Attempting to update wcAppDbSchema'
		UPDATE	wcAppDbSchema
		SET
		ApDbScm_Name = RTRIM(LTRIM(@ApDbScm_Name)),
		ApDbScm_Desc = RTRIM(LTRIM(@ApDbScm_Desc)),
		ApDbScm_IsActiveRow = @ApDbScm_IsActiveRow,
		ApDbScm_UserId = @ApDbScm_UserId,
		ApDbScm_LastModifiedDate = GETDATE()
		WHERE	(ApDbScm_Id=@ApDbScm_Id)
	END

	SET @TranCount = @@TRANCOUNT;
	IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_Id', 'uniqueidentifier', @ApDbScm_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_Ap_Id', 'uniqueidentifier', @ApDbScm_Ap_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_Name', 'varchar', @ApDbScm_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_Desc', 'varchar', @ApDbScm_Desc, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_IsActiveRow', 'bit', @ApDbScm_IsActiveRow, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_UserId', 'uniqueidentifier', @ApDbScm_UserId, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 8;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcAppDbSchema_putInsert
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putInsert__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcAppDbSchema_putInsert__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcAppDbSchema_putInsert', 'spWcAppDbSchema_putInsert__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcAppDbSchema_putInsert', 'spWcAppDbSchema_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcAppDbSchema_putInsert', 'spWcAppDbSchema_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcAppDbSchema_putInsert', 'spWcAppDbSchema_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcAppDbSchema_putInsert', 'spWcAppDbSchema_putInsert__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcAppDbSchema_putInsert]
-- Script for this SP was created on: 1/6/2018 10:57:04 PM
(
@ApDbScm_Id uniqueidentifier = NULL OUTPUT,
@ApDbScm_Ap_Id uniqueidentifier,
@ApDbScm_Name varchar(50),
@ApDbScm_Desc varchar(200),
@ApDbScm_IsActiveRow bit,
@ApDbScm_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
      SET @ErrMsg = 'Attempting to insert into wcAppDbSchema'
      INSERT INTO wcAppDbSchema
          (
		ApDbScm_Id,
		ApDbScm_Ap_Id,
		ApDbScm_Name,
		ApDbScm_Desc,
		ApDbScm_IsActiveRow,
		ApDbScm_CreatedUserId,
		ApDbScm_CreatedDate,
		ApDbScm_UserId,
		ApDbScm_LastModifiedDate
				)
VALUES	(
		@ApDbScm_Id, 
		@ApDbScm_Ap_Id, 
		RTRIM(LTRIM(@ApDbScm_Name)), 
		RTRIM(LTRIM(@ApDbScm_Desc)), 
		@ApDbScm_IsActiveRow, 
		@ApDbScm_UserId, 
		GETDATE(), 
		@ApDbScm_UserId, 
		GETDATE()
				)
                
    END

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data

		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_Id', 'uniqueidentifier', @ApDbScm_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_Ap_Id', 'uniqueidentifier', @ApDbScm_Ap_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_Name', 'varchar', @ApDbScm_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_Desc', 'varchar', @ApDbScm_Desc, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_IsActiveRow', 'bit', @ApDbScm_IsActiveRow, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_UserId', 'uniqueidentifier', @ApDbScm_UserId, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 8;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcAppDbSchema_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcAppDbSchema_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcAppDbSchema_put', 'spWcAppDbSchema_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcAppDbSchema_put', 'spWcAppDbSchema_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcAppDbSchema_put', 'spWcAppDbSchema_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcAppDbSchema_put', 'spWcAppDbSchema_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcAppDbSchema_put', 'spWcAppDbSchema_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcAppDbSchema_put]
-- Script for this SP was created on: 1/6/2018 10:57:04 PM
(
@ApDbScm_Id uniqueidentifier = NULL OUTPUT,
@ApDbScm_Ap_Id uniqueidentifier,
@ApDbScm_Name varchar(50),
@ApDbScm_Desc varchar(200),
@ApDbScm_IsActiveRow bit,
@ApDbScm_UserId uniqueidentifier,
@ApDbScm_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@ApDbScm_Id Is NULL)
	BEGIN
		SET @ApDbScm_Id = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcAppDbSchema WHERE ApDbScm_Id = @ApDbScm_Id)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcAppDbSchema_putInsert'
		EXEC spWcAppDbSchema_putInsert
		@ApDbScm_Id,
		@ApDbScm_Ap_Id,
		@ApDbScm_Name,
		@ApDbScm_Desc,
		@ApDbScm_IsActiveRow,
		@ApDbScm_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @ApDbScm_Stamp =( SELECT ApDbScm_Stamp FROM wcAppDbSchema WHERE (ApDbScm_Id = @ApDbScm_Id))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcAppDbSchema WHERE (ApDbScm_Id = @ApDbScm_Id) AND (ApDbScm_Stamp = @ApDbScm_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcAppDbSchema_putUpdate'
				EXEC spWcAppDbSchema_putUpdate
				@ApDbScm_Id,
				@ApDbScm_Ap_Id,
				@ApDbScm_Name,
				@ApDbScm_Desc,
				@ApDbScm_IsActiveRow,
				@ApDbScm_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @ApDbScm_Stamp =( SELECT ApDbScm_Stamp FROM wcAppDbSchema WHERE (ApDbScm_Id = @ApDbScm_Id))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_Id', 'uniqueidentifier', @ApDbScm_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_Ap_Id', 'uniqueidentifier', @ApDbScm_Ap_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_Name', 'varchar', @ApDbScm_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_Desc', 'varchar', @ApDbScm_Desc, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_IsActiveRow', 'bit', @ApDbScm_IsActiveRow, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApDbScm_UserId', 'uniqueidentifier', @ApDbScm_UserId, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 8;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcAppDbSchema_putIsDeleted
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putIsDeleted]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putIsDeleted__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcAppDbSchema_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcAppDbSchema_putIsDeleted__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcAppDbSchema_putIsDeleted', 'spWcAppDbSchema_putIsDeleted__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcAppDbSchema_putIsDeleted', 'spWcAppDbSchema_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcAppDbSchema_putIsDeleted', 'spWcAppDbSchema_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcAppDbSchema_putIsDeleted', 'spWcAppDbSchema_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcAppDbSchema_putIsDeleted', 'spWcAppDbSchema_putIsDeleted__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcAppDbSchema_putIsDeleted]
-- Script for this SP was created on: 1/6/2018 10:57:04 PM
(
@Id UniqueIdentifier,
@IsDeleted bit,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

BEGIN TRANSACTION;
BEGIN TRY
    BEGIN

	   UPDATE       wcAppDbSchema
	   SET          ApDbScm_IsDeleted = @IsDeleted
	   WHERE        (ApDbScm_Id = @Id)

	END
    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END


END TRY


BEGIN CATCH
    SET @Success = 0
    IF @TranCount = 0
    ROLLBACK TRANSACTION;

    SET @ErrorLogId = NewID()
    INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
    VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
    --  Log Stored Procedure Parameter Values
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Id', 'uniqueidentifier', @Id, 1;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@IsDeleted', 'bit', @IsDeleted, 2;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 3;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 4;

END CATCH
	
RETURN





--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcAppDbSchema')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcAppDbSchema', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


