-- wcStoredProcParamValue


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcStoredProcParamValue

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValue_lst
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_lst]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_lst__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_lst__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValue_lst__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValue_lst', 'spWcStoredProcParamValue_lst__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValue_lst', 'spWcStoredProcParamValue_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValue_lst', 'spWcStoredProcParamValue_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValue_lst', 'spWcStoredProcParamValue_lst__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValue_lst', 'spWcStoredProcParamValue_lst__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValue_lst]
-- Script for this SP was created on: 12/26/2017 3:50:05 PM
(
@SpPV_SpPVGrp_Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
SpPV_Id,
SpPV_SpPVGrp_Id,
SpPV_SpP_Id,
SpP_Name AS SpPV_SpP_Id_TextField,

SpPV_Value,
SpPV_Notes,
SpPV_IsActiveRow,
SpPV_IsDeleted,
SpPV_CreatedUserId,
SpPV_CreatedDate,
SpPV_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

SpPV_LastModifiedDate,
SpPV_Stamp


FROM 		wcStoredProcParamValue LEFT OUTER JOIN
                tbPerson ON wcStoredProcParamValue.SpPV_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcStoredProcParam ON wcStoredProcParamValue.SpPV_SpP_Id = wcStoredProcParam.SpP_ID


WHERE   (SpPV_SpPVGrp_Id = @SpPV_SpPVGrp_Id) AND (SpPV_IsDeleted = 0)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValue_row
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_row]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_row__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_row__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValue_row__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValue_row', 'spWcStoredProcParamValue_row__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValue_row', 'spWcStoredProcParamValue_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValue_row', 'spWcStoredProcParamValue_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValue_row', 'spWcStoredProcParamValue_row__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValue_row', 'spWcStoredProcParamValue_row__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValue_row]
-- Script for this SP was created on: 12/26/2017 3:50:05 PM
(
@Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
SpPV_Id,
SpPV_SpPVGrp_Id,
SpPV_SpP_Id,
SpP_Name AS SpPV_SpP_Id_TextField,

SpPV_Value,
SpPV_Notes,
SpPV_IsActiveRow,
SpPV_IsDeleted,
SpPV_CreatedUserId,
SpPV_CreatedDate,
SpPV_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

SpPV_LastModifiedDate,
SpPV_Stamp


FROM 		wcStoredProcParamValue LEFT OUTER JOIN
                tbPerson ON wcStoredProcParamValue.SpPV_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcStoredProcParam ON wcStoredProcParamValue.SpPV_SpP_Id = wcStoredProcParam.SpP_ID


WHERE   (SpPV_Id = @Id)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValue_lstAll
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_lstAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_lstAll__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValue_lstAll__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValue_lstAll', 'spWcStoredProcParamValue_lstAll__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValue_lstAll', 'spWcStoredProcParamValue_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValue_lstAll', 'spWcStoredProcParamValue_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValue_lstAll', 'spWcStoredProcParamValue_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValue_lstAll', 'spWcStoredProcParamValue_lstAll__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValue_lstAll]
-- Script for this SP was created on: 12/26/2017 3:50:05 PM
AS
SET NOCOUNT ON

SELECT 	
SpPV_Id,
SpPV_SpPVGrp_Id,
SpPV_SpP_Id,
SpP_Name AS SpPV_SpP_Id_TextField,

SpPV_Value,
SpPV_Notes,
SpPV_IsActiveRow,
SpPV_IsDeleted,
SpPV_CreatedUserId,
SpPV_CreatedDate,
SpPV_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

SpPV_LastModifiedDate,
SpPV_Stamp


FROM 		wcStoredProcParamValue LEFT OUTER JOIN
                tbPerson ON wcStoredProcParamValue.SpPV_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcStoredProcParam ON wcStoredProcParamValue.SpPV_SpP_Id = wcStoredProcParam.SpP_ID

WHERE   (SpPV_IsDeleted = 0)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValue_putInsert
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putInsert__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValue_putInsert__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValue_putInsert', 'spWcStoredProcParamValue_putInsert__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValue_putInsert', 'spWcStoredProcParamValue_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValue_putInsert', 'spWcStoredProcParamValue_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValue_putInsert', 'spWcStoredProcParamValue_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValue_putInsert', 'spWcStoredProcParamValue_putInsert__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValue_putInsert]
-- Script for this SP was created on: 12/26/2017 3:50:06 PM
(
@SpPV_Id uniqueidentifier = NULL OUTPUT,
@SpPV_SpPVGrp_Id uniqueidentifier,
@SpPV_SpP_Id uniqueidentifier,
@SpPV_Value varchar(500),
@SpPV_Notes varchar(255),
@SpPV_IsActiveRow bit,
@SpPV_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
      SET @ErrMsg = 'Attempting to insert into wcStoredProcParamValue'
      INSERT INTO wcStoredProcParamValue
          (
		SpPV_Id,
		SpPV_SpPVGrp_Id,
		SpPV_SpP_Id,
		SpPV_Value,
		SpPV_Notes,
		SpPV_IsActiveRow,
		SpPV_CreatedUserId,
		SpPV_CreatedDate,
		SpPV_UserId,
		SpPV_LastModifiedDate
				)
VALUES	(
		@SpPV_Id, 
		@SpPV_SpPVGrp_Id, 
		@SpPV_SpP_Id, 
		RTRIM(LTRIM(@SpPV_Value)), 
		RTRIM(LTRIM(@SpPV_Notes)), 
		@SpPV_IsActiveRow, 
		@SpPV_UserId, 
		GETDATE(), 
		@SpPV_UserId, 
		GETDATE()
				)
                
    END

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data

		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_Id', 'uniqueidentifier', @SpPV_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_SpPVGrp_Id', 'uniqueidentifier', @SpPV_SpPVGrp_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_SpP_Id', 'uniqueidentifier', @SpPV_SpP_Id, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_Value', 'varchar', @SpPV_Value, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_Notes', 'varchar', @SpPV_Notes, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_IsActiveRow', 'bit', @SpPV_IsActiveRow, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_UserId', 'uniqueidentifier', @SpPV_UserId, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 9;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValue_putUpdate
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putUpdate__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValue_putUpdate__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValue_putUpdate', 'spWcStoredProcParamValue_putUpdate__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValue_putUpdate', 'spWcStoredProcParamValue_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValue_putUpdate', 'spWcStoredProcParamValue_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValue_putUpdate', 'spWcStoredProcParamValue_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValue_putUpdate', 'spWcStoredProcParamValue_putUpdate__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValue_putUpdate]
-- Script for this SP was created on: 12/26/2017 3:50:06 PM
(
@SpPV_Id uniqueidentifier = NULL OUTPUT,
@SpPV_SpPVGrp_Id uniqueidentifier,
@SpPV_SpP_Id uniqueidentifier,
@SpPV_Value varchar(500),
@SpPV_Notes varchar(255),
@SpPV_IsActiveRow bit,
@SpPV_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
	IF @TranCount > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
		SET @ErrMsg = 'Attempting to update wcStoredProcParamValue'
		UPDATE	wcStoredProcParamValue
		SET
		SpPV_SpP_Id = @SpPV_SpP_Id,
		SpPV_Value = RTRIM(LTRIM(@SpPV_Value)),
		SpPV_Notes = RTRIM(LTRIM(@SpPV_Notes)),
		SpPV_IsActiveRow = @SpPV_IsActiveRow,
		SpPV_UserId = @SpPV_UserId,
		SpPV_LastModifiedDate = GETDATE()
		WHERE	(SpPV_Id=@SpPV_Id)
	END

	SET @TranCount = @@TRANCOUNT;
	IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_Id', 'uniqueidentifier', @SpPV_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_SpPVGrp_Id', 'uniqueidentifier', @SpPV_SpPVGrp_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_SpP_Id', 'uniqueidentifier', @SpPV_SpP_Id, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_Value', 'varchar', @SpPV_Value, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_Notes', 'varchar', @SpPV_Notes, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_IsActiveRow', 'bit', @SpPV_IsActiveRow, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_UserId', 'uniqueidentifier', @SpPV_UserId, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 9;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValue_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValue_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValue_put', 'spWcStoredProcParamValue_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValue_put', 'spWcStoredProcParamValue_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValue_put', 'spWcStoredProcParamValue_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValue_put', 'spWcStoredProcParamValue_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValue_put', 'spWcStoredProcParamValue_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValue_put]
-- Script for this SP was created on: 12/26/2017 3:50:06 PM
(
@SpPV_Id uniqueidentifier = NULL OUTPUT,
@SpPV_SpPVGrp_Id uniqueidentifier,
@SpPV_SpP_Id uniqueidentifier,
@SpPV_Value varchar(500),
@SpPV_Notes varchar(255),
@SpPV_IsActiveRow bit,
@SpPV_UserId uniqueidentifier,
@SpPV_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@SpPV_Id Is NULL)
	BEGIN
		SET @SpPV_Id = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcStoredProcParamValue WHERE SpPV_Id = @SpPV_Id)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcStoredProcParamValue_putInsert'
		EXEC spWcStoredProcParamValue_putInsert
		@SpPV_Id,
		@SpPV_SpPVGrp_Id,
		@SpPV_SpP_Id,
		@SpPV_Value,
		@SpPV_Notes,
		@SpPV_IsActiveRow,
		@SpPV_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @SpPV_Stamp =( SELECT SpPV_Stamp FROM wcStoredProcParamValue WHERE (SpPV_Id = @SpPV_Id))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcStoredProcParamValue WHERE (SpPV_Id = @SpPV_Id) AND (SpPV_Stamp = @SpPV_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcStoredProcParamValue_putUpdate'
				EXEC spWcStoredProcParamValue_putUpdate
				@SpPV_Id,
				@SpPV_SpPVGrp_Id,
				@SpPV_SpP_Id,
				@SpPV_Value,
				@SpPV_Notes,
				@SpPV_IsActiveRow,
				@SpPV_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @SpPV_Stamp =( SELECT SpPV_Stamp FROM wcStoredProcParamValue WHERE (SpPV_Id = @SpPV_Id))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_Id', 'uniqueidentifier', @SpPV_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_SpPVGrp_Id', 'uniqueidentifier', @SpPV_SpPVGrp_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_SpP_Id', 'uniqueidentifier', @SpPV_SpP_Id, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_Value', 'varchar', @SpPV_Value, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_Notes', 'varchar', @SpPV_Notes, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_IsActiveRow', 'bit', @SpPV_IsActiveRow, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPV_UserId', 'uniqueidentifier', @SpPV_UserId, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 9;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValue_putIsDeleted
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putIsDeleted]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putIsDeleted__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValue_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValue_putIsDeleted__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValue_putIsDeleted', 'spWcStoredProcParamValue_putIsDeleted__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValue_putIsDeleted', 'spWcStoredProcParamValue_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValue_putIsDeleted', 'spWcStoredProcParamValue_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValue_putIsDeleted', 'spWcStoredProcParamValue_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValue_putIsDeleted', 'spWcStoredProcParamValue_putIsDeleted__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValue_putIsDeleted]
-- Script for this SP was created on: 12/26/2017 3:50:06 PM
(
@Id UniqueIdentifier,
@IsDeleted bit,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

BEGIN TRANSACTION;
BEGIN TRY
    BEGIN

	   UPDATE       wcStoredProcParamValue
	   SET          SpPV_IsDeleted = @IsDeleted
	   WHERE        (SpPV_Id = @Id)

	END
    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END


END TRY


BEGIN CATCH
    SET @Success = 0
    IF @TranCount = 0
    ROLLBACK TRANSACTION;

    SET @ErrorLogId = NewID()
    INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
    VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
    --  Log Stored Procedure Parameter Values
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Id', 'uniqueidentifier', @Id, 1;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@IsDeleted', 'bit', @IsDeleted, 2;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 3;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 4;

END CATCH
	
RETURN





--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcStoredProcParamValue')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcStoredProcParamValue', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


